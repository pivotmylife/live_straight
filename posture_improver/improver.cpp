#include "improver.h"
#include "imp2.h"
#include "APMain.h"

#include "QuImageManager.h"
#include "QuApExtensions.h"
#include "QuSoundManager.h"


int main()
{
	QuStupidPointer<QuGlobalSettings> s = new QuGlobalTypeSettings<QuApRawSoundManager<44100>,QuImageManager,QuStdFileManager>();
	INITIALIZE_GAME<DiaLaunch>(s);
}