#include "imp2.h"
#include "QuGlobals.h"
#include "s3eVibra.h"
#include <algorithm>

SavedSettings * SavedSettings::mSelf = NULL;

//-----------
//helper nonsense
//-----------
ButtonGroupManager::ButtonGroupManager():mLast(-1)
{
	mClickDownSound = G_SOUND_MANAGER.getSound("sounds/clickDown.raw");
	mClickUpSound = G_SOUND_MANAGER.getSound("sounds/clickUp.raw");
	//mClickOffSound = G_SOUND_MANAGER.getSound("sounds/silence.raw");
}
ButtonGroupManager::~ButtonGroupManager()
{
}
void ButtonGroupManager::addButton(QuStupidPointer<QuEnlargeableRelScreenFloatBoxButton> aButton, QuStupidPointer<QuBaseImage> image)
{ mButtonGroup.addButton(aButton,image); }
void ButtonGroupManager::singleMotion(QuScreenCoord crd, GDeviceRotation aRot)
{ 
	int k = mButtonGroup.click(crd,aRot);
	if(k != mLast && mLast >= 0)
		mButtonGroup.getButton(mLast).enlargeOffset(false);
	else if(mLast >= 0)
		mButtonGroup.getButton(mLast).enlargeOffset(true,0.05f);
}
void ButtonGroupManager::singleDown(QuScreenCoord crd, GDeviceRotation aRot)
{
	mLast = mButtonGroup.click(crd,aRot);
	if(mLast != -1)
	{
		//mClickDownSound->play();
		mButtonGroup.getButton(mLast).enlargeOffset(true,0.05f);
	}
	//std::cout << "down on button " << mLast << std::endl;
}
int ButtonGroupManager::singleUp(QuScreenCoord crd, GDeviceRotation aRot)
{
	//TEMP
	//return -1;

	if(mLast == -1)
		return -1;
	if(mLast == mButtonGroup.click(crd,aRot))
	{
		mClickUpSound->play();
		mButtonGroup.getButton(mLast).enlargeOffset(false);
		return mLast;
	}
	//mClickOffSound->play();
	return -1;
}


//-----------
//LAUNCH DIALOG
//links to FIRST LAUNCH and DEFAULT dialogs
//-----------
void DiaLaunch::initialize()
{
	G_SOUND_MANAGER.getSound("sounds/clickDown.raw")->setVolume(0.1f);
	G_SOUND_MANAGER.getSound("sounds/clickUp.raw")->setVolume(0.1f);
	QuStupidPointer<QuBaseManager> next;
	if(!SavedSettings::getRef().attemptRead())
		next = QuStupidPointer<QuBaseManager>(new DiaFirstLaunch());
	else
	{
		next = QuStupidPointer<QuBaseManager>(new DiaDefault());
	}
	next->initialize();
	G_GAME_GLOBAL.setManager(next);
}

//-----------
//FIRST LAUNCH DIALOG
//links to AUTO SET MENU and DEFAULT dialogs
//-----------
DiaFirstLaunch::DiaFirstLaunch():mDelay(0,10){}
void DiaFirstLaunch::initialize()
{
	std::cout << "initializing DiaFirstLaunch, free memory is: " << s3eMemoryGetInt(S3E_MEMORY_FREE) << std::endl;
	BgImageManager::initialize();
	mTitle.setImage(G_IMAGE_MANAGER.getFlyImage("images/FL.png"));
	
	mButtonGroup.addButton(new QuEnlargeableRelScreenFloatBoxButton(QuBox2<float>(0.35f,0.2f,0.3f,0.135f*G_SCREEN_RATIO)),G_IMAGE_MANAGER.getFlyImage("images/FL_auto.png"));	//id 0: to auto set
	mButtonGroup.addButton(new QuEnlargeableRelScreenFloatBoxButton(QuBox2<float>(0.35f,0.1f,0.3f,0.08f*G_SCREEN_RATIO)),G_IMAGE_MANAGER.getFlyImage("images/FL_default.png"));	//id 1: to default
}
void DiaFirstLaunch::update()
{
	mDelay++;
	BgImageManager::update();
}
void DiaFirstLaunch::singleUp(int button, QuScreenCoord crd)
{
	if(mDelay.isExpired())
	{
		switch(mButtonGroup.singleUp(crd, mCamera.getRotation()))
		{
			case -1:
				break;
			case 0:
			{
				QuStupidPointer<QuBaseManager> next = QuStupidPointer<DiaAutoSetMenu>(new DiaAutoSetMenu()).safeCast<QuBaseManager>();
				next->initialize();
				G_GAME_GLOBAL.setManager(next);
				break;
			}
			case 1:
			{
				QuStupidPointer<QuBaseManager> next = QuStupidPointer<DiaDefault>(new DiaDefault()).safeCast<QuBaseManager>();
				next->initialize();
				G_GAME_GLOBAL.setManager(next);
				break;
			}
			default:
				break;	
		}
	}
}


//-----------
//AUTO SET MENU DIALOG
//links to AUTO SET and DEFAULT dialogs
//-----------
DiaAutoSetMenu::DiaAutoSetMenu():mInputDelay(0,10),mLaunchTimer(0,150){}
void DiaAutoSetMenu::initialize()
{
	std::cout << "initializing DiaAutoSetMenu, free memory is: " << s3eMemoryGetInt(S3E_MEMORY_FREE) << std::endl;
	BgImageManager::initialize();
	mTitle.setImage(G_IMAGE_MANAGER.getFlyImage("images/AUTO.png"));
	
	//TODO fix button position
	mButtonGroup.addButton(new QuEnlargeableRelScreenFloatBoxButton(QuBox2<float>(0.3f,0.15f,0.4f,0.215f*G_SCREEN_RATIO)),G_IMAGE_MANAGER.getFlyImage("images/AUTO_begin.png"));	//id 0: to auto set
	mButtonGroup.addButton(new QuEnlargeableRelScreenFloatBoxButton(QuBox2<float>(0.3f,0.05f,0.4f,0.085f*G_SCREEN_RATIO)),G_IMAGE_MANAGER.getFlyImage("images/AUTO_return.png"));	//id 1: to default
}
void DiaAutoSetMenu::update()
{
	

	if(mLaunchTimer.getLinear() > 0)
	{
		mLaunchTimer.update();
		if(mLaunchTimer.isExpired())
		{
			QuStupidPointer<QuBaseManager> next(QuStupidPointer<DiaAutoSetConfigure>(new DiaAutoSetConfigure()).safeCast<QuBaseManager>());
			next->initialize();
			G_GAME_GLOBAL.setManager(next);
		}
		else
		{
			mTitle.setImage(G_IMAGE_MANAGER.getFlyImage("images/AUTO_pocket.png"));
		}
	}

	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	drawBackground();
	if(mLaunchTimer.getLinear() == 0)
		mButtonGroup.getButtonGroup().drawAllButtonsWithImages(mCamera.getRotation());

	mInputDelay++;
}
void DiaAutoSetMenu::singleUp(int button, QuScreenCoord crd)
{
	if(mInputDelay.isExpired())
	{
		//TODO make sure switch does what you think it does
		switch(mButtonGroup.singleUp(crd, mCamera.getRotation()))
		{
			case -1:
				break;
			case 0:
				{
					mLaunchTimer.update();
					break;
				}
			case 1:
				{
					QuStupidPointer<QuBaseManager> next = QuStupidPointer<DiaDefault>(new DiaDefault()).safeCast<QuBaseManager>();
					next->initialize();
					G_GAME_GLOBAL.setManager(next);
					break;
				}
			default:
				break;
		}
	}
	else
		mButtonGroup.singleUp(QuScreenCoord(-999,-999,1,1),G_DR_0);
}

//-----------
//AUTO SET CONFIGURE DIALOG
//links to DEFAULT dialog
//-----------
DiaAutoSetConfigure::DiaAutoSetConfigure():mFailTimer(0,AUTO_SET_CONFIGURE_FAIL_TIME){}

void DiaAutoSetConfigure::initialize()
{
	std::cout << "initializing DiaAutoSetConfigure, free memory is: " << s3eMemoryGetInt(S3E_MEMORY_FREE) << std::endl;
	BgImageManager::initialize();
	mTitle.setImage(G_IMAGE_MANAGER.getFlyImage("images/AUTOCONFIGURE.png"));
	//TODO make beep, beep, beep, beep, beep, boop (like mario kart ready set go sounds)
	std::cout << "DiaAutoSetConfigure initialize done, free memory is: " << s3eMemoryGetInt(S3E_MEMORY_FREE) << std::endl;
}

void DiaAutoSetConfigure::update()
{
	mFailTimer.update(G_GAME_GLOBAL.getDeltaMillis());
	//prune till last five seconds
	mSignal.update(2*TILT_OSC_BACKTRACK);
	//std::cout << mSignal.mAccel.getOscillation(G_GAME_GLOBAL.getMillis() - TILT_OSC_BACKTRACK,G_GAME_GLOBAL.getMillis())*1000 << std::endl;
	//std::cout << mSignal.getTilt(TILT_OSC_BACKTRACK) << std::endl;
	//std::cout << "updated signal" << std::endl;

	if(mSignal.mAccel.hasValues() && mSignal.mAccel.getSignalTotalTime() > TILT_OSC_BACKTRACK)
	{
		//std::cout << "got signal total time" << std::endl;
		if(mFailTimer.isExpired() || mSignal.mAccel.getOscillation(G_GAME_GLOBAL.getMillis() - TILT_OSC_BACKTRACK,G_GAME_GLOBAL.getMillis())*1000 < TILT_OSC_THRESHOLD) 
		{
			//std::cout << "good to go!" << std::endl;
			float angle = mSignal.getTilt(TILT_OSC_BACKTRACK);
			SavedSettings::getRef().mLeft = quClamp(angle - 0.1f,-PI/2,PI/2);
			SavedSettings::getRef().mRight = quClamp(angle + 0.1f,-PI/2,PI/2);
			QuStupidPointer<QuBaseManager> next = QuStupidPointer<DiaAutoSetDone>(new DiaAutoSetDone()).safeCast<QuBaseManager>();
			next->initialize();
			G_GAME_GLOBAL.setManager(next);
		}
	}
	BgImageManager::update();
}

void DiaAutoSetConfigure::singleUp(int button, QuScreenCoord crd)
{
}

//-----------
//AUTO SET DONE
//-----------
DiaAutoSetDone::DiaAutoSetDone():mLaunchTimer(0,30)
{}
void DiaAutoSetDone::initialize()
{
	s3eVibraVibrate(255,1500);
}
void DiaAutoSetDone::update()
{
	QuStupidPointer<QuBaseManager> next = QuStupidPointer<DiaDefault>(new DiaDefault()).safeCast<QuBaseManager>();
	next->initialize();
	G_GAME_GLOBAL.setManager(next);
}
void DiaAutoSetDone::singleDown(int button, QuScreenCoord crd)
{
	if(mLaunchTimer.isExpired())
		mButtonGroup.mClickDownSound->play();
}
void DiaAutoSetDone::singleUp(int button, QuScreenCoord crd)
{
	if(mLaunchTimer.isExpired())
		mButtonGroup.mClickUpSound->play();
}

//-----------
//DEFAULT DIALOG
//links to AUTO SET MENU dialog
//-----------
void DiaDefault::initialize()
{
	std::cout << "initializing DiaDefault, free memory is: " << s3eMemoryGetInt(S3E_MEMORY_FREE) << std::endl;

	//get the settings here
	//NOTE default values are set in SavedSettings
	mLeft = SavedSettings::getRef().mLeft;
	mRight = SavedSettings::getRef().mRight;
	mSlider.setSliderPosition(SavedSettings::getRef().mGrace);
	mAudio = SavedSettings::getRef().mAudio;
	mVibra = SavedSettings::getRef().mVibra;

	mGraceTimer.setTargetAndReset(mSlider.getSliderPosition());
	mGraceTimer.expire();
		
	
	BgImageManager::initialize();
	mTitle.setImage(G_IMAGE_MANAGER.getFlyImage("images/DEFAULT.png"));
	mAlert = G_SOUND_MANAGER.getSound("sounds/ERROR.raw");
	
	//TODO set button position
	mButtonGroup.addButton(new QuEnlargeableRelScreenFloatBoxButton(QuBox2<float>(0.82f,0.0f,0.16f,0.16f*G_SCREEN_RATIO)),G_IMAGE_MANAGER.getFlyImage("images/DEFAULT_auto.png"));	//id 0: to auto set
	mButtonGroup.addButton(new QuEnlargeableRelScreenFloatBoxButton(QuBox2<float>(0.5f,0.0f,0.16f,0.16f*G_SCREEN_RATIO)));	//id 1: to sound set
	mButtonGroup.addButton(new QuEnlargeableRelScreenFloatBoxButton(QuBox2<float>(0.66f,0.0f,0.16f,0.16f*G_SCREEN_RATIO)));	//id 2: vibra set
	updateImages();
}

void DiaDefault::updateImages()
{
	if(mAudio)
		mButtonGroup.getButtonGroup().setImage(1,G_IMAGE_MANAGER.getFlyImage("images/DEFAULT_sound_on.png"));
	else 
		mButtonGroup.getButtonGroup().setImage(1,G_IMAGE_MANAGER.getFlyImage("images/DEFAULT_sound_off.png"));

	if(mVibra)
		mButtonGroup.getButtonGroup().setImage(2,G_IMAGE_MANAGER.getFlyImage("images/DEFAULT_vibra_on.png"));
	else
		mButtonGroup.getButtonGroup().setImage(2,G_IMAGE_MANAGER.getFlyImage("images/DEFAULT_vibra_off.png"));


}
void DiaDefault::update()
{
	mGraceTimer.update(G_GAME_GLOBAL.getDeltaMillis());
	mGraceTimer.clamp();
	BgImageManager::update();
	mSlider.draw(mCamera.getRotation());

	mSignal.update(GRACE_MAX);
	drawCone(mSignal.getTilt(1000));

	float angle = mSignal.getTilt(mSlider.getSliderPosition()+IPHONE_HARD_FIX_GRACE_OFFSET);
	float shortAngle = mSignal.getTilt(500);
	//std::cout << angle << " " << shortAngle << std::endl;
	if((angle > mRight || angle < mLeft) && (shortAngle > mRight || shortAngle < mLeft))
	{
		if(mGraceTimer.isExpired())
		{
			if(mAudio)
				playAlert();
			if(mVibra)
				vibrate();
			mWasAlert = true;
		}
	}
	else
	{
		//we are good now go back on grace timer
		mGraceTimer.update(-G_GAME_GLOBAL.getDeltaMillis()*3);
		if(mWasAlert)
		{
			//we prune back 3 seconds in this case, 3 seconds is totally arbitrary
			mSignal.prune(3000);
			if(mGraceTimer.getTimeUntilEnd() > 2500 || mGraceTimer.getTimeTotal() <= 2500)
			{
				//TODO different sound
				//BgImageManager::mButtonGroup.mClickUpSound->play();
				mWasAlert = false;
			}
			
		}
	}
}

void DiaDefault::drawCone(float angle)
{
	QuBasicCamera cam(G_SCREEN_WIDTH);
	float w = cam.getDrawingSurfaceWidth();
	float theta = atan(w/1.0);
	QuColor col(141/255.0f,198/255.0f,63/255.0f,0.46f);

	std::vector<float> angles;
	angles.push_back(mLeft);
	if(-theta > mLeft && -theta < mRight)
		angles.push_back(-theta);
	if(theta > mLeft && theta < mRight)
		angles.push_back(theta);
	angles.push_back(mRight);

	QuDrawObject cone;
	if(angles.size() == 4)
	{
		cone.setCount(12);
		float verts[] = { 0,0,0, w,1,0, 0,1,0,    0,0,0, w,1,0, 2*sin(mRight),2*cos(mRight),0, 0,0,0, -w,1,0, 0,1,0,    0,0,0, -w,1,0, 2*sin(mLeft),2*cos(mLeft),0 };
		cone.loadVertices(ARRAYN<float,3*12>(verts));
	}
	else
	{
		cone.setCount(3);
		float verts[] =	{ 0,0,0, 10*sin(mRight),10*cos(mRight),0, 2*sin(mLeft),2*cos(mLeft),0 };
		cone.loadVertices(ARRAYN<float,3*12>(verts));
	}

	QuDrawObject line;
	line.setCount(2);
	float verts[] = { 0,0,0, 10*sin(angle),10*cos(angle),0};
	line.loadVertices(ARRAYN<float,3*2>(verts));
	if(angle >= mLeft && angle <= mRight)
		line.loadColor(QuColor(0,0,1));
	else line.loadColor(QuColor(1,0.2f,0.2f));
	line.setDrawType(GL_LINES);

	cone.loadColor(col);
	cone.setDrawType(GL_TRIANGLES);
	glPushMatrix();
	cam.setScene();
	cone.autoDraw();
	line.autoDraw();
	glPopMatrix();
}

void DiaDefault::singleUp(int button, QuScreenCoord crd)
{
	mMouse = false;
	//TODO finish button functionality
	switch(mButtonGroup.singleUp(crd,mCamera.getRotation()))
	{
		case -1:
		break;
		case 0: //goto auto set dialog
			{
				QuStupidPointer<QuBaseManager> next(QuStupidPointer<DiaAutoSetMenu>(new DiaAutoSetMenu()).safeCast<QuBaseManager>());
				next->initialize();
				G_GAME_GLOBAL.setManager(next);
				break;
			}
		case 1: 
			mAudio = !mAudio;
			if(mAudio)
				playAlert();
			break;
		case 2: 
			mVibra = !mVibra;
			if(mVibra)
				vibrate();
			break;
		default:
			break;
	}
	updateImages();
	if(mSlider.mouseUp())
	{
		mGraceTimer.setTargetAndReset(mSlider.getSliderPosition());
	}
	SavedSettings::getRef().mGrace = mSlider.getSliderPosition();
	editByte = 0;
	saveConfig("improve.txt");
}

void DiaDefault::playAlert()
{
	if(!mAlert->isPlaying())
		mAlert->play();
}

void DiaDefault::vibrate()
{
	s3eVibraVibrate(255,500);
}

void DiaDefault::singleDown(int button, QuScreenCoord crd)
{
	BgImageManager::singleDown(button,crd);
	mSlider.mouseDown(crd);
	mMouse = true;
}

void DiaDefault::singleMotion(QuScreenCoord crd)
{
	BgImageManager::singleMotion(crd);
	mSlider.singleMotion(crd);
	QuFVector2<int> pos = crd.getIPosition();
	float angle = -(atan2((pos.y-G_SCREEN_HEIGHT/2.0f),(pos.x-G_SCREEN_WIDTH/2.0f))-PI/2.0f);

	if(editByte == 0 && mMouse)
	{
		float ri = quAbs(angle - mRight);
		float le = quAbs(angle - mLeft);
		if( ri <= le && ri < 0.2f)
			editByte = 2;
		else if(le <= ri && le < 0.2f)
			editByte = 1;
	}
	if(mMouse && angle > -PI/2 && angle < PI/2)
	{
		switch (editByte)
		{
		case 1:
			mLeft = quClamp<float>(angle,-PI/2.0f,mRight-0.02f);
			break;
		case 2:
			mRight = quClamp<float>(angle,mLeft+0.02f,PI/2.0f);
			break;
		default: break;
		}
	}
}

void DiaDefault::saveConfig(std::string aFilename)
{
	QuStupidPointer<QuOutStream> stream = G_FILE_MANAGER.getWriteStream("improve.txt");
	stream->GetStream() << mLeft << " " << mRight << " " << mSlider.getSliderPosition() << " " << (mAudio ? 1 : 0) << " " << (mVibra ? 1 : 0);
}