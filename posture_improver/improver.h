#pragma once
#include "QuGlobals.h"
#include "QuGameManager.h"
#include "QuInterface.h" 
#include "QuButton.h"
#include "QuClutils.h"
#include "s3eVibra.h"
#include "s3eFile.h"
#include "s3eDevice.h"
#include <sstream>
#include "QuMouse.h"

class DataWriter
{
	s3eFile * mFile;
	bool isInit;
public:
	DataWriter():isInit(false){}
	DataWriter(std::string aFilename):isInit(false)
	{
		openFile(aFilename);
	}
	~DataWriter()
	{
		close();
	}
	bool openFile(std::string aFilename)
	{
		quAssert(!isInit);
		mFile = s3eFileOpen(aFilename.c_str(),"w");
		if(mFile == NULL)
		{
			std::cout << "failed to open file " << aFilename << std::endl;
			isInit = false;
			return false;
		}
		isInit = true;
	}
	void operator<<(std::string aMsg)
	{
		if(isInit)
			write(aMsg);
	}
	void operator<<(float aMsg)
	{
		stringstream ss (stringstream::in | stringstream::out);
		ss << aMsg;
		string test = ss.str();
		write(test);
	}
	void operator<<(bool aEnd)
	{
		if(aEnd && isInit)
			write("\n");
	}
	void write(std::string aMsg)
	{
		if(isInit)
			s3eFilePrintf(mFile,"%s",aMsg);
	}
	void close()
	{
		if(!isInit)
			return;
		if( mFile != NULL )
			s3eFileClose(mFile);
		mFile = NULL;
	}

};

class ImproverManager : public QuBaseManager
{
	float mThresh;
	bool mMouseDown;
	QuTimer mIdleTimer;
	QuAlgebraicTimedSignal<float> mSignal;
	DataWriter mWrite;
	QuStupidPointer<QuSoundStructInterface> mErrorSound;
	QuStupidPointer<QuSoundStructInterface> mClickSound;


	QuBasicCamera mCamera;
	QuImageDrawObject mBackground;
	QuImageDrawObject mPhone;

	//QuGraphDrawer mGraphDrawer;

	bool mIsSound,mIsVibra;//,mIsGraph;
	QuAbsScreenIntBoxButton mSoundButton;
	QuAbsScreenIntBoxButton mVibraButton;
	QuAbsScreenIntBoxButton mGraphButton;

	uint64 mTimeSinceStart;

	QuMultiTouchManager mMulti;
public:
	ImproverManager():mIdleTimer(0,100),mSignal(3600000),mCamera(G_SCREEN_WIDTH,G_SCREEN_HEIGHT,G_SCREEN_HEIGHT){}
	void initialize()
	{
		glClearColor(0,0,0,1);


		mThresh = 0.25;//15*ANGLES_TO_RADIANS;
		mMouseDown = false;

		//mIsGraph = false;
		mIsSound = mIsVibra = true;

		mClickSound = G_SOUND_MANAGER.loadSound("sounds/SELECT.raw");
		mErrorSound = G_SOUND_MANAGER.loadSound("sounds/ERROR.raw");

		mBackground.setImage(G_IMAGE_MANAGER.getFlyImage("images/mainbg.png"));
		mPhone.setImage(G_IMAGE_MANAGER.getFlyImage("images/phone.png"));

		G_IMAGE_MANAGER.getFlyImage("images/soundon.png");
		G_IMAGE_MANAGER.getFlyImage("images/soundoff.png");
		G_IMAGE_MANAGER.getFlyImage("images/vibraon.png");
		G_IMAGE_MANAGER.getFlyImage("images/vibraoff.png");
		
		mSoundButton.setBox(QuBox2<int>(10,10,64,64));
		mVibraButton.setBox(QuBox2<int>(10*2 + 64,10,64,64));

		//mGraphButton.setBox(QuBox2<int>(10*3 + 64*2,10,64,64));
		//mGraphDrawer.setHorizontalBounds(0,1);
		//mGraphDrawer.setVerticalBounds(3,-3);

		mTimeSinceStart = G_GAME_GLOBAL.getMillis();
	}
	void singleDown(int button, QuScreenCoord crd)
	{
		mIdleTimer.reset();
		mMouseDown = true;
	}
	void singleUp(int button, QuScreenCoord crd)
	{
		mMouseDown = false;
		if(mSoundButton.click(crd,G_DR_0))
		{
			mIsSound = !mIsSound;
			mClickSound->play();
		}
		if(mVibraButton.click(crd,G_DR_0))
		{
			mIsVibra = !mIsVibra;
			mClickSound->play();
		}

		/*GDeviceRotation rot = G_DR_0;
		if(mIsGraph)
		{
			rot = G_DEVICE_ROTATION;
			if(rot == G_DR_0 || rot == G_DR_180)
				rot = G_DR_90;
		}
		if(mGraphButton.click(crd,rot))
		{
			if(mIsGraph)
			{
				mGraphButton.setBox(QuBox2<int>(10*3 + 64*2,10,64,64));
			}
			else
			{
				//TODO set the graph button to someplace sensible
				//mGraphButton.setBox(QuBox2<int>(10*3 + 64*2,10,64,64));
			}
			mIsGraph = !mIsGraph;
			mClickSound->play();
		}*/
	}
	void singleMotion(QuScreenCoord prevCrd, QuScreenCoord crd)
	{
		if(mMouseDown)
		{
			//crd at G_DR_0 position right now, which is probably what we want (for now)
			QuVector2<float> change = crd.getFChange(prevCrd);
			mThresh = quClamp<float>(mThresh - (change.y-change.x)*0.3f,0,1);
			/*
			if(!mIsGraph)
				mThresh = quClamp<float>(mThresh - (change.y-change.x)*0.3f,0,1);
			else
			{
				//TODO this should keep track of what the old bounds were
				//mGraphDrawer.setHorizontalBounds(0,1);
			}*/
		}

	}

	void multiTouch(int button, bool state,  QuScreenCoord crd)
	{
		mMulti.multiTouch(button,state,crd);
	}

	void multiMotion(int button,  QuScreenCoord crd)
	{
		mMulti.multiMotion(button,crd);
	}

	void drawPhone()
	{
		glPushMatrix();
		mCamera.setScene();
		glPushMatrix();
		glScalef(G_SCREEN_WIDTH,G_SCREEN_HEIGHT,1);
		mBackground.autoDraw();
		glPopMatrix();

		glRotatef(-RADIANS_TO_ANGLES*asin(quClamp<double>(mThresh,-1,1)),0,0,1);
		glScalef(G_SCREEN_WIDTH*128/(float)640,G_SCREEN_HEIGHT*1024/(float)960,1);
		mPhone.autoDraw();

		glPopMatrix();
	}
	void writeFile()
	{
		mWrite << (float)G_GAME_GLOBAL.getMillis();
		mWrite << " ";
		mWrite << mSignal.weightedAverage(500);
		mWrite << true;	//\n
	}
	void update()
	{
		if(mIdleTimer.isExpired())
		{
			s3eDeviceBacklightOn();
			mIdleTimer.reset();
		}
		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
		//if(!mIdleTimer.isExpired())
		{
			drawPhone();
			mSoundButton.drawRotatedImage(G_IMAGE_MANAGER.getFlyImage(mIsSound ? "images/soundon.png" : "images/soundoff.png"));
			mVibraButton.drawRotatedImage(G_IMAGE_MANAGER.getFlyImage(mIsVibra ? "images/vibraon.png" : "images/vibraoff.png"));
			/*
			if(!mIsGraph)
			{
				drawPhone();
				mSoundButton.drawRotatedImage(G_IMAGE_MANAGER.getFlyImage(mIsSound ? "images/soundon.png" : "images/soundoff.png"));
				mVibraButton.drawRotatedImage(G_IMAGE_MANAGER.getFlyImage(mIsVibra ? "images/vibraon.png" : "images/vibraoff.png"));
				mGraphButton.drawRotatedImage(G_IMAGE_MANAGER.getFlyImage("images/graph.png"));
			}
			else
			{
				QuShapeDrawer shape;
				shape.drawRectangle(QuBox2<float>(0,0,1,1),QuColor(1,1,1,1));
				mGraphDrawer.setPoints(mSignal.getTruncatedList(50,0,5000),0.005f);
				mGraphDrawer.draw();
				GDeviceRotation rot = G_DEVICE_ROTATION;
				if(rot == G_DR_0 || rot == G_DR_180)
					rot = G_DR_90;
				mGraphButton.drawRotatedRect(QuColor(0,0,0,1),rot);
				//mGraphButton.drawRotatedImage(G_IMAGE_MANAGER.getFlyImage("images/angle.png"),rot);
			}*/
		}
		//vibrate and make sounds
		uint64 time =  G_GAME_GLOBAL.getMillis();
		mSignal.add(G_GAME_GLOBAL.getAccel().z,time-mTimeSinceStart);
		std::cout << G_GAME_GLOBAL.getAccel().z << std::endl;
		//writeFile();
		if((mSignal.size() > 20) && quAbs(mSignal.weightedAverage(3000)) > (mThresh) && ! quAbs(mSignal.weightedAverage(1000)) < (mThresh))
		{
			if(mIsVibra)
				s3eVibraVibrate(255,500);
			if(mIsSound)
				mErrorSound->play();
		}
		mTimeSinceStart = time;
		mIdleTimer++;
	}
};


class InstructionManager : public QuBaseManager
{
	QuTimer mDelay;
	QuStupidPointer<QuSoundStructInterface> mClickSound;
	QuImageDrawObject mTitle;
	QuAbsScreenIntBoxButton mButton;
	QuBasicCamera mCamera;
public:
	InstructionManager():mDelay(0,10),mCamera(G_SCREEN_WIDTH,G_SCREEN_HEIGHT,G_SCREEN_HEIGHT){}
	void initialize()
	{
		mTitle.setImage(G_IMAGE_MANAGER.getFlyImage("images/title.png"));
		mButton.setBox(QuBox2<int>::getCenterRelBox(QuBox2<float>(0.5,0.2,0.5,0.3),G_GAME_GLOBAL.getRotatedScreenDimensions(G_DR_0)));
		mClickSound = G_SOUND_MANAGER.loadSound("sounds/SELECT.raw");
	}
	void update()
	{
		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
		mDelay++;

		mCamera.setScene();
		glScalef(G_SCREEN_WIDTH,G_SCREEN_HEIGHT,1);
		glDisable(GL_LIGHTING);
		mTitle.autoDraw();
		mCamera.setScene();

		//mButton.drawRotatedRect(QuColor(0,0,0,.5));
	}
	void singleUp(int button, QuScreenCoord crd)
	{
		if(mButton.click(crd,G_DR_0) && mDelay.isExpired())
		{
			mClickSound->play();
			QuStupidPointer<QuBaseManager> next(new ImproverManager());
			next->initialize();
			G_GAME_GLOBAL.setManager(next);
		}
	}

};