#include "QuGlobals.h"
#include "QuGameManager.h"
#include "QuInterface.h" 
#include "QuButton.h"
#include "QuClutils.h"
#include "s3eVibra.h"
#include "s3eFile.h"
#include "s3eDevice.h"
#include <sstream>
#include "QuMouse.h"
#include "QuCMath.h"

//CONSTANTS
static const float TILT_OSC_THRESHOLD = 0.4f;
static const float TILT_OSC_BACKTRACK = 2500;
static const int GRACE_MAX = 30000;
static const int AUTO_SET_CONFIGURE_FAIL_TIME = 10000;
static const int IPHONE_HARD_FIX_GRACE_OFFSET = 100; //uggg
//singleton object where settings are stored
class SavedSettings
{
	static SavedSettings * mSelf;
	bool mRead;
public:
	float mLeft,mRight,mGrace;
	bool mAudio, mVibra;
	static void destroy()
	{
		if(mSelf != NULL)
			delete mSelf;
	}
	static SavedSettings & getRef()
	{
		//NOTE not thread safe...
		if(mSelf == NULL)
			mSelf = new SavedSettings();
		return *mSelf;
	}
	bool attemptRead()
	{
		QuStupidPointer<QuInStream> stream = G_FILE_MANAGER.getReadStream("improve.txt");
		if(stream.isNull())
		{
			std::cout << "could nod find improver.txt" << std::endl;
			mRead = false;
		}
		else if(stream->GetStream().eof()) //you don't really need this here TODO delet
		{
			std::cout << "reading improver.txt, found eof" << std::endl;
			mRead = false;
		}
		else
		{
			std::cout << "reading improver.txt..." << std::endl;

			/*while(!stream->GetStream().eof()){
				std::string in;
				stream->GetStream() >> in;
				std::cout << in << std::endl; }*/

			//hack version could crash...
			stream->GetStream() >> mLeft;
			stream->GetStream() >> mRight;
			stream->GetStream() >> mGrace;
			stream->GetStream() >> mAudio;
			stream->GetStream() >> mVibra;
			mRead = true;
		}
		stream.setNull();
		return mRead;
	}
	bool haveSettingsBeenRead() { return mRead; }
private:
	SavedSettings():mRead(false),mLeft(-0.1f),mRight(0.1f),mAudio(true),mVibra(true),mGrace(5000)
	{
		
	}
};

template<typename T, typename F = float>
class AccelSignal : public QuMapSignalBase<T,F>
{
public:
	//sloppy meanning it wont do interpolation
	//TODO write note sloppy version and clean this one up..
	virtual T getAverageValueOverRangeSloppy(const F & start, const F & end)
	{
		F timeTotal = 0;
		T valTotal = T(0);
		typename std::template map<F,T>::iterator it = getClosestIterator(start);
		typename std::template map<F,T>::iterator endit = getClosestIterator(end);
		float startTime = it->first;
		float lastTime = startTime;
		while(it != endit)
		{
			timeTotal += (it->first - lastTime);
			valTotal += it->second * (it->first - lastTime);
			lastTime = it->first;
			++it;
		}
		/*if((endit->first - startTime) == 0)
			return 0;*/
		if(timeTotal == 0) return 0;
		return valTotal * 1.0f/timeTotal;
	}

	virtual T getOscillation(const F & start, const F & end)
	{
		F timeTotal = 0;
		T valTotal = T(0);
		
		typename std::template map<F,T>::iterator it = getClosestIterator(start);
		T last = it->second;
		F lastTime = it->first;
		++it;
		for(; it != getClosestIterator(end); ++it)
		{
			timeTotal += (it->first-lastTime);
			valTotal += quAbs(it->second - last);
			last = it->second;
			lastTime = it->first;
		}
		return valTotal * 1/timeTotal;
	}
	/*
	virtual T getSignalTotalSum()
	{
		T r(0);
		F startTime;
		for(typename std::template map<F,T>::iterator it = QuMapSignalBase<T,F>::mSignal.begin(); it != QuMapSignalBase<T,F>::mSignal.end(); ++it)
			r += it->second * it->first; 
		return r/getSignalTotalTime();
	}*/
};

struct AccelMan
{
	AccelSignal<float> mAccel;
	AccelMan()
	{
	}
	void update(int toPrune = GRACE_MAX)
	{
		float accel = G_GAME_GLOBAL.getAccel().z;
		mAccel.addAbsolute(accel,G_GAME_GLOBAL.getMillis());
		int k = G_GAME_GLOBAL.getMillis() - toPrune;
		mAccel.prune(k);
	}
	float getTilt(int backtrack)
	{
		return mAccel.getAverageValueOverRangeSloppy(G_GAME_GLOBAL.getMillis()-backtrack, G_GAME_GLOBAL.getMillis()) * PI/2.0f;
	}
	void prune( int toPrune )
	{
		int k = G_GAME_GLOBAL.getMillis() - toPrune;
		mAccel.prune(k);
	}
};

struct GraceSlider
{
	QuStupidPointer<QuSoundStructInterface> mClickDownSound; 
	QuStupidPointer<QuSoundStructInterface> mClickUpSound; 
	QuRelScreenFloatBoxButton mBackground;
	QuRelScreenFloatBoxButton mSlider;
	QuStupidPointer<QuBaseImage> mImage;
	QuStupidPointer<QuBaseImage> mSliderImage;
	float mPos;
	bool mMouse;
	float mMax;
	float mArea;
	QuScreenCoord mDown;
	GraceSlider(float aPos = 5):mMouse(false),mPos(aPos),mMax(GRACE_MAX),mArea(0.05f)
	{
		//these are specific to our current scenario
		mClickDownSound = G_SOUND_MANAGER.getSound("sounds/clickDown.raw");
		mClickUpSound = G_SOUND_MANAGER.getSound("sounds/clickUp.raw");
		mImage = G_IMAGE_MANAGER.getFlyImage("images/DEFAULT_grace_bg.png");
		mSliderImage = G_IMAGE_MANAGER.getFlyImage("images/DEFAULT_grace_slider.png");
		mBackground.setBox(QuBox2<float>(0.00f,0.025f,0.52f,0.52f*66.0f/390.0f*G_SCREEN_RATIO));
		mSlider.setBox(QuBox2<float>(0,0,0.05f,0.05f*G_SCREEN_RATIO));
		updateSlider();
	}
	void updateSlider()
	{
		QuBox2<float> r = mSlider.getFloatBox();
		QuBox2<float> ref = mBackground.getFloatBox();
		r.y = ref.y + ref.h/2.0f - r.h/2.0f;
		r.x = ref.x + mArea*ref.w + (mPos/mMax)*(1-mArea*2)*ref.w - r.w/2.0f;
		mSlider.setBox(r);
	}
	void setSliderPosition(float aPos)
	{
		mPos = quClamp<float>(aPos,0,mMax);
		updateSlider();
	}
	float getSliderPosition()
	{
		return mPos;
	}
	void draw(GDeviceRotation aRot)
	{
		glDisable(GL_DEPTH_TEST);
		mBackground.drawRotatedImage(mImage,aRot);
		mSlider.drawRotatedImage(mSliderImage);
		//mSlider.drawRotatedRect(QuColor(0,0,0));
	}
	//assumes already rotated to G_DR_0, this should be the standard really...
	bool mouseDown(QuScreenCoord crd)
	{
		if(mSlider.click(crd,G_DR_0))
		{
			mClickDownSound->play();
			mMouse = true;
			mDown = crd;
			return true;
		}
		return false;
	}
	bool mouseUp()
	{
		if(mMouse)
		{
			mClickUpSound->play();
			mMouse = false;
			return true;
		}
		return false;
		
	} 
	void singleMotion(QuScreenCoord crd)
	{
		if(mMouse)
		{
			//width is distance from 0 to GRACE_MAX
			//note this are absolute screen positions
			float width = mBackground.getBox().w * (1-2*mArea);
			float zero = mBackground.getBox().x + mBackground.getBox().w * mArea;
			setSliderPosition(mMax*(crd.getIPosition().x-zero)/width);
		}
	}
};
//dummy class that checks for save file and starts standard dialog or first set dialog
class DiaLaunch : public QuBaseManager
{
	void initialize();	
};

class ButtonGroupManager
{
public: //bleah, just easier this way.
	QuStupidPointer<QuSoundStructInterface> mClickDownSound; //plays when click down on button
	QuStupidPointer<QuSoundStructInterface> mClickUpSound; //plays when click up on button
	QuStupidPointer<QuSoundStructInterface> mClickOffSound; //plays when click down on button
	int mLast;
	QuButtonList<QuEnlargeableRelScreenFloatBoxButton> mButtonGroup;
public:
	ButtonGroupManager();
	~ButtonGroupManager();
	QuButtonList<QuEnlargeableRelScreenFloatBoxButton> & getButtonGroup() { return mButtonGroup; }
	void addButton(QuStupidPointer<QuEnlargeableRelScreenFloatBoxButton> aButton, QuStupidPointer<QuBaseImage> image = NULL);
	void singleMotion(QuScreenCoord crd, GDeviceRotation aRot);
	void singleDown(QuScreenCoord crd, GDeviceRotation aRot);
	int singleUp(QuScreenCoord crd, GDeviceRotation aRot);
};

//base class to ease full screen image drawing
//TODO write button event class and make it part of this class
class  BgImageManager : public QuBaseManager
{
protected:
	QuImageDrawObject mTitle; //always drawn in the background
	QuBasicCamera mCamera;
	ButtonGroupManager mButtonGroup;
	BgImageManager():mCamera(G_SCREEN_WIDTH,G_SCREEN_HEIGHT,G_SCREEN_HEIGHT){}
	virtual ~BgImageManager() 
	{
		//stop leaks, but who cares because it might compromise some other functionality....
		//SavedSettings::destroy();
	}
	void initialize()
	{
		mCamera.setRotation(G_DR_0);
	}
	void drawBackground()
	{
		mCamera.setRotation(G_DEVICE_ROTATION,G_RESTRICT_PORTRAIT);
		glPushMatrix();
		mCamera.setScene();
		glScalef(G_SCREEN_WIDTH,G_SCREEN_HEIGHT,1);
		glDisable(GL_LIGHTING);
		mTitle.autoDraw();
		glPopMatrix();
	}
	virtual void singleDown(int button, QuScreenCoord crd)
	{
		//std::cout << crd.getFPosition().x << " " << crd.getFPosition().y << std::endl;
		mButtonGroup.singleDown(crd,mCamera.getRotation());
	}
	virtual void singleMotion(QuScreenCoord crd)
	{
		mButtonGroup.singleMotion(crd,mCamera.getRotation());
	}
	virtual void update()
	{
		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
		drawBackground();
		mButtonGroup.getButtonGroup().drawAllButtonsWithImages(mCamera.getRotation());
		s3eDeviceBacklightOn();
	}
};

//contains instructions and links to either standard or autosetmenu dialogs
class DiaFirstLaunch : public BgImageManager
{
	QuTimer mDelay;
public:
	DiaFirstLaunch();
	void initialize();
	void update();
	void singleUp(int button, QuScreenCoord crd);
};

//first screen of auto set dialog
class DiaAutoSetMenu : public BgImageManager
{
	QuTimer mInputDelay;
	QuTimer mLaunchTimer;	//delay timer for starting auto set configure
public:
	DiaAutoSetMenu();
	void initialize();
	void update();
	void singleUp(int button, QuScreenCoord crd);
};

class DiaAutoSetConfigure : public BgImageManager
{
	AccelMan mSignal;
	ButtonGroupManager mRestartButton;		//in case the user F***s up and wants to start over
	QuTimer mFailTimer;

public:
	DiaAutoSetConfigure();
	void initialize();
	void update();
	void singleUp(int button, QuScreenCoord crd);
};

class DiaAutoSetDone : public BgImageManager
{
	QuTimer mLaunchTimer;	//delay timer for starting auto set configure
public:
	DiaAutoSetDone();
	void update();
	void initialize();
	void singleDown(int button, QuScreenCoord crd);
	void singleUp(int button, QuScreenCoord crd);
};

//default manager 
class DiaDefault : public BgImageManager
{
	bool mVibra,mAudio;
	float mLeft, mRight;
	GraceSlider mSlider;
	QuStupidPointer<QuSoundStructInterface> mAlert;
	int editByte; //0 none, 1 left, 2 right
	AccelMan mSignal;
	bool mMouse, mWasAlert;
	QuTimer mGraceTimer;
public:
	DiaDefault():editByte(0),mMouse(false),mWasAlert(false)///*left,right,grace,audio,vibra*/)//:mSlider(grace)
	{}
	void initialize();
	void update();
	void singleUp(int button, QuScreenCoord crd);
	void singleDown(int button, QuScreenCoord crd);
	void singleMotion(QuScreenCoord crd);
	void saveConfig(std::string aFilename);
	void updateImages();
	void playAlert();
	void vibrate();
	void drawCone(float angle);
};