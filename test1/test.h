#pragma once
#include "QuGlobals.h"
#include "QuGameManager.h"
#include "QuInterface.h" 
#include "QuButton.h"
#include "QuClutils.h"
#include "s3eVibra.h"
#include "s3eFile.h"
#include <sstream>


class DataWriter
{
	s3eFile * mFile;
public:
	DataWriter(std::string aFilename)
	{
		mFile = s3eFileOpen(aFilename.c_str(),"w");
	}
	~DataWriter()
	{
		close();
	}

	void operator<<(std::string aMsg)
	{
		write(aMsg);
	}
	void operator<<(float aMsg)
	{
		stringstream ss (stringstream::in | stringstream::out);
		ss << aMsg;
		string test = ss.str();
		write(test);
	}
	void operator<<(bool aEnd)
	{
		if(aEnd)
			write("\n");
	}
	void write(std::string aMsg)
	{
		s3eFilePrintf(mFile,aMsg.c_str());
	}
	void close()
	{
		if( mFile != NULL )
			s3eFileClose(mFile);
		mFile = NULL;
	}

};

class InstructionManager : public QuBaseManager
{
	QuTimer mDelay;
	QuStupidPointer<QuSoundStructInterface> mClickSound;
	QuImageDrawObject mTitle;
	QuAbsScreenIntBoxButton mButton;
	QuBasicCamera mCamera;
	DataWriter mWrite;
public:
	InstructionManager():mDelay(0,10),mWrite("accel.txt"),mCamera(G_SCREEN_WIDTH,G_SCREEN_HEIGHT,G_SCREEN_HEIGHT){}
	void initialize()
	{
		mTitle.setImage(G_IMAGE_MANAGER.getFlyImage("images/title.png"));
		mButton.setBox(QuBox2<int>::getCenterRelBox(QuBox2<float>(0.5,0.2,0.5,0.3),G_GAME_GLOBAL.getRotatedScreenDimensions(G_DR_0)));
		mClickSound = G_SOUND_MANAGER.loadSound("sounds/SELECT.raw");
		mWrite<<1.234234f;
		mWrite.close();
	}
	void update()
	{
		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
		mDelay++;

		mCamera.setScene();
		glScalef(G_SCREEN_WIDTH,G_SCREEN_HEIGHT,1);
		glDisable(GL_LIGHTING);
		mTitle.autoDraw();
		mCamera.setScene();
	}
	void singleUp(int button, QuScreenCoord crd)
	{
		if(mButton.click(crd,G_DR_0) && mDelay.isExpired())
		{
			mClickSound->play();
		}
	}

};