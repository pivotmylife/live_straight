# -*- coding: utf-8 -*-
# Deployment settings for test1.
# This file is autogenerated by the mkb system and used by the s3e deployment
# tool during the build process.

config = {}
cmdline = ['c:/Users/plu/Documents/petetrlu/kitchen/spices/airplay4.4/s3e/makefile_builder/mkb.py', '--use-temp-extension', '--verbose=1', '--non-interactive', 'C:/Users/plu/Documents/petetrlu/kitchen/faucet/self_help/test1/test1.mkb', '--deploy-only']
mkb = 'C:/Users/plu/Documents/petetrlu/kitchen/faucet/self_help/test1/test1.mkb'
mkf = ['c:\\users\\plu\\documents\\petetrlu\\kitchen\\faucet\\airplaygl44template\\TEMPLATE.mkf', 'c:\\users\\plu\\documents\\petetrlu\\kitchen\\spices\\airplay4.4\\modules\\iwgx.mkf', 'c:\\users\\plu\\documents\\petetrlu\\kitchen\\spices\\airplay4.4\\modules\\iwgeom.mkf', 'c:\\users\\plu\\documents\\petetrlu\\kitchen\\spices\\airplay4.4\\modules\\iwutil.mkf', 'c:\\users\\plu\\documents\\petetrlu\\kitchen\\spices\\airplay4.4\\modules\\iwresmanager.mkf', 'c:\\users\\plu\\documents\\petetrlu\\kitchen\\spices\\airplay4.4\\modules\\iwgl.mkf', 'c:\\users\\plu\\documents\\petetrlu\\kitchen\\spices\\airplay4.4\\modules\\third_party\\libpng.mkf', 'c:\\users\\plu\\documents\\petetrlu\\kitchen\\spices\\airplay4.4\\modules\\third_party\\zlib.mkf', 'c:\\users\\plu\\documents\\petetrlu\\kitchen\\faucet\\airplaygl44template\\ug.mkf', 'c:\\users\\plu\\documents\\petetrlu\\kitchen\\spices\\airplay4.4\\examples\\SoundEngine.mkf']

class DeployConfig(object):
    os = ['iphone']
    pass

######### ASSET GROUPS #############

assets = {}

assets['Default'] = [
    ('C:/Users/plu/Documents/petetrlu/kitchen/faucet/self_help/test1/data/images', 'images', 0),
    ('C:/Users/plu/Documents/petetrlu/kitchen/faucet/self_help/test1/data/sounds', 'sounds', 0),
]

######### DEFAULT CONFIG #############

class DefaultConfig(DeployConfig):
    os = ['iphone']
    embed_icf = -1
    name = 'test_rawsound'
    pub_sign_key = 0
    priv_sign_key = 0
    caption = 'test_rawsound1'
    long_caption = 'test_rawsound'
    version = [0, 0, 1]
    config = ['C:/Users/plu/Documents/petetrlu/kitchen/faucet/self_help/test1/data/app.icf']
    data_dir = 'C:/Users/plu/Documents/petetrlu/kitchen/faucet/self_help/test1\data'
    iphone_signing_identity = 'iPhone Developer'
    target = {
         'arm' : {
                   'debug'   : 'C:/Users/plu/Documents/petetrlu/kitchen/faucet/self_help/test1/build_test1_vc10/Debug_test1_VC10_arm/test1.s3e',
                   'release' : 'C:/Users/plu/Documents/petetrlu/kitchen/faucet/self_help/test1/build_test1_vc10/Release_test1_VC10_arm/test1.s3e',
                 },
         'arm_gcc' : {
                   'debug'   : 'C:/Users/plu/Documents/petetrlu/kitchen/faucet/self_help/test1/build_test1_vc10/Debug_test1_VC10_gcc_arm/test1.s3e',
                   'release' : 'C:/Users/plu/Documents/petetrlu/kitchen/faucet/self_help/test1/build_test1_vc10/Release_test1_VC10_gcc_arm/test1.s3e',
                 },
         'x86' : {
                   'debug'   : 'C:/Users/plu/Documents/petetrlu/kitchen/faucet/self_help/test1/build_test1_vc10/Debug_test1_VC10_x86/test1.s86',
                   'release' : 'C:/Users/plu/Documents/petetrlu/kitchen/faucet/self_help/test1/build_test1_vc10/Release_test1_VC10_x86/test1.s86',
                 },
        }
    assets = assets['Default']

default = DefaultConfig()
