	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"QuBlunt.cpp"
	.section	.text._ZN4_STL14__malloc_allocILi0EE8allocateEj,"axG",%progbits,_ZN4_STL14__malloc_allocILi0EE8allocateEj,comdat
	.align	2
	.weak	_ZN4_STL14__malloc_allocILi0EE8allocateEj
	.hidden	_ZN4_STL14__malloc_allocILi0EE8allocateEj
	.type	_ZN4_STL14__malloc_allocILi0EE8allocateEj, %function
_ZN4_STL14__malloc_allocILi0EE8allocateEj:
	.fnstart
.LFB4492:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	bl	malloc
	cmp	r0, #0
	beq	.L5
.L2:
	ldmfd	sp!, {r4, lr}
	bx	lr
.L5:
	mov	r0, r4
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
	b	.L2
	.fnend
	.size	_ZN4_STL14__malloc_allocILi0EE8allocateEj, .-_ZN4_STL14__malloc_allocILi0EE8allocateEj
	.section	.text._ZN4_STL4listIiNS_9allocatorIiEEED1Ev,"axG",%progbits,_ZN4_STL4listIiNS_9allocatorIiEEED1Ev,comdat
	.align	2
	.weak	_ZN4_STL4listIiNS_9allocatorIiEEED1Ev
	.hidden	_ZN4_STL4listIiNS_9allocatorIiEEED1Ev
	.type	_ZN4_STL4listIiNS_9allocatorIiEEED1Ev, %function
_ZN4_STL4listIiNS_9allocatorIiEEED1Ev:
	.fnstart
.LFB4089:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	ldr	r2, [r0, #0]
	ldr	r3, [r2, #0]
	cmp	r2, r3
	mov	r5, r0
	bne	.L12
	b	.L7
.L13:
	mov	r3, r4
.L12:
	mov	r0, r3
	ldr	r4, [r3, #0]
	bl	free
	ldr	r3, [r5, #0]
	cmp	r4, r3
	bne	.L13
	mov	r3, r4
.L7:
	str	r3, [r3, #0]
	ldr	r0, [r5, #0]
	str	r0, [r0, #4]
	ldr	r0, [r5, #0]
	cmp	r0, #0
	blne	free
.L10:
	mov	r0, r5
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN4_STL4listIiNS_9allocatorIiEEED1Ev, .-_ZN4_STL4listIiNS_9allocatorIiEEED1Ev
	.global	__cxa_end_cleanup
	.section	.text._GLOBAL__I__ZN8QuGlobal5mSelfE,"ax",%progbits
	.align	2
	.type	_GLOBAL__I__ZN8QuGlobal5mSelfE, %function
_GLOBAL__I__ZN8QuGlobal5mSelfE:
	.fnstart
.LFB5159:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	ldr	r4, .L37
	mov	r0, r4
.LEHB0:
	bl	_ZN4_STL8ios_base9_Loc_initC1Ev
	ldr	r1, .L37+4
	ldr	r2, .L37+8
	mov	r0, r4
	bl	__aeabi_atexit
	add	r0, r4, #4
	bl	_ZN4_STL8ios_base4InitC1Ev
.LEHE0:
	add	r0, r4, #4
	ldr	r1, .L37+12
	ldr	r2, .L37+8
	bl	__aeabi_atexit
	mov	r3, #0
	mov	r0, #24
	str	r3, [r4, #8]
.LEHB1:
	bl	_ZN4_STL14__malloc_allocILi0EE8allocateEj
.LEHE1:
	mov	r5, #0
	str	r0, [r4, #8]
	str	r5, [r4, #12]
	strb	r5, [r0, #0]
	ldr	r2, [r4, #8]
	str	r5, [r2, #4]
	ldr	r1, [r4, #8]
	str	r1, [r1, #8]
	ldr	r0, [r4, #8]
	ldr	r1, .L37+16
	str	r0, [r0, #12]
	ldr	r2, .L37+8
	ldr	r0, .L37+20
	bl	__aeabi_atexit
	mov	r0, #12
	str	r5, [r4, #20]
.LEHB2:
	bl	_ZN4_STL14__malloc_allocILi0EE8allocateEj
.LEHE2:
.L18:
.L33:
.L21:
	mov	ip, r0
	str	r0, [ip, #0]
	str	r0, [ip, #4]
	ldr	r1, .L37+24
	ldr	r2, .L37+8
	ldr	r0, .L37+28
	str	ip, [r4, #20]
	bl	__aeabi_atexit
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L32:
.L36:
.L26:
.L34:
.L28:
.LEHB3:
	bl	__cxa_end_cleanup
.LEHE3:
.L31:
	b	.L36
.L38:
	.align	2
.L37:
	.word	.LANCHOR0
	.word	_ZN4_STL8ios_base9_Loc_initD1Ev
	.word	__dso_handle
	.word	_ZN4_STL8ios_base4InitD1Ev
	.word	_ZN4_STL3mapIPviNS_4lessIS1_EENS_9allocatorINS_4pairIKS1_iEEEEED1Ev
	.word	.LANCHOR0+8
	.word	_ZN4_STL4listIiNS_9allocatorIiEEED1Ev
	.word	.LANCHOR0+20
	.global	__gxx_personality_v0
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA5159:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5159-.LLSDACSB5159
.LLSDACSB5159:
	.uleb128 .LEHB0-.LFB5159
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB1-.LFB5159
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L32-.LFB5159
	.uleb128 0x0
	.uleb128 .LEHB2-.LFB5159
	.uleb128 .LEHE2-.LEHB2
	.uleb128 .L31-.LFB5159
	.uleb128 0x0
	.uleb128 .LEHB3-.LFB5159
	.uleb128 .LEHE3-.LEHB3
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE5159:
	.fnend
	.size	_GLOBAL__I__ZN8QuGlobal5mSelfE, .-_GLOBAL__I__ZN8QuGlobal5mSelfE
	.section	.init_array,"aw",%init_array
	.align	2
	.word	_GLOBAL__I__ZN8QuGlobal5mSelfE(target1)
	.section	.text._ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE8_M_eraseEPNS_13_Rb_tree_nodeIS4_EE,"axG",%progbits,_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE8_M_eraseEPNS_13_Rb_tree_nodeIS4_EE,comdat
	.align	2
	.weak	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE8_M_eraseEPNS_13_Rb_tree_nodeIS4_EE
	.hidden	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE8_M_eraseEPNS_13_Rb_tree_nodeIS4_EE
	.type	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE8_M_eraseEPNS_13_Rb_tree_nodeIS4_EE, %function
_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE8_M_eraseEPNS_13_Rb_tree_nodeIS4_EE:
	.fnstart
.LFB4876:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.pad #20
	sub	sp, sp, #20
	str	r1, [sp, #12]
	mov	r5, r0
.L40:
	ldr	r3, [sp, #12]
	cmp	r3, #0
	beq	.L63
	ldr	r0, [sp, #12]
	ldr	r3, [r0, #12]
	str	r3, [sp, #8]
.L41:
	ldr	ip, [sp, #8]
	cmp	ip, #0
	beq	.L64
	ldr	r1, [sp, #8]
	ldr	r8, [r1, #12]
.L42:
	cmp	r8, #0
	beq	.L65
	ldr	sl, [r8, #12]
.L43:
	cmp	sl, #0
	beq	.L66
	ldr	r9, [sl, #12]
.L44:
	cmp	r9, #0
	beq	.L67
	ldr	fp, [r9, #12]
	cmp	fp, #0
	beq	.L45
.L62:
	ldr	r7, [fp, #12]
	cmp	r7, #0
	beq	.L46
.L61:
	ldr	r6, [r7, #12]
	cmp	r6, #0
	beq	.L47
.L60:
	ldr	r4, [r6, #12]
	cmp	r4, #0
	bne	.L59
	b	.L48
.L68:
	mov	r4, r3
.L59:
	ldr	r1, [r4, #12]
	mov	r0, r5
	bl	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE8_M_eraseEPNS_13_Rb_tree_nodeIS4_EE
	ldr	r2, [r4, #8]
	mov	r0, r4
	str	r2, [sp, #4]
	bl	free
	ldr	r3, [sp, #4]
	cmp	r3, #0
	bne	.L68
.L48:
	ldr	r4, [r6, #8]
	mov	r0, r6
	bl	free
	cmp	r4, #0
	movne	r6, r4
	bne	.L60
.L47:
	ldr	r4, [r7, #8]
	mov	r0, r7
	bl	free
	cmp	r4, #0
	movne	r7, r4
	bne	.L61
.L46:
	ldr	r4, [fp, #8]
	mov	r0, fp
	bl	free
	cmp	r4, #0
	movne	fp, r4
	bne	.L62
.L45:
	mov	r0, r9
	ldr	r9, [r9, #8]
	bl	free
	b	.L44
.L67:
	mov	r0, sl
	ldr	sl, [sl, #8]
	bl	free
	b	.L43
.L66:
	mov	r0, r8
	ldr	r8, [r8, #8]
	bl	free
	b	.L42
.L65:
	ldr	r0, [sp, #8]
	ldr	r4, [r0, #8]
	bl	free
	str	r4, [sp, #8]
	b	.L41
.L64:
	ldr	r0, [sp, #12]
	ldr	r4, [r0, #8]
	bl	free
	str	r4, [sp, #12]
	b	.L40
.L63:
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
	.fnend
	.size	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE8_M_eraseEPNS_13_Rb_tree_nodeIS4_EE, .-_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE8_M_eraseEPNS_13_Rb_tree_nodeIS4_EE
	.section	.text._ZN4_STL3mapIPviNS_4lessIS1_EENS_9allocatorINS_4pairIKS1_iEEEEED1Ev,"axG",%progbits,_ZN4_STL3mapIPviNS_4lessIS1_EENS_9allocatorINS_4pairIKS1_iEEEEED1Ev,comdat
	.align	2
	.weak	_ZN4_STL3mapIPviNS_4lessIS1_EENS_9allocatorINS_4pairIKS1_iEEEEED1Ev
	.hidden	_ZN4_STL3mapIPviNS_4lessIS1_EENS_9allocatorINS_4pairIKS1_iEEEEED1Ev
	.type	_ZN4_STL3mapIPviNS_4lessIS1_EENS_9allocatorINS_4pairIKS1_iEEEEED1Ev, %function
_ZN4_STL3mapIPviNS_4lessIS1_EENS_9allocatorINS_4pairIKS1_iEEEEED1Ev:
	.fnstart
.LFB5091:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	ldr	r3, [r0, #4]
	cmp	r3, #0
	mov	r4, r0
	bne	.L76
.L70:
	ldr	r0, [r4, #0]
	cmp	r0, #0
	blne	free
.L73:
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L76:
	ldr	r2, [r0, #0]
	ldr	r1, [r2, #4]
.LEHB4:
	bl	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE8_M_eraseEPNS_13_Rb_tree_nodeIS4_EE
.LEHE4:
	ldr	lr, [r4, #0]
	str	lr, [lr, #8]
	ldr	ip, [r4, #0]
	mov	r0, #0
	str	r0, [ip, #4]
	ldr	r1, [r4, #0]
	str	r1, [r1, #12]
	str	r0, [r4, #4]
	b	.L70
.L75:
.L71:
	ldr	r3, [r4, #0]
	cmp	r3, #0
	mov	r4, r0
	movne	r0, r3
	blne	free
.L72:
	mov	r0, r4
.LEHB5:
	bl	__cxa_end_cleanup
.LEHE5:
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA5091:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5091-.LLSDACSB5091
.LLSDACSB5091:
	.uleb128 .LEHB4-.LFB5091
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L75-.LFB5091
	.uleb128 0x0
	.uleb128 .LEHB5-.LFB5091
	.uleb128 .LEHE5-.LEHB5
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE5091:
	.fnend
	.size	_ZN4_STL3mapIPviNS_4lessIS1_EENS_9allocatorINS_4pairIKS1_iEEEEED1Ev, .-_ZN4_STL3mapIPviNS_4lessIS1_EENS_9allocatorINS_4pairIKS1_iEEEEED1Ev
	.hidden	_ZN8QuGlobal5mSelfE
	.global	_ZN8QuGlobal5mSelfE
	.hidden	_ZN21QuStupidPointerHelper6mSPMapE
	.global	_ZN21QuStupidPointerHelper6mSPMapE
	.hidden	_ZN8QuLights11globalAvailE
	.global	_ZN8QuLights11globalAvailE
	.bss
	.align	2
	.set	.LANCHOR0,. + 0
	.type	_ZN4_STLL8_LocInitE, %object
	.size	_ZN4_STLL8_LocInitE, 1
_ZN4_STLL8_LocInitE:
	.space	1
	.space	3
	.type	_ZN4_STLL8_IosInitE, %object
	.size	_ZN4_STLL8_IosInitE, 1
_ZN4_STLL8_IosInitE:
	.space	1
	.space	3
	.type	_ZN21QuStupidPointerHelper6mSPMapE, %object
	.size	_ZN21QuStupidPointerHelper6mSPMapE, 12
_ZN21QuStupidPointerHelper6mSPMapE:
	.space	12
	.type	_ZN8QuLights11globalAvailE, %object
	.size	_ZN8QuLights11globalAvailE, 4
_ZN8QuLights11globalAvailE:
	.space	4
	.type	_ZN8QuGlobal5mSelfE, %object
	.size	_ZN8QuGlobal5mSelfE, 4
_ZN8QuGlobal5mSelfE:
	.space	4
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
