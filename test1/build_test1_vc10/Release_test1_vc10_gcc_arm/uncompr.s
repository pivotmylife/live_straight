	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"uncompr.c"
	.section	.text.uncompress,"ax",%progbits
	.align	2
	.global	uncompress
	.hidden	uncompress
	.type	uncompress, %function
uncompress:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 56
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	sub	sp, sp, #56
	ldr	lr, [r1, #0]
	mov	ip, #0
	str	r2, [sp, #0]
	str	r0, [sp, #12]
	mov	r5, r1
	mov	r0, sp
	ldr	r1, .L10
	mov	r2, #56
	str	r3, [sp, #4]
	str	lr, [sp, #16]
	str	ip, [sp, #36]
	str	ip, [sp, #32]
	bl	inflateInit_
	subs	r6, r0, #0
	mov	r4, sp
	beq	.L8
.L3:
	mov	r0, r6
	add	sp, sp, #56
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L8:
	mov	r0, sp
	mov	r1, #4
	bl	inflate
	cmp	r0, #1
	mov	r6, r0
	bne	.L9
	ldr	r0, [sp, #20]
	str	r0, [r5, #0]
	mov	r0, sp
	bl	inflateEnd
	mov	r6, r0
	b	.L3
.L9:
	mov	r0, sp
	bl	inflateEnd
	cmp	r6, #2
	beq	.L5
	cmn	r6, #5
	bne	.L3
	ldr	r3, [sp, #4]
	cmp	r3, #0
	bne	.L3
.L5:
	mvn	r6, #2
	b	.L3
.L11:
	.align	2
.L10:
	.word	.LC0
	.size	uncompress, .-uncompress
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"1.2.3\000"
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
