	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"IwSoundGroup.cpp"
	.section	.text._ZN10CIwManaged9ParseOpenEP16CIwTextParserITX,"axG",%progbits,_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX,comdat
	.align	2
	.weak	_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX
	.hidden	_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX
	.type	_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX, %function
_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX:
	.fnstart
.LFB331:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX, .-_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX
	.section	.text._ZN10CIwManaged11HandleEventEP8CIwEvent,"axG",%progbits,_ZN10CIwManaged11HandleEventEP8CIwEvent,comdat
	.align	2
	.weak	_ZN10CIwManaged11HandleEventEP8CIwEvent
	.hidden	_ZN10CIwManaged11HandleEventEP8CIwEvent
	.type	_ZN10CIwManaged11HandleEventEP8CIwEvent, %function
_ZN10CIwManaged11HandleEventEP8CIwEvent:
	.fnstart
.LFB332:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #0
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN10CIwManaged11HandleEventEP8CIwEvent, .-_ZN10CIwManaged11HandleEventEP8CIwEvent
	.section	.text._ZN10CIwManaged11DebugRenderEv,"axG",%progbits,_ZN10CIwManaged11DebugRenderEv,comdat
	.align	2
	.weak	_ZN10CIwManaged11DebugRenderEv
	.hidden	_ZN10CIwManaged11DebugRenderEv
	.type	_ZN10CIwManaged11DebugRenderEv, %function
_ZN10CIwManaged11DebugRenderEv:
	.fnstart
.LFB334:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN10CIwManaged11DebugRenderEv, .-_ZN10CIwManaged11DebugRenderEv
	.section	.text._ZN11CIwResource10ApplyScaleEi,"axG",%progbits,_ZN11CIwResource10ApplyScaleEi,comdat
	.align	2
	.weak	_ZN11CIwResource10ApplyScaleEi
	.hidden	_ZN11CIwResource10ApplyScaleEi
	.type	_ZN11CIwResource10ApplyScaleEi, %function
_ZN11CIwResource10ApplyScaleEi:
	.fnstart
.LFB360:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN11CIwResource10ApplyScaleEi, .-_ZN11CIwResource10ApplyScaleEi
	.section	.text._Z21_GetCIwSoundGroupSizev,"ax",%progbits
	.align	2
	.global	_Z21_GetCIwSoundGroupSizev
	.hidden	_Z21_GetCIwSoundGroupSizev
	.type	_Z21_GetCIwSoundGroupSizev, %function
_Z21_GetCIwSoundGroupSizev:
	.fnstart
.LFB1370:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #32
	bx	lr
	.cantunwind
	.fnend
	.size	_Z21_GetCIwSoundGroupSizev, .-_Z21_GetCIwSoundGroupSizev
	.section	.text._ZNK13CIwSoundGroup12GetClassNameEv,"ax",%progbits
	.align	2
	.global	_ZNK13CIwSoundGroup12GetClassNameEv
	.hidden	_ZNK13CIwSoundGroup12GetClassNameEv
	.type	_ZNK13CIwSoundGroup12GetClassNameEv, %function
_ZNK13CIwSoundGroup12GetClassNameEv:
	.fnstart
.LFB1371:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L13
	bx	lr
.L14:
	.align	2
.L13:
	.word	.LC0
	.cantunwind
	.fnend
	.size	_ZNK13CIwSoundGroup12GetClassNameEv, .-_ZNK13CIwSoundGroup12GetClassNameEv
	.section	.text._ZN13CIwSoundGroup14ParseAttributeEP16CIwTextParserITXPKc,"ax",%progbits
	.align	2
	.global	_ZN13CIwSoundGroup14ParseAttributeEP16CIwTextParserITXPKc
	.hidden	_ZN13CIwSoundGroup14ParseAttributeEP16CIwTextParserITXPKc
	.type	_ZN13CIwSoundGroup14ParseAttributeEP16CIwTextParserITXPKc, %function
_ZN13CIwSoundGroup14ParseAttributeEP16CIwTextParserITXPKc:
	.fnstart
.LFB1380:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #0
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN13CIwSoundGroup14ParseAttributeEP16CIwTextParserITXPKc, .-_ZN13CIwSoundGroup14ParseAttributeEP16CIwTextParserITXPKc
	.section	.text._ZN13CIwSoundGroup10ParseCloseEP16CIwTextParserITX,"ax",%progbits
	.align	2
	.global	_ZN13CIwSoundGroup10ParseCloseEP16CIwTextParserITX
	.hidden	_ZN13CIwSoundGroup10ParseCloseEP16CIwTextParserITX
	.type	_ZN13CIwSoundGroup10ParseCloseEP16CIwTextParserITX, %function
_ZN13CIwSoundGroup10ParseCloseEP16CIwTextParserITX:
	.fnstart
.LFB1381:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN13CIwSoundGroup10ParseCloseEP16CIwTextParserITX, .-_ZN13CIwSoundGroup10ParseCloseEP16CIwTextParserITX
	.section	.text._ZN13CIwSoundGroupD0Ev,"axG",%progbits,_ZN13CIwSoundGroupD0Ev,comdat
	.align	2
	.weak	_ZN13CIwSoundGroupD0Ev
	.hidden	_ZN13CIwSoundGroupD0Ev
	.type	_ZN13CIwSoundGroupD0Ev, %function
_ZN13CIwSoundGroupD0Ev:
	.fnstart
.LFB1520:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L21
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	str	r3, [r0, #0]
	bl	_ZN10CIwManagedD2Ev
	mov	r0, r4
	bl	_ZdlPv
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L22:
	.align	2
.L21:
	.word	_ZTV18CIwManagedRefCount+8
	.fnend
	.size	_ZN13CIwSoundGroupD0Ev, .-_ZN13CIwSoundGroupD0Ev
	.section	.text._ZN13CIwSoundGroupD1Ev,"axG",%progbits,_ZN13CIwSoundGroupD1Ev,comdat
	.align	2
	.weak	_ZN13CIwSoundGroupD1Ev
	.hidden	_ZN13CIwSoundGroupD1Ev
	.type	_ZN13CIwSoundGroupD1Ev, %function
_ZN13CIwSoundGroupD1Ev:
	.fnstart
.LFB1519:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L25
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	str	r3, [r0, #0]
	bl	_ZN10CIwManagedD2Ev
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L26:
	.align	2
.L25:
	.word	_ZTV18CIwManagedRefCount+8
	.fnend
	.size	_ZN13CIwSoundGroupD1Ev, .-_ZN13CIwSoundGroupD1Ev
	.section	.text._ZN13CIwSoundGroup6ResumeEv,"ax",%progbits
	.align	2
	.global	_ZN13CIwSoundGroup6ResumeEv
	.hidden	_ZN13CIwSoundGroup6ResumeEv
	.type	_ZN13CIwSoundGroup6ResumeEv, %function
_ZN13CIwSoundGroup6ResumeEv:
	.fnstart
.LFB1379:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	ldr	r6, .L35
	ldr	r3, [r6, #0]
	ldr	r2, [r3, #20]
	cmp	r2, #0
	mov	r5, r0
	beq	.L32
	mov	r4, #0
.L31:
	ldr	r1, [r3, #12]
	ldr	r0, [r1, r4, asl #2]
	ldr	ip, [r0, #0]
	ldr	r1, [ip, #36]
	cmp	r5, r1
	add	r4, r4, #1
	beq	.L34
.L30:
	cmp	r4, r2
	bcc	.L31
.L32:
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L34:
	bl	_ZN12CIwSoundInst6ResumeEv
	ldr	r3, [r6, #0]
	ldr	r2, [r3, #20]
	b	.L30
.L36:
	.align	2
.L35:
	.word	g_IwSoundManager
	.fnend
	.size	_ZN13CIwSoundGroup6ResumeEv, .-_ZN13CIwSoundGroup6ResumeEv
	.section	.text._ZN13CIwSoundGroup5PauseEv,"ax",%progbits
	.align	2
	.global	_ZN13CIwSoundGroup5PauseEv
	.hidden	_ZN13CIwSoundGroup5PauseEv
	.type	_ZN13CIwSoundGroup5PauseEv, %function
_ZN13CIwSoundGroup5PauseEv:
	.fnstart
.LFB1378:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	ldr	r6, .L44
	ldr	r3, [r6, #0]
	ldr	r2, [r3, #20]
	cmp	r2, #0
	mov	r5, r0
	beq	.L42
	mov	r4, #0
.L41:
	ldr	r1, [r3, #12]
	ldr	r0, [r1, r4, asl #2]
	ldr	ip, [r0, #0]
	ldr	r1, [ip, #36]
	cmp	r5, r1
	add	r4, r4, #1
	beq	.L43
.L40:
	cmp	r4, r2
	bcc	.L41
.L42:
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L43:
	bl	_ZN12CIwSoundInst5PauseEv
	ldr	r3, [r6, #0]
	ldr	r2, [r3, #20]
	b	.L40
.L45:
	.align	2
.L44:
	.word	g_IwSoundManager
	.fnend
	.size	_ZN13CIwSoundGroup5PauseEv, .-_ZN13CIwSoundGroup5PauseEv
	.section	.text._ZN13CIwSoundGroup4StopEv,"ax",%progbits
	.align	2
	.global	_ZN13CIwSoundGroup4StopEv
	.hidden	_ZN13CIwSoundGroup4StopEv
	.type	_ZN13CIwSoundGroup4StopEv, %function
_ZN13CIwSoundGroup4StopEv:
	.fnstart
.LFB1377:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	ldr	r5, .L54
	ldr	r3, [r5, #0]
	ldr	r2, [r3, #20]
	cmp	r2, #0
	mov	r6, r0
	beq	.L52
	mov	r4, #0
	b	.L51
.L53:
	ldr	r3, [r5, #0]
	ldr	r2, [r3, #20]
.L49:
	add	r4, r4, #1
	cmp	r4, r2
	bcs	.L52
.L51:
	ldr	r1, [r3, #12]
	ldr	r0, [r1, r4, asl #2]
	ldr	r1, [r0, #0]
	cmp	r1, #0
	beq	.L49
	ldr	r3, [r1, #36]
	cmp	r6, r3
	bne	.L53
	bl	_ZN12CIwSoundInst4StopEv
	b	.L53
.L52:
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L55:
	.align	2
.L54:
	.word	g_IwSoundManager
	.fnend
	.size	_ZN13CIwSoundGroup4StopEv, .-_ZN13CIwSoundGroup4StopEv
	.section	.text._ZN13CIwSoundGroup14KillOldestInstEb,"ax",%progbits
	.align	2
	.global	_ZN13CIwSoundGroup14KillOldestInstEb
	.hidden	_ZN13CIwSoundGroup14KillOldestInstEb
	.type	_ZN13CIwSoundGroup14KillOldestInstEb, %function
_ZN13CIwSoundGroup14KillOldestInstEb:
	.fnstart
.LFB1376:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	ldr	r4, .L96
	ldr	r2, [r4, #0]
	ldr	r3, [r2, #20]
	subs	r3, r3, r1
	mov	r4, r0
	moveq	r0, r3
	beq	.L60
	sub	r5, r3, #1
	mov	r0, #0
	ands	r5, r5, #3
	ldr	r1, [r2, #12]
	mov	ip, r0
	mov	r2, r0
	beq	.L61
	ldr	r2, [r1, r0]
	ldr	ip, [r2, #0]
	ldr	lr, [ip, #36]
	cmp	r4, lr
	beq	.L82
.L64:
	mov	lr, #0
.L83:
	cmp	r5, #1
	mov	r2, #1
	mov	ip, lr
	beq	.L61
	cmp	r5, #2
	beq	.L79
	ldr	r5, [r1, #4]
	ldr	ip, [r5, #0]
	ldr	ip, [ip, #36]
	cmp	r4, ip
	beq	.L84
.L65:
	mov	ip, lr
.L85:
	add	r2, r2, #1
.L79:
	ldr	r5, [r1, r2, asl #2]
	ldr	lr, [r5, #0]
	ldr	lr, [lr, #36]
	cmp	r4, lr
	beq	.L86
.L67:
	mov	lr, ip
.L87:
	add	r2, r2, #1
	mov	ip, lr
.L61:
	ldr	lr, [r1, r2, asl #2]
	ldr	r5, [lr, #0]
	ldr	r5, [r5, #36]
	cmp	r4, r5
	beq	.L93
.L58:
	mov	r5, ip
.L59:
	add	r2, r2, #1
	cmp	r3, r2
	bls	.L60
	ldr	lr, [r1, r2, asl #2]
	ldr	ip, [lr, #0]
	ldr	ip, [ip, #36]
	cmp	r4, ip
	beq	.L94
.L70:
	mov	ip, r5
.L88:
	add	lr, r2, #1
	ldr	r5, [r1, lr, asl #2]
	ldr	lr, [r5, #0]
	ldr	lr, [lr, #36]
	cmp	r4, lr
	beq	.L89
.L71:
	mov	lr, ip
.L90:
	add	ip, r2, #2
	ldr	r5, [r1, ip, asl #2]
	ldr	ip, [r5, #0]
	ldr	ip, [ip, #36]
	cmp	r4, ip
	beq	.L91
.L72:
	mov	ip, lr
	add	r2, r2, #3
.L95:
	ldr	lr, [r1, r2, asl #2]
	ldr	r5, [lr, #0]
	ldr	r5, [r5, #36]
	cmp	r4, r5
	bne	.L58
	b	.L93
.L60:
	bl	_ZN12CIwSoundInst4StopEv
	ldrh	r1, [r4, #24]
	sub	r0, r1, #1
	strh	r0, [r4, #24]	@ movhi
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L93:
	ldrh	r5, [lr, #14]
	cmp	r5, ip
	movhi	r0, lr
	bhi	.L59
	b	.L58
.L91:
	ldrh	ip, [r5, #14]
	cmp	ip, lr
	bls	.L72
	mov	r0, r5
	add	r2, r2, #3
	b	.L95
.L89:
	ldrh	lr, [r5, #14]
	cmp	lr, ip
	bls	.L71
	mov	r0, r5
	b	.L90
.L94:
	ldrh	ip, [lr, #14]
	cmp	ip, r5
	bls	.L70
	mov	r0, lr
	b	.L88
.L86:
	ldrh	lr, [r5, #14]
	cmp	lr, ip
	bls	.L67
	mov	r0, r5
	b	.L87
.L82:
	ldrh	lr, [r2, #14]
	cmp	lr, #0
	bls	.L64
	mov	r0, r2
	b	.L83
.L84:
	ldrh	ip, [r5, #14]
	cmp	ip, lr
	bls	.L65
	mov	r0, r5
	b	.L85
.L97:
	.align	2
.L96:
	.word	g_IwSoundManager
	.fnend
	.size	_ZN13CIwSoundGroup14KillOldestInstEb, .-_ZN13CIwSoundGroup14KillOldestInstEb
	.section	.text._ZN13CIwSoundGroup9SerialiseEv,"ax",%progbits
	.align	2
	.global	_ZN13CIwSoundGroup9SerialiseEv
	.hidden	_ZN13CIwSoundGroup9SerialiseEv
	.type	_ZN13CIwSoundGroup9SerialiseEv, %function
_ZN13CIwSoundGroup9SerialiseEv:
	.fnstart
.LFB1375:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	bl	_ZN10CIwManaged9SerialiseEv
	add	r0, r4, #16
	mov	r1, #1
	mov	r2, #15
	mov	r3, #2
	bl	_Z16IwSerialiseInt16Rsiii
	add	r0, r4, #18
	mov	r1, #1
	mov	r2, #15
	mov	r3, #2
	bl	_Z16IwSerialiseInt16Rsiii
	add	r0, r4, #20
	mov	r1, #1
	mov	r2, #15
	mov	r3, #2
	bl	_Z16IwSerialiseInt16Rsiii
	add	r0, r4, #22
	mov	r1, #1
	mov	r2, #16
	mov	r3, #2
	bl	_Z17IwSerialiseUInt16Rtiii
	add	r0, r4, #26
	mov	r1, #1
	mov	r2, #16
	mov	r3, #2
	bl	_Z17IwSerialiseUInt16Rtiii
	ldmfd	sp!, {r4, lr}
	bx	lr
	.fnend
	.size	_ZN13CIwSoundGroup9SerialiseEv, .-_ZN13CIwSoundGroup9SerialiseEv
	.section	.text._ZN13CIwSoundGroupC1Ev,"ax",%progbits
	.align	2
	.global	_ZN13CIwSoundGroupC1Ev
	.hidden	_ZN13CIwSoundGroupC1Ev
	.type	_ZN13CIwSoundGroupC1Ev, %function
_ZN13CIwSoundGroupC1Ev:
	.fnstart
.LFB1374:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	bl	_ZN10CIwManagedC2Ev
	ldr	r1, .L104
	mov	r3, #0
	mov	r0, #4096
	mov	r2, #4	@ movhi
	strh	r0, [r4, #20]	@ movhi
	strh	r0, [r4, #16]	@ movhi
	str	r1, [r4, #0]
	strh	r3, [r4, #26]	@ movhi
	strh	r3, [r4, #12]	@ movhi
	strh	r3, [r4, #14]	@ movhi
	strh	r3, [r4, #18]	@ movhi
	strh	r2, [r4, #22]	@ movhi
	strh	r3, [r4, #24]	@ movhi
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L105:
	.align	2
.L104:
	.word	.LANCHOR0+8
	.fnend
	.size	_ZN13CIwSoundGroupC1Ev, .-_ZN13CIwSoundGroupC1Ev
	.section	.text._ZN13CIwSoundGroupC2Ev,"ax",%progbits
	.align	2
	.global	_ZN13CIwSoundGroupC2Ev
	.hidden	_ZN13CIwSoundGroupC2Ev
	.type	_ZN13CIwSoundGroupC2Ev, %function
_ZN13CIwSoundGroupC2Ev:
	.fnstart
.LFB1373:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	bl	_ZN10CIwManagedC2Ev
	ldr	r1, .L110
	mov	r3, #0
	mov	r0, #4096
	mov	r2, #4	@ movhi
	strh	r0, [r4, #20]	@ movhi
	strh	r0, [r4, #16]	@ movhi
	str	r1, [r4, #0]
	strh	r3, [r4, #26]	@ movhi
	strh	r3, [r4, #12]	@ movhi
	strh	r3, [r4, #14]	@ movhi
	strh	r3, [r4, #18]	@ movhi
	strh	r2, [r4, #22]	@ movhi
	strh	r3, [r4, #24]	@ movhi
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L111:
	.align	2
.L110:
	.word	.LANCHOR0+8
	.fnend
	.size	_ZN13CIwSoundGroupC2Ev, .-_ZN13CIwSoundGroupC2Ev
	.global	__cxa_end_cleanup
	.section	.text._Z21_CIwSoundGroupFactoryv,"ax",%progbits
	.align	2
	.global	_Z21_CIwSoundGroupFactoryv
	.hidden	_Z21_CIwSoundGroupFactoryv
	.type	_Z21_CIwSoundGroupFactoryv, %function
_Z21_CIwSoundGroupFactoryv:
	.fnstart
.LFB1369:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	mov	r0, #32
.LEHB0:
	bl	_Znwj
.LEHE0:
	mov	r4, r0
.LEHB1:
	bl	_ZN10CIwManagedC2Ev
.LEHE1:
	ldr	r3, .L118
	mov	lr, #0	@ movhi
	str	r3, [r4, #0]
	mov	ip, #4096	@ movhi
	mov	r3, #0	@ movhi
	mov	r2, #0	@ movhi
	mov	r1, #4096	@ movhi
	mov	r0, #4	@ movhi
	strh	r0, [r4, #22]	@ movhi
	strh	lr, [r4, #12]	@ movhi
	strh	lr, [r4, #14]	@ movhi
	strh	ip, [r4, #16]	@ movhi
	strh	r2, [r4, #18]	@ movhi
	strh	r1, [r4, #20]	@ movhi
	strh	r3, [r4, #24]	@ movhi
	strh	r3, [r4, #26]	@ movhi
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L117:
.L115:
	mov	r5, r0
	mov	r0, r4
	bl	_ZdlPv
	mov	r0, r5
.LEHB2:
	bl	__cxa_end_cleanup
.LEHE2:
.L119:
	.align	2
.L118:
	.word	.LANCHOR0+8
	.global	__gxx_personality_v0
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA1369:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE1369-.LLSDACSB1369
.LLSDACSB1369:
	.uleb128 .LEHB0-.LFB1369
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB1-.LFB1369
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L117-.LFB1369
	.uleb128 0x0
	.uleb128 .LEHB2-.LFB1369
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE1369:
	.fnend
	.size	_Z21_CIwSoundGroupFactoryv, .-_Z21_CIwSoundGroupFactoryv
	.hidden	_ZTV13CIwSoundGroup
	.global	_ZTV13CIwSoundGroup
	.hidden	_ZTS13CIwSoundGroup
	.global	_ZTS13CIwSoundGroup
	.hidden	_ZTI13CIwSoundGroup
	.global	_ZTI13CIwSoundGroup
	.section	.rodata
	.align	3
	.set	.LANCHOR0,. + 0
	.type	_ZTV13CIwSoundGroup, %object
	.size	_ZTV13CIwSoundGroup, 68
_ZTV13CIwSoundGroup:
	.word	0
	.word	_ZTI13CIwSoundGroup
	.word	_ZN13CIwSoundGroupD1Ev
	.word	_ZN13CIwSoundGroupD0Ev
	.word	_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX
	.word	_ZN13CIwSoundGroup10ParseCloseEP16CIwTextParserITX
	.word	_ZN13CIwSoundGroup14ParseAttributeEP16CIwTextParserITXPKc
	.word	_ZN13CIwSoundGroup9SerialiseEv
	.word	_ZN10CIwManaged7ResolveEv
	.word	_ZN10CIwManaged15ParseCloseChildEP16CIwTextParserITXPS_
	.word	_ZN10CIwManaged11HandleEventEP8CIwEvent
	.word	_ZN10CIwManaged7SetNameEPKc
	.word	_ZNK13CIwSoundGroup12GetClassNameEv
	.word	_ZN10CIwManaged11DebugRenderEv
	.word	_ZN18CIwManagedRefCount17DebugAddMenuItemsEP7CIwMenu
	.word	_ZN18CIwManagedRefCount8_ReplaceEP10CIwManaged
	.word	_ZN11CIwResource10ApplyScaleEi
	.type	_ZTS13CIwSoundGroup, %object
	.size	_ZTS13CIwSoundGroup, 16
_ZTS13CIwSoundGroup:
	.ascii	"13CIwSoundGroup\000"
	.type	_ZTI13CIwSoundGroup, %object
	.size	_ZTI13CIwSoundGroup, 12
_ZTI13CIwSoundGroup:
	.word	_ZTVN10__cxxabiv120__si_class_type_infoE+8
	.word	_ZTS13CIwSoundGroup
	.word	_ZTI11CIwResource
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"CIwSoundGroup\000"
	.hidden	_ZTV13CIwSoundGroup
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
