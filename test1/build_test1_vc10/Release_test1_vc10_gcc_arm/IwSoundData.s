	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"IwSoundData.cpp"
	.section	.text._ZN10CIwManaged9ParseOpenEP16CIwTextParserITX,"axG",%progbits,_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX,comdat
	.align	2
	.weak	_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX
	.hidden	_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX
	.type	_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX, %function
_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX:
	.fnstart
.LFB301:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX, .-_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX
	.section	.text._ZN10CIwManaged11HandleEventEP8CIwEvent,"axG",%progbits,_ZN10CIwManaged11HandleEventEP8CIwEvent,comdat
	.align	2
	.weak	_ZN10CIwManaged11HandleEventEP8CIwEvent
	.hidden	_ZN10CIwManaged11HandleEventEP8CIwEvent
	.type	_ZN10CIwManaged11HandleEventEP8CIwEvent, %function
_ZN10CIwManaged11HandleEventEP8CIwEvent:
	.fnstart
.LFB302:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #0
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN10CIwManaged11HandleEventEP8CIwEvent, .-_ZN10CIwManaged11HandleEventEP8CIwEvent
	.section	.text._ZN10CIwManaged11DebugRenderEv,"axG",%progbits,_ZN10CIwManaged11DebugRenderEv,comdat
	.align	2
	.weak	_ZN10CIwManaged11DebugRenderEv
	.hidden	_ZN10CIwManaged11DebugRenderEv
	.type	_ZN10CIwManaged11DebugRenderEv, %function
_ZN10CIwManaged11DebugRenderEv:
	.fnstart
.LFB304:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN10CIwManaged11DebugRenderEv, .-_ZN10CIwManaged11DebugRenderEv
	.section	.text._ZN11CIwResource10ApplyScaleEi,"axG",%progbits,_ZN11CIwResource10ApplyScaleEi,comdat
	.align	2
	.weak	_ZN11CIwResource10ApplyScaleEi
	.hidden	_ZN11CIwResource10ApplyScaleEi
	.type	_ZN11CIwResource10ApplyScaleEi, %function
_ZN11CIwResource10ApplyScaleEi:
	.fnstart
.LFB330:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN11CIwResource10ApplyScaleEi, .-_ZN11CIwResource10ApplyScaleEi
	.section	.text._Z20_GetCIwSoundDataSizev,"ax",%progbits
	.align	2
	.global	_Z20_GetCIwSoundDataSizev
	.hidden	_Z20_GetCIwSoundDataSizev
	.type	_Z20_GetCIwSoundDataSizev, %function
_Z20_GetCIwSoundDataSizev:
	.fnstart
.LFB1370:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #36
	bx	lr
	.cantunwind
	.fnend
	.size	_Z20_GetCIwSoundDataSizev, .-_Z20_GetCIwSoundDataSizev
	.section	.text._ZNK12CIwSoundData12GetClassNameEv,"ax",%progbits
	.align	2
	.global	_ZNK12CIwSoundData12GetClassNameEv
	.hidden	_ZNK12CIwSoundData12GetClassNameEv
	.type	_ZNK12CIwSoundData12GetClassNameEv, %function
_ZNK12CIwSoundData12GetClassNameEv:
	.fnstart
.LFB1371:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L13
	bx	lr
.L14:
	.align	2
.L13:
	.word	.LC0
	.cantunwind
	.fnend
	.size	_ZNK12CIwSoundData12GetClassNameEv, .-_ZNK12CIwSoundData12GetClassNameEv
	.section	.text._ZNK12CIwSoundData13GetBufferSizeEv,"ax",%progbits
	.align	2
	.global	_ZNK12CIwSoundData13GetBufferSizeEv
	.hidden	_ZNK12CIwSoundData13GetBufferSizeEv
	.type	_ZNK12CIwSoundData13GetBufferSizeEv, %function
_ZNK12CIwSoundData13GetBufferSizeEv:
	.fnstart
.LFB1383:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, [r0, #16]
	bx	lr
	.cantunwind
	.fnend
	.size	_ZNK12CIwSoundData13GetBufferSizeEv, .-_ZNK12CIwSoundData13GetBufferSizeEv
	.section	.text._Z25_GetCIwSoundDataADPCMSizev,"ax",%progbits
	.align	2
	.global	_Z25_GetCIwSoundDataADPCMSizev
	.hidden	_Z25_GetCIwSoundDataADPCMSizev
	.type	_Z25_GetCIwSoundDataADPCMSizev, %function
_Z25_GetCIwSoundDataADPCMSizev:
	.fnstart
.LFB1387:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #44
	bx	lr
	.cantunwind
	.fnend
	.size	_Z25_GetCIwSoundDataADPCMSizev, .-_Z25_GetCIwSoundDataADPCMSizev
	.section	.text._ZNK17CIwSoundDataADPCM12GetClassNameEv,"ax",%progbits
	.align	2
	.global	_ZNK17CIwSoundDataADPCM12GetClassNameEv
	.hidden	_ZNK17CIwSoundDataADPCM12GetClassNameEv
	.type	_ZNK17CIwSoundDataADPCM12GetClassNameEv, %function
_ZNK17CIwSoundDataADPCM12GetClassNameEv:
	.fnstart
.LFB1388:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L21
	bx	lr
.L22:
	.align	2
.L21:
	.word	.LC1
	.cantunwind
	.fnend
	.size	_ZNK17CIwSoundDataADPCM12GetClassNameEv, .-_ZNK17CIwSoundDataADPCM12GetClassNameEv
	.section	.text._ZN12CIwSoundData13SetBufferSizeEj,"ax",%progbits
	.align	2
	.global	_ZN12CIwSoundData13SetBufferSizeEj
	.hidden	_ZN12CIwSoundData13SetBufferSizeEj
	.type	_ZN12CIwSoundData13SetBufferSizeEj, %function
_ZN12CIwSoundData13SetBufferSizeEj:
	.fnstart
.LFB1382:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	ldr	r3, [r0, #16]
	cmp	r3, r1
	mov	r4, r0
	mov	r5, r1
	beq	.L34
	ldr	r0, [r0, #24]
	cmp	r0, #0
	blne	_ZdlPv
.L26:
	ldr	r3, [r4, #32]
	cmp	r3, #0
	str	r5, [r4, #20]
	streq	r5, [r4, #20]
	bne	.L35
.L30:
	str	r5, [r4, #16]
	mov	r0, r5
	bl	_Znaj
	str	r0, [r4, #24]
.L31:
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L35:
	cmp	r3, #2
	moveq	r3, r5, asl #1
	movne	r3, r5, lsr #1
	str	r3, [r4, #20]
	b	.L30
.L34:
	ldr	r0, [r0, #24]
	cmp	r0, #0
	bne	.L31
	b	.L26
	.fnend
	.size	_ZN12CIwSoundData13SetBufferSizeEj, .-_ZN12CIwSoundData13SetBufferSizeEj
	.section	.text._ZN12CIwSoundDataD0Ev,"ax",%progbits
	.align	2
	.global	_ZN12CIwSoundDataD0Ev
	.hidden	_ZN12CIwSoundDataD0Ev
	.type	_ZN12CIwSoundDataD0Ev, %function
_ZN12CIwSoundDataD0Ev:
	.fnstart
.LFB1381:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	ldr	r0, [r0, #24]
	ldr	r3, .L39
	cmp	r0, #0
	str	r3, [r4, #0]
	blne	_ZdaPv
.L37:
	ldr	r0, .L39+4
	str	r0, [r4, #0]
	mov	r0, r4
	bl	_ZN10CIwManagedD2Ev
	mov	r0, r4
	bl	_ZdlPv
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L40:
	.align	2
.L39:
	.word	.LANCHOR0+8
	.word	_ZTV18CIwManagedRefCount+8
	.fnend
	.size	_ZN12CIwSoundDataD0Ev, .-_ZN12CIwSoundDataD0Ev
	.section	.text._ZN12CIwSoundDataD1Ev,"ax",%progbits
	.align	2
	.global	_ZN12CIwSoundDataD1Ev
	.hidden	_ZN12CIwSoundDataD1Ev
	.type	_ZN12CIwSoundDataD1Ev, %function
_ZN12CIwSoundDataD1Ev:
	.fnstart
.LFB1380:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	ldr	r0, [r0, #24]
	ldr	r3, .L44
	cmp	r0, #0
	str	r3, [r4, #0]
	blne	_ZdaPv
.L42:
	ldr	r0, .L44+4
	str	r0, [r4, #0]
	mov	r0, r4
	bl	_ZN10CIwManagedD2Ev
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L45:
	.align	2
.L44:
	.word	.LANCHOR0+8
	.word	_ZTV18CIwManagedRefCount+8
	.fnend
	.size	_ZN12CIwSoundDataD1Ev, .-_ZN12CIwSoundDataD1Ev
	.section	.text._ZN12CIwSoundDataD2Ev,"ax",%progbits
	.align	2
	.global	_ZN12CIwSoundDataD2Ev
	.hidden	_ZN12CIwSoundDataD2Ev
	.type	_ZN12CIwSoundDataD2Ev, %function
_ZN12CIwSoundDataD2Ev:
	.fnstart
.LFB1379:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	ldr	r0, [r0, #24]
	ldr	r3, .L49
	cmp	r0, #0
	str	r3, [r4, #0]
	blne	_ZdaPv
.L47:
	ldr	r0, .L49+4
	str	r0, [r4, #0]
	mov	r0, r4
	bl	_ZN10CIwManagedD2Ev
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L50:
	.align	2
.L49:
	.word	.LANCHOR0+8
	.word	_ZTV18CIwManagedRefCount+8
	.fnend
	.size	_ZN12CIwSoundDataD2Ev, .-_ZN12CIwSoundDataD2Ev
	.section	.text._ZN12CIwSoundDataC1E17IwSoundDataFormatj,"ax",%progbits
	.align	2
	.global	_ZN12CIwSoundDataC1E17IwSoundDataFormatj
	.hidden	_ZN12CIwSoundDataC1E17IwSoundDataFormatj
	.type	_ZN12CIwSoundDataC1E17IwSoundDataFormatj, %function
_ZN12CIwSoundDataC1E17IwSoundDataFormatj:
	.fnstart
.LFB1377:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	mov	r5, r0
	mov	r6, r2
	mov	r4, r1
	bl	_ZN10CIwManagedC2Ev
	ldr	r2, .L55
	mov	r3, #0
	str	r2, [r5, #0]
	str	r3, [r5, #24]
	str	r6, [r5, #28]
	str	r4, [r5, #32]
	strh	r3, [r5, #12]	@ movhi
	strh	r3, [r5, #14]	@ movhi
	str	r3, [r5, #16]
	str	r3, [r5, #20]
	mov	r0, r5
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L56:
	.align	2
.L55:
	.word	.LANCHOR0+8
	.fnend
	.size	_ZN12CIwSoundDataC1E17IwSoundDataFormatj, .-_ZN12CIwSoundDataC1E17IwSoundDataFormatj
	.section	.text._ZN12CIwSoundDataC2E17IwSoundDataFormatj,"ax",%progbits
	.align	2
	.global	_ZN12CIwSoundDataC2E17IwSoundDataFormatj
	.hidden	_ZN12CIwSoundDataC2E17IwSoundDataFormatj
	.type	_ZN12CIwSoundDataC2E17IwSoundDataFormatj, %function
_ZN12CIwSoundDataC2E17IwSoundDataFormatj:
	.fnstart
.LFB1376:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	mov	r5, r0
	mov	r6, r2
	mov	r4, r1
	bl	_ZN10CIwManagedC2Ev
	ldr	r2, .L61
	mov	r3, #0
	str	r2, [r5, #0]
	str	r3, [r5, #24]
	str	r6, [r5, #28]
	str	r4, [r5, #32]
	strh	r3, [r5, #12]	@ movhi
	strh	r3, [r5, #14]	@ movhi
	str	r3, [r5, #16]
	str	r3, [r5, #20]
	mov	r0, r5
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L62:
	.align	2
.L61:
	.word	.LANCHOR0+8
	.fnend
	.size	_ZN12CIwSoundDataC2E17IwSoundDataFormatj, .-_ZN12CIwSoundDataC2E17IwSoundDataFormatj
	.section	.text._ZN12CIwSoundDataC1Ev,"ax",%progbits
	.align	2
	.global	_ZN12CIwSoundDataC1Ev
	.hidden	_ZN12CIwSoundDataC1Ev
	.type	_ZN12CIwSoundDataC1Ev, %function
_ZN12CIwSoundDataC1Ev:
	.fnstart
.LFB1374:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	bl	_ZN10CIwManagedC2Ev
	mov	ip, #44032
	ldr	r1, .L67
	mov	r3, #0
	add	r0, ip, #68
	mov	r2, #1
	str	r0, [r4, #28]
	str	r1, [r4, #0]
	str	r3, [r4, #24]
	str	r2, [r4, #32]
	strh	r3, [r4, #12]	@ movhi
	strh	r3, [r4, #14]	@ movhi
	str	r3, [r4, #16]
	str	r3, [r4, #20]
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L68:
	.align	2
.L67:
	.word	.LANCHOR0+8
	.fnend
	.size	_ZN12CIwSoundDataC1Ev, .-_ZN12CIwSoundDataC1Ev
	.section	.text._ZN12CIwSoundDataC2Ev,"ax",%progbits
	.align	2
	.global	_ZN12CIwSoundDataC2Ev
	.hidden	_ZN12CIwSoundDataC2Ev
	.type	_ZN12CIwSoundDataC2Ev, %function
_ZN12CIwSoundDataC2Ev:
	.fnstart
.LFB1373:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	bl	_ZN10CIwManagedC2Ev
	mov	ip, #44032
	ldr	r1, .L73
	mov	r3, #0
	add	r0, ip, #68
	mov	r2, #1
	str	r0, [r4, #28]
	str	r1, [r4, #0]
	str	r3, [r4, #24]
	str	r2, [r4, #32]
	strh	r3, [r4, #12]	@ movhi
	strh	r3, [r4, #14]	@ movhi
	str	r3, [r4, #16]
	str	r3, [r4, #20]
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L74:
	.align	2
.L73:
	.word	.LANCHOR0+8
	.fnend
	.size	_ZN12CIwSoundDataC2Ev, .-_ZN12CIwSoundDataC2Ev
	.section	.text._ZN12CIwSoundData14SwitchDataSignEv,"ax",%progbits
	.align	2
	.global	_ZN12CIwSoundData14SwitchDataSignEv
	.hidden	_ZN12CIwSoundData14SwitchDataSignEv
	.type	_ZN12CIwSoundData14SwitchDataSignEv, %function
_ZN12CIwSoundData14SwitchDataSignEv:
	.fnstart
.LFB1384:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	ip, [r0, #16]
	cmp	ip, #0
	ldr	r2, [r0, #24]
	bxeq	lr
	ldrb	r1, [r2, #0]	@ zero_extendqisi2
	mov	r3, #1
	sub	r0, r1, #128
	cmp	r3, ip
	sub	r1, ip, #1
	strb	r0, [r2, #0]
	and	r1, r1, #3
	bxcs	lr
	cmp	r1, #0
	beq	.L78
	cmp	r1, #1
	beq	.L92
	cmp	r1, #2
	movne	r3, r2
	ldrneb	r1, [r3, #1]!	@ zero_extendqisi2
	subne	r1, r1, #128
	strneb	r1, [r3, #0]
	movne	r3, #2
	add	r1, r2, r3
	ldrb	r0, [r1, #0]	@ zero_extendqisi2
	sub	r0, r0, #128
	strb	r0, [r1, #0]
	add	r3, r3, #1
.L92:
	add	r1, r2, r3
	ldrb	r0, [r1, #0]	@ zero_extendqisi2
	add	r3, r3, #1
	sub	r0, r0, #128
	cmp	r3, ip
	strb	r0, [r1, #0]
	bxcs	lr
.L78:
	ldrb	r0, [r2, r3]	@ zero_extendqisi2
	sub	r1, r0, #128
	strb	r1, [r2, r3]
	add	r1, r3, #1
	ldrb	r0, [r2, r1]	@ zero_extendqisi2
	sub	r0, r0, #128
	strb	r0, [r2, r1]
	add	r1, r1, #1
	ldrb	r0, [r2, r1]	@ zero_extendqisi2
	sub	r0, r0, #128
	strb	r0, [r2, r1]
	add	r1, r3, #3
	ldrb	r0, [r2, r1]	@ zero_extendqisi2
	add	r3, r3, #4
	sub	r0, r0, #128
	cmp	r3, ip
	strb	r0, [r2, r1]
	bcc	.L78
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN12CIwSoundData14SwitchDataSignEv, .-_ZN12CIwSoundData14SwitchDataSignEv
	.global	__cxa_end_cleanup
	.section	.text._Z20_CIwSoundDataFactoryv,"ax",%progbits
	.align	2
	.global	_Z20_CIwSoundDataFactoryv
	.hidden	_Z20_CIwSoundDataFactoryv
	.type	_Z20_CIwSoundDataFactoryv, %function
_Z20_CIwSoundDataFactoryv:
	.fnstart
.LFB1369:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	mov	r0, #36
.LEHB0:
	bl	_Znwj
.LEHE0:
	mov	r4, r0
.LEHB1:
	bl	_ZN10CIwManagedC2Ev
.LEHE1:
	mov	ip, #44032
	ldr	r1, .L100
	mov	r3, #0
	add	r0, ip, #68
	mov	r2, #1
	str	r0, [r4, #28]
	str	r1, [r4, #0]
	str	r3, [r4, #24]
	str	r2, [r4, #32]
	strh	r3, [r4, #12]	@ movhi
	strh	r3, [r4, #14]	@ movhi
	str	r3, [r4, #16]
	str	r3, [r4, #20]
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L99:
.L97:
	mov	r5, r0
	mov	r0, r4
	bl	_ZdlPv
	mov	r0, r5
.LEHB2:
	bl	__cxa_end_cleanup
.LEHE2:
.L101:
	.align	2
.L100:
	.word	.LANCHOR0+8
	.global	__gxx_personality_v0
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA1369:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE1369-.LLSDACSB1369
.LLSDACSB1369:
	.uleb128 .LEHB0-.LFB1369
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB1-.LFB1369
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L99-.LFB1369
	.uleb128 0x0
	.uleb128 .LEHB2-.LFB1369
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE1369:
	.fnend
	.size	_Z20_CIwSoundDataFactoryv, .-_Z20_CIwSoundDataFactoryv
	.section	.text._ZN17CIwSoundDataADPCMC2E17IwSoundDataFormatjjj,"ax",%progbits
	.align	2
	.global	_ZN17CIwSoundDataADPCMC2E17IwSoundDataFormatjjj
	.hidden	_ZN17CIwSoundDataADPCMC2E17IwSoundDataFormatjjj
	.type	_ZN17CIwSoundDataADPCMC2E17IwSoundDataFormatjjj, %function
_ZN17CIwSoundDataADPCMC2E17IwSoundDataFormatjjj:
	.fnstart
.LFB1393:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, lr}
	.save {r3, r4, r5, r6, r7, lr}
	mov	r6, r0
	mov	r5, r2
	mov	r7, r3
	mov	r4, r1
	bl	_ZN10CIwManagedC2Ev
	ldr	r0, .L107
	mov	r3, #0
	str	r0, [r6, #0]
	str	r3, [r6, #24]
	str	r5, [r6, #28]
	str	r4, [r6, #32]
	str	r7, [r6, #36]
	ldr	r2, [sp, #24]
	mov	r0, r6
	str	r2, [r6, #40]
	strh	r3, [r6, #12]	@ movhi
	strh	r3, [r6, #14]	@ movhi
	str	r3, [r6, #16]
	str	r3, [r6, #20]
	ldmfd	sp!, {r3, r4, r5, r6, r7, lr}
	bx	lr
.L108:
	.align	2
.L107:
	.word	.LANCHOR0+80
	.fnend
	.size	_ZN17CIwSoundDataADPCMC2E17IwSoundDataFormatjjj, .-_ZN17CIwSoundDataADPCMC2E17IwSoundDataFormatjjj
	.section	.text._ZN17CIwSoundDataADPCMC1E17IwSoundDataFormatjjj,"ax",%progbits
	.align	2
	.global	_ZN17CIwSoundDataADPCMC1E17IwSoundDataFormatjjj
	.hidden	_ZN17CIwSoundDataADPCMC1E17IwSoundDataFormatjjj
	.type	_ZN17CIwSoundDataADPCMC1E17IwSoundDataFormatjjj, %function
_ZN17CIwSoundDataADPCMC1E17IwSoundDataFormatjjj:
	.fnstart
.LFB1394:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, lr}
	.save {r3, r4, r5, r6, r7, lr}
	mov	r6, r0
	mov	r5, r2
	mov	r7, r3
	mov	r4, r1
	bl	_ZN10CIwManagedC2Ev
	ldr	r0, .L114
	mov	r3, #0
	str	r0, [r6, #0]
	str	r3, [r6, #24]
	str	r5, [r6, #28]
	str	r4, [r6, #32]
	str	r7, [r6, #36]
	ldr	r2, [sp, #24]
	mov	r0, r6
	str	r2, [r6, #40]
	strh	r3, [r6, #12]	@ movhi
	strh	r3, [r6, #14]	@ movhi
	str	r3, [r6, #16]
	str	r3, [r6, #20]
	ldmfd	sp!, {r3, r4, r5, r6, r7, lr}
	bx	lr
.L115:
	.align	2
.L114:
	.word	.LANCHOR0+80
	.fnend
	.size	_ZN17CIwSoundDataADPCMC1E17IwSoundDataFormatjjj, .-_ZN17CIwSoundDataADPCMC1E17IwSoundDataFormatjjj
	.section	.text._ZN17CIwSoundDataADPCMC1Ev,"ax",%progbits
	.align	2
	.global	_ZN17CIwSoundDataADPCMC1Ev
	.hidden	_ZN17CIwSoundDataADPCMC1Ev
	.type	_ZN17CIwSoundDataADPCMC1Ev, %function
_ZN17CIwSoundDataADPCMC1Ev:
	.fnstart
.LFB1391:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	bl	_ZN10CIwManagedC2Ev
	mov	ip, #44032
	ldr	r2, .L121
	mov	r3, #0
	add	r1, ip, #68
	mov	r0, #1
	str	r0, [r4, #32]
	str	r1, [r4, #28]
	str	r2, [r4, #0]
	str	r3, [r4, #40]
	strh	r3, [r4, #12]	@ movhi
	strh	r3, [r4, #14]	@ movhi
	str	r3, [r4, #16]
	str	r3, [r4, #20]
	str	r3, [r4, #24]
	str	r3, [r4, #36]
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L122:
	.align	2
.L121:
	.word	.LANCHOR0+80
	.fnend
	.size	_ZN17CIwSoundDataADPCMC1Ev, .-_ZN17CIwSoundDataADPCMC1Ev
	.section	.text._ZN17CIwSoundDataADPCMC2Ev,"ax",%progbits
	.align	2
	.global	_ZN17CIwSoundDataADPCMC2Ev
	.hidden	_ZN17CIwSoundDataADPCMC2Ev
	.type	_ZN17CIwSoundDataADPCMC2Ev, %function
_ZN17CIwSoundDataADPCMC2Ev:
	.fnstart
.LFB1390:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	bl	_ZN10CIwManagedC2Ev
	mov	ip, #44032
	ldr	r2, .L128
	mov	r3, #0
	add	r1, ip, #68
	mov	r0, #1
	str	r0, [r4, #32]
	str	r1, [r4, #28]
	str	r2, [r4, #0]
	str	r3, [r4, #40]
	strh	r3, [r4, #12]	@ movhi
	strh	r3, [r4, #14]	@ movhi
	str	r3, [r4, #16]
	str	r3, [r4, #20]
	str	r3, [r4, #24]
	str	r3, [r4, #36]
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L129:
	.align	2
.L128:
	.word	.LANCHOR0+80
	.fnend
	.size	_ZN17CIwSoundDataADPCMC2Ev, .-_ZN17CIwSoundDataADPCMC2Ev
	.section	.text._Z25_CIwSoundDataADPCMFactoryv,"ax",%progbits
	.align	2
	.global	_Z25_CIwSoundDataADPCMFactoryv
	.hidden	_Z25_CIwSoundDataADPCMFactoryv
	.type	_Z25_CIwSoundDataADPCMFactoryv, %function
_Z25_CIwSoundDataADPCMFactoryv:
	.fnstart
.LFB1386:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	mov	r0, #44
.LEHB3:
	bl	_Znwj
.LEHE3:
	mov	r4, r0
.LEHB4:
	bl	_ZN10CIwManagedC2Ev
.LEHE4:
	mov	ip, #44032
	ldr	r2, .L137
	mov	r3, #0
	add	r1, ip, #68
	mov	r0, #1
	str	r0, [r4, #32]
	str	r1, [r4, #28]
	str	r2, [r4, #0]
	str	r3, [r4, #40]
	strh	r3, [r4, #12]	@ movhi
	strh	r3, [r4, #14]	@ movhi
	str	r3, [r4, #16]
	str	r3, [r4, #20]
	str	r3, [r4, #24]
	str	r3, [r4, #36]
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L136:
.L134:
	mov	r5, r0
	mov	r0, r4
	bl	_ZdlPv
	mov	r0, r5
.LEHB5:
	bl	__cxa_end_cleanup
.LEHE5:
.L138:
	.align	2
.L137:
	.word	.LANCHOR0+80
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA1386:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE1386-.LLSDACSB1386
.LLSDACSB1386:
	.uleb128 .LEHB3-.LFB1386
	.uleb128 .LEHE3-.LEHB3
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB4-.LFB1386
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L136-.LFB1386
	.uleb128 0x0
	.uleb128 .LEHB5-.LFB1386
	.uleb128 .LEHE5-.LEHB5
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE1386:
	.fnend
	.size	_Z25_CIwSoundDataADPCMFactoryv, .-_Z25_CIwSoundDataADPCMFactoryv
	.section	.text._ZN17CIwSoundDataADPCMD0Ev,"axG",%progbits,_ZN17CIwSoundDataADPCMD0Ev,comdat
	.align	2
	.weak	_ZN17CIwSoundDataADPCMD0Ev
	.hidden	_ZN17CIwSoundDataADPCMD0Ev
	.type	_ZN17CIwSoundDataADPCMD0Ev, %function
_ZN17CIwSoundDataADPCMD0Ev:
	.fnstart
.LFB1546:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	ldr	r0, [r0, #24]
	ldr	r3, .L142
	cmp	r0, #0
	str	r3, [r4, #0]
	blne	_ZdaPv
.L140:
	ldr	r0, .L142+4
	str	r0, [r4, #0]
	mov	r0, r4
	bl	_ZN10CIwManagedD2Ev
	mov	r0, r4
	bl	_ZdlPv
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L143:
	.align	2
.L142:
	.word	.LANCHOR0+8
	.word	_ZTV18CIwManagedRefCount+8
	.fnend
	.size	_ZN17CIwSoundDataADPCMD0Ev, .-_ZN17CIwSoundDataADPCMD0Ev
	.section	.text._ZN17CIwSoundDataADPCMD1Ev,"axG",%progbits,_ZN17CIwSoundDataADPCMD1Ev,comdat
	.align	2
	.weak	_ZN17CIwSoundDataADPCMD1Ev
	.hidden	_ZN17CIwSoundDataADPCMD1Ev
	.type	_ZN17CIwSoundDataADPCMD1Ev, %function
_ZN17CIwSoundDataADPCMD1Ev:
	.fnstart
.LFB1545:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	ldr	r0, [r0, #24]
	ldr	r3, .L147
	cmp	r0, #0
	str	r3, [r4, #0]
	blne	_ZdaPv
.L145:
	ldr	r0, .L147+4
	str	r0, [r4, #0]
	mov	r0, r4
	bl	_ZN10CIwManagedD2Ev
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L148:
	.align	2
.L147:
	.word	.LANCHOR0+8
	.word	_ZTV18CIwManagedRefCount+8
	.fnend
	.size	_ZN17CIwSoundDataADPCMD1Ev, .-_ZN17CIwSoundDataADPCMD1Ev
	.section	.text._ZN12CIwSoundData9SerialiseEv,"ax",%progbits
	.align	2
	.global	_ZN12CIwSoundData9SerialiseEv
	.hidden	_ZN12CIwSoundData9SerialiseEv
	.type	_ZN12CIwSoundData9SerialiseEv, %function
_ZN12CIwSoundData9SerialiseEv:
	.fnstart
.LFB1385:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	bl	_ZN10CIwManaged9SerialiseEv
	add	r0, r4, #16
	mov	r3, #4
	mov	r1, #1
	mov	r2, #32
	bl	_Z17IwSerialiseUInt32Rjiii
	ldr	r3, [r4, #24]
	cmp	r3, #0
	ldr	r0, [r4, #16]
	beq	.L157
.L150:
	add	r0, r4, #20
	mov	r1, #1
	mov	r2, #32
	mov	r3, #4
	bl	_Z17IwSerialiseUInt32Rjiii
	ldr	r0, [r4, #24]
	ldr	r1, [r4, #16]
	mov	r2, #7
	mov	r3, #1
	bl	_Z15IwSerialiseInt8Raiii
	add	r0, r4, #28
	mov	r1, #1
	mov	r2, #32
	mov	r3, #4
	bl	_Z17IwSerialiseUInt32Rjiii
	add	r0, r4, #32
	mov	r1, #1
	mov	r2, #16
	mov	r3, #2
	bl	_Z17IwSerialiseUInt16Rtiii
	ldmfd	sp!, {r4, lr}
	bx	lr
.L157:
	ldr	r3, [r4, #32]
	cmp	r3, #0
	str	r0, [r4, #20]
	streq	r0, [r4, #20]
	bne	.L158
	str	r0, [r4, #16]
	bl	_Znaj
	str	r0, [r4, #24]
	b	.L150
.L158:
	cmp	r3, #2
	moveq	r3, r0, asl #1
	movne	r3, r0, lsr #1
	str	r3, [r4, #20]
	str	r0, [r4, #16]
	bl	_Znaj
	str	r0, [r4, #24]
	b	.L150
	.fnend
	.size	_ZN12CIwSoundData9SerialiseEv, .-_ZN12CIwSoundData9SerialiseEv
	.section	.text._ZN17CIwSoundDataADPCM9SerialiseEv,"ax",%progbits
	.align	2
	.global	_ZN17CIwSoundDataADPCM9SerialiseEv
	.hidden	_ZN17CIwSoundDataADPCM9SerialiseEv
	.type	_ZN17CIwSoundDataADPCM9SerialiseEv, %function
_ZN17CIwSoundDataADPCM9SerialiseEv:
	.fnstart
.LFB1395:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 1424
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.pad #1424
	sub	sp, sp, #1424
	.pad #4
	sub	sp, sp, #4
	mov	r4, r0
	bl	_ZN12CIwSoundData9SerialiseEv
	add	r0, r4, #36
	mov	r1, #1
	mov	r2, #32
	mov	r3, #4
	bl	_Z17IwSerialiseUInt32Rjiii
	add	r0, r4, #40
	mov	r3, #4
	mov	r1, #1
	mov	r2, #32
	bl	_Z17IwSerialiseUInt32Rjiii
	ldr	r0, .L166
	ldrb	r3, [r0, #0]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L164
.L163:
	add	sp, sp, #404
	add	sp, sp, #1024
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L164:
	mov	r0, #5
	bl	s3eSoundGetInt
	cmp	r0, #123
	bne	.L163
	ldr	r1, [r4, #32]
	cmp	r1, #2
	bne	.L163
	ldr	r8, [r4, #20]
	add	r5, r8, #22
	mov	r0, r5, asl #1
	bl	_Znaj
	ldr	r7, .L166+4
	ldr	r6, [r7, #0]
	mov	r2, #0
	cmp	r6, #0
	mov	r5, r0
	str	r2, [sp, #20]
	beq	.L165
.L161:
	ldr	r0, [r4, #16]
	mov	r3, #0
	ldr	sl, [r4, #20]
	ldr	r1, [r4, #24]
	mov	r9, r0, lsr #1
	add	r2, r5, #44
	mov	r0, r3
	str	r3, [sp, #1392]
	strb	r3, [sp, #1420]
	str	r3, [sp, #1404]
	str	r3, [sp, #1416]
	str	sl, [sp, #1400]
	str	r9, [sp, #1412]
	str	r1, [sp, #1408]
	str	r2, [sp, #1396]
	bl	s3eSoundChannelStop
	mov	r1, #2
	mov	r2, r4
	ldr	r0, [sp, #1392]
	bl	s3eSoundChannelSetInt
	mov	r0, #1
	ldr	r8, [sp, #1392]
	bl	s3eSoundGetInt
	mov	r1, #1
	mov	r2, r0
	mov	r0, r8
	bl	s3eSoundChannelSetInt
	mov	r2, #256
	mov	r1, #3
	ldr	r0, [sp, #1392]
	bl	s3eSoundChannelSetInt
	add	r1, sp, #1392
	add	r0, sp, #20
	bl	_ZN15CIwChannelADPCM13GenerateAudioEP20s3eSoundGenAudioInfo
	ldr	r7, .L166+8
	add	fp, sp, #1344
	mov	r8, r0
	ldmia	r7!, {r0, r1, r2, r3}
	mov	ip, fp
	stmia	ip!, {r0, r1, r2, r3}
	ldmia	r7!, {r0, r1, r2, r3}
	mov	r6, ip
	stmia	r6!, {r0, r1, r2, r3}
	ldmia	r7, {r0, r1, r2, r3}
	mov	lr, r6
	stmia	lr!, {r0, r1, r2}
	mov	r6, r8, asl #1
	ldr	r7, [r4, #28]
	mov	r1, fp
	mov	r2, #44
	strb	r3, [lr, #0]
	mov	r0, r5
	bl	memcpy
	add	r3, r6, #36
	mov	ip, r3, lsr #24
	mov	sl, r7, lsr #24
	mov	r9, r6, lsr #24
	str	sl, [sp, #4]
	str	r9, [sp, #12]
	strb	ip, [r5, #7]
	mov	ip, #16
	strb	ip, [r5, #34]
	mov	r0, r7, asr #8
	mov	ip, #1
	mov	r9, r3, asr #8
	mov	sl, r3, asr #16
	mov	r1, r7, asr #16
	strb	r9, [r5, #5]
	strb	sl, [r5, #6]
	strb	ip, [r5, #22]
	strb	r0, [r5, #25]
	strb	r1, [r5, #26]
	ldr	r0, [sp, #4]
	mov	fp, r6, asr #8
	mov	r2, r6, asr #16
	strb	r0, [r5, #27]
	strb	fp, [r5, #41]
	strb	r2, [r5, #42]
	ldr	ip, [sp, #12]
	strb	r3, [r5, #4]
	strb	ip, [r5, #43]
	strb	r7, [r5, #24]
	strb	r6, [r5, #40]
	ldr	r0, [r4, #24]
	cmp	r0, #0
	blne	_ZdaPv
.L162:
	add	r3, r8, #22
	mov	ip, r3, asl #1
	mov	r2, #1
	str	r2, [r4, #32]
	str	r5, [r4, #24]
	str	ip, [r4, #16]
	str	r8, [r4, #20]
	b	.L163
.L165:
	bl	_ZN15CIwChannelADPCM4InitEv
	b	.L161
.L167:
	.align	2
.L166:
	.word	g_IwSerialiseContext
	.word	_ZN15CIwChannelADPCM13isInitializedE
	.word	.LANCHOR0+140
	.fnend
	.size	_ZN17CIwSoundDataADPCM9SerialiseEv, .-_ZN17CIwSoundDataADPCM9SerialiseEv
	.hidden	_ZTV17CIwSoundDataADPCM
	.global	_ZTV17CIwSoundDataADPCM
	.hidden	_ZTV12CIwSoundData
	.global	_ZTV12CIwSoundData
	.hidden	_ZTS17CIwSoundDataADPCM
	.global	_ZTS17CIwSoundDataADPCM
	.hidden	_ZTI17CIwSoundDataADPCM
	.global	_ZTI17CIwSoundDataADPCM
	.hidden	_ZTS12CIwSoundData
	.global	_ZTS12CIwSoundData
	.hidden	_ZTI12CIwSoundData
	.global	_ZTI12CIwSoundData
	.section	.rodata
	.align	3
	.set	.LANCHOR0,. + 0
	.type	_ZTV12CIwSoundData, %object
	.size	_ZTV12CIwSoundData, 68
_ZTV12CIwSoundData:
	.word	0
	.word	_ZTI12CIwSoundData
	.word	_ZN12CIwSoundDataD1Ev
	.word	_ZN12CIwSoundDataD0Ev
	.word	_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX
	.word	_ZN10CIwManaged10ParseCloseEP16CIwTextParserITX
	.word	_ZN10CIwManaged14ParseAttributeEP16CIwTextParserITXPKc
	.word	_ZN12CIwSoundData9SerialiseEv
	.word	_ZN10CIwManaged7ResolveEv
	.word	_ZN10CIwManaged15ParseCloseChildEP16CIwTextParserITXPS_
	.word	_ZN10CIwManaged11HandleEventEP8CIwEvent
	.word	_ZN10CIwManaged7SetNameEPKc
	.word	_ZNK12CIwSoundData12GetClassNameEv
	.word	_ZN10CIwManaged11DebugRenderEv
	.word	_ZN18CIwManagedRefCount17DebugAddMenuItemsEP7CIwMenu
	.word	_ZN18CIwManagedRefCount8_ReplaceEP10CIwManaged
	.word	_ZN11CIwResource10ApplyScaleEi
	.space	4
	.type	_ZTV17CIwSoundDataADPCM, %object
	.size	_ZTV17CIwSoundDataADPCM, 68
_ZTV17CIwSoundDataADPCM:
	.word	0
	.word	_ZTI17CIwSoundDataADPCM
	.word	_ZN17CIwSoundDataADPCMD1Ev
	.word	_ZN17CIwSoundDataADPCMD0Ev
	.word	_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX
	.word	_ZN10CIwManaged10ParseCloseEP16CIwTextParserITX
	.word	_ZN10CIwManaged14ParseAttributeEP16CIwTextParserITXPKc
	.word	_ZN17CIwSoundDataADPCM9SerialiseEv
	.word	_ZN10CIwManaged7ResolveEv
	.word	_ZN10CIwManaged15ParseCloseChildEP16CIwTextParserITXPS_
	.word	_ZN10CIwManaged11HandleEventEP8CIwEvent
	.word	_ZN10CIwManaged7SetNameEPKc
	.word	_ZNK17CIwSoundDataADPCM12GetClassNameEv
	.word	_ZN10CIwManaged11DebugRenderEv
	.word	_ZN18CIwManagedRefCount17DebugAddMenuItemsEP7CIwMenu
	.word	_ZN18CIwManagedRefCount8_ReplaceEP10CIwManaged
	.word	_ZN11CIwResource10ApplyScaleEi
.LC2:
	.ascii	"RIFF\244\374\000\000WAVEfmt \020\000\000\000\001\000"
	.ascii	"\001\000@\037\000\000\200>\000\000\002\000\020\000d"
	.ascii	"ata\200\374\000\000\000"
	.space	3
	.type	_ZTS17CIwSoundDataADPCM, %object
	.size	_ZTS17CIwSoundDataADPCM, 20
_ZTS17CIwSoundDataADPCM:
	.ascii	"17CIwSoundDataADPCM\000"
	.type	_ZTI17CIwSoundDataADPCM, %object
	.size	_ZTI17CIwSoundDataADPCM, 12
_ZTI17CIwSoundDataADPCM:
	.word	_ZTVN10__cxxabiv120__si_class_type_infoE+8
	.word	_ZTS17CIwSoundDataADPCM
	.word	_ZTI12CIwSoundData
	.type	_ZTS12CIwSoundData, %object
	.size	_ZTS12CIwSoundData, 15
_ZTS12CIwSoundData:
	.ascii	"12CIwSoundData\000"
	.space	1
	.type	_ZTI12CIwSoundData, %object
	.size	_ZTI12CIwSoundData, 12
_ZTI12CIwSoundData:
	.word	_ZTVN10__cxxabiv120__si_class_type_infoE+8
	.word	_ZTS12CIwSoundData
	.word	_ZTI11CIwResource
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"CIwSoundData\000"
	.space	3
.LC1:
	.ascii	"CIwSoundDataADPCM\000"
	.hidden	_ZTV17CIwSoundDataADPCM
	.hidden	_ZTV12CIwSoundData
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
