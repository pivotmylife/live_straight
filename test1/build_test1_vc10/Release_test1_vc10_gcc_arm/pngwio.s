	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"pngwio.c"
	.section	.text.png_flush,"ax",%progbits
	.align	2
	.global	png_flush
	.hidden	png_flush
	.type	png_flush, %function
png_flush:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, lr}
	ldr	r3, [r0, #360]
	cmp	r3, #0
	movne	lr, pc
	bxne	r3
.L3:
	ldmfd	sp!, {r3, lr}
	bx	lr
	.size	png_flush, .-png_flush
	.section	.text.png_set_write_fn,"ax",%progbits
	.align	2
	.global	png_set_write_fn
	.hidden	png_set_write_fn
	.type	png_set_write_fn, %function
png_set_write_fn:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	subs	r4, r0, #0
	beq	.L11
	cmp	r2, #0
	ldreq	r2, .L14
	cmp	r3, #0
	ldreq	r3, .L14+4
	str	r3, [r4, #360]
	ldr	r3, [r4, #108]
	cmp	r3, #0
	str	r1, [r4, #112]
	str	r2, [r4, #104]
	beq	.L11
	mov	r0, #0
	str	r0, [r4, #108]
	ldr	r1, .L14+8
	mov	r0, r4
	bl	png_warning
	mov	r0, r4
	ldr	r1, .L14+12
	bl	png_warning
.L11:
	ldmfd	sp!, {r4, lr}
	bx	lr
.L15:
	.align	2
.L14:
	.word	png_default_write_data
	.word	png_default_flush
	.word	.LC0
	.word	.LC1
	.size	png_set_write_fn, .-png_set_write_fn
	.section	.text.png_default_flush,"ax",%progbits
	.align	2
	.global	png_default_flush
	.hidden	png_default_flush
	.type	png_default_flush, %function
png_default_flush:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	stmfd	sp!, {r3, lr}
	beq	.L18
	ldr	r0, [r0, #112]
	cmp	r0, #0
	blne	fflush
.L18:
	ldmfd	sp!, {r3, lr}
	bx	lr
	.size	png_default_flush, .-png_default_flush
	.section	.text.png_default_write_data,"ax",%progbits
	.align	2
	.global	png_default_write_data
	.hidden	png_default_write_data
	.type	png_default_write_data, %function
png_default_write_data:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	subs	r4, r0, #0
	mov	r5, r2
	beq	.L21
	mov	r0, r1
	ldr	r3, [r4, #112]
	mov	r1, #1
	bl	fwrite
	cmp	r5, r0
	movne	r0, r4
	ldrne	r1, .L22
	blne	png_error
.L21:
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L23:
	.align	2
.L22:
	.word	.LC2
	.size	png_default_write_data, .-png_default_write_data
	.section	.text.png_write_data,"ax",%progbits
	.align	2
	.global	png_write_data
	.hidden	png_write_data
	.type	png_write_data, %function
png_write_data:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, lr}
	ldr	r3, [r0, #104]
	cmp	r3, #0
	beq	.L25
	mov	lr, pc
	bx	r3
.L27:
	ldmfd	sp!, {r3, lr}
	bx	lr
.L25:
	ldr	r1, .L28
	bl	png_error
	b	.L27
.L29:
	.align	2
.L28:
	.word	.LC3
	.size	png_write_data, .-png_write_data
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"Attempted to set both read_data_fn and write_data_f"
	.ascii	"n in\000"
.LC1:
	.ascii	"the same structure.  Resetting read_data_fn to NULL"
	.ascii	".\000"
	.space	3
.LC2:
	.ascii	"Write Error\000"
.LC3:
	.ascii	"Call to NULL write function\000"
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
