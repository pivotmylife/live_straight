	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"pngset.c"
	.global	__aeabi_d2f
	.global	__aeabi_dmul
	.global	__aeabi_dadd
	.global	__aeabi_d2iz
	.section	.text.png_set_cHRM,"ax",%progbits
	.align	2
	.global	png_set_cHRM
	.hidden	png_set_cHRM
	.type	png_set_cHRM, %function
png_set_cHRM:
	@ Function supports interworking.
	@ args = 56, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, fp, lr}
	cmp	r0, #0
	cmpne	r1, #0
	mov	r4, r1
	mov	sl, r2
	mov	fp, r3
	add	r6, sp, #32
	ldmia	r6, {r5-r6}
	add	r8, sp, #40
	ldmia	r8, {r7-r8}
	beq	.L3
	mov	r1, r3
	mov	r0, r2
	bl	__aeabi_d2f
	mov	r1, r6
	str	r0, [r4, #128]	@ float
	mov	r0, r5
	bl	__aeabi_d2f
	mov	r1, r8
	str	r0, [r4, #132]	@ float
	mov	r0, r7
	bl	__aeabi_d2f
	str	r0, [r4, #136]	@ float
	add	r1, sp, #48
	ldmia	r1, {r0-r1}
	bl	__aeabi_d2f
	str	r0, [r4, #140]	@ float
	add	r1, sp, #56
	ldmia	r1, {r0-r1}
	bl	__aeabi_d2f
	str	r0, [r4, #144]	@ float
	add	r1, sp, #64
	ldmia	r1, {r0-r1}
	bl	__aeabi_d2f
	str	r0, [r4, #148]	@ float
	add	r1, sp, #72
	ldmia	r1, {r0-r1}
	bl	__aeabi_d2f
	str	r0, [r4, #152]	@ float
	add	r1, sp, #80
	ldmia	r1, {r0-r1}
	bl	__aeabi_d2f
	mov	ip, #1073741824
	add	r3, ip, #16252928
	str	r0, [r4, #156]	@ float
	mov	r2, #0
	add	r3, r3, #27136
	mov	r0, sl
	mov	r1, fp
	bl	__aeabi_dmul
	mov	ip, #1069547520
	add	r3, ip, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2iz
	mov	r3, #1073741824
	add	ip, r3, #16252928
	add	r3, ip, #27136
	str	r0, [r4, #256]
	mov	r2, #0
	mov	r0, r5
	mov	r1, r6
	bl	__aeabi_dmul
	mov	r3, #1069547520
	mov	r2, #0
	add	r3, r3, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2iz
	mov	ip, #1073741824
	add	r3, ip, #16252928
	str	r0, [r4, #260]
	mov	r2, #0
	add	r3, r3, #27136
	mov	r0, r7
	mov	r1, r8
	bl	__aeabi_dmul
	mov	ip, #1069547520
	add	r3, ip, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2iz
	mov	r3, #1073741824
	add	ip, r3, #16252928
	str	r0, [r4, #264]
	add	r3, ip, #27136
	add	r1, sp, #48
	ldmia	r1, {r0-r1}
	mov	r2, #0
	bl	__aeabi_dmul
	mov	r3, #1069547520
	mov	r2, #0
	add	r3, r3, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2iz
	mov	ip, #1073741824
	add	r3, ip, #16252928
	str	r0, [r4, #268]
	add	r1, sp, #56
	ldmia	r1, {r0-r1}
	mov	r2, #0
	add	r3, r3, #27136
	bl	__aeabi_dmul
	mov	ip, #1069547520
	add	r3, ip, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2iz
	mov	r3, #1073741824
	add	ip, r3, #16252928
	str	r0, [r4, #272]
	add	r3, ip, #27136
	add	r1, sp, #64
	ldmia	r1, {r0-r1}
	mov	r2, #0
	bl	__aeabi_dmul
	mov	r3, #1069547520
	mov	r2, #0
	add	r3, r3, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2iz
	mov	ip, #1073741824
	add	r3, ip, #16252928
	str	r0, [r4, #276]
	mov	r2, #0
	add	r1, sp, #72
	ldmia	r1, {r0-r1}
	add	r3, r3, #27136
	bl	__aeabi_dmul
	mov	ip, #1069547520
	add	r3, ip, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2iz
	mov	r3, #1073741824
	add	ip, r3, #16252928
	str	r0, [r4, #280]
	add	r3, ip, #27136
	add	r1, sp, #80
	ldmia	r1, {r0-r1}
	mov	r2, #0
	bl	__aeabi_dmul
	mov	r3, #1069547520
	add	r3, r3, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2iz
	ldr	ip, [r4, #8]
	orr	r3, ip, #4
	str	r0, [r4, #284]
	str	r3, [r4, #8]
.L3:
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, fp, lr}
	bx	lr
	.size	png_set_cHRM, .-png_set_cHRM
	.section	.text.png_set_oFFs,"ax",%progbits
	.align	2
	.global	png_set_oFFs
	.hidden	png_set_oFFs
	.type	png_set_oFFs, %function
png_set_oFFs:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	cmpne	r1, #0
	bxeq	lr
	ldr	ip, [r1, #8]
	str	r3, [r1, #104]
	ldr	r3, [sp, #0]
	orr	r0, ip, #256
	str	r0, [r1, #8]
	str	r2, [r1, #100]
	strb	r3, [r1, #108]
	bx	lr
	.size	png_set_oFFs, .-png_set_oFFs
	.section	.text.png_set_sCAL,"ax",%progbits
	.align	2
	.global	png_set_sCAL
	.hidden	png_set_sCAL
	.type	png_set_sCAL, %function
png_set_sCAL:
	@ Function supports interworking.
	@ args = 16, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	cmpne	r1, #0
	str	fp, [sp, #-4]!
	beq	.L10
	ldmib	sp, {fp-ip}
	ldr	r0, [r1, #8]
	str	fp, [r1, #224]
	str	ip, [r1, #228]
	add	ip, sp, #12
	ldmia	ip, {fp-ip}
	orr	r3, r0, #16384
	str	fp, [r1, #232]
	str	ip, [r1, #236]
	str	r3, [r1, #8]
	strb	r2, [r1, #220]
.L10:
	ldmfd	sp!, {fp}
	bx	lr
	.size	png_set_sCAL, .-png_set_sCAL
	.section	.text.png_set_pHYs,"ax",%progbits
	.align	2
	.global	png_set_pHYs
	.hidden	png_set_pHYs
	.type	png_set_pHYs, %function
png_set_pHYs:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	cmpne	r1, #0
	bxeq	lr
	ldr	ip, [r1, #8]
	str	r3, [r1, #116]
	ldr	r3, [sp, #0]
	orr	r0, ip, #128
	str	r0, [r1, #8]
	str	r2, [r1, #112]
	strb	r3, [r1, #120]
	bx	lr
	.size	png_set_pHYs, .-png_set_pHYs
	.section	.text.png_set_sRGB,"ax",%progbits
	.align	2
	.global	png_set_sRGB
	.hidden	png_set_sRGB
	.type	png_set_sRGB, %function
png_set_sRGB:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	cmpne	r1, #0
	ldrne	r3, [r1, #8]
	orrne	r3, r3, #2048
	strne	r3, [r1, #8]
	strneb	r2, [r1, #44]
	bx	lr
	.size	png_set_sRGB, .-png_set_sRGB
	.section	.text.png_set_unknown_chunk_location,"ax",%progbits
	.align	2
	.global	png_set_unknown_chunk_location
	.hidden	png_set_unknown_chunk_location
	.type	png_set_unknown_chunk_location, %function
png_set_unknown_chunk_location:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	bxeq	lr
	cmp	r2, #0
	bxlt	lr
	ldr	r0, [r1, #192]
	cmp	r2, r0
	ldrlt	r1, [r1, #188]
	addlt	r2, r2, r2, asl #2
	addlt	r2, r1, r2, asl #2
	strltb	r3, [r2, #16]
	bx	lr
	.size	png_set_unknown_chunk_location, .-png_set_unknown_chunk_location
	.section	.text.png_permit_empty_plte,"ax",%progbits
	.align	2
	.global	png_permit_empty_plte
	.hidden	png_permit_empty_plte
	.type	png_permit_empty_plte, %function
png_permit_empty_plte:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	ldrne	r3, [r0, #588]
	andne	r1, r1, #1
	andne	r3, r3, #254
	orrne	r1, r3, r1
	strne	r1, [r0, #588]
	bx	lr
	.size	png_permit_empty_plte, .-png_permit_empty_plte
	.section	.text.png_permit_mng_features,"ax",%progbits
	.align	2
	.global	png_permit_mng_features
	.hidden	png_permit_mng_features
	.type	png_permit_mng_features, %function
png_permit_mng_features:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	subs	r3, r0, #0
	andne	r0, r1, #5
	moveq	r0, r3
	strne	r0, [r3, #588]
	bx	lr
	.size	png_permit_mng_features, .-png_permit_mng_features
	.section	.text.png_set_read_user_chunk_fn,"ax",%progbits
	.align	2
	.global	png_set_read_user_chunk_fn
	.hidden	png_set_read_user_chunk_fn
	.type	png_set_read_user_chunk_fn, %function
png_set_read_user_chunk_fn:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	strne	r1, [r0, #564]
	strne	r2, [r0, #568]
	bx	lr
	.size	png_set_read_user_chunk_fn, .-png_set_read_user_chunk_fn
	.section	.text.png_set_invalid,"ax",%progbits
	.align	2
	.global	png_set_invalid
	.hidden	png_set_invalid
	.type	png_set_invalid, %function
png_set_invalid:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	ldrne	r3, [r1, #8]
	bicne	r2, r3, r2
	strne	r2, [r1, #8]
	bx	lr
	.size	png_set_invalid, .-png_set_invalid
	.section	.text.png_set_asm_flags,"ax",%progbits
	.align	2
	.global	png_set_asm_flags
	.hidden	png_set_asm_flags
	.type	png_set_asm_flags, %function
png_set_asm_flags:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	movne	r3, #0
	strne	r3, [r0, #604]
	bx	lr
	.size	png_set_asm_flags, .-png_set_asm_flags
	.section	.text.png_set_mmx_thresholds,"ax",%progbits
	.align	2
	.global	png_set_mmx_thresholds
	.hidden	png_set_mmx_thresholds
	.type	png_set_mmx_thresholds, %function
png_set_mmx_thresholds:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.size	png_set_mmx_thresholds, .-png_set_mmx_thresholds
	.section	.text.png_set_user_limits,"ax",%progbits
	.align	2
	.global	png_set_user_limits
	.hidden	png_set_user_limits
	.type	png_set_user_limits, %function
png_set_user_limits:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	strne	r2, [r0, #644]
	strne	r1, [r0, #640]
	bx	lr
	.size	png_set_user_limits, .-png_set_user_limits
	.section	.text.png_set_compression_buffer_size,"ax",%progbits
	.align	2
	.global	png_set_compression_buffer_size
	.hidden	png_set_compression_buffer_size
	.type	png_set_compression_buffer_size, %function
png_set_compression_buffer_size:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	subs	r4, r0, #0
	mov	r5, r1
	beq	.L43
	ldr	r1, [r4, #200]
	bl	png_free
	str	r5, [r4, #204]
	mov	r1, r5
	mov	r0, r4
	bl	png_malloc
	ldr	r3, [r4, #204]
	str	r0, [r4, #156]
	str	r3, [r4, #160]
	str	r0, [r4, #200]
.L43:
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
	.size	png_set_compression_buffer_size, .-png_set_compression_buffer_size
	.section	.text.png_set_rows,"ax",%progbits
	.align	2
	.global	png_set_rows
	.hidden	png_set_rows
	.type	png_set_rows, %function
png_set_rows:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	cmpne	r1, #0
	stmfd	sp!, {r3, r4, r5, lr}
	movne	r3, #0
	moveq	r3, #1
	mov	r5, r2
	mov	r4, r1
	beq	.L47
	ldr	r2, [r1, #248]
	cmp	r2, #0
	beq	.L46
	cmp	r2, r5
	movne	r2, #64
	blne	png_free_data
.L46:
	cmp	r5, #0
	ldrne	r3, [r4, #8]
	orrne	r3, r3, #32768
	str	r5, [r4, #248]
	strne	r3, [r4, #8]
.L47:
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
	.size	png_set_rows, .-png_set_rows
	.section	.text.png_set_keep_unknown_chunks,"ax",%progbits
	.align	2
	.global	png_set_keep_unknown_chunks
	.hidden	png_set_keep_unknown_chunks
	.type	png_set_keep_unknown_chunks, %function
png_set_keep_unknown_chunks:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, fp, lr}
	subs	r4, r0, #0
	mov	r5, r1
	mov	r6, r2
	mov	r7, r3
	beq	.L58
	cmp	r3, #0
	beq	.L73
	cmp	r2, #0
	beq	.L58
	ldr	fp, [r4, #572]
	add	sl, r3, fp
	add	r1, sl, sl, asl #2
	bl	png_malloc
	ldr	r1, [r4, #576]
	cmp	r1, #0
	mov	r9, r0
	add	fp, fp, fp, asl #2
	beq	.L55
	mov	r2, fp
	bl	memcpy
	mov	r8, #0
	mov	r0, r4
	ldr	r1, [r4, #576]
	bl	png_free
	str	r8, [r4, #576]
.L55:
	add	r8, r7, r7, asl #2
	mov	r1, r6
	add	r0, r9, fp
	mov	r2, r8
	bl	memcpy
	cmp	r7, #0
	ble	.L56
	add	lr, fp, #4
	mov	r3, #5
	add	fp, r9, lr
	and	r5, r5, #255
	sub	ip, r8, #5
	cmp	r3, r8
	and	r2, ip, #3
	strb	r5, [fp, #0]
	beq	.L56
	cmp	r2, #0
	beq	.L57
	cmp	r2, #1
	beq	.L71
	cmp	r2, #2
	movne	r3, #10
	strneb	r5, [fp, #5]
	strb	r5, [fp, r3]
	add	r3, r3, #5
.L71:
	strb	r5, [fp, r3]
	add	r3, r3, #5
	cmp	r3, r8
	beq	.L56
.L57:
	strb	r5, [fp, r3]
	add	r0, r3, #5
	add	r1, r3, #10
	add	r2, r3, #15
	add	r3, r3, #20
	cmp	r3, r8
	strb	r5, [fp, r0]
	strb	r5, [fp, r1]
	strb	r5, [fp, r2]
	bne	.L57
.L56:
	ldr	ip, [r4, #560]
	orr	r3, ip, #1024
	str	r3, [r4, #560]
	str	sl, [r4, #572]
	str	r9, [r4, #576]
.L58:
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L73:
	sub	r3, r1, #2
	cmp	r3, #1
	bls	.L74
	ldr	r0, [r4, #136]
	bic	r3, r0, #32768
	str	r3, [r4, #136]
.L53:
	bic	r1, r3, #65536
	str	r1, [r4, #136]
	b	.L58
.L74:
	ldr	r2, [r4, #136]
	cmp	r1, #3
	orr	r3, r2, #32768
	orreq	r2, r2, #98304
	str	r3, [r4, #136]
	streq	r2, [r4, #136]
	beq	.L58
	b	.L53
	.size	png_set_keep_unknown_chunks, .-png_set_keep_unknown_chunks
	.section	.text.png_set_tIME,"ax",%progbits
	.align	2
	.global	png_set_tIME
	.hidden	png_set_tIME
	.type	png_set_tIME, %function
png_set_tIME:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	cmpne	r1, #0
	stmfd	sp!, {r4, lr}
	mov	r4, r1
	beq	.L77
	ldr	r3, [r0, #132]
	tst	r3, #512
	beq	.L78
.L77:
	ldmfd	sp!, {r4, lr}
	bx	lr
.L78:
	mov	r1, r2
	add	r0, r4, #60
	mov	r2, #8
	bl	memcpy
	ldr	r1, [r4, #8]
	orr	r0, r1, #512
	str	r0, [r4, #8]
	b	.L77
	.size	png_set_tIME, .-png_set_tIME
	.section	.text.png_set_sBIT,"ax",%progbits
	.align	2
	.global	png_set_sBIT
	.hidden	png_set_sBIT
	.type	png_set_sBIT, %function
png_set_sBIT:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	cmpne	r1, #0
	stmfd	sp!, {r4, lr}
	mov	r4, r1
	beq	.L81
	mov	r1, r2
	add	r0, r4, #68
	mov	r2, #5
	bl	memcpy
	ldr	r0, [r4, #8]
	orr	r3, r0, #2
	str	r3, [r4, #8]
.L81:
	ldmfd	sp!, {r4, lr}
	bx	lr
	.size	png_set_sBIT, .-png_set_sBIT
	.section	.text.png_set_bKGD,"ax",%progbits
	.align	2
	.global	png_set_bKGD
	.hidden	png_set_bKGD
	.type	png_set_bKGD, %function
png_set_bKGD:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	cmpne	r1, #0
	stmfd	sp!, {r4, lr}
	mov	r4, r1
	beq	.L84
	mov	r1, r2
	add	r0, r4, #90
	mov	r2, #10
	bl	memcpy
	ldr	r0, [r4, #8]
	orr	r3, r0, #32
	str	r3, [r4, #8]
.L84:
	ldmfd	sp!, {r4, lr}
	bx	lr
	.size	png_set_bKGD, .-png_set_bKGD
	.section	.text.png_set_unknown_chunks,"ax",%progbits
	.align	2
	.global	png_set_unknown_chunks
	.hidden	png_set_unknown_chunks
	.type	png_set_unknown_chunks, %function
png_set_unknown_chunks:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	cmp	r0, #0
	cmpne	r1, #0
	sub	sp, sp, #12
	mov	r4, r1
	movne	r6, #0
	moveq	r6, #1
	mov	r5, r2
	mov	sl, r3
	mov	r8, r0
	beq	.L93
	cmp	r3, #0
	bne	.L116
.L93:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L116:
	ldr	r2, [r1, #192]
	add	r1, r3, r2
	add	r3, r1, r1, asl #2
	mov	r1, r3, asl #2
	bl	png_malloc_warn
	subs	r7, r0, #0
	beq	.L117
	ldr	lr, [r4, #192]
	add	ip, lr, lr, asl #2
	mov	r2, ip, asl #2
	ldr	r1, [r4, #188]
	bl	memcpy
	mov	r0, r8
	ldr	r1, [r4, #188]
	bl	png_free
	cmp	sl, #0
	str	r6, [r4, #188]
	ble	.L88
	ldr	r1, [r4, #192]
	ldr	r3, [r5, #12]
	add	r9, r1, r1, asl #2
	add	r9, r7, r9, asl #2
	ldmia	r5, {r0, r1}
	str	r3, [r9, #12]
	str	r0, [r9, #0]
	strb	r6, [r9, #4]
	ldr	r1, [r5, #12]
	ldr	r0, [r8, #132]
	cmp	r1, #0
	sub	fp, sl, #1
	mov	ip, r6
	strb	r0, [r9, #16]
	and	fp, fp, #1
	streq	r6, [r9, #8]
	bne	.L118
.L109:
	mov	r6, #1
	cmp	sl, r6
	add	r5, r5, #20
	ble	.L88
	cmp	fp, #0
	beq	.L92
	ldr	r0, [r4, #192]
	add	r1, r6, r0
	add	fp, r1, r1, asl #2
	ldr	r2, [r5, #12]
	add	fp, r7, fp, asl #2
	ldmia	r5, {r0, r1}
	str	r2, [fp, #12]
	str	r0, [fp, #0]
	strb	ip, [fp, #4]
	ldr	r1, [r5, #12]
	ldr	r3, [r8, #132]
	cmp	r1, #0
	strb	r3, [fp, #16]
	streq	r1, [fp, #8]
	bne	.L119
.L114:
	add	r6, r6, #1
	cmp	sl, r6
	add	r5, r5, #20
	ble	.L88
.L92:
	ldr	r3, [r4, #192]
	add	r0, r6, r3
	add	lr, r0, r0, asl #2
	ldr	r2, [r5, #12]
	add	r9, r7, lr, asl #2
	ldmia	r5, {r0, r1}
	str	r2, [r9, #12]
	str	r0, [r7, lr, asl #2]
	strb	ip, [r9, #4]
	ldr	r1, [r5, #12]
	ldr	r2, [r8, #132]
	cmp	r1, #0
	strb	r2, [r9, #16]
	streq	r1, [r9, #8]
	bne	.L120
.L90:
	ldr	r1, [r4, #192]
	add	r6, r6, #1
	add	r5, r5, #20
	add	r9, r6, r1
	add	r3, r9, r9, asl #2
	ldr	r2, [r5, #12]
	add	r9, r7, r3, asl #2
	ldmia	r5, {r0, r1}
	str	r2, [r9, #12]
	str	r0, [r7, r3, asl #2]
	strb	ip, [r9, #4]
	ldr	r1, [r5, #12]
	ldr	lr, [r8, #132]
	cmp	r1, #0
	strb	lr, [r9, #16]
	streq	r1, [r9, #8]
	beq	.L114
	mov	r0, r8
	str	ip, [sp, #4]
	bl	png_malloc_warn
	str	r0, [r9, #8]
	cmp	r0, #0
	mov	fp, r0
	ldr	ip, [sp, #4]
	beq	.L113
.L115:
	add	r1, r5, #8
	ldmia	r1, {r1, r2}	@ phole ldm
	bl	memcpy
	ldr	ip, [sp, #4]
	b	.L114
.L88:
	ldr	lr, [r4, #192]
	ldr	r0, [r4, #184]
	add	sl, lr, sl
	orr	ip, r0, #512
	str	ip, [r4, #184]
	str	r7, [r4, #188]
	str	sl, [r4, #192]
	b	.L93
.L120:
	mov	r0, r8
	str	ip, [sp, #4]
	bl	png_malloc_warn
	str	r0, [r9, #8]
	cmp	r0, #0
	mov	fp, r0
	ldr	ip, [sp, #4]
	beq	.L121
	add	r1, r5, #8
	ldmia	r1, {r1, r2}	@ phole ldm
	str	ip, [sp, #4]
	bl	memcpy
	ldr	ip, [sp, #4]
	b	.L90
.L118:
	mov	r0, r8
	str	r6, [sp, #4]
	bl	png_malloc_warn
	str	r0, [r9, #8]
	cmp	r0, #0
	ldr	ip, [sp, #4]
	beq	.L108
	add	r1, r5, #8
	ldmia	r1, {r1, r2}	@ phole ldm
	bl	memcpy
	ldr	ip, [sp, #4]
	b	.L109
.L119:
	mov	r0, r8
	str	ip, [sp, #4]
	bl	png_malloc_warn
	str	r0, [fp, #8]
	cmp	r0, #0
	mov	r9, r0
	ldr	ip, [sp, #4]
	bne	.L115
	mov	r0, r8
	ldr	r1, .L122
	str	ip, [sp, #4]
	bl	png_warning
	str	r9, [fp, #12]
	ldr	ip, [sp, #4]
	b	.L114
.L108:
	mov	r0, r8
	ldr	r1, .L122
	str	ip, [sp, #4]
	bl	png_warning
	ldr	ip, [sp, #4]
	str	ip, [r9, #12]
	b	.L109
.L121:
	mov	r0, r8
	ldr	r1, .L122
	bl	png_warning
	str	fp, [r9, #12]
	ldr	ip, [sp, #4]
	b	.L90
.L113:
	mov	r0, r8
	ldr	r1, .L122
	str	ip, [sp, #4]
	bl	png_warning
	str	fp, [r9, #12]
	ldr	ip, [sp, #4]
	b	.L114
.L117:
	mov	r0, r8
	ldr	r1, .L122
	bl	png_warning
	b	.L93
.L123:
	.align	2
.L122:
	.word	.LC0
	.size	png_set_unknown_chunks, .-png_set_unknown_chunks
	.section	.text.png_set_tRNS,"ax",%progbits
	.align	2
	.global	png_set_tRNS
	.hidden	png_set_tRNS
	.type	png_set_tRNS, %function
png_set_tRNS:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	cmp	r0, #0
	cmpne	r1, #0
	movne	ip, #0
	moveq	ip, #1
	mov	r7, r2
	mov	r5, r3
	ldr	r8, [sp, #24]
	mov	r4, r1
	mov	r6, r0
	beq	.L134
	cmp	r2, #0
	beq	.L126
	mov	r3, ip
	mov	r2, #8192
	bl	png_free_data
	mov	r0, r6
	mov	r1, #256
	bl	png_malloc
	sub	r3, r5, #1
	cmp	r3, #255
	str	r0, [r4, #76]
	str	r0, [r6, #420]
	bls	.L135
.L126:
	cmp	r8, #0
	beq	.L127
	ldrb	r3, [r4, #25]	@ zero_extendqisi2
	ldrb	r2, [r4, #24]	@ zero_extendqisi2
	mov	r1, #1
	cmp	r3, #0
	mov	r2, r1, asl r2
	bne	.L128
	ldrh	r0, [r8, #8]
	cmp	r2, r0
	blt	.L129
.L130:
	mov	r1, r8
	add	r0, r4, #80
	mov	r2, #10
	bl	memcpy
	cmp	r5, #0
	movne	r5, r5, asl #16
	movne	r5, r5, lsr #16
	moveq	r5, #1
.L132:
	strh	r5, [r4, #22]	@ movhi
.L133:
	ldr	ip, [r4, #8]
	ldr	r0, [r4, #184]
	orr	r1, ip, #16
	orr	r2, r0, #8192
	str	r2, [r4, #184]
	str	r1, [r4, #8]
.L134:
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L128:
	cmp	r3, #2
	bne	.L130
	ldrh	ip, [r8, #2]
	cmp	r2, ip
	blt	.L129
	ldrh	lr, [r8, #4]
	cmp	r2, lr
	bge	.L136
.L129:
	mov	r0, r6
	ldr	r1, .L137
	bl	png_warning
	b	.L130
.L135:
	mov	r1, r7
	ldr	r0, [r4, #76]
	mov	r2, r5
	bl	memcpy
	b	.L126
.L127:
	cmp	r5, #0
	strh	r5, [r4, #22]	@ movhi
	beq	.L134
	b	.L133
.L136:
	ldrh	r3, [r8, #6]
	cmp	r2, r3
	bge	.L130
	b	.L129
.L138:
	.align	2
.L137:
	.word	.LC1
	.size	png_set_tRNS, .-png_set_tRNS
	.section	.text.png_set_hIST,"ax",%progbits
	.align	2
	.global	png_set_hIST
	.hidden	png_set_hIST
	.type	png_set_hIST, %function
png_set_hIST:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	cmpne	r1, #0
	stmfd	sp!, {r3, r4, r5, r6, r7, lr}
	movne	r7, #0
	moveq	r7, #1
	mov	r6, r2
	mov	r4, r1
	mov	r5, r0
	beq	.L147
	ldrh	ip, [r1, #20]
	sub	r2, ip, #1
	mov	r3, r2, asl #16
	cmp	r3, #16711680
	bhi	.L148
	mov	r2, #8
	mov	r3, r7
	bl	png_free_data
	mov	r0, r5
	mov	r1, #512
	bl	png_malloc_warn
	cmp	r0, #0
	str	r0, [r5, #528]
	beq	.L142
	ldrh	r3, [r4, #20]
	cmp	r3, #0
	movne	r3, r7
	bne	.L146
	b	.L144
.L149:
	ldr	r0, [r5, #528]
.L146:
	ldrh	r1, [r6, r7]
	strh	r1, [r0, r7]	@ movhi
	ldrh	r0, [r4, #20]
	add	r3, r3, #1
	cmp	r0, r3
	add	r7, r7, #2
	bgt	.L149
	ldr	r0, [r5, #528]
.L144:
	ldr	r3, [r4, #8]
	ldr	ip, [r4, #184]
	orr	lr, r3, #64
	orr	r2, ip, #8
	str	r2, [r4, #184]
	str	r0, [r4, #124]
	str	lr, [r4, #8]
.L147:
	ldmfd	sp!, {r3, r4, r5, r6, r7, lr}
	bx	lr
.L148:
	ldr	r1, .L150
	bl	png_warning
	b	.L147
.L142:
	mov	r0, r5
	ldr	r1, .L150+4
	bl	png_warning
	b	.L147
.L151:
	.align	2
.L150:
	.word	.LC2
	.word	.LC3
	.size	png_set_hIST, .-png_set_hIST
	.global	__aeabi_i2d
	.global	__aeabi_ddiv
	.section	.text.png_set_gAMA_fixed,"ax",%progbits
	.align	2
	.global	png_set_gAMA_fixed
	.hidden	png_set_gAMA_fixed
	.type	png_set_gAMA_fixed, %function
png_set_gAMA_fixed:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	cmpne	r1, #0
	stmfd	sp!, {r3, r4, r5, r6, r7, lr}
	mov	r4, r1
	movne	r7, #0
	moveq	r7, #1
	mov	r5, r2
	mov	r6, r0
	beq	.L156
	cmp	r2, #0
	blt	.L157
	mov	r0, r2
	bl	__aeabi_i2d
	mov	r3, #1073741824
	add	ip, r3, #16252928
	add	r3, ip, #27136
	mov	r2, #0
	bl	__aeabi_ddiv
	bl	__aeabi_d2f
	ldr	r3, [r4, #8]
	cmp	r5, #0
	orr	ip, r3, #1
	str	ip, [r4, #8]
	str	r0, [r4, #40]	@ float
	str	r5, [r4, #252]
	beq	.L155
.L156:
	ldmfd	sp!, {r3, r4, r5, r6, r7, lr}
	bx	lr
.L157:
	ldr	r1, .L158
	bl	png_warning
	ldr	r1, [r4, #8]
	mov	r3, #0
	orr	r0, r1, #1
	str	r0, [r4, #8]
	str	r3, [r4, #40]	@ float
	str	r7, [r4, #252]
.L155:
	mov	r0, r6
	ldr	r1, .L158+4
	bl	png_warning
	b	.L156
.L159:
	.align	2
.L158:
	.word	.LC4
	.word	.LC5
	.size	png_set_gAMA_fixed, .-png_set_gAMA_fixed
	.global	__aeabi_dcmpgt
	.global	__aeabi_dcmpeq
	.section	.text.png_set_gAMA,"ax",%progbits
	.align	2
	.global	png_set_gAMA
	.hidden	png_set_gAMA
	.type	png_set_gAMA, %function
png_set_gAMA:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	cmpne	r1, #0
	stmfd	sp!, {r3, r4, r5, r6, r7, lr}
	mov	r4, r1
	mov	r7, r0
	mov	r5, r2
	mov	r6, r3
	beq	.L165
	mov	r0, r2
	mov	r1, r3
	adr	r3, .L170
	ldmia	r3, {r2-r3}
	bl	__aeabi_dcmpgt
	cmp	r0, #0
	bne	.L169
	mov	r1, r6
	mov	r0, r5
	bl	__aeabi_d2f
	mov	ip, #1073741824
	str	r0, [r4, #40]	@ float
	add	r0, ip, #16252928
	add	r3, r0, #27136
	mov	r2, #0
	mov	r0, r5
	mov	r1, r6
	bl	__aeabi_dmul
	mov	r3, #1069547520
	mov	r2, #0
	add	r3, r3, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2iz
	ldr	ip, [r4, #8]
	orr	r3, ip, #1
	str	r3, [r4, #8]
	str	r0, [r4, #252]
	mov	r1, r6
	mov	r0, r5
	mov	r2, #0
	mov	r3, #0
	bl	__aeabi_dcmpeq
	cmp	r0, #0
	movne	r0, r7
	ldrne	r1, .L170+8
	blne	png_warning
.L165:
	ldmfd	sp!, {r3, r4, r5, r6, r7, lr}
	bx	lr
.L169:
	mov	r0, r7
	ldr	r1, .L170+12
	bl	png_warning
	ldr	r1, [r4, #8]
	mov	ip, #-2147483648
	ldr	r2, .L170+16
	sub	r3, ip, #648
	orr	r0, r1, #1
	str	r0, [r4, #8]
	str	r2, [r4, #40]	@ float
	str	r3, [r4, #252]
	b	.L165
.L171:
	.align	3
.L170:
	.word	515396076
	.word	1087699125
	.word	.LC5
	.word	.LC6
	.word	1185400233
	.size	png_set_gAMA, .-png_set_gAMA
	.section	.text.png_set_sPLT,"ax",%progbits
	.align	2
	.global	png_set_sPLT
	.hidden	png_set_sPLT
	.type	png_set_sPLT, %function
png_set_sPLT:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	cmp	r0, #0
	cmpne	r1, #0
	sub	sp, sp, #12
	mov	r4, r1
	movne	r6, #0
	moveq	r6, #1
	mov	r5, r2
	str	r3, [sp, #4]
	mov	sl, r0
	bne	.L182
.L180:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L182:
	ldr	r2, [r1, #216]
	add	r1, r3, r2
	mov	r1, r1, asl #4
	bl	png_malloc_warn
	subs	r9, r0, #0
	beq	.L183
	add	r1, r4, #212
	ldmia	r1, {r1, r3}	@ phole ldm
	mov	r2, r3, asl #4
	bl	memcpy
	mov	r0, sl
	ldr	r1, [r4, #212]
	bl	png_free
	ldr	r0, [sp, #4]
	cmp	r0, #0
	str	r6, [r4, #212]
	bgt	.L179
	b	.L175
.L176:
	ldr	r1, [r5, #0]
	bl	memcpy
	ldr	r1, [r5, #12]
	add	fp, r1, r1, asl #2
	mov	r1, fp, asl #1
	mov	r0, sl
	bl	png_malloc_warn
	cmp	r0, #0
	mov	fp, r0
	str	r0, [r7, #8]
	beq	.L184
	ldr	r3, [r5, #12]
	add	lr, r3, r3, asl #2
	mov	r2, lr, asl #1
	ldr	r1, [r5, #8]
	bl	memcpy
	ldr	r0, [r5, #12]
	ldrb	r2, [r5, #4]	@ zero_extendqisi2
	str	r0, [r7, #12]
	strb	r2, [r7, #4]
.L177:
	ldr	ip, [sp, #4]
	add	r6, r6, #1
	cmp	ip, r6
	add	r5, r5, #16
	ble	.L175
.L179:
	ldr	r0, [r5, #0]
	bl	strlen
	add	r7, r0, #1
	mov	r1, r7
	mov	r0, sl
	ldr	r8, [r4, #216]
	bl	png_malloc_warn
	add	r8, r6, r8
	cmp	r0, #0
	mov	r2, r7
	str	r0, [r9, r8, asl #4]
	add	r7, r9, r8, asl #4
	bne	.L176
	mov	r0, sl
	ldr	r1, .L185
	bl	png_warning
	ldr	ip, [sp, #4]
	add	r6, r6, #1
	cmp	ip, r6
	add	r5, r5, #16
	bgt	.L179
.L175:
	ldr	r0, [sp, #4]
	ldr	ip, [r4, #216]
	ldr	r3, [r4, #8]
	ldr	lr, [r4, #184]
	add	r2, ip, r0
	orr	r1, lr, #32
	orr	r0, r3, #8192
	str	r1, [r4, #184]
	str	r9, [r4, #212]
	str	r2, [r4, #216]
	str	r0, [r4, #8]
	b	.L180
.L184:
	ldr	r1, .L185
	mov	r0, sl
	bl	png_warning
	mov	r0, sl
	ldr	r1, [r9, r8, asl #4]
	bl	png_free
	str	fp, [r9, r8, asl #4]
	b	.L177
.L183:
	mov	r0, sl
	ldr	r1, .L185+4
	bl	png_warning
	b	.L180
.L186:
	.align	2
.L185:
	.word	.LC8
	.word	.LC7
	.size	png_set_sPLT, .-png_set_sPLT
	.section	.text.png_set_text_2,"ax",%progbits
	.align	2
	.global	png_set_text_2
	.hidden	png_set_text_2
	.type	png_set_text_2, %function
png_set_text_2:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	cmp	r0, #0
	cmpne	r1, #0
	sub	sp, sp, #20
	mov	r4, r1
	mov	r5, r2
	str	r3, [sp, #8]
	mov	r9, r0
	beq	.L188
	cmp	r3, #0
	beq	.L188
	add	r3, r1, #48
	ldmia	r3, {r3, r7}	@ phole ldm
	ldr	r1, [sp, #8]
	add	r3, r1, r3
	cmp	r3, r7
	ble	.L189
	ldr	r6, [r4, #56]
	cmp	r6, #0
	beq	.L190
	add	ip, r3, #8
	str	ip, [r4, #52]
	mov	r1, ip, asl #4
	bl	png_malloc_warn
	cmp	r0, #0
	str	r0, [r4, #56]
	beq	.L240
	mov	r1, r6
	mov	r2, r7, asl #4
	bl	memcpy
	mov	r0, r9
	mov	r1, r6
	bl	png_free
.L189:
	ldr	r2, [sp, #8]
	cmp	r2, #0
	ble	.L188
	ldr	r0, [r5, #4]
	sub	fp, r2, #1
	cmp	r0, #0
	and	fp, fp, #1
	ldr	r6, [r4, #56]
	ldr	r8, [r4, #48]
	beq	.L205
	bl	strlen
	ldr	sl, [r5, #0]
	cmp	sl, #0
	mov	r7, r0
	ble	.L228
	mov	r0, r9
	ldr	r1, .L242
	bl	png_warning
.L205:
	ldr	r0, [sp, #8]
	mov	r7, #1
	cmp	r0, r7
	add	r5, r5, #16
	ble	.L188
	cmp	fp, #0
	beq	.L202
	ldr	r0, [r5, #4]
	cmp	r0, #0
	ldr	fp, [r4, #56]
	ldr	r6, [r4, #48]
	beq	.L211
	bl	strlen
	ldr	sl, [r5, #0]
	cmp	sl, #0
	mov	r8, r0
	ble	.L232
	mov	r0, r9
	ldr	r1, .L242
	bl	png_warning
.L211:
	ldr	r0, [sp, #8]
	add	r7, r7, #1
	cmp	r0, r7
	add	r5, r5, #16
	bgt	.L202
.L188:
	mov	r0, #0
.L192:
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L196:
	mov	r0, r9
	ldr	r1, .L242
	bl	png_warning
.L195:
	add	r6, r5, #16
	ldr	r0, [r6, #4]
	ldr	r1, [r4, #48]
	cmp	r0, #0
	ldr	fp, [r4, #56]
	add	r7, r7, #1
	str	r1, [sp, #4]
	beq	.L218
	bl	strlen
	ldr	r3, [r5, #16]
	cmp	r3, #0
	mov	r8, r0
	ble	.L236
	mov	r0, r9
	ldr	r1, .L242
	bl	png_warning
.L218:
	ldr	r5, [sp, #8]
	add	r7, r7, #1
	cmp	r5, r7
	add	r5, r6, #16
	ble	.L188
.L202:
	ldr	ip, [r4, #56]
	ldr	r0, [r5, #4]
	str	ip, [sp, #4]
	ldr	r2, [r4, #48]
	cmp	r0, #0
	str	r2, [sp, #12]
	beq	.L195
	bl	strlen
	ldr	fp, [r5, #0]
	cmp	fp, #0
	mov	r8, r0
	bgt	.L196
	ldr	r0, [r5, #8]
	ldr	sl, [sp, #4]
	ldr	lr, [sp, #12]
	cmp	r0, #0
	add	r6, sl, lr, asl #4
	beq	.L197
	ldrb	r3, [r0, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L197
	bl	strlen
	ldr	r3, [sp, #12]
	ldr	ip, [sp, #4]
	str	fp, [ip, r3, asl #4]
	mov	sl, r0
.L200:
	add	lr, r8, #4
	add	r1, lr, sl
	mov	r0, r9
	bl	png_malloc_warn
	cmp	r0, #0
	str	r0, [r6, #4]
	beq	.L194
	mov	r2, r8
	ldr	r1, [r5, #4]
	bl	memcpy
	ldr	r2, [r6, #4]
	mov	r0, #0
	strb	r0, [r2, r8]
	ldr	fp, [r6, #4]
	add	r8, r8, #1
	add	r0, r8, fp
	cmp	sl, #0
	str	r0, [r6, #8]
	bne	.L241
.L201:
	mov	r3, #0
	strb	r3, [r0, sl]
	str	sl, [r6, #12]
	ldr	ip, [r4, #48]
	add	r6, ip, #1
	str	r6, [r4, #48]
	b	.L195
.L236:
	ldr	r0, [r6, #8]
	ldr	sl, [sp, #4]
	cmp	r0, #0
	add	r5, fp, sl, asl #4
	beq	.L220
	ldrb	r2, [r0, #0]	@ zero_extendqisi2
	cmp	r2, #0
	beq	.L220
	str	r3, [sp, #0]
	bl	strlen
	ldmia	sp, {r3, ip}	@ phole ldm
	str	r3, [fp, ip, asl #4]
	mov	sl, r0
.L239:
	add	lr, r8, #4
	add	r1, lr, sl
	mov	r0, r9
	bl	png_malloc_warn
	cmp	r0, #0
	str	r0, [r5, #4]
	beq	.L194
	mov	r2, r8
	ldr	r1, [r6, #4]
	bl	memcpy
	ldr	ip, [r5, #4]
	mov	r2, #0
	strb	r2, [ip, r8]
	ldr	r3, [r5, #4]
	add	r8, r8, #1
	add	r0, r8, r3
	cmp	sl, #0
	str	r0, [r5, #8]
	bne	.L238
.L221:
	mov	r1, #0
	strb	r1, [r0, sl]
	str	sl, [r5, #12]
	ldr	r0, [r4, #48]
	add	r5, r0, #1
	str	r5, [r4, #48]
	b	.L218
.L197:
	ldr	r2, [sp, #12]
	ldr	r0, [sp, #4]
	mvn	r1, #0
	str	r1, [r0, r2, asl #4]
	mov	sl, #0
	b	.L200
.L220:
	ldr	lr, [sp, #4]
	mvn	sl, #0
	str	sl, [fp, lr, asl #4]
	mov	sl, #0
	b	.L239
.L241:
	ldr	r1, [r5, #8]
	mov	r2, sl
	bl	memcpy
	ldr	r0, [r6, #8]
	b	.L201
.L238:
	ldr	r1, [r6, #8]
	mov	r2, sl
	bl	memcpy
	ldr	r0, [r5, #8]
	b	.L221
.L228:
	ldr	r0, [r5, #8]
	cmp	r0, #0
	add	r6, r6, r8, asl #4
	beq	.L207
	ldrb	ip, [r0, #0]	@ zero_extendqisi2
	cmp	ip, #0
	beq	.L207
	bl	strlen
	str	sl, [r6, #0]
	mov	r8, r0
.L231:
	add	lr, r7, #4
	add	r1, lr, r8
	mov	r0, r9
	bl	png_malloc_warn
	cmp	r0, #0
	str	r0, [r6, #4]
	beq	.L194
	ldr	r1, [r5, #4]
	mov	r2, r7
	bl	memcpy
	ldr	r3, [r6, #4]
	mov	r1, #0
	strb	r1, [r3, r7]
	ldr	ip, [r6, #4]
	add	r0, r7, #1
	add	r0, r0, ip
	cmp	r8, #0
	str	r0, [r6, #8]
	bne	.L230
.L208:
	mov	lr, #0
	strb	lr, [r0, r8]
	str	r8, [r6, #12]
	ldr	r2, [r4, #48]
	add	r7, r2, #1
	str	r7, [r4, #48]
	b	.L205
.L232:
	ldr	r0, [r5, #8]
	cmp	r0, #0
	add	r6, fp, r6, asl #4
	beq	.L213
	ldrb	ip, [r0, #0]	@ zero_extendqisi2
	cmp	ip, #0
	beq	.L213
	bl	strlen
	str	sl, [r6, #0]
	mov	fp, r0
.L235:
	add	r3, r8, #4
	add	r1, r3, fp
	mov	r0, r9
	bl	png_malloc_warn
	cmp	r0, #0
	str	r0, [r6, #4]
	beq	.L194
	ldr	r1, [r5, #4]
	mov	r2, r8
	bl	memcpy
	ldr	r1, [r6, #4]
	mov	r0, #0
	strb	r0, [r1, r8]
	ldr	ip, [r6, #4]
	add	r2, r8, #1
	add	r0, r2, ip
	cmp	fp, #0
	str	r0, [r6, #8]
	bne	.L234
.L214:
	mov	r2, #0
	strb	r2, [r0, fp]
	str	fp, [r6, #12]
	ldr	r3, [r4, #48]
	add	lr, r3, #1
	str	lr, [r4, #48]
	ldr	r0, [sp, #8]
	add	r7, r7, #1
	cmp	r0, r7
	add	r5, r5, #16
	bgt	.L202
	b	.L188
.L194:
	mov	r0, #1
	b	.L192
.L207:
	mvn	r8, #0
	str	r8, [r6, #0]
	mov	r8, #0
	b	.L231
.L213:
	mvn	fp, #0
	str	fp, [r6, #0]
	mov	fp, #0
	b	.L235
.L230:
	ldr	r1, [r5, #8]
	mov	r2, r8
	bl	memcpy
	ldr	r0, [r6, #8]
	b	.L208
.L234:
	ldr	r1, [r5, #8]
	mov	r2, fp
	bl	memcpy
	ldr	r0, [r6, #8]
	b	.L214
.L190:
	ldr	r3, [sp, #8]
	add	lr, r3, #8
	str	r6, [r4, #48]
	str	lr, [r4, #52]
	mov	r1, lr, asl #4
	bl	png_malloc_warn
	cmp	r0, #0
	str	r0, [r4, #56]
	beq	.L194
	ldr	r1, [r4, #184]
	orr	r0, r1, #16384
	str	r0, [r4, #184]
	b	.L189
.L240:
	mov	r0, r9
	mov	r1, r6
	bl	png_free
	mov	r0, #1
	b	.L192
.L243:
	.align	2
.L242:
	.word	.LC9
	.size	png_set_text_2, .-png_set_text_2
	.section	.text.png_set_iCCP,"ax",%progbits
	.align	2
	.global	png_set_iCCP
	.hidden	png_set_iCCP
	.type	png_set_iCCP, %function
png_set_iCCP:
	@ Function supports interworking.
	@ args = 8, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	cmp	r0, #0
	cmpne	r1, #0
	sub	sp, sp, #12
	mov	r4, r1
	mov	r5, r0
	mov	r6, r2
	mov	r7, r3
	add	r8, sp, #48
	ldmia	r8, {r8, r9}	@ phole ldm
	beq	.L248
	cmp	r2, #0
	cmpne	r8, #0
	movne	sl, #0
	moveq	sl, #1
	bne	.L249
.L248:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L249:
	mov	r0, r2
	bl	strlen
	add	fp, r0, #1
	mov	r1, fp
	mov	r0, r5
	bl	png_malloc_warn
	subs	r2, r0, #0
	str	r2, [sp, #4]
	beq	.L250
	mov	r1, r6
	mov	r2, fp
	bl	memcpy
	mov	r0, r5
	mov	r1, r9
	bl	png_malloc_warn
	subs	r6, r0, #0
	beq	.L251
	mov	r1, r8
	mov	r2, r9
	bl	memcpy
	mov	r0, r5
	mov	r1, r4
	mov	r3, sl
	mov	r2, #16
	bl	png_free_data
	ldr	ip, [r4, #8]
	orr	r1, ip, #4096
	ldr	r2, [r4, #184]
	str	r1, [r4, #8]
	str	r9, [r4, #204]
	ldr	r0, [sp, #4]
	orr	r3, r2, #16
	str	r0, [r4, #196]
	str	r6, [r4, #200]
	strb	r7, [r4, #208]
	str	r3, [r4, #184]
	b	.L248
.L251:
	ldr	r1, [sp, #4]
	mov	r0, r5
	bl	png_free
	mov	r0, r5
	ldr	r1, .L252
	bl	png_warning
	b	.L248
.L250:
	mov	r0, r5
	ldr	r1, .L252+4
	bl	png_warning
	b	.L248
.L253:
	.align	2
.L252:
	.word	.LC11
	.word	.LC10
	.size	png_set_iCCP, .-png_set_iCCP
	.section	.text.png_set_pCAL,"ax",%progbits
	.align	2
	.global	png_set_pCAL
	.hidden	png_set_pCAL
	.type	png_set_pCAL, %function
png_set_pCAL:
	@ Function supports interworking.
	@ args = 20, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, fp, lr}
	cmp	r0, #0
	cmpne	r1, #0
	mov	r4, r1
	mov	r5, r0
	movne	sl, #0
	moveq	sl, #1
	mov	r6, r2
	mov	r7, r3
	ldr	fp, [sp, #52]
	ldr	r8, [sp, #56]
	bne	.L273
.L262:
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L273:
	mov	r0, r2
	bl	strlen
	add	r9, r0, #1
	mov	r1, r9
	mov	r0, r5
	bl	png_malloc_warn
	cmp	r0, #0
	str	r0, [r4, #160]
	beq	.L274
	mov	r1, r6
	mov	r2, r9
	bl	memcpy
	str	r7, [r4, #164]
	ldr	r6, [sp, #40]
	str	r6, [r4, #168]
	ldr	r2, [sp, #44]
	strb	r2, [r4, #180]
	ldr	r0, [sp, #48]
	strb	r0, [r4, #181]
	mov	r0, fp
	bl	strlen
	add	r6, r0, #1
	mov	r1, r6
	mov	r0, r5
	bl	png_malloc_warn
	cmp	r0, #0
	str	r0, [r4, #172]
	beq	.L275
	mov	r1, fp
	mov	r2, r6
	bl	memcpy
	ldr	r3, [sp, #48]
	add	ip, r3, #1
	mov	r1, ip, asl #2
	mov	r0, r5
	bl	png_malloc_warn
	cmp	r0, #0
	str	r0, [r4, #176]
	beq	.L276
	ldr	r3, [sp, #48]
	cmp	r3, #0
	str	sl, [r0, r3, asl #2]
	ble	.L259
	ldr	r0, [r8, #0]
	sub	r7, r3, #1
	bl	strlen
	add	r6, r0, #1
	mov	r1, r6
	mov	r0, r5
	ldr	sl, [r4, #176]
	bl	png_malloc_warn
	str	r0, [sl, #0]
	ldr	r1, [r4, #176]
	ldr	r0, [r1, #0]
	cmp	r0, #0
	and	r7, r7, #1
	beq	.L272
	mov	r2, r6
	ldr	r1, [r8, #0]
	bl	memcpy
	ldr	r6, [sp, #48]
	mov	r9, #1
	cmp	r6, r9
	mov	r6, #4
	ble	.L259
	cmp	r7, #0
	beq	.L261
	ldr	r0, [r8, r6]
	bl	strlen
	add	r7, r0, r9
	mov	r1, r7
	mov	r0, r5
	ldr	sl, [r4, #176]
	bl	png_malloc_warn
	add	sl, r6, sl
	str	r0, [sl, #0]
	ldr	r3, [r4, #176]
	ldr	r0, [r3, r6]
	cmp	r0, #0
	beq	.L272
	ldr	r1, [r8, r6]
	mov	r2, r7
	bl	memcpy
	ldr	r2, [sp, #48]
	mov	r9, #2
	cmp	r2, r9
	mov	r6, #8
	bgt	.L261
	b	.L259
.L260:
	ldr	r1, [r8, r6]
	bl	memcpy
	ldr	r0, [r8, r7]
	bl	strlen
	add	r6, r0, #1
	mov	r1, r6
	mov	r0, r5
	ldr	sl, [r4, #176]
	bl	png_malloc_warn
	str	r0, [r7, sl]
	ldr	r2, [r4, #176]
	ldr	r3, [r2, r7]
	add	r9, r9, #1
	subs	r0, r3, #0
	mov	r2, r6
	add	r9, r9, #1
	beq	.L272
	ldr	r1, [r8, r7]
	bl	memcpy
	ldr	r1, [sp, #48]
	cmp	r1, r9
	add	r6, r7, #4
	ble	.L259
.L261:
	ldr	r0, [r8, r6]
	bl	strlen
	add	fp, r0, #1
	mov	r1, fp
	mov	r0, r5
	ldr	sl, [r4, #176]
	bl	png_malloc_warn
	str	r0, [r6, sl]
	ldr	r0, [r4, #176]
	ldr	ip, [r0, r6]
	subs	r0, ip, #0
	add	r7, r6, #4
	mov	r2, fp
	bne	.L260
.L272:
	mov	r0, r5
	ldr	r1, .L277
	bl	png_warning
	b	.L262
.L259:
	ldr	r0, [r4, #8]
	ldr	ip, [r4, #184]
	orr	lr, r0, #1024
	orr	r1, ip, #128
	str	r1, [r4, #184]
	str	lr, [r4, #8]
	b	.L262
.L274:
	mov	r0, r5
	ldr	r1, .L277+4
	bl	png_warning
	b	.L262
.L275:
	mov	r0, r5
	ldr	r1, .L277+8
	bl	png_warning
	b	.L262
.L276:
	mov	r0, r5
	ldr	r1, .L277+12
	bl	png_warning
	b	.L262
.L278:
	.align	2
.L277:
	.word	.LC15
	.word	.LC12
	.word	.LC13
	.word	.LC14
	.size	png_set_pCAL, .-png_set_pCAL
	.section	.text.png_set_text,"ax",%progbits
	.align	2
	.global	png_set_text
	.hidden	png_set_text
	.type	png_set_text, %function
png_set_text:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	mov	r4, r0
	bl	png_set_text_2
	cmp	r0, #0
	movne	r0, r4
	ldrne	r1, .L282
	blne	png_error
.L281:
	ldmfd	sp!, {r4, lr}
	bx	lr
.L283:
	.align	2
.L282:
	.word	.LC16
	.size	png_set_text, .-png_set_text
	.section	.text.png_set_IHDR,"ax",%progbits
	.align	2
	.global	png_set_IHDR
	.hidden	png_set_IHDR
	.type	png_set_IHDR, %function
png_set_IHDR:
	@ Function supports interworking.
	@ args = 20, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, fp, lr}
	cmp	r0, #0
	cmpne	r1, #0
	mov	r4, r1
	mov	r5, r2
	mov	sl, r3
	ldr	r8, [sp, #40]
	ldr	r7, [sp, #44]
	ldr	fp, [sp, #52]
	ldr	r9, [sp, #56]
	mov	r6, r0
	beq	.L309
	cmp	r2, #0
	cmpne	r3, #0
	beq	.L310
	ldr	r3, [r6, #640]
	cmp	r5, r3
	bhi	.L287
.L318:
	ldr	r0, [r6, #644]
	cmp	sl, r0
	bls	.L288
.L287:
	mov	r0, r6
	ldr	r1, .L319
	bl	png_error
.L288:
	orrs	r2, sl, r5
	bmi	.L311
.L289:
	mov	ip, #536870912
	sub	r2, ip, #130
	cmp	r5, r2
	bhi	.L312
.L290:
	sub	r3, r8, #1
	cmp	r8, #4
	cmpne	r3, #1
	bls	.L291
	cmp	r8, #16
	cmpne	r8, #8
	bne	.L313
.L291:
	cmp	r7, #1
	movne	r0, #0
	moveq	r0, #1
	orrs	r3, r0, r7, lsr #31
	bne	.L292
	cmp	r7, #5
	movne	r2, #0
	moveq	r2, #1
	cmp	r7, #6
	orrgt	r2, r2, #1
	cmp	r2, #0
	beq	.L293
.L292:
	mov	r0, r6
	ldr	r1, .L319+4
	bl	png_error
.L293:
	cmp	r7, #3
	movne	ip, #0
	moveq	ip, #1
	cmp	r8, #8
	movle	ip, #0
	cmp	ip, #0
	bne	.L294
	cmp	r7, #2
	cmpne	r7, #4
	beq	.L295
	cmp	r7, #6
	beq	.L295
.L296:
	ldr	r3, [sp, #48]
	cmp	r3, #1
	movgt	r0, r6
	ldrgt	r1, .L319+8
	blgt	png_error
.L297:
	cmp	fp, #0
	bne	.L314
.L298:
	ldr	r0, [r6, #132]
	tst	r0, #4096
	beq	.L299
	ldr	r2, [r6, #588]
	cmp	r2, #0
	bne	.L315
.L299:
	cmp	r9, #0
	beq	.L300
	ldr	r0, [r6, #588]
	mov	r3, r0, lsr #2
	eor	ip, r3, #1
	cmp	r9, #64
	orrne	ip, ip, #1
	tst	ip, #1
	bne	.L301
	ldr	r2, [r6, #132]
	tst	r2, #4096
	beq	.L316
.L301:
	ldr	r1, .L319+12
	mov	r0, r6
	bl	png_error
	ldr	r1, [r6, #132]
	tst	r1, #4096
	bne	.L317
.L300:
	str	sl, [r4, #4]
	strb	fp, [r4, #26]
	strb	r9, [r4, #27]
	ldr	r2, [sp, #48]
	and	r3, r7, #255
	cmp	r3, #3
	and	r8, r8, #255
	strb	r2, [r4, #28]
	moveq	r2, #1
	str	r5, [r4, #0]
	strb	r8, [r4, #24]
	strb	r3, [r4, #25]
	streqb	r2, [r4, #29]
	beq	.L303
	tst	r7, #2
	movne	r2, #3
	moveq	r2, #1
	strneb	r2, [r4, #29]
	movne	r1, #4
	streqb	r2, [r4, #29]
	moveq	r1, #2
	tst	r3, #4
	strneb	r1, [r4, #29]
	ldrneb	r8, [r4, #24]	@ zero_extendqisi2
	movne	r2, r1
.L303:
	mul	r8, r2, r8
	mov	r3, #536870912
	sub	ip, r3, #130
	cmp	r5, ip
	and	r8, r8, #255
	movhi	ip, #0
	strb	r8, [r4, #30]
	strhi	ip, [r4, #12]
	bhi	.L309
	cmp	r8, #7
	mulls	r5, r8, r5
	movhi	r8, r8, lsr #3
	mulhi	r5, r8, r5
	addls	r5, r5, #7
	movls	r5, r5, lsr #3
	str	r5, [r4, #12]
.L309:
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L295:
	cmp	r8, #7
	bgt	.L296
.L294:
	mov	r0, r6
	ldr	r1, .L319+16
	bl	png_error
	b	.L296
.L310:
	ldr	r1, .L319+20
	bl	png_error
	ldr	r3, [r6, #640]
	cmp	r5, r3
	bls	.L318
	b	.L287
.L316:
	cmp	r7, #6
	cmpne	r7, #2
	beq	.L300
	b	.L301
.L317:
	mov	r0, r6
	ldr	r1, .L319+24
	bl	png_warning
	b	.L300
.L312:
	mov	r0, r6
	ldr	r1, .L319+28
	bl	png_warning
	b	.L290
.L311:
	mov	r0, r6
	ldr	r1, .L319+32
	bl	png_error
	b	.L289
.L314:
	mov	r0, r6
	ldr	r1, .L319+36
	bl	png_error
	b	.L298
.L313:
	mov	r0, r6
	ldr	r1, .L319+40
	bl	png_error
	b	.L291
.L315:
	mov	r0, r6
	ldr	r1, .L319+44
	bl	png_warning
	b	.L299
.L320:
	.align	2
.L319:
	.word	.LC18
	.word	.LC22
	.word	.LC24
	.word	.LC27
	.word	.LC23
	.word	.LC17
	.word	.LC28
	.word	.LC20
	.word	.LC19
	.word	.LC25
	.word	.LC21
	.word	.LC26
	.size	png_set_IHDR, .-png_set_IHDR
	.section	.text.png_set_cHRM_fixed,"ax",%progbits
	.align	2
	.global	png_set_cHRM_fixed
	.hidden	png_set_cHRM_fixed
	.type	png_set_cHRM_fixed, %function
png_set_cHRM_fixed:
	@ Function supports interworking.
	@ args = 24, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	cmp	r0, #0
	cmpne	r1, #0
	sub	sp, sp, #28
	mov	r4, r1
	mov	r7, r2
	mov	r6, r3
	add	r5, sp, #64
	ldmia	r5, {r5, r8, sl}	@ phole ldm
	add	r9, sp, #76
	ldmia	r9, {r9, fp}	@ phole ldm
	bne	.L324
.L323:
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L324:
	ldr	ip, [sp, #84]
	mov	r1, r2
	mov	r2, r3
	mov	r3, r5
	stmia	sp, {r8, sl}	@ phole stm
	str	r9, [sp, #8]
	str	fp, [sp, #12]
	str	ip, [sp, #16]
	bl	png_check_cHRM_fixed
	cmp	r0, #0
	beq	.L323
	str	r7, [r4, #256]
	str	r6, [r4, #260]
	str	r5, [r4, #264]
	str	r8, [r4, #268]
	str	sl, [r4, #272]
	str	r9, [r4, #276]
	str	fp, [r4, #280]
	ldr	r0, [sp, #84]
	str	r0, [r4, #284]
	mov	r0, r7
	bl	__aeabi_i2d
	mov	ip, #1073741824
	add	r3, ip, #16252928
	mov	r2, #0
	add	r3, r3, #27136
	bl	__aeabi_ddiv
	bl	__aeabi_d2f
	str	r0, [r4, #128]	@ float
	mov	r0, r6
	bl	__aeabi_i2d
	mov	ip, #1073741824
	add	r3, ip, #16252928
	mov	r2, #0
	add	r3, r3, #27136
	bl	__aeabi_ddiv
	bl	__aeabi_d2f
	str	r0, [r4, #132]	@ float
	mov	r0, r5
	bl	__aeabi_i2d
	mov	ip, #1073741824
	add	r3, ip, #16252928
	mov	r2, #0
	add	r3, r3, #27136
	bl	__aeabi_ddiv
	bl	__aeabi_d2f
	str	r0, [r4, #136]	@ float
	mov	r0, r8
	bl	__aeabi_i2d
	mov	ip, #1073741824
	add	r3, ip, #16252928
	mov	r2, #0
	add	r3, r3, #27136
	bl	__aeabi_ddiv
	bl	__aeabi_d2f
	str	r0, [r4, #140]	@ float
	mov	r0, sl
	bl	__aeabi_i2d
	mov	ip, #1073741824
	add	r3, ip, #16252928
	mov	r2, #0
	add	r3, r3, #27136
	bl	__aeabi_ddiv
	bl	__aeabi_d2f
	str	r0, [r4, #144]	@ float
	mov	r0, r9
	bl	__aeabi_i2d
	mov	ip, #1073741824
	add	r3, ip, #16252928
	mov	r2, #0
	add	r3, r3, #27136
	bl	__aeabi_ddiv
	bl	__aeabi_d2f
	str	r0, [r4, #148]	@ float
	mov	r0, fp
	bl	__aeabi_i2d
	mov	ip, #1073741824
	add	r3, ip, #16252928
	mov	r2, #0
	add	r3, r3, #27136
	bl	__aeabi_ddiv
	bl	__aeabi_d2f
	str	r0, [r4, #152]	@ float
	ldr	r0, [sp, #84]
	bl	__aeabi_i2d
	mov	ip, #1073741824
	add	r3, ip, #16252928
	add	r3, r3, #27136
	mov	r2, #0
	bl	__aeabi_ddiv
	bl	__aeabi_d2f
	ldr	ip, [r4, #8]
	orr	r3, ip, #4
	str	r3, [r4, #8]
	str	r0, [r4, #156]	@ float
	b	.L323
	.size	png_set_cHRM_fixed, .-png_set_cHRM_fixed
	.section	.text.png_set_PLTE,"ax",%progbits
	.align	2
	.global	png_set_PLTE
	.hidden	png_set_PLTE
	.type	png_set_PLTE, %function
png_set_PLTE:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	cmpne	r1, #0
	stmfd	sp!, {r3, r4, r5, r6, r7, lr}
	mov	r4, r1
	mov	r7, r2
	mov	r6, r3
	mov	r5, r0
	beq	.L329
	cmp	r3, #256
	bls	.L327
	ldrb	r3, [r1, #25]	@ zero_extendqisi2
	cmp	r3, #3
	ldr	r1, .L331
	beq	.L330
	bl	png_warning
.L329:
	ldmfd	sp!, {r3, r4, r5, r6, r7, lr}
	bx	lr
.L330:
	bl	png_error
.L327:
	mov	r3, #0
	mov	r2, #4096
	mov	r1, r4
	mov	r0, r5
	bl	png_free_data
	mov	r1, #768
	mov	r0, r5
	bl	png_malloc
	mov	r1, #0
	mov	r2, #768
	str	r0, [r5, #304]
	bl	memset
	add	r2, r6, r6, asl #1
	mov	r1, r7
	ldr	r0, [r5, #304]
	bl	memcpy
	mov	r6, r6, asl #16
	ldr	r0, [r5, #304]
	mov	r6, r6, lsr #16
	mov	r3, #308
	str	r0, [r4, #16]
	strh	r6, [r5, r3]	@ movhi
	ldr	ip, [r4, #184]
	ldr	r1, [r4, #8]
	orr	r0, ip, #4096
	orr	r2, r1, #8
	str	r2, [r4, #8]
	strh	r6, [r4, #20]	@ movhi
	str	r0, [r4, #184]
	b	.L329
.L332:
	.align	2
.L331:
	.word	.LC29
	.size	png_set_PLTE, .-png_set_PLTE
	.section	.text.png_set_sRGB_gAMA_and_cHRM,"ax",%progbits
	.align	2
	.global	png_set_sRGB_gAMA_and_cHRM
	.hidden	png_set_sRGB_gAMA_and_cHRM
	.type	png_set_sRGB_gAMA_and_cHRM, %function
png_set_sRGB_gAMA_and_cHRM:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	cmp	r0, #0
	cmpne	r1, #0
	sub	sp, sp, #36
	mov	r4, r1
	beq	.L336
	ldr	r9, [r1, #8]
	strb	r2, [r4, #44]
	orr	fp, r9, #2048
	mov	lr, #45568
	ldr	r2, .L340
	mov	r7, #31232
	mov	r6, #32768
	orr	r1, fp, #1
	sub	r3, lr, #113
	add	r7, r7, #38
	add	r6, r6, #132
	mov	r5, #32768
	mov	r8, #29952
	mov	sl, #59904
	mov	r9, #14976
	mov	fp, #5952
	str	r2, [r4, #40]	@ float
	str	r3, [r4, #252]
	str	r1, [r4, #8]
	add	r5, r5, #232
	add	r8, r8, #48
	add	sl, sl, #96
	add	r9, r9, #24
	add	fp, fp, #48
	mov	r1, r7
	mov	r2, r6
	mov	r3, #64000
	stmia	sp, {r5, r8, sl}	@ phole stm
	str	r9, [sp, #12]
	str	fp, [sp, #16]
	str	r0, [sp, #28]
	bl	png_check_cHRM_fixed
	cmp	r0, #0
	ldr	ip, [sp, #28]
	bne	.L339
.L336:
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L339:
	mov	r0, ip
	mov	r1, r7
	mov	r2, r6
	mov	r3, #64000
	stmia	sp, {r5, r8, sl}	@ phole stm
	str	r9, [sp, #12]
	str	fp, [sp, #16]
	bl	png_check_cHRM_fixed
	cmp	r0, #0
	ldr	r0, [r4, #8]
	beq	.L335
	ldr	ip, .L340+4
	str	r5, [r4, #268]
	ldr	r5, .L340+8
	mov	r1, #64000
	mvn	lr, #14417920
	str	r7, [r4, #256]
	str	r1, [r4, #264]
	str	r5, [r4, #128]	@ float
	str	ip, [r4, #132]	@ float
	sub	r7, lr, #10432
	ldr	r2, .L340+12
	ldr	r3, .L340+16
	ldr	r1, .L340+20
	ldr	r5, .L340+24
	ldr	ip, .L340+28
	orr	r0, r0, #4
	str	r6, [r4, #260]
	sub	r6, r7, #-1073741771
	str	r8, [r4, #272]
	str	sl, [r4, #276]
	str	r9, [r4, #280]
	str	fp, [r4, #284]
	str	r6, [r4, #136]	@ float
	str	r2, [r4, #140]	@ float
	str	r3, [r4, #144]	@ float
	str	r1, [r4, #148]	@ float
	str	r5, [r4, #152]	@ float
	str	ip, [r4, #156]	@ float
	str	r0, [r4, #8]
.L335:
	orr	r0, r0, #4
	str	r0, [r4, #8]
	ldr	r0, .L340+8
	str	r0, [r4, #128]	@ float
	ldr	r0, .L340+4
	str	r0, [r4, #132]	@ float
	mvn	r0, #14417920
	sub	r0, r0, #10432
	sub	r0, r0, #-1073741771
	str	r0, [r4, #136]	@ float
	ldr	r0, .L340+12
	str	r0, [r4, #140]	@ float
	ldr	r0, .L340+16
	str	r0, [r4, #144]	@ float
	ldr	r0, .L340+20
	str	r0, [r4, #148]	@ float
	ldr	r0, .L340+24
	mov	r6, #32768
	str	r0, [r4, #152]	@ float
	ldr	r0, .L340+28
	mov	r5, r6
	mov	lr, #29952
	mov	r7, #31232
	mov	r1, #59904
	mov	r2, #14976
	mov	r3, #5952
	add	ip, lr, #48
	add	r7, r7, #38
	add	r6, r6, #132
	add	r5, r5, #232
	add	lr, r1, #96
	add	r2, r2, #24
	add	r3, r3, #48
	str	r0, [r4, #156]	@ float
	mov	r0, #64000
	str	r7, [r4, #256]
	str	r6, [r4, #260]
	str	r0, [r4, #264]
	str	r5, [r4, #268]
	str	ip, [r4, #272]
	str	lr, [r4, #276]
	str	r2, [r4, #280]
	str	r3, [r4, #284]
	b	.L336
.L341:
	.align	2
.L340:
	.word	1055439559
	.word	1051226800
	.word	1050679863
	.word	1051260355
	.word	1050253722
	.word	1058642330
	.word	1041865114
	.word	1031127695
	.size	png_set_sRGB_gAMA_and_cHRM, .-png_set_sRGB_gAMA_and_cHRM
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"Out of memory while processing unknown chunk.\000"
	.space	2
.LC1:
	.ascii	"tRNS chunk has out-of-range samples for bit_depth\000"
	.space	2
.LC2:
	.ascii	"Invalid palette size, hIST allocation skipped.\000"
	.space	1
.LC3:
	.ascii	"Insufficient memory for hIST chunk data.\000"
	.space	3
.LC4:
	.ascii	"Setting negative gamma to zero\000"
	.space	1
.LC5:
	.ascii	"Setting gamma=0\000"
.LC6:
	.ascii	"Limiting gamma to 21474.83\000"
	.space	1
.LC7:
	.ascii	"No memory for sPLT palettes.\000"
	.space	3
.LC8:
	.ascii	"Out of memory while processing sPLT chunk\000"
	.space	2
.LC9:
	.ascii	"iTXt chunk not supported.\000"
	.space	2
.LC10:
	.ascii	"Insufficient memory to process iCCP chunk.\000"
	.space	1
.LC11:
	.ascii	"Insufficient memory to process iCCP profile.\000"
	.space	3
.LC12:
	.ascii	"Insufficient memory for pCAL purpose.\000"
	.space	2
.LC13:
	.ascii	"Insufficient memory for pCAL units.\000"
.LC14:
	.ascii	"Insufficient memory for pCAL params.\000"
	.space	3
.LC15:
	.ascii	"Insufficient memory for pCAL parameter.\000"
.LC16:
	.ascii	"Insufficient memory to store text\000"
	.space	2
.LC17:
	.ascii	"Image width or height is zero in IHDR\000"
	.space	2
.LC18:
	.ascii	"image size exceeds user limits in IHDR\000"
	.space	1
.LC19:
	.ascii	"Invalid image size in IHDR\000"
	.space	1
.LC20:
	.ascii	"Width is too large for libpng to process pixels\000"
.LC21:
	.ascii	"Invalid bit depth in IHDR\000"
	.space	2
.LC22:
	.ascii	"Invalid color type in IHDR\000"
	.space	1
.LC23:
	.ascii	"Invalid color type/bit depth combination in IHDR\000"
	.space	3
.LC24:
	.ascii	"Unknown interlace method in IHDR\000"
	.space	3
.LC25:
	.ascii	"Unknown compression method in IHDR\000"
	.space	1
.LC26:
	.ascii	"MNG features are not allowed in a PNG datastream\000"
	.space	3
.LC27:
	.ascii	"Unknown filter method in IHDR\000"
	.space	2
.LC28:
	.ascii	"Invalid filter method in IHDR\000"
	.space	2
.LC29:
	.ascii	"Invalid palette length\000"
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
