	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"testApp.cpp"
	.section	.text._ZN7QuTimerD1Ev,"axG",%progbits,_ZN7QuTimerD1Ev,comdat
	.align	2
	.weak	_ZN7QuTimerD1Ev
	.hidden	_ZN7QuTimerD1Ev
	.type	_ZN7QuTimerD1Ev, %function
_ZN7QuTimerD1Ev:
	.fnstart
.LFB2708:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L3
	str	r3, [r0, #0]
	bx	lr
.L4:
	.align	2
.L3:
	.word	_ZTV7QuTimer+8
	.cantunwind
	.fnend
	.size	_ZN7QuTimerD1Ev, .-_ZN7QuTimerD1Ev
	.section	.text._ZN7QuTimer6updateEv,"axG",%progbits,_ZN7QuTimer6updateEv,comdat
	.align	2
	.weak	_ZN7QuTimer6updateEv
	.hidden	_ZN7QuTimer6updateEv
	.type	_ZN7QuTimer6updateEv, %function
_ZN7QuTimer6updateEv:
	.fnstart
.LFB2710:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r1, [r0, #8]
	add	r3, r1, #1
	str	r3, [r0, #8]
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN7QuTimer6updateEv, .-_ZN7QuTimer6updateEv
	.section	.text._ZN7QuTimer5resetEv,"axG",%progbits,_ZN7QuTimer5resetEv,comdat
	.align	2
	.weak	_ZN7QuTimer5resetEv
	.hidden	_ZN7QuTimer5resetEv
	.type	_ZN7QuTimer5resetEv, %function
_ZN7QuTimer5resetEv:
	.fnstart
.LFB2711:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, [r0, #4]
	str	r3, [r0, #8]
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN7QuTimer5resetEv, .-_ZN7QuTimer5resetEv
	.section	.text._ZN13QuBasicCameraD1Ev,"axG",%progbits,_ZN13QuBasicCameraD1Ev,comdat
	.align	2
	.weak	_ZN13QuBasicCameraD1Ev
	.hidden	_ZN13QuBasicCameraD1Ev
	.type	_ZN13QuBasicCameraD1Ev, %function
_ZN13QuBasicCameraD1Ev:
	.fnstart
.LFB3711:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L11
	str	r3, [r0, #0]
	bx	lr
.L12:
	.align	2
.L11:
	.word	_ZTV13QuBasicCamera+8
	.cantunwind
	.fnend
	.size	_ZN13QuBasicCameraD1Ev, .-_ZN13QuBasicCameraD1Ev
	.section	.text._ZN9QuContext7disableEv,"axG",%progbits,_ZN9QuContext7disableEv,comdat
	.align	2
	.weak	_ZN9QuContext7disableEv
	.hidden	_ZN9QuContext7disableEv
	.type	_ZN9QuContext7disableEv, %function
_ZN9QuContext7disableEv:
	.fnstart
.LFB3722:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN9QuContext7disableEv, .-_ZN9QuContext7disableEv
	.section	.text._ZN7testApp10initializeEv,"ax",%progbits
	.align	2
	.global	_ZN7testApp10initializeEv
	.hidden	_ZN7testApp10initializeEv
	.type	_ZN7testApp10initializeEv, %function
_ZN7testApp10initializeEv:
	.fnstart
.LFB3870:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN7testApp10initializeEv, .-_ZN7testApp10initializeEv
	.section	.text._ZN7testApp5setupEv,"ax",%progbits
	.align	2
	.global	_ZN7testApp5setupEv
	.hidden	_ZN7testApp5setupEv
	.type	_ZN7testApp5setupEv, %function
_ZN7testApp5setupEv:
	.fnstart
.LFB3871:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN7testApp5setupEv, .-_ZN7testApp5setupEv
	.section	.text._ZN7testApp11restartGameEv,"ax",%progbits
	.align	2
	.global	_ZN7testApp11restartGameEv
	.hidden	_ZN7testApp11restartGameEv
	.type	_ZN7testApp11restartGameEv, %function
_ZN7testApp11restartGameEv:
	.fnstart
.LFB3872:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, #1
	strb	r3, [r0, #0]
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN7testApp11restartGameEv, .-_ZN7testApp11restartGameEv
	.section	.text._ZN7testApp6updateEv,"ax",%progbits
	.align	2
	.global	_ZN7testApp6updateEv
	.hidden	_ZN7testApp6updateEv
	.type	_ZN7testApp6updateEv, %function
_ZN7testApp6updateEv:
	.fnstart
.LFB3874:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, [r0, #4]
	cmp	r3, #1
	moveq	r3, #2
	streq	r3, [r0, #4]
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN7testApp6updateEv, .-_ZN7testApp6updateEv
	.section	.text._ZN7testApp10keyPressedEi,"ax",%progbits
	.align	2
	.global	_ZN7testApp10keyPressedEi
	.hidden	_ZN7testApp10keyPressedEi
	.type	_ZN7testApp10keyPressedEi, %function
_ZN7testApp10keyPressedEi:
	.fnstart
.LFB3876:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN7testApp10keyPressedEi, .-_ZN7testApp10keyPressedEi
	.section	.text._ZN7testApp11keyReleasedEi,"ax",%progbits
	.align	2
	.global	_ZN7testApp11keyReleasedEi
	.hidden	_ZN7testApp11keyReleasedEi
	.type	_ZN7testApp11keyReleasedEi, %function
_ZN7testApp11keyReleasedEi:
	.fnstart
.LFB3877:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #210
	moveq	r3, #1
	streqb	r3, [r0, #0]
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN7testApp11keyReleasedEi, .-_ZN7testApp11keyReleasedEi
	.section	.text._ZN7testApp13windowResizedEii,"ax",%progbits
	.align	2
	.global	_ZN7testApp13windowResizedEii
	.hidden	_ZN7testApp13windowResizedEii
	.type	_ZN7testApp13windowResizedEii, %function
_ZN7testApp13windowResizedEii:
	.fnstart
.LFB3881:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN7testApp13windowResizedEii, .-_ZN7testApp13windowResizedEii
	.section	.text._GLOBAL__I__ZN7testApp10initializeEv,"ax",%progbits
	.align	2
	.type	_GLOBAL__I__ZN7testApp10initializeEv, %function
_GLOBAL__I__ZN7testApp10initializeEv:
	.fnstart
.LFB5513:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	ldr	r4, .L33
	ldr	r5, .L33+4
	mov	r0, r4
	bl	_ZN4_STL8ios_base9_Loc_initC1Ev
	add	r6, r4, #4
	mov	r2, r5
	ldr	r1, .L33+8
	mov	r0, r4
	bl	__aeabi_atexit
	mov	r0, r6
	bl	_ZN4_STL8ios_base4InitC1Ev
	mov	r0, r6
	mov	r2, r5
	ldr	r1, .L33+12
	bl	__aeabi_atexit
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L34:
	.align	2
.L33:
	.word	.LANCHOR0
	.word	__dso_handle
	.word	_ZN4_STL8ios_base9_Loc_initD1Ev
	.word	_ZN4_STL8ios_base4InitD1Ev
	.fnend
	.size	_GLOBAL__I__ZN7testApp10initializeEv, .-_GLOBAL__I__ZN7testApp10initializeEv
	.section	.init_array,"aw",%init_array
	.align	2
	.word	_GLOBAL__I__ZN7testApp10initializeEv(target1)
	.section	.text._ZN7testApp10tiltUpdateEv,"ax",%progbits
	.align	2
	.global	_ZN7testApp10tiltUpdateEv
	.hidden	_ZN7testApp10tiltUpdateEv
	.type	_ZN7testApp10tiltUpdateEv, %function
_ZN7testApp10tiltUpdateEv:
	.fnstart
.LFB3882:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, lr}
	.save {r3, lr}
	bl	s3eAccelerometerGetY
	bl	s3eAccelerometerGetX
	mov	r0, #11
	bl	s3eSurfaceGetInt
	ldmfd	sp!, {r3, lr}
	bx	lr
	.fnend
	.size	_ZN7testApp10tiltUpdateEv, .-_ZN7testApp10tiltUpdateEv
	.section	.text._ZN21QuGuiRenderingContext7disableEv,"axG",%progbits,_ZN21QuGuiRenderingContext7disableEv,comdat
	.align	2
	.weak	_ZN21QuGuiRenderingContext7disableEv
	.hidden	_ZN21QuGuiRenderingContext7disableEv
	.type	_ZN21QuGuiRenderingContext7disableEv, %function
_ZN21QuGuiRenderingContext7disableEv:
	.fnstart
.LFB3736:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, lr}
	.save {r3, lr}
	mov	r0, #2896
	bl	glEnable
	mov	r1, #2928
	add	r0, r1, #1
	bl	glEnable
	mov	r0, #1
	bl	glDepthMask
	mov	r0, #3040
	add	r0, r0, #2
	bl	glDisable
	ldmfd	sp!, {r3, lr}
	bx	lr
	.fnend
	.size	_ZN21QuGuiRenderingContext7disableEv, .-_ZN21QuGuiRenderingContext7disableEv
	.section	.text._ZN13QuBasicCameraD0Ev,"axG",%progbits,_ZN13QuBasicCameraD0Ev,comdat
	.align	2
	.weak	_ZN13QuBasicCameraD0Ev
	.hidden	_ZN13QuBasicCameraD0Ev
	.type	_ZN13QuBasicCameraD0Ev, %function
_ZN13QuBasicCameraD0Ev:
	.fnstart
.LFB3712:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L41
	stmfd	sp!, {r4, lr}
	mov	r4, r0
	str	r3, [r0, #0]
	bl	_ZdlPv
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L42:
	.align	2
.L41:
	.word	_ZTV13QuBasicCamera+8
	.cantunwind
	.fnend
	.size	_ZN13QuBasicCameraD0Ev, .-_ZN13QuBasicCameraD0Ev
	.section	.text._ZN7QuTimerD0Ev,"axG",%progbits,_ZN7QuTimerD0Ev,comdat
	.align	2
	.weak	_ZN7QuTimerD0Ev
	.hidden	_ZN7QuTimerD0Ev
	.type	_ZN7QuTimerD0Ev, %function
_ZN7QuTimerD0Ev:
	.fnstart
.LFB2709:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L45
	stmfd	sp!, {r4, lr}
	mov	r4, r0
	str	r3, [r0, #0]
	bl	_ZdlPv
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L46:
	.align	2
.L45:
	.word	_ZTV7QuTimer+8
	.cantunwind
	.fnend
	.size	_ZN7QuTimerD0Ev, .-_ZN7QuTimerD0Ev
	.section	.text._ZN14QuArrayPointerIfE6deInitEv,"axG",%progbits,_ZN14QuArrayPointerIfE6deInitEv,comdat
	.align	2
	.weak	_ZN14QuArrayPointerIfE6deInitEv
	.hidden	_ZN14QuArrayPointerIfE6deInitEv
	.type	_ZN14QuArrayPointerIfE6deInitEv, %function
_ZN14QuArrayPointerIfE6deInitEv:
	.fnstart
.LFB4031:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	ldrb	r3, [r0, #0]	@ zero_extendqisi2
	cmp	r3, #0
	mov	r4, r0
	beq	.L50
	ldr	r0, [r0, #8]
	cmp	r0, #0
	blne	_ZdaPv
.L49:
	mov	r0, #0
	strb	r0, [r4, #0]
	str	r0, [r4, #8]
.L50:
	ldmfd	sp!, {r4, lr}
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN14QuArrayPointerIfE6deInitEv, .-_ZN14QuArrayPointerIfE6deInitEv
	.global	__aeabi_fmul
	.global	__aeabi_fadd
	.global	__aeabi_f2d
	.global	__aeabi_d2f
	.global	__aeabi_fcmpeq
	.global	__aeabi_fdiv
	.global	__aeabi_fsub
	.global	__aeabi_i2f
	.section	.text._ZN13QuBasicCamera18setModelViewMatrixEv,"axG",%progbits,_ZN13QuBasicCamera18setModelViewMatrixEv,comdat
	.align	2
	.weak	_ZN13QuBasicCamera18setModelViewMatrixEv
	.hidden	_ZN13QuBasicCamera18setModelViewMatrixEv
	.type	_ZN13QuBasicCamera18setModelViewMatrixEv, %function
_ZN13QuBasicCamera18setModelViewMatrixEv:
	.fnstart
.LFB3719:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 88
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.pad #92
	sub	sp, sp, #92
	str	r0, [sp, #20]
	mov	r0, #5888
	bl	glMatrixMode
	bl	glLoadIdentity
	ldr	r1, [sp, #20]
	ldr	r1, [r1, #12]	@ float
	mov	r0, r1
	str	r1, [sp, #12]	@ float
	bl	__aeabi_fmul
	mov	r1, #0
	bl	__aeabi_fadd
	bl	__aeabi_f2d
	bl	sqrt
	bl	__aeabi_d2f
	mov	r1, #0
	mov	r4, r0
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	beq	.L53
	mov	r2, #0
	str	r2, [sp, #16]	@ float
	ldr	r8, [sp, #12]	@ float
	mov	r9, #-2147483648
	mov	sl, r2
.L54:
	mov	r1, #0
	mov	r0, sl
	bl	__aeabi_fmul
	mov	r5, r0
	mov	r1, r5
	mov	r0, r8
	bl	__aeabi_fsub
	mov	r1, #-2147483648
	mov	r7, r0
	mov	r0, r8
	bl	__aeabi_fmul
	mov	r1, r0
	mov	r0, r5
	bl	__aeabi_fadd
	ldr	r1, [sp, #16]	@ float
	mov	r6, r0
	mov	r0, r5
	bl	__aeabi_fsub
	mov	r5, r0
	mov	r1, r5
	mov	r0, sl
	bl	__aeabi_fmul
	mov	r1, r6
	mov	fp, r0
	mov	r0, r8
	bl	__aeabi_fmul
	mov	r1, r0
	mov	r0, fp
	bl	__aeabi_fsub
	mov	r1, r9
	mov	fp, r0
	mov	r0, r5
	bl	__aeabi_fmul
	mov	r1, r7
	mov	r9, r0
	mov	r0, r8
	bl	__aeabi_fmul
	mov	r1, r0
	mov	r0, r9
	bl	__aeabi_fadd
	mov	r1, r6
	mov	r9, r0
	ldr	r0, [sp, #16]	@ float
	bl	__aeabi_fmul
	mov	r1, r7
	mov	r4, r0
	mov	r0, sl
	bl	__aeabi_fmul
	mov	r1, r0
	mov	r0, r4
	bl	__aeabi_fsub
	mov	r1, r7
	str	r0, [sp, #8]	@ float
	mov	r0, r7
	bl	__aeabi_fmul
	mov	r1, r6
	mov	r4, r0
	mov	r0, r6
	bl	__aeabi_fmul
	mov	r1, r0
	mov	r0, r4
	bl	__aeabi_fadd
	mov	r1, r5
	mov	r4, r0
	mov	r0, r5
	bl	__aeabi_fmul
	mov	r1, r0
	mov	r0, r4
	bl	__aeabi_fadd
	bl	__aeabi_f2d
	bl	sqrt
	bl	__aeabi_d2f
	mov	r1, #0
	mov	r4, r0
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	bne	.L55
	mov	r0, r7
	mov	r1, r4
	bl	__aeabi_fdiv
	mov	r1, r4
	mov	r7, r0
	mov	r0, r6
	bl	__aeabi_fdiv
	mov	r1, r4
	mov	r6, r0
	mov	r0, r5
	bl	__aeabi_fdiv
	mov	r5, r0
.L55:
	mov	r1, fp
	mov	r0, fp
	bl	__aeabi_fmul
	mov	r1, r9
	mov	r4, r0
	mov	r0, r9
	bl	__aeabi_fmul
	mov	r1, r0
	mov	r0, r4
	bl	__aeabi_fadd
	mov	r4, r0
	ldr	r0, [sp, #8]	@ float
	mov	r1, r0
	bl	__aeabi_fmul
	mov	r1, r0
	mov	r0, r4
	bl	__aeabi_fadd
	bl	__aeabi_f2d
	bl	sqrt
	bl	__aeabi_d2f
	mov	r1, #0
	mov	r4, r0
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	bne	.L57
	mov	r0, fp
	mov	r1, r4
	bl	__aeabi_fdiv
	mov	r1, r4
	mov	fp, r0
	mov	r0, r9
	bl	__aeabi_fdiv
	mov	r1, r4
	mov	r9, r0
	ldr	r0, [sp, #8]	@ float
	bl	__aeabi_fdiv
	str	r0, [sp, #8]	@ float
.L57:
	ldr	r2, [sp, #8]	@ float
	ldr	r1, [sp, #16]	@ float
	mov	r4, #0
	mov	lr, #1065353216
	add	r0, sp, #24
	str	lr, [sp, #84]	@ float
	str	lr, [sp, #4]
	str	r2, [sp, #60]	@ float
	str	r1, [sp, #32]	@ float
	str	r7, [sp, #24]	@ float
	str	r6, [sp, #40]	@ float
	str	r5, [sp, #56]	@ float
	str	fp, [sp, #28]	@ float
	str	r9, [sp, #44]	@ float
	str	sl, [sp, #48]	@ float
	str	r8, [sp, #64]	@ float
	str	r4, [sp, #72]	@ float
	str	r4, [sp, #76]	@ float
	str	r4, [sp, #80]	@ float
	str	r4, [sp, #36]	@ float
	str	r4, [sp, #52]	@ float
	str	r4, [sp, #68]	@ float
	bl	glMultMatrixf
	ldr	ip, [sp, #12]
	mov	r0, #-2147483648
	add	r2, ip, #-2147483648
	mov	r1, r0
	bl	glTranslatef
	ldr	r3, [sp, #20]
	ldr	r0, [r3, #32]
	bl	__aeabi_i2f
	mov	r1, r4
	mov	r2, r4
	ldr	r3, [sp, #4]
	bl	glRotatef
	add	sp, sp, #92
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L53:
	mov	r1, r4
	mov	r0, #0
	bl	__aeabi_fdiv
	mov	r1, r4
	mov	sl, r0
	ldr	r0, [sp, #12]	@ float
	bl	__aeabi_fdiv
	add	r9, sl, #-2147483648
	mov	r8, r0
	str	sl, [sp, #16]	@ float
	b	.L54
	.fnend
	.size	_ZN13QuBasicCamera18setModelViewMatrixEv, .-_ZN13QuBasicCamera18setModelViewMatrixEv
	.global	__aeabi_dadd
	.global	__aeabi_dmul
	.global	__aeabi_ddiv
	.section	.text._ZN13QuBasicCamera19setProjectionMatrixEv,"axG",%progbits,_ZN13QuBasicCamera19setProjectionMatrixEv,comdat
	.align	2
	.weak	_ZN13QuBasicCamera19setProjectionMatrixEv
	.hidden	_ZN13QuBasicCamera19setProjectionMatrixEv
	.type	_ZN13QuBasicCamera19setProjectionMatrixEv, %function
_ZN13QuBasicCamera19setProjectionMatrixEv:
	.fnstart
.LFB3718:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	r5, [r0, #12]	@ float
	.pad #20
	sub	sp, sp, #20
	mov	r9, r0
	mov	r1, #1056964608
	ldr	r0, [r0, #8]	@ float
	bl	__aeabi_fmul
	mov	r1, r5
	bl	__aeabi_fdiv
	bl	__aeabi_f2d
	bl	atan
	mov	r2, #1107296256
	mov	r6, r0
	mov	r7, r1
	mov	r0, r5
	add	r1, r2, #13107200
	bl	__aeabi_fdiv
	mov	sl, #5888
	add	r4, sl, #1
	mov	r8, r0
	mov	r0, r4
	bl	glMatrixMode
	bl	glLoadIdentity
	ldr	r0, [r9, #16]
	bl	__aeabi_i2f
	mov	fp, r0
	ldr	r0, [r9, #20]
	bl	__aeabi_i2f
	mov	r1, r0
	mov	r0, fp
	bl	__aeabi_fdiv
	bl	__aeabi_f2d
	str	r0, [sp, #8]
	str	r1, [sp, #12]
	mov	r0, r4
	bl	glMatrixMode
	bl	glLoadIdentity
	mov	r2, r6
	mov	r3, r7
	mov	r0, r6
	mov	r1, r7
	bl	__aeabi_dadd
	mov	r9, #1124073472
	bl	__aeabi_d2f
	mov	r7, #4784128
	add	r1, r9, #3407872
	bl	__aeabi_fmul
	add	r1, r7, #4048
	add	r1, r1, #1073741835
	bl	__aeabi_fdiv
	mov	r4, #598016
	bl	__aeabi_f2d
	add	ip, r4, #504
	add	r3, ip, #1073741827
	mov	r2, #1610612736
	mov	fp, #1073741824
	bl	__aeabi_dmul
	add	r3, fp, #7733248
	mov	r2, #0
	add	r3, r3, #32768
	bl	__aeabi_ddiv
	bl	tan
	mov	r6, r0
	mov	r9, r1
	mov	r0, r8
	bl	__aeabi_f2d
	mov	r2, r6
	mov	r3, r9
	bl	__aeabi_dmul
	add	r7, r1, #-2147483648
	mov	r4, r0
	mov	r6, r1
	mov	r2, r0
	mov	r3, r7
	add	r1, sp, #8
	ldmia	r1, {r0-r1}
	bl	__aeabi_dmul
	bl	__aeabi_d2f
	mov	r2, r4
	mov	r3, r6
	mov	fp, r0
	add	r1, sp, #8
	ldmia	r1, {r0-r1}
	bl	__aeabi_dmul
	bl	__aeabi_d2f
	mov	r1, r7
	mov	r9, r0
	mov	r0, r4
	bl	__aeabi_d2f
	mov	r1, r6
	mov	r7, r0
	mov	r0, r4
	bl	__aeabi_d2f
	mov	ip, #1107296256
	add	r1, ip, #13107200
	mov	r4, r0
	mov	r0, r5
	str	r8, [sp, #0]	@ float
	bl	__aeabi_fmul
	mov	r1, r9
	mov	r2, r7
	mov	r3, r4
	str	r0, [sp, #4]	@ float
	mov	r0, fp
	bl	glFrustumf
	mov	r0, sl
	bl	glMatrixMode
	mov	r1, #4352
	add	r1, r1, #2
	mov	r0, #3152
	bl	glHint
	mov	r0, #1
	bl	glDepthMask
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
	.fnend
	.size	_ZN13QuBasicCamera19setProjectionMatrixEv, .-_ZN13QuBasicCamera19setProjectionMatrixEv
	.section	.text._ZN12QuDrawObject7disableEv,"axG",%progbits,_ZN12QuDrawObject7disableEv,comdat
	.align	2
	.weak	_ZN12QuDrawObject7disableEv
	.hidden	_ZN12QuDrawObject7disableEv
	.type	_ZN12QuDrawObject7disableEv, %function
_ZN12QuDrawObject7disableEv:
	.fnstart
.LFB2973:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	ldrb	r3, [r0, #120]	@ zero_extendqisi2
	cmp	r3, #0
	mov	r4, r0
	bne	.L71
.L65:
	ldrb	r0, [r4, #8]	@ zero_extendqisi2
	cmp	r0, #0
	bne	.L72
.L66:
	ldrb	r1, [r4, #36]	@ zero_extendqisi2
	cmp	r1, #0
	bne	.L73
.L67:
	ldrb	r2, [r4, #64]	@ zero_extendqisi2
	cmp	r2, #0
	bne	.L74
.L68:
	ldrb	ip, [r4, #92]	@ zero_extendqisi2
	cmp	ip, #0
	ldrne	r0, [r4, #112]
	blne	glDisableClientState
.L70:
	ldmfd	sp!, {r4, lr}
	bx	lr
.L74:
	ldr	r0, [r4, #84]
	bl	glDisableClientState
	b	.L68
.L73:
	ldr	r0, [r4, #56]
	bl	glDisableClientState
	b	.L67
.L72:
	ldr	r0, [r4, #28]
	bl	glDisableClientState
	b	.L66
.L71:
	ldr	r0, [r0, #124]
	bl	_ZN11QuBaseImage6unbindEv
	b	.L65
	.fnend
	.size	_ZN12QuDrawObject7disableEv, .-_ZN12QuDrawObject7disableEv
	.section	.text._ZN7testApp10mouseMovedE13QuScreenCoord,"ax",%progbits
	.align	2
	.global	_ZN7testApp10mouseMovedE13QuScreenCoord
	.hidden	_ZN7testApp10mouseMovedE13QuScreenCoord
	.type	_ZN7testApp10mouseMovedE13QuScreenCoord, %function
_ZN7testApp10mouseMovedE13QuScreenCoord:
	.fnstart
.LFB3880:
	@ Function supports interworking.
	@ args = 20, pretend = 16, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	sub	sp, sp, #16
	add	r0, sp, #4
	stmia	r0, {r1, r2, r3}
	add	sp, sp, #16
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN7testApp10mouseMovedE13QuScreenCoord, .-_ZN7testApp10mouseMovedE13QuScreenCoord
	.section	.text._ZN7testApp12mousePressedE13QuScreenCoordi,"ax",%progbits
	.align	2
	.global	_ZN7testApp12mousePressedE13QuScreenCoordi
	.hidden	_ZN7testApp12mousePressedE13QuScreenCoordi
	.type	_ZN7testApp12mousePressedE13QuScreenCoordi, %function
_ZN7testApp12mousePressedE13QuScreenCoordi:
	.fnstart
.LFB3878:
	@ Function supports interworking.
	@ args = 24, pretend = 16, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	sub	sp, sp, #16
	add	r0, sp, #4
	stmia	r0, {r1, r2, r3}
	add	sp, sp, #16
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN7testApp12mousePressedE13QuScreenCoordi, .-_ZN7testApp12mousePressedE13QuScreenCoordi
	.section	.text._ZN7testApp13mouseReleasedE13QuScreenCoordi,"ax",%progbits
	.align	2
	.global	_ZN7testApp13mouseReleasedE13QuScreenCoordi
	.hidden	_ZN7testApp13mouseReleasedE13QuScreenCoordi
	.type	_ZN7testApp13mouseReleasedE13QuScreenCoordi, %function
_ZN7testApp13mouseReleasedE13QuScreenCoordi:
	.fnstart
.LFB3879:
	@ Function supports interworking.
	@ args = 24, pretend = 16, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	sub	sp, sp, #16
	add	r0, sp, #4
	stmia	r0, {r1, r2, r3}
	add	sp, sp, #16
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN7testApp13mouseReleasedE13QuScreenCoordi, .-_ZN7testApp13mouseReleasedE13QuScreenCoordi
	.section	.text.T.1125,"ax",%progbits
	.align	2
	.type	T.1125, %function
T.1125:
	.fnstart
.LFB5514:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	cmp	r0, #0
	.pad #12
	sub	sp, sp, #12
	str	r0, [sp, #4]
	beq	.L103
.L112:
	ldr	r3, [sp, #4]
	ldr	fp, [r3, #12]
	cmp	fp, #0
	beq	.L86
.L111:
	ldr	r4, [fp, #12]
	cmp	r4, #0
	beq	.L87
.L110:
	ldr	r5, [r4, #12]
	cmp	r5, #0
	beq	.L88
.L109:
	ldr	r6, [r5, #12]
	cmp	r6, #0
	beq	.L89
.L108:
	ldr	r7, [r6, #12]
	cmp	r7, #0
	beq	.L90
.L107:
	ldr	r8, [r7, #12]
	cmp	r8, #0
	beq	.L91
.L106:
	ldr	sl, [r8, #12]
	cmp	sl, #0
	beq	.L92
.L105:
	ldr	r9, [sl, #12]
	cmp	r9, #0
	beq	.L93
	b	.L104
.L113:
	mov	r9, r3
.L104:
	ldr	r0, [r9, #12]
	bl	T.1125
	ldr	r1, [r9, #8]
	mov	r0, r9
	str	r1, [sp, #0]
	bl	free
	ldr	r3, [sp, #0]
	cmp	r3, #0
	bne	.L113
.L93:
	ldr	r9, [sl, #8]
	mov	r0, sl
	bl	free
	cmp	r9, #0
	movne	sl, r9
	bne	.L105
.L92:
	ldr	sl, [r8, #8]
	mov	r0, r8
	bl	free
	cmp	sl, #0
	movne	r8, sl
	bne	.L106
.L91:
	ldr	r8, [r7, #8]
	mov	r0, r7
	bl	free
	cmp	r8, #0
	movne	r7, r8
	bne	.L107
.L90:
	ldr	r7, [r6, #8]
	mov	r0, r6
	bl	free
	cmp	r7, #0
	movne	r6, r7
	bne	.L108
.L89:
	ldr	r6, [r5, #8]
	mov	r0, r5
	bl	free
	cmp	r6, #0
	movne	r5, r6
	bne	.L109
.L88:
	ldr	r5, [r4, #8]
	mov	r0, r4
	bl	free
	cmp	r5, #0
	movne	r4, r5
	bne	.L110
.L87:
	ldr	r4, [fp, #8]
	mov	r0, fp
	bl	free
	cmp	r4, #0
	movne	fp, r4
	bne	.L111
.L86:
	ldr	r2, [sp, #4]
	ldr	r4, [r2, #8]
	mov	r0, r2
	bl	free
	cmp	r4, #0
	strne	r4, [sp, #4]
	bne	.L112
.L103:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
	.fnend
	.size	T.1125, .-T.1125
	.section	.text._ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE9_M_insertEPNS_18_Rb_tree_node_baseESE_RKS5_SE_,"axG",%progbits,_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE9_M_insertEPNS_18_Rb_tree_node_baseESE_RKS5_SE_,comdat
	.align	2
	.weak	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE9_M_insertEPNS_18_Rb_tree_node_baseESE_RKS5_SE_
	.hidden	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE9_M_insertEPNS_18_Rb_tree_node_baseESE_RKS5_SE_
	.type	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE9_M_insertEPNS_18_Rb_tree_node_baseESE_RKS5_SE_, %function
_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE9_M_insertEPNS_18_Rb_tree_node_baseESE_RKS5_SE_:
	.fnstart
.LFB5332:
	@ Function supports interworking.
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	.save {r4, r5, r6, r7, r8, lr}
	mov	r4, r1
	ldr	r1, [r1, #0]
	cmp	r1, r3
	mov	r7, r3
	mov	r5, r0
	ldr	r8, [sp, #24]
	beq	.L115
	ldr	r3, [sp, #28]
	cmp	r3, #0
	beq	.L125
.L116:
	mov	r0, #48
	bl	malloc
	subs	r6, r0, #0
	beq	.L126
.L123:
	ldr	r3, [r8, #0]
	mov	r1, #0
	mov	lr, #4
	str	r3, [r6, #16]
	strb	r1, [r6, #20]
	str	lr, [r6, #24]
	str	r6, [r7, #12]
	ldr	r0, [r4, #0]
	ldr	ip, [r0, #12]
	cmp	r7, ip
	streq	r6, [r0, #12]
.L119:
	mov	lr, #0
	str	r7, [r6, #4]
	str	lr, [r6, #12]
	str	lr, [r6, #8]
	ldr	ip, [r4, #0]
	mov	r0, r6
	add	r1, ip, #4
	bl	_ZN4_STL10_Rb_globalIbE10_RebalanceEPNS_18_Rb_tree_node_baseERS3_
	ldr	r0, [r4, #4]
	add	r2, r0, #1
	str	r2, [r4, #4]
	mov	r0, r5
	str	r6, [r5, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L125:
	cmp	r2, #0
	beq	.L127
.L115:
	mov	r0, #48
	bl	malloc
	subs	r6, r0, #0
	bne	.L117
	mov	r0, #48
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
	mov	r6, r0
.L117:
	ldr	r1, [r8, #0]
	mov	lr, #0
	mov	ip, #4
	str	r1, [r6, #16]
	strb	lr, [r6, #20]
	str	ip, [r6, #24]
	str	r6, [r7, #8]
	ldr	r3, [r4, #0]
	cmp	r3, r7
	beq	.L128
	ldr	r2, [r3, #8]
	cmp	r7, r2
	streq	r6, [r3, #8]
	b	.L119
.L128:
	str	r6, [r7, #4]
	ldr	r3, [r4, #0]
	str	r6, [r3, #12]
	b	.L119
.L126:
	mov	r0, #48
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
	mov	r6, r0
	b	.L123
.L127:
	ldr	r2, [r8, #0]
	ldr	r0, [r7, #16]
	cmp	r2, r0
	bge	.L116
	b	.L115
	.fnend
	.size	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE9_M_insertEPNS_18_Rb_tree_node_baseESE_RKS5_SE_, .-_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE9_M_insertEPNS_18_Rb_tree_node_baseESE_RKS5_SE_
	.section	.text._ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE13insert_uniqueERKS5_,"axG",%progbits,_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE13insert_uniqueERKS5_,comdat
	.align	2
	.weak	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE13insert_uniqueERKS5_
	.hidden	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE13insert_uniqueERKS5_
	.type	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE13insert_uniqueERKS5_, %function
_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE13insert_uniqueERKS5_:
	.fnstart
.LFB5329:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	.save {r4, r5, r6, r7, lr}
	ldr	r3, [r1, #0]
	ldr	r6, [r3, #4]
	cmp	r6, #0
	.pad #20
	sub	sp, sp, #20
	mov	r7, r1
	mov	r4, r0
	mov	r5, r2
	moveq	r6, r3
	beq	.L137
	ldr	r0, [r2, #0]
	b	.L135
.L150:
	mov	r6, r2
.L135:
	ldr	r2, [r6, #16]
	cmp	r0, r2
	ldrlt	r2, [r6, #8]
	ldrge	r2, [r6, #12]
	movlt	r1, #1
	movge	r1, #0
	cmp	r2, #0
	bne	.L150
	cmp	r1, #0
	moveq	r0, r6
	bne	.L137
	ldr	r3, [r5, #0]
	ldr	r2, [r0, #16]
	cmp	r2, r3
	movge	r3, #0
	strge	r0, [r4, #0]
	strgeb	r3, [r4, #4]
	blt	.L151
.L129:
	mov	r0, r4
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L137:
	ldr	r3, [r3, #8]
	cmp	r6, r3
	beq	.L152
	mov	r0, r6
	bl	_ZN4_STL10_Rb_globalIbE12_M_decrementEPNS_18_Rb_tree_node_baseE
	ldr	r3, [r5, #0]
	ldr	r2, [r0, #16]
	cmp	r2, r3
	movge	r3, #0
	strge	r0, [r4, #0]
	strgeb	r3, [r4, #4]
	bge	.L129
.L151:
	mov	lr, #0
	mov	r1, r7
	add	r0, sp, #8
	mov	r2, lr
	mov	r3, r6
	stmia	sp, {r5, lr}	@ phole stm
	bl	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE9_M_insertEPNS_18_Rb_tree_node_baseESE_RKS5_SE_
	ldr	r1, [sp, #8]
	mov	r0, #1
	str	r1, [r4, #0]
	strb	r0, [r4, #4]
	b	.L129
.L152:
	mov	r1, r7
	add	r0, sp, #12
	mov	ip, #0
	mov	r2, r6
	mov	r3, r6
	stmia	sp, {r5, ip}	@ phole stm
	bl	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE9_M_insertEPNS_18_Rb_tree_node_baseESE_RKS5_SE_
	ldr	r1, [sp, #12]
	mov	r0, #1
	str	r1, [r4, #0]
	strb	r0, [r4, #4]
	b	.L129
	.fnend
	.size	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE13insert_uniqueERKS5_, .-_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE13insert_uniqueERKS5_
	.section	.text._ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS5_NS_16_Nonconst_traitsIS5_EEEERKS5_,"axG",%progbits,_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS5_NS_16_Nonconst_traitsIS5_EEEERKS5_,comdat
	.align	2
	.weak	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS5_NS_16_Nonconst_traitsIS5_EEEERKS5_
	.hidden	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS5_NS_16_Nonconst_traitsIS5_EEEERKS5_
	.type	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS5_NS_16_Nonconst_traitsIS5_EEEERKS5_, %function
_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS5_NS_16_Nonconst_traitsIS5_EEEERKS5_:
	.fnstart
.LFB5165:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	.save {r4, r5, r6, r7, r8, lr}
	ldr	r6, [r1, #0]
	ldr	ip, [r2, #0]
	mov	r8, r2
	ldr	r2, [r6, #8]
	cmp	ip, r2
	.pad #40
	sub	sp, sp, #40
	mov	r5, r1
	mov	r4, r3
	mov	r7, r0
	beq	.L181
	cmp	ip, r6
	beq	.L182
	mov	r0, ip
	bl	_ZN4_STL10_Rb_globalIbE12_M_decrementEPNS_18_Rb_tree_node_baseE
	mov	ip, r0
	ldr	r0, [r8, #0]
	ldr	r3, [r4, #0]
	ldr	r1, [r0, #16]
	cmp	r3, r1
	bge	.L168
	ldr	r2, [ip, #16]
	cmp	r3, r2
	ble	.L169
	ldr	r3, [ip, #12]
	cmp	r3, #0
	beq	.L180
.L174:
	mov	r2, r0
	mov	r1, r5
	mov	r0, r7
.L178:
	mov	ip, #0
	mov	r3, r2
	stmia	sp, {r4, ip}	@ phole stm
	bl	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE9_M_insertEPNS_18_Rb_tree_node_baseESE_RKS5_SE_
	b	.L153
.L168:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	ip, [r8, #0]
	ldr	r3, [r4, #0]
	ldr	r1, [ip, #16]
	cmp	r1, r3
	bge	.L171
	ldr	lr, [r5, #0]
	cmp	lr, r0
	beq	.L172
	ldr	r2, [r0, #16]
	cmp	r3, r2
	bge	.L173
.L172:
	ldr	r2, [ip, #12]
	cmp	r2, #0
	bne	.L174
	mov	r1, r5
	b	.L179
.L181:
	ldr	r3, [r1, #4]
	cmp	r3, #0
	beq	.L183
	ldr	r2, [r4, #0]
	ldr	r3, [ip, #16]
	cmp	r2, r3
	movlt	r2, ip
	blt	.L178
	bgt	.L159
.L171:
	str	ip, [r7, #0]
.L153:
	mov	r0, r7
	add	sp, sp, #40
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L169:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
.L173:
	add	r0, sp, #8
	mov	r1, r5
	mov	r2, r4
	bl	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE13insert_uniqueERKS5_
	ldr	r0, [sp, #8]
	str	r0, [r7, #0]
	b	.L153
.L182:
	ldr	r3, [ip, #12]
	ldr	r2, [r4, #0]
	ldr	lr, [r3, #16]
	cmp	lr, r2
	blt	.L184
	mov	r2, r4
	add	r0, sp, #16
	bl	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE13insert_uniqueERKS5_
	ldr	r3, [sp, #16]
	str	r3, [r7, #0]
	b	.L153
.L159:
	mov	r0, ip
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r1, [r5, #0]
	cmp	r1, r0
	beq	.L185
	ldr	lr, [r4, #0]
	ldr	r2, [r0, #16]
	cmp	lr, r2
	bge	.L162
	ldr	ip, [r8, #0]
	ldr	r3, [ip, #12]
	cmp	r3, #0
	bne	.L174
.L180:
	mov	r1, r5
	mov	r2, r3
.L179:
	mov	r3, ip
	mov	r0, r7
	stmia	sp, {r4, ip}	@ phole stm
	bl	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE9_M_insertEPNS_18_Rb_tree_node_baseESE_RKS5_SE_
	b	.L153
.L184:
	mov	r2, #0
	stmia	sp, {r4, ip}	@ phole stm
	bl	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE9_M_insertEPNS_18_Rb_tree_node_baseESE_RKS5_SE_
	b	.L153
.L183:
	add	r0, sp, #32
	mov	r2, r4
	bl	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE13insert_uniqueERKS5_
	ldr	r0, [sp, #32]
	str	r0, [r7, #0]
	b	.L153
.L162:
	add	r0, sp, #24
	mov	r1, r5
	mov	r2, r4
	bl	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE13insert_uniqueERKS5_
	ldr	r0, [sp, #24]
	str	r0, [r7, #0]
	b	.L153
.L185:
	ldr	ip, [r8, #0]
	mov	r1, r5
	mov	r3, ip
	mov	r0, r7
	mov	r2, #0
	stmia	sp, {r4, ip}	@ phole stm
	bl	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE9_M_insertEPNS_18_Rb_tree_node_baseESE_RKS5_SE_
	b	.L153
	.fnend
	.size	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS5_NS_16_Nonconst_traitsIS5_EEEERKS5_, .-_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS5_NS_16_Nonconst_traitsIS5_EEEERKS5_
	.section	.text.T.1135,"ax",%progbits
	.align	2
	.type	T.1135, %function
T.1135:
	.fnstart
.LFB5524:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	.save {r4, r5, r6, r7, r8, lr}
	ldr	r4, .L201
	ldr	ip, [r4, #0]
	cmp	r2, ip
	mov	r5, r2
	mov	r6, r0
	mov	r8, r3
	beq	.L187
	ldr	r3, [sp, #24]
	cmp	r3, #0
	beq	.L197
.L188:
	mov	r0, #24
	bl	malloc
	subs	r7, r0, #0
	beq	.L194
.L195:
	ldmia	r8, {ip, lr}	@ phole ldm
	str	lr, [r7, #20]
	str	ip, [r7, #16]
	str	r7, [r5, #12]
	ldr	r0, [r4, #0]
	ldr	r1, [r0, #12]
	cmp	r5, r1
	streq	r7, [r0, #12]
.L191:
	mov	r1, #0
	str	r1, [r7, #12]
	str	r1, [r7, #8]
	str	r5, [r7, #4]
	ldr	r0, [r4, #0]
	add	r1, r0, #4
	mov	r0, r7
	bl	_ZN4_STL10_Rb_globalIbE10_RebalanceEPNS_18_Rb_tree_node_baseERS3_
	ldr	r2, [r4, #4]
	add	r3, r2, #1
	str	r3, [r4, #4]
	mov	r0, r6
	str	r7, [r6, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L197:
	cmp	r1, #0
	beq	.L198
.L187:
	mov	r0, #24
	bl	malloc
	subs	r7, r0, #0
	beq	.L199
.L189:
	ldr	r1, [r8, #4]
	ldr	lr, [r8, #0]
	str	r1, [r7, #20]
	str	lr, [r7, #16]
	str	r7, [r5, #8]
	ldr	r3, [r4, #0]
	cmp	r5, r3
	beq	.L200
	ldr	r2, [r3, #8]
	cmp	r5, r2
	streq	r7, [r3, #8]
	b	.L191
.L200:
	str	r7, [r5, #4]
	ldr	r3, .L201
	ldr	ip, [r3, #0]
	str	r7, [ip, #12]
	b	.L191
.L194:
	mov	r0, #24
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
	mov	r7, r0
	b	.L195
.L199:
	mov	r0, #24
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
	mov	r7, r0
	b	.L189
.L198:
	ldr	r2, [r8, #0]
	ldr	r0, [r5, #16]
	cmp	r2, r0
	bcs	.L188
	b	.L187
.L202:
	.align	2
.L201:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	T.1135, .-T.1135
	.section	.text.T.1137,"ax",%progbits
	.align	2
	.type	T.1137, %function
T.1137:
	.fnstart
.LFB5526:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	ldr	r3, .L226
	ldr	r3, [r3, #0]
	ldr	r6, [r3, #4]
	cmp	r6, #0
	.pad #16
	sub	sp, sp, #16
	mov	r4, r0
	mov	r5, r1
	beq	.L204
	ldr	r0, [r1, #0]
	b	.L208
.L223:
	mov	r6, r2
.L208:
	ldr	r2, [r6, #16]
	cmp	r2, r0
	ldrhi	r2, [r6, #8]
	ldrls	r2, [r6, #12]
	movhi	r1, #1
	movls	r1, #0
	cmp	r2, #0
	bne	.L223
	cmp	r1, #0
	moveq	r0, r6
	beq	.L211
.L210:
	ldr	r0, [r3, #8]
	cmp	r6, r0
	beq	.L224
	mov	r0, r6
	bl	_ZN4_STL10_Rb_globalIbE12_M_decrementEPNS_18_Rb_tree_node_baseE
.L211:
	ldr	r3, [r5, #0]
	ldr	r1, [r0, #16]
	cmp	r1, r3
	movcs	r3, #0
	strcs	r0, [r4, #0]
	strcsb	r3, [r4, #4]
	bcc	.L225
.L203:
	mov	r0, r4
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L225:
	mov	lr, #0
	mov	r2, r6
	add	r0, sp, #12
	mov	r1, lr
	mov	r3, r5
	str	lr, [sp, #0]
	bl	T.1135
	ldr	r2, [sp, #12]
	mov	r0, #1
	str	r2, [r4, #0]
	strb	r0, [r4, #4]
	b	.L203
.L224:
	mov	r1, r6
	mov	r2, r6
	mov	ip, #0
	mov	r3, r5
	add	r0, sp, #8
	str	ip, [sp, #0]
	bl	T.1135
	ldr	r2, [sp, #8]
	mov	r1, #1
	str	r2, [r4, #0]
	strb	r1, [r4, #4]
	b	.L203
.L204:
	mov	r6, r3
	b	.L210
.L227:
	.align	2
.L226:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	T.1137, .-T.1137
	.section	.text.T.1130,"ax",%progbits
	.align	2
	.type	T.1130, %function
T.1130:
	.fnstart
.LFB5519:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	.save {r4, r5, r6, r7, lr}
	ldr	r6, .L262
	ldr	r3, [r6, #0]
	ldr	ip, [r1, #0]
	mov	r7, r1
	ldr	r1, [r3, #8]
	cmp	ip, r1
	.pad #44
	sub	sp, sp, #44
	mov	r5, r2
	mov	r4, r0
	beq	.L256
	cmp	ip, r3
	beq	.L257
	mov	r0, ip
	bl	_ZN4_STL10_Rb_globalIbE12_M_decrementEPNS_18_Rb_tree_node_baseE
	mov	ip, r0
	ldr	r0, [r7, #0]
	ldr	r3, [r5, #0]
	ldr	r2, [r0, #16]
	cmp	r3, r2
	bcs	.L243
	ldr	r1, [ip, #16]
	cmp	r3, r1
	bls	.L244
.L247:
	ldr	r1, [ip, #12]
	cmp	r1, #0
	beq	.L254
.L249:
	mov	r1, r0
	mov	ip, #0
	mov	r3, r5
	mov	r0, r4
	mov	r2, r1
	str	ip, [sp, #0]
	bl	T.1135
	b	.L228
.L243:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	ip, [r7, #0]
	ldr	r3, [r5, #0]
	ldr	r2, [ip, #16]
	cmp	r2, r3
	bcs	.L246
	ldr	lr, [r6, #0]
	cmp	r0, lr
	beq	.L247
	ldr	r1, [r0, #16]
	cmp	r3, r1
	bcs	.L248
	b	.L247
.L256:
	ldr	r3, [r6, #4]
	cmp	r3, #0
	beq	.L258
	ldr	r2, [r2, #0]
	ldr	r1, [ip, #16]
	cmp	r2, r1
	bcc	.L259
	bhi	.L234
.L246:
	str	ip, [r4, #0]
.L228:
	mov	r0, r4
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L244:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
.L248:
	add	r0, sp, #32
	mov	r1, r5
	bl	T.1137
	ldr	r0, [sp, #32]
	str	r0, [r4, #0]
	b	.L228
.L257:
	ldr	r2, [ip, #12]
	ldr	r3, [r5, #0]
	ldr	lr, [r2, #16]
	cmp	lr, r3
	bcc	.L260
	mov	r1, r5
	add	r0, sp, #24
	bl	T.1137
	ldr	ip, [sp, #24]
	str	ip, [r4, #0]
	b	.L228
.L234:
	mov	r0, ip
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r3, [r6, #0]
	cmp	r0, r3
	beq	.L261
	ldr	r2, [r5, #0]
	ldr	r1, [r0, #16]
	cmp	r2, r1
	bcs	.L237
	ldr	ip, [r7, #0]
	ldr	r1, [ip, #12]
	cmp	r1, #0
	bne	.L249
.L254:
	mov	r2, ip
	mov	r3, r5
	mov	r0, r4
	str	ip, [sp, #0]
	bl	T.1135
	b	.L228
.L260:
	mov	r3, r5
	mov	r1, #0
	str	ip, [sp, #0]
	bl	T.1135
	b	.L228
.L258:
	add	r0, sp, #8
	mov	r1, r2
	bl	T.1137
	ldr	r0, [sp, #8]
	str	r0, [r4, #0]
	b	.L228
.L259:
	mov	r1, ip
	mov	r2, ip
	mov	r3, r5
	mov	ip, #0
	str	ip, [sp, #0]
	bl	T.1135
	b	.L228
.L237:
	add	r0, sp, #16
	mov	r1, r5
	bl	T.1137
	ldr	r0, [sp, #16]
	str	r0, [r4, #0]
	b	.L228
.L261:
	ldr	lr, [r7, #0]
	mov	r3, r5
	mov	r2, lr
	mov	r0, r4
	mov	r1, #0
	str	lr, [sp, #0]
	bl	T.1135
	b	.L228
.L263:
	.align	2
.L262:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	T.1130, .-T.1130
	.section	.text._ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev,"axG",%progbits,_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev,comdat
	.align	2
	.weak	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	.hidden	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	.type	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev, %function
_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev:
	.fnstart
.LFB4372:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	ldr	r4, [r0, #0]
	mov	r5, r0
	ldr	r0, [r4, #0]
	ldr	r3, [r0, #-12]
	add	r3, r4, r3
	ldr	r2, [r3, #4]
	tst	r2, #8192
	bne	.L268
.L265:
	mov	r0, r5
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L268:
	ldr	r3, [r3, #88]
	cmp	r3, #0
	beq	.L265
	mov	r0, r3
	ldr	r1, [r3, #0]
	ldr	ip, [r1, #20]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	bne	.L265
	ldr	r3, [r4, #0]
	ldr	r0, [r3, #-12]
	add	r0, r4, r0
	ldr	r2, [r0, #8]
	ldr	lr, [r0, #20]
	orr	ip, r2, #1
	tst	ip, lr
	str	ip, [r0, #8]
	beq	.L265
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
	b	.L265
	.fnend
	.size	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev, .-_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	.section	.text._ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE8_M_eraseEPNS_13_Rb_tree_nodeIS5_EE,"axG",%progbits,_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE8_M_eraseEPNS_13_Rb_tree_nodeIS5_EE,comdat
	.align	2
	.weak	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE8_M_eraseEPNS_13_Rb_tree_nodeIS5_EE
	.hidden	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE8_M_eraseEPNS_13_Rb_tree_nodeIS5_EE
	.type	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE8_M_eraseEPNS_13_Rb_tree_nodeIS5_EE, %function
_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE8_M_eraseEPNS_13_Rb_tree_nodeIS5_EE:
	.fnstart
.LFB4888:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.pad #20
	sub	sp, sp, #20
	str	r1, [sp, #12]
	mov	r6, r0
	mov	r5, #0
.L270:
	ldr	r0, [sp, #12]
	cmp	r0, #0
	beq	.L311
	ldr	r0, [sp, #12]
	ldr	r2, [r0, #12]
	str	r2, [sp, #8]
.L271:
	ldr	r3, [sp, #8]
	cmp	r3, #0
	beq	.L312
	ldr	r1, [sp, #8]
	ldr	r3, [r1, #12]
	str	r3, [sp, #4]
.L272:
	ldr	lr, [sp, #4]
	cmp	lr, #0
	beq	.L313
	ldr	r4, [sp, #4]
	ldr	sl, [r4, #12]
.L273:
	cmp	sl, #0
	beq	.L314
	ldr	r9, [sl, #12]
.L274:
	cmp	r9, #0
	beq	.L315
	ldr	fp, [r9, #12]
	cmp	fp, #0
	beq	.L275
.L310:
	ldr	r8, [fp, #12]
	cmp	r8, #0
	beq	.L276
.L309:
	ldr	r7, [r8, #12]
	cmp	r7, #0
	beq	.L277
.L308:
	ldr	r4, [r7, #12]
	cmp	r4, #0
	bne	.L307
	b	.L278
.L316:
	mov	r4, r3
.L307:
	ldr	r1, [r4, #12]
	mov	r0, r6
	bl	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE8_M_eraseEPNS_13_Rb_tree_nodeIS5_EE
	ldrb	ip, [r4, #20]	@ zero_extendqisi2
	cmp	ip, #0
	ldr	r3, [r4, #8]
	beq	.L279
	ldr	r0, [r4, #28]
	cmp	r0, #0
	beq	.L280
	str	r3, [sp, #0]
	bl	_ZdaPv
	ldr	r3, [sp, #0]
.L280:
	str	r5, [r4, #28]
	strb	r5, [r4, #20]
.L279:
	mov	r0, r4
	str	r3, [sp, #0]
	bl	free
	ldr	r3, [sp, #0]
	cmp	r3, #0
	bne	.L316
.L278:
	ldrb	lr, [r7, #20]	@ zero_extendqisi2
	cmp	lr, #0
	ldr	r4, [r7, #8]
	beq	.L282
	ldr	r0, [r7, #28]
	cmp	r0, #0
	blne	_ZdaPv
.L283:
	str	r5, [r7, #28]
	strb	r5, [r7, #20]
.L282:
	mov	r0, r7
	bl	free
	cmp	r4, #0
	movne	r7, r4
	bne	.L308
.L277:
	ldrb	r2, [r8, #20]	@ zero_extendqisi2
	cmp	r2, #0
	ldr	r4, [r8, #8]
	beq	.L285
	ldr	r0, [r8, #28]
	cmp	r0, #0
	blne	_ZdaPv
.L286:
	str	r5, [r8, #28]
	strb	r5, [r8, #20]
.L285:
	mov	r0, r8
	bl	free
	cmp	r4, #0
	movne	r8, r4
	bne	.L309
.L276:
	ldrb	r0, [fp, #20]	@ zero_extendqisi2
	cmp	r0, #0
	ldr	r4, [fp, #8]
	beq	.L288
	ldr	r0, [fp, #28]
	cmp	r0, #0
	blne	_ZdaPv
.L289:
	str	r5, [fp, #28]
	strb	r5, [fp, #20]
.L288:
	mov	r0, fp
	bl	free
	cmp	r4, #0
	movne	fp, r4
	bne	.L310
.L275:
	ldrb	r3, [r9, #20]	@ zero_extendqisi2
	cmp	r3, #0
	ldr	r4, [r9, #8]
	beq	.L291
	ldr	r0, [r9, #28]
	cmp	r0, #0
	blne	_ZdaPv
.L292:
	str	r5, [r9, #28]
	strb	r5, [r9, #20]
.L291:
	mov	r0, r9
	bl	free
	mov	r9, r4
	b	.L274
.L315:
	ldrb	r1, [sl, #20]	@ zero_extendqisi2
	cmp	r1, #0
	ldr	r4, [sl, #8]
	beq	.L294
	ldr	r0, [sl, #28]
	cmp	r0, #0
	blne	_ZdaPv
.L295:
	str	r5, [sl, #28]
	strb	r5, [sl, #20]
.L294:
	mov	r0, sl
	bl	free
	mov	sl, r4
	b	.L273
.L314:
	ldr	r2, [sp, #4]
	ldrb	r4, [r2, #20]	@ zero_extendqisi2
	cmp	r4, #0
	ldr	r4, [r2, #8]
	beq	.L297
	ldr	r0, [r2, #28]
	cmp	r0, #0
	blne	_ZdaPv
.L298:
	ldr	ip, [sp, #4]
	str	r5, [ip, #28]
	strb	r5, [ip, #20]
.L297:
	ldr	r0, [sp, #4]
	bl	free
	str	r4, [sp, #4]
	b	.L272
.L313:
	ldr	r2, [sp, #8]
	ldrb	r0, [r2, #20]	@ zero_extendqisi2
	cmp	r0, #0
	ldr	r4, [r2, #8]
	beq	.L300
	ldr	r0, [r2, #28]
	cmp	r0, #0
	blne	_ZdaPv
.L301:
	ldr	r2, [sp, #8]
	str	r5, [r2, #28]
	strb	r5, [r2, #20]
.L300:
	ldr	r0, [sp, #8]
	bl	free
	str	r4, [sp, #8]
	b	.L271
.L312:
	ldr	r2, [sp, #12]
	ldrb	r1, [r2, #20]	@ zero_extendqisi2
	cmp	r1, #0
	ldr	r4, [r2, #8]
	beq	.L303
	ldr	r0, [r2, #28]
	cmp	r0, #0
	blne	_ZdaPv
.L304:
	ldr	ip, [sp, #12]
	str	r5, [ip, #28]
	strb	r5, [ip, #20]
.L303:
	ldr	r0, [sp, #12]
	bl	free
	str	r4, [sp, #12]
	b	.L270
.L311:
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
	.fnend
	.size	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE8_M_eraseEPNS_13_Rb_tree_nodeIS5_EE, .-_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE8_M_eraseEPNS_13_Rb_tree_nodeIS5_EE
	.global	__cxa_end_cleanup
	.section	.text._ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE,"axG",%progbits,_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE,comdat
	.align	2
	.weak	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
	.hidden	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
	.type	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE, %function
_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE:
	.fnstart
.LFB4797:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	subs	r5, r1, #0
	.pad #28
	sub	sp, sp, #28
	str	r0, [sp, #4]
	beq	.L359
	ldr	r7, .L373
.L361:
	ldr	r0, [sp, #4]
	ldr	r1, [r5, #12]
.LEHB0:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE0:
	ldr	ip, [r5, #28]
	cmp	ip, #0
	ldr	fp, [r5, #8]
	add	r8, r5, #28
	ldr	sl, [r5, #32]
	beq	.L319
	ldr	r0, [r7, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L323
	mov	r1, r0
	b	.L324
.L366:
	mov	r1, r3
	mov	r3, r2
.L324:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L366
.L323:
	cmp	r3, r0
	beq	.L326
	ldr	r2, [r3, #16]
	cmp	ip, r2
	mov	r2, r3
	bcs	.L327
.L326:
	str	r3, [sp, #16]
	add	r0, sp, #20
	mov	r3, #0
	add	r1, sp, #16
	add	r2, sp, #8
	str	ip, [sp, #8]
	str	r3, [sp, #12]
.LEHB1:
	bl	T.1130
	ldr	r2, [sp, #20]
.L327:
	ldr	r1, [r2, #20]
	sub	r0, r1, #1
	cmp	r0, #0
	str	r0, [r2, #20]
	beq	.L367
.L319:
	ldr	r0, [r5, #16]
	mov	r3, #0
	cmp	r0, #0
	str	sl, [r5, #32]
	str	r3, [r5, #28]
	blne	free
.L353:
	cmp	r5, #0
	movne	r0, r5
	blne	free
.L355:
	subs	r5, fp, #0
	bne	.L361
.L359:
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L367:
	ldr	r2, [r7, #0]
	ldr	r4, [r2, #4]
	cmp	r4, #0
	ldr	r1, [r5, #28]
	beq	.L328
	mov	r0, r2
	mov	r6, r4
	b	.L332
.L368:
	mov	r0, r6
	mov	r6, r3
.L332:
	ldr	r3, [r6, #16]
	cmp	r1, r3
	ldrhi	r3, [r6, #12]
	ldrls	r3, [r6, #8]
	movhi	r6, r0
	cmp	r3, #0
	bne	.L368
	mov	r0, r2
	b	.L336
.L369:
	mov	r0, r4
	mov	r4, r3
.L336:
	ldr	r3, [r4, #16]
	cmp	r1, r3
	ldrcs	r3, [r4, #12]
	ldrcc	r3, [r4, #8]
	movcs	r4, r0
	cmp	r3, #0
	bne	.L369
	cmp	r6, r4
	mov	r9, r6
	moveq	r6, r4
	beq	.L339
	mov	r0, r6
.L340:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r4, r0
	bne	.L340
	ldr	r2, [r7, #0]
	mov	r9, r4
.L339:
	ldr	ip, [r2, #8]
	cmp	r6, ip
	beq	.L370
.L342:
	cmp	r6, r9
	bne	.L364
	b	.L345
.L348:
	mov	r6, r4
.L364:
	mov	r0, r6
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	ip, [r7, #0]
	mov	r4, r0
	add	r3, ip, #12
	mov	r0, r6
	add	r1, ip, #4
	add	r2, ip, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L347:
	ldr	r2, [r7, #4]
	cmp	r4, r9
	sub	lr, r2, #1
	str	lr, [r7, #4]
	bne	.L348
.L345:
	ldr	r3, [r5, #32]
	cmp	r3, #1
	ldr	r3, [r5, #28]
	beq	.L371
	cmp	r3, #0
	beq	.L319
	ldr	r0, [r3, #-4]
	rsb	r4, r0, r0, asl #3
	add	r4, r3, r4, asl #2
	b	.L351
.L372:
	ldr	r2, [r4, #-28]!
	mov	r0, r4
	ldr	ip, [r2, #4]
	mov	lr, pc
	bx	ip
.L351:
	ldr	ip, [r8, #0]
	cmp	ip, r4
	bne	.L372
	ldr	lr, [r5, #28]
	sub	r0, lr, #8
	bl	_ZdaPv
	b	.L319
.L371:
	cmp	r3, #0
	beq	.L319
	mov	r0, r3
	ldr	r1, [r3, #0]
	ldr	ip, [r1, #8]
	mov	lr, pc
	bx	ip
	b	.L319
.L370:
	cmp	r9, r2
	bne	.L342
	ldr	lr, [r7, #4]
	cmp	lr, #0
	beq	.L345
	ldr	r0, [r9, #4]
	bl	T.1125
.LEHE1:
	ldr	r1, [r7, #0]
	str	r1, [r1, #8]
	ldr	r0, [r7, #0]
	mov	r2, #0
	str	r2, [r0, #4]
	ldr	r3, [r7, #0]
	str	r3, [r3, #12]
	str	r2, [r7, #4]
	b	.L345
.L328:
	mov	r9, r2
	mov	r6, r2
	b	.L339
.L360:
.L356:
	ldr	r3, [r5, #16]
	cmp	r3, #0
	mov	r4, r0
	movne	r0, r3
	blne	free
.L357:
	mov	r0, r4
.LEHB2:
	bl	__cxa_end_cleanup
.LEHE2:
.L374:
	.align	2
.L373:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.global	__gxx_personality_v0
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA4797:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4797-.LLSDACSB4797
.LLSDACSB4797:
	.uleb128 .LEHB0-.LFB4797
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB1-.LFB4797
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L360-.LFB4797
	.uleb128 0x0
	.uleb128 .LEHB2-.LFB4797
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE4797:
	.fnend
	.size	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE, .-_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
	.section	.text._ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev,"axG",%progbits,_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev,comdat
	.align	2
	.weak	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev
	.hidden	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev
	.type	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev, %function
_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev:
	.fnstart
.LFB3184:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	ldr	r3, [r0, #4]
	cmp	r3, #0
	mov	r4, r0
	bne	.L382
.L376:
	ldr	r0, [r4, #0]
	cmp	r0, #0
	blne	free
.L379:
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L382:
	ldr	r2, [r0, #0]
	ldr	r1, [r2, #4]
.LEHB3:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE3:
	ldr	lr, [r4, #0]
	str	lr, [lr, #8]
	ldr	ip, [r4, #0]
	mov	r0, #0
	str	r0, [ip, #4]
	ldr	r1, [r4, #0]
	str	r1, [r1, #12]
	str	r0, [r4, #4]
	b	.L376
.L381:
.L377:
	ldr	r3, [r4, #0]
	cmp	r3, #0
	mov	r4, r0
	movne	r0, r3
	blne	free
.L378:
	mov	r0, r4
.LEHB4:
	bl	__cxa_end_cleanup
.LEHE4:
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA3184:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3184-.LLSDACSB3184
.LLSDACSB3184:
	.uleb128 .LEHB3-.LFB3184
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L381-.LFB3184
	.uleb128 0x0
	.uleb128 .LEHB4-.LFB3184
	.uleb128 .LEHE4-.LEHB4
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE3184:
	.fnend
	.size	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev, .-_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev
	.section	.text._ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE3putEc,"axG",%progbits,_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE3putEc,comdat
	.align	2
	.weak	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE3putEc
	.hidden	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE3putEc
	.type	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE3putEc, %function
_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE3putEc:
	.fnstart
.LFB4381:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	.pad #8
	sub	sp, sp, #8
	str	r0, [sp, #0]
	ldr	r2, [r0, #0]
	ldr	r3, [r2, #-12]
	add	r3, r0, r3
	mov	r4, r0
	ldr	r0, [r3, #8]
	cmp	r0, #0
	mov	r6, r1
	movne	r3, #0
	bne	.L385
	ldr	r1, [r3, #88]
	cmp	r1, #0
	beq	.L404
.L386:
	ldr	r5, [r3, #92]
	cmp	r5, #0
	beq	.L388
	ldr	ip, [r5, #0]
	ldr	r1, [ip, #-12]
	add	r0, r5, r1
	ldr	r1, [r0, #88]
	cmp	r1, #0
	beq	.L388
	mov	r0, r1
	ldr	r3, [r1, #0]
.LEHB5:
	ldr	ip, [r3, #20]
	mov	lr, pc
	bx	ip
.LEHE5:
	cmn	r0, #1
	beq	.L389
.L403:
	ldr	r2, [r4, #0]
	ldr	ip, [r2, #-12]
	add	r3, r4, ip
.L388:
	ldr	r3, [r3, #8]
	rsbs	r3, r3, #1
	movcc	r3, #0
.L385:
	cmp	r3, #0
	strb	r3, [sp, #4]
	beq	.L393
	ldr	r1, [r2, #-12]
	add	r0, r4, r1
	ldr	r3, [r0, #88]
	add	r2, r3, #20
	ldmia	r2, {r2, r5}	@ phole ldm
	cmp	r2, r5
	strccb	r6, [r2], #1
	strcc	r2, [r3, #20]
	bcs	.L405
.L395:
	ldr	r5, [sp, #0]
	ldr	r3, [r5, #0]
	ldr	ip, [r3, #-12]
	add	r3, r5, ip
	ldr	r1, [r3, #4]
	tst	r1, #8192
	bne	.L406
.L397:
	mov	r0, r4
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L405:
	mov	r0, r3
	mov	r1, r6
	ldr	ip, [r3, #0]
.LEHB6:
	ldr	ip, [ip, #52]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	bne	.L395
	ldr	r2, [r4, #0]
.L393:
	ldr	r0, [r2, #-12]
	add	r0, r4, r0
	ldr	r5, [r0, #8]
	ldr	r2, [r0, #20]
	orr	r3, r5, #1
	tst	r3, r2
	str	r3, [r0, #8]
	beq	.L395
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
.LEHE6:
	ldr	r5, [sp, #0]
	ldr	r3, [r5, #0]
	ldr	ip, [r3, #-12]
	add	r3, r5, ip
	ldr	r1, [r3, #4]
	tst	r1, #8192
	beq	.L397
.L406:
	ldr	r3, [r3, #88]
	cmp	r3, #0
	beq	.L397
	mov	r0, r3
	ldr	r2, [r3, #0]
.LEHB7:
	ldr	ip, [r2, #20]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	bne	.L397
	ldr	r3, [r5, #0]
	ldr	r0, [r3, #-12]
	add	r0, r5, r0
	ldr	ip, [r0, #8]
	ldr	r1, [r0, #20]
	orr	lr, ip, #1
	tst	lr, r1
	str	lr, [r0, #8]
	beq	.L397
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
	b	.L397
.L389:
	ldr	r1, [r5, #0]
	ldr	r0, [r1, #-12]
	add	r0, r5, r0
	ldr	r5, [r0, #8]
	ldr	r2, [r0, #20]
	orr	lr, r5, #1
	tst	lr, r2
	str	lr, [r0, #8]
	beq	.L403
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
	b	.L403
.L404:
	ldr	r5, [r3, #20]
	mov	r2, #1
	tst	r2, r5
	str	r2, [r3, #8]
	movne	r0, r3
	blne	_ZN4_STL8ios_base16_M_throw_failureEv
.LEHE7:
.L402:
	ldr	r2, [r4, #0]
	ldr	ip, [r2, #-12]
	add	r3, r4, ip
	b	.L386
.L401:
.L399:
	mov	r4, r0
	mov	r0, sp
	bl	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	mov	r0, r4
.LEHB8:
	bl	__cxa_end_cleanup
.LEHE8:
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA4381:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4381-.LLSDACSB4381
.LLSDACSB4381:
	.uleb128 .LEHB5-.LFB4381
	.uleb128 .LEHE5-.LEHB5
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB6-.LFB4381
	.uleb128 .LEHE6-.LEHB6
	.uleb128 .L401-.LFB4381
	.uleb128 0x0
	.uleb128 .LEHB7-.LFB4381
	.uleb128 .LEHE7-.LEHB7
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB8-.LFB4381
	.uleb128 .LEHE8-.LEHB8
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE4381:
	.fnend
	.size	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE3putEc, .-_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE3putEc
	.section	.text._ZN4_STL4endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_,"axG",%progbits,_ZN4_STL4endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_,comdat
	.align	2
	.weak	_ZN4_STL4endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.hidden	_ZN4_STL4endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.type	_ZN4_STL4endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_, %function
_ZN4_STL4endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_:
	.fnstart
.LFB3931:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	ldr	r0, [r0, #0]
	ldr	r3, [r0, #-12]
	add	lr, r4, r3
	ldr	ip, [lr, #64]
	mov	r1, #10
	mov	r0, ip
	ldr	r2, [ip, #0]
	ldr	ip, [r2, #24]
	mov	lr, pc
	bx	ip
	mov	r1, r0
	mov	r0, r4
	bl	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE3putEc
	ldr	r1, [r4, #0]
	ldr	r0, [r1, #-12]
	add	r3, r4, r0
	ldr	r3, [r3, #88]
	cmp	r3, #0
	beq	.L408
	mov	r0, r3
	ldr	r1, [r3, #0]
	ldr	ip, [r1, #20]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	beq	.L411
.L408:
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L411:
	ldr	r3, [r4, #0]
	ldr	r0, [r3, #-12]
	add	r0, r4, r0
	ldr	lr, [r0, #8]
	ldr	r2, [r0, #20]
	orr	ip, lr, #1
	tst	ip, r2
	str	ip, [r0, #8]
	beq	.L408
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
	b	.L408
	.fnend
	.size	_ZN4_STL4endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_, .-_ZN4_STL4endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.section	.text._ZN12QuDrawObject6enableEv,"axG",%progbits,_ZN12QuDrawObject6enableEv,comdat
	.align	2
	.weak	_ZN12QuDrawObject6enableEv
	.hidden	_ZN12QuDrawObject6enableEv
	.type	_ZN12QuDrawObject6enableEv, %function
_ZN12QuDrawObject6enableEv:
	.fnstart
.LFB2972:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	ldrb	r3, [r0, #120]	@ zero_extendqisi2
	cmp	r3, #0
	mov	r4, r0
	beq	.L413
	ldr	r2, [r0, #124]
	mov	r0, r2
	ldr	r1, [r2, #0]
	ldr	ip, [r1, #0]
	mov	lr, pc
	bx	ip
	cmp	r0, #0
	beq	.L438
.L413:
	ldrb	r0, [r4, #8]	@ zero_extendqisi2
	cmp	r0, #0
	bne	.L439
.L415:
	ldrb	r2, [r4, #36]	@ zero_extendqisi2
	cmp	r2, #0
	bne	.L440
.L421:
	ldrb	r2, [r4, #64]	@ zero_extendqisi2
	cmp	r2, #0
	bne	.L441
.L427:
	ldrb	r2, [r4, #92]	@ zero_extendqisi2
	cmp	r2, #0
	bne	.L442
.L438:
	ldmfd	sp!, {r4, lr}
	bx	lr
.L442:
	ldr	r0, [r4, #112]
	bl	glEnableClientState
	ldr	r0, [r4, #112]
	sub	r1, r0, #32768
	sub	ip, r1, #116
	cmp	ip, #4
	ldrls	pc, [pc, ip, asl #2]
	b	.L438
.L437:
	.word	.L433
	.word	.L434
	.word	.L435
	.word	.L438
	.word	.L436
.L441:
	ldr	r0, [r4, #84]
	bl	glEnableClientState
	ldr	r0, [r4, #84]
	sub	r1, r0, #32768
	sub	ip, r1, #116
	cmp	ip, #4
	ldrls	pc, [pc, ip, asl #2]
	b	.L427
.L432:
	.word	.L428
	.word	.L429
	.word	.L430
	.word	.L427
	.word	.L431
.L440:
	ldr	r0, [r4, #56]
	bl	glEnableClientState
	ldr	r3, [r4, #56]
	sub	r1, r3, #32768
	sub	ip, r1, #116
	cmp	ip, #4
	ldrls	pc, [pc, ip, asl #2]
	b	.L421
.L426:
	.word	.L422
	.word	.L423
	.word	.L424
	.word	.L421
	.word	.L425
.L439:
	ldr	r0, [r4, #28]
	bl	glEnableClientState
	ldr	r1, [r4, #28]
	sub	r3, r1, #32768
	sub	ip, r3, #116
	cmp	ip, #4
	ldrls	pc, [pc, ip, asl #2]
	b	.L415
.L420:
	.word	.L416
	.word	.L417
	.word	.L418
	.word	.L415
	.word	.L419
.L419:
	ldr	r0, [r4, #24]
	ldr	r2, [r4, #20]
	ldr	ip, [r4, #12]
	ldr	r3, [r4, #16]
	mov	lr, #5120
	add	r3, r3, r0, asl #2
	mul	r2, ip, r2
	add	r1, lr, #6
	ldr	r0, [r4, #32]
	bl	glTexCoordPointer
	b	.L415
.L418:
	ldr	r0, [r4, #12]
	add	r2, r4, #20
	ldmia	r2, {r2, lr}	@ phole ldm
	ldr	r3, [r4, #16]
	mov	r1, #5120
	mul	r2, r0, r2
	add	r3, r3, lr, asl #2
	add	r1, r1, #6
	ldr	r0, [r4, #32]
	bl	glColorPointer
	b	.L415
.L417:
	add	r0, r4, #16
	ldmia	r0, {r0, r1}	@ phole ldm
	ldr	ip, [r4, #12]
	ldr	r2, [r4, #24]
	mov	lr, #5120
	add	r2, r0, r2, asl #2
	mul	r1, ip, r1
	add	r0, lr, #6
	bl	glNormalPointer
	b	.L415
.L416:
	ldr	r3, [r4, #12]
	ldr	r0, [r4, #24]
	ldr	r2, [r4, #20]
	ldr	lr, [r4, #16]
	mov	r1, #5120
	mul	r2, r3, r2
	add	r1, r1, #6
	add	r3, lr, r0, asl #2
	ldr	r0, [r4, #32]
	bl	glVertexPointer
	b	.L415
.L425:
	ldr	r0, [r4, #52]
	add	r1, r4, #44
	ldmia	r1, {r1, r2}	@ phole ldm
	ldr	ip, [r4, #40]
	mov	lr, #5120
	add	r3, r1, r0, asl #2
	mul	r2, ip, r2
	add	r1, lr, #6
	ldr	r0, [r4, #60]
	bl	glTexCoordPointer
	b	.L421
.L424:
	ldr	r3, [r4, #40]
	ldr	r0, [r4, #52]
	ldr	r2, [r4, #48]
	ldr	lr, [r4, #44]
	mov	r1, #5120
	mul	r2, r3, r2
	add	r1, r1, #6
	add	r3, lr, r0, asl #2
	ldr	r0, [r4, #60]
	bl	glColorPointer
	b	.L421
.L423:
	ldr	r1, [r4, #48]
	ldr	ip, [r4, #40]
	ldr	r2, [r4, #52]
	ldr	r3, [r4, #44]
	mov	lr, #5120
	mul	r1, ip, r1
	add	r2, r3, r2, asl #2
	add	r0, lr, #6
	bl	glNormalPointer
	b	.L421
.L422:
	ldr	r0, [r4, #40]
	ldr	r1, [r4, #52]
	ldr	r2, [r4, #48]
	ldr	r3, [r4, #44]
	mov	lr, #5120
	mul	r2, r0, r2
	add	r3, r3, r1, asl #2
	ldr	r0, [r4, #60]
	add	r1, lr, #6
	bl	glVertexPointer
	b	.L421
.L431:
	ldr	r1, [r4, #80]
	ldr	r2, [r4, #76]
	ldr	ip, [r4, #68]
	ldr	r3, [r4, #72]
	mov	lr, #5120
	add	r3, r3, r1, asl #2
	mul	r2, ip, r2
	add	r1, lr, #6
	ldr	r0, [r4, #88]
	bl	glTexCoordPointer
	b	.L427
.L430:
	ldr	r3, [r4, #68]
	ldr	r0, [r4, #80]
	ldr	r2, [r4, #76]
	ldr	lr, [r4, #72]
	mov	r1, #5120
	mul	r2, r3, r2
	add	r1, r1, #6
	add	r3, lr, r0, asl #2
	ldr	r0, [r4, #88]
	bl	glColorPointer
	b	.L427
.L429:
	ldr	r1, [r4, #76]
	ldr	ip, [r4, #68]
	ldr	r2, [r4, #80]
	ldr	r3, [r4, #72]
	mov	lr, #5120
	mul	r1, ip, r1
	add	r2, r3, r2, asl #2
	add	r0, lr, #6
	bl	glNormalPointer
	b	.L427
.L428:
	ldr	r1, [r4, #68]
	ldr	r0, [r4, #80]
	ldr	r2, [r4, #76]
	ldr	r3, [r4, #72]
	mov	lr, #5120
	mul	r2, r1, r2
	add	r3, r3, r0, asl #2
	add	r1, lr, #6
	ldr	r0, [r4, #88]
	bl	glVertexPointer
	b	.L427
.L436:
	ldr	r1, [r4, #108]
	ldr	r2, [r4, #104]
	ldr	ip, [r4, #96]
	ldr	r3, [r4, #100]
	mov	lr, #5120
	add	r3, r3, r1, asl #2
	mul	r2, ip, r2
	ldr	r0, [r4, #116]
	add	r1, lr, #6
	bl	glTexCoordPointer
	b	.L438
.L435:
	ldr	r3, [r4, #96]
	ldr	r0, [r4, #108]
	ldr	r2, [r4, #104]
	ldr	lr, [r4, #100]
	mov	r1, #5120
	mul	r2, r3, r2
	add	r1, r1, #6
	add	r3, lr, r0, asl #2
	ldr	r0, [r4, #116]
	bl	glColorPointer
	b	.L438
.L434:
	ldr	r3, [r4, #100]
	ldr	r1, [r4, #104]
	ldr	ip, [r4, #96]
	ldr	r2, [r4, #108]
	mov	lr, #5120
	mul	r1, ip, r1
	add	r2, r3, r2, asl #2
	add	r0, lr, #6
	bl	glNormalPointer
	b	.L438
.L433:
	ldr	r1, [r4, #96]
	ldr	r0, [r4, #108]
	ldr	r2, [r4, #104]
	ldr	r3, [r4, #100]
	mov	lr, #5120
	mul	r2, r1, r2
	add	r3, r3, r0, asl #2
	add	r1, lr, #6
	ldr	r0, [r4, #116]
	bl	glVertexPointer
	b	.L438
	.fnend
	.size	_ZN12QuDrawObject6enableEv, .-_ZN12QuDrawObject6enableEv
	.section	.text._ZN12QuDrawObjectD2Ev,"axG",%progbits,_ZN12QuDrawObjectD2Ev,comdat
	.align	2
	.weak	_ZN12QuDrawObjectD2Ev
	.hidden	_ZN12QuDrawObjectD2Ev
	.type	_ZN12QuDrawObjectD2Ev, %function
_ZN12QuDrawObjectD2Ev:
	.fnstart
.LFB2953:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, lr}
	ldr	ip, [r0, #124]
	ldr	r3, .L508
	mov	r8, r0
	cmp	ip, #0
	.pad #16
	sub	sp, sp, #16
	mov	r4, r0
	str	r3, [r8], #124
	ldr	sl, [r0, #128]
	beq	.L444
	ldr	r7, .L508+4
	ldr	r0, [r7, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L448
	mov	r1, r0
	b	.L449
.L501:
	mov	r1, r3
	mov	r3, r2
.L449:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L501
.L448:
	cmp	r3, r0
	beq	.L451
	ldr	r2, [r3, #16]
	cmp	ip, r2
	mov	r2, r3
	bcs	.L452
.L451:
	str	ip, [sp, #0]
	add	r0, sp, #12
	mov	ip, #0
	add	r1, sp, #8
	mov	r2, sp
	str	r3, [sp, #8]
	str	ip, [sp, #4]
.LEHB9:
	bl	T.1130
	ldr	r2, [sp, #12]
.L452:
	ldr	r1, [r2, #20]
	sub	r0, r1, #1
	cmp	r0, #0
	str	r0, [r2, #20]
	beq	.L502
.L444:
	ldrb	ip, [r4, #92]	@ zero_extendqisi2
	mov	r3, #0
	cmp	ip, #0
	str	r3, [r4, #124]
	str	sl, [r4, #128]
	beq	.L478
	ldr	r0, [r4, #100]
	cmp	r0, r3
	blne	_ZdaPv
.L479:
	mov	r1, #0
	strb	r1, [r4, #92]
	str	r1, [r4, #100]
.L478:
	ldrb	r0, [r4, #64]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L482
	ldr	r0, [r4, #72]
	cmp	r0, #0
	blne	_ZdaPv
.L483:
	mov	r2, #0
	strb	r2, [r4, #64]
	str	r2, [r4, #72]
.L482:
	ldrb	r3, [r4, #36]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L486
	ldr	r0, [r4, #44]
	cmp	r0, #0
	blne	_ZdaPv
.L487:
	mov	ip, #0
	strb	ip, [r4, #36]
	str	ip, [r4, #44]
.L486:
	ldrb	r1, [r4, #8]	@ zero_extendqisi2
	cmp	r1, #0
	beq	.L490
	ldr	r0, [r4, #16]
	cmp	r0, #0
	blne	_ZdaPv
.L491:
	mov	r0, #0
	strb	r0, [r4, #8]
	str	r0, [r4, #16]
.L490:
	mov	r0, r4
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L502:
	ldr	r1, [r7, #0]
	ldr	r5, [r1, #4]
	cmp	r5, #0
	ldr	r2, [r4, #124]
	beq	.L453
	mov	r0, r1
	mov	r6, r5
	b	.L457
.L503:
	mov	r0, r6
	mov	r6, r3
.L457:
	ldr	r3, [r6, #16]
	cmp	r2, r3
	ldrhi	r3, [r6, #12]
	ldrls	r3, [r6, #8]
	movhi	r6, r0
	cmp	r3, #0
	bne	.L503
	mov	r0, r1
	b	.L461
.L504:
	mov	r0, r5
	mov	r5, r3
.L461:
	ldr	r3, [r5, #16]
	cmp	r2, r3
	ldrcs	r3, [r5, #12]
	ldrcc	r3, [r5, #8]
	movcs	r5, r0
	cmp	r3, #0
	bne	.L504
	cmp	r6, r5
	mov	r9, r6
	moveq	r6, r5
	beq	.L464
	mov	r0, r6
.L465:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r5, r0
	bne	.L465
	ldr	r1, [r7, #0]
	mov	r9, r5
.L464:
	ldr	r3, [r1, #8]
	cmp	r6, r3
	beq	.L505
.L467:
	cmp	r6, r9
	bne	.L500
	b	.L470
.L473:
	mov	r6, r5
.L500:
	mov	r0, r6
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r2, [r7, #0]
	mov	r5, r0
	add	r3, r2, #12
	add	r1, r2, #4
	mov	r0, r6
	add	r2, r2, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L472:
	ldr	lr, [r7, #4]
	cmp	r5, r9
	sub	r3, lr, #1
	str	r3, [r7, #4]
	bne	.L473
.L470:
	ldr	ip, [r4, #128]
	cmp	ip, #1
	ldr	r3, [r4, #124]
	beq	.L506
	cmp	r3, #0
	beq	.L444
	ldr	r0, [r3, #-4]
	rsb	r5, r0, r0, asl #3
	add	r5, r3, r5, asl #2
	b	.L476
.L507:
	ldr	r3, [r5, #-28]!
	mov	r0, r5
	ldr	ip, [r3, #4]
	mov	lr, pc
	bx	ip
.L476:
	ldr	r2, [r8, #0]
	cmp	r2, r5
	bne	.L507
	ldr	lr, [r4, #124]
	sub	r0, lr, #8
	bl	_ZdaPv
	b	.L444
.L506:
	cmp	r3, #0
	beq	.L444
	mov	r0, r3
	ldr	r1, [r3, #0]
	ldr	ip, [r1, #8]
	mov	lr, pc
	bx	ip
	b	.L444
.L453:
	mov	r9, r1
	mov	r6, r1
	b	.L464
.L505:
	cmp	r9, r1
	bne	.L467
	ldr	r2, [r7, #4]
	cmp	r2, #0
	beq	.L470
	ldr	r0, [r9, #4]
	bl	T.1125
.LEHE9:
	ldr	r1, [r7, #0]
	str	r1, [r1, #8]
	ldr	r0, [r7, #0]
	mov	lr, #0
	str	lr, [r0, #4]
	ldr	ip, [r7, #0]
	str	ip, [ip, #12]
	str	lr, [r7, #4]
	b	.L470
.L495:
.L480:
.L496:
.L484:
.L497:
.L488:
.L498:
.L492:
	mov	r6, r0
	add	r0, r4, #92
	bl	_ZN14QuArrayPointerIfE6deInitEv
	add	r0, r4, #64
	bl	_ZN14QuArrayPointerIfE6deInitEv
	add	r0, r4, #36
	bl	_ZN14QuArrayPointerIfE6deInitEv
	add	r0, r4, #8
	bl	_ZN14QuArrayPointerIfE6deInitEv
	mov	r0, r6
.LEHB10:
	bl	__cxa_end_cleanup
.LEHE10:
.L509:
	.align	2
.L508:
	.word	_ZTV12QuDrawObject+8
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA2953:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2953-.LLSDACSB2953
.LLSDACSB2953:
	.uleb128 .LEHB9-.LFB2953
	.uleb128 .LEHE9-.LEHB9
	.uleb128 .L495-.LFB2953
	.uleb128 0x0
	.uleb128 .LEHB10-.LFB2953
	.uleb128 .LEHE10-.LEHB10
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE2953:
	.fnend
	.size	_ZN12QuDrawObjectD2Ev, .-_ZN12QuDrawObjectD2Ev
	.section	.text._ZN17QuImageDrawObjectD1Ev,"axG",%progbits,_ZN17QuImageDrawObjectD1Ev,comdat
	.align	2
	.weak	_ZN17QuImageDrawObjectD1Ev
	.hidden	_ZN17QuImageDrawObjectD1Ev
	.type	_ZN17QuImageDrawObjectD1Ev, %function
_ZN17QuImageDrawObjectD1Ev:
	.fnstart
.LFB2982:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L512
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	str	r3, [r0, #0]
	bl	_ZN12QuDrawObjectD2Ev
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L513:
	.align	2
.L512:
	.word	_ZTV17QuImageDrawObject+8
	.fnend
	.size	_ZN17QuImageDrawObjectD1Ev, .-_ZN17QuImageDrawObjectD1Ev
	.section	.text._ZN21QuColorableDrawObjectIiED0Ev,"axG",%progbits,_ZN21QuColorableDrawObjectIiED0Ev,comdat
	.align	2
	.weak	_ZN21QuColorableDrawObjectIiED0Ev
	.hidden	_ZN21QuColorableDrawObjectIiED0Ev
	.type	_ZN21QuColorableDrawObjectIiED0Ev, %function
_ZN21QuColorableDrawObjectIiED0Ev:
	.fnstart
.LFB3750:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	mov	r4, r0
	ldr	r0, [r0, #144]
	ldr	r3, .L524
	cmp	r0, #0
	str	r3, [r4, #0]
	bne	.L523
.L515:
	ldr	r0, [r4, #140]
	cmp	r0, #0
	blne	free
.L518:
	ldr	r2, .L524+4
	mov	r0, r4
	str	r2, [r4, #0]
.LEHB11:
	bl	_ZN12QuDrawObjectD2Ev
.LEHE11:
	mov	r0, r4
	bl	_ZdlPv
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L523:
	ldr	r2, [r4, #140]
	add	r0, r4, #140
	ldr	r1, [r2, #4]
.LEHB12:
	bl	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE8_M_eraseEPNS_13_Rb_tree_nodeIS5_EE
.LEHE12:
	ldr	r3, [r4, #140]
	str	r3, [r3, #8]
	ldr	lr, [r4, #140]
	mov	r1, #0
	str	r1, [lr, #4]
	ldr	ip, [r4, #140]
	str	ip, [ip, #12]
	str	r1, [r4, #144]
	b	.L515
.L521:
.L516:
	ldr	r3, [r4, #140]
	cmp	r3, #0
	mov	r5, r0
	movne	r0, r3
	blne	free
.L517:
.L522:
.L519:
	ldr	r0, .L524+4
	str	r0, [r4, #0]
	mov	r0, r4
	bl	_ZN12QuDrawObjectD2Ev
	mov	r0, r5
.LEHB13:
	bl	__cxa_end_cleanup
.LEHE13:
.L525:
	.align	2
.L524:
	.word	_ZTV21QuColorableDrawObjectIiE+8
	.word	_ZTV17QuImageDrawObject+8
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA3750:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3750-.LLSDACSB3750
.LLSDACSB3750:
	.uleb128 .LEHB11-.LFB3750
	.uleb128 .LEHE11-.LEHB11
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB12-.LFB3750
	.uleb128 .LEHE12-.LEHB12
	.uleb128 .L521-.LFB3750
	.uleb128 0x0
	.uleb128 .LEHB13-.LFB3750
	.uleb128 .LEHE13-.LEHB13
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE3750:
	.fnend
	.size	_ZN21QuColorableDrawObjectIiED0Ev, .-_ZN21QuColorableDrawObjectIiED0Ev
	.section	.text._ZN21QuColorableDrawObjectIiED1Ev,"axG",%progbits,_ZN21QuColorableDrawObjectIiED1Ev,comdat
	.align	2
	.weak	_ZN21QuColorableDrawObjectIiED1Ev
	.hidden	_ZN21QuColorableDrawObjectIiED1Ev
	.type	_ZN21QuColorableDrawObjectIiED1Ev, %function
_ZN21QuColorableDrawObjectIiED1Ev:
	.fnstart
.LFB3749:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	mov	r4, r0
	ldr	r0, [r0, #144]
	ldr	r3, .L536
	cmp	r0, #0
	str	r3, [r4, #0]
	bne	.L535
.L527:
	ldr	r0, [r4, #140]
	cmp	r0, #0
	blne	free
.L530:
	ldr	r2, .L536+4
	mov	r0, r4
	str	r2, [r4, #0]
.LEHB14:
	bl	_ZN12QuDrawObjectD2Ev
.LEHE14:
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L535:
	ldr	r2, [r4, #140]
	add	r0, r4, #140
	ldr	r1, [r2, #4]
.LEHB15:
	bl	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE8_M_eraseEPNS_13_Rb_tree_nodeIS5_EE
.LEHE15:
	ldr	r3, [r4, #140]
	str	r3, [r3, #8]
	ldr	lr, [r4, #140]
	mov	r1, #0
	str	r1, [lr, #4]
	ldr	ip, [r4, #140]
	str	ip, [ip, #12]
	str	r1, [r4, #144]
	b	.L527
.L533:
.L528:
	ldr	r3, [r4, #140]
	cmp	r3, #0
	mov	r5, r0
	movne	r0, r3
	blne	free
.L529:
.L534:
.L531:
	ldr	r0, .L536+4
	str	r0, [r4, #0]
	mov	r0, r4
	bl	_ZN12QuDrawObjectD2Ev
	mov	r0, r5
.LEHB16:
	bl	__cxa_end_cleanup
.LEHE16:
.L537:
	.align	2
.L536:
	.word	_ZTV21QuColorableDrawObjectIiE+8
	.word	_ZTV17QuImageDrawObject+8
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA3749:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3749-.LLSDACSB3749
.LLSDACSB3749:
	.uleb128 .LEHB14-.LFB3749
	.uleb128 .LEHE14-.LEHB14
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB15-.LFB3749
	.uleb128 .LEHE15-.LEHB15
	.uleb128 .L533-.LFB3749
	.uleb128 0x0
	.uleb128 .LEHB16-.LFB3749
	.uleb128 .LEHE16-.LEHB16
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE3749:
	.fnend
	.size	_ZN21QuColorableDrawObjectIiED1Ev, .-_ZN21QuColorableDrawObjectIiED1Ev
	.section	.text._ZN21QuColorableDrawObjectIiE7disableEv,"axG",%progbits,_ZN21QuColorableDrawObjectIiE7disableEv,comdat
	.align	2
	.weak	_ZN21QuColorableDrawObjectIiE7disableEv
	.hidden	_ZN21QuColorableDrawObjectIiE7disableEv
	.type	_ZN21QuColorableDrawObjectIiE7disableEv, %function
_ZN21QuColorableDrawObjectIiE7disableEv:
	.fnstart
.LFB5508:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 72
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	ldrb	r3, [r0, #120]	@ zero_extendqisi2
	cmp	r3, #0
	.pad #72
	sub	sp, sp, #72
	mov	r6, r0
	bne	.L566
.L539:
	ldrb	r0, [r6, #8]	@ zero_extendqisi2
	cmp	r0, #0
	bne	.L567
.L540:
	ldrb	r1, [r6, #36]	@ zero_extendqisi2
	cmp	r1, #0
	bne	.L568
.L541:
	ldrb	r2, [r6, #64]	@ zero_extendqisi2
	cmp	r2, #0
	bne	.L569
.L542:
	ldrb	ip, [r6, #92]	@ zero_extendqisi2
	cmp	ip, #0
	ldrne	r0, [r6, #112]
.LEHB17:
	blne	glDisableClientState
.LEHE17:
.L543:
	ldr	r3, [r6, #144]
	cmp	r3, #0
	bne	.L570
.L563:
	add	sp, sp, #72
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L570:
	ldr	r1, [r6, #140]
	ldr	ip, [r1, #4]
	cmp	ip, #0
	ldr	r5, [r6, #152]
	beq	.L545
	mov	r2, r1
	b	.L549
.L571:
	mov	r2, ip
	mov	ip, r3
.L549:
	ldr	r3, [ip, #16]
	cmp	r3, r5
	ldrlt	r3, [ip, #12]
	ldrge	r3, [ip, #8]
	movlt	ip, r2
	cmp	r3, #0
	bne	.L571
.L548:
	cmp	ip, r1
	beq	.L551
	ldr	r4, [ip, #16]
	cmp	r5, r4
	mov	r4, ip
	bge	.L552
.L551:
	add	r4, sp, #4
	mov	lr, #4
	add	r1, r6, #140
	add	r0, sp, #64
	mov	r6, #0
	add	r2, sp, #68
	mov	r3, r4
	str	ip, [sp, #68]
	str	r5, [sp, #4]
	strb	r6, [sp, #8]
	str	lr, [sp, #12]
	strb	r6, [sp, #36]
	str	lr, [sp, #40]
.LEHB18:
	bl	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS5_NS_16_Nonconst_traitsIS5_EEEERKS5_
.LEHE18:
	ldrb	r0, [sp, #8]	@ zero_extendqisi2
	cmp	r0, r6
	ldr	r4, [sp, #64]
	beq	.L555
	ldr	r0, [sp, #16]
	cmp	r0, r6
	blne	_ZdaPv
.L556:
	mov	r1, #0
	strb	r1, [sp, #8]
	str	r1, [sp, #16]
.L555:
	ldrb	r2, [sp, #36]	@ zero_extendqisi2
	cmp	r2, #0
	beq	.L552
	ldr	r0, [sp, #44]
	cmp	r0, #0
	blne	_ZdaPv
.L558:
	mov	ip, #0
	strb	ip, [sp, #36]
	str	ip, [sp, #44]
.L552:
	ldrb	r3, [r4, #20]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L563
	ldr	r0, [r4, #40]
.LEHB19:
	bl	glDisableClientState
	b	.L563
.L569:
	ldr	r0, [r6, #84]
	bl	glDisableClientState
	b	.L542
.L568:
	ldr	r0, [r6, #56]
	bl	glDisableClientState
	b	.L541
.L567:
	ldr	r0, [r6, #28]
	bl	glDisableClientState
	b	.L540
.L566:
	ldr	r0, [r0, #124]
	bl	_ZN11QuBaseImage6unbindEv
	b	.L539
.L545:
	mov	ip, r1
	b	.L548
.L564:
.L559:
.L565:
.L561:
	mov	r5, r0
	add	r0, r4, #4
	bl	_ZN14QuArrayPointerIfE6deInitEv
	add	r0, sp, #36
	bl	_ZN14QuArrayPointerIfE6deInitEv
	mov	r0, r5
	bl	__cxa_end_cleanup
.LEHE19:
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA5508:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5508-.LLSDACSB5508
.LLSDACSB5508:
	.uleb128 .LEHB17-.LFB5508
	.uleb128 .LEHE17-.LEHB17
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB18-.LFB5508
	.uleb128 .LEHE18-.LEHB18
	.uleb128 .L564-.LFB5508
	.uleb128 0x0
	.uleb128 .LEHB19-.LFB5508
	.uleb128 .LEHE19-.LEHB19
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE5508:
	.fnend
	.size	_ZN21QuColorableDrawObjectIiE7disableEv, .-_ZN21QuColorableDrawObjectIiE7disableEv
	.section	.text._ZN21QuColorableDrawObjectIiE6enableEv,"axG",%progbits,_ZN21QuColorableDrawObjectIiE6enableEv,comdat
	.align	2
	.weak	_ZN21QuColorableDrawObjectIiE6enableEv
	.hidden	_ZN21QuColorableDrawObjectIiE6enableEv
	.type	_ZN21QuColorableDrawObjectIiE6enableEv, %function
_ZN21QuColorableDrawObjectIiE6enableEv:
	.fnstart
.LFB5507:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 72
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	ldrb	r3, [r0, #120]	@ zero_extendqisi2
	cmp	r3, #0
	.pad #72
	sub	sp, sp, #72
	mov	r5, r0
	beq	.L573
	ldr	r2, [r0, #124]
	mov	r0, r2
	ldr	r1, [r2, #0]
.LEHB20:
	ldr	ip, [r1, #0]
	mov	lr, pc
	bx	ip
	cmp	r0, #0
	beq	.L574
.L573:
	ldrb	r0, [r5, #8]	@ zero_extendqisi2
	cmp	r0, #0
	bne	.L625
.L575:
	ldrb	r2, [r5, #36]	@ zero_extendqisi2
	cmp	r2, #0
	bne	.L626
.L581:
	ldrb	r2, [r5, #64]	@ zero_extendqisi2
	cmp	r2, #0
	bne	.L627
.L587:
	ldrb	r2, [r5, #92]	@ zero_extendqisi2
	cmp	r2, #0
	bne	.L628
.L574:
	ldr	r2, [r5, #144]
	cmp	r2, #0
	bne	.L629
.L622:
	add	sp, sp, #72
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L628:
	ldr	r0, [r5, #112]
	bl	glEnableClientState
	ldr	r0, [r5, #112]
	sub	r1, r0, #32768
	sub	ip, r1, #116
	cmp	ip, #4
	ldrls	pc, [pc, ip, asl #2]
	b	.L574
.L597:
	.word	.L593
	.word	.L594
	.word	.L595
	.word	.L574
	.word	.L596
.L627:
	ldr	r0, [r5, #84]
	bl	glEnableClientState
	ldr	r0, [r5, #84]
	sub	r1, r0, #32768
	sub	ip, r1, #116
	cmp	ip, #4
	ldrls	pc, [pc, ip, asl #2]
	b	.L587
.L592:
	.word	.L588
	.word	.L589
	.word	.L590
	.word	.L587
	.word	.L591
.L626:
	ldr	r0, [r5, #56]
	bl	glEnableClientState
	ldr	r3, [r5, #56]
	sub	r1, r3, #32768
	sub	ip, r1, #116
	cmp	ip, #4
	ldrls	pc, [pc, ip, asl #2]
	b	.L581
.L586:
	.word	.L582
	.word	.L583
	.word	.L584
	.word	.L581
	.word	.L585
.L625:
	ldr	r0, [r5, #28]
	bl	glEnableClientState
.LEHE20:
	ldr	r1, [r5, #28]
	sub	r3, r1, #32768
	sub	ip, r3, #116
	cmp	ip, #4
	ldrls	pc, [pc, ip, asl #2]
	b	.L575
.L580:
	.word	.L576
	.word	.L577
	.word	.L578
	.word	.L575
	.word	.L579
.L629:
	ldr	r1, [r5, #140]
	ldr	ip, [r1, #4]
	cmp	ip, #0
	ldr	r6, [r5, #152]
	beq	.L599
	mov	r2, r1
	b	.L603
.L630:
	mov	r2, ip
	mov	ip, r3
.L603:
	ldr	r3, [ip, #16]
	cmp	r3, r6
	ldrlt	r3, [ip, #12]
	ldrge	r3, [ip, #8]
	movlt	ip, r2
	cmp	r3, #0
	bne	.L630
.L602:
	cmp	ip, r1
	beq	.L605
	ldr	r4, [ip, #16]
	cmp	r6, r4
	mov	r4, ip
	bge	.L606
.L605:
	add	r4, sp, #4
	mov	lr, #4
	add	r1, r5, #140
	add	r0, sp, #64
	mov	r5, #0
	add	r2, sp, #68
	mov	r3, r4
	str	ip, [sp, #68]
	str	r6, [sp, #4]
	strb	r5, [sp, #8]
	str	lr, [sp, #12]
	strb	r5, [sp, #36]
	str	lr, [sp, #40]
.LEHB21:
	bl	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS5_NS_16_Nonconst_traitsIS5_EEEERKS5_
.LEHE21:
	ldrb	ip, [sp, #8]	@ zero_extendqisi2
	cmp	ip, r5
	ldr	r4, [sp, #64]
	beq	.L609
	ldr	r0, [sp, #16]
	cmp	r0, r5
	blne	_ZdaPv
.L610:
	mov	r1, #0
	strb	r1, [sp, #8]
	str	r1, [sp, #16]
.L609:
	ldrb	r0, [sp, #36]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L606
	ldr	r0, [sp, #44]
	cmp	r0, #0
	blne	_ZdaPv
.L612:
	mov	r3, #0
	strb	r3, [sp, #36]
	str	r3, [sp, #44]
.L606:
	ldrb	r2, [r4, #20]	@ zero_extendqisi2
	cmp	r2, #0
	beq	.L622
	ldr	r0, [r4, #40]
.LEHB22:
	bl	glEnableClientState
	ldr	r0, [r4, #40]
	sub	r1, r0, #32768
	sub	ip, r1, #116
	cmp	ip, #4
	ldrls	pc, [pc, ip, asl #2]
	b	.L622
.L621:
	.word	.L617
	.word	.L618
	.word	.L619
	.word	.L622
	.word	.L620
.L593:
	ldr	r1, [r5, #96]
	ldr	r0, [r5, #108]
	ldr	r2, [r5, #104]
	ldr	r3, [r5, #100]
	mov	lr, #5120
	mul	r2, r1, r2
	add	r3, r3, r0, asl #2
	add	r1, lr, #6
	ldr	r0, [r5, #116]
	bl	glVertexPointer
	b	.L574
.L579:
	ldr	r0, [r5, #24]
	ldr	r2, [r5, #20]
	ldr	ip, [r5, #12]
	ldr	r3, [r5, #16]
	mov	lr, #5120
	add	r3, r3, r0, asl #2
	mul	r2, ip, r2
	add	r1, lr, #6
	ldr	r0, [r5, #32]
	bl	glTexCoordPointer
	b	.L575
.L578:
	ldr	r0, [r5, #12]
	add	r2, r5, #20
	ldmia	r2, {r2, lr}	@ phole ldm
	ldr	r3, [r5, #16]
	mov	r1, #5120
	mul	r2, r0, r2
	add	r3, r3, lr, asl #2
	add	r1, r1, #6
	ldr	r0, [r5, #32]
	bl	glColorPointer
	b	.L575
.L577:
	add	r0, r5, #16
	ldmia	r0, {r0, r1}	@ phole ldm
	ldr	ip, [r5, #12]
	ldr	r2, [r5, #24]
	mov	lr, #5120
	add	r2, r0, r2, asl #2
	mul	r1, ip, r1
	add	r0, lr, #6
	bl	glNormalPointer
	b	.L575
.L588:
	ldr	r1, [r5, #68]
	ldr	r0, [r5, #80]
	ldr	r2, [r5, #76]
	ldr	r3, [r5, #72]
	mov	lr, #5120
	mul	r2, r1, r2
	add	r3, r3, r0, asl #2
	add	r1, lr, #6
	ldr	r0, [r5, #88]
	bl	glVertexPointer
	b	.L587
.L596:
	ldr	r1, [r5, #108]
	ldr	r2, [r5, #104]
	ldr	ip, [r5, #96]
	ldr	r3, [r5, #100]
	mov	lr, #5120
	add	r3, r3, r1, asl #2
	mul	r2, ip, r2
	add	r1, lr, #6
	ldr	r0, [r5, #116]
	bl	glTexCoordPointer
	b	.L574
.L595:
	ldr	r3, [r5, #96]
	ldr	r0, [r5, #108]
	ldr	r2, [r5, #104]
	ldr	lr, [r5, #100]
	mov	r1, #5120
	mul	r2, r3, r2
	add	r1, r1, #6
	add	r3, lr, r0, asl #2
	ldr	r0, [r5, #116]
	bl	glColorPointer
	b	.L574
.L594:
	ldr	r1, [r5, #104]
	ldr	ip, [r5, #96]
	ldr	r2, [r5, #108]
	ldr	r3, [r5, #100]
	mov	lr, #5120
	mul	r1, ip, r1
	add	r2, r3, r2, asl #2
	add	r0, lr, #6
	bl	glNormalPointer
	b	.L574
.L576:
	ldr	r3, [r5, #12]
	ldr	r0, [r5, #24]
	ldr	r2, [r5, #20]
	ldr	lr, [r5, #16]
	mov	r1, #5120
	mul	r2, r3, r2
	add	r1, r1, #6
	add	r3, lr, r0, asl #2
	ldr	r0, [r5, #32]
	bl	glVertexPointer
	b	.L575
.L585:
	ldr	r0, [r5, #52]
	add	r1, r5, #44
	ldmia	r1, {r1, r2}	@ phole ldm
	ldr	ip, [r5, #40]
	mov	lr, #5120
	add	r3, r1, r0, asl #2
	mul	r2, ip, r2
	add	r1, lr, #6
	ldr	r0, [r5, #60]
	bl	glTexCoordPointer
	b	.L581
.L584:
	ldr	r3, [r5, #40]
	ldr	r0, [r5, #52]
	ldr	r2, [r5, #48]
	ldr	lr, [r5, #44]
	mov	r1, #5120
	mul	r2, r3, r2
	add	r1, r1, #6
	add	r3, lr, r0, asl #2
	ldr	r0, [r5, #60]
	bl	glColorPointer
	b	.L581
.L583:
	ldr	r1, [r5, #48]
	ldr	ip, [r5, #40]
	ldr	r2, [r5, #52]
	ldr	r3, [r5, #44]
	mov	lr, #5120
	mul	r1, ip, r1
	add	r2, r3, r2, asl #2
	add	r0, lr, #6
	bl	glNormalPointer
	b	.L581
.L582:
	ldr	r0, [r5, #40]
	ldr	r1, [r5, #52]
	ldr	r2, [r5, #48]
	ldr	r3, [r5, #44]
	mov	lr, #5120
	mul	r2, r0, r2
	add	r3, r3, r1, asl #2
	ldr	r0, [r5, #60]
	add	r1, lr, #6
	bl	glVertexPointer
	b	.L581
.L591:
	ldr	r1, [r5, #80]
	ldr	r2, [r5, #76]
	ldr	ip, [r5, #68]
	ldr	r3, [r5, #72]
	mov	lr, #5120
	add	r3, r3, r1, asl #2
	mul	r2, ip, r2
	add	r1, lr, #6
	ldr	r0, [r5, #88]
	bl	glTexCoordPointer
	b	.L587
.L590:
	ldr	r3, [r5, #68]
	ldr	r0, [r5, #80]
	ldr	r2, [r5, #76]
	ldr	lr, [r5, #72]
	mov	r1, #5120
	mul	r2, r3, r2
	add	r1, r1, #6
	add	r3, lr, r0, asl #2
	ldr	r0, [r5, #88]
	bl	glColorPointer
	b	.L587
.L589:
	ldr	r1, [r5, #76]
	ldr	ip, [r5, #68]
	ldr	r2, [r5, #80]
	ldr	r3, [r5, #72]
	mov	lr, #5120
	mul	r1, ip, r1
	add	r2, r3, r2, asl #2
	add	r0, lr, #6
	bl	glNormalPointer
	b	.L587
.L620:
	ldr	r1, [r4, #36]
	ldr	r2, [r4, #32]
	ldr	ip, [r4, #24]
	ldr	r3, [r4, #28]
	mov	lr, #5120
	add	r3, r3, r1, asl #2
	mul	r2, ip, r2
	ldr	r0, [r4, #44]
	add	r1, lr, #6
	bl	glTexCoordPointer
	b	.L622
.L619:
	ldr	r3, [r4, #24]
	ldr	r0, [r4, #36]
	ldr	r2, [r4, #32]
	ldr	lr, [r4, #28]
	mov	r1, #5120
	mul	r2, r3, r2
	add	r1, r1, #6
	add	r3, lr, r0, asl #2
	ldr	r0, [r4, #44]
	bl	glColorPointer
	b	.L622
.L618:
	ldr	r3, [r4, #28]
	ldr	r1, [r4, #32]
	ldr	ip, [r4, #24]
	ldr	r2, [r4, #36]
	mov	lr, #5120
	mul	r1, ip, r1
	add	r2, r3, r2, asl #2
	add	r0, lr, #6
	bl	glNormalPointer
	b	.L622
.L617:
	ldr	r1, [r4, #24]
	ldr	r0, [r4, #36]
	ldr	r2, [r4, #32]
	ldr	r3, [r4, #28]
	mov	lr, #5120
	mul	r2, r1, r2
	add	r3, r3, r0, asl #2
	add	r1, lr, #6
	ldr	r0, [r4, #44]
	bl	glVertexPointer
	b	.L622
.L599:
	mov	ip, r1
	b	.L602
.L623:
.L613:
.L624:
.L615:
	mov	r5, r0
	add	r0, r4, #4
	bl	_ZN14QuArrayPointerIfE6deInitEv
	add	r0, sp, #36
	bl	_ZN14QuArrayPointerIfE6deInitEv
	mov	r0, r5
	bl	__cxa_end_cleanup
.LEHE22:
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA5507:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5507-.LLSDACSB5507
.LLSDACSB5507:
	.uleb128 .LEHB20-.LFB5507
	.uleb128 .LEHE20-.LEHB20
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB21-.LFB5507
	.uleb128 .LEHE21-.LEHB21
	.uleb128 .L623-.LFB5507
	.uleb128 0x0
	.uleb128 .LEHB22-.LFB5507
	.uleb128 .LEHE22-.LEHB22
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE5507:
	.fnend
	.size	_ZN21QuColorableDrawObjectIiE6enableEv, .-_ZN21QuColorableDrawObjectIiE6enableEv
	.section	.text._ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE,"axG",%progbits,_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE,comdat
	.align	2
	.weak	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
	.hidden	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
	.type	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE, %function
_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE:
	.fnstart
.LFB4809:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 64
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	subs	r8, r1, #0
	.pad #68
	sub	sp, sp, #68
	str	r0, [sp, #12]
	beq	.L751
	ldr	r7, .L788
.L755:
	ldr	r0, [sp, #12]
	ldr	r1, [r8, #12]
.LEHB23:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE23:
	ldr	r2, [r8, #8]
	ldr	ip, [r8, #28]
	str	r2, [sp, #4]
	ldr	r3, [r8, #32]
	cmp	ip, #0
	str	r3, [sp, #8]
	beq	.L633
	ldr	r0, [r7, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L637
	mov	r1, r0
	b	.L638
.L767:
	mov	r1, r3
	mov	r3, r2
.L638:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L767
.L637:
	cmp	r3, r0
	beq	.L640
	ldr	r0, [r3, #16]
	cmp	ip, r0
	mov	r2, r3
	bcs	.L641
.L640:
	str	ip, [sp, #32]
	add	r0, sp, #60
	mov	ip, #0
	add	r1, sp, #56
	add	r2, sp, #32
	str	r3, [sp, #56]
	str	ip, [sp, #36]
.LEHB24:
	bl	T.1130
	ldr	r2, [sp, #60]
.L641:
	ldr	lr, [r2, #20]
	sub	r1, lr, #1
	cmp	r1, #0
	str	r1, [r2, #20]
	beq	.L768
.L633:
	ldr	r0, [r8, #16]
	ldr	r3, [sp, #8]
	mov	ip, #0
	cmp	r0, #0
	str	r3, [r8, #32]
	str	ip, [r8, #28]
	blne	free
.L745:
	cmp	r8, #0
	movne	r0, r8
	blne	free
.L747:
	ldr	r8, [sp, #4]
	cmp	r8, #0
	bne	.L755
.L751:
	add	sp, sp, #68
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L768:
	ldr	r2, [r7, #0]
	ldr	r4, [r2, #4]
	cmp	r4, #0
	ldr	r1, [r8, #28]
	beq	.L644
	mov	r0, r2
	mov	r5, r4
	b	.L648
.L769:
	mov	r0, r5
	mov	r5, r3
.L648:
	ldr	r3, [r5, #16]
	cmp	r1, r3
	ldrhi	r3, [r5, #12]
	ldrls	r3, [r5, #8]
	movhi	r5, r0
	cmp	r3, #0
	bne	.L769
	mov	r0, r2
	b	.L652
.L770:
	mov	r0, r4
	mov	r4, r3
.L652:
	ldr	r3, [r4, #16]
	cmp	r1, r3
	ldrcs	r3, [r4, #12]
	ldrcc	r3, [r4, #8]
	movcs	r4, r0
	cmp	r3, #0
	bne	.L770
	cmp	r5, r4
	mov	r6, r5
	moveq	r5, r4
	beq	.L655
	mov	r0, r5
.L656:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r4, r0
	bne	.L656
	ldr	r2, [r7, #0]
	mov	r6, r4
.L655:
	ldr	r3, [r2, #8]
	cmp	r5, r3
	beq	.L771
.L658:
	cmp	r5, r6
	bne	.L760
	b	.L661
.L664:
	mov	r5, r4
.L760:
	mov	r0, r5
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r2, [r7, #0]
	mov	r4, r0
	add	r3, r2, #12
	add	r1, r2, #4
	mov	r0, r5
	add	r2, r2, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
.LEHE24:
	cmp	r0, #0
	blne	free
.L663:
	ldr	r0, [r7, #4]
	cmp	r4, r6
	sub	r3, r0, #1
	str	r3, [r7, #4]
	bne	.L664
.L661:
	ldr	ip, [r8, #32]
	cmp	ip, #1
	beq	.L772
	ldr	r3, [r8, #28]
	cmp	r3, #0
	beq	.L633
	ldr	r0, [r3, #-4]
	add	r6, r0, r0, asl #2
	add	r6, r3, r6, asl #2
	cmp	r6, r3
	beq	.L706
.L775:
	ldr	ip, [r6, #-8]
	cmp	ip, #0
	sub	r9, r6, #20
	sub	sl, r6, #8
	ldr	fp, [r6, #-4]
	beq	.L707
	ldr	r0, [r7, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L711
	mov	r1, r0
	b	.L712
.L773:
	mov	r1, r3
	mov	r3, r2
.L712:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L773
.L711:
	cmp	r3, r0
	beq	.L714
	ldr	r2, [r3, #16]
	cmp	ip, r2
	mov	r2, r3
	bcs	.L715
.L714:
	str	r3, [sp, #40]
	add	r0, sp, #44
	mov	r3, #0
	add	r1, sp, #40
	add	r2, sp, #16
	str	ip, [sp, #16]
	str	r3, [sp, #20]
.LEHB25:
	bl	T.1130
	ldr	r2, [sp, #44]
.L715:
	ldr	r1, [r2, #20]
	sub	ip, r1, #1
	cmp	ip, #0
	str	ip, [r2, #20]
	beq	.L774
.L707:
	ldr	r0, [r6, #-20]
	mov	r2, #0
	cmp	r0, #0
	stmdb	r6, {r2, fp}	@ phole stm
	blne	free
.L741:
	ldr	r3, [r8, #28]
	mov	r6, r9
	cmp	r6, r3
	bne	.L775
.L706:
	sub	r0, r6, #8
	bl	_ZdaPv
	b	.L633
.L774:
	ldr	r2, [r7, #0]
	ldr	r4, [r2, #4]
	cmp	r4, #0
	ldr	r1, [r6, #-8]
	beq	.L716
	mov	r0, r2
	mov	r5, r4
	b	.L720
.L776:
	mov	r0, r5
	mov	r5, r3
.L720:
	ldr	r3, [r5, #16]
	cmp	r1, r3
	ldrhi	r3, [r5, #12]
	ldrls	r3, [r5, #8]
	movhi	r5, r0
	cmp	r3, #0
	bne	.L776
	mov	r0, r2
	b	.L724
.L777:
	mov	r0, r4
	mov	r4, r3
.L724:
	ldr	r3, [r4, #16]
	cmp	r1, r3
	ldrcs	r3, [r4, #12]
	ldrcc	r3, [r4, #8]
	movcs	r4, r0
	cmp	r3, #0
	bne	.L777
	cmp	r5, r4
	str	r5, [sp, #0]
	moveq	r5, r4
	beq	.L727
	mov	r0, r5
.L728:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r4, r0
	bne	.L728
	ldr	r2, [r7, #0]
	str	r4, [sp, #0]
.L727:
	ldr	lr, [r2, #8]
	cmp	r5, lr
	beq	.L778
.L730:
	ldr	r3, [sp, #0]
	cmp	r5, r3
	bne	.L762
	b	.L733
.L736:
	mov	r5, r4
.L762:
	mov	r0, r5
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	ip, [r7, #0]
	mov	r4, r0
	add	r3, ip, #12
	mov	r0, r5
	add	r1, ip, #4
	add	r2, ip, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L735:
	ldr	r1, [r7, #4]
	ldr	lr, [sp, #0]
	sub	r3, r1, #1
	cmp	r4, lr
	str	r3, [r7, #4]
	bne	.L736
.L733:
	ldr	r0, [r6, #-4]
	cmp	r0, #1
	ldr	r3, [r6, #-8]
	beq	.L779
	cmp	r3, #0
	beq	.L707
	ldr	ip, [r3, #-4]
	rsb	r4, ip, ip, asl #3
	add	r4, r3, r4, asl #2
	b	.L739
.L780:
	ldr	r1, [r4, #-28]!
	mov	r0, r4
	ldr	ip, [r1, #4]
	mov	lr, pc
	bx	ip
.L739:
	ldr	r3, [sl, #0]
	cmp	r3, r4
	bne	.L780
	ldr	lr, [r6, #-8]
	sub	r0, lr, #8
	bl	_ZdaPv
	b	.L707
.L779:
	cmp	r3, #0
	beq	.L707
	mov	r0, r3
	ldr	r2, [r3, #0]
	ldr	ip, [r2, #8]
	mov	lr, pc
	bx	ip
	b	.L707
.L778:
	ldr	r0, [sp, #0]
	cmp	r0, r2
	bne	.L730
	ldr	r2, [r7, #4]
	cmp	r2, #0
	beq	.L733
	ldr	ip, [sp, #0]
	ldr	r0, [ip, #4]
	bl	T.1125
.LEHE25:
	ldr	r2, [r7, #0]
	str	r2, [r2, #8]
	ldr	r0, [r7, #0]
	mov	r1, #0
	str	r1, [r0, #4]
	ldr	lr, [r7, #0]
	str	lr, [lr, #12]
	str	r1, [r7, #4]
	b	.L733
.L772:
	ldr	r5, [r8, #28]
	cmp	r5, #0
	beq	.L633
	ldr	ip, [r5, #12]
	cmp	ip, #0
	add	r9, r5, #12
	ldr	fp, [r5, #16]
	beq	.L666
	ldr	r0, [r7, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L670
	mov	r1, r0
	b	.L671
.L781:
	mov	r1, r3
	mov	r3, r2
.L671:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L781
.L670:
	cmp	r3, r0
	beq	.L673
	ldr	r1, [r3, #16]
	cmp	ip, r1
	mov	r2, r3
	bcs	.L674
.L673:
	mov	lr, #0
	add	r0, sp, #52
	add	r1, sp, #48
	add	r2, sp, #24
	str	ip, [sp, #24]
	str	r3, [sp, #48]
	str	lr, [sp, #28]
.LEHB26:
	bl	T.1130
.LEHE26:
	ldr	r2, [sp, #52]
.L674:
	ldr	r0, [r2, #20]
	sub	r3, r0, #1
	cmp	r3, #0
	str	r3, [r2, #20]
	beq	.L782
.L666:
	ldr	r0, [r5, #0]
	mov	lr, #0
	cmp	r0, #0
	str	fp, [r5, #16]
	str	lr, [r5, #12]
	blne	free
.L702:
	mov	r0, r5
	bl	_ZdlPv
	b	.L633
.L716:
	str	r2, [sp, #0]
	mov	r5, r2
	b	.L727
.L771:
	cmp	r6, r2
	bne	.L658
	ldr	r2, [r7, #4]
	cmp	r2, #0
	beq	.L661
	ldr	r0, [r6, #4]
.LEHB27:
	bl	T.1125
.LEHE27:
	ldr	lr, [r7, #0]
	str	lr, [lr, #8]
	ldr	r1, [r7, #0]
	mov	r0, #0
	str	r0, [r1, #4]
	ldr	ip, [r7, #0]
	str	ip, [ip, #12]
	str	r0, [r7, #4]
	b	.L661
.L644:
	mov	r6, r2
	mov	r5, r2
	b	.L655
.L782:
	ldr	r2, [r7, #0]
	ldr	r4, [r2, #4]
	cmp	r4, #0
	ldr	r0, [r5, #12]
	beq	.L677
	mov	ip, r2
	mov	r3, r4
	b	.L681
.L783:
	mov	ip, r3
	mov	r3, r1
.L681:
	ldr	r1, [r3, #16]
	cmp	r0, r1
	ldrhi	r1, [r3, #12]
	ldrls	r1, [r3, #8]
	movhi	r3, ip
	cmp	r1, #0
	bne	.L783
	mov	ip, r2
	b	.L685
.L784:
	mov	ip, r4
	mov	r4, r1
.L685:
	ldr	r1, [r4, #16]
	cmp	r0, r1
	ldrcs	r1, [r4, #12]
	ldrcc	r1, [r4, #8]
	movcs	r4, ip
	cmp	r1, #0
	bne	.L784
	cmp	r3, r4
	mov	r6, r3
	mov	sl, r4
	beq	.L688
	mov	r0, r3
.L689:
.LEHB28:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r4, r0
	bne	.L689
	ldr	r2, [r7, #0]
.L688:
	ldr	ip, [r2, #8]
	cmp	r6, ip
	beq	.L785
.L691:
	cmp	r6, sl
	bne	.L761
	b	.L694
.L697:
	mov	r6, r4
.L761:
	mov	r0, r6
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	ip, [r7, #0]
	mov	r4, r0
	add	r3, ip, #12
	mov	r0, r6
	add	r1, ip, #4
	add	r2, ip, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L696:
	ldr	r1, [r7, #4]
	cmp	r4, sl
	sub	r2, r1, #1
	str	r2, [r7, #4]
	bne	.L697
.L694:
	ldr	lr, [r5, #16]
	cmp	lr, #1
	ldr	r3, [r5, #12]
	beq	.L786
	cmp	r3, #0
	beq	.L666
	ldr	r0, [r3, #-4]
	mov	r4, #28
	mla	r4, r0, r4, r3
	b	.L700
.L787:
	ldr	r2, [r4, #-28]!
	mov	r0, r4
	ldr	ip, [r2, #4]
	mov	lr, pc
	bx	ip
.L700:
	ldr	ip, [r9, #0]
	cmp	ip, r4
	bne	.L787
	ldr	r1, [r5, #12]
	sub	r0, r1, #8
	bl	_ZdaPv
	b	.L666
.L785:
	cmp	sl, r2
	bne	.L691
	ldr	r2, [r7, #4]
	cmp	r2, #0
	beq	.L694
	ldr	r0, [sl, #4]
	bl	T.1125
	ldr	r0, [r7, #0]
	str	r0, [r0, #8]
	ldr	r3, [r7, #0]
	mov	r1, #0
	str	r1, [r3, #4]
	ldr	lr, [r7, #0]
	str	lr, [lr, #12]
	str	r1, [r7, #4]
	b	.L694
.L786:
	cmp	r3, #0
	beq	.L666
	mov	r0, r3
	ldr	r3, [r3, #0]
	ldr	ip, [r3, #8]
	mov	lr, pc
	bx	ip
.LEHE28:
	b	.L666
.L754:
	mov	r4, r0
.L748:
	ldr	r0, [r8, #16]
	cmp	r0, #0
	blne	free
.L749:
	mov	r0, r4
.LEHB29:
	bl	__cxa_end_cleanup
.LEHE29:
.L752:
.L742:
	ldr	r3, [r9, #0]
	cmp	r3, #0
	mov	r4, r0
	beq	.L748
.L766:
	mov	r0, r3
	bl	free
	b	.L748
.L753:
.L703:
	ldr	r3, [r5, #0]
	cmp	r3, #0
	mov	r4, r0
	bne	.L766
	b	.L748
.L677:
	mov	sl, r2
	mov	r6, r2
	b	.L688
.L789:
	.align	2
.L788:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA4809:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4809-.LLSDACSB4809
.LLSDACSB4809:
	.uleb128 .LEHB23-.LFB4809
	.uleb128 .LEHE23-.LEHB23
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB24-.LFB4809
	.uleb128 .LEHE24-.LEHB24
	.uleb128 .L754-.LFB4809
	.uleb128 0x0
	.uleb128 .LEHB25-.LFB4809
	.uleb128 .LEHE25-.LEHB25
	.uleb128 .L752-.LFB4809
	.uleb128 0x0
	.uleb128 .LEHB26-.LFB4809
	.uleb128 .LEHE26-.LEHB26
	.uleb128 .L753-.LFB4809
	.uleb128 0x0
	.uleb128 .LEHB27-.LFB4809
	.uleb128 .LEHE27-.LEHB27
	.uleb128 .L754-.LFB4809
	.uleb128 0x0
	.uleb128 .LEHB28-.LFB4809
	.uleb128 .LEHE28-.LEHB28
	.uleb128 .L753-.LFB4809
	.uleb128 0x0
	.uleb128 .LEHB29-.LFB4809
	.uleb128 .LEHE29-.LEHB29
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE4809:
	.fnend
	.size	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE, .-_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
	.section	.text._ZN14QuImageManagerD1Ev,"axG",%progbits,_ZN14QuImageManagerD1Ev,comdat
	.align	2
	.weak	_ZN14QuImageManagerD1Ev
	.hidden	_ZN14QuImageManagerD1Ev
	.type	_ZN14QuImageManagerD1Ev, %function
_ZN14QuImageManagerD1Ev:
	.fnstart
.LFB3192:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	ldr	r3, [r0, #28]
	cmp	r3, #0
	mov	r4, r0
	bne	.L813
.L791:
	ldr	r0, [r4, #24]
	cmp	r0, #0
	blne	free
.L794:
	ldr	r3, [r4, #16]
	cmp	r3, #0
	bne	.L814
.L796:
	ldr	r0, [r4, #12]
	cmp	r0, #0
	blne	free
.L800:
	ldr	r3, [r4, #4]
	cmp	r3, #0
	bne	.L815
.L802:
	ldr	r0, [r4, #0]
	cmp	r0, #0
	blne	free
.L806:
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L813:
	ldr	r0, [r0, #24]
	ldr	r1, [r0, #4]
	add	r0, r4, #24
.LEHB30:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE30:
	ldr	lr, [r4, #24]
	str	lr, [lr, #8]
	ldr	ip, [r4, #24]
	mov	r1, #0
	str	r1, [ip, #4]
	ldr	r2, [r4, #24]
	str	r2, [r2, #12]
	str	r1, [r4, #28]
	b	.L791
.L815:
	ldr	r0, [r4, #0]
	ldr	r1, [r0, #4]
	mov	r0, r4
.LEHB31:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE31:
	ldr	lr, [r4, #0]
	str	lr, [lr, #8]
	ldr	ip, [r4, #0]
	mov	r1, #0
	str	r1, [ip, #4]
	ldr	r2, [r4, #0]
	str	r2, [r2, #12]
	str	r1, [r4, #4]
	b	.L802
.L814:
	ldr	r0, [r4, #12]
	ldr	r1, [r0, #4]
	add	r0, r4, #12
.LEHB32:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE32:
	ldr	lr, [r4, #12]
	str	lr, [lr, #8]
	ldr	ip, [r4, #12]
	mov	r1, #0
	str	r1, [ip, #4]
	ldr	r2, [r4, #12]
	str	r2, [r2, #12]
	str	r1, [r4, #16]
	b	.L796
.L810:
.L792:
	ldr	r3, [r4, #24]
	cmp	r3, #0
	mov	r5, r0
	movne	r0, r3
	blne	free
.L793:
.L811:
.L797:
	add	r0, r4, #12
	bl	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev
.L812:
.L803:
	mov	r0, r4
	bl	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev
	mov	r0, r5
.LEHB33:
	bl	__cxa_end_cleanup
.L808:
.L804:
	ldr	r3, [r4, #0]
	cmp	r3, #0
	mov	r4, r0
	movne	r0, r3
	blne	free
.L805:
	mov	r0, r4
	bl	__cxa_end_cleanup
.LEHE33:
.L809:
.L798:
	ldr	r3, [r4, #12]
	cmp	r3, #0
	mov	r5, r0
	beq	.L803
	mov	r0, r3
	bl	free
	b	.L803
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA3192:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3192-.LLSDACSB3192
.LLSDACSB3192:
	.uleb128 .LEHB30-.LFB3192
	.uleb128 .LEHE30-.LEHB30
	.uleb128 .L810-.LFB3192
	.uleb128 0x0
	.uleb128 .LEHB31-.LFB3192
	.uleb128 .LEHE31-.LEHB31
	.uleb128 .L808-.LFB3192
	.uleb128 0x0
	.uleb128 .LEHB32-.LFB3192
	.uleb128 .LEHE32-.LEHB32
	.uleb128 .L809-.LFB3192
	.uleb128 0x0
	.uleb128 .LEHB33-.LFB3192
	.uleb128 .LEHE33-.LEHB33
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE3192:
	.fnend
	.size	_ZN14QuImageManagerD1Ev, .-_ZN14QuImageManagerD1Ev
	.section	.text.T.1129,"ax",%progbits
	.align	2
	.type	T.1129, %function
T.1129:
	.fnstart
.LFB5518:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	.save {r4, r5, r6, r7, r8, sl, lr}
	ldr	r3, [r0, #0]
	cmp	r3, #0
	.pad #20
	sub	sp, sp, #20
	mov	r4, r0
	mov	r7, r1
	beq	.L817
	ldr	r5, .L861
	ldr	r1, [r5, #0]
	ldr	r2, [r1, #4]
	cmp	r2, #0
	mov	ip, r1
	beq	.L819
.L822:
	ldr	r0, [r2, #16]
	cmp	r3, r0
	movls	ip, r2
	ldrhi	r2, [r2, #12]
	ldrls	r2, [r2, #8]
	cmp	r2, #0
	bne	.L822
.L819:
	cmp	ip, r1
	beq	.L824
	ldr	r2, [ip, #16]
	cmp	r3, r2
	mov	r2, ip
	bcs	.L825
.L824:
	mov	r2, sp
	str	r3, [sp, #0]
	add	r0, sp, #12
	mov	r3, #0
	add	r1, sp, #8
	stmib	sp, {r3, ip}	@ phole stm
	bl	T.1130
	ldr	r2, [sp, #12]
.L825:
	ldr	ip, [r2, #20]
	sub	r1, ip, #1
	cmp	r1, #0
	str	r1, [r2, #20]
	beq	.L855
.L817:
	mov	r3, #0
	stmia	r4, {r3, r7}	@ phole stm
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	bx	lr
.L855:
	ldr	r1, [r5, #0]
	ldr	r6, [r1, #4]
	cmp	r6, #0
	ldr	r2, [r4, #0]
	moveq	sl, r1
	moveq	r8, r1
	beq	.L837
	mov	sl, r6
	ldr	r3, [sl, #16]
	cmp	r2, r3
	ldrhi	r3, [sl, #12]
	ldrls	r3, [sl, #8]
	mov	r0, r1
	movhi	sl, r0
	cmp	r3, #0
	beq	.L829
.L856:
	mov	r0, sl
	mov	sl, r3
	ldr	r3, [sl, #16]
	cmp	r2, r3
	ldrhi	r3, [sl, #12]
	ldrls	r3, [sl, #8]
	movhi	sl, r0
	cmp	r3, #0
	bne	.L856
.L829:
	ldr	r3, [r6, #16]
	cmp	r2, r3
	ldrcs	r3, [r6, #12]
	ldrcc	r3, [r6, #8]
	mov	r0, r1
	movcs	r6, r0
	cmp	r3, #0
	beq	.L833
.L857:
	mov	r0, r6
	mov	r6, r3
	ldr	r3, [r6, #16]
	cmp	r2, r3
	ldrcs	r3, [r6, #12]
	ldrcc	r3, [r6, #8]
	movcs	r6, r0
	cmp	r3, #0
	bne	.L857
.L833:
	cmp	sl, r6
	mov	r8, sl
	moveq	sl, r6
	beq	.L837
	mov	r0, sl
.L838:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r6, r0
	bne	.L838
	ldr	r1, [r5, #0]
	mov	r8, r6
.L837:
	ldr	r0, [r1, #8]
	cmp	sl, r0
	beq	.L858
.L840:
	cmp	sl, r8
	bne	.L853
	b	.L843
.L846:
	mov	sl, r6
.L853:
	mov	r0, sl
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r2, [r5, #0]
	mov	r6, r0
	add	r3, r2, #12
	add	r1, r2, #4
	mov	r0, sl
	add	r2, r2, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L845:
	ldr	r3, [r5, #4]
	cmp	r6, r8
	sub	lr, r3, #1
	str	lr, [r5, #4]
	bne	.L846
.L843:
	ldr	r1, [r4, #4]
	cmp	r1, #1
	ldr	r3, [r4, #0]
	beq	.L859
	cmp	r3, #0
	beq	.L817
	ldr	r0, [r3, #-4]
	add	r0, r3, r0, asl #2
	cmp	r3, r0
	beq	.L848
	b	.L854
.L860:
	mov	r0, r5
.L854:
	sub	r5, r0, #4
	ldr	r2, [r0, #-4]
	mov	r0, r5
	ldr	ip, [r2, #0]
	mov	lr, pc
	bx	ip
	ldr	r0, [r4, #0]
	cmp	r0, r5
	bne	.L860
.L848:
	sub	r0, r0, #8
	bl	_ZdaPv
	b	.L817
.L859:
	cmp	r3, #0
	beq	.L817
	mov	r0, r3
	ldr	ip, [r3, #0]
	ldr	ip, [ip, #4]
	mov	lr, pc
	bx	ip
	b	.L817
.L858:
	cmp	r8, r1
	bne	.L840
	ldr	r2, [r5, #4]
	cmp	r2, #0
	ldr	r5, .L861
	beq	.L843
	ldr	r0, [r8, #4]
	bl	T.1125
	ldr	r0, [r5, #0]
	str	r0, [r0, #8]
	ldr	ip, [r5, #0]
	mov	r3, #0
	str	r3, [ip, #4]
	ldr	r1, [r5, #0]
	str	r1, [r1, #12]
	str	r3, [r5, #4]
	b	.L843
.L862:
	.align	2
.L861:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	T.1129, .-T.1129
	.section	.text.T.1131,"ax",%progbits
	.align	2
	.type	T.1131, %function
T.1131:
	.fnstart
.LFB5520:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	.save {r4, r5, r6, r7, r8, sl, lr}
	ldr	r3, [r0, #0]
	cmp	r3, #0
	.pad #20
	sub	sp, sp, #20
	mov	r5, r0
	mov	r8, r1
	beq	.L864
	ldr	r4, .L928
	ldr	r1, [r4, #0]
	ldr	r2, [r1, #4]
	cmp	r2, #0
	mov	ip, r1
	beq	.L866
.L869:
	ldr	r0, [r2, #16]
	cmp	r3, r0
	movls	ip, r2
	ldrhi	r2, [r2, #12]
	ldrls	r2, [r2, #8]
	cmp	r2, #0
	bne	.L869
.L866:
	cmp	ip, r1
	beq	.L871
	ldr	r2, [ip, #16]
	cmp	r3, r2
	mov	r2, ip
	bcs	.L872
.L871:
	mov	r2, sp
	str	r3, [sp, #0]
	add	r0, sp, #12
	mov	r3, #0
	add	r1, sp, #8
	stmib	sp, {r3, ip}	@ phole stm
.LEHB34:
	bl	T.1130
	ldr	r2, [sp, #12]
.L872:
	ldr	ip, [r2, #20]
	sub	r1, ip, #1
	cmp	r1, #0
	str	r1, [r2, #20]
	beq	.L922
.L864:
	mov	r2, #0
	stmia	r5, {r2, r8}	@ phole stm
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	bx	lr
.L922:
	ldr	r1, [r4, #0]
	ldr	r6, [r1, #4]
	cmp	r6, #0
	ldr	r2, [r5, #0]
	moveq	sl, r1
	moveq	r7, r1
	beq	.L884
	mov	sl, r6
	ldr	r3, [sl, #16]
	cmp	r2, r3
	ldrhi	r3, [sl, #12]
	ldrls	r3, [sl, #8]
	mov	r0, r1
	movhi	sl, r0
	cmp	r3, #0
	beq	.L876
.L923:
	mov	r0, sl
	mov	sl, r3
	ldr	r3, [sl, #16]
	cmp	r2, r3
	ldrhi	r3, [sl, #12]
	ldrls	r3, [sl, #8]
	movhi	sl, r0
	cmp	r3, #0
	bne	.L923
.L876:
	ldr	r3, [r6, #16]
	cmp	r2, r3
	ldrcs	r3, [r6, #12]
	ldrcc	r3, [r6, #8]
	mov	r0, r1
	movcs	r6, r0
	cmp	r3, #0
	beq	.L880
.L924:
	mov	r0, r6
	mov	r6, r3
	ldr	r3, [r6, #16]
	cmp	r2, r3
	ldrcs	r3, [r6, #12]
	ldrcc	r3, [r6, #8]
	movcs	r6, r0
	cmp	r3, #0
	bne	.L924
.L880:
	cmp	sl, r6
	mov	r7, sl
	moveq	sl, r6
	beq	.L884
	mov	r0, sl
.L885:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r6, r0
	bne	.L885
	ldr	r1, [r4, #0]
	mov	r7, r6
.L884:
	ldr	r0, [r1, #8]
	cmp	sl, r0
	beq	.L925
.L887:
	cmp	sl, r7
	bne	.L921
	b	.L890
.L893:
	mov	sl, r6
.L921:
	mov	r0, sl
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r2, [r4, #0]
	mov	r6, r0
	add	r3, r2, #12
	add	r1, r2, #4
	mov	r0, sl
	add	r2, r2, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
.LEHE34:
	cmp	r0, #0
	blne	free
.L892:
	ldr	r3, [r4, #4]
	cmp	r6, r7
	sub	lr, r3, #1
	str	lr, [r4, #4]
	bne	.L893
.L890:
	ldr	r1, [r5, #4]
	cmp	r1, #1
	beq	.L926
	ldr	r3, [r5, #0]
	cmp	r3, #0
	beq	.L864
	ldr	r7, [r3, #-4]
	add	r4, r7, r7, asl #3
	add	r4, r3, r4, asl #2
	mov	r7, #0
	b	.L895
.L897:
	ldr	r0, [r4, #-12]
	cmp	r0, #0
	blne	free
.L900:
	ldr	r3, [r4, #-20]
	cmp	r3, #0
	add	r0, r6, #12
	bne	.L927
.L902:
	ldr	r0, [r4, #-24]
	cmp	r0, #0
	blne	free
.L906:
	ldr	lr, [r4, #-32]
	cmp	lr, #0
	bne	.L907
.L908:
	ldr	r0, [r4, #-36]
	cmp	r0, #0
	blne	free
.L912:
	ldr	r3, [r5, #0]
	mov	r4, r6
.L895:
	cmp	r4, r3
	beq	.L896
	ldr	r6, [r4, #-8]
	cmp	r6, #0
	sub	r0, r4, #12
	sub	r6, r4, #36
	beq	.L897
	ldr	ip, [r4, #-12]
	ldr	r1, [ip, #4]
.LEHB35:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE35:
	ldr	lr, [r4, #-12]
	str	lr, [lr, #8]
	ldr	r2, [r4, #-12]
	str	r7, [r2, #4]
	ldr	r0, [r4, #-12]
	str	r0, [r0, #12]
	str	r7, [r4, #-8]
	b	.L897
.L907:
	ldr	r3, [r4, #-36]
	mov	r0, r6
	ldr	r1, [r3, #4]
.LEHB36:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE36:
	ldr	r0, [r4, #-36]
	str	r0, [r0, #8]
	ldr	r1, [r4, #-36]
	str	r7, [r1, #4]
	ldr	ip, [r4, #-36]
	str	ip, [ip, #12]
	str	r7, [r4, #-32]
	b	.L908
.L927:
	ldr	ip, [r4, #-24]
	ldr	r1, [ip, #4]
.LEHB37:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE37:
	ldr	r2, [r4, #-24]
	str	r2, [r2, #8]
	ldr	r0, [r4, #-24]
	str	r7, [r0, #4]
	ldr	r1, [r4, #-24]
	str	r1, [r1, #12]
	str	r7, [r4, #-20]
	b	.L902
.L896:
	sub	r0, r4, #8
	bl	_ZdaPv
	b	.L864
.L926:
	ldr	r4, [r5, #0]
	cmp	r4, #0
	beq	.L864
	mov	r0, r4
.LEHB38:
	bl	_ZN14QuImageManagerD1Ev
	mov	r0, r4
	bl	_ZdlPv
	b	.L864
.L925:
	cmp	r7, r1
	bne	.L887
	ldr	r2, [r4, #4]
	cmp	r2, #0
	ldr	r4, .L928
	beq	.L890
	ldr	r0, [r7, #4]
	bl	T.1125
.LEHE38:
	ldr	r0, [r4, #0]
	str	r0, [r0, #8]
	ldr	ip, [r4, #0]
	mov	r3, #0
	str	r3, [ip, #4]
	ldr	r1, [r4, #0]
	str	r1, [r1, #12]
	str	r3, [r4, #4]
	b	.L890
.L916:
.L898:
	ldr	r3, [r6, #24]
	cmp	r3, #0
	mov	r4, r0
	movne	r0, r3
	blne	free
.L899:
.L917:
.L903:
	add	r0, r6, #12
	bl	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev
.L918:
.L909:
	mov	r0, r6
	bl	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev
	mov	r0, r4
.LEHB39:
	bl	__cxa_end_cleanup
.L915:
.L904:
	ldr	r3, [r6, #12]
	cmp	r3, #0
	mov	r4, r0
	beq	.L909
	mov	r0, r3
	bl	free
	b	.L909
.L914:
.L910:
	ldr	r3, [r6, #0]
	cmp	r3, #0
	mov	r4, r0
	movne	r0, r3
	blne	free
.L911:
	mov	r0, r4
	bl	__cxa_end_cleanup
.LEHE39:
.L929:
	.align	2
.L928:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA5520:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5520-.LLSDACSB5520
.LLSDACSB5520:
	.uleb128 .LEHB34-.LFB5520
	.uleb128 .LEHE34-.LEHB34
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB35-.LFB5520
	.uleb128 .LEHE35-.LEHB35
	.uleb128 .L916-.LFB5520
	.uleb128 0x0
	.uleb128 .LEHB36-.LFB5520
	.uleb128 .LEHE36-.LEHB36
	.uleb128 .L914-.LFB5520
	.uleb128 0x0
	.uleb128 .LEHB37-.LFB5520
	.uleb128 .LEHE37-.LEHB37
	.uleb128 .L915-.LFB5520
	.uleb128 0x0
	.uleb128 .LEHB38-.LFB5520
	.uleb128 .LEHE38-.LEHB38
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB39-.LFB5520
	.uleb128 .LEHE39-.LEHB39
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE5520:
	.fnend
	.size	T.1131, .-T.1131
	.section	.text.T.1132,"ax",%progbits
	.align	2
	.type	T.1132, %function
T.1132:
	.fnstart
.LFB5521:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	.save {r4, r5, r6, r7, r8, lr}
	ldr	r3, [r0, #0]
	cmp	r3, #0
	.pad #16
	sub	sp, sp, #16
	mov	r4, r0
	beq	.L931
	ldr	r5, .L975
	ldr	r1, [r5, #0]
	ldr	r2, [r1, #4]
	cmp	r2, #0
	mov	ip, r1
	beq	.L933
.L936:
	ldr	r0, [r2, #16]
	cmp	r3, r0
	movls	ip, r2
	ldrhi	r2, [r2, #12]
	ldrls	r2, [r2, #8]
	cmp	r2, #0
	bne	.L936
.L933:
	cmp	ip, r1
	beq	.L938
	ldr	r2, [ip, #16]
	cmp	r3, r2
	mov	r2, ip
	bcs	.L939
.L938:
	mov	r2, sp
	str	r3, [sp, #0]
	add	r0, sp, #12
	mov	r3, #0
	add	r1, sp, #8
	stmib	sp, {r3, ip}	@ phole stm
	bl	T.1130
	ldr	r2, [sp, #12]
.L939:
	ldr	ip, [r2, #20]
	sub	r1, ip, #1
	cmp	r1, #0
	str	r1, [r2, #20]
	beq	.L969
.L931:
	mov	r1, #1
	mov	r3, #0
	str	r1, [r4, #4]
	str	r3, [r4, #0]
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L969:
	ldr	r1, [r5, #0]
	ldr	r6, [r1, #4]
	cmp	r6, #0
	ldr	r2, [r4, #0]
	moveq	r8, r1
	moveq	r7, r1
	beq	.L951
	mov	r8, r6
	ldr	r3, [r8, #16]
	cmp	r2, r3
	ldrhi	r3, [r8, #12]
	ldrls	r3, [r8, #8]
	mov	r0, r1
	movhi	r8, r0
	cmp	r3, #0
	beq	.L943
.L970:
	mov	r0, r8
	mov	r8, r3
	ldr	r3, [r8, #16]
	cmp	r2, r3
	ldrhi	r3, [r8, #12]
	ldrls	r3, [r8, #8]
	movhi	r8, r0
	cmp	r3, #0
	bne	.L970
.L943:
	ldr	r3, [r6, #16]
	cmp	r2, r3
	ldrcs	r3, [r6, #12]
	ldrcc	r3, [r6, #8]
	mov	r0, r1
	movcs	r6, r0
	cmp	r3, #0
	beq	.L947
.L971:
	mov	r0, r6
	mov	r6, r3
	ldr	r3, [r6, #16]
	cmp	r2, r3
	ldrcs	r3, [r6, #12]
	ldrcc	r3, [r6, #8]
	movcs	r6, r0
	cmp	r3, #0
	bne	.L971
.L947:
	cmp	r8, r6
	mov	r7, r8
	moveq	r8, r6
	beq	.L951
	mov	r0, r8
.L952:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r6, r0
	bne	.L952
	ldr	r1, [r5, #0]
	mov	r7, r6
.L951:
	ldr	r0, [r1, #8]
	cmp	r8, r0
	beq	.L972
.L954:
	cmp	r8, r7
	bne	.L967
	b	.L957
.L960:
	mov	r8, r6
.L967:
	mov	r0, r8
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r2, [r5, #0]
	mov	r6, r0
	add	r3, r2, #12
	add	r1, r2, #4
	mov	r0, r8
	add	r2, r2, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L959:
	ldr	r3, [r5, #4]
	cmp	r6, r7
	sub	lr, r3, #1
	str	lr, [r5, #4]
	bne	.L960
.L957:
	ldr	r1, [r4, #4]
	cmp	r1, #1
	ldr	r3, [r4, #0]
	beq	.L973
	cmp	r3, #0
	beq	.L931
	ldr	r0, [r3, #-4]
	add	r0, r3, r0, asl #2
	cmp	r3, r0
	beq	.L962
	b	.L968
.L974:
	mov	r0, r5
.L968:
	sub	r5, r0, #4
	ldr	r2, [r0, #-4]
	mov	r0, r5
	ldr	ip, [r2, #0]
	mov	lr, pc
	bx	ip
	ldr	r0, [r4, #0]
	cmp	r0, r5
	bne	.L974
.L962:
	sub	r0, r0, #8
	bl	_ZdaPv
	b	.L931
.L973:
	cmp	r3, #0
	beq	.L931
	mov	r0, r3
	ldr	ip, [r3, #0]
	ldr	ip, [ip, #4]
	mov	lr, pc
	bx	ip
	b	.L931
.L972:
	cmp	r7, r1
	bne	.L954
	ldr	r2, [r5, #4]
	cmp	r2, #0
	ldr	r5, .L975
	beq	.L957
	ldr	r0, [r7, #4]
	bl	T.1125
	ldr	r0, [r5, #0]
	str	r0, [r0, #8]
	ldr	ip, [r5, #0]
	mov	r3, #0
	str	r3, [ip, #4]
	ldr	r1, [r5, #0]
	str	r1, [r1, #12]
	str	r3, [r5, #4]
	b	.L957
.L976:
	.align	2
.L975:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	T.1132, .-T.1132
	.section	.text._ZN12QuDrawObjectD1Ev,"axG",%progbits,_ZN12QuDrawObjectD1Ev,comdat
	.align	2
	.weak	_ZN12QuDrawObjectD1Ev
	.hidden	_ZN12QuDrawObjectD1Ev
	.type	_ZN12QuDrawObjectD1Ev, %function
_ZN12QuDrawObjectD1Ev:
	.fnstart
.LFB2954:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, lr}
	ldr	ip, [r0, #124]
	ldr	r3, .L1042
	mov	r8, r0
	cmp	ip, #0
	.pad #16
	sub	sp, sp, #16
	mov	r4, r0
	str	r3, [r8], #124
	ldr	sl, [r0, #128]
	beq	.L978
	ldr	r7, .L1042+4
	ldr	r0, [r7, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L982
	mov	r1, r0
	b	.L983
.L1035:
	mov	r1, r3
	mov	r3, r2
.L983:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L1035
.L982:
	cmp	r3, r0
	beq	.L985
	ldr	r2, [r3, #16]
	cmp	ip, r2
	mov	r2, r3
	bcs	.L986
.L985:
	str	ip, [sp, #0]
	add	r0, sp, #12
	mov	ip, #0
	add	r1, sp, #8
	mov	r2, sp
	str	r3, [sp, #8]
	str	ip, [sp, #4]
.LEHB40:
	bl	T.1130
	ldr	r2, [sp, #12]
.L986:
	ldr	r1, [r2, #20]
	sub	r0, r1, #1
	cmp	r0, #0
	str	r0, [r2, #20]
	beq	.L1036
.L978:
	ldrb	ip, [r4, #92]	@ zero_extendqisi2
	mov	r3, #0
	cmp	ip, #0
	str	r3, [r4, #124]
	str	sl, [r4, #128]
	beq	.L1012
	ldr	r0, [r4, #100]
	cmp	r0, r3
	blne	_ZdaPv
.L1013:
	mov	r1, #0
	strb	r1, [r4, #92]
	str	r1, [r4, #100]
.L1012:
	ldrb	r0, [r4, #64]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L1016
	ldr	r0, [r4, #72]
	cmp	r0, #0
	blne	_ZdaPv
.L1017:
	mov	r2, #0
	strb	r2, [r4, #64]
	str	r2, [r4, #72]
.L1016:
	ldrb	r3, [r4, #36]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1020
	ldr	r0, [r4, #44]
	cmp	r0, #0
	blne	_ZdaPv
.L1021:
	mov	ip, #0
	strb	ip, [r4, #36]
	str	ip, [r4, #44]
.L1020:
	ldrb	r1, [r4, #8]	@ zero_extendqisi2
	cmp	r1, #0
	beq	.L1024
	ldr	r0, [r4, #16]
	cmp	r0, #0
	blne	_ZdaPv
.L1025:
	mov	r0, #0
	strb	r0, [r4, #8]
	str	r0, [r4, #16]
.L1024:
	mov	r0, r4
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L1036:
	ldr	r1, [r7, #0]
	ldr	r5, [r1, #4]
	cmp	r5, #0
	ldr	r2, [r4, #124]
	beq	.L987
	mov	r0, r1
	mov	r6, r5
	b	.L991
.L1037:
	mov	r0, r6
	mov	r6, r3
.L991:
	ldr	r3, [r6, #16]
	cmp	r2, r3
	ldrhi	r3, [r6, #12]
	ldrls	r3, [r6, #8]
	movhi	r6, r0
	cmp	r3, #0
	bne	.L1037
	mov	r0, r1
	b	.L995
.L1038:
	mov	r0, r5
	mov	r5, r3
.L995:
	ldr	r3, [r5, #16]
	cmp	r2, r3
	ldrcs	r3, [r5, #12]
	ldrcc	r3, [r5, #8]
	movcs	r5, r0
	cmp	r3, #0
	bne	.L1038
	cmp	r6, r5
	mov	r9, r6
	moveq	r6, r5
	beq	.L998
	mov	r0, r6
.L999:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r5, r0
	bne	.L999
	ldr	r1, [r7, #0]
	mov	r9, r5
.L998:
	ldr	r3, [r1, #8]
	cmp	r6, r3
	beq	.L1039
.L1001:
	cmp	r6, r9
	bne	.L1034
	b	.L1004
.L1007:
	mov	r6, r5
.L1034:
	mov	r0, r6
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r2, [r7, #0]
	mov	r5, r0
	add	r3, r2, #12
	add	r1, r2, #4
	mov	r0, r6
	add	r2, r2, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L1006:
	ldr	lr, [r7, #4]
	cmp	r5, r9
	sub	r3, lr, #1
	str	r3, [r7, #4]
	bne	.L1007
.L1004:
	ldr	ip, [r4, #128]
	cmp	ip, #1
	ldr	r3, [r4, #124]
	beq	.L1040
	cmp	r3, #0
	beq	.L978
	ldr	r0, [r3, #-4]
	rsb	r5, r0, r0, asl #3
	add	r5, r3, r5, asl #2
	b	.L1010
.L1041:
	ldr	r3, [r5, #-28]!
	mov	r0, r5
	ldr	ip, [r3, #4]
	mov	lr, pc
	bx	ip
.L1010:
	ldr	r2, [r8, #0]
	cmp	r2, r5
	bne	.L1041
	ldr	lr, [r4, #124]
	sub	r0, lr, #8
	bl	_ZdaPv
	b	.L978
.L1040:
	cmp	r3, #0
	beq	.L978
	mov	r0, r3
	ldr	r1, [r3, #0]
	ldr	ip, [r1, #8]
	mov	lr, pc
	bx	ip
	b	.L978
.L987:
	mov	r9, r1
	mov	r6, r1
	b	.L998
.L1039:
	cmp	r9, r1
	bne	.L1001
	ldr	r2, [r7, #4]
	cmp	r2, #0
	beq	.L1004
	ldr	r0, [r9, #4]
	bl	T.1125
.LEHE40:
	ldr	r1, [r7, #0]
	str	r1, [r1, #8]
	ldr	r0, [r7, #0]
	mov	lr, #0
	str	lr, [r0, #4]
	ldr	ip, [r7, #0]
	str	ip, [ip, #12]
	str	lr, [r7, #4]
	b	.L1004
.L1029:
.L1014:
.L1030:
.L1018:
.L1031:
.L1022:
.L1032:
.L1026:
	mov	r6, r0
	add	r0, r4, #92
	bl	_ZN14QuArrayPointerIfE6deInitEv
	add	r0, r4, #64
	bl	_ZN14QuArrayPointerIfE6deInitEv
	add	r0, r4, #36
	bl	_ZN14QuArrayPointerIfE6deInitEv
	add	r0, r4, #8
	bl	_ZN14QuArrayPointerIfE6deInitEv
	mov	r0, r6
.LEHB41:
	bl	__cxa_end_cleanup
.LEHE41:
.L1043:
	.align	2
.L1042:
	.word	_ZTV12QuDrawObject+8
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA2954:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2954-.LLSDACSB2954
.LLSDACSB2954:
	.uleb128 .LEHB40-.LFB2954
	.uleb128 .LEHE40-.LEHB40
	.uleb128 .L1029-.LFB2954
	.uleb128 0x0
	.uleb128 .LEHB41-.LFB2954
	.uleb128 .LEHE41-.LEHB41
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE2954:
	.fnend
	.size	_ZN12QuDrawObjectD1Ev, .-_ZN12QuDrawObjectD1Ev
	.section	.text._ZN12QuDrawObjectD0Ev,"axG",%progbits,_ZN12QuDrawObjectD0Ev,comdat
	.align	2
	.weak	_ZN12QuDrawObjectD0Ev
	.hidden	_ZN12QuDrawObjectD0Ev
	.type	_ZN12QuDrawObjectD0Ev, %function
_ZN12QuDrawObjectD0Ev:
	.fnstart
.LFB2955:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, lr}
	ldr	ip, [r0, #124]
	ldr	r3, .L1109
	mov	r8, r0
	cmp	ip, #0
	.pad #16
	sub	sp, sp, #16
	mov	r4, r0
	str	r3, [r8], #124
	ldr	sl, [r0, #128]
	beq	.L1045
	ldr	r7, .L1109+4
	ldr	r0, [r7, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L1049
	mov	r1, r0
	b	.L1050
.L1102:
	mov	r1, r3
	mov	r3, r2
.L1050:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L1102
.L1049:
	cmp	r3, r0
	beq	.L1052
	ldr	r2, [r3, #16]
	cmp	ip, r2
	mov	r2, r3
	bcs	.L1053
.L1052:
	str	ip, [sp, #0]
	add	r0, sp, #12
	mov	ip, #0
	add	r1, sp, #8
	mov	r2, sp
	str	r3, [sp, #8]
	str	ip, [sp, #4]
.LEHB42:
	bl	T.1130
	ldr	r2, [sp, #12]
.L1053:
	ldr	r1, [r2, #20]
	sub	r0, r1, #1
	cmp	r0, #0
	str	r0, [r2, #20]
	beq	.L1103
.L1045:
	ldrb	r2, [r4, #92]	@ zero_extendqisi2
	mov	r3, #0
	cmp	r2, #0
	str	r3, [r4, #124]
	str	sl, [r4, #128]
	beq	.L1079
	ldr	r0, [r4, #100]
	cmp	r0, r3
	blne	_ZdaPv
.L1080:
	mov	r1, #0
	strb	r1, [r4, #92]
	str	r1, [r4, #100]
.L1079:
	ldrb	r0, [r4, #64]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L1083
	ldr	r0, [r4, #72]
	cmp	r0, #0
	blne	_ZdaPv
.L1084:
	mov	lr, #0
	strb	lr, [r4, #64]
	str	lr, [r4, #72]
.L1083:
	ldrb	r3, [r4, #36]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1087
	ldr	r0, [r4, #44]
	cmp	r0, #0
	blne	_ZdaPv
.L1088:
	mov	ip, #0
	strb	ip, [r4, #36]
	str	ip, [r4, #44]
.L1087:
	ldrb	r2, [r4, #8]	@ zero_extendqisi2
	cmp	r2, #0
	beq	.L1091
	ldr	r0, [r4, #16]
	cmp	r0, #0
	blne	_ZdaPv
.L1092:
	mov	r1, #0
	strb	r1, [r4, #8]
	str	r1, [r4, #16]
.L1091:
	mov	r0, r4
	bl	_ZdlPv
	mov	r0, r4
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L1103:
	ldr	r1, [r7, #0]
	ldr	r5, [r1, #4]
	cmp	r5, #0
	ldr	r2, [r4, #124]
	beq	.L1054
	mov	r0, r1
	mov	r6, r5
	b	.L1058
.L1104:
	mov	r0, r6
	mov	r6, r3
.L1058:
	ldr	r3, [r6, #16]
	cmp	r2, r3
	ldrhi	r3, [r6, #12]
	ldrls	r3, [r6, #8]
	movhi	r6, r0
	cmp	r3, #0
	bne	.L1104
	mov	r0, r1
	b	.L1062
.L1105:
	mov	r0, r5
	mov	r5, r3
.L1062:
	ldr	r3, [r5, #16]
	cmp	r2, r3
	ldrcs	r3, [r5, #12]
	ldrcc	r3, [r5, #8]
	movcs	r5, r0
	cmp	r3, #0
	bne	.L1105
	cmp	r6, r5
	mov	r9, r6
	moveq	r6, r5
	beq	.L1065
	mov	r0, r6
.L1066:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r5, r0
	bne	.L1066
	ldr	r1, [r7, #0]
	mov	r9, r5
.L1065:
	ldr	lr, [r1, #8]
	cmp	r6, lr
	beq	.L1106
.L1068:
	cmp	r6, r9
	bne	.L1101
	b	.L1071
.L1074:
	mov	r6, r5
.L1101:
	mov	r0, r6
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	lr, [r7, #0]
	mov	r5, r0
	add	r3, lr, #12
	mov	r0, r6
	add	r1, lr, #4
	add	r2, lr, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L1073:
	ldr	ip, [r7, #4]
	cmp	r5, r9
	sub	r3, ip, #1
	str	r3, [r7, #4]
	bne	.L1074
.L1071:
	ldr	r2, [r4, #128]
	cmp	r2, #1
	ldr	r3, [r4, #124]
	beq	.L1107
	cmp	r3, #0
	beq	.L1045
	ldr	r0, [r3, #-4]
	rsb	r5, r0, r0, asl #3
	add	r5, r3, r5, asl #2
	b	.L1077
.L1108:
	ldr	r3, [r5, #-28]!
	mov	r0, r5
	ldr	ip, [r3, #4]
	mov	lr, pc
	bx	ip
.L1077:
	ldr	lr, [r8, #0]
	cmp	lr, r5
	bne	.L1108
	ldr	ip, [r4, #124]
	sub	r0, ip, #8
	bl	_ZdaPv
	b	.L1045
.L1107:
	cmp	r3, #0
	beq	.L1045
	mov	r0, r3
	ldr	r1, [r3, #0]
	ldr	ip, [r1, #8]
	mov	lr, pc
	bx	ip
	b	.L1045
.L1054:
	mov	r9, r1
	mov	r6, r1
	b	.L1065
.L1106:
	cmp	r9, r1
	bne	.L1068
	ldr	r3, [r7, #4]
	cmp	r3, #0
	beq	.L1071
	ldr	r0, [r9, #4]
	bl	T.1125
.LEHE42:
	ldr	r1, [r7, #0]
	str	r1, [r1, #8]
	ldr	r0, [r7, #0]
	mov	ip, #0
	str	ip, [r0, #4]
	ldr	r2, [r7, #0]
	str	r2, [r2, #12]
	str	ip, [r7, #4]
	b	.L1071
.L1096:
.L1081:
.L1097:
.L1085:
.L1098:
.L1089:
.L1099:
.L1093:
	mov	r6, r0
	add	r0, r4, #92
	bl	_ZN14QuArrayPointerIfE6deInitEv
	add	r0, r4, #64
	bl	_ZN14QuArrayPointerIfE6deInitEv
	add	r0, r4, #36
	bl	_ZN14QuArrayPointerIfE6deInitEv
	add	r0, r4, #8
	bl	_ZN14QuArrayPointerIfE6deInitEv
	mov	r0, r6
.LEHB43:
	bl	__cxa_end_cleanup
.LEHE43:
.L1110:
	.align	2
.L1109:
	.word	_ZTV12QuDrawObject+8
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA2955:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2955-.LLSDACSB2955
.LLSDACSB2955:
	.uleb128 .LEHB42-.LFB2955
	.uleb128 .LEHE42-.LEHB42
	.uleb128 .L1096-.LFB2955
	.uleb128 0x0
	.uleb128 .LEHB43-.LFB2955
	.uleb128 .LEHE43-.LEHB43
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE2955:
	.fnend
	.size	_ZN12QuDrawObjectD0Ev, .-_ZN12QuDrawObjectD0Ev
	.section	.text._ZN17QuImageDrawObjectD0Ev,"axG",%progbits,_ZN17QuImageDrawObjectD0Ev,comdat
	.align	2
	.weak	_ZN17QuImageDrawObjectD0Ev
	.hidden	_ZN17QuImageDrawObjectD0Ev
	.type	_ZN17QuImageDrawObjectD0Ev, %function
_ZN17QuImageDrawObjectD0Ev:
	.fnstart
.LFB2983:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, lr}
	ldr	ip, [r0, #124]
	ldr	r3, .L1176
	mov	r8, r0
	cmp	ip, #0
	.pad #16
	sub	sp, sp, #16
	mov	r4, r0
	str	r3, [r8], #124
	ldr	sl, [r0, #128]
	beq	.L1112
	ldr	r7, .L1176+4
	ldr	r0, [r7, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L1116
	mov	r1, r0
	b	.L1117
.L1169:
	mov	r1, r3
	mov	r3, r2
.L1117:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L1169
.L1116:
	cmp	r3, r0
	beq	.L1119
	ldr	r2, [r3, #16]
	cmp	ip, r2
	mov	r2, r3
	bcs	.L1120
.L1119:
	str	ip, [sp, #0]
	add	r0, sp, #12
	mov	ip, #0
	add	r1, sp, #8
	mov	r2, sp
	str	r3, [sp, #8]
	str	ip, [sp, #4]
.LEHB44:
	bl	T.1130
	ldr	r2, [sp, #12]
.L1120:
	ldr	r1, [r2, #20]
	sub	r0, r1, #1
	cmp	r0, #0
	str	r0, [r2, #20]
	beq	.L1170
.L1112:
	ldrb	r2, [r4, #92]	@ zero_extendqisi2
	mov	r3, #0
	cmp	r2, #0
	str	r3, [r4, #124]
	str	sl, [r4, #128]
	beq	.L1146
	ldr	r0, [r4, #100]
	cmp	r0, r3
	blne	_ZdaPv
.L1147:
	mov	r1, #0
	strb	r1, [r4, #92]
	str	r1, [r4, #100]
.L1146:
	ldrb	r0, [r4, #64]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L1150
	ldr	r0, [r4, #72]
	cmp	r0, #0
	blne	_ZdaPv
.L1151:
	mov	lr, #0
	strb	lr, [r4, #64]
	str	lr, [r4, #72]
.L1150:
	ldrb	r3, [r4, #36]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L1154
	ldr	r0, [r4, #44]
	cmp	r0, #0
	blne	_ZdaPv
.L1155:
	mov	ip, #0
	strb	ip, [r4, #36]
	str	ip, [r4, #44]
.L1154:
	ldrb	r2, [r4, #8]	@ zero_extendqisi2
	cmp	r2, #0
	beq	.L1158
	ldr	r0, [r4, #16]
	cmp	r0, #0
	blne	_ZdaPv
.L1159:
	mov	r1, #0
	strb	r1, [r4, #8]
	str	r1, [r4, #16]
.L1158:
	mov	r0, r4
	bl	_ZdlPv
	mov	r0, r4
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L1170:
	ldr	r1, [r7, #0]
	ldr	r5, [r1, #4]
	cmp	r5, #0
	ldr	r2, [r4, #124]
	beq	.L1121
	mov	r0, r1
	mov	r6, r5
	b	.L1125
.L1171:
	mov	r0, r6
	mov	r6, r3
.L1125:
	ldr	r3, [r6, #16]
	cmp	r2, r3
	ldrhi	r3, [r6, #12]
	ldrls	r3, [r6, #8]
	movhi	r6, r0
	cmp	r3, #0
	bne	.L1171
	mov	r0, r1
	b	.L1129
.L1172:
	mov	r0, r5
	mov	r5, r3
.L1129:
	ldr	r3, [r5, #16]
	cmp	r2, r3
	ldrcs	r3, [r5, #12]
	ldrcc	r3, [r5, #8]
	movcs	r5, r0
	cmp	r3, #0
	bne	.L1172
	cmp	r6, r5
	mov	r9, r6
	moveq	r6, r5
	beq	.L1132
	mov	r0, r6
.L1133:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r5, r0
	bne	.L1133
	ldr	r1, [r7, #0]
	mov	r9, r5
.L1132:
	ldr	lr, [r1, #8]
	cmp	r6, lr
	beq	.L1173
.L1135:
	cmp	r6, r9
	bne	.L1168
	b	.L1138
.L1141:
	mov	r6, r5
.L1168:
	mov	r0, r6
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	lr, [r7, #0]
	mov	r5, r0
	add	r3, lr, #12
	mov	r0, r6
	add	r1, lr, #4
	add	r2, lr, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L1140:
	ldr	ip, [r7, #4]
	cmp	r5, r9
	sub	r3, ip, #1
	str	r3, [r7, #4]
	bne	.L1141
.L1138:
	ldr	r2, [r4, #128]
	cmp	r2, #1
	ldr	r3, [r4, #124]
	beq	.L1174
	cmp	r3, #0
	beq	.L1112
	ldr	r0, [r3, #-4]
	rsb	r5, r0, r0, asl #3
	add	r5, r3, r5, asl #2
	b	.L1144
.L1175:
	ldr	r3, [r5, #-28]!
	mov	r0, r5
	ldr	ip, [r3, #4]
	mov	lr, pc
	bx	ip
.L1144:
	ldr	lr, [r8, #0]
	cmp	lr, r5
	bne	.L1175
	ldr	ip, [r4, #124]
	sub	r0, ip, #8
	bl	_ZdaPv
	b	.L1112
.L1174:
	cmp	r3, #0
	beq	.L1112
	mov	r0, r3
	ldr	r1, [r3, #0]
	ldr	ip, [r1, #8]
	mov	lr, pc
	bx	ip
	b	.L1112
.L1121:
	mov	r9, r1
	mov	r6, r1
	b	.L1132
.L1173:
	cmp	r9, r1
	bne	.L1135
	ldr	r3, [r7, #4]
	cmp	r3, #0
	beq	.L1138
	ldr	r0, [r9, #4]
	bl	T.1125
.LEHE44:
	ldr	r1, [r7, #0]
	str	r1, [r1, #8]
	ldr	r0, [r7, #0]
	mov	ip, #0
	str	ip, [r0, #4]
	ldr	r2, [r7, #0]
	str	r2, [r2, #12]
	str	ip, [r7, #4]
	b	.L1138
.L1163:
.L1148:
.L1164:
.L1152:
.L1165:
.L1156:
.L1166:
.L1160:
	mov	r6, r0
	add	r0, r4, #92
	bl	_ZN14QuArrayPointerIfE6deInitEv
	add	r0, r4, #64
	bl	_ZN14QuArrayPointerIfE6deInitEv
	add	r0, r4, #36
	bl	_ZN14QuArrayPointerIfE6deInitEv
	add	r0, r4, #8
	bl	_ZN14QuArrayPointerIfE6deInitEv
	mov	r0, r6
.LEHB45:
	bl	__cxa_end_cleanup
.LEHE45:
.L1177:
	.align	2
.L1176:
	.word	_ZTV12QuDrawObject+8
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA2983:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2983-.LLSDACSB2983
.LLSDACSB2983:
	.uleb128 .LEHB44-.LFB2983
	.uleb128 .LEHE44-.LEHB44
	.uleb128 .L1163-.LFB2983
	.uleb128 0x0
	.uleb128 .LEHB45-.LFB2983
	.uleb128 .LEHE45-.LEHB45
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE2983:
	.fnend
	.size	_ZN17QuImageDrawObjectD0Ev, .-_ZN17QuImageDrawObjectD0Ev
	.section	.text._ZN8QuGlobal6getRefEv,"axG",%progbits,_ZN8QuGlobal6getRefEv,comdat
	.align	2
	.weak	_ZN8QuGlobal6getRefEv
	.hidden	_ZN8QuGlobal6getRefEv
	.type	_ZN8QuGlobal6getRefEv, %function
_ZN8QuGlobal6getRefEv:
	.fnstart
.LFB3679:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	ldr	r5, .L1192
	ldr	r4, [r5, #0]
	cmp	r4, #0
	beq	.L1191
.L1179:
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L1191:
	mov	r0, #60
.LEHB46:
	bl	_Znwj
.LEHE46:
	mov	r3, #0
	mov	r6, r0
	mov	r1, #1
	str	r3, [r0, #12]!
	strb	r3, [r6, #0]
	strb	r3, [r6, #1]
	str	r3, [r6, #4]
	str	r1, [r6, #8]
	mov	r4, r6
.LEHB47:
	bl	T.1131
.LEHE47:
	mov	r0, #0
	mov	r2, #0
	mov	lr, #1
	mov	ip, #30
	mov	r1, #1065353216
	str	lr, [r6, #24]
	str	ip, [r6, #40]
	str	r2, [r6, #48]	@ float
	str	r1, [r6, #52]	@ float
	str	r0, [r6, #56]
	str	r0, [r6, #20]
	str	r0, [r6, #36]
	str	r2, [r6, #44]	@ float
	str	r6, [r5, #0]
	b	.L1179
.L1189:
.L1185:
.L1190:
.L1187:
	mov	r4, r0
	ldr	r1, [r6, #8]
	add	r0, r6, #4
	bl	T.1129
	mov	r0, r6
	bl	_ZdlPv
	mov	r0, r4
.LEHB48:
	bl	__cxa_end_cleanup
.LEHE48:
.L1193:
	.align	2
.L1192:
	.word	_ZN8QuGlobal5mSelfE
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA3679:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3679-.LLSDACSB3679
.LLSDACSB3679:
	.uleb128 .LEHB46-.LFB3679
	.uleb128 .LEHE46-.LEHB46
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB47-.LFB3679
	.uleb128 .LEHE47-.LEHB47
	.uleb128 .L1189-.LFB3679
	.uleb128 0x0
	.uleb128 .LEHB48-.LFB3679
	.uleb128 .LEHE48-.LEHB48
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE3679:
	.fnend
	.size	_ZN8QuGlobal6getRefEv, .-_ZN8QuGlobal6getRefEv
	.section	.text._ZN21QuGuiRenderingContext6enableEv,"axG",%progbits,_ZN21QuGuiRenderingContext6enableEv,comdat
	.align	2
	.weak	_ZN21QuGuiRenderingContext6enableEv
	.hidden	_ZN21QuGuiRenderingContext6enableEv
	.type	_ZN21QuGuiRenderingContext6enableEv, %function
_ZN21QuGuiRenderingContext6enableEv:
	.fnstart
.LFB3735:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	ldr	r3, [r0, #44]
	cmp	r3, #4
	mov	r4, r0
	beq	.L1200
.L1195:
	add	r5, r4, #4
	mov	r0, r5
	ldr	r3, [r4, #4]
	ldr	ip, [r3, #8]
	mov	lr, pc
	bx	ip
	mov	r0, r5
	ldr	ip, [r4, #4]
	ldr	ip, [ip, #12]
	mov	lr, pc
	bx	ip
	ldrb	r0, [r4, #40]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L1198
	ldr	r0, [r4, #28]
	bl	__aeabi_i2f
	mov	r5, r0
	ldr	r0, [r4, #32]
	bl	__aeabi_i2f
	mov	r2, #1065353216
	mov	r1, r0
	mov	r0, r5
	bl	glScalef
.L1198:
	mov	lr, #3040
	add	r0, lr, #2
	bl	glEnable
	mov	r2, #768
	add	r1, r2, #3
	add	r0, r2, #2
	bl	glBlendFunc
	mov	r0, #0
	bl	glDepthMask
	mov	r1, #2928
	add	r0, r1, #1
	bl	glDisable
	mov	r0, #2896
	bl	glDisable
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L1200:
	bl	_ZN8QuGlobal6getRefEv
	bl	_ZN8QuGlobal17getDeviceRotationEv
	cmp	r0, #1
	cmpne	r0, #3
	ldreq	r2, [r4, #20]
	ldreq	r3, [r4, #24]
	streq	r2, [r4, #32]
	streq	r3, [r4, #28]
	cmp	r0, #0
	cmpne	r0, #2
	ldreq	r2, [r4, #24]
	streq	r2, [r4, #32]
	ldr	r2, .L1201
	ldreq	r3, [r4, #20]
	ldr	r1, [r2, r0, asl #2]
	streq	r3, [r4, #28]
	str	r1, [r4, #36]
	b	.L1195
.L1202:
	.align	2
.L1201:
	.word	.LANCHOR1
	.fnend
	.size	_ZN21QuGuiRenderingContext6enableEv, .-_ZN21QuGuiRenderingContext6enableEv
	.global	__aeabi_f2iz
	.global	__aeabi_fcmple
	.section	.text._ZN18QuTintableGuiImageC1Ev,"axG",%progbits,_ZN18QuTintableGuiImageC1Ev,comdat
	.align	2
	.weak	_ZN18QuTintableGuiImageC1Ev
	.hidden	_ZN18QuTintableGuiImageC1Ev
	.type	_ZN18QuTintableGuiImageC1Ev, %function
_ZN18QuTintableGuiImageC1Ev:
	.fnstart
.LFB3752:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	sl, .L1268
	ldr	r7, .L1268+4
	str	sl, [r0, #0]
	ldr	r6, [r7, #0]
	cmp	r6, #0
	.pad #44
	sub	sp, sp, #44
	mov	r4, r0
	beq	.L1264
.L1205:
	ldr	r5, [r6, #32]
	ldr	r8, .L1268+8
	ldr	r6, [r6, #28]
	cmn	r5, #1
	mov	lr, #0
	str	lr, [r4, #36]
	str	r8, [r4, #4]
	str	r6, [r4, #20]
	strne	r5, [r4, #24]
	movne	r0, r5
	beq	.L1265
	ldr	r6, [r7, #0]
	cmp	r6, #0
	beq	.L1266
.L1216:
	bl	__aeabi_i2f
	mov	r7, r0
	ldr	r0, [r4, #20]
	bl	__aeabi_i2f
	mov	r1, r0
	mov	r0, r7
	bl	__aeabi_fdiv
	mov	r7, r0
	ldr	r0, [r6, #32]
	bl	__aeabi_i2f
	mov	r9, r0
	ldr	r0, [r6, #28]
	bl	__aeabi_i2f
	mov	r1, r0
	mov	r0, r9
	bl	__aeabi_fdiv
	mov	r1, r7
	bl	__aeabi_fcmple
	subs	fp, r0, #0
	beq	.L1263
.LEHB49:
	bl	_ZN8QuGlobal6getRefEv
	ldr	r0, [r0, #28]
	bl	__aeabi_i2f
	mov	r9, r0
	bl	_ZN8QuGlobal6getRefEv
	ldr	r0, [r0, #28]
	bl	__aeabi_i2f
	mov	r1, r0
	mov	r0, r7
	bl	__aeabi_fmul
	mov	r6, r0
	bl	_ZN8QuGlobal6getRefEv
	ldr	r0, [r0, #32]
	bl	__aeabi_i2f
	mov	r1, r6
	bl	__aeabi_fsub
	mov	r1, #1056964608
	bl	__aeabi_fmul
	mov	r3, #0
	str	r3, [sp, #4]
	bl	__aeabi_f2iz
	mov	fp, r0
.L1229:
	mov	r0, r9
	bl	__aeabi_f2iz
	mov	r9, r0
	mov	r0, r6
	bl	__aeabi_f2iz
	mov	r2, r9
	mov	r3, r0
	mov	r1, fp
	ldr	r0, [sp, #4]
	bl	glViewport
	mov	r3, #1124073472
	add	r0, r3, #16384000
	str	r0, [r4, #16]	@ float
	mov	r0, r5
	bl	__aeabi_i2f
	add	r7, r4, #20
	ldmia	r7, {r7, r9}	@ phole ldm
	ldr	r1, .L1268+12
	mov	r5, #0
	mov	r6, #4
	mov	ip, #1
	mov	r2, #5
	str	r0, [r4, #12]	@ float
	str	r7, [r4, #28]
	str	r9, [r4, #32]
	str	r6, [r4, #144]
	str	ip, [r4, #176]
	str	r2, [r4, #180]
	str	r1, [r4, #48]
	str	r5, [r4, #188]
	strb	r5, [r4, #40]
	str	r6, [r4, #44]
	strb	r5, [r4, #56]
	str	r6, [r4, #60]
	strb	r5, [r4, #84]
	str	r6, [r4, #88]
	strb	r5, [r4, #112]
	str	r6, [r4, #116]
	strb	r5, [r4, #140]
	str	r5, [r4, #172]
	str	r5, [r4, #52]
	strb	r5, [r4, #168]
	strb	r5, [r4, #184]
	mov	r0, #48
	bl	malloc
	cmp	r0, #0
	beq	.L1267
.L1240:
	mov	lr, #0
	str	r0, [r4, #188]
	str	lr, [r4, #192]
	strb	lr, [r0, #0]
	ldr	r3, [r4, #188]
	str	lr, [r3, #4]
	ldr	r0, [r4, #188]
	ldr	r8, .L1268+16
	str	r0, [r0, #8]
	ldmia	r8, {r0, r1, r2, r3}
	ldr	r7, [r4, #188]
	add	r6, sp, #24
	str	r7, [r7, #12]
	stmia	r6, {r0, r1, r2, r3}
	add	sl, r8, #16
	ldr	r6, [sp, #24]	@ float
	ldr	r7, [sp, #28]	@ float
	ldr	r8, [sp, #32]	@ float
	ldr	r9, [sp, #36]	@ float
	ldmia	sl, {r0, r1, r2, r3}
	ldr	ip, .L1268+20
	mov	r5, #30
	str	r5, [r4, #216]
	add	sl, sp, #8
	mov	r5, #1
	str	ip, [r4, #204]
	str	lr, [r4, #212]
	strb	r5, [r4, #252]
	str	r6, [r4, #220]	@ float
	str	r7, [r4, #224]	@ float
	str	r8, [r4, #228]	@ float
	str	r9, [r4, #232]	@ float
	str	lr, [r4, #208]
	stmia	sl, {r0, r1, r2, r3}
	ldr	lr, [sp, #20]	@ float
	ldr	r1, [sp, #12]	@ float
	ldr	r2, [sp, #16]	@ float
	ldr	ip, [sp, #8]	@ float
	mov	r0, r4
	str	ip, [r4, #236]	@ float
	str	r1, [r4, #240]	@ float
	str	r2, [r4, #244]	@ float
	str	lr, [r4, #248]	@ float
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L1263:
	bl	_ZN8QuGlobal6getRefEv
	ldr	r0, [r0, #32]
	bl	__aeabi_i2f
	mov	r6, r0
	bl	_ZN8QuGlobal6getRefEv
	ldr	r0, [r0, #32]
	bl	__aeabi_i2f
	mov	r1, r7
	bl	__aeabi_fdiv
	mov	r9, r0
	bl	_ZN8QuGlobal6getRefEv
	ldr	r0, [r0, #28]
	bl	__aeabi_i2f
	mov	r1, r9
	bl	__aeabi_fsub
	mov	r1, #1056964608
	bl	__aeabi_fmul
	bl	__aeabi_f2iz
	str	r0, [sp, #4]
	b	.L1229
.L1265:
	bl	_ZN8QuGlobal6getRefEv
	ldr	r0, [r0, #32]
	mul	r0, r6, r0
	bl	__aeabi_i2f
	mov	r6, r0
	bl	_ZN8QuGlobal6getRefEv
	ldr	r0, [r0, #28]
	bl	__aeabi_i2f
	mov	r1, r0
	mov	r0, r6
	bl	__aeabi_fdiv
	bl	__aeabi_f2iz
	str	r0, [r4, #24]
	ldr	r6, [r7, #0]
	cmp	r6, #0
	bne	.L1216
.L1266:
	mov	r0, #60
	bl	_Znwj
.LEHE49:
	mov	r9, r0
	add	r3, r9, #4
	mov	r0, #0
	str	r3, [sp, #4]
	str	r0, [r9, #4]
	strb	r0, [r9, #0]
	strb	r0, [r9, #1]
	ldr	r0, [sp, #4]
	mov	r1, #1
	mov	r6, r9
.LEHB50:
	bl	T.1129
.LEHE50:
	mov	r1, #0
	add	fp, r9, #12
	str	r1, [r9, #12]
	mov	r0, fp
	mov	r1, #1
.LEHB51:
	bl	T.1131
.LEHE51:
	mov	r0, r9
	mov	r2, #0
	str	r2, [r0, #20]!
.LEHB52:
	bl	T.1132
.LEHE52:
	mov	fp, #0
	mov	ip, #0
	mov	r0, #30
	mov	lr, #1065353216
	str	r0, [r9, #40]
	str	fp, [r9, #48]	@ float
	str	lr, [r9, #52]	@ float
	str	ip, [r9, #56]
	str	ip, [r9, #36]
	str	fp, [r9, #44]	@ float
	str	r9, [r7, #0]
	ldr	r0, [r4, #24]
	b	.L1216
.L1267:
	mov	r0, #48
.LEHB53:
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
.LEHE53:
	b	.L1240
.L1264:
	mov	r0, #60
.LEHB54:
	bl	_Znwj
.LEHE54:
	mov	r3, #0
	add	r8, r0, #4
	mov	r5, r0
	str	r3, [r0, #4]
	strb	r3, [r0, #0]
	strb	r3, [r0, #1]
	mov	r1, #1
	mov	r0, r8
	mov	r6, r5
.LEHB55:
	bl	T.1129
.LEHE55:
	mov	r1, #0
	mov	r0, r5
	str	r1, [r0, #12]!
	mov	r1, #1
.LEHB56:
	bl	T.1131
.LEHE56:
	mov	r8, #0
	mov	r2, #0
	mov	ip, #1
	mov	fp, #30
	mov	r9, #1065353216
	str	ip, [r5, #24]
	str	fp, [r5, #40]
	str	r2, [r5, #48]	@ float
	str	r9, [r5, #52]	@ float
	str	r8, [r5, #56]
	str	r8, [r5, #20]
	str	r8, [r5, #36]
	str	r2, [r5, #44]	@ float
	str	r5, [r7, #0]
	b	.L1205
.L1256:
	mov	r4, r0
.L1226:
	mov	r0, r9
	bl	_ZdlPv
	mov	r0, r4
.LEHB57:
	bl	__cxa_end_cleanup
.LEHE57:
.L1257:
.L1211:
	mov	r4, r0
	ldr	r1, [r5, #8]
	mov	r0, r8
	bl	T.1129
.L1213:
	mov	r0, r5
	bl	_ZdlPv
	mov	r0, r4
.LEHB58:
	bl	__cxa_end_cleanup
.LEHE58:
.L1258:
	mov	r4, r0
	b	.L1213
.L1259:
.L1242:
.L1260:
.L1245:
.L1261:
.L1247:
.L1262:
.L1250:
	ldr	r2, .L1268+24
	mov	r9, r4
	str	r2, [r9, #48]!
	mov	fp, r0
	mov	r0, r9
	bl	_ZN12QuDrawObjectD2Ev
	str	r8, [r4, #4]
	str	sl, [r4, #0]
	mov	r0, fp
.LEHB59:
	bl	__cxa_end_cleanup
.LEHE59:
.L1254:
.L1222:
	mov	r4, r0
	ldr	r1, [r9, #16]
	mov	r0, fp
	bl	T.1131
.L1224:
	ldr	r0, [sp, #4]
	ldr	r1, [r9, #8]
	bl	T.1129
	b	.L1226
.L1255:
	mov	r4, r0
	b	.L1224
.L1269:
	.align	2
.L1268:
	.word	_ZTV21QuGuiRenderingContext+8
	.word	_ZN8QuGlobal5mSelfE
	.word	_ZTV13QuBasicCamera+8
	.word	_ZTV21QuColorableDrawObjectIiE+8
	.word	.LANCHOR1+16
	.word	_ZTV7QuTimer+8
	.word	_ZTV17QuImageDrawObject+8
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA3752:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3752-.LLSDACSB3752
.LLSDACSB3752:
	.uleb128 .LEHB49-.LFB3752
	.uleb128 .LEHE49-.LEHB49
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB50-.LFB3752
	.uleb128 .LEHE50-.LEHB50
	.uleb128 .L1256-.LFB3752
	.uleb128 0x0
	.uleb128 .LEHB51-.LFB3752
	.uleb128 .LEHE51-.LEHB51
	.uleb128 .L1255-.LFB3752
	.uleb128 0x0
	.uleb128 .LEHB52-.LFB3752
	.uleb128 .LEHE52-.LEHB52
	.uleb128 .L1254-.LFB3752
	.uleb128 0x0
	.uleb128 .LEHB53-.LFB3752
	.uleb128 .LEHE53-.LEHB53
	.uleb128 .L1259-.LFB3752
	.uleb128 0x0
	.uleb128 .LEHB54-.LFB3752
	.uleb128 .LEHE54-.LEHB54
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB55-.LFB3752
	.uleb128 .LEHE55-.LEHB55
	.uleb128 .L1258-.LFB3752
	.uleb128 0x0
	.uleb128 .LEHB56-.LFB3752
	.uleb128 .LEHE56-.LEHB56
	.uleb128 .L1257-.LFB3752
	.uleb128 0x0
	.uleb128 .LEHB57-.LFB3752
	.uleb128 .LEHE57-.LEHB57
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB58-.LFB3752
	.uleb128 .LEHE58-.LEHB58
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB59-.LFB3752
	.uleb128 .LEHE59-.LEHB59
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE3752:
	.fnend
	.size	_ZN18QuTintableGuiImageC1Ev, .-_ZN18QuTintableGuiImageC1Ev
	.section	.text._ZN7testApp21drawDummyLoadingImageEv,"ax",%progbits
	.align	2
	.global	_ZN7testApp21drawDummyLoadingImageEv
	.hidden	_ZN7testApp21drawDummyLoadingImageEv
	.type	_ZN7testApp21drawDummyLoadingImageEv, %function
_ZN7testApp21drawDummyLoadingImageEv:
	.fnstart
.LFB3873:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 280
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, lr}
	.pad #288
	sub	sp, sp, #288
	add	r0, sp, #12
.LEHB60:
	bl	_ZN18QuTintableGuiImageC1Ev
.LEHE60:
	mov	r0, #17
	bl	malloc
	subs	r5, r0, #0
	beq	.L1384
.L1272:
	ldr	r1, .L1400
	mov	r2, #16
	mov	r0, r5
	bl	memcpy
	mov	r3, #0
	strb	r3, [r0, #16]
	ldr	r4, .L1400+4
	str	r4, [sp, #280]
	ldr	r6, [r4, #0]
	ldr	r2, [r6, #-12]
	add	r2, r4, r2
	ldr	r1, [r2, #8]
	cmp	r1, r3
	add	r9, r0, #16
	mov	r7, r4
	beq	.L1273
.L1274:
	cmp	r3, #0
	strb	r3, [sp, #284]
	beq	.L1284
	ldr	r6, [r4, #0]
	ldr	r2, [r6, #-12]
	add	r2, r7, r2
	ldr	r8, [r2, #28]
	cmp	r8, #49
	ldr	r6, .L1400+4
	ble	.L1285
	ldr	ip, [r2, #4]
	and	sl, ip, #7
	cmp	sl, #1
	sub	r8, r8, #49
	beq	.L1385
	ldr	ip, [r2, #88]
	ldrb	r1, [r2, #84]	@ zero_extendqisi2
	mov	r0, ip
	ldr	sl, [ip, #0]
	mov	r2, r8
.LEHB61:
	ldr	ip, [sl, #48]
	mov	lr, pc
	bx	ip
.LEHE61:
	cmp	r8, r0
	beq	.L1386
.L1289:
	ldr	ip, [r4, #0]
	ldr	r2, [ip, #-12]
	mov	r1, #0
	add	r3, r7, r2
	str	r1, [r3, #28]
	ldr	r6, .L1400+4
.L1353:
	ldr	r2, [r4, #0]
	ldr	r0, [r2, #-12]
	add	r0, r6, r0
	ldr	r8, [r0, #88]
	ldr	ip, [r0, #8]
	cmp	r8, #0
	ldr	sl, [r0, #20]
	orr	r1, ip, #4
	orreq	r1, ip, #5
	tst	r1, sl
	str	r1, [r0, #8]
	bne	.L1387
.L1284:
	ldr	r6, [sp, #280]
	ldr	r1, [r6, #0]
	ldr	r3, [r1, #-12]
	add	r3, r6, r3
	ldr	lr, [r3, #4]
	tst	lr, #8192
	bne	.L1388
.L1293:
	ldr	r6, [r4, #0]
	ldr	sl, [r6, #-12]
	add	r1, r4, sl
	ldr	lr, [r1, #64]
	mov	r1, #10
	mov	r0, lr
	ldr	r3, [lr, #0]
	ldr	r8, .L1400+4
.LEHB62:
	ldr	ip, [r3, #24]
	mov	lr, pc
	bx	ip
	str	r4, [sp, #280]
	ldr	ip, [r4, #0]
	ldr	r2, [ip, #-12]
	mov	sl, r0
	add	r0, r7, r2
	ldr	r3, [r0, #8]
	cmp	r3, #0
	movne	r3, #0
	bne	.L1296
	ldr	lr, [r0, #88]
	cmp	lr, #0
	mov	r3, r0
	beq	.L1389
.L1297:
	ldr	r6, [r3, #92]
	cmp	r6, #0
	beq	.L1299
	ldr	lr, [r6, #0]
	ldr	ip, [lr, #-12]
	add	r3, r6, ip
	ldr	r3, [r3, #88]
	cmp	r3, #0
	beq	.L1299
	mov	r0, r3
	ldr	r2, [r3, #0]
	ldr	ip, [r2, #20]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	beq	.L1300
.L1377:
	ldr	lr, [r4, #0]
	ldr	r2, [lr, #-12]
.L1299:
	add	r7, r7, r2
	ldr	r2, [r7, #8]
	rsbs	r3, r2, #1
	movcc	r3, #0
.L1296:
	cmp	r3, #0
	strb	r3, [sp, #284]
	beq	.L1304
	ldr	ip, [r4, #0]
	ldr	r0, [ip, #-12]
	add	r3, r8, r0
	ldr	r3, [r3, #88]
	ldr	r6, [r3, #20]
	ldr	r1, [r3, #24]
	cmp	r6, r1
	strccb	sl, [r6], #1
	strcc	r6, [r3, #20]
	bcs	.L1390
.L1306:
	ldr	r6, [sp, #280]
	ldr	r2, [r6, #0]
	ldr	ip, [r2, #-12]
	add	r3, r6, ip
	ldr	r0, [r3, #4]
	tst	r0, #8192
	bne	.L1391
.L1308:
	ldr	r7, [r4, #0]
	ldr	r2, [r7, #-12]
	add	ip, r8, r2
	ldr	r3, [ip, #88]
	cmp	r3, #0
	beq	.L1311
	mov	r0, r3
	ldr	r6, [r3, #0]
	ldr	ip, [r6, #20]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	beq	.L1392
.L1311:
	mov	r3, #4
	mov	r0, #32
	str	r3, [sp, #64]
	bl	_Znaj
	ldr	r7, .L1400+8
	add	r8, r7, #48
	mov	r6, r0
	ldmia	r8!, {r0, r1, r2, r3}
	mov	ip, r6
	stmia	ip!, {r0, r1, r2, r3}
	rsb	r4, r5, r9
	ldmia	r8, {r0, r1, r2, r3}
	mov	lr, #0
	adds	r8, r4, #1
	stmia	ip, {r0, r1, r2, r3}
	str	lr, [sp, #276]
	str	lr, [sp, #268]
	str	lr, [sp, #272]
	beq	.L1316
	mov	r0, r8
	bl	malloc
	cmp	r0, #0
	beq	.L1393
.L1317:
	add	r8, r0, r8
	str	r8, [sp, #276]
	str	r0, [sp, #268]
	str	r0, [sp, #272]
	b	.L1354
.L1275:
.L1273:
	ldr	r3, [r2, #88]
	cmp	r3, #0
	beq	.L1394
.L1277:
	ldr	r6, [r2, #92]
	cmp	r6, #0
	beq	.L1279
	ldr	r8, [r6, #0]
	ldr	r0, [r8, #-12]
	add	r3, r6, r0
	ldr	r3, [r3, #88]
	cmp	r3, #0
	beq	.L1279
	mov	r0, r3
	ldr	r2, [r3, #0]
	ldr	ip, [r2, #20]
	mov	lr, pc
	bx	ip
.LEHE62:
	cmn	r0, #1
	beq	.L1280
.L1375:
	ldr	r3, [r4, #0]
	ldr	r0, [r3, #-12]
	add	r2, r7, r0
.L1279:
	ldr	r8, [r2, #8]
	rsbs	r3, r8, #1
	movcc	r3, #0
	b	.L1274
.L1390:
	mov	r0, r3
	mov	r1, sl
	ldr	r2, [r3, #0]
.LEHB63:
	ldr	ip, [r2, #52]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	bne	.L1306
.L1304:
	ldr	r3, [r4, #0]
	ldr	r1, [r3, #-12]
	add	r0, r8, r1
	ldr	r6, [r0, #8]
	ldr	r7, [r0, #20]
	orr	lr, r6, #1
	tst	lr, r7
	str	lr, [r0, #8]
	beq	.L1306
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
.LEHE63:
	ldr	r6, [sp, #280]
	ldr	r2, [r6, #0]
	ldr	ip, [r2, #-12]
	add	r3, r6, ip
	ldr	r0, [r3, #4]
	tst	r0, #8192
	beq	.L1308
.L1391:
	ldr	r3, [r3, #88]
	cmp	r3, #0
	beq	.L1308
	mov	r0, r3
	ldr	r7, [r3, #0]
.LEHB64:
	ldr	ip, [r7, #20]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	bne	.L1308
	ldr	r3, [r6, #0]
	ldr	r0, [r3, #-12]
	add	r0, r6, r0
	ldr	r1, [r0, #8]
	ldr	r6, [r0, #20]
	orr	lr, r1, #1
	tst	lr, r6
	str	lr, [r0, #8]
	beq	.L1308
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
.LEHE64:
	b	.L1308
.L1316:
	ldr	r0, .L1400+12
.LEHB65:
	bl	_ZN4_STL24__stl_throw_length_errorEPKc
.LEHE65:
	ldr	r0, [sp, #268]
.L1354:
	cmp	r9, r5
	beq	.L1319
	mov	r1, r5
	mov	r2, r4
	bl	memmove
	add	r0, r0, r4
.L1319:
	mov	r4, #0
	str	r0, [sp, #272]
	mov	r2, r6
	strb	r4, [r0, #0]
	mov	r3, r4
	add	r0, sp, #60
	add	r1, sp, #268
	str	r4, [sp, #0]
.LEHB66:
	bl	_ZN12QuDrawObject9loadImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEEPfii
.LEHE66:
.L1321:
	ldr	r3, [sp, #268]
	cmp	r3, #0
	mov	r4, r0
	movne	r0, r3
	blne	free
.L1323:
	cmp	r4, #0
	beq	.L1324
	mov	r0, #48
.LEHB67:
	bl	_Znaj
.LEHE67:
	ldr	lr, .L1400+16
	mov	r4, r0
	ldmia	lr!, {r0, r1, r2, r3}
	mov	ip, r4
	stmia	ip!, {r0, r1, r2, r3}
	ldmia	lr!, {r0, r1, r2, r3}
	mov	r6, ip
	stmia	r6!, {r0, r1, r2, r3}
	ldmia	lr, {r0, r1, r2, r3}
	stmia	r6, {r0, r1, r2, r3}
	ldrb	r2, [sp, #68]	@ zero_extendqisi2
	cmp	r2, #0
	bne	.L1395
.L1356:
	mov	lr, #32768
	mov	r1, #0
	add	r2, lr, #116
	mov	r3, #3
	mov	r0, #1
	str	r4, [sp, #76]
	str	r1, [sp, #84]
	str	r2, [sp, #88]
	str	r3, [sp, #92]
	strb	r0, [sp, #68]
	str	r1, [sp, #80]
.L1324:
	cmp	r5, #0
	movne	r0, r5
	blne	free
.L1367:
.L1333:
.L1330:
.LEHB68:
	bl	glPushMatrix
	ldr	r1, [sp, #56]
	mov	ip, #1
	cmp	r1, #4
	strb	ip, [sp, #52]
	beq	.L1396
.L1335:
	add	r0, sp, #16
	ldr	r2, [sp, #16]
	ldr	ip, [r2, #8]
	mov	lr, pc
	bx	ip
	add	r0, sp, #16
	ldr	ip, [sp, #16]
	ldr	ip, [ip, #12]
	mov	lr, pc
	bx	ip
	ldrb	r0, [sp, #52]	@ zero_extendqisi2
	cmp	r0, #0
	bne	.L1397
.L1338:
	mov	lr, #3040
	add	r0, lr, #2
	bl	glEnable
	mov	r3, #768
	add	r0, r3, #2
	add	r1, r3, #3
	bl	glBlendFunc
	mov	r0, #0
	bl	glDepthMask
	mov	r2, #2928
	add	r0, r2, #1
	bl	glDisable
	mov	r0, #2896
	bl	glDisable
	add	r0, sp, #60
	ldr	ip, [sp, #60]
	ldr	ip, [ip, #8]
	mov	lr, pc
	bx	ip
	ldr	r2, [sp, #64]
	cmp	r2, #0
	ble	.L1339
	ldrb	r0, [sp, #68]	@ zero_extendqisi2
	cmp	r0, #0
	bne	.L1398
.L1339:
	add	r0, sp, #60
	ldr	r1, [sp, #60]
	ldr	ip, [r1, #12]
	mov	lr, pc
	bx	ip
	mov	r0, #2896
	bl	glEnable
	mov	lr, #2928
	add	r0, lr, #1
	bl	glEnable
	mov	r0, #1
	bl	glDepthMask
	mov	r3, #3040
	add	r0, r3, #2
	bl	glDisable
	bl	glPopMatrix
.LEHE68:
	ldr	r0, [sp, #204]
	ldr	ip, .L1400+20
	ldr	r2, .L1400+24
	cmp	r0, #0
	str	ip, [sp, #216]
	str	r2, [sp, #60]
	bne	.L1399
.L1345:
	ldr	r0, [sp, #200]
	cmp	r0, #0
	blne	free
.L1348:
	ldr	lr, .L1400+28
	add	r0, sp, #60
	str	lr, [sp, #60]
.LEHB69:
	bl	_ZN12QuDrawObjectD2Ev
.LEHE69:
	add	sp, sp, #288
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L1285:
	ldr	lr, [r2, #88]
	ldr	r1, .L1400+32
	mov	r0, lr
	ldr	r3, [lr, #0]
	mov	r2, #49
.LEHB70:
	ldr	ip, [r3, #44]
	mov	lr, pc
	bx	ip
.LEHE70:
	ldr	r2, [r4, #0]
	ldr	r8, [r2, #-12]
	cmp	r0, #49
	mov	r1, #0
	add	r0, r7, r8
	str	r1, [r0, #28]
	bne	.L1353
	ldr	r6, [sp, #280]
	ldr	r1, [r6, #0]
	ldr	r3, [r1, #-12]
	add	r3, r6, r3
	ldr	lr, [r3, #4]
	tst	lr, #8192
	beq	.L1293
.L1388:
	ldr	r3, [r3, #88]
	cmp	r3, #0
	beq	.L1293
	mov	r0, r3
	ldr	sl, [r3, #0]
.LEHB71:
	ldr	ip, [sl, #20]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	bne	.L1293
	ldr	r2, [r6, #0]
	ldr	r0, [r2, #-12]
	add	r0, r6, r0
	ldr	r8, [r0, #8]
	ldr	ip, [r0, #20]
	orr	r6, r8, #1
	tst	r6, ip
	str	r6, [r0, #8]
	beq	.L1293
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
.LEHE71:
	b	.L1293
.L1398:
	ldr	r0, [sp, #192]
	mov	r1, #0
.LEHB72:
	bl	glDrawArrays
	b	.L1339
.L1397:
	ldr	r0, [sp, #40]
	bl	__aeabi_i2f
	mov	r4, r0
	ldr	r0, [sp, #44]
	bl	__aeabi_i2f
	mov	r2, #1065353216
	mov	r1, r0
	mov	r0, r4
	bl	glScalef
.LEHE72:
	b	.L1338
.L1399:
	ldr	lr, [sp, #200]
	add	r0, sp, #200
	ldr	r1, [lr, #4]
.LEHB73:
	bl	_ZN4_STL8_Rb_treeIiNS_4pairIKi14QuArrayPointerIfEEENS_10_Select1stIS5_EENS_4lessIiEENS_9allocatorIS5_EEE8_M_eraseEPNS_13_Rb_tree_nodeIS5_EE
.LEHE73:
	ldr	ip, [sp, #200]
	str	ip, [ip, #8]
	ldr	r2, [sp, #200]
	mov	r1, #0
	str	r1, [r2, #4]
	ldr	r3, [sp, #200]
	str	r3, [r3, #12]
	str	r1, [sp, #204]
	b	.L1345
.L1395:
	ldr	r0, [sp, #76]
	cmp	r0, #0
	beq	.L1356
	bl	_ZdaPv
	b	.L1356
.L1387:
.LEHB74:
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
	b	.L1284
.L1385:
	ldr	ip, [r2, #88]
	ldr	r1, .L1400+32
	mov	r0, ip
	ldr	sl, [ip, #0]
	mov	r2, #49
	ldr	ip, [sl, #44]
	mov	lr, pc
	bx	ip
	cmp	r0, #49
	bne	.L1289
	ldr	r0, [r4, #0]
	ldr	r1, [r0, #-12]
	add	lr, r6, r1
	ldr	r3, [lr, #88]
	ldrb	r1, [lr, #84]	@ zero_extendqisi2
	mov	r0, r3
	ldr	r6, [r3, #0]
	mov	r2, r8
	ldr	ip, [r6, #48]
	mov	lr, pc
	bx	ip
.LEHE74:
	cmp	r8, r0
	bne	.L1289
.L1290:
	ldr	r0, [r4, #0]
	ldr	r8, [r0, #-12]
	mov	sl, #0
	add	r6, r7, r8
	str	sl, [r6, #28]
	b	.L1284
.L1392:
	ldr	r0, [r4, #0]
	ldr	r1, [r0, #-12]
	add	r0, r8, r1
	ldr	lr, [r0, #8]
	ldr	r8, [r0, #20]
	orr	r4, lr, #1
	tst	r4, r8
	str	r4, [r0, #8]
	beq	.L1311
.LEHB75:
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
.LEHE75:
	b	.L1311
.L1396:
.LEHB76:
	bl	_ZN8QuGlobal6getRefEv
	bl	_ZN8QuGlobal17getDeviceRotationEv
.LEHE76:
	cmp	r0, #1
	cmpne	r0, #3
	ldreq	r3, [sp, #32]
	streq	r3, [sp, #44]
	ldreq	r3, [sp, #36]
	streq	r3, [sp, #40]
	cmp	r0, #0
	cmpne	r0, #2
	ldreq	r3, [sp, #36]
	streq	r3, [sp, #44]
	ldreq	r3, [sp, #32]
	streq	r3, [sp, #40]
	ldr	r3, [r7, r0, asl #2]
	str	r3, [sp, #48]
	b	.L1335
.L1386:
	ldr	lr, [r4, #0]
	ldr	r3, [lr, #-12]
	add	r6, r6, r3
	ldr	r2, [r6, #88]
	ldr	r1, .L1400+32
	mov	r0, r2
	ldr	r8, [r2, #0]
	mov	r2, #49
.LEHB77:
	ldr	ip, [r8, #44]
	mov	lr, pc
	bx	ip
.LEHE77:
	cmp	r0, #49
	bne	.L1289
	b	.L1290
.L1300:
	ldr	ip, [r6, #0]
	ldr	r0, [ip, #-12]
	add	r0, r6, r0
	ldr	r3, [r0, #8]
	ldr	r1, [r0, #20]
	orr	r6, r3, #1
	tst	r6, r1
	str	r6, [r0, #8]
	beq	.L1377
.LEHB78:
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
	b	.L1377
.L1280:
	ldr	r1, [r6, #0]
	ldr	lr, [r1, #-12]
	add	r0, r6, lr
	ldr	ip, [r0, #8]
	ldr	sl, [r0, #20]
	orr	r6, ip, #1
	tst	r6, sl
	str	r6, [r0, #8]
	beq	.L1375
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
.LEHE78:
	b	.L1375
.L1389:
	ldr	r1, [r0, #20]
	mov	r2, #1
	tst	r2, r1
	str	r2, [r0, #8]
	bne	.L1298
.L1376:
	ldr	r0, [r4, #0]
	ldr	r2, [r0, #-12]
	add	r3, r8, r2
	b	.L1297
.L1394:
	ldr	r8, [r2, #20]
	mov	r0, #1
	tst	r0, r8
	str	r0, [r2, #8]
	bne	.L1278
	ldr	ip, [r4, #0]
	ldr	sl, [ip, #-12]
	add	r2, r4, sl
	b	.L1277
.L1393:
	mov	r0, r8
.LEHB79:
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
.LEHE79:
	b	.L1317
.L1384:
	mov	r0, #17
.LEHB80:
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
.LEHE80:
	mov	r5, r0
	b	.L1272
.L1298:
.LEHB81:
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
	b	.L1376
.L1278:
	mov	r0, r2
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
.LEHE81:
	ldr	r1, [r4, #0]
	ldr	lr, [r1, #-12]
	add	r2, r4, lr
	b	.L1277
.L1365:
.L1379:
	mov	r4, r0
.L1341:
	ldr	r5, .L1400+20
	add	r0, sp, #60
	str	r5, [sp, #216]
.LEHB82:
	bl	_ZN21QuColorableDrawObjectIiED1Ev
.LEHE82:
.L1343:
	mov	r0, r4
.L1380:
.LEHB83:
	bl	__cxa_end_cleanup
.L1361:
	mov	r4, r0
.L1351:
	mov	r0, r4
	bl	__cxa_end_cleanup
.LEHE83:
.L1360:
.L1292:
	mov	r4, r0
	add	r0, sp, #280
	bl	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
.L1331:
	cmp	r5, #0
	beq	.L1341
	mov	r0, r5
	bl	free
	b	.L1341
.L1368:
	b	.L1379
.L1366:
	mov	r4, r0
	b	.L1331
.L1357:
.L1346:
	ldr	r3, [sp, #200]
	cmp	r3, #0
	mov	r4, r0
	movne	r0, r3
	blne	free
.L1347:
.L1358:
.L1350:
	ldr	r9, .L1400+28
	add	r0, sp, #60
	str	r9, [sp, #60]
	bl	_ZN12QuDrawObjectD2Ev
	b	.L1351
.L1364:
.L1382:
.L1326:
	ldr	r3, [sp, #268]
	cmp	r3, #0
	mov	r4, r0
	beq	.L1331
	mov	r0, r3
	bl	free
	b	.L1331
.L1363:
	b	.L1382
.L1359:
.L1312:
	mov	r4, r0
	add	r0, sp, #280
	bl	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	b	.L1331
.L1362:
	b	.L1380
.L1401:
	.align	2
.L1400:
	.word	.LC0
	.word	_ZN4_STL4coutE
	.word	.LANCHOR1
	.word	.LC2
	.word	.LANCHOR1+80
	.word	_ZTV7QuTimer+8
	.word	_ZTV21QuColorableDrawObjectIiE+8
	.word	_ZTV17QuImageDrawObject+8
	.word	.LC1
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA3873:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3873-.LLSDACSB3873
.LLSDACSB3873:
	.uleb128 .LEHB60-.LFB3873
	.uleb128 .LEHE60-.LEHB60
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB61-.LFB3873
	.uleb128 .LEHE61-.LEHB61
	.uleb128 .L1360-.LFB3873
	.uleb128 0x0
	.uleb128 .LEHB62-.LFB3873
	.uleb128 .LEHE62-.LEHB62
	.uleb128 .L1366-.LFB3873
	.uleb128 0x0
	.uleb128 .LEHB63-.LFB3873
	.uleb128 .LEHE63-.LEHB63
	.uleb128 .L1359-.LFB3873
	.uleb128 0x0
	.uleb128 .LEHB64-.LFB3873
	.uleb128 .LEHE64-.LEHB64
	.uleb128 .L1366-.LFB3873
	.uleb128 0x0
	.uleb128 .LEHB65-.LFB3873
	.uleb128 .LEHE65-.LEHB65
	.uleb128 .L1363-.LFB3873
	.uleb128 0x0
	.uleb128 .LEHB66-.LFB3873
	.uleb128 .LEHE66-.LEHB66
	.uleb128 .L1364-.LFB3873
	.uleb128 0x0
	.uleb128 .LEHB67-.LFB3873
	.uleb128 .LEHE67-.LEHB67
	.uleb128 .L1366-.LFB3873
	.uleb128 0x0
	.uleb128 .LEHB68-.LFB3873
	.uleb128 .LEHE68-.LEHB68
	.uleb128 .L1368-.LFB3873
	.uleb128 0x0
	.uleb128 .LEHB69-.LFB3873
	.uleb128 .LEHE69-.LEHB69
	.uleb128 .L1361-.LFB3873
	.uleb128 0x0
	.uleb128 .LEHB70-.LFB3873
	.uleb128 .LEHE70-.LEHB70
	.uleb128 .L1360-.LFB3873
	.uleb128 0x0
	.uleb128 .LEHB71-.LFB3873
	.uleb128 .LEHE71-.LEHB71
	.uleb128 .L1366-.LFB3873
	.uleb128 0x0
	.uleb128 .LEHB72-.LFB3873
	.uleb128 .LEHE72-.LEHB72
	.uleb128 .L1368-.LFB3873
	.uleb128 0x0
	.uleb128 .LEHB73-.LFB3873
	.uleb128 .LEHE73-.LEHB73
	.uleb128 .L1357-.LFB3873
	.uleb128 0x0
	.uleb128 .LEHB74-.LFB3873
	.uleb128 .LEHE74-.LEHB74
	.uleb128 .L1360-.LFB3873
	.uleb128 0x0
	.uleb128 .LEHB75-.LFB3873
	.uleb128 .LEHE75-.LEHB75
	.uleb128 .L1366-.LFB3873
	.uleb128 0x0
	.uleb128 .LEHB76-.LFB3873
	.uleb128 .LEHE76-.LEHB76
	.uleb128 .L1368-.LFB3873
	.uleb128 0x0
	.uleb128 .LEHB77-.LFB3873
	.uleb128 .LEHE77-.LEHB77
	.uleb128 .L1360-.LFB3873
	.uleb128 0x0
	.uleb128 .LEHB78-.LFB3873
	.uleb128 .LEHE78-.LEHB78
	.uleb128 .L1366-.LFB3873
	.uleb128 0x0
	.uleb128 .LEHB79-.LFB3873
	.uleb128 .LEHE79-.LEHB79
	.uleb128 .L1363-.LFB3873
	.uleb128 0x0
	.uleb128 .LEHB80-.LFB3873
	.uleb128 .LEHE80-.LEHB80
	.uleb128 .L1365-.LFB3873
	.uleb128 0x0
	.uleb128 .LEHB81-.LFB3873
	.uleb128 .LEHE81-.LEHB81
	.uleb128 .L1366-.LFB3873
	.uleb128 0x0
	.uleb128 .LEHB82-.LFB3873
	.uleb128 .LEHE82-.LEHB82
	.uleb128 .L1362-.LFB3873
	.uleb128 0x0
	.uleb128 .LEHB83-.LFB3873
	.uleb128 .LEHE83-.LEHB83
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE3873:
	.fnend
	.size	_ZN7testApp21drawDummyLoadingImageEv, .-_ZN7testApp21drawDummyLoadingImageEv
	.section	.text._ZN7testApp4drawEv,"ax",%progbits
	.align	2
	.global	_ZN7testApp4drawEv
	.hidden	_ZN7testApp4drawEv
	.type	_ZN7testApp4drawEv, %function
_ZN7testApp4drawEv:
	.fnstart
.LFB3875:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	mov	r0, #16640
	bl	glClear
	ldr	r3, [r4, #4]
	cmp	r3, #0
	mov	r0, r4
	beq	.L1406
	ldmfd	sp!, {r4, lr}
	b	_ZN7testApp21drawDummyLoadingImageEv
.L1406:
	bl	_ZN7testApp21drawDummyLoadingImageEv
	ldr	r1, [r4, #4]
	add	r0, r1, #1
	str	r0, [r4, #4]
	ldmfd	sp!, {r4, lr}
	bx	lr
	.fnend
	.size	_ZN7testApp4drawEv, .-_ZN7testApp4drawEv
	.hidden	_ZTV21QuGuiRenderingContext
	.weak	_ZTV21QuGuiRenderingContext
	.section	.rodata._ZTV21QuGuiRenderingContext,"aG",%progbits,_ZTV21QuGuiRenderingContext,comdat
	.align	3
	.type	_ZTV21QuGuiRenderingContext, %object
	.size	_ZTV21QuGuiRenderingContext, 16
_ZTV21QuGuiRenderingContext:
	.word	0
	.word	_ZTI21QuGuiRenderingContext
	.word	_ZN21QuGuiRenderingContext6enableEv
	.word	_ZN21QuGuiRenderingContext7disableEv
	.hidden	_ZTI21QuGuiRenderingContext
	.weak	_ZTI21QuGuiRenderingContext
	.section	.rodata._ZTI21QuGuiRenderingContext,"aG",%progbits,_ZTI21QuGuiRenderingContext,comdat
	.align	2
	.type	_ZTI21QuGuiRenderingContext, %object
	.size	_ZTI21QuGuiRenderingContext, 24
_ZTI21QuGuiRenderingContext:
	.word	_ZTVN10__cxxabiv121__vmi_class_type_infoE+8
	.word	_ZTS21QuGuiRenderingContext
	.word	0
	.word	1
	.word	_ZTI9QuContext
	.word	0
	.hidden	_ZTS21QuGuiRenderingContext
	.weak	_ZTS21QuGuiRenderingContext
	.section	.rodata._ZTS21QuGuiRenderingContext,"aG",%progbits,_ZTS21QuGuiRenderingContext,comdat
	.align	2
	.type	_ZTS21QuGuiRenderingContext, %object
	.size	_ZTS21QuGuiRenderingContext, 24
_ZTS21QuGuiRenderingContext:
	.ascii	"21QuGuiRenderingContext\000"
	.hidden	_ZTI9QuContext
	.weak	_ZTI9QuContext
	.section	.rodata._ZTI9QuContext,"aG",%progbits,_ZTI9QuContext,comdat
	.align	2
	.type	_ZTI9QuContext, %object
	.size	_ZTI9QuContext, 8
_ZTI9QuContext:
	.word	_ZTVN10__cxxabiv117__class_type_infoE+8
	.word	_ZTS9QuContext
	.hidden	_ZTS9QuContext
	.weak	_ZTS9QuContext
	.section	.rodata._ZTS9QuContext,"aG",%progbits,_ZTS9QuContext,comdat
	.align	2
	.type	_ZTS9QuContext, %object
	.size	_ZTS9QuContext, 11
_ZTS9QuContext:
	.ascii	"9QuContext\000"
	.hidden	_ZTV9QuContext
	.weak	_ZTV9QuContext
	.section	.rodata._ZTV9QuContext,"aG",%progbits,_ZTV9QuContext,comdat
	.align	3
	.type	_ZTV9QuContext, %object
	.size	_ZTV9QuContext, 16
_ZTV9QuContext:
	.word	0
	.word	_ZTI9QuContext
	.word	__cxa_pure_virtual
	.word	_ZN9QuContext7disableEv
	.hidden	_ZTV13QuBasicCamera
	.weak	_ZTV13QuBasicCamera
	.section	.rodata._ZTV13QuBasicCamera,"aG",%progbits,_ZTV13QuBasicCamera,comdat
	.align	3
	.type	_ZTV13QuBasicCamera, %object
	.size	_ZTV13QuBasicCamera, 24
_ZTV13QuBasicCamera:
	.word	0
	.word	_ZTI13QuBasicCamera
	.word	_ZN13QuBasicCameraD1Ev
	.word	_ZN13QuBasicCameraD0Ev
	.word	_ZN13QuBasicCamera19setProjectionMatrixEv
	.word	_ZN13QuBasicCamera18setModelViewMatrixEv
	.hidden	_ZTI13QuBasicCamera
	.weak	_ZTI13QuBasicCamera
	.section	.rodata._ZTI13QuBasicCamera,"aG",%progbits,_ZTI13QuBasicCamera,comdat
	.align	2
	.type	_ZTI13QuBasicCamera, %object
	.size	_ZTI13QuBasicCamera, 8
_ZTI13QuBasicCamera:
	.word	_ZTVN10__cxxabiv117__class_type_infoE+8
	.word	_ZTS13QuBasicCamera
	.hidden	_ZTS13QuBasicCamera
	.weak	_ZTS13QuBasicCamera
	.section	.rodata._ZTS13QuBasicCamera,"aG",%progbits,_ZTS13QuBasicCamera,comdat
	.align	2
	.type	_ZTS13QuBasicCamera, %object
	.size	_ZTS13QuBasicCamera, 16
_ZTS13QuBasicCamera:
	.ascii	"13QuBasicCamera\000"
	.hidden	_ZTV21QuColorableDrawObjectIiE
	.weak	_ZTV21QuColorableDrawObjectIiE
	.section	.rodata._ZTV21QuColorableDrawObjectIiE,"aG",%progbits,_ZTV21QuColorableDrawObjectIiE,comdat
	.align	3
	.type	_ZTV21QuColorableDrawObjectIiE, %object
	.size	_ZTV21QuColorableDrawObjectIiE, 24
_ZTV21QuColorableDrawObjectIiE:
	.word	0
	.word	_ZTI21QuColorableDrawObjectIiE
	.word	_ZN21QuColorableDrawObjectIiED1Ev
	.word	_ZN21QuColorableDrawObjectIiED0Ev
	.word	_ZN21QuColorableDrawObjectIiE6enableEv
	.word	_ZN21QuColorableDrawObjectIiE7disableEv
	.hidden	_ZTI21QuColorableDrawObjectIiE
	.weak	_ZTI21QuColorableDrawObjectIiE
	.section	.rodata._ZTI21QuColorableDrawObjectIiE,"aG",%progbits,_ZTI21QuColorableDrawObjectIiE,comdat
	.align	2
	.type	_ZTI21QuColorableDrawObjectIiE, %object
	.size	_ZTI21QuColorableDrawObjectIiE, 12
_ZTI21QuColorableDrawObjectIiE:
	.word	_ZTVN10__cxxabiv120__si_class_type_infoE+8
	.word	_ZTS21QuColorableDrawObjectIiE
	.word	_ZTI17QuImageDrawObject
	.hidden	_ZTS21QuColorableDrawObjectIiE
	.weak	_ZTS21QuColorableDrawObjectIiE
	.section	.rodata._ZTS21QuColorableDrawObjectIiE,"aG",%progbits,_ZTS21QuColorableDrawObjectIiE,comdat
	.align	2
	.type	_ZTS21QuColorableDrawObjectIiE, %object
	.size	_ZTS21QuColorableDrawObjectIiE, 27
_ZTS21QuColorableDrawObjectIiE:
	.ascii	"21QuColorableDrawObjectIiE\000"
	.hidden	_ZTI17QuImageDrawObject
	.weak	_ZTI17QuImageDrawObject
	.section	.rodata._ZTI17QuImageDrawObject,"aG",%progbits,_ZTI17QuImageDrawObject,comdat
	.align	2
	.type	_ZTI17QuImageDrawObject, %object
	.size	_ZTI17QuImageDrawObject, 12
_ZTI17QuImageDrawObject:
	.word	_ZTVN10__cxxabiv120__si_class_type_infoE+8
	.word	_ZTS17QuImageDrawObject
	.word	_ZTI12QuDrawObject
	.hidden	_ZTS17QuImageDrawObject
	.weak	_ZTS17QuImageDrawObject
	.section	.rodata._ZTS17QuImageDrawObject,"aG",%progbits,_ZTS17QuImageDrawObject,comdat
	.align	2
	.type	_ZTS17QuImageDrawObject, %object
	.size	_ZTS17QuImageDrawObject, 20
_ZTS17QuImageDrawObject:
	.ascii	"17QuImageDrawObject\000"
	.hidden	_ZTI12QuDrawObject
	.weak	_ZTI12QuDrawObject
	.section	.rodata._ZTI12QuDrawObject,"aG",%progbits,_ZTI12QuDrawObject,comdat
	.align	2
	.type	_ZTI12QuDrawObject, %object
	.size	_ZTI12QuDrawObject, 8
_ZTI12QuDrawObject:
	.word	_ZTVN10__cxxabiv117__class_type_infoE+8
	.word	_ZTS12QuDrawObject
	.hidden	_ZTS12QuDrawObject
	.weak	_ZTS12QuDrawObject
	.section	.rodata._ZTS12QuDrawObject,"aG",%progbits,_ZTS12QuDrawObject,comdat
	.align	2
	.type	_ZTS12QuDrawObject, %object
	.size	_ZTS12QuDrawObject, 15
_ZTS12QuDrawObject:
	.ascii	"12QuDrawObject\000"
	.hidden	_ZTV17QuImageDrawObject
	.weak	_ZTV17QuImageDrawObject
	.section	.rodata._ZTV17QuImageDrawObject,"aG",%progbits,_ZTV17QuImageDrawObject,comdat
	.align	3
	.type	_ZTV17QuImageDrawObject, %object
	.size	_ZTV17QuImageDrawObject, 24
_ZTV17QuImageDrawObject:
	.word	0
	.word	_ZTI17QuImageDrawObject
	.word	_ZN17QuImageDrawObjectD1Ev
	.word	_ZN17QuImageDrawObjectD0Ev
	.word	_ZN12QuDrawObject6enableEv
	.word	_ZN12QuDrawObject7disableEv
	.hidden	_ZTV12QuDrawObject
	.weak	_ZTV12QuDrawObject
	.section	.rodata._ZTV12QuDrawObject,"aG",%progbits,_ZTV12QuDrawObject,comdat
	.align	3
	.type	_ZTV12QuDrawObject, %object
	.size	_ZTV12QuDrawObject, 24
_ZTV12QuDrawObject:
	.word	0
	.word	_ZTI12QuDrawObject
	.word	_ZN12QuDrawObjectD1Ev
	.word	_ZN12QuDrawObjectD0Ev
	.word	_ZN12QuDrawObject6enableEv
	.word	_ZN12QuDrawObject7disableEv
	.hidden	_ZTV7QuTimer
	.weak	_ZTV7QuTimer
	.section	.rodata._ZTV7QuTimer,"aG",%progbits,_ZTV7QuTimer,comdat
	.align	3
	.type	_ZTV7QuTimer, %object
	.size	_ZTV7QuTimer, 24
_ZTV7QuTimer:
	.word	0
	.word	_ZTI7QuTimer
	.word	_ZN7QuTimerD1Ev
	.word	_ZN7QuTimerD0Ev
	.word	_ZN7QuTimer6updateEv
	.word	_ZN7QuTimer5resetEv
	.hidden	_ZTI7QuTimer
	.weak	_ZTI7QuTimer
	.section	.rodata._ZTI7QuTimer,"aG",%progbits,_ZTI7QuTimer,comdat
	.align	2
	.type	_ZTI7QuTimer, %object
	.size	_ZTI7QuTimer, 8
_ZTI7QuTimer:
	.word	_ZTVN10__cxxabiv117__class_type_infoE+8
	.word	_ZTS7QuTimer
	.hidden	_ZTS7QuTimer
	.weak	_ZTS7QuTimer
	.section	.rodata._ZTS7QuTimer,"aG",%progbits,_ZTS7QuTimer,comdat
	.align	2
	.type	_ZTS7QuTimer, %object
	.size	_ZTS7QuTimer, 9
_ZTS7QuTimer:
	.ascii	"7QuTimer\000"
	.section	.rodata
	.align	2
	.set	.LANCHOR1,. + 0
	.type	_ZL24DEVICE_ROTATION_TO_ANGLE, %object
	.size	_ZL24DEVICE_ROTATION_TO_ANGLE, 16
_ZL24DEVICE_ROTATION_TO_ANGLE:
	.word	0
	.word	270
	.word	180
	.word	90
	.type	_ZZN18QuTintableGuiImageC1EvE5C.339, %object
	.size	_ZZN18QuTintableGuiImageC1EvE5C.339, 16
_ZZN18QuTintableGuiImageC1EvE5C.339:
	.word	1058642330
	.word	1058642330
	.word	1061997773
	.word	1065353216
	.type	_ZZN18QuTintableGuiImageC1EvE5C.340, %object
	.size	_ZZN18QuTintableGuiImageC1EvE5C.340, 16
_ZZN18QuTintableGuiImageC1EvE5C.340:
	.word	1061997773
	.word	1058642330
	.word	1058642330
	.word	1065353216
	.type	_ZL11GUI_RECT_UV, %object
	.size	_ZL11GUI_RECT_UV, 32
_ZL11GUI_RECT_UV:
	.word	0
	.word	0
	.word	0
	.word	1065353216
	.word	1065353216
	.word	0
	.word	1065353216
	.word	1065353216
	.type	_ZL15GUI_RECT_COORDS, %object
	.size	_ZL15GUI_RECT_COORDS, 48
_ZL15GUI_RECT_COORDS:
	.word	-1090519040
	.word	1056964608
	.word	0
	.word	1056964608
	.word	1056964608
	.word	0
	.word	-1090519040
	.word	-1090519040
	.word	0
	.word	1056964608
	.word	-1090519040
	.word	0
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"images/image.png\000"
	.space	3
.LC1:
	.ascii	"loading QuTintableImage by filename is deprecated\000"
	.space	2
.LC2:
	.ascii	"basic_string\000"
	.bss
	.align	2
	.set	.LANCHOR0,. + 0
	.type	_ZN4_STLL8_LocInitE, %object
	.size	_ZN4_STLL8_LocInitE, 1
_ZN4_STLL8_LocInitE:
	.space	1
	.space	3
	.type	_ZN4_STLL8_IosInitE, %object
	.size	_ZN4_STLL8_IosInitE, 1
_ZN4_STLL8_IosInitE:
	.space	1
	.hidden	_ZTV7QuTimer
	.hidden	_ZTV21QuColorableDrawObjectIiE
	.hidden	_ZTV21QuGuiRenderingContext
	.hidden	_ZTV9QuContext
	.hidden	_ZTV13QuBasicCamera
	.hidden	_ZTV17QuImageDrawObject
	.hidden	_ZTV12QuDrawObject
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
