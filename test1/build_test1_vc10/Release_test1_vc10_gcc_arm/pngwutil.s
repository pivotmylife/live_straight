	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"pngwutil.c"
	.section	.text.png_save_uint_32,"ax",%progbits
	.align	2
	.global	png_save_uint_32
	.hidden	png_save_uint_32
	.type	png_save_uint_32, %function
png_save_uint_32:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	ip, r1, lsr #24
	mov	r2, r1, lsr #16
	mov	r3, r1, lsr #8
	strb	r1, [r0, #3]
	strb	ip, [r0, #0]
	strb	r2, [r0, #1]
	strb	r3, [r0, #2]
	bx	lr
	.size	png_save_uint_32, .-png_save_uint_32
	.section	.text.png_save_int_32,"ax",%progbits
	.align	2
	.global	png_save_int_32
	.hidden	png_save_int_32
	.type	png_save_int_32, %function
png_save_int_32:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	ip, r1, lsr #24
	mov	r2, r1, asr #16
	mov	r3, r1, asr #8
	strb	r1, [r0, #3]
	strb	ip, [r0, #0]
	strb	r2, [r0, #1]
	strb	r3, [r0, #2]
	bx	lr
	.size	png_save_int_32, .-png_save_int_32
	.section	.text.png_save_uint_16,"ax",%progbits
	.align	2
	.global	png_save_uint_16
	.hidden	png_save_uint_16
	.type	png_save_uint_16, %function
png_save_uint_16:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, r1, lsr #8
	strb	r1, [r0, #1]
	strb	r3, [r0, #0]
	bx	lr
	.size	png_save_uint_16, .-png_save_uint_16
	.global	__aeabi_uidiv
	.section	.text.png_do_write_interlace,"ax",%progbits
	.align	2
	.global	png_do_write_interlace
	.hidden	png_do_write_interlace
	.type	png_do_write_interlace, %function
png_do_write_interlace:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	cmp	r2, #5
	sub	sp, sp, #12
	mov	r8, r0
	bgt	.L37
	ldrb	r5, [r0, #11]	@ zero_extendqisi2
	cmp	r5, #2
	beq	.L11
	cmp	r5, #4
	beq	.L12
	cmp	r5, #1
	beq	.L47
	ldr	r3, .L50
	ldr	r3, [r3, r2, asl #2]
	str	r3, [sp, #4]
	ldr	r9, [r0, #0]
	cmp	r9, r3
	mov	sl, r5, lsr #3
	bls	.L48
	ldr	r6, .L50+4
	ldr	r4, [r6, r2, asl #2]
	ldr	r2, [sp, #4]
	mov	r5, r3
	mla	r5, sl, r5, r1
	mul	fp, sl, r4
	mov	r6, r1
	add	r7, r2, r4
	b	.L34
.L49:
	add	r6, r6, sl
.L34:
	cmp	r6, r5
	add	r7, r7, r4
	mov	r1, r5
	mov	r0, r6
	mov	r2, sl
	blne	memcpy
.L32:
	rsb	lr, r4, r7
	cmp	r9, lr
	add	r5, r5, fp
	bhi	.L49
	ldr	r9, [r8, #0]
	ldrb	r5, [r8, #11]	@ zero_extendqisi2
	ldr	r3, [sp, #4]
.L14:
	add	r9, r9, r4
	sub	r1, r9, #1
	rsb	r0, r3, r1
	mov	r1, r4
	bl	__aeabi_uidiv
	cmp	r5, #7
	str	r0, [r8, #0]
	mulls	r0, r5, r0
	movhi	r5, r5, lsr #3
	mulhi	r0, r5, r0
	addls	r0, r0, #7
	movls	r0, r0, lsr #3
	str	r0, [r8, #4]
.L37:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L11:
	ldr	r6, .L50
	ldr	r9, [r0, #0]
	ldr	r3, [r6, r2, asl #2]
	cmp	r9, r3
	bls	.L45
	ldr	r5, .L50+4
	mov	ip, #0
	ldr	r7, [r5, r2, asl #2]
	mov	r0, #6
	mov	r5, r1
.L22:
	mvn	r4, r3
	and	lr, r4, #3
	ldrb	r4, [r1, r3, lsr #2]	@ zero_extendqisi2
	mov	lr, lr, asl #1
	mov	lr, r4, asr lr
	and	r4, lr, #3
	orr	ip, ip, r4, asl r0
	cmp	r0, #0
	add	r3, r3, r7
	streqb	ip, [r5], #1
	subne	r0, r0, #2
	moveq	ip, r0
	moveq	r0, #6
	cmp	r9, r3
	mov	r4, r7
	bhi	.L22
	cmp	r0, #6
	beq	.L44
.L23:
	strb	ip, [r5, #0]
.L44:
	ldr	r6, [r6, r2, asl #2]
	str	r6, [sp, #4]
	mov	r3, r6
	ldr	r9, [r8, #0]
	ldrb	r5, [r8, #11]	@ zero_extendqisi2
	b	.L14
.L47:
	ldr	r6, .L50
	ldr	r9, [r0, #0]
	ldr	r3, [r6, r2, asl #2]
	cmp	r9, r3
	bls	.L45
	ldr	r0, .L50+4
	mov	r5, r1
	ldr	r7, [r0, r2, asl #2]
	mov	ip, #0
	mov	r0, #7
.L17:
	ldrb	r4, [r1, r3, lsr #3]	@ zero_extendqisi2
	mvn	lr, r3
	and	lr, lr, #7
	mov	lr, r4, asr lr
	and	lr, lr, #1
	orr	ip, ip, lr, asl r0
	cmp	r0, #0
	add	r3, r3, r7
	streqb	ip, [r5], #1
	subne	r0, r0, #1
	moveq	ip, r0
	moveq	r0, #7
	cmp	r9, r3
	mov	r4, r7
	bhi	.L17
	cmp	r0, #7
	bne	.L23
	b	.L44
.L12:
	ldr	r6, .L50
	ldr	r9, [r0, #0]
	ldr	r3, [r6, r2, asl #2]
	cmp	r9, r3
	bls	.L45
	ldr	r7, .L50+4
	mov	r0, #0
	ldr	lr, [r7, r2, asl #2]
	mov	r7, r1
.L29:
	ldrb	r4, [r1, r3, lsr #1]	@ zero_extendqisi2
	tst	r3, #1
	movne	ip, #0
	moveq	ip, #4
	mov	ip, r4, asr ip
	and	ip, ip, #15
	orr	r0, r0, ip, asl r5
	cmp	r5, #0
	add	r3, r3, lr
	streqb	r0, [r7], #1
	subne	r5, r5, #4
	moveq	r0, r5
	moveq	r5, #4
	cmp	r9, r3
	mov	r4, lr
	bhi	.L29
	cmp	r5, #4
	strneb	r0, [r7, #0]
	b	.L44
.L48:
	ldr	r0, .L50+4
	ldr	r3, [sp, #4]
	ldr	r4, [r0, r2, asl #2]
	b	.L14
.L45:
	ldr	r1, .L50+4
	str	r3, [sp, #4]
	ldr	r4, [r1, r2, asl #2]
	b	.L14
.L51:
	.align	2
.L50:
	.word	png_pass_start
	.word	png_pass_inc
	.size	png_do_write_interlace, .-png_do_write_interlace
	.section	.text.png_write_start_row,"ax",%progbits
	.align	2
	.global	png_write_start_row
	.hidden	png_write_start_row
	.type	png_write_start_row, %function
png_write_start_row:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	mov	r4, r0
	ldrb	r1, [r4, #327]	@ zero_extendqisi2
	ldrb	r0, [r0, #324]	@ zero_extendqisi2
	mul	r0, r1, r0
	ldr	r5, [r4, #228]
	cmp	r0, #7
	mulle	r5, r0, r5
	movgt	r0, r0, lsr #3
	mulgt	r5, r0, r5
	addle	r5, r5, #7
	movle	r5, r5, lsr #3
	add	r5, r5, #1
	mov	r0, r4
	mov	r1, r5
	bl	png_malloc
	mov	r3, #0
	str	r0, [r4, #264]
	strb	r3, [r0, #0]
	ldrb	r3, [r4, #321]	@ zero_extendqisi2
	tst	r3, #16
	bne	.L63
.L55:
	tst	r3, #224
	bne	.L64
.L56:
	ldrb	r3, [r4, #319]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L59
	ldr	lr, [r4, #140]
	tst	lr, #2
	beq	.L65
.L59:
	add	r1, r4, #228
	ldmia	r1, {r1, r3}	@ phole ldm
	str	r3, [r4, #236]
	str	r1, [r4, #240]
.L61:
	add	r0, r4, #200
	ldmia	r0, {r0, ip}	@ phole ldm
	str	ip, [r4, #160]
	str	r0, [r4, #156]
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L65:
	ldr	lr, .L68
	ldr	r2, [r4, #232]
	ldr	ip, .L68+4
	ldr	r1, [lr, #0]
	sub	r0, r2, #1
	ldr	r3, [ip, #0]
	add	lr, r0, r1
	rsb	r0, r3, lr
	bl	__aeabi_uidiv
	ldr	r2, .L68+8
	ldr	ip, [r4, #228]
	ldr	r1, [r2, #0]
	ldr	r2, .L68+12
	sub	r3, ip, #1
	str	r0, [r4, #236]
	ldr	r0, [r2, #0]
	add	ip, r3, r1
	rsb	r0, r0, ip
	bl	__aeabi_uidiv
	str	r0, [r4, #240]
	b	.L61
.L64:
	mov	r1, r5
	mov	r0, r4
	bl	png_malloc
	mov	r2, r5
	str	r0, [r4, #260]
	mov	r1, #0
	bl	memset
	ldrb	r3, [r4, #321]	@ zero_extendqisi2
	tst	r3, #32
	bne	.L66
.L57:
	tst	r3, #64
	bne	.L67
.L58:
	tst	r3, #128
	beq	.L56
	ldr	ip, [r4, #244]
	mov	r0, r4
	add	r1, ip, #1
	bl	png_malloc
	mov	r2, #4
	str	r0, [r4, #280]
	strb	r2, [r0, #0]
	b	.L56
.L63:
	ldr	ip, [r4, #244]
	mov	r0, r4
	add	r1, ip, #1
	bl	png_malloc
	mov	r2, #1
	str	r0, [r4, #268]
	strb	r2, [r0, #0]
	ldrb	r3, [r4, #321]	@ zero_extendqisi2
	b	.L55
.L67:
	ldr	r0, [r4, #244]
	add	r1, r0, #1
	mov	r0, r4
	bl	png_malloc
	mov	r1, #3
	str	r0, [r4, #276]
	strb	r1, [r0, #0]
	ldrb	r3, [r4, #321]	@ zero_extendqisi2
	b	.L58
.L66:
	ldr	lr, [r4, #244]
	mov	r0, r4
	add	r1, lr, #1
	bl	png_malloc
	mov	r3, #2
	str	r0, [r4, #272]
	strb	r3, [r0, #0]
	ldrb	r3, [r4, #321]	@ zero_extendqisi2
	b	.L57
.L69:
	.align	2
.L68:
	.word	png_pass_yinc
	.word	png_pass_ystart
	.word	png_pass_inc
	.word	png_pass_start
	.size	png_write_start_row, .-png_write_start_row
	.section	.text.png_text_compress,"ax",%progbits
	.align	2
	.type	png_text_compress, %function
png_text_compress:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 56
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	sub	sp, sp, #60
	ldr	r5, [sp, #88]
	cmn	r3, #1
	mov	ip, #0
	mov	r4, r0
	moveq	r0, r2
	str	ip, [r5, #4]
	str	ip, [r5, #0]
	mov	r6, r3
	mov	r8, r1
	mov	r7, r2
	str	ip, [r5, #8]
	str	ip, [r5, #12]
	str	ip, [r5, #16]
	streq	r1, [r5, #0]
	streq	r0, [r5, #4]
	beq	.L72
	cmp	r3, #2
	bgt	.L95
.L73:
	ldr	r2, [r4, #204]
	ldr	r3, [r4, #200]
	str	r7, [r4, #148]
	str	r8, [r4, #144]
	str	r2, [r4, #160]
	str	r3, [r4, #156]
	add	r6, r4, #144
.L80:
	mov	r0, r6
	mov	r1, #0
	bl	deflate
	cmp	r0, #0
	beq	.L74
	ldr	r1, [r4, #168]
	cmp	r1, #0
	mov	r0, r4
	ldreq	r1, .L97
	bl	png_error
.L74:
	ldr	r0, [r4, #160]
	cmp	r0, #0
	bne	.L76
	add	r8, r5, #8
	ldmia	r8, {r8, sl}	@ phole ldm
	cmp	r8, sl
	ldr	r7, [r5, #16]
	blt	.L78
	add	r8, r8, #4
	cmp	r7, #0
	str	r8, [r5, #12]
	mov	r1, r8, asl #2
	mov	r0, r4
	beq	.L79
	bl	png_malloc
	mov	r1, r7
	mov	r2, sl, asl #2
	str	r0, [r5, #16]
	bl	memcpy
	mov	r1, r7
	mov	r0, r4
	bl	png_free
	ldr	r7, [r5, #16]
	ldr	r8, [r5, #8]
.L78:
	ldr	r1, [r4, #204]
	mov	r0, r4
	bl	png_malloc
	str	r0, [r7, r8, asl #2]
	ldr	r2, [r5, #8]
	ldr	r1, [r5, #16]
	ldr	r0, [r1, r2, asl #2]
	add	r1, r4, #200
	ldmia	r1, {r1, r2}	@ phole ldm
	bl	memcpy
	ldr	r3, [r5, #8]
	add	ip, r3, #1
	str	ip, [r5, #8]
	add	r1, r4, #200
	ldmia	r1, {r1, r7}	@ phole ldm
	str	r7, [r4, #160]
	str	r1, [r4, #156]
.L76:
	ldr	lr, [r4, #148]
	cmp	lr, #0
	bne	.L80
.L94:
	mov	r0, r6
	mov	r1, #4
	bl	deflate
	cmp	r0, #0
	bne	.L81
.L96:
	ldr	r0, [r4, #160]
	cmp	r0, #0
	bne	.L94
	add	r8, r5, #8
	ldmia	r8, {r8, sl}	@ phole ldm
	cmp	r8, sl
	ldr	r7, [r5, #16]
	blt	.L84
	add	ip, r8, #4
	cmp	r7, #0
	str	ip, [r5, #12]
	mov	r1, ip, asl #2
	mov	r0, r4
	beq	.L85
	bl	png_malloc
	mov	r1, r7
	mov	r2, sl, asl #2
	str	r0, [r5, #16]
	bl	memcpy
	mov	r1, r7
	mov	r0, r4
	bl	png_free
	ldr	r7, [r5, #16]
	ldr	r8, [r5, #8]
.L84:
	ldr	r1, [r4, #204]
	mov	r0, r4
	bl	png_malloc
	str	r0, [r7, r8, asl #2]
	ldr	ip, [r5, #8]
	ldr	r7, [r5, #16]
	ldr	r1, [r4, #200]
	ldr	r0, [r7, ip, asl #2]
	ldr	r2, [r4, #204]
	bl	memcpy
	ldr	r1, [r5, #8]
	add	r2, r1, #1
	str	r2, [r5, #8]
	ldr	r3, [r4, #204]
	ldr	r7, [r4, #200]
	str	r3, [r4, #160]
	str	r7, [r4, #156]
	mov	r0, r6
	mov	r1, #4
	bl	deflate
	cmp	r0, #0
	beq	.L96
.L81:
	cmp	r0, #1
	beq	.L86
	ldr	r1, [r4, #168]
	cmp	r1, #0
	mov	r0, r4
	ldreq	r1, .L97
	bl	png_error
	b	.L94
.L86:
	ldr	lr, [r4, #204]
	ldr	r7, [r5, #8]
	mul	r7, lr, r7
	ldr	r0, [r4, #160]
	cmp	lr, r0
	rsbhi	lr, r0, lr
	addhi	r7, r7, lr
	mov	r0, r7
.L72:
	add	sp, sp, #60
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	bx	lr
.L95:
	add	r6, sp, #4
	mov	r0, r6
	mov	r1, #50
	ldr	r2, .L97+4
	bl	snprintf
	mov	r0, r4
	mov	r1, r6
	bl	png_warning
	b	.L73
.L79:
	bl	png_malloc
	ldr	r8, [r5, #8]
	mov	r7, r0
	str	r0, [r5, #16]
	b	.L78
.L85:
	bl	png_malloc
	ldr	r8, [r5, #8]
	mov	r7, r0
	str	r0, [r5, #16]
	b	.L84
.L98:
	.align	2
.L97:
	.word	.LC1
	.word	.LC0
	.size	png_text_compress, .-png_text_compress
	.section	.text.png_check_keyword,"ax",%progbits
	.align	2
	.global	png_check_keyword
	.hidden	png_check_keyword
	.type	png_check_keyword, %function
png_check_keyword:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	mov	r3, #0
	subs	r5, r1, #0
	sub	sp, sp, #40
	mov	r8, r2
	str	r3, [r2, #0]
	mov	r7, r0
	beq	.L100
	mov	r0, r5
	bl	strlen
	subs	r6, r0, #0
	bne	.L101
.L100:
	mov	r0, r7
	ldr	r1, .L135
	bl	png_warning
	mov	r6, #0
.L102:
	mov	r0, r6
	add	sp, sp, #40
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L101:
	mov	r0, r7
	add	r1, r6, #2
	bl	png_malloc_warn
	cmp	r0, #0
	mov	r4, r0
	str	r0, [r8, #0]
	beq	.L103
	ldrb	r3, [r5, #0]	@ zero_extendqisi2
	cmp	r3, #0
	movne	r9, #32
	bne	.L108
	b	.L105
.L107:
	ldrb	r3, [r5, #1]!	@ zero_extendqisi2
	cmp	r3, #0
	add	r4, r4, #1
	beq	.L105
.L108:
	sub	r2, r3, #127
	cmp	r3, #31
	cmphi	r2, #33
	strhib	r3, [r4, #0]
	bhi	.L107
	ldr	r2, .L135+4
	mov	r1, #40
	mov	r0, sp
	bl	snprintf
	mov	r0, r7
	mov	r1, sp
	bl	png_warning
	strb	r9, [r4, #0]
	ldrb	r3, [r5, #1]!	@ zero_extendqisi2
	cmp	r3, #0
	add	r4, r4, #1
	bne	.L108
.L105:
	mov	sl, #0
	strb	sl, [r4, #0]
	ldr	r4, [r8, #0]
	sub	r5, r6, #1
	ldrb	r0, [r4, r5]	@ zero_extendqisi2
	cmp	r0, #32
	add	r9, r4, r5
	beq	.L130
.L109:
	ldrb	r3, [r4, #0]	@ zero_extendqisi2
	cmp	r3, #32
	movne	ip, r4
	beq	.L131
.L113:
	cmp	r3, #0
	streqb	r3, [ip, #0]
	beq	.L120
	mov	sl, #0
	mov	r0, sl
	mov	r5, #32
.L119:
	eor	lr, r0, #1
	cmp	r3, #32
	movne	r2, #0
	moveq	r2, #1
	tst	r2, lr
	strneb	r5, [ip], #1
	movne	r0, #1
	bne	.L117
	cmp	r2, #0
	streqb	r3, [ip], #1
	subne	r6, r6, #1
	movne	sl, #1
	moveq	r0, r2
.L117:
	ldrb	r3, [r4, #1]!	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L119
	cmp	sl, #0
	strb	r3, [ip, #0]
	bne	.L132
.L120:
	cmp	r6, #0
	beq	.L133
	cmp	r6, #79
	bls	.L102
	mov	r0, r7
	ldr	r1, .L135+8
	bl	png_warning
	ldr	ip, [r8, #0]
	mov	r6, #0
	strb	r6, [ip, #79]
	mov	r6, #79
	b	.L102
.L133:
	mov	r0, r7
	ldr	r1, [r8, #0]
	bl	png_free
	str	r6, [r8, #0]
	mov	r0, r7
	ldr	r1, .L135+12
	bl	png_warning
	b	.L102
.L132:
	mov	r0, r7
	ldr	r1, .L135+16
	bl	png_warning
	b	.L120
.L131:
	mov	r0, r7
	ldr	r1, .L135+20
	bl	png_warning
	ldrb	r3, [r4, #0]	@ zero_extendqisi2
	cmp	r3, #32
	bne	.L129
.L124:
	ldrb	r3, [r4, #1]!	@ zero_extendqisi2
	cmp	r3, #32
	sub	r6, r6, #1
	beq	.L124
.L129:
	ldr	ip, [r8, #0]
	b	.L113
.L130:
	ldr	r1, .L135+24
	mov	r0, r7
	bl	png_warning
	ldrb	r1, [r4, r5]	@ zero_extendqisi2
	cmp	r1, #32
	beq	.L111
	b	.L128
.L134:
	sub	r5, r5, #1
.L111:
	strb	sl, [r9, #0]
	ldrb	r4, [r9, #-1]!	@ zero_extendqisi2
	cmp	r4, #32
	mov	r6, r5
	beq	.L134
.L128:
	ldr	r4, [r8, #0]
	b	.L109
.L103:
	mov	r0, r7
	ldr	r1, .L135+28
	bl	png_warning
	mov	r6, r4
	b	.L102
.L136:
	.align	2
.L135:
	.word	.LC2
	.word	.LC4
	.word	.LC9
	.word	.LC8
	.word	.LC7
	.word	.LC6
	.word	.LC5
	.word	.LC3
	.size	png_check_keyword, .-png_check_keyword
	.section	.text.png_write_chunk_end,"ax",%progbits
	.align	2
	.global	png_write_chunk_end
	.hidden	png_write_chunk_end
	.type	png_write_chunk_end, %function
png_write_chunk_end:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	subs	r3, r0, #0
	sub	sp, sp, #8
	beq	.L139
	ldr	r3, [r3, #300]
	add	r1, sp, #4
	mov	r4, r3, lsr #24
	mov	lr, r3, lsr #16
	mov	ip, r3, lsr #8
	mov	r2, #4
	strb	r4, [sp, #4]
	strb	lr, [sp, #5]
	strb	ip, [sp, #6]
	strb	r3, [sp, #7]
	bl	png_write_data
.L139:
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	bx	lr
	.size	png_write_chunk_end, .-png_write_chunk_end
	.section	.text.png_write_sig,"ax",%progbits
	.align	2
	.global	png_write_sig
	.hidden	png_write_sig
	.type	png_write_sig, %function
png_write_sig:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	ldr	r2, .L143
	mov	r4, r0
	ldrb	ip, [r0, #328]	@ zero_extendqisi2
	sub	sp, sp, #8
	ldmia	r2, {r0, r1}
	mov	lr, sp
	stmia	lr, {r0, r1}
	rsb	r2, ip, #8
	add	r1, sp, ip
	mov	r0, r4
	bl	png_write_data
	ldrb	r3, [r4, #328]	@ zero_extendqisi2
	cmp	r3, #2
	ldrls	r3, [r4, #132]
	orrls	r3, r3, #4096
	strls	r3, [r4, #132]
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	bx	lr
.L144:
	.align	2
.L143:
	.word	.LANCHOR0
	.size	png_write_sig, .-png_write_sig
	.section	.text.png_write_chunk_data,"ax",%progbits
	.align	2
	.global	png_write_chunk_data
	.hidden	png_write_chunk_data
	.type	png_write_chunk_data, %function
png_write_chunk_data:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	subs	r6, r0, #0
	mov	r5, r1
	mov	r4, r2
	beq	.L147
	cmp	r2, #0
	cmpne	r1, #0
	beq	.L147
	bl	png_write_data
	mov	r0, r6
	mov	r1, r5
	mov	r2, r4
	bl	png_calculate_crc
.L147:
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
	.size	png_write_chunk_data, .-png_write_chunk_data
	.section	.text.png_write_chunk_start,"ax",%progbits
	.align	2
	.global	png_write_chunk_start
	.hidden	png_write_chunk_start
	.type	png_write_chunk_start, %function
png_write_chunk_start:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	subs	r4, r0, #0
	sub	sp, sp, #12
	mov	r3, r2
	mov	r5, r1
	beq	.L150
	mov	lr, r2, lsr #16
	mov	ip, r2, lsr #8
	mov	r7, r2, lsr #24
	mov	r2, #4
	add	r0, sp, r2
	strb	lr, [sp, #1]
	strb	ip, [sp, #2]
	strb	r3, [sp, #3]
	strb	r7, [sp, #0]
	bl	memcpy
	mov	r0, r4
	mov	r1, sp
	mov	r2, #8
	bl	png_write_data
	mov	r2, #4
	mov	r1, r5
	add	r0, r4, #312
	bl	memcpy
	mov	r0, r4
	bl	png_reset_crc
	mov	r0, r4
	mov	r1, r5
	mov	r2, #4
	mov	r6, sp
	bl	png_calculate_crc
.L150:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
	.size	png_write_chunk_start, .-png_write_chunk_start
	.section	.text.png_write_hIST,"ax",%progbits
	.align	2
	.global	png_write_hIST
	.hidden	png_write_hIST
	.type	png_write_hIST, %function
png_write_hIST:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	mov	r7, #308
	ldrh	r3, [r0, r7]
	cmp	r3, r2
	sub	sp, sp, #12
	mov	r4, r0
	mov	r8, r2
	mov	r7, r1
	blt	.L171
	ldr	r1, .L172
	mov	r2, r2, asl #1
	bl	png_write_chunk_start
	cmp	r8, #0
	ble	.L154
	ldrh	r6, [r7, #0]
	add	r5, sp, #4
	mov	ip, r6, lsr #8
	mov	r2, #2
	mov	r0, r4
	mov	r1, r5
	strb	ip, [sp, #4]
	strb	r6, [sp, #5]
	bl	png_write_data
	mov	r0, r4
	mov	r1, r5
	mov	r2, #2
	sub	sl, r8, #1
	bl	png_calculate_crc
	cmp	r8, #1
	and	sl, sl, #3
	mov	r6, #1
	ble	.L154
	cmp	sl, #0
	beq	.L155
	cmp	sl, #1
	beq	.L169
	cmp	sl, #2
	beq	.L170
	mov	r6, #2
	ldrh	r3, [r7, r6]
	mov	r0, r4
	mov	ip, r3, lsr #8
	mov	r1, r5
	mov	r2, r6
	strb	ip, [sp, #4]
	strb	r3, [sp, #5]
	bl	png_write_data
	mov	r0, r4
	mov	r1, r5
	mov	r2, r6
	bl	png_calculate_crc
.L170:
	mov	r0, r6, asl #1
	ldrh	lr, [r7, r0]
	mov	r2, #2
	mov	ip, lr, lsr #8
	mov	r0, r4
	mov	r1, r5
	strb	ip, [sp, #4]
	strb	lr, [sp, #5]
	bl	png_write_data
	mov	r0, r4
	mov	r1, r5
	mov	r2, #2
	bl	png_calculate_crc
	add	r6, r6, #1
.L169:
	mov	r1, r6, asl #1
	ldrh	r3, [r7, r1]
	mov	r2, #2
	mov	lr, r3, lsr #8
	mov	r0, r4
	mov	r1, r5
	strb	lr, [sp, #4]
	strb	r3, [sp, #5]
	add	r6, r6, #1
	bl	png_write_data
	mov	r0, r4
	mov	r1, r5
	mov	r2, #2
	bl	png_calculate_crc
	cmp	r8, r6
	ble	.L154
.L155:
	mov	r2, r6, asl #1
	ldrh	lr, [r7, r2]
	mov	r0, r4
	mov	sl, lr, lsr #8
	mov	r2, #2
	mov	r1, r5
	strb	lr, [sp, #5]
	strb	sl, [sp, #4]
	bl	png_write_data
	add	sl, r6, #1
	mov	r0, r4
	mov	r1, r5
	mov	r2, #2
	bl	png_calculate_crc
	mov	r1, sl, asl #1
	ldrh	r3, [r7, r1]
	mov	r2, #2
	mov	ip, r3, lsr #8
	mov	r0, r4
	mov	r1, r5
	strb	ip, [sp, #4]
	strb	r3, [sp, #5]
	add	sl, r7, sl, asl #1
	bl	png_write_data
	mov	r0, r4
	mov	r1, r5
	mov	r2, #2
	bl	png_calculate_crc
	ldrh	r3, [sl, #2]
	mov	r2, #2
	mov	ip, r3, lsr #8
	mov	r0, r4
	mov	r1, r5
	strb	ip, [sp, #4]
	strb	r3, [sp, #5]
	bl	png_write_data
	mov	r0, r4
	mov	r1, r5
	mov	r2, #2
	bl	png_calculate_crc
	add	r0, r7, r6, asl #1
	ldrh	r3, [r0, #6]
	mov	r2, #2
	mov	ip, r3, lsr #8
	mov	r0, r4
	mov	r1, r5
	strb	ip, [sp, #4]
	strb	r3, [sp, #5]
	add	r6, r6, #4
	bl	png_write_data
	mov	r0, r4
	mov	r1, r5
	mov	r2, #2
	bl	png_calculate_crc
	cmp	r8, r6
	bgt	.L155
.L154:
	ldr	r3, [r4, #300]
	mov	r0, r4
	mov	lr, r3, lsr #16
	mov	r4, r3, lsr #24
	mov	ip, r3, lsr #8
	mov	r1, sp
	mov	r2, #4
	strb	r4, [sp, #0]
	strb	lr, [sp, #1]
	strb	ip, [sp, #2]
	strb	r3, [sp, #3]
	bl	png_write_data
.L156:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	bx	lr
.L171:
	ldr	r1, .L172+4
	bl	png_warning
	b	.L156
.L173:
	.align	2
.L172:
	.word	png_hIST
	.word	.LC10
	.size	png_write_hIST, .-png_write_hIST
	.section	.text.png_write_PLTE,"ax",%progbits
	.align	2
	.global	png_write_PLTE
	.hidden	png_write_PLTE
	.type	png_write_PLTE, %function
png_write_PLTE:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	mov	r4, r0
	ldr	r0, [r0, #588]
	eor	r3, r0, #1
	cmp	r2, #0
	movne	r3, #0
	andeq	r3, r3, #1
	cmp	r3, #0
	sub	sp, sp, #8
	mov	r8, r2
	mov	r7, r1
	bne	.L175
	cmp	r2, #256
	bls	.L176
.L175:
	ldrb	r1, [r4, #322]	@ zero_extendqisi2
	cmp	r1, #3
	mov	r0, r4
	ldr	r1, .L199
	beq	.L197
	bl	png_warning
.L182:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L197:
	bl	png_error
.L176:
	ldrb	r2, [r4, #322]	@ zero_extendqisi2
	tst	r2, #2
	beq	.L198
	mov	r5, #308
	strh	r8, [r4, r5]	@ movhi
	mov	r0, r4
	ldr	r1, .L199+4
	add	r2, r8, r8, asl #1
	bl	png_write_chunk_start
	cmp	r8, #0
	beq	.L180
	ldrb	lr, [r7, #0]	@ zero_extendqisi2
	ldrb	ip, [r7, #1]	@ zero_extendqisi2
	ldrb	r6, [r7, #2]	@ zero_extendqisi2
	add	r5, sp, #4
	mov	r2, #3
	mov	r0, r4
	mov	r1, r5
	sub	r9, r8, #1
	strb	lr, [sp, #4]
	strb	ip, [sp, #5]
	strb	r6, [sp, #6]
	and	r9, r9, r2
	bl	png_write_data
	mov	r0, r4
	mov	r1, r5
	mov	r2, #3
	bl	png_calculate_crc
	cmp	r8, #1
	mov	sl, #1
	mov	r6, #3
	bls	.L180
	cmp	r9, #0
	beq	.L181
	cmp	r9, #1
	beq	.L195
	cmp	r9, #2
	beq	.L196
	mov	r2, r7
	ldrb	ip, [r2, #3]!	@ zero_extendqisi2
	ldrb	sl, [r2, #2]	@ zero_extendqisi2
	ldrb	r3, [r2, #1]	@ zero_extendqisi2
	mov	r0, r4
	mov	r2, r6
	mov	r1, r5
	strb	ip, [sp, #4]
	strb	r3, [sp, #5]
	strb	sl, [sp, #6]
	bl	png_write_data
	mov	r2, r6
	mov	r0, r4
	mov	r1, r5
	bl	png_calculate_crc
	mov	sl, #2
	mov	r6, #6
.L196:
	mov	r1, r7
	ldrb	ip, [r1, r6]!	@ zero_extendqisi2
	ldrb	lr, [r1, #2]	@ zero_extendqisi2
	ldrb	r3, [r1, #1]	@ zero_extendqisi2
	mov	r2, #3
	mov	r0, r4
	mov	r1, r5
	strb	ip, [sp, #4]
	strb	r3, [sp, #5]
	strb	lr, [sp, #6]
	bl	png_write_data
	mov	r0, r4
	mov	r1, r5
	mov	r2, #3
	bl	png_calculate_crc
	add	sl, sl, #1
	add	r6, r6, #3
.L195:
	mov	r0, r7
	ldrb	ip, [r0, r6]!	@ zero_extendqisi2
	ldrb	lr, [r0, #2]	@ zero_extendqisi2
	ldrb	r3, [r0, #1]	@ zero_extendqisi2
	mov	r2, #3
	mov	r0, r4
	mov	r1, r5
	strb	ip, [sp, #4]
	strb	r3, [sp, #5]
	strb	lr, [sp, #6]
	add	sl, sl, #1
	bl	png_write_data
	mov	r0, r4
	mov	r1, r5
	mov	r2, #3
	bl	png_calculate_crc
	cmp	r8, sl
	add	r6, r6, #3
	bls	.L180
.L181:
	mov	r2, r7
	ldrb	ip, [r2, r6]!	@ zero_extendqisi2
	ldrb	lr, [r2, #2]	@ zero_extendqisi2
	ldrb	r3, [r2, #1]	@ zero_extendqisi2
	mov	r0, r4
	mov	r2, #3
	mov	r1, r5
	strb	ip, [sp, #4]
	strb	r3, [sp, #5]
	strb	lr, [sp, #6]
	bl	png_write_data
	mov	r0, r4
	mov	r1, r5
	mov	r2, #3
	bl	png_calculate_crc
	mov	r1, r7
	add	r0, r6, #3
	ldrb	ip, [r1, r0]!	@ zero_extendqisi2
	ldrb	lr, [r1, #2]	@ zero_extendqisi2
	ldrb	r3, [r1, #1]	@ zero_extendqisi2
	mov	r2, #3
	mov	r0, r4
	mov	r1, r5
	strb	ip, [sp, #4]
	strb	r3, [sp, #5]
	strb	lr, [sp, #6]
	bl	png_write_data
	mov	r0, r4
	mov	r1, r5
	mov	r2, #3
	bl	png_calculate_crc
	mov	r0, r7
	add	r2, r6, #6
	ldrb	ip, [r0, r2]!	@ zero_extendqisi2
	ldrb	lr, [r0, #2]	@ zero_extendqisi2
	ldrb	r3, [r0, #1]	@ zero_extendqisi2
	mov	r2, #3
	mov	r0, r4
	mov	r1, r5
	strb	ip, [sp, #4]
	strb	r3, [sp, #5]
	strb	lr, [sp, #6]
	bl	png_write_data
	mov	r0, r4
	mov	r1, r5
	mov	r2, #3
	bl	png_calculate_crc
	mov	r0, r7
	add	r1, r6, #9
	ldrb	ip, [r0, r1]!	@ zero_extendqisi2
	ldrb	lr, [r0, #2]	@ zero_extendqisi2
	ldrb	r3, [r0, #1]	@ zero_extendqisi2
	mov	r2, #3
	mov	r0, r4
	mov	r1, r5
	strb	ip, [sp, #4]
	strb	r3, [sp, #5]
	strb	lr, [sp, #6]
	add	sl, sl, #4
	bl	png_write_data
	mov	r0, r4
	mov	r1, r5
	mov	r2, #3
	bl	png_calculate_crc
	cmp	r8, sl
	add	r6, r6, #12
	bhi	.L181
.L180:
	ldr	r3, [r4, #300]
	mov	r0, r4
	mov	r5, r3, lsr #24
	mov	lr, r3, lsr #16
	mov	ip, r3, lsr #8
	mov	r1, sp
	mov	r2, #4
	strb	r5, [sp, #0]
	strb	lr, [sp, #1]
	strb	ip, [sp, #2]
	strb	r3, [sp, #3]
	bl	png_write_data
	ldr	r0, [r4, #132]
	orr	r1, r0, #2
	str	r1, [r4, #132]
	b	.L182
.L198:
	mov	r0, r4
	ldr	r1, .L199+8
	bl	png_warning
	b	.L182
.L200:
	.align	2
.L199:
	.word	.LC11
	.word	png_PLTE
	.word	.LC12
	.size	png_write_PLTE, .-png_write_PLTE
	.section	.text.png_write_pCAL,"ax",%progbits
	.align	2
	.global	png_write_pCAL
	.hidden	png_write_pCAL
	.type	png_write_pCAL, %function
png_write_pCAL:
	@ Function supports interworking.
	@ args = 16, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	sub	sp, sp, #52
	ldr	r7, [sp, #88]
	cmp	r7, #3
	mov	r4, r1
	stmia	sp, {r2, r3}	@ phole stm
	mov	r6, r0
	ldr	r8, [sp, #92]
	ldr	r5, [sp, #100]
	bgt	.L257
.L202:
	mov	r1, r4
	add	r2, sp, #44
	mov	r0, r6
	bl	png_check_keyword
	add	r0, r0, #1
	str	r0, [sp, #8]
	ldr	r0, [sp, #96]
	bl	strlen
	ldr	ip, [sp, #8]
	cmp	r8, #0
	addne	r0, r0, #1
	add	r2, ip, r0
	str	r0, [sp, #16]
	mov	r1, r8, asl #2
	mov	r0, r6
	add	r7, r2, #10
	bl	png_malloc
	cmp	r8, #0
	mov	r4, r0
	ble	.L203
	ldr	r0, [r5, #0]
	bl	strlen
	sub	fp, r8, #1
	mov	sl, #1
	cmp	fp, #0
	addne	r0, r0, #1
	cmp	sl, r8
	add	r7, r7, r0
	str	r0, [r4, #0]
	and	r3, fp, #3
	mov	r9, #4
	beq	.L203
	cmp	r3, #0
	beq	.L256
	cmp	r3, #1
	beq	.L248
	cmp	r3, #2
	beq	.L249
	ldr	r0, [r5, #4]
	bl	strlen
	cmp	fp, #1
	addne	r0, r0, #1
	str	r0, [r4, #4]
	add	r7, r7, r0
	mov	sl, #2
	mov	r9, #8
.L249:
	ldr	r0, [r5, r9]
	bl	strlen
	cmp	fp, sl
	addne	r0, r0, #1
	str	r0, [r4, r9]
	add	r7, r7, r0
	add	sl, sl, #1
	add	r9, r9, #4
.L248:
	ldr	r0, [r5, r9]
	bl	strlen
	cmp	fp, sl
	addne	r0, r0, #1
	add	sl, sl, #1
	cmp	sl, r8
	str	r0, [r4, r9]
	add	r7, r7, r0
	add	r9, r9, #4
	beq	.L203
.L256:
	str	r6, [sp, #20]
	str	r8, [sp, #12]
.L204:
	ldr	r0, [r5, r9]
	bl	strlen
	cmp	fp, sl
	moveq	r3, r0
	addne	r3, r0, #1
	str	r3, [r4, r9]
	add	r6, r9, #4
	ldr	r0, [r5, r6]
	add	r8, r7, r3
	bl	strlen
	add	r7, sl, #1
	cmp	fp, r7
	moveq	r1, r0
	addne	r1, r0, #1
	str	r1, [r4, r6]
	add	r6, r6, #4
	ldr	r0, [r5, r6]
	add	r8, r8, r1
	bl	strlen
	add	ip, r7, #1
	cmp	fp, ip
	moveq	r2, r0
	addne	r2, r0, #1
	str	r2, [r4, r6]
	add	r6, r9, #12
	ldr	r0, [r5, r6]
	add	r8, r8, r2
	bl	strlen
	ldr	r3, [sp, #12]
	add	r7, sl, #3
	add	sl, sl, #4
	cmp	fp, r7
	moveq	r1, r0
	addne	r1, r0, #1
	cmp	sl, r3
	str	r1, [r4, r6]
	add	r7, r8, r1
	add	r9, r9, #16
	bne	.L204
	ldr	r6, [sp, #20]
	ldr	r8, [sp, #12]
.L203:
	mov	r2, r7
	mov	r0, r6
	ldr	r1, .L260
	bl	png_write_chunk_start
	cmp	r6, #0
	ldr	r7, [sp, #44]
	beq	.L205
	ldr	r2, [sp, #8]
	cmp	r2, #0
	cmpne	r7, #0
	beq	.L205
	mov	r0, r6
	mov	r1, r7
	bl	png_write_data
	mov	r0, r6
	mov	r1, r7
	ldr	r2, [sp, #8]
	bl	png_calculate_crc
.L205:
	ldr	r7, [sp, #0]
	mov	lr, r7, lsr #24
	mov	r0, r7, asr #8
	mov	ip, r7, asr #16
	ldr	r7, [sp, #4]
	strb	lr, [sp, #28]
	strb	r0, [sp, #30]
	ldr	lr, [sp, #88]
	ldr	r0, [sp, #0]
	mov	r1, r7, lsr #24
	mov	r2, r7, asr #16
	mov	r3, r7, asr #8
	cmp	r6, #0
	strb	ip, [sp, #29]
	strb	r0, [sp, #31]
	strb	r1, [sp, #32]
	strb	r2, [sp, #33]
	strb	r3, [sp, #34]
	strb	r7, [sp, #35]
	strb	lr, [sp, #36]
	strb	r8, [sp, #37]
	beq	.L206
	add	r7, sp, #28
	mov	r2, #10
	mov	r0, r6
	mov	r1, r7
	bl	png_write_data
	mov	r2, #10
	mov	r0, r6
	mov	r1, r7
	bl	png_calculate_crc
	ldr	r2, [sp, #16]
	ldr	r3, [sp, #96]
	cmp	r2, #0
	cmpne	r3, #0
	bne	.L258
.L206:
	mov	r0, r6
	ldr	r1, [sp, #44]
	bl	png_free
	cmp	r8, #0
	ble	.L207
	cmp	r6, #0
	beq	.L208
	ldr	sl, [r5, #0]
	ldr	r7, [r4, #0]
	sub	fp, r8, #1
	cmp	r7, #0
	cmpne	sl, #0
	and	fp, fp, #3
	bne	.L259
.L217:
	mov	r9, #1
	cmp	r8, r9
	mov	r7, #4
	ble	.L252
	cmp	fp, #0
	beq	.L254
	cmp	fp, r9
	beq	.L246
	cmp	fp, #2
	beq	.L247
	ldr	fp, [r5, r7]
	ldr	sl, [r4, r7]
	cmp	sl, #0
	cmpne	fp, #0
	beq	.L219
	mov	r0, r6
	mov	r1, fp
	mov	r2, sl
	bl	png_write_data
	mov	r0, r6
	mov	r1, fp
	mov	r2, sl
	bl	png_calculate_crc
.L219:
	add	r9, r9, #1
	add	r7, r7, #4
.L247:
	ldr	fp, [r5, r7]
	ldr	sl, [r4, r7]
	cmp	sl, #0
	cmpne	fp, #0
	beq	.L222
	mov	r0, r6
	mov	r1, fp
	mov	r2, sl
	bl	png_write_data
	mov	r0, r6
	mov	r1, fp
	mov	r2, sl
	bl	png_calculate_crc
.L222:
	add	r9, r9, #1
	add	r7, r7, #4
.L246:
	ldr	fp, [r5, r7]
	ldr	sl, [r4, r7]
	cmp	sl, #0
	cmpne	fp, #0
	beq	.L225
	mov	r0, r6
	mov	r1, fp
	mov	r2, sl
	bl	png_write_data
	mov	r0, r6
	mov	r1, fp
	mov	r2, sl
	bl	png_calculate_crc
.L225:
	add	r9, r9, #1
	cmp	r8, r9
	add	r7, r7, #4
	ble	.L252
.L254:
	mov	sl, r6
	mov	fp, r8
.L210:
	ldr	r8, [r5, r7]
	add	r6, r7, #4
	ldr	r7, [r4, r7]
	cmp	r7, #0
	cmpne	r8, #0
	beq	.L209
	mov	r0, sl
	mov	r1, r8
	mov	r2, r7
	bl	png_write_data
	mov	r0, sl
	mov	r1, r8
	mov	r2, r7
	bl	png_calculate_crc
.L209:
	ldr	r8, [r5, r6]
	ldr	r7, [r4, r6]
	cmp	r7, #0
	cmpne	r8, #0
	add	r9, r9, #1
	beq	.L228
	mov	r0, sl
	mov	r1, r8
	mov	r2, r7
	bl	png_write_data
	mov	r0, sl
	mov	r1, r8
	mov	r2, r7
	bl	png_calculate_crc
.L228:
	add	r2, r6, #4
	ldr	r7, [r4, r2]
	ldr	r8, [r5, r2]
	cmp	r7, #0
	cmpne	r8, #0
	add	r9, r9, #3
	beq	.L230
	mov	r0, sl
	mov	r1, r8
	mov	r2, r7
	bl	png_write_data
	mov	r0, sl
	mov	r1, r8
	mov	r2, r7
	bl	png_calculate_crc
.L230:
	add	r1, r6, #8
	ldr	r7, [r4, r1]
	ldr	r8, [r5, r1]
	cmp	r7, #0
	cmpne	r8, #0
	beq	.L232
	mov	r0, sl
	mov	r1, r8
	mov	r2, r7
	bl	png_write_data
	mov	r0, sl
	mov	r1, r8
	mov	r2, r7
	bl	png_calculate_crc
.L232:
	cmp	fp, r9
	add	r7, r6, #12
	bgt	.L210
	mov	r6, sl
.L252:
	mov	r1, r4
	mov	r0, r6
	bl	png_free
.L213:
	ldr	r3, [r6, #300]
	mov	r0, r6
	mov	r4, r3, lsr #24
	mov	lr, r3, lsr #16
	mov	ip, r3, lsr #8
	add	r1, sp, #40
	mov	r2, #4
	strb	r4, [sp, #40]
	strb	lr, [sp, #41]
	strb	ip, [sp, #42]
	strb	r3, [sp, #43]
	bl	png_write_data
.L214:
	add	sp, sp, #52
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L259:
	mov	r0, r6
	mov	r1, sl
	mov	r2, r7
	bl	png_write_data
	mov	r0, r6
	mov	r1, sl
	mov	r2, r7
	bl	png_calculate_crc
	b	.L217
.L258:
	mov	r0, r6
	mov	r1, r3
	bl	png_write_data
	mov	r0, r6
	ldr	r1, [sp, #96]
	ldr	r2, [sp, #16]
	bl	png_calculate_crc
	b	.L206
.L257:
	ldr	r1, .L260+4
	bl	png_warning
	b	.L202
.L208:
	mov	r0, r6
	mov	r1, r4
	bl	png_free
	b	.L214
.L207:
	mov	r1, r4
	mov	r0, r6
	bl	png_free
	cmp	r6, #0
	beq	.L214
	b	.L213
.L261:
	.align	2
.L260:
	.word	png_pCAL
	.word	.LC13
	.size	png_write_pCAL, .-png_write_pCAL
	.section	.text.png_write_chunk,"ax",%progbits
	.align	2
	.global	png_write_chunk
	.hidden	png_write_chunk
	.type	png_write_chunk, %function
png_write_chunk:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	subs	r4, r0, #0
	sub	sp, sp, #8
	mov	r5, r2
	mov	r6, r3
	beq	.L265
	mov	r2, r3
	bl	png_write_chunk_start
	cmp	r6, #0
	cmpne	r5, #0
	bne	.L266
.L264:
	ldr	r3, [r4, #300]
	mov	r0, r4
	mov	lr, r3, lsr #16
	mov	r4, r3, lsr #24
	mov	ip, r3, lsr #8
	add	r1, sp, #4
	mov	r2, #4
	strb	r4, [sp, #4]
	strb	lr, [sp, #5]
	strb	ip, [sp, #6]
	strb	r3, [sp, #7]
	bl	png_write_data
.L265:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L266:
	mov	r0, r4
	mov	r1, r5
	mov	r2, r6
	bl	png_write_data
	mov	r0, r4
	mov	r1, r5
	mov	r2, r6
	bl	png_calculate_crc
	b	.L264
	.size	png_write_chunk, .-png_write_chunk
	.section	.text.png_write_tIME,"ax",%progbits
	.align	2
	.global	png_write_tIME
	.hidden	png_write_tIME
	.type	png_write_tIME, %function
png_write_tIME:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	ldrb	lr, [r1, #2]	@ zero_extendqisi2
	sub	r3, lr, #1
	cmp	r3, #11
	sub	sp, sp, #8
	mov	r3, r0
	bhi	.L268
	ldrb	ip, [r1, #3]	@ zero_extendqisi2
	cmp	ip, #31
	bhi	.L268
	cmp	ip, #0
	beq	.L268
	ldrb	r4, [r1, #4]	@ zero_extendqisi2
	cmp	r4, #23
	bhi	.L268
	ldrb	r5, [r1, #6]	@ zero_extendqisi2
	cmp	r5, #60
	bhi	.L268
	ldrh	r6, [r1, #0]
	ldrb	r8, [r1, #5]	@ zero_extendqisi2
	mov	r7, r6, lsr #8
	ldr	r1, .L272
	mov	r2, sp
	mov	r3, #7
	strb	r7, [sp, #0]
	strb	r6, [sp, #1]
	strb	lr, [sp, #2]
	strb	ip, [sp, #3]
	strb	r4, [sp, #4]
	strb	r8, [sp, #5]
	strb	r5, [sp, #6]
	bl	png_write_chunk
	b	.L271
.L268:
	mov	r0, r3
	ldr	r1, .L272+4
	bl	png_warning
.L271:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L273:
	.align	2
.L272:
	.word	png_tIME
	.word	.LC14
	.size	png_write_tIME, .-png_write_tIME
	.section	.text.png_write_sCAL,"ax",%progbits
	.align	2
	.global	png_write_sCAL
	.hidden	png_write_sCAL
	.type	png_write_sCAL, %function
png_write_sCAL:
	@ Function supports interworking.
	@ args = 8, pretend = 0, frame = 64
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, fp, lr}
	sub	sp, sp, #76
	add	r4, sp, #72
	strb	r1, [r4, #-64]!
	ldr	r5, .L276
	add	r6, r4, #1
	stmia	sp, {r2-r3}
	mov	r8, r0
	mov	r2, r5
	mov	r1, #63
	mov	r0, r6
	bl	snprintf
	mov	r0, r6
	bl	strlen
	add	r6, r0, #2
	add	ip, sp, #104
	ldmia	ip, {fp-ip}
	add	r7, r4, r6
	mov	r2, r5
	rsb	r1, r6, #64
	mov	r0, r7
	stmia	sp, {fp-ip}
	bl	snprintf
	mov	r0, r7
	bl	strlen
	mov	r2, r4
	add	r3, r6, r0
	ldr	r1, .L276+4
	mov	r0, r8
	bl	png_write_chunk
	add	sp, sp, #76
	ldmfd	sp!, {r4, r5, r6, r7, r8, fp, lr}
	bx	lr
.L277:
	.align	2
.L276:
	.word	.LC15
	.word	png_sCAL
	.size	png_write_sCAL, .-png_write_sCAL
	.section	.text.png_write_oFFs,"ax",%progbits
	.align	2
	.global	png_write_oFFs
	.hidden	png_write_oFFs
	.type	png_write_oFFs, %function
png_write_oFFs:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	cmp	r3, #1
	sub	sp, sp, #16
	mov	r6, r3
	mov	r5, r1
	mov	r4, r2
	mov	r7, r0
	ldrgt	r1, .L281
	blgt	png_warning
.L279:
	mov	r0, r7
	mov	lr, r5, asr #16
	mov	r7, r5, lsr #24
	mov	r8, r5, asr #8
	mov	sl, r4, lsr #24
	mov	r9, r4, asr #16
	mov	ip, r4, asr #8
	ldr	r1, .L281+4
	add	r2, sp, #4
	mov	r3, #9
	strb	r7, [sp, #4]
	strb	lr, [sp, #5]
	strb	r8, [sp, #6]
	strb	r5, [sp, #7]
	strb	sl, [sp, #8]
	strb	r9, [sp, #9]
	strb	ip, [sp, #10]
	strb	r4, [sp, #11]
	strb	r6, [sp, #12]
	bl	png_write_chunk
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L282:
	.align	2
.L281:
	.word	.LC16
	.word	png_oFFs
	.size	png_write_oFFs, .-png_write_oFFs
	.section	.text.png_write_bKGD,"ax",%progbits
	.align	2
	.global	png_write_bKGD
	.hidden	png_write_bKGD
	.type	png_write_bKGD, %function
png_write_bKGD:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
	cmp	r2, #3
	sub	sp, sp, #12
	mov	r3, r0
	beq	.L295
	tst	r2, #2
	bne	.L296
	ldrh	ip, [r1, #8]
	ldrb	r3, [r0, #323]	@ zero_extendqisi2
	mov	r2, #1
	cmp	ip, r2, asl r3
	bge	.L297
	mov	lr, ip, lsr #8
	ldr	r1, .L301
	mov	r2, sp
	mov	r3, #2
	strb	lr, [sp, #0]
	strb	ip, [sp, #1]
	bl	png_write_chunk
.L293:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, lr}
	bx	lr
.L296:
	ldrh	r2, [r1, #6]
	ldrh	ip, [r1, #2]
	ldrb	lr, [r0, #323]	@ zero_extendqisi2
	ldrh	r1, [r1, #4]
	cmp	lr, #8
	mov	r4, ip, lsr #8
	mov	r5, r1, lsr #8
	mov	lr, r2, lsr #8
	strb	ip, [sp, #1]
	strb	r1, [sp, #3]
	strb	r2, [sp, #5]
	strb	r4, [sp, #0]
	strb	r5, [sp, #2]
	strb	lr, [sp, #4]
	bne	.L291
	orr	r4, r5, r4
	orrs	r4, lr, r4
	bne	.L298
.L291:
	mov	r0, r3
	ldr	r1, .L301
	mov	r2, sp
	mov	r3, #6
	bl	png_write_chunk
	b	.L293
.L295:
	mov	r2, #308
	ldrh	r2, [r0, r2]
	cmp	r2, #0
	beq	.L299
	ldrb	r1, [r1, #0]	@ zero_extendqisi2
	cmp	r2, r1
	bls	.L286
.L288:
	add	ip, sp, #8
	strb	r1, [ip, #-8]!
	mov	r0, r3
	mov	r2, sp
	ldr	r1, .L301
	mov	r3, #1
	bl	png_write_chunk
	b	.L293
.L299:
	ldr	r0, [r0, #588]
	tst	r0, #1
	bne	.L300
.L286:
	mov	r0, r3
	ldr	r1, .L301+4
	bl	png_warning
	b	.L293
.L298:
	ldr	r1, .L301+8
	bl	png_warning
	b	.L293
.L297:
	ldr	r1, .L301+12
	bl	png_warning
	b	.L293
.L300:
	ldrb	r1, [r1, #0]	@ zero_extendqisi2
	b	.L288
.L302:
	.align	2
.L301:
	.word	png_bKGD
	.word	.LC17
	.word	.LC18
	.word	.LC19
	.size	png_write_bKGD, .-png_write_bKGD
	.section	.text.png_write_tRNS,"ax",%progbits
	.align	2
	.global	png_write_tRNS
	.hidden	png_write_tRNS
	.type	png_write_tRNS, %function
png_write_tRNS:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
	sub	sp, sp, #12
	ldr	ip, [sp, #24]
	cmp	ip, #3
	mov	lr, r0
	mov	r4, r3
	beq	.L313
	cmp	ip, #0
	bne	.L308
	ldrh	ip, [r2, #8]
	ldrb	r3, [r0, #323]	@ zero_extendqisi2
	mov	r1, #1
	cmp	ip, r1, asl r3
	blt	.L309
	ldr	r1, .L316
	bl	png_warning
	b	.L312
.L308:
	cmp	ip, #2
	beq	.L314
	ldr	r1, .L316+4
	bl	png_warning
	b	.L312
.L313:
	cmp	r3, #0
	ble	.L305
	mov	ip, #308
	ldrh	r2, [r0, ip]
	cmp	r3, r2
	ble	.L306
.L305:
	mov	r0, lr
	ldr	r1, .L316+8
	bl	png_warning
.L312:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, lr}
	bx	lr
.L309:
	mov	lr, ip, lsr #8
	ldr	r1, .L316+12
	mov	r2, sp
	mov	r3, #2
	strb	lr, [sp, #0]
	strb	ip, [sp, #1]
	bl	png_write_chunk
	b	.L312
.L314:
	ldrh	r3, [r2, #6]
	ldrh	r1, [r2, #2]
	ldrb	r4, [r0, #323]	@ zero_extendqisi2
	ldrh	r2, [r2, #4]
	cmp	r4, #8
	mov	r5, r2, lsr #8
	mov	r4, r1, lsr #8
	mov	ip, r3, lsr #8
	strb	r1, [sp, #1]
	strb	r2, [sp, #3]
	strb	r3, [sp, #5]
	strb	r4, [sp, #0]
	strb	r5, [sp, #2]
	strb	ip, [sp, #4]
	bne	.L311
	orr	r3, r5, r4
	orrs	r4, ip, r3
	bne	.L315
.L311:
	mov	r0, lr
	ldr	r1, .L316+12
	mov	r2, sp
	mov	r3, #6
	bl	png_write_chunk
	b	.L312
.L306:
	mov	r2, r1
	ldr	r1, .L316+12
	bl	png_write_chunk
	b	.L312
.L315:
	ldr	r1, .L316+16
	bl	png_warning
	b	.L312
.L317:
	.align	2
.L316:
	.word	.LC21
	.word	.LC23
	.word	.LC20
	.word	png_tRNS
	.word	.LC22
	.size	png_write_tRNS, .-png_write_tRNS
	.section	.text.png_write_cHRM_fixed,"ax",%progbits
	.align	2
	.global	png_write_cHRM_fixed
	.hidden	png_write_cHRM_fixed
	.type	png_write_cHRM_fixed, %function
png_write_cHRM_fixed:
	@ Function supports interworking.
	@ args = 20, pretend = 0, frame = 120
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	sub	sp, sp, #148
	add	r4, sp, #184
	ldmia	r4, {r4, r8, sl}	@ phole ldm
	add	r9, sp, #196
	ldmia	r9, {r9, fp}	@ phole ldm
	stmia	sp, {r4, r8, sl}	@ phole stm
	str	r9, [sp, #12]
	str	fp, [sp, #16]
	str	r0, [sp, #24]
	mov	r7, r1
	mov	r6, r2
	mov	r5, r3
	bl	png_check_cHRM_fixed
	cmp	r0, #0
	beq	.L320
	mov	r1, r7, lsr #16
	ldr	r0, [sp, #24]
	mov	lr, r7, lsr #24
	str	r1, [sp, #24]
	mov	r3, r7, lsr #8
	strb	lr, [sp, #112]
	ldr	lr, [sp, #24]
	str	r3, [sp, #28]
	mov	ip, r6, lsr #24
	strb	lr, [sp, #113]
	ldr	lr, [sp, #28]
	str	ip, [sp, #32]
	mov	r2, r6, lsr #16
	strb	lr, [sp, #114]
	ldr	lr, [sp, #32]
	str	r2, [sp, #36]
	mov	r1, r6, lsr #8
	mov	r3, r5, lsr #24
	str	r1, [sp, #40]
	str	r3, [sp, #44]
	mov	ip, r5, lsr #16
	mov	r2, r5, lsr #8
	mov	r1, r4, lsr #24
	mov	r3, r4, lsr #16
	strb	lr, [sp, #116]
	ldr	lr, [sp, #36]
	str	ip, [sp, #48]
	str	r2, [sp, #52]
	str	r1, [sp, #56]
	str	r3, [sp, #60]
	mov	ip, r4, lsr #8
	mov	r2, r8, lsr #24
	mov	r1, r8, lsr #16
	mov	r3, r8, lsr #8
	str	ip, [sp, #64]
	str	r2, [sp, #68]
	str	r1, [sp, #72]
	str	r3, [sp, #76]
	mov	ip, sl, lsr #24
	mov	r2, sl, lsr #16
	mov	r1, sl, lsr #8
	mov	r3, r9, lsr #24
	strb	lr, [sp, #117]
	ldr	lr, [sp, #40]
	str	ip, [sp, #80]
	str	r2, [sp, #84]
	str	r1, [sp, #88]
	str	r3, [sp, #92]
	mov	ip, r9, lsr #16
	mov	r2, r9, lsr #8
	mov	r1, fp, lsr #24
	mov	r3, fp, lsr #16
	str	ip, [sp, #96]
	str	r2, [sp, #100]
	str	r1, [sp, #104]
	str	r3, [sp, #108]
	strb	r7, [sp, #115]
	strb	lr, [sp, #118]
	ldr	lr, [sp, #44]
	strb	lr, [sp, #120]
	ldr	lr, [sp, #48]
	strb	lr, [sp, #121]
	ldr	lr, [sp, #52]
	strb	lr, [sp, #122]
	ldr	lr, [sp, #56]
	strb	lr, [sp, #124]
	ldr	lr, [sp, #60]
	strb	lr, [sp, #125]
	ldr	lr, [sp, #64]
	strb	lr, [sp, #126]
	ldr	lr, [sp, #68]
	strb	lr, [sp, #128]
	ldr	lr, [sp, #72]
	strb	lr, [sp, #129]
	ldr	lr, [sp, #76]
	strb	lr, [sp, #130]
	ldr	lr, [sp, #80]
	strb	lr, [sp, #132]
	ldr	lr, [sp, #84]
	strb	lr, [sp, #133]
	ldr	lr, [sp, #88]
	strb	lr, [sp, #134]
	ldr	lr, [sp, #92]
	strb	lr, [sp, #136]
	ldr	lr, [sp, #96]
	strb	lr, [sp, #137]
	ldr	lr, [sp, #100]
	strb	r6, [sp, #119]
	strb	r5, [sp, #123]
	strb	r4, [sp, #127]
	strb	r8, [sp, #131]
	strb	sl, [sp, #135]
	strb	lr, [sp, #138]
	ldr	lr, [sp, #104]
	strb	lr, [sp, #140]
	ldr	lr, [sp, #108]
	mov	ip, fp, lsr #8
	ldr	r1, .L321
	add	r2, sp, #112
	mov	r3, #32
	strb	r9, [sp, #139]
	strb	lr, [sp, #141]
	strb	ip, [sp, #142]
	strb	fp, [sp, #143]
	bl	png_write_chunk
.L320:
	add	sp, sp, #148
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L322:
	.align	2
.L321:
	.word	png_cHRM
	.size	png_write_cHRM_fixed, .-png_write_cHRM_fixed
	.global	__aeabi_dmul
	.global	__aeabi_dadd
	.global	__aeabi_d2uiz
	.section	.text.png_write_cHRM,"ax",%progbits
	.align	2
	.global	png_write_cHRM
	.hidden	png_write_cHRM
	.type	png_write_cHRM, %function
png_write_cHRM:
	@ Function supports interworking.
	@ args = 56, pretend = 0, frame = 120
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	mov	r4, #1073741824
	sub	sp, sp, #148
	add	lr, r4, #16252928
	str	r0, [sp, #24]
	mov	r1, r3
	mov	r0, r2
	add	r3, lr, #27136
	mov	r2, #0
	mov	r7, #1069547520
	bl	__aeabi_dmul
	add	r3, r7, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	mov	fp, #1073741824
	add	r3, fp, #16252928
	mov	r7, r0
	mov	r2, #0
	add	r1, sp, #184
	ldmia	r1, {r0-r1}
	add	r3, r3, #27136
	mov	r6, #1069547520
	bl	__aeabi_dmul
	add	r3, r6, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	mov	r8, #1073741824
	add	r9, r8, #16252928
	add	r3, r9, #27136
	mov	r6, r0
	mov	r2, #0
	add	r1, sp, #192
	ldmia	r1, {r0-r1}
	mov	r5, #1069547520
	bl	__aeabi_dmul
	add	r3, r5, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	mov	ip, #1073741824
	add	sl, ip, #16252928
	add	r3, sl, #27136
	mov	r5, r0
	mov	r2, #0
	add	r1, sp, #200
	ldmia	r1, {r0-r1}
	mov	r4, #1069547520
	bl	__aeabi_dmul
	add	r3, r4, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	mov	fp, #1073741824
	add	r3, fp, #16252928
	mov	r4, r0
	mov	r2, #0
	add	r1, sp, #208
	ldmia	r1, {r0-r1}
	add	r3, r3, #27136
	mov	r8, #1069547520
	bl	__aeabi_dmul
	add	r3, r8, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	mov	r9, #1073741824
	add	ip, r9, #16252928
	add	r3, ip, #27136
	mov	r8, r0
	mov	r2, #0
	add	r1, sp, #216
	ldmia	r1, {r0-r1}
	mov	sl, #1069547520
	bl	__aeabi_dmul
	add	r3, sl, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	mov	fp, #1073741824
	add	r3, fp, #16252928
	mov	sl, r0
	mov	r2, #0
	add	r1, sp, #224
	ldmia	r1, {r0-r1}
	add	r3, r3, #27136
	mov	r9, #1069547520
	bl	__aeabi_dmul
	add	r3, r9, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	mov	ip, #1073741824
	add	fp, ip, #16252928
	add	r3, fp, #27136
	mov	r9, r0
	mov	r2, #0
	add	r1, sp, #232
	ldmia	r1, {r0-r1}
	bl	__aeabi_dmul
	mov	r3, #1069547520
	mov	r2, #0
	add	r3, r3, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	mov	r1, r7
	mov	fp, r0
	mov	r2, r6
	ldr	r0, [sp, #24]
	mov	r3, r5
	stmia	sp, {r4, r8, sl}	@ phole stm
	str	r9, [sp, #12]
	str	fp, [sp, #16]
	bl	png_check_cHRM_fixed
	cmp	r0, #0
	beq	.L325
	mov	r2, r7, lsr #16
	ldr	r0, [sp, #24]
	mov	lr, r7, lsr #24
	str	r2, [sp, #24]
	mov	r1, r7, lsr #8
	strb	lr, [sp, #112]
	ldr	lr, [sp, #24]
	str	r1, [sp, #28]
	mov	ip, r6, lsr #24
	strb	lr, [sp, #113]
	ldr	lr, [sp, #28]
	str	ip, [sp, #32]
	mov	r3, r6, lsr #16
	strb	lr, [sp, #114]
	ldr	lr, [sp, #32]
	str	r3, [sp, #36]
	mov	r2, r6, lsr #8
	mov	r1, r5, lsr #24
	str	r2, [sp, #40]
	str	r1, [sp, #44]
	mov	ip, r5, lsr #16
	mov	r3, r5, lsr #8
	mov	r2, r4, lsr #24
	mov	r1, r4, lsr #16
	strb	lr, [sp, #116]
	ldr	lr, [sp, #36]
	str	ip, [sp, #48]
	str	r3, [sp, #52]
	str	r2, [sp, #56]
	str	r1, [sp, #60]
	mov	ip, r4, lsr #8
	mov	r3, r8, lsr #24
	mov	r2, r8, lsr #16
	mov	r1, r8, lsr #8
	str	ip, [sp, #64]
	str	r3, [sp, #68]
	str	r2, [sp, #72]
	str	r1, [sp, #76]
	mov	ip, sl, lsr #24
	mov	r3, sl, lsr #16
	mov	r2, sl, lsr #8
	mov	r1, r9, lsr #24
	strb	lr, [sp, #117]
	ldr	lr, [sp, #40]
	str	ip, [sp, #80]
	str	r3, [sp, #84]
	str	r2, [sp, #88]
	str	r1, [sp, #92]
	mov	ip, r9, lsr #16
	mov	r3, r9, lsr #8
	mov	r2, fp, lsr #24
	mov	r1, fp, lsr #16
	str	ip, [sp, #96]
	str	r3, [sp, #100]
	str	r2, [sp, #104]
	str	r1, [sp, #108]
	strb	r7, [sp, #115]
	strb	lr, [sp, #118]
	ldr	lr, [sp, #44]
	strb	lr, [sp, #120]
	ldr	lr, [sp, #48]
	strb	lr, [sp, #121]
	ldr	lr, [sp, #52]
	strb	lr, [sp, #122]
	ldr	lr, [sp, #56]
	strb	lr, [sp, #124]
	ldr	lr, [sp, #60]
	strb	lr, [sp, #125]
	ldr	lr, [sp, #64]
	strb	lr, [sp, #126]
	ldr	lr, [sp, #68]
	strb	lr, [sp, #128]
	ldr	lr, [sp, #72]
	strb	lr, [sp, #129]
	ldr	lr, [sp, #76]
	strb	lr, [sp, #130]
	ldr	lr, [sp, #80]
	strb	lr, [sp, #132]
	ldr	lr, [sp, #84]
	strb	lr, [sp, #133]
	ldr	lr, [sp, #88]
	strb	lr, [sp, #134]
	ldr	lr, [sp, #92]
	strb	lr, [sp, #136]
	ldr	lr, [sp, #96]
	strb	lr, [sp, #137]
	ldr	lr, [sp, #100]
	strb	r6, [sp, #119]
	strb	r5, [sp, #123]
	strb	r4, [sp, #127]
	strb	r8, [sp, #131]
	strb	sl, [sp, #135]
	strb	lr, [sp, #138]
	ldr	lr, [sp, #104]
	strb	lr, [sp, #140]
	ldr	lr, [sp, #108]
	mov	ip, fp, lsr #8
	ldr	r1, .L326
	add	r2, sp, #112
	mov	r3, #32
	strb	r9, [sp, #139]
	strb	lr, [sp, #141]
	strb	ip, [sp, #142]
	strb	fp, [sp, #143]
	bl	png_write_chunk
.L325:
	add	sp, sp, #148
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L327:
	.align	2
.L326:
	.word	png_cHRM
	.size	png_write_cHRM, .-png_write_cHRM
	.section	.text.png_write_sBIT,"ax",%progbits
	.align	2
	.global	png_write_sBIT
	.hidden	png_write_sBIT
	.type	png_write_sBIT, %function
png_write_sBIT:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	tst	r2, #2
	sub	sp, sp, #8
	beq	.L329
	ldrb	r3, [r1, #0]	@ zero_extendqisi2
	cmp	r2, #3
	moveq	ip, #8
	ldrneb	ip, [r0, #324]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L339
	cmp	ip, r3
	bcc	.L339
	ldrb	lr, [r1, #1]	@ zero_extendqisi2
	cmp	lr, #0
	beq	.L339
	cmp	ip, lr
	bcc	.L339
	ldrb	r4, [r1, #2]	@ zero_extendqisi2
	cmp	r4, #0
	beq	.L339
	cmp	ip, r4
	bcc	.L339
	strb	r3, [sp, #4]
	strb	lr, [sp, #5]
	strb	r4, [sp, #6]
	mov	r3, #3
	b	.L335
.L329:
	ldrb	r3, [r1, #3]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L342
.L339:
	ldr	r1, .L343
	bl	png_warning
.L341:
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	bx	lr
.L342:
	ldrb	ip, [r0, #324]	@ zero_extendqisi2
	cmp	ip, r3
	strcsb	r3, [sp, #4]
	movcs	r3, #1
	bcc	.L339
.L335:
	tst	r2, #4
	beq	.L338
	ldrb	r2, [r1, #4]	@ zero_extendqisi2
	cmp	r2, #0
	beq	.L339
	ldrb	r1, [r0, #324]	@ zero_extendqisi2
	cmp	r1, r2
	bcc	.L339
	add	ip, sp, #8
	add	lr, ip, r3
	strb	r2, [lr, #-4]
	add	r3, r3, #1
.L338:
	ldr	r1, .L343+4
	add	r2, sp, #4
	bl	png_write_chunk
	b	.L341
.L344:
	.align	2
.L343:
	.word	.LC24
	.word	png_sBIT
	.size	png_write_sBIT, .-png_write_sBIT
	.section	.text.png_write_sRGB,"ax",%progbits
	.align	2
	.global	png_write_sRGB
	.hidden	png_write_sRGB
	.type	png_write_sRGB, %function
png_write_sRGB:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
	cmp	r1, #3
	sub	sp, sp, #12
	mov	r4, r1
	mov	r5, r0
	ldrgt	r1, .L348
	blgt	png_warning
.L346:
	add	r2, sp, #8
	strb	r4, [r2, #-4]!
	mov	r0, r5
	ldr	r1, .L348+4
	mov	r3, #1
	bl	png_write_chunk
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, lr}
	bx	lr
.L349:
	.align	2
.L348:
	.word	.LC25
	.word	png_sRGB
	.size	png_write_sRGB, .-png_write_sRGB
	.section	.text.png_write_gAMA_fixed,"ax",%progbits
	.align	2
	.global	png_write_gAMA_fixed
	.hidden	png_write_gAMA_fixed
	.type	png_write_gAMA_fixed, %function
png_write_gAMA_fixed:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
	sub	sp, sp, #12
	mov	ip, r1
	mov	r5, r1, lsr #24
	mov	r4, r1, lsr #16
	mov	lr, r1, lsr #8
	add	r2, sp, #4
	ldr	r1, .L352
	mov	r3, #4
	strb	r5, [sp, #4]
	strb	r4, [sp, #5]
	strb	lr, [sp, #6]
	strb	ip, [sp, #7]
	bl	png_write_chunk
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, lr}
	bx	lr
.L353:
	.align	2
.L352:
	.word	png_gAMA
	.size	png_write_gAMA_fixed, .-png_write_gAMA_fixed
	.section	.text.png_write_gAMA,"ax",%progbits
	.align	2
	.global	png_write_gAMA
	.hidden	png_write_gAMA
	.type	png_write_gAMA, %function
png_write_gAMA:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
	mov	ip, #1073741824
	add	r5, ip, #16252928
	sub	sp, sp, #12
	mov	r4, r0
	mov	r1, r3
	mov	r0, r2
	add	r3, r5, #27136
	mov	r2, #0
	bl	__aeabi_dmul
	mov	r3, #1069547520
	mov	r2, #0
	add	r3, r3, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	mov	ip, r0
	mov	r5, ip, lsr #24
	mov	r0, r4
	mov	lr, ip, lsr #8
	mov	r4, ip, lsr #16
	ldr	r1, .L356
	add	r2, sp, #4
	mov	r3, #4
	strb	r5, [sp, #4]
	strb	r4, [sp, #5]
	strb	lr, [sp, #6]
	strb	ip, [sp, #7]
	bl	png_write_chunk
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, lr}
	bx	lr
.L357:
	.align	2
.L356:
	.word	png_gAMA
	.size	png_write_gAMA, .-png_write_gAMA
	.section	.text.png_write_IEND,"ax",%progbits
	.align	2
	.global	png_write_IEND
	.hidden	png_write_IEND
	.type	png_write_IEND, %function
png_write_IEND:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r2, #0
	stmfd	sp!, {r4, lr}
	mov	r3, r2
	mov	r4, r0
	ldr	r1, .L360
	bl	png_write_chunk
	ldr	r0, [r4, #132]
	orr	r3, r0, #16
	str	r3, [r4, #132]
	ldmfd	sp!, {r4, lr}
	bx	lr
.L361:
	.align	2
.L360:
	.word	png_IEND
	.size	png_write_IEND, .-png_write_IEND
	.section	.text.png_write_IDAT,"ax",%progbits
	.align	2
	.global	png_write_IDAT
	.hidden	png_write_IDAT
	.type	png_write_IDAT, %function
png_write_IDAT:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	ldr	r3, [r0, #132]
	tst	r3, #4
	mov	r4, r0
	mov	r5, r1
	mov	r6, r2
	bne	.L363
	ldrb	r0, [r0, #636]	@ zero_extendqisi2
	cmp	r0, #0
	bne	.L363
	ldrb	r2, [r1, #0]	@ zero_extendqisi2
	and	r1, r2, #15
	cmp	r1, #8
	beq	.L370
.L364:
	mov	r0, r4
	ldr	r1, .L371
	bl	png_error
.L363:
	mov	r3, r6
	mov	r0, r4
	mov	r2, r5
	ldr	r1, .L371+4
	bl	png_write_chunk
	ldr	r3, [r4, #132]
	orr	r0, r3, #4
	str	r0, [r4, #132]
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L370:
	and	ip, r2, #240
	cmp	ip, #112
	bhi	.L364
	cmp	r6, #1
	bls	.L363
	ldr	r0, [r4, #232]
	mov	lr, #16320
	add	r3, lr, #63
	cmp	r0, r3
	bhi	.L363
	ldr	r1, [r4, #228]
	cmp	r1, r3
	bhi	.L363
	ldrb	r3, [r4, #326]	@ zero_extendqisi2
	ldrb	ip, [r4, #323]	@ zero_extendqisi2
	mul	ip, r3, ip
	mul	ip, r1, ip
	mov	r1, r2, lsr #4
	add	r3, r1, #7
	mov	lr, #1
	mov	r3, lr, asl r3
	add	ip, ip, #15
	mov	ip, ip, lsr #3
	mul	ip, r0, ip
	cmp	ip, r3
	movhi	r0, #0
	movls	r0, #1
	cmp	r3, #255
	movls	r0, #0
	cmp	r0, #0
	beq	.L365
.L368:
	mov	r3, r3, lsr #1
	cmp	r3, #255
	movls	r0, #0
	movhi	r0, #1
	cmp	ip, r3
	movhi	r0, #0
	cmp	r0, #0
	sub	r1, r1, #1
	bne	.L368
.L365:
	mov	r1, r1, asl #4
	orr	r1, r1, #8
	and	r0, r1, #255
	cmp	r2, r0
	beq	.L363
	ldrb	ip, [r5, #1]	@ zero_extendqisi2
	mov	r2, #138412032
	add	lr, r2, #135168
	and	ip, ip, #224
	add	r2, ip, r1, asl #8
	add	r3, lr, #133
	umull	r1, r3, r2, r3
	rsb	r1, r3, r2
	add	lr, r3, r1, lsr #1
	mov	r1, lr, lsr #4
	rsb	r3, r1, r1, asl #5
	rsb	lr, r3, r2
	add	ip, ip, #31
	rsb	r2, lr, ip
	strb	r0, [r5, #0]
	strb	r2, [r5, #1]
	b	.L363
.L372:
	.align	2
.L371:
	.word	.LC26
	.word	png_IDAT
	.size	png_write_IDAT, .-png_write_IDAT
	.section	.text.png_write_finish_row,"ax",%progbits
	.align	2
	.global	png_write_finish_row
	.hidden	png_write_finish_row
	.type	png_write_finish_row, %function
png_write_finish_row:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, fp, lr}
	mov	r4, r0
	ldr	r0, [r0, #256]
	ldr	r2, [r4, #236]
	add	r3, r0, #1
	cmp	r3, r2
	str	r3, [r4, #256]
	bcc	.L386
	ldrb	r1, [r4, #319]	@ zero_extendqisi2
	cmp	r1, #0
	bne	.L393
.L375:
	add	r5, r4, #144
.L392:
	mov	r0, r5
	mov	r1, #4
	bl	deflate
	cmp	r0, #0
	bne	.L381
.L394:
	ldr	r0, [r4, #160]
	cmp	r0, #0
	bne	.L392
	mov	r0, r4
	add	r1, r4, #200
	ldmia	r1, {r1, r2}	@ phole ldm
	bl	png_write_IDAT
	add	r3, r4, #200
	ldmia	r3, {r3, ip}	@ phole ldm
	str	r3, [r4, #156]
	str	ip, [r4, #160]
	mov	r0, r5
	mov	r1, #4
	bl	deflate
	cmp	r0, #0
	beq	.L394
.L381:
	cmp	r0, #1
	beq	.L383
	ldr	r1, [r4, #168]
	cmp	r1, #0
	mov	r0, r4
	ldreq	r1, .L396
	bl	png_error
	b	.L392
.L393:
	ldr	r7, [r4, #140]
	mov	ip, #0
	ands	r7, r7, #2
	str	ip, [r4, #256]
	beq	.L376
	ldrb	r0, [r4, #320]	@ zero_extendqisi2
	add	r2, r0, #1
	and	r3, r2, #255
	cmp	r3, #6
	strb	r3, [r4, #320]
	bhi	.L375
.L377:
	ldr	r0, [r4, #260]
	cmp	r0, #0
	beq	.L386
	ldrb	r2, [r4, #327]	@ zero_extendqisi2
	ldrb	lr, [r4, #324]	@ zero_extendqisi2
	mul	lr, r2, lr
	ldr	r1, [r4, #228]
	cmp	lr, #7
	mulle	r1, lr, r1
	movgt	lr, lr, lsr #3
	mulgt	r1, lr, r1
	addle	r1, r1, #7
	movle	r1, r1, lsr #3
	add	r2, r1, #1
	mov	r1, #0
	bl	memset
.L386:
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L383:
	ldr	r3, [r4, #160]
	ldr	r2, [r4, #204]
	cmp	r3, r2
	bcc	.L395
.L385:
	mov	r0, r5
	bl	deflateReset
	mov	r1, #0
	str	r1, [r4, #188]
	b	.L386
.L376:
	ldrb	r5, [r4, #320]	@ zero_extendqisi2
	ldr	fp, .L396+4
	ldr	r9, .L396+8
	ldr	sl, .L396+12
	ldr	r8, .L396+16
.L391:
	add	r5, r5, #1
	and	r5, r5, #255
	cmp	r5, #6
	strb	r5, [r4, #320]
	bhi	.L375
	ldr	r3, [r4, #228]
	ldr	r1, [fp, r5, asl #2]
	sub	ip, r3, #1
	ldr	lr, [r9, r5, asl #2]
	add	r6, ip, r1
	rsb	r0, lr, r6
	bl	__aeabi_uidiv
	mov	r6, r0
	ldr	r0, [r4, #232]
	ldr	r1, [sl, r5, asl #2]
	sub	r2, r0, #1
	ldr	r3, [r8, r5, asl #2]
	add	ip, r2, r1
	rsb	r0, r3, ip
	str	r6, [r4, #240]
	bl	__aeabi_uidiv
	cmp	r7, #0
	str	r0, [r4, #236]
	bne	.L377
	cmp	r6, #0
	beq	.L391
	cmp	r0, #0
	beq	.L391
	b	.L377
.L395:
	rsb	r2, r3, r2
	mov	r0, r4
	ldr	r1, [r4, #200]
	bl	png_write_IDAT
	b	.L385
.L397:
	.align	2
.L396:
	.word	.LC1
	.word	png_pass_inc
	.word	png_pass_start
	.word	png_pass_yinc
	.word	png_pass_ystart
	.size	png_write_finish_row, .-png_write_finish_row
	.section	.text.png_write_filtered_row,"ax",%progbits
	.align	2
	.global	png_write_filtered_row
	.hidden	png_write_filtered_row
	.type	png_write_filtered_row, %function
png_write_filtered_row:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	mov	r4, r0
	ldr	r0, [r0, #288]
	add	r3, r0, #1
	str	r1, [r4, #144]
	str	r3, [r4, #148]
	add	r5, r4, #144
	b	.L402
.L401:
	ldr	lr, [r4, #148]
	cmp	lr, #0
	beq	.L407
.L402:
	mov	r1, #0
	mov	r0, r5
	bl	deflate
	cmp	r0, #0
	beq	.L399
	ldr	r1, [r4, #168]
	cmp	r1, #0
	mov	r0, r4
	ldreq	r1, .L409
	bl	png_error
.L399:
	ldr	r1, [r4, #160]
	cmp	r1, #0
	bne	.L401
	ldr	r2, [r4, #204]
	mov	r0, r4
	ldr	r1, [r4, #200]
	bl	png_write_IDAT
	ldr	lr, [r4, #148]
	add	r2, r4, #200
	ldmia	r2, {r2, ip}	@ phole ldm
	cmp	lr, #0
	str	r2, [r4, #156]
	str	ip, [r4, #160]
	bne	.L402
.L407:
	ldr	r3, [r4, #260]
	cmp	r3, #0
	ldrne	r2, [r4, #264]
	mov	r0, r4
	strne	r3, [r4, #264]
	strne	r2, [r4, #260]
	bl	png_write_finish_row
	ldr	r2, [r4, #368]
	ldr	r3, [r4, #364]
	add	r2, r2, #1
	cmp	r3, #0
	str	r2, [r4, #368]
	beq	.L405
	cmp	r2, r3
	bcs	.L408
.L405:
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L408:
	mov	r0, r4
	bl	png_write_flush
	b	.L405
.L410:
	.align	2
.L409:
	.word	.LC1
	.size	png_write_filtered_row, .-png_write_filtered_row
	.section	.text.png_write_find_filter,"ax",%progbits
	.align	2
	.global	png_write_find_filter
	.hidden	png_write_find_filter
	.type	png_write_find_filter, %function
png_write_find_filter:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	mov	r4, r0
	ldrb	r0, [r0, #321]	@ zero_extendqisi2
	sub	sp, sp, #44
	str	r0, [sp, #4]
	ldrb	r3, [r4, #533]	@ zero_extendqisi2
	ldrb	sl, [r1, #11]	@ zero_extendqisi2
	subs	ip, r0, #8
	movne	ip, #1
	ldr	r1, [r1, #4]
	str	r3, [sp, #16]
	ands	r0, ip, r0, lsr #3
	add	r2, sl, #7
	mov	r2, r2, asr #3
	add	r0, r4, #260
	ldmia	r0, {r0, r3}	@ phole ldm
	beq	.L412
	cmp	r1, #0
	moveq	r5, r1
	beq	.L416
	sub	r6, r1, #1
	ands	r6, r6, #3
	mov	ip, #1
	mov	lr, #0
	beq	.L417
	ldrb	lr, [r3, #1]	@ zero_extendqisi2
	cmp	lr, #127
	rsbgt	lr, lr, #256
	cmp	r6, #1
	mov	ip, #2
	beq	.L417
	cmp	r6, ip
	beq	.L1084
	ldrb	r5, [r3, ip]	@ zero_extendqisi2
	cmp	r5, #127
	rsbgt	r5, r5, #256
	add	lr, lr, r5
	add	ip, ip, #1
.L1084:
	ldrb	sl, [r3, ip]	@ zero_extendqisi2
	cmp	sl, #127
	rsbgt	sl, sl, #256
	add	lr, lr, sl
	add	ip, ip, #1
	b	.L417
.L1250:
	add	ip, ip, #1
	ldrb	r7, [r3, ip]	@ zero_extendqisi2
	cmp	r7, #127
	movle	lr, r7
	rsbgt	lr, r7, #256
.L1170:
	add	r6, ip, #1
	ldrb	r7, [r3, r6]	@ zero_extendqisi2
	add	sl, r5, lr
	add	r5, ip, #2
	ldrb	r6, [r3, r5]	@ zero_extendqisi2
	cmp	r7, #127
	rsbgt	r7, r7, #256
	cmp	r6, #127
	add	lr, sl, r7
	rsbgt	r6, r6, #256
	add	lr, lr, r6
	add	ip, ip, #3
.L417:
	ldrb	r5, [r3, ip]	@ zero_extendqisi2
	cmp	r5, #127
	rsbgt	r5, r5, #256
	cmp	r1, ip
	add	r5, lr, r5
	bhi	.L1250
.L416:
	ldrb	lr, [r4, #532]	@ zero_extendqisi2
	cmp	lr, #2
	strne	r5, [sp, #20]
	beq	.L1251
.L419:
	ldr	sl, [sp, #4]
	cmp	sl, #16
	beq	.L1252
.L423:
	tst	sl, #16
	beq	.L441
	ldrb	ip, [r4, #532]	@ zero_extendqisi2
	cmp	ip, #2
	ldrne	r8, [sp, #20]
	beq	.L1253
.L443:
	ldr	r7, [r4, #268]
	cmp	r2, #0
	add	r6, r3, #1
	add	r5, r7, #1
	moveq	ip, r2
	beq	.L449
	ldrb	lr, [r3, #1]	@ zero_extendqisi2
	cmp	lr, #127
	rsb	ip, lr, #256
	sub	sl, r2, #1
	movle	ip, lr
	cmp	r2, #1
	strb	lr, [r7, #1]
	and	sl, sl, #3
	mov	lr, #2
	bls	.L1093
	cmp	sl, #0
	beq	.L1244
	cmp	sl, #1
	beq	.L1076
	cmp	sl, #2
	beq	.L1077
	ldrb	lr, [r3, #2]	@ zero_extendqisi2
	cmp	lr, #127
	rsb	fp, lr, #256
	movle	fp, lr
	strb	lr, [r7, #2]
	add	ip, ip, fp
	mov	lr, #3
.L1077:
	ldrb	sl, [r3, lr]	@ zero_extendqisi2
	cmp	sl, #127
	rsb	r9, sl, #256
	movle	r9, sl
	strb	sl, [r7, lr]
	add	ip, ip, r9
	add	lr, lr, #1
.L1076:
	ldrb	sl, [r3, lr]	@ zero_extendqisi2
	strb	sl, [r7, lr]
	add	lr, lr, #1
	cmp	sl, #127
	rsb	r9, sl, #256
	sub	fp, lr, #1
	movgt	sl, r9
	cmp	r2, fp
	add	ip, ip, sl
	bls	.L1093
.L1244:
	str	r8, [sp, #8]
	str	r5, [sp, #12]
	str	r6, [sp, #24]
.L452:
	ldrb	r9, [r3, lr]	@ zero_extendqisi2
	strb	r9, [r7, lr]
	add	sl, lr, #1
	ldrb	r8, [r3, sl]	@ zero_extendqisi2
	strb	r8, [r7, sl]
	add	r5, sl, #1
	ldrb	r6, [r3, r5]	@ zero_extendqisi2
	strb	r6, [r7, r5]
	add	sl, lr, #3
	rsb	fp, r9, #256
	cmp	r9, #127
	movgt	r9, fp
	ldrb	r5, [r3, sl]	@ zero_extendqisi2
	cmp	r8, #127
	rsb	fp, r8, #256
	movgt	r8, fp
	add	ip, ip, r9
	cmp	r6, #127
	rsb	r9, r6, #256
	add	lr, lr, #4
	movgt	r6, r9
	add	ip, ip, r8
	cmp	r5, #127
	sub	r9, lr, #1
	rsb	r8, r5, #256
	add	ip, ip, r6
	movle	r8, r5
	cmp	r2, r9
	strb	r5, [r7, sl]
	add	ip, ip, r8
	bhi	.L452
	ldr	r8, [sp, #8]
	ldr	r5, [sp, #12]
	ldr	r6, [sp, #24]
.L1093:
	add	r6, r6, r2
	add	r5, r5, r2
.L449:
	cmp	r1, r2
	bls	.L453
	ldrb	sl, [r3, #1]	@ zero_extendqisi2
	ldrb	r7, [r6, #0]	@ zero_extendqisi2
	rsb	lr, sl, r7
	and	lr, lr, #255
	mvn	sl, r2
	add	sl, sl, r1
	cmp	lr, #127
	and	sl, sl, #3
	strb	lr, [r5, #0]
	bgt	.L1254
.L1152:
	add	ip, ip, lr
	cmp	r8, ip
	bcc	.L453
	mov	lr, #1
	add	r7, lr, r2
	cmp	r1, r7
	add	r7, r3, lr
	bls	.L453
	cmp	sl, #0
	beq	.L456
	cmp	sl, lr
	beq	.L1074
	cmp	sl, #2
	beq	.L1075
	ldrb	r9, [r6, lr]	@ zero_extendqisi2
	ldrb	sl, [r7, #1]	@ zero_extendqisi2
	rsb	sl, sl, r9
	and	sl, sl, #255
	cmp	sl, #127
	strb	sl, [r5, lr]
	rsbgt	sl, sl, #256
	add	ip, ip, sl
	cmp	r8, ip
	bcc	.L453
	add	lr, lr, #1
	add	r7, r7, #1
.L1075:
	ldrb	r9, [r6, lr]	@ zero_extendqisi2
	ldrb	sl, [r7, #1]	@ zero_extendqisi2
	rsb	sl, sl, r9
	and	sl, sl, #255
	cmp	sl, #127
	strb	sl, [r5, lr]
	rsbgt	sl, sl, #256
	add	ip, ip, sl
	cmp	r8, ip
	bcc	.L453
	add	lr, lr, #1
	add	r7, r7, #1
.L1074:
	ldrb	r9, [r6, lr]	@ zero_extendqisi2
	ldrb	sl, [r7, #1]	@ zero_extendqisi2
	rsb	sl, sl, r9
	and	sl, sl, #255
	cmp	sl, #127
	strb	sl, [r5, lr]
	rsbgt	sl, sl, #256
	add	ip, ip, sl
	cmp	r8, ip
	bcc	.L453
	add	lr, lr, #1
	add	sl, lr, r2
	cmp	r1, sl
	add	r7, r7, #1
	bls	.L453
.L456:
	ldrb	r9, [r6, lr]	@ zero_extendqisi2
	ldrb	sl, [r7, #1]	@ zero_extendqisi2
	rsb	sl, sl, r9
	and	sl, sl, #255
	cmp	sl, #127
	strb	sl, [r5, lr]
	rsbgt	sl, sl, #256
	add	ip, ip, sl
	cmp	r8, ip
	bcc	.L453
	add	lr, lr, #1
	add	r7, r7, #1
	ldrb	r9, [r6, lr]	@ zero_extendqisi2
	ldrb	sl, [r7, #1]	@ zero_extendqisi2
	rsb	sl, sl, r9
	and	sl, sl, #255
	cmp	sl, #127
	strb	sl, [r5, lr]
	rsbgt	sl, sl, #256
.L1159:
	add	ip, ip, sl
	cmp	r8, ip
	bcc	.L453
	add	sl, lr, #1
	ldrb	fp, [r6, sl]	@ zero_extendqisi2
	ldrb	r9, [r7, #2]	@ zero_extendqisi2
	rsb	r9, r9, fp
	and	r9, r9, #255
	cmp	r9, #127
	strb	r9, [r5, sl]
	rsbgt	r9, r9, #256
	add	ip, ip, r9
	cmp	r8, ip
	bcc	.L453
	add	sl, lr, #2
	ldrb	fp, [r6, sl]	@ zero_extendqisi2
	ldrb	r9, [r7, #3]	@ zero_extendqisi2
	rsb	r9, r9, fp
	and	r9, r9, #255
	cmp	r9, #127
	strb	r9, [r5, sl]
	rsbgt	r9, r9, #256
	add	ip, ip, r9
	cmp	r8, ip
	bcc	.L453
	add	lr, lr, #3
	add	sl, lr, r2
	cmp	r1, sl
	add	r7, r7, #3
	bhi	.L456
.L453:
	ldrb	lr, [r4, #532]	@ zero_extendqisi2
	cmp	lr, #2
	beq	.L1255
.L457:
	ldr	sl, [sp, #20]
	cmp	sl, ip
	bhi	.L1256
.L441:
	str	r3, [sp, #8]
.L462:
	ldr	ip, [sp, #4]
	cmp	ip, #32
	beq	.L1257
	ldr	r7, [sp, #4]
	tst	r7, #32
	beq	.L470
.L1267:
	ldrb	lr, [r4, #532]	@ zero_extendqisi2
	cmp	lr, #2
	ldrne	r5, [sp, #20]
	beq	.L1258
.L472:
	cmp	r1, #0
	ldr	r6, [r4, #272]
	moveq	r7, r1
	beq	.L478
	ldrb	lr, [r3, #1]	@ zero_extendqisi2
	ldrb	ip, [r0, #1]	@ zero_extendqisi2
	rsb	r7, ip, lr
	and	r7, r7, #255
	sub	lr, r1, #1
	cmp	r7, #127
	and	lr, lr, #3
	strb	r7, [r6, #1]
	bgt	.L1259
.L1138:
	cmp	r5, r7
	bcc	.L1140
	cmp	r1, #1
	mov	ip, #2
	bls	.L1140
	cmp	lr, #0
	beq	.L482
	cmp	lr, #1
	beq	.L1047
	cmp	lr, ip
	beq	.L1048
	ldrb	r8, [r3, ip]	@ zero_extendqisi2
	ldrb	lr, [r0, ip]	@ zero_extendqisi2
	rsb	lr, lr, r8
	and	lr, lr, #255
	cmp	lr, #127
	strb	lr, [r6, ip]
	rsbgt	lr, lr, #256
	add	r7, r7, lr
	cmp	r5, r7
	bcc	.L1140
	add	ip, ip, #1
.L1048:
	ldrb	r8, [r3, ip]	@ zero_extendqisi2
	ldrb	lr, [r0, ip]	@ zero_extendqisi2
	rsb	lr, lr, r8
	and	lr, lr, #255
	cmp	lr, #127
	strb	lr, [r6, ip]
	rsbgt	lr, lr, #256
	add	r7, r7, lr
	cmp	r5, r7
	bcc	.L1140
	add	ip, ip, #1
.L1047:
	ldrb	r8, [r3, ip]	@ zero_extendqisi2
	ldrb	lr, [r0, ip]	@ zero_extendqisi2
	rsb	lr, lr, r8
	and	lr, lr, #255
	cmp	lr, #127
	strb	lr, [r6, ip]
	rsbgt	lr, lr, #256
	add	r7, r7, lr
	cmp	r5, r7
	bcc	.L1140
	add	ip, ip, #1
	sub	lr, ip, #1
	cmp	r1, lr
	bls	.L1140
.L482:
	ldrb	r8, [r3, ip]	@ zero_extendqisi2
	ldrb	lr, [r0, ip]	@ zero_extendqisi2
	rsb	lr, lr, r8
	and	lr, lr, #255
	cmp	lr, #127
	strb	lr, [r6, ip]
	rsbgt	lr, lr, #256
	add	r7, r7, lr
	cmp	r5, r7
	bcc	.L1140
	add	ip, ip, #1
	ldrb	r8, [r3, ip]	@ zero_extendqisi2
	ldrb	lr, [r0, ip]	@ zero_extendqisi2
	rsb	lr, lr, r8
	and	lr, lr, #255
	cmp	lr, #127
	strb	lr, [r6, ip]
	rsbgt	lr, lr, #256
.L1147:
	add	r7, r7, lr
	cmp	r5, r7
	bcc	.L1140
	add	lr, ip, #1
	ldrb	sl, [r3, lr]	@ zero_extendqisi2
	ldrb	r8, [r0, lr]	@ zero_extendqisi2
	rsb	r8, r8, sl
	and	r8, r8, #255
	cmp	r8, #127
	strb	r8, [r6, lr]
	rsbgt	r8, r8, #256
	add	r7, r7, r8
	cmp	r5, r7
	bcc	.L1140
	add	lr, ip, #2
	ldrb	sl, [r3, lr]	@ zero_extendqisi2
	ldrb	r8, [r0, lr]	@ zero_extendqisi2
	rsb	r8, r8, sl
	and	r8, r8, #255
	cmp	r8, #127
	strb	r8, [r6, lr]
	rsbgt	r8, r8, #256
	add	r7, r7, r8
	cmp	r5, r7
	bcc	.L1140
	add	lr, ip, #2
	cmp	r1, lr
	add	ip, ip, #3
	bhi	.L482
.L1140:
	ldrb	lr, [r4, #532]	@ zero_extendqisi2
.L478:
	cmp	lr, #2
	beq	.L1260
.L483:
	ldr	fp, [sp, #20]
	cmp	r7, fp
	ldrcc	ip, [r4, #272]
	strcc	r7, [sp, #20]
	strcc	ip, [sp, #8]
.L470:
	ldr	r5, [sp, #4]
	cmp	r5, #64
	beq	.L1261
.L463:
	ldr	lr, [sp, #4]
	tst	lr, #64
	beq	.L494
.L1268:
	ldrb	ip, [r4, #532]	@ zero_extendqisi2
	cmp	ip, #2
	beq	.L495
	ldr	r6, [sp, #20]
	str	r6, [sp, #12]
.L496:
	ldr	r5, [r4, #276]
	cmp	r2, #0
	add	r8, r3, #1
	add	r7, r5, #1
	add	r6, r0, #1
	moveq	ip, r2
	beq	.L502
	ldrb	ip, [r0, #1]	@ zero_extendqisi2
	ldrb	lr, [r3, #1]	@ zero_extendqisi2
	sub	fp, lr, ip, lsr #1
	and	lr, fp, #255
	cmp	lr, #127
	rsb	ip, lr, #256
	sub	sl, r2, #1
	movle	ip, lr
	cmp	r2, #1
	strb	lr, [r5, #1]
	and	sl, sl, #3
	mov	lr, #2
	bls	.L1108
	cmp	sl, #0
	beq	.L1206
	cmp	sl, #1
	beq	.L1038
	cmp	sl, #2
	beq	.L1039
	ldrb	fp, [r0, #2]	@ zero_extendqisi2
	ldrb	sl, [r3, #2]	@ zero_extendqisi2
	sub	r9, sl, fp, lsr #1
	and	lr, r9, #255
	cmp	lr, #127
	rsb	fp, lr, #256
	movle	fp, lr
	strb	lr, [r5, #2]
	add	ip, ip, fp
	mov	lr, #3
.L1039:
	ldrb	r9, [r0, lr]	@ zero_extendqisi2
	ldrb	fp, [r3, lr]	@ zero_extendqisi2
	sub	sl, fp, r9, lsr #1
	and	fp, sl, #255
	cmp	fp, #127
	rsb	r9, fp, #256
	movle	r9, fp
	strb	fp, [r5, lr]
	add	ip, ip, r9
	add	lr, lr, #1
.L1038:
	ldrb	r9, [r0, lr]	@ zero_extendqisi2
	ldrb	fp, [r3, lr]	@ zero_extendqisi2
	sub	sl, fp, r9, lsr #1
	and	sl, sl, #255
	strb	sl, [r5, lr]
	add	lr, lr, #1
	cmp	sl, #127
	rsb	r9, sl, #256
	sub	fp, lr, #1
	movgt	sl, r9
	cmp	r2, fp
	add	ip, ip, sl
	bls	.L1108
.L1206:
	str	r6, [sp, #24]
	str	r7, [sp, #28]
	str	r8, [sp, #32]
	str	r1, [sp, #36]
.L505:
	ldrb	sl, [r0, lr]	@ zero_extendqisi2
	ldrb	r9, [r3, lr]	@ zero_extendqisi2
	sub	r7, r9, sl, lsr #1
	and	r7, r7, #255
	strb	r7, [r5, lr]
	add	fp, lr, #1
	ldrb	r1, [r0, fp]	@ zero_extendqisi2
	ldrb	r8, [r3, fp]	@ zero_extendqisi2
	sub	r6, r8, r1, lsr #1
	and	r6, r6, #255
	strb	r6, [r5, fp]
	add	r8, fp, #1
	ldrb	r9, [r0, r8]	@ zero_extendqisi2
	ldrb	sl, [r3, r8]	@ zero_extendqisi2
	sub	r1, sl, r9, lsr #1
	and	r1, r1, #255
	strb	r1, [r5, r8]
	add	r8, lr, #3
	ldrb	r9, [r3, r8]	@ zero_extendqisi2
	ldrb	sl, [r0, r8]	@ zero_extendqisi2
	rsb	fp, r7, #256
	cmp	r7, #127
	movgt	r7, fp
	sub	sl, r9, sl, lsr #1
	cmp	r6, #127
	rsb	r9, r6, #256
	and	sl, sl, #255
	movgt	r6, r9
	rsb	fp, r1, #256
	add	r9, ip, r7
	cmp	r1, #127
	add	lr, lr, #4
	movgt	r1, fp
	add	r6, r9, r6
	cmp	sl, #127
	sub	r9, lr, #1
	rsb	r7, sl, #256
	add	r1, r6, r1
	movle	r7, sl
	cmp	r2, r9
	strb	sl, [r5, r8]
	add	ip, r1, r7
	bhi	.L505
	add	r6, sp, #24
	ldmia	r6, {r6, r7, r8}	@ phole ldm
	ldr	r1, [sp, #36]
.L1108:
	add	r7, r7, r2
	add	r8, r8, r2
	add	r6, r6, r2
.L502:
	cmp	r1, r2
	bls	.L506
	ldrb	sl, [r3, #1]	@ zero_extendqisi2
	ldrb	lr, [r6, #0]	@ zero_extendqisi2
	ldrb	fp, [r8, #0]	@ zero_extendqisi2
	add	r5, sl, lr
	sub	lr, fp, r5, lsr #1
	and	lr, lr, #255
	mvn	fp, r2
	add	r5, fp, r1
	cmp	lr, #127
	and	sl, r5, #1
	strb	lr, [r7, #0]
	bgt	.L1262
.L1134:
	ldr	r5, [sp, #12]
	add	ip, ip, lr
	cmp	r5, ip
	bcc	.L506
	mov	lr, #1
	add	fp, lr, r2
	cmp	r1, fp
	add	r5, r3, lr
	bls	.L506
	cmp	sl, #0
	beq	.L1203
	ldrb	r9, [r6, lr]	@ zero_extendqisi2
	ldrb	sl, [r5, #1]	@ zero_extendqisi2
	ldrb	fp, [r8, lr]	@ zero_extendqisi2
	add	sl, sl, r9
	sub	r9, fp, sl, lsr lr
	and	sl, r9, #255
	cmp	sl, #127
	strb	sl, [r7, lr]
	ldr	r9, [sp, #12]
	rsbgt	sl, sl, #256
	add	ip, ip, sl
	cmp	r9, ip
	bcc	.L506
	add	lr, lr, #1
	add	sl, lr, r2
	cmp	r1, sl
	add	r5, r5, #1
	bls	.L506
.L1203:
	ldr	sl, [sp, #12]
	str	r0, [sp, #12]
	b	.L509
.L1263:
	ldrb	fp, [r5, #1]	@ zero_extendqisi2
	ldrb	r9, [r6, lr]	@ zero_extendqisi2
	ldrb	r0, [r8, lr]	@ zero_extendqisi2
	add	r9, fp, r9
	sub	r0, r0, r9, lsr #1
	and	r0, r0, #255
	cmp	r0, #127
	strb	r0, [r7, lr]
	rsbgt	r0, r0, #256
.L1137:
	add	ip, ip, r0
	add	lr, lr, #1
	cmp	sl, ip
	add	r0, lr, r2
	bcc	.L1204
	cmp	r1, r0
	add	r5, r5, #1
	bls	.L1204
.L509:
	ldrb	fp, [r5, #1]	@ zero_extendqisi2
	ldrb	r9, [r6, lr]	@ zero_extendqisi2
	ldrb	r0, [r8, lr]	@ zero_extendqisi2
	add	r9, fp, r9
	sub	fp, r0, r9, lsr #1
	and	r0, fp, #255
	cmp	r0, #127
	strb	r0, [r7, lr]
	rsbgt	r0, r0, #256
	add	ip, ip, r0
	cmp	sl, ip
	add	lr, lr, #1
	add	r5, r5, #1
	bcs	.L1263
.L1204:
	ldr	r0, [sp, #12]
.L506:
	ldrb	lr, [r4, #532]	@ zero_extendqisi2
	cmp	lr, #2
	beq	.L1264
.L510:
	ldr	fp, [sp, #20]
	cmp	ip, fp
	ldrcc	lr, [r4, #276]
	strcc	ip, [sp, #20]
	strcc	lr, [sp, #8]
.L494:
	ldr	r5, [sp, #4]
	cmp	r5, #128
	beq	.L1265
	tst	r5, #128
	mov	r7, r5
	bne	.L1266
.L493:
	mov	r0, r4
	ldr	r1, [sp, #8]
	bl	png_write_filtered_row
	ldrb	ip, [r4, #533]	@ zero_extendqisi2
	cmp	ip, #0
	beq	.L557
	ldr	r2, [sp, #16]
	cmp	r2, #1
	movle	r1, #1
	ble	.L555
	ldr	lr, [r4, #536]
	ldrb	r1, [lr, #0]	@ zero_extendqisi2
	cmp	r2, #2
	sub	r6, r2, #2
	mov	r5, r2
	strb	r1, [lr, #1]
	and	r2, r6, #3
	mov	r3, #2
	ble	.L1123
	cmp	r2, #0
	beq	.L1181
	cmp	r2, #1
	beq	.L1014
	cmp	r2, #2
	ldrne	r3, [r4, #536]
	ldrneb	r2, [r3, #1]	@ zero_extendqisi2
	strneb	r2, [r3, #2]
	ldr	lr, [r4, #536]
	movne	r3, #3
	add	r1, r3, lr
	ldrb	r7, [r1, #-1]	@ zero_extendqisi2
	strb	r7, [lr, r3]
	add	r3, r3, #1
.L1014:
	ldr	r6, [r4, #536]
	add	r2, r3, r6
	ldrb	r0, [r2, #-1]	@ zero_extendqisi2
	strb	r0, [r6, r3]
	ldr	r7, [sp, #16]
	add	r3, r3, #1
	cmp	r7, r3
	movgt	ip, r7
	ble	.L1123
.L556:
	ldr	r6, [r4, #536]
	add	r2, r3, r6
	ldrb	r0, [r2, #-1]	@ zero_extendqisi2
	strb	r0, [r6, r3]
	ldr	r1, [r4, #536]
	ldrb	lr, [r3, r1]	@ zero_extendqisi2
	add	r2, r3, #1
	strb	lr, [r1, r2]
	ldr	r0, [r4, #536]
	ldrb	lr, [r2, r0]	@ zero_extendqisi2
	add	r6, r2, #1
	strb	lr, [r0, r6]
	ldr	r6, [r4, #536]
	add	r2, r3, #3
	add	r1, r2, r6
	ldrb	r0, [r1, #-1]	@ zero_extendqisi2
	add	r3, r3, #4
	cmp	ip, r3
	strb	r0, [r6, r2]
	bgt	.L556
.L1123:
	ldr	r1, [sp, #16]
.L555:
	ldr	r6, [sp, #8]
	ldr	r3, [r4, #536]
	ldrb	ip, [r6, #0]	@ zero_extendqisi2
	strb	ip, [r3, r1]
.L557:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L412:
	ldr	sl, [sp, #4]
	mvn	r9, #-2147483648
	cmp	sl, #16
	str	r9, [sp, #20]
	bne	.L423
.L1252:
	ldr	lr, [r4, #268]
	cmp	r2, #0
	add	fp, r3, #1
	add	ip, lr, #1
	str	fp, [sp, #8]
	moveq	lr, fp
	moveq	r8, r2
	beq	.L425
	orr	sl, ip, fp
	tst	sl, #3
	add	r5, lr, #5
	add	r7, r3, #5
	movne	r6, #0
	moveq	r6, #1
	cmp	fp, r5
	cmpls	ip, r7
	movls	r5, #0
	movhi	r5, #1
	cmp	r2, #3
	movls	r6, #0
	andhi	r6, r6, #1
	tst	r6, r5
	beq	.L426
	mov	r8, r2, lsr #2
	movs	r7, r8, asl #2
	moveq	r5, ip
	ldreq	r6, [sp, #8]
	beq	.L428
	ldr	r5, [r3, #1]
	mov	r6, #1
	sub	r9, r8, #1
	cmp	r6, r8
	str	r5, [lr, #1]
	and	sl, r9, #3
	add	lr, lr, #4
	add	r5, r3, #4
	bcs	.L1089
	cmp	sl, #0
	beq	.L429
	cmp	sl, #1
	beq	.L1062
	cmp	sl, #2
	ldrne	r6, [r5, #1]
	strne	r6, [lr, #1]
	addne	r5, r5, #4
	ldr	r9, [r5, #1]
	addne	lr, lr, #4
	movne	r6, #2
	str	r9, [lr, #1]
	add	r6, r6, #1
	add	r5, r5, #4
	add	lr, lr, #4
.L1062:
	ldr	sl, [r5, #1]
	add	r6, r6, #1
	cmp	r6, r8
	str	sl, [lr, #1]
	add	r5, r5, #4
	add	lr, lr, #4
	bcs	.L1089
.L429:
	ldr	fp, [r5, #1]
	str	fp, [lr, #1]
	add	r9, r5, #4
	ldr	fp, [r9, #1]
	add	sl, lr, #4
	str	fp, [sl, #1]
	ldr	r9, [r9, #5]
	str	r9, [sl, #5]
	add	r6, r6, #4
	ldr	sl, [r5, #13]
	cmp	r6, r8
	str	sl, [lr, #13]
	add	r5, r5, #16
	add	lr, lr, #16
	bcc	.L429
.L1089:
	ldr	r9, [sp, #8]
	cmp	r2, r7
	add	r6, r9, r7
	add	r5, ip, r7
	beq	.L430
.L428:
	ldrb	lr, [r6, #0]	@ zero_extendqisi2
	mvn	r8, r7
	add	sl, r7, #1
	add	r8, r8, r2
	cmp	r2, sl
	strb	lr, [r5, #0]
	and	r8, r8, #3
	mov	lr, #1
	bls	.L430
	cmp	r8, #0
	beq	.L431
	cmp	r8, #1
	beq	.L1060
	cmp	r8, #2
	ldrneb	lr, [r6, #1]	@ zero_extendqisi2
	strneb	lr, [r5, #1]
	movne	lr, #2
	ldrb	sl, [r6, lr]	@ zero_extendqisi2
	strb	sl, [r5, lr]
	add	lr, lr, #1
.L1060:
	ldrb	sl, [r6, lr]	@ zero_extendqisi2
	strb	sl, [r5, lr]
	add	lr, lr, #1
	add	r8, lr, r7
	cmp	r2, r8
	bls	.L430
.L431:
	ldrb	sl, [r6, lr]	@ zero_extendqisi2
	strb	sl, [r5, lr]
	add	r8, lr, #1
	ldrb	r9, [r6, r8]	@ zero_extendqisi2
	strb	r9, [r5, r8]
	add	r9, r8, #1
	ldrb	sl, [r6, r9]	@ zero_extendqisi2
	strb	sl, [r5, r9]
	add	r8, lr, #3
	add	lr, lr, #4
	ldrb	sl, [r6, r8]	@ zero_extendqisi2
	add	r9, lr, r7
	cmp	r2, r9
	strb	sl, [r5, r8]
	bhi	.L431
.L430:
	ldr	sl, [sp, #8]
	add	ip, ip, r2
	add	lr, sl, r2
	mov	r8, r2
.L425:
	cmp	r1, r2
	bls	.L433
	mvn	r6, r2
	ldr	fp, [sp, #8]
	add	r6, r6, r1
	orr	r5, lr, ip
	add	r7, r6, #1
	orr	sl, r5, fp
	str	r7, [sp, #28]
	tst	sl, #3
	ldr	r9, [sp, #28]
	add	r5, ip, #4
	movne	sl, #0
	moveq	sl, #1
	add	r7, lr, #4
	cmp	r9, #3
	movls	sl, #0
	andhi	sl, sl, #1
	cmp	lr, r5
	cmpls	ip, r7
	add	r9, r3, #5
	movls	r7, #0
	movhi	r7, #1
	cmp	fp, r5
	cmpls	ip, r9
	movls	r5, #0
	movhi	r5, #1
	and	fp, sl, r7
	ands	r5, fp, r5
	beq	.L434
	ldr	fp, [sp, #28]
	mov	r9, fp, lsr #2
	movs	fp, r9, asl #2
	str	r9, [sp, #12]
	str	fp, [sp, #24]
	ldreq	r6, [sp, #8]
	beq	.L436
	ldr	fp, [lr, #0]
	ldr	r6, [r3, #1]
	eor	r9, r6, fp
	mvn	sl, r9
	orr	r7, fp, #-2147483648
	bic	r8, r6, #-2147483648
	bic	fp, r8, #8388608
	bic	r5, sl, #2130706432
	orr	r9, r7, #8388608
	orr	r7, r9, #32768
	bic	r6, fp, #32768
	bic	sl, r5, #8323072
	orr	r8, r7, #128
	bic	r9, r6, #128
	bic	r5, sl, #32512
	ldr	r6, [sp, #12]
	rsb	fp, r9, r8
	bic	sl, r5, #127
	mov	r9, #1
	eor	r7, sl, fp
	sub	r8, r6, #1
	cmp	r9, r6
	mov	r5, #4
	str	r7, [ip, #0]
	add	r6, r3, r5
	and	r7, r8, #3
	bcs	.L1090
	cmp	r7, #0
	beq	.L1223
	cmp	r7, #1
	beq	.L1056
	cmp	r7, #2
	beq	.L1057
	ldr	sl, [r6, #1]
	ldr	r8, [lr, #4]
	eor	r5, sl, r8
	mvn	r9, r5
	orr	fp, r8, #-2147483648
	bic	r7, sl, #-2147483648
	bic	r5, r9, #2130706432
	orr	r8, fp, #8388608
	bic	sl, r7, #8388608
	bic	r9, r5, #8323072
	orr	fp, r8, #32768
	bic	r7, sl, #32768
	bic	r5, r9, #32512
	orr	r8, fp, #128
	bic	sl, r7, #128
	bic	r9, r5, #127
	rsb	fp, sl, r8
	eor	r8, r9, fp
	str	r8, [ip, #4]
	add	r6, r6, #4
	mov	r9, #2
	mov	r5, #8
.L1057:
	ldr	sl, [lr, r5]
	ldr	r8, [r6, #1]
	eor	fp, r8, sl
	mvn	r7, fp
	bic	r8, r8, #-2147483648
	orr	fp, sl, #-2147483648
	orr	sl, fp, #8388608
	bic	r7, r7, #2130706432
	bic	fp, r8, #8388608
	bic	r7, r7, #8323072
	bic	r8, fp, #32768
	orr	sl, sl, #32768
	bic	fp, r7, #32512
	orr	sl, sl, #128
	bic	r8, r8, #128
	rsb	r8, r8, sl
	bic	r7, fp, #127
	eor	fp, r7, r8
	str	fp, [ip, r5]
	add	r9, r9, #1
	add	r5, r5, #4
	add	r6, r6, #4
.L1056:
	ldr	sl, [lr, r5]
	ldr	r8, [r6, #1]
	eor	r7, r8, sl
	mvn	fp, r7
	orr	sl, sl, #-2147483648
	bic	r8, r8, #-2147483648
	bic	r7, fp, #2130706432
	bic	r8, r8, #8388608
	orr	fp, sl, #8388608
	orr	sl, fp, #32768
	bic	r7, r7, #8323072
	bic	fp, r8, #32768
	bic	r7, r7, #32512
	orr	sl, sl, #128
	bic	r8, fp, #128
	rsb	fp, r8, sl
	bic	sl, r7, #127
	ldr	r7, [sp, #12]
	add	r9, r9, #1
	eor	r8, sl, fp
	cmp	r9, r7
	str	r8, [ip, r5]
	add	r6, r6, #4
	add	r5, r5, #4
	bcs	.L1090
.L1223:
	str	r1, [sp, #32]
.L437:
	ldr	sl, [lr, r5]
	ldr	fp, [r6, #1]
	eor	r7, fp, sl
	mvn	r1, r7
	orr	r8, sl, #-2147483648
	bic	r7, fp, #-2147483648
	bic	sl, r1, #2130706432
	orr	fp, r8, #8388608
	bic	r7, r7, #8388608
	bic	r1, sl, #8323072
	orr	r8, fp, #32768
	bic	sl, r7, #32768
	orr	fp, r8, #128
	bic	r1, r1, #32512
	bic	r7, sl, #128
	rsb	sl, r7, fp
	bic	r8, r1, #127
	eor	fp, r8, sl
	str	fp, [ip, r5]
	add	sl, r5, #4
	add	fp, r6, #4
	ldr	r8, [lr, sl]
	ldr	r7, [fp, #1]
	eor	r1, r7, r8
	mvn	r1, r1
	orr	r8, r8, #-2147483648
	bic	r7, r7, #-2147483648
	bic	r1, r1, #2130706432
	orr	r8, r8, #8388608
	bic	r7, r7, #8388608
	bic	r1, r1, #8323072
	orr	r8, r8, #32768
	bic	r7, r7, #32768
	orr	r8, r8, #128
	bic	r1, r1, #32512
	bic	r7, r7, #128
	rsb	r7, r7, r8
	bic	r1, r1, #127
	eor	r7, r1, r7
	str	r7, [ip, sl]
	add	sl, sl, #4
	ldr	r7, [fp, #5]
	ldr	fp, [lr, sl]
	eor	r8, r7, fp
	mvn	r1, r8
	bic	r7, r7, #-2147483648
	orr	r8, fp, #-2147483648
	orr	r8, r8, #8388608
	bic	fp, r1, #2130706432
	bic	r7, r7, #8388608
	bic	r1, fp, #8323072
	bic	r7, r7, #32768
	orr	fp, r8, #32768
	orr	r8, fp, #128
	bic	r1, r1, #32512
	bic	fp, r7, #128
	rsb	r7, fp, r8
	bic	r8, r1, #127
	eor	fp, r8, r7
	str	fp, [ip, sl]
	add	sl, r5, #12
	ldr	r7, [r6, #13]
	ldr	r8, [lr, sl]
	eor	r1, r7, r8
	mvn	fp, r1
	orr	r8, r8, #-2147483648
	bic	r7, r7, #-2147483648
	bic	r1, fp, #2130706432
	bic	r7, r7, #8388608
	orr	fp, r8, #8388608
	orr	r8, fp, #32768
	bic	r1, r1, #8323072
	bic	fp, r7, #32768
	bic	r1, r1, #32512
	orr	r8, r8, #128
	bic	r7, fp, #128
	rsb	fp, r7, r8
	bic	r8, r1, #127
	ldr	r1, [sp, #12]
	add	r9, r9, #4
	eor	r7, r8, fp
	cmp	r9, r1
	str	r7, [ip, sl]
	add	r5, r5, #16
	add	r6, r6, #16
	bcc	.L437
	ldr	r1, [sp, #32]
.L1090:
	ldr	sl, [sp, #24]
	ldr	r9, [sp, #28]
	ldr	fp, [sp, #8]
	cmp	r9, sl
	add	lr, lr, sl
	add	r6, fp, sl
	add	ip, ip, sl
	add	r8, r2, sl
	beq	.L433
.L436:
	ldrb	r9, [lr, #0]	@ zero_extendqisi2
	ldrb	sl, [r6, #0]	@ zero_extendqisi2
	add	r7, r8, #1
	mvn	r5, r8
	add	r5, r5, r1
	rsb	sl, sl, r9
	cmp	r1, r7
	strb	sl, [ip, #0]
	and	r7, r5, #3
	mov	r5, #1
	bls	.L433
	cmp	r7, #0
	beq	.L438
	cmp	r7, #1
	beq	.L1054
	cmp	r7, #2
	ldrneb	r5, [r6, #1]	@ zero_extendqisi2
	ldrneb	r7, [lr, #1]	@ zero_extendqisi2
	rsbne	r5, r5, r7
	strneb	r5, [ip, #1]
	movne	r5, #2
	ldrb	sl, [lr, r5]	@ zero_extendqisi2
	ldrb	r7, [r6, r5]	@ zero_extendqisi2
	rsb	r7, r7, sl
	strb	r7, [ip, r5]
	add	r5, r5, #1
.L1054:
	ldrb	sl, [lr, r5]	@ zero_extendqisi2
	ldrb	r7, [r6, r5]	@ zero_extendqisi2
	rsb	r7, r7, sl
	strb	r7, [ip, r5]
	add	r5, r5, #1
	add	r7, r5, r8
	cmp	r1, r7
	bls	.L433
.L438:
	ldrb	sl, [lr, r5]	@ zero_extendqisi2
	ldrb	fp, [r6, r5]	@ zero_extendqisi2
	rsb	r7, fp, sl
	strb	r7, [ip, r5]
	add	r7, r5, #1
	ldrb	r9, [lr, r7]	@ zero_extendqisi2
	ldrb	sl, [r6, r7]	@ zero_extendqisi2
	rsb	fp, sl, r9
	strb	fp, [ip, r7]
	add	fp, r7, #1
	ldrb	r9, [lr, fp]	@ zero_extendqisi2
	ldrb	sl, [r6, fp]	@ zero_extendqisi2
	rsb	sl, sl, r9
	strb	sl, [ip, fp]
	add	r7, r5, #3
	ldrb	fp, [lr, r7]	@ zero_extendqisi2
	ldrb	r9, [r6, r7]	@ zero_extendqisi2
	add	r5, r5, #4
	add	sl, r5, r8
	rsb	r9, r9, fp
	cmp	r1, sl
	strb	r9, [ip, r7]
	bhi	.L438
.L433:
	ldr	r7, [sp, #4]
	ldr	ip, [r4, #268]
	tst	r7, #32
	str	ip, [sp, #8]
	beq	.L470
	b	.L1267
.L1257:
	ldr	lr, [r4, #272]
	cmp	r1, #0
	str	lr, [sp, #8]
	beq	.L463
	add	r8, r3, #1
	add	r7, r0, #1
	add	r6, lr, #1
	orr	ip, r7, r8
	orr	r5, ip, r6
	tst	r5, #3
	add	ip, lr, #5
	movne	r5, #0
	moveq	r5, #1
	add	sl, r3, #5
	cmp	r1, #3
	movls	r5, #0
	andhi	r5, r5, #1
	cmp	r8, ip
	cmpls	r6, sl
	add	sl, r0, #5
	movls	lr, #0
	movhi	lr, #1
	cmp	r7, ip
	cmpls	r6, sl
	movls	ip, #0
	movhi	ip, #1
	and	lr, r5, lr
	tst	lr, ip
	beq	.L464
	mov	r5, r1, lsr #2
	movs	r9, r5, asl #2
	str	r5, [sp, #28]
	str	r9, [sp, #12]
	beq	.L465
	ldr	lr, [r3, #1]
	ldr	r9, [r0, #1]
	eor	ip, r9, lr
	mov	fp, r5
	mvn	sl, ip
	orr	r5, lr, #-2147483648
	bic	lr, r9, #-2147483648
	bic	ip, sl, #2130706432
	orr	r9, r5, #8388608
	bic	sl, lr, #8388608
	bic	ip, ip, #8323072
	orr	r5, r9, #32768
	bic	r9, sl, #32768
	orr	r5, r5, #128
	bic	sl, ip, #32512
	bic	lr, r9, #128
	rsb	r9, lr, r5
	bic	ip, sl, #127
	eor	r5, ip, r9
	mov	sl, #1
	ldr	r9, [sp, #8]
	sub	lr, fp, #1
	cmp	sl, fp
	str	sl, [sp, #24]
	add	ip, r9, #4
	str	r5, [r9, #1]
	and	sl, lr, #3
	add	r5, r3, #4
	add	lr, r0, #4
	bcs	.L1098
	cmp	sl, #0
	beq	.L1235
	cmp	sl, #1
	beq	.L1068
	cmp	sl, #2
	beq	.L1069
	ldr	fp, [r5, #1]
	ldr	r9, [lr, #1]
	eor	sl, r9, fp
	mvn	sl, sl
	orr	fp, fp, #-2147483648
	bic	r9, r9, #-2147483648
	bic	sl, sl, #2130706432
	orr	fp, fp, #8388608
	bic	r9, r9, #8388608
	bic	sl, sl, #8323072
	orr	fp, fp, #32768
	bic	r9, r9, #32768
	orr	fp, fp, #128
	bic	sl, sl, #32512
	bic	r9, r9, #128
	rsb	r9, r9, fp
	bic	sl, sl, #127
	eor	fp, sl, r9
	mov	r9, #2
	str	fp, [ip, #1]
	str	r9, [sp, #24]
	add	r5, r5, #4
	add	lr, lr, #4
	add	ip, ip, #4
.L1069:
	ldr	fp, [r5, #1]
	ldr	r9, [lr, #1]
	eor	sl, r9, fp
	mvn	sl, sl
	orr	fp, fp, #-2147483648
	bic	r9, r9, #-2147483648
	bic	sl, sl, #2130706432
	orr	fp, fp, #8388608
	bic	r9, r9, #8388608
	bic	sl, sl, #8323072
	orr	fp, fp, #32768
	bic	r9, r9, #32768
	orr	fp, fp, #128
	bic	sl, sl, #32512
	bic	r9, r9, #128
	rsb	r9, r9, fp
	bic	sl, sl, #127
	eor	r9, sl, r9
	str	r9, [ip, #1]
	ldr	fp, [sp, #24]
	add	r9, fp, #1
	str	r9, [sp, #24]
	add	r5, r5, #4
	add	lr, lr, #4
	add	ip, ip, #4
.L1068:
	ldr	fp, [r5, #1]
	ldr	r9, [lr, #1]
	eor	sl, r9, fp
	mvn	sl, sl
	orr	fp, fp, #-2147483648
	bic	r9, r9, #-2147483648
	bic	sl, sl, #2130706432
	orr	fp, fp, #8388608
	bic	r9, r9, #8388608
	bic	sl, sl, #8323072
	orr	fp, fp, #32768
	bic	r9, r9, #32768
	orr	fp, fp, #128
	bic	sl, sl, #32512
	bic	r9, r9, #128
	rsb	r9, r9, fp
	bic	sl, sl, #127
	ldr	fp, [sp, #24]
	eor	r9, sl, r9
	ldr	sl, [sp, #28]
	add	fp, fp, #1
	cmp	fp, sl
	str	fp, [sp, #24]
	add	r5, r5, #4
	str	r9, [ip, #1]
	add	lr, lr, #4
	add	ip, ip, #4
	bcs	.L1098
.L1235:
	str	r7, [sp, #8]
	str	r6, [sp, #32]
	str	r8, [sp, #36]
	ldr	sl, [sp, #24]
.L466:
	ldr	r9, [r5, #1]
	ldr	fp, [lr, #1]
	eor	r7, fp, r9
	mvn	r6, r7
	orr	r8, r9, #-2147483648
	bic	r7, fp, #-2147483648
	bic	r9, r6, #2130706432
	orr	fp, r8, #8388608
	bic	r7, r7, #8388608
	bic	r6, r9, #8323072
	orr	r8, fp, #32768
	bic	r9, r7, #32768
	orr	fp, r8, #128
	bic	r6, r6, #32512
	bic	r7, r9, #128
	rsb	r9, r7, fp
	bic	r8, r6, #127
	eor	fp, r8, r9
	str	fp, [ip, #1]
	add	r9, lr, #4
	add	fp, r5, #4
	ldr	r7, [r9, #1]
	ldr	r8, [fp, #1]
	eor	r6, r7, r8
	mvn	r6, r6
	orr	r8, r8, #-2147483648
	bic	r7, r7, #-2147483648
	bic	r6, r6, #2130706432
	orr	r8, r8, #8388608
	bic	r7, r7, #8388608
	bic	r6, r6, #8323072
	orr	r8, r8, #32768
	bic	r7, r7, #32768
	bic	r6, r6, #32512
	orr	r8, r8, #128
	bic	r7, r7, #128
	rsb	r7, r7, r8
	bic	r8, r6, #127
	eor	r6, r8, r7
	add	r7, ip, #4
	str	r6, [r7, #1]
	ldr	r8, [fp, #5]
	ldr	r6, [r9, #5]
	eor	r9, r6, r8
	mvn	r9, r9
	orr	r8, r8, #-2147483648
	bic	r6, r6, #-2147483648
	bic	r9, r9, #2130706432
	orr	r8, r8, #8388608
	bic	r6, r6, #8388608
	bic	r9, r9, #8323072
	orr	r8, r8, #32768
	bic	r6, r6, #32768
	bic	r6, r6, #128
	bic	r9, r9, #32512
	orr	r8, r8, #128
	rsb	r8, r6, r8
	bic	r9, r9, #127
	eor	r8, r9, r8
	str	r8, [r7, #5]
	ldr	r8, [r5, #13]
	ldr	r7, [lr, #13]
	eor	r6, r7, r8
	mvn	r9, r6
	orr	r8, r8, #-2147483648
	bic	r7, r7, #-2147483648
	bic	r6, r9, #2130706432
	bic	r7, r7, #8388608
	orr	r9, r8, #8388608
	orr	r8, r9, #32768
	bic	r6, r6, #8323072
	bic	r9, r7, #32768
	bic	r6, r6, #32512
	orr	r8, r8, #128
	bic	r7, r9, #128
	rsb	r9, r7, r8
	bic	r8, r6, #127
	ldr	r6, [sp, #28]
	add	sl, sl, #4
	eor	r7, r8, r9
	cmp	sl, r6
	str	r7, [ip, #13]
	add	r5, r5, #16
	add	lr, lr, #16
	add	ip, ip, #16
	bcc	.L466
	ldr	r7, [sp, #8]
	add	r6, sp, #32
	ldmia	r6, {r6, r8}	@ phole ldm
.L1098:
	ldr	r9, [sp, #12]
	cmp	r1, r9
	add	r8, r8, r9
	add	r6, r6, r9
	add	r7, r7, r9
	beq	.L467
.L465:
	ldr	fp, [sp, #12]
	ldrb	sl, [r8, #0]	@ zero_extendqisi2
	ldrb	r5, [r7, #0]	@ zero_extendqisi2
	add	lr, fp, #1
	mvn	ip, fp
	add	ip, ip, r1
	rsb	r5, r5, sl
	cmp	r1, lr
	strb	r5, [r6, #0]
	and	lr, ip, #3
	mov	ip, #1
	bls	.L467
	cmp	lr, #0
	beq	.L1232
	cmp	lr, #1
	beq	.L1066
	cmp	lr, #2
	ldrneb	ip, [r7, #1]	@ zero_extendqisi2
	ldrneb	lr, [r8, #1]	@ zero_extendqisi2
	rsbne	ip, ip, lr
	strneb	ip, [r6, #1]
	movne	ip, #2
	ldrb	r5, [r8, ip]	@ zero_extendqisi2
	ldrb	lr, [r7, ip]	@ zero_extendqisi2
	rsb	fp, lr, r5
	strb	fp, [r6, ip]
	add	ip, ip, #1
.L1066:
	ldrb	lr, [r7, ip]	@ zero_extendqisi2
	ldrb	r5, [r8, ip]	@ zero_extendqisi2
	rsb	fp, lr, r5
	strb	fp, [r6, ip]
	ldr	fp, [sp, #12]
	add	ip, ip, #1
	add	lr, ip, fp
	cmp	r1, lr
	bls	.L467
.L468:
	ldrb	r5, [r8, ip]	@ zero_extendqisi2
	ldrb	lr, [r7, ip]	@ zero_extendqisi2
	rsb	r9, lr, r5
	strb	r9, [r6, ip]
	add	r9, ip, #1
	ldrb	sl, [r8, r9]	@ zero_extendqisi2
	ldrb	r5, [r7, r9]	@ zero_extendqisi2
	rsb	lr, r5, sl
	strb	lr, [r6, r9]
	add	lr, r9, #1
	ldrb	sl, [r8, lr]	@ zero_extendqisi2
	ldrb	r5, [r7, lr]	@ zero_extendqisi2
	rsb	r9, r5, sl
	strb	r9, [r6, lr]
	add	lr, ip, #3
	ldrb	r9, [r8, lr]	@ zero_extendqisi2
	ldrb	sl, [r7, lr]	@ zero_extendqisi2
	add	ip, ip, #4
	add	r5, ip, fp
	rsb	sl, sl, r9
	cmp	r1, r5
	strb	sl, [r6, lr]
	bhi	.L468
.L467:
	ldr	lr, [sp, #4]
	ldr	r6, [r4, #272]
	tst	lr, #64
	str	r6, [sp, #8]
	beq	.L494
	b	.L1268
.L1265:
	ldr	r5, [r4, #280]
	add	r6, r0, #1
	cmp	r2, #0
	add	lr, r3, #1
	add	ip, r5, #1
	str	r6, [sp, #4]
	beq	.L517
	mov	r9, r6
	orr	r6, r6, lr
	orr	r8, r6, ip
	tst	r8, #3
	add	r6, r5, #5
	movne	r8, #0
	moveq	r8, #1
	add	sl, r3, #5
	cmp	r2, #3
	movls	r8, #0
	andhi	r8, r8, #1
	cmp	lr, r6
	cmpls	ip, sl
	add	sl, r0, #5
	movls	r7, #0
	movhi	r7, #1
	cmp	r9, r6
	cmpls	ip, sl
	movls	r6, #0
	movhi	r6, #1
	and	r7, r8, r7
	tst	r7, r6
	beq	.L518
	mov	sl, r2, lsr #2
	movs	fp, sl, asl #2
	str	sl, [sp, #20]
	str	fp, [sp, #8]
	moveq	r7, r9
	moveq	r6, ip
	moveq	r8, lr
	beq	.L520
	ldr	r9, [r3, #1]
	ldr	fp, [r0, #1]
	eor	sl, fp, r9
	mvn	r6, sl
	orr	r8, r9, #-2147483648
	bic	r7, fp, #-2147483648
	orr	r9, r8, #8388608
	bic	sl, r6, #2130706432
	bic	fp, r7, #8388608
	bic	r6, sl, #8323072
	bic	r7, fp, #32768
	orr	sl, r9, #32768
	bic	fp, r6, #32512
	bic	r9, r7, #128
	orr	r8, sl, #128
	ldr	r7, [sp, #20]
	bic	r6, fp, #127
	rsb	sl, r9, r8
	mov	fp, #1
	eor	r8, r6, sl
	sub	r9, r7, #1
	cmp	fp, r7
	str	r8, [r5, #1]
	add	r7, r3, #4
	and	r8, r9, #3
	add	r5, r5, #4
	add	r6, r0, #4
	bcs	.L1113
	cmp	r8, #0
	beq	.L1192
	cmp	r8, #1
	beq	.L1024
	cmp	r8, #2
	beq	.L1025
	ldr	r9, [r7, #1]
	ldr	sl, [r6, #1]
	eor	fp, sl, r9
	mvn	r8, fp
	bic	sl, sl, #-2147483648
	orr	fp, r9, #-2147483648
	orr	r9, fp, #8388608
	bic	r8, r8, #2130706432
	bic	fp, sl, #8388608
	bic	r8, r8, #8323072
	bic	sl, fp, #32768
	orr	r9, r9, #32768
	bic	fp, r8, #32512
	orr	r9, r9, #128
	bic	sl, sl, #128
	rsb	sl, sl, r9
	bic	r8, fp, #127
	eor	fp, r8, sl
	str	fp, [r5, #1]
	add	r7, r7, #4
	add	r6, r6, #4
	add	r5, r5, #4
	mov	fp, #2
.L1025:
	ldr	r9, [r7, #1]
	ldr	sl, [r6, #1]
	eor	r8, sl, r9
	mvn	r8, r8
	orr	r9, r9, #-2147483648
	bic	sl, sl, #-2147483648
	bic	r8, r8, #2130706432
	orr	r9, r9, #8388608
	bic	sl, sl, #8388608
	bic	r8, r8, #8323072
	orr	r9, r9, #32768
	bic	sl, sl, #32768
	bic	r8, r8, #32512
	orr	r9, r9, #128
	bic	sl, sl, #128
	rsb	sl, sl, r9
	bic	r8, r8, #127
	eor	sl, r8, sl
	str	sl, [r5, #1]
	add	fp, fp, #1
	add	r7, r7, #4
	add	r6, r6, #4
	add	r5, r5, #4
.L1024:
	ldr	r9, [r7, #1]
	ldr	sl, [r6, #1]
	eor	r8, sl, r9
	orr	r9, r9, #-2147483648
	bic	sl, sl, #-2147483648
	mvn	r8, r8
	orr	r9, r9, #8388608
	bic	sl, sl, #8388608
	bic	r8, r8, #2130706432
	orr	r9, r9, #32768
	bic	sl, sl, #32768
	orr	r9, r9, #128
	bic	r8, r8, #8323072
	bic	sl, sl, #128
	rsb	sl, sl, r9
	bic	r8, r8, #32512
	ldr	r9, [sp, #20]
	bic	r8, r8, #127
	add	fp, fp, #1
	eor	sl, r8, sl
	cmp	fp, r9
	str	sl, [r5, #1]
	add	r7, r7, #4
	add	r6, r6, #4
	add	r5, r5, #4
	bcs	.L1113
.L1192:
	str	ip, [sp, #12]
	str	lr, [sp, #24]
.L521:
	ldr	r9, [r7, #1]
	ldr	sl, [r6, #1]
	eor	lr, sl, r9
	mvn	ip, lr
	orr	r8, r9, #-2147483648
	bic	lr, sl, #-2147483648
	bic	r9, ip, #2130706432
	orr	sl, r8, #8388608
	bic	lr, lr, #8388608
	bic	ip, r9, #8323072
	orr	r8, sl, #32768
	bic	r9, lr, #32768
	orr	sl, r8, #128
	bic	ip, ip, #32512
	bic	lr, r9, #128
	rsb	r9, lr, sl
	bic	r8, ip, #127
	eor	sl, r8, r9
	str	sl, [r5, #1]
	add	r9, r7, #4
	add	sl, r6, #4
	ldr	r8, [r9, #1]
	ldr	lr, [sl, #1]
	eor	ip, lr, r8
	mvn	ip, ip
	orr	r8, r8, #-2147483648
	bic	lr, lr, #-2147483648
	bic	ip, ip, #2130706432
	orr	r8, r8, #8388608
	bic	lr, lr, #8388608
	bic	ip, ip, #8323072
	orr	r8, r8, #32768
	bic	lr, lr, #32768
	bic	ip, ip, #32512
	orr	r8, r8, #128
	bic	lr, lr, #128
	rsb	lr, lr, r8
	bic	r8, ip, #127
	eor	ip, r8, lr
	add	lr, r5, #4
	str	ip, [lr, #1]
	ldr	r8, [r9, #5]
	ldr	ip, [sl, #5]
	eor	sl, ip, r8
	mvn	r9, sl
	orr	r8, r8, #-2147483648
	bic	ip, ip, #-2147483648
	bic	sl, r9, #2130706432
	bic	ip, ip, #8388608
	orr	r9, r8, #8388608
	orr	r8, r9, #32768
	bic	sl, sl, #8323072
	bic	r9, ip, #32768
	orr	r8, r8, #128
	bic	ip, r9, #128
	bic	sl, sl, #32512
	rsb	r9, ip, r8
	bic	sl, sl, #127
	eor	r8, sl, r9
	str	r8, [lr, #5]
	ldr	lr, [r7, #13]
	ldr	r9, [r6, #13]
	eor	ip, r9, lr
	orr	r8, lr, #-2147483648
	mvn	sl, ip
	bic	lr, r9, #-2147483648
	bic	ip, sl, #2130706432
	orr	r9, r8, #8388608
	bic	sl, lr, #8388608
	bic	ip, ip, #8323072
	orr	r8, r9, #32768
	bic	r9, sl, #32768
	orr	r8, r8, #128
	bic	sl, ip, #32512
	bic	lr, r9, #128
	ldr	ip, [sp, #20]
	rsb	r9, lr, r8
	add	fp, fp, #4
	bic	r8, sl, #127
	eor	lr, r8, r9
	cmp	fp, ip
	str	lr, [r5, #13]
	add	r7, r7, #16
	add	r6, r6, #16
	add	r5, r5, #16
	bcc	.L521
	ldr	ip, [sp, #12]
	ldr	lr, [sp, #24]
.L1113:
	ldr	r9, [sp, #8]
	ldr	sl, [sp, #4]
	cmp	r2, r9
	add	r8, lr, r9
	add	r6, ip, r9
	add	r7, sl, r9
	beq	.L522
.L520:
	ldr	fp, [sp, #8]
	mvn	r5, fp
	ldr	fp, [sp, #8]
	ldrb	r9, [r8, #0]	@ zero_extendqisi2
	ldrb	sl, [r7, #0]	@ zero_extendqisi2
	str	r5, [sp, #12]
	add	r5, fp, #1
	cmp	r2, r5
	ldr	r5, [sp, #12]
	rsb	sl, sl, r9
	add	fp, r5, r2
	strb	sl, [r6, #0]
	and	fp, fp, #3
	mov	r5, #1
	bls	.L522
	cmp	fp, #0
	beq	.L1190
	cmp	fp, #1
	beq	.L1022
	cmp	fp, #2
	ldrneb	r5, [r7, #1]	@ zero_extendqisi2
	ldrneb	sl, [r8, #1]	@ zero_extendqisi2
	rsbne	r5, r5, sl
	strneb	r5, [r6, #1]
	movne	r5, #2
	ldrb	r9, [r8, r5]	@ zero_extendqisi2
	ldrb	sl, [r7, r5]	@ zero_extendqisi2
	rsb	sl, sl, r9
	strb	sl, [r6, r5]
	add	r5, r5, #1
.L1022:
	ldrb	r9, [r8, r5]	@ zero_extendqisi2
	ldrb	sl, [r7, r5]	@ zero_extendqisi2
	rsb	sl, sl, r9
	strb	sl, [r6, r5]
	ldr	r9, [sp, #8]
	add	r5, r5, #1
	add	sl, r5, r9
	cmp	r2, sl
	bls	.L522
.L1190:
	str	ip, [sp, #20]
	str	r3, [sp, #12]
.L523:
	ldrb	sl, [r8, r5]	@ zero_extendqisi2
	ldrb	ip, [r7, r5]	@ zero_extendqisi2
	rsb	r9, ip, sl
	strb	r9, [r6, r5]
	add	ip, r5, #1
	ldrb	r3, [r8, ip]	@ zero_extendqisi2
	ldrb	fp, [r7, ip]	@ zero_extendqisi2
	rsb	sl, fp, r3
	strb	sl, [r6, ip]
	add	fp, ip, #1
	ldrb	r9, [r8, fp]	@ zero_extendqisi2
	ldrb	r3, [r7, fp]	@ zero_extendqisi2
	rsb	sl, r3, r9
	strb	sl, [r6, fp]
	add	ip, r5, #3
	ldr	r3, [sp, #8]
	ldrb	fp, [r8, ip]	@ zero_extendqisi2
	ldrb	r9, [r7, ip]	@ zero_extendqisi2
	add	r5, r5, #4
	add	sl, r5, r3
	rsb	r9, r9, fp
	cmp	r2, sl
	strb	r9, [r6, ip]
	bhi	.L523
	ldr	ip, [sp, #20]
	ldr	r3, [sp, #12]
.L522:
	ldr	r5, [sp, #4]
	add	r5, r5, r2
	str	r5, [sp, #4]
	add	ip, ip, r2
	add	lr, lr, r2
.L517:
	cmp	r1, r2
	bls	.L525
	add	r5, r2, #1
	rsb	r6, r5, r1
	rsb	r2, r2, #0
	tst	r6, #1
	add	r3, r3, r2
	add	r0, r0, r2
	mov	r6, #0
	beq	.L1269
	ldr	r9, [sp, #4]
	ldrb	r2, [r0, r5]	@ zero_extendqisi2
	ldrb	sl, [r9, #0]	@ zero_extendqisi2
	ldrb	r8, [r3, r5]	@ zero_extendqisi2
	rsb	r6, r2, sl
	rsb	r7, r2, r8
	add	r9, r7, r6
	cmp	r9, #0
	rsblt	r9, r9, #0
	cmp	r6, #0
	rsblt	r6, r6, #0
	cmp	r7, #0
	rsblt	r7, r7, #0
	cmp	r6, r7
	cmple	r6, r9
	bgt	.L1270
.L1125:
	ldrb	r6, [lr, #0]	@ zero_extendqisi2
	cmp	r1, r5
	rsb	r2, r8, r6
	strb	r2, [ip, #0]
	mov	r6, #1
	bls	.L525
	ldr	r8, [sp, #4]
	add	r5, r5, r6
	str	r4, [sp, #4]
	b	.L529
.L1271:
	ldrb	r4, [r0, r2]	@ zero_extendqisi2
	ldrb	sl, [r8, r6]	@ zero_extendqisi2
	ldrb	r7, [r3, r2]	@ zero_extendqisi2
	rsb	r5, r4, sl
	rsb	fp, r4, r7
	add	r9, fp, r5
	cmp	r9, #0
	rsblt	r9, r9, #0
	cmp	fp, #0
	rsblt	fp, fp, #0
	cmp	r5, #0
	rsblt	r5, r5, #0
	cmp	r5, fp
	cmple	r5, r9
	ble	.L1126
	cmp	fp, r9
	movle	r7, sl
	movgt	r7, r4
.L1126:
	ldrb	fp, [lr, r6]	@ zero_extendqisi2
	rsb	r4, r7, fp
	strb	r4, [ip, r6]
	add	r5, r2, #1
	add	r6, r6, #1
.L529:
	ldrb	r2, [r0, r5]	@ zero_extendqisi2
	ldrb	r7, [r8, r6]	@ zero_extendqisi2
	ldrb	fp, [r3, r5]	@ zero_extendqisi2
	rsb	r4, r2, r7
	rsb	r9, r2, fp
	add	sl, r9, r4
	cmp	sl, #0
	rsblt	sl, sl, #0
	cmp	r9, #0
	rsblt	r9, r9, #0
	cmp	r4, #0
	rsblt	r4, r4, #0
	cmp	r4, r9
	cmple	r4, sl
	ble	.L527
	cmp	r9, sl
	movle	fp, r7
	movgt	fp, r2
.L527:
	ldrb	r4, [lr, r6]	@ zero_extendqisi2
	cmp	r1, r5
	rsb	fp, fp, r4
	strb	fp, [ip, r6]
	add	r2, r5, #1
	add	r6, r6, #1
	bhi	.L1271
	ldr	r4, [sp, #4]
.L525:
	ldr	r6, [r4, #280]
	str	r6, [sp, #8]
	b	.L493
.L1261:
	ldr	r7, [r4, #276]
	cmp	r2, #0
	add	r6, r3, #1
	add	r5, r7, #1
	add	lr, r0, #1
	beq	.L489
	ldrb	r8, [r3, #1]	@ zero_extendqisi2
	ldrb	ip, [r0, #1]	@ zero_extendqisi2
	cmp	r2, #1
	sub	ip, r8, ip, lsr #1
	sub	r8, r2, #1
	strb	ip, [r7, #1]
	and	r8, r8, #3
	mov	ip, #2
	bls	.L1105
	cmp	r8, #0
	beq	.L490
	cmp	r8, #1
	beq	.L1018
	cmp	r8, #2
	ldrneb	ip, [r0, #2]	@ zero_extendqisi2
	ldrneb	r8, [r3, #2]	@ zero_extendqisi2
	subne	ip, r8, ip, lsr #1
	strneb	ip, [r7, #2]
	movne	ip, #3
	ldrb	r8, [r0, ip]	@ zero_extendqisi2
	ldrb	sl, [r3, ip]	@ zero_extendqisi2
	sub	r8, sl, r8, lsr #1
	strb	r8, [r7, ip]
	add	ip, ip, #1
.L1018:
	ldrb	r8, [r0, ip]	@ zero_extendqisi2
	ldrb	sl, [r3, ip]	@ zero_extendqisi2
	sub	r8, sl, r8, lsr #1
	strb	r8, [r7, ip]
	add	ip, ip, #1
	sub	r8, ip, #1
	cmp	r2, r8
	bls	.L1105
.L490:
	ldrb	fp, [r0, ip]	@ zero_extendqisi2
	ldrb	sl, [r3, ip]	@ zero_extendqisi2
	sub	r8, sl, fp, lsr #1
	strb	r8, [r7, ip]
	add	r8, ip, #1
	ldrb	sl, [r0, r8]	@ zero_extendqisi2
	ldrb	r9, [r3, r8]	@ zero_extendqisi2
	sub	fp, r9, sl, lsr #1
	strb	fp, [r7, r8]
	add	fp, r8, #1
	ldrb	r9, [r3, fp]	@ zero_extendqisi2
	ldrb	sl, [r0, fp]	@ zero_extendqisi2
	sub	sl, r9, sl, lsr #1
	strb	sl, [r7, fp]
	add	r8, ip, #3
	ldrb	r9, [r0, r8]	@ zero_extendqisi2
	ldrb	fp, [r3, r8]	@ zero_extendqisi2
	add	ip, ip, #4
	sub	sl, ip, #1
	sub	r9, fp, r9, lsr #1
	cmp	r2, sl
	strb	r9, [r7, r8]
	bhi	.L490
.L1105:
	add	r5, r5, r2
	add	r6, r6, r2
	add	lr, lr, r2
.L489:
	cmp	r1, r2
	bls	.L491
	ldrb	ip, [r3, #1]!	@ zero_extendqisi2
	ldrb	r7, [lr, #0]	@ zero_extendqisi2
	ldrb	r8, [r6, #0]	@ zero_extendqisi2
	add	r0, ip, r7
	mvn	ip, r2
	add	r7, r2, #1
	sub	r0, r8, r0, lsr #1
	add	ip, ip, r1
	cmp	r1, r7
	strb	r0, [r5, #0]
	and	ip, ip, #3
	mov	r0, #1
	bls	.L491
	cmp	ip, #0
	beq	.L492
	cmp	ip, #1
	beq	.L1016
	cmp	ip, #2
	beq	.L1017
	ldrb	r8, [lr, #1]	@ zero_extendqisi2
	ldrb	r7, [r3, #1]!	@ zero_extendqisi2
	ldrb	ip, [r6, #1]	@ zero_extendqisi2
	add	r0, r7, r8
	sub	r8, ip, r0, lsr #1
	strb	r8, [r5, #1]
	mov	r0, #2
.L1017:
	ldrb	ip, [lr, r0]	@ zero_extendqisi2
	ldrb	r8, [r3, #1]!	@ zero_extendqisi2
	ldrb	r7, [r6, r0]	@ zero_extendqisi2
	add	ip, r8, ip
	sub	ip, r7, ip, lsr #1
	strb	ip, [r5, r0]
	add	r0, r0, #1
.L1016:
	ldrb	ip, [lr, r0]	@ zero_extendqisi2
	ldrb	r8, [r3, #1]!	@ zero_extendqisi2
	ldrb	r7, [r6, r0]	@ zero_extendqisi2
	add	ip, r8, ip
	sub	ip, r7, ip, lsr #1
	strb	ip, [r5, r0]
	add	r0, r0, #1
	add	ip, r0, r2
	cmp	r1, ip
	bls	.L491
.L492:
	mov	r7, r3
	ldrb	sl, [r7, #1]!	@ zero_extendqisi2
	ldrb	r9, [lr, r0]	@ zero_extendqisi2
	ldrb	r8, [r6, r0]	@ zero_extendqisi2
	add	ip, sl, r9
	sub	r9, r8, ip, lsr #1
	strb	r9, [r5, r0]
	add	ip, r0, #1
	ldrb	r9, [r7, #1]!	@ zero_extendqisi2
	ldrb	r8, [lr, ip]	@ zero_extendqisi2
	ldrb	sl, [r6, ip]	@ zero_extendqisi2
	add	r8, r9, r8
	sub	r8, sl, r8, lsr #1
	strb	r8, [r5, ip]
	add	ip, ip, #1
	ldrb	sl, [r7, #1]	@ zero_extendqisi2
	ldrb	r7, [lr, ip]	@ zero_extendqisi2
	ldrb	r8, [r6, ip]	@ zero_extendqisi2
	add	r7, sl, r7
	sub	sl, r8, r7, lsr #1
	strb	sl, [r5, ip]
	add	ip, r0, #3
	ldrb	r8, [r3, #4]	@ zero_extendqisi2
	ldrb	r7, [lr, ip]	@ zero_extendqisi2
	ldrb	sl, [r6, ip]	@ zero_extendqisi2
	add	r0, r0, #4
	add	r7, r8, r7
	add	r8, r0, r2
	sub	r7, sl, r7, lsr #1
	cmp	r1, r8
	strb	r7, [r5, ip]
	add	r3, r3, #4
	bhi	.L492
.L491:
	ldr	r3, [r4, #276]
	str	r3, [sp, #8]
	b	.L493
.L1256:
	str	ip, [sp, #20]
	ldr	fp, [r4, #268]
	str	fp, [sp, #8]
	b	.L462
.L1259:
	rsb	r7, r7, #256
	b	.L1138
.L1262:
	rsb	lr, lr, #256
	b	.L1134
.L1254:
	rsb	lr, lr, #256
	b	.L1152
.L1251:
	mov	r6, r5, lsr #10
	bic	sl, r6, #-1073741761
	ldr	r6, [sp, #16]
	mov	ip, r5, asl #16
	cmp	r6, #0
	mov	r5, ip, lsr #16
	bic	lr, sl, #1069547520
	beq	.L420
	mov	r8, r6
	ldr	r6, [r4, #536]
	ldrb	sl, [r6, #0]	@ zero_extendqisi2
	sub	r7, r8, #1
	cmp	sl, #0
	and	r7, r7, #3
	bne	.L963
	ldr	sl, [r4, #540]
	ldrh	ip, [sl, #0]
	mul	lr, ip, lr
	mul	r5, ip, r5
	mov	lr, lr, lsr #8
	mov	r5, r5, lsr #8
.L963:
	ldr	sl, [sp, #16]
	mov	ip, #1
	cmp	sl, ip
	ble	.L420
	cmp	r7, #0
	beq	.L1247
	cmp	r7, ip
	beq	.L1082
	cmp	r7, #2
	beq	.L1083
	ldrb	r7, [r6, ip]	@ zero_extendqisi2
	cmp	r7, #0
	bne	.L965
	ldr	sl, [r4, #540]
	ldrh	fp, [sl, #2]
	mul	lr, fp, lr
	mul	r5, fp, r5
	mov	lr, lr, lsr #8
	mov	r5, r5, lsr #8
.L965:
	add	ip, ip, #1
.L1083:
	ldrb	r7, [r6, ip]	@ zero_extendqisi2
	cmp	r7, #0
	bne	.L968
	ldr	r7, [r4, #540]
	add	sl, r7, ip, asl #1
	ldrh	fp, [sl, #0]
	mul	lr, fp, lr
	mul	r5, fp, r5
	mov	lr, lr, lsr #8
	mov	r5, r5, lsr #8
.L968:
	add	ip, ip, #1
.L1082:
	ldrb	r7, [r6, ip]	@ zero_extendqisi2
	cmp	r7, #0
	bne	.L971
	ldr	sl, [r4, #540]
	add	fp, sl, ip, asl #1
	ldrh	r7, [fp, #0]
	mul	lr, r7, lr
	mul	r5, r7, r5
	mov	lr, lr, lsr #8
	mov	r5, r5, lsr #8
.L971:
	ldr	fp, [sp, #16]
	add	ip, ip, #1
	cmp	fp, ip
	ble	.L420
.L422:
	ldrb	r8, [r6, ip]	@ zero_extendqisi2
	cmp	r8, #0
	add	r7, ip, #1
	bne	.L421
	ldr	sl, [r4, #540]
	mov	ip, ip, asl #1
	ldrh	r9, [ip, sl]
	mul	lr, r9, lr
	mul	r5, r9, r5
	mov	lr, lr, lsr #8
	mov	r5, r5, lsr #8
.L421:
	ldrb	r8, [r6, r7]	@ zero_extendqisi2
	cmp	r8, #0
	bne	.L974
	ldr	ip, [r4, #540]
	mov	r8, r7, asl #1
	ldrh	r9, [r8, ip]
	mul	lr, r9, lr
	mul	r5, r9, r5
	mov	lr, lr, lsr #8
	mov	r5, r5, lsr #8
.L974:
	add	sl, r7, #1
	ldrb	r9, [r6, sl]	@ zero_extendqisi2
	cmp	r9, #0
	add	r8, r7, #2
	add	ip, r7, #3
	bne	.L976
	ldr	r7, [r4, #540]
	mov	sl, sl, asl #1
	ldrh	r7, [sl, r7]
	mul	lr, r7, lr
	mul	r5, r7, r5
	mov	lr, lr, lsr #8
	mov	r5, r5, lsr #8
.L976:
	ldrb	r7, [r6, r8]	@ zero_extendqisi2
	cmp	r7, #0
	bne	.L978
	ldr	r7, [r4, #540]
	mov	r8, r8, asl #1
	ldrh	r7, [r8, r7]
	mul	lr, r7, lr
	mul	r5, r7, r5
	mov	lr, lr, lsr #8
	mov	r5, r5, lsr #8
.L978:
	cmp	fp, ip
	bgt	.L422
.L420:
	ldr	ip, [r4, #548]
	ldrh	r6, [ip, #0]
	mul	lr, r6, lr
	mov	sl, #4194304
	mov	lr, lr, lsr #3
	sub	ip, sl, #64
	cmp	lr, ip
	bhi	.L412
	mul	sl, r6, r5
	mov	ip, sl, lsr #3
	add	sl, ip, lr, asl #10
	str	sl, [sp, #20]
	b	.L419
.L1269:
	ldr	r8, [sp, #4]
	str	r4, [sp, #4]
	b	.L529
.L1270:
	cmp	r7, r9
	movle	r8, sl
	movgt	r8, r2
	b	.L1125
.L1266:
	ldrb	lr, [r4, #532]	@ zero_extendqisi2
	cmp	lr, #2
	beq	.L530
	ldr	r8, [sp, #20]
	str	r8, [sp, #12]
.L531:
	ldr	r5, [r4, #280]
	cmp	r2, #0
	add	ip, r5, #1
	add	lr, r0, #1
	str	ip, [sp, #24]
	add	r7, r3, #1
	str	lr, [sp, #4]
	moveq	ip, r2
	beq	.L537
	ldrb	r6, [r3, #1]	@ zero_extendqisi2
	ldrb	ip, [r0, #1]	@ zero_extendqisi2
	rsb	lr, ip, r6
	and	lr, lr, #255
	cmp	lr, #127
	rsb	ip, lr, #256
	sub	r6, r2, #1
	movle	ip, lr
	cmp	r2, #1
	strb	lr, [r5, #1]
	and	r6, r6, #3
	mov	lr, #2
	bls	.L1118
	cmp	r6, #0
	beq	.L1197
	cmp	r6, #1
	beq	.L1029
	cmp	r6, #2
	beq	.L1030
	ldrb	r8, [r0, #2]	@ zero_extendqisi2
	ldrb	r6, [r3, #2]	@ zero_extendqisi2
	rsb	sl, r8, r6
	and	lr, sl, #255
	cmp	lr, #127
	rsb	r8, lr, #256
	movle	r8, lr
	strb	lr, [r5, #2]
	add	ip, ip, r8
	mov	lr, #3
.L1030:
	ldrb	r8, [r3, lr]	@ zero_extendqisi2
	ldrb	sl, [r0, lr]	@ zero_extendqisi2
	rsb	r6, sl, r8
	and	sl, r6, #255
	cmp	sl, #127
	rsb	r8, sl, #256
	movle	r8, sl
	strb	sl, [r5, lr]
	add	ip, ip, r8
	add	lr, lr, #1
.L1029:
	ldrb	r8, [r3, lr]	@ zero_extendqisi2
	ldrb	r6, [r0, lr]	@ zero_extendqisi2
	rsb	sl, r6, r8
	and	r6, sl, #255
	strb	r6, [r5, lr]
	add	lr, lr, #1
	cmp	r6, #127
	rsb	r8, r6, #256
	sub	sl, lr, #1
	movgt	r6, r8
	cmp	r2, sl
	add	ip, ip, r6
	bls	.L1118
.L1197:
	str	r7, [sp, #28]
	str	r1, [sp, #32]
.L540:
	ldrb	r9, [r3, lr]	@ zero_extendqisi2
	ldrb	sl, [r0, lr]	@ zero_extendqisi2
	rsb	r7, sl, r9
	and	r7, r7, #255
	strb	r7, [r5, lr]
	add	fp, lr, #1
	ldrb	r8, [r3, fp]	@ zero_extendqisi2
	ldrb	r1, [r0, fp]	@ zero_extendqisi2
	rsb	r6, r1, r8
	and	r6, r6, #255
	strb	r6, [r5, fp]
	add	r8, fp, #1
	ldrb	sl, [r3, r8]	@ zero_extendqisi2
	ldrb	r9, [r0, r8]	@ zero_extendqisi2
	rsb	r1, r9, sl
	and	r1, r1, #255
	strb	r1, [r5, r8]
	add	r8, lr, #3
	ldrb	r9, [r3, r8]	@ zero_extendqisi2
	ldrb	sl, [r0, r8]	@ zero_extendqisi2
	rsb	fp, r7, #256
	cmp	r7, #127
	movgt	r7, fp
	rsb	sl, sl, r9
	rsb	fp, r6, #256
	cmp	r6, #127
	and	sl, sl, #255
	movgt	r6, fp
	rsb	r9, r1, #256
	add	fp, ip, r7
	cmp	r1, #127
	add	lr, lr, #4
	movgt	r1, r9
	add	r6, fp, r6
	cmp	sl, #127
	sub	r9, lr, #1
	rsb	fp, sl, #256
	add	r1, r6, r1
	movle	fp, sl
	cmp	r2, r9
	strb	sl, [r5, r8]
	add	ip, r1, fp
	bhi	.L540
	ldr	r7, [sp, #28]
	ldr	r1, [sp, #32]
.L1118:
	ldr	r5, [sp, #24]
	ldr	lr, [sp, #4]
	add	r5, r5, r2
	add	r6, lr, r2
	str	r5, [sp, #24]
	str	r6, [sp, #4]
	add	r7, r7, r2
.L537:
	cmp	r1, r2
	bls	.L541
	add	lr, r2, #1
	rsb	fp, lr, r1
	rsb	r2, r2, #0
	tst	fp, #1
	add	r3, r3, r2
	add	r0, r0, r2
	mov	r5, #0
	bne	.L1119
	ldr	r8, [sp, #24]
	str	r4, [sp, #24]
	b	.L547
.L1272:
	cmp	r1, lr
	add	r5, r5, #1
	bls	.L1195
	ldr	fp, [sp, #4]
	add	lr, lr, #1
	ldrb	r2, [r0, lr]	@ zero_extendqisi2
	ldrb	r9, [fp, r5]	@ zero_extendqisi2
	ldrb	sl, [r3, lr]	@ zero_extendqisi2
	rsb	r4, r2, r9
	rsb	r6, r2, sl
	add	fp, r6, r4
	cmp	fp, #0
	rsblt	fp, fp, #0
	cmp	r4, #0
	rsblt	r4, r4, #0
	cmp	r6, #0
	rsblt	r6, r6, #0
	cmp	r4, r6
	cmple	r4, fp
	ble	.L1131
	cmp	r6, fp
	movle	sl, r9
	movgt	sl, r2
.L1131:
	ldrb	r4, [r7, r5]	@ zero_extendqisi2
	rsb	sl, sl, r4
	and	sl, sl, #255
	cmp	sl, #127
	strb	sl, [r8, r5]
	ldr	r6, [sp, #12]
	rsbgt	sl, sl, #256
	add	ip, ip, sl
	cmp	r6, ip
	bcc	.L1195
	add	r5, r5, #1
	add	lr, lr, #1
.L547:
	ldr	r6, [sp, #4]
	ldrb	r2, [r0, lr]	@ zero_extendqisi2
	ldrb	sl, [r6, r5]	@ zero_extendqisi2
	ldrb	fp, [r3, lr]	@ zero_extendqisi2
	rsb	r4, r2, sl
	rsb	r6, r2, fp
	add	r9, r6, r4
	cmp	r9, #0
	rsblt	r9, r9, #0
	cmp	r4, #0
	rsblt	r4, r4, #0
	cmp	r6, #0
	rsblt	r6, r6, #0
	cmp	r4, r6
	cmple	r4, r9
	ble	.L543
	cmp	r6, r9
	movle	fp, sl
	movgt	fp, r2
.L543:
	ldrb	r4, [r7, r5]	@ zero_extendqisi2
	rsb	r2, fp, r4
	and	sl, r2, #255
	cmp	sl, #127
	strb	sl, [r8, r5]
	rsbgt	sl, sl, #256
	add	ip, ip, sl
	ldr	sl, [sp, #12]
	cmp	sl, ip
	bcs	.L1272
.L1195:
	ldr	r4, [sp, #24]
.L541:
	ldrb	r0, [r4, #532]	@ zero_extendqisi2
	cmp	r0, #2
	beq	.L1273
.L548:
	ldr	r0, [sp, #20]
	cmp	r0, ip
	ldrhi	r1, [r4, #280]
	strhi	r1, [sp, #8]
	b	.L493
.L1119:
	ldr	fp, [sp, #4]
	ldrb	r2, [r0, lr]	@ zero_extendqisi2
	ldrb	sl, [fp, #0]	@ zero_extendqisi2
	ldrb	r9, [r3, lr]	@ zero_extendqisi2
	rsb	r6, r2, sl
	rsb	r5, r2, r9
	add	r8, r5, r6
	cmp	r8, #0
	rsblt	r8, r8, #0
	cmp	r6, #0
	rsblt	r6, r6, #0
	cmp	r5, #0
	rsblt	r5, r5, #0
	cmp	r6, r5
	cmple	r6, r8
	ble	.L1128
	cmp	r5, r8
	movle	r9, sl
	movgt	r9, r2
.L1128:
	ldrb	r6, [r7, #0]	@ zero_extendqisi2
	ldr	r2, [sp, #24]
	rsb	r5, r9, r6
	and	r6, r5, #255
	cmp	r6, #127
	strb	r6, [r2, #0]
	ldr	r5, [sp, #12]
	rsbgt	r6, r6, #256
	add	ip, ip, r6
	cmp	r5, ip
	bcc	.L541
	cmp	r1, lr
	mov	r5, #1
	bls	.L541
	ldr	r8, [sp, #24]
	add	lr, lr, r5
	str	r4, [sp, #24]
	b	.L547
.L1253:
	ldr	r5, [sp, #20]
	ldr	r7, [sp, #16]
	mov	r6, r5, lsr #10
	bic	lr, r6, #-1073741761
	cmp	r7, #0
	mov	sl, r5, asl #16
	mov	r7, sl, lsr #16
	bic	r6, lr, #1069547520
	beq	.L444
	ldr	lr, [r4, #536]
	ldrb	r8, [lr, #0]	@ zero_extendqisi2
	cmp	r8, #1
	ldr	r8, [sp, #16]
	sub	ip, r8, #1
	and	r5, ip, #3
	beq	.L1274
.L946:
	mov	ip, #1
	cmp	r8, ip
	ble	.L444
	cmp	r5, #0
	beq	.L1245
	cmp	r5, ip
	beq	.L1079
	cmp	r5, #2
	beq	.L1080
	ldrb	r5, [lr, ip]	@ zero_extendqisi2
	cmp	r5, ip
	beq	.L1275
.L948:
	add	ip, ip, #1
.L1080:
	ldrb	r5, [lr, ip]	@ zero_extendqisi2
	cmp	r5, #1
	beq	.L1276
.L951:
	add	ip, ip, #1
.L1079:
	ldrb	r9, [lr, ip]	@ zero_extendqisi2
	cmp	r9, #1
	beq	.L1277
.L954:
	ldr	r9, [sp, #16]
	add	ip, ip, #1
	cmp	r9, ip
	movgt	fp, r9
	bgt	.L446
	b	.L444
.L445:
	ldrb	r9, [lr, r5]	@ zero_extendqisi2
	cmp	r9, #1
	beq	.L1278
.L957:
	add	sl, r5, #1
	ldrb	r9, [lr, sl]	@ zero_extendqisi2
	cmp	r9, #1
	add	r8, r5, #2
	add	ip, r5, #3
	beq	.L1279
.L959:
	ldrb	r5, [lr, r8]	@ zero_extendqisi2
	cmp	r5, #1
	beq	.L1280
.L961:
	cmp	fp, ip
	ble	.L444
.L446:
	ldrb	r9, [lr, ip]	@ zero_extendqisi2
	cmp	r9, #1
	add	r5, ip, #1
	bne	.L445
	ldr	r8, [r4, #544]
	mov	ip, ip, asl #1
	ldrh	sl, [ip, r8]
	mul	r6, sl, r6
	mul	r7, sl, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L445
.L1264:
	ldr	sl, [sp, #16]
	mov	r5, ip, lsr #10
	bic	r6, r5, #-1073741761
	mov	ip, ip, asl #16
	cmp	sl, #0
	mov	ip, ip, lsr #16
	bic	r5, r6, #1069547520
	beq	.L511
	ldr	r6, [r4, #536]
	ldrb	lr, [r6, #0]	@ zero_extendqisi2
	sub	r7, sl, #1
	cmp	lr, #0
	and	r7, r7, #3
	beq	.L1281
.L703:
	ldr	sl, [sp, #16]
	mov	lr, #1
	cmp	sl, lr
	ble	.L511
	cmp	r7, #0
	beq	.L1200
	cmp	r7, lr
	beq	.L1035
	cmp	r7, #2
	beq	.L1036
	ldrb	r7, [r6, lr]	@ zero_extendqisi2
	cmp	r7, #0
	beq	.L1282
.L705:
	add	lr, lr, #1
.L1036:
	ldrb	fp, [r6, lr]	@ zero_extendqisi2
	cmp	fp, #0
	beq	.L1283
.L708:
	add	lr, lr, #1
.L1035:
	ldrb	r7, [r6, lr]	@ zero_extendqisi2
	cmp	r7, #0
	beq	.L1284
.L711:
	ldr	fp, [sp, #16]
	add	lr, lr, #1
	cmp	fp, lr
	ble	.L511
.L513:
	ldrb	r8, [r6, lr]	@ zero_extendqisi2
	cmp	r8, #0
	add	r7, lr, #1
	bne	.L512
	ldr	r8, [r4, #540]
	mov	r9, lr, asl #1
	ldrh	sl, [r9, r8]
	mul	r5, sl, r5
	mul	ip, sl, ip
	mov	r5, r5, lsr #8
	mov	ip, ip, lsr #8
.L512:
	ldrb	lr, [r6, r7]	@ zero_extendqisi2
	cmp	lr, #0
	bne	.L714
	ldr	sl, [r4, #540]
	mov	r8, r7, asl #1
	ldrh	r9, [r8, sl]
	mul	r5, r9, r5
	mul	ip, r9, ip
	mov	r5, r5, lsr #8
	mov	ip, ip, lsr #8
.L714:
	add	sl, r7, #1
	ldrb	r9, [r6, sl]	@ zero_extendqisi2
	cmp	r9, #0
	add	r8, r7, #2
	add	lr, r7, #3
	bne	.L716
	ldr	r7, [r4, #540]
	mov	sl, sl, asl #1
	ldrh	r7, [sl, r7]
	mul	r5, r7, r5
	mul	ip, r7, ip
	mov	r5, r5, lsr #8
	mov	ip, ip, lsr #8
.L716:
	ldrb	r7, [r6, r8]	@ zero_extendqisi2
	cmp	r7, #0
	bne	.L718
	ldr	r7, [r4, #540]
	mov	r8, r8, asl #1
	ldrh	r7, [r8, r7]
	mul	r5, r7, r5
	mul	ip, r7, ip
	mov	r5, r5, lsr #8
	mov	ip, ip, lsr #8
.L718:
	cmp	fp, lr
	bgt	.L513
.L511:
	ldr	fp, [r4, #548]
	ldrh	r6, [fp, #6]
	mul	r5, r6, r5
	mov	lr, #4194304
	mov	fp, r5, lsr #3
	sub	lr, lr, #64
	cmp	fp, lr
	mulls	ip, r6, ip
	movls	ip, ip, lsr #3
	mvnhi	ip, #-2147483648
	addls	ip, ip, fp, asl #10
	b	.L510
.L1260:
	mov	sl, r7, lsr #10
	bic	r6, sl, #-1073741761
	ldr	sl, [sp, #16]
	mov	r5, r7, asl #16
	cmp	sl, #0
	mov	r7, r5, lsr #16
	bic	r6, r6, #1069547520
	beq	.L484
	ldr	lr, [r4, #536]
	ldrb	ip, [lr, #0]	@ zero_extendqisi2
	sub	r8, sl, #1
	cmp	ip, #2
	and	r5, r8, #3
	beq	.L1285
.L754:
	ldr	r8, [sp, #16]
	mov	ip, #1
	cmp	r8, ip
	ble	.L484
	cmp	r5, #0
	beq	.L1209
	cmp	r5, ip
	beq	.L1044
	cmp	r5, #2
	beq	.L1045
	ldrb	r5, [lr, ip]	@ zero_extendqisi2
	cmp	r5, #2
	beq	.L1286
.L756:
	add	ip, ip, #1
.L1045:
	ldrb	r5, [lr, ip]	@ zero_extendqisi2
	cmp	r5, #2
	beq	.L1287
.L759:
	add	ip, ip, #1
.L1044:
	ldrb	r5, [lr, ip]	@ zero_extendqisi2
	cmp	r5, #2
	beq	.L1288
.L762:
	ldr	r9, [sp, #16]
	add	ip, ip, #1
	cmp	r9, ip
	movgt	fp, r9
	bgt	.L486
	b	.L484
.L485:
	ldrb	r9, [lr, r5]	@ zero_extendqisi2
	cmp	r9, #2
	beq	.L1289
.L765:
	add	sl, r5, #1
	ldrb	r9, [lr, sl]	@ zero_extendqisi2
	cmp	r9, #2
	add	r8, r5, #2
	add	ip, r5, #3
	beq	.L1290
.L767:
	ldrb	r5, [lr, r8]	@ zero_extendqisi2
	cmp	r5, #2
	beq	.L1291
.L769:
	cmp	fp, ip
	ble	.L484
.L486:
	ldrb	r9, [lr, ip]	@ zero_extendqisi2
	cmp	r9, #2
	add	r5, ip, #1
	bne	.L485
	ldr	r8, [r4, #540]
	mov	ip, ip, asl #1
	ldrh	sl, [ip, r8]
	mul	r6, sl, r6
	mul	r7, sl, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L485
.L495:
	ldr	r7, [sp, #20]
	ldr	r8, [sp, #16]
	mov	ip, r7, lsr #10
	bic	r5, ip, #-1073741761
	mov	fp, r7, asl #16
	cmp	r8, #0
	mov	r7, fp, lsr #16
	bic	r6, r5, #1069547520
	beq	.L497
	ldr	lr, [r4, #536]
	ldrb	r5, [lr, #0]	@ zero_extendqisi2
	sub	sl, r8, #1
	cmp	r5, #3
	and	r5, sl, #3
	beq	.L1292
.L737:
	ldr	sl, [sp, #16]
	mov	ip, #1
	cmp	sl, ip
	ble	.L497
	cmp	r5, #0
	beq	.L1207
	cmp	r5, ip
	beq	.L1041
	cmp	r5, #2
	beq	.L1042
	ldrb	r5, [lr, ip]	@ zero_extendqisi2
	cmp	r5, #3
	beq	.L1293
.L739:
	add	ip, ip, #1
.L1042:
	ldrb	r5, [lr, ip]	@ zero_extendqisi2
	cmp	r5, #3
	beq	.L1294
.L742:
	add	ip, ip, #1
.L1041:
	ldrb	fp, [lr, ip]	@ zero_extendqisi2
	cmp	fp, #3
	beq	.L1295
.L745:
	ldr	fp, [sp, #16]
	add	ip, ip, #1
	cmp	fp, ip
	bgt	.L499
	b	.L497
.L498:
	ldrb	r8, [lr, r5]	@ zero_extendqisi2
	cmp	r8, #3
	beq	.L1296
.L748:
	add	sl, r5, #1
	ldrb	r9, [lr, sl]	@ zero_extendqisi2
	cmp	r9, #3
	add	r8, r5, #2
	add	ip, r5, #3
	beq	.L1297
.L750:
	ldrb	r5, [lr, r8]	@ zero_extendqisi2
	cmp	r5, #3
	beq	.L1298
.L752:
	cmp	fp, ip
	ble	.L497
.L499:
	ldrb	r8, [lr, ip]	@ zero_extendqisi2
	cmp	r8, #3
	add	r5, ip, #1
	bne	.L498
	ldr	sl, [r4, #544]
	mov	ip, ip, asl #1
	ldrh	r9, [ip, sl]
	mul	r6, r9, r6
	mul	r7, r9, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L498
.L497:
	ldr	ip, [r4, #552]
	ldrh	fp, [ip, #6]
	mul	r5, fp, r6
	mov	r6, #4194304
	sub	ip, r6, #64
	mov	lr, r5, lsr #3
	cmp	lr, ip
	mulls	ip, fp, r7
	movls	ip, ip, lsr #3
	mvnhi	r9, #-2147483648
	addls	ip, ip, lr, asl #10
	strhi	r9, [sp, #12]
	strls	ip, [sp, #12]
	b	.L496
.L444:
	ldr	ip, [r4, #552]
	ldrh	r5, [ip, #2]
	mul	sl, r5, r6
	mov	r6, #4194304
	mov	lr, sl, lsr #3
	sub	ip, r6, #64
	cmp	lr, ip
	mulls	r5, r7, r5
	movls	r8, r5, lsr #3
	mvnhi	r8, #-2147483648
	addls	r8, r8, lr, asl #10
	b	.L443
.L1255:
	mov	r9, ip, lsr #10
	bic	r7, r9, #-1073741761
	ldr	r9, [sp, #16]
	mov	r6, ip, asl #16
	cmp	r9, #0
	mov	ip, r6, lsr #16
	bic	r7, r7, #1069547520
	beq	.L458
	ldr	r5, [r4, #536]
	ldrb	sl, [r5, #0]	@ zero_extendqisi2
	sub	fp, r9, #1
	cmp	sl, #1
	and	r6, fp, #3
	beq	.L1299
.L902:
	ldr	fp, [sp, #16]
	mov	lr, #1
	cmp	fp, lr
	ble	.L458
	cmp	r6, #0
	beq	.L1236
	cmp	r6, lr
	beq	.L1071
	cmp	r6, #2
	beq	.L1072
	ldrb	r6, [r5, lr]	@ zero_extendqisi2
	cmp	r6, lr
	beq	.L1300
.L904:
	add	lr, lr, #1
.L1072:
	ldrb	sl, [r5, lr]	@ zero_extendqisi2
	cmp	sl, #1
	beq	.L1301
.L907:
	add	lr, lr, #1
.L1071:
	ldrb	sl, [r5, lr]	@ zero_extendqisi2
	cmp	sl, #1
	beq	.L1302
.L910:
	ldr	sl, [sp, #16]
	add	lr, lr, #1
	cmp	sl, lr
	movgt	fp, sl
	bgt	.L460
	b	.L458
.L459:
	ldrb	r8, [r5, r6]	@ zero_extendqisi2
	cmp	r8, #1
	beq	.L1303
.L913:
	add	sl, r6, #1
	ldrb	r9, [r5, sl]	@ zero_extendqisi2
	cmp	r9, #1
	add	r8, r6, #2
	add	lr, r6, #3
	beq	.L1304
.L915:
	ldrb	r6, [r5, r8]	@ zero_extendqisi2
	cmp	r6, #1
	beq	.L1305
.L917:
	cmp	fp, lr
	ble	.L458
.L460:
	ldrb	r8, [r5, lr]	@ zero_extendqisi2
	cmp	r8, #1
	add	r6, lr, #1
	bne	.L459
	ldr	sl, [r4, #544]
	mov	r9, lr, asl #1
	ldrh	lr, [r9, sl]
	mul	r7, lr, r7
	mul	ip, lr, ip
	mov	r7, r7, lsr #8
	mov	ip, ip, lsr #8
	b	.L459
.L458:
	ldr	lr, [r4, #552]
	ldrh	r6, [lr, #2]
	mul	r5, r6, r7
	mov	sl, #4194304
	mov	r5, r5, lsr #3
	sub	lr, sl, #64
	cmp	r5, lr
	mulls	ip, r6, ip
	movls	ip, ip, lsr #3
	mvnhi	ip, #-2147483648
	addls	ip, ip, r5, asl #10
	b	.L457
.L1258:
	ldr	r8, [sp, #20]
	mov	r9, r8, lsr #10
	bic	ip, r9, #-1073741761
	ldr	r9, [sp, #16]
	mov	r5, r8, asl #16
	cmp	r9, #0
	mov	r8, r5, lsr #16
	bic	r7, ip, #1069547520
	beq	.L473
	ldr	r5, [r4, #536]
	ldrb	r6, [r5, #0]	@ zero_extendqisi2
	sub	sl, r9, #1
	cmp	r6, #2
	and	r6, sl, #3
	beq	.L1306
.L795:
	ldr	sl, [sp, #16]
	mov	ip, #1
	cmp	sl, ip
	ble	.L473
	cmp	r6, #0
	beq	.L475
	cmp	r6, ip
	beq	.L1050
	cmp	r6, #2
	beq	.L1051
	ldrb	sl, [r5, ip]	@ zero_extendqisi2
	cmp	sl, #2
	beq	.L1307
.L797:
	add	ip, ip, #1
.L1051:
	ldrb	r6, [r5, ip]	@ zero_extendqisi2
	cmp	r6, #2
	beq	.L1308
.L800:
	add	ip, ip, #1
.L1050:
	ldrb	r6, [r5, ip]	@ zero_extendqisi2
	cmp	r6, #2
	beq	.L1309
.L803:
	ldr	fp, [sp, #16]
	add	ip, ip, #1
	cmp	fp, ip
	bgt	.L475
.L473:
	ldr	ip, [r4, #552]
	ldrh	r6, [ip, #4]
	mul	r5, r6, r7
	mov	ip, #4194304
	sub	ip, ip, #64
	mov	r5, r5, lsr #3
	cmp	r5, ip
	mulls	ip, r6, r8
	movls	ip, ip, lsr #3
	mvnhi	r5, #-2147483648
	addls	r5, ip, r5, asl #10
	b	.L472
.L1328:
	ldr	sl, [r4, #544]
	mov	r9, ip, asl #1
	ldrh	ip, [r9, sl]
	mul	r7, ip, r7
	mul	r8, ip, r8
	mov	r7, r7, lsr #8
	mov	r8, r8, lsr #8
.L474:
	ldrb	fp, [r5, r6]	@ zero_extendqisi2
	cmp	fp, #2
	beq	.L1310
.L806:
	add	r9, r6, #1
	ldrb	fp, [r5, r9]	@ zero_extendqisi2
	cmp	fp, #2
	add	sl, r6, #2
	add	ip, r6, #3
	beq	.L1311
.L808:
	ldrb	r6, [r5, sl]	@ zero_extendqisi2
	cmp	r6, #2
	beq	.L1312
.L810:
	ldr	r6, [sp, #16]
	cmp	r6, ip
	ble	.L473
.L475:
	ldrb	fp, [r5, ip]	@ zero_extendqisi2
	cmp	fp, #2
	add	r6, ip, #1
	bne	.L474
	b	.L1328
.L484:
	ldr	fp, [r4, #548]
	ldrh	r5, [fp, #4]
	mul	lr, r5, r6
	mov	ip, #4194304
	mov	fp, lr, lsr #3
	sub	ip, ip, #64
	cmp	fp, ip
	mulls	r7, r5, r7
	movls	r7, r7, lsr #3
	mvnhi	r7, #-2147483648
	addls	r7, r7, fp, asl #10
	b	.L483
.L1181:
	ldr	ip, [sp, #16]
	b	.L556
.L1279:
	ldr	r5, [r4, #544]
	mov	sl, sl, asl #1
	ldrh	r5, [sl, r5]
	mul	r6, r5, r6
	mul	r7, r5, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L959
.L1278:
	ldr	sl, [r4, #544]
	mov	r8, r5, asl #1
	ldrh	ip, [r8, sl]
	mul	r6, ip, r6
	mul	r7, ip, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L957
.L1304:
	ldr	r6, [r4, #544]
	mov	sl, sl, asl #1
	ldrh	r6, [sl, r6]
	mul	r7, r6, r7
	mul	ip, r6, ip
	mov	r7, r7, lsr #8
	mov	ip, ip, lsr #8
	b	.L915
.L1303:
	ldr	lr, [r4, #544]
	mov	r8, r6, asl #1
	ldrh	r9, [r8, lr]
	mul	r7, r9, r7
	mul	ip, r9, ip
	mov	r7, r7, lsr #8
	mov	ip, ip, lsr #8
	b	.L913
.L1311:
	ldr	r6, [r4, #544]
	mov	r9, r9, asl #1
	ldrh	r6, [r9, r6]
	mul	r7, r6, r7
	mul	r8, r6, r8
	mov	r7, r7, lsr #8
	mov	r8, r8, lsr #8
	b	.L808
.L1310:
	ldr	ip, [r4, #544]
	mov	sl, r6, asl #1
	ldrh	r9, [sl, ip]
	mul	r7, r9, r7
	mul	r8, r9, r8
	mov	r7, r7, lsr #8
	mov	r8, r8, lsr #8
	b	.L806
.L1298:
	ldr	r5, [r4, #544]
	mov	r8, r8, asl #1
	ldrh	r5, [r8, r5]
	mul	r6, r5, r6
	mul	r7, r5, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L752
.L1297:
	ldr	r5, [r4, #544]
	mov	sl, sl, asl #1
	ldrh	r5, [sl, r5]
	mul	r6, r5, r6
	mul	r7, r5, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L750
.L1296:
	ldr	r9, [r4, #544]
	mov	ip, r5, asl #1
	ldrh	sl, [ip, r9]
	mul	r6, sl, r6
	mul	r7, sl, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L748
.L1312:
	ldr	r6, [r4, #544]
	mov	sl, sl, asl #1
	ldrh	r6, [sl, r6]
	mul	r7, r6, r7
	mul	r8, r6, r8
	mov	r7, r7, lsr #8
	mov	r8, r8, lsr #8
	b	.L810
.L1291:
	ldr	r5, [r4, #540]
	mov	r8, r8, asl #1
	ldrh	r5, [r8, r5]
	mul	r6, r5, r6
	mul	r7, r5, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L769
.L1290:
	ldr	r5, [r4, #540]
	mov	sl, sl, asl #1
	ldrh	r5, [sl, r5]
	mul	r6, r5, r6
	mul	r7, r5, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L767
.L1289:
	ldr	ip, [r4, #540]
	mov	r8, r5, asl #1
	ldrh	sl, [r8, ip]
	mul	r6, sl, r6
	mul	r7, sl, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L765
.L1305:
	ldr	r6, [r4, #544]
	mov	r8, r8, asl #1
	ldrh	sl, [r8, r6]
	mul	r7, sl, r7
	mul	ip, sl, ip
	mov	r7, r7, lsr #8
	mov	ip, ip, lsr #8
	b	.L917
.L1280:
	ldr	r5, [r4, #544]
	mov	r8, r8, asl #1
	ldrh	sl, [r8, r5]
	mul	r6, sl, r6
	mul	r7, sl, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L961
.L530:
	ldr	r9, [sp, #20]
	mov	sl, r9, lsr #10
	bic	ip, sl, #-1073741761
	ldr	sl, [sp, #16]
	mov	r7, r9, asl #16
	cmp	sl, #0
	mov	r7, r7, lsr #16
	bic	r6, ip, #1069547520
	beq	.L532
	ldr	lr, [r4, #536]
	ldrb	r5, [lr, #0]	@ zero_extendqisi2
	sub	r8, sl, #1
	cmp	r5, #4
	and	r5, r8, #3
	beq	.L1314
.L686:
	ldr	r8, [sp, #16]
	mov	ip, #1
	cmp	r8, ip
	ble	.L532
	cmp	r5, #0
	beq	.L1198
	cmp	r5, ip
	beq	.L1032
	cmp	r5, #2
	beq	.L1033
	ldrb	r9, [lr, ip]	@ zero_extendqisi2
	cmp	r9, #4
	beq	.L1315
.L688:
	add	ip, ip, #1
.L1033:
	ldrb	r5, [lr, ip]	@ zero_extendqisi2
	cmp	r5, #4
	beq	.L1316
.L691:
	add	ip, ip, #1
.L1032:
	ldrb	r5, [lr, ip]	@ zero_extendqisi2
	cmp	r5, #4
	beq	.L1317
.L694:
	ldr	r9, [sp, #16]
	add	ip, ip, #1
	cmp	r9, ip
	movgt	fp, r9
	bgt	.L534
	b	.L532
.L533:
	ldrb	r9, [lr, r5]	@ zero_extendqisi2
	cmp	r9, #4
	beq	.L1318
.L697:
	add	sl, r5, #1
	ldrb	r9, [lr, sl]	@ zero_extendqisi2
	cmp	r9, #4
	add	r8, r5, #2
	add	ip, r5, #3
	beq	.L1319
.L699:
	ldrb	r5, [lr, r8]	@ zero_extendqisi2
	cmp	r5, #4
	beq	.L1320
.L701:
	cmp	fp, ip
	ble	.L532
.L534:
	ldrb	r9, [lr, ip]	@ zero_extendqisi2
	cmp	r9, #4
	add	r5, ip, #1
	bne	.L533
	ldr	r8, [r4, #544]
	mov	sl, ip, asl #1
	ldrh	ip, [sl, r8]
	mul	r6, ip, r6
	mul	r7, ip, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L533
.L532:
	ldr	ip, [r4, #552]
	ldrh	r5, [ip, #8]
	mul	lr, r5, r6
	mov	ip, #4194304
	sub	ip, ip, #64
	mov	lr, lr, lsr #3
	cmp	lr, ip
	mulls	ip, r5, r7
	movls	ip, ip, lsr #3
	mvnhi	fp, #-2147483648
	addls	ip, ip, lr, asl #10
	strhi	fp, [sp, #12]
	strls	ip, [sp, #12]
	b	.L531
.L1273:
	ldr	lr, [sp, #16]
	mov	r1, ip, lsr #10
	bic	r3, r1, #-1073741761
	mov	ip, ip, asl #16
	cmp	lr, #0
	mov	ip, ip, lsr #16
	bic	r0, r3, #1069547520
	beq	.L549
	ldr	r2, [r4, #536]
	ldrb	r3, [r2, #0]	@ zero_extendqisi2
	sub	r9, lr, #1
	cmp	r3, #4
	and	r1, r9, #3
	beq	.L1321
.L653:
	ldr	r9, [sp, #16]
	mov	r3, #1
	cmp	r9, r3
	ble	.L549
	cmp	r1, #0
	beq	.L1193
	cmp	r1, r3
	beq	.L1027
	cmp	r1, #2
	beq	.L1028
	ldrb	r1, [r2, r3]	@ zero_extendqisi2
	cmp	r1, #4
	beq	.L1322
.L655:
	add	r3, r3, #1
.L1028:
	ldrb	r1, [r2, r3]	@ zero_extendqisi2
	cmp	r1, #4
	beq	.L1323
.L658:
	add	r3, r3, #1
.L1027:
	ldrb	sl, [r2, r3]	@ zero_extendqisi2
	cmp	sl, #4
	beq	.L1324
.L661:
	ldr	sl, [sp, #16]
	add	r3, r3, #1
	cmp	sl, r3
	movgt	r7, sl
	bgt	.L551
	b	.L549
.L550:
	ldrb	r3, [r2, r1]	@ zero_extendqisi2
	cmp	r3, #4
	beq	.L1325
.L664:
	add	r5, r1, #1
	ldrb	r6, [r2, r5]	@ zero_extendqisi2
	cmp	r6, #4
	add	lr, r1, #2
	add	r3, r1, #3
	beq	.L1326
.L666:
	ldrb	r1, [r2, lr]	@ zero_extendqisi2
	cmp	r1, #4
	beq	.L1327
.L668:
	cmp	r7, r3
	ble	.L549
.L551:
	ldrb	lr, [r2, r3]	@ zero_extendqisi2
	cmp	lr, #4
	add	r1, r3, #1
	bne	.L550
	ldr	lr, [r4, #540]
	mov	r6, r3, asl #1
	ldrh	r5, [r6, lr]
	mul	r0, r5, r0
	mul	ip, r5, ip
	mov	r0, r0, lsr #8
	mov	ip, ip, lsr #8
	b	.L550
.L549:
	ldr	r2, [r4, #548]
	ldrh	r1, [r2, #8]
	mul	r3, r1, r0
	mov	lr, #4194304
	mov	r2, r3, lsr #3
	sub	r0, lr, #64
	cmp	r2, r0
	mulls	ip, r1, ip
	movls	ip, ip, lsr #3
	mvnhi	ip, #-2147483648
	addls	ip, ip, r2, asl #10
	b	.L548
.L518:
	ldrb	r7, [r3, #1]	@ zero_extendqisi2
	ldrb	r6, [r0, #1]	@ zero_extendqisi2
	cmp	r2, #1
	rsb	r6, r6, r7
	sub	r7, r2, #1
	strb	r6, [r5, #1]
	and	r7, r7, #3
	mov	r6, #2
	bls	.L522
	cmp	r7, #0
	beq	.L524
	cmp	r7, #1
	beq	.L1020
	cmp	r7, #2
	ldrneb	r6, [r0, #2]	@ zero_extendqisi2
	ldrneb	r7, [r3, #2]	@ zero_extendqisi2
	rsbne	r6, r6, r7
	strneb	r6, [r5, #2]
	movne	r6, #3
	ldrb	r8, [r3, r6]	@ zero_extendqisi2
	ldrb	r7, [r0, r6]	@ zero_extendqisi2
	rsb	r7, r7, r8
	strb	r7, [r5, r6]
	add	r6, r6, #1
.L1020:
	ldrb	r8, [r3, r6]	@ zero_extendqisi2
	ldrb	r7, [r0, r6]	@ zero_extendqisi2
	rsb	r7, r7, r8
	strb	r7, [r5, r6]
	add	r6, r6, #1
	sub	r7, r6, #1
	cmp	r2, r7
	bls	.L522
.L524:
	ldrb	r8, [r3, r6]	@ zero_extendqisi2
	ldrb	r9, [r0, r6]	@ zero_extendqisi2
	rsb	r7, r9, r8
	strb	r7, [r5, r6]
	add	r7, r6, #1
	ldrb	sl, [r3, r7]	@ zero_extendqisi2
	ldrb	r8, [r0, r7]	@ zero_extendqisi2
	rsb	r9, r8, sl
	strb	r9, [r5, r7]
	add	r9, r7, #1
	ldrb	sl, [r3, r9]	@ zero_extendqisi2
	ldrb	r8, [r0, r9]	@ zero_extendqisi2
	rsb	r8, r8, sl
	strb	r8, [r5, r9]
	add	r7, r6, #3
	ldrb	r9, [r3, r7]	@ zero_extendqisi2
	ldrb	sl, [r0, r7]	@ zero_extendqisi2
	add	r6, r6, #4
	sub	r8, r6, #1
	rsb	sl, sl, r9
	cmp	r2, r8
	strb	sl, [r5, r7]
	bhi	.L524
	b	.L522
.L464:
	ldrb	r6, [r3, #1]	@ zero_extendqisi2
	ldrb	r5, [r0, #1]	@ zero_extendqisi2
	ldr	lr, [sp, #8]
	rsb	ip, r5, r6
	cmp	r1, #1
	sub	r6, r1, #1
	strb	ip, [lr, #1]
	and	lr, r6, #3
	mov	ip, #2
	bls	.L467
	cmp	lr, #0
	beq	.L1231
	cmp	lr, #1
	beq	.L1064
	cmp	lr, #2
	beq	.L1065
	ldrb	lr, [r3, #2]	@ zero_extendqisi2
	ldrb	r5, [r0, #2]	@ zero_extendqisi2
	ldr	r8, [sp, #8]
	rsb	ip, r5, lr
	strb	ip, [r8, #2]
	mov	ip, #3
.L1065:
	ldrb	r9, [r0, ip]	@ zero_extendqisi2
	ldrb	sl, [r3, ip]	@ zero_extendqisi2
	rsb	r6, r9, sl
	ldr	r9, [sp, #8]
	strb	r6, [r9, ip]
	add	ip, ip, #1
.L1064:
	ldrb	sl, [r3, ip]	@ zero_extendqisi2
	ldrb	r6, [r0, ip]	@ zero_extendqisi2
	rsb	lr, r6, sl
	ldr	sl, [sp, #8]
	strb	lr, [sl, ip]
	add	ip, ip, #1
	sub	r5, ip, #1
	cmp	r1, r5
	bls	.L467
.L1231:
	ldr	r5, [sp, #8]
.L469:
	ldrb	r6, [r3, ip]	@ zero_extendqisi2
	ldrb	lr, [r0, ip]	@ zero_extendqisi2
	rsb	r8, lr, r6
	strb	r8, [r5, ip]
	add	r8, ip, #1
	ldrb	r7, [r3, r8]	@ zero_extendqisi2
	ldrb	r6, [r0, r8]	@ zero_extendqisi2
	rsb	lr, r6, r7
	strb	lr, [r5, r8]
	add	lr, r8, #1
	ldrb	r7, [r3, lr]	@ zero_extendqisi2
	ldrb	r6, [r0, lr]	@ zero_extendqisi2
	rsb	r8, r6, r7
	strb	r8, [r5, lr]
	add	lr, ip, #3
	ldrb	r8, [r3, lr]	@ zero_extendqisi2
	ldrb	r6, [r0, lr]	@ zero_extendqisi2
	add	ip, ip, #4
	sub	r7, ip, #1
	rsb	r6, r6, r8
	cmp	r1, r7
	strb	r6, [r5, lr]
	bhi	.L469
	b	.L467
.L434:
	mov	fp, r3
	ldrb	r7, [lr, r5]	@ zero_extendqisi2
	ldrb	sl, [fp, #1]!	@ zero_extendqisi2
	mov	r8, #1
	add	r9, r8, r2
	rsb	r7, sl, r7
	cmp	r1, r9
	strb	r7, [ip, r5]
	and	r6, r6, #3
	bls	.L433
	cmp	r6, #0
	beq	.L439
	cmp	r6, #1
	beq	.L1052
	cmp	r6, #2
	ldrneb	r6, [lr, r8]	@ zero_extendqisi2
	ldrneb	r5, [fp, #1]!	@ zero_extendqisi2
	rsbne	r5, r5, r6
	strneb	r5, [ip, r8]
	movne	r8, #2
	ldrb	r6, [lr, r8]	@ zero_extendqisi2
	ldrb	r5, [fp, #1]!	@ zero_extendqisi2
	rsb	r7, r5, r6
	strb	r7, [ip, r8]
	add	r8, r8, #1
.L1052:
	ldrb	r6, [lr, r8]	@ zero_extendqisi2
	ldrb	r5, [fp, #1]!	@ zero_extendqisi2
	rsb	r7, r5, r6
	strb	r7, [ip, r8]
	add	r8, r8, #1
	add	r5, r8, r2
	cmp	r1, r5
	bls	.L433
.L439:
	mov	r5, fp
	ldrb	sl, [r5, #1]!	@ zero_extendqisi2
	ldrb	r7, [lr, r8]	@ zero_extendqisi2
	rsb	r6, sl, r7
	strb	r6, [ip, r8]
	add	r6, r8, #1
	ldrb	r7, [r5, #1]!	@ zero_extendqisi2
	ldrb	sl, [lr, r6]	@ zero_extendqisi2
	rsb	r7, r7, sl
	strb	r7, [ip, r6]
	add	r6, r6, #1
	ldrb	r7, [lr, r6]	@ zero_extendqisi2
	ldrb	sl, [r5, #1]	@ zero_extendqisi2
	rsb	r5, sl, r7
	strb	r5, [ip, r6]
	add	r5, r8, #3
	ldrb	r7, [fp, #4]	@ zero_extendqisi2
	ldrb	sl, [lr, r5]	@ zero_extendqisi2
	add	r8, r8, #4
	add	r6, r8, r2
	rsb	r7, r7, sl
	cmp	r1, r6
	strb	r7, [ip, r5]
	add	fp, fp, #4
	bhi	.L439
	b	.L433
.L426:
	ldrb	r6, [r3, #1]	@ zero_extendqisi2
	sub	r7, r2, #1
	cmp	r2, #1
	strb	r6, [lr, #1]
	mov	r5, #2
	and	r6, r7, #3
	bls	.L430
	cmp	r6, #0
	beq	.L432
	cmp	r6, #1
	beq	.L1058
	cmp	r6, #2
	ldrneb	r5, [r3, #2]	@ zero_extendqisi2
	strneb	r5, [lr, #2]
	movne	r5, #3
	ldrb	r6, [r3, r5]	@ zero_extendqisi2
	strb	r6, [lr, r5]
	add	r5, r5, #1
.L1058:
	ldrb	r8, [r3, r5]	@ zero_extendqisi2
	strb	r8, [lr, r5]
	add	r5, r5, #1
	sub	r6, r5, #1
	cmp	r2, r6
	bls	.L430
.L432:
	ldrb	r6, [r3, r5]	@ zero_extendqisi2
	strb	r6, [lr, r5]
	add	sl, r5, #1
	ldrb	r8, [r3, sl]	@ zero_extendqisi2
	strb	r8, [lr, sl]
	add	r6, sl, #1
	ldrb	r7, [r3, r6]	@ zero_extendqisi2
	strb	r7, [lr, r6]
	add	r6, r5, #3
	add	r5, r5, #4
	ldrb	sl, [r3, r6]	@ zero_extendqisi2
	sub	r8, r5, #1
	cmp	r2, r8
	strb	sl, [lr, r6]
	bhi	.L432
	b	.L430
.L1281:
	ldr	lr, [r4, #540]
	ldrh	fp, [lr, #0]
	mul	r5, fp, r5
	mul	ip, fp, ip
	mov	r5, r5, lsr #8
	mov	ip, ip, lsr #8
	b	.L703
.L1284:
	ldr	r7, [r4, #540]
	add	fp, r7, lr, asl #1
	ldrh	r7, [fp, #0]
	mul	r5, r7, r5
	mul	ip, r7, ip
	mov	r5, r5, lsr #8
	mov	ip, ip, lsr #8
	b	.L711
.L1283:
	ldr	r7, [r4, #540]
	add	fp, r7, lr, asl #1
	ldrh	r7, [fp, #0]
	mul	r5, r7, r5
	mul	ip, r7, ip
	mov	r5, r5, lsr #8
	mov	ip, ip, lsr #8
	b	.L708
.L1282:
	ldr	r7, [r4, #540]
	ldrh	fp, [r7, #2]
	mul	r5, fp, r5
	mul	ip, fp, ip
	mov	r5, r5, lsr #8
	mov	ip, ip, lsr #8
	b	.L705
.L1287:
	ldr	r9, [r4, #540]
	add	r5, r9, ip, asl #1
	ldrh	r9, [r5, #0]
	mul	r6, r9, r6
	mul	r7, r9, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L759
.L1286:
	ldr	r8, [r4, #540]
	ldrh	r9, [r5, r8]
	mul	r6, r9, r6
	mul	r7, r9, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L756
.L1294:
	ldr	r5, [r4, #544]
	add	r8, r5, ip, asl #1
	ldrh	fp, [r8, #0]
	mul	r6, fp, r6
	mul	r7, fp, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L742
.L1293:
	ldr	r8, [r4, #544]
	ldrh	fp, [r8, #2]
	mul	r6, fp, r6
	mul	r7, fp, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L739
.L1276:
	ldr	r5, [r4, #544]
	add	sl, r5, ip, asl #1
	ldrh	r9, [sl, #0]
	mul	r6, r9, r6
	mul	r7, r9, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L951
.L1275:
	ldr	sl, [r4, #544]
	ldrh	r9, [sl, #2]
	mul	r6, r9, r6
	mul	r7, r9, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L948
.L1308:
	ldr	fp, [r4, #544]
	add	r6, fp, ip, asl #1
	ldrh	fp, [r6, #0]
	mul	r7, fp, r7
	mul	r8, fp, r8
	mov	r7, r7, lsr #8
	mov	r8, r8, lsr #8
	b	.L800
.L1307:
	ldr	fp, [r4, #544]
	ldrh	r6, [sl, fp]
	mul	r7, r6, r7
	mul	r8, r6, r8
	mov	r7, r7, lsr #8
	mov	r8, r8, lsr #8
	b	.L797
.L1301:
	ldr	r6, [r4, #544]
	add	sl, r6, lr, asl #1
	ldrh	r6, [sl, #0]
	mul	r7, r6, r7
	mul	ip, r6, ip
	mov	r7, r7, lsr #8
	mov	ip, ip, lsr #8
	b	.L907
.L1300:
	ldr	r6, [r4, #544]
	ldrh	sl, [r6, #2]
	mul	r7, sl, r7
	mul	ip, sl, ip
	mov	r7, r7, lsr #8
	mov	ip, ip, lsr #8
	b	.L904
.L1314:
	ldr	r8, [r4, #544]
	ldrh	ip, [r8, #0]
	mul	r6, ip, r6
	mul	r7, ip, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L686
.L1321:
	ldr	lr, [r4, #540]
	ldrh	r3, [lr, #0]
	mul	r0, r3, r0
	mul	ip, r3, ip
	mov	r0, r0, lsr #8
	mov	ip, ip, lsr #8
	b	.L653
.L1322:
	ldr	lr, [r4, #540]
	ldrh	sl, [lr, #2]
	mul	r0, sl, r0
	mul	ip, sl, ip
	mov	r0, r0, lsr #8
	mov	ip, ip, lsr #8
	b	.L655
.L1316:
	ldr	r9, [r4, #544]
	add	r5, r9, ip, asl #1
	ldrh	r9, [r5, #0]
	mul	r6, r9, r6
	mul	r7, r9, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L691
.L1323:
	ldr	r1, [r4, #540]
	add	lr, r1, r3, asl #1
	ldrh	sl, [lr, #0]
	mul	r0, sl, r0
	mul	ip, sl, ip
	mov	r0, r0, lsr #8
	mov	ip, ip, lsr #8
	b	.L658
.L1315:
	ldr	r9, [r4, #544]
	ldrh	r5, [r9, #2]
	mul	r6, r5, r6
	mul	r7, r5, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L688
.L1320:
	ldr	r5, [r4, #544]
	mov	r8, r8, asl #1
	ldrh	r5, [r8, r5]
	mul	r6, r5, r6
	mul	r7, r5, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L701
.L1319:
	ldr	r5, [r4, #544]
	mov	sl, sl, asl #1
	ldrh	r5, [sl, r5]
	mul	r6, r5, r6
	mul	r7, r5, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L699
.L1318:
	ldr	ip, [r4, #544]
	mov	r8, r5, asl #1
	ldrh	sl, [r8, ip]
	mul	r6, sl, r6
	mul	r7, sl, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L697
.L1326:
	ldr	r1, [r4, #540]
	mov	r5, r5, asl #1
	ldrh	r1, [r5, r1]
	mul	r0, r1, r0
	mul	ip, r1, ip
	mov	r0, r0, lsr #8
	mov	ip, ip, lsr #8
	b	.L666
.L1325:
	ldr	r6, [r4, #540]
	mov	r3, r1, asl #1
	ldrh	r5, [r3, r6]
	mul	r0, r5, r0
	mul	ip, r5, ip
	mov	r0, r0, lsr #8
	mov	ip, ip, lsr #8
	b	.L664
.L1327:
	ldr	r1, [r4, #540]
	mov	lr, lr, asl #1
	ldrh	r1, [lr, r1]
	mul	r0, r1, r0
	mul	ip, r1, ip
	mov	r0, r0, lsr #8
	mov	ip, ip, lsr #8
	b	.L668
.L1232:
	ldr	fp, [sp, #12]
	b	.L468
.L1306:
	ldr	ip, [r4, #544]
	ldrh	sl, [ip, #0]
	mul	r7, sl, r7
	mul	r8, sl, r8
	mov	r7, r7, lsr #8
	mov	r8, r8, lsr #8
	b	.L795
.L1274:
	ldr	r8, [r4, #544]
	ldrh	ip, [r8, #0]
	mul	r6, ip, r6
	mul	r7, ip, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	ldr	r8, [sp, #16]
	b	.L946
.L1292:
	ldr	r8, [r4, #544]
	ldrh	ip, [r8, #0]
	mul	r6, ip, r6
	mul	r7, ip, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L737
.L1285:
	ldr	r8, [r4, #540]
	ldrh	ip, [r8, #0]
	mul	r6, ip, r6
	mul	r7, ip, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L754
.L1299:
	ldr	fp, [r4, #544]
	ldrh	lr, [fp, #0]
	mul	r7, lr, r7
	mul	ip, lr, ip
	mov	r7, r7, lsr #8
	mov	ip, ip, lsr #8
	b	.L902
.L1288:
	ldr	r9, [r4, #540]
	add	r5, r9, ip, asl #1
	ldrh	r9, [r5, #0]
	mul	r6, r9, r6
	mul	r7, r9, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L762
.L1277:
	ldr	r9, [r4, #544]
	add	r5, r9, ip, asl #1
	ldrh	sl, [r5, #0]
	mul	r6, sl, r6
	mul	r7, sl, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L954
.L1295:
	ldr	fp, [r4, #544]
	add	r5, fp, ip, asl #1
	ldrh	r8, [r5, #0]
	mul	r6, r8, r6
	mul	r7, r8, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L745
.L1302:
	ldr	r6, [r4, #544]
	add	sl, r6, lr, asl #1
	ldrh	r6, [sl, #0]
	mul	r7, r6, r7
	mul	ip, r6, ip
	mov	r7, r7, lsr #8
	mov	ip, ip, lsr #8
	b	.L910
.L1309:
	ldr	fp, [r4, #544]
	add	r6, fp, ip, asl #1
	ldrh	fp, [r6, #0]
	mul	r7, fp, r7
	mul	r8, fp, r8
	mov	r7, r7, lsr #8
	mov	r8, r8, lsr #8
	b	.L803
.L1247:
	ldr	fp, [sp, #16]
	b	.L422
.L1245:
	ldr	fp, [sp, #16]
	b	.L446
.L1200:
	ldr	fp, [sp, #16]
	b	.L513
.L1207:
	ldr	fp, [sp, #16]
	b	.L499
.L1236:
	ldr	fp, [sp, #16]
	b	.L460
.L1209:
	ldr	fp, [sp, #16]
	b	.L486
.L1317:
	ldr	r9, [r4, #544]
	add	r5, r9, ip, asl #1
	ldrh	r9, [r5, #0]
	mul	r6, r9, r6
	mul	r7, r9, r7
	mov	r6, r6, lsr #8
	mov	r7, r7, lsr #8
	b	.L694
.L1324:
	ldr	sl, [r4, #540]
	add	r1, sl, r3, asl #1
	ldrh	lr, [r1, #0]
	mul	r0, lr, r0
	mul	ip, lr, ip
	mov	r0, r0, lsr #8
	mov	ip, ip, lsr #8
	b	.L661
.L1193:
	ldr	r7, [sp, #16]
	b	.L551
.L1198:
	ldr	fp, [sp, #16]
	b	.L534
	.size	png_write_find_filter, .-png_write_find_filter
	.section	.text.png_write_IHDR,"ax",%progbits
	.align	2
	.global	png_write_IHDR
	.hidden	png_write_IHDR
	.type	png_write_IHDR, %function
png_write_IHDR:
	@ Function supports interworking.
	@ args = 16, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	sub	sp, sp, #52
	ldr	r7, [sp, #88]
	mov	r4, r0
	mov	r5, r1
	mov	r6, r2
	mov	r9, r3
	ldr	sl, [sp, #96]
	ldr	r8, [sp, #100]
	cmp	r7, #6
	ldrls	pc, [pc, r7, asl #2]
	b	.L1330
.L1336:
	.word	.L1331
	.word	.L1330
	.word	.L1332
	.word	.L1333
	.word	.L1334
	.word	.L1330
	.word	.L1335
.L1335:
	cmp	r3, #16
	cmpne	r3, #8
	bne	.L1369
.L1344:
	mov	ip, #4
	strb	ip, [r4, #326]
.L1339:
	ldr	r2, [sp, #92]
	cmp	r2, #0
	bne	.L1370
.L1345:
	ldr	lr, [r4, #588]
	tst	lr, #4
	beq	.L1346
	ldr	r3, [r4, #132]
	tst	r3, #4096
	bne	.L1346
	cmp	r7, #6
	cmpne	r7, #2
	bne	.L1346
	cmp	sl, #64
	beq	.L1347
.L1346:
	cmp	sl, #0
	bne	.L1371
.L1347:
	cmp	r8, #1
	andls	r8, r8, #255
	bls	.L1350
	mov	r0, r4
	ldr	r1, .L1379
	bl	png_warning
	mov	r8, #1
.L1350:
	ldrb	fp, [r4, #326]	@ zero_extendqisi2
	mul	lr, fp, r9
	and	r0, lr, #255
	cmp	r0, #7
	strb	r0, [r4, #325]
	mulls	r0, r5, r0
	movhi	r0, r0, lsr #3
	mulhi	r0, r5, r0
	addls	r0, r0, #7
	and	ip, r9, #255
	and	lr, r7, #255
	movls	r0, r0, lsr #3
	mov	r3, r5, lsr #8
	mov	r1, #0
	strb	r1, [r4, #636]
	strb	ip, [r4, #323]
	str	r0, [r4, #244]
	strb	fp, [r4, #327]
	strb	ip, [r4, #324]
	strb	lr, [r4, #322]
	strb	r8, [r4, #319]
	strb	sl, [r4, #596]
	str	r5, [r4, #228]
	str	r6, [r4, #232]
	str	r5, [r4, #240]
	mov	r9, r5, lsr #24
	str	r3, [sp, #20]
	mov	r2, r6, lsr #24
	mov	r7, r6, lsr #16
	strb	r9, [sp, #32]
	ldr	r9, [sp, #20]
	str	r2, [sp, #24]
	str	r7, [sp, #28]
	mov	fp, r5, lsr #16
	strb	r9, [sp, #34]
	strb	r5, [sp, #35]
	add	r5, sp, #24
	ldmia	r5, {r5, r9}	@ phole ldm
	mov	r0, r4
	ldr	r1, .L1379+4
	add	r2, sp, #32
	strb	ip, [sp, #40]
	mov	r7, r6, lsr #8
	mov	ip, #0
	mov	r3, #13
	strb	fp, [sp, #33]
	strb	r5, [sp, #36]
	strb	r9, [sp, #37]
	strb	r7, [sp, #38]
	strb	r6, [sp, #39]
	strb	lr, [sp, #41]
	strb	ip, [sp, #42]
	strb	sl, [sp, #43]
	strb	r8, [sp, #44]
	bl	png_write_chunk
	ldrb	r2, [r4, #321]	@ zero_extendqisi2
	ldr	r1, .L1379+8
	ldr	r0, .L1379+12
	cmp	r2, #0
	str	r1, [r4, #176]
	str	r0, [r4, #180]
	str	r4, [r4, #184]
	bne	.L1353
	ldrb	ip, [r4, #322]	@ zero_extendqisi2
	cmp	ip, #3
	beq	.L1354
	ldrb	r3, [r4, #323]	@ zero_extendqisi2
	cmp	r3, #7
	mvnhi	r3, #7
	strhib	r3, [r4, #321]
	movhi	r2, #248
	bhi	.L1353
.L1354:
	mov	r2, #8
	strb	r2, [r4, #321]
.L1353:
	ldr	r3, [r4, #136]
	ands	r1, r3, #1
	bne	.L1356
	cmp	r2, #8
	movne	r2, #1
	strne	r2, [r4, #224]
	streq	r1, [r4, #224]
.L1356:
	tst	r3, #2
	mvneq	r2, #0
	streq	r2, [r4, #208]
	tst	r3, #4
	moveq	r2, #8
	streq	r2, [r4, #220]
	tst	r3, #8
	moveq	r2, #15
	streq	r2, [r4, #216]
	ldr	ip, [r4, #224]
	tst	r3, #16
	ldr	lr, [r4, #220]
	moveq	r2, #8
	ldrne	r2, [r4, #212]
	streq	r2, [r4, #212]
	ldr	r1, [r4, #208]
	ldr	r3, [r4, #216]
	str	ip, [sp, #4]
	ldr	ip, .L1379+16
	str	lr, [sp, #0]
	add	r0, r4, #144
	mov	lr, #56
	str	ip, [sp, #8]
	str	lr, [sp, #12]
	bl	deflateInit2_
	cmp	r0, #0
	beq	.L1363
	cmn	r0, #6
	beq	.L1372
	cmn	r0, #2
	beq	.L1373
	cmn	r0, #4
	beq	.L1374
.L1365:
	mov	r0, r4
	ldr	r1, .L1379+20
	bl	png_error
.L1363:
	ldr	r3, [r4, #200]
	ldr	r0, [r4, #204]
	mov	r1, #1
	mov	r2, #0
	str	r1, [r4, #132]
	str	r3, [r4, #156]
	str	r0, [r4, #160]
	str	r2, [r4, #188]
	add	sp, sp, #52
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L1330:
	ldr	r1, .L1379+24
	bl	png_error
	ldr	r2, [sp, #92]
	cmp	r2, #0
	beq	.L1345
.L1370:
	mov	r0, r4
	ldr	r1, .L1379+28
	bl	png_warning
	b	.L1345
.L1331:
	cmp	r3, #16
	bls	.L1375
.L1337:
	mov	r0, r4
	ldr	r1, .L1379+32
	bl	png_error
	b	.L1339
.L1332:
	cmp	r3, #16
	cmpne	r3, #8
	bne	.L1376
.L1340:
	mov	r2, #3
	strb	r2, [r4, #326]
	b	.L1339
.L1333:
	cmp	r3, #8
	bls	.L1377
.L1341:
	mov	r0, r4
	ldr	r1, .L1379+36
	bl	png_error
	b	.L1339
.L1334:
	cmp	r3, #16
	cmpne	r3, #8
	bne	.L1378
.L1343:
	mov	fp, #2
	strb	fp, [r4, #326]
	b	.L1339
.L1371:
	mov	r0, r4
	ldr	r1, .L1379+40
	bl	png_warning
	mov	sl, #0
	b	.L1347
.L1374:
	mov	r0, r4
	ldr	r1, .L1379+44
	bl	png_error
	b	.L1365
.L1373:
	mov	r0, r4
	ldr	r1, .L1379+48
	bl	png_error
	b	.L1365
.L1372:
	mov	r0, r4
	ldr	r1, .L1379+52
	bl	png_error
	b	.L1365
.L1377:
	mov	r2, #1
	mov	r1, r2, asl r3
	bic	r0, r1, #233
	mov	r3, r0, asl #23
	mov	lr, r3, lsr #23
	cmp	lr, #0
	beq	.L1341
	strb	r2, [r4, #326]
	b	.L1339
.L1375:
	mov	r2, #1
	mov	ip, r2, asl r3
	bic	fp, ip, #-16777216
	bic	r1, fp, #16646144
	bic	r0, r1, #65024
	bic	r3, r0, #233
	cmp	r3, #0
	beq	.L1337
	strb	r2, [r4, #326]
	b	.L1339
.L1369:
	ldr	r1, .L1379+56
	bl	png_error
	b	.L1344
.L1378:
	ldr	r1, .L1379+60
	bl	png_error
	b	.L1343
.L1376:
	ldr	r1, .L1379+64
	bl	png_error
	b	.L1340
.L1380:
	.align	2
.L1379:
	.word	.LC35
	.word	png_IHDR
	.word	png_zalloc
	.word	png_zfree
	.word	.LC36
	.word	.LC40
	.word	.LC32
	.word	.LC33
	.word	.LC27
	.word	.LC29
	.word	.LC34
	.word	.LC39
	.word	.LC38
	.word	.LC37
	.word	.LC31
	.word	.LC30
	.word	.LC28
	.size	png_write_IHDR, .-png_write_IHDR
	.section	.text.png_write_pHYs,"ax",%progbits
	.align	2
	.global	png_write_pHYs
	.hidden	png_write_pHYs
	.type	png_write_pHYs, %function
png_write_pHYs:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	cmp	r3, #1
	sub	sp, sp, #20
	mov	r7, r3
	mov	r6, r1
	mov	r5, r2
	mov	r4, r0
	ldrgt	r1, .L1385
	blgt	png_warning
.L1382:
	cmp	r4, #0
	beq	.L1384
	mov	lr, r6, lsr #24
	mov	ip, r6, lsr #16
	mov	r3, r5, lsr #8
	mov	r0, r4
	ldr	r1, .L1385+4
	mov	r2, #9
	mov	sl, r6, lsr #8
	mov	r9, r5, lsr #24
	mov	fp, r5, lsr #16
	strb	lr, [sp, #0]
	strb	ip, [sp, #1]
	strb	r3, [sp, #6]
	strb	sl, [sp, #2]
	strb	r6, [sp, #3]
	strb	r9, [sp, #4]
	strb	fp, [sp, #5]
	strb	r5, [sp, #7]
	strb	r7, [sp, #8]
	bl	png_write_chunk_start
	mov	r0, r4
	mov	r2, #9
	mov	r1, sp
	bl	png_write_data
	mov	r0, r4
	mov	r1, sp
	mov	r2, #9
	bl	png_calculate_crc
	ldr	r3, [r4, #300]
	mov	r0, r4
	mov	lr, r3, lsr #16
	mov	r4, r3, lsr #24
	mov	ip, r3, lsr #8
	add	r1, sp, #12
	mov	r2, #4
	mov	r8, sp
	strb	r4, [sp, #12]
	strb	lr, [sp, #13]
	strb	ip, [sp, #14]
	strb	r3, [sp, #15]
	bl	png_write_data
.L1384:
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L1386:
	.align	2
.L1385:
	.word	.LC41
	.word	png_pHYs
	.size	png_write_pHYs, .-png_write_pHYs
	.section	.text.png_write_sPLT,"ax",%progbits
	.align	2
	.global	png_write_sPLT
	.hidden	png_write_sPLT
	.type	png_write_sPLT, %function
png_write_sPLT:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	ldrb	r7, [r1, #4]	@ zero_extendqisi2
	sub	sp, sp, #24
	cmp	r7, #8
	mov	r5, r1
	add	r2, sp, #20
	ldr	r1, [r1, #0]
	moveq	r7, #6
	movne	r7, #10
	mov	r6, r0
	ldr	r8, [r5, #12]
	bl	png_check_keyword
	subs	r4, r0, #0
	bne	.L1410
.L1403:
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L1410:
	add	r2, r4, #2
	mla	r2, r8, r7, r2
	mov	r0, r6
	ldr	r1, .L1412
	bl	png_write_chunk_start
	cmp	r6, #0
	ldr	r8, [sp, #20]
	beq	.L1391
	cmp	r8, #0
	cmnne	r4, #1
	add	r4, r4, #1
	beq	.L1392
	mov	r0, r6
	mov	r1, r8
	mov	r2, r4
	bl	png_write_data
	mov	r0, r6
	mov	r1, r8
	mov	r2, r4
	bl	png_calculate_crc
.L1392:
	add	r4, r5, #4
	mov	r2, #1
	mov	r0, r6
	mov	r1, r4
	bl	png_write_data
	mov	r0, r6
	mov	r1, r4
	mov	r2, #1
	bl	png_calculate_crc
.L1391:
	ldr	r0, [r5, #12]
	ldr	r4, [r5, #8]
	add	r3, r0, r0, asl #2
	add	r2, r4, r3, asl #1
	cmp	r4, r2
	bcs	.L1393
	cmp	r6, #0
	beq	.L1394
	add	r4, r4, #10
	add	r8, sp, #4
	b	.L1397
.L1411:
	mov	lr, r3, lsr #8
	strb	r3, [sp, #5]
	strb	lr, [sp, #4]
	ldrh	ip, [r4, #-8]
	mov	r2, ip, lsr #8
	strb	r2, [sp, #6]
	strb	ip, [sp, #7]
	ldrh	r0, [r4, #-6]
	mov	r1, r0, lsr #8
	strb	r1, [sp, #8]
	strb	r0, [sp, #9]
	ldrh	lr, [r4, #-4]
	mov	r3, lr, lsr #8
	strb	r3, [sp, #10]
	strb	lr, [sp, #11]
	ldrh	ip, [r4, #-2]
	mov	r2, ip, lsr #8
	strb	r2, [sp, #12]
	strb	ip, [sp, #13]
.L1396:
	mov	r0, r6
	mov	r1, r8
	mov	r2, r7
	bl	png_write_data
	mov	r1, r8
	mov	r2, r7
	mov	r0, r6
	bl	png_calculate_crc
	ldr	r2, [r5, #12]
	ldr	ip, [r5, #8]
	add	r1, r2, r2, asl #2
	add	r3, ip, r1, asl #1
	cmp	r4, r3
	add	r4, r4, #10
	bcs	.L1398
.L1397:
	ldrb	r1, [r5, #4]	@ zero_extendqisi2
	cmp	r1, #8
	ldrh	r3, [r4, #-10]
	bne	.L1411
	strb	r3, [sp, #4]
	ldrh	r2, [r4, #-8]
	strb	r2, [sp, #5]
	ldrh	ip, [r4, #-6]
	strb	ip, [sp, #6]
	ldrh	r1, [r4, #-4]
	strb	r1, [sp, #7]
	ldrh	r3, [r4, #-2]
	mov	r0, r3, lsr #8
	strb	r0, [sp, #8]
	strb	r3, [sp, #9]
	b	.L1396
.L1393:
	cmp	r6, #0
	beq	.L1402
.L1398:
	ldr	r3, [r6, #300]
	mov	r0, r6
	mov	r4, r3, lsr #24
	mov	lr, r3, lsr #16
	mov	ip, r3, lsr #8
	add	r1, sp, #16
	mov	r2, #4
	strb	r4, [sp, #16]
	strb	lr, [sp, #17]
	strb	ip, [sp, #18]
	strb	r3, [sp, #19]
	bl	png_write_data
.L1402:
	mov	r0, r6
	ldr	r1, [sp, #20]
	bl	png_free
	b	.L1403
.L1394:
	ldrb	lr, [r5, #4]	@ zero_extendqisi2
	cmp	lr, #8
	add	r3, r4, #10
	mov	r0, r2
	beq	.L1409
.L1401:
	ldrh	r2, [r3, #-10]
	mov	r1, r2, lsr #8
	strb	r1, [sp, #4]
	strb	r2, [sp, #5]
	ldrh	lr, [r3, #-8]
	mov	ip, lr, lsr #8
	strb	ip, [sp, #6]
	strb	lr, [sp, #7]
	ldrh	r2, [r3, #-6]
	mov	r1, r2, lsr #8
	strb	r1, [sp, #8]
	strb	r2, [sp, #9]
	ldrh	lr, [r3, #-4]
	mov	ip, lr, lsr #8
	strb	ip, [sp, #10]
	strb	lr, [sp, #11]
	ldrh	ip, [r3, #-2]
	cmp	r0, r3
	mov	r1, ip, lsr #8
	strb	r1, [sp, #12]
	strb	ip, [sp, #13]
	add	r3, r3, #10
	bhi	.L1401
	b	.L1402
.L1409:
	ldrh	ip, [r3, #-10]
	strb	ip, [sp, #4]
	ldrh	r1, [r3, #-8]
	strb	r1, [sp, #5]
	ldrh	r2, [r3, #-6]
	strb	r2, [sp, #6]
	ldrh	lr, [r3, #-4]
	strb	lr, [sp, #7]
	ldrh	r2, [r3, #-2]
	cmp	r0, r3
	mov	r1, r2, lsr #8
	strb	r1, [sp, #8]
	strb	r2, [sp, #9]
	add	r3, r3, #10
	bhi	.L1409
	b	.L1402
.L1413:
	.align	2
.L1412:
	.word	png_sPLT
	.size	png_write_sPLT, .-png_write_sPLT
	.section	.text.png_write_tEXt,"ax",%progbits
	.align	2
	.global	png_write_tEXt
	.hidden	png_write_tEXt
	.type	png_write_tEXt, %function
png_write_tEXt:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	sub	sp, sp, #8
	mov	r5, r2
	add	r2, sp, #4
	mov	r4, r0
	bl	png_check_keyword
	subs	r6, r0, #0
	beq	.L1422
	cmp	r5, #0
	beq	.L1416
	ldrb	r3, [r5, #0]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L1423
.L1416:
	mov	r7, #0
.L1417:
	add	r6, r6, #1
	mov	r0, r4
	ldr	r1, .L1425
	add	r2, r7, r6
	bl	png_write_chunk_start
	cmp	r4, #0
	ldr	r8, [sp, #4]
	beq	.L1418
	cmp	r6, #0
	cmpne	r8, #0
	beq	.L1418
	mov	r0, r4
	mov	r1, r8
	mov	r2, r6
	bl	png_write_data
	mov	r0, r4
	mov	r1, r8
	mov	r2, r6
	bl	png_calculate_crc
.L1418:
	cmp	r7, #0
	bne	.L1424
	cmp	r4, #0
	beq	.L1420
.L1421:
	ldr	r3, [r4, #300]
	mov	r0, r4
	mov	r5, r3, lsr #24
	mov	lr, r3, lsr #16
	mov	ip, r3, lsr #8
	mov	r1, sp
	mov	r2, #4
	strb	r5, [sp, #0]
	strb	lr, [sp, #1]
	strb	ip, [sp, #2]
	strb	r3, [sp, #3]
	bl	png_write_data
.L1420:
	mov	r0, r4
	ldr	r1, [sp, #4]
	bl	png_free
.L1422:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L1423:
	mov	r0, r5
	bl	strlen
	mov	r7, r0
	b	.L1417
.L1424:
	cmp	r4, #0
	beq	.L1420
	cmp	r5, #0
	beq	.L1421
	mov	r0, r4
	mov	r1, r5
	mov	r2, r7
	bl	png_write_data
	mov	r0, r4
	mov	r1, r5
	mov	r2, r7
	bl	png_calculate_crc
	b	.L1421
.L1426:
	.align	2
.L1425:
	.word	png_tEXt
	.size	png_write_tEXt, .-png_write_tEXt
	.section	.text.png_write_compressed_data_out,"ax",%progbits
	.align	2
	.type	png_write_compressed_data_out, %function
png_write_compressed_data_out:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	ldr	r4, [r1, #0]
	cmp	r4, #0
	mov	r5, r1
	mov	r7, r0
	beq	.L1438
	cmp	r0, #0
	ldr	r5, [r1, #4]
	beq	.L1436
	cmp	r5, #0
	bne	.L1439
.L1436:
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L1439:
	mov	r1, r4
	mov	r2, r5
	bl	png_write_data
	mov	r0, r7
	mov	r1, r4
	mov	r2, r5
	bl	png_calculate_crc
	b	.L1436
.L1438:
	ldr	r3, [r1, #8]
	cmp	r3, #0
	movgt	r9, r4
	ble	.L1430
.L1433:
	ldr	r0, [r5, #16]
	ldr	r8, [r7, #204]
	ldr	r6, [r0, r4, asl #2]
	cmp	r6, #0
	cmpne	r8, #0
	mov	sl, r4, asl #2
	beq	.L1432
	mov	r1, r6
	mov	r0, r7
	mov	r2, r8
	bl	png_write_data
	mov	r1, r6
	mov	r0, r7
	mov	r2, r8
	bl	png_calculate_crc
	ldr	r1, [r5, #16]
	ldr	r6, [r1, r4, asl #2]
.L1432:
	mov	r0, r7
	mov	r1, r6
	bl	png_free
	ldr	ip, [r5, #16]
	str	r9, [ip, sl]
	ldr	r2, [r5, #8]
	add	r4, r4, #1
	cmp	r2, r4
	bgt	.L1433
.L1430:
	ldr	lr, [r5, #12]
	cmp	lr, #0
	bne	.L1440
	mov	r3, #0
	str	r3, [r5, #16]
	ldr	r3, [r7, #160]
	ldr	r5, [r7, #204]
	cmp	r3, r5
	bcc	.L1441
.L1435:
	add	r0, r7, #144
	bl	deflateReset
	mov	r0, #0
	str	r0, [r7, #188]
	b	.L1436
.L1440:
	ldr	r1, [r5, #16]
	mov	r0, r7
	bl	png_free
	mov	r3, #0
	str	r3, [r5, #16]
	ldr	r3, [r7, #160]
	ldr	r5, [r7, #204]
	cmp	r3, r5
	bcs	.L1435
.L1441:
	ldr	r4, [r7, #200]
	cmp	r5, r3
	cmpne	r4, #0
	rsb	r5, r3, r5
	beq	.L1435
	mov	r0, r7
	mov	r1, r4
	mov	r2, r5
	bl	png_write_data
	mov	r0, r7
	mov	r1, r4
	mov	r2, r5
	bl	png_calculate_crc
	b	.L1435
	.size	png_write_compressed_data_out, .-png_write_compressed_data_out
	.section	.text.png_write_zTXt,"ax",%progbits
	.align	2
	.global	png_write_zTXt
	.hidden	png_write_zTXt
	.type	png_write_zTXt, %function
png_write_zTXt:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	sub	sp, sp, #40
	mov	r3, #0
	mov	r7, r2
	add	r2, sp, #32
	str	r3, [sp, #12]
	str	r3, [sp, #16]
	str	r3, [sp, #20]
	str	r3, [sp, #24]
	str	r3, [sp, #8]
	mov	r4, r0
	ldr	r5, [sp, #64]
	bl	png_check_keyword
	subs	r6, r0, #0
	beq	.L1450
	cmp	r7, #0
	beq	.L1445
	ldrb	r0, [r7, #0]	@ zero_extendqisi2
	cmn	r5, #1
	cmpne	r0, #0
	bne	.L1446
.L1445:
	mov	r2, r7
	ldr	r1, [sp, #32]
	mov	r0, r4
	mov	r3, #0
	bl	png_write_tEXt
.L1450:
	mov	r0, r4
	ldr	r1, [sp, #32]
	bl	png_free
.L1449:
	add	sp, sp, #40
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L1446:
	mov	r0, r7
	bl	strlen
	mov	r1, r7
	mov	r2, r0
	mov	r3, r5
	add	r7, sp, #8
	mov	r0, r4
	str	r7, [sp, #0]
	bl	png_text_compress
	add	r2, r6, #2
	add	r2, r2, r0
	ldr	r1, .L1452
	mov	r0, r4
	bl	png_write_chunk_start
	cmp	r4, #0
	ldr	r8, [sp, #32]
	beq	.L1447
	cmp	r8, #0
	cmnne	r6, #1
	add	r6, r6, #1
	bne	.L1451
.L1448:
	add	r6, sp, #40
	mov	r1, r8
	mov	r0, r4
	bl	png_free
	strb	r5, [r6, #-4]!
	mov	r0, r4
	mov	r1, r6
	mov	r2, #1
	bl	png_write_data
	mov	r0, r4
	mov	r1, r6
	mov	r2, #1
	bl	png_calculate_crc
	mov	r0, r4
	mov	r1, r7
	bl	png_write_compressed_data_out
	ldr	r3, [r4, #300]
	mov	r0, r4
	mov	lr, r3, lsr #16
	mov	r4, r3, lsr #24
	mov	ip, r3, lsr #8
	add	r1, sp, #28
	mov	r2, #4
	strb	r4, [sp, #28]
	strb	lr, [sp, #29]
	strb	ip, [sp, #30]
	strb	r3, [sp, #31]
	bl	png_write_data
	b	.L1449
.L1451:
	mov	r1, r8
	mov	r0, r4
	mov	r2, r6
	bl	png_write_data
	mov	r1, r8
	mov	r0, r4
	mov	r2, r6
	bl	png_calculate_crc
	ldr	r8, [sp, #32]
	b	.L1448
.L1447:
	mov	r1, r8
	mov	r0, r4
	bl	png_free
	mov	r0, r4
	mov	r1, r7
	strb	r5, [sp, #36]
	bl	png_write_compressed_data_out
	b	.L1449
.L1453:
	.align	2
.L1452:
	.word	png_zTXt
	.size	png_write_zTXt, .-png_write_zTXt
	.section	.text.png_write_iCCP,"ax",%progbits
	.align	2
	.global	png_write_iCCP
	.hidden	png_write_iCCP
	.type	png_write_iCCP, %function
png_write_iCCP:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	sub	sp, sp, #40
	mov	ip, #0
	mov	r6, r2
	add	r2, sp, #36
	str	ip, [sp, #16]
	mov	r8, r3
	str	ip, [sp, #20]
	str	ip, [sp, #24]
	str	ip, [sp, #28]
	str	ip, [sp, #12]
	mov	r4, r0
	ldr	r7, [sp, #64]
	bl	png_check_keyword
	subs	r5, r0, #0
	beq	.L1467
	cmp	r6, #0
	bne	.L1469
.L1456:
	cmp	r8, #0
	moveq	r7, r8
	moveq	r2, r7
	beq	.L1458
	cmp	r7, #3
	ldrgtb	r3, [r8, #3]	@ zero_extendqisi2
	ldrgtb	r6, [r8, #0]	@ zero_extendqisi2
	ldrgtb	r2, [r8, #1]	@ zero_extendqisi2
	orrgt	r6, r3, r6, asl #24
	ldrgtb	r3, [r8, #2]	@ zero_extendqisi2
	orrgt	r6, r6, r2, asl #16
	movle	r6, #0
	orrgt	r6, r6, r3, asl #8
	cmp	r6, r7
	bgt	.L1470
	bge	.L1462
	mov	r0, r4
	ldr	r1, .L1472
	bl	png_warning
	mov	r7, r6
.L1462:
	cmp	r7, #0
	bne	.L1463
.L1468:
	mov	r2, r7
.L1458:
	add	r6, r5, #2
	add	r2, r2, r6
	mov	r0, r4
	ldr	r1, .L1472+4
	bl	png_write_chunk_start
	ldr	r3, [sp, #36]
	add	r5, r5, r3
	mov	r3, #0
	strb	r3, [r5, #1]
	cmp	r4, r3
	ldr	r5, [sp, #36]
	beq	.L1464
	cmp	r6, r3
	cmpne	r5, r3
	beq	.L1464
	mov	r0, r4
	mov	r1, r5
	mov	r2, r6
	bl	png_write_data
	mov	r0, r4
	mov	r1, r5
	mov	r2, r6
	bl	png_calculate_crc
.L1464:
	cmp	r7, #0
	bne	.L1471
.L1465:
	cmp	r4, #0
	beq	.L1466
	ldr	r3, [r4, #300]
	mov	r0, r4
	mov	r5, r3, lsr #24
	mov	lr, r3, lsr #16
	mov	ip, r3, lsr #8
	add	r1, sp, #32
	mov	r2, #4
	strb	r5, [sp, #32]
	strb	lr, [sp, #33]
	strb	ip, [sp, #34]
	strb	r3, [sp, #35]
	bl	png_write_data
.L1466:
	mov	r0, r4
	ldr	r1, [sp, #36]
	bl	png_free
.L1467:
	add	sp, sp, #40
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L1469:
	mov	r0, r4
	ldr	r1, .L1472+8
	bl	png_warning
	b	.L1456
.L1463:
	mov	r2, r7
	mov	r1, r8
	mov	r0, r4
	mov	r3, #0
	add	r6, sp, #12
	str	r6, [sp, #0]
	bl	png_text_compress
	mov	r7, r0
	b	.L1468
.L1470:
	mov	r0, r4
	ldr	r1, .L1472+12
	bl	png_warning
	b	.L1467
.L1471:
	mov	r0, r4
	add	r1, sp, #12
	bl	png_write_compressed_data_out
	b	.L1465
.L1473:
	.align	2
.L1472:
	.word	.LC44
	.word	png_iCCP
	.word	.LC42
	.word	.LC43
	.size	png_write_iCCP, .-png_write_iCCP
	.section	.rodata
	.align	2
	.set	.LANCHOR0,. + 0
	.type	C.1.4084, %object
	.size	C.1.4084, 8
C.1.4084:
	.byte	-119
	.byte	80
	.byte	78
	.byte	71
	.byte	13
	.byte	10
	.byte	26
	.byte	10
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"Unknown compression type %d\000"
.LC1:
	.ascii	"zlib error\000"
	.space	1
.LC2:
	.ascii	"zero length keyword\000"
.LC3:
	.ascii	"Out of memory while procesing keyword\000"
	.space	2
.LC4:
	.ascii	"invalid keyword character 0x%02X\000"
	.space	3
.LC5:
	.ascii	"trailing spaces removed from keyword\000"
	.space	3
.LC6:
	.ascii	"leading spaces removed from keyword\000"
.LC7:
	.ascii	"extra interior spaces removed from keyword\000"
	.space	1
.LC8:
	.ascii	"Zero length keyword\000"
.LC9:
	.ascii	"keyword length must be 1 - 79 characters\000"
	.space	3
.LC10:
	.ascii	"Invalid number of histogram entries specified\000"
	.space	2
.LC11:
	.ascii	"Invalid number of colors in palette\000"
.LC12:
	.ascii	"Ignoring request to write a PLTE chunk in grayscale"
	.ascii	" PNG\000"
.LC13:
	.ascii	"Unrecognized equation type for pCAL chunk\000"
	.space	2
.LC14:
	.ascii	"Invalid time specified for tIME chunk\000"
	.space	2
.LC15:
	.ascii	"%12.12e\000"
.LC16:
	.ascii	"Unrecognized unit type for oFFs chunk\000"
	.space	2
.LC17:
	.ascii	"Invalid background palette index\000"
	.space	3
.LC18:
	.ascii	"Ignoring attempt to write 16-bit bKGD chunk when bi"
	.ascii	"t_depth is 8\000"
.LC19:
	.ascii	"Ignoring attempt to write bKGD chunk out-of-range f"
	.ascii	"or bit_depth\000"
.LC20:
	.ascii	"Invalid number of transparent colors specified\000"
	.space	1
.LC21:
	.ascii	"Ignoring attempt to write tRNS chunk out-of-range f"
	.ascii	"or bit_depth\000"
.LC22:
	.ascii	"Ignoring attempt to write 16-bit tRNS chunk when bi"
	.ascii	"t_depth is 8\000"
.LC23:
	.ascii	"Can't write tRNS with an alpha channel\000"
	.space	1
.LC24:
	.ascii	"Invalid sBIT depth specified\000"
	.space	3
.LC25:
	.ascii	"Invalid sRGB rendering intent specified\000"
.LC26:
	.ascii	"Invalid zlib compression method or flags in IDAT\000"
	.space	3
.LC27:
	.ascii	"Invalid bit depth for grayscale image\000"
	.space	2
.LC28:
	.ascii	"Invalid bit depth for RGB image\000"
.LC29:
	.ascii	"Invalid bit depth for paletted image\000"
	.space	3
.LC30:
	.ascii	"Invalid bit depth for grayscale+alpha image\000"
.LC31:
	.ascii	"Invalid bit depth for RGBA image\000"
	.space	3
.LC32:
	.ascii	"Invalid image color type specified\000"
	.space	1
.LC33:
	.ascii	"Invalid compression type specified\000"
	.space	1
.LC34:
	.ascii	"Invalid filter type specified\000"
	.space	2
.LC35:
	.ascii	"Invalid interlace type specified\000"
	.space	3
.LC36:
	.ascii	"1.2.3\000"
	.space	2
.LC37:
	.ascii	"zlib failed to initialize compressor -- version err"
	.ascii	"or\000"
	.space	2
.LC38:
	.ascii	"zlib failed to initialize compressor -- stream erro"
	.ascii	"r\000"
	.space	3
.LC39:
	.ascii	"zlib failed to initialize compressor -- mem error\000"
	.space	2
.LC40:
	.ascii	"zlib failed to initialize compressor\000"
	.space	3
.LC41:
	.ascii	"Unrecognized unit type for pHYs chunk\000"
	.space	2
.LC42:
	.ascii	"Unknown compression type in iCCP chunk\000"
	.space	1
.LC43:
	.ascii	"Embedded profile length too large in iCCP chunk\000"
.LC44:
	.ascii	"Truncating profile to actual length in iCCP chunk\000"
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
