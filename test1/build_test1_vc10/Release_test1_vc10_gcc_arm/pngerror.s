	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"pngerror.c"
	.section	.text.png_set_error_fn,"ax",%progbits
	.align	2
	.global	png_set_error_fn
	.hidden	png_set_error_fn
	.type	png_set_error_fn, %function
png_set_error_fn:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	strne	r3, [r0, #96]
	strne	r1, [r0, #100]
	strne	r2, [r0, #92]
	bx	lr
	.size	png_set_error_fn, .-png_set_error_fn
	.section	.text.png_get_error_ptr,"ax",%progbits
	.align	2
	.global	png_get_error_ptr
	.hidden	png_get_error_ptr
	.type	png_get_error_ptr, %function
png_get_error_ptr:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	ldrne	r0, [r0, #100]
	bx	lr
	.size	png_get_error_ptr, .-png_get_error_ptr
	.section	.text.png_set_strip_error_numbers,"ax",%progbits
	.align	2
	.global	png_set_strip_error_numbers
	.hidden	png_set_strip_error_numbers
	.type	png_set_strip_error_numbers, %function
png_set_strip_error_numbers:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	ldrne	r3, [r0, #136]
	bicne	r3, r3, #786432
	andne	r1, r3, r1
	strne	r1, [r0, #136]
	bx	lr
	.size	png_set_strip_error_numbers, .-png_set_strip_error_numbers
	.section	.text.png_warning,"ax",%progbits
	.align	2
	.global	png_warning
	.hidden	png_warning
	.type	png_warning, %function
png_warning:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
	cmp	r0, #0
	sub	sp, sp, #20
	beq	.L12
	ldr	r3, [r0, #136]
	tst	r3, #786432
	bne	.L38
.L13:
	mov	ip, r1
.L14:
	ldr	r3, [r0, #96]
	cmp	r3, #0
	beq	.L39
	mov	r1, ip
	mov	lr, pc
	bx	r3
.L35:
	add	sp, sp, #20
	ldr	lr, [sp], #4
	bx	lr
.L38:
	ldrb	r2, [r1, #0]	@ zero_extendqisi2
	cmp	r2, #35
	bne	.L13
	ldrb	ip, [r1, #1]	@ zero_extendqisi2
	cmp	ip, #32
	add	ip, r1, #1
	beq	.L14
	ldrb	lr, [r1, #2]	@ zero_extendqisi2
	cmp	lr, #32
	add	ip, r1, #2
	beq	.L14
	ldrb	r3, [r1, #3]	@ zero_extendqisi2
	cmp	r3, #32
	add	ip, r1, #3
	beq	.L14
	ldrb	r2, [r1, #4]	@ zero_extendqisi2
	cmp	r2, #32
	add	ip, r1, #4
	beq	.L14
	ldrb	ip, [r1, #5]	@ zero_extendqisi2
	cmp	ip, #32
	add	ip, r1, #5
	beq	.L14
	ldrb	lr, [r1, #6]	@ zero_extendqisi2
	cmp	lr, #32
	add	ip, r1, #6
	beq	.L14
	ldrb	r3, [r1, #7]	@ zero_extendqisi2
	cmp	r3, #32
	add	ip, r1, #7
	beq	.L14
	ldrb	r2, [r1, #8]	@ zero_extendqisi2
	cmp	r2, #32
	add	ip, r1, #8
	beq	.L14
	ldrb	ip, [r1, #9]	@ zero_extendqisi2
	cmp	ip, #32
	add	ip, r1, #9
	beq	.L14
	ldrb	lr, [r1, #10]	@ zero_extendqisi2
	cmp	lr, #32
	add	ip, r1, #10
	beq	.L14
	ldrb	r3, [r1, #11]	@ zero_extendqisi2
	cmp	r3, #32
	add	ip, r1, #11
	beq	.L14
	ldrb	r2, [r1, #12]	@ zero_extendqisi2
	cmp	r2, #32
	add	ip, r1, #12
	beq	.L14
	ldrb	ip, [r1, #13]	@ zero_extendqisi2
	cmp	ip, #32
	add	ip, r1, #13
	beq	.L14
	ldrb	lr, [r1, #14]	@ zero_extendqisi2
	cmp	lr, #32
	add	ip, r1, #14
	addne	ip, r1, #15
	b	.L14
.L39:
	mov	r1, ip
.L12:
	ldrb	r0, [r1, #0]	@ zero_extendqisi2
	cmp	r0, #35
	bne	.L16
	ldrb	r2, [r1, #1]	@ zero_extendqisi2
	strb	r2, [sp, #0]
	ldrb	r3, [r1, #0]	@ zero_extendqisi2
	cmp	r3, #32
	moveq	ip, #0
	beq	.L18
	ldrb	r0, [r1, #2]	@ zero_extendqisi2
	strb	r0, [sp, #1]
	ldrb	lr, [r1, #1]	@ zero_extendqisi2
	cmp	lr, #32
	moveq	ip, #1
	beq	.L18
	ldrb	r2, [r1, #3]	@ zero_extendqisi2
	strb	r2, [sp, #2]
	ldrb	r3, [r1, #2]	@ zero_extendqisi2
	cmp	r3, #32
	moveq	ip, #2
	beq	.L18
	ldrb	r0, [r1, #4]	@ zero_extendqisi2
	strb	r0, [sp, #3]
	ldrb	lr, [r1, #3]	@ zero_extendqisi2
	cmp	lr, #32
	moveq	ip, #3
	beq	.L18
	ldrb	r2, [r1, #5]	@ zero_extendqisi2
	strb	r2, [sp, #4]
	ldrb	r3, [r1, #4]	@ zero_extendqisi2
	cmp	r3, #32
	moveq	ip, #4
	beq	.L18
	ldrb	r0, [r1, #6]	@ zero_extendqisi2
	strb	r0, [sp, #5]
	ldrb	lr, [r1, #5]	@ zero_extendqisi2
	cmp	lr, #32
	moveq	ip, #5
	beq	.L18
	ldrb	r2, [r1, #7]	@ zero_extendqisi2
	strb	r2, [sp, #6]
	ldrb	r3, [r1, #6]	@ zero_extendqisi2
	cmp	r3, #32
	moveq	ip, #6
	beq	.L18
	ldrb	r0, [r1, #8]	@ zero_extendqisi2
	strb	r0, [sp, #7]
	ldrb	lr, [r1, #7]	@ zero_extendqisi2
	cmp	lr, #32
	moveq	ip, #7
	beq	.L18
	ldrb	r2, [r1, #9]	@ zero_extendqisi2
	strb	r2, [sp, #8]
	ldrb	r3, [r1, #8]	@ zero_extendqisi2
	cmp	r3, #32
	moveq	ip, #8
	beq	.L18
	ldrb	r0, [r1, #10]	@ zero_extendqisi2
	strb	r0, [sp, #9]
	ldrb	lr, [r1, #9]	@ zero_extendqisi2
	cmp	lr, #32
	moveq	ip, #9
	beq	.L18
	ldrb	r2, [r1, #11]	@ zero_extendqisi2
	strb	r2, [sp, #10]
	ldrb	r3, [r1, #10]	@ zero_extendqisi2
	cmp	r3, #32
	moveq	ip, #10
	beq	.L18
	ldrb	r0, [r1, #12]	@ zero_extendqisi2
	strb	r0, [sp, #11]
	ldrb	lr, [r1, #11]	@ zero_extendqisi2
	cmp	lr, #32
	moveq	ip, #11
	beq	.L18
	ldrb	r2, [r1, #13]	@ zero_extendqisi2
	strb	r2, [sp, #12]
	ldrb	r3, [r1, #12]	@ zero_extendqisi2
	cmp	r3, #32
	moveq	ip, #12
	beq	.L18
	ldrb	r0, [r1, #14]	@ zero_extendqisi2
	strb	r0, [sp, #13]
	ldrb	lr, [r1, #13]	@ zero_extendqisi2
	cmp	lr, #32
	moveq	ip, #13
	beq	.L18
	ldrb	r3, [r1, #15]	@ zero_extendqisi2
	strb	r3, [sp, #14]
	ldrb	ip, [r1, #14]	@ zero_extendqisi2
	cmp	ip, #32
	beq	.L40
.L16:
	ldr	r3, .L41
	mov	r2, r1
	ldr	r0, [r3, #0]
	ldr	r1, .L41+4
	bl	fprintf
	b	.L35
.L40:
	mov	ip, #14
.L18:
	sub	r2, ip, #2
	cmp	r2, #12
	bhi	.L16
	add	r3, r1, ip
	ldr	r2, .L41
	add	r1, sp, #16
	add	ip, r1, ip
	mov	lr, #0
	ldr	r0, [r2, #0]
	ldr	r1, .L41+8
	mov	r2, sp
	strb	lr, [ip, #-15]
	bl	fprintf
	b	.L35
.L42:
	.align	2
.L41:
	.word	__aeabi_stderr
	.word	.LC1
	.word	.LC0
	.size	png_warning, .-png_warning
	.section	.text.png_chunk_warning,"ax",%progbits
	.align	2
	.global	png_chunk_warning
	.hidden	png_chunk_warning
	.type	png_chunk_warning, %function
png_chunk_warning:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 88
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	subs	r5, r0, #0
	sub	sp, sp, #92
	mov	r2, r1
	beq	.L57
	ldrb	r3, [r5, #312]	@ zero_extendqisi2
	sub	r4, r3, #65
	cmp	r4, #57
	movls	r1, #0
	movhi	r1, #1
	sub	r0, r3, #91
	cmp	r0, #5
	orrls	r1, r1, #1
	cmp	r1, #0
	streqb	r3, [sp, #4]
	moveq	r3, #1
	bne	.L58
.L47:
	ldrb	r1, [r5, #313]	@ zero_extendqisi2
	sub	r4, r1, #65
	cmp	r4, #57
	movls	lr, #0
	movhi	lr, #1
	sub	ip, r1, #91
	cmp	ip, #5
	orrls	lr, lr, #1
	cmp	lr, #0
	bne	.L48
	add	r4, sp, #4
	strb	r1, [r4, r3]
	add	r3, r3, #1
.L49:
	ldrb	r1, [r5, #314]	@ zero_extendqisi2
	sub	lr, r1, #65
	cmp	lr, #57
	movls	r0, #0
	movhi	r0, #1
	sub	ip, r1, #91
	cmp	ip, #5
	orrls	r0, r0, #1
	cmp	r0, #0
	streqb	r1, [r4, r3]
	addeq	r3, r3, #1
	beq	.L51
	ldr	r7, .L59
	add	ip, r3, #1
	and	r0, r1, #15
	ldrb	r6, [r7, r0]	@ zero_extendqisi2
	ldrb	lr, [r7, r1, lsr #4]	@ zero_extendqisi2
	add	r0, ip, #1
	mov	r7, #91
	strb	r7, [r4, r3]
	add	r1, r0, #1
	mov	r3, #93
	strb	lr, [r4, ip]
	strb	r6, [r4, r0]
	strb	r3, [r4, r1]
	add	r3, r1, #1
.L51:
	ldrb	r1, [r5, #315]	@ zero_extendqisi2
	sub	r0, r1, #65
	cmp	r0, #57
	movls	lr, #0
	movhi	lr, #1
	sub	ip, r1, #91
	cmp	ip, #5
	orrls	lr, lr, #1
	cmp	lr, #0
	streqb	r1, [r4, r3]
	addeq	r3, r3, #1
	beq	.L53
	ldr	r7, .L59
	add	ip, r3, #1
	and	r0, r1, #15
	ldrb	r6, [r7, r0]	@ zero_extendqisi2
	ldrb	lr, [r7, r1, lsr #4]	@ zero_extendqisi2
	add	r0, ip, #1
	mov	r7, #91
	strb	r7, [r4, r3]
	add	r1, r0, #1
	mov	r3, #93
	strb	lr, [r4, ip]
	strb	r6, [r4, r0]
	strb	r3, [r4, r1]
	add	r3, r1, #1
.L53:
	cmp	r2, #0
	streqb	r2, [r4, r3]
	beq	.L55
	add	ip, r3, #1
	add	r6, ip, #1
	mov	lr, #58
	strb	lr, [r4, r3]
	add	r6, r4, r6
	mov	r3, #32
	mov	r1, r2
	mov	r0, r6
	mov	r2, #64
	strb	r3, [r4, ip]
	bl	memcpy
	mov	r2, #0
	strb	r2, [r6, #63]
.L55:
	mov	r0, r5
	mov	r1, r4
.L57:
	bl	png_warning
	add	sp, sp, #92
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L48:
	ldr	r7, .L59
	add	ip, r3, #1
	and	r4, r1, #15
	ldrb	r6, [r7, r4]	@ zero_extendqisi2
	ldrb	lr, [r7, r1, lsr #4]	@ zero_extendqisi2
	add	r4, sp, #4
	add	r0, ip, #1
	mov	r7, #91
	strb	r7, [r4, r3]
	add	r1, r0, #1
	mov	r3, #93
	strb	lr, [r4, ip]
	strb	r6, [r4, r0]
	strb	r3, [r4, r1]
	add	r3, r1, #1
	b	.L49
.L58:
	ldr	r4, .L59
	and	r0, r3, #15
	ldrb	lr, [r4, r3, lsr #4]	@ zero_extendqisi2
	ldrb	ip, [r4, r0]	@ zero_extendqisi2
	mov	r3, #93
	mov	r1, #91
	strb	r3, [sp, #7]
	strb	r1, [sp, #4]
	strb	lr, [sp, #5]
	strb	ip, [sp, #6]
	mov	r3, #4
	b	.L47
.L60:
	.align	2
.L59:
	.word	.LANCHOR0
	.size	png_chunk_warning, .-png_chunk_warning
	.section	.text.png_error,"ax",%progbits
	.align	2
	.global	png_error
	.hidden	png_error
	.type	png_error, %function
png_error:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	subs	r4, r0, #0
	sub	sp, sp, #36
	mov	r5, r1
	beq	.L62
	ldr	r3, [r4, #136]
	tst	r3, #786432
	bne	.L155
.L63:
	ldr	r3, [r4, #92]
	cmp	r3, #0
	movne	r0, r4
	movne	r1, r5
	movne	lr, pc
	bxne	r3
.L62:
	ldrb	r3, [r5, #0]	@ zero_extendqisi2
	cmp	r3, #35
	beq	.L156
	ldr	ip, .L160
	mov	r2, r5
	ldr	r0, [ip, #0]
	ldr	r1, .L160+4
	bl	fprintf
.L108:
	cmp	r4, #0
	bne	.L157
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	bx	lr
.L155:
	ldrb	r2, [r1, #0]	@ zero_extendqisi2
	cmp	r2, #35
	beq	.L158
	tst	r3, #524288
	beq	.L63
	mov	r1, #48
	mov	r2, #0
	strb	r1, [sp, #16]
	strb	r2, [sp, #17]
	add	r5, sp, #16
	b	.L63
.L156:
	ldrb	lr, [r5, #1]	@ zero_extendqisi2
	strb	lr, [sp, #0]
	ldrb	r0, [r5, #0]	@ zero_extendqisi2
	cmp	r0, #32
	moveq	ip, #0
	beq	.L92
	ldrb	r1, [r5, #2]	@ zero_extendqisi2
	strb	r1, [sp, #1]
	ldrb	r2, [r5, #1]	@ zero_extendqisi2
	cmp	r2, #32
	moveq	ip, #1
	beq	.L92
	ldrb	r0, [r5, #3]	@ zero_extendqisi2
	strb	r0, [sp, #2]
	ldrb	r3, [r5, #2]	@ zero_extendqisi2
	cmp	r3, #32
	moveq	ip, #2
	beq	.L92
	ldrb	r2, [r5, #4]	@ zero_extendqisi2
	strb	r2, [sp, #3]
	ldrb	lr, [r5, #3]	@ zero_extendqisi2
	cmp	lr, #32
	moveq	ip, #3
	beq	.L92
	ldrb	r3, [r5, #5]	@ zero_extendqisi2
	strb	r3, [sp, #4]
	ldrb	r1, [r5, #4]	@ zero_extendqisi2
	cmp	r1, #32
	moveq	ip, #4
	beq	.L92
	ldrb	lr, [r5, #6]	@ zero_extendqisi2
	strb	lr, [sp, #5]
	ldrb	r0, [r5, #5]	@ zero_extendqisi2
	cmp	r0, #32
	moveq	ip, #5
	beq	.L92
	ldrb	r1, [r5, #7]	@ zero_extendqisi2
	strb	r1, [sp, #6]
	ldrb	r2, [r5, #6]	@ zero_extendqisi2
	cmp	r2, #32
	moveq	ip, #6
	beq	.L92
	ldrb	r0, [r5, #8]	@ zero_extendqisi2
	strb	r0, [sp, #7]
	ldrb	r3, [r5, #7]	@ zero_extendqisi2
	cmp	r3, #32
	moveq	ip, #7
	beq	.L92
	ldrb	r2, [r5, #9]	@ zero_extendqisi2
	strb	r2, [sp, #8]
	ldrb	lr, [r5, #8]	@ zero_extendqisi2
	cmp	lr, #32
	moveq	ip, #8
	beq	.L92
	ldrb	r3, [r5, #10]	@ zero_extendqisi2
	strb	r3, [sp, #9]
	ldrb	r1, [r5, #9]	@ zero_extendqisi2
	cmp	r1, #32
	moveq	ip, #9
	beq	.L92
	ldrb	lr, [r5, #11]	@ zero_extendqisi2
	strb	lr, [sp, #10]
	ldrb	r0, [r5, #10]	@ zero_extendqisi2
	cmp	r0, #32
	moveq	ip, #10
	beq	.L92
	ldrb	r1, [r5, #12]	@ zero_extendqisi2
	strb	r1, [sp, #11]
	ldrb	r2, [r5, #11]	@ zero_extendqisi2
	cmp	r2, #32
	moveq	ip, #11
	beq	.L92
	ldrb	r0, [r5, #13]	@ zero_extendqisi2
	strb	r0, [sp, #12]
	ldrb	r3, [r5, #12]	@ zero_extendqisi2
	cmp	r3, #32
	moveq	ip, #12
	beq	.L92
	ldrb	r2, [r5, #14]	@ zero_extendqisi2
	strb	r2, [sp, #13]
	ldrb	lr, [r5, #13]	@ zero_extendqisi2
	cmp	lr, #32
	moveq	ip, #13
	beq	.L92
	ldrb	r3, [r5, #15]	@ zero_extendqisi2
	strb	r3, [sp, #14]
	ldrb	r1, [r5, #14]	@ zero_extendqisi2
	cmp	r1, #32
	movne	ip, #15
	beq	.L159
.L107:
	ldr	lr, .L160
	mov	r2, r5
	ldr	r0, [lr, #0]
	mov	r3, ip
	ldr	r1, .L160+8
	bl	fprintf
	b	.L108
.L159:
	mov	ip, #14
.L92:
	sub	r0, ip, #2
	cmp	r0, #12
	bhi	.L107
	add	r2, sp, #32
	ldr	r1, .L160
	add	r3, ip, #1
	mov	lr, #0
	add	ip, r2, ip
	add	r3, r5, r3
	ldr	r0, [r1, #0]
	mov	r2, sp
	ldr	r1, .L160+12
	strb	lr, [ip, #-33]
	bl	fprintf
	b	.L108
.L158:
	ldrb	r2, [r1, #1]	@ zero_extendqisi2
	cmp	r2, #32
	moveq	r6, #1
	beq	.L66
	ldrb	r2, [r1, #2]	@ zero_extendqisi2
	cmp	r2, #32
	moveq	r6, #2
	beq	.L66
	ldrb	r2, [r1, #3]	@ zero_extendqisi2
	cmp	r2, #32
	moveq	r6, #3
	beq	.L66
	ldrb	r2, [r1, #4]	@ zero_extendqisi2
	cmp	r2, #32
	moveq	r6, #4
	beq	.L66
	ldrb	r2, [r1, #5]	@ zero_extendqisi2
	cmp	r2, #32
	moveq	r6, #5
	beq	.L66
	ldrb	r2, [r1, #6]	@ zero_extendqisi2
	cmp	r2, #32
	moveq	r6, #6
	beq	.L66
	ldrb	r2, [r1, #7]	@ zero_extendqisi2
	cmp	r2, #32
	moveq	r6, #7
	beq	.L66
	ldrb	r2, [r1, #8]	@ zero_extendqisi2
	cmp	r2, #32
	moveq	r6, #8
	beq	.L66
	ldrb	r2, [r1, #9]	@ zero_extendqisi2
	cmp	r2, #32
	moveq	r6, #9
	beq	.L66
	ldrb	r2, [r1, #10]	@ zero_extendqisi2
	cmp	r2, #32
	moveq	r6, #10
	beq	.L66
	ldrb	r2, [r1, #11]	@ zero_extendqisi2
	cmp	r2, #32
	moveq	r6, #11
	beq	.L66
	ldrb	r2, [r1, #12]	@ zero_extendqisi2
	cmp	r2, #32
	moveq	r6, #12
	beq	.L66
	ldrb	r2, [r1, #13]	@ zero_extendqisi2
	cmp	r2, #32
	moveq	r6, #13
	beq	.L66
	ldrb	r6, [r1, #14]	@ zero_extendqisi2
	cmp	r6, #32
	moveq	r6, #14
	movne	r6, #15
.L66:
	tst	r3, #524288
	addeq	r5, r5, r6
	beq	.L63
	subs	lr, r6, #1
	mvneq	r6, #0
	addeq	r2, sp, #16
	beq	.L82
	add	r0, r5, #1
	add	r2, sp, #16
	tst	r0, #3
	add	r3, r2, #4
	add	ip, r5, #5
	movne	r1, #0
	moveq	r1, #1
	cmp	r3, r0
	cmpcs	ip, r2
	movcs	r3, #0
	movcc	r3, #1
	cmp	lr, #3
	movls	r1, #0
	andhi	r1, r1, #1
	ands	r1, r1, r3
	beq	.L83
	mov	r7, lr, lsr #2
	movs	r8, r7, asl #2
	beq	.L85
	ldr	r1, [r5, #1]
	mov	r3, #1
	sub	r0, r7, #1
	cmp	r3, r7
	str	r1, [r2, #0]
	and	r0, r0, #3
	add	r1, r5, #4
	bcs	.L154
	cmp	r0, #0
	beq	.L86
	cmp	r0, #1
	beq	.L152
	cmp	r0, #2
	ldrne	r3, [r1, #1]
	strne	r3, [r2, #4]
	addne	r1, r1, #4
	ldr	r0, [r1, #1]
	movne	r3, #2
	str	r0, [r2, r3, asl #2]
	add	r1, r1, #4
	add	r3, r3, #1
.L152:
	ldr	r0, [r1, #1]
	str	r0, [r2, r3, asl #2]
	add	r3, r3, #1
	cmp	r3, r7
	add	r1, r1, #4
	bcs	.L154
.L86:
	ldr	sl, [r1, #1]
	str	sl, [r2, r3, asl #2]
	add	ip, r1, #4
	ldr	sl, [ip, #1]
	add	r0, r3, #1
	str	sl, [r2, r0, asl #2]
	ldr	ip, [ip, #5]
	add	r0, r0, #1
	str	ip, [r2, r0, asl #2]
	add	r0, r3, #3
	ldr	ip, [r1, #13]
	add	r3, r3, #4
	cmp	r3, r7
	str	ip, [r2, r0, asl #2]
	add	r1, r1, #16
	bcc	.L86
.L154:
	cmp	lr, r8
	beq	.L87
.L85:
	add	r5, r5, r8
	mvn	r3, r8
	add	r1, r8, #1
	ldrb	ip, [r5, #1]!	@ zero_extendqisi2
	add	r0, r3, lr
	cmp	lr, r1
	add	r3, r2, r8
	strb	ip, [r3], #1
	and	r0, r0, #3
	ble	.L87
	cmp	r0, #0
	beq	.L88
	cmp	r0, #1
	beq	.L150
	cmp	r0, #2
	ldrneb	r0, [r5, #1]!	@ zero_extendqisi2
	strneb	r0, [r3], #1
	ldrb	r0, [r5, #1]!	@ zero_extendqisi2
	addne	r1, r1, #1
	strb	r0, [r3], #1
	add	r1, r1, #1
.L150:
	add	r1, r1, #1
	ldrb	r0, [r5, #1]!	@ zero_extendqisi2
	cmp	lr, r1
	strb	r0, [r3], #1
	ble	.L87
.L88:
	mov	ip, r5
	ldrb	r7, [ip, #1]!	@ zero_extendqisi2
	mov	r0, r3
	strb	r7, [r0], #1
	ldrb	r7, [ip, #1]!	@ zero_extendqisi2
	strb	r7, [r3, #1]
	ldrb	ip, [ip, #1]	@ zero_extendqisi2
	strb	ip, [r0, #1]
	add	r1, r1, #4
	ldrb	r0, [r5, #4]	@ zero_extendqisi2
	cmp	lr, r1
	strb	r0, [r3, #3]
	add	r5, r5, #4
	add	r3, r3, #4
	bgt	.L88
.L87:
	sub	r6, r6, #2
.L82:
	add	lr, sp, #32
	mov	r5, #0
	add	r6, lr, r6
	strb	r5, [r6, #-16]
	mov	r5, r2
	b	.L63
.L83:
	ldrb	ip, [r5, #1]	@ zero_extendqisi2
	mov	r3, #1
	sub	r0, lr, #1
	cmp	lr, r3
	strb	ip, [r2, r1]
	and	r0, r0, #3
	mov	r1, #2
	ble	.L87
	cmp	r0, #0
	beq	.L89
	cmp	r0, #1
	beq	.L148
	cmp	r0, #2
	ldrneb	r0, [r5, #2]	@ zero_extendqisi2
	strneb	r0, [r2, r3]
	movne	r3, r1
	movne	r1, #3
	ldrb	r0, [r5, r1]	@ zero_extendqisi2
	add	r1, r1, #1
	strb	r0, [r2, r3]
	add	r3, r3, #1
.L148:
	ldrb	r0, [r5, r1]	@ zero_extendqisi2
	strb	r0, [r2, r3]
	add	r3, r3, #1
	cmp	lr, r3
	add	r1, r1, #1
	ble	.L87
.L89:
	ldrb	r7, [r5, r1]	@ zero_extendqisi2
	strb	r7, [r2, r3]
	add	ip, r1, #1
	ldrb	r7, [r5, ip]	@ zero_extendqisi2
	add	r0, r3, #1
	strb	r7, [r2, r0]
	add	ip, ip, #1
	ldrb	ip, [r5, ip]	@ zero_extendqisi2
	add	r0, r0, #1
	strb	ip, [r2, r0]
	add	r0, r1, #3
	ldrb	ip, [r5, r0]	@ zero_extendqisi2
	add	r0, r3, #3
	add	r3, r3, #4
	cmp	lr, r3
	strb	ip, [r2, r0]
	add	r1, r1, #4
	bgt	.L89
	sub	r6, r6, #2
	b	.L82
.L157:
	mov	r0, r4
	mov	r1, #1
	bl	longjmp
.L161:
	.align	2
.L160:
	.word	__aeabi_stderr
	.word	.LC4
	.word	.LC3
	.word	.LC2
	.size	png_error, .-png_error
	.section	.text.png_chunk_error,"ax",%progbits
	.align	2
	.global	png_chunk_error
	.hidden	png_chunk_error
	.type	png_chunk_error, %function
png_chunk_error:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 88
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	subs	r5, r0, #0
	sub	sp, sp, #92
	mov	r2, r1
	beq	.L176
	ldrb	r3, [r5, #312]	@ zero_extendqisi2
	sub	r4, r3, #65
	cmp	r4, #57
	movls	r1, #0
	movhi	r1, #1
	sub	r0, r3, #91
	cmp	r0, #5
	orrls	r1, r1, #1
	cmp	r1, #0
	streqb	r3, [sp, #4]
	moveq	r3, #1
	bne	.L177
.L166:
	ldrb	r1, [r5, #313]	@ zero_extendqisi2
	sub	r4, r1, #65
	cmp	r4, #57
	movls	lr, #0
	movhi	lr, #1
	sub	ip, r1, #91
	cmp	ip, #5
	orrls	lr, lr, #1
	cmp	lr, #0
	bne	.L167
	add	r4, sp, #4
	strb	r1, [r4, r3]
	add	r3, r3, #1
.L168:
	ldrb	r1, [r5, #314]	@ zero_extendqisi2
	sub	lr, r1, #65
	cmp	lr, #57
	movls	r0, #0
	movhi	r0, #1
	sub	ip, r1, #91
	cmp	ip, #5
	orrls	r0, r0, #1
	cmp	r0, #0
	streqb	r1, [r4, r3]
	addeq	r3, r3, #1
	beq	.L170
	ldr	r7, .L178
	add	ip, r3, #1
	and	r0, r1, #15
	ldrb	r6, [r7, r0]	@ zero_extendqisi2
	ldrb	lr, [r7, r1, lsr #4]	@ zero_extendqisi2
	add	r0, ip, #1
	mov	r7, #91
	strb	r7, [r4, r3]
	add	r1, r0, #1
	mov	r3, #93
	strb	lr, [r4, ip]
	strb	r6, [r4, r0]
	strb	r3, [r4, r1]
	add	r3, r1, #1
.L170:
	ldrb	r1, [r5, #315]	@ zero_extendqisi2
	sub	r0, r1, #65
	cmp	r0, #57
	movls	lr, #0
	movhi	lr, #1
	sub	ip, r1, #91
	cmp	ip, #5
	orrls	lr, lr, #1
	cmp	lr, #0
	streqb	r1, [r4, r3]
	addeq	r3, r3, #1
	beq	.L172
	ldr	r7, .L178
	add	ip, r3, #1
	and	r0, r1, #15
	ldrb	r6, [r7, r0]	@ zero_extendqisi2
	ldrb	lr, [r7, r1, lsr #4]	@ zero_extendqisi2
	add	r0, ip, #1
	mov	r7, #91
	strb	r7, [r4, r3]
	add	r1, r0, #1
	mov	r3, #93
	strb	lr, [r4, ip]
	strb	r6, [r4, r0]
	strb	r3, [r4, r1]
	add	r3, r1, #1
.L172:
	cmp	r2, #0
	streqb	r2, [r4, r3]
	beq	.L174
	add	ip, r3, #1
	add	r6, ip, #1
	mov	lr, #58
	strb	lr, [r4, r3]
	add	r6, r4, r6
	mov	r3, #32
	mov	r1, r2
	mov	r0, r6
	mov	r2, #64
	strb	r3, [r4, ip]
	bl	memcpy
	mov	r2, #0
	strb	r2, [r6, #63]
.L174:
	mov	r0, r5
	mov	r1, r4
.L176:
	bl	png_error
	add	sp, sp, #92
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L167:
	ldr	r7, .L178
	add	ip, r3, #1
	and	r4, r1, #15
	ldrb	r6, [r7, r4]	@ zero_extendqisi2
	ldrb	lr, [r7, r1, lsr #4]	@ zero_extendqisi2
	add	r4, sp, #4
	add	r0, ip, #1
	mov	r7, #91
	strb	r7, [r4, r3]
	add	r1, r0, #1
	mov	r3, #93
	strb	lr, [r4, ip]
	strb	r6, [r4, r0]
	strb	r3, [r4, r1]
	add	r3, r1, #1
	b	.L168
.L177:
	ldr	r4, .L178
	and	r0, r3, #15
	ldrb	lr, [r4, r3, lsr #4]	@ zero_extendqisi2
	ldrb	ip, [r4, r0]	@ zero_extendqisi2
	mov	r3, #93
	mov	r1, #91
	strb	r3, [sp, #7]
	strb	r1, [sp, #4]
	strb	lr, [sp, #5]
	strb	ip, [sp, #6]
	mov	r3, #4
	b	.L166
.L179:
	.align	2
.L178:
	.word	.LANCHOR0
	.size	png_chunk_error, .-png_chunk_error
	.section	.rodata
	.align	2
	.set	.LANCHOR0,. + 0
	.type	png_digit, %object
	.size	png_digit, 16
png_digit:
	.byte	48
	.byte	49
	.byte	50
	.byte	51
	.byte	52
	.byte	53
	.byte	54
	.byte	55
	.byte	56
	.byte	57
	.byte	65
	.byte	66
	.byte	67
	.byte	68
	.byte	69
	.byte	70
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"libpng warning no. %s: %s\012\000"
	.space	1
.LC1:
	.ascii	"libpng warning: %s\012\000"
.LC2:
	.ascii	"libpng error no. %s: %s\012\000"
	.space	3
.LC3:
	.ascii	"libpng error: %s, offset=%d\012\000"
	.space	3
.LC4:
	.ascii	"libpng error: %s\012\000"
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
