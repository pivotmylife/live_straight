	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"IwSoundInst.cpp"
	.section	.text._ZN12CIwSoundInstC2Ev,"ax",%progbits
	.align	2
	.global	_ZN12CIwSoundInstC2Ev
	.hidden	_ZN12CIwSoundInstC2Ev
	.type	_ZN12CIwSoundInstC2Ev, %function
_ZN12CIwSoundInstC2Ev:
	.fnstart
.LFB1370:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r2, #0
	mov	ip, #4096
	mov	r1, #4	@ movhi
	strh	r2, [r0, #16]	@ movhi
	strh	ip, [r0, #8]	@ movhi
	str	r2, [r0, #0]
	strh	ip, [r0, #4]	@ movhi
	strh	r2, [r0, #6]	@ movhi
	strh	r1, [r0, #10]	@ movhi
	str	r2, [r0, #20]
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN12CIwSoundInstC2Ev, .-_ZN12CIwSoundInstC2Ev
	.section	.text._ZN12CIwSoundInstC1Ev,"ax",%progbits
	.align	2
	.global	_ZN12CIwSoundInstC1Ev
	.hidden	_ZN12CIwSoundInstC1Ev
	.type	_ZN12CIwSoundInstC1Ev, %function
_ZN12CIwSoundInstC1Ev:
	.fnstart
.LFB1371:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r2, #0
	mov	ip, #4096
	mov	r1, #4	@ movhi
	strh	r2, [r0, #16]	@ movhi
	strh	ip, [r0, #8]	@ movhi
	str	r2, [r0, #0]
	strh	ip, [r0, #4]	@ movhi
	strh	r2, [r0, #6]	@ movhi
	strh	r1, [r0, #10]	@ movhi
	str	r2, [r0, #20]
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN12CIwSoundInstC1Ev, .-_ZN12CIwSoundInstC1Ev
	.section	.text._ZN12CIwSoundInst6SetVolEs,"ax",%progbits
	.align	2
	.global	_ZN12CIwSoundInst6SetVolEs
	.hidden	_ZN12CIwSoundInst6SetVolEs
	.type	_ZN12CIwSoundInst6SetVolEs, %function
_ZN12CIwSoundInst6SetVolEs:
	.fnstart
.LFB1376:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldrh	r2, [r0, #10]
	orr	r3, r2, #1
	strh	r3, [r0, #10]	@ movhi
	strh	r1, [r0, #4]	@ movhi
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN12CIwSoundInst6SetVolEs, .-_ZN12CIwSoundInst6SetVolEs
	.section	.text._ZN12CIwSoundInst6SetPanEs,"ax",%progbits
	.align	2
	.global	_ZN12CIwSoundInst6SetPanEs
	.hidden	_ZN12CIwSoundInst6SetPanEs
	.type	_ZN12CIwSoundInst6SetPanEs, %function
_ZN12CIwSoundInst6SetPanEs:
	.fnstart
.LFB1377:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldrh	r2, [r0, #10]
	orr	r3, r2, #1
	strh	r3, [r0, #10]	@ movhi
	strh	r1, [r0, #6]	@ movhi
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN12CIwSoundInst6SetPanEs, .-_ZN12CIwSoundInst6SetPanEs
	.section	.text._ZN12CIwSoundInst8SetPitchEs,"ax",%progbits
	.align	2
	.global	_ZN12CIwSoundInst8SetPitchEs
	.hidden	_ZN12CIwSoundInst8SetPitchEs
	.type	_ZN12CIwSoundInst8SetPitchEs, %function
_ZN12CIwSoundInst8SetPitchEs:
	.fnstart
.LFB1378:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldrh	r2, [r0, #10]
	orr	r3, r2, #1
	strh	r3, [r0, #10]	@ movhi
	strh	r1, [r0, #8]	@ movhi
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN12CIwSoundInst8SetPitchEs, .-_ZN12CIwSoundInst8SetPitchEs
	.section	.text._ZNK12CIwSoundInst9IsPlayingEv,"ax",%progbits
	.align	2
	.global	_ZNK12CIwSoundInst9IsPlayingEv
	.hidden	_ZNK12CIwSoundInst9IsPlayingEv
	.type	_ZNK12CIwSoundInst9IsPlayingEv, %function
_ZNK12CIwSoundInst9IsPlayingEv:
	.fnstart
.LFB1375:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	ldr	r2, .L19
	ldr	r1, [r2, #0]
	ldrh	r3, [r1, #30]
	tst	r3, #2
	mov	r4, r0
	bne	.L18
.L14:
	mov	r0, #0
.L15:
	ldmfd	sp!, {r4, lr}
	bx	lr
.L18:
	ldrh	r0, [r0, #12]
	mov	r1, #4
	bl	s3eSoundChannelGetInt
	cmp	r0, #1
	bne	.L14
	ldrh	r0, [r4, #12]
	mov	r1, #5
	bl	s3eSoundChannelGetInt
	subs	r0, r0, #1
	movne	r0, #1
	b	.L15
.L20:
	.align	2
.L19:
	.word	g_IwSoundManager
	.fnend
	.size	_ZNK12CIwSoundInst9IsPlayingEv, .-_ZNK12CIwSoundInst9IsPlayingEv
	.section	.text._ZN12CIwSoundInst6ResumeEv,"ax",%progbits
	.align	2
	.global	_ZN12CIwSoundInst6ResumeEv
	.hidden	_ZN12CIwSoundInst6ResumeEv
	.type	_ZN12CIwSoundInst6ResumeEv, %function
_ZN12CIwSoundInst6ResumeEv:
	.fnstart
.LFB1374:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, lr}
	.save {r3, lr}
	ldr	r2, .L24
	ldr	r1, [r2, #0]
	ldrh	r3, [r1, #30]
	tst	r3, #2
	ldrneh	r0, [r0, #12]
	blne	s3eSoundChannelResume
.L23:
	ldmfd	sp!, {r3, lr}
	bx	lr
.L25:
	.align	2
.L24:
	.word	g_IwSoundManager
	.fnend
	.size	_ZN12CIwSoundInst6ResumeEv, .-_ZN12CIwSoundInst6ResumeEv
	.section	.text._ZN12CIwSoundInst5PauseEv,"ax",%progbits
	.align	2
	.global	_ZN12CIwSoundInst5PauseEv
	.hidden	_ZN12CIwSoundInst5PauseEv
	.type	_ZN12CIwSoundInst5PauseEv, %function
_ZN12CIwSoundInst5PauseEv:
	.fnstart
.LFB1373:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, lr}
	.save {r3, lr}
	ldr	r2, .L29
	ldr	r1, [r2, #0]
	ldrh	r3, [r1, #30]
	tst	r3, #2
	ldrneh	r0, [r0, #12]
	blne	s3eSoundChannelPause
.L28:
	ldmfd	sp!, {r3, lr}
	bx	lr
.L30:
	.align	2
.L29:
	.word	g_IwSoundManager
	.fnend
	.size	_ZN12CIwSoundInst5PauseEv, .-_ZN12CIwSoundInst5PauseEv
	.section	.text._ZN12CIwSoundInst4StopEv,"ax",%progbits
	.align	2
	.global	_ZN12CIwSoundInst4StopEv
	.hidden	_ZN12CIwSoundInst4StopEv
	.type	_ZN12CIwSoundInst4StopEv, %function
_ZN12CIwSoundInst4StopEv:
	.fnstart
.LFB1372:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	ldr	r2, .L34
	ldr	r1, [r2, #0]
	ldrh	r3, [r1, #30]
	tst	r3, #2
	mov	r4, r0
	beq	.L33
	ldrh	r0, [r0, #12]
	bl	s3eSoundChannelStop
	ldrh	ip, [r4, #10]
	orr	r0, ip, #2
	strh	r0, [r4, #10]	@ movhi
.L33:
	ldmfd	sp!, {r4, lr}
	bx	lr
.L35:
	.align	2
.L34:
	.word	g_IwSoundManager
	.fnend
	.size	_ZN12CIwSoundInst4StopEv, .-_ZN12CIwSoundInst4StopEv
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
