	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"pngget.c"
	.section	.text.png_get_valid,"ax",%progbits
	.align	2
	.global	png_get_valid
	.hidden	png_get_valid
	.type	png_get_valid, %function
png_get_valid:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	moveq	r0, #0
	movne	r0, #1
	ldrne	r0, [r1, #8]
	andne	r0, r2, r0
	bx	lr
	.size	png_get_valid, .-png_get_valid
	.section	.text.png_get_rowbytes,"ax",%progbits
	.align	2
	.global	png_get_rowbytes
	.hidden	png_get_rowbytes
	.type	png_get_rowbytes, %function
png_get_rowbytes:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	moveq	r0, #0
	movne	r0, #1
	ldrne	r0, [r1, #12]
	bx	lr
	.size	png_get_rowbytes, .-png_get_rowbytes
	.section	.text.png_get_rows,"ax",%progbits
	.align	2
	.global	png_get_rows
	.hidden	png_get_rows
	.type	png_get_rows, %function
png_get_rows:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	moveq	r0, #0
	movne	r0, #1
	ldrne	r0, [r1, #248]
	bx	lr
	.size	png_get_rows, .-png_get_rows
	.section	.text.png_get_image_width,"ax",%progbits
	.align	2
	.global	png_get_image_width
	.hidden	png_get_image_width
	.type	png_get_image_width, %function
png_get_image_width:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	moveq	r0, #0
	movne	r0, #1
	ldrne	r0, [r1, #0]
	bx	lr
	.size	png_get_image_width, .-png_get_image_width
	.section	.text.png_get_image_height,"ax",%progbits
	.align	2
	.global	png_get_image_height
	.hidden	png_get_image_height
	.type	png_get_image_height, %function
png_get_image_height:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	moveq	r0, #0
	movne	r0, #1
	ldrne	r0, [r1, #4]
	bx	lr
	.size	png_get_image_height, .-png_get_image_height
	.section	.text.png_get_bit_depth,"ax",%progbits
	.align	2
	.global	png_get_bit_depth
	.hidden	png_get_bit_depth
	.type	png_get_bit_depth, %function
png_get_bit_depth:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	moveq	r0, #0
	movne	r0, #1
	ldrneb	r0, [r1, #24]	@ zero_extendqisi2
	bx	lr
	.size	png_get_bit_depth, .-png_get_bit_depth
	.section	.text.png_get_color_type,"ax",%progbits
	.align	2
	.global	png_get_color_type
	.hidden	png_get_color_type
	.type	png_get_color_type, %function
png_get_color_type:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	moveq	r0, #0
	movne	r0, #1
	ldrneb	r0, [r1, #25]	@ zero_extendqisi2
	bx	lr
	.size	png_get_color_type, .-png_get_color_type
	.section	.text.png_get_filter_type,"ax",%progbits
	.align	2
	.global	png_get_filter_type
	.hidden	png_get_filter_type
	.type	png_get_filter_type, %function
png_get_filter_type:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	moveq	r0, #0
	movne	r0, #1
	ldrneb	r0, [r1, #27]	@ zero_extendqisi2
	bx	lr
	.size	png_get_filter_type, .-png_get_filter_type
	.section	.text.png_get_interlace_type,"ax",%progbits
	.align	2
	.global	png_get_interlace_type
	.hidden	png_get_interlace_type
	.type	png_get_interlace_type, %function
png_get_interlace_type:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	moveq	r0, #0
	movne	r0, #1
	ldrneb	r0, [r1, #28]	@ zero_extendqisi2
	bx	lr
	.size	png_get_interlace_type, .-png_get_interlace_type
	.section	.text.png_get_compression_type,"ax",%progbits
	.align	2
	.global	png_get_compression_type
	.hidden	png_get_compression_type
	.type	png_get_compression_type, %function
png_get_compression_type:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	moveq	r0, #0
	movne	r0, #1
	ldrneb	r0, [r1, #26]	@ zero_extendqisi2
	bx	lr
	.size	png_get_compression_type, .-png_get_compression_type
	.section	.text.png_get_x_pixels_per_meter,"ax",%progbits
	.align	2
	.global	png_get_x_pixels_per_meter
	.hidden	png_get_x_pixels_per_meter
	.type	png_get_x_pixels_per_meter, %function
png_get_x_pixels_per_meter:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	beq	.L42
	ldr	r3, [r1, #8]
	tst	r3, #128
	beq	.L42
	ldrb	r2, [r1, #120]	@ zero_extendqisi2
	cmp	r2, #1
	ldreq	r0, [r1, #112]
	bxeq	lr
.L42:
	mov	r0, #0
	bx	lr
	.size	png_get_x_pixels_per_meter, .-png_get_x_pixels_per_meter
	.section	.text.png_get_y_pixels_per_meter,"ax",%progbits
	.align	2
	.global	png_get_y_pixels_per_meter
	.hidden	png_get_y_pixels_per_meter
	.type	png_get_y_pixels_per_meter, %function
png_get_y_pixels_per_meter:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	beq	.L47
	ldr	r3, [r1, #8]
	tst	r3, #128
	beq	.L47
	ldrb	r2, [r1, #120]	@ zero_extendqisi2
	cmp	r2, #1
	ldreq	r0, [r1, #116]
	bxeq	lr
.L47:
	mov	r0, #0
	bx	lr
	.size	png_get_y_pixels_per_meter, .-png_get_y_pixels_per_meter
	.section	.text.png_get_pixels_per_meter,"ax",%progbits
	.align	2
	.global	png_get_pixels_per_meter
	.hidden	png_get_pixels_per_meter
	.type	png_get_pixels_per_meter, %function
png_get_pixels_per_meter:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	beq	.L51
	ldr	r3, [r1, #8]
	tst	r3, #128
	beq	.L51
	ldrb	r0, [r1, #120]	@ zero_extendqisi2
	cmp	r0, #1
	beq	.L54
.L51:
	mov	r0, #0
	bx	lr
.L54:
	add	r0, r1, #112
	ldmia	r0, {r0, r2}	@ phole ldm
	cmp	r0, r2
	bne	.L51
	bx	lr
	.size	png_get_pixels_per_meter, .-png_get_pixels_per_meter
	.global	__aeabi_ui2f
	.global	__aeabi_fdiv
	.section	.text.png_get_pixel_aspect_ratio,"ax",%progbits
	.align	2
	.global	png_get_pixel_aspect_ratio
	.hidden	png_get_pixel_aspect_ratio
	.type	png_get_pixel_aspect_ratio, %function
png_get_pixel_aspect_ratio:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #0
	cmpne	r0, #0
	stmfd	sp!, {r3, r4, r5, lr}
	beq	.L56
	ldr	r3, [r1, #8]
	tst	r3, #128
	beq	.L56
	ldr	r4, [r1, #112]
	cmp	r4, #0
	beq	.L56
	ldr	r0, [r1, #116]
	bl	__aeabi_ui2f
	mov	r5, r0
	mov	r0, r4
	bl	__aeabi_ui2f
	mov	r1, r0
	mov	r0, r5
	bl	__aeabi_fdiv
	b	.L58
.L56:
	mov	r0, #0
.L58:
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
	.size	png_get_pixel_aspect_ratio, .-png_get_pixel_aspect_ratio
	.section	.text.png_get_x_offset_microns,"ax",%progbits
	.align	2
	.global	png_get_x_offset_microns
	.hidden	png_get_x_offset_microns
	.type	png_get_x_offset_microns, %function
png_get_x_offset_microns:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	beq	.L61
	ldr	r3, [r1, #8]
	tst	r3, #256
	beq	.L61
	ldrb	r2, [r1, #108]	@ zero_extendqisi2
	cmp	r2, #1
	ldreq	r0, [r1, #100]
	bxeq	lr
.L61:
	mov	r0, #0
	bx	lr
	.size	png_get_x_offset_microns, .-png_get_x_offset_microns
	.section	.text.png_get_y_offset_microns,"ax",%progbits
	.align	2
	.global	png_get_y_offset_microns
	.hidden	png_get_y_offset_microns
	.type	png_get_y_offset_microns, %function
png_get_y_offset_microns:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	beq	.L65
	ldr	r3, [r1, #8]
	tst	r3, #256
	beq	.L65
	ldrb	r2, [r1, #108]	@ zero_extendqisi2
	cmp	r2, #1
	ldreq	r0, [r1, #104]
	bxeq	lr
.L65:
	mov	r0, #0
	bx	lr
	.size	png_get_y_offset_microns, .-png_get_y_offset_microns
	.section	.text.png_get_x_offset_pixels,"ax",%progbits
	.align	2
	.global	png_get_x_offset_pixels
	.hidden	png_get_x_offset_pixels
	.type	png_get_x_offset_pixels, %function
png_get_x_offset_pixels:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	beq	.L69
	ldr	r3, [r1, #8]
	tst	r3, #256
	beq	.L69
	ldrb	r2, [r1, #108]	@ zero_extendqisi2
	cmp	r2, #0
	ldreq	r0, [r1, #100]
	bxeq	lr
.L69:
	mov	r0, #0
	bx	lr
	.size	png_get_x_offset_pixels, .-png_get_x_offset_pixels
	.section	.text.png_get_y_offset_pixels,"ax",%progbits
	.align	2
	.global	png_get_y_offset_pixels
	.hidden	png_get_y_offset_pixels
	.type	png_get_y_offset_pixels, %function
png_get_y_offset_pixels:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	beq	.L73
	ldr	r3, [r1, #8]
	tst	r3, #256
	beq	.L73
	ldrb	r2, [r1, #108]	@ zero_extendqisi2
	cmp	r2, #0
	ldreq	r0, [r1, #104]
	bxeq	lr
.L73:
	mov	r0, #0
	bx	lr
	.size	png_get_y_offset_pixels, .-png_get_y_offset_pixels
	.section	.text.png_get_channels,"ax",%progbits
	.align	2
	.global	png_get_channels
	.hidden	png_get_channels
	.type	png_get_channels, %function
png_get_channels:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	moveq	r0, #0
	movne	r0, #1
	ldrneb	r0, [r1, #29]	@ zero_extendqisi2
	bx	lr
	.size	png_get_channels, .-png_get_channels
	.section	.text.png_get_signature,"ax",%progbits
	.align	2
	.global	png_get_signature
	.hidden	png_get_signature
	.type	png_get_signature, %function
png_get_signature:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	moveq	r0, #0
	movne	r0, #1
	addne	r0, r1, #32
	bx	lr
	.size	png_get_signature, .-png_get_signature
	.section	.text.png_get_bKGD,"ax",%progbits
	.align	2
	.global	png_get_bKGD
	.hidden	png_get_bKGD
	.type	png_get_bKGD, %function
png_get_bKGD:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	beq	.L85
	ldr	ip, [r1, #8]
	mov	r3, ip, lsr #5
	cmp	r2, #0
	moveq	r3, #0
	andne	r3, r3, #1
	cmp	r3, #0
	addne	r1, r1, #90
	strne	r1, [r2, #0]
	movne	r0, #32
	bxne	lr
.L85:
	mov	r0, #0
	bx	lr
	.size	png_get_bKGD, .-png_get_bKGD
	.global	__aeabi_f2d
	.section	.text.png_get_cHRM,"ax",%progbits
	.align	2
	.global	png_get_cHRM
	.hidden	png_get_cHRM
	.type	png_get_cHRM, %function
png_get_cHRM:
	@ Function supports interworking.
	@ args = 24, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, fp, lr}
	cmp	r1, #0
	cmpne	r0, #0
	mov	r4, r1
	mov	r5, r2
	mov	fp, r3
	add	r9, sp, #40
	ldmia	r9, {r9, sl}	@ phole ldm
	ldr	r8, [sp, #48]
	ldr	r7, [sp, #52]
	ldr	r6, [sp, #60]
	beq	.L89
	ldr	r3, [r1, #8]
	tst	r3, #4
	beq	.L89
	cmp	r2, #0
	beq	.L90
	ldr	r0, [r1, #128]	@ float
	bl	__aeabi_f2d
	stmia	r5, {r0-r1}
.L90:
	cmp	fp, #0
	beq	.L91
	ldr	r0, [r4, #132]	@ float
	bl	__aeabi_f2d
	stmia	fp, {r0-r1}
.L91:
	cmp	r9, #0
	beq	.L92
	ldr	r0, [r4, #136]	@ float
	bl	__aeabi_f2d
	stmia	r9, {r0-r1}
.L92:
	cmp	sl, #0
	beq	.L93
	ldr	r0, [r4, #140]	@ float
	bl	__aeabi_f2d
	stmia	sl, {r0-r1}
.L93:
	cmp	r8, #0
	beq	.L94
	ldr	r0, [r4, #144]	@ float
	bl	__aeabi_f2d
	stmia	r8, {r0-r1}
.L94:
	cmp	r7, #0
	beq	.L95
	ldr	r0, [r4, #148]	@ float
	bl	__aeabi_f2d
	stmia	r7, {r0-r1}
.L95:
	ldr	r0, [sp, #56]
	cmp	r0, #0
	beq	.L96
	ldr	r0, [r4, #152]	@ float
	bl	__aeabi_f2d
	ldr	r2, [sp, #56]
	stmia	r2, {r0-r1}
.L96:
	cmp	r6, #0
	beq	.L100
	ldr	r0, [r4, #156]	@ float
	bl	__aeabi_f2d
	stmia	r6, {r0-r1}
.L100:
	mov	r0, #4
	b	.L98
.L89:
	mov	r0, #0
.L98:
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
	.size	png_get_cHRM, .-png_get_cHRM
	.section	.text.png_get_cHRM_fixed,"ax",%progbits
	.align	2
	.global	png_get_cHRM_fixed
	.hidden	png_get_cHRM_fixed
	.type	png_get_cHRM_fixed, %function
png_get_cHRM_fixed:
	@ Function supports interworking.
	@ args = 24, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, r5, r6, r7, r8}
	cmp	r1, #0
	cmpne	r0, #0
	ldr	r7, [sp, #20]
	ldr	r6, [sp, #24]
	ldr	r5, [sp, #28]
	add	r4, sp, #32
	ldmia	r4, {r4, ip}	@ phole ldm
	ldr	r0, [sp, #40]
	beq	.L102
	ldr	r8, [r1, #8]
	tst	r8, #4
	beq	.L102
	cmp	r2, #0
	ldrne	r8, [r1, #256]
	strne	r8, [r2, #0]
	cmp	r3, #0
	ldrne	r2, [r1, #260]
	strne	r2, [r3, #0]
	cmp	r7, #0
	ldrne	r3, [r1, #264]
	strne	r3, [r7, #0]
	cmp	r6, #0
	ldrne	r3, [r1, #268]
	strne	r3, [r6, #0]
	cmp	r5, #0
	ldrne	r3, [r1, #272]
	strne	r3, [r5, #0]
	cmp	r4, #0
	ldrne	r3, [r1, #276]
	strne	r3, [r4, #0]
	cmp	ip, #0
	ldrne	r3, [r1, #280]
	strne	r3, [ip, #0]
	cmp	r0, #0
	ldrne	r3, [r1, #284]
	strne	r3, [r0, #0]
	mov	r0, #4
	b	.L111
.L102:
	mov	r0, #0
.L111:
	ldmfd	sp!, {r4, r5, r6, r7, r8}
	bx	lr
	.size	png_get_cHRM_fixed, .-png_get_cHRM_fixed
	.section	.text.png_get_gAMA,"ax",%progbits
	.align	2
	.global	png_get_gAMA
	.hidden	png_get_gAMA
	.type	png_get_gAMA, %function
png_get_gAMA:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #0
	cmpne	r0, #0
	stmfd	sp!, {r4, lr}
	mov	r4, r2
	beq	.L115
	ldr	r3, [r1, #8]
	cmp	r2, #0
	moveq	r3, #0
	andne	r3, r3, #1
	cmp	r3, #0
	beq	.L115
	ldr	r0, [r1, #40]	@ float
	bl	__aeabi_f2d
	stmia	r4, {r0-r1}
	mov	r0, #1
	b	.L116
.L115:
	mov	r0, #0
.L116:
	ldmfd	sp!, {r4, lr}
	bx	lr
	.size	png_get_gAMA, .-png_get_gAMA
	.section	.text.png_get_gAMA_fixed,"ax",%progbits
	.align	2
	.global	png_get_gAMA_fixed
	.hidden	png_get_gAMA_fixed
	.type	png_get_gAMA_fixed, %function
png_get_gAMA_fixed:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	beq	.L119
	ldr	r3, [r1, #8]
	cmp	r2, #0
	moveq	r3, #0
	andne	r3, r3, #1
	cmp	r3, #0
	beq	.L119
	ldr	r0, [r1, #252]
	str	r0, [r2, #0]
	mov	r0, #1
	bx	lr
.L119:
	mov	r0, #0
	bx	lr
	.size	png_get_gAMA_fixed, .-png_get_gAMA_fixed
	.section	.text.png_get_sRGB,"ax",%progbits
	.align	2
	.global	png_get_sRGB
	.hidden	png_get_sRGB
	.type	png_get_sRGB, %function
png_get_sRGB:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	beq	.L123
	ldr	r0, [r1, #8]
	mov	r3, r0, lsr #11
	cmp	r2, #0
	moveq	r3, #0
	andne	r3, r3, #1
	cmp	r3, #0
	beq	.L123
	ldrb	ip, [r1, #44]	@ zero_extendqisi2
	mov	r0, #2048
	str	ip, [r2, #0]
	bx	lr
.L123:
	mov	r0, #0
	bx	lr
	.size	png_get_sRGB, .-png_get_sRGB
	.section	.text.png_get_iCCP,"ax",%progbits
	.align	2
	.global	png_get_iCCP
	.hidden	png_get_iCCP
	.type	png_get_iCCP, %function
png_get_iCCP:
	@ Function supports interworking.
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	str	r4, [sp, #-4]!
	cmp	r1, #0
	cmpne	r0, #0
	ldr	ip, [sp, #4]
	ldr	r0, [sp, #8]
	beq	.L127
	ldr	r4, [r1, #8]
	mov	r4, r4, lsr #12
	cmp	r2, #0
	moveq	r4, #0
	andne	r4, r4, #1
	cmp	r4, #0
	beq	.L127
	cmp	r0, #0
	cmpne	ip, #0
	beq	.L127
	ldr	r4, [r1, #196]
	str	r4, [r2, #0]
	ldr	r2, [r1, #200]
	str	r2, [ip, #0]
	ldr	ip, [r1, #204]
	str	ip, [r0, #0]
	ldrb	r2, [r1, #208]	@ zero_extendqisi2
	mov	r0, #4096
	str	r2, [r3, #0]
	b	.L128
.L127:
	mov	r0, #0
.L128:
	ldmfd	sp!, {r4}
	bx	lr
	.size	png_get_iCCP, .-png_get_iCCP
	.section	.text.png_get_sPLT,"ax",%progbits
	.align	2
	.global	png_get_sPLT
	.hidden	png_get_sPLT
	.type	png_get_sPLT, %function
png_get_sPLT:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	beq	.L131
	cmp	r2, #0
	beq	.L131
	ldr	r3, [r1, #212]
	str	r3, [r2, #0]
	ldr	r0, [r1, #216]
	bx	lr
.L131:
	mov	r0, #0
	bx	lr
	.size	png_get_sPLT, .-png_get_sPLT
	.section	.text.png_get_hIST,"ax",%progbits
	.align	2
	.global	png_get_hIST
	.hidden	png_get_hIST
	.type	png_get_hIST, %function
png_get_hIST:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	beq	.L135
	ldr	r0, [r1, #8]
	mov	r3, r0, lsr #6
	cmp	r2, #0
	moveq	r3, #0
	andne	r3, r3, #1
	cmp	r3, #0
	beq	.L135
	ldr	ip, [r1, #124]
	mov	r0, #64
	str	ip, [r2, #0]
	bx	lr
.L135:
	mov	r0, #0
	bx	lr
	.size	png_get_hIST, .-png_get_hIST
	.section	.text.png_get_oFFs,"ax",%progbits
	.align	2
	.global	png_get_oFFs
	.hidden	png_get_oFFs
	.type	png_get_oFFs, %function
png_get_oFFs:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	ldr	r0, [sp, #0]
	beq	.L139
	ldr	ip, [r1, #8]
	mov	ip, ip, lsr #8
	cmp	r2, #0
	moveq	ip, #0
	andne	ip, ip, #1
	cmp	ip, #0
	beq	.L139
	cmp	r0, #0
	cmpne	r3, #0
	beq	.L139
	ldr	ip, [r1, #100]
	str	ip, [r2, #0]
	ldr	r2, [r1, #104]
	str	r2, [r3, #0]
	ldrb	r3, [r1, #108]	@ zero_extendqisi2
	str	r3, [r0, #0]
	mov	r0, #256
	bx	lr
.L139:
	mov	r0, #0
	bx	lr
	.size	png_get_oFFs, .-png_get_oFFs
	.section	.text.png_get_pCAL,"ax",%progbits
	.align	2
	.global	png_get_pCAL
	.hidden	png_get_pCAL
	.type	png_get_pCAL, %function
png_get_pCAL:
	@ Function supports interworking.
	@ args = 20, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, r5, r6, r7}
	cmp	r1, #0
	cmpne	r0, #0
	add	r0, sp, #16
	ldmia	r0, {r0, r4, ip}	@ phole ldm
	ldr	r6, [sp, #28]
	ldr	r5, [sp, #32]
	beq	.L143
	ldr	r7, [r1, #8]
	mov	r7, r7, lsr #10
	cmp	r2, #0
	moveq	r7, #0
	andne	r7, r7, #1
	cmp	r7, #0
	beq	.L143
	cmp	r0, #0
	cmpne	r3, #0
	beq	.L143
	cmp	ip, #0
	cmpne	r4, #0
	beq	.L143
	cmp	r5, #0
	cmpne	r6, #0
	beq	.L143
	ldr	r7, [r1, #160]
	str	r7, [r2, #0]
	ldr	r2, [r1, #164]
	str	r2, [r3, #0]
	ldr	r2, [r1, #168]
	str	r2, [r0, #0]
	ldrb	r3, [r1, #180]	@ zero_extendqisi2
	str	r3, [r4, #0]
	ldrb	r2, [r1, #181]	@ zero_extendqisi2
	str	r2, [ip, #0]
	ldr	r0, [r1, #172]
	str	r0, [r6, #0]
	ldr	r3, [r1, #176]
	mov	r0, #1024
	str	r3, [r5, #0]
	b	.L144
.L143:
	mov	r0, #0
.L144:
	ldmfd	sp!, {r4, r5, r6, r7}
	bx	lr
	.size	png_get_pCAL, .-png_get_pCAL
	.section	.text.png_get_sCAL,"ax",%progbits
	.align	2
	.global	png_get_sCAL
	.hidden	png_get_sCAL
	.type	png_get_sCAL, %function
png_get_sCAL:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	str	fp, [sp, #-4]!
	beq	.L147
	ldr	r0, [r1, #8]
	tst	r0, #16384
	beq	.L147
	ldrb	fp, [r1, #220]	@ zero_extendqisi2
	str	fp, [r2, #0]
	add	ip, r1, #224
	ldmia	ip, {fp-ip}
	stmia	r3, {fp-ip}
	ldr	r3, [sp, #4]
	add	r2, r1, #232
	ldmia	r2, {r1-r2}
	mov	r0, #16384
	stmia	r3, {r1-r2}
	b	.L148
.L147:
	mov	r0, #0
.L148:
	ldmfd	sp!, {fp}
	bx	lr
	.size	png_get_sCAL, .-png_get_sCAL
	.section	.text.png_get_pHYs,"ax",%progbits
	.align	2
	.global	png_get_pHYs
	.hidden	png_get_pHYs
	.type	png_get_pHYs, %function
png_get_pHYs:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	ldr	ip, [sp, #0]
	beq	.L151
	ldr	r0, [r1, #8]
	tst	r0, #128
	beq	.L151
	cmp	r2, #0
	ldrne	r0, [r1, #112]
	strne	r0, [r2, #0]
	moveq	r0, r2
	movne	r0, #128
	cmp	r3, #0
	ldrne	r2, [r1, #116]
	strne	r2, [r3, #0]
	orrne	r0, r0, #128
	cmp	ip, #0
	ldrneb	r3, [r1, #120]	@ zero_extendqisi2
	orrne	r0, r0, #128
	strne	r3, [ip, #0]
	bx	lr
.L151:
	mov	r0, #0
	bx	lr
	.size	png_get_pHYs, .-png_get_pHYs
	.section	.text.png_get_PLTE,"ax",%progbits
	.align	2
	.global	png_get_PLTE
	.hidden	png_get_PLTE
	.type	png_get_PLTE, %function
png_get_PLTE:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	beq	.L158
	ldr	ip, [r1, #8]
	mov	r0, ip, lsr #3
	cmp	r2, #0
	moveq	r0, #0
	andne	r0, r0, #1
	cmp	r0, #0
	beq	.L158
	ldr	r0, [r1, #16]
	str	r0, [r2, #0]
	ldrh	r2, [r1, #20]
	mov	r0, #8
	str	r2, [r3, #0]
	bx	lr
.L158:
	mov	r0, #0
	bx	lr
	.size	png_get_PLTE, .-png_get_PLTE
	.section	.text.png_get_sBIT,"ax",%progbits
	.align	2
	.global	png_get_sBIT
	.hidden	png_get_sBIT
	.type	png_get_sBIT, %function
png_get_sBIT:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	beq	.L162
	ldr	ip, [r1, #8]
	mov	r3, ip, lsr #1
	cmp	r2, #0
	moveq	r3, #0
	andne	r3, r3, #1
	cmp	r3, #0
	addne	r1, r1, #68
	strne	r1, [r2, #0]
	movne	r0, #2
	bxne	lr
.L162:
	mov	r0, #0
	bx	lr
	.size	png_get_sBIT, .-png_get_sBIT
	.section	.text.png_get_text,"ax",%progbits
	.align	2
	.global	png_get_text
	.hidden	png_get_text
	.type	png_get_text, %function
png_get_text:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	beq	.L166
	ldr	r0, [r1, #48]
	cmp	r0, #0
	ble	.L166
	cmp	r2, #0
	ldrne	r0, [r1, #56]
	strne	r0, [r2, #0]
	ldrne	r0, [r1, #48]
	cmp	r3, #0
	strne	r0, [r3, #0]
	ldrne	r0, [r1, #48]
	bx	lr
.L166:
	cmp	r3, #0
	movne	r0, #0
	moveq	r0, r3
	strne	r0, [r3, #0]
	bx	lr
	.size	png_get_text, .-png_get_text
	.section	.text.png_get_tIME,"ax",%progbits
	.align	2
	.global	png_get_tIME
	.hidden	png_get_tIME
	.type	png_get_tIME, %function
png_get_tIME:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	beq	.L173
	ldr	ip, [r1, #8]
	mov	r3, ip, lsr #9
	cmp	r2, #0
	moveq	r3, #0
	andne	r3, r3, #1
	cmp	r3, #0
	addne	r1, r1, #60
	strne	r1, [r2, #0]
	movne	r0, #512
	bxne	lr
.L173:
	mov	r0, #0
	bx	lr
	.size	png_get_tIME, .-png_get_tIME
	.section	.text.png_get_tRNS,"ax",%progbits
	.align	2
	.global	png_get_tRNS
	.hidden	png_get_tRNS
	.type	png_get_tRNS, %function
png_get_tRNS:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	ldr	ip, [sp, #0]
	beq	.L177
	ldr	r0, [r1, #8]
	tst	r0, #16
	beq	.L177
	ldrb	r0, [r1, #25]	@ zero_extendqisi2
	cmp	r0, #3
	beq	.L186
	cmp	ip, #0
	addne	r0, r1, #80
	strne	r0, [ip, #0]
	moveq	r0, ip
	movne	r0, #16
	cmp	r2, #0
	movne	ip, #0
	strne	ip, [r2, #0]
	cmp	r3, #0
	ldrneh	r2, [r1, #22]
	orrne	r0, r0, #16
	strne	r2, [r3, #0]
	bx	lr
.L177:
	mov	r0, #0
	bx	lr
.L186:
	cmp	r2, #0
	ldrne	r0, [r1, #76]
	moveq	r0, r2
	strne	r0, [r2, #0]
	movne	r0, #16
	cmp	ip, #0
	addne	r2, r1, #80
	strne	r2, [ip, #0]
	cmp	r3, #0
	ldrneh	r2, [r1, #22]
	orrne	r0, r0, #16
	strne	r2, [r3, #0]
	bx	lr
	.size	png_get_tRNS, .-png_get_tRNS
	.section	.text.png_get_unknown_chunks,"ax",%progbits
	.align	2
	.global	png_get_unknown_chunks
	.hidden	png_get_unknown_chunks
	.type	png_get_unknown_chunks, %function
png_get_unknown_chunks:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	cmpne	r0, #0
	beq	.L188
	cmp	r2, #0
	beq	.L188
	ldr	r3, [r1, #188]
	str	r3, [r2, #0]
	ldr	r0, [r1, #192]
	bx	lr
.L188:
	mov	r0, #0
	bx	lr
	.size	png_get_unknown_chunks, .-png_get_unknown_chunks
	.section	.text.png_get_rgb_to_gray_status,"ax",%progbits
	.align	2
	.global	png_get_rgb_to_gray_status
	.hidden	png_get_rgb_to_gray_status
	.type	png_get_rgb_to_gray_status, %function
png_get_rgb_to_gray_status:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	ldrneb	r0, [r0, #580]	@ zero_extendqisi2
	bx	lr
	.size	png_get_rgb_to_gray_status, .-png_get_rgb_to_gray_status
	.section	.text.png_get_user_chunk_ptr,"ax",%progbits
	.align	2
	.global	png_get_user_chunk_ptr
	.hidden	png_get_user_chunk_ptr
	.type	png_get_user_chunk_ptr, %function
png_get_user_chunk_ptr:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	ldrne	r0, [r0, #564]
	bx	lr
	.size	png_get_user_chunk_ptr, .-png_get_user_chunk_ptr
	.section	.text.png_get_compression_buffer_size,"ax",%progbits
	.align	2
	.global	png_get_compression_buffer_size
	.hidden	png_get_compression_buffer_size
	.type	png_get_compression_buffer_size, %function
png_get_compression_buffer_size:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	ldrne	r0, [r0, #204]
	bx	lr
	.size	png_get_compression_buffer_size, .-png_get_compression_buffer_size
	.section	.text.png_get_asm_flags,"ax",%progbits
	.align	2
	.global	png_get_asm_flags
	.hidden	png_get_asm_flags
	.type	png_get_asm_flags, %function
png_get_asm_flags:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #0
	bx	lr
	.size	png_get_asm_flags, .-png_get_asm_flags
	.section	.text.png_get_asm_flagmask,"ax",%progbits
	.align	2
	.global	png_get_asm_flagmask
	.hidden	png_get_asm_flagmask
	.type	png_get_asm_flagmask, %function
png_get_asm_flagmask:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #0
	bx	lr
	.size	png_get_asm_flagmask, .-png_get_asm_flagmask
	.section	.text.png_get_mmx_flagmask,"ax",%progbits
	.align	2
	.global	png_get_mmx_flagmask
	.hidden	png_get_mmx_flagmask
	.type	png_get_mmx_flagmask, %function
png_get_mmx_flagmask:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mvn	r3, #0
	str	r3, [r1, #0]
	mov	r0, #0
	bx	lr
	.size	png_get_mmx_flagmask, .-png_get_mmx_flagmask
	.section	.text.png_get_mmx_bitdepth_threshold,"ax",%progbits
	.align	2
	.global	png_get_mmx_bitdepth_threshold
	.hidden	png_get_mmx_bitdepth_threshold
	.type	png_get_mmx_bitdepth_threshold, %function
png_get_mmx_bitdepth_threshold:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #0
	bx	lr
	.size	png_get_mmx_bitdepth_threshold, .-png_get_mmx_bitdepth_threshold
	.section	.text.png_get_mmx_rowbytes_threshold,"ax",%progbits
	.align	2
	.global	png_get_mmx_rowbytes_threshold
	.hidden	png_get_mmx_rowbytes_threshold
	.type	png_get_mmx_rowbytes_threshold, %function
png_get_mmx_rowbytes_threshold:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #0
	bx	lr
	.size	png_get_mmx_rowbytes_threshold, .-png_get_mmx_rowbytes_threshold
	.section	.text.png_get_user_width_max,"ax",%progbits
	.align	2
	.global	png_get_user_width_max
	.hidden	png_get_user_width_max
	.type	png_get_user_width_max, %function
png_get_user_width_max:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	ldrne	r0, [r0, #640]
	bx	lr
	.size	png_get_user_width_max, .-png_get_user_width_max
	.section	.text.png_get_user_height_max,"ax",%progbits
	.align	2
	.global	png_get_user_height_max
	.hidden	png_get_user_height_max
	.type	png_get_user_height_max, %function
png_get_user_height_max:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	ldrne	r0, [r0, #644]
	bx	lr
	.size	png_get_user_height_max, .-png_get_user_height_max
	.section	.text.png_get_IHDR,"ax",%progbits
	.align	2
	.global	png_get_IHDR
	.hidden	png_get_IHDR
	.type	png_get_IHDR, %function
png_get_IHDR:
	@ Function supports interworking.
	@ args = 20, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	cmp	r1, #0
	cmpne	r0, #0
	sub	sp, sp, #12
	mov	r4, r1
	add	r1, sp, #40
	ldmia	r1, {r1, r5, r6, r8}	@ phole ldm
	ldr	r7, [sp, #56]
	mov	sl, r0
	beq	.L222
	cmp	r3, #0
	cmpne	r2, #0
	beq	.L222
	cmp	r5, #0
	cmpne	r1, #0
	beq	.L222
	ldr	ip, [r4, #0]
	str	ip, [r2, #0]
	ldr	ip, [r4, #4]
	str	ip, [r3, #0]
	ldrb	ip, [r4, #24]	@ zero_extendqisi2
	str	ip, [r1, #0]
	ldrb	ip, [r4, #24]	@ zero_extendqisi2
	sub	r1, ip, #1
	cmp	r1, #15
	bhi	.L234
.L223:
	ldrb	r0, [r4, #25]	@ zero_extendqisi2
	str	r0, [r5, #0]
	ldrb	r1, [r4, #25]	@ zero_extendqisi2
	cmp	r1, #6
	bhi	.L235
.L224:
	cmp	r8, #0
	ldrneb	r1, [r4, #26]	@ zero_extendqisi2
	strne	r1, [r8, #0]
	cmp	r7, #0
	ldrneb	r1, [r4, #27]	@ zero_extendqisi2
	strne	r1, [r7, #0]
	cmp	r6, #0
	ldrneb	r1, [r4, #28]	@ zero_extendqisi2
	strne	r1, [r6, #0]
	ldr	r2, [r2, #0]
	cmp	r2, #0
	ble	.L236
.L228:
	ldr	r3, [r3, #0]
	cmp	r3, #0
	ble	.L237
.L229:
	ldr	r1, [r4, #0]
	mov	r0, #536870912
	sub	ip, r0, #130
	cmp	r1, ip
	bhi	.L230
	mov	r0, #1
	b	.L231
.L222:
	mov	r0, #0
.L231:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	bx	lr
.L230:
	mov	r0, sl
	ldr	r1, .L238
	bl	png_warning
	mov	r0, #1
	b	.L231
.L235:
	mov	r0, sl
	ldr	r1, .L238+4
	str	r2, [sp, #4]
	str	r3, [sp, #0]
	bl	png_error
	ldr	r3, [sp, #0]
	ldr	r2, [sp, #4]
	b	.L224
.L234:
	ldr	r1, .L238+8
	str	r2, [sp, #4]
	str	r3, [sp, #0]
	bl	png_error
	ldr	r3, [sp, #0]
	ldr	r2, [sp, #4]
	b	.L223
.L237:
	mov	r0, sl
	ldr	r1, .L238+12
	bl	png_error
	b	.L229
.L236:
	mov	r0, sl
	ldr	r1, .L238+16
	str	r3, [sp, #0]
	bl	png_error
	ldr	r3, [sp, #0]
	b	.L228
.L239:
	.align	2
.L238:
	.word	.LC4
	.word	.LC1
	.word	.LC0
	.word	.LC3
	.word	.LC2
	.size	png_get_IHDR, .-png_get_IHDR
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"Invalid bit depth\000"
	.space	2
.LC1:
	.ascii	"Invalid color type\000"
	.space	1
.LC2:
	.ascii	"Invalid image width\000"
.LC3:
	.ascii	"Invalid image height\000"
	.space	3
.LC4:
	.ascii	"Width too large for libpng to process image data.\000"
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
