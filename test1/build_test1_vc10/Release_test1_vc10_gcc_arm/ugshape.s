	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"ugshape.c"
	.global	__aeabi_fmul
	.section	.text.ugWireCubef,"ax",%progbits
	.align	2
	.global	ugWireCubef
	.hidden	ugWireCubef
	.type	ugWireCubef, %function
ugWireCubef:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, lr}
	mov	r1, #1056964608
	bl	__aeabi_fmul
	ldr	r7, .L9
	ldr	r6, .L9+4
	mov	r5, r0
	mov	r4, #0
.L2:
	ldr	r0, [r6, r4]	@ float
	mov	r1, r5
	bl	__aeabi_fmul
	add	r8, r4, #4
	str	r0, [r7, r4]	@ float
	mov	r1, r5
	ldr	r0, [r6, r8]	@ float
	bl	__aeabi_fmul
	add	sl, r8, #4
	str	r0, [r7, r8]	@ float
	mov	r1, r5
	ldr	r0, [r6, sl]	@ float
	bl	__aeabi_fmul
	add	r8, r4, #12
	str	r0, [r7, sl]	@ float
	mov	r1, r5
	ldr	r0, [r6, r8]	@ float
	bl	__aeabi_fmul
	add	r4, r4, #16
	cmp	r4, #288
	str	r0, [r7, r8]	@ float
	bne	.L2
	mov	lr, #5120
	add	r1, lr, #6
	ldr	r3, .L9
	mov	r0, #3
	mov	r2, #0
	bl	glVertexPointer
	mov	ip, #5120
	add	r0, ip, #6
	mov	r1, #0
	ldr	r2, .L9+8
	bl	glNormalPointer
	mov	r3, #32768
	add	r0, r3, #116
	bl	glEnableClientState
	mov	r2, #32768
	add	r0, r2, #117
	bl	glEnableClientState
	mov	r0, #2
	mov	r1, #0
	mov	r2, #4
	bl	glDrawArrays
	mov	r1, #4
	mov	r2, r1
	mov	r0, #2
	bl	glDrawArrays
	mov	r0, #2
	mov	r1, #8
	mov	r2, #4
	bl	glDrawArrays
	mov	r0, #2
	mov	r1, #12
	mov	r2, #4
	bl	glDrawArrays
	mov	r0, #2
	mov	r1, #16
	mov	r2, #4
	bl	glDrawArrays
	mov	r2, #4
	mov	r0, #2
	mov	r1, #20
	bl	glDrawArrays
	mov	r1, #32768
	add	r0, r1, #116
	bl	glDisableClientState
	mov	r0, #32768
	add	r0, r0, #117
	bl	glDisableClientState
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, lr}
	bx	lr
.L10:
	.align	2
.L9:
	.word	.LANCHOR0
	.word	.LANCHOR1
	.word	.LANCHOR1+288
	.size	ugWireCubef, .-ugWireCubef
	.section	.text.ugSolidCubef,"ax",%progbits
	.align	2
	.global	ugSolidCubef
	.hidden	ugSolidCubef
	.type	ugSolidCubef, %function
ugSolidCubef:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, lr}
	mov	r1, #1056964608
	bl	__aeabi_fmul
	ldr	r7, .L18
	ldr	r6, .L18+4
	mov	r5, r0
	mov	r4, #0
.L12:
	ldr	r0, [r6, r4]	@ float
	mov	r1, r5
	bl	__aeabi_fmul
	add	r8, r4, #4
	str	r0, [r7, r4]	@ float
	mov	r1, r5
	ldr	r0, [r6, r8]	@ float
	bl	__aeabi_fmul
	add	sl, r8, #4
	str	r0, [r7, r8]	@ float
	mov	r1, r5
	ldr	r0, [r6, sl]	@ float
	bl	__aeabi_fmul
	add	r8, r4, #12
	str	r0, [r7, sl]	@ float
	mov	r1, r5
	ldr	r0, [r6, r8]	@ float
	bl	__aeabi_fmul
	add	r4, r4, #16
	cmp	r4, #432
	str	r0, [r7, r8]	@ float
	bne	.L12
	mov	lr, #5120
	add	r1, lr, #6
	ldr	r3, .L18
	mov	r0, #3
	mov	r2, #0
	bl	glVertexPointer
	mov	ip, #5120
	add	r0, ip, #6
	mov	r1, #0
	ldr	r2, .L18+8
	bl	glNormalPointer
	mov	r3, #32768
	add	r0, r3, #116
	bl	glEnableClientState
	mov	r2, #32768
	add	r0, r2, #117
	bl	glEnableClientState
	mov	r2, #36
	mov	r0, #4
	mov	r1, #0
	bl	glDrawArrays
	mov	r1, #32768
	add	r0, r1, #116
	bl	glDisableClientState
	mov	r0, #32768
	add	r0, r0, #117
	bl	glDisableClientState
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, lr}
	bx	lr
.L19:
	.align	2
.L18:
	.word	.LANCHOR0+288
	.word	.LANCHOR1+576
	.word	.LANCHOR1+1008
	.size	ugSolidCubef, .-ugSolidCubef
	.global	__aeabi_i2f
	.global	__aeabi_fdiv
	.global	__aeabi_f2d
	.global	__aeabi_d2f
	.global	__aeabi_fadd
	.section	.text.PlotSpherePoints,"ax",%progbits
	.align	2
	.global	PlotSpherePoints
	.hidden	PlotSpherePoints
	.type	PlotSpherePoints, %function
PlotSpherePoints:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 56
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	mov	r4, r0
	sub	sp, sp, #60
	mov	r0, r1
	str	r3, [sp, #40]
	str	r1, [sp, #52]
	str	r2, [sp, #20]
	bl	__aeabi_i2f
	mov	r2, #4784128
	add	ip, r2, #4048
	mov	r1, r0
	add	r0, ip, #1073741835
	bl	__aeabi_fdiv
	str	r0, [sp, #44]	@ float
	ldr	r0, [sp, #20]
	bl	__aeabi_i2f
	mov	r3, #13172736
	mov	r1, r0
	add	r0, r3, #4048
	add	r0, r0, #1073741835
	bl	__aeabi_fdiv
	ldr	r2, [sp, #52]
	cmp	r2, #0
	str	r0, [sp, #32]	@ float
	ble	.L25
	ldr	r9, [sp, #20]
	add	r7, r9, #1
	add	r6, r7, r7, asl #1
	mov	r5, r6, asl #3
	mov	r1, #0
	str	r5, [sp, #48]
	str	r1, [sp, #36]
.L24:
	ldr	r0, [sp, #36]
	bl	__aeabi_i2f
	ldr	r1, [sp, #44]	@ float
	bl	__aeabi_fmul
	mov	r5, r0
	bl	__aeabi_f2d
	mov	r6, r0
	mov	r7, r1
	bl	sin
	bl	__aeabi_d2f
	ldr	r1, [sp, #44]	@ float
	mov	r9, r0
	mov	r0, r5
	bl	__aeabi_fadd
	bl	__aeabi_f2d
	str	r0, [sp, #8]
	str	r1, [sp, #4]
	bl	sin
	bl	__aeabi_d2f
	mov	r1, r7
	mov	sl, r0
	mov	r0, r6
	bl	cos
	bl	__aeabi_d2f
	ldmib	sp, {r3, ip}	@ phole ldm
	str	r0, [sp, #16]	@ float
	mov	r1, r3
	mov	r0, ip
	bl	cos
	bl	__aeabi_d2f
	str	r0, [sp, #12]	@ float
	ldr	r0, [sp, #20]
	cmp	r0, #0
	blt	.L22
	mov	r1, r4
	ldr	r0, [sp, #16]	@ float
	bl	__aeabi_fmul
	mov	r1, r4
	str	r0, [sp, #28]	@ float
	ldr	r0, [sp, #12]	@ float
	bl	__aeabi_fmul
	ldr	r6, [sp, #96]
	str	r0, [sp, #24]	@ float
	ldr	r5, [sp, #40]
	mov	r7, #0
.L23:
	mov	r0, r7
	bl	__aeabi_i2f
	ldr	r1, [sp, #32]	@ float
	bl	__aeabi_fmul
	bl	__aeabi_f2d
	str	r0, [sp, #8]
	str	r1, [sp, #4]
	bl	cos
	bl	__aeabi_d2f
	ldr	r3, [sp, #8]
	ldr	fp, [sp, #4]
	mov	r8, r0
	mov	r1, fp
	mov	r0, r3
	bl	sin
	bl	__aeabi_d2f
	mov	r1, r9
	mov	fp, r0
	mov	r0, r8
	bl	__aeabi_fmul
	mov	r1, r4
	str	r0, [r6, #0]	@ float
	bl	__aeabi_fmul
	mov	r1, r9
	str	r0, [r5, #0]	@ float
	mov	r0, fp
	bl	__aeabi_fmul
	mov	r1, r0
	str	r0, [r6, #4]	@ float
	mov	r0, r4
	bl	__aeabi_fmul
	str	r0, [r5, #4]	@ float
	ldr	r2, [sp, #16]	@ float
	str	r2, [r6, #8]	@ float
	ldr	ip, [sp, #28]	@ float
	mov	r1, sl
	str	ip, [r5, #8]	@ float
	mov	r0, r8
	bl	__aeabi_fmul
	mov	r1, r0
	str	r0, [r6, #12]	@ float
	mov	r0, r4
	bl	__aeabi_fmul
	mov	r1, sl
	str	r0, [r5, #12]	@ float
	mov	r0, fp
	bl	__aeabi_fmul
	mov	r1, r0
	str	r0, [r6, #16]	@ float
	mov	r0, r4
	bl	__aeabi_fmul
	ldr	r3, [sp, #20]
	str	r0, [r5, #16]	@ float
	ldr	r0, [sp, #12]	@ float
	str	r0, [r6, #20]	@ float
	add	r7, r7, #1
	ldr	r1, [sp, #24]	@ float
	cmp	r3, r7
	str	r1, [r5, #20]	@ float
	add	r6, r6, #24
	add	r5, r5, #24
	bge	.L23
	ldr	r3, [sp, #96]
	ldr	r0, [sp, #48]
	ldr	r1, [sp, #40]
	add	r2, r3, r0
	add	lr, r1, r0
	str	r2, [sp, #96]
	str	lr, [sp, #40]
.L22:
	ldr	r1, [sp, #36]
	ldr	ip, [sp, #52]
	add	lr, r1, #1
	cmp	lr, ip
	str	lr, [sp, #36]
	bne	.L24
.L25:
	add	sp, sp, #60
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
	.size	PlotSpherePoints, .-PlotSpherePoints
	.global	__aeabi_fsub
	.section	.text.PlotDiskPoints,"ax",%progbits
	.align	2
	.global	PlotDiskPoints
	.hidden	PlotDiskPoints
	.type	PlotDiskPoints, %function
PlotDiskPoints:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	sub	sp, sp, #52
	str	r0, [sp, #40]	@ float
	mov	r0, r3
	str	r2, [sp, #36]
	mov	r5, r1
	mov	r4, r3
	bl	__aeabi_i2f
	mov	r3, #13172736
	mov	r1, r0
	add	r0, r3, #4048
	add	r0, r0, #1073741835
	bl	__aeabi_fdiv
	ldr	r1, [sp, #40]	@ float
	str	r0, [sp, #20]	@ float
	mov	r0, r5
	bl	__aeabi_fsub
	mov	r5, r0
	ldr	r0, [sp, #36]
	bl	__aeabi_i2f
	mov	r1, r0
	mov	r0, r5
	bl	__aeabi_fdiv
	ldr	r2, [sp, #36]
	cmp	r2, #0
	str	r0, [sp, #32]	@ float
	ble	.L35
	ldr	r9, [sp, #88]
	add	sl, r4, #1
	add	r8, sl, sl, asl #1
	str	r9, [sp, #28]
	mov	r1, r8, asl #3
	mov	r9, #0
	str	r1, [sp, #44]
	str	r9, [sp, #24]
.L34:
	ldr	r0, [sp, #24]
	bl	__aeabi_i2f
	ldr	r1, [sp, #32]	@ float
	bl	__aeabi_fmul
	ldr	r1, [sp, #40]	@ float
	bl	__aeabi_fadd
	ldr	r1, [sp, #32]	@ float
	mov	sl, r0
	bl	__aeabi_fadd
	cmp	r4, #0
	mov	r8, r0
	blt	.L30
	ldr	r5, [sp, #28]
	mov	r6, #0
	b	.L33
.L31:
	mov	r0, r6
	bl	__aeabi_i2f
	ldr	r1, [sp, #20]	@ float
	bl	__aeabi_fmul
	mov	r1, #0
	bl	__aeabi_fadd
	bl	__aeabi_f2d
	mov	fp, r0
	str	r1, [sp, #4]
	bl	cos
	ldr	ip, [sp, #4]
	mov	r2, r0
	mov	r3, r1
	mov	r0, fp
	mov	r1, ip
	str	r2, [sp, #4]
	str	r3, [sp, #0]
	bl	sin
	str	r0, [sp, #8]
	str	r1, [sp, #12]
	ldr	r3, [sp, #0]
	ldr	r2, [sp, #4]
.L32:
	mov	r1, r3
	mov	r0, r2
	bl	__aeabi_d2f
	mov	r7, r0
	add	r1, sp, #8
	ldmia	r1, {r0-r1}
	bl	__aeabi_d2f
	mov	r1, sl
	mov	fp, r0
	mov	r0, r7
	bl	__aeabi_fmul
	mov	r1, sl
	str	r0, [r5, #0]	@ float
	mov	r0, fp
	bl	__aeabi_fmul
	str	r9, [r5, #8]	@ float
	str	r0, [r5, #4]	@ float
	mov	r1, r8
	mov	r0, r7
	bl	__aeabi_fmul
	mov	r1, r8
	str	r0, [r5, #12]	@ float
	mov	r0, fp
	bl	__aeabi_fmul
	add	r6, r6, #1
	cmp	r6, r4
	str	r0, [r5, #16]	@ float
	str	r9, [r5, #20]	@ float
	add	r5, r5, #24
	bgt	.L38
.L33:
	cmp	r6, r4
	bne	.L31
	mov	r2, #0
	mov	r3, #0
	mov	r7, #1069547520
	str	r2, [sp, #8]
	str	r3, [sp, #12]
	mov	r2, #0
	add	r3, r7, #3145728
	b	.L32
.L38:
	ldr	r0, [sp, #28]
	ldr	r1, [sp, #44]
	add	lr, r0, r1
	str	lr, [sp, #28]
.L30:
	ldr	ip, [sp, #24]
	ldr	r2, [sp, #36]
	add	r3, ip, #1
	cmp	r3, r2
	str	r3, [sp, #24]
	blt	.L34
.L35:
	add	sp, sp, #52
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
	.size	PlotDiskPoints, .-PlotDiskPoints
	.section	.text.PlotTubePoints,"ax",%progbits
	.align	2
	.global	PlotTubePoints
	.hidden	PlotTubePoints
	.type	PlotTubePoints, %function
PlotTubePoints:
	@ Function supports interworking.
	@ args = 8, pretend = 0, frame = 56
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	mov	r5, r0
	sub	sp, sp, #60
	mov	r0, r3
	str	r2, [sp, #52]
	mov	r6, r1
	mov	r4, r3
	bl	__aeabi_i2f
	mov	r3, #13172736
	mov	r1, r0
	add	r0, r3, #4048
	add	r0, r0, #1073741835
	bl	__aeabi_fdiv
	str	r0, [sp, #28]	@ float
	ldr	r0, [sp, #52]
	bl	__aeabi_i2f
	mov	r1, r0
	mov	r0, r6
	bl	__aeabi_fdiv
	ldr	r2, [sp, #52]
	cmp	r2, #0
	str	r0, [sp, #44]	@ float
	ble	.L46
	add	r3, r4, #1
	add	lr, r3, r3, asl #1
	ldr	r0, [sp, #100]
	ldr	r2, [sp, #96]
	mov	ip, lr, asl #3
	mov	r1, #0
	str	r0, [sp, #36]
	str	r2, [sp, #40]
	str	ip, [sp, #48]
	str	r1, [sp, #32]
.L45:
	ldr	r0, [sp, #32]
	bl	__aeabi_i2f
	ldr	r1, [sp, #44]	@ float
	bl	__aeabi_fmul
	mov	r1, #0
	bl	__aeabi_fadd
	ldr	r1, [sp, #44]	@ float
	str	r0, [sp, #24]	@ float
	bl	__aeabi_fadd
	cmp	r4, #0
	str	r0, [sp, #20]	@ float
	blt	.L41
	mov	r9, #0
	ldr	r7, [sp, #36]
	ldr	r6, [sp, #40]
	mov	r8, #0
	b	.L44
.L42:
	mov	r0, r8
	bl	__aeabi_i2f
	ldr	r1, [sp, #28]	@ float
	bl	__aeabi_fmul
	mov	r1, r0
	mov	r0, #0
	bl	__aeabi_fsub
	bl	__aeabi_f2d
	str	r0, [sp, #4]
	str	r1, [sp, #0]
	bl	cos
	ldr	ip, [sp, #0]
	mov	fp, r1
	ldr	r1, [sp, #4]
	mov	sl, r0
	mov	r0, r1
	mov	r1, ip
	bl	sin
	str	r0, [sp, #8]
	str	r1, [sp, #12]
.L43:
	mov	r1, fp
	mov	r0, sl
	bl	__aeabi_d2f
	mov	sl, r0
	add	r1, sp, #8
	ldmia	r1, {r0-r1}
	bl	__aeabi_d2f
	mov	r1, r5
	mov	fp, r0
	mov	r0, sl
	bl	__aeabi_fmul
	mov	r1, r5
	str	r0, [r6, #0]	@ float
	mov	r0, fp
	bl	__aeabi_fmul
	str	r0, [r6, #4]	@ float
	ldr	r1, [sp, #24]	@ float
	str	r1, [r6, #8]	@ float
	str	sl, [r7, #0]	@ float
	str	fp, [r7, #4]	@ float
	str	r9, [r7, #8]	@ float
	ldr	ip, [r6, #0]	@ float
	ldr	r3, [r6, #4]	@ float
	str	ip, [r6, #12]	@ float
	str	r3, [r6, #16]	@ float
	ldr	r0, [sp, #20]	@ float
	add	r8, r8, #1
	cmp	r8, r4
	str	r0, [r6, #20]	@ float
	str	sl, [r7, #12]	@ float
	str	fp, [r7, #16]	@ float
	str	r9, [r7, #20]	@ float
	add	r6, r6, #24
	add	r7, r7, #24
	bgt	.L49
.L44:
	cmp	r8, r4
	bne	.L42
	mov	r2, #0
	mov	r3, #0
	mov	fp, #1069547520
	str	r2, [sp, #8]
	str	r3, [sp, #12]
	mov	sl, #0
	add	fp, fp, #3145728
	b	.L43
.L49:
	ldr	ip, [sp, #40]
	ldr	r2, [sp, #48]
	ldr	r0, [sp, #36]
	add	r3, ip, r2
	add	lr, r0, r2
	str	r3, [sp, #40]
	str	lr, [sp, #36]
.L41:
	ldr	r0, [sp, #32]
	ldr	lr, [sp, #52]
	add	r1, r0, #1
	cmp	r1, lr
	str	r1, [sp, #32]
	blt	.L45
.L46:
	add	sp, sp, #60
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
	.size	PlotTubePoints, .-PlotTubePoints
	.global	__aeabi_fcmpeq
	.global	__aeabi_i2d
	.global	__aeabi_dadd
	.section	.text.ugSolidConef,"ax",%progbits
	.align	2
	.global	ugSolidConef
	.hidden	ugSolidConef
	.type	ugSolidConef, %function
ugSolidConef:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 64
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	ip, .L88
	ldr	r5, [ip, #720]
	sub	sp, sp, #68
	cmp	r5, #0
	str	r0, [sp, #32]	@ float
	str	r1, [sp, #36]	@ float
	mov	r4, r2
	str	r3, [sp, #16]
	beq	.L51
	mov	r1, r0
	ldr	r0, [ip, #724]	@ float
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	beq	.L52
	ldr	r1, .L88
	ldr	r0, [r1, #728]	@ float
	ldr	r1, [sp, #36]	@ float
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	bne	.L86
.L52:
	mov	r0, r5
	bl	free
	ldr	r2, .L88
	ldr	r0, [r2, #740]
	bl	free
	ldr	ip, .L88
	mov	r5, #0
	mov	r1, #5120
	mov	r2, r5
	mov	r3, r5
	str	r5, [ip, #720]
	str	r5, [ip, #740]
	add	r1, r1, #6
	mov	r0, #3
	bl	glVertexPointer
	mov	r1, r5
	mov	r5, #5120
	add	r0, r5, #6
	mov	r2, r1
	bl	glNormalPointer
	ldr	r0, .L88
	ldr	r5, [r0, #720]
	cmp	r5, #0
	beq	.L51
.L53:
	mov	lr, #5120
	add	r1, lr, #6
	mov	r3, r5
	mov	r0, #3
	mov	r2, #0
	bl	glVertexPointer
	ldr	ip, .L88
	mov	r1, #5120
	ldr	r2, [ip, #740]
	add	r0, r1, #6
	mov	r1, #0
	bl	glNormalPointer
	mov	r0, #32768
	add	r0, r0, #116
	bl	glEnableClientState
	mov	r2, #32768
	add	r0, r2, #117
	bl	glEnableClientState
	ldr	ip, [sp, #16]
	cmp	ip, #0
	addgt	r4, r4, #1
	strgt	r4, [sp, #44]
	movgt	ip, r4
	ble	.L63
.L64:
	ldr	r3, [sp, #16]
	mov	r4, ip, asl #1
	sub	lr, r3, #1
	mov	r0, #5
	mov	r1, #0
	mov	r2, r4
	and	r7, lr, #3
	bl	glDrawArrays
	ldr	ip, [sp, #16]
	cmp	ip, #1
	mov	r6, #1
	mov	r5, r4
	ble	.L63
	cmp	r7, #0
	beq	.L84
	cmp	r7, #1
	beq	.L81
	cmp	r7, #2
	beq	.L82
	mov	r0, #5
	mov	r1, r4
	mov	r2, r4
	bl	glDrawArrays
	mov	r6, #2
	mov	r5, r4, asl #1
.L82:
	mov	r1, r5
	mov	r0, #5
	mov	r2, r4
	bl	glDrawArrays
	add	r6, r6, #1
	add	r5, r5, r4
.L81:
	mov	r1, r5
	mov	r0, #5
	mov	r2, r4
	bl	glDrawArrays
	ldr	r3, [sp, #16]
	add	r6, r6, #1
	cmp	r3, r6
	add	r5, r5, r4
	movgt	sl, r3
	movgt	r1, r5
	ble	.L63
.L65:
	add	r8, r1, r4
	mov	r0, #5
	mov	r2, r4
	bl	glDrawArrays
	add	r7, r8, r4
	mov	r0, #5
	mov	r1, r8
	mov	r2, r4
	bl	glDrawArrays
	add	r5, r7, r4
	mov	r0, #5
	mov	r1, r7
	mov	r2, r4
	bl	glDrawArrays
	add	r6, r6, #4
	mov	r1, r5
	mov	r0, #5
	mov	r2, r4
	bl	glDrawArrays
	cmp	sl, r6
	add	r1, r5, r4
	bgt	.L65
.L63:
	mov	r2, #32768
	add	r0, r2, #116
	bl	glDisableClientState
	mov	r3, #32768
	add	r0, r3, #117
	bl	glDisableClientState
	add	sp, sp, #68
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L86:
	mov	r0, r4
	bl	__aeabi_i2f
	ldr	r2, .L88
	mov	r1, r0
	ldr	r0, [r2, #732]	@ float
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	beq	.L52
	ldr	r0, [sp, #16]
	bl	__aeabi_i2f
	ldr	r3, .L88
	mov	r1, r0
	ldr	r0, [r3, #736]	@ float
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	bne	.L53
	b	.L52
.L51:
	ldr	r0, [sp, #36]	@ float
	mov	r1, #0
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	beq	.L55
	ldr	lr, .L88
	ldr	r5, [lr, #720]
	b	.L53
.L84:
	mov	r1, r5
	ldr	sl, [sp, #16]
	b	.L65
.L55:
	ldr	r1, [sp, #36]	@ float
	ldr	r0, [sp, #32]	@ float
	bl	__aeabi_fdiv
	bl	__aeabi_f2d
	bl	atan
	bl	__aeabi_d2f
	bl	__aeabi_f2d
	mov	r5, r0
	mov	r6, r1
	bl	cos
	bl	__aeabi_d2f
	mov	r1, r6
	str	r0, [sp, #12]	@ float
	mov	r0, r5
	bl	sin
	bl	__aeabi_d2f
	ldr	ip, .L88
	ldr	r1, [sp, #36]	@ float
	ldr	r5, [sp, #32]	@ float
	str	r1, [ip, #728]	@ float
	str	r5, [ip, #724]	@ float
	add	r3, r4, #1
	str	r0, [sp, #8]	@ float
	mov	r0, r4
	str	r3, [sp, #44]
	bl	__aeabi_i2f
	str	r0, [sp, #28]	@ float
	ldr	r5, [sp, #28]	@ float
	ldr	r2, .L88
	ldr	r0, [sp, #16]
	str	r5, [r2, #732]	@ float
	bl	__aeabi_i2f
	ldr	r1, [sp, #44]
	ldr	ip, [sp, #16]
	mul	ip, r1, ip
	str	r0, [sp, #48]	@ float
	ldr	r3, .L88
	add	r0, ip, ip, asl #1
	ldr	r2, [sp, #48]	@ float
	mov	r5, r0, asl #3
	str	r2, [r3, #736]	@ float
	mov	r0, r5
	bl	malloc
	str	r0, [sp, #56]
	ldr	ip, [sp, #56]
	ldr	r1, .L88
	mov	r0, r5
	str	ip, [r1, #720]
	bl	malloc
	ldr	r5, [sp, #16]
	ldr	r3, .L88
	cmp	r5, #0
	str	r0, [sp, #52]
	str	r0, [r3, #740]
	mov	r5, r3
	ble	.L56
	ldr	lr, [sp, #44]
	add	fp, lr, lr, asl #1
	mov	r7, fp, asl #3
	mov	r6, #0
	str	r7, [sp, #60]
	str	r6, [sp, #40]
	str	r4, [sp, #0]
.L61:
	ldr	r0, [sp, #40]
	bl	__aeabi_i2f
	ldr	r1, [sp, #48]	@ float
	mov	r5, r0
	bl	__aeabi_fdiv
	mov	r1, r0
	mov	r0, #1065353216
	bl	__aeabi_fsub
	ldr	r1, [sp, #32]	@ float
	bl	__aeabi_fmul
	str	r0, [sp, #4]	@ float
	ldr	r0, [sp, #40]
	bl	__aeabi_i2d
	mov	r3, #1069547520
	mov	r2, #0
	add	r3, r3, #3145728
	bl	__aeabi_dadd
	bl	__aeabi_d2f
	ldr	r1, [sp, #48]	@ float
	bl	__aeabi_fdiv
	mov	r1, r0
	mov	r0, #1065353216
	bl	__aeabi_fsub
	ldr	r1, [sp, #32]	@ float
	bl	__aeabi_fmul
	mov	r1, r5
	mov	fp, r0
	ldr	r0, [sp, #36]	@ float
	bl	__aeabi_fmul
	ldr	r1, [sp, #48]	@ float
	bl	__aeabi_fdiv
	mov	r1, #1065353216
	str	r0, [sp, #24]	@ float
	mov	r0, r5
	bl	__aeabi_fadd
	ldr	r1, [sp, #36]	@ float
	bl	__aeabi_fmul
	ldr	r1, [sp, #48]	@ float
	bl	__aeabi_fdiv
	ldr	r4, [sp, #0]
	cmp	r4, #0
	str	r0, [sp, #20]	@ float
	blt	.L57
	ldr	r5, [sp, #52]
	ldr	r4, [sp, #56]
	mov	r6, #0
	b	.L60
.L58:
	mov	r0, r6
	bl	__aeabi_i2f
	ldr	r1, [sp, #28]	@ float
	bl	__aeabi_fdiv
	mov	r1, #13172736
	add	r2, r1, #4048
	add	r1, r2, #1073741835
	bl	__aeabi_fmul
	bl	__aeabi_f2d
	mov	r9, r0
	mov	sl, r1
	bl	cos
	mov	r7, r0
	mov	r8, r1
	mov	r0, r9
	mov	r1, sl
	bl	sin
	mov	r9, r0
	mov	sl, r1
.L59:
	mov	r1, r8
	mov	r0, r7
	bl	__aeabi_d2f
	mov	r1, sl
	mov	r8, r0
	mov	r0, r9
	bl	__aeabi_d2f
	mov	r1, r8
	mov	r7, r0
	mov	r0, fp
	bl	__aeabi_fmul
	mov	r1, r7
	str	r0, [r4, #0]	@ float
	mov	r0, fp
	bl	__aeabi_fmul
	str	r0, [r4, #4]	@ float
	ldr	sl, [sp, #20]	@ float
	ldr	r0, [sp, #12]	@ float
	mov	r1, r8
	str	sl, [r4, #8]	@ float
	bl	__aeabi_fmul
	mov	r9, r0
	mov	r1, r7
	ldr	r0, [sp, #12]	@ float
	str	r9, [r5, #0]	@ float
	bl	__aeabi_fmul
	mov	sl, r0
	ldr	r0, [sp, #4]	@ float
	str	sl, [r5, #4]	@ float
	ldr	r2, [sp, #8]	@ float
	mov	r1, r8
	str	r2, [r5, #8]	@ float
	bl	__aeabi_fmul
	str	r0, [r4, #12]	@ float
	ldr	r0, [sp, #4]	@ float
	mov	r1, r7
	bl	__aeabi_fmul
	ldr	r3, [sp, #0]
	str	r0, [r4, #16]	@ float
	str	r9, [r5, #12]	@ float
	str	sl, [r5, #16]	@ float
	ldr	r7, [sp, #24]	@ float
	str	r7, [r4, #20]	@ float
	add	r6, r6, #1
	ldr	ip, [sp, #8]	@ float
	cmp	r3, r6
	str	ip, [r5, #20]	@ float
	add	r4, r4, #24
	add	r5, r5, #24
	blt	.L87
.L60:
	ldr	r8, [sp, #0]
	cmp	r6, r8
	bne	.L58
	mov	r0, #1069547520
	mov	r9, #0
	mov	sl, #0
	mov	r7, #0
	add	r8, r0, #3145728
	b	.L59
.L87:
	add	r0, sp, #56
	ldmia	r0, {r0, r4}	@ phole ldm
	ldr	lr, [sp, #52]
	add	r5, r0, r4
	add	r6, lr, r4
	str	r5, [sp, #56]
	str	r6, [sp, #52]
.L57:
	ldr	r3, [sp, #40]
	ldr	ip, [sp, #16]
	add	r1, r3, #1
	cmp	r1, ip
	str	r1, [sp, #40]
	bne	.L61
	ldr	r0, .L88
	mov	r1, #5120
	ldr	r4, .L88
	ldr	r3, [r0, #720]
	add	r1, r1, #6
	mov	r0, #3
	mov	r2, #0
	mov	r5, #5120
	bl	glVertexPointer
	mov	r6, #32768
	ldr	r2, [r4, #740]
	mov	r1, #0
	add	r0, r5, #6
	bl	glNormalPointer
	mov	r7, #32768
	add	r0, r6, #116
	bl	glEnableClientState
	add	r0, r7, #117
	bl	glEnableClientState
	ldr	ip, [sp, #44]
	b	.L64
.L56:
	mov	ip, #5120
	add	r1, ip, #6
	ldr	r3, [r3, #720]
	mov	r0, #3
	mov	r2, #0
	bl	glVertexPointer
	mov	r1, #5120
	ldr	r2, [r5, #740]
	add	r0, r1, #6
	mov	r1, #0
	bl	glNormalPointer
	mov	r0, #32768
	add	r0, r0, #116
	bl	glEnableClientState
	mov	r2, #32768
	add	r0, r2, #117
	bl	glEnableClientState
	b	.L63
.L89:
	.align	2
.L88:
	.word	.LANCHOR0
	.size	ugSolidConef, .-ugSolidConef
	.section	.text.ugWireSpheref,"ax",%progbits
	.align	2
	.global	ugWireSpheref
	.hidden	ugWireSpheref
	.type	ugWireSpheref, %function
ugWireSpheref:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	r5, .L116
	ldr	r6, [r5, #744]
	cmp	r6, #0
	sub	sp, sp, #12
	mov	r7, r0
	mov	r4, r1
	mov	r9, r2
	beq	.L91
	ldr	r0, [r5, #748]	@ float
	mov	r1, r7
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	beq	.L92
	mov	r0, r4
	bl	__aeabi_i2f
	mov	r1, r0
	ldr	r0, [r5, #752]	@ float
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	bne	.L115
.L92:
	mov	r0, r6
	bl	free
	ldr	r0, [r5, #760]
	bl	free
	mov	r6, #0
	mov	r1, #5120
	mov	r2, r6
	mov	r3, r6
	add	r1, r1, #6
	mov	r0, #3
	str	r6, [r5, #744]
	str	r6, [r5, #760]
	bl	glVertexPointer
	mov	r0, #5120
	mov	r1, r6
	mov	r2, r6
	add	r0, r0, #6
	bl	glNormalPointer
	ldr	r6, [r5, #744]
	cmp	r6, #0
	beq	.L91
.L93:
	mov	lr, #5120
	add	r1, lr, #6
	mov	r3, r6
	mov	r0, #3
	mov	r2, #0
	bl	glVertexPointer
	mov	ip, #5120
	add	r0, ip, #6
	ldr	r2, [r5, #760]
	mov	r1, #0
	bl	glNormalPointer
	mov	r1, #32768
	add	r0, r1, #116
	bl	glEnableClientState
	mov	r0, #32768
	add	r0, r0, #117
	bl	glEnableClientState
	cmp	r9, #0
	ble	.L95
	add	fp, r4, #1
	mov	r8, #0
	mov	fp, fp, asl #1
	mov	sl, r8
.L98:
	cmp	r4, #0
	blt	.L96
	mov	r0, #2
	mov	r1, r8
	mov	r2, #3
	mov	r6, #1
	bl	glDrawArrays
	cmp	r4, r6
	and	r3, r4, #3
	add	r5, r8, #2
	blt	.L96
	cmp	r3, #0
	beq	.L97
	cmp	r3, #1
	beq	.L113
	cmp	r3, #2
	beq	.L114
	mov	r1, r5
	mov	r0, #2
	mov	r2, #3
	bl	glDrawArrays
	add	r5, r5, #2
	mov	r6, #2
.L114:
	mov	r1, r5
	mov	r0, #2
	mov	r2, #3
	bl	glDrawArrays
	add	r6, r6, #1
	add	r5, r5, #2
.L113:
	mov	r1, r5
	add	r6, r6, #1
	mov	r0, #2
	mov	r2, #3
	bl	glDrawArrays
	cmp	r4, r6
	add	r5, r5, #2
	blt	.L96
.L97:
	mov	r1, r5
	add	r7, r5, #2
	mov	r0, #2
	mov	r2, #3
	bl	glDrawArrays
	mov	r0, #2
	mov	r1, r7
	mov	r2, #3
	bl	glDrawArrays
	add	r1, r7, #2
	mov	r0, #2
	mov	r2, #3
	bl	glDrawArrays
	add	r6, r6, #4
	add	r1, r5, #6
	mov	r0, #2
	mov	r2, #3
	bl	glDrawArrays
	cmp	r4, r6
	add	r5, r5, #8
	bge	.L97
.L96:
	add	sl, sl, #1
	cmp	r9, sl
	add	r8, r8, fp
	bgt	.L98
.L95:
	mov	r3, #32768
	add	r0, r3, #116
	bl	glDisableClientState
	mov	r2, #32768
	add	r0, r2, #117
	bl	glDisableClientState
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L115:
	mov	r0, r9
	bl	__aeabi_i2f
	mov	r1, r0
	ldr	r0, [r5, #756]	@ float
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	bne	.L93
	b	.L92
.L91:
	mov	r0, r4
	str	r7, [r5, #748]	@ float
	bl	__aeabi_i2f
	str	r0, [r5, #752]	@ float
	mov	r0, r9
	bl	__aeabi_i2f
	add	r3, r4, #1
	mul	r3, r9, r3
	mov	r2, #6
	mul	r2, r3, r2
	mov	r6, r2, asl #2
	str	r0, [r5, #756]	@ float
	mov	r0, r6
	bl	malloc
	str	r0, [r5, #744]
	mov	r0, r6
	bl	malloc
	ldr	r6, [r5, #744]
	mov	ip, r0
	str	ip, [sp, #0]
	mov	r0, r7
	mov	r1, r9
	mov	r2, r4
	mov	r3, r6
	str	ip, [r5, #760]
	bl	PlotSpherePoints
	b	.L93
.L117:
	.align	2
.L116:
	.word	.LANCHOR0
	.size	ugWireSpheref, .-ugWireSpheref
	.section	.text.ugSolidSpheref,"ax",%progbits
	.align	2
	.global	ugSolidSpheref
	.hidden	ugSolidSpheref
	.type	ugSolidSpheref, %function
ugSolidSpheref:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	ldr	r4, .L142
	ldr	r6, [r4, #764]
	cmp	r6, #0
	sub	sp, sp, #12
	mov	sl, r0
	mov	r7, r1
	mov	r5, r2
	beq	.L119
	ldr	r0, [r4, #768]	@ float
	mov	r1, sl
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	beq	.L120
	mov	r0, r7
	bl	__aeabi_i2f
	mov	r1, r0
	ldr	r0, [r4, #772]	@ float
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	bne	.L141
.L120:
	mov	r0, r6
	bl	free
	ldr	r0, [r4, #780]
	bl	free
	mov	r6, #0
	mov	r1, #5120
	mov	r2, r6
	mov	r3, r6
	add	r1, r1, #6
	mov	r0, #3
	str	r6, [r4, #764]
	str	r6, [r4, #780]
	bl	glVertexPointer
	mov	r0, #5120
	mov	r1, r6
	mov	r2, r6
	add	r0, r0, #6
	bl	glNormalPointer
	ldr	r6, [r4, #764]
	cmp	r6, #0
	beq	.L119
.L139:
	add	r8, r7, #1
.L121:
	mov	lr, #5120
	add	r1, lr, #6
	mov	r3, r6
	mov	r0, #3
	mov	r2, #0
	bl	glVertexPointer
	mov	ip, #5120
	add	r0, ip, #6
	ldr	r2, [r4, #780]
	mov	r1, #0
	bl	glNormalPointer
	mov	r1, #32768
	add	r0, r1, #116
	bl	glEnableClientState
	mov	r0, #32768
	add	r0, r0, #117
	bl	glEnableClientState
	cmp	r5, #0
	mov	r8, r8, asl #1
	ble	.L122
	mov	r0, #5
	mov	r1, #0
	mov	r2, r8
	sub	r7, r5, #1
	bl	glDrawArrays
	cmp	r5, #1
	and	r7, r7, #3
	mov	r6, #1
	mov	r4, r8
	ble	.L122
	cmp	r7, #0
	beq	.L123
	cmp	r7, #1
	beq	.L137
	cmp	r7, #2
	beq	.L138
	mov	r0, #5
	mov	r1, r8
	mov	r2, r8
	bl	glDrawArrays
	mov	r6, #2
	mov	r4, r8, asl #1
.L138:
	mov	r1, r4
	mov	r0, #5
	mov	r2, r8
	bl	glDrawArrays
	add	r6, r6, #1
	add	r4, r4, r8
.L137:
	mov	r1, r4
	add	r6, r6, #1
	mov	r0, #5
	mov	r2, r8
	bl	glDrawArrays
	cmp	r5, r6
	add	r4, r4, r8
	ble	.L122
.L123:
	add	sl, r4, r8
	mov	r1, r4
	mov	r0, #5
	mov	r2, r8
	bl	glDrawArrays
	add	r7, sl, r8
	mov	r0, #5
	mov	r1, sl
	mov	r2, r8
	bl	glDrawArrays
	add	r4, r7, r8
	mov	r0, #5
	mov	r1, r7
	mov	r2, r8
	bl	glDrawArrays
	add	r6, r6, #4
	mov	r1, r4
	mov	r0, #5
	mov	r2, r8
	bl	glDrawArrays
	cmp	r5, r6
	add	r4, r4, r8
	bgt	.L123
.L122:
	mov	r3, #32768
	add	r0, r3, #116
	bl	glDisableClientState
	mov	r2, #32768
	add	r0, r2, #117
	bl	glDisableClientState
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	bx	lr
.L141:
	mov	r0, r5
	bl	__aeabi_i2f
	mov	r1, r0
	ldr	r0, [r4, #776]	@ float
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	beq	.L120
	b	.L139
.L119:
	mov	r0, r7
	str	sl, [r4, #768]	@ float
	bl	__aeabi_i2f
	str	r0, [r4, #772]	@ float
	mov	r0, r5
	bl	__aeabi_i2f
	add	r8, r7, #1
	mul	r3, r8, r5
	add	r2, r3, r3, asl #1
	mov	r6, r2, asl #3
	str	r0, [r4, #776]	@ float
	mov	r0, r6
	bl	malloc
	str	r0, [r4, #764]
	mov	r0, r6
	bl	malloc
	ldr	r6, [r4, #764]
	mov	ip, r0
	str	ip, [sp, #0]
	mov	r0, sl
	mov	r2, r7
	mov	r1, r5
	mov	r3, r6
	str	ip, [r4, #780]
	bl	PlotSpherePoints
	b	.L121
.L143:
	.align	2
.L142:
	.word	.LANCHOR0
	.size	ugSolidSpheref, .-ugSolidSpheref
	.global	__aeabi_fcmple
	.section	.text.ugSolidTube,"ax",%progbits
	.align	2
	.global	ugSolidTube
	.hidden	ugSolidTube
	.type	ugSolidTube, %function
ugSolidTube:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	mov	r6, r1
	sub	sp, sp, #8
	mov	r1, #0
	mov	r4, r2
	mov	r7, r3
	mov	sl, r0
	bl	__aeabi_fcmple
	cmp	r0, #0
	bne	.L154
	mov	r1, #0
	mov	r0, r6
	bl	__aeabi_fcmple
	cmp	r4, #0
	movgt	r1, #0
	movle	r1, #1
	cmp	r0, #0
	mov	r2, #0
	movne	r2, #1
	orr	r3, r1, r2
	tst	r3, #1
	bne	.L154
	cmp	r7, #2
	ble	.L154
	ldr	r5, .L174
	ldr	r8, [r5, #784]
	cmp	r8, #0
	beq	.L148
	ldr	r0, [r5, #788]	@ float
	mov	r1, sl
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	beq	.L149
	ldr	r0, [r5, #792]	@ float
	mov	r1, r6
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	beq	.L149
	mov	r0, r4
	bl	__aeabi_i2f
	mov	r1, r0
	ldr	r0, [r5, #796]	@ float
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	bne	.L172
.L149:
	mov	r0, r8
	bl	free
	mov	r8, #0
	ldr	r0, [r5, #804]
	mov	r9, #5120
	bl	free
	mov	r2, r8
	mov	r3, r8
	add	r1, r9, #6
	mov	r0, #3
	str	r8, [r5, #804]
	str	r8, [r5, #784]
	bl	glVertexPointer
	mov	r0, #5120
	mov	r1, r8
	mov	r2, r8
	add	r0, r0, #6
	bl	glNormalPointer
	ldr	r8, [r5, #784]
	cmp	r8, #0
	beq	.L148
.L170:
	add	r9, r7, #1
.L150:
	mov	lr, #5120
	add	r1, lr, #6
	mov	r3, r8
	mov	r0, #3
	mov	r2, #0
	bl	glVertexPointer
	mov	r0, #5120
	ldr	r2, [r5, #804]
	mov	r1, #0
	add	r0, r0, #6
	mov	r6, #32768
	bl	glNormalPointer
	mov	r5, #32768
	add	r0, r6, #117
	bl	glEnableClientState
	mov	r9, r9, asl #1
	add	r0, r5, #116
	bl	glEnableClientState
	mov	r0, #5
	mov	r1, #0
	mov	r2, r9
	sub	r7, r4, #1
	bl	glDrawArrays
	cmp	r4, #1
	and	r7, r7, #3
	mov	r6, #1
	mov	r5, r9
	ble	.L169
	cmp	r7, #0
	beq	.L153
	cmp	r7, #1
	beq	.L167
	cmp	r7, #2
	beq	.L168
	mov	r0, #5
	mov	r1, r9
	mov	r2, r9
	bl	glDrawArrays
	mov	r6, #2
	mov	r5, r9, asl #1
.L168:
	mov	r1, r5
	mov	r0, #5
	mov	r2, r9
	bl	glDrawArrays
	add	r6, r6, #1
	add	r5, r5, r9
.L167:
	mov	r1, r5
	add	r6, r6, #1
	mov	r0, #5
	mov	r2, r9
	bl	glDrawArrays
	cmp	r4, r6
	add	r5, r5, r9
	ble	.L169
.L153:
	add	r8, r5, r9
	mov	r1, r5
	mov	r0, #5
	mov	r2, r9
	bl	glDrawArrays
	add	r7, r8, r9
	mov	r0, #5
	mov	r1, r8
	mov	r2, r9
	bl	glDrawArrays
	add	r5, r7, r9
	mov	r0, #5
	mov	r1, r7
	mov	r2, r9
	bl	glDrawArrays
	add	r6, r6, #4
	mov	r1, r5
	mov	r0, #5
	mov	r2, r9
	bl	glDrawArrays
	cmp	r4, r6
	add	r5, r5, r9
	bgt	.L153
.L169:
	mov	ip, #32768
	add	r0, ip, #116
	bl	glDisableClientState
	mov	r2, #32768
	add	r0, r2, #117
	bl	glDisableClientState
.L154:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L172:
	mov	r0, r7
	bl	__aeabi_i2f
	mov	r1, r0
	ldr	r0, [r5, #800]	@ float
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	beq	.L149
	b	.L170
.L148:
	mov	r0, r4
	str	sl, [r5, #788]	@ float
	str	r6, [r5, #792]	@ float
	bl	__aeabi_i2f
	str	r0, [r5, #796]	@ float
	mov	r0, r7
	bl	__aeabi_i2f
	add	r3, r4, r4, asl #1
	mov	ip, r3, asl #1
	add	r9, r7, #1
	mul	ip, r9, ip
	mov	r8, ip, asl #2
	str	r0, [r5, #800]	@ float
	mov	r0, r8
	bl	malloc
	str	r0, [r5, #784]
	mov	r0, r8
	bl	malloc
	ldr	r8, [r5, #784]
	cmp	r8, #0
	str	r0, [r5, #804]
	mov	ip, r0
	beq	.L151
	cmp	r0, #0
	beq	.L173
	mov	r0, sl
	mov	r1, r6
	mov	r3, r7
	mov	r2, r4
	stmia	sp, {r8, ip}	@ phole stm
	bl	PlotTubePoints
	b	.L150
.L173:
	mov	r0, r8
	bl	free
	ldr	r1, .L174
	ldr	r0, [r1, #804]
.L151:
	cmp	r0, #0
	beq	.L154
	bl	free
	b	.L154
.L175:
	.align	2
.L174:
	.word	.LANCHOR0
	.size	ugSolidTube, .-ugSolidTube
	.section	.text.ugWireTube,"ax",%progbits
	.align	2
	.global	ugWireTube
	.hidden	ugWireTube
	.type	ugWireTube, %function
ugWireTube:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	mov	r5, r1
	sub	sp, sp, #12
	mov	r1, #0
	mov	r9, r2
	mov	r4, r3
	mov	r6, r0
	bl	__aeabi_fcmple
	cmp	r0, #0
	bne	.L187
	mov	r1, #0
	mov	r0, r5
	bl	__aeabi_fcmple
	cmp	r9, #0
	movgt	r1, #0
	movle	r1, #1
	cmp	r0, #0
	mov	r2, #0
	movne	r2, #1
	orr	r3, r1, r2
	tst	r3, #1
	bne	.L187
	cmp	r4, #2
	ble	.L187
	ldr	r2, .L207
	ldr	r7, [r2, #808]
	cmp	r7, #0
	beq	.L180
	ldr	r0, [r2, #812]	@ float
	mov	r1, r6
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	beq	.L181
	ldr	fp, .L207
	mov	r1, r5
	ldr	r0, [fp, #816]	@ float
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	beq	.L181
	mov	r0, r9
	bl	__aeabi_i2f
	ldr	ip, .L207
	mov	r1, r0
	ldr	r0, [ip, #820]	@ float
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	bne	.L205
.L181:
	mov	r0, r7
	bl	free
	ldr	r2, .L207
	ldr	r0, [r2, #828]
	bl	free
	ldr	ip, .L207
	mov	fp, #0
	mov	r1, #5120
	add	r1, r1, #6
	mov	r0, #3
	mov	r2, fp
	mov	r3, fp
	str	fp, [ip, #828]
	str	fp, [ip, #808]
	mov	r7, #5120
	bl	glVertexPointer
	add	r0, r7, #6
	mov	r1, fp
	mov	r2, fp
	bl	glNormalPointer
	ldr	r0, .L207
	ldr	r7, [r0, #808]
	cmp	r7, #0
	beq	.L180
.L204:
	add	fp, r4, #1
.L182:
	mov	r0, #5120
	ldr	r8, .L207
	mov	r3, r7
	add	r1, r0, #6
	mov	r2, #0
	mov	r0, #3
	mov	sl, #5120
	bl	glVertexPointer
	mov	r6, #32768
	ldr	r2, [r8, #828]
	add	r0, sl, #6
	mov	r1, #0
	bl	glNormalPointer
	mov	r5, #32768
	add	r0, r6, #117
	bl	glEnableClientState
	add	r0, r5, #116
	bl	glEnableClientState
	mov	fp, fp, asl #1
	mov	r8, #1
	mov	sl, #0
.L186:
	mov	r2, #3
	sub	r7, r4, #1
	mov	r0, #2
	mov	r1, r8
	mov	r6, #1
	and	r7, r7, r2
	bl	glDrawArrays
	cmp	r6, r4
	add	r5, r8, #2
	bge	.L203
	cmp	r7, #0
	beq	.L185
	cmp	r7, #1
	beq	.L201
	cmp	r7, #2
	beq	.L202
	mov	r1, r5
	mov	r0, #2
	mov	r2, #3
	bl	glDrawArrays
	add	r5, r5, #2
	mov	r6, #2
.L202:
	mov	r1, r5
	mov	r0, #2
	mov	r2, #3
	bl	glDrawArrays
	add	r6, r6, #1
	add	r5, r5, #2
.L201:
	mov	r1, r5
	add	r6, r6, #1
	mov	r0, #2
	mov	r2, #3
	bl	glDrawArrays
	cmp	r6, r4
	add	r5, r5, #2
	bge	.L203
.L185:
	mov	r1, r5
	add	r7, r5, #2
	mov	r0, #2
	mov	r2, #3
	bl	glDrawArrays
	mov	r0, #2
	mov	r1, r7
	mov	r2, #3
	bl	glDrawArrays
	add	r1, r7, #2
	mov	r0, #2
	mov	r2, #3
	bl	glDrawArrays
	add	r6, r6, #4
	add	r1, r5, #6
	mov	r0, #2
	mov	r2, #3
	bl	glDrawArrays
	cmp	r6, r4
	add	r5, r5, #8
	blt	.L185
.L203:
	add	sl, sl, #1
	cmp	sl, r9
	add	r8, r8, fp
	blt	.L186
	ldr	lr, .L207
	mov	r0, #5120
	ldr	r3, [lr, #808]
	add	r1, r0, #6
	mov	r2, #24
	mov	r0, #3
	bl	glVertexPointer
	ldr	r3, .L207
	mov	ip, #5120
	add	r0, ip, #6
	ldr	r2, [r3, #828]
	mov	r1, #24
	bl	glNormalPointer
	mov	r1, #0
	mov	r2, r4
	mov	r0, #2
	bl	glDrawArrays
	mov	r2, #32768
	add	r0, r2, #116
	bl	glDisableClientState
	mov	r1, #32768
	add	r0, r1, #117
	bl	glDisableClientState
.L187:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L205:
	mov	r0, r4
	bl	__aeabi_i2f
	ldr	r3, .L207
	mov	r1, r0
	ldr	r0, [r3, #824]	@ float
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	beq	.L181
	b	.L204
.L180:
	ldr	lr, .L207
	mov	r0, r9
	str	r6, [lr, #812]	@ float
	str	r5, [lr, #816]	@ float
	bl	__aeabi_i2f
	ldr	r3, .L207
	str	r0, [r3, #820]	@ float
	mov	r0, r4
	bl	__aeabi_i2f
	add	fp, r9, r9, asl #1
	mov	r2, fp, asl #1
	add	fp, r4, #1
	mul	r2, fp, r2
	ldr	r1, .L207
	mov	r7, r2, asl #2
	str	r0, [r1, #824]	@ float
	mov	r0, r7
	bl	malloc
	ldr	ip, .L207
	str	r0, [ip, #808]
	mov	r0, r7
	bl	malloc
	ldr	r3, .L207
	ldr	r7, [r3, #808]
	cmp	r7, #0
	str	r0, [r3, #828]
	mov	ip, r0
	beq	.L183
	cmp	r0, #0
	beq	.L206
	mov	r0, r6
	mov	r1, r5
	mov	r2, r9
	mov	r3, r4
	stmia	sp, {r7, ip}	@ phole stm
	bl	PlotTubePoints
	b	.L182
.L206:
	mov	r0, r7
	bl	free
	ldr	ip, .L207
	ldr	r0, [ip, #828]
.L183:
	cmp	r0, #0
	beq	.L187
	bl	free
	b	.L187
.L208:
	.align	2
.L207:
	.word	.LANCHOR0
	.size	ugWireTube, .-ugWireTube
	.global	__aeabi_idivmod
	.section	.text.ugSolidTorusf,"ax",%progbits
	.align	2
	.global	ugSolidTorusf
	.hidden	ugSolidTorusf
	.type	ugSolidTorusf, %function
ugSolidTorusf:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 80
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	ip, .L242
	ldr	r4, [ip, #832]
	sub	sp, sp, #84
	cmp	r4, #0
	str	r0, [sp, #44]	@ float
	str	r1, [sp, #52]	@ float
	str	r2, [sp, #28]
	str	r3, [sp, #12]
	beq	.L210
	mov	r1, r0
	ldr	r0, [ip, #836]	@ float
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	beq	.L211
	ldr	r1, .L242
	ldr	r0, [r1, #840]	@ float
	ldr	r1, [sp, #52]	@ float
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	bne	.L241
.L211:
	mov	r0, r4
	bl	free
	ldr	r2, .L242
	ldr	r0, [r2, #852]
	bl	free
	ldr	ip, .L242
	mov	r4, #0
	mov	r1, #5120
	mov	r2, r4
	mov	r3, r4
	str	r4, [ip, #832]
	str	r4, [ip, #852]
	add	r1, r1, #6
	mov	r0, #3
	bl	glVertexPointer
	mov	r1, r4
	mov	r4, #5120
	add	r0, r4, #6
	mov	r2, r1
	bl	glNormalPointer
	ldr	r0, .L242
	ldr	r4, [r0, #832]
	cmp	r4, #0
	beq	.L210
.L212:
	mov	lr, #5120
	add	r1, lr, #6
	mov	r3, r4
	mov	r0, #3
	mov	r2, #0
	bl	glVertexPointer
	ldr	r3, .L242
	mov	r1, #5120
	ldr	r2, [r3, #852]
	add	r0, r1, #6
	mov	r1, #0
	bl	glNormalPointer
	mov	ip, #32768
	add	r0, ip, #116
	bl	glEnableClientState
	mov	r0, #32768
	add	r0, r0, #117
	bl	glEnableClientState
	ldr	r1, [sp, #28]
	cmp	r1, #0
	ble	.L220
	ldr	r6, [sp, #12]
	add	r4, r6, #1
	mov	r4, r4, asl #1
.L221:
	ldr	r2, [sp, #28]
	mov	r0, #5
	sub	r7, r2, #1
	mov	r1, #0
	mov	r2, r4
	bl	glDrawArrays
	ldr	r6, [sp, #28]
	cmp	r6, #1
	and	r7, r7, #3
	mov	r6, #1
	mov	r5, r4
	ble	.L220
	cmp	r7, #0
	beq	.L239
	cmp	r7, #1
	beq	.L237
	cmp	r7, #2
	beq	.L238
	mov	r0, #5
	mov	r1, r4
	mov	r2, r4
	bl	glDrawArrays
	mov	r6, #2
	mov	r5, r4, asl #1
.L238:
	mov	r1, r5
	mov	r0, #5
	mov	r2, r4
	bl	glDrawArrays
	add	r6, r6, #1
	add	r5, r5, r4
.L237:
	mov	r1, r5
	mov	r2, r4
	mov	r0, #5
	bl	glDrawArrays
	ldr	r2, [sp, #28]
	add	r6, r6, #1
	cmp	r2, r6
	add	r5, r5, r4
	movgt	sl, r2
	movgt	r1, r5
	ble	.L220
.L222:
	add	r8, r1, r4
	mov	r0, #5
	mov	r2, r4
	bl	glDrawArrays
	add	r7, r8, r4
	mov	r0, #5
	mov	r1, r8
	mov	r2, r4
	bl	glDrawArrays
	add	r5, r7, r4
	mov	r0, #5
	mov	r1, r7
	mov	r2, r4
	bl	glDrawArrays
	add	r6, r6, #4
	mov	r1, r5
	mov	r0, #5
	mov	r2, r4
	bl	glDrawArrays
	cmp	sl, r6
	add	r1, r5, r4
	bgt	.L222
.L220:
	mov	r0, #32768
	add	r0, r0, #116
	bl	glDisableClientState
	mov	r2, #32768
	add	r0, r2, #117
	bl	glDisableClientState
	add	sp, sp, #84
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L241:
	ldr	r0, [sp, #28]
	bl	__aeabi_i2f
	ldr	r2, .L242
	mov	r1, r0
	ldr	r0, [r2, #844]	@ float
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	beq	.L211
	ldr	r0, [sp, #12]
	bl	__aeabi_i2f
	ldr	r3, .L242
	mov	r1, r0
	ldr	r0, [r3, #848]	@ float
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	bne	.L212
	b	.L211
.L239:
	mov	r1, r5
	ldr	sl, [sp, #28]
	b	.L222
.L210:
	ldr	lr, [sp, #52]	@ float
	ldr	ip, [sp, #44]	@ float
	ldr	r5, .L242
	ldr	r0, [sp, #28]
	str	lr, [r5, #840]	@ float
	str	ip, [r5, #836]	@ float
	bl	__aeabi_i2f
	ldr	r4, .L242
	mov	r5, r0
	str	r5, [r4, #844]	@ float
	ldr	r0, [sp, #12]
	bl	__aeabi_i2f
	ldr	r2, [sp, #28]
	mov	r4, r0
	ldr	r0, [sp, #12]
	add	r1, r2, r2, asl #1
	add	r3, r0, #1
	mov	r6, r1, asl #1
	mul	r6, r3, r6
	ldr	ip, .L242
	mov	r6, r6, asl #2
	str	r4, [ip, #848]	@ float
	mov	r0, r6
	str	r3, [sp, #76]
	bl	malloc
	str	r0, [sp, #60]
	ldr	r1, [sp, #60]
	ldr	r2, .L242
	mov	r0, r6
	str	r1, [r2, #832]
	bl	malloc
	str	r0, [sp, #56]
	ldr	r2, [sp, #56]
	ldr	ip, .L242
	mov	r0, #13172736
	add	r3, r0, #4048
	mov	r1, r5
	add	r0, r3, #1073741835
	str	r2, [ip, #852]
	bl	__aeabi_fdiv
	str	r0, [sp, #68]	@ float
	mov	r0, #13172736
	add	r3, r0, #4048
	mov	r1, r4
	add	r0, r3, #1073741835
	bl	__aeabi_fdiv
	ldr	r1, [sp, #28]
	cmp	r1, #0
	str	r0, [sp, #40]	@ float
	ldr	r4, .L242
	ble	.L214
	ldr	r0, [sp, #76]
	add	r3, r0, r0, asl #1
	mov	r6, r3, asl #3
	mov	r7, #1
	str	r6, [sp, #72]
	str	r7, [sp, #48]
.L215:
	ldr	r7, [sp, #12]
	ldr	ip, [sp, #48]
	cmp	r7, #0
	sub	r4, ip, #1
	strlt	ip, [sp, #64]
	movlt	r1, ip
	blt	.L219
	mov	r0, ip
	ldr	r1, [sp, #28]
	bl	__aeabi_idivmod
	mov	r0, r1
	bl	__aeabi_i2f
	mov	r1, #1056964608
	bl	__aeabi_fadd
	mov	r1, r0
	ldr	r0, [sp, #68]	@ float
	bl	__aeabi_fmul
	bl	__aeabi_f2d
	mov	fp, r0
	mov	sl, r1
	bl	cos
	mov	r9, r1
	mov	r7, r0
	mov	r1, sl
	mov	r0, fp
	bl	sin
	mov	fp, r1
	mov	r6, r0
	ldr	r1, [sp, #28]
	mov	r0, r4
	bl	__aeabi_idivmod
	mov	r0, r1
	bl	__aeabi_i2f
	mov	r1, #1056964608
	bl	__aeabi_fadd
	mov	r1, r0
	ldr	r0, [sp, #68]	@ float
	bl	__aeabi_fmul
	bl	__aeabi_f2d
	mov	r5, r0
	mov	r8, r1
	bl	cos
	mov	r2, r0
	mov	r3, r1
	mov	r0, r5
	mov	r1, r8
	str	r2, [sp, #8]
	str	r3, [sp, #4]
	bl	sin
	mov	r4, r0
	mov	r5, r1
	mov	r0, r7
	mov	r1, r9
	bl	__aeabi_d2f
	mov	r1, fp
	mov	r9, r0
	mov	r0, r6
	bl	__aeabi_d2f
	mov	r1, r9
	str	r0, [sp, #24]	@ float
	ldr	r0, [sp, #44]	@ float
	bl	__aeabi_fmul
	ldr	r1, [sp, #52]	@ float
	bl	__aeabi_fadd
	ldr	r1, [sp, #24]	@ float
	mov	fp, r0
	ldr	r0, [sp, #44]	@ float
	bl	__aeabi_fmul
	ldmib	sp, {sl, ip}	@ phole ldm
	str	r0, [sp, #36]	@ float
	mov	r1, sl
	mov	r0, ip
	bl	__aeabi_d2f
	mov	r1, r5
	mov	sl, r0
	mov	r0, r4
	bl	__aeabi_d2f
	mov	r1, sl
	str	r0, [sp, #20]	@ float
	ldr	r0, [sp, #44]	@ float
	bl	__aeabi_fmul
	ldr	r1, [sp, #52]	@ float
	bl	__aeabi_fadd
	str	r0, [sp, #16]	@ float
	ldr	r0, [sp, #44]	@ float
	ldr	r1, [sp, #20]	@ float
	bl	__aeabi_fmul
	ldr	r8, [sp, #48]
	str	r0, [sp, #32]	@ float
	ldr	r5, [sp, #56]
	ldr	r4, [sp, #60]
	str	r8, [sp, #64]
	mov	r8, #0
.L216:
	ldr	r1, [sp, #12]
	mov	r0, r8
	bl	__aeabi_idivmod
	mov	r0, r1
	bl	__aeabi_i2f
	ldr	r1, [sp, #40]	@ float
	bl	__aeabi_fmul
	bl	__aeabi_f2d
	str	r0, [sp, #8]
	str	r1, [sp, #4]
	bl	cos
	bl	__aeabi_d2f
	ldr	r2, [sp, #8]
	ldr	r6, [sp, #4]
	mov	r7, r0
	mov	r1, r6
	mov	r0, r2
	bl	sin
	bl	__aeabi_d2f
	mov	r1, r7
	mov	r6, r0
	mov	r0, fp
	bl	__aeabi_fmul
	mov	r1, fp
	str	r0, [r4, #0]	@ float
	mov	r0, r6
	bl	__aeabi_fmul
	str	r0, [r4, #4]	@ float
	ldr	r0, [sp, #36]	@ float
	mov	r1, r7
	str	r0, [r4, #8]	@ float
	mov	r0, r9
	bl	__aeabi_fmul
	mov	r1, r6
	str	r0, [r5, #0]	@ float
	mov	r0, r9
	bl	__aeabi_fmul
	str	r0, [r5, #4]	@ float
	ldr	r3, [sp, #24]	@ float
	str	r3, [r5, #8]	@ float
	ldr	r1, [sp, #16]	@ float
	mov	r0, r7
	bl	__aeabi_fmul
	str	r0, [r4, #12]	@ float
	ldr	r1, [sp, #16]	@ float
	mov	r0, r6
	bl	__aeabi_fmul
	str	r0, [r4, #16]	@ float
	ldr	ip, [sp, #32]	@ float
	mov	r1, sl
	str	ip, [r4, #20]	@ float
	mov	r0, r7
	bl	__aeabi_fmul
	mov	r1, sl
	str	r0, [r5, #12]	@ float
	mov	r0, r6
	bl	__aeabi_fmul
	ldr	r2, [sp, #12]
	str	r0, [r5, #16]	@ float
	add	r8, r8, #1
	ldr	r1, [sp, #20]	@ float
	cmp	r2, r8
	str	r1, [r5, #20]	@ float
	add	r4, r4, #24
	add	r5, r5, #24
	bge	.L216
	ldr	r3, [sp, #60]
	ldr	lr, [sp, #72]
	ldr	r5, [sp, #56]
	add	r1, r3, lr
	add	r4, r5, lr
	str	r1, [sp, #60]
	str	r4, [sp, #56]
	ldr	r1, [sp, #64]
.L219:
	ldr	r6, [sp, #48]
	ldr	r2, [sp, #28]
	add	r0, r6, #1
	cmp	r2, r1
	str	r0, [sp, #48]
	bgt	.L215
	ldr	lr, .L242
	mov	r4, #5120
	ldr	r3, [lr, #832]
	add	r1, r4, #6
	mov	r0, #3
	mov	r2, #0
	bl	glVertexPointer
	ldr	r3, .L242
	mov	r1, #5120
	ldr	r2, [r3, #852]
	add	r0, r1, #6
	mov	r1, #0
	bl	glNormalPointer
	mov	ip, #32768
	add	r0, ip, #116
	mov	r5, #32768
	bl	glEnableClientState
	add	r0, r5, #117
	bl	glEnableClientState
	ldr	r7, [sp, #76]
	mov	r4, r7, asl #1
	b	.L221
.L214:
	mov	r3, #5120
	add	r1, r3, #6
	mov	r0, #3
	ldr	r3, [r4, #832]
	mov	r2, #0
	bl	glVertexPointer
	mov	r1, #5120
	ldr	r2, [r4, #852]
	add	r0, r1, #6
	mov	r1, #0
	bl	glNormalPointer
	mov	ip, #32768
	add	r0, ip, #116
	bl	glEnableClientState
	mov	r0, #32768
	add	r0, r0, #117
	bl	glEnableClientState
	b	.L220
.L243:
	.align	2
.L242:
	.word	.LANCHOR0
	.size	ugSolidTorusf, .-ugSolidTorusf
	.global	__aeabi_fcmplt
	.section	.text.ugSolidDisk,"ax",%progbits
	.align	2
	.global	ugSolidDisk
	.hidden	ugSolidDisk
	.type	ugSolidDisk, %function
ugSolidDisk:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	cmp	r3, #2
	cmple	r2, #0
	sub	sp, sp, #12
	mov	r6, r3
	mov	r4, r2
	mov	r8, r1
	mov	r7, r0
	bgt	.L245
	mov	r1, #0
	bl	__aeabi_fcmplt
	cmp	r0, #0
	bne	.L271
.L245:
	ldr	r5, .L274
	ldr	sl, [r5, #856]
	cmp	sl, #0
	beq	.L248
.L273:
	ldr	r0, [r5, #860]	@ float
	mov	r1, r7
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	beq	.L249
	ldr	r0, [r5, #864]	@ float
	mov	r1, r8
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	bne	.L272
.L249:
	mov	r0, sl
	bl	free
	mov	ip, #0
	mov	r1, #5120
	mov	r3, ip
	mov	r2, ip
	add	r1, r1, #6
	mov	r0, #3
	str	ip, [r5, #856]
	bl	glVertexPointer
	ldr	r3, [r5, #856]
	cmp	r3, #0
	beq	.L248
.L269:
	add	sl, r6, #1
.L250:
	mov	r0, #0
	mov	r1, r0
	mov	r2, #1065353216
	bl	glNormal3f
	mov	r3, #5120
	add	r1, r3, #6
	mov	r0, #3
	ldr	r3, [r5, #856]
	mov	r2, #0
	bl	glVertexPointer
	mov	r0, #32768
	add	r0, r0, #116
	bl	glEnableClientState
	cmp	r4, #0
	mov	sl, sl, asl #1
	ble	.L251
	mov	r0, #5
	mov	r1, #0
	mov	r2, sl
	sub	r7, r4, #1
	bl	glDrawArrays
	cmp	r4, #1
	and	r7, r7, #3
	mov	r6, #1
	mov	r5, sl
	ble	.L251
	cmp	r7, #0
	beq	.L252
	cmp	r7, #1
	beq	.L267
	cmp	r7, #2
	beq	.L268
	mov	r0, #5
	mov	r1, sl
	mov	r2, sl
	bl	glDrawArrays
	mov	r6, #2
	mov	r5, sl, asl #1
.L268:
	mov	r1, r5
	mov	r0, #5
	mov	r2, sl
	bl	glDrawArrays
	add	r6, r6, #1
	add	r5, r5, sl
.L267:
	mov	r1, r5
	add	r6, r6, #1
	mov	r0, #5
	mov	r2, sl
	bl	glDrawArrays
	cmp	r4, r6
	add	r5, r5, sl
	ble	.L251
.L252:
	add	r8, r5, sl
	mov	r1, r5
	mov	r0, #5
	mov	r2, sl
	bl	glDrawArrays
	add	r7, r8, sl
	mov	r0, #5
	mov	r1, r8
	mov	r2, sl
	bl	glDrawArrays
	add	r5, r7, sl
	mov	r0, #5
	mov	r1, r7
	mov	r2, sl
	bl	glDrawArrays
	add	r6, r6, #4
	mov	r1, r5
	mov	r0, #5
	mov	r2, sl
	bl	glDrawArrays
	cmp	r4, r6
	add	r5, r5, sl
	bgt	.L252
.L251:
	mov	lr, #32768
	add	r0, lr, #116
	bl	glDisableClientState
.L253:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	bx	lr
.L272:
	mov	r0, r4
	bl	__aeabi_i2f
	mov	r1, r0
	ldr	r0, [r5, #868]	@ float
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	beq	.L249
	mov	r0, r6
	bl	__aeabi_i2f
	mov	r1, r0
	ldr	r0, [r5, #872]	@ float
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	beq	.L249
	b	.L269
.L271:
	mov	r0, r8
	mov	r1, r7
	bl	__aeabi_fcmple
	cmp	r0, #0
	bne	.L253
	ldr	r5, .L274
	ldr	sl, [r5, #856]
	cmp	sl, #0
	bne	.L273
.L248:
	mov	r0, r4
	str	r7, [r5, #860]	@ float
	str	r8, [r5, #864]	@ float
	bl	__aeabi_i2f
	str	r0, [r5, #868]	@ float
	mov	r0, r6
	bl	__aeabi_i2f
	add	sl, r4, r4, asl #1
	mov	r2, sl, asl #1
	add	sl, r6, #1
	mul	r2, sl, r2
	str	r0, [r5, #872]	@ float
	mov	r0, r2, asl #2
	bl	malloc
	cmp	r0, #0
	mov	ip, r0
	str	r0, [r5, #856]
	beq	.L253
	mov	r0, r7
	mov	r1, r8
	mov	r3, r6
	mov	r2, r4
	str	ip, [sp, #0]
	bl	PlotDiskPoints
	b	.L250
.L275:
	.align	2
.L274:
	.word	.LANCHOR0
	.size	ugSolidDisk, .-ugSolidDisk
	.section	.text.ugWireDisk,"ax",%progbits
	.align	2
	.global	ugWireDisk
	.hidden	ugWireDisk
	.type	ugWireDisk, %function
ugWireDisk:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	cmp	r3, #2
	cmple	r2, #0
	sub	sp, sp, #12
	mov	r4, r3
	mov	r9, r2
	mov	r6, r1
	mov	r5, r0
	bgt	.L277
	mov	r1, #0
	bl	__aeabi_fcmplt
	cmp	r0, #0
	bne	.L304
.L277:
	ldr	r2, .L307
	ldr	r7, [r2, #876]
	cmp	r7, #0
	beq	.L280
.L306:
	ldr	r0, [r2, #880]	@ float
	mov	r1, r5
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	beq	.L281
	ldr	r3, .L307
	mov	r1, r6
	ldr	r0, [r3, #884]	@ float
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	bne	.L305
.L281:
	mov	r0, r7
	bl	free
	ldr	lr, .L307
	mov	ip, #0
	mov	r1, #5120
	mov	r2, ip
	mov	r0, #3
	add	r1, r1, #6
	mov	r3, ip
	str	ip, [lr, #876]
	bl	glVertexPointer
	ldr	r2, .L307
	ldr	r0, [r2, #876]
	cmp	r0, #0
	beq	.L280
.L282:
	mov	r0, #0
	mov	r1, r0
	mov	r2, #1065353216
	bl	glNormal3f
	ldr	ip, .L307
	mov	r2, #5120
	add	r1, r2, #6
	mov	r0, #3
	mov	r2, #0
	ldr	r3, [ip, #876]
	bl	glVertexPointer
	mov	r0, #32768
	add	r0, r0, #116
	bl	glEnableClientState
	cmp	r9, #0
	ble	.L283
	add	fp, r4, #1
	mov	fp, fp, asl #1
	mov	r8, #1
	mov	sl, #0
.L286:
	cmp	r4, #0
	ble	.L284
	mov	r2, #3
	sub	r7, r4, #1
	mov	r0, #2
	mov	r1, r8
	mov	r6, #1
	and	r7, r7, r2
	bl	glDrawArrays
	cmp	r6, r4
	add	r5, r8, #2
	bge	.L284
	cmp	r7, #0
	beq	.L285
	cmp	r7, #1
	beq	.L302
	cmp	r7, #2
	beq	.L303
	mov	r1, r5
	mov	r0, #2
	mov	r2, #3
	bl	glDrawArrays
	add	r5, r5, #2
	mov	r6, #2
.L303:
	mov	r1, r5
	mov	r0, #2
	mov	r2, #3
	bl	glDrawArrays
	add	r6, r6, #1
	add	r5, r5, #2
.L302:
	mov	r1, r5
	add	r6, r6, #1
	mov	r0, #2
	mov	r2, #3
	bl	glDrawArrays
	cmp	r6, r4
	add	r5, r5, #2
	bge	.L284
.L285:
	mov	r1, r5
	add	r7, r5, #2
	mov	r0, #2
	mov	r2, #3
	bl	glDrawArrays
	mov	r0, #2
	mov	r1, r7
	mov	r2, #3
	bl	glDrawArrays
	add	r1, r7, #2
	mov	r0, #2
	mov	r2, #3
	bl	glDrawArrays
	add	r6, r6, #4
	add	r1, r5, #6
	mov	r0, #2
	mov	r2, #3
	bl	glDrawArrays
	cmp	r6, r4
	add	r5, r5, #8
	blt	.L285
.L284:
	add	sl, sl, #1
	cmp	sl, r9
	add	r8, r8, fp
	blt	.L286
.L283:
	ldr	lr, .L307
	mov	r3, #5120
	add	r1, r3, #6
	mov	r0, #3
	ldr	r3, [lr, #876]
	mov	r2, #24
	bl	glVertexPointer
	mov	r0, #2
	mov	r1, #0
	mov	r2, r4
	bl	glDrawArrays
	mov	r1, #32768
	add	r0, r1, #116
	bl	glDisableClientState
.L287:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L305:
	mov	r0, r9
	bl	__aeabi_i2f
	ldr	ip, .L307
	mov	r1, r0
	ldr	r0, [ip, #888]	@ float
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	beq	.L281
	mov	r0, r4
	bl	__aeabi_i2f
	ldr	lr, .L307
	mov	r1, r0
	ldr	r0, [lr, #892]	@ float
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	bne	.L282
	b	.L281
.L304:
	mov	r0, r6
	mov	r1, r5
	bl	__aeabi_fcmple
	cmp	r0, #0
	bne	.L287
	ldr	r2, .L307
	ldr	r7, [r2, #876]
	cmp	r7, #0
	bne	.L306
.L280:
	ldr	lr, .L307
	mov	r0, r9
	str	r5, [lr, #880]	@ float
	str	r6, [lr, #884]	@ float
	bl	__aeabi_i2f
	ldr	r3, .L307
	str	r0, [r3, #888]	@ float
	mov	r0, r4
	bl	__aeabi_i2f
	add	r1, r9, r9, asl #1
	mov	r2, r1, asl #1
	mla	r2, r4, r2, r2
	ldr	ip, .L307
	str	r0, [ip, #892]	@ float
	mov	r0, r2, asl #2
	bl	malloc
	ldr	r3, .L307
	cmp	r0, #0
	mov	ip, r0
	str	r0, [r3, #876]
	beq	.L287
	mov	r0, r5
	mov	r1, r6
	mov	r2, r9
	mov	r3, r4
	str	ip, [sp, #0]
	bl	PlotDiskPoints
	b	.L282
.L308:
	.align	2
.L307:
	.word	.LANCHOR0
	.size	ugWireDisk, .-ugWireDisk
	.section	.text.ugWireBox,"ax",%progbits
	.align	2
	.global	ugWireBox
	.hidden	ugWireBox
	.type	ugWireBox, %function
ugWireBox:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 96
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	mov	r4, r1
	sub	sp, sp, #100
	mov	r1, #-1090519040
	mov	r7, r2
	mov	r5, r0
	bl	__aeabi_fmul
	mov	r1, #1056964608
	mov	ip, r0
	mov	r0, r5
	str	ip, [sp, #0]	@ float
	str	ip, [sp, #36]	@ float
	str	ip, [sp, #24]	@ float
	str	ip, [sp, #12]	@ float
	bl	__aeabi_fmul
	mov	r1, #-1090519040
	mov	r6, r0
	mov	r0, r4
	str	r6, [sp, #48]	@ float
	str	r6, [sp, #84]	@ float
	str	r6, [sp, #72]	@ float
	str	r6, [sp, #60]	@ float
	bl	__aeabi_fmul
	mov	r1, #1056964608
	mov	r5, r0
	mov	r0, r4
	str	r5, [sp, #4]	@ float
	str	r5, [sp, #64]	@ float
	str	r5, [sp, #52]	@ float
	str	r5, [sp, #16]	@ float
	bl	__aeabi_fmul
	mov	r1, #-1090519040
	mov	r2, r0
	mov	r0, r7
	str	r2, [sp, #28]	@ float
	str	r2, [sp, #88]	@ float
	str	r2, [sp, #76]	@ float
	str	r2, [sp, #40]	@ float
	bl	__aeabi_fmul
	mov	r1, #1056964608
	mov	r3, r0
	mov	r0, r7
	str	r3, [sp, #8]	@ float
	str	r3, [sp, #92]	@ float
	str	r3, [sp, #56]	@ float
	str	r3, [sp, #44]	@ float
	bl	__aeabi_fmul
	mov	r6, #5120
	mov	r7, #32768
	mov	ip, r0
	ldr	r4, .L311
	mov	r3, sp
	add	r1, r6, #6
	add	r7, r7, #116
	mov	r2, #0
	mov	r0, #3
	str	ip, [sp, #20]	@ float
	str	ip, [sp, #80]	@ float
	str	ip, [sp, #68]	@ float
	str	ip, [sp, #32]	@ float
	ldr	r5, .L311+4
	bl	glVertexPointer
	mov	r0, r7
	add	r6, r6, #3
	bl	glEnableClientState
	ldr	r0, [r4, #1440]	@ float
	ldr	r1, [r4, #1444]	@ float
	ldr	r2, [r4, #1448]	@ float
	bl	glNormal3f
	mov	r3, r5
	mov	r2, r6
	mov	r0, #2
	mov	r1, #4
	bl	glDrawElements
	ldr	r0, [r4, #1452]	@ float
	ldr	r1, [r4, #1456]	@ float
	ldr	r2, [r4, #1460]	@ float
	bl	glNormal3f
	add	r3, r5, #8
	mov	r2, r6
	mov	r0, #2
	mov	r1, #4
	bl	glDrawElements
	ldr	r0, [r4, #1464]	@ float
	ldr	r1, [r4, #1468]	@ float
	ldr	r2, [r4, #1472]	@ float
	bl	glNormal3f
	add	r3, r5, #16
	mov	r2, r6
	mov	r0, #2
	mov	r1, #4
	bl	glDrawElements
	ldr	r0, [r4, #1476]	@ float
	ldr	r1, [r4, #1480]	@ float
	ldr	r2, [r4, #1484]	@ float
	bl	glNormal3f
	add	r3, r5, #24
	mov	r2, r6
	mov	r0, #2
	mov	r1, #4
	bl	glDrawElements
	ldr	r0, [r4, #1488]	@ float
	ldr	r1, [r4, #1492]	@ float
	ldr	r2, [r4, #1496]	@ float
	bl	glNormal3f
	add	r3, r5, #32
	mov	r2, r6
	mov	r0, #2
	mov	r1, #4
	bl	glDrawElements
	ldr	r2, [r4, #1508]	@ float
	ldr	r0, [r4, #1500]	@ float
	ldr	r1, [r4, #1504]	@ float
	bl	glNormal3f
	mov	r2, r6
	add	r3, r5, #40
	mov	r0, #2
	mov	r1, #4
	bl	glDrawElements
	mov	r0, r7
	bl	glDisableClientState
	add	sp, sp, #100
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L312:
	.align	2
.L311:
	.word	.LANCHOR1
	.word	.LANCHOR2
	.size	ugWireBox, .-ugWireBox
	.section	.text.ugSolidBox,"ax",%progbits
	.align	2
	.global	ugSolidBox
	.hidden	ugSolidBox
	.type	ugSolidBox, %function
ugSolidBox:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 96
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	mov	r4, r1
	sub	sp, sp, #100
	mov	r1, #-1090519040
	mov	r7, r2
	mov	r5, r0
	bl	__aeabi_fmul
	mov	r1, #1056964608
	mov	ip, r0
	mov	r0, r5
	str	ip, [sp, #0]	@ float
	str	ip, [sp, #36]	@ float
	str	ip, [sp, #24]	@ float
	str	ip, [sp, #12]	@ float
	bl	__aeabi_fmul
	mov	r1, #-1090519040
	mov	r6, r0
	mov	r0, r4
	str	r6, [sp, #48]	@ float
	str	r6, [sp, #84]	@ float
	str	r6, [sp, #72]	@ float
	str	r6, [sp, #60]	@ float
	bl	__aeabi_fmul
	mov	r1, #1056964608
	mov	r5, r0
	mov	r0, r4
	str	r5, [sp, #4]	@ float
	str	r5, [sp, #64]	@ float
	str	r5, [sp, #52]	@ float
	str	r5, [sp, #16]	@ float
	bl	__aeabi_fmul
	mov	r1, #-1090519040
	mov	r2, r0
	mov	r0, r7
	str	r2, [sp, #28]	@ float
	str	r2, [sp, #88]	@ float
	str	r2, [sp, #76]	@ float
	str	r2, [sp, #40]	@ float
	bl	__aeabi_fmul
	mov	r1, #1056964608
	mov	r3, r0
	mov	r0, r7
	str	r3, [sp, #8]	@ float
	str	r3, [sp, #92]	@ float
	str	r3, [sp, #56]	@ float
	str	r3, [sp, #44]	@ float
	bl	__aeabi_fmul
	mov	r6, #5120
	mov	r7, #32768
	mov	ip, r0
	ldr	r4, .L315
	mov	r3, sp
	add	r1, r6, #6
	add	r7, r7, #116
	mov	r2, #0
	mov	r0, #3
	str	ip, [sp, #20]	@ float
	str	ip, [sp, #80]	@ float
	str	ip, [sp, #68]	@ float
	str	ip, [sp, #32]	@ float
	ldr	r5, .L315+4
	bl	glVertexPointer
	mov	r0, r7
	add	r6, r6, #3
	bl	glEnableClientState
	ldr	r0, [r4, #1440]	@ float
	ldr	r1, [r4, #1444]	@ float
	ldr	r2, [r4, #1448]	@ float
	bl	glNormal3f
	add	r3, r5, #48
	mov	r2, r6
	mov	r0, #4
	mov	r1, #6
	bl	glDrawElements
	ldr	r0, [r4, #1452]	@ float
	ldr	r1, [r4, #1456]	@ float
	ldr	r2, [r4, #1460]	@ float
	bl	glNormal3f
	add	r3, r5, #60
	mov	r2, r6
	mov	r0, #4
	mov	r1, #6
	bl	glDrawElements
	ldr	r0, [r4, #1464]	@ float
	ldr	r1, [r4, #1468]	@ float
	ldr	r2, [r4, #1472]	@ float
	bl	glNormal3f
	add	r3, r5, #72
	mov	r2, r6
	mov	r0, #4
	mov	r1, #6
	bl	glDrawElements
	ldr	r0, [r4, #1476]	@ float
	ldr	r1, [r4, #1480]	@ float
	ldr	r2, [r4, #1484]	@ float
	bl	glNormal3f
	add	r3, r5, #84
	mov	r2, r6
	mov	r0, #4
	mov	r1, #6
	bl	glDrawElements
	ldr	r0, [r4, #1488]	@ float
	ldr	r1, [r4, #1492]	@ float
	ldr	r2, [r4, #1496]	@ float
	bl	glNormal3f
	add	r3, r5, #96
	mov	r2, r6
	mov	r0, #4
	mov	r1, #6
	bl	glDrawElements
	ldr	r2, [r4, #1508]	@ float
	ldr	r0, [r4, #1500]	@ float
	ldr	r1, [r4, #1504]	@ float
	bl	glNormal3f
	mov	r2, r6
	add	r3, r5, #108
	mov	r0, #4
	mov	r1, #6
	bl	glDrawElements
	mov	r0, r7
	bl	glDisableClientState
	add	sp, sp, #100
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L316:
	.align	2
.L315:
	.word	.LANCHOR1
	.word	.LANCHOR2
	.size	ugSolidBox, .-ugSolidBox
	.section	.rodata
	.align	2
	.set	.LANCHOR1,. + 0
	.type	cubev.3365, %object
	.size	cubev.3365, 288
cubev.3365:
	.word	-1082130432
	.word	-1082130432
	.word	1065353216
	.word	1065353216
	.word	-1082130432
	.word	1065353216
	.word	1065353216
	.word	1065353216
	.word	1065353216
	.word	-1082130432
	.word	1065353216
	.word	1065353216
	.word	-1082130432
	.word	1065353216
	.word	-1082130432
	.word	1065353216
	.word	1065353216
	.word	-1082130432
	.word	1065353216
	.word	-1082130432
	.word	-1082130432
	.word	-1082130432
	.word	-1082130432
	.word	-1082130432
	.word	-1082130432
	.word	-1082130432
	.word	-1082130432
	.word	-1082130432
	.word	-1082130432
	.word	1065353216
	.word	-1082130432
	.word	1065353216
	.word	1065353216
	.word	-1082130432
	.word	1065353216
	.word	-1082130432
	.word	1065353216
	.word	-1082130432
	.word	1065353216
	.word	1065353216
	.word	-1082130432
	.word	-1082130432
	.word	1065353216
	.word	1065353216
	.word	-1082130432
	.word	1065353216
	.word	1065353216
	.word	1065353216
	.word	-1082130432
	.word	1065353216
	.word	1065353216
	.word	1065353216
	.word	1065353216
	.word	1065353216
	.word	1065353216
	.word	1065353216
	.word	-1082130432
	.word	-1082130432
	.word	1065353216
	.word	-1082130432
	.word	-1082130432
	.word	-1082130432
	.word	-1082130432
	.word	1065353216
	.word	-1082130432
	.word	-1082130432
	.word	1065353216
	.word	-1082130432
	.word	1065353216
	.word	-1082130432
	.word	-1082130432
	.word	1065353216
	.type	cuben.3366, %object
	.size	cuben.3366, 288
cuben.3366:
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	-1082130432
	.word	0
	.word	0
	.word	-1082130432
	.word	0
	.word	0
	.word	-1082130432
	.word	0
	.word	0
	.word	-1082130432
	.word	-1082130432
	.word	0
	.word	0
	.word	-1082130432
	.word	0
	.word	0
	.word	-1082130432
	.word	0
	.word	0
	.word	-1082130432
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	-1082130432
	.word	0
	.word	0
	.word	-1082130432
	.word	0
	.word	0
	.word	-1082130432
	.word	0
	.word	0
	.word	-1082130432
	.word	0
	.type	cubev.3352, %object
	.size	cubev.3352, 432
cubev.3352:
	.word	-1082130432
	.word	-1082130432
	.word	1065353216
	.word	1065353216
	.word	-1082130432
	.word	1065353216
	.word	-1082130432
	.word	1065353216
	.word	1065353216
	.word	1065353216
	.word	-1082130432
	.word	1065353216
	.word	1065353216
	.word	1065353216
	.word	1065353216
	.word	-1082130432
	.word	1065353216
	.word	1065353216
	.word	-1082130432
	.word	1065353216
	.word	-1082130432
	.word	1065353216
	.word	-1082130432
	.word	-1082130432
	.word	-1082130432
	.word	-1082130432
	.word	-1082130432
	.word	-1082130432
	.word	1065353216
	.word	-1082130432
	.word	1065353216
	.word	1065353216
	.word	-1082130432
	.word	1065353216
	.word	-1082130432
	.word	-1082130432
	.word	-1082130432
	.word	-1082130432
	.word	-1082130432
	.word	-1082130432
	.word	-1082130432
	.word	1065353216
	.word	-1082130432
	.word	1065353216
	.word	-1082130432
	.word	-1082130432
	.word	-1082130432
	.word	1065353216
	.word	-1082130432
	.word	1065353216
	.word	1065353216
	.word	-1082130432
	.word	1065353216
	.word	-1082130432
	.word	1065353216
	.word	-1082130432
	.word	1065353216
	.word	1065353216
	.word	-1082130432
	.word	-1082130432
	.word	1065353216
	.word	1065353216
	.word	1065353216
	.word	1065353216
	.word	-1082130432
	.word	-1082130432
	.word	1065353216
	.word	1065353216
	.word	-1082130432
	.word	1065353216
	.word	1065353216
	.word	1065353216
	.word	-1082130432
	.word	1065353216
	.word	1065353216
	.word	1065353216
	.word	1065353216
	.word	1065353216
	.word	-1082130432
	.word	1065353216
	.word	-1082130432
	.word	1065353216
	.word	1065353216
	.word	1065353216
	.word	1065353216
	.word	1065353216
	.word	-1082130432
	.word	-1082130432
	.word	1065353216
	.word	-1082130432
	.word	-1082130432
	.word	-1082130432
	.word	-1082130432
	.word	1065353216
	.word	-1082130432
	.word	-1082130432
	.word	-1082130432
	.word	-1082130432
	.word	1065353216
	.word	1065353216
	.word	-1082130432
	.word	-1082130432
	.word	1065353216
	.word	-1082130432
	.word	1065353216
	.word	-1082130432
	.word	-1082130432
	.word	1065353216
	.type	cuben.3353, %object
	.size	cuben.3353, 432
cuben.3353:
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	-1082130432
	.word	0
	.word	0
	.word	-1082130432
	.word	0
	.word	0
	.word	-1082130432
	.word	0
	.word	0
	.word	-1082130432
	.word	0
	.word	0
	.word	-1082130432
	.word	0
	.word	0
	.word	-1082130432
	.word	-1082130432
	.word	0
	.word	0
	.word	-1082130432
	.word	0
	.word	0
	.word	-1082130432
	.word	0
	.word	0
	.word	-1082130432
	.word	0
	.word	0
	.word	-1082130432
	.word	0
	.word	0
	.word	-1082130432
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	-1082130432
	.word	0
	.word	0
	.word	-1082130432
	.word	0
	.word	0
	.word	-1082130432
	.word	0
	.word	0
	.word	-1082130432
	.word	0
	.word	0
	.word	-1082130432
	.word	0
	.word	0
	.word	-1082130432
	.word	0
	.type	boxvec, %object
	.size	boxvec, 72
boxvec:
	.word	-1082130432
	.word	0
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	0
	.word	-1082130432
	.word	0
	.word	0
	.word	0
	.word	1065353216
	.word	0
	.word	0
	.word	-1082130432
	.data
	.align	2
	.set	.LANCHOR2,. + 0
	.type	wireboxndex, %object
	.size	wireboxndex, 48
wireboxndex:
	.short	0
	.short	1
	.short	2
	.short	3
	.short	3
	.short	2
	.short	6
	.short	7
	.short	7
	.short	6
	.short	5
	.short	4
	.short	4
	.short	5
	.short	1
	.short	0
	.short	5
	.short	6
	.short	2
	.short	1
	.short	7
	.short	4
	.short	0
	.short	3
	.type	boxndex, %object
	.size	boxndex, 72
boxndex:
	.short	0
	.short	1
	.short	2
	.short	0
	.short	2
	.short	3
	.short	3
	.short	2
	.short	6
	.short	3
	.short	6
	.short	7
	.short	6
	.short	4
	.short	7
	.short	6
	.short	5
	.short	4
	.short	4
	.short	5
	.short	1
	.short	4
	.short	1
	.short	0
	.short	2
	.short	1
	.short	5
	.short	2
	.short	5
	.short	6
	.short	3
	.short	7
	.short	4
	.short	3
	.short	4
	.short	0
	.bss
	.align	2
	.set	.LANCHOR0,. + 0
	.type	v.3364, %object
	.size	v.3364, 288
v.3364:
	.space	288
	.type	v.3351, %object
	.size	v.3351, 432
v.3351:
	.space	432
	.type	v.3259, %object
	.size	v.3259, 4
v.3259:
	.space	4
	.type	parms.3261, %object
	.size	parms.3261, 16
parms.3261:
	.space	16
	.type	n.3260, %object
	.size	n.3260, 4
n.3260:
	.space	4
	.type	v.3210, %object
	.size	v.3210, 4
v.3210:
	.space	4
	.type	parms.3212, %object
	.size	parms.3212, 12
parms.3212:
	.space	12
	.type	n.3211, %object
	.size	n.3211, 4
n.3211:
	.space	4
	.type	v.3169, %object
	.size	v.3169, 4
v.3169:
	.space	4
	.type	parms.3171, %object
	.size	parms.3171, 12
parms.3171:
	.space	12
	.type	n.3170, %object
	.size	n.3170, 4
n.3170:
	.space	4
	.type	v.2912, %object
	.size	v.2912, 4
v.2912:
	.space	4
	.type	parms.2914, %object
	.size	parms.2914, 16
parms.2914:
	.space	16
	.type	n.2913, %object
	.size	n.2913, 4
n.2913:
	.space	4
	.type	v.2847, %object
	.size	v.2847, 4
v.2847:
	.space	4
	.type	parms.2849, %object
	.size	parms.2849, 16
parms.2849:
	.space	16
	.type	n.2848, %object
	.size	n.2848, 4
n.2848:
	.space	4
	.type	v.2653, %object
	.size	v.2653, 4
v.2653:
	.space	4
	.type	parms.2655, %object
	.size	parms.2655, 16
parms.2655:
	.space	16
	.type	n.2654, %object
	.size	n.2654, 4
n.2654:
	.space	4
	.type	v.3071, %object
	.size	v.3071, 4
v.3071:
	.space	4
	.type	parms.3072, %object
	.size	parms.3072, 16
parms.3072:
	.space	16
	.type	v.3012, %object
	.size	v.3012, 4
v.3012:
	.space	4
	.type	parms.3013, %object
	.size	parms.3013, 16
parms.3013:
	.space	16
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
