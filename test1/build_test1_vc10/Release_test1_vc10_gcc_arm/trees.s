	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"trees.c"
	.section	.text._tr_init,"ax",%progbits
	.align	2
	.global	_tr_init
	.hidden	_tr_init
	.type	_tr_init, %function
_tr_init:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L25
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl}
	mov	r2, #5760
	add	r1, r0, #2432
	add	r7, r0, #2672
	add	r6, r2, #52
	add	ip, r1, #8
	add	r5, r2, #56
	add	r1, r7, #12
	add	r4, r2, #60
	add	r7, r3, #40
	mov	r2, #0
	add	r8, r3, #20
	mov	r9, #8
	add	sl, r0, #148
	str	r9, [r0, r6]
	strh	r2, [r0, r5]	@ movhi
	str	r2, [r0, r4]
	mov	r6, #1136
	str	sl, [r0, #2840]
	str	ip, [r0, #2852]
	str	r8, [r0, #2860]
	str	r1, [r0, #2864]
	str	r7, [r0, #2872]
	strh	r2, [r0, #148]	@ movhi
	str	r3, [r0, #2848]
	add	r6, r6, r9
	mov	r3, #4
.L2:
	add	r5, r0, r3
	add	r1, r3, #4
	add	r3, r3, #20
	mov	r7, #0	@ movhi
	add	r4, r0, r1
	cmp	r3, r6
	strh	r7, [r5, #148]	@ movhi
	strh	r7, [r4, #148]	@ movhi
	strh	r7, [r4, #152]	@ movhi
	strh	r7, [r5, #160]	@ movhi
	strh	r7, [r5, #164]	@ movhi
	bne	.L2
	mov	r3, #0
.L3:
	add	r2, r3, #4
	add	r5, r0, r3
	add	r8, r0, r2
	add	r7, r8, #2432
	add	r2, r5, #2448
	add	ip, r5, #2432
	add	r3, r3, #20
	mov	r8, #0	@ movhi
	add	r5, ip, #8
	add	r4, r7, #8
	add	ip, r7, #12
	add	r6, r2, #8
	add	r7, r2, #4
	cmp	r3, #120
	strh	r8, [r5, #0]	@ movhi
	strh	r8, [r4, #0]	@ movhi
	strh	r8, [ip, #0]	@ movhi
	strh	r8, [r7, #0]	@ movhi
	strh	r8, [r6, #0]	@ movhi
	bne	.L3
	add	r6, r0, #2672
	add	r3, r6, #12
	strh	r8, [r3, #0]	@ movhi
	mov	r3, #4
.L4:
	add	r8, r3, #4
	add	r6, r0, r3
	add	ip, r0, r8
	add	r5, r0, r8
	add	r2, r6, #2688
	add	r8, r0, r3
	add	r4, r6, #2672
	add	r7, ip, #2672
	add	r3, r3, #24
	add	ip, r7, #12
	add	r1, r2, #8
	add	r7, r5, #2688
	add	r6, r8, #2704
	add	r4, r4, #12
	mov	r8, #0	@ movhi
	add	r2, r2, #12
	mov	r5, #0
	cmp	r3, #76
	strh	r8, [r4, #0]	@ movhi
	strh	r8, [ip, #0]	@ movhi
	strh	r8, [r7, #0]	@ movhi
	strh	r8, [r1, #0]	@ movhi
	strh	r8, [r2, #0]	@ movhi
	strh	r5, [r6, #0]	@ movhi
	bne	.L4
	mov	r1, #5760
	mov	r6, r1
	mov	r2, r1
	mov	r3, r1
	mov	r4, #1168
	add	ip, r6, #32
	add	r4, r4, #4
	add	r1, r1, #44
	add	r2, r2, #40
	add	r3, r3, #48
	mov	r6, #1	@ movhi
	strh	r6, [r0, r4]	@ movhi
	str	r5, [r0, ip]
	str	r5, [r0, r1]
	str	r5, [r0, r2]
	str	r5, [r0, r3]
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl}
	bx	lr
.L26:
	.align	2
.L25:
	.word	.LANCHOR0
	.size	_tr_init, .-_tr_init
	.section	.text.send_tree,"ax",%progbits
	.align	2
	.type	send_tree, %function
send_tree:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
	sub	sp, sp, #40
	str	r1, [sp, #4]
	ldrh	r3, [r1, #2]
	cmp	r3, #0
	movne	r6, #7
	moveq	r6, #138
	movne	r7, #4
	moveq	r7, #3
	cmp	r2, #0
	blt	.L56
	mov	r8, #2752
	mov	r5, #2736
	add	r4, r2, #2
	add	fp, r8, #6
	add	sl, r8, #4
	mov	ip, #5760
	str	r4, [sp, #8]
	str	fp, [sp, #28]
	add	r4, r8, #2
	add	fp, r5, #14
	add	r8, r5, #12
	mov	r1, ip
	str	r4, [sp, #32]
	str	fp, [sp, #20]
	str	r8, [sp, #24]
	str	sl, [sp, #36]
	add	ip, ip, #60
	add	r1, r1, #56
	mov	r5, #1
	mov	r2, #0
	mvn	sl, #0
.L55:
	ldr	r9, [sp, #4]
	add	r8, r2, #1
	add	fp, r9, r5, asl #2
	cmp	r8, r6
	ldrh	r4, [fp, #2]
	bge	.L31
	cmp	r3, r4
	beq	.L32
.L31:
	cmp	r8, r7
	bge	.L33
	add	r8, r3, #668
	add	r6, r8, #2
	add	r6, r0, r6, asl #2
	ldrh	r8, [r6, #6]
	ldr	sl, [r0, ip]
	rsb	r7, r8, #16
	add	r9, r6, #4
	cmp	r7, sl
	str	r9, [sp, #0]
	and	r9, r2, #1
	ldrh	fp, [r0, r1]
	add	r7, r6, #6
	str	r9, [sp, #12]
	bge	.L58
	ldrh	r9, [r6, #4]
	orr	sl, fp, r9, asl sl
	str	r9, [sp, #16]
	ldr	fp, [r0, #8]
	ldr	r9, [r0, #20]
	mov	sl, sl, asl #16
	mov	sl, sl, lsr #16
	strh	sl, [r0, r1]	@ movhi
	strb	sl, [fp, r9]
	ldrh	sl, [r0, r1]
	add	fp, r9, #1
	ldr	r9, [r0, #8]
	mov	sl, sl, lsr #8
	strb	sl, [r9, fp]
	ldr	sl, [r0, ip]
	rsb	r9, sl, #16
	ldr	sl, [sp, #16]
	mov	r9, sl, asr r9
	ldr	sl, [r0, ip]
	mov	r9, r9, asl #16
	sub	sl, sl, #16
	add	r8, sl, r8
	add	fp, fp, #1
	mov	r9, r9, lsr #16
	str	sl, [sp, #16]
	str	fp, [r0, #20]
	strh	r9, [r0, r1]	@ movhi
	str	r8, [r0, ip]
.L68:
	subs	r2, r2, #1
	bcc	.L37
	ldr	fp, [sp, #12]
	cmp	fp, #0
	beq	.L73
	ldrh	fp, [r6, #6]
	rsb	sl, fp, #16
	cmp	sl, r8
	blt	.L69
	ldrh	r6, [r6, #4]
	orr	r9, r9, r6, asl r8
	ldr	r8, [r0, ip]
	mov	r9, r9, asl #16
	add	r8, fp, r8
	mov	r9, r9, lsr #16
	strh	r9, [r0, r1]	@ movhi
	str	r8, [r0, ip]
.L70:
	subs	r2, r2, #1
	bcc	.L37
.L73:
	mov	fp, r4
	b	.L36
.L75:
	orr	r4, r9, sl, asl r8
	ldr	r8, [r0, #20]
	mov	r9, r4, asl #16
	ldr	r4, [r0, #8]
	mov	r9, r9, lsr #16
	strh	r9, [r0, r1]	@ movhi
	strb	r9, [r4, r8]
	ldrh	r9, [r0, r1]
	ldr	r4, [r0, #8]
	add	r8, r8, #1
	mov	r9, r9, lsr #8
	strb	r9, [r4, r8]
	ldr	r4, [r0, ip]
	rsb	r9, r4, #16
	mov	sl, sl, asr r9
	sub	r4, r4, #16
	mov	sl, sl, asl #16
	add	r4, r4, r6
	add	r9, r8, #1
	mov	sl, sl, lsr #16
	str	r9, [r0, #20]
	strh	sl, [r0, r1]	@ movhi
	str	r4, [r0, ip]
	ldrh	r6, [r7, #0]
	rsb	r9, r6, #16
	cmp	r9, r4
	sub	r2, r2, #1
	bge	.L63
.L76:
	ldr	r8, [sp, #0]
	ldrh	r9, [r8, #0]
	orr	r8, sl, r9, asl r4
	ldr	r4, [r0, #20]
	mov	sl, r8, asl #16
	ldr	r8, [r0, #8]
	mov	sl, sl, lsr #16
	strh	sl, [r0, r1]	@ movhi
	strb	sl, [r8, r4]
	ldrh	sl, [r0, r1]
	ldr	r8, [r0, #8]
	add	r4, r4, #1
	mov	sl, sl, lsr #8
	strb	sl, [r8, r4]
	ldr	r8, [r0, ip]
	rsb	sl, r8, #16
	mov	r9, r9, asr sl
	sub	r8, r8, #16
	mov	r9, r9, asl #16
	add	r8, r8, r6
	add	r4, r4, #1
	mov	r9, r9, lsr #16
	subs	r2, r2, #1
	str	r4, [r0, #20]
	strh	r9, [r0, r1]	@ movhi
	str	r8, [r0, ip]
	bcc	.L74
.L36:
	ldrh	r6, [r7, #0]
	ldr	r4, [sp, #0]
	rsb	sl, r6, #16
	cmp	sl, r8
	ldrh	sl, [r4, #0]
	blt	.L75
	orr	r4, r9, sl, asl r8
	ldr	r8, [r0, ip]
	mov	sl, r4, asl #16
	mov	sl, sl, lsr #16
	add	r4, r6, r8
	str	r4, [r0, ip]
	strh	sl, [r0, r1]	@ movhi
	ldrh	r6, [r7, #0]
	rsb	r9, r6, #16
	cmp	r9, r4
	sub	r2, r2, #1
	blt	.L76
.L63:
	ldr	r8, [sp, #0]
	ldrh	r9, [r8, #0]
	orr	r9, sl, r9, asl r4
	ldr	sl, [r0, ip]
	mov	r4, r9, asl #16
	add	r8, r6, sl
	mov	r9, r4, lsr #16
	subs	r2, r2, #1
	strh	r9, [r0, r1]	@ movhi
	str	r8, [r0, ip]
	bcs	.L36
.L74:
	mov	r4, fp
.L37:
	cmp	r4, #0
	bne	.L53
.L78:
	mov	sl, r3
	mov	r7, #3
	mov	r6, #138
	mov	r8, r4
.L32:
	ldr	fp, [sp, #8]
	add	r5, r5, #1
	cmp	r5, fp
	beq	.L56
.L79:
	mov	r2, r8
	mov	r3, r4
	b	.L55
.L33:
	cmp	r3, #0
	beq	.L38
	cmp	sl, r3
	beq	.L77
	add	r6, r3, #668
	add	r7, r6, #2
	add	r7, r0, r7, asl #2
	ldrh	r8, [r7, #6]
	ldr	r6, [r0, ip]
	rsb	fp, r8, #16
	cmp	r6, fp
	ble	.L41
	ldrh	r7, [r7, #4]
	ldrh	sl, [r0, r1]
	orr	fp, sl, r7, asl r6
	ldr	r9, [r0, #8]
	ldr	sl, [r0, #20]
	mov	r6, fp, asl #16
	mov	fp, r6, lsr #16
	strh	fp, [r0, r1]	@ movhi
	strb	fp, [r9, sl]
	ldrh	r6, [r0, r1]
	ldr	r9, [r0, #8]
	add	fp, sl, #1
	mov	r6, r6, lsr #8
	strb	r6, [r9, fp]
	ldr	r6, [r0, ip]
	rsb	r9, r6, #16
	mov	r7, r7, asr r9
	sub	r6, r6, #16
	mov	r9, r7, asl #16
	add	r6, r6, r8
	add	sl, fp, #1
	mov	r7, r9, lsr #16
	str	sl, [r0, #20]
	strh	r7, [r0, r1]	@ movhi
	str	r6, [r0, ip]
.L42:
	mov	r8, r2
.L40:
	ldr	fp, [sp, #20]
	ldrh	r2, [r0, fp]
	rsb	r9, r2, #16
	cmp	r9, r6
	bge	.L43
	ldr	r9, [sp, #24]
	ldrh	sl, [r0, r9]
	orr	fp, r7, sl, asl r6
	ldr	r9, [r0, #8]
	ldr	r6, [r0, #20]
	mov	r7, fp, asl #16
	mov	fp, r7, lsr #16
	strh	fp, [r0, r1]	@ movhi
	strb	fp, [r9, r6]
	ldrh	r9, [r0, r1]
	ldr	r7, [r0, #8]
	add	fp, r6, #1
	mov	r9, r9, lsr #8
	strb	r9, [r7, fp]
	ldr	r7, [r0, ip]
	rsb	r9, r7, #16
	mov	sl, sl, asr r9
	sub	r7, r7, #16
	mov	sl, sl, asl #16
	add	r2, r7, r2
	add	r6, fp, #1
	mov	sl, sl, lsr #16
	str	r6, [r0, #20]
	strh	sl, [r0, r1]	@ movhi
	str	r2, [r0, ip]
.L44:
	cmp	r2, #14
	sub	r8, r8, #3
	orr	r2, sl, r8, asl r2
	ble	.L45
	ldr	r6, [r0, #20]
	ldr	r7, [r0, #8]
	mov	r2, r2, asl #16
	mov	fp, r2, lsr #16
	strh	fp, [r0, r1]	@ movhi
	strb	fp, [r7, r6]
	ldrh	r2, [r0, r1]
	ldr	fp, [r0, #8]
	mov	r7, r2, lsr #8
	add	r6, r6, #1
	strb	r7, [fp, r6]
	ldr	fp, [r0, ip]
	mov	r2, r8, asl #16
	mov	r8, r2, lsr #16
	rsb	r7, fp, #16
	mov	r7, r8, asr r7
	add	r6, r6, #1
	sub	r2, fp, #14
	cmp	r4, #0
	str	r6, [r0, #20]
	strh	r7, [r0, r1]	@ movhi
	str	r2, [r0, ip]
	beq	.L78
.L53:
	ldr	fp, [sp, #8]
	cmp	r3, r4
	add	r5, r5, #1
	movne	sl, r3
	movne	r7, #4
	movne	r6, #7
	moveq	sl, r4
	moveq	r7, #3
	moveq	r6, #6
	cmp	r5, fp
	mov	r8, #0
	bne	.L79
.L56:
	add	sp, sp, #40
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
	bx	lr
.L38:
	cmp	r8, #10
	bgt	.L46
	ldr	fp, [sp, #32]
	ldrh	r2, [r0, fp]
	ldr	r7, [r0, ip]
	rsb	sl, r2, #16
	mov	r6, #2752
	cmp	r7, sl
	ldrh	sl, [r0, r6]
	ldrh	r6, [r0, r1]
	ble	.L47
	orr	fp, r6, sl, asl r7
	ldr	r9, [r0, #8]
	ldr	r6, [r0, #20]
	mov	r7, fp, asl #16
	mov	fp, r7, lsr #16
	strh	fp, [r0, r1]	@ movhi
	strb	fp, [r9, r6]
	ldrh	r7, [r0, r1]
	ldr	r9, [r0, #8]
	add	fp, r6, #1
	mov	r7, r7, lsr #8
	strb	r7, [r9, fp]
	ldr	r7, [r0, ip]
	rsb	r9, r7, #16
	mov	sl, sl, asr r9
	sub	r7, r7, #16
	add	r2, r7, r2
	mov	sl, sl, asl #16
	add	r6, fp, #1
	mov	sl, sl, lsr #16
	cmp	r2, #13
	str	r6, [r0, #20]
	strh	sl, [r0, r1]	@ movhi
	str	r2, [r0, ip]
	sub	r8, r8, #3
	ble	.L49
.L80:
	orr	r6, sl, r8, asl r2
	ldr	r7, [r0, #8]
	mov	r2, r6, asl #16
	ldr	r6, [r0, #20]
	mov	fp, r2, lsr #16
	strh	fp, [r0, r1]	@ movhi
	strb	fp, [r7, r6]
	ldrh	r2, [r0, r1]
	ldr	fp, [r0, #8]
	mov	r7, r2, lsr #8
	add	r6, r6, #1
	strb	r7, [fp, r6]
	ldr	fp, [r0, ip]
	mov	r2, r8, asl #16
	mov	r8, r2, lsr #16
	rsb	r7, fp, #16
	mov	r7, r8, asr r7
	add	r6, r6, #1
	sub	r2, fp, #13
	str	r6, [r0, #20]
	strh	r7, [r0, r1]	@ movhi
	str	r2, [r0, ip]
	b	.L37
.L58:
	ldrh	r9, [r6, #4]
	orr	r9, fp, r9, asl sl
	mov	r9, r9, asl #16
	add	r8, r8, sl
	mov	r9, r9, lsr #16
	strh	r9, [r0, r1]	@ movhi
	str	r8, [r0, ip]
	b	.L68
.L69:
	ldrh	sl, [r6, #4]
	orr	r6, r9, sl, asl r8
	ldr	r9, [r0, #8]
	mov	r8, r6, asl #16
	ldr	r6, [r0, #20]
	mov	r8, r8, lsr #16
	strh	r8, [r0, r1]	@ movhi
	strb	r8, [r9, r6]
	ldrh	r8, [r0, r1]
	ldr	r9, [r0, #8]
	add	r6, r6, #1
	mov	r8, r8, lsr #8
	strb	r8, [r9, r6]
	ldr	r9, [r0, ip]
	rsb	r8, r9, #16
	mov	r8, sl, asr r8
	sub	sl, r9, #16
	mov	r9, r8, asl #16
	add	r6, r6, #1
	add	r8, sl, fp
	mov	r9, r9, lsr #16
	str	r6, [r0, #20]
	strh	r9, [r0, r1]	@ movhi
	str	r8, [r0, ip]
	b	.L70
.L46:
	ldr	sl, [sp, #28]
	ldrh	r2, [r0, sl]
	ldr	r7, [r0, ip]
	rsb	r9, r2, #16
	cmp	r7, r9
	ble	.L50
	ldr	r9, [sp, #36]
	ldrh	r6, [r0, r1]
	ldrh	sl, [r0, r9]
	orr	fp, r6, sl, asl r7
	ldr	r9, [r0, #8]
	ldr	r6, [r0, #20]
	mov	r7, fp, asl #16
	mov	fp, r7, lsr #16
	strh	fp, [r0, r1]	@ movhi
	strb	fp, [r9, r6]
	ldrh	r7, [r0, r1]
	ldr	r9, [r0, #8]
	mov	fp, r7, lsr #8
	add	r6, r6, #1
	strb	fp, [r9, r6]
	ldr	r7, [r0, ip]
	rsb	r9, r7, #16
	mov	fp, sl, asr r9
	sub	r7, r7, #16
	mov	sl, fp, asl #16
	add	r2, r7, r2
	add	fp, r6, #1
	mov	sl, sl, lsr #16
	str	fp, [r0, #20]
	strh	sl, [r0, r1]	@ movhi
	str	r2, [r0, ip]
.L51:
	cmp	r2, #9
	sub	r8, r8, #11
	ble	.L52
	orr	r6, sl, r8, asl r2
	ldr	r7, [r0, #8]
	mov	r2, r6, asl #16
	ldr	r6, [r0, #20]
	mov	fp, r2, lsr #16
	strh	fp, [r0, r1]	@ movhi
	strb	fp, [r7, r6]
	ldrh	r2, [r0, r1]
	ldr	fp, [r0, #8]
	mov	r7, r2, lsr #8
	add	r6, r6, #1
	strb	r7, [fp, r6]
	ldr	fp, [r0, ip]
	mov	r2, r8, asl #16
	mov	r8, r2, lsr #16
	rsb	r7, fp, #16
	mov	r7, r8, asr r7
	add	r6, r6, #1
	sub	r2, fp, #9
	str	r6, [r0, #20]
	strh	r7, [r0, r1]	@ movhi
	str	r2, [r0, ip]
	b	.L37
.L45:
	ldr	r8, [r0, ip]
	add	r6, r8, #2
	strh	r2, [r0, r1]	@ movhi
	str	r6, [r0, ip]
	b	.L37
.L43:
	ldr	r9, [sp, #24]
	ldrh	sl, [r0, r9]
	orr	fp, r7, sl, asl r6
	ldr	r9, [r0, ip]
	mov	sl, fp, asl #16
	add	r2, r2, r9
	mov	sl, sl, lsr #16
	strh	sl, [r0, r1]	@ movhi
	str	r2, [r0, ip]
	b	.L44
.L41:
	ldrh	sl, [r7, #4]
	ldrh	fp, [r0, r1]
	orr	r9, fp, sl, asl r6
	mov	r7, r9, asl #16
	add	r6, r8, r6
	mov	r7, r7, lsr #16
	strh	r7, [r0, r1]	@ movhi
	str	r6, [r0, ip]
	b	.L42
.L77:
	ldr	r6, [r0, ip]
	ldrh	r7, [r0, r1]
	b	.L40
.L47:
	orr	fp, r6, sl, asl r7
	add	r2, r2, r7
	mov	sl, fp, asl #16
	mov	sl, sl, lsr #16
	cmp	r2, #13
	strh	sl, [r0, r1]	@ movhi
	str	r2, [r0, ip]
	sub	r8, r8, #3
	bgt	.L80
.L49:
	orr	sl, sl, r8, asl r2
	ldr	r8, [r0, ip]
	add	r6, r8, #3
	strh	sl, [r0, r1]	@ movhi
	str	r6, [r0, ip]
	b	.L37
.L52:
	orr	sl, sl, r8, asl r2
	ldr	r8, [r0, ip]
	add	r6, r8, #7
	strh	sl, [r0, r1]	@ movhi
	str	r6, [r0, ip]
	b	.L37
.L50:
	ldr	r9, [sp, #36]
	ldrh	r6, [r0, r1]
	ldrh	sl, [r0, r9]
	orr	fp, r6, sl, asl r7
	mov	sl, fp, asl #16
	add	r2, r2, r7
	mov	sl, sl, lsr #16
	strh	sl, [r0, r1]	@ movhi
	str	r2, [r0, ip]
	b	.L51
	.size	send_tree, .-send_tree
	.section	.text._tr_stored_block,"ax",%progbits
	.align	2
	.global	_tr_stored_block
	.hidden	_tr_stored_block
	.type	_tr_stored_block, %function
_tr_stored_block:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	ip, #5760
	add	ip, ip, #60
	stmfd	sp!, {r4, r5, r6, r7, r8}
	ldr	r4, [r0, ip]
	mov	r5, #5760
	add	r5, r5, #56
	cmp	r4, #13
	ldrh	r6, [r0, r5]
	ble	.L82
	orr	r8, r6, r3, asl r4
	ldr	r7, [r0, #8]
	ldr	r6, [r0, #20]
	mov	r4, r8, asl #16
	mov	r8, r4, lsr #16
	strh	r8, [r0, r5]	@ movhi
	strb	r8, [r7, r6]
	ldrh	r7, [r0, r5]
	ldr	r4, [r0, #8]
	add	r8, r6, #1
	mov	r7, r7, lsr #8
	strb	r7, [r4, r8]
	ldr	r4, [r0, ip]
	mov	r3, r3, asl #16
	rsb	r7, r4, #16
	mov	r3, r3, lsr #16
	mov	r3, r3, asr r7
	mov	r3, r3, asl #16
	add	r6, r8, #1
	mov	r3, r3, lsr #16
	sub	r4, r4, #13
	str	r6, [r0, #20]
	strh	r3, [r0, r5]	@ movhi
	str	r4, [r0, ip]
.L83:
	cmp	r4, #8
	ble	.L84
	ldr	r7, [r0, #20]
	ldr	r6, [r0, #8]
	mov	ip, #5760
	strb	r3, [r6, r7]
	add	r3, ip, #56
	ldrh	r8, [r0, r3]
	add	r6, r7, #1
	ldr	r4, [r0, #8]
	mov	r5, r8, lsr #8
	add	ip, r6, #1
	strb	r5, [r4, r6]
	str	ip, [r0, #20]
.L85:
	mov	r3, #5760
	mov	r5, r3
	ldr	r6, [r0, #20]
	ldr	r8, [r0, #8]
	add	r4, r5, #60
	mov	r7, #0
	str	r7, [r0, r4]
	add	ip, r5, #52
	add	r3, r3, #56
	and	r5, r2, #255
	mov	r4, #8
	str	r4, [r0, ip]
	strh	r7, [r0, r3]	@ movhi
	strb	r5, [r8, r6]
	mov	ip, r2, asl #16
	ldr	r4, [r0, #8]
	add	r3, r6, #1
	mov	r6, ip, lsr #24
	strb	r6, [r4, r3]
	ldr	r4, [r0, #8]
	add	r3, r3, #1
	mvn	r5, r5
	strb	r5, [r4, r3]
	eor	ip, ip, #-16777216
	add	r3, r3, #1
	ldr	r4, [r0, #8]
	mov	r5, ip, lsr #24
	cmp	r2, r7
	add	ip, r3, #1
	strb	r5, [r4, r3]
	str	ip, [r0, #20]
	beq	.L88
	ldrb	r6, [r1, #0]	@ zero_extendqisi2
	ldr	r5, [r0, #8]
	add	r3, ip, #1
	sub	r4, r2, #1
	cmp	r2, #1
	strb	r6, [r5, ip]
	and	r4, r4, #3
	str	r3, [r0, #20]
	mov	ip, #1
	beq	.L88
	cmp	r4, #0
	beq	.L87
	cmp	r4, #1
	beq	.L101
	cmp	r4, #2
	beq	.L102
	ldrb	r4, [r1, #1]	@ zero_extendqisi2
	ldr	ip, [r0, #8]
	strb	r4, [ip, r3]
	add	r3, r3, #1
	str	r3, [r0, #20]
	mov	ip, #2
.L102:
	ldrb	r5, [r1, ip]	@ zero_extendqisi2
	ldr	r4, [r0, #8]
	strb	r5, [r4, r3]
	add	r3, r3, #1
	str	r3, [r0, #20]
	add	ip, ip, #1
.L101:
	ldrb	r5, [r1, ip]	@ zero_extendqisi2
	ldr	r4, [r0, #8]
	add	ip, ip, #1
	strb	r5, [r4, r3]
	cmp	r2, ip
	add	r3, r3, #1
	str	r3, [r0, #20]
	beq	.L88
.L87:
	ldrb	r5, [r1, ip]	@ zero_extendqisi2
	ldr	r8, [r0, #8]
	add	r4, r3, #1
	strb	r5, [r8, r3]
	str	r4, [r0, #20]
	add	r6, ip, #1
	ldrb	r8, [r1, r6]	@ zero_extendqisi2
	ldr	r7, [r0, #8]
	add	r5, r4, #1
	strb	r8, [r7, r4]
	str	r5, [r0, #20]
	add	r4, r6, #1
	ldrb	r7, [r1, r4]	@ zero_extendqisi2
	ldr	r6, [r0, #8]
	add	r4, r3, #3
	strb	r7, [r6, r5]
	str	r4, [r0, #20]
	add	r5, ip, #3
	ldrb	r6, [r1, r5]	@ zero_extendqisi2
	add	ip, ip, #4
	ldr	r5, [r0, #8]
	add	r3, r3, #4
	cmp	r2, ip
	strb	r6, [r5, r4]
	str	r3, [r0, #20]
	bne	.L87
.L88:
	ldmfd	sp!, {r4, r5, r6, r7, r8}
	bx	lr
.L84:
	cmp	r4, #0
	ldrgt	ip, [r0, #20]
	ldrgt	r5, [r0, #8]
	addgt	r4, ip, #1
	strgtb	r3, [r5, ip]
	strgt	r4, [r0, #20]
	b	.L85
.L82:
	orr	r7, r6, r3, asl r4
	mov	r3, r7, asl #16
	mov	r3, r3, lsr #16
	add	r4, r4, #3
	strh	r3, [r0, r5]	@ movhi
	str	r4, [r0, ip]
	b	.L83
	.size	_tr_stored_block, .-_tr_stored_block
	.section	.text._tr_align,"ax",%progbits
	.align	2
	.global	_tr_align
	.hidden	_tr_align
	.type	_tr_align, %function
_tr_align:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, #5760
	add	r3, r3, #60
	ldr	r1, [r0, r3]
	mov	ip, #5760
	cmp	r1, #13
	stmfd	sp!, {r4, r5, r6, r7, r8}
	add	ip, ip, #56
	ble	.L104
	ldrh	r4, [r0, ip]
	mov	r2, #2
	orr	r6, r4, r2, asl r1
	ldr	r5, [r0, #8]
	ldr	r4, [r0, #20]
	mov	r1, r6, asl #16
	mov	r6, r1, lsr #16
	strh	r6, [r0, ip]	@ movhi
	strb	r6, [r5, r4]
	ldrh	r5, [r0, ip]
	ldr	r1, [r0, #8]
	add	r6, r4, #1
	mov	r5, r5, lsr #8
	strb	r5, [r1, r6]
	ldr	r1, [r0, r3]
	rsb	r5, r1, #16
	mov	r2, r2, asr r5
	mov	r2, r2, asl #16
	add	r4, r6, #1
	mov	r2, r2, lsr #16
	sub	r1, r1, #13
	str	r4, [r0, #20]
	strh	r2, [r0, ip]	@ movhi
	str	r1, [r0, r3]
.L105:
	ldr	ip, .L122
	mov	r6, #1024
	add	r5, r6, #2
	ldrh	r4, [ip, r5]
	rsb	r3, r4, #16
	cmp	r3, r1
	mov	r3, #1024
	bge	.L106
	ldrh	r7, [ip, r3]
	orr	r6, r2, r7, asl r1
	ldr	r3, [r0, #8]
	mov	r1, r6, asl #16
	mov	r5, #5760
	ldr	r6, [r0, #20]
	mov	r2, r1, lsr #16
	add	r5, r5, #56
	strh	r2, [r0, r5]	@ movhi
	strb	r2, [r3, r6]
	ldrh	r1, [r0, r5]
	ldr	r3, [r0, #8]
	mov	r2, r1, lsr #8
	add	r6, r6, #1
	strb	r2, [r3, r6]
	mov	r1, #5760
	add	r1, r1, #60
	ldr	r3, [r0, r1]
	rsb	r2, r3, #16
	mov	r2, r7, asr r2
	sub	r3, r3, #16
	add	r3, r3, r4
	mov	r2, r2, asl #16
	add	r6, r6, #1
	mov	r2, r2, lsr #16
	cmp	r3, #16
	str	r6, [r0, #20]
	strh	r2, [r0, r5]	@ movhi
	str	r3, [r0, r1]
	beq	.L117
.L108:
	cmp	r3, #7
	bgt	.L118
.L109:
	mov	r1, #5760
	add	r1, r1, #52
	ldr	r1, [r0, r1]
	add	r1, r1, #11
	rsb	r1, r3, r1
	cmp	r1, #8
	bgt	.L110
	cmp	r3, #13
	bgt	.L119
	mov	r1, #2
	orr	r5, r2, r1, asl r3
	mov	r4, #5760
	mov	r3, r5, asl #16
	mov	r5, #5760
	add	r2, r4, #60
	add	r5, r5, #56
	mov	r4, #1024
	mov	r3, r3, lsr #16
	ldr	r1, [r0, r2]
	strh	r3, [r0, r5]	@ movhi
	add	r5, r4, #2
	ldrh	ip, [ip, r5]
	add	r1, r1, #3
	str	r1, [r0, r2]
	rsb	r2, ip, #16
	cmp	r2, r1
	ldr	r4, .L122
	mov	r2, #1024
	bge	.L113
.L121:
	ldrh	r6, [r4, r2]
	orr	r5, r3, r6, asl r1
	ldr	r4, [r0, #8]
	mov	r3, r5, asl #16
	mov	r2, #5760
	ldr	r5, [r0, #20]
	mov	r1, r3, lsr #16
	add	r2, r2, #56
	strh	r1, [r0, r2]	@ movhi
	strb	r1, [r4, r5]
	ldrh	r4, [r0, r2]
	ldr	r3, [r0, #8]
	mov	r1, r4, lsr #8
	add	r5, r5, #1
	strb	r1, [r3, r5]
	mov	r4, #5760
	add	r4, r4, #60
	ldr	r3, [r0, r4]
	rsb	r1, r3, #16
	mov	r1, r6, asr r1
	sub	r3, r3, #16
	add	r3, r3, ip
	mov	r1, r1, asl #16
	add	r5, r5, #1
	mov	r1, r1, lsr #16
	cmp	r3, #16
	str	r5, [r0, #20]
	strh	r1, [r0, r2]	@ movhi
	str	r3, [r0, r4]
	beq	.L120
.L115:
	cmp	r3, #7
	ble	.L110
	ldr	ip, [r0, #20]
	ldr	r4, [r0, #8]
	mov	r2, #5760
	strb	r1, [r4, ip]
	mov	r3, r2
	add	r3, r3, #60
	add	r2, r2, #56
	ldrh	r4, [r0, r2]
	ldr	r1, [r0, r3]
	add	ip, ip, #1
	mov	r4, r4, lsr #8
	sub	r1, r1, #8
	strh	r4, [r0, r2]	@ movhi
	str	r1, [r0, r3]
	str	ip, [r0, #20]
.L110:
	mov	r3, #5760
	add	ip, r3, #52
	mov	r1, #7
	str	r1, [r0, ip]
	ldmfd	sp!, {r4, r5, r6, r7, r8}
	bx	lr
.L106:
	ldrh	r3, [ip, r3]
	orr	r6, r2, r3, asl r1
	mov	r5, #5760
	add	r5, r5, #60
	ldr	r3, [r0, r5]
	mov	r2, r6, asl #16
	add	r3, r4, r3
	mov	r1, #5760
	mov	r2, r2, lsr #16
	add	r6, r1, #56
	cmp	r3, #16
	strh	r2, [r0, r6]	@ movhi
	str	r3, [r0, r5]
	bne	.L108
.L117:
	ldr	r7, [r0, #20]
	ldr	r6, [r0, #8]
	mov	r4, #5760
	strb	r2, [r6, r7]
	add	r3, r4, #56
	ldrh	r8, [r0, r3]
	add	r5, r7, #1
	mov	r2, #5760
	ldr	r7, [r0, #8]
	mov	r4, #0
	add	r1, r2, #60
	mov	r8, r8, lsr #8
	add	r6, r5, #1
	strb	r8, [r7, r5]
	mov	r2, r4
	strh	r4, [r0, r3]	@ movhi
	str	r6, [r0, #20]
	str	r4, [r0, r1]
	mov	r3, r4
	b	.L109
.L104:
	ldrh	r6, [r0, ip]
	mov	r4, #2
	orr	r5, r6, r4, asl r1
	mov	r2, r5, asl #16
	mov	r2, r2, lsr #16
	add	r1, r1, #3
	strh	r2, [r0, ip]	@ movhi
	str	r1, [r0, r3]
	b	.L105
.L118:
	ldr	r5, [r0, #20]
	ldr	r3, [r0, #8]
	mov	r4, #5760
	strb	r2, [r3, r5]
	mov	r1, r4
	add	r1, r1, #60
	add	r4, r4, #56
	ldrh	r2, [r0, r4]
	ldr	r3, [r0, r1]
	add	r5, r5, #1
	mov	r2, r2, lsr #8
	sub	r3, r3, #8
	str	r5, [r0, #20]
	strh	r2, [r0, r4]	@ movhi
	str	r3, [r0, r1]
	b	.L109
.L119:
	mov	r7, #2
	orr	r3, r2, r7, asl r3
	ldr	r6, [r0, #20]
	mov	r1, r3, asl #16
	ldr	r5, [r0, #8]
	mov	r4, #5760
	add	r4, r4, #56
	mov	r2, r1, lsr #16
	strh	r2, [r0, r4]	@ movhi
	strb	r2, [r5, r6]
	ldrh	r3, [r0, r4]
	ldr	r1, [r0, #8]
	mov	r2, r3, lsr #8
	add	r6, r6, #1
	strb	r2, [r1, r6]
	mov	r5, #5760
	add	r2, r5, #60
	ldr	r1, [r0, r2]
	rsb	r3, r1, #16
	mov	r3, r7, asr r3
	mov	r3, r3, asl #16
	mov	r3, r3, lsr #16
	strh	r3, [r0, r4]	@ movhi
	mov	r4, #1024
	add	r5, r4, #2
	ldrh	ip, [ip, r5]
	sub	r1, r1, #13
	str	r1, [r0, r2]
	rsb	r2, ip, #16
	add	r6, r6, #1
	cmp	r2, r1
	str	r6, [r0, #20]
	ldr	r4, .L122
	mov	r2, #1024
	blt	.L121
.L113:
	ldrh	r5, [r4, r2]
	orr	r1, r3, r5, asl r1
	mov	r4, #5760
	add	r4, r4, #60
	ldr	r5, [r0, r4]
	mov	r1, r1, asl #16
	add	r3, ip, r5
	mov	r2, #5760
	mov	r1, r1, lsr #16
	add	ip, r2, #56
	cmp	r3, #16
	strh	r1, [r0, ip]	@ movhi
	str	r3, [r0, r4]
	bne	.L115
.L120:
	ldr	r4, [r0, #20]
	ldr	r3, [r0, #8]
	mov	r5, #5760
	strb	r1, [r3, r4]
	add	r3, r5, #56
	ldrh	r6, [r0, r3]
	add	ip, r4, #1
	mov	r1, #5760
	ldr	r5, [r0, #8]
	add	r2, r1, #60
	mov	r6, r6, lsr #8
	mov	r1, #0
	add	r4, ip, #1
	strb	r6, [r5, ip]
	str	r1, [r0, r2]
	str	r4, [r0, #20]
	strh	r1, [r0, r3]	@ movhi
	b	.L110
.L123:
	.align	2
.L122:
	.word	.LANCHOR1
	.size	_tr_align, .-_tr_align
	.section	.text._tr_tally,"ax",%progbits
	.align	2
	.global	_tr_tally
	.hidden	_tr_tally
	.type	_tr_tally, %function
_tr_tally:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, r5}
	mov	r3, #5760
	add	r3, r3, #32
	mov	ip, #5760
	ldr	r4, [r0, r3]
	add	r5, ip, #36
	ldr	ip, [r0, r5]
	mov	r5, r4, asl #1
	strh	r1, [r5, ip]	@ movhi
	mov	r4, #5760
	add	r5, r4, #24
	ldr	r4, [r0, r3]
	ldr	ip, [r0, r5]
	cmp	r1, #0
	add	r5, r4, #1
	strb	r2, [ip, r4]
	str	r5, [r0, r3]
	beq	.L130
	ldr	ip, .L131
	mov	r3, #5760
	add	r3, r3, #48
	add	r4, r2, ip
	ldrb	r2, [r4, #1152]	@ zero_extendqisi2
	ldr	r4, [r0, r3]
	add	r2, r2, #292
	add	r4, r4, #1
	str	r4, [r0, r3]
	add	r2, r2, #1
	add	r2, r0, r2, asl #2
	sub	r1, r1, #1
	ldrh	r3, [r2, #4]
	cmp	r1, #255
	add	r3, r3, #1
	addls	r1, r1, ip
	addhi	r1, ip, r1, lsr #7
	strh	r3, [r2, #4]	@ movhi
	ldrlsb	r3, [r1, #1408]	@ zero_extendqisi2
	ldrhib	r3, [r1, #1664]	@ zero_extendqisi2
	add	r2, r3, #608
	add	r3, r2, #2
	mov	r1, r3, asl #2
	ldrh	ip, [r1, r0]
	add	r2, ip, #1
	strh	r2, [r1, r0]	@ movhi
.L126:
	mov	r2, #5760
	add	ip, r2, #28
	ldr	r1, [r0, ip]
	sub	r0, r1, #1
	cmp	r5, r0
	movne	r0, #0
	moveq	r0, #1
	ldmfd	sp!, {r4, r5}
	bx	lr
.L130:
	add	r3, r2, #36
	add	r2, r0, r3, asl #2
	ldrh	ip, [r2, #4]
	add	r1, ip, #1
	strh	r1, [r2, #4]	@ movhi
	b	.L126
.L132:
	.align	2
.L131:
	.word	.LANCHOR1
	.size	_tr_tally, .-_tr_tally
	.section	.text.compress_block,"ax",%progbits
	.align	2
	.type	compress_block, %function
compress_block:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
	mov	r4, #5760
	add	r3, r4, #32
	ldr	ip, [r0, r3]
	sub	sp, sp, #16
	cmp	ip, #0
	str	r2, [sp, #12]
	bne	.L134
	mov	r3, #5760
	add	r2, r3, #60
	add	r5, r3, #56
	ldr	r2, [r0, r2]
	ldrh	ip, [r0, r5]
.L135:
	add	r1, r1, #1024
	ldrh	r4, [r1, #2]
	rsb	r5, r4, #16
	cmp	r5, r2
	blt	.L153
	ldrh	r5, [r1, #0]
	orr	r2, ip, r5, asl r2
	mov	r3, #5760
	add	r3, r3, #60
	ldr	ip, [r0, r3]
	mov	r5, #5760
	add	r4, r4, ip
	add	r5, r5, #56
	strh	r2, [r0, r5]	@ movhi
	str	r4, [r0, r3]
.L150:
	ldrh	r2, [r1, #2]
	mov	ip, #5760
	add	r1, ip, #52
	str	r2, [r0, r1]
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
	bx	lr
.L134:
	mov	r9, #5760
	mov	r7, #5760
	mov	sl, #5760
	add	r4, r9, #60
	add	r5, r9, #56
	add	r6, r7, #24
	str	sl, [sp, #8]
	ldr	r2, [r0, r4]
	ldrh	ip, [r0, r5]
	add	sl, sl, #36
	str	r6, [sp, #8]
	mov	r3, #0
	b	.L148
.L154:
	mov	r7, r7, asl #2
	add	r8, r1, r7
	ldrh	r6, [r8, #2]
	rsb	r8, r6, #16
	cmp	r8, r2
	bge	.L137
	ldrh	r8, [r1, r7]
	orr	r7, ip, r8, asl r2
	ldr	r9, [r0, #8]
	mov	ip, r7, asl #16
	ldr	r7, [r0, #20]
	mov	r2, ip, lsr #16
	strh	r2, [r0, r5]	@ movhi
	strb	r2, [r9, r7]
	ldrh	ip, [r0, r5]
	ldr	r9, [r0, #8]
	mov	r2, ip, lsr #8
	add	r7, r7, #1
	strb	r2, [r9, r7]
	ldr	r2, [r0, r4]
	rsb	ip, r2, #16
	mov	r9, r8, asr ip
	sub	r2, r2, #16
	mov	ip, r9, asl #16
	add	r2, r2, r6
	add	r7, r7, #1
	mov	ip, ip, lsr #16
	str	r7, [r0, #20]
	strh	ip, [r0, r5]	@ movhi
	str	r2, [r0, r4]
.L138:
	mov	r6, #5760
	add	r9, r6, #32
	ldr	r6, [r0, r9]
	cmp	r3, r6
	bcs	.L135
.L148:
	ldr	r6, [r0, sl]
	mov	r9, r3, asl #1
	ldr	r8, [sp, #8]
	ldrh	r6, [r9, r6]
	ldr	r7, [r0, r8]
	cmp	r6, #0
	ldrb	r7, [r7, r3]	@ zero_extendqisi2
	add	r3, r3, #1
	beq	.L154
	ldr	r8, .L155
	add	fp, r7, r8
	ldrb	r9, [fp, #1152]	@ zero_extendqisi2
	add	r8, r9, #256
	add	fp, r8, #1
	str	r9, [sp, #4]
	mov	r8, fp, asl #2
	add	r9, r1, r8
	ldrh	fp, [r9, #2]
	ldrh	r8, [r1, r8]
	rsb	r9, fp, #16
	cmp	r9, r2
	orr	ip, ip, r8, asl r2
	bge	.L139
	ldr	r2, [r0, #20]
	ldr	r9, [r0, #8]
	mov	ip, ip, asl #16
	mov	ip, ip, lsr #16
	strh	ip, [r0, r5]	@ movhi
	strb	ip, [r9, r2]
	ldrh	ip, [r0, r5]
	ldr	r9, [r0, #8]
	add	r2, r2, #1
	mov	ip, ip, lsr #8
	strb	ip, [r9, r2]
	ldr	r9, [r0, r4]
	rsb	ip, r9, #16
	mov	ip, r8, asr ip
	sub	r8, r9, #16
	mov	ip, ip, asl #16
	add	fp, r8, fp
	add	r2, r2, #1
	mov	ip, ip, lsr #16
	str	r2, [r0, #20]
	strh	ip, [r0, r5]	@ movhi
	str	fp, [r0, r4]
.L140:
	ldr	r2, .L155
	ldr	r9, [sp, #4]
	add	r8, r2, r9, asl #2
	ldr	r2, [r8, #1920]
	cmp	r2, #0
	beq	.L141
	ldr	r9, [r8, #2036]
	rsb	r8, r2, #16
	rsb	r7, r9, r7
	cmp	r8, fp
	orr	ip, ip, r7, asl fp
	bge	.L142
	ldr	r8, [r0, #20]
	ldr	r9, [r0, #8]
	mov	ip, ip, asl #16
	mov	fp, ip, lsr #16
	strh	fp, [r0, r5]	@ movhi
	strb	fp, [r9, r8]
	ldrh	ip, [r0, r5]
	ldr	r9, [r0, #8]
	mov	fp, ip, lsr #8
	add	r8, r8, #1
	strb	fp, [r9, r8]
	ldr	r9, [r0, r4]
	mov	ip, r7, asl #16
	rsb	fp, r9, #16
	mov	ip, ip, lsr #16
	mov	ip, ip, asr fp
	sub	fp, r9, #16
	mov	ip, ip, asl #16
	add	fp, fp, r2
	add	r8, r8, #1
	mov	ip, ip, lsr #16
	str	r8, [r0, #20]
	strh	ip, [r0, r5]	@ movhi
	str	fp, [r0, r4]
.L141:
	sub	r6, r6, #1
	cmp	r6, #255
	ldrls	r7, .L155
	ldrhi	r8, .L155
	addls	r2, r6, r7
	addhi	r2, r8, r6, lsr #7
	ldrlsb	r7, [r2, #1408]	@ zero_extendqisi2
	ldrhib	r7, [r2, #1664]	@ zero_extendqisi2
	ldr	r9, [sp, #12]
	mov	r7, r7, asl #2
	add	r8, r9, r7
	ldrh	r9, [r8, #2]
	rsb	r2, r9, #16
	cmp	r2, fp
	bge	.L145
	ldr	r8, [sp, #12]
	ldrh	r2, [r8, r7]
	orr	r8, ip, r2, asl fp
	ldr	fp, [r0, #8]
	mov	ip, r8, asl #16
	ldr	r8, [r0, #20]
	mov	ip, ip, lsr #16
	strh	ip, [r0, r5]	@ movhi
	strb	ip, [fp, r8]
	ldrh	ip, [r0, r5]
	ldr	fp, [r0, #8]
	add	r8, r8, #1
	mov	ip, ip, lsr #8
	strb	ip, [fp, r8]
	ldr	fp, [r0, r4]
	rsb	ip, fp, #16
	mov	ip, r2, asr ip
	sub	fp, fp, #16
	mov	ip, ip, asl #16
	add	r2, fp, r9
	add	r8, r8, #1
	mov	ip, ip, lsr #16
	str	r8, [r0, #20]
	strh	ip, [r0, r5]	@ movhi
	str	r2, [r0, r4]
.L146:
	ldr	r9, .L155
	add	r7, r7, r9
	ldr	r8, [r7, #2152]
	cmp	r8, #0
	beq	.L138
	ldr	r7, [r7, #2272]
	rsb	r9, r8, #16
	cmp	r9, r2
	rsb	r6, r7, r6
	bge	.L147
	orr	r7, ip, r6, asl r2
	ldr	r9, [r0, #20]
	mov	ip, r7, asl #16
	ldr	r7, [r0, #8]
	mov	r2, ip, lsr #16
	strh	r2, [r0, r5]	@ movhi
	strb	r2, [r7, r9]
	ldrh	fp, [r0, r5]
	ldr	r2, [r0, #8]
	mov	ip, fp, lsr #8
	add	r9, r9, #1
	strb	ip, [r2, r9]
	ldr	r2, [r0, r4]
	mov	fp, r6, asl #16
	mov	ip, fp, lsr #16
	rsb	fp, r2, #16
	mov	r6, ip, asr fp
	sub	r2, r2, #16
	mov	ip, r6, asl #16
	add	r2, r2, r8
	add	r6, r9, #1
	mov	ip, ip, lsr #16
	str	r6, [r0, #20]
	strh	ip, [r0, r5]	@ movhi
	str	r2, [r0, r4]
	b	.L138
.L139:
	ldr	r8, [r0, r4]
	mov	r2, ip, asl #16
	add	fp, fp, r8
	mov	ip, r2, lsr #16
	strh	ip, [r0, r5]	@ movhi
	str	fp, [r0, r4]
	b	.L140
.L137:
	ldrh	r7, [r1, r7]
	orr	ip, ip, r7, asl r2
	ldr	r7, [r0, r4]
	mov	r9, ip, asl #16
	add	r2, r6, r7
	mov	ip, r9, lsr #16
	strh	ip, [r0, r5]	@ movhi
	str	r2, [r0, r4]
	b	.L138
.L145:
	ldr	r2, [sp, #12]
	ldrh	r8, [r2, r7]
	orr	ip, ip, r8, asl fp
	ldr	r2, [r0, r4]
	mov	ip, ip, asl #16
	add	r2, r9, r2
	mov	ip, ip, lsr #16
	strh	ip, [r0, r5]	@ movhi
	str	r2, [r0, r4]
	b	.L146
.L147:
	orr	ip, ip, r6, asl r2
	ldr	r9, [r0, r4]
	mov	r6, ip, asl #16
	add	r2, r8, r9
	mov	ip, r6, lsr #16
	strh	ip, [r0, r5]	@ movhi
	str	r2, [r0, r4]
	b	.L138
.L142:
	ldr	r8, [r0, r4]
	mov	r9, ip, asl #16
	add	fp, r2, r8
	mov	ip, r9, lsr #16
	strh	ip, [r0, r5]	@ movhi
	str	fp, [r0, r4]
	b	.L141
.L153:
	ldrh	r6, [r1, #0]
	orr	r2, ip, r6, asl r2
	ldr	r5, [r0, #8]
	ldr	ip, [r0, #20]
	mov	r7, r2, asl #16
	mov	r3, #5760
	mov	r2, r7, lsr #16
	add	r3, r3, #56
	strh	r2, [r0, r3]	@ movhi
	strb	r2, [r5, ip]
	ldrh	r7, [r0, r3]
	ldr	r2, [r0, #8]
	mov	r5, r7, lsr #8
	add	ip, ip, #1
	strb	r5, [r2, ip]
	mov	r7, #5760
	add	r2, r7, #60
	ldr	r5, [r0, r2]
	rsb	r7, r5, #16
	mov	r6, r6, asr r7
	sub	r5, r5, #16
	add	r4, r5, r4
	add	ip, ip, #1
	strh	r6, [r0, r3]	@ movhi
	str	r4, [r0, r2]
	str	ip, [r0, #20]
	b	.L150
.L156:
	.align	2
.L155:
	.word	.LANCHOR1
	.size	compress_block, .-compress_block
	.section	.text.build_tree,"ax",%progbits
	.align	2
	.type	build_tree, %function
build_tree:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 80
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
	sub	sp, sp, #80
	str	r1, [sp, #44]
	ldr	r5, [r1, #8]
	ldr	r4, [r5, #12]
	str	r4, [sp, #24]
	ldr	r2, [sp, #24]
	mov	ip, #5184
	cmp	r2, #0
	ldr	r6, [sp, #44]
	mov	r3, ip
	mov	r1, #572
	mov	r4, #0
	add	ip, ip, #16
	add	r3, r3, #20
	add	r1, r1, #1
	mvnle	r9, #0
	ldr	r2, [r6, #0]
	ldr	r8, [r5, #0]
	movle	r7, r4
	str	r1, [r0, r3]
	str	r4, [r0, ip]
	strle	r9, [sp, #40]
	ble	.L159
	ldrh	r5, [r2, #0]
	ldr	sl, [sp, #24]
	cmp	r5, #0
	sub	r9, sl, #1
	mvn	fp, #0
	str	fp, [sp, #40]
	and	r5, r9, #3
	streqh	r4, [r2, #2]	@ movhi
	beq	.L325
	add	r6, r0, #5184
	add	sl, r6, #24
	add	r1, r0, #2912
	mov	r6, #1
	str	r6, [r0, ip]
	str	r4, [r1, #0]
	strb	r4, [sl, #0]
	str	r4, [sp, #40]
.L325:
	ldr	fp, [sp, #24]
	mov	r1, #1
	cmp	r1, fp
	mov	r3, #4
	beq	.L326
	cmp	r5, #0
	beq	.L348
	cmp	r5, r1
	beq	.L300
	cmp	r5, #2
	beq	.L301
	ldrh	r5, [r2, r3]
	cmp	r5, #0
	ldrne	r6, [r0, ip]
	addne	r5, r0, #5184
	addne	r7, r0, r6, asl #2
	addne	r5, r5, #25
	addne	r6, r6, #1
	streqh	r5, [r2, #6]	@ movhi
	strne	r1, [r7, #2912]
	strne	r6, [r0, ip]
	strneb	r4, [r5, #0]
	add	r3, r3, #4
	strne	r1, [sp, #40]
	add	r1, r1, #1
.L301:
	ldrh	r7, [r2, r3]
	cmp	r7, #0
	addeq	r6, r2, r3
	streqh	r7, [r6, #2]	@ movhi
	beq	.L330
	ldr	r6, [r0, ip]
	add	r5, r0, r1
	add	fp, r5, #5184
	add	sl, r0, r6, asl #2
	add	r9, fp, #24
	add	r6, r6, #1
	str	r6, [r0, ip]
	str	r1, [sl, #2912]
	strb	r4, [r9, #0]
	str	r1, [sp, #40]
.L330:
	add	r1, r1, #1
	add	r3, r3, #4
.L300:
	ldrh	r7, [r2, r3]
	cmp	r7, #0
	addeq	r6, r2, r3
	streqh	r7, [r6, #2]	@ movhi
	beq	.L332
	ldr	r6, [r0, ip]
	add	r5, r0, r1
	add	fp, r5, #5184
	add	sl, r0, r6, asl #2
	add	r9, fp, #24
	add	r6, r6, #1
	str	r6, [r0, ip]
	str	r1, [sl, #2912]
	strb	r4, [r9, #0]
	str	r1, [sp, #40]
.L332:
	ldr	r7, [sp, #24]
	add	r1, r1, #1
	cmp	r1, r7
	add	r3, r3, #4
	beq	.L326
.L348:
	ldr	sl, [sp, #40]
	ldr	r9, [sp, #24]
.L162:
	ldrh	r7, [r2, r3]
	cmp	r7, #0
	addeq	r6, r2, r3
	streqh	r7, [r6, #2]	@ movhi
	beq	.L161
	ldr	r6, [r0, ip]
	add	r7, r0, r1
	add	r5, r7, #5184
	add	sl, r0, r6, asl #2
	add	fp, r5, #24
	add	r6, r6, #1
	str	r6, [r0, ip]
	strb	r4, [fp, #0]
	str	r1, [sl, #2912]
	mov	sl, r1
.L161:
	add	r3, r3, #4
	ldrh	fp, [r2, r3]
	cmp	fp, #0
	addeq	r6, r2, r3
	add	r1, r1, #1
	streqh	fp, [r6, #2]	@ movhi
	beq	.L333
	ldr	r7, [r0, ip]
	add	sl, r0, r1
	add	r5, sl, #5184
	add	fp, r0, r7, asl #2
	add	r6, r7, #1
	add	sl, r5, #24
	str	r6, [r0, ip]
	str	r1, [fp, #2912]
	strb	r4, [sl, #0]
	mov	sl, r1
.L333:
	add	r6, r3, #4
	ldrh	fp, [r2, r6]
	cmp	fp, #0
	addeq	r6, r2, r6
	add	r5, r1, #1
	streqh	fp, [r6, #2]	@ movhi
	beq	.L335
	ldr	r7, [r0, ip]
	add	fp, r0, r5
	add	r6, fp, #5184
	add	sl, r0, r7, asl #2
	add	fp, r6, #24
	add	r7, r7, #1
	str	r7, [r0, ip]
	strb	r4, [fp, #0]
	str	r5, [sl, #2912]
	mov	sl, r5
.L335:
	add	r7, r3, #8
	ldrh	r6, [r2, r7]
	cmp	r6, #0
	addeq	r7, r2, r7
	add	r5, r1, #2
	streqh	r6, [r7, #2]	@ movhi
	beq	.L337
	ldr	r7, [r0, ip]
	add	sl, r0, r5
	add	r6, sl, #5184
	add	r6, r6, #24
	add	fp, r7, #1
	add	sl, r0, r7, asl #2
	str	fp, [r0, ip]
	strb	r4, [r6, #0]
	str	r5, [sl, #2912]
	mov	sl, r5
.L337:
	add	r1, r1, #3
	cmp	r1, r9
	add	r3, r3, #12
	bne	.L162
	str	sl, [sp, #40]
.L326:
	mov	r3, #5184
	add	r3, r3, #16
	ldr	r7, [r0, r3]
.L159:
	cmp	r8, #0
	beq	.L352
	mov	r4, #5760
	mov	ip, r4
	mov	r5, #5184
	add	r5, r5, #16
	add	r4, r4, #40
	add	ip, ip, #44
	mov	r6, #0
	ldr	sl, [sp, #40]
	b	.L305
.L170:
	cmp	sl, #1
	addle	r3, sl, #1
	movgt	r1, #0
	str	r7, [r0, r5]
	movgt	r3, r1
	movle	r1, r3, asl #2
	add	r7, r0, r7, asl #2
	mov	fp, #1	@ movhi
	str	r3, [r7, #2908]
	strh	fp, [r2, r1]	@ movhi
	add	r9, r3, r0
	ldr	r7, [r0, r4]
	movle	sl, r3
	add	r3, r9, #5184
	add	fp, r3, #24
	sub	r9, r7, #1
	strb	r6, [fp, #0]
	str	r9, [r0, r4]
	add	r3, r8, r1
	ldrh	fp, [r3, #2]
	ldr	r1, [r0, ip]
	rsb	r3, fp, r1
	str	r3, [r0, ip]
	ldr	r7, [r0, r5]
.L305:
	cmp	r7, #1
	add	r7, r7, #1
	ble	.L170
	str	sl, [sp, #40]
.L164:
	add	r6, sp, #40
	ldmia	r6, {r6, sl}	@ phole ldm
	mov	r1, #5184
	str	r6, [sl, #4]
	add	sl, r1, #16
	ldr	r5, [r0, sl]
	add	r8, r5, r5, lsr #31
	mov	r3, r8, asr #1
	cmp	r3, #0
	str	r3, [sp, #8]
	ble	.L171
	add	r7, r3, #724
	add	fp, r7, #2
	add	r9, r0, fp, asl #2
	add	r3, r9, #4
	str	r3, [sp, #16]
	mov	ip, r3
.L182:
	ldr	fp, [sp, #8]
	ldr	ip, [ip, #0]
	mov	r3, fp, asl #1
	cmp	r3, r5
	str	ip, [sp, #4]
	movgt	ip, fp
	bgt	.L173
	add	r1, ip, r0
	add	r8, r1, #5184
	mov	r4, ip
	add	ip, r8, #24
	mov	r6, fp
	add	r8, r2, r4, asl #2
	str	ip, [sp, #28]
	b	.L180
.L354:
	add	r9, r0, r3, asl #2
	ldr	r1, [r9, #2908]
	mov	r5, r1, asl #2
	ldrh	r4, [r5, r2]
	mov	ip, r3
.L175:
	ldrh	r3, [r8, #0]
	cmp	r3, r4
	bcc	.L178
.L355:
	beq	.L353
.L179:
	ldr	r5, [r0, sl]
	mov	r3, ip, asl #1
	add	r6, r0, r6, asl #2
	cmp	r3, r5
	str	r1, [r6, #2908]
	bgt	.L173
	mov	r6, ip
.L180:
	cmp	r3, r5
	bge	.L354
	add	r4, r0, r3, asl #2
	ldr	r5, [r4, #2912]
	ldr	r1, [r4, #2908]
	mov	ip, r5, asl #2
	mov	fp, r1, asl #2
	ldrh	r7, [ip, r2]
	ldrh	r4, [fp, r2]
	cmp	r7, r4
	add	ip, r3, #1
	bcc	.L176
	beq	.L177
.L349:
	mov	ip, r3
	ldrh	r3, [r8, #0]
	cmp	r3, r4
	bcs	.L355
.L178:
	mov	ip, r6
.L173:
	ldr	r4, [sp, #8]
	ldr	r5, [sp, #4]
	add	ip, r0, ip, asl #2
	subs	r7, r4, #1
	str	r7, [sp, #8]
	str	r5, [ip, #2908]
	ldr	r1, [sp, #16]
	sub	r8, r1, #4
	str	r8, [sp, #16]
	beq	.L181
	ldr	r5, [r0, sl]
	ldr	ip, [sp, #16]
	b	.L182
.L177:
	add	r9, r1, r0
	add	r9, r9, #5184
	add	fp, r5, r0
	str	r9, [sp, #12]
	add	fp, fp, #5184
	add	fp, fp, #24
	ldrb	fp, [fp, #0]	@ zero_extendqisi2
	str	fp, [sp, #20]
	add	r9, r9, #24
	ldrb	fp, [r9, #0]	@ zero_extendqisi2
	ldr	r9, [sp, #20]
	cmp	r9, fp
	bhi	.L349
.L176:
	mov	r4, r7
	mov	r1, r5
	b	.L175
.L353:
	add	r3, r1, r0
	add	fp, r3, #5184
	ldr	r4, [sp, #28]
	add	r9, fp, #24
	ldrb	r5, [r9, #0]	@ zero_extendqisi2
	ldrb	r7, [r4, #0]	@ zero_extendqisi2
	cmp	r7, r5
	bhi	.L179
	b	.L178
.L181:
	mov	r9, #5184
	add	sl, r9, #16
	ldr	r5, [r0, sl]
.L171:
	ldr	r6, [sp, #24]
	add	r4, r6, #5184
	add	r7, r4, #24
	mov	r8, #5184
	add	r1, r0, r7
	add	r3, r8, #20
	mov	fp, #5184
	str	r1, [sp, #8]
	str	r6, [sp, #12]
	str	r3, [sp, #28]
	add	fp, fp, #16
	add	sl, r2, r6, asl #2
.L202:
	add	r3, r0, r5, asl #2
	ldr	r6, [r3, #2908]
	str	r6, [sp, #16]
	sub	r5, r5, #1
	ldr	r9, [r0, #2912]
	cmp	r5, #1
	str	r5, [r0, fp]
	movle	ip, #1
	str	r9, [sp, #20]
	str	r6, [r0, #2912]
	ble	.L184
	ldr	ip, [sp, #16]
	add	r4, ip, r0
	add	r7, r4, #5184
	add	r1, r7, #24
	add	r8, r2, ip, asl #2
	str	sl, [sp, #32]
	str	r1, [sp, #4]
	mov	r6, #1
	mov	r3, #2
	mov	sl, r8
	str	fp, [sp, #36]
	b	.L191
.L357:
	add	r9, r0, r3, asl #2
	ldr	r1, [r9, #2908]
	mov	fp, r1, asl #2
	ldrh	r4, [fp, r2]
	mov	ip, r3
.L186:
	ldrh	r3, [sl, #0]
	cmp	r3, r4
	bcc	.L189
.L358:
	beq	.L356
	mov	r3, ip, asl #1
	add	r6, r0, r6, asl #2
	cmp	r5, r3
	str	r1, [r6, #2908]
	blt	.L340
.L362:
	mov	r6, ip
.L191:
	cmp	r5, r3
	ble	.L357
	add	r1, r0, r3, asl #2
	ldr	r7, [r1, #2912]
	ldr	r1, [r1, #2908]
	mov	r4, r7, asl #2
	mov	ip, r1, asl #2
	ldrh	r8, [r4, r2]
	ldrh	r4, [ip, r2]
	cmp	r8, r4
	add	ip, r3, #1
	bcc	.L187
	beq	.L188
.L350:
	mov	ip, r3
	ldrh	r3, [sl, #0]
	cmp	r3, r4
	bcs	.L358
.L189:
	add	sl, sp, #32
	ldmia	sl, {sl, fp}	@ phole ldm
	mov	ip, r6
.L184:
	ldr	r1, [sp, #16]
	add	r4, r0, ip, asl #2
	str	r1, [r4, #2908]
	ldr	r3, [sp, #28]
	ldr	r7, [r0, r3]
	ldr	r9, [sp, #20]
	add	ip, r0, r7, asl #2
	ldr	r3, [r0, #2912]
	str	r9, [ip, #2904]
	ldr	r6, [sp, #28]
	sub	r5, r7, #2
	str	r5, [r0, r6]
	str	r3, [ip, #2900]
	mov	ip, r9, asl #2
	mov	r9, r3, asl #2
	ldr	r7, [sp, #20]
	ldrh	r1, [r2, ip]
	ldrh	r4, [r2, r9]
	add	r5, r7, r0
	add	r8, r4, r1
	add	r3, r3, r0
	strh	r8, [sl, #0]	@ movhi
	add	r6, r3, #5184
	add	r4, r5, #5184
	add	r1, r6, #24
	add	r7, r4, #24
	ldrb	r8, [r1, #0]	@ zero_extendqisi2
	ldrb	r6, [r7, #0]	@ zero_extendqisi2
	cmp	r8, r6
	movcs	r6, r8
	add	r5, sp, #8
	ldmia	r5, {r5, r8}	@ phole ldm
	mov	r3, r8, asl #16
	mov	r7, r3, lsr #16
	add	r4, r6, #1
	add	ip, r2, ip
	add	r1, r2, r9
	strb	r4, [r5, #0]
	strh	r7, [r1, #2]	@ movhi
	strh	r7, [ip, #2]	@ movhi
	ldr	r4, [r0, fp]
	cmp	r4, #1
	str	r8, [r0, #2912]
	movle	ip, #1
	ble	.L193
	mov	r5, #1
	mov	r3, #2
	b	.L200
.L360:
	add	ip, r0, r3, asl #2
	ldr	r1, [ip, #2908]
	mov	r9, r1, asl #2
	ldrh	r4, [r9, r2]
	mov	ip, r3
.L195:
	ldrh	r6, [sl, #0]
	cmp	r6, r4
	bcc	.L198
.L361:
	beq	.L359
.L199:
	ldr	r4, [r0, fp]
	mov	r3, ip, asl #1
	add	r5, r0, r5, asl #2
	cmp	r3, r4
	str	r1, [r5, #2908]
	bgt	.L193
	mov	r5, ip
.L200:
	cmp	r3, r4
	bge	.L360
	add	r1, r0, r3, asl #2
	ldr	r6, [r1, #2912]
	ldr	r1, [r1, #2908]
	mov	r4, r6, asl #2
	mov	r8, r1, asl #2
	ldrh	r7, [r4, r2]
	ldrh	r4, [r8, r2]
	cmp	r7, r4
	add	ip, r3, #1
	bcc	.L196
	beq	.L197
.L351:
	ldrh	r6, [sl, #0]
	cmp	r6, r4
	mov	ip, r3
	bcs	.L361
.L198:
	mov	ip, r5
.L193:
	ldr	r7, [sp, #12]
	add	ip, r0, ip, asl #2
	str	r7, [ip, #2908]
	ldr	r5, [r0, fp]
	ldr	r3, [sp, #8]
	cmp	r5, #1
	add	r1, r3, #1
	add	sl, sl, #4
	str	r1, [sp, #8]
	ble	.L201
	add	r4, r7, #1
	str	r4, [sp, #12]
	b	.L202
.L188:
	add	r9, r7, r0
	add	r9, r9, #5184
	add	r9, r9, #24
	ldrb	r9, [r9, #0]	@ zero_extendqisi2
	add	fp, r1, r0
	str	r9, [sp, #24]
	add	fp, fp, #5184
	add	fp, fp, #24
	ldrb	r9, [fp, #0]	@ zero_extendqisi2
	ldr	fp, [sp, #24]
	cmp	fp, r9
	bhi	.L350
.L187:
	mov	r4, r8
	mov	r1, r7
	b	.L186
.L356:
	add	r3, r1, r0
	add	r9, r3, #5184
	ldr	r7, [sp, #4]
	add	fp, r9, #24
	ldrb	r8, [fp, #0]	@ zero_extendqisi2
	ldrb	r4, [r7, #0]	@ zero_extendqisi2
	cmp	r4, r8
	bls	.L189
	mov	r3, ip, asl #1
	add	r6, r0, r6, asl #2
	cmp	r5, r3
	str	r1, [r6, #2908]
	bge	.L362
.L340:
	add	sl, sp, #32
	ldmia	sl, {sl, fp}	@ phole ldm
	b	.L184
.L197:
	add	r8, r6, r0
	add	r8, r8, #5184
	add	r8, r8, #24
	ldrb	r8, [r8, #0]	@ zero_extendqisi2
	add	r9, r1, r0
	str	r8, [sp, #4]
	add	r9, r9, #5184
	add	r9, r9, #24
	ldrb	r8, [r9, #0]	@ zero_extendqisi2
	ldr	r9, [sp, #4]
	cmp	r9, r8
	bhi	.L351
.L196:
	mov	r4, r7
	mov	r1, r6
	b	.L195
.L359:
	add	r6, r1, r0
	add	r8, r6, #5184
	ldr	r7, [sp, #8]
	add	r9, r8, #24
	ldrb	r3, [r9, #0]	@ zero_extendqisi2
	ldrb	r4, [r7, #0]	@ zero_extendqisi2
	cmp	r4, r3
	bhi	.L199
	b	.L198
.L201:
	mov	r1, #5184
	add	r1, r1, #20
	ldr	r4, [r0, r1]
	ldr	r3, [r0, #2912]
	add	r5, r0, r4, asl #2
	sub	ip, r4, #1
	str	ip, [r0, r1]
	str	r3, [r5, #2904]
	add	sl, r0, #2880
	ldr	r9, [sp, #44]
	add	r4, sl, #4
	add	ip, r4, #4
	ldr	r5, [r9, #0]
	add	sl, ip, #4
	ldr	fp, [sp, #44]
	add	r9, sl, #4
	add	r6, r5, r3, asl #2
	add	r8, r9, #4
	ldr	r7, [fp, #8]
	str	r6, [sp, #4]
	str	r8, [sp, #16]
	ldr	r8, [fp, #4]
	ldr	fp, [r7, #0]
	ldr	r6, [r7, #16]
	str	fp, [sp, #12]
	ldr	fp, [r7, #4]
	str	fp, [sp, #20]
	ldr	r7, [r7, #8]
	mov	r3, #0
	str	r7, [sp, #8]
	str	r3, [r0, #2876]
	str	r3, [r0, #2880]
	str	r3, [r0, #2884]
	str	r3, [r4, #4]
	str	r3, [ip, #4]
	str	r3, [sl, #4]
	str	r3, [r9, #4]
	ldr	ip, [sp, #16]
	str	r3, [ip, #4]
	ldr	fp, [sp, #4]
	strh	r3, [fp, #2]	@ movhi
	ldr	r1, [r0, r1]
	add	sl, r1, #1
	cmp	sl, #572
	str	r1, [sp, #28]
	str	sl, [sp, #24]
	bgt	.L203
	ldr	r7, [sp, #12]
	cmp	r7, r3
	beq	.L363
	ldr	r4, [sp, #28]
	add	r9, r4, #724
	add	fp, r9, #2
	add	ip, r0, fp, asl #2
	add	fp, ip, #4
	ldr	ip, [fp, #4]
	mov	sl, ip, asl #2
	str	sl, [sp, #32]
	add	r7, r0, #5184
	add	r4, r7, #12
	add	r9, r5, sl
	ldrh	r1, [r9, #2]
	str	r4, [sp, #4]
	ldr	r7, [sp, #4]
	add	sl, r5, r1, asl #2
	ldrh	r4, [sl, #2]
	rsb	r1, fp, r7
	sub	sl, r1, #4
	mov	r7, #5760
	add	r1, r4, #1
	mov	r4, sl, lsr #2
	and	r4, r4, #1
	mov	sl, r7
	cmp	r6, r1
	str	r3, [sp, #16]
	add	r7, r7, #40
	add	sl, sl, #44
	str	r4, [sp, #36]
	movlt	r1, #1
	strlt	r1, [sp, #16]
	movlt	r1, r6
.L264:
	cmp	r8, ip
	strh	r1, [r9, #2]	@ movhi
	blt	.L265
	add	r4, r1, #1424
	add	r3, r4, #12
	add	r4, r0, r3, asl #1
	ldrh	r3, [r4, #4]!
	str	r3, [sp, #44]
	ldr	r3, [sp, #8]
	cmp	r3, ip
	ldr	r3, [sp, #44]
	add	r3, r3, #1
	str	r3, [sp, #44]
	strh	r3, [r4, #0]	@ movhi
	ble	.L266
	mov	ip, #0
.L319:
	ldrh	r4, [r9, #0]
	ldr	r9, [r0, r7]
	add	r3, ip, r1
	mla	r3, r4, r3, r9
	str	r3, [r0, r7]
	ldr	r9, [sp, #12]
	ldr	r1, [sp, #32]
	add	r3, r9, r1
	ldrh	r3, [r3, #2]
	ldr	r1, [r0, sl]
	add	r9, ip, r3
	mla	r9, r4, r9, r1
	str	r9, [r0, sl]
.L265:
	add	r1, fp, #4
	ldr	fp, [sp, #4]
	cmp	r1, fp
	beq	.L320
	ldr	ip, [sp, #36]
	cmp	ip, #0
	beq	.L346
	ldr	r4, [r1, #4]
	mov	r9, r4, asl #2
	str	r9, [sp, #32]
	add	fp, r5, r9
	ldrh	ip, [fp, #2]
	add	r3, r5, ip, asl #2
	ldrh	r9, [r3, #2]
	add	ip, r9, #1
	cmp	r6, ip
	ldrlt	r9, [sp, #16]
	addlt	r9, r9, #1
	strlt	r9, [sp, #16]
	movlt	ip, r6
	cmp	r8, r4
	strh	ip, [fp, #2]	@ movhi
	blt	.L269
	add	r9, ip, #1424
	add	r3, r9, #12
	add	r9, r0, r3, asl #1
	ldrh	r3, [r9, #4]!
	str	r3, [sp, #36]
	ldr	r3, [sp, #8]
	cmp	r3, r4
	ldr	r3, [sp, #36]
	add	r3, r3, #1
	str	r3, [sp, #36]
	strh	r3, [r9, #0]	@ movhi
	ble	.L270
	mov	r4, #0
.L322:
	ldrh	r9, [fp, #0]
	ldr	fp, [r0, r7]
	add	r3, r4, ip
	mla	r3, r9, r3, fp
	str	r3, [r0, r7]
	ldr	fp, [sp, #12]
	ldr	ip, [sp, #32]
	add	r3, fp, ip
	ldrh	r3, [r3, #2]
	ldr	ip, [r0, sl]
	add	fp, r4, r3
	mla	fp, r9, fp, ip
	str	fp, [r0, sl]
.L269:
	ldr	r4, [sp, #4]
	add	r1, r1, #4
	cmp	r1, r4
	beq	.L320
.L346:
	ldr	fp, [sp, #16]
	str	r2, [sp, #32]
	b	.L215
.L275:
	ldr	r4, [sp, #8]
	ldr	r9, [sp, #20]
	rsb	r2, r4, r2
	ldr	r4, [r9, r2, asl #2]
.L324:
	ldrh	r2, [r5, ip]
	ldr	r9, [r0, r7]
	add	r3, r4, r3
	mla	r3, r2, r3, r9
	str	r3, [r0, r7]
	ldr	r3, [sp, #12]
	add	ip, r3, ip
	ldrh	r9, [ip, #2]
	ldr	r3, [r0, sl]
	add	ip, r4, r9
	mla	r3, r2, ip, r3
	str	r3, [r0, sl]
.L274:
	ldr	ip, [sp, #4]
	add	r1, r1, #4
	cmp	r1, ip
	beq	.L364
.L215:
	ldr	r2, [r1, #4]
	mov	ip, r2, asl #2
	add	r9, r5, ip
	ldrh	r3, [r9, #2]
	add	r4, r5, r3, asl #2
	ldrh	r3, [r4, #2]
	add	r3, r3, #1
	cmp	r6, r3
	movlt	r3, r6
	addlt	fp, fp, #1
	cmp	r8, r2
	strh	r3, [r9, #2]	@ movhi
	blt	.L212
	add	r9, r3, #1424
	add	r4, r9, #12
	add	r4, r0, r4, asl #1
	ldrh	r9, [r4, #4]
	str	r9, [sp, #16]
	ldr	r9, [sp, #8]
	cmp	r9, r2
	ldr	r9, [sp, #16]
	add	r9, r9, #1
	str	r9, [sp, #16]
	strh	r9, [r4, #4]	@ movhi
	ldrle	r4, [sp, #8]
	ldrle	r9, [sp, #20]
	rsble	r2, r4, r2
	ldrle	r4, [r9, r2, asl #2]
	movgt	r4, #0
	ldrh	r2, [r5, ip]
	ldr	r9, [r0, r7]
	add	r3, r4, r3
	mla	r3, r2, r3, r9
	str	r3, [r0, r7]
	ldr	r3, [sp, #12]
	add	r9, r3, ip
	ldrh	r3, [r9, #2]
	ldr	r9, [r0, sl]
	add	ip, r4, r3
	mla	r9, r2, ip, r9
	str	r9, [r0, sl]
.L212:
	add	r1, r1, #4
	ldr	r2, [r1, #4]
	mov	ip, r2, asl #2
	add	r4, r5, ip
	ldrh	r3, [r4, #2]
	add	r9, r5, r3, asl #2
	ldrh	r3, [r9, #2]
	add	r3, r3, #1
	cmp	r6, r3
	addlt	fp, fp, #1
	movlt	r3, r6
.L273:
	cmp	r8, r2
	strh	r3, [r4, #2]	@ movhi
	blt	.L274
	add	r9, r3, #1424
	add	r4, r9, #12
	add	r4, r0, r4, asl #1
	ldrh	r9, [r4, #4]
	str	r9, [sp, #16]
	ldr	r9, [sp, #8]
	cmp	r9, r2
	ldr	r9, [sp, #16]
	add	r9, r9, #1
	str	r9, [sp, #16]
	strh	r9, [r4, #4]	@ movhi
	ble	.L275
	mov	r4, #0
	b	.L324
.L342:
	ldr	r2, [sp, #12]
.L203:
	mov	sl, #2864
	add	ip, sl, #12
	ldrh	r1, [r0, ip]
	mov	r6, #2864
	add	fp, r6, #14
	ldrh	r4, [r0, fp]
	mov	r8, r1, asl #17
	mov	r8, r8, lsr #16
	add	r3, fp, #2
	add	r9, r8, r4
	ldrh	r5, [r0, r3]
	mov	r7, r9, asl #17
	mov	r7, r7, lsr #16
	add	sl, r7, r5
	add	ip, r3, #2
	ldrh	r1, [r0, ip]
	mov	r6, sl, asl #17
	mov	r6, r6, lsr #16
	mov	r4, #2880
	add	r9, r6, r1
	add	fp, r4, #4
	ldrh	sl, [r0, fp]
	mov	r5, r9, asl #17
	mov	r5, r5, lsr #16
	mov	r3, #2880
	add	ip, r5, sl
	add	r1, r3, #6
	ldrh	r9, [r0, r1]
	mov	r4, ip, asl #17
	mov	r4, r4, lsr #16
	mov	fp, #2880
	add	sl, r4, r9
	add	r3, fp, #8
	ldrh	r1, [r0, r3]
	mov	ip, sl, asl #17
	mov	ip, ip, lsr #16
	mov	r9, #2880
	add	fp, ip, r1
	add	sl, r9, #10
	ldrh	r3, [r0, sl]
	mov	r1, fp, asl #17
	mov	r1, r1, lsr #16
	add	fp, r1, r3
	mov	r9, #2880
	add	sl, r9, #12
	mov	r9, fp, asl #17
	mov	r3, r9, lsr #16
	ldrh	sl, [r0, sl]
	str	r3, [sp, #16]
	ldr	fp, [sp, #16]
	mov	r9, #2880
	add	sl, fp, sl
	add	r3, r9, #14
	ldrh	fp, [r0, r3]
	mov	r9, sl, asl #17
	mov	sl, r9, lsr #16
	add	r9, sl, fp
	str	sl, [sp, #12]
	mov	fp, r9, asl #17
	mov	sl, fp, lsr #16
	mov	r3, #2896
	ldrh	r3, [r0, r3]
	str	sl, [sp, #4]
	ldr	r9, [sp, #4]
	mov	fp, #2896
	add	r3, r9, r3
	add	sl, fp, #2
	mov	fp, r3, asl #17
	mov	r3, fp, lsr #16
	ldrh	r9, [r0, sl]
	str	r3, [sp, #8]
	ldr	fp, [sp, #8]
	mov	r3, #2896
	add	sl, fp, r9
	add	fp, r3, #4
	mov	r9, sl, asl #17
	ldrh	sl, [r0, fp]
	mov	r9, r9, lsr #16
	mov	r3, #2896
	add	fp, r9, sl
	add	r3, r3, #6
	mov	sl, fp, asl #17
	ldrh	fp, [r0, r3]
	mov	sl, sl, lsr #16
	mov	r3, #2896
	add	fp, sl, fp
	add	r3, r3, #8
	ldrh	r0, [r0, r3]
	mov	r3, fp, asl #17
	mov	r3, r3, lsr #16
	add	fp, r3, r0
	mov	r0, fp, asl #1
	strh	r4, [sp, #58]	@ movhi
	ldr	fp, [sp, #40]
	strh	ip, [sp, #60]	@ movhi
	add	r4, sp, #12
	ldmia	r4, {r4, ip}	@ phole ldm
	cmn	fp, #1
	strh	r0, [sp, #78]	@ movhi
	strh	r7, [sp, #52]	@ movhi
	strh	ip, [sp, #64]	@ movhi
	strh	r8, [sp, #50]	@ movhi
	strh	r6, [sp, #54]	@ movhi
	strh	r5, [sp, #56]	@ movhi
	strh	r1, [sp, #62]	@ movhi
	strh	r4, [sp, #66]	@ movhi
	ldmib	sp, {r0, r4}	@ phole ldm
	movne	ip, #0
	strh	r4, [sp, #70]	@ movhi
	strh	r0, [sp, #68]	@ movhi
	strh	r9, [sp, #72]	@ movhi
	strh	sl, [sp, #74]	@ movhi
	strh	r3, [sp, #76]	@ movhi
	movne	r4, ip
	ldrne	r7, [sp, #40]
	beq	.L232
.L231:
	add	r0, r2, ip
	ldrh	r3, [r0, #2]
	cmp	r3, #0
	beq	.L229
	mov	r6, r3, asl #1
	add	r5, sp, #80
	add	r1, r6, r5
	ldrh	r5, [r1, #-32]
	sub	r3, r3, #1
	and	r8, r5, #1
	subs	r1, r3, #0
	mov	r0, r8, asl #1
	and	r8, r3, #3
	mov	r3, r5, lsr #1
	beq	.L310
	cmp	r8, #0
	beq	.L230
	cmp	r8, #1
	beq	.L295
	cmp	r8, #2
	andne	r3, r3, #1
	orrne	r0, r3, r0
	movne	r3, r5, lsr #2
	and	r8, r3, #1
	movne	r0, r0, asl #1
	subne	r1, r1, #1
	orr	r0, r8, r0
	mov	r3, r3, lsr #1
	mov	r0, r0, asl #1
	sub	r1, r1, #1
.L295:
	and	r8, r3, #1
	orr	r0, r8, r0
	subs	r1, r1, #1
	mov	r3, r3, lsr #1
	mov	r0, r0, asl #1
	beq	.L310
.L230:
	and	r8, r3, #1
	mov	sl, r3, lsr #1
	orr	r0, r8, r0
	and	sl, sl, #1
	mov	r8, r3, lsr #2
	orr	sl, sl, r0, asl #1
	and	r8, r8, #1
	mov	r0, r3, lsr #3
	orr	r8, r8, sl, asl #1
	and	r0, r0, #1
	orr	r0, r0, r8, asl #1
	subs	r1, r1, #4
	mov	r3, r3, lsr #4
	mov	r0, r0, asl #1
	bne	.L230
.L310:
	add	r1, sp, #80
	add	r6, r6, r1
	add	r5, r5, #1
	mov	r3, r0, lsr #1
	strh	r5, [r6, #-32]	@ movhi
	strh	r3, [r2, ip]	@ movhi
.L229:
	add	r4, r4, #1
	cmp	r7, r4
	add	ip, ip, #4
	bge	.L231
.L232:
	add	sp, sp, #80
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
	bx	lr
.L364:
	str	fp, [sp, #16]
	ldr	r2, [sp, #32]
.L320:
	ldr	sl, [sp, #24]
	ldr	r7, [sp, #28]
	rsb	r1, r7, sl
	add	r1, r1, #572
.L216:
	ldr	fp, [sp, #16]
	cmp	fp, #0
	beq	.L203
	add	ip, r6, #1424
	add	r9, ip, #12
	add	r4, r6, #1424
	add	fp, r4, #11
	add	ip, r0, r9, asl #1
	sub	r3, r6, #1
	add	r7, r0, fp, asl #1
	add	sl, ip, #4
	str	r3, [sp, #4]
	ldr	fp, [sp, #16]
	add	r9, r7, #4
	mov	r7, sl
.L223:
	ldrh	r4, [r9, #0]
	cmp	r4, #0
	ldrne	ip, [sp, #4]
	bne	.L221
	ldr	ip, [sp, #4]
	mov	r3, sl
.L222:
	ldrh	r4, [r3, #-4]
	cmp	r4, #0
	sub	ip, ip, #1
	sub	r3, r3, #2
	beq	.L222
.L221:
	add	r3, ip, #1424
	add	r3, r3, #12
	add	r3, r0, r3, asl #1
	sub	r4, r4, #1
	add	ip, ip, #1424
	strh	r4, [r3, #4]	@ movhi
	add	ip, ip, #13
	add	r4, r0, ip, asl #1
	ldrh	r3, [r4, #4]
	add	r3, r3, #2
	strh	r3, [r4, #4]	@ movhi
	ldrh	ip, [r7, #0]
	sub	r4, ip, #1
	mov	r3, r4, asl #16
	sub	fp, fp, #2
	mov	ip, r3, lsr #16
	cmp	fp, #0
	strh	ip, [r7, #0]	@ movhi
	bgt	.L223
	cmp	r6, #0
	beq	.L203
	ldr	r7, [sp, #4]
	add	r3, r7, #1424
	add	r4, r3, #12
	add	r9, r0, r4, asl #1
	add	fp, r9, #4
	mov	sl, #5760
	str	r2, [sp, #12]
	str	fp, [sp, #8]
	add	sl, sl, #40
	mov	fp, r7
.L228:
	subs	r2, ip, #0
	beq	.L224
	add	r9, r1, #724
	add	r7, r9, #2
	add	ip, r0, r7, asl #2
	add	r3, ip, #4
.L227:
	ldr	r4, [r3, #-4]
	mov	r7, r4, asl #2
	cmp	r8, r4
	add	ip, r5, r7
	sub	r1, r1, #1
	blt	.L225
	ldrh	r4, [ip, #2]
	cmp	r4, r6
	beq	.L226
	ldrh	r9, [r5, r7]
	ldr	r7, [r0, sl]
	rsb	r4, r4, r6
	mla	r4, r9, r4, r7
	str	r4, [r0, sl]
	strh	r6, [ip, #2]	@ movhi
.L226:
	sub	r2, r2, #1
.L225:
	cmp	r2, #0
	sub	r3, r3, #4
	bne	.L227
.L224:
	ldr	r2, [sp, #8]
	cmp	fp, #0
	sub	ip, r2, #2
	str	ip, [sp, #8]
	mov	r6, fp
	beq	.L342
	sub	fp, fp, #1
	ldrh	ip, [ip, #2]
	b	.L228
.L270:
	ldr	r9, [sp, #8]
	rsb	r3, r9, r4
	ldr	r9, [sp, #20]
	ldr	r4, [r9, r3, asl #2]
	b	.L322
.L266:
	ldr	r4, [sp, #8]
	rsb	r3, r4, ip
	ldr	r4, [sp, #20]
	ldr	ip, [r4, r3, asl #2]
	b	.L319
.L363:
	add	ip, r1, #724
	add	r9, ip, #2
	add	sl, r0, r9, asl #2
	add	r9, sl, #4
	ldr	r1, [r9, #4]
	add	sl, r5, r1, asl #2
	ldrh	r4, [sl, #2]
	add	fp, r0, #5184
	add	r7, r5, r4, asl #2
	add	fp, fp, #12
	ldrh	ip, [r7, #2]
	rsb	r3, r9, fp
	sub	r4, r3, #4
	mov	r4, r4, lsr #2
	add	r3, ip, #1
	ldr	r7, [sp, #12]
	mov	ip, #5760
	and	r4, r4, #1
	cmp	r6, r3
	str	r7, [sp, #16]
	add	ip, ip, #40
	str	r4, [sp, #4]
	movlt	r4, #1
	strlt	r4, [sp, #16]
	movlt	r3, r6
.L251:
	cmp	r8, r1
	strh	r3, [sl, #2]	@ movhi
	blt	.L252
	add	r4, r3, #1424
	add	r7, r4, #12
	add	r7, r0, r7, asl #1
	ldrh	r4, [r7, #4]!
	str	r4, [sp, #12]
	ldr	r4, [sp, #8]
	cmp	r4, r1
	ldr	r4, [sp, #12]
	add	r4, r4, #1
	str	r4, [sp, #12]
	strh	r4, [r7, #0]	@ movhi
	ldrle	r7, [sp, #8]
	ldrle	r4, [sp, #20]
	rsble	r1, r7, r1
	ldrle	r1, [r4, r1, asl #2]
	ldr	r7, [r0, ip]
	ldrh	r4, [sl, #0]
	movgt	r1, #0
	add	r3, r1, r3
	mla	r3, r4, r3, r7
	str	r3, [r0, ip]
.L252:
	add	r3, r9, #4
	cmp	r3, fp
	beq	.L313
	ldr	r1, [sp, #4]
	cmp	r1, #0
	beq	.L344
	ldr	r4, [r3, #4]
	add	r9, r5, r4, asl #2
	ldrh	r7, [r9, #2]
	add	r1, r5, r7, asl #2
	ldrh	r7, [r1, #2]
	add	r1, r7, #1
	cmp	r6, r1
	ldrlt	sl, [sp, #16]
	addlt	sl, sl, #1
	strlt	sl, [sp, #16]
	movlt	r1, r6
	cmp	r8, r4
	strh	r1, [r9, #2]	@ movhi
	blt	.L256
	add	sl, r1, #1424
	add	r7, sl, #12
	add	sl, r0, r7, asl #1
	ldrh	r7, [sl, #4]!
	str	r7, [sp, #4]
	ldr	r7, [sp, #8]
	cmp	r7, r4
	ldr	r7, [sp, #4]
	add	r7, r7, #1
	str	r7, [sp, #4]
	strh	r7, [sl, #0]	@ movhi
	ldrle	sl, [sp, #8]
	ldrle	r7, [sp, #20]
	rsble	r4, sl, r4
	ldrle	r4, [r7, r4, asl #2]
	ldr	sl, [r0, ip]
	ldrh	r7, [r9, #0]
	movgt	r4, #0
	add	r1, r4, r1
	mla	r1, r7, r1, sl
	str	r1, [r0, ip]
.L256:
	add	r3, r3, #4
	cmp	r3, fp
	beq	.L313
.L344:
	str	r2, [sp, #4]
	b	.L209
.L366:
	mov	r1, #0
.L208:
	ldrh	r4, [r5, r4]
	ldr	sl, [r0, ip]
	add	r2, r1, r2
	mla	r2, r4, r2, sl
	str	r2, [r0, ip]
.L206:
	add	r1, r3, #4
	ldr	r2, [r1, #4]
	mov	r4, r2, asl #2
	add	r7, r5, r4
	ldrh	r3, [r7, #2]
	add	sl, r5, r3, asl #2
	ldrh	r3, [sl, #2]
	add	r3, r3, #1
	cmp	r6, r3
	ldrlt	r9, [sp, #16]
	addlt	r9, r9, #1
	strlt	r9, [sp, #16]
	movlt	r3, r6
.L260:
	cmp	r8, r2
	strh	r3, [r7, #2]	@ movhi
	blt	.L261
	add	r7, r3, #1424
	add	r9, r7, #12
	add	r7, r0, r9, asl #1
	ldrh	sl, [r7, #4]
	ldr	r9, [sp, #8]
	add	sl, sl, #1
	strh	sl, [r7, #4]	@ movhi
	cmp	r9, r2
	ldrle	sl, [sp, #8]
	ldrle	r7, [sp, #20]
	rsble	r2, sl, r2
	ldrle	r2, [r7, r2, asl #2]
	ldrh	r4, [r5, r4]
	ldr	r7, [r0, ip]
	movgt	r2, #0
	add	r3, r2, r3
	mla	r3, r4, r3, r7
	str	r3, [r0, ip]
.L261:
	add	r3, r1, #4
	cmp	r3, fp
	beq	.L365
.L209:
	ldr	r1, [r3, #4]
	mov	r4, r1, asl #2
	add	r7, r5, r4
	ldrh	r2, [r7, #2]
	add	sl, r5, r2, asl #2
	ldrh	r2, [sl, #2]
	add	r2, r2, #1
	cmp	r6, r2
	ldrlt	r9, [sp, #16]
	addlt	r9, r9, #1
	strlt	r9, [sp, #16]
	movlt	r2, r6
	cmp	r8, r1
	strh	r2, [r7, #2]	@ movhi
	blt	.L206
	add	r7, r2, #1424
	add	r9, r7, #12
	add	r7, r0, r9, asl #1
	ldrh	sl, [r7, #4]
	ldr	r9, [sp, #8]
	add	sl, sl, #1
	cmp	r9, r1
	strh	sl, [r7, #4]	@ movhi
	bgt	.L366
	ldr	sl, [sp, #8]
	ldr	r7, [sp, #20]
	rsb	r1, sl, r1
	ldr	r1, [r7, r1, asl #2]
	b	.L208
.L365:
	ldr	r2, [sp, #4]
.L313:
	ldr	ip, [sp, #24]
	ldr	r9, [sp, #28]
	rsb	fp, r9, ip
	add	r1, fp, #572
	b	.L216
.L352:
	mov	ip, #5184
	mov	r1, #5760
	add	ip, ip, #16
	add	r1, r1, #40
	ldr	r5, [sp, #40]
	b	.L304
.L367:
	cmp	r5, #1
	addle	r3, r5, #1
	movgt	r4, #0
	movgt	r3, r4
	str	r7, [r0, ip]
	movle	r4, r3, asl #2
	add	r7, r0, r7, asl #2
	mov	r6, #1	@ movhi
	str	r3, [r7, #2908]
	strh	r6, [r2, r4]	@ movhi
	add	sl, r3, r0
	ldr	r4, [r0, r1]
	add	r9, sl, #5184
	movle	r5, r3
	sub	r4, r4, #1
	add	r3, r9, #24
	strb	r8, [r3, #0]
	str	r4, [r0, r1]
	ldr	r7, [r0, ip]
.L304:
	cmp	r7, #1
	add	r7, r7, #1
	ble	.L367
	str	r5, [sp, #40]
	b	.L164
	.size	build_tree, .-build_tree
	.section	.text._tr_flush_block,"ax",%progbits
	.align	2
	.global	_tr_flush_block
	.hidden	_tr_flush_block
	.type	_tr_flush_block, %function
_tr_flush_block:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	mov	r4, r0
	ldr	r0, [r0, #132]
	sub	sp, sp, #12
	cmp	r0, #0
	mov	r5, r1
	stmia	sp, {r2, r3}	@ phole stm
	ble	.L369
	cmp	r2, #0
	bne	.L590
.L370:
	add	ip, r4, #2832
	add	r1, ip, #8
	mov	r0, r4
	bl	build_tree
	add	r1, r4, #2848
	mov	r0, r4
	add	r1, r1, #4
	bl	build_tree
	ldrh	r0, [r4, #150]
	ldr	r9, [r4, #2844]
	cmp	r0, #0
	movne	ip, #7
	moveq	ip, #138
	movne	r6, #4
	moveq	r6, #3
	add	r2, r4, r9, asl #2
	mvn	r1, #0	@ movhi
	cmp	r9, #0
	strh	r1, [r2, #154]	@ movhi
	add	r8, r4, #148
	blt	.L384
	tst	r9, #1
	mov	fp, #2752
	mov	sl, #2736
	add	fp, fp, #4
	add	sl, sl, #12
	add	r9, r9, #2
	mov	lr, #1
	mvn	r7, #0
	mov	r2, #0
	beq	.L394
	cmp	lr, ip
	mov	r2, lr
	ldrh	r3, [r4, #154]
	bge	.L511
	cmp	r3, r0
	beq	.L591
.L511:
	cmp	r2, r6
	bge	.L512
	add	r7, r0, #668
	add	ip, r7, #2
	add	r1, r4, ip, asl #2
	ldrh	ip, [r1, #4]!
	add	r2, r2, ip
	strh	r2, [r1, #0]	@ movhi
.L570:
	cmp	r3, #0
	moveq	r6, #3
	moveq	ip, #138
	moveq	r2, r3
	beq	.L567
	cmp	r3, r0
	moveq	r0, r3
	moveq	r6, #3
	moveq	ip, #6
	movne	r6, #4
	movne	ip, #7
	mov	r2, #0
.L567:
	mov	r7, r0
	mov	lr, #2
	mov	r0, r3
	b	.L394
.L593:
	add	ip, r0, #668
	add	r1, ip, #2
	add	r7, r4, r1, asl #2
	ldrh	ip, [r7, #4]
	add	r2, r2, ip
	strh	r2, [r7, #4]	@ movhi
.L388:
	cmp	r3, #0
	moveq	r6, #3
	moveq	ip, #138
	moveq	r2, r3
	beq	.L386
	cmp	r3, r0
	movne	r6, #4
	movne	ip, #7
	moveq	r0, r3
	moveq	r6, #3
	moveq	ip, #6
	mov	r2, #0
.L386:
	add	lr, lr, #1
	cmp	lr, r9
	beq	.L384
	add	r2, r2, #1
	add	r7, r8, lr, asl #2
	cmp	r2, ip
	ldrh	r1, [r7, #2]
	mov	r7, r3
	bge	.L519
	cmp	r1, r3
	beq	.L592
.L519:
	cmp	r2, r6
	bge	.L520
	add	r0, r3, #668
	add	ip, r0, #2
	add	r0, r4, ip, asl #2
	ldrh	ip, [r0, #4]
	add	r2, r2, ip
	strh	r2, [r0, #4]	@ movhi
.L578:
	cmp	r1, #0
	moveq	r6, #3
	moveq	ip, #138
	moveq	r2, r1
	beq	.L575
	cmp	r1, r3
	moveq	r7, r1
	movne	r6, #4
	moveq	r6, #3
	movne	ip, #7
	moveq	ip, #6
	movne	r2, #0
	moveq	r2, #0
.L575:
	add	lr, lr, #1
	mov	r0, r1
.L394:
	add	r2, r2, #1
	add	r3, r8, lr, asl #2
	cmp	r2, ip
	ldrh	r3, [r3, #2]
	bge	.L385
	cmp	r3, r0
	moveq	r0, r7
	beq	.L386
.L385:
	cmp	r2, r6
	blt	.L593
	cmp	r0, #0
	beq	.L389
	cmp	r0, r7
	beq	.L390
	add	r2, r0, #668
	add	r7, r2, #2
	add	r2, r4, r7, asl #2
	ldrh	r1, [r2, #4]
	add	r7, r1, #1
	strh	r7, [r2, #4]	@ movhi
.L390:
	ldrh	r1, [r4, sl]
	add	r2, r1, #1
	strh	r2, [r4, sl]	@ movhi
	b	.L388
.L520:
	cmp	r3, #0
	bne	.L576
	cmp	r2, #10
	movle	r6, #2752
	ldrgth	r2, [r4, fp]
	ldrleh	r2, [r4, r6]
	addgt	r2, r2, #1
	addle	r2, r2, #1
	strgth	r2, [r4, fp]	@ movhi
	strleh	r2, [r4, r6]	@ movhi
	b	.L578
.L389:
	cmp	r2, #10
	movle	r6, #2752
	ldrleh	r2, [r4, r6]
	ldrgth	r2, [r4, fp]
	addle	r2, r2, #1
	addgt	r2, r2, #1
	strleh	r2, [r4, r6]	@ movhi
	strgth	r2, [r4, fp]	@ movhi
	b	.L388
.L576:
	cmp	r3, r0
	beq	.L525
	add	r0, r3, #668
	add	r2, r0, #2
	add	r2, r4, r2, asl #2
	ldrh	r0, [r2, #4]
	add	r0, r0, #1
	strh	r0, [r2, #4]	@ movhi
.L525:
	ldrh	r0, [r4, sl]
	add	r2, r0, #1
	strh	r2, [r4, sl]	@ movhi
	b	.L578
.L384:
	mov	r0, #2432
	ldr	r9, [r4, #2856]
	add	ip, r0, #10
	ldrh	r0, [r4, ip]
	add	r3, r9, #608
	cmp	r0, #0
	add	r6, r3, #3
	add	r8, r4, r6, asl #2
	movne	ip, #7
	moveq	ip, #138
	add	lr, r4, #2432
	movne	r6, #4
	moveq	r6, #3
	mvn	r2, #0	@ movhi
	cmp	r9, #0
	strh	r2, [r8, #2]	@ movhi
	add	r8, lr, #8
	blt	.L397
	tst	r9, #1
	mov	fp, #2752
	mov	sl, #2736
	add	fp, fp, #4
	add	sl, sl, #12
	add	r9, r9, #2
	mov	lr, #1
	mvn	r7, #0
	mov	r2, #0
	beq	.L407
	cmp	ip, lr
	mov	r2, lr
	ldrh	r3, [r8, #6]
	ble	.L493
	cmp	r3, r0
	beq	.L594
.L493:
	cmp	r2, r6
	bge	.L494
	add	r1, r0, #668
	add	ip, r1, #2
	add	lr, r4, ip, asl #2
	ldrh	ip, [lr, #4]!
	add	r2, r2, ip
	strh	r2, [lr, #0]	@ movhi
.L553:
	cmp	r3, #0
	moveq	r6, #3
	moveq	ip, #138
	moveq	r2, r3
	beq	.L550
	cmp	r3, r0
	moveq	r0, r3
	moveq	r6, #3
	moveq	ip, #6
	movne	r6, #4
	movne	ip, #7
	mov	r2, #0
.L550:
	mov	r7, r0
	mov	lr, #2
	mov	r0, r3
	b	.L407
.L596:
	add	r1, r0, #668
	add	ip, r1, #2
	add	r7, r4, ip, asl #2
	ldrh	ip, [r7, #4]
	add	r2, r2, ip
	strh	r2, [r7, #4]	@ movhi
.L401:
	cmp	r3, #0
	moveq	r6, #3
	moveq	ip, #138
	moveq	r2, r3
	beq	.L399
	cmp	r3, r0
	movne	r6, #4
	movne	ip, #7
	moveq	r0, r3
	moveq	r6, #3
	moveq	ip, #6
	mov	r2, #0
.L399:
	add	lr, lr, #1
	cmp	lr, r9
	beq	.L397
	add	r2, r2, #1
	add	r7, r8, lr, asl #2
	cmp	ip, r2
	ldrh	r1, [r7, #2]
	mov	r7, r3
	ble	.L501
	cmp	r1, r3
	beq	.L595
.L501:
	cmp	r2, r6
	bge	.L502
	add	r0, r3, #668
	add	ip, r0, #2
	add	r0, r4, ip, asl #2
	ldrh	ip, [r0, #4]
	add	r2, r2, ip
	strh	r2, [r0, #4]	@ movhi
.L561:
	cmp	r1, #0
	moveq	r6, #3
	moveq	ip, #138
	moveq	r2, r1
	beq	.L558
	cmp	r1, r3
	moveq	r7, r1
	movne	r6, #4
	moveq	r6, #3
	movne	ip, #7
	moveq	ip, #6
	movne	r2, #0
	moveq	r2, #0
.L558:
	add	lr, lr, #1
	mov	r0, r1
.L407:
	add	r2, r2, #1
	add	r1, r8, lr, asl #2
	cmp	ip, r2
	ldrh	r3, [r1, #2]
	ble	.L398
	cmp	r3, r0
	moveq	r0, r7
	beq	.L399
.L398:
	cmp	r2, r6
	blt	.L596
	cmp	r0, #0
	beq	.L402
	cmp	r0, r7
	beq	.L403
	add	r2, r0, #668
	add	r7, r2, #2
	add	r2, r4, r7, asl #2
	ldrh	r1, [r2, #4]
	add	r7, r1, #1
	strh	r7, [r2, #4]	@ movhi
.L403:
	ldrh	r1, [r4, sl]
	add	r2, r1, #1
	strh	r2, [r4, sl]	@ movhi
	b	.L401
.L502:
	cmp	r3, #0
	bne	.L559
	cmp	r2, #10
	movle	r0, #2752
	ldrgth	r2, [r4, fp]
	ldrleh	r2, [r4, r0]
	addgt	r2, r2, #1
	addle	r2, r2, #1
	strgth	r2, [r4, fp]	@ movhi
	strleh	r2, [r4, r0]	@ movhi
	b	.L561
.L402:
	cmp	r2, #10
	movle	ip, #2752
	ldrleh	r2, [r4, ip]
	ldrgth	r2, [r4, fp]
	addle	r2, r2, #1
	addgt	r2, r2, #1
	strleh	r2, [r4, ip]	@ movhi
	strgth	r2, [r4, fp]	@ movhi
	b	.L401
.L559:
	cmp	r3, r0
	beq	.L507
	add	r0, r3, #668
	add	r2, r0, #2
	add	r2, r4, r2, asl #2
	ldrh	r0, [r2, #4]
	add	r0, r0, #1
	strh	r0, [r2, #4]	@ movhi
.L507:
	ldrh	r0, [r4, sl]
	add	r2, r0, #1
	strh	r2, [r4, sl]	@ movhi
	b	.L561
.L397:
	add	r6, r4, #2736
	mov	r0, r4
	add	r1, r4, #2864
	add	r8, r6, #4
	bl	build_tree
	ldrh	r3, [r8, #6]
	cmp	r3, #0
	movne	r3, #18
	bne	.L409
	add	r3, r4, #2672
	add	lr, r3, #12
	ldrh	r3, [lr, #6]
	cmp	r3, #0
	movne	r3, #17
	bne	.L409
	add	r0, r4, #2736
	ldrh	r3, [r0, #6]
	cmp	r3, #0
	movne	r3, #16
	bne	.L409
	add	ip, r4, #2688
	ldrh	r3, [ip, #6]
	cmp	r3, #0
	movne	r3, #15
	bne	.L409
	add	r1, r4, #2720
	add	r2, r1, #12
	ldrh	r3, [r2, #6]
	cmp	r3, #0
	movne	r3, #14
	bne	.L409
	add	r8, r4, #2688
	add	r7, r8, #4
	ldrh	r3, [r7, #6]
	cmp	r3, #0
	movne	r3, #13
	bne	.L409
	add	lr, r4, #2720
	add	r6, lr, #8
	ldrh	r3, [r6, #6]
	cmp	r3, #0
	movne	r3, #12
	bne	.L409
	add	r0, r4, #2688
	add	r3, r0, #8
	ldrh	r3, [r3, #6]
	cmp	r3, #0
	movne	r3, #11
	bne	.L409
	add	r2, r4, #2720
	add	ip, r2, #4
	ldrh	r3, [ip, #6]
	cmp	r3, #0
	movne	r3, #10
	bne	.L409
	add	r7, r4, #2688
	add	r1, r7, #12
	ldrh	r3, [r1, #6]
	cmp	r3, #0
	movne	r3, #9
	bne	.L409
	add	r8, r4, #2720
	ldrh	r3, [r8, #6]
	cmp	r3, #0
	movne	r3, #8
	bne	.L409
	add	r6, r4, #2704
	ldrh	r3, [r6, #6]
	cmp	r3, #0
	movne	r3, #7
	bne	.L409
	add	r3, r4, #2704
	add	lr, r3, #12
	ldrh	r3, [lr, #6]
	cmp	r3, #0
	movne	r3, #6
	bne	.L409
	add	ip, r4, #2704
	add	r0, ip, #4
	ldrh	r3, [r0, #6]
	cmp	r3, #0
	movne	r3, #5
	bne	.L409
	add	r1, r4, #2704
	add	r2, r1, #8
	ldrh	r3, [r2, #6]
	cmp	r3, #0
	movne	r3, #4
	bne	.L409
	add	r8, r4, #2672
	add	r7, r8, #8
	ldrh	r3, [r7, #6]
	cmp	r3, #0
	movne	r3, #3
	moveq	r3, #2
.L409:
	mov	r2, #5760
	add	r2, r2, #40
	ldr	lr, [r4, r2]
	mov	r1, #5760
	add	r6, r1, #44
	add	r3, r3, #1
	ldr	r7, [r4, r6]
	add	r8, lr, #14
	add	r0, r3, r3, asl #1
	add	lr, r8, r0
	add	ip, lr, #10
	add	r1, r7, #10
	mov	r6, ip, lsr #3
	str	lr, [r4, r2]
	mov	r1, r1, lsr #3
	cmp	r6, r1
	movcc	r2, r6
	movcs	r2, r1
	ldr	r6, [sp, #0]
.L425:
	add	r0, r6, #4
	cmp	r0, r2
	movhi	r7, #0
	movls	r7, #1
	cmp	r5, #0
	moveq	r7, #0
	cmp	r7, #0
	bne	.L597
	ldr	r5, [r4, #136]
	cmp	r5, #4
	cmpne	r1, r2
	bne	.L433
	mov	r3, #5760
	add	r3, r3, #60
	ldr	r2, [r4, r3]
	mov	lr, #5760
	add	r1, lr, #56
	cmp	r2, #13
	ldrh	r0, [r4, r1]
	bgt	.L598
	ldr	r5, [sp, #4]
	add	ip, r5, #2
	orr	r8, r0, ip, asl r2
	add	r6, r2, #3
	strh	r8, [r4, r1]	@ movhi
	str	r6, [r4, r3]
.L435:
	mov	r0, r4
	ldr	r1, .L608
	ldr	r2, .L608+4
	bl	compress_block
.L431:
	mov	r1, #0	@ movhi
	mov	r3, #1136
	strh	r1, [r4, #148]	@ movhi
	add	r6, r3, #8
	mov	r3, #4
.L436:
	add	r5, r4, r3
	add	r7, r3, #4
	add	r3, r3, #20
	mov	r8, #0	@ movhi
	add	ip, r4, r7
	cmp	r3, r6
	strh	r8, [r5, #148]	@ movhi
	strh	r8, [ip, #148]	@ movhi
	strh	r8, [ip, #152]	@ movhi
	strh	r8, [r5, #160]	@ movhi
	strh	r8, [r5, #164]	@ movhi
	bne	.L436
	mov	r3, #0
.L450:
	add	r2, r3, #4
	add	r5, r4, r3
	add	r8, r4, r2
	add	r6, r8, #2432
	add	r2, r5, #2448
	add	r0, r5, #2432
	add	r3, r3, #20
	mov	r8, #0	@ movhi
	add	r5, r0, #8
	add	ip, r6, #8
	add	r0, r6, #12
	add	r1, r2, #4
	add	r6, r2, #8
	cmp	r3, #120
	strh	r8, [r5, #0]	@ movhi
	strh	r8, [ip, #0]	@ movhi
	strh	r8, [r0, #0]	@ movhi
	strh	r8, [r1, #0]	@ movhi
	strh	r8, [r6, #0]	@ movhi
	bne	.L450
	add	r7, r4, #2672
	add	r3, r7, #12
	strh	r8, [r3, #0]	@ movhi
	mov	r3, #4
.L451:
	add	r8, r3, #4
	add	r6, r4, r3
	add	r0, r4, r8
	add	r5, r4, r8
	add	r2, r6, #2688
	add	r8, r4, r3
	add	ip, r6, #2672
	add	r7, r0, #2672
	add	r3, r3, #24
	add	r0, r7, #12
	add	r1, r2, #8
	add	r7, r5, #2688
	add	r6, r8, #2704
	add	ip, ip, #12
	mov	r8, #0	@ movhi
	add	r2, r2, #12
	mov	r5, #0
	cmp	r3, #76
	strh	r8, [ip, #0]	@ movhi
	strh	r8, [r0, #0]	@ movhi
	strh	r8, [r7, #0]	@ movhi
	strh	r8, [r1, #0]	@ movhi
	strh	r8, [r2, #0]	@ movhi
	strh	r5, [r6, #0]	@ movhi
	bne	.L451
	ldr	r6, [sp, #4]
	mov	r1, #5760
	mov	r7, r1
	mov	r0, #1168
	mov	r2, r1
	mov	r3, r1
	add	ip, r0, #4
	add	r1, r1, #44
	add	r0, r7, #32
	add	r2, r2, #40
	add	r3, r3, #48
	mov	r7, #1	@ movhi
	cmp	r6, r5
	strh	r7, [r4, ip]	@ movhi
	str	r5, [r4, r0]
	str	r5, [r4, r1]
	str	r5, [r4, r2]
	str	r5, [r4, r3]
	beq	.L455
	mov	r3, #5760
	add	ip, r3, #60
	ldr	r3, [r4, ip]
	cmp	r3, #8
	ble	.L453
	mov	r0, #5760
	add	r3, r0, #56
	ldr	r2, [r4, #20]
	ldr	r1, [r4, #8]
	ldrh	ip, [r4, r3]
	strb	ip, [r1, r2]
	ldrh	r0, [r4, r3]
	add	ip, r2, #1
	ldr	r1, [r4, #8]
	mov	r0, r0, lsr #8
	add	r2, ip, #1
	strb	r0, [r1, ip]
	str	r2, [r4, #20]
.L454:
	mov	r3, #5760
	mov	ip, #0
	add	r1, r3, #56
	add	r2, r3, #60
	str	ip, [r4, r2]
	strh	ip, [r4, r1]	@ movhi
.L455:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L590:
	ldr	r1, [r4, #0]
	ldr	r3, [r1, #44]
	cmp	r3, #2
	bne	.L370
	ldrh	r2, [r4, #148]
	cmp	r2, #0
	movne	r3, #0
	bne	.L372
	ldrh	r6, [r4, #152]
	cmp	r6, #0
	movne	r3, #1
	bne	.L372
	ldrh	r7, [r4, #156]
	cmp	r7, #0
	bne	.L372
	ldrh	r3, [r4, #160]
	cmp	r3, #0
	movne	r3, #3
	bne	.L372
	ldrh	r3, [r4, #164]
	cmp	r3, #0
	movne	r3, #4
	bne	.L372
	ldrh	r3, [r4, #168]
	cmp	r3, #0
	movne	r3, #5
	bne	.L372
	ldrh	r3, [r4, #172]
	cmp	r3, #0
	movne	r3, #6
	bne	.L372
	ldrh	r3, [r4, #176]
	cmp	r3, #0
	movne	r3, #7
	bne	.L372
	ldrh	r3, [r4, #180]
	cmp	r3, #0
	movne	r3, #8
	bne	.L372
	add	r2, r4, #204
	mov	r3, #14
.L381:
	ldrh	r8, [r2, #0]
	cmp	r8, #0
	bne	.L372
	ldrh	r9, [r2, #4]
	add	r3, r3, #1
	cmp	r9, #0
	mov	r0, r3
	add	ip, r2, #4
	bne	.L372
	ldrh	r9, [ip, #4]
	cmp	r9, #0
	add	r3, r3, #1
	bne	.L372
	ldrh	r3, [r2, #12]
	cmp	r3, #0
	add	r3, r0, #2
	bne	.L372
	ldrh	r6, [r2, #16]
	cmp	r6, #0
	add	r3, r0, #3
	bne	.L372
	ldrh	r8, [r2, #20]
	cmp	r8, #0
	add	r2, r2, #24
	add	r3, r0, #4
	bne	.L372
	add	r3, r0, #5
	cmp	r3, #32
	bne	.L381
.L372:
	cmp	r3, #32
	movne	r3, #0
	moveq	r3, #1
	str	r3, [r1, #44]
	b	.L370
.L433:
	mov	lr, #5760
	add	r2, lr, #60
	ldr	r1, [r4, r2]
	mov	r0, #5760
	cmp	r1, #13
	add	r0, r0, #56
	ble	.L437
	ldr	r7, [sp, #4]
	ldrh	lr, [r4, r0]
	add	ip, r7, #4
	orr	r8, lr, ip, asl r1
	ldr	r5, [r4, #8]
	ldr	lr, [r4, #20]
	mov	r6, r8, asl #16
	mov	r7, r6, lsr #16
	strh	r7, [r4, r0]	@ movhi
	strb	r7, [r5, lr]
	ldrh	r1, [r4, r0]
	ldr	r6, [r4, #8]
	mov	r8, r1, lsr #8
	add	lr, lr, #1
	strb	r8, [r6, lr]
	ldr	r6, [r4, r2]
	mov	r7, ip, asl #16
	rsb	r1, r6, #16
	mov	r5, r7, lsr #16
	mov	ip, r5, asr r1
	sub	r1, r6, #13
	mov	r8, ip, asl #16
	str	r1, [r4, r2]
	ldr	r2, [r4, #2844]
	add	r7, lr, #1
	mov	ip, r8, lsr #16
	cmp	r1, #11
	strh	ip, [r4, r0]	@ movhi
	str	r7, [r4, #20]
	ldr	r6, [r4, #2856]
	sub	r0, r2, #256
	ble	.L439
.L604:
	orr	r7, ip, r0, asl r1
	ldr	r5, [r4, #20]
	mov	r1, r7, asl #16
	ldr	lr, [r4, #8]
	mov	ip, #5760
	mov	r8, r1, lsr #16
	add	ip, ip, #56
	strh	r8, [r4, ip]	@ movhi
	strb	r8, [lr, r5]
	ldrh	r7, [r4, ip]
	ldr	r8, [r4, #8]
	mov	r1, r7, lsr #8
	add	r5, r5, #1
	strb	r1, [r8, r5]
	mov	lr, #5760
	add	r8, lr, #60
	ldr	r1, [r4, r8]
	mov	r0, r0, asl #16
	mov	r7, r0, lsr #16
	rsb	r0, r1, #16
	mov	r0, r7, asr r0
	sub	r1, r1, #11
	mov	r0, r0, asl #16
	add	r7, r5, #1
	mov	r0, r0, lsr #16
	cmp	r1, #11
	str	r7, [r4, #20]
	strh	r0, [r4, ip]	@ movhi
	str	r1, [r4, r8]
	ble	.L441
.L605:
	orr	r1, r0, r6, asl r1
	ldr	r5, [r4, #20]
	mov	r8, r1, asl #16
	ldr	r0, [r4, #8]
	mov	ip, #5760
	mov	lr, r8, lsr #16
	add	ip, ip, #56
	strh	lr, [r4, ip]	@ movhi
	strb	lr, [r0, r5]
	ldrh	r7, [r4, ip]
	ldr	r1, [r4, #8]
	mov	r8, r7, lsr #8
	add	r5, r5, #1
	strb	r8, [r1, r5]
	mov	lr, #5760
	add	r8, lr, #60
	ldr	r1, [r4, r8]
	mov	r0, r6, asl #16
	mov	r7, r0, lsr #16
	rsb	r0, r1, #16
	mov	r0, r7, asr r0
	sub	r1, r1, #11
	mov	r0, r0, asl #16
	add	r5, r5, #1
	mov	r0, r0, lsr #16
	cmp	r1, #12
	str	r5, [r4, #20]
	strh	r0, [r4, ip]	@ movhi
	str	r1, [r4, r8]
	ble	.L443
.L606:
	sub	r7, r3, #4
	orr	r5, r0, r7, asl r1
	ldr	ip, [r4, #20]
	mov	lr, r5, asl #16
	ldr	r8, [r4, #8]
	mov	r1, #5760
	mov	r0, lr, lsr #16
	add	r1, r1, #56
	strh	r0, [r4, r1]	@ movhi
	strb	r0, [r8, ip]
	ldrh	r5, [r4, r1]
	ldr	r8, [r4, #8]
	mov	lr, r5, lsr #8
	add	ip, ip, #1
	strb	lr, [r8, ip]
	mov	r0, #5760
	add	r8, r0, #60
	ldr	lr, [r4, r8]
	mov	r5, r7, asl #16
	mov	r7, r5, lsr #16
	rsb	r5, lr, #16
	mov	r5, r7, asr r5
	mov	r5, r5, asl #16
	add	r7, ip, #1
	mov	r5, r5, lsr #16
	sub	lr, lr, #12
	str	r7, [r4, #20]
	strh	r5, [r4, r1]	@ movhi
	str	lr, [r4, r8]
.L446:
	cmp	r3, #0
	ble	.L445
	mov	r8, #5760
	tst	r3, #1
	add	r1, r8, #56
	add	r0, r8, #60
	mov	ip, #0
	ldr	r7, .L608+8
	bne	.L449
	ldrb	ip, [r7, #0]	@ zero_extendqisi2
	add	r8, ip, #668
	add	ip, r8, #2
	cmp	lr, #13
	add	ip, r4, ip, asl #2
	bgt	.L543
	ldrh	ip, [ip, #6]
	orr	lr, r5, ip, asl lr
	ldr	r8, [r4, r0]
	add	r5, r8, #3
	str	r5, [r4, r0]
	strh	lr, [r4, r1]	@ movhi
	mov	lr, r5
.L544:
	mov	ip, #1
	ldrh	r5, [r4, r1]
	b	.L449
.L599:
	ldr	r5, [r4, #20]
	ldr	sl, [r4, #8]
	mov	lr, lr, asl #16
	mov	lr, lr, lsr #16
	strh	lr, [r4, r1]	@ movhi
	strb	lr, [sl, r5]
	ldrh	sl, [r4, r1]
	ldr	lr, [r4, #8]
	add	r5, r5, #1
	mov	sl, sl, lsr #8
	strb	sl, [lr, r5]
	ldr	lr, [r4, r0]
	rsb	sl, lr, #16
	mov	r8, r8, asr sl
	add	ip, ip, #1
	add	r5, r5, #1
	sub	lr, lr, #13
	cmp	ip, r3
	str	r5, [r4, #20]
	strh	r8, [r4, r1]	@ movhi
	str	lr, [r4, r0]
	beq	.L445
.L600:
	ldr	lr, [r4, r0]
	cmp	lr, #13
	ldrh	r5, [r4, r1]
	ble	.L489
	ldrb	sl, [r7, ip]	@ zero_extendqisi2
	add	r8, sl, #668
	add	sl, r8, #2
	add	r8, r4, sl, asl #2
	ldrh	r8, [r8, #6]
	orr	sl, r5, r8, asl lr
	ldr	r5, [r4, #20]
	mov	lr, sl, asl #16
	ldr	sl, [r4, #8]
	mov	lr, lr, lsr #16
	strh	lr, [r4, r1]	@ movhi
	strb	lr, [sl, r5]
	ldrh	sl, [r4, r1]
	ldr	lr, [r4, #8]
	add	r5, r5, #1
	mov	sl, sl, lsr #8
	strb	sl, [lr, r5]
	ldr	lr, [r4, r0]
	rsb	sl, lr, #16
	mov	r8, r8, asr sl
	add	r5, r5, #1
	sub	lr, lr, #13
	strh	r8, [r4, r1]	@ movhi
	str	lr, [r4, r0]
	str	r5, [r4, #20]
.L545:
	ldrh	r5, [r4, r1]
	add	ip, ip, #1
.L449:
	ldrb	r8, [r7, ip]	@ zero_extendqisi2
	add	r8, r8, #668
	add	r8, r8, #2
	add	r8, r4, r8, asl #2
	ldrh	r8, [r8, #6]
	cmp	lr, #13
	orr	lr, r5, r8, asl lr
	bgt	.L599
	ldr	r5, [r4, r0]
	add	ip, ip, #1
	add	r8, r5, #3
	cmp	ip, r3
	strh	lr, [r4, r1]	@ movhi
	str	r8, [r4, r0]
	bne	.L600
.L445:
	add	r3, r4, #2432
	add	r7, r4, #148
	add	r8, r3, #8
	mov	r0, r4
	mov	r1, r7
	bl	send_tree
	mov	r2, r6
	mov	r0, r4
	mov	r1, r8
	bl	send_tree
	mov	r0, r4
	mov	r1, r7
	mov	r2, r8
	bl	compress_block
	b	.L431
.L494:
	cmp	r0, #0
	beq	.L601
	cmn	r0, #1
	beq	.L499
	add	r7, r0, #668
	add	r2, r7, #2
	add	lr, r4, r2, asl #2
	ldrh	r1, [lr, #4]!
	add	r7, r1, #1
	strh	r7, [lr, #0]	@ movhi
.L499:
	ldrh	r2, [r4, sl]
	add	r2, r2, #1
	strh	r2, [r4, sl]	@ movhi
	b	.L553
.L512:
	cmp	r0, #0
	beq	.L602
	cmn	r0, #1
	beq	.L517
	add	lr, r0, #668
	add	r7, lr, #2
	add	r2, r4, r7, asl #2
	ldrh	r1, [r2, #4]!
	add	lr, r1, #1
	strh	lr, [r2, #0]	@ movhi
.L517:
	ldrh	r7, [r4, sl]
	add	r2, r7, #1
	strh	r2, [r4, sl]	@ movhi
	b	.L570
.L592:
	mov	r7, r0
	b	.L575
.L595:
	mov	r7, r0
	b	.L558
.L597:
	mov	r3, #5760
	add	r3, r3, #60
	ldr	r2, [r4, r3]
	mov	r8, #5760
	add	r1, r8, #56
	cmp	r2, #13
	ldrh	r0, [r4, r1]
	bgt	.L603
	ldr	r8, [sp, #4]
	orr	ip, r0, r8, asl r2
	add	r2, r2, #3
	mov	r7, ip, asl #16
	mov	r0, r7, lsr #16
	cmp	r2, #8
	strh	r0, [r4, r1]	@ movhi
	str	r2, [r4, r3]
	ble	.L429
.L607:
	ldr	r7, [r4, #20]
	ldr	r2, [r4, #8]
	mov	r8, #5760
	strb	r0, [r2, r7]
	add	ip, r8, #56
	ldrh	r6, [r4, ip]
	add	r3, r7, #1
	ldr	r1, [r4, #8]
	mov	r0, r6, lsr #8
	add	r2, r3, #1
	strb	r0, [r1, r3]
	str	r2, [r4, #20]
.L430:
	ldr	r8, [sp, #0]
	mov	r0, #5760
	ldr	ip, [r4, #20]
	ldr	r7, [r4, #8]
	add	r1, r0, #60
	mov	r6, #0
	str	r6, [r4, r1]
	add	r3, r0, #56
	add	r2, r0, #52
	mov	r1, #8
	and	r0, r8, #255
	str	r1, [r4, r2]
	strh	r6, [r4, r3]	@ movhi
	strb	r0, [r7, ip]
	mov	r2, r8, asl #16
	ldr	r7, [r4, #8]
	add	r3, ip, #1
	mov	ip, r2, lsr #24
	strb	ip, [r7, r3]
	ldr	r1, [r4, #8]
	add	ip, r3, #1
	mvn	r0, r0
	strb	r0, [r1, ip]
	add	r3, ip, #1
	eor	r7, r2, #-16777216
	ldr	r1, [r4, #8]
	mov	r0, r7, lsr #24
	add	r2, r3, #1
	cmp	r8, r6
	strb	r0, [r1, r3]
	str	r2, [r4, #20]
	beq	.L431
	ldrb	ip, [r5, #0]	@ zero_extendqisi2
	ldr	r1, [r4, #8]
	add	r3, r2, #1
	sub	r7, r8, #1
	cmp	r8, #1
	strb	ip, [r1, r2]
	and	r1, r7, #3
	str	r3, [r4, #20]
	mov	r2, #1
	beq	.L431
	cmp	r1, #0
	beq	.L585
	cmp	r1, #1
	beq	.L534
	cmp	r1, #2
	beq	.L535
	ldrb	r8, [r5, #1]	@ zero_extendqisi2
	ldr	r6, [r4, #8]
	strb	r8, [r6, r3]
	add	r3, r3, #1
	str	r3, [r4, #20]
	mov	r2, #2
.L535:
	ldrb	r7, [r5, r2]	@ zero_extendqisi2
	ldr	ip, [r4, #8]
	strb	r7, [ip, r3]
	add	r3, r3, #1
	str	r3, [r4, #20]
	add	r2, r2, #1
.L534:
	ldrb	r0, [r5, r2]	@ zero_extendqisi2
	ldr	r8, [r4, #8]
	strb	r0, [r8, r3]
	ldr	r1, [sp, #0]
	add	r2, r2, #1
	add	r3, r3, #1
	cmp	r1, r2
	str	r3, [r4, #20]
	beq	.L431
.L585:
	ldr	r6, [sp, #0]
.L432:
	ldrb	ip, [r5, r2]	@ zero_extendqisi2
	ldr	r0, [r4, #8]
	add	r1, r3, #1
	strb	ip, [r0, r3]
	str	r1, [r4, #20]
	add	ip, r2, #1
	ldrb	r8, [r5, ip]	@ zero_extendqisi2
	ldr	r7, [r4, #8]
	add	r0, r1, #1
	strb	r8, [r7, r1]
	str	r0, [r4, #20]
	add	r1, ip, #1
	ldrb	r8, [r5, r1]	@ zero_extendqisi2
	ldr	ip, [r4, #8]
	add	r1, r3, #3
	strb	r8, [ip, r0]
	str	r1, [r4, #20]
	add	r0, r2, #3
	ldrb	r7, [r5, r0]	@ zero_extendqisi2
	add	r2, r2, #4
	ldr	r8, [r4, #8]
	add	r3, r3, #4
	cmp	r6, r2
	strb	r7, [r8, r1]
	str	r3, [r4, #20]
	bne	.L432
	b	.L431
.L369:
	mov	ip, r2
	add	r1, r2, #5
	mov	r2, r1
	mov	r3, #1
	mov	r6, ip
	b	.L425
.L598:
	ldr	r7, [sp, #4]
	add	r6, r7, #2
	orr	r2, r0, r6, asl r2
	ldr	r5, [r4, #8]
	ldr	r0, [r4, #20]
	mov	lr, r2, asl #16
	mov	ip, lr, lsr #16
	strh	ip, [r4, r1]	@ movhi
	strb	ip, [r5, r0]
	ldrh	r8, [r4, r1]
	ldr	r2, [r4, #8]
	mov	r7, r8, lsr #8
	add	r0, r0, #1
	strb	r7, [r2, r0]
	ldr	r8, [r4, r3]
	mov	lr, r6, asl #16
	mov	r5, lr, lsr #16
	rsb	ip, r8, #16
	mov	r7, r5, asr ip
	add	r6, r0, #1
	sub	r2, r8, #13
	strh	r7, [r4, r1]	@ movhi
	str	r2, [r4, r3]
	str	r6, [r4, #20]
	b	.L435
.L437:
	ldr	lr, [sp, #4]
	ldrh	r6, [r4, r0]
	add	r8, lr, #4
	orr	r5, r6, r8, asl r1
	add	r1, r1, #3
	mov	ip, r5, asl #16
	str	r1, [r4, r2]
	ldr	r2, [r4, #2844]
	mov	ip, ip, lsr #16
	cmp	r1, #11
	strh	ip, [r4, r0]	@ movhi
	ldr	r6, [r4, #2856]
	sub	r0, r2, #256
	bgt	.L604
.L439:
	orr	r7, ip, r0, asl r1
	mov	lr, #5760
	add	r5, lr, #60
	ldr	r1, [r4, r5]
	mov	r0, r7, asl #16
	add	r1, r1, #5
	mov	ip, #5760
	mov	r0, r0, lsr #16
	add	r8, ip, #56
	cmp	r1, #11
	strh	r0, [r4, r8]	@ movhi
	str	r1, [r4, r5]
	bgt	.L605
.L441:
	orr	r5, r0, r6, asl r1
	mov	ip, #5760
	add	r7, ip, #60
	ldr	r8, [r4, r7]
	mov	r0, r5, asl #16
	add	r1, r8, #5
	mov	lr, #5760
	mov	r0, r0, lsr #16
	add	r5, lr, #56
	cmp	r1, #12
	strh	r0, [r4, r5]	@ movhi
	str	r1, [r4, r7]
	bgt	.L606
.L443:
	sub	r7, r3, #4
	orr	r1, r0, r7, asl r1
	mov	ip, #5760
	add	r7, ip, #60
	ldr	r8, [r4, r7]
	mov	lr, #5760
	mov	r5, r1, asl #16
	add	r0, lr, #56
	mov	r5, r5, lsr #16
	add	lr, r8, #4
	strh	r5, [r4, r0]	@ movhi
	str	lr, [r4, r7]
	b	.L446
.L603:
	ldr	r7, [sp, #4]
	orr	r6, r0, r7, asl r2
	ldr	ip, [r4, #20]
	mov	r2, r6, asl #16
	ldr	r0, [r4, #8]
	mov	r8, r2, lsr #16
	strh	r8, [r4, r1]	@ movhi
	strb	r8, [r0, ip]
	ldrh	r6, [r4, r1]
	ldr	r2, [r4, #8]
	mov	r8, r6, lsr #8
	add	ip, ip, #1
	strb	r8, [r2, ip]
	ldr	r2, [r4, r3]
	mov	r0, r7, asl #16
	mov	r6, r0, lsr #16
	rsb	r8, r2, #16
	mov	r0, r6, asr r8
	sub	r2, r2, #13
	mov	r7, r0, asl #16
	add	r6, ip, #1
	mov	r0, r7, lsr #16
	cmp	r2, #8
	str	r6, [r4, #20]
	strh	r0, [r4, r1]	@ movhi
	str	r2, [r4, r3]
	bgt	.L607
.L429:
	cmp	r2, #0
	ldrgt	r3, [r4, #20]
	ldrgt	r1, [r4, #8]
	addgt	r2, r3, #1
	strgtb	r0, [r1, r3]
	strgt	r2, [r4, #20]
	b	.L430
.L602:
	cmp	r2, #10
	movle	r1, #2752
	ldrgth	r2, [r4, fp]
	ldrleh	r2, [r4, r1]
	addgt	r2, r2, #1
	addle	r2, r2, #1
	strgth	r2, [r4, fp]	@ movhi
	strleh	r2, [r4, r1]	@ movhi
	b	.L570
.L601:
	cmp	r2, #10
	movle	ip, #2752
	ldrgth	r2, [r4, fp]
	ldrleh	r2, [r4, ip]
	addgt	r2, r2, #1
	addle	r2, r2, #1
	strgth	r2, [r4, fp]	@ movhi
	strleh	r2, [r4, ip]	@ movhi
	b	.L553
.L591:
	mov	r0, r7
	b	.L567
.L594:
	mov	r0, r7
	b	.L550
.L489:
	ldrb	r8, [r7, ip]	@ zero_extendqisi2
	add	r8, r8, #668
	add	r8, r8, #2
	add	r8, r4, r8, asl #2
	ldrh	r8, [r8, #6]
	orr	r5, r5, r8, asl lr
	add	lr, lr, #3
	strh	r5, [r4, r1]	@ movhi
	str	lr, [r4, r0]
	b	.L545
.L453:
	cmp	r3, #0
	ble	.L454
	mov	r3, #5760
	ldr	r2, [r4, #20]
	add	r0, r3, #56
	ldrh	ip, [r4, r0]
	ldr	r1, [r4, #8]
	add	r3, r2, #1
	strb	ip, [r1, r2]
	str	r3, [r4, #20]
	b	.L454
.L543:
	ldrh	r8, [ip, #6]
	orr	ip, r5, r8, asl lr
	ldr	lr, [r4, #20]
	ldr	r5, [r4, #8]
	mov	ip, ip, asl #16
	mov	ip, ip, lsr #16
	strh	ip, [r4, r1]	@ movhi
	strb	ip, [r5, lr]
	ldrh	r5, [r4, r1]
	ldr	ip, [r4, #8]
	add	lr, lr, #1
	mov	r5, r5, lsr #8
	strb	r5, [ip, lr]
	ldr	ip, [r4, r0]
	rsb	r5, ip, #16
	mov	r8, r8, asr r5
	add	lr, lr, #1
	sub	r5, ip, #13
	str	lr, [r4, #20]
	strh	r8, [r4, r1]	@ movhi
	str	r5, [r4, r0]
	mov	lr, r5
	b	.L544
.L609:
	.align	2
.L608:
	.word	.LANCHOR1
	.word	.LANCHOR1+2392
	.word	.LANCHOR1+2512
	.size	_tr_flush_block, .-_tr_flush_block
	.hidden	_dist_code
	.global	_dist_code
	.hidden	_length_code
	.global	_length_code
	.section	.rodata
	.align	2
	.set	.LANCHOR1,. + 0
	.type	static_ltree, %object
	.size	static_ltree, 1152
static_ltree:
	.short	12
	.short	8
	.short	140
	.short	8
	.short	76
	.short	8
	.short	204
	.short	8
	.short	44
	.short	8
	.short	172
	.short	8
	.short	108
	.short	8
	.short	236
	.short	8
	.short	28
	.short	8
	.short	156
	.short	8
	.short	92
	.short	8
	.short	220
	.short	8
	.short	60
	.short	8
	.short	188
	.short	8
	.short	124
	.short	8
	.short	252
	.short	8
	.short	2
	.short	8
	.short	130
	.short	8
	.short	66
	.short	8
	.short	194
	.short	8
	.short	34
	.short	8
	.short	162
	.short	8
	.short	98
	.short	8
	.short	226
	.short	8
	.short	18
	.short	8
	.short	146
	.short	8
	.short	82
	.short	8
	.short	210
	.short	8
	.short	50
	.short	8
	.short	178
	.short	8
	.short	114
	.short	8
	.short	242
	.short	8
	.short	10
	.short	8
	.short	138
	.short	8
	.short	74
	.short	8
	.short	202
	.short	8
	.short	42
	.short	8
	.short	170
	.short	8
	.short	106
	.short	8
	.short	234
	.short	8
	.short	26
	.short	8
	.short	154
	.short	8
	.short	90
	.short	8
	.short	218
	.short	8
	.short	58
	.short	8
	.short	186
	.short	8
	.short	122
	.short	8
	.short	250
	.short	8
	.short	6
	.short	8
	.short	134
	.short	8
	.short	70
	.short	8
	.short	198
	.short	8
	.short	38
	.short	8
	.short	166
	.short	8
	.short	102
	.short	8
	.short	230
	.short	8
	.short	22
	.short	8
	.short	150
	.short	8
	.short	86
	.short	8
	.short	214
	.short	8
	.short	54
	.short	8
	.short	182
	.short	8
	.short	118
	.short	8
	.short	246
	.short	8
	.short	14
	.short	8
	.short	142
	.short	8
	.short	78
	.short	8
	.short	206
	.short	8
	.short	46
	.short	8
	.short	174
	.short	8
	.short	110
	.short	8
	.short	238
	.short	8
	.short	30
	.short	8
	.short	158
	.short	8
	.short	94
	.short	8
	.short	222
	.short	8
	.short	62
	.short	8
	.short	190
	.short	8
	.short	126
	.short	8
	.short	254
	.short	8
	.short	1
	.short	8
	.short	129
	.short	8
	.short	65
	.short	8
	.short	193
	.short	8
	.short	33
	.short	8
	.short	161
	.short	8
	.short	97
	.short	8
	.short	225
	.short	8
	.short	17
	.short	8
	.short	145
	.short	8
	.short	81
	.short	8
	.short	209
	.short	8
	.short	49
	.short	8
	.short	177
	.short	8
	.short	113
	.short	8
	.short	241
	.short	8
	.short	9
	.short	8
	.short	137
	.short	8
	.short	73
	.short	8
	.short	201
	.short	8
	.short	41
	.short	8
	.short	169
	.short	8
	.short	105
	.short	8
	.short	233
	.short	8
	.short	25
	.short	8
	.short	153
	.short	8
	.short	89
	.short	8
	.short	217
	.short	8
	.short	57
	.short	8
	.short	185
	.short	8
	.short	121
	.short	8
	.short	249
	.short	8
	.short	5
	.short	8
	.short	133
	.short	8
	.short	69
	.short	8
	.short	197
	.short	8
	.short	37
	.short	8
	.short	165
	.short	8
	.short	101
	.short	8
	.short	229
	.short	8
	.short	21
	.short	8
	.short	149
	.short	8
	.short	85
	.short	8
	.short	213
	.short	8
	.short	53
	.short	8
	.short	181
	.short	8
	.short	117
	.short	8
	.short	245
	.short	8
	.short	13
	.short	8
	.short	141
	.short	8
	.short	77
	.short	8
	.short	205
	.short	8
	.short	45
	.short	8
	.short	173
	.short	8
	.short	109
	.short	8
	.short	237
	.short	8
	.short	29
	.short	8
	.short	157
	.short	8
	.short	93
	.short	8
	.short	221
	.short	8
	.short	61
	.short	8
	.short	189
	.short	8
	.short	125
	.short	8
	.short	253
	.short	8
	.short	19
	.short	9
	.short	275
	.short	9
	.short	147
	.short	9
	.short	403
	.short	9
	.short	83
	.short	9
	.short	339
	.short	9
	.short	211
	.short	9
	.short	467
	.short	9
	.short	51
	.short	9
	.short	307
	.short	9
	.short	179
	.short	9
	.short	435
	.short	9
	.short	115
	.short	9
	.short	371
	.short	9
	.short	243
	.short	9
	.short	499
	.short	9
	.short	11
	.short	9
	.short	267
	.short	9
	.short	139
	.short	9
	.short	395
	.short	9
	.short	75
	.short	9
	.short	331
	.short	9
	.short	203
	.short	9
	.short	459
	.short	9
	.short	43
	.short	9
	.short	299
	.short	9
	.short	171
	.short	9
	.short	427
	.short	9
	.short	107
	.short	9
	.short	363
	.short	9
	.short	235
	.short	9
	.short	491
	.short	9
	.short	27
	.short	9
	.short	283
	.short	9
	.short	155
	.short	9
	.short	411
	.short	9
	.short	91
	.short	9
	.short	347
	.short	9
	.short	219
	.short	9
	.short	475
	.short	9
	.short	59
	.short	9
	.short	315
	.short	9
	.short	187
	.short	9
	.short	443
	.short	9
	.short	123
	.short	9
	.short	379
	.short	9
	.short	251
	.short	9
	.short	507
	.short	9
	.short	7
	.short	9
	.short	263
	.short	9
	.short	135
	.short	9
	.short	391
	.short	9
	.short	71
	.short	9
	.short	327
	.short	9
	.short	199
	.short	9
	.short	455
	.short	9
	.short	39
	.short	9
	.short	295
	.short	9
	.short	167
	.short	9
	.short	423
	.short	9
	.short	103
	.short	9
	.short	359
	.short	9
	.short	231
	.short	9
	.short	487
	.short	9
	.short	23
	.short	9
	.short	279
	.short	9
	.short	151
	.short	9
	.short	407
	.short	9
	.short	87
	.short	9
	.short	343
	.short	9
	.short	215
	.short	9
	.short	471
	.short	9
	.short	55
	.short	9
	.short	311
	.short	9
	.short	183
	.short	9
	.short	439
	.short	9
	.short	119
	.short	9
	.short	375
	.short	9
	.short	247
	.short	9
	.short	503
	.short	9
	.short	15
	.short	9
	.short	271
	.short	9
	.short	143
	.short	9
	.short	399
	.short	9
	.short	79
	.short	9
	.short	335
	.short	9
	.short	207
	.short	9
	.short	463
	.short	9
	.short	47
	.short	9
	.short	303
	.short	9
	.short	175
	.short	9
	.short	431
	.short	9
	.short	111
	.short	9
	.short	367
	.short	9
	.short	239
	.short	9
	.short	495
	.short	9
	.short	31
	.short	9
	.short	287
	.short	9
	.short	159
	.short	9
	.short	415
	.short	9
	.short	95
	.short	9
	.short	351
	.short	9
	.short	223
	.short	9
	.short	479
	.short	9
	.short	63
	.short	9
	.short	319
	.short	9
	.short	191
	.short	9
	.short	447
	.short	9
	.short	127
	.short	9
	.short	383
	.short	9
	.short	255
	.short	9
	.short	511
	.short	9
	.short	0
	.short	7
	.short	64
	.short	7
	.short	32
	.short	7
	.short	96
	.short	7
	.short	16
	.short	7
	.short	80
	.short	7
	.short	48
	.short	7
	.short	112
	.short	7
	.short	8
	.short	7
	.short	72
	.short	7
	.short	40
	.short	7
	.short	104
	.short	7
	.short	24
	.short	7
	.short	88
	.short	7
	.short	56
	.short	7
	.short	120
	.short	7
	.short	4
	.short	7
	.short	68
	.short	7
	.short	36
	.short	7
	.short	100
	.short	7
	.short	20
	.short	7
	.short	84
	.short	7
	.short	52
	.short	7
	.short	116
	.short	7
	.short	3
	.short	8
	.short	131
	.short	8
	.short	67
	.short	8
	.short	195
	.short	8
	.short	35
	.short	8
	.short	163
	.short	8
	.short	99
	.short	8
	.short	227
	.short	8
	.type	_length_code, %object
	.size	_length_code, 256
_length_code:
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	8
	.byte	9
	.byte	9
	.byte	10
	.byte	10
	.byte	11
	.byte	11
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	16
	.byte	17
	.byte	17
	.byte	17
	.byte	17
	.byte	17
	.byte	17
	.byte	17
	.byte	17
	.byte	18
	.byte	18
	.byte	18
	.byte	18
	.byte	18
	.byte	18
	.byte	18
	.byte	18
	.byte	19
	.byte	19
	.byte	19
	.byte	19
	.byte	19
	.byte	19
	.byte	19
	.byte	19
	.byte	20
	.byte	20
	.byte	20
	.byte	20
	.byte	20
	.byte	20
	.byte	20
	.byte	20
	.byte	20
	.byte	20
	.byte	20
	.byte	20
	.byte	20
	.byte	20
	.byte	20
	.byte	20
	.byte	21
	.byte	21
	.byte	21
	.byte	21
	.byte	21
	.byte	21
	.byte	21
	.byte	21
	.byte	21
	.byte	21
	.byte	21
	.byte	21
	.byte	21
	.byte	21
	.byte	21
	.byte	21
	.byte	22
	.byte	22
	.byte	22
	.byte	22
	.byte	22
	.byte	22
	.byte	22
	.byte	22
	.byte	22
	.byte	22
	.byte	22
	.byte	22
	.byte	22
	.byte	22
	.byte	22
	.byte	22
	.byte	23
	.byte	23
	.byte	23
	.byte	23
	.byte	23
	.byte	23
	.byte	23
	.byte	23
	.byte	23
	.byte	23
	.byte	23
	.byte	23
	.byte	23
	.byte	23
	.byte	23
	.byte	23
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	28
	.type	_dist_code, %object
	.size	_dist_code, 512
_dist_code:
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	4
	.byte	4
	.byte	5
	.byte	5
	.byte	6
	.byte	6
	.byte	6
	.byte	6
	.byte	7
	.byte	7
	.byte	7
	.byte	7
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	8
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	9
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	10
	.byte	11
	.byte	11
	.byte	11
	.byte	11
	.byte	11
	.byte	11
	.byte	11
	.byte	11
	.byte	11
	.byte	11
	.byte	11
	.byte	11
	.byte	11
	.byte	11
	.byte	11
	.byte	11
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	12
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	13
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	14
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	15
	.byte	0
	.byte	0
	.byte	16
	.byte	17
	.byte	18
	.byte	18
	.byte	19
	.byte	19
	.byte	20
	.byte	20
	.byte	20
	.byte	20
	.byte	21
	.byte	21
	.byte	21
	.byte	21
	.byte	22
	.byte	22
	.byte	22
	.byte	22
	.byte	22
	.byte	22
	.byte	22
	.byte	22
	.byte	23
	.byte	23
	.byte	23
	.byte	23
	.byte	23
	.byte	23
	.byte	23
	.byte	23
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	24
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	25
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	26
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	27
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	28
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.byte	29
	.type	extra_lbits, %object
	.size	extra_lbits, 116
extra_lbits:
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	1
	.word	1
	.word	1
	.word	1
	.word	2
	.word	2
	.word	2
	.word	2
	.word	3
	.word	3
	.word	3
	.word	3
	.word	4
	.word	4
	.word	4
	.word	4
	.word	5
	.word	5
	.word	5
	.word	5
	.word	0
	.type	base_length, %object
	.size	base_length, 116
base_length:
	.word	0
	.word	1
	.word	2
	.word	3
	.word	4
	.word	5
	.word	6
	.word	7
	.word	8
	.word	10
	.word	12
	.word	14
	.word	16
	.word	20
	.word	24
	.word	28
	.word	32
	.word	40
	.word	48
	.word	56
	.word	64
	.word	80
	.word	96
	.word	112
	.word	128
	.word	160
	.word	192
	.word	224
	.word	0
	.type	extra_dbits, %object
	.size	extra_dbits, 120
extra_dbits:
	.word	0
	.word	0
	.word	0
	.word	0
	.word	1
	.word	1
	.word	2
	.word	2
	.word	3
	.word	3
	.word	4
	.word	4
	.word	5
	.word	5
	.word	6
	.word	6
	.word	7
	.word	7
	.word	8
	.word	8
	.word	9
	.word	9
	.word	10
	.word	10
	.word	11
	.word	11
	.word	12
	.word	12
	.word	13
	.word	13
	.type	base_dist, %object
	.size	base_dist, 120
base_dist:
	.word	0
	.word	1
	.word	2
	.word	3
	.word	4
	.word	6
	.word	8
	.word	12
	.word	16
	.word	24
	.word	32
	.word	48
	.word	64
	.word	96
	.word	128
	.word	192
	.word	256
	.word	384
	.word	512
	.word	768
	.word	1024
	.word	1536
	.word	2048
	.word	3072
	.word	4096
	.word	6144
	.word	8192
	.word	12288
	.word	16384
	.word	24576
	.type	static_dtree, %object
	.size	static_dtree, 120
static_dtree:
	.short	0
	.short	5
	.short	16
	.short	5
	.short	8
	.short	5
	.short	24
	.short	5
	.short	4
	.short	5
	.short	20
	.short	5
	.short	12
	.short	5
	.short	28
	.short	5
	.short	2
	.short	5
	.short	18
	.short	5
	.short	10
	.short	5
	.short	26
	.short	5
	.short	6
	.short	5
	.short	22
	.short	5
	.short	14
	.short	5
	.short	30
	.short	5
	.short	1
	.short	5
	.short	17
	.short	5
	.short	9
	.short	5
	.short	25
	.short	5
	.short	5
	.short	5
	.short	21
	.short	5
	.short	13
	.short	5
	.short	29
	.short	5
	.short	3
	.short	5
	.short	19
	.short	5
	.short	11
	.short	5
	.short	27
	.short	5
	.short	7
	.short	5
	.short	23
	.short	5
	.type	bl_order, %object
	.size	bl_order, 19
bl_order:
	.byte	16
	.byte	17
	.byte	18
	.byte	0
	.byte	8
	.byte	7
	.byte	9
	.byte	6
	.byte	10
	.byte	5
	.byte	11
	.byte	4
	.byte	12
	.byte	3
	.byte	13
	.byte	2
	.byte	14
	.byte	1
	.byte	15
	.space	1
	.type	extra_blbits, %object
	.size	extra_blbits, 76
extra_blbits:
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	0
	.word	2
	.word	3
	.word	7
	.data
	.align	2
	.set	.LANCHOR0,. + 0
	.type	static_l_desc, %object
	.size	static_l_desc, 20
static_l_desc:
	.word	static_ltree
	.word	extra_lbits
	.word	257
	.word	286
	.word	15
	.type	static_d_desc, %object
	.size	static_d_desc, 20
static_d_desc:
	.word	static_dtree
	.word	extra_dbits
	.word	0
	.word	30
	.word	15
	.type	static_bl_desc, %object
	.size	static_bl_desc, 20
static_bl_desc:
	.word	0
	.word	extra_blbits
	.word	0
	.word	19
	.word	7
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
