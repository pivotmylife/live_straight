	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"QuDrawer.cpp"
	.section	.text._GLOBAL__I__ZN12QuDrawObject9loadImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEEPfii,"ax",%progbits
	.align	2
	.type	_GLOBAL__I__ZN12QuDrawObject9loadImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEEPfii, %function
_GLOBAL__I__ZN12QuDrawObject9loadImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEEPfii:
	.fnstart
.LFB4996:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	ldr	r4, .L3
	ldr	r5, .L3+4
	mov	r0, r4
	bl	_ZN4_STL8ios_base9_Loc_initC1Ev
	add	r6, r4, #4
	mov	r2, r5
	ldr	r1, .L3+8
	mov	r0, r4
	bl	__aeabi_atexit
	mov	r0, r6
	bl	_ZN4_STL8ios_base4InitC1Ev
	mov	r0, r6
	mov	r2, r5
	ldr	r1, .L3+12
	bl	__aeabi_atexit
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L4:
	.align	2
.L3:
	.word	.LANCHOR0
	.word	__dso_handle
	.word	_ZN4_STL8ios_base9_Loc_initD1Ev
	.word	_ZN4_STL8ios_base4InitD1Ev
	.fnend
	.size	_GLOBAL__I__ZN12QuDrawObject9loadImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEEPfii, .-_GLOBAL__I__ZN12QuDrawObject9loadImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEEPfii
	.section	.init_array,"aw",%init_array
	.align	2
	.word	_GLOBAL__I__ZN12QuDrawObject9loadImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEEPfii(target1)
	.section	.text._ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev,"axG",%progbits,_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev,comdat
	.align	2
	.weak	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.hidden	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.type	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev, %function
_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev:
	.fnstart
.LFB3717:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	mov	r4, r0
	ldr	r0, [r0, #0]
	cmp	r0, #0
	blne	free
.L6:
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev, .-_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.section	.text._ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_,"axG",%progbits,_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_,comdat
	.align	2
	.weak	_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_
	.hidden	_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_
	.type	_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_, %function
_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_:
	.fnstart
.LFB4194:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	ldmia	r1, {r0, r5}	@ phole ldm
	ldmia	r2, {r1, r4}	@ phole ldm
	rsb	r5, r0, r5
	rsb	r4, r1, r4
	cmp	r4, r5
	movlt	r2, r4
	movge	r2, r5
	bl	memcmp
	cmp	r0, #0
	movne	r0, r0, lsr #31
	bne	.L11
	cmp	r5, r4
	movge	r0, #0
	movlt	r0, #1
.L11:
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
	.cantunwind
	.fnend
	.size	_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_, .-_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_
	.global	__cxa_end_cleanup
	.section	.text._ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_,"axG",%progbits,_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_,comdat
	.align	2
	.weak	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
	.hidden	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
	.type	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_, %function
_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_:
	.fnstart
.LFB3720:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	.save {r4, r5, r6, r7, r8, lr}
	mov	r3, #0
	str	r3, [r0, #0]
	str	r3, [r0, #4]
	ldmia	r1, {r5, r7}	@ phole ldm
	rsb	r6, r5, r7
	adds	r8, r6, #1
	mov	r4, r0
	str	r3, [r0, #8]
	beq	.L16
	mov	r0, r8
	bl	malloc
	cmp	r0, #0
	beq	.L26
.L17:
	add	r8, r0, r8
	str	r8, [r4, #8]
	str	r0, [r4, #0]
	str	r0, [r4, #4]
	b	.L23
.L16:
	ldr	r0, .L27
.LEHB0:
	bl	_ZN4_STL24__stl_throw_length_errorEPKc
	ldr	r0, [r4, #0]
.L23:
	cmp	r5, r7
	beq	.L19
	mov	r1, r5
	mov	r2, r6
	bl	memmove
	add	r0, r0, r6
.L19:
	mov	r1, #0
	str	r0, [r4, #4]
	strb	r1, [r0, #0]
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L26:
	mov	r0, r8
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
.LEHE0:
	b	.L17
.L25:
.L21:
	ldr	r3, [r4, #0]
	cmp	r3, #0
	mov	r4, r0
	movne	r0, r3
	blne	free
.L22:
	mov	r0, r4
.LEHB1:
	bl	__cxa_end_cleanup
.LEHE1:
.L28:
	.align	2
.L27:
	.word	.LC0
	.global	__gxx_personality_v0
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA3720:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3720-.LLSDACSB3720
.LLSDACSB3720:
	.uleb128 .LEHB0-.LFB3720
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L25-.LFB3720
	.uleb128 0x0
	.uleb128 .LEHB1-.LFB3720
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE3720:
	.fnend
	.size	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_, .-_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
	.section	.text.T.932,"ax",%progbits
	.align	2
	.type	T.932, %function
T.932:
	.fnstart
.LFB4997:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	cmp	r0, #0
	.pad #12
	sub	sp, sp, #12
	str	r0, [sp, #4]
	beq	.L48
.L57:
	ldr	r3, [sp, #4]
	ldr	fp, [r3, #12]
	cmp	fp, #0
	beq	.L31
.L56:
	ldr	r4, [fp, #12]
	cmp	r4, #0
	beq	.L32
.L55:
	ldr	r5, [r4, #12]
	cmp	r5, #0
	beq	.L33
.L54:
	ldr	r6, [r5, #12]
	cmp	r6, #0
	beq	.L34
.L53:
	ldr	r7, [r6, #12]
	cmp	r7, #0
	beq	.L35
.L52:
	ldr	r8, [r7, #12]
	cmp	r8, #0
	beq	.L36
.L51:
	ldr	sl, [r8, #12]
	cmp	sl, #0
	beq	.L37
.L50:
	ldr	r9, [sl, #12]
	cmp	r9, #0
	beq	.L38
	b	.L49
.L58:
	mov	r9, r3
.L49:
	ldr	r0, [r9, #12]
	bl	T.932
	ldr	r1, [r9, #8]
	mov	r0, r9
	str	r1, [sp, #0]
	bl	free
	ldr	r3, [sp, #0]
	cmp	r3, #0
	bne	.L58
.L38:
	ldr	r9, [sl, #8]
	mov	r0, sl
	bl	free
	cmp	r9, #0
	movne	sl, r9
	bne	.L50
.L37:
	ldr	sl, [r8, #8]
	mov	r0, r8
	bl	free
	cmp	sl, #0
	movne	r8, sl
	bne	.L51
.L36:
	ldr	r8, [r7, #8]
	mov	r0, r7
	bl	free
	cmp	r8, #0
	movne	r7, r8
	bne	.L52
.L35:
	ldr	r7, [r6, #8]
	mov	r0, r6
	bl	free
	cmp	r7, #0
	movne	r6, r7
	bne	.L53
.L34:
	ldr	r6, [r5, #8]
	mov	r0, r5
	bl	free
	cmp	r6, #0
	movne	r5, r6
	bne	.L54
.L33:
	ldr	r5, [r4, #8]
	mov	r0, r4
	bl	free
	cmp	r5, #0
	movne	r4, r5
	bne	.L55
.L32:
	ldr	r4, [fp, #8]
	mov	r0, fp
	bl	free
	cmp	r4, #0
	movne	fp, r4
	bne	.L56
.L31:
	ldr	r2, [sp, #4]
	ldr	r4, [r2, #8]
	mov	r0, r2
	bl	free
	cmp	r4, #0
	strne	r4, [sp, #4]
	bne	.L57
.L48:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
	.fnend
	.size	T.932, .-T.932
	.section	.text._ZN21QuStupidPointerHelper6removeEPv,"axG",%progbits,_ZN21QuStupidPointerHelper6removeEPv,comdat
	.align	2
	.weak	_ZN21QuStupidPointerHelper6removeEPv
	.hidden	_ZN21QuStupidPointerHelper6removeEPv
	.type	_ZN21QuStupidPointerHelper6removeEPv, %function
_ZN21QuStupidPointerHelper6removeEPv:
	.fnstart
.LFB2722:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, lr}
	.save {r3, r4, r5, r6, r7, lr}
	ldr	r4, .L87
	ldr	r3, [r4, #0]
	ldr	r5, [r3, #4]
	cmp	r5, #0
	moveq	r6, r3
	moveq	r7, r3
	beq	.L71
	mov	r1, r3
	mov	r7, r5
	b	.L64
.L84:
	mov	r1, r7
	mov	r7, r2
.L64:
	ldr	r2, [r7, #16]
	cmp	r0, r2
	ldrhi	r2, [r7, #12]
	ldrls	r2, [r7, #8]
	movhi	r7, r1
	cmp	r2, #0
	bne	.L84
	mov	r1, r3
	b	.L68
.L85:
	mov	r1, r5
	mov	r5, r2
.L68:
	ldr	r2, [r5, #16]
	cmp	r0, r2
	ldrcs	r2, [r5, #12]
	ldrcc	r2, [r5, #8]
	movcs	r5, r1
	cmp	r2, #0
	bne	.L85
	cmp	r7, r5
	mov	r6, r7
	moveq	r7, r5
	beq	.L71
	mov	r0, r7
.L72:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r5, r0
	bne	.L72
	ldr	r3, [r4, #0]
	mov	r6, r5
.L71:
	ldr	r2, [r3, #8]
	cmp	r7, r2
	beq	.L86
.L74:
	cmp	r7, r6
	bne	.L83
.L81:
	ldmfd	sp!, {r3, r4, r5, r6, r7, lr}
	bx	lr
.L80:
	mov	r7, r5
.L83:
	mov	r0, r7
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	ip, [r4, #0]
	mov	r5, r0
	add	r3, ip, #12
	mov	r0, r7
	add	r1, ip, #4
	add	r2, ip, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L79:
	ldr	r3, [r4, #4]
	cmp	r5, r6
	sub	lr, r3, #1
	str	lr, [r4, #4]
	bne	.L80
	b	.L81
.L86:
	cmp	r6, r3
	bne	.L74
	ldr	r3, [r4, #4]
	cmp	r3, #0
	ldr	r4, .L87
	beq	.L81
	ldr	r0, [r6, #4]
	bl	T.932
	ldr	r2, [r4, #0]
	str	r2, [r2, #8]
	ldr	ip, [r4, #0]
	mov	r0, #0
	str	r0, [ip, #4]
	ldr	r1, [r4, #0]
	str	r1, [r1, #12]
	str	r0, [r4, #4]
	b	.L81
.L88:
	.align	2
.L87:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	_ZN21QuStupidPointerHelper6removeEPv, .-_ZN21QuStupidPointerHelper6removeEPv
	.section	.text.T.941,"ax",%progbits
	.align	2
	.type	T.941, %function
T.941:
	.fnstart
.LFB5006:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	.save {r4, r5, r6, r7, r8, lr}
	ldr	r4, .L104
	ldr	ip, [r4, #0]
	cmp	r2, ip
	mov	r5, r2
	mov	r6, r0
	mov	r8, r3
	beq	.L90
	ldr	r3, [sp, #24]
	cmp	r3, #0
	beq	.L100
.L91:
	mov	r0, #24
	bl	malloc
	subs	r7, r0, #0
	beq	.L97
.L98:
	ldmia	r8, {ip, lr}	@ phole ldm
	str	lr, [r7, #20]
	str	ip, [r7, #16]
	str	r7, [r5, #12]
	ldr	r0, [r4, #0]
	ldr	r1, [r0, #12]
	cmp	r5, r1
	streq	r7, [r0, #12]
.L94:
	mov	r1, #0
	str	r1, [r7, #12]
	str	r1, [r7, #8]
	str	r5, [r7, #4]
	ldr	r0, [r4, #0]
	add	r1, r0, #4
	mov	r0, r7
	bl	_ZN4_STL10_Rb_globalIbE10_RebalanceEPNS_18_Rb_tree_node_baseERS3_
	ldr	r2, [r4, #4]
	add	r3, r2, #1
	str	r3, [r4, #4]
	mov	r0, r6
	str	r7, [r6, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L100:
	cmp	r1, #0
	beq	.L101
.L90:
	mov	r0, #24
	bl	malloc
	subs	r7, r0, #0
	beq	.L102
.L92:
	ldr	r1, [r8, #4]
	ldr	lr, [r8, #0]
	str	r1, [r7, #20]
	str	lr, [r7, #16]
	str	r7, [r5, #8]
	ldr	r3, [r4, #0]
	cmp	r5, r3
	beq	.L103
	ldr	r2, [r3, #8]
	cmp	r5, r2
	streq	r7, [r3, #8]
	b	.L94
.L103:
	str	r7, [r5, #4]
	ldr	r3, .L104
	ldr	ip, [r3, #0]
	str	r7, [ip, #12]
	b	.L94
.L97:
	mov	r0, #24
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
	mov	r7, r0
	b	.L98
.L102:
	mov	r0, #24
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
	mov	r7, r0
	b	.L92
.L101:
	ldr	r2, [r8, #0]
	ldr	r0, [r5, #16]
	cmp	r2, r0
	bcs	.L91
	b	.L90
.L105:
	.align	2
.L104:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	T.941, .-T.941
	.section	.text.T.943,"ax",%progbits
	.align	2
	.type	T.943, %function
T.943:
	.fnstart
.LFB5008:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	ldr	r3, .L129
	ldr	r3, [r3, #0]
	ldr	r6, [r3, #4]
	cmp	r6, #0
	.pad #16
	sub	sp, sp, #16
	mov	r4, r0
	mov	r5, r1
	beq	.L107
	ldr	r0, [r1, #0]
	b	.L111
.L126:
	mov	r6, r2
.L111:
	ldr	r2, [r6, #16]
	cmp	r2, r0
	ldrhi	r2, [r6, #8]
	ldrls	r2, [r6, #12]
	movhi	r1, #1
	movls	r1, #0
	cmp	r2, #0
	bne	.L126
	cmp	r1, #0
	moveq	r0, r6
	beq	.L114
.L113:
	ldr	r0, [r3, #8]
	cmp	r6, r0
	beq	.L127
	mov	r0, r6
	bl	_ZN4_STL10_Rb_globalIbE12_M_decrementEPNS_18_Rb_tree_node_baseE
.L114:
	ldr	r3, [r5, #0]
	ldr	r1, [r0, #16]
	cmp	r1, r3
	movcs	r3, #0
	strcs	r0, [r4, #0]
	strcsb	r3, [r4, #4]
	bcc	.L128
.L106:
	mov	r0, r4
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L128:
	mov	lr, #0
	mov	r2, r6
	add	r0, sp, #12
	mov	r1, lr
	mov	r3, r5
	str	lr, [sp, #0]
	bl	T.941
	ldr	r2, [sp, #12]
	mov	r0, #1
	str	r2, [r4, #0]
	strb	r0, [r4, #4]
	b	.L106
.L127:
	mov	r1, r6
	mov	r2, r6
	mov	ip, #0
	mov	r3, r5
	add	r0, sp, #8
	str	ip, [sp, #0]
	bl	T.941
	ldr	r2, [sp, #8]
	mov	r1, #1
	str	r2, [r4, #0]
	strb	r1, [r4, #4]
	b	.L106
.L107:
	mov	r6, r3
	b	.L113
.L130:
	.align	2
.L129:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	T.943, .-T.943
	.section	.text.T.938,"ax",%progbits
	.align	2
	.type	T.938, %function
T.938:
	.fnstart
.LFB5003:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	.save {r4, r5, r6, r7, lr}
	ldr	r6, .L165
	ldr	r3, [r6, #0]
	ldr	ip, [r1, #0]
	mov	r7, r1
	ldr	r1, [r3, #8]
	cmp	ip, r1
	.pad #44
	sub	sp, sp, #44
	mov	r5, r2
	mov	r4, r0
	beq	.L159
	cmp	ip, r3
	beq	.L160
	mov	r0, ip
	bl	_ZN4_STL10_Rb_globalIbE12_M_decrementEPNS_18_Rb_tree_node_baseE
	mov	ip, r0
	ldr	r0, [r7, #0]
	ldr	r3, [r5, #0]
	ldr	r2, [r0, #16]
	cmp	r3, r2
	bcs	.L146
	ldr	r1, [ip, #16]
	cmp	r3, r1
	bls	.L147
.L150:
	ldr	r1, [ip, #12]
	cmp	r1, #0
	beq	.L157
.L152:
	mov	r1, r0
	mov	ip, #0
	mov	r3, r5
	mov	r0, r4
	mov	r2, r1
	str	ip, [sp, #0]
	bl	T.941
	b	.L131
.L146:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	ip, [r7, #0]
	ldr	r3, [r5, #0]
	ldr	r2, [ip, #16]
	cmp	r2, r3
	bcs	.L149
	ldr	lr, [r6, #0]
	cmp	r0, lr
	beq	.L150
	ldr	r1, [r0, #16]
	cmp	r3, r1
	bcs	.L151
	b	.L150
.L159:
	ldr	r3, [r6, #4]
	cmp	r3, #0
	beq	.L161
	ldr	r2, [r2, #0]
	ldr	r1, [ip, #16]
	cmp	r2, r1
	bcc	.L162
	bhi	.L137
.L149:
	str	ip, [r4, #0]
.L131:
	mov	r0, r4
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L147:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
.L151:
	add	r0, sp, #32
	mov	r1, r5
	bl	T.943
	ldr	r0, [sp, #32]
	str	r0, [r4, #0]
	b	.L131
.L160:
	ldr	r2, [ip, #12]
	ldr	r3, [r5, #0]
	ldr	lr, [r2, #16]
	cmp	lr, r3
	bcc	.L163
	mov	r1, r5
	add	r0, sp, #24
	bl	T.943
	ldr	ip, [sp, #24]
	str	ip, [r4, #0]
	b	.L131
.L137:
	mov	r0, ip
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r3, [r6, #0]
	cmp	r0, r3
	beq	.L164
	ldr	r2, [r5, #0]
	ldr	r1, [r0, #16]
	cmp	r2, r1
	bcs	.L140
	ldr	ip, [r7, #0]
	ldr	r1, [ip, #12]
	cmp	r1, #0
	bne	.L152
.L157:
	mov	r2, ip
	mov	r3, r5
	mov	r0, r4
	str	ip, [sp, #0]
	bl	T.941
	b	.L131
.L163:
	mov	r3, r5
	mov	r1, #0
	str	ip, [sp, #0]
	bl	T.941
	b	.L131
.L161:
	add	r0, sp, #8
	mov	r1, r2
	bl	T.943
	ldr	r0, [sp, #8]
	str	r0, [r4, #0]
	b	.L131
.L162:
	mov	r1, ip
	mov	r2, ip
	mov	r3, r5
	mov	ip, #0
	str	ip, [sp, #0]
	bl	T.941
	b	.L131
.L140:
	add	r0, sp, #16
	mov	r1, r5
	bl	T.943
	ldr	r0, [sp, #16]
	str	r0, [r4, #0]
	b	.L131
.L164:
	ldr	lr, [r7, #0]
	mov	r3, r5
	mov	r2, lr
	mov	r0, r4
	mov	r1, #0
	str	lr, [sp, #0]
	bl	T.941
	b	.L131
.L166:
	.align	2
.L165:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	T.938, .-T.938
	.section	.text._ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev,"axG",%progbits,_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev,comdat
	.align	2
	.weak	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	.hidden	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	.type	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev, %function
_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev:
	.fnstart
.LFB4160:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	ldr	r4, [r0, #0]
	mov	r5, r0
	ldr	r0, [r4, #0]
	ldr	r3, [r0, #-12]
	add	r3, r4, r3
	ldr	r2, [r3, #4]
	tst	r2, #8192
	bne	.L171
.L168:
	mov	r0, r5
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L171:
	ldr	r3, [r3, #88]
	cmp	r3, #0
	beq	.L168
	mov	r0, r3
	ldr	r1, [r3, #0]
	ldr	ip, [r1, #20]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	bne	.L168
	ldr	r3, [r4, #0]
	ldr	r0, [r3, #-12]
	add	r0, r4, r0
	ldr	r2, [r0, #8]
	ldr	lr, [r0, #20]
	orr	ip, r2, #1
	tst	ip, lr
	str	ip, [r0, #8]
	beq	.L168
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
	b	.L168
	.fnend
	.size	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev, .-_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	.section	.text._ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE3putEc,"axG",%progbits,_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE3putEc,comdat
	.align	2
	.weak	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE3putEc
	.hidden	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE3putEc
	.type	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE3putEc, %function
_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE3putEc:
	.fnstart
.LFB4169:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	.pad #8
	sub	sp, sp, #8
	str	r0, [sp, #0]
	ldr	r2, [r0, #0]
	ldr	r3, [r2, #-12]
	add	r3, r0, r3
	mov	r4, r0
	ldr	r0, [r3, #8]
	cmp	r0, #0
	mov	r6, r1
	movne	r3, #0
	bne	.L174
	ldr	r1, [r3, #88]
	cmp	r1, #0
	beq	.L193
.L175:
	ldr	r5, [r3, #92]
	cmp	r5, #0
	beq	.L177
	ldr	ip, [r5, #0]
	ldr	r1, [ip, #-12]
	add	r0, r5, r1
	ldr	r1, [r0, #88]
	cmp	r1, #0
	beq	.L177
	mov	r0, r1
	ldr	r3, [r1, #0]
.LEHB2:
	ldr	ip, [r3, #20]
	mov	lr, pc
	bx	ip
.LEHE2:
	cmn	r0, #1
	beq	.L178
.L192:
	ldr	r2, [r4, #0]
	ldr	ip, [r2, #-12]
	add	r3, r4, ip
.L177:
	ldr	r3, [r3, #8]
	rsbs	r3, r3, #1
	movcc	r3, #0
.L174:
	cmp	r3, #0
	strb	r3, [sp, #4]
	beq	.L182
	ldr	r1, [r2, #-12]
	add	r0, r4, r1
	ldr	r3, [r0, #88]
	add	r2, r3, #20
	ldmia	r2, {r2, r5}	@ phole ldm
	cmp	r2, r5
	strccb	r6, [r2], #1
	strcc	r2, [r3, #20]
	bcs	.L194
.L184:
	ldr	r5, [sp, #0]
	ldr	r3, [r5, #0]
	ldr	ip, [r3, #-12]
	add	r3, r5, ip
	ldr	r1, [r3, #4]
	tst	r1, #8192
	bne	.L195
.L186:
	mov	r0, r4
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L194:
	mov	r0, r3
	mov	r1, r6
	ldr	ip, [r3, #0]
.LEHB3:
	ldr	ip, [ip, #52]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	bne	.L184
	ldr	r2, [r4, #0]
.L182:
	ldr	r0, [r2, #-12]
	add	r0, r4, r0
	ldr	r5, [r0, #8]
	ldr	r2, [r0, #20]
	orr	r3, r5, #1
	tst	r3, r2
	str	r3, [r0, #8]
	beq	.L184
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
.LEHE3:
	ldr	r5, [sp, #0]
	ldr	r3, [r5, #0]
	ldr	ip, [r3, #-12]
	add	r3, r5, ip
	ldr	r1, [r3, #4]
	tst	r1, #8192
	beq	.L186
.L195:
	ldr	r3, [r3, #88]
	cmp	r3, #0
	beq	.L186
	mov	r0, r3
	ldr	r2, [r3, #0]
.LEHB4:
	ldr	ip, [r2, #20]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	bne	.L186
	ldr	r3, [r5, #0]
	ldr	r0, [r3, #-12]
	add	r0, r5, r0
	ldr	ip, [r0, #8]
	ldr	r1, [r0, #20]
	orr	lr, ip, #1
	tst	lr, r1
	str	lr, [r0, #8]
	beq	.L186
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
	b	.L186
.L178:
	ldr	r1, [r5, #0]
	ldr	r0, [r1, #-12]
	add	r0, r5, r0
	ldr	r5, [r0, #8]
	ldr	r2, [r0, #20]
	orr	lr, r5, #1
	tst	lr, r2
	str	lr, [r0, #8]
	beq	.L192
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
	b	.L192
.L193:
	ldr	r5, [r3, #20]
	mov	r2, #1
	tst	r2, r5
	str	r2, [r3, #8]
	movne	r0, r3
	blne	_ZN4_STL8ios_base16_M_throw_failureEv
.LEHE4:
.L191:
	ldr	r2, [r4, #0]
	ldr	ip, [r2, #-12]
	add	r3, r4, ip
	b	.L175
.L190:
.L188:
	mov	r4, r0
	mov	r0, sp
	bl	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	mov	r0, r4
.LEHB5:
	bl	__cxa_end_cleanup
.LEHE5:
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA4169:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4169-.LLSDACSB4169
.LLSDACSB4169:
	.uleb128 .LEHB2-.LFB4169
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB3-.LFB4169
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L190-.LFB4169
	.uleb128 0x0
	.uleb128 .LEHB4-.LFB4169
	.uleb128 .LEHE4-.LEHB4
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB5-.LFB4169
	.uleb128 .LEHE5-.LEHB5
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE4169:
	.fnend
	.size	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE3putEc, .-_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE3putEc
	.section	.text._ZN4_STL4endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_,"axG",%progbits,_ZN4_STL4endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_,comdat
	.align	2
	.weak	_ZN4_STL4endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.hidden	_ZN4_STL4endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.type	_ZN4_STL4endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_, %function
_ZN4_STL4endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_:
	.fnstart
.LFB3836:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	ldr	r0, [r0, #0]
	ldr	r3, [r0, #-12]
	add	lr, r4, r3
	ldr	ip, [lr, #64]
	mov	r1, #10
	mov	r0, ip
	ldr	r2, [ip, #0]
	ldr	ip, [r2, #24]
	mov	lr, pc
	bx	ip
	mov	r1, r0
	mov	r0, r4
	bl	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE3putEc
	ldr	r1, [r4, #0]
	ldr	r0, [r1, #-12]
	add	r3, r4, r0
	ldr	r3, [r3, #88]
	cmp	r3, #0
	beq	.L197
	mov	r0, r3
	ldr	r1, [r3, #0]
	ldr	ip, [r1, #20]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	beq	.L200
.L197:
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L200:
	ldr	r3, [r4, #0]
	ldr	r0, [r3, #-12]
	add	r0, r4, r0
	ldr	lr, [r0, #8]
	ldr	r2, [r0, #20]
	orr	ip, lr, #1
	tst	ip, r2
	str	ip, [r0, #8]
	beq	.L197
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
	b	.L197
	.fnend
	.size	_ZN4_STL4endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_, .-_ZN4_STL4endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.section	.text.T.937,"ax",%progbits
	.align	2
	.type	T.937, %function
T.937:
	.fnstart
.LFB5002:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	.save {r4, r5, r6, r7, r8, sl, lr}
	ldr	r3, [r0, #0]
	cmp	r3, #0
	.pad #20
	sub	sp, sp, #20
	mov	r4, r0
	mov	r7, r1
	beq	.L202
	ldr	r5, .L246
	ldr	r1, [r5, #0]
	ldr	r2, [r1, #4]
	cmp	r2, #0
	mov	ip, r1
	beq	.L204
.L207:
	ldr	r0, [r2, #16]
	cmp	r3, r0
	movls	ip, r2
	ldrhi	r2, [r2, #12]
	ldrls	r2, [r2, #8]
	cmp	r2, #0
	bne	.L207
.L204:
	cmp	ip, r1
	beq	.L209
	ldr	r2, [ip, #16]
	cmp	r3, r2
	mov	r2, ip
	bcs	.L210
.L209:
	mov	r2, sp
	str	r3, [sp, #0]
	add	r0, sp, #12
	mov	r3, #0
	add	r1, sp, #8
	stmib	sp, {r3, ip}	@ phole stm
	bl	T.938
	ldr	r2, [sp, #12]
.L210:
	ldr	ip, [r2, #20]
	sub	r1, ip, #1
	cmp	r1, #0
	str	r1, [r2, #20]
	beq	.L240
.L202:
	mov	r3, #0
	stmia	r4, {r3, r7}	@ phole stm
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	bx	lr
.L240:
	ldr	r1, [r5, #0]
	ldr	r6, [r1, #4]
	cmp	r6, #0
	ldr	r2, [r4, #0]
	moveq	sl, r1
	moveq	r8, r1
	beq	.L222
	mov	sl, r6
	ldr	r3, [sl, #16]
	cmp	r2, r3
	ldrhi	r3, [sl, #12]
	ldrls	r3, [sl, #8]
	mov	r0, r1
	movhi	sl, r0
	cmp	r3, #0
	beq	.L214
.L241:
	mov	r0, sl
	mov	sl, r3
	ldr	r3, [sl, #16]
	cmp	r2, r3
	ldrhi	r3, [sl, #12]
	ldrls	r3, [sl, #8]
	movhi	sl, r0
	cmp	r3, #0
	bne	.L241
.L214:
	ldr	r3, [r6, #16]
	cmp	r2, r3
	ldrcs	r3, [r6, #12]
	ldrcc	r3, [r6, #8]
	mov	r0, r1
	movcs	r6, r0
	cmp	r3, #0
	beq	.L218
.L242:
	mov	r0, r6
	mov	r6, r3
	ldr	r3, [r6, #16]
	cmp	r2, r3
	ldrcs	r3, [r6, #12]
	ldrcc	r3, [r6, #8]
	movcs	r6, r0
	cmp	r3, #0
	bne	.L242
.L218:
	cmp	sl, r6
	mov	r8, sl
	moveq	sl, r6
	beq	.L222
	mov	r0, sl
.L223:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r6, r0
	bne	.L223
	ldr	r1, [r5, #0]
	mov	r8, r6
.L222:
	ldr	r0, [r1, #8]
	cmp	sl, r0
	beq	.L243
.L225:
	cmp	sl, r8
	bne	.L238
	b	.L228
.L231:
	mov	sl, r6
.L238:
	mov	r0, sl
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r2, [r5, #0]
	mov	r6, r0
	add	r3, r2, #12
	add	r1, r2, #4
	mov	r0, sl
	add	r2, r2, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L230:
	ldr	r3, [r5, #4]
	cmp	r6, r8
	sub	lr, r3, #1
	str	lr, [r5, #4]
	bne	.L231
.L228:
	ldr	r1, [r4, #4]
	cmp	r1, #1
	ldr	r3, [r4, #0]
	beq	.L244
	cmp	r3, #0
	beq	.L202
	ldr	r0, [r3, #-4]
	add	r0, r3, r0, asl #2
	cmp	r3, r0
	beq	.L233
	b	.L239
.L245:
	mov	r0, r5
.L239:
	sub	r5, r0, #4
	ldr	r2, [r0, #-4]
	mov	r0, r5
	ldr	ip, [r2, #0]
	mov	lr, pc
	bx	ip
	ldr	r0, [r4, #0]
	cmp	r0, r5
	bne	.L245
.L233:
	sub	r0, r0, #8
	bl	_ZdaPv
	b	.L202
.L244:
	cmp	r3, #0
	beq	.L202
	mov	r0, r3
	ldr	ip, [r3, #0]
	ldr	ip, [ip, #4]
	mov	lr, pc
	bx	ip
	b	.L202
.L243:
	cmp	r8, r1
	bne	.L225
	ldr	r2, [r5, #4]
	cmp	r2, #0
	ldr	r5, .L246
	beq	.L228
	ldr	r0, [r8, #4]
	bl	T.932
	ldr	r0, [r5, #0]
	str	r0, [r0, #8]
	ldr	ip, [r5, #0]
	mov	r3, #0
	str	r3, [ip, #4]
	ldr	r1, [r5, #0]
	str	r1, [r1, #12]
	str	r3, [r5, #4]
	b	.L228
.L247:
	.align	2
.L246:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	T.937, .-T.937
	.section	.text.T.933,"ax",%progbits
	.align	2
	.type	T.933, %function
T.933:
	.fnstart
.LFB4998:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	.save {r4, r5, r6, r7, r8, sl, lr}
	ldr	r3, [r0, #0]
	cmp	r3, #0
	.pad #20
	sub	sp, sp, #20
	mov	r4, r0
	mov	r7, r1
	beq	.L249
	ldr	r5, .L294
	ldr	r1, [r5, #0]
	ldr	r2, [r1, #4]
	cmp	r2, #0
	mov	ip, r1
	beq	.L251
.L254:
	ldr	r0, [r2, #16]
	cmp	r3, r0
	movls	ip, r2
	ldrhi	r2, [r2, #12]
	ldrls	r2, [r2, #8]
	cmp	r2, #0
	bne	.L254
.L251:
	cmp	ip, r1
	beq	.L256
	ldr	r2, [ip, #16]
	cmp	r3, r2
	mov	r2, ip
	bcs	.L257
.L256:
	mov	r2, sp
	str	r3, [sp, #0]
	add	r0, sp, #12
	mov	r3, #0
	add	r1, sp, #8
	stmib	sp, {r3, ip}	@ phole stm
	bl	T.938
	ldr	r2, [sp, #12]
.L257:
	ldr	ip, [r2, #20]
	sub	r1, ip, #1
	cmp	r1, #0
	str	r1, [r2, #20]
	beq	.L288
.L249:
	mov	r0, #0
	stmia	r4, {r0, r7}	@ phole stm
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	bx	lr
.L288:
	ldr	r1, [r5, #0]
	ldr	r6, [r1, #4]
	cmp	r6, #0
	ldr	r2, [r4, #0]
	moveq	sl, r1
	moveq	r8, r1
	beq	.L269
	mov	sl, r6
	ldr	r3, [sl, #16]
	cmp	r2, r3
	ldrhi	r3, [sl, #12]
	ldrls	r3, [sl, #8]
	mov	r0, r1
	movhi	sl, r0
	cmp	r3, #0
	beq	.L261
.L289:
	mov	r0, sl
	mov	sl, r3
	ldr	r3, [sl, #16]
	cmp	r2, r3
	ldrhi	r3, [sl, #12]
	ldrls	r3, [sl, #8]
	movhi	sl, r0
	cmp	r3, #0
	bne	.L289
.L261:
	ldr	r3, [r6, #16]
	cmp	r2, r3
	ldrcs	r3, [r6, #12]
	ldrcc	r3, [r6, #8]
	mov	r0, r1
	movcs	r6, r0
	cmp	r3, #0
	beq	.L265
.L290:
	mov	r0, r6
	mov	r6, r3
	ldr	r3, [r6, #16]
	cmp	r2, r3
	ldrcs	r3, [r6, #12]
	ldrcc	r3, [r6, #8]
	movcs	r6, r0
	cmp	r3, #0
	bne	.L290
.L265:
	cmp	sl, r6
	mov	r8, sl
	moveq	sl, r6
	beq	.L269
	mov	r0, sl
.L270:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r6, r0
	bne	.L270
	ldr	r1, [r5, #0]
	mov	r8, r6
.L269:
	ldr	r0, [r1, #8]
	cmp	sl, r0
	beq	.L291
.L272:
	cmp	sl, r8
	bne	.L286
	b	.L275
.L278:
	mov	sl, r6
.L286:
	mov	r0, sl
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r2, [r5, #0]
	mov	r6, r0
	add	r3, r2, #12
	add	r1, r2, #4
	mov	r0, sl
	add	r2, r2, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L277:
	ldr	r3, [r5, #4]
	cmp	r6, r8
	sub	lr, r3, #1
	str	lr, [r5, #4]
	bne	.L278
.L275:
	ldr	r1, [r4, #4]
	cmp	r1, #1
	beq	.L292
	ldr	r3, [r4, #0]
	cmp	r3, #0
	beq	.L249
	ldr	ip, [r3, #-4]
	add	r0, r3, ip, asl #6
	cmp	r3, r0
	beq	.L281
	b	.L287
.L293:
	mov	r0, r5
.L287:
	sub	r5, r0, #64
	mov	r0, r5
	bl	_ZN9PngLoader5abortEv
	mov	r0, r5
	bl	_ZN9PngLoader15deleteImageDataEv
	ldr	r0, [r4, #0]
	cmp	r0, r5
	bne	.L293
.L281:
	sub	r0, r0, #8
	bl	_ZdaPv
	b	.L249
.L292:
	ldr	r5, [r4, #0]
	cmp	r5, #0
	beq	.L249
	mov	r0, r5
	bl	_ZN9PngLoader5abortEv
	mov	r0, r5
	bl	_ZN9PngLoader15deleteImageDataEv
	mov	r0, r5
	bl	_ZdlPv
	b	.L249
.L291:
	cmp	r8, r1
	bne	.L272
	ldr	r2, [r5, #4]
	cmp	r2, #0
	ldr	r5, .L294
	beq	.L275
	ldr	r0, [r8, #4]
	bl	T.932
	ldr	r0, [r5, #0]
	str	r0, [r0, #8]
	ldr	ip, [r5, #0]
	mov	r3, #0
	str	r3, [ip, #4]
	ldr	r1, [r5, #0]
	str	r1, [r1, #12]
	str	r3, [r5, #4]
	b	.L275
.L295:
	.align	2
.L294:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	T.933, .-T.933
	.section	.text.T.936,"ax",%progbits
	.align	2
	.type	T.936, %function
T.936:
	.fnstart
.LFB5001:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	.save {r4, r5, r6, r7, r8, lr}
	ldr	r3, [r0, #0]
	cmp	r3, #0
	.pad #16
	sub	sp, sp, #16
	mov	r4, r0
	beq	.L297
	ldr	r5, .L341
	ldr	r1, [r5, #0]
	ldr	r2, [r1, #4]
	cmp	r2, #0
	mov	ip, r1
	beq	.L299
.L302:
	ldr	r0, [r2, #16]
	cmp	r3, r0
	movls	ip, r2
	ldrhi	r2, [r2, #12]
	ldrls	r2, [r2, #8]
	cmp	r2, #0
	bne	.L302
.L299:
	cmp	ip, r1
	beq	.L304
	ldr	r2, [ip, #16]
	cmp	r3, r2
	mov	r2, ip
	bcs	.L305
.L304:
	mov	r2, sp
	str	r3, [sp, #0]
	add	r0, sp, #12
	mov	r3, #0
	add	r1, sp, #8
	stmib	sp, {r3, ip}	@ phole stm
	bl	T.938
	ldr	r2, [sp, #12]
.L305:
	ldr	ip, [r2, #20]
	sub	r1, ip, #1
	cmp	r1, #0
	str	r1, [r2, #20]
	beq	.L335
.L297:
	mov	r1, #1
	mov	r3, #0
	str	r1, [r4, #4]
	str	r3, [r4, #0]
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L335:
	ldr	r1, [r5, #0]
	ldr	r6, [r1, #4]
	cmp	r6, #0
	ldr	r2, [r4, #0]
	moveq	r8, r1
	moveq	r7, r1
	beq	.L317
	mov	r8, r6
	ldr	r3, [r8, #16]
	cmp	r2, r3
	ldrhi	r3, [r8, #12]
	ldrls	r3, [r8, #8]
	mov	r0, r1
	movhi	r8, r0
	cmp	r3, #0
	beq	.L309
.L336:
	mov	r0, r8
	mov	r8, r3
	ldr	r3, [r8, #16]
	cmp	r2, r3
	ldrhi	r3, [r8, #12]
	ldrls	r3, [r8, #8]
	movhi	r8, r0
	cmp	r3, #0
	bne	.L336
.L309:
	ldr	r3, [r6, #16]
	cmp	r2, r3
	ldrcs	r3, [r6, #12]
	ldrcc	r3, [r6, #8]
	mov	r0, r1
	movcs	r6, r0
	cmp	r3, #0
	beq	.L313
.L337:
	mov	r0, r6
	mov	r6, r3
	ldr	r3, [r6, #16]
	cmp	r2, r3
	ldrcs	r3, [r6, #12]
	ldrcc	r3, [r6, #8]
	movcs	r6, r0
	cmp	r3, #0
	bne	.L337
.L313:
	cmp	r8, r6
	mov	r7, r8
	moveq	r8, r6
	beq	.L317
	mov	r0, r8
.L318:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r6, r0
	bne	.L318
	ldr	r1, [r5, #0]
	mov	r7, r6
.L317:
	ldr	r0, [r1, #8]
	cmp	r8, r0
	beq	.L338
.L320:
	cmp	r8, r7
	bne	.L333
	b	.L323
.L326:
	mov	r8, r6
.L333:
	mov	r0, r8
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r2, [r5, #0]
	mov	r6, r0
	add	r3, r2, #12
	add	r1, r2, #4
	mov	r0, r8
	add	r2, r2, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L325:
	ldr	r3, [r5, #4]
	cmp	r6, r7
	sub	lr, r3, #1
	str	lr, [r5, #4]
	bne	.L326
.L323:
	ldr	r1, [r4, #4]
	cmp	r1, #1
	ldr	r3, [r4, #0]
	beq	.L339
	cmp	r3, #0
	beq	.L297
	ldr	r0, [r3, #-4]
	add	r0, r3, r0, asl #2
	cmp	r3, r0
	beq	.L328
	b	.L334
.L340:
	mov	r0, r5
.L334:
	sub	r5, r0, #4
	ldr	r2, [r0, #-4]
	mov	r0, r5
	ldr	ip, [r2, #0]
	mov	lr, pc
	bx	ip
	ldr	r0, [r4, #0]
	cmp	r0, r5
	bne	.L340
.L328:
	sub	r0, r0, #8
	bl	_ZdaPv
	b	.L297
.L339:
	cmp	r3, #0
	beq	.L297
	mov	r0, r3
	ldr	ip, [r3, #0]
	ldr	ip, [ip, #4]
	mov	lr, pc
	bx	ip
	b	.L297
.L338:
	cmp	r7, r1
	bne	.L320
	ldr	r2, [r5, #4]
	cmp	r2, #0
	ldr	r5, .L341
	beq	.L323
	ldr	r0, [r7, #4]
	bl	T.932
	ldr	r0, [r5, #0]
	str	r0, [r0, #8]
	ldr	ip, [r5, #0]
	mov	r3, #0
	str	r3, [ip, #4]
	ldr	r1, [r5, #0]
	str	r1, [r1, #12]
	str	r3, [r5, #4]
	b	.L323
.L342:
	.align	2
.L341:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	T.936, .-T.936
	.section	.text._ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i,"axG",%progbits,_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i,comdat
	.align	2
	.weak	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
	.hidden	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
	.type	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i, %function
_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i:
	.fnstart
.LFB4125:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, lr}
	ldr	ip, [r0, #0]
	cmp	ip, #0
	.pad #32
	sub	sp, sp, #32
	mov	r5, r0
	mov	r4, r1
	mov	r8, r2
	beq	.L344
	ldr	r6, .L414
	ldr	r0, [r6, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L348
	mov	r1, r0
	b	.L349
.L404:
	mov	r1, r3
	mov	r3, r2
.L349:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L404
.L348:
	cmp	r3, r0
	beq	.L351
	ldr	r2, [r3, #16]
	cmp	ip, r2
	mov	r2, r3
	bcs	.L352
.L351:
	add	r2, sp, #8
	str	r3, [sp, #24]
	add	r0, sp, #28
	mov	r3, #0
	add	r1, sp, #24
	str	ip, [sp, #8]
	str	r3, [sp, #12]
	bl	T.938
	ldr	r2, [sp, #28]
.L352:
	ldr	r1, [r2, #20]
	sub	r0, r1, #1
	cmp	r0, #0
	str	r0, [r2, #20]
	beq	.L405
.L344:
	cmp	r4, #0
	stmia	r5, {r4, r8}	@ phole stm
	beq	.L398
	ldr	r5, .L414
	ldr	r3, [r5, #0]
	ldr	ip, [r3, #4]
	cmp	ip, #0
	beq	.L378
	mov	r0, r3
	mov	r2, ip
	b	.L382
.L406:
	mov	r0, r2
	mov	r2, r1
.L382:
	ldr	r1, [r2, #16]
	cmp	r4, r1
	ldrhi	r1, [r2, #12]
	ldrls	r1, [r2, #8]
	movhi	r2, r0
	cmp	r1, #0
	bne	.L406
	cmp	r3, r2
	beq	.L378
	ldr	r0, [r2, #16]
	cmp	r4, r0
	bcc	.L378
.L383:
	cmp	r2, r3
	mov	r1, r2
	beq	.L403
.L385:
	ldr	ip, [r1, #20]
	add	r0, ip, #1
	str	r0, [r1, #20]
.L398:
	add	sp, sp, #32
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L407:
	ldr	r1, [ip, #16]
	cmp	r4, r1
	movls	r3, ip
	ldrhi	ip, [ip, #12]
	ldrls	ip, [ip, #8]
.L403:
	cmp	ip, #0
	bne	.L407
	cmp	r2, r3
	beq	.L391
	ldr	ip, [r3, #16]
	cmp	r4, ip
	mov	r2, r3
	bcs	.L392
.L391:
	mov	r2, sp
	mov	lr, #0
	add	r0, sp, #20
	add	r1, sp, #16
	str	r3, [sp, #16]
	stmia	sp, {r4, lr}	@ phole stm
	bl	T.938
	ldr	r2, [sp, #20]
.L392:
	mov	r3, #0
	str	r3, [r2, #20]
	ldr	r0, [r5, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	beq	.L393
	mov	r1, r0
	b	.L397
.L408:
	mov	r1, r3
	mov	r3, r2
.L397:
	ldr	r2, [r3, #16]
	cmp	r4, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L408
	cmp	r0, r3
	beq	.L393
	ldr	r2, [r3, #16]
	cmp	r4, r2
	movcs	r0, r3
.L393:
	mov	r1, r0
	b	.L385
.L378:
	mov	r2, r3
	b	.L383
.L405:
	ldr	r1, [r6, #0]
	ldr	r7, [r1, #4]
	cmp	r7, #0
	ldr	r2, [r5, #0]
	moveq	sl, r1
	moveq	r9, r1
	beq	.L364
	mov	r0, r1
	mov	r9, r7
	b	.L357
.L409:
	mov	r0, r9
	mov	r9, r3
.L357:
	ldr	r3, [r9, #16]
	cmp	r2, r3
	ldrhi	r3, [r9, #12]
	ldrls	r3, [r9, #8]
	movhi	r9, r0
	cmp	r3, #0
	bne	.L409
	mov	r0, r1
	b	.L361
.L410:
	mov	r0, r7
	mov	r7, r3
.L361:
	ldr	r3, [r7, #16]
	cmp	r2, r3
	ldrcs	r3, [r7, #12]
	ldrcc	r3, [r7, #8]
	movcs	r7, r0
	cmp	r3, #0
	bne	.L410
	cmp	r9, r7
	mov	sl, r9
	moveq	r9, r7
	beq	.L364
	mov	r0, r9
.L365:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r7, r0
	bne	.L365
	ldr	r1, [r6, #0]
	mov	sl, r7
.L364:
	ldr	ip, [r1, #8]
	cmp	r9, ip
	beq	.L411
.L367:
	cmp	r9, sl
	bne	.L400
	b	.L370
.L373:
	mov	r9, r7
.L400:
	mov	r0, r9
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r2, [r6, #0]
	mov	r7, r0
	add	r3, r2, #12
	add	r1, r2, #4
	mov	r0, r9
	add	r2, r2, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L372:
	ldr	r3, [r6, #4]
	cmp	r7, sl
	sub	lr, r3, #1
	str	lr, [r6, #4]
	bne	.L373
.L370:
	ldr	r0, [r5, #4]
	cmp	r0, #1
	ldr	r3, [r5, #0]
	beq	.L412
	cmp	r3, #0
	beq	.L344
	ldr	r2, [r3, #-4]
	rsb	ip, r2, r2, asl #3
	add	r0, r3, ip, asl #2
	cmp	r3, r0
	bne	.L401
	b	.L375
.L413:
	mov	r0, r6
.L401:
	sub	r6, r0, #28
	ldr	r3, [r0, #-28]
	mov	r0, r6
	ldr	ip, [r3, #4]
	mov	lr, pc
	bx	ip
	ldr	r0, [r5, #0]
	cmp	r0, r6
	bne	.L413
.L375:
	sub	r0, r0, #8
	bl	_ZdaPv
	b	.L344
.L412:
	cmp	r3, #0
	beq	.L344
	mov	r0, r3
	ldr	r1, [r3, #0]
	ldr	ip, [r1, #8]
	mov	lr, pc
	bx	ip
	b	.L344
.L411:
	cmp	sl, r1
	bne	.L367
	ldr	r2, [r6, #4]
	cmp	r2, #0
	ldr	r6, .L414
	beq	.L370
	ldr	r0, [sl, #4]
	bl	T.932
	ldr	ip, [r6, #0]
	str	ip, [ip, #8]
	ldr	r1, [r6, #0]
	mov	r3, #0
	str	r3, [r1, #4]
	ldr	r0, [r6, #0]
	str	r0, [r0, #12]
	str	r3, [r6, #4]
	b	.L370
.L415:
	.align	2
.L414:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i, .-_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
	.section	.text._ZN15QuStupidPointerI10QuPngImageE3setEPS0_i,"axG",%progbits,_ZN15QuStupidPointerI10QuPngImageE3setEPS0_i,comdat
	.align	2
	.weak	_ZN15QuStupidPointerI10QuPngImageE3setEPS0_i
	.hidden	_ZN15QuStupidPointerI10QuPngImageE3setEPS0_i
	.type	_ZN15QuStupidPointerI10QuPngImageE3setEPS0_i, %function
_ZN15QuStupidPointerI10QuPngImageE3setEPS0_i:
	.fnstart
.LFB4241:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, lr}
	ldr	ip, [r0, #0]
	cmp	ip, #0
	.pad #32
	sub	sp, sp, #32
	mov	r5, r0
	mov	r4, r1
	mov	r8, r2
	beq	.L417
	ldr	r6, .L487
	ldr	r0, [r6, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L421
	mov	r1, r0
	b	.L422
.L477:
	mov	r1, r3
	mov	r3, r2
.L422:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L477
.L421:
	cmp	r3, r0
	beq	.L424
	ldr	r2, [r3, #16]
	cmp	ip, r2
	mov	r2, r3
	bcs	.L425
.L424:
	add	r2, sp, #8
	str	r3, [sp, #24]
	add	r0, sp, #28
	mov	r3, #0
	add	r1, sp, #24
	str	ip, [sp, #8]
	str	r3, [sp, #12]
	bl	T.938
	ldr	r2, [sp, #28]
.L425:
	ldr	r1, [r2, #20]
	sub	r0, r1, #1
	cmp	r0, #0
	str	r0, [r2, #20]
	beq	.L478
.L417:
	cmp	r4, #0
	stmia	r5, {r4, r8}	@ phole stm
	beq	.L471
	ldr	r5, .L487
	ldr	r3, [r5, #0]
	ldr	ip, [r3, #4]
	cmp	ip, #0
	beq	.L451
	mov	r0, r3
	mov	r2, ip
	b	.L455
.L479:
	mov	r0, r2
	mov	r2, r1
.L455:
	ldr	r1, [r2, #16]
	cmp	r4, r1
	ldrhi	r1, [r2, #12]
	ldrls	r1, [r2, #8]
	movhi	r2, r0
	cmp	r1, #0
	bne	.L479
	cmp	r3, r2
	beq	.L451
	ldr	r0, [r2, #16]
	cmp	r4, r0
	bcc	.L451
.L456:
	cmp	r2, r3
	mov	r1, r2
	beq	.L476
.L458:
	ldr	ip, [r1, #20]
	add	r0, ip, #1
	str	r0, [r1, #20]
.L471:
	add	sp, sp, #32
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L480:
	ldr	r1, [ip, #16]
	cmp	r4, r1
	movls	r3, ip
	ldrhi	ip, [ip, #12]
	ldrls	ip, [ip, #8]
.L476:
	cmp	ip, #0
	bne	.L480
	cmp	r2, r3
	beq	.L464
	ldr	ip, [r3, #16]
	cmp	r4, ip
	mov	r2, r3
	bcs	.L465
.L464:
	mov	r2, sp
	mov	lr, #0
	add	r0, sp, #20
	add	r1, sp, #16
	str	r3, [sp, #16]
	stmia	sp, {r4, lr}	@ phole stm
	bl	T.938
	ldr	r2, [sp, #20]
.L465:
	mov	r3, #0
	str	r3, [r2, #20]
	ldr	r0, [r5, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	beq	.L466
	mov	r1, r0
	b	.L470
.L481:
	mov	r1, r3
	mov	r3, r2
.L470:
	ldr	r2, [r3, #16]
	cmp	r4, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L481
	cmp	r0, r3
	beq	.L466
	ldr	r2, [r3, #16]
	cmp	r4, r2
	movcs	r0, r3
.L466:
	mov	r1, r0
	b	.L458
.L451:
	mov	r2, r3
	b	.L456
.L478:
	ldr	r1, [r6, #0]
	ldr	r7, [r1, #4]
	cmp	r7, #0
	ldr	r2, [r5, #0]
	moveq	sl, r1
	moveq	r9, r1
	beq	.L437
	mov	r0, r1
	mov	r9, r7
	b	.L430
.L482:
	mov	r0, r9
	mov	r9, r3
.L430:
	ldr	r3, [r9, #16]
	cmp	r2, r3
	ldrhi	r3, [r9, #12]
	ldrls	r3, [r9, #8]
	movhi	r9, r0
	cmp	r3, #0
	bne	.L482
	mov	r0, r1
	b	.L434
.L483:
	mov	r0, r7
	mov	r7, r3
.L434:
	ldr	r3, [r7, #16]
	cmp	r2, r3
	ldrcs	r3, [r7, #12]
	ldrcc	r3, [r7, #8]
	movcs	r7, r0
	cmp	r3, #0
	bne	.L483
	cmp	r9, r7
	mov	sl, r9
	moveq	r9, r7
	beq	.L437
	mov	r0, r9
.L438:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r7, r0
	bne	.L438
	ldr	r1, [r6, #0]
	mov	sl, r7
.L437:
	ldr	ip, [r1, #8]
	cmp	r9, ip
	beq	.L484
.L440:
	cmp	r9, sl
	bne	.L473
	b	.L443
.L446:
	mov	r9, r7
.L473:
	mov	r0, r9
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r2, [r6, #0]
	mov	r7, r0
	add	r3, r2, #12
	add	r1, r2, #4
	mov	r0, r9
	add	r2, r2, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L445:
	ldr	r3, [r6, #4]
	cmp	r7, sl
	sub	lr, r3, #1
	str	lr, [r6, #4]
	bne	.L446
.L443:
	ldr	r0, [r5, #4]
	cmp	r0, #1
	ldr	r3, [r5, #0]
	beq	.L485
	cmp	r3, #0
	beq	.L417
	ldr	r2, [r3, #-4]
	add	ip, r2, r2, asl #1
	add	r0, r3, ip, asl #4
	cmp	r3, r0
	bne	.L474
	b	.L448
.L486:
	mov	r0, r6
.L474:
	sub	r6, r0, #48
	ldr	r3, [r0, #-48]
	mov	r0, r6
	ldr	ip, [r3, #4]
	mov	lr, pc
	bx	ip
	ldr	r0, [r5, #0]
	cmp	r0, r6
	bne	.L486
.L448:
	sub	r0, r0, #8
	bl	_ZdaPv
	b	.L417
.L485:
	cmp	r3, #0
	beq	.L417
	mov	r0, r3
	ldr	r1, [r3, #0]
	ldr	ip, [r1, #8]
	mov	lr, pc
	bx	ip
	b	.L417
.L484:
	cmp	sl, r1
	bne	.L440
	ldr	r2, [r6, #4]
	cmp	r2, #0
	ldr	r6, .L487
	beq	.L443
	ldr	r0, [sl, #4]
	bl	T.932
	ldr	ip, [r6, #0]
	str	ip, [ip, #8]
	ldr	r1, [r6, #0]
	mov	r3, #0
	str	r3, [r1, #4]
	ldr	r0, [r6, #0]
	str	r0, [r0, #12]
	str	r3, [r6, #4]
	b	.L443
.L488:
	.align	2
.L487:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	_ZN15QuStupidPointerI10QuPngImageE3setEPS0_i, .-_ZN15QuStupidPointerI10QuPngImageE3setEPS0_i
	.section	.text._ZN12QuDrawObject9loadImageE15QuStupidPointerI11QuBaseImageEPfii,"ax",%progbits
	.align	2
	.global	_ZN12QuDrawObject9loadImageE15QuStupidPointerI11QuBaseImageEPfii
	.hidden	_ZN12QuDrawObject9loadImageE15QuStupidPointerI11QuBaseImageEPfii
	.type	_ZN12QuDrawObject9loadImageE15QuStupidPointerI11QuBaseImageEPfii, %function
_ZN12QuDrawObject9loadImageE15QuStupidPointerI11QuBaseImageEPfii:
	.fnstart
.LFB3705:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	ip, [r0, #124]
	.pad #44
	sub	sp, sp, #44
	str	r3, [sp, #4]
	cmp	ip, #0
	mov	r5, r0
	mov	sl, r2
	ldr	r9, [r1, #4]
	add	r7, r0, #124
	ldr	r4, [r1, #0]
	beq	.L490
	ldr	r6, .L566
	ldr	r0, [r6, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L494
	mov	r1, r0
	b	.L495
.L556:
	mov	r1, r3
	mov	r3, r2
.L495:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L556
.L494:
	cmp	r3, r0
	beq	.L497
	ldr	r2, [r3, #16]
	cmp	ip, r2
	mov	r2, r3
	bcs	.L498
.L497:
	add	r2, sp, #16
	str	r3, [sp, #32]
	add	r0, sp, #36
	mov	r3, #0
	add	r1, sp, #32
	str	ip, [sp, #16]
	str	r3, [sp, #20]
	bl	T.938
	ldr	r2, [sp, #36]
.L498:
	ldr	r0, [r2, #20]
	sub	r3, r0, #1
	cmp	r3, #0
	str	r3, [r2, #20]
	beq	.L557
.L490:
	cmp	r4, #0
	str	r9, [r5, #128]
	str	r4, [r5, #124]
	beq	.L523
	ldr	r6, .L566
	ldr	r3, [r6, #0]
	ldr	ip, [r3, #4]
	cmp	ip, #0
	beq	.L524
	mov	r0, r3
	mov	r2, ip
	b	.L528
.L558:
	mov	r0, r2
	mov	r2, r1
.L528:
	ldr	r1, [r2, #16]
	cmp	r4, r1
	ldrhi	r1, [r2, #12]
	ldrls	r1, [r2, #8]
	movhi	r2, r0
	cmp	r1, #0
	bne	.L558
	cmp	r3, r2
	beq	.L524
	ldr	r0, [r2, #16]
	cmp	r4, r0
	bcc	.L524
.L529:
	cmp	r2, r3
	mov	r1, r2
	beq	.L555
.L531:
	ldr	ip, [r1, #20]
	add	r0, ip, #1
	str	r0, [r1, #20]
.L523:
	mov	r1, #1
	cmp	sl, #0
	strb	r1, [r5, #120]
	beq	.L559
.L544:
	ldrb	r3, [r5, #36]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L549
	ldr	r0, [r5, #44]
	cmp	r0, #0
	blne	_ZdaPv
.L546:
	mov	r2, #0
	strb	r2, [r5, #36]
	str	r2, [r5, #44]
.L549:
	str	sl, [r5, #44]
	ldr	ip, [sp, #4]
	str	ip, [r5, #48]
	ldr	r2, [sp, #80]
	mov	r0, #32768
	add	r1, r0, #120
	mov	ip, #2
	mov	r0, #1
	str	r2, [r5, #52]
	str	r1, [r5, #56]
	str	ip, [r5, #60]
	strb	r0, [r5, #36]
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L560:
	ldr	r1, [ip, #16]
	cmp	r4, r1
	movls	r3, ip
	ldrhi	ip, [ip, #12]
	ldrls	ip, [ip, #8]
.L555:
	cmp	ip, #0
	bne	.L560
	cmp	r2, r3
	beq	.L537
	ldr	ip, [r3, #16]
	cmp	r4, ip
	mov	r2, r3
	bcs	.L538
.L537:
	add	r2, sp, #8
	mov	lr, #0
	add	r0, sp, #28
	add	r1, sp, #24
	str	r3, [sp, #24]
	str	lr, [sp, #12]
	str	r4, [sp, #8]
	bl	T.938
	ldr	r2, [sp, #28]
.L538:
	mov	r3, #0
	str	r3, [r2, #20]
	ldr	r0, [r6, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	beq	.L539
	mov	r1, r0
	b	.L543
.L561:
	mov	r1, r3
	mov	r3, r2
.L543:
	ldr	r2, [r3, #16]
	cmp	r4, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L561
	cmp	r0, r3
	beq	.L539
	ldr	r2, [r3, #16]
	cmp	r4, r2
	movcs	r0, r3
.L539:
	mov	r1, r0
	b	.L531
.L524:
	mov	r2, r3
	b	.L529
.L557:
	ldr	r1, [r6, #0]
	ldr	r8, [r1, #4]
	cmp	r8, #0
	ldr	r0, [r5, #124]
	moveq	fp, r1
	moveq	r3, r1
	beq	.L510
	mov	ip, r1
	mov	r3, r8
	b	.L503
.L562:
	mov	ip, r3
	mov	r3, r2
.L503:
	ldr	r2, [r3, #16]
	cmp	r0, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, ip
	cmp	r2, #0
	bne	.L562
	mov	ip, r1
	b	.L507
.L563:
	mov	ip, r8
	mov	r8, r2
.L507:
	ldr	r2, [r8, #16]
	cmp	r0, r2
	ldrcs	r2, [r8, #12]
	ldrcc	r2, [r8, #8]
	movcs	r8, ip
	cmp	r2, #0
	bne	.L563
	cmp	r3, r8
	mov	fp, r3
	moveq	r3, r8
	beq	.L510
	mov	r0, r3
.L511:
	str	r3, [sp, #0]
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r8, r0
	ldr	r3, [sp, #0]
	bne	.L511
	ldr	r1, [r6, #0]
	mov	fp, r8
.L510:
	ldr	ip, [r1, #8]
	cmp	r3, ip
	beq	.L564
.L513:
	cmp	r3, fp
	bne	.L553
	b	.L516
.L519:
	mov	r3, r8
.L553:
	mov	r0, r3
	str	r3, [sp, #0]
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r2, [r6, #0]
	ldr	r1, [sp, #0]
	add	r3, r2, #12
	mov	r8, r0
	mov	r0, r1
	add	r1, r2, #4
	add	r2, r2, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L518:
	ldr	r3, [r6, #4]
	cmp	r8, fp
	sub	lr, r3, #1
	str	lr, [r6, #4]
	bne	.L519
.L516:
	ldr	r0, [r5, #128]
	cmp	r0, #1
	ldr	r3, [r5, #124]
	beq	.L565
	cmp	r3, #0
	beq	.L490
	ldr	r2, [r3, #-4]
	rsb	r6, r2, r2, asl #3
	add	r6, r3, r6, asl #2
	cmp	r3, r6
	beq	.L521
.L550:
	ldr	r3, [r6, #-28]!
	mov	r0, r6
	ldr	ip, [r3, #4]
	mov	lr, pc
	bx	ip
	ldr	r1, [r7, #0]
	cmp	r1, r6
	bne	.L550
	ldr	r6, [r5, #124]
.L521:
	sub	r0, r6, #8
	bl	_ZdaPv
	b	.L490
.L559:
	mov	sl, #4
	str	sl, [r5, #4]
	ldr	r0, [r5, #124]
	bl	_ZN11QuBaseImage9getUVDataEv
	mov	sl, r0
	b	.L544
.L565:
	cmp	r3, #0
	beq	.L490
	mov	r0, r3
	ldr	ip, [r3, #0]
	ldr	ip, [ip, #8]
	mov	lr, pc
	bx	ip
	b	.L490
.L564:
	cmp	fp, r1
	bne	.L513
	ldr	r1, [r6, #4]
	cmp	r1, #0
	ldr	r6, .L566
	beq	.L516
	ldr	r0, [fp, #4]
	bl	T.932
	ldr	ip, [r6, #0]
	str	ip, [ip, #8]
	ldr	r0, [r6, #0]
	mov	r3, #0
	str	r3, [r0, #4]
	ldr	r2, [r6, #0]
	str	r2, [r2, #12]
	str	r3, [r6, #4]
	b	.L516
.L567:
	.align	2
.L566:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	_ZN12QuDrawObject9loadImageE15QuStupidPointerI11QuBaseImageEPfii, .-_ZN12QuDrawObject9loadImageE15QuStupidPointerI11QuBaseImageEPfii
	.section	.text._ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_,"axG",%progbits,_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_,comdat
	.align	2
	.weak	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_
	.hidden	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_
	.type	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_, %function
_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_:
	.fnstart
.LFB4681:
	@ Function supports interworking.
	@ args = 8, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	mov	r5, r1
	ldr	r1, [r1, #0]
	.pad #20
	sub	sp, sp, #20
	cmp	r1, r3
	mov	r7, r3
	mov	r9, r0
	ldr	r8, [sp, #56]
	beq	.L569
	ldr	r3, [sp, #60]
	cmp	r3, #0
	beq	.L613
.L570:
	mov	r0, #36
	bl	malloc
	subs	r4, r0, #0
	beq	.L614
.L603:
	add	sl, r4, #16
	mov	r0, sl
	mov	r1, r8
	mov	r6, r4
.LEHB6:
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
.LEHE6:
	mov	r0, r4
	mov	r3, #0
	str	r3, [r0, #28]!
	add	r1, r8, #12
	ldmia	r1, {r1, r2}	@ phole ldm
.LEHB7:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE7:
	str	r4, [r7, #12]
	ldr	lr, [r5, #0]
	ldr	r2, [lr, #12]
	cmp	r7, r2
	streq	r4, [lr, #12]
.L596:
	mov	r3, #0
	str	r7, [r6, #4]
	str	r3, [r6, #12]
	str	r3, [r6, #8]
	ldr	r0, [r5, #0]
	add	r1, r0, #4
	mov	r0, r6
.LEHB8:
	bl	_ZN4_STL10_Rb_globalIbE10_RebalanceEPNS_18_Rb_tree_node_baseERS3_
.LEHE8:
	ldr	r1, [r5, #4]
	add	ip, r1, #1
	str	ip, [r5, #4]
	mov	r0, r9
	str	r6, [r9, #0]
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L613:
	cmp	r2, #0
	beq	.L615
.L569:
	mov	r0, #36
	bl	malloc
	subs	sl, r0, #0
	beq	.L616
.L571:
	add	fp, sl, #16
	mov	r0, fp
	mov	r1, r8
	mov	r6, sl
.LEHB9:
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
.LEHE9:
	mov	r2, #0
	str	r2, [sl, #28]
	ldr	r4, [r8, #12]
	ldr	r0, [r8, #16]
	cmp	r4, #0
	str	r0, [sl, #32]
	str	r4, [sl, #28]
	beq	.L572
	ldr	r8, .L621
	ldr	r3, [r8, #0]
	ldr	ip, [r3, #4]
	cmp	ip, #0
	beq	.L573
	mov	r0, r3
	mov	r2, ip
	b	.L577
.L617:
	mov	r0, r2
	mov	r2, r1
.L577:
	ldr	r1, [r2, #16]
	cmp	r4, r1
	ldrhi	r1, [r2, #12]
	ldrls	r1, [r2, #8]
	movhi	r2, r0
	cmp	r1, #0
	bne	.L617
	cmp	r3, r2
	beq	.L573
	ldr	lr, [r2, #16]
	cmp	r4, lr
	bcc	.L573
.L578:
	cmp	r2, r3
	mov	r1, r2
	beq	.L611
.L580:
	ldr	ip, [r1, #20]
	add	lr, ip, #1
	str	lr, [r1, #20]
.L594:
.L572:
	str	sl, [r7, #8]
	ldr	r3, [r5, #0]
	cmp	r3, r7
	beq	.L618
	ldr	r0, [r3, #8]
	cmp	r7, r0
	streq	sl, [r3, #8]
	b	.L596
.L619:
	ldr	r1, [ip, #16]
	cmp	r4, r1
	movls	r3, ip
	ldrhi	ip, [ip, #12]
	ldrls	ip, [ip, #8]
.L611:
	cmp	ip, #0
	bne	.L619
	cmp	r2, r3
	beq	.L586
	ldr	ip, [r3, #16]
	cmp	r4, ip
	mov	r2, r3
	bcc	.L586
.L587:
	mov	r0, #0
	str	r0, [r2, #20]
	ldr	r0, [r8, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	beq	.L588
	mov	r1, r0
	b	.L592
.L620:
	mov	r1, r3
	mov	r3, r2
.L592:
	ldr	r2, [r3, #16]
	cmp	r4, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L620
	cmp	r0, r3
	beq	.L588
	ldr	r2, [r3, #16]
	cmp	r4, r2
	movcs	r0, r3
.L588:
	mov	r1, r0
	b	.L580
.L573:
	mov	r2, r3
	b	.L578
.L618:
	str	sl, [r7, #4]
	ldr	r1, [r5, #0]
	str	sl, [r1, #12]
	b	.L596
.L616:
	mov	r0, #36
.LEHB10:
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
	mov	sl, r0
	b	.L571
.L614:
	mov	r0, #36
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
.LEHE10:
	mov	r4, r0
	b	.L603
.L586:
	str	r3, [sp, #8]
	add	r0, sp, #12
	mov	r3, #0
	add	r1, sp, #8
	mov	r2, sp
	str	r3, [sp, #4]
	str	r4, [sp, #0]
.LEHB11:
	bl	T.938
.LEHE11:
	ldr	r2, [sp, #12]
	b	.L587
.L615:
	add	r0, r5, #8
	mov	r1, r8
	add	r2, r7, #16
	bl	_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_
	cmp	r0, #0
	beq	.L570
	b	.L569
.L605:
.L598:
	mov	r4, r0
	mov	r0, sl
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
.L599:
	mov	r0, r4
.LEHB12:
	bl	__cxa_end_cleanup
.LEHE12:
.L606:
.L612:
	mov	r4, r0
	b	.L599
.L608:
	b	.L612
.L607:
.L593:
	mov	r4, r0
	mov	r0, fp
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	b	.L599
.L622:
	.align	2
.L621:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA4681:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4681-.LLSDACSB4681
.LLSDACSB4681:
	.uleb128 .LEHB6-.LFB4681
	.uleb128 .LEHE6-.LEHB6
	.uleb128 .L606-.LFB4681
	.uleb128 0x0
	.uleb128 .LEHB7-.LFB4681
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L605-.LFB4681
	.uleb128 0x0
	.uleb128 .LEHB8-.LFB4681
	.uleb128 .LEHE8-.LEHB8
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB9-.LFB4681
	.uleb128 .LEHE9-.LEHB9
	.uleb128 .L608-.LFB4681
	.uleb128 0x0
	.uleb128 .LEHB10-.LFB4681
	.uleb128 .LEHE10-.LEHB10
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB11-.LFB4681
	.uleb128 .LEHE11-.LEHB11
	.uleb128 .L607-.LFB4681
	.uleb128 0x0
	.uleb128 .LEHB12-.LFB4681
	.uleb128 .LEHE12-.LEHB12
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE4681:
	.fnend
	.size	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_, .-_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_
	.section	.text._ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_,"axG",%progbits,_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_,comdat
	.align	2
	.weak	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_
	.hidden	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_
	.type	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_, %function
_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_:
	.fnstart
.LFB4678:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	r9, [r1, #0]
	ldr	r6, [r9, #4]
	cmp	r6, #0
	.pad #20
	sub	sp, sp, #20
	mov	fp, r1
	mov	r5, r0
	mov	r4, r2
	beq	.L648
	ldr	sl, [r2, #0]
	ldr	r8, [r2, #4]
	rsb	r8, sl, r8
	b	.L624
.L649:
	cmp	r8, r7
	blt	.L628
.L627:
	ldr	r3, [r6, #12]
	cmp	r3, #0
	mov	r2, #0
	beq	.L630
.L650:
	mov	r6, r3
.L624:
	add	r1, r6, #16
	ldmia	r1, {r1, r7}	@ phole ldm
	rsb	r7, r1, r7
	cmp	r7, r8
	movlt	r2, r7
	movge	r2, r8
	mov	r0, sl
	bl	memcmp
	cmp	r0, #0
	beq	.L649
	bge	.L627
.L628:
	ldr	r3, [r6, #8]
	cmp	r3, #0
	mov	r2, #1
	bne	.L650
.L630:
	cmp	r2, #0
	moveq	r7, r6
	beq	.L633
.L632:
	ldr	r3, [r9, #8]
	cmp	r6, r3
	beq	.L651
	mov	r0, r6
	bl	_ZN4_STL10_Rb_globalIbE12_M_decrementEPNS_18_Rb_tree_node_baseE
	mov	r7, r0
.L633:
	ldr	r0, [r7, #16]
	ldmia	r4, {r1, r2}	@ phole ldm
	ldr	sl, [r7, #20]
	rsb	r8, r1, r2
	rsb	sl, r0, sl
	cmp	r8, sl
	movlt	r2, r8
	movge	r2, sl
	bl	memcmp
	cmp	r0, #0
	bne	.L639
	cmp	sl, r8
	blt	.L641
.L640:
	mov	r1, #0
	str	r7, [r5, #0]
	strb	r1, [r5, #4]
.L623:
	mov	r0, r5
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L639:
	bge	.L640
.L641:
	mov	lr, #0
	mov	r3, r6
	add	r0, sp, #8
	mov	r1, fp
	mov	r2, lr
	stmia	sp, {r4, lr}	@ phole stm
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_
	ldr	r0, [sp, #8]
	mov	r3, #1
	str	r0, [r5, #0]
	strb	r3, [r5, #4]
	b	.L623
.L651:
	mov	r1, fp
	add	r0, sp, #12
	mov	ip, #0
	mov	r2, r6
	mov	r3, r6
	stmia	sp, {r4, ip}	@ phole stm
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_
	ldr	r1, [sp, #12]
	mov	r0, #1
	str	r1, [r5, #0]
	strb	r0, [r5, #4]
	b	.L623
.L648:
	mov	r6, r9
	b	.L632
	.fnend
	.size	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_, .-_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_
	.section	.text._ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueENS_17_Rb_tree_iteratorISC_NS_16_Nonconst_traitsISC_EEEERKSC_,"axG",%progbits,_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueENS_17_Rb_tree_iteratorISC_NS_16_Nonconst_traitsISC_EEEERKSC_,comdat
	.align	2
	.weak	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueENS_17_Rb_tree_iteratorISC_NS_16_Nonconst_traitsISC_EEEERKSC_
	.hidden	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueENS_17_Rb_tree_iteratorISC_NS_16_Nonconst_traitsISC_EEEERKSC_
	.type	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueENS_17_Rb_tree_iteratorISC_NS_16_Nonconst_traitsISC_EEEERKSC_, %function
_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueENS_17_Rb_tree_iteratorISC_NS_16_Nonconst_traitsISC_EEEERKSC_:
	.fnstart
.LFB4470:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	ip, [r1, #0]
	ldr	r5, [r2, #0]
	mov	r9, r2
	ldr	r2, [ip, #8]
	cmp	r5, r2
	.pad #52
	sub	sp, sp, #52
	mov	r6, r1
	mov	r7, r0
	mov	r4, r3
	beq	.L687
	cmp	r5, ip
	beq	.L688
	mov	r0, r5
	bl	_ZN4_STL10_Rb_globalIbE12_M_decrementEPNS_18_Rb_tree_node_baseE
	str	r0, [sp, #12]
	ldr	r5, [r9, #0]
	ldr	r8, [r4, #0]
	ldr	r1, [r5, #16]
	ldr	fp, [r4, #4]
	ldr	sl, [r5, #20]
	rsb	fp, r8, fp
	rsb	sl, r1, sl
	cmp	sl, fp
	movlt	r2, sl
	movge	r2, fp
	mov	r0, r8
	bl	memcmp
	cmp	r0, #0
	bne	.L667
	cmp	fp, sl
	blt	.L669
.L668:
	mov	r0, r5
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	mov	sl, #0
	cmp	sl, #0
	mov	r8, r0
	beq	.L689
.L675:
	mov	r1, r6
	mov	r2, r4
	add	r0, sp, #16
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_
	ldr	r1, [sp, #16]
	str	r1, [r7, #0]
.L652:
	mov	r0, r7
	add	sp, sp, #52
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L667:
	bge	.L668
.L669:
	ldr	r3, [sp, #12]
	add	r0, r3, #16
	ldmia	r0, {r0, lr}	@ phole ldm
	rsb	sl, r0, lr
	cmp	fp, sl
	movlt	r2, fp
	movge	r2, sl
	mov	r1, r8
	bl	memcmp
	cmp	r0, #0
	bne	.L670
	cmp	fp, sl
	bgt	.L672
.L671:
	mov	r0, r5
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	mov	sl, #1
	cmp	sl, #0
	mov	r8, r0
	bne	.L675
.L689:
	ldr	r5, [r9, #0]
	ldr	r1, [r4, #0]
	ldr	r0, [r5, #16]
	ldr	ip, [r4, #4]
	ldr	r9, [r5, #20]
	rsb	sl, r1, ip
	rsb	r9, r0, r9
	cmp	sl, r9
	movlt	r2, sl
	movge	r2, r9
	bl	memcmp
	cmp	r0, #0
	bne	.L676
	cmp	r9, sl
	blt	.L678
.L677:
	str	r5, [r7, #0]
	b	.L652
.L670:
	bge	.L671
.L672:
	ldr	ip, [sp, #12]
	ldr	r2, [ip, #12]
	cmp	r2, #0
	beq	.L686
.L673:
	mov	r2, r5
.L684:
	mov	lr, #0
	mov	r1, r6
	mov	r0, r7
	mov	r3, r2
	stmia	sp, {r4, lr}	@ phole stm
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_
	b	.L652
.L686:
	mov	r1, r6
.L685:
	mov	r3, ip
	mov	r0, r7
	stmia	sp, {r4, ip}	@ phole stm
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_
	b	.L652
.L687:
	ldr	r3, [r1, #4]
	cmp	r3, #0
	bne	.L654
	add	r0, sp, #40
	mov	r2, r4
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_
	ldr	r0, [sp, #40]
	str	r0, [r7, #0]
	b	.L652
.L676:
	bge	.L677
.L678:
	ldr	r0, [r6, #0]
	cmp	r0, r8
	beq	.L679
	add	r0, r6, #8
	mov	r1, r4
	add	r2, r8, #16
	bl	_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_
	cmp	r0, #0
	beq	.L675
.L679:
	ldr	r2, [r5, #12]
	cmp	r2, #0
	movne	r2, r8
	bne	.L684
	mov	r1, r6
	mov	r3, r5
	mov	r0, r7
	stmia	sp, {r4, r5}	@ phole stm
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_
	b	.L652
.L654:
	add	r8, r1, #8
	add	sl, r5, #16
	mov	r0, r8
	mov	r1, r4
	mov	r2, sl
	bl	_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_
	subs	fp, r0, #0
	bne	.L673
	mov	r1, sl
	mov	r0, r8
	mov	r2, r4
	bl	_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_
	cmp	r0, #0
	beq	.L677
	mov	r0, r5
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r1, [r6, #0]
	cmp	r1, r0
	mov	r5, r0
	beq	.L690
	mov	r0, r8
	mov	r1, r4
	add	r2, r5, #16
	bl	_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_
	cmp	r0, #0
	beq	.L661
	ldr	ip, [r9, #0]
	ldr	r2, [ip, #12]
	cmp	r2, #0
	beq	.L686
	mov	r2, r5
	mov	r1, r6
	mov	r0, r7
	mov	r3, r5
	stmia	sp, {r4, fp}	@ phole stm
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_
	b	.L652
.L688:
	ldr	r8, [r5, #12]
	add	r0, r1, #8
	mov	r2, r3
	add	r1, r8, #16
	bl	_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_
	cmp	r0, #0
	mov	r1, r6
	bne	.L691
	mov	r2, r4
	add	r0, sp, #24
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_
	ldr	r2, [sp, #24]
	str	r2, [r7, #0]
	b	.L652
.L691:
	mov	r3, r8
	mov	r0, r7
	mov	r2, #0
	stmia	sp, {r4, r5}	@ phole stm
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_
	b	.L652
.L661:
	mov	r1, r6
	mov	r2, r4
	add	r0, sp, #32
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_
	ldr	ip, [sp, #32]
	str	ip, [r7, #0]
	b	.L652
.L690:
	ldr	ip, [r9, #0]
	mov	r1, r6
	mov	r2, fp
	b	.L685
	.fnend
	.size	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueENS_17_Rb_tree_iteratorISC_NS_16_Nonconst_traitsISC_EEEERKSC_, .-_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueENS_17_Rb_tree_iteratorISC_NS_16_Nonconst_traitsISC_EEEERKSC_
	.section	.text._ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE,"axG",%progbits,_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE,comdat
	.align	2
	.weak	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
	.hidden	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
	.type	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE, %function
_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE:
	.fnstart
.LFB4464:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 64
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	subs	r8, r1, #0
	.pad #68
	sub	sp, sp, #68
	str	r0, [sp, #12]
	beq	.L811
	ldr	r7, .L847
.L815:
	ldr	r0, [sp, #12]
	ldr	r1, [r8, #12]
.LEHB13:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE13:
	ldr	r2, [r8, #8]
	ldr	ip, [r8, #28]
	str	r2, [sp, #4]
	ldr	r3, [r8, #32]
	cmp	ip, #0
	str	r3, [sp, #8]
	beq	.L694
	ldr	r0, [r7, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L698
	mov	r1, r0
	b	.L699
.L826:
	mov	r1, r3
	mov	r3, r2
.L699:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L826
.L698:
	cmp	r3, r0
	beq	.L701
	ldr	r0, [r3, #16]
	cmp	ip, r0
	mov	r2, r3
	bcs	.L702
.L701:
	str	ip, [sp, #32]
	add	r0, sp, #60
	mov	ip, #0
	add	r1, sp, #56
	add	r2, sp, #32
	str	r3, [sp, #56]
	str	ip, [sp, #36]
.LEHB14:
	bl	T.938
	ldr	r2, [sp, #60]
.L702:
	ldr	lr, [r2, #20]
	sub	r1, lr, #1
	cmp	r1, #0
	str	r1, [r2, #20]
	beq	.L827
.L694:
	ldr	r0, [r8, #16]
	ldr	r3, [sp, #8]
	mov	ip, #0
	cmp	r0, #0
	str	r3, [r8, #32]
	str	ip, [r8, #28]
	blne	free
.L806:
	cmp	r8, #0
	movne	r0, r8
	blne	free
.L808:
	ldr	r8, [sp, #4]
	cmp	r8, #0
	bne	.L815
.L811:
	add	sp, sp, #68
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L827:
	ldr	r2, [r7, #0]
	ldr	r4, [r2, #4]
	cmp	r4, #0
	ldr	r1, [r8, #28]
	beq	.L705
	mov	r0, r2
	mov	r5, r4
	b	.L709
.L828:
	mov	r0, r5
	mov	r5, r3
.L709:
	ldr	r3, [r5, #16]
	cmp	r1, r3
	ldrhi	r3, [r5, #12]
	ldrls	r3, [r5, #8]
	movhi	r5, r0
	cmp	r3, #0
	bne	.L828
	mov	r0, r2
	b	.L713
.L829:
	mov	r0, r4
	mov	r4, r3
.L713:
	ldr	r3, [r4, #16]
	cmp	r1, r3
	ldrcs	r3, [r4, #12]
	ldrcc	r3, [r4, #8]
	movcs	r4, r0
	cmp	r3, #0
	bne	.L829
	cmp	r5, r4
	mov	r6, r5
	moveq	r5, r4
	beq	.L716
	mov	r0, r5
.L717:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r4, r0
	bne	.L717
	ldr	r2, [r7, #0]
	mov	r6, r4
.L716:
	ldr	r3, [r2, #8]
	cmp	r5, r3
	beq	.L830
.L719:
	cmp	r5, r6
	bne	.L820
	b	.L722
.L725:
	mov	r5, r4
.L820:
	mov	r0, r5
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r2, [r7, #0]
	mov	r4, r0
	add	r3, r2, #12
	add	r1, r2, #4
	mov	r0, r5
	add	r2, r2, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
.LEHE14:
	cmp	r0, #0
	blne	free
.L724:
	ldr	r0, [r7, #4]
	cmp	r4, r6
	sub	r3, r0, #1
	str	r3, [r7, #4]
	bne	.L725
.L722:
	ldr	ip, [r8, #32]
	cmp	ip, #1
	beq	.L831
	ldr	r3, [r8, #28]
	cmp	r3, #0
	beq	.L694
	ldr	r0, [r3, #-4]
	add	r6, r0, r0, asl #2
	add	r6, r3, r6, asl #2
	cmp	r6, r3
	beq	.L767
.L834:
	ldr	ip, [r6, #-8]
	cmp	ip, #0
	sub	r9, r6, #20
	sub	sl, r6, #8
	ldr	fp, [r6, #-4]
	beq	.L768
	ldr	r0, [r7, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L772
	mov	r1, r0
	b	.L773
.L832:
	mov	r1, r3
	mov	r3, r2
.L773:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L832
.L772:
	cmp	r3, r0
	beq	.L775
	ldr	r2, [r3, #16]
	cmp	ip, r2
	mov	r2, r3
	bcs	.L776
.L775:
	str	r3, [sp, #40]
	add	r0, sp, #44
	mov	r3, #0
	add	r1, sp, #40
	add	r2, sp, #16
	str	ip, [sp, #16]
	str	r3, [sp, #20]
.LEHB15:
	bl	T.938
	ldr	r2, [sp, #44]
.L776:
	ldr	r1, [r2, #20]
	sub	ip, r1, #1
	cmp	ip, #0
	str	ip, [r2, #20]
	beq	.L833
.L768:
	ldr	r0, [r6, #-20]
	mov	r2, #0
	cmp	r0, #0
	stmdb	r6, {r2, fp}	@ phole stm
	blne	free
.L803:
	ldr	r3, [r8, #28]
	mov	r6, r9
	cmp	r6, r3
	bne	.L834
.L767:
	sub	r0, r6, #8
	bl	_ZdaPv
	b	.L694
.L833:
	ldr	r2, [r7, #0]
	ldr	r4, [r2, #4]
	cmp	r4, #0
	ldr	r1, [r6, #-8]
	beq	.L777
	mov	r0, r2
	mov	r5, r4
	b	.L781
.L835:
	mov	r0, r5
	mov	r5, r3
.L781:
	ldr	r3, [r5, #16]
	cmp	r1, r3
	ldrhi	r3, [r5, #12]
	ldrls	r3, [r5, #8]
	movhi	r5, r0
	cmp	r3, #0
	bne	.L835
	mov	r0, r2
	b	.L785
.L836:
	mov	r0, r4
	mov	r4, r3
.L785:
	ldr	r3, [r4, #16]
	cmp	r1, r3
	ldrcs	r3, [r4, #12]
	ldrcc	r3, [r4, #8]
	movcs	r4, r0
	cmp	r3, #0
	bne	.L836
	cmp	r5, r4
	str	r5, [sp, #0]
	moveq	r5, r4
	beq	.L788
	mov	r0, r5
.L789:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r4, r0
	bne	.L789
	ldr	r2, [r7, #0]
	str	r4, [sp, #0]
.L788:
	ldr	lr, [r2, #8]
	cmp	r5, lr
	beq	.L837
.L791:
	ldr	r3, [sp, #0]
	cmp	r5, r3
	bne	.L822
	b	.L794
.L797:
	mov	r5, r4
.L822:
	mov	r0, r5
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	ip, [r7, #0]
	mov	r4, r0
	add	r3, ip, #12
	mov	r0, r5
	add	r1, ip, #4
	add	r2, ip, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L796:
	ldr	r1, [r7, #4]
	ldr	lr, [sp, #0]
	sub	r3, r1, #1
	cmp	r4, lr
	str	r3, [r7, #4]
	bne	.L797
.L794:
	ldr	r0, [r6, #-4]
	cmp	r0, #1
	ldr	r3, [r6, #-8]
	beq	.L838
	cmp	r3, #0
	beq	.L768
	ldr	ip, [r3, #-4]
	rsb	r4, ip, ip, asl #3
	add	r4, r3, r4, asl #2
	b	.L800
.L839:
	ldr	r1, [r4, #-28]!
	mov	r0, r4
	ldr	ip, [r1, #4]
	mov	lr, pc
	bx	ip
.L800:
	ldr	r3, [sl, #0]
	cmp	r3, r4
	bne	.L839
	ldr	lr, [r6, #-8]
	sub	r0, lr, #8
	bl	_ZdaPv
	b	.L768
.L838:
	cmp	r3, #0
	beq	.L768
	mov	r0, r3
	ldr	r2, [r3, #0]
	ldr	ip, [r2, #8]
	mov	lr, pc
	bx	ip
	b	.L768
.L837:
	ldr	r0, [sp, #0]
	cmp	r0, r2
	bne	.L791
	ldr	r2, [r7, #4]
	cmp	r2, #0
	beq	.L794
	ldr	ip, [sp, #0]
	ldr	r0, [ip, #4]
	bl	T.932
.LEHE15:
	ldr	r2, [r7, #0]
	str	r2, [r2, #8]
	ldr	r0, [r7, #0]
	mov	r1, #0
	str	r1, [r0, #4]
	ldr	lr, [r7, #0]
	str	lr, [lr, #12]
	str	r1, [r7, #4]
	b	.L794
.L831:
	ldr	r5, [r8, #28]
	cmp	r5, #0
	beq	.L694
	ldr	ip, [r5, #12]
	cmp	ip, #0
	add	r9, r5, #12
	ldr	fp, [r5, #16]
	beq	.L727
	ldr	r0, [r7, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L731
	mov	r1, r0
	b	.L732
.L840:
	mov	r1, r3
	mov	r3, r2
.L732:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L840
.L731:
	cmp	r3, r0
	beq	.L734
	ldr	r1, [r3, #16]
	cmp	ip, r1
	mov	r2, r3
	bcs	.L735
.L734:
	mov	lr, #0
	add	r0, sp, #52
	add	r1, sp, #48
	add	r2, sp, #24
	str	ip, [sp, #24]
	str	r3, [sp, #48]
	str	lr, [sp, #28]
.LEHB16:
	bl	T.938
.LEHE16:
	ldr	r2, [sp, #52]
.L735:
	ldr	r0, [r2, #20]
	sub	r3, r0, #1
	cmp	r3, #0
	str	r3, [r2, #20]
	beq	.L841
.L727:
	ldr	r0, [r5, #0]
	mov	lr, #0
	cmp	r0, #0
	str	fp, [r5, #16]
	str	lr, [r5, #12]
	blne	free
.L764:
	mov	r0, r5
	bl	_ZdlPv
	b	.L694
.L777:
	str	r2, [sp, #0]
	mov	r5, r2
	b	.L788
.L830:
	cmp	r6, r2
	bne	.L719
	ldr	r2, [r7, #4]
	cmp	r2, #0
	beq	.L722
	ldr	r0, [r6, #4]
.LEHB17:
	bl	T.932
.LEHE17:
	ldr	lr, [r7, #0]
	str	lr, [lr, #8]
	ldr	r1, [r7, #0]
	mov	r0, #0
	str	r0, [r1, #4]
	ldr	ip, [r7, #0]
	str	ip, [ip, #12]
	str	r0, [r7, #4]
	b	.L722
.L705:
	mov	r6, r2
	mov	r5, r2
	b	.L716
.L841:
	ldr	r2, [r7, #0]
	ldr	r4, [r2, #4]
	cmp	r4, #0
	ldr	r0, [r5, #12]
	beq	.L738
	mov	ip, r2
	mov	r3, r4
	b	.L742
.L842:
	mov	ip, r3
	mov	r3, r1
.L742:
	ldr	r1, [r3, #16]
	cmp	r0, r1
	ldrhi	r1, [r3, #12]
	ldrls	r1, [r3, #8]
	movhi	r3, ip
	cmp	r1, #0
	bne	.L842
	mov	ip, r2
	b	.L746
.L843:
	mov	ip, r4
	mov	r4, r1
.L746:
	ldr	r1, [r4, #16]
	cmp	r0, r1
	ldrcs	r1, [r4, #12]
	ldrcc	r1, [r4, #8]
	movcs	r4, ip
	cmp	r1, #0
	bne	.L843
	cmp	r3, r4
	mov	r6, r3
	mov	sl, r4
	beq	.L749
	mov	r0, r3
.L750:
.LEHB18:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r4, r0
	bne	.L750
	ldr	r2, [r7, #0]
.L749:
	ldr	ip, [r2, #8]
	cmp	r6, ip
	beq	.L844
.L752:
	cmp	r6, sl
	bne	.L821
	b	.L755
.L758:
	mov	r6, r4
.L821:
	mov	r0, r6
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	ip, [r7, #0]
	mov	r4, r0
	add	r3, ip, #12
	mov	r0, r6
	add	r1, ip, #4
	add	r2, ip, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L757:
	ldr	r1, [r7, #4]
	cmp	r4, sl
	sub	r2, r1, #1
	str	r2, [r7, #4]
	bne	.L758
.L755:
	ldr	lr, [r5, #16]
	cmp	lr, #1
	ldr	r3, [r5, #12]
	beq	.L845
	cmp	r3, #0
	beq	.L727
	ldr	r0, [r3, #-4]
	mov	r4, #28
	mla	r4, r0, r4, r3
	b	.L761
.L846:
	ldr	r2, [r4, #-28]!
	mov	r0, r4
	ldr	ip, [r2, #4]
	mov	lr, pc
	bx	ip
.L761:
	ldr	ip, [r9, #0]
	cmp	ip, r4
	bne	.L846
	ldr	r1, [r5, #12]
	sub	r0, r1, #8
	bl	_ZdaPv
	b	.L727
.L844:
	cmp	sl, r2
	bne	.L752
	ldr	r2, [r7, #4]
	cmp	r2, #0
	beq	.L755
	ldr	r0, [sl, #4]
	bl	T.932
	ldr	r0, [r7, #0]
	str	r0, [r0, #8]
	ldr	r3, [r7, #0]
	mov	r1, #0
	str	r1, [r3, #4]
	ldr	lr, [r7, #0]
	str	lr, [lr, #12]
	str	r1, [r7, #4]
	b	.L755
.L845:
	cmp	r3, #0
	beq	.L727
	mov	r0, r3
	ldr	r3, [r3, #0]
	ldr	ip, [r3, #8]
	mov	lr, pc
	bx	ip
.LEHE18:
	b	.L727
.L814:
	mov	r4, r0
.L809:
	add	r0, r8, #16
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r4
.LEHB19:
	bl	__cxa_end_cleanup
.LEHE19:
.L812:
.L804:
	mov	r4, r0
	mov	r0, r9
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	b	.L809
.L813:
.L765:
	mov	r4, r0
	mov	r0, r5
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	b	.L809
.L738:
	mov	sl, r2
	mov	r6, r2
	b	.L749
.L848:
	.align	2
.L847:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA4464:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4464-.LLSDACSB4464
.LLSDACSB4464:
	.uleb128 .LEHB13-.LFB4464
	.uleb128 .LEHE13-.LEHB13
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB14-.LFB4464
	.uleb128 .LEHE14-.LEHB14
	.uleb128 .L814-.LFB4464
	.uleb128 0x0
	.uleb128 .LEHB15-.LFB4464
	.uleb128 .LEHE15-.LEHB15
	.uleb128 .L812-.LFB4464
	.uleb128 0x0
	.uleb128 .LEHB16-.LFB4464
	.uleb128 .LEHE16-.LEHB16
	.uleb128 .L813-.LFB4464
	.uleb128 0x0
	.uleb128 .LEHB17-.LFB4464
	.uleb128 .LEHE17-.LEHB17
	.uleb128 .L814-.LFB4464
	.uleb128 0x0
	.uleb128 .LEHB18-.LFB4464
	.uleb128 .LEHE18-.LEHB18
	.uleb128 .L813-.LFB4464
	.uleb128 0x0
	.uleb128 .LEHB19-.LFB4464
	.uleb128 .LEHE19-.LEHB19
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE4464:
	.fnend
	.size	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE, .-_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
	.section	.text._ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE,"axG",%progbits,_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE,comdat
	.align	2
	.weak	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
	.hidden	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
	.type	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE, %function
_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE:
	.fnstart
.LFB4452:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	r9, .L972
	.pad #36
	sub	sp, sp, #36
	mov	r6, r0
	mov	sl, r1
.L850:
	cmp	sl, #0
	beq	.L959
	ldr	r2, [sl, #12]
	str	sl, [sp, #12]
	str	r2, [sp, #8]
.L851:
	ldr	r0, [sp, #8]
	cmp	r0, #0
	beq	.L960
	ldr	r0, [sp, #8]
	ldr	r3, [r0, #12]
	str	r3, [sp, #0]
.L852:
	ldr	r2, [sp, #0]
	cmp	r2, #0
	beq	.L961
	ldr	r4, [sp, #0]
	ldr	r1, [r4, #12]
	str	r1, [sp, #4]
.L853:
	ldr	r2, [sp, #4]
	cmp	r2, #0
	beq	.L962
	ldr	fp, [sp, #4]
	ldr	sl, [fp, #12]
.L854:
	cmp	sl, #0
	beq	.L963
	ldr	fp, [sl, #12]
	cmp	fp, #0
	beq	.L855
.L953:
	ldr	r8, [fp, #12]
	cmp	r8, #0
	beq	.L856
.L952:
	ldr	r7, [r8, #12]
	cmp	r7, #0
	beq	.L857
.L951:
	ldr	r4, [r7, #12]
	cmp	r4, #0
	bne	.L950
	b	.L858
.L964:
	mov	r4, r5
.L950:
	mov	r0, r6
	ldr	r1, [r4, #12]
.LEHB20:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE20:
	add	r0, r4, #28
	mov	r1, #0
	ldr	r2, [r4, #32]
	ldr	r5, [r4, #8]
.LEHB21:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE21:
	ldr	r0, [r4, #16]
	cmp	r0, #0
	blne	free
.L861:
	cmp	r4, #0
	movne	r0, r4
	blne	free
.L863:
	cmp	r5, #0
	bne	.L964
.L858:
	add	r0, r7, #28
	mov	r1, #0
	ldr	r2, [r7, #32]
	ldr	r4, [r7, #8]
.LEHB22:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE22:
	ldr	r0, [r7, #16]
	cmp	r0, #0
	blne	free
.L867:
	cmp	r7, #0
	movne	r0, r7
	blne	free
.L869:
	cmp	r4, #0
	movne	r7, r4
	bne	.L951
.L857:
	add	r0, r8, #28
	mov	r1, #0
	ldr	r2, [r8, #32]
	ldr	r4, [r8, #8]
.LEHB23:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE23:
	ldr	r0, [r8, #16]
	cmp	r0, #0
	blne	free
.L873:
	cmp	r8, #0
	movne	r0, r8
	blne	free
.L875:
	cmp	r4, #0
	movne	r8, r4
	bne	.L952
.L856:
	add	r0, fp, #28
	mov	r1, #0
	ldr	r2, [fp, #32]
	ldr	r4, [fp, #8]
.LEHB24:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE24:
	ldr	r0, [fp, #16]
	cmp	r0, #0
	blne	free
.L879:
	cmp	fp, #0
	movne	r0, fp
	blne	free
.L881:
	cmp	r4, #0
	movne	fp, r4
	bne	.L953
.L855:
	add	r0, sl, #28
	mov	r1, #0
	ldr	r2, [sl, #32]
	ldr	r4, [sl, #8]
.LEHB25:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE25:
	ldr	r0, [sl, #16]
	cmp	r0, #0
.L886:
	blne	free
.L885:
	mov	r0, sl
	bl	free
	mov	sl, r4
	b	.L854
.L944:
.L958:
.L937:
	mov	r9, r0
	add	r0, sl, #16
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r9
.LEHB26:
	bl	__cxa_end_cleanup
.LEHE26:
.L963:
	ldr	ip, [sp, #4]
	mov	r1, sl
	ldr	sl, [sp, #4]
	add	r0, ip, #28
	ldr	r2, [ip, #32]
	ldr	r4, [sl, #8]
.LEHB27:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE27:
	ldr	lr, [sp, #4]
	ldr	r0, [lr, #16]
	cmp	r0, #0
	blne	free
.L890:
	ldr	r0, [sp, #4]
	bl	free
	str	r4, [sp, #4]
	b	.L853
.L943:
.L891:
	ldr	r2, [sp, #4]
	mov	r9, r0
	add	r0, r2, #16
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r9
.LEHB28:
	bl	__cxa_end_cleanup
.L945:
.L880:
	mov	r8, r0
	add	r0, fp, #16
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r8
	bl	__cxa_end_cleanup
.L946:
.L874:
	mov	r7, r0
	add	r0, r8, #16
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r7
	bl	__cxa_end_cleanup
.L947:
.L868:
	mov	r6, r0
	add	r0, r7, #16
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r6
	bl	__cxa_end_cleanup
.L948:
.L862:
	mov	r5, r0
	add	r0, r4, #16
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r5
	bl	__cxa_end_cleanup
.LEHE28:
.L962:
	ldr	r3, [sp, #0]
	mov	r1, r2
	add	r0, r3, #28
	ldr	r2, [r3, #32]
	ldr	r4, [r3, #8]
.LEHB29:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE29:
	ldr	r1, [sp, #0]
	ldr	r0, [r1, #16]
	cmp	r0, #0
	blne	free
.L895:
	ldr	r0, [sp, #0]
	bl	free
	str	r4, [sp, #0]
	b	.L852
.L942:
.L896:
	ldr	fp, [sp, #0]
	mov	r4, r0
	add	r0, fp, #16
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r4
.LEHB30:
	bl	__cxa_end_cleanup
.LEHE30:
.L961:
	ldr	sl, [sp, #8]
	mov	r1, r2
	add	r0, sl, #28
	ldr	r2, [sl, #32]
	ldr	r4, [sl, #8]
.LEHB31:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE31:
	ldr	ip, [sp, #8]
	ldr	r0, [ip, #16]
	cmp	r0, #0
	blne	free
.L900:
	ldr	r0, [sp, #8]
	bl	free
	str	r4, [sp, #8]
	b	.L851
.L941:
.L901:
	ldr	r6, [sp, #8]
	mov	r5, r0
	add	r0, r6, #16
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r5
.LEHB32:
	bl	__cxa_end_cleanup
.LEHE32:
.L960:
	ldr	sl, [sp, #12]
	ldr	r3, [sl, #8]
	ldr	ip, [sl, #28]
	str	r3, [sp, #4]
	ldr	r2, [sl, #32]
	cmp	ip, #0
	ldrne	r1, [r9, #0]
	str	r2, [sp, #0]
	add	fp, sl, #28
	ldrne	r2, [r1, #4]
	movne	r3, r1
	beq	.L903
.L904:
	cmp	r2, #0
	beq	.L965
	ldr	lr, [r2, #16]
	cmp	ip, lr
	ldrhi	lr, [r2, #12]
	ldrls	lr, [r2, #8]
	movhi	r2, r3
	mov	r3, r2
	mov	r2, lr
	b	.L904
.L959:
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L965:
	cmp	r3, r1
	beq	.L910
	ldr	r2, [r3, #16]
	cmp	ip, r2
	mov	r2, r3
	bcs	.L911
.L910:
	str	r3, [sp, #24]
	add	r0, sp, #28
	mov	r3, #0
	add	r1, sp, #24
	add	r2, sp, #16
	str	ip, [sp, #16]
	str	r3, [sp, #20]
.LEHB33:
	bl	T.938
	ldr	r2, [sp, #28]
.L911:
	ldr	ip, [r2, #20]
	sub	r1, ip, #1
	cmp	r1, #0
	str	r1, [r2, #20]
	beq	.L966
.L903:
	ldr	r0, [sl, #16]
	ldr	r3, [sp, #0]
	mov	r1, #0
	cmp	r0, #0
	str	r3, [sl, #32]
	str	r1, [sl, #28]
	blne	free
.L936:
	mov	r0, sl
	bl	free
	ldr	sl, [sp, #4]
	b	.L850
.L966:
	ldr	r4, [r9, #0]
	ldr	r3, [r4, #4]
	ldr	r1, [sl, #28]
	mov	r2, r3
	mov	r5, r4
.L912:
	cmp	r2, #0
	beq	.L967
	ldr	r7, [r2, #16]
	cmp	r1, r7
	ldrhi	r7, [r2, #12]
	ldrls	r7, [r2, #8]
	movhi	r2, r5
	mov	r5, r2
	mov	r2, r7
	b	.L912
.L940:
	b	.L958
.L967:
	mov	r8, r5
.L917:
	cmp	r3, #0
	beq	.L968
	ldr	r0, [r3, #16]
	cmp	r1, r0
	ldrcs	r0, [r3, #12]
	ldrcc	r0, [r3, #8]
	movcs	r3, r4
	mov	r4, r3
	mov	r3, r0
	b	.L917
.L968:
	mov	r7, r5
.L922:
	cmp	r4, r7
	beq	.L969
	mov	r0, r7
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	mov	r7, r0
	b	.L922
.L969:
	ldr	r3, [r9, #0]
	ldr	lr, [r3, #8]
	cmp	r5, lr
	beq	.L970
.L949:
	cmp	r7, r8
	beq	.L928
	mov	r0, r8
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	lr, [r9, #0]
	mov	r4, r0
	add	r3, lr, #12
	mov	r0, r8
	add	r1, lr, #4
	add	r2, lr, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L930:
	ldr	r2, [r9, #4]
	sub	r8, r2, #1
	str	r8, [r9, #4]
	mov	r8, r4
	b	.L949
.L970:
	cmp	r4, r3
	bne	.L949
	ldr	r2, [r9, #4]
	cmp	r2, #0
	beq	.L928
	ldr	r0, [r4, #4]
	bl	T.932
	ldr	r0, [r9, #0]
	str	r0, [r0, #8]
	ldr	ip, [r9, #0]
	mov	r1, #0
	str	r1, [ip, #4]
	ldr	r3, [r9, #0]
	str	r3, [r3, #12]
	str	r1, [r9, #4]
.L928:
	ldr	r1, [sl, #32]
	cmp	r1, #1
	ldr	r3, [sl, #28]
	beq	.L971
	cmp	r3, #0
	beq	.L903
	ldr	ip, [r3, #-4]
	mov	r4, #28
	mla	r4, ip, r4, r3
.L933:
	ldr	r0, [fp, #0]
	cmp	r0, r4
	beq	.L932
	ldr	r2, [r4, #-28]!
	mov	r0, r4
	ldr	ip, [r2, #4]
	mov	lr, pc
	bx	ip
	b	.L933
.L932:
	ldr	lr, [sl, #28]
	sub	r0, lr, #8
	bl	_ZdaPv
	b	.L903
.L971:
	cmp	r3, #0
	beq	.L903
	mov	r0, r3
	ldr	r3, [r3, #0]
	ldr	ip, [r3, #8]
	mov	lr, pc
	bx	ip
.LEHE33:
	b	.L903
.L973:
	.align	2
.L972:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA4452:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4452-.LLSDACSB4452
.LLSDACSB4452:
	.uleb128 .LEHB20-.LFB4452
	.uleb128 .LEHE20-.LEHB20
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB21-.LFB4452
	.uleb128 .LEHE21-.LEHB21
	.uleb128 .L948-.LFB4452
	.uleb128 0x0
	.uleb128 .LEHB22-.LFB4452
	.uleb128 .LEHE22-.LEHB22
	.uleb128 .L947-.LFB4452
	.uleb128 0x0
	.uleb128 .LEHB23-.LFB4452
	.uleb128 .LEHE23-.LEHB23
	.uleb128 .L946-.LFB4452
	.uleb128 0x0
	.uleb128 .LEHB24-.LFB4452
	.uleb128 .LEHE24-.LEHB24
	.uleb128 .L945-.LFB4452
	.uleb128 0x0
	.uleb128 .LEHB25-.LFB4452
	.uleb128 .LEHE25-.LEHB25
	.uleb128 .L944-.LFB4452
	.uleb128 0x0
	.uleb128 .LEHB26-.LFB4452
	.uleb128 .LEHE26-.LEHB26
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB27-.LFB4452
	.uleb128 .LEHE27-.LEHB27
	.uleb128 .L943-.LFB4452
	.uleb128 0x0
	.uleb128 .LEHB28-.LFB4452
	.uleb128 .LEHE28-.LEHB28
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB29-.LFB4452
	.uleb128 .LEHE29-.LEHB29
	.uleb128 .L942-.LFB4452
	.uleb128 0x0
	.uleb128 .LEHB30-.LFB4452
	.uleb128 .LEHE30-.LEHB30
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB31-.LFB4452
	.uleb128 .LEHE31-.LEHB31
	.uleb128 .L941-.LFB4452
	.uleb128 0x0
	.uleb128 .LEHB32-.LFB4452
	.uleb128 .LEHE32-.LEHB32
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB33-.LFB4452
	.uleb128 .LEHE33-.LEHB33
	.uleb128 .L940-.LFB4452
	.uleb128 0x0
.LLSDACSE4452:
	.fnend
	.size	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE, .-_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
	.section	.text._ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev,"axG",%progbits,_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev,comdat
	.align	2
	.weak	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev
	.hidden	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev
	.type	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev, %function
_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev:
	.fnstart
.LFB3184:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	ldr	r3, [r0, #4]
	cmp	r3, #0
	mov	r4, r0
	bne	.L981
.L975:
	ldr	r0, [r4, #0]
	cmp	r0, #0
	blne	free
.L978:
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L981:
	ldr	r2, [r0, #0]
	ldr	r1, [r2, #4]
.LEHB34:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE34:
	ldr	lr, [r4, #0]
	str	lr, [lr, #8]
	ldr	ip, [r4, #0]
	mov	r0, #0
	str	r0, [ip, #4]
	ldr	r1, [r4, #0]
	str	r1, [r1, #12]
	str	r0, [r4, #4]
	b	.L975
.L980:
.L976:
	ldr	r3, [r4, #0]
	cmp	r3, #0
	mov	r4, r0
	movne	r0, r3
	blne	free
.L977:
	mov	r0, r4
.LEHB35:
	bl	__cxa_end_cleanup
.LEHE35:
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA3184:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3184-.LLSDACSB3184
.LLSDACSB3184:
	.uleb128 .LEHB34-.LFB3184
	.uleb128 .LEHE34-.LEHB34
	.uleb128 .L980-.LFB3184
	.uleb128 0x0
	.uleb128 .LEHB35-.LFB3184
	.uleb128 .LEHE35-.LEHB35
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE3184:
	.fnend
	.size	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev, .-_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev
	.section	.text._ZN14QuImageManagerD1Ev,"axG",%progbits,_ZN14QuImageManagerD1Ev,comdat
	.align	2
	.weak	_ZN14QuImageManagerD1Ev
	.hidden	_ZN14QuImageManagerD1Ev
	.type	_ZN14QuImageManagerD1Ev, %function
_ZN14QuImageManagerD1Ev:
	.fnstart
.LFB3192:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	ldr	r3, [r0, #28]
	cmp	r3, #0
	mov	r4, r0
	bne	.L1005
.L983:
	ldr	r0, [r4, #24]
	cmp	r0, #0
	blne	free
.L986:
	ldr	r3, [r4, #16]
	cmp	r3, #0
	bne	.L1006
.L988:
	ldr	r0, [r4, #12]
	cmp	r0, #0
	blne	free
.L992:
	ldr	r3, [r4, #4]
	cmp	r3, #0
	bne	.L1007
.L994:
	ldr	r0, [r4, #0]
	cmp	r0, #0
	blne	free
.L998:
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L1005:
	ldr	r0, [r0, #24]
	ldr	r1, [r0, #4]
	add	r0, r4, #24
.LEHB36:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE36:
	ldr	lr, [r4, #24]
	str	lr, [lr, #8]
	ldr	ip, [r4, #24]
	mov	r1, #0
	str	r1, [ip, #4]
	ldr	r2, [r4, #24]
	str	r2, [r2, #12]
	str	r1, [r4, #28]
	b	.L983
.L1007:
	ldr	r0, [r4, #0]
	ldr	r1, [r0, #4]
	mov	r0, r4
.LEHB37:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE37:
	ldr	lr, [r4, #0]
	str	lr, [lr, #8]
	ldr	ip, [r4, #0]
	mov	r1, #0
	str	r1, [ip, #4]
	ldr	r2, [r4, #0]
	str	r2, [r2, #12]
	str	r1, [r4, #4]
	b	.L994
.L1006:
	ldr	r0, [r4, #12]
	ldr	r1, [r0, #4]
	add	r0, r4, #12
.LEHB38:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE38:
	ldr	lr, [r4, #12]
	str	lr, [lr, #8]
	ldr	ip, [r4, #12]
	mov	r1, #0
	str	r1, [ip, #4]
	ldr	r2, [r4, #12]
	str	r2, [r2, #12]
	str	r1, [r4, #16]
	b	.L988
.L1002:
.L984:
	ldr	r3, [r4, #24]
	cmp	r3, #0
	mov	r5, r0
	movne	r0, r3
	blne	free
.L985:
.L1003:
.L989:
	add	r0, r4, #12
	bl	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev
.L1004:
.L995:
	mov	r0, r4
	bl	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev
	mov	r0, r5
.LEHB39:
	bl	__cxa_end_cleanup
.L1000:
.L996:
	ldr	r3, [r4, #0]
	cmp	r3, #0
	mov	r4, r0
	movne	r0, r3
	blne	free
.L997:
	mov	r0, r4
	bl	__cxa_end_cleanup
.LEHE39:
.L1001:
.L990:
	ldr	r3, [r4, #12]
	cmp	r3, #0
	mov	r5, r0
	beq	.L995
	mov	r0, r3
	bl	free
	b	.L995
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA3192:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3192-.LLSDACSB3192
.LLSDACSB3192:
	.uleb128 .LEHB36-.LFB3192
	.uleb128 .LEHE36-.LEHB36
	.uleb128 .L1002-.LFB3192
	.uleb128 0x0
	.uleb128 .LEHB37-.LFB3192
	.uleb128 .LEHE37-.LEHB37
	.uleb128 .L1000-.LFB3192
	.uleb128 0x0
	.uleb128 .LEHB38-.LFB3192
	.uleb128 .LEHE38-.LEHB38
	.uleb128 .L1001-.LFB3192
	.uleb128 0x0
	.uleb128 .LEHB39-.LFB3192
	.uleb128 .LEHE39-.LEHB39
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE3192:
	.fnend
	.size	_ZN14QuImageManagerD1Ev, .-_ZN14QuImageManagerD1Ev
	.section	.text.T.935,"ax",%progbits
	.align	2
	.type	T.935, %function
T.935:
	.fnstart
.LFB5000:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	.save {r4, r5, r6, r7, r8, sl, lr}
	ldr	r3, [r0, #0]
	cmp	r3, #0
	.pad #20
	sub	sp, sp, #20
	mov	r5, r0
	mov	r8, r1
	beq	.L1009
	ldr	r4, .L1073
	ldr	r1, [r4, #0]
	ldr	r2, [r1, #4]
	cmp	r2, #0
	mov	ip, r1
	beq	.L1011
.L1014:
	ldr	r0, [r2, #16]
	cmp	r3, r0
	movls	ip, r2
	ldrhi	r2, [r2, #12]
	ldrls	r2, [r2, #8]
	cmp	r2, #0
	bne	.L1014
.L1011:
	cmp	ip, r1
	beq	.L1016
	ldr	r2, [ip, #16]
	cmp	r3, r2
	mov	r2, ip
	bcs	.L1017
.L1016:
	mov	r2, sp
	str	r3, [sp, #0]
	add	r0, sp, #12
	mov	r3, #0
	add	r1, sp, #8
	stmib	sp, {r3, ip}	@ phole stm
.LEHB40:
	bl	T.938
	ldr	r2, [sp, #12]
.L1017:
	ldr	ip, [r2, #20]
	sub	r1, ip, #1
	cmp	r1, #0
	str	r1, [r2, #20]
	beq	.L1067
.L1009:
	mov	r2, #0
	stmia	r5, {r2, r8}	@ phole stm
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	bx	lr
.L1067:
	ldr	r1, [r4, #0]
	ldr	r6, [r1, #4]
	cmp	r6, #0
	ldr	r2, [r5, #0]
	moveq	sl, r1
	moveq	r7, r1
	beq	.L1029
	mov	sl, r6
	ldr	r3, [sl, #16]
	cmp	r2, r3
	ldrhi	r3, [sl, #12]
	ldrls	r3, [sl, #8]
	mov	r0, r1
	movhi	sl, r0
	cmp	r3, #0
	beq	.L1021
.L1068:
	mov	r0, sl
	mov	sl, r3
	ldr	r3, [sl, #16]
	cmp	r2, r3
	ldrhi	r3, [sl, #12]
	ldrls	r3, [sl, #8]
	movhi	sl, r0
	cmp	r3, #0
	bne	.L1068
.L1021:
	ldr	r3, [r6, #16]
	cmp	r2, r3
	ldrcs	r3, [r6, #12]
	ldrcc	r3, [r6, #8]
	mov	r0, r1
	movcs	r6, r0
	cmp	r3, #0
	beq	.L1025
.L1069:
	mov	r0, r6
	mov	r6, r3
	ldr	r3, [r6, #16]
	cmp	r2, r3
	ldrcs	r3, [r6, #12]
	ldrcc	r3, [r6, #8]
	movcs	r6, r0
	cmp	r3, #0
	bne	.L1069
.L1025:
	cmp	sl, r6
	mov	r7, sl
	moveq	sl, r6
	beq	.L1029
	mov	r0, sl
.L1030:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r6, r0
	bne	.L1030
	ldr	r1, [r4, #0]
	mov	r7, r6
.L1029:
	ldr	r0, [r1, #8]
	cmp	sl, r0
	beq	.L1070
.L1032:
	cmp	sl, r7
	bne	.L1066
	b	.L1035
.L1038:
	mov	sl, r6
.L1066:
	mov	r0, sl
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r2, [r4, #0]
	mov	r6, r0
	add	r3, r2, #12
	add	r1, r2, #4
	mov	r0, sl
	add	r2, r2, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
.LEHE40:
	cmp	r0, #0
	blne	free
.L1037:
	ldr	r3, [r4, #4]
	cmp	r6, r7
	sub	lr, r3, #1
	str	lr, [r4, #4]
	bne	.L1038
.L1035:
	ldr	r1, [r5, #4]
	cmp	r1, #1
	beq	.L1071
	ldr	r3, [r5, #0]
	cmp	r3, #0
	beq	.L1009
	ldr	r7, [r3, #-4]
	add	r4, r7, r7, asl #3
	add	r4, r3, r4, asl #2
	mov	r7, #0
	b	.L1040
.L1042:
	ldr	r0, [r4, #-12]
	cmp	r0, #0
	blne	free
.L1045:
	ldr	r3, [r4, #-20]
	cmp	r3, #0
	add	r0, r6, #12
	bne	.L1072
.L1047:
	ldr	r0, [r4, #-24]
	cmp	r0, #0
	blne	free
.L1051:
	ldr	lr, [r4, #-32]
	cmp	lr, #0
	bne	.L1052
.L1053:
	ldr	r0, [r4, #-36]
	cmp	r0, #0
	blne	free
.L1057:
	ldr	r3, [r5, #0]
	mov	r4, r6
.L1040:
	cmp	r4, r3
	beq	.L1041
	ldr	r6, [r4, #-8]
	cmp	r6, #0
	sub	r0, r4, #12
	sub	r6, r4, #36
	beq	.L1042
	ldr	ip, [r4, #-12]
	ldr	r1, [ip, #4]
.LEHB41:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE41:
	ldr	lr, [r4, #-12]
	str	lr, [lr, #8]
	ldr	r2, [r4, #-12]
	str	r7, [r2, #4]
	ldr	r0, [r4, #-12]
	str	r0, [r0, #12]
	str	r7, [r4, #-8]
	b	.L1042
.L1052:
	ldr	r3, [r4, #-36]
	mov	r0, r6
	ldr	r1, [r3, #4]
.LEHB42:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE42:
	ldr	r0, [r4, #-36]
	str	r0, [r0, #8]
	ldr	r1, [r4, #-36]
	str	r7, [r1, #4]
	ldr	ip, [r4, #-36]
	str	ip, [ip, #12]
	str	r7, [r4, #-32]
	b	.L1053
.L1072:
	ldr	ip, [r4, #-24]
	ldr	r1, [ip, #4]
.LEHB43:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE43:
	ldr	r2, [r4, #-24]
	str	r2, [r2, #8]
	ldr	r0, [r4, #-24]
	str	r7, [r0, #4]
	ldr	r1, [r4, #-24]
	str	r1, [r1, #12]
	str	r7, [r4, #-20]
	b	.L1047
.L1041:
	sub	r0, r4, #8
	bl	_ZdaPv
	b	.L1009
.L1071:
	ldr	r4, [r5, #0]
	cmp	r4, #0
	beq	.L1009
	mov	r0, r4
.LEHB44:
	bl	_ZN14QuImageManagerD1Ev
	mov	r0, r4
	bl	_ZdlPv
	b	.L1009
.L1070:
	cmp	r7, r1
	bne	.L1032
	ldr	r2, [r4, #4]
	cmp	r2, #0
	ldr	r4, .L1073
	beq	.L1035
	ldr	r0, [r7, #4]
	bl	T.932
.LEHE44:
	ldr	r0, [r4, #0]
	str	r0, [r0, #8]
	ldr	ip, [r4, #0]
	mov	r3, #0
	str	r3, [ip, #4]
	ldr	r1, [r4, #0]
	str	r1, [r1, #12]
	str	r3, [r4, #4]
	b	.L1035
.L1061:
.L1043:
	ldr	r3, [r6, #24]
	cmp	r3, #0
	mov	r4, r0
	movne	r0, r3
	blne	free
.L1044:
.L1062:
.L1048:
	add	r0, r6, #12
	bl	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev
.L1063:
.L1054:
	mov	r0, r6
	bl	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev
	mov	r0, r4
.LEHB45:
	bl	__cxa_end_cleanup
.L1060:
.L1049:
	ldr	r3, [r6, #12]
	cmp	r3, #0
	mov	r4, r0
	beq	.L1054
	mov	r0, r3
	bl	free
	b	.L1054
.L1059:
.L1055:
	ldr	r3, [r6, #0]
	cmp	r3, #0
	mov	r4, r0
	movne	r0, r3
	blne	free
.L1056:
	mov	r0, r4
	bl	__cxa_end_cleanup
.LEHE45:
.L1074:
	.align	2
.L1073:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA5000:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5000-.LLSDACSB5000
.LLSDACSB5000:
	.uleb128 .LEHB40-.LFB5000
	.uleb128 .LEHE40-.LEHB40
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB41-.LFB5000
	.uleb128 .LEHE41-.LEHB41
	.uleb128 .L1061-.LFB5000
	.uleb128 0x0
	.uleb128 .LEHB42-.LFB5000
	.uleb128 .LEHE42-.LEHB42
	.uleb128 .L1059-.LFB5000
	.uleb128 0x0
	.uleb128 .LEHB43-.LFB5000
	.uleb128 .LEHE43-.LEHB43
	.uleb128 .L1060-.LFB5000
	.uleb128 0x0
	.uleb128 .LEHB44-.LFB5000
	.uleb128 .LEHE44-.LEHB44
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB45-.LFB5000
	.uleb128 .LEHE45-.LEHB45
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE5000:
	.fnend
	.size	T.935, .-T.935
	.section	.text._ZN4_STL4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageEED1Ev,"axG",%progbits,_ZN4_STL4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageEED1Ev,comdat
	.align	2
	.weak	_ZN4_STL4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageEED1Ev
	.hidden	_ZN4_STL4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageEED1Ev
	.type	_ZN4_STL4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageEED1Ev, %function
_ZN4_STL4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageEED1Ev:
	.fnstart
.LFB3893:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, lr}
	ldr	ip, [r0, #12]
	cmp	ip, #0
	.pad #16
	sub	sp, sp, #16
	mov	r6, r0
	add	r8, r0, #12
	ldr	sl, [r0, #16]
	beq	.L1076
	ldr	r7, .L1124
	ldr	r0, [r7, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L1080
	mov	r1, r0
	b	.L1081
.L1117:
	mov	r1, r3
	mov	r3, r2
.L1081:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L1117
.L1080:
	cmp	r3, r0
	beq	.L1083
	ldr	r2, [r3, #16]
	cmp	ip, r2
	mov	r2, r3
	bcs	.L1084
.L1083:
	str	r3, [sp, #8]
	add	r0, sp, #12
	mov	r3, #0
	add	r1, sp, #8
	mov	r2, sp
	str	ip, [sp, #0]
	str	r3, [sp, #4]
.LEHB46:
	bl	T.938
	ldr	r2, [sp, #12]
.L1084:
	ldr	r1, [r2, #20]
	sub	r0, r1, #1
	cmp	r0, #0
	str	r0, [r2, #20]
	beq	.L1118
.L1076:
	ldr	r0, [r6, #0]
	mov	r3, #0
	cmp	r0, #0
	str	r3, [r6, #12]
	str	sl, [r6, #16]
	blne	free
.L1111:
	mov	r0, r6
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L1118:
	ldr	r1, [r7, #0]
	ldr	r4, [r1, #4]
	cmp	r4, #0
	ldr	r2, [r6, #12]
	beq	.L1085
	mov	r0, r1
	mov	r5, r4
	b	.L1089
.L1119:
	mov	r0, r5
	mov	r5, r3
.L1089:
	ldr	r3, [r5, #16]
	cmp	r2, r3
	ldrhi	r3, [r5, #12]
	ldrls	r3, [r5, #8]
	movhi	r5, r0
	cmp	r3, #0
	bne	.L1119
	mov	r0, r1
	b	.L1093
.L1120:
	mov	r0, r4
	mov	r4, r3
.L1093:
	ldr	r3, [r4, #16]
	cmp	r2, r3
	ldrcs	r3, [r4, #12]
	ldrcc	r3, [r4, #8]
	movcs	r4, r0
	cmp	r3, #0
	bne	.L1120
	cmp	r5, r4
	mov	r9, r5
	moveq	r5, r4
	beq	.L1096
	mov	r0, r5
.L1097:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r4, r0
	bne	.L1097
	ldr	r1, [r7, #0]
	mov	r9, r4
.L1096:
	ldr	ip, [r1, #8]
	cmp	r5, ip
	beq	.L1121
.L1099:
	cmp	r5, r9
	bne	.L1116
	b	.L1102
.L1105:
	mov	r5, r4
.L1116:
	mov	r0, r5
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	ip, [r7, #0]
	mov	r4, r0
	add	r3, ip, #12
	mov	r0, r5
	add	r1, ip, #4
	add	r2, ip, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L1104:
	ldr	lr, [r7, #4]
	cmp	r4, r9
	sub	r2, lr, #1
	str	r2, [r7, #4]
	bne	.L1105
.L1102:
	ldr	r3, [r6, #16]
	cmp	r3, #1
	ldr	r3, [r6, #12]
	beq	.L1122
	cmp	r3, #0
	beq	.L1076
	ldr	r0, [r3, #-4]
	rsb	r4, r0, r0, asl #3
	add	r4, r3, r4, asl #2
	b	.L1108
.L1123:
	ldr	r2, [r4, #-28]!
	mov	r0, r4
	ldr	ip, [r2, #4]
	mov	lr, pc
	bx	ip
.L1108:
	ldr	ip, [r8, #0]
	cmp	ip, r4
	bne	.L1123
	ldr	lr, [r6, #12]
	sub	r0, lr, #8
	bl	_ZdaPv
	b	.L1076
.L1122:
	cmp	r3, #0
	beq	.L1076
	mov	r0, r3
	ldr	r1, [r3, #0]
	ldr	ip, [r1, #8]
	mov	lr, pc
	bx	ip
	b	.L1076
.L1085:
	mov	r9, r1
	mov	r5, r1
	b	.L1096
.L1121:
	cmp	r9, r1
	bne	.L1099
	ldr	r2, [r7, #4]
	cmp	r2, #0
	beq	.L1102
	ldr	r0, [r9, #4]
	bl	T.932
.LEHE46:
	ldr	r1, [r7, #0]
	str	r1, [r1, #8]
	ldr	r0, [r7, #0]
	mov	lr, #0
	str	lr, [r0, #4]
	ldr	r3, [r7, #0]
	str	r3, [r3, #12]
	str	lr, [r7, #4]
	b	.L1102
.L1114:
.L1112:
	mov	r5, r0
	mov	r0, r6
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r5
.LEHB47:
	bl	__cxa_end_cleanup
.LEHE47:
.L1125:
	.align	2
.L1124:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA3893:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3893-.LLSDACSB3893
.LLSDACSB3893:
	.uleb128 .LEHB46-.LFB3893
	.uleb128 .LEHE46-.LEHB46
	.uleb128 .L1114-.LFB3893
	.uleb128 0x0
	.uleb128 .LEHB47-.LFB3893
	.uleb128 .LEHE47-.LEHB47
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE3893:
	.fnend
	.size	_ZN4_STL4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageEED1Ev, .-_ZN4_STL4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageEED1Ev
	.section	.text._ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_,"axG",%progbits,_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_,comdat
	.align	2
	.weak	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_
	.hidden	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_
	.type	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_, %function
_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_:
	.fnstart
.LFB3890:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 72
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	r9, [r0, #0]
	ldr	r4, [r9, #4]
	cmp	r4, #0
	.pad #76
	sub	sp, sp, #76
	mov	fp, r0
	mov	sl, r1
	moveq	r4, r9
	beq	.L1132
	ldr	r8, [r1, #0]
	ldr	r6, [r1, #4]
	mov	r7, r9
	rsb	r6, r8, r6
.L1133:
	add	r0, r4, #16
	ldmia	r0, {r0, r5}	@ phole ldm
	rsb	r5, r0, r5
	cmp	r6, r5
	movlt	r2, r6
	movge	r2, r5
	mov	r1, r8
	bl	memcmp
	cmp	r0, #0
	bne	.L1128
	cmp	r5, r6
	blt	.L1130
.L1129:
	ldr	r3, [r4, #8]
	cmp	r3, #0
	beq	.L1132
.L1218:
	mov	r7, r4
	mov	r4, r3
	b	.L1133
.L1128:
	bge	.L1129
.L1130:
	ldr	r3, [r4, #12]
	cmp	r3, #0
	mov	r4, r7
	bne	.L1218
.L1132:
	cmp	r4, r9
	beq	.L1135
	ldr	r0, [sl, #0]
	ldr	r1, [r4, #16]
	ldr	r3, [sl, #4]
	ldr	r2, [r4, #20]
	rsb	r6, r0, r3
	rsb	r5, r1, r2
	cmp	r5, r6
	movlt	r2, r5
	movge	r2, r6
	bl	memcmp
	cmp	r0, #0
	mov	r0, r4
	bne	.L1136
	cmp	r6, r5
	blt	.L1135
.L1137:
	add	r0, r0, #28
	add	sp, sp, #76
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L1136:
	bge	.L1137
.L1135:
	add	r6, sp, #4
	mov	lr, #0
	mov	ip, #1
	mov	r1, sl
	mov	r0, r6
	str	lr, [sp, #40]
	str	ip, [sp, #44]
.LEHB48:
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
.LEHE48:
	ldr	r5, [sp, #40]
	ldr	r0, [sp, #44]
	cmp	r5, #0
	str	r0, [sp, #20]
	str	r5, [sp, #16]
	beq	.L1139
	ldr	r7, .L1228
	ldr	r3, [r7, #0]
	ldr	ip, [r3, #4]
	cmp	ip, #0
	beq	.L1140
	mov	r0, r3
	mov	r2, ip
	b	.L1144
.L1219:
	mov	r0, r2
	mov	r2, r1
.L1144:
	ldr	r1, [r2, #16]
	cmp	r5, r1
	ldrhi	r1, [r2, #12]
	ldrls	r1, [r2, #8]
	movhi	r2, r0
	cmp	r1, #0
	bne	.L1219
	cmp	r3, r2
	beq	.L1140
	ldr	r1, [r2, #16]
	cmp	r5, r1
	bcc	.L1140
.L1145:
	cmp	r2, r3
	mov	r1, r2
	beq	.L1215
.L1147:
	ldr	lr, [r1, #20]
	add	r0, lr, #1
	str	r0, [r1, #20]
.L1162:
.L1139:
	mov	r1, fp
	add	r0, sp, #68
	add	r2, sp, #64
	mov	r3, r6
	str	r4, [sp, #64]
.LEHB49:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueENS_17_Rb_tree_iteratorISC_NS_16_Nonconst_traitsISC_EEEERKSC_
.LEHE49:
	add	r0, r6, #12
	mov	r1, #0
	ldr	r2, [sp, #20]
	ldr	r4, [sp, #68]
.LEHB50:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE50:
	ldr	r0, [sp, #4]
	cmp	r0, #0
	blne	free
.L1165:
	ldr	ip, [sp, #40]
	cmp	ip, #0
	beq	.L1217
	ldr	r5, .L1228
	ldr	r0, [r5, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L1174
	mov	r1, r0
	b	.L1175
.L1220:
	mov	r1, r3
	mov	r3, r2
.L1175:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L1220
.L1174:
	cmp	r3, r0
	beq	.L1177
	ldr	r2, [r3, #16]
	cmp	ip, r2
	mov	r2, r3
	bcs	.L1178
.L1177:
	add	r2, sp, #24
	str	r3, [sp, #48]
	add	r0, sp, #52
	mov	r3, #0
	add	r1, sp, #48
	str	ip, [sp, #24]
	str	r3, [sp, #28]
.LEHB51:
	bl	T.938
	ldr	r2, [sp, #52]
.L1178:
	ldr	r1, [r2, #20]
	sub	ip, r1, #1
	cmp	ip, #0
	str	ip, [r2, #20]
	beq	.L1179
.L1217:
	mov	r0, r4
	b	.L1137
.L1221:
	ldr	lr, [ip, #16]
	cmp	r5, lr
	movls	r3, ip
	ldrhi	ip, [ip, #12]
	ldrls	ip, [ip, #8]
.L1215:
	cmp	ip, #0
	bne	.L1221
	cmp	r2, r3
	beq	.L1153
	ldr	r2, [r3, #16]
	cmp	r5, r2
	mov	r2, r3
	bcc	.L1153
.L1154:
	mov	ip, #0
	str	ip, [r2, #20]
	ldr	r0, [r7, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	beq	.L1157
	mov	r1, r0
	b	.L1161
.L1222:
	mov	r1, r3
	mov	r3, r2
.L1161:
	ldr	r2, [r3, #16]
	cmp	r5, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L1222
	cmp	r0, r3
	beq	.L1157
	ldr	r1, [r3, #16]
	cmp	r5, r1
	movcs	r0, r3
.L1157:
	mov	r1, r0
	b	.L1147
.L1140:
	mov	r2, r3
	b	.L1145
.L1179:
	ldr	r1, [r5, #0]
	ldr	r6, [r1, #4]
	cmp	r6, #0
	ldr	r2, [sp, #40]
	moveq	r8, r1
	moveq	r7, r1
	beq	.L1191
	mov	r0, r1
	mov	r8, r6
	b	.L1184
.L1223:
	mov	r0, r8
	mov	r8, r3
.L1184:
	ldr	r3, [r8, #16]
	cmp	r2, r3
	ldrhi	r3, [r8, #12]
	ldrls	r3, [r8, #8]
	movhi	r8, r0
	cmp	r3, #0
	bne	.L1223
	mov	r0, r1
	b	.L1188
.L1224:
	mov	r0, r6
	mov	r6, r3
.L1188:
	ldr	r3, [r6, #16]
	cmp	r2, r3
	ldrcs	r3, [r6, #12]
	ldrcc	r3, [r6, #8]
	movcs	r6, r0
	cmp	r3, #0
	bne	.L1224
	cmp	r8, r6
	mov	r7, r8
	moveq	r8, r6
	beq	.L1191
	mov	r0, r8
.L1192:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r6, r0
	bne	.L1192
	ldr	r1, [r5, #0]
	mov	r7, r6
.L1191:
	ldr	r0, [r1, #8]
	cmp	r8, r0
	beq	.L1225
.L1194:
	cmp	r8, r7
	bne	.L1212
.L1197:
	ldr	r3, [sp, #44]
	cmp	r3, #1
	ldr	r3, [sp, #40]
	beq	.L1226
	cmp	r3, #0
	beq	.L1217
	ldr	r2, [r3, #-4]
	rsb	r1, r2, r2, asl #3
	add	r0, r3, r1, asl #2
	cmp	r3, r0
	bne	.L1214
	b	.L1204
.L1227:
	mov	r0, r5
.L1214:
	sub	r5, r0, #28
	ldr	r3, [r0, #-28]
	mov	r0, r5
	ldr	ip, [r3, #4]
	mov	lr, pc
	bx	ip
	ldr	r0, [sp, #40]
	cmp	r0, r5
	bne	.L1227
.L1204:
	sub	r0, r0, #8
	bl	_ZdaPv
	mov	r0, r4
	b	.L1137
.L1200:
	mov	r8, r6
.L1212:
	mov	r0, r8
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r2, [r5, #0]
	mov	r6, r0
	add	r3, r2, #12
	add	r1, r2, #4
	mov	r0, r8
	add	r2, r2, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
.LEHE51:
	cmp	r0, #0
	blne	free
.L1199:
	ldr	lr, [r5, #4]
	cmp	r6, r7
	sub	r0, lr, #1
	str	r0, [r5, #4]
	bne	.L1200
	b	.L1197
.L1153:
	str	r3, [sp, #56]
	add	r0, sp, #60
	mov	r3, #0
	add	r1, sp, #56
	add	r2, sp, #32
	str	r3, [sp, #36]
	str	r5, [sp, #32]
.LEHB52:
	bl	T.938
.LEHE52:
	ldr	r2, [sp, #60]
	b	.L1154
.L1226:
	cmp	r3, #0
	beq	.L1217
	mov	r0, r3
	ldr	ip, [r3, #0]
.LEHB53:
	ldr	ip, [ip, #8]
	mov	lr, pc
	bx	ip
	mov	r0, r4
	b	.L1137
.L1225:
	cmp	r7, r1
	bne	.L1194
	ldr	lr, [r5, #4]
	cmp	lr, #0
	ldr	r5, .L1228
	beq	.L1197
	ldr	r0, [r7, #4]
	bl	T.932
.LEHE53:
	ldr	r1, [r5, #0]
	str	r1, [r1, #8]
	ldr	ip, [r5, #0]
	mov	r3, #0
	str	r3, [ip, #4]
	ldr	r2, [r5, #0]
	str	r2, [r2, #12]
	str	r3, [r5, #4]
	b	.L1197
.L1208:
.L1216:
.L1166:
	mov	r4, r0
	mov	r0, r6
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
.L1169:
	add	r0, sp, #40
	mov	r1, #0
	ldr	r2, [sp, #44]
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
	mov	r0, r4
.LEHB54:
	bl	__cxa_end_cleanup
.LEHE54:
.L1209:
.L1168:
	mov	r4, r0
	mov	r0, r6
	bl	_ZN4_STL4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageEED1Ev
	b	.L1169
.L1207:
	b	.L1216
.L1210:
	mov	r4, r0
	b	.L1169
.L1229:
	.align	2
.L1228:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA3890:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3890-.LLSDACSB3890
.LLSDACSB3890:
	.uleb128 .LEHB48-.LFB3890
	.uleb128 .LEHE48-.LEHB48
	.uleb128 .L1210-.LFB3890
	.uleb128 0x0
	.uleb128 .LEHB49-.LFB3890
	.uleb128 .LEHE49-.LEHB49
	.uleb128 .L1209-.LFB3890
	.uleb128 0x0
	.uleb128 .LEHB50-.LFB3890
	.uleb128 .LEHE50-.LEHB50
	.uleb128 .L1207-.LFB3890
	.uleb128 0x0
	.uleb128 .LEHB51-.LFB3890
	.uleb128 .LEHE51-.LEHB51
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB52-.LFB3890
	.uleb128 .LEHE52-.LEHB52
	.uleb128 .L1208-.LFB3890
	.uleb128 0x0
	.uleb128 .LEHB53-.LFB3890
	.uleb128 .LEHE53-.LEHB53
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB54-.LFB3890
	.uleb128 .LEHE54-.LEHB54
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE3890:
	.fnend
	.size	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_, .-_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_
	.section	.text._ZN14QuImageManager11setFlyImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE,"axG",%progbits,_ZN14QuImageManager11setFlyImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE,comdat
	.align	2
	.weak	_ZN14QuImageManager11setFlyImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.hidden	_ZN14QuImageManager11setFlyImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.type	_ZN14QuImageManager11setFlyImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE, %function
_ZN14QuImageManager11setFlyImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE:
	.fnstart
.LFB3193:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 240
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.pad #244
	sub	sp, sp, #244
	stmib	sp, {r0, r1}	@ phole stm
	mov	r1, r2
	add	r0, sp, #52
	mov	r4, r2
.LEHB55:
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
	ldr	r1, [sp, #8]
	ldr	r9, [r1, #0]
	ldr	r5, [r9, #4]
	cmp	r5, #0
	ldr	sl, [sp, #52]
	beq	.L1231
	ldr	fp, [sp, #56]
	mov	r8, r9
	rsb	r7, sl, fp
	mov	r6, r4
.L1237:
	add	r0, r5, #16
	ldmia	r0, {r0, r4}	@ phole ldm
	rsb	r4, r0, r4
	cmp	r7, r4
	movlt	r2, r7
	movge	r2, r4
	mov	r1, sl
	bl	memcmp
	cmp	r0, #0
	bne	.L1232
	cmp	r4, r7
	blt	.L1234
.L1233:
	ldr	r3, [r5, #8]
	cmp	r3, #0
	beq	.L1236
.L1567:
	mov	r8, r5
	mov	r5, r3
	b	.L1237
.L1232:
	bge	.L1233
.L1234:
	ldr	r3, [r5, #12]
	cmp	r3, #0
	mov	r5, r8
	bne	.L1567
.L1236:
	cmp	r9, r5
	mov	r4, r6
	beq	.L1231
	add	r1, r5, #16
	ldmia	r1, {r1, r6}	@ phole ldm
	rsb	fp, sl, fp
	rsb	r6, r1, r6
	cmp	r6, fp
	movlt	r2, r6
	movge	r2, fp
	mov	r0, sl
	bl	memcmp
	cmp	r0, #0
	bne	.L1238
	cmp	fp, r6
	blt	.L1231
.L1239:
	cmp	sl, #0
	movne	r0, sl
	blne	free
.L1240:
	cmp	r9, r5
	ldr	r2, [sp, #8]
	beq	.L1241
	ldr	r9, [r2, #0]
	ldr	r5, [r9, #4]
	cmp	r5, #0
	moveq	r5, r9
	beq	.L1247
	ldr	sl, [r4, #0]
	ldr	r7, [r4, #4]
	mov	r8, r9
	rsb	r7, sl, r7
.L1248:
	add	r0, r5, #16
	ldmia	r0, {r0, r2}	@ phole ldm
	rsb	r6, r0, r2
	cmp	r7, r6
	movlt	r2, r7
	movge	r2, r6
	mov	r1, sl
	bl	memcmp
	cmp	r0, #0
	bne	.L1243
	cmp	r6, r7
	blt	.L1245
.L1244:
	ldr	r3, [r5, #8]
	cmp	r3, #0
	beq	.L1247
.L1568:
	mov	r8, r5
	mov	r5, r3
	b	.L1248
.L1238:
	bge	.L1239
.L1231:
	mov	r5, r9
	b	.L1239
.L1243:
	bge	.L1244
.L1245:
	ldr	r3, [r5, #12]
	cmp	r3, #0
	mov	r5, r8
	bne	.L1568
.L1247:
	cmp	r5, r9
	beq	.L1250
	ldr	r3, [r5, #20]
	ldr	r0, [r4, #0]
	ldr	r1, [r5, #16]
	ldr	ip, [r4, #4]
	rsb	r6, r1, r3
	rsb	r7, r0, ip
	cmp	r6, r7
	movlt	r2, r6
	movge	r2, r7
	bl	memcmp
	cmp	r0, #0
	mov	r3, r5
	bne	.L1251
	cmp	r7, r6
	blt	.L1250
.L1252:
	ldr	r0, [sp, #4]
	mov	r2, #0
	str	r2, [r0, #0]
	ldr	r5, [r3, #28]
	ldr	r1, [r3, #32]
	cmp	r5, r2
	str	r1, [r0, #4]
	str	r5, [r0, #0]
	beq	.L1230
	ldr	r4, .L1610+8
	ldr	r3, [r4, #0]
	ldr	ip, [r3, #4]
	cmp	ip, r2
	beq	.L1265
	mov	r0, r3
	mov	r2, ip
	b	.L1269
.L1569:
	mov	r0, r2
	mov	r2, r1
.L1269:
	ldr	r1, [r2, #16]
	cmp	r5, r1
	ldrhi	r1, [r2, #12]
	ldrls	r1, [r2, #8]
	movhi	r2, r0
	cmp	r1, #0
	bne	.L1569
	cmp	r3, r2
	beq	.L1265
	ldr	lr, [r2, #16]
	cmp	r5, lr
	bcc	.L1265
.L1270:
	cmp	r2, r3
	mov	r1, r2
	beq	.L1562
.L1272:
	ldr	r0, [r1, #20]
	add	lr, r0, #1
	str	lr, [r1, #20]
.L1230:
	ldr	r0, [sp, #4]
	add	sp, sp, #244
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L1570:
	ldr	r0, [ip, #16]
	cmp	r5, r0
	movls	r3, ip
	ldrhi	ip, [ip, #12]
	ldrls	ip, [ip, #8]
.L1562:
	cmp	ip, #0
	bne	.L1570
	cmp	r2, r3
	beq	.L1278
	ldr	r2, [r3, #16]
	cmp	r5, r2
	mov	r2, r3
	bcs	.L1279
.L1278:
	add	r2, sp, #128
	mov	ip, #0
	add	r0, sp, #228
	add	r1, sp, #224
	str	ip, [sp, #132]
	str	r3, [sp, #224]
	str	r5, [sp, #128]
	bl	T.938
	ldr	r2, [sp, #228]
.L1279:
	mov	r3, #0
	str	r3, [r2, #20]
	ldr	r0, [r4, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	beq	.L1282
	mov	r1, r0
	b	.L1286
.L1571:
	mov	r1, r3
	mov	r3, r2
.L1286:
	ldr	r2, [r3, #16]
	cmp	r5, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L1571
	cmp	r0, r3
	beq	.L1282
	ldr	r1, [r3, #16]
	cmp	r5, r1
	movcs	r0, r3
.L1282:
	mov	r1, r0
	b	.L1272
.L1265:
	mov	r2, r3
	b	.L1270
.L1251:
	bge	.L1252
.L1250:
	mov	r1, #0
	add	r7, sp, #240
	str	r1, [r7, #-104]!
	mov	r2, #1
	mov	r0, r7
	add	r6, sp, #20
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE55:
	mov	r1, r4
	mov	r0, r6
.LEHB56:
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
.LEHE56:
	mov	lr, #0
	add	r0, r6, #12
	add	r1, sp, #136
	ldmia	r1, {r1, r2}	@ phole ldm
	str	lr, [sp, #32]
.LEHB57:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE57:
.L1255:
	ldr	r1, [sp, #8]
	add	r0, sp, #236
	add	r2, sp, #232
	mov	r3, r6
	str	r5, [sp, #232]
.LEHB58:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueENS_17_Rb_tree_iteratorISC_NS_16_Nonconst_traitsISC_EEEERKSC_
.LEHE58:
	add	r0, r6, #12
	mov	r1, #0
	ldr	r2, [sp, #36]
	ldr	r4, [sp, #236]
.LEHB59:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE59:
	ldr	r0, [sp, #20]
	cmp	r0, #0
	blne	free
.L1259:
	mov	r0, r7
	mov	r1, #0
	ldr	r2, [sp, #140]
.LEHB60:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
	mov	r3, r4
	b	.L1252
.L1241:
	add	r0, r2, #12
	mov	r1, r4
	bl	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_
	mov	r1, r4
	mov	r9, r0
	ldr	r0, [sp, #8]
	bl	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_
	mov	sl, r0
	mov	r0, #48
	bl	_Znwj
.LEHE60:
	ldr	r2, .L1610
	mov	fp, #0
	mvn	ip, #0
	mov	r6, #1
	str	ip, [r0, #8]
	str	r2, [r0, #0]
	str	r6, [r0, #32]
	str	fp, [r0, #44]
	strb	fp, [r0, #4]
	str	fp, [r0, #28]
	str	fp, [r0, #36]
	str	fp, [r0, #40]
	mov	r5, r0
	mov	r0, #8
	bl	malloc
	cmp	r0, #0
	beq	.L1572
.L1290:
	add	lr, r0, #8
	mov	r1, #0
	str	r0, [r5, #36]
	str	r0, [r5, #40]
	str	lr, [r5, #44]
	ldr	r6, .L1610+8
	strb	r1, [r0, #0]
	mov	r3, #1
	str	r3, [sp, #156]
	str	r5, [sp, #152]
	ldr	r3, [r6, #0]
	ldr	ip, [r3, #4]
	cmp	ip, #0
	movne	r0, r3
	movne	r2, ip
	bne	.L1301
.L1297:
	mov	r2, r3
.L1302:
	cmp	r2, r3
	mov	r1, r2
	beq	.L1563
.L1304:
	ldr	lr, [r1, #20]
	add	r5, lr, #1
	str	r5, [r1, #20]
	add	r5, sp, #152
	ldmia	r5, {r5, fp}	@ phole ldm
	cmp	r5, #0
	str	r5, [sp, #144]
	str	fp, [sp, #148]
	beq	.L1323
	ldr	r3, [r6, #0]
	ldr	ip, [r3, #4]
	cmp	ip, #0
	beq	.L1324
	mov	r0, r3
	mov	r2, ip
	b	.L1328
.L1573:
	mov	r0, r2
	mov	r2, r1
.L1328:
	ldr	r1, [r2, #16]
	cmp	r5, r1
	ldrhi	r1, [r2, #12]
	ldrls	r1, [r2, #8]
	movhi	r2, r0
	cmp	r1, #0
	bne	.L1573
	cmp	r3, r2
	beq	.L1324
	ldr	r0, [r2, #16]
	cmp	r5, r0
	bcc	.L1324
.L1329:
	cmp	r2, r3
	mov	r1, r2
	beq	.L1564
.L1331:
	ldr	lr, [r1, #20]
	add	r5, lr, #1
	str	r5, [r1, #20]
	add	r5, sp, #144
	ldmia	r5, {r5, fp}	@ phole ldm
.L1323:
	ldr	ip, [sl, #0]
	cmp	ip, #0
	beq	.L1347
	ldr	r0, [r6, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L1351
	mov	r1, r0
	b	.L1352
.L1574:
	mov	r1, r3
	mov	r3, r2
.L1352:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L1574
.L1351:
	cmp	r3, r0
	beq	.L1354
	ldr	r0, [r3, #16]
	cmp	ip, r0
	mov	r2, r3
	bcs	.L1355
.L1354:
	str	ip, [sp, #104]
	add	r0, sp, #204
	mov	ip, #0
	add	r1, sp, #200
	add	r2, sp, #104
	str	ip, [sp, #108]
	str	r3, [sp, #200]
.LEHB61:
	bl	T.938
	ldr	r2, [sp, #204]
.L1355:
	ldr	r1, [r2, #20]
	sub	r3, r1, #1
	cmp	r3, #0
	str	r3, [r2, #20]
	beq	.L1575
.L1347:
	cmp	r5, #0
	stmia	sl, {r5, fp}	@ phole stm
	beq	.L1382
	ldr	r3, [r6, #0]
	ldr	ip, [r3, #4]
	cmp	ip, #0
	beq	.L1383
	mov	r0, r3
	mov	r2, ip
	b	.L1387
.L1576:
	mov	r0, r2
	mov	r2, r1
.L1387:
	ldr	r1, [r2, #16]
	cmp	r5, r1
	ldrhi	r1, [r2, #12]
	ldrls	r1, [r2, #8]
	movhi	r2, r0
	cmp	r1, #0
	bne	.L1576
	cmp	r3, r2
	beq	.L1383
	ldr	fp, [r2, #16]
	cmp	r5, fp
	bcc	.L1383
.L1388:
	cmp	r2, r3
	mov	r1, r2
	beq	.L1565
.L1390:
	ldr	r3, [r1, #20]
	add	r5, r3, #1
	str	r5, [r1, #20]
	ldmia	sl, {r5, fp}	@ phole ldm
.L1382:
	ldr	ip, [r9, #0]
	cmp	ip, #0
	beq	.L1406
	ldr	r0, [r6, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L1410
	mov	r1, r0
	b	.L1411
.L1577:
	mov	r1, r3
	mov	r3, r2
.L1411:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L1577
.L1410:
	cmp	r3, r0
	beq	.L1413
	ldr	r0, [r3, #16]
	cmp	ip, r0
	mov	r2, r3
	bcs	.L1414
.L1413:
	mov	lr, #0
	add	r0, sp, #188
	add	r1, sp, #184
	add	r2, sp, #88
	str	ip, [sp, #88]
	str	lr, [sp, #92]
	str	r3, [sp, #184]
	bl	T.938
.LEHE61:
	ldr	r2, [sp, #188]
.L1414:
	ldr	ip, [r2, #20]
	sub	r1, ip, #1
	cmp	r1, #0
	str	r1, [r2, #20]
	beq	.L1578
.L1406:
	cmp	r5, #0
	stmia	r9, {r5, fp}	@ phole stm
	beq	.L1440
	ldr	r3, [r6, #0]
	ldr	ip, [r3, #4]
	cmp	ip, #0
	beq	.L1441
	mov	r0, r3
	mov	r2, ip
	b	.L1445
.L1579:
	mov	r0, r2
	mov	r2, r1
.L1445:
	ldr	r1, [r2, #16]
	cmp	r1, r5
	ldrcc	r1, [r2, #12]
	ldrcs	r1, [r2, #8]
	movcc	r2, r0
	cmp	r1, #0
	bne	.L1579
	cmp	r3, r2
	beq	.L1441
	ldr	r0, [r2, #16]
	cmp	r0, r5
	bhi	.L1441
.L1446:
	cmp	r2, r3
	mov	r1, r2
	beq	.L1566
.L1448:
	ldr	r0, [r1, #20]
	add	r5, r0, #1
	str	r5, [r1, #20]
.L1440:
	add	r0, sp, #144
	mov	r1, #0
	ldr	r2, [sp, #148]
.LEHB62:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE62:
	ldr	ip, [sp, #152]
	cmp	ip, #0
	ldr	r5, [sp, #156]
	beq	.L1466
	ldr	r0, [r6, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L1507
	mov	r1, r0
	b	.L1508
.L1580:
	mov	r1, r3
	mov	r3, r2
.L1508:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L1580
.L1507:
	cmp	r3, r0
	beq	.L1510
	ldr	r0, [r3, #16]
	cmp	ip, r0
	mov	r2, r3
	bcs	.L1511
.L1510:
	add	r2, sp, #64
	mov	lr, #0
	add	r0, sp, #164
	add	r1, sp, #160
	str	ip, [sp, #64]
	str	lr, [sp, #68]
	str	r3, [sp, #160]
.LEHB63:
	bl	T.938
	ldr	r2, [sp, #164]
.L1511:
	ldr	r1, [r2, #20]
	sub	ip, r1, #1
	cmp	ip, #0
	str	ip, [r2, #20]
	beq	.L1581
.L1466:
	mov	ip, #0
	mov	r1, r4
	ldr	r0, [sp, #8]
	str	ip, [sp, #152]
	str	r5, [sp, #156]
	bl	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_
	add	r5, sp, #40
	ldr	r6, [r0, #0]
	mov	r1, r4
	mov	r0, r5
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
.LEHE63:
	mov	r0, r6
	mov	r1, r5
.LEHB64:
	bl	_ZN10QuPngImage9loadImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
.LEHE64:
	ldr	r0, [sp, #40]
	cmp	r0, #0
	blne	free
.L1520:
	mov	r0, r6
.LEHB65:
	bl	_ZN10QuPngImage18finishLoadingImageEv
	mov	r1, r4
	ldr	r0, [sp, #8]
	bl	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_
	ldr	r1, [sp, #4]
	mov	r2, #0
	str	r2, [r1, #0]
	mov	r3, r0
	ldr	r2, [r0, #4]
	ldr	r1, [r3, #0]
	ldr	r0, [sp, #4]
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
	b	.L1230
.L1582:
	mov	r0, r2
	mov	r2, r1
.L1301:
	ldr	r1, [r2, #16]
	cmp	r5, r1
	ldrhi	r1, [r2, #12]
	ldrls	r1, [r2, #8]
	movhi	r2, r0
	cmp	r1, #0
	bne	.L1582
	cmp	r3, r2
	beq	.L1297
	ldr	r0, [r2, #16]
	cmp	r5, r0
	movcc	r2, r3
	b	.L1302
.L1583:
	ldr	fp, [ip, #16]
	cmp	r5, fp
	movls	r3, ip
	ldrhi	ip, [ip, #12]
	ldrls	ip, [ip, #8]
.L1563:
	cmp	ip, #0
	bne	.L1583
	cmp	r2, r3
	beq	.L1310
	ldr	r2, [r3, #16]
	cmp	r5, r2
	mov	r2, r3
	bcs	.L1311
.L1310:
	add	r2, sp, #120
	mov	ip, #0
	add	r0, sp, #220
	add	r1, sp, #216
	str	ip, [sp, #124]
	str	r3, [sp, #216]
	str	r5, [sp, #120]
	bl	T.938
.LEHE65:
	ldr	r2, [sp, #220]
.L1311:
	mov	r3, #0
	str	r3, [r2, #20]
	ldr	r0, [r6, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	beq	.L1314
	mov	r1, r0
	b	.L1318
.L1584:
	mov	r1, r3
	mov	r3, r2
.L1318:
	ldr	r2, [r3, #16]
	cmp	r5, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L1584
	cmp	r0, r3
	beq	.L1314
	ldr	r1, [r3, #16]
	cmp	r5, r1
	bcs	.L1319
.L1314:
	mov	r3, r0
.L1319:
	mov	r1, r3
	b	.L1304
.L1585:
	ldr	fp, [ip, #16]
	cmp	r5, fp
	movls	r3, ip
	ldrhi	ip, [ip, #12]
	ldrls	ip, [ip, #8]
.L1564:
	cmp	ip, #0
	bne	.L1585
	cmp	r2, r3
	beq	.L1337
	ldr	r2, [r3, #16]
	cmp	r5, r2
	mov	r2, r3
	bcs	.L1338
.L1337:
	mov	ip, #0
	add	r0, sp, #212
	add	r1, sp, #208
	add	r2, sp, #112
	str	ip, [sp, #116]
	str	r3, [sp, #208]
	str	r5, [sp, #112]
.LEHB66:
	bl	T.938
.LEHE66:
	ldr	r2, [sp, #212]
.L1338:
	mov	r3, #0
	str	r3, [r2, #20]
	ldr	r0, [r6, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	beq	.L1341
	mov	r1, r0
	b	.L1345
.L1586:
	mov	r1, r3
	mov	r3, r2
.L1345:
	ldr	r2, [r3, #16]
	cmp	r5, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L1586
	cmp	r0, r3
	beq	.L1341
	ldr	r1, [r3, #16]
	cmp	r5, r1
	bcs	.L1346
.L1341:
	mov	r3, r0
.L1346:
	mov	r1, r3
	b	.L1331
.L1587:
	ldr	r0, [ip, #16]
	cmp	r5, r0
	movls	r3, ip
	ldrhi	ip, [ip, #12]
	ldrls	ip, [ip, #8]
.L1565:
	cmp	ip, #0
	bne	.L1587
	cmp	r2, r3
	beq	.L1396
	ldr	r1, [r3, #16]
	cmp	r5, r1
	mov	r2, r3
	bcs	.L1397
.L1396:
	mov	lr, #0
	add	r0, sp, #196
	add	r1, sp, #192
	add	r2, sp, #96
	str	lr, [sp, #100]
	str	r3, [sp, #192]
	str	r5, [sp, #96]
.LEHB67:
	bl	T.938
	ldr	r2, [sp, #196]
.L1397:
	mov	ip, #0
	str	ip, [r2, #20]
	ldr	r0, [r6, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	beq	.L1400
	mov	r1, r0
	b	.L1404
.L1588:
	mov	r1, r3
	mov	r3, r2
.L1404:
	ldr	r2, [r3, #16]
	cmp	r5, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L1588
	cmp	r0, r3
	beq	.L1400
	ldr	r2, [r3, #16]
	cmp	r5, r2
	bcs	.L1405
.L1400:
	mov	r3, r0
.L1405:
	mov	r1, r3
	b	.L1390
.L1589:
	ldr	lr, [ip, #16]
	cmp	lr, r5
	movcs	r3, ip
	ldrcc	ip, [ip, #12]
	ldrcs	ip, [ip, #8]
.L1566:
	cmp	ip, #0
	bne	.L1589
	cmp	r2, r3
	beq	.L1454
	ldr	r1, [r3, #16]
	cmp	r1, r5
	mov	r2, r3
	bls	.L1455
.L1454:
	mov	ip, #0
	add	r0, sp, #180
	add	r1, sp, #176
	add	r2, sp, #80
	str	ip, [sp, #84]
	str	r3, [sp, #176]
	str	r5, [sp, #80]
	bl	T.938
.LEHE67:
	ldr	r2, [sp, #180]
.L1455:
	mov	r3, #0
	str	r3, [r2, #20]
	ldr	r0, [r6, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	beq	.L1458
	mov	r1, r0
	b	.L1462
.L1590:
	mov	r1, r3
	mov	r3, r2
.L1462:
	ldr	r2, [r3, #16]
	cmp	r2, r5
	ldrcc	r2, [r3, #12]
	ldrcs	r2, [r3, #8]
	movcc	r3, r1
	cmp	r2, #0
	bne	.L1590
	cmp	r0, r3
	beq	.L1458
	ldr	r2, [r3, #16]
	cmp	r2, r5
	bls	.L1463
.L1458:
	mov	r3, r0
.L1463:
	mov	r1, r3
	b	.L1448
.L1383:
	mov	r2, r3
	b	.L1388
.L1441:
	mov	r2, r3
	b	.L1446
.L1324:
	mov	r2, r3
	b	.L1329
.L1572:
	mov	r0, #8
.LEHB68:
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
.LEHE68:
	b	.L1290
.L1578:
	ldr	r2, [r6, #0]
	ldr	r7, [r2, #4]
	cmp	r7, #0
	ldr	r1, [r9, #0]
	beq	.L1417
	mov	r0, r2
	mov	r8, r7
	b	.L1421
.L1591:
	mov	r0, r8
	mov	r8, r3
.L1421:
	ldr	r3, [r8, #16]
	cmp	r1, r3
	ldrhi	r3, [r8, #12]
	ldrls	r3, [r8, #8]
	movhi	r8, r0
	cmp	r3, #0
	bne	.L1591
	mov	sl, r8
	mov	r0, r2
	b	.L1425
.L1592:
	mov	r0, r7
	mov	r7, r3
.L1425:
	ldr	r3, [r7, #16]
	cmp	r1, r3
	ldrcs	r3, [r7, #12]
	ldrcc	r3, [r7, #8]
	movcs	r7, r0
	cmp	r3, #0
	bne	.L1592
	cmp	r8, r7
	beq	.L1427
	mov	r0, r8
.L1428:
.LEHB69:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r7, r0
	bne	.L1428
	ldr	r2, [r6, #0]
	mov	sl, r7
.L1427:
	ldr	r3, [r2, #8]
	cmp	r8, r3
	beq	.L1593
.L1430:
	cmp	r8, sl
	bne	.L1537
	b	.L1433
.L1436:
	mov	r8, r7
.L1537:
	mov	r0, r8
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r2, [r6, #0]
	mov	r7, r0
	add	r3, r2, #12
	add	r1, r2, #4
	mov	r0, r8
	add	r2, r2, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L1435:
	ldr	r0, [r6, #4]
	cmp	r7, sl
	sub	r3, r0, #1
	str	r3, [r6, #4]
	bne	.L1436
.L1433:
	ldr	lr, [r9, #4]
	cmp	lr, #1
	beq	.L1594
	ldr	r0, [r9, #0]
	cmp	r0, #0
	beq	.L1406
	ldr	ip, [r0, #-4]
	rsb	r7, ip, ip, asl #3
	add	r7, r0, r7, asl #2
	b	.L1439
.L1595:
	ldr	r2, [r7, #-28]!
	mov	r0, r7
	ldr	ip, [r2, #4]
	mov	lr, pc
	bx	ip
	ldr	r0, [r9, #0]
.L1439:
	cmp	r0, r7
	bne	.L1595
	sub	r0, r0, #8
	bl	_ZdaPv
	b	.L1406
.L1575:
	ldr	r1, [r6, #0]
	ldr	r7, [r1, #4]
	cmp	r7, #0
	ldr	r2, [sl, #0]
	beq	.L1358
	mov	r0, r1
	mov	r8, r7
	b	.L1362
.L1596:
	mov	r0, r8
	mov	r8, r3
.L1362:
	ldr	r3, [r8, #16]
	cmp	r2, r3
	ldrhi	r3, [r8, #12]
	ldrls	r3, [r8, #8]
	movhi	r8, r0
	cmp	r3, #0
	bne	.L1596
	str	r8, [sp, #12]
	mov	r0, r1
	b	.L1366
.L1597:
	mov	r0, r7
	mov	r7, r3
.L1366:
	ldr	r3, [r7, #16]
	cmp	r2, r3
	ldrcs	r3, [r7, #12]
	ldrcc	r3, [r7, #8]
	movcs	r7, r0
	cmp	r3, #0
	bne	.L1597
	cmp	r8, r7
	beq	.L1368
	mov	r0, r8
.L1369:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r7, r0
	bne	.L1369
	ldr	r1, [r6, #0]
	str	r7, [sp, #12]
.L1368:
	ldr	r2, [r1, #8]
	cmp	r8, r2
	beq	.L1598
.L1371:
	ldr	ip, [sp, #12]
	cmp	r8, ip
	bne	.L1536
	b	.L1374
.L1377:
	mov	r8, r7
.L1536:
	mov	r0, r8
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	ip, [r6, #0]
	mov	r7, r0
	add	r3, ip, #12
	mov	r0, r8
	add	r1, ip, #4
	add	r2, ip, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L1376:
	ldr	r2, [r6, #4]
	ldr	r1, [sp, #12]
	sub	r3, r2, #1
	cmp	r7, r1
	str	r3, [r6, #4]
	bne	.L1377
.L1374:
	ldr	lr, [sl, #4]
	cmp	lr, #1
	beq	.L1599
	ldr	r0, [sl, #0]
	cmp	r0, #0
	beq	.L1347
	ldr	r3, [r0, #-4]
	rsb	r7, r3, r3, asl #3
	add	r7, r0, r7, asl #2
	b	.L1380
.L1600:
	ldr	r2, [r7, #-28]!
	mov	r0, r7
	ldr	ip, [r2, #4]
	mov	lr, pc
	bx	ip
.LEHE69:
	ldr	r0, [sl, #0]
.L1380:
	cmp	r0, r7
	bne	.L1600
	sub	r0, r0, #8
	bl	_ZdaPv
	b	.L1347
.L1581:
	ldr	r0, [sp, #152]
.LEHB70:
	bl	_ZN21QuStupidPointerHelper6removeEPv
	ldr	r2, [sp, #156]
	cmp	r2, #1
	ldr	r3, [sp, #152]
	beq	.L1601
	cmp	r3, #0
	beq	.L1466
	ldr	lr, [r3, #-4]
	add	r0, lr, lr, asl #1
	add	r0, r3, r0, asl #4
	cmp	r3, r0
	bne	.L1547
	b	.L1515
.L1602:
	mov	r0, r6
.L1547:
	sub	r6, r0, #48
	ldr	r3, [r0, #-48]
	mov	r0, r6
	ldr	ip, [r3, #4]
	mov	lr, pc
	bx	ip
	ldr	r0, [sp, #152]
	cmp	r0, r6
	bne	.L1602
.L1515:
	sub	r0, r0, #8
	bl	_ZdaPv
	b	.L1466
.L1601:
	cmp	r3, #0
	beq	.L1466
	mov	r0, r3
	ldr	r6, [r3, #0]
	ldr	ip, [r6, #8]
	mov	lr, pc
	bx	ip
.LEHE70:
	b	.L1466
.L1594:
	ldr	r3, [r9, #0]
	cmp	r3, #0
	beq	.L1406
	mov	r0, r3
	ldr	r1, [r3, #0]
.LEHB71:
	ldr	ip, [r1, #8]
	mov	lr, pc
	bx	ip
	b	.L1406
.L1599:
	ldr	r3, [sl, #0]
	cmp	r3, #0
	beq	.L1347
	mov	r0, r3
	ldr	ip, [r3, #0]
	ldr	ip, [ip, #8]
	mov	lr, pc
	bx	ip
	b	.L1347
.L1598:
	ldr	lr, [sp, #12]
	cmp	lr, r1
	bne	.L1371
	ldr	r0, [r6, #4]
	cmp	r0, #0
	beq	.L1374
	ldr	r3, [sp, #12]
	ldr	r0, [r3, #4]
	bl	T.932
	ldr	r0, [r6, #0]
	str	r0, [r0, #8]
	ldr	lr, [r6, #0]
	mov	r1, #0
	str	r1, [lr, #4]
	ldr	r2, [r6, #0]
	str	r2, [r2, #12]
	str	r1, [r6, #4]
	b	.L1374
.L1593:
	cmp	sl, r2
	bne	.L1430
	ldr	r2, [r6, #4]
	cmp	r2, #0
	beq	.L1433
	ldr	r0, [sl, #4]
	bl	T.932
.LEHE71:
	ldr	ip, [r6, #0]
	str	ip, [ip, #8]
	ldr	r1, [r6, #0]
	mov	r0, #0
	str	r0, [r1, #4]
	ldr	lr, [r6, #0]
	str	lr, [lr, #12]
	str	r0, [r6, #4]
	b	.L1433
.L1358:
	mov	r8, r1
	str	r1, [sp, #12]
	b	.L1368
.L1417:
	mov	r8, r2
	mov	sl, r2
	b	.L1427
.L1531:
	mov	r5, r0
.L1502:
	add	r0, sp, #152
	mov	r1, #0
	ldr	r2, [sp, #156]
	bl	_ZN15QuStupidPointerI10QuPngImageE3setEPS0_i
	mov	r0, r5
.LEHB72:
	bl	__cxa_end_cleanup
.L1529:
.L1519:
	mov	r8, r0
	mov	r0, r5
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r8
	bl	__cxa_end_cleanup
.LEHE72:
.L1523:
.L1561:
.L1260:
	mov	r4, r0
	mov	r0, r6
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
.L1262:
	mov	r0, r7
	mov	r1, #0
	ldr	r2, [sp, #140]
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
	mov	r0, r4
.LEHB73:
	bl	__cxa_end_cleanup
.LEHE73:
.L1530:
.L1467:
	ldr	ip, [sp, #144]
	cmp	ip, #0
	ldrne	r1, [r6, #0]
	mov	r5, r0
	ldr	r8, [sp, #148]
	ldrne	r2, [r1, #4]
	movne	r3, r1
	beq	.L1468
.L1469:
	cmp	r2, #0
	beq	.L1603
	ldr	r9, [r2, #16]
	cmp	ip, r9
	ldrhi	r9, [r2, #12]
	ldrls	r9, [r2, #8]
	movhi	r2, r3
	mov	r3, r2
	mov	r2, r9
	b	.L1469
.L1525:
	mov	r4, r0
	b	.L1262
.L1526:
.L1292:
	ldr	r3, [r5, #36]
	cmp	r3, #0
	mov	r4, r0
	movne	r0, r3
	blne	free
.L1293:
.L1527:
.L1294:
.L1528:
.L1296:
.L1532:
.L1321:
	ldr	r8, .L1610+4
	ldr	r1, [r5, #32]
	add	r0, r5, #28
	bl	T.933
	mov	r0, r5
	str	r8, [r5, #0]
	bl	_ZN11QuBaseImage7destroyEv
	mov	r0, r5
	bl	_ZdlPv
	mov	r0, r4
.LEHB74:
	bl	__cxa_end_cleanup
.LEHE74:
.L1524:
.L1261:
	mov	r4, r0
	mov	r0, r6
	bl	_ZN4_STL4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageEED1Ev
	b	.L1262
.L1522:
	b	.L1561
.L1611:
	.align	2
.L1610:
	.word	_ZTV10QuPngImage+8
	.word	_ZTV11QuBaseImage+8
	.word	_ZN21QuStupidPointerHelper6mSPMapE
.L1603:
	cmp	r3, r1
	beq	.L1475
	ldr	sl, [r3, #16]
	cmp	ip, sl
	mov	r2, r3
	bcs	.L1476
.L1475:
	add	r2, sp, #72
	add	r0, sp, #172
	add	r1, sp, #168
	mov	r4, #0
	str	ip, [sp, #72]
	str	r4, [sp, #76]
	str	r3, [sp, #168]
	bl	T.938
	ldr	r2, [sp, #172]
.L1476:
	ldr	r7, [r2, #20]
	sub	fp, r7, #1
	cmp	fp, #0
	str	fp, [r2, #20]
	beq	.L1604
.L1468:
	mov	r3, #0
	str	r3, [sp, #144]
	str	r8, [sp, #148]
	b	.L1502
.L1604:
	ldr	r7, [r6, #0]
	ldr	r3, [r7, #4]
	ldr	r1, [sp, #144]
	mov	r2, r3
	mov	sl, r7
.L1479:
	cmp	r2, #0
	beq	.L1605
	ldr	lr, [r2, #16]
	cmp	r1, lr
	ldrhi	lr, [r2, #12]
	ldrls	lr, [r2, #8]
	movhi	r2, sl
	mov	sl, r2
	mov	r2, lr
	b	.L1479
.L1605:
	mov	r9, sl
.L1484:
	cmp	r3, #0
	beq	.L1606
	ldr	ip, [r3, #16]
	cmp	r1, ip
	ldrcs	ip, [r3, #12]
	ldrcc	ip, [r3, #8]
	movcs	r3, r7
	mov	r7, r3
	mov	r3, ip
	b	.L1484
.L1606:
	mov	r0, sl
.L1489:
	cmp	r7, r0
	beq	.L1607
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	b	.L1489
.L1607:
	ldr	r3, [r6, #0]
	ldr	r2, [r3, #8]
	cmp	sl, r2
	mov	r4, r0
	ldr	sl, .L1610+8
	beq	.L1608
.L1538:
	cmp	r4, r9
	beq	.L1495
	mov	r0, r9
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	sl, [r6, #0]
	mov	r7, r0
	add	r3, sl, #12
	mov	r0, r9
	add	r1, sl, #4
	add	r2, sl, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L1497:
	ldr	lr, [r6, #4]
	sub	fp, lr, #1
	str	fp, [r6, #4]
	mov	r9, r7
	b	.L1538
.L1608:
	cmp	r7, r3
	bne	.L1538
	ldr	r6, [sl, #4]
	cmp	r6, #0
	beq	.L1495
	ldr	r0, [r7, #4]
	bl	T.932
	ldr	r4, [sl, #0]
	str	r4, [r4, #8]
	ldr	r9, [sl, #0]
	mov	r3, #0
	str	r3, [r9, #4]
	ldr	r0, [sl, #0]
	str	r0, [r0, #12]
	str	r3, [sl, #4]
.L1495:
	ldr	r7, [sp, #148]
	cmp	r7, #1
	beq	.L1609
	ldr	r0, [sp, #144]
	cmp	r0, #0
	beq	.L1468
	ldr	r1, [r0, #-4]
	mov	r4, #28
	mla	r4, r1, r4, r0
.L1500:
	cmp	r4, r0
	sub	r4, r4, #28
	beq	.L1499
	ldr	r6, [r4, #0]
	mov	r0, r4
	ldr	ip, [r6, #4]
	mov	lr, pc
	bx	ip
	ldr	r0, [sp, #144]
	b	.L1500
.L1499:
	sub	r0, r0, #8
	bl	_ZdaPv
	b	.L1468
.L1609:
	ldr	r3, [sp, #144]
	cmp	r3, #0
	beq	.L1468
	mov	r0, r3
	ldr	ip, [r3, #0]
	ldr	ip, [ip, #8]
	mov	lr, pc
	bx	ip
	b	.L1468
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA3193:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3193-.LLSDACSB3193
.LLSDACSB3193:
	.uleb128 .LEHB55-.LFB3193
	.uleb128 .LEHE55-.LEHB55
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB56-.LFB3193
	.uleb128 .LEHE56-.LEHB56
	.uleb128 .L1525-.LFB3193
	.uleb128 0x0
	.uleb128 .LEHB57-.LFB3193
	.uleb128 .LEHE57-.LEHB57
	.uleb128 .L1523-.LFB3193
	.uleb128 0x0
	.uleb128 .LEHB58-.LFB3193
	.uleb128 .LEHE58-.LEHB58
	.uleb128 .L1524-.LFB3193
	.uleb128 0x0
	.uleb128 .LEHB59-.LFB3193
	.uleb128 .LEHE59-.LEHB59
	.uleb128 .L1522-.LFB3193
	.uleb128 0x0
	.uleb128 .LEHB60-.LFB3193
	.uleb128 .LEHE60-.LEHB60
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB61-.LFB3193
	.uleb128 .LEHE61-.LEHB61
	.uleb128 .L1530-.LFB3193
	.uleb128 0x0
	.uleb128 .LEHB62-.LFB3193
	.uleb128 .LEHE62-.LEHB62
	.uleb128 .L1531-.LFB3193
	.uleb128 0x0
	.uleb128 .LEHB63-.LFB3193
	.uleb128 .LEHE63-.LEHB63
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB64-.LFB3193
	.uleb128 .LEHE64-.LEHB64
	.uleb128 .L1529-.LFB3193
	.uleb128 0x0
	.uleb128 .LEHB65-.LFB3193
	.uleb128 .LEHE65-.LEHB65
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB66-.LFB3193
	.uleb128 .LEHE66-.LEHB66
	.uleb128 .L1531-.LFB3193
	.uleb128 0x0
	.uleb128 .LEHB67-.LFB3193
	.uleb128 .LEHE67-.LEHB67
	.uleb128 .L1530-.LFB3193
	.uleb128 0x0
	.uleb128 .LEHB68-.LFB3193
	.uleb128 .LEHE68-.LEHB68
	.uleb128 .L1526-.LFB3193
	.uleb128 0x0
	.uleb128 .LEHB69-.LFB3193
	.uleb128 .LEHE69-.LEHB69
	.uleb128 .L1530-.LFB3193
	.uleb128 0x0
	.uleb128 .LEHB70-.LFB3193
	.uleb128 .LEHE70-.LEHB70
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB71-.LFB3193
	.uleb128 .LEHE71-.LEHB71
	.uleb128 .L1530-.LFB3193
	.uleb128 0x0
	.uleb128 .LEHB72-.LFB3193
	.uleb128 .LEHE72-.LEHB72
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB73-.LFB3193
	.uleb128 .LEHE73-.LEHB73
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB74-.LFB3193
	.uleb128 .LEHE74-.LEHB74
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE3193:
	.fnend
	.size	_ZN14QuImageManager11setFlyImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE, .-_ZN14QuImageManager11setFlyImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.section	.text._ZN12QuDrawObject9loadImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEEPfii,"ax",%progbits
	.align	2
	.global	_ZN12QuDrawObject9loadImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEEPfii
	.hidden	_ZN12QuDrawObject9loadImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEEPfii
	.type	_ZN12QuDrawObject9loadImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEEPfii, %function
_ZN12QuDrawObject9loadImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEEPfii:
	.fnstart
.LFB3704:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 152
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	r4, .L1899
	.pad #156
	sub	sp, sp, #156
	str	r4, [sp, #88]
	ldr	lr, [r4, #0]
	ldr	ip, [lr, #-12]
	add	ip, r4, ip
	ldr	r5, [ip, #8]
	cmp	r5, #0
	str	r3, [sp, #8]
	mov	r6, r0
	mov	r9, r1
	str	r2, [sp, #4]
	mov	r7, r4
	sub	lr, lr, #12
	movne	r5, r4
	movne	r3, #0
	bne	.L1614
	ldr	r3, [ip, #88]
	cmp	r3, #0
	mov	r5, r4
	beq	.L1863
.L1615:
	ldr	r8, [ip, #92]
	cmp	r8, #0
	beq	.L1617
	ldr	r3, [r8, #0]
	ldr	fp, [r3, #-12]
	add	sl, r8, fp
	ldr	r3, [sl, #88]
	cmp	r3, #0
	beq	.L1617
	mov	r0, r3
	ldr	ip, [r3, #0]
.LEHB75:
	ldr	ip, [ip, #20]
	mov	lr, pc
	bx	ip
.LEHE75:
	cmn	r0, #1
	beq	.L1618
.L1857:
	ldr	r8, [r4, #0]
	ldr	fp, [r8, #-12]
	sub	lr, r8, #12
	add	ip, r7, fp
.L1617:
	ldr	r3, [ip, #8]
	rsbs	r3, r3, #1
	movcc	r3, #0
.L1614:
	cmp	r3, #0
	strb	r3, [sp, #92]
	beq	.L1622
	ldr	ip, [lr, #0]
	add	r2, r7, ip
	ldr	r8, [r2, #28]
	cmp	r8, #36
	ble	.L1623
	ldr	r1, [r2, #4]
	and	lr, r1, #7
	cmp	lr, #1
	sub	r8, r8, #36
	beq	.L1864
	ldr	ip, [r2, #88]
	ldrb	r1, [r2, #84]	@ zero_extendqisi2
	mov	r0, ip
	ldr	r3, [ip, #0]
	mov	r2, r8
.LEHB76:
	ldr	ip, [r3, #48]
	mov	lr, pc
	bx	ip
.LEHE76:
	cmp	r8, r0
	beq	.L1865
.L1627:
	ldr	lr, [r4, #0]
	ldr	r2, [lr, #-12]
	mov	ip, #0
	add	r1, r7, r2
	str	ip, [r1, #28]
.L1822:
	ldr	lr, [r4, #0]
	ldr	r0, [lr, #-12]
	add	r0, r5, r0
	ldr	r8, [r0, #88]
	ldr	ip, [r0, #8]
	cmp	r8, #0
	ldr	r3, [r0, #20]
	orr	r1, ip, #4
	orreq	r1, ip, #5
	tst	r1, r3
	str	r1, [r0, #8]
	bne	.L1866
.L1622:
	ldr	r8, [sp, #88]
	ldr	r2, [r8, #0]
	ldr	fp, [r2, #-12]
	add	r3, r8, fp
	ldr	sl, [r3, #4]
	tst	sl, #8192
	bne	.L1867
.L1631:
	ldr	ip, [r4, #0]
	ldr	r3, [ip, #-12]
	add	r1, r5, r3
	ldr	r2, [r1, #64]
	mov	r1, #10
	ldr	sl, [r2, #0]
	mov	r0, r2
.LEHB77:
	ldr	ip, [sl, #24]
	mov	lr, pc
	bx	ip
	ldr	r3, [r4, #0]
	str	r4, [sp, #88]
	ldr	r2, [r3, #-12]
	mov	sl, r0
	add	r0, r7, r2
	ldr	fp, [r0, #8]
	cmp	fp, #0
	sub	r1, r3, #12
	movne	r3, #0
	bne	.L1634
	ldr	r8, [r0, #88]
	cmp	r8, #0
	beq	.L1868
.L1635:
	ldr	r8, [r0, #92]
	cmp	r8, #0
	beq	.L1637
	ldr	lr, [r8, #0]
	ldr	ip, [lr, #-12]
	add	r3, r8, ip
	ldr	r3, [r3, #88]
	cmp	r3, #0
	beq	.L1637
	mov	r0, r3
	ldr	r2, [r3, #0]
	ldr	ip, [r2, #20]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	beq	.L1638
.L1859:
	ldr	ip, [r4, #0]
	ldr	r2, [ip, #-12]
	sub	r1, ip, #12
.L1637:
	add	lr, r7, r2
	ldr	r7, [lr, #8]
	rsbs	r3, r7, #1
	movcc	r3, #0
.L1634:
	cmp	r3, #0
	strb	r3, [sp, #92]
	beq	.L1642
	ldr	fp, [r1, #0]
	add	r8, r5, fp
	ldr	r3, [r8, #88]
	ldr	r2, [r3, #20]
	ldr	r1, [r3, #24]
	cmp	r2, r1
	strccb	sl, [r2], #1
	strcc	r2, [r3, #20]
	bcs	.L1869
.L1644:
	ldr	r7, [sp, #88]
	ldr	r8, [r7, #0]
	ldr	r1, [r8, #-12]
	add	r3, r7, r1
	ldr	r2, [r3, #4]
	tst	r2, #8192
	bne	.L1870
.L1646:
	ldr	r2, [r4, #0]
	ldr	r7, [r2, #-12]
	add	r4, r5, r7
	ldr	r3, [r4, #88]
	cmp	r3, #0
	beq	.L1649
	mov	r0, r3
	ldr	r1, [r3, #0]
	ldr	ip, [r1, #20]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	beq	.L1871
.L1649:
	ldr	sl, .L1899+4
	ldr	r4, [sl, #0]
	cmp	r4, #0
	beq	.L1872
.L1662:
.L1652:
	ldr	r5, [r4, #12]
	add	r8, sp, #44
	mov	r1, r9
	mov	r0, r8
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
.LEHE77:
	mov	r1, r5
	add	r0, sp, #112
	mov	r2, r8
.LEHB78:
	bl	_ZN14QuImageManager11setFlyImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	ldr	ip, [sp, #112]
	cmp	ip, #0
	ldr	fp, [sp, #116]
	beq	.L1663
	ldr	r7, .L1899+12
	ldr	r0, [r7, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L1667
	mov	r1, r0
	b	.L1668
.L1873:
	mov	r1, r3
	mov	r3, r2
.L1668:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L1873
.L1667:
	cmp	r3, r0
	beq	.L1670
	ldr	r0, [r3, #16]
	cmp	ip, r0
	mov	r2, r3
	bcs	.L1671
.L1670:
	str	ip, [sp, #80]
	add	r0, sp, #148
	mov	ip, #0
	add	r1, sp, #144
	add	r2, sp, #80
	str	ip, [sp, #84]
	str	r3, [sp, #144]
	bl	T.938
.LEHE78:
	ldr	r2, [sp, #148]
.L1671:
	ldr	r4, [r2, #20]
	sub	r3, r4, #1
	cmp	r3, #0
	str	r3, [r2, #20]
	beq	.L1874
.L1663:
	ldr	r0, [sp, #44]
	mov	r8, #0
	cmp	r0, #0
	str	r8, [sp, #112]
	str	fp, [sp, #116]
	blne	free
.L1699:
	ldr	r4, [sl, #0]
	cmp	r4, #0
	beq	.L1875
.L1701:
	add	r7, sp, #32
	mov	r1, r9
	mov	r0, r7
	add	r5, sp, #20
	ldr	r4, [r4, #12]
.LEHB79:
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
.LEHE79:
	mov	r0, r5
	mov	r1, r7
.LEHB80:
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
.LEHE80:
	add	r8, sp, #104
	mov	r1, r4
	mov	r0, r8
	mov	r2, r5
.LEHB81:
	bl	_ZN14QuImageManager11setFlyImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
.LEHE81:
	ldr	r0, [sp, #20]
	cmp	r0, #0
	blne	free
.L1714:
	mov	r0, r8
.LEHB82:
	bl	_ZN10QuPngImage18finishLoadingImageEv
.LEHE82:
	ldr	r0, [sp, #32]
	cmp	r0, #0
	blne	free
.L1719:
	ldr	r4, [sp, #104]
	ldr	r2, [sp, #108]
	cmp	r4, #0
	str	r4, [sp, #96]
	str	r2, [sp, #100]
	moveq	r1, r4
	beq	.L1721
	ldr	r5, .L1899+12
	ldr	r3, [r5, #0]
	ldr	ip, [r3, #4]
	cmp	ip, #0
	beq	.L1722
	mov	r0, r3
	mov	r2, ip
	b	.L1726
.L1876:
	mov	r0, r2
	mov	r2, r1
.L1726:
	ldr	r1, [r2, #16]
	cmp	r4, r1
	ldrhi	r1, [r2, #12]
	ldrls	r1, [r2, #8]
	movhi	r2, r0
	cmp	r1, #0
	bne	.L1876
	cmp	r3, r2
	beq	.L1722
	ldr	sl, [r2, #16]
	cmp	r4, sl
	bcc	.L1722
.L1727:
	cmp	r2, r3
	mov	r1, r2
	beq	.L1860
.L1729:
	ldr	sl, [r1, #20]
	add	r3, sl, #1
	str	r3, [r1, #20]
	add	r1, sp, #96
	ldmia	r1, {r1, r2}	@ phole ldm
.L1721:
	add	r0, r6, #124
.LEHB83:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE83:
	ldr	r2, [sp, #4]
	mov	r1, #1
	cmp	r2, #0
	strb	r1, [r6, #120]
	beq	.L1877
.L1745:
	ldrb	r0, [r6, #36]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L1824
	ldr	r0, [r6, #44]
	cmp	r0, #0
	blne	_ZdaPv
.L1747:
	mov	lr, #0
	strb	lr, [r6, #36]
	str	lr, [r6, #44]
.L1824:
	mov	lr, #1
	ldr	r3, [sp, #96]
	strb	lr, [r6, #36]
	ldr	r0, [sp, #4]
	str	r0, [r6, #44]
	ldr	ip, [sp, #8]
	str	ip, [r6, #48]
	ldr	r1, [sp, #192]
	mov	sl, #32768
	add	r2, sl, #120
	mov	sl, #2
	str	r1, [r6, #52]
	str	r2, [r6, #56]
	str	sl, [r6, #60]
	cmp	r3, #0
	ldr	sl, [sp, #100]
	beq	.L1749
	ldr	r6, .L1899+12
	ldr	r0, [r6, #0]
	ldr	ip, [r0, #4]
	cmp	ip, #0
	moveq	ip, r0
	beq	.L1753
	mov	r1, r0
	b	.L1754
.L1878:
	mov	r1, ip
	mov	ip, r2
.L1754:
	ldr	r2, [ip, #16]
	cmp	r3, r2
	ldrhi	r2, [ip, #12]
	ldrls	r2, [ip, #8]
	movhi	ip, r1
	cmp	r2, #0
	bne	.L1878
.L1753:
	cmp	ip, r0
	beq	.L1756
	ldr	r2, [ip, #16]
	cmp	r3, r2
	mov	r2, ip
	bcs	.L1757
.L1756:
	str	r3, [sp, #64]
	add	r0, sp, #132
	mov	r3, #0
	add	r1, sp, #128
	add	r2, sp, #64
	str	r3, [sp, #68]
	str	ip, [sp, #128]
.LEHB84:
	bl	T.938
.LEHE84:
	ldr	r2, [sp, #132]
.L1757:
	ldr	ip, [r2, #20]
	sub	r1, ip, #1
	cmp	r1, #0
	str	r1, [r2, #20]
	beq	.L1879
.L1749:
	ldr	ip, [sp, #104]
	cmp	ip, #0
	beq	.L1784
	ldr	r4, .L1899+12
	mov	lr, #0
	ldr	r0, [r4, #0]
	str	lr, [sp, #96]
	str	sl, [sp, #100]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L1792
	mov	r1, r0
	b	.L1793
.L1880:
	mov	r1, r3
	mov	r3, r2
.L1793:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L1880
.L1792:
	cmp	r3, r0
	beq	.L1795
	ldr	r0, [r3, #16]
	cmp	ip, r0
	mov	r2, r3
	bcs	.L1796
.L1795:
	add	r2, sp, #56
	str	ip, [sp, #56]
	add	r0, sp, #124
	mov	ip, #0
	add	r1, sp, #120
	str	ip, [sp, #60]
	str	r3, [sp, #120]
.LEHB85:
	bl	T.938
.LEHE85:
	ldr	r2, [sp, #124]
.L1796:
	ldr	r3, [r2, #20]
	sub	r1, r3, #1
	cmp	r1, #0
	str	r1, [r2, #20]
	beq	.L1881
.L1784:
	mov	r0, #1
	add	sp, sp, #156
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L1882:
	ldr	r1, [ip, #16]
	cmp	r4, r1
	movls	r3, ip
	ldrhi	ip, [ip, #12]
	ldrls	ip, [ip, #8]
.L1860:
	cmp	ip, #0
	bne	.L1882
	cmp	r2, r3
	beq	.L1735
	ldr	r2, [r3, #16]
	cmp	r4, r2
	mov	r2, r3
	bcs	.L1736
.L1735:
	mov	ip, #0
	add	r0, sp, #140
	add	r1, sp, #136
	add	r2, sp, #72
	str	ip, [sp, #76]
	str	r3, [sp, #136]
	str	r4, [sp, #72]
.LEHB86:
	bl	T.938
.LEHE86:
	ldr	r2, [sp, #140]
.L1736:
	mov	r0, #0
	str	r0, [r2, #20]
	ldr	r0, [r5, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	beq	.L1739
	mov	r1, r0
	b	.L1743
.L1883:
	mov	r1, r3
	mov	r3, r2
.L1743:
	ldr	r2, [r3, #16]
	cmp	r4, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L1883
	cmp	r0, r3
	beq	.L1739
	ldr	lr, [r3, #16]
	cmp	r4, lr
	bcs	.L1744
.L1739:
	mov	r3, r0
.L1744:
	mov	r1, r3
	b	.L1729
.L1869:
	mov	r1, sl
	mov	r0, r3
	ldr	sl, [r3, #0]
.LEHB87:
	ldr	ip, [sl, #52]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	bne	.L1644
	ldr	r0, [r4, #0]
	sub	r1, r0, #12
.L1642:
	ldr	lr, [r1, #0]
	add	r0, r5, lr
	ldr	r7, [r0, #8]
	ldr	ip, [r0, #20]
	orr	r3, r7, #1
	tst	r3, ip
	str	r3, [r0, #8]
	beq	.L1644
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
.LEHE87:
	ldr	r7, [sp, #88]
	ldr	r8, [r7, #0]
	ldr	r1, [r8, #-12]
	add	r3, r7, r1
	ldr	r2, [r3, #4]
	tst	r2, #8192
	beq	.L1646
.L1870:
	ldr	r3, [r3, #88]
	cmp	r3, #0
	beq	.L1646
	mov	r0, r3
	ldr	fp, [r3, #0]
.LEHB88:
	ldr	ip, [fp, #20]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	bne	.L1646
	ldr	lr, [r7, #0]
	ldr	r0, [lr, #-12]
	add	r0, r7, r0
	ldr	r3, [r0, #8]
	ldr	ip, [r0, #20]
	orr	sl, r3, #1
	tst	sl, ip
	str	sl, [r0, #8]
	beq	.L1646
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
.LEHE88:
	b	.L1646
.L1623:
	ldr	r8, [r2, #88]
	ldr	r1, .L1899+8
	mov	r0, r8
	ldr	sl, [r8, #0]
	mov	r2, #36
.LEHB89:
	ldr	ip, [sl, #44]
	mov	lr, pc
	bx	ip
.LEHE89:
	ldr	r3, [r4, #0]
	ldr	fp, [r3, #-12]
	cmp	r0, #36
	mov	r2, #0
	add	r0, r7, fp
	str	r2, [r0, #28]
	bne	.L1822
	ldr	r8, [sp, #88]
	ldr	r2, [r8, #0]
	ldr	fp, [r2, #-12]
	add	r3, r8, fp
	ldr	sl, [r3, #4]
	tst	sl, #8192
	beq	.L1631
.L1867:
	ldr	r3, [r3, #88]
	cmp	r3, #0
	beq	.L1631
	mov	r0, r3
	ldr	r1, [r3, #0]
.LEHB90:
	ldr	ip, [r1, #20]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	bne	.L1631
	ldr	lr, [r8, #0]
	ldr	r0, [lr, #-12]
	add	r0, r8, r0
	ldr	r8, [r0, #8]
	ldr	ip, [r0, #20]
	orr	r3, r8, #1
	tst	r3, ip
	str	r3, [r0, #8]
	beq	.L1631
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
.LEHE90:
	b	.L1631
.L1722:
	mov	r2, r3
	b	.L1727
.L1874:
	ldr	r1, [r7, #0]
	ldr	r4, [r1, #4]
	cmp	r4, #0
	ldr	r2, [sp, #112]
	beq	.L1674
	mov	r0, r1
	mov	r5, r4
	b	.L1678
.L1884:
	mov	r0, r5
	mov	r5, r3
.L1678:
	ldr	r3, [r5, #16]
	cmp	r2, r3
	ldrhi	r3, [r5, #12]
	ldrls	r3, [r5, #8]
	movhi	r5, r0
	cmp	r3, #0
	bne	.L1884
	str	r5, [sp, #12]
	mov	r0, r1
	b	.L1682
.L1885:
	mov	r0, r4
	mov	r4, r3
.L1682:
	ldr	r3, [r4, #16]
	cmp	r2, r3
	ldrcs	r3, [r4, #12]
	ldrcc	r3, [r4, #8]
	movcs	r4, r0
	cmp	r3, #0
	bne	.L1885
	cmp	r5, r4
	beq	.L1684
	mov	r0, r5
.L1685:
.LEHB91:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r4, r0
	bne	.L1685
	ldr	r1, [r7, #0]
	str	r4, [sp, #12]
.L1684:
	ldr	lr, [r1, #8]
	cmp	r5, lr
	beq	.L1886
.L1687:
	ldr	r0, [sp, #12]
	cmp	r0, r5
	bne	.L1842
	b	.L1690
.L1693:
	mov	r5, r4
.L1842:
	mov	r0, r5
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r2, [r7, #0]
	mov	r4, r0
	add	r3, r2, #12
	add	r1, r2, #4
	mov	r0, r5
	add	r2, r2, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L1692:
	ldr	r0, [r7, #4]
	ldr	r5, [sp, #12]
	sub	r1, r0, #1
	cmp	r5, r4
	str	r1, [r7, #4]
	bne	.L1693
.L1690:
	ldr	r7, [sp, #116]
	cmp	r7, #1
	ldr	r3, [sp, #112]
	beq	.L1887
	cmp	r3, #0
	beq	.L1663
	ldr	lr, [r3, #-4]
	rsb	r4, lr, lr, asl #3
	add	r4, r3, r4, asl #2
	b	.L1696
.L1888:
	ldr	r3, [r4, #-28]!
	mov	r0, r4
	ldr	ip, [r3, #4]
	mov	lr, pc
	bx	ip
.LEHE91:
	ldr	r3, [sp, #112]
.L1696:
	cmp	r4, r3
	bne	.L1888
	sub	r0, r4, #8
	bl	_ZdaPv
	b	.L1663
.L1879:
	ldr	r1, [r6, #0]
	ldr	r4, [r1, #4]
	cmp	r4, #0
	ldr	r2, [sp, #96]
	beq	.L1760
	mov	r0, r1
	mov	r5, r4
	b	.L1764
.L1889:
	mov	r0, r5
	mov	r5, r3
.L1764:
	ldr	r3, [r5, #16]
	cmp	r2, r3
	ldrhi	r3, [r5, #12]
	ldrls	r3, [r5, #8]
	movhi	r5, r0
	cmp	r3, #0
	bne	.L1889
	mov	r7, r5
	mov	r0, r1
	b	.L1768
.L1890:
	mov	r0, r4
	mov	r4, r3
.L1768:
	ldr	r3, [r4, #16]
	cmp	r2, r3
	ldrcs	r3, [r4, #12]
	ldrcc	r3, [r4, #8]
	movcs	r4, r0
	cmp	r3, #0
	bne	.L1890
	cmp	r5, r4
	beq	.L1770
	mov	r0, r5
.L1771:
.LEHB92:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r4, r0
	bne	.L1771
	ldr	r1, [r6, #0]
	mov	r7, r4
.L1770:
	ldr	r0, [r1, #8]
	cmp	r5, r0
	beq	.L1891
.L1773:
	cmp	r5, r7
	bne	.L1843
	b	.L1776
.L1779:
	mov	r5, r4
.L1843:
	mov	r0, r5
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	lr, [r6, #0]
	mov	r4, r0
	add	r3, lr, #12
	mov	r0, r5
	add	r1, lr, #4
	add	r2, lr, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L1778:
	ldr	r3, [r6, #4]
	cmp	r7, r4
	sub	r0, r3, #1
	str	r0, [r6, #4]
	bne	.L1779
.L1776:
	ldr	r2, [sp, #100]
	cmp	r2, #1
	ldr	r3, [sp, #96]
	beq	.L1892
	cmp	r3, #0
	beq	.L1749
	ldr	ip, [r3, #-4]
	rsb	r4, ip, ip, asl #3
	add	r4, r3, r4, asl #2
	b	.L1782
.L1893:
	ldr	r3, [r4, #-28]!
	mov	r0, r4
	ldr	ip, [r3, #4]
	mov	lr, pc
	bx	ip
.LEHE92:
	ldr	r3, [sp, #96]
.L1782:
	cmp	r4, r3
	bne	.L1893
	sub	r0, r4, #8
	bl	_ZdaPv
	b	.L1749
.L1881:
	ldr	r0, [r4, #0]
	ldr	r5, [r0, #4]
	cmp	r5, #0
	ldr	r2, [sp, #104]
	moveq	r6, r0
	moveq	r7, r0
	beq	.L1809
	mov	r1, r0
	mov	r7, r5
	b	.L1803
.L1894:
	mov	r1, r7
	mov	r7, r3
.L1803:
	ldr	r3, [r7, #16]
	cmp	r2, r3
	ldrhi	r3, [r7, #12]
	ldrls	r3, [r7, #8]
	movhi	r7, r1
	cmp	r3, #0
	bne	.L1894
	mov	r6, r7
	mov	r1, r0
	b	.L1807
.L1895:
	mov	r1, r5
	mov	r5, r3
.L1807:
	ldr	r3, [r5, #16]
	cmp	r2, r3
	ldrcs	r3, [r5, #12]
	ldrcc	r3, [r5, #8]
	movcs	r5, r1
	cmp	r3, #0
	bne	.L1895
	cmp	r7, r5
	beq	.L1809
	mov	r0, r7
.L1810:
.LEHB93:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r5, r0
	bne	.L1810
	ldr	r0, [r4, #0]
	mov	r6, r5
.L1809:
	ldr	r2, [r0, #8]
	cmp	r7, r2
	beq	.L1896
.L1812:
	cmp	r6, r7
	bne	.L1844
	b	.L1815
.L1818:
	mov	r7, r5
.L1844:
	mov	r0, r7
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r2, [r4, #0]
	mov	r5, r0
	add	r3, r2, #12
	add	r1, r2, #4
	mov	r0, r7
	add	r2, r2, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L1817:
	ldr	r0, [r4, #4]
	cmp	r6, r5
	sub	lr, r0, #1
	str	lr, [r4, #4]
	bne	.L1818
.L1815:
	ldr	ip, [sp, #108]
	cmp	ip, #1
	ldr	r3, [sp, #104]
	beq	.L1897
	cmp	r3, #0
	beq	.L1784
	ldr	lr, [r3, #-4]
	rsb	r2, lr, lr, asl #3
	add	r0, r3, r2, asl #2
	cmp	r3, r0
	bne	.L1849
	b	.L1820
.L1898:
	mov	r0, r4
.L1849:
	sub	r4, r0, #28
	ldr	r3, [r0, #-28]
	mov	r0, r4
	ldr	ip, [r3, #4]
	mov	lr, pc
	bx	ip
.LEHE93:
	ldr	r0, [sp, #104]
	cmp	r0, r4
	bne	.L1898
.L1820:
	sub	r0, r0, #8
	bl	_ZdaPv
	b	.L1784
.L1866:
.LEHB94:
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
	b	.L1622
.L1864:
	ldr	lr, [r2, #88]
	ldr	r1, .L1899+8
	mov	r0, lr
	ldr	ip, [lr, #0]
	mov	r2, #36
	ldr	ip, [ip, #44]
	mov	lr, pc
	bx	ip
	cmp	r0, #36
	bne	.L1627
	ldr	r0, [r4, #0]
	ldr	r1, [r0, #-12]
	add	r2, r5, r1
	ldr	fp, [r2, #88]
	ldrb	r1, [r2, #84]	@ zero_extendqisi2
	mov	r0, fp
	ldr	sl, [fp, #0]
	mov	r2, r8
	ldr	ip, [sl, #48]
	mov	lr, pc
	bx	ip
.LEHE94:
	cmp	r8, r0
	bne	.L1627
.L1628:
	ldr	r0, [r4, #0]
	ldr	sl, [r0, #-12]
	mov	fp, #0
	add	r8, r7, sl
	str	fp, [r8, #28]
	b	.L1622
.L1871:
	ldr	r0, .L1899
	ldr	r3, [r0, #0]
	ldr	ip, [r3, #-12]
	add	r0, r5, ip
	ldr	sl, [r0, #8]
	ldr	fp, [r0, #20]
	orr	r8, sl, #1
	tst	r8, fp
	str	r8, [r0, #8]
	beq	.L1649
.LEHB95:
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
	b	.L1649
.L1875:
	mov	r0, #60
	bl	_Znwj
.LEHE95:
	mov	r2, #0
	add	r8, r0, #4
	mov	r5, r0
	str	r2, [r0, #4]
	strb	r2, [r0, #0]
	strb	r2, [r0, #1]
	mov	r1, #1
	mov	r0, r8
	mov	r4, r5
.LEHB96:
	bl	T.937
.LEHE96:
	mov	r1, #0
	add	r7, r5, #12
	str	r1, [r5, #12]
	mov	r0, r7
	mov	r1, #1
.LEHB97:
	bl	T.935
.LEHE97:
	mov	r0, r5
	mov	ip, #0
	str	ip, [r0, #20]!
.LEHB98:
	bl	T.936
.LEHE98:
	mov	r0, #0
	mov	r7, #0
	mov	r3, #30
	mov	lr, #1065353216
	str	r3, [r5, #40]
	str	r0, [r5, #48]	@ float
	str	lr, [r5, #52]	@ float
	str	r7, [r5, #56]
	str	r7, [r5, #36]
	str	r0, [r5, #44]	@ float
	str	r5, [sl, #0]
	b	.L1701
.L1872:
	mov	r0, #60
.LEHB99:
	bl	_Znwj
.LEHE99:
	mov	r4, #0
	add	r8, r0, #4
	mov	r5, r0
	str	r4, [r0, #4]
	strb	r4, [r0, #0]
	strb	r4, [r0, #1]
	mov	r1, #1
	mov	r0, r8
	mov	r4, r5
.LEHB100:
	bl	T.937
.LEHE100:
	add	r7, r5, #12
	mov	lr, #0
	str	lr, [r5, #12]
	mov	r0, r7
	mov	r1, #1
.LEHB101:
	bl	T.935
.LEHE101:
	mov	r0, r5
	mov	r2, #0
	str	r2, [r0, #20]!
.LEHB102:
	bl	T.936
.LEHE102:
	mov	r7, #0
	mov	r8, #0
	mov	fp, #30
	mov	r1, #1065353216
	str	fp, [r5, #40]
	str	r7, [r5, #48]	@ float
	str	r1, [r5, #52]	@ float
	str	r8, [r5, #56]
	str	r8, [r5, #36]
	str	r7, [r5, #44]	@ float
	str	r5, [sl, #0]
	b	.L1652
.L1877:
	mov	ip, #4
	str	ip, [r6, #4]
	ldr	r0, [r6, #124]
.LEHB103:
	bl	_ZN11QuBaseImage9getUVDataEv
.LEHE103:
	str	r0, [sp, #4]
	b	.L1745
.L1892:
	cmp	r3, #0
	beq	.L1749
	mov	r0, r3
	ldr	r1, [r3, #0]
.LEHB104:
	ldr	ip, [r1, #8]
	mov	lr, pc
	bx	ip
.LEHE104:
	b	.L1749
.L1887:
	cmp	r3, #0
	beq	.L1663
	mov	r0, r3
	ldr	ip, [r3, #0]
.LEHB105:
	ldr	ip, [ip, #8]
	mov	lr, pc
	bx	ip
.LEHE105:
	b	.L1663
.L1897:
	cmp	r3, #0
	beq	.L1784
	mov	r0, r3
	ldr	r1, [r3, #0]
.LEHB106:
	ldr	ip, [r1, #8]
	mov	lr, pc
	bx	ip
.LEHE106:
	b	.L1784
.L1865:
	ldr	r2, [r4, #0]
	ldr	fp, [r2, #-12]
	add	sl, r5, fp
	ldr	lr, [sl, #88]
	ldr	r1, .L1899+8
	mov	r0, lr
	ldr	r8, [lr, #0]
	mov	r2, #36
.LEHB107:
	ldr	ip, [r8, #44]
	mov	lr, pc
	bx	ip
.LEHE107:
	cmp	r0, #36
	bne	.L1627
	b	.L1628
.L1638:
	ldr	r3, [r8, #0]
	ldr	r0, [r3, #-12]
	add	r0, r8, r0
	ldr	fp, [r0, #8]
	ldr	r8, [r0, #20]
	orr	r1, fp, #1
	tst	r1, r8
	str	r1, [r0, #8]
	beq	.L1859
.LEHB108:
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
	b	.L1859
.L1618:
	ldr	sl, [r8, #0]
	ldr	r0, [sl, #-12]
	add	r0, r8, r0
	ldr	r1, [r0, #8]
	ldr	r2, [r0, #20]
	orr	lr, r1, #1
	tst	lr, r2
	str	lr, [r0, #8]
	beq	.L1857
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
	b	.L1857
.L1900:
	.align	2
.L1899:
	.word	_ZN4_STL4coutE
	.word	_ZN8QuGlobal5mSelfE
	.word	.LC1
	.word	_ZN21QuStupidPointerHelper6mSPMapE
.L1868:
	ldr	fp, [r0, #20]
	mov	lr, #1
	tst	lr, fp
	str	lr, [r0, #8]
	blne	_ZN4_STL8ios_base16_M_throw_failureEv
.L1858:
	ldr	r0, [r4, #0]
	ldr	r2, [r0, #-12]
	sub	r1, r0, #12
	add	r0, r5, r2
	b	.L1635
.L1863:
	ldr	r2, [ip, #20]
	mov	r0, #1
	tst	r0, r2
	str	r0, [ip, #8]
	movne	r0, ip
	blne	_ZN4_STL8ios_base16_M_throw_failureEv
.L1856:
	ldr	r1, [r4, #0]
	ldr	r8, [r1, #-12]
	sub	lr, r1, #12
	add	ip, r4, r8
	b	.L1615
.L1896:
	cmp	r6, r0
	bne	.L1812
	ldr	lr, [r4, #4]
	cmp	lr, #0
	ldr	r4, .L1899+12
	beq	.L1815
	ldr	r0, [r6, #4]
	bl	T.932
.LEHE108:
	ldr	r3, [r4, #0]
	str	r3, [r3, #8]
	ldr	r1, [r4, #0]
	mov	r0, #0
	str	r0, [r1, #4]
	ldr	ip, [r4, #0]
	str	ip, [ip, #12]
	str	r0, [r4, #4]
	b	.L1815
.L1760:
	mov	r5, r1
	mov	r7, r1
	b	.L1770
.L1674:
	str	r1, [sp, #12]
	mov	r5, r1
	b	.L1684
.L1886:
	ldr	r2, [sp, #12]
	cmp	r2, r1
	bne	.L1687
	ldr	r1, [r7, #4]
	cmp	r1, #0
	beq	.L1690
	ldr	r5, [sp, #12]
	ldr	r0, [r5, #4]
.LEHB109:
	bl	T.932
.LEHE109:
	ldr	lr, [r7, #0]
	str	lr, [lr, #8]
	ldr	r4, [r7, #0]
	mov	ip, #0
	str	ip, [r4, #4]
	ldr	r3, [r7, #0]
	str	r3, [r3, #12]
	str	ip, [r7, #4]
	b	.L1690
.L1891:
	cmp	r7, r1
	bne	.L1773
	ldr	lr, [r6, #4]
	cmp	lr, #0
	beq	.L1776
	ldr	r0, [r7, #4]
.LEHB110:
	bl	T.932
.LEHE110:
	ldr	ip, [r6, #0]
	str	ip, [ip, #8]
	ldr	r1, [r6, #0]
	mov	r3, #0
	str	r3, [r1, #4]
	ldr	r2, [r6, #0]
	str	r2, [r2, #12]
	str	r3, [r6, #4]
	b	.L1776
.L1829:
	mov	r4, r0
.L1660:
	mov	r0, r8
	ldr	r1, [r5, #8]
	bl	T.937
.L1711:
	mov	r0, r5
	bl	_ZdlPv
	mov	r0, r4
.LEHB111:
	bl	__cxa_end_cleanup
.LEHE111:
.L1830:
.L1861:
	mov	r4, r0
	b	.L1711
.L1828:
.L1658:
	mov	r4, r0
	ldr	r1, [r5, #16]
	mov	r0, r7
	bl	T.935
	b	.L1660
.L1836:
	mov	r4, r0
.L1787:
	mov	r0, r8
	mov	r1, #0
	ldr	r2, [sp, #108]
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
	mov	r0, r4
.LEHB112:
	bl	__cxa_end_cleanup
.LEHE112:
.L1831:
.L1650:
	mov	r5, r0
	add	r0, sp, #88
	bl	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	mov	r0, r5
.LEHB113:
	bl	__cxa_end_cleanup
.L1838:
.L1700:
	mov	r6, r0
	mov	r0, r8
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r6
	bl	__cxa_end_cleanup
.LEHE113:
.L1827:
	b	.L1861
.L1832:
.L1630:
	mov	r4, r0
	add	r0, sp, #88
	bl	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	mov	r0, r4
.LEHB114:
	bl	__cxa_end_cleanup
.LEHE114:
.L1825:
.L1707:
	mov	r4, r0
	ldr	r1, [r5, #16]
	mov	r0, r7
	bl	T.935
.L1709:
	mov	r0, r8
	ldr	r1, [r5, #8]
	bl	T.937
	b	.L1711
.L1826:
	mov	r4, r0
	b	.L1709
.L1837:
	mov	r4, r0
.L1718:
	mov	r0, r7
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r4
.LEHB115:
	bl	__cxa_end_cleanup
.LEHE115:
.L1833:
.L1716:
	mov	r4, r0
	mov	r1, #0
	mov	r0, r8
	ldr	r2, [sp, #108]
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
	b	.L1718
.L1834:
.L1713:
	mov	r4, r0
	mov	r0, r5
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	b	.L1718
.L1835:
.L1785:
	mov	r4, r0
	mov	r1, #0
	add	r0, sp, #96
	ldr	r2, [sp, #100]
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
	b	.L1787
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA3704:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3704-.LLSDACSB3704
.LLSDACSB3704:
	.uleb128 .LEHB75-.LFB3704
	.uleb128 .LEHE75-.LEHB75
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB76-.LFB3704
	.uleb128 .LEHE76-.LEHB76
	.uleb128 .L1832-.LFB3704
	.uleb128 0x0
	.uleb128 .LEHB77-.LFB3704
	.uleb128 .LEHE77-.LEHB77
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB78-.LFB3704
	.uleb128 .LEHE78-.LEHB78
	.uleb128 .L1838-.LFB3704
	.uleb128 0x0
	.uleb128 .LEHB79-.LFB3704
	.uleb128 .LEHE79-.LEHB79
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB80-.LFB3704
	.uleb128 .LEHE80-.LEHB80
	.uleb128 .L1837-.LFB3704
	.uleb128 0x0
	.uleb128 .LEHB81-.LFB3704
	.uleb128 .LEHE81-.LEHB81
	.uleb128 .L1834-.LFB3704
	.uleb128 0x0
	.uleb128 .LEHB82-.LFB3704
	.uleb128 .LEHE82-.LEHB82
	.uleb128 .L1833-.LFB3704
	.uleb128 0x0
	.uleb128 .LEHB83-.LFB3704
	.uleb128 .LEHE83-.LEHB83
	.uleb128 .L1835-.LFB3704
	.uleb128 0x0
	.uleb128 .LEHB84-.LFB3704
	.uleb128 .LEHE84-.LEHB84
	.uleb128 .L1836-.LFB3704
	.uleb128 0x0
	.uleb128 .LEHB85-.LFB3704
	.uleb128 .LEHE85-.LEHB85
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB86-.LFB3704
	.uleb128 .LEHE86-.LEHB86
	.uleb128 .L1836-.LFB3704
	.uleb128 0x0
	.uleb128 .LEHB87-.LFB3704
	.uleb128 .LEHE87-.LEHB87
	.uleb128 .L1831-.LFB3704
	.uleb128 0x0
	.uleb128 .LEHB88-.LFB3704
	.uleb128 .LEHE88-.LEHB88
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB89-.LFB3704
	.uleb128 .LEHE89-.LEHB89
	.uleb128 .L1832-.LFB3704
	.uleb128 0x0
	.uleb128 .LEHB90-.LFB3704
	.uleb128 .LEHE90-.LEHB90
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB91-.LFB3704
	.uleb128 .LEHE91-.LEHB91
	.uleb128 .L1838-.LFB3704
	.uleb128 0x0
	.uleb128 .LEHB92-.LFB3704
	.uleb128 .LEHE92-.LEHB92
	.uleb128 .L1836-.LFB3704
	.uleb128 0x0
	.uleb128 .LEHB93-.LFB3704
	.uleb128 .LEHE93-.LEHB93
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB94-.LFB3704
	.uleb128 .LEHE94-.LEHB94
	.uleb128 .L1832-.LFB3704
	.uleb128 0x0
	.uleb128 .LEHB95-.LFB3704
	.uleb128 .LEHE95-.LEHB95
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB96-.LFB3704
	.uleb128 .LEHE96-.LEHB96
	.uleb128 .L1827-.LFB3704
	.uleb128 0x0
	.uleb128 .LEHB97-.LFB3704
	.uleb128 .LEHE97-.LEHB97
	.uleb128 .L1826-.LFB3704
	.uleb128 0x0
	.uleb128 .LEHB98-.LFB3704
	.uleb128 .LEHE98-.LEHB98
	.uleb128 .L1825-.LFB3704
	.uleb128 0x0
	.uleb128 .LEHB99-.LFB3704
	.uleb128 .LEHE99-.LEHB99
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB100-.LFB3704
	.uleb128 .LEHE100-.LEHB100
	.uleb128 .L1830-.LFB3704
	.uleb128 0x0
	.uleb128 .LEHB101-.LFB3704
	.uleb128 .LEHE101-.LEHB101
	.uleb128 .L1829-.LFB3704
	.uleb128 0x0
	.uleb128 .LEHB102-.LFB3704
	.uleb128 .LEHE102-.LEHB102
	.uleb128 .L1828-.LFB3704
	.uleb128 0x0
	.uleb128 .LEHB103-.LFB3704
	.uleb128 .LEHE103-.LEHB103
	.uleb128 .L1835-.LFB3704
	.uleb128 0x0
	.uleb128 .LEHB104-.LFB3704
	.uleb128 .LEHE104-.LEHB104
	.uleb128 .L1836-.LFB3704
	.uleb128 0x0
	.uleb128 .LEHB105-.LFB3704
	.uleb128 .LEHE105-.LEHB105
	.uleb128 .L1838-.LFB3704
	.uleb128 0x0
	.uleb128 .LEHB106-.LFB3704
	.uleb128 .LEHE106-.LEHB106
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB107-.LFB3704
	.uleb128 .LEHE107-.LEHB107
	.uleb128 .L1832-.LFB3704
	.uleb128 0x0
	.uleb128 .LEHB108-.LFB3704
	.uleb128 .LEHE108-.LEHB108
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB109-.LFB3704
	.uleb128 .LEHE109-.LEHB109
	.uleb128 .L1838-.LFB3704
	.uleb128 0x0
	.uleb128 .LEHB110-.LFB3704
	.uleb128 .LEHE110-.LEHB110
	.uleb128 .L1836-.LFB3704
	.uleb128 0x0
	.uleb128 .LEHB111-.LFB3704
	.uleb128 .LEHE111-.LEHB111
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB112-.LFB3704
	.uleb128 .LEHE112-.LEHB112
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB113-.LFB3704
	.uleb128 .LEHE113-.LEHB113
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB114-.LFB3704
	.uleb128 .LEHE114-.LEHB114
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB115-.LFB3704
	.uleb128 .LEHE115-.LEHB115
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE3704:
	.fnend
	.size	_ZN12QuDrawObject9loadImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEEPfii, .-_ZN12QuDrawObject9loadImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEEPfii
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"basic_string\000"
	.space	3
.LC1:
	.ascii	"loading imaeg by filename deprecated\000"
	.bss
	.align	2
	.set	.LANCHOR0,. + 0
	.type	_ZN4_STLL8_LocInitE, %object
	.size	_ZN4_STLL8_LocInitE, 1
_ZN4_STLL8_LocInitE:
	.space	1
	.space	3
	.type	_ZN4_STLL8_IosInitE, %object
	.size	_ZN4_STLL8_IosInitE, 1
_ZN4_STLL8_IosInitE:
	.space	1
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
