	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"IwResHandlerWAV.cpp"
	.section	.text._ZN10CIwManaged9ParseOpenEP16CIwTextParserITX,"axG",%progbits,_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX,comdat
	.align	2
	.weak	_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX
	.hidden	_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX
	.type	_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX, %function
_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX:
	.fnstart
.LFB301:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX, .-_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX
	.section	.text._ZN10CIwManaged11HandleEventEP8CIwEvent,"axG",%progbits,_ZN10CIwManaged11HandleEventEP8CIwEvent,comdat
	.align	2
	.weak	_ZN10CIwManaged11HandleEventEP8CIwEvent
	.hidden	_ZN10CIwManaged11HandleEventEP8CIwEvent
	.type	_ZN10CIwManaged11HandleEventEP8CIwEvent, %function
_ZN10CIwManaged11HandleEventEP8CIwEvent:
	.fnstart
.LFB302:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #0
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN10CIwManaged11HandleEventEP8CIwEvent, .-_ZN10CIwManaged11HandleEventEP8CIwEvent
	.section	.text._ZN10CIwManaged11DebugRenderEv,"axG",%progbits,_ZN10CIwManaged11DebugRenderEv,comdat
	.align	2
	.weak	_ZN10CIwManaged11DebugRenderEv
	.hidden	_ZN10CIwManaged11DebugRenderEv
	.type	_ZN10CIwManaged11DebugRenderEv, %function
_ZN10CIwManaged11DebugRenderEv:
	.fnstart
.LFB304:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN10CIwManaged11DebugRenderEv, .-_ZN10CIwManaged11DebugRenderEv
	.section	.text._ZNK16CIwResHandlerWAV12GetClassNameEv,"ax",%progbits
	.align	2
	.global	_ZNK16CIwResHandlerWAV12GetClassNameEv
	.hidden	_ZNK16CIwResHandlerWAV12GetClassNameEv
	.type	_ZNK16CIwResHandlerWAV12GetClassNameEv, %function
_ZNK16CIwResHandlerWAV12GetClassNameEv:
	.fnstart
.LFB1369:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L9
	bx	lr
.L10:
	.align	2
.L9:
	.word	.LC0
	.cantunwind
	.fnend
	.size	_ZNK16CIwResHandlerWAV12GetClassNameEv, .-_ZNK16CIwResHandlerWAV12GetClassNameEv
	.section	.text._ZN16CIwResHandlerWAV5BuildERK9CIwStringILi160EE,"ax",%progbits
	.align	2
	.global	_ZN16CIwResHandlerWAV5BuildERK9CIwStringILi160EE
	.hidden	_ZN16CIwResHandlerWAV5BuildERK9CIwStringILi160EE
	.type	_ZN16CIwResHandlerWAV5BuildERK9CIwStringILi160EE, %function
_ZN16CIwResHandlerWAV5BuildERK9CIwStringILi160EE:
	.fnstart
.LFB1373:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #0
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN16CIwResHandlerWAV5BuildERK9CIwStringILi160EE, .-_ZN16CIwResHandlerWAV5BuildERK9CIwStringILi160EE
	.section	.text._ZN16CIwResHandlerWAVD1Ev,"axG",%progbits,_ZN16CIwResHandlerWAVD1Ev,comdat
	.align	2
	.weak	_ZN16CIwResHandlerWAVD1Ev
	.hidden	_ZN16CIwResHandlerWAVD1Ev
	.type	_ZN16CIwResHandlerWAVD1Ev, %function
_ZN16CIwResHandlerWAVD1Ev:
	.fnstart
.LFB1515:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L15
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	str	r3, [r0, #0]
	bl	_ZN13CIwResHandlerD2Ev
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L16:
	.align	2
.L15:
	.word	.LANCHOR0+8
	.fnend
	.size	_ZN16CIwResHandlerWAVD1Ev, .-_ZN16CIwResHandlerWAVD1Ev
	.section	.text._ZN16CIwResHandlerWAVD0Ev,"axG",%progbits,_ZN16CIwResHandlerWAVD0Ev,comdat
	.align	2
	.weak	_ZN16CIwResHandlerWAVD0Ev
	.hidden	_ZN16CIwResHandlerWAVD0Ev
	.type	_ZN16CIwResHandlerWAVD0Ev, %function
_ZN16CIwResHandlerWAVD0Ev:
	.fnstart
.LFB1516:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L19
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	str	r3, [r0, #0]
	bl	_ZN13CIwResHandlerD2Ev
	mov	r0, r4
	bl	_ZdlPv
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L20:
	.align	2
.L19:
	.word	.LANCHOR0+8
	.fnend
	.size	_ZN16CIwResHandlerWAVD0Ev, .-_ZN16CIwResHandlerWAVD0Ev
	.section	.text._ZN16CIwResHandlerWAV13ReadChunkFactERK17IwRIFFChunkHeaderRP12CIwSoundDataR7s3eFile,"ax",%progbits
	.align	2
	.global	_ZN16CIwResHandlerWAV13ReadChunkFactERK17IwRIFFChunkHeaderRP12CIwSoundDataR7s3eFile
	.hidden	_ZN16CIwResHandlerWAV13ReadChunkFactERK17IwRIFFChunkHeaderRP12CIwSoundDataR7s3eFile
	.type	_ZN16CIwResHandlerWAV13ReadChunkFactERK17IwRIFFChunkHeaderRP12CIwSoundDataR7s3eFile, %function
_ZN16CIwResHandlerWAV13ReadChunkFactERK17IwRIFFChunkHeaderRP12CIwSoundDataR7s3eFile:
	.fnstart
.LFB1377:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	.pad #8
	sub	sp, sp, #8
	mov	r4, r2
	add	r0, sp, #4
	mov	r2, #1
	mov	r1, #4
	bl	s3eFileRead
	cmp	r0, #1
	ldreq	r3, [r4, #0]
	ldreq	r2, [sp, #4]
	movne	r0, #0
	streq	r2, [r3, #20]
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	bx	lr
	.fnend
	.size	_ZN16CIwResHandlerWAV13ReadChunkFactERK17IwRIFFChunkHeaderRP12CIwSoundDataR7s3eFile, .-_ZN16CIwResHandlerWAV13ReadChunkFactERK17IwRIFFChunkHeaderRP12CIwSoundDataR7s3eFile
	.section	.text._ZN16CIwResHandlerWAV15ReadChunkHeaderER17IwRIFFChunkHeaderR7s3eFile,"ax",%progbits
	.align	2
	.global	_ZN16CIwResHandlerWAV15ReadChunkHeaderER17IwRIFFChunkHeaderR7s3eFile
	.hidden	_ZN16CIwResHandlerWAV15ReadChunkHeaderER17IwRIFFChunkHeaderR7s3eFile
	.type	_ZN16CIwResHandlerWAV15ReadChunkHeaderER17IwRIFFChunkHeaderR7s3eFile, %function
_ZN16CIwResHandlerWAV15ReadChunkHeaderER17IwRIFFChunkHeaderR7s3eFile:
	.fnstart
.LFB1374:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, lr}
	.save {r3, lr}
	mov	r0, r1
	mov	r3, r2
	mov	r1, #8
	mov	r2, #1
	bl	s3eFileRead
	cmp	r0, #1
	movne	r0, #0
	moveq	r0, #1
	ldmfd	sp!, {r3, lr}
	bx	lr
	.fnend
	.size	_ZN16CIwResHandlerWAV15ReadChunkHeaderER17IwRIFFChunkHeaderR7s3eFile, .-_ZN16CIwResHandlerWAV15ReadChunkHeaderER17IwRIFFChunkHeaderR7s3eFile
	.section	.text._ZN16CIwResHandlerWAV13ReadChunkDataERK17IwRIFFChunkHeaderRP12CIwSoundDataR7s3eFile,"ax",%progbits
	.align	2
	.global	_ZN16CIwResHandlerWAV13ReadChunkDataERK17IwRIFFChunkHeaderRP12CIwSoundDataR7s3eFile
	.hidden	_ZN16CIwResHandlerWAV13ReadChunkDataERK17IwRIFFChunkHeaderRP12CIwSoundDataR7s3eFile
	.type	_ZN16CIwResHandlerWAV13ReadChunkDataERK17IwRIFFChunkHeaderRP12CIwSoundDataR7s3eFile, %function
_ZN16CIwResHandlerWAV13ReadChunkDataERK17IwRIFFChunkHeaderRP12CIwSoundDataR7s3eFile:
	.fnstart
.LFB1376:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	ldr	r0, [r2, #0]
	cmp	r0, #0
	mov	r5, r2
	mov	r4, r1
	mov	r6, r3
	beq	.L28
	ldr	r1, [r1, #4]
	bl	_ZN12CIwSoundData13SetBufferSizeEj
	ldr	r2, [r5, #0]
	mov	r3, r6
	ldr	r0, [r2, #24]
	mov	r1, #1
	ldr	r2, [r4, #4]
	bl	s3eFileRead
	ldr	r3, [r4, #4]
	cmp	r0, r3
	beq	.L34
.L28:
	mov	r0, #0
.L30:
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L34:
	ldr	r0, [r5, #0]
	ldr	r1, [r0, #32]
	cmp	r1, #0
	movne	r0, #1
	bne	.L30
.L29:
	bl	_ZN12CIwSoundData14SwitchDataSignEv
	mov	r0, #1
	b	.L30
	.fnend
	.size	_ZN16CIwResHandlerWAV13ReadChunkDataERK17IwRIFFChunkHeaderRP12CIwSoundDataR7s3eFile, .-_ZN16CIwResHandlerWAV13ReadChunkDataERK17IwRIFFChunkHeaderRP12CIwSoundDataR7s3eFile
	.global	__cxa_end_cleanup
	.section	.text._ZN16CIwResHandlerWAV15ReadChunkFormatERK17IwRIFFChunkHeaderRP12CIwSoundDataR7s3eFile,"ax",%progbits
	.align	2
	.global	_ZN16CIwResHandlerWAV15ReadChunkFormatERK17IwRIFFChunkHeaderRP12CIwSoundDataR7s3eFile
	.hidden	_ZN16CIwResHandlerWAV15ReadChunkFormatERK17IwRIFFChunkHeaderRP12CIwSoundDataR7s3eFile
	.type	_ZN16CIwResHandlerWAV15ReadChunkFormatERK17IwRIFFChunkHeaderRP12CIwSoundDataR7s3eFile, %function
_ZN16CIwResHandlerWAV15ReadChunkFormatERK17IwRIFFChunkHeaderRP12CIwSoundDataR7s3eFile:
	.fnstart
.LFB1375:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	ldr	r1, [r1, #4]
	cmp	r1, #15
	.pad #32
	sub	sp, sp, #32
	mov	r4, r2
	bhi	.L58
.L36:
	mov	r0, #0
.L43:
	add	sp, sp, #32
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L58:
	add	r0, sp, #12
	mov	r1, #20
	mov	r2, #1
.LEHB0:
	bl	s3eFileRead
	cmp	r0, #1
	bne	.L36
	ldrh	r6, [sp, #12]
	cmp	r6, #1
	beq	.L38
	cmp	r6, #17
	bne	.L36
	mov	r0, #44
	bl	_Znwj
.LEHE0:
	ldrh	ip, [sp, #24]
	ldrh	r3, [sp, #30]
	ldr	r2, [sp, #16]
	mov	r1, #2
	mov	r5, r0
	str	ip, [sp, #0]
.LEHB1:
	bl	_ZN17CIwSoundDataADPCMC1E17IwSoundDataFormatjjj
.LEHE1:
.L48:
.L56:
	str	r5, [r4, #0]
	mov	r0, #1
	b	.L43
.L38:
	ldrh	r3, [sp, #26]
	cmp	r3, #8
	beq	.L39
	cmp	r3, #16
	bne	.L36
.L42:
	mov	r0, #36
.LEHB2:
	bl	_Znwj
.LEHE2:
	mov	r1, r6
	ldr	r2, [sp, #16]
	mov	r5, r0
.LEHB3:
	bl	_ZN12CIwSoundDataC1E17IwSoundDataFormatj
.LEHE3:
	b	.L56
.L39:
	mov	r0, #36
.LEHB4:
	bl	_Znwj
.LEHE4:
	mov	r1, #0
	ldr	r2, [sp, #16]
	mov	r5, r0
.LEHB5:
	bl	_ZN12CIwSoundDataC1E17IwSoundDataFormatj
.LEHE5:
	b	.L56
.L50:
.L57:
.L45:
	mov	r4, r0
	mov	r0, r5
	bl	_ZdlPv
	mov	r0, r4
.LEHB6:
	bl	__cxa_end_cleanup
.LEHE6:
.L52:
	b	.L57
.L51:
	b	.L57
	.global	__gxx_personality_v0
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA1375:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE1375-.LLSDACSB1375
.LLSDACSB1375:
	.uleb128 .LEHB0-.LFB1375
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB1-.LFB1375
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L50-.LFB1375
	.uleb128 0x0
	.uleb128 .LEHB2-.LFB1375
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB3-.LFB1375
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L51-.LFB1375
	.uleb128 0x0
	.uleb128 .LEHB4-.LFB1375
	.uleb128 .LEHE4-.LEHB4
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB5-.LFB1375
	.uleb128 .LEHE5-.LEHB5
	.uleb128 .L52-.LFB1375
	.uleb128 0x0
	.uleb128 .LEHB6-.LFB1375
	.uleb128 .LEHE6-.LEHB6
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE1375:
	.fnend
	.size	_ZN16CIwResHandlerWAV15ReadChunkFormatERK17IwRIFFChunkHeaderRP12CIwSoundDataR7s3eFile, .-_ZN16CIwResHandlerWAV15ReadChunkFormatERK17IwRIFFChunkHeaderRP12CIwSoundDataR7s3eFile
	.section	.text._ZN16CIwResHandlerWAVC1Ev,"ax",%progbits
	.align	2
	.global	_ZN16CIwResHandlerWAVC1Ev
	.hidden	_ZN16CIwResHandlerWAVC1Ev
	.type	_ZN16CIwResHandlerWAVC1Ev, %function
_ZN16CIwResHandlerWAVC1Ev:
	.fnstart
.LFB1372:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	ldr	r1, .L66
	ldr	r2, .L66+4
	mov	r4, r0
.LEHB7:
	bl	_ZN13CIwResHandlerC2EPKcS1_
.LEHE7:
	ldr	r0, .L66+8
	mov	r3, #0
	str	r0, [r4, #0]
	strb	r3, [r4, #52]
	mov	r0, r4
	ldr	r1, .L66+12
.LEHB8:
	bl	_ZN10CIwManaged7SetNameEPKc
.LEHE8:
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L64:
.L62:
	mov	r5, r0
	mov	r0, r4
	bl	_ZN13CIwResHandlerD2Ev
	mov	r0, r5
.LEHB9:
	bl	__cxa_end_cleanup
.LEHE9:
.L67:
	.align	2
.L66:
	.word	.LC1
	.word	.LC2
	.word	.LANCHOR0+8
	.word	.LC3
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA1372:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE1372-.LLSDACSB1372
.LLSDACSB1372:
	.uleb128 .LEHB7-.LFB1372
	.uleb128 .LEHE7-.LEHB7
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB8-.LFB1372
	.uleb128 .LEHE8-.LEHB8
	.uleb128 .L64-.LFB1372
	.uleb128 0x0
	.uleb128 .LEHB9-.LFB1372
	.uleb128 .LEHE9-.LEHB9
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE1372:
	.fnend
	.size	_ZN16CIwResHandlerWAVC1Ev, .-_ZN16CIwResHandlerWAVC1Ev
	.section	.text._ZN16CIwResHandlerWAVC2Ev,"ax",%progbits
	.align	2
	.global	_ZN16CIwResHandlerWAVC2Ev
	.hidden	_ZN16CIwResHandlerWAVC2Ev
	.type	_ZN16CIwResHandlerWAVC2Ev, %function
_ZN16CIwResHandlerWAVC2Ev:
	.fnstart
.LFB1371:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	ldr	r1, .L75
	ldr	r2, .L75+4
	mov	r4, r0
.LEHB10:
	bl	_ZN13CIwResHandlerC2EPKcS1_
.LEHE10:
	ldr	r0, .L75+8
	mov	r3, #0
	str	r0, [r4, #0]
	strb	r3, [r4, #52]
	mov	r0, r4
	ldr	r1, .L75+12
.LEHB11:
	bl	_ZN10CIwManaged7SetNameEPKc
.LEHE11:
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L73:
.L71:
	mov	r5, r0
	mov	r0, r4
	bl	_ZN13CIwResHandlerD2Ev
	mov	r0, r5
.LEHB12:
	bl	__cxa_end_cleanup
.LEHE12:
.L76:
	.align	2
.L75:
	.word	.LC1
	.word	.LC2
	.word	.LANCHOR0+8
	.word	.LC3
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA1371:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE1371-.LLSDACSB1371
.LLSDACSB1371:
	.uleb128 .LEHB10-.LFB1371
	.uleb128 .LEHE10-.LEHB10
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB11-.LFB1371
	.uleb128 .LEHE11-.LEHB11
	.uleb128 .L73-.LFB1371
	.uleb128 0x0
	.uleb128 .LEHB12-.LFB1371
	.uleb128 .LEHE12-.LEHB12
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE1371:
	.fnend
	.size	_ZN16CIwResHandlerWAVC2Ev, .-_ZN16CIwResHandlerWAVC2Ev
	.hidden	_ZTV16CIwResHandlerWAV
	.global	_ZTV16CIwResHandlerWAV
	.hidden	_ZTS16CIwResHandlerWAV
	.global	_ZTS16CIwResHandlerWAV
	.hidden	_ZTI16CIwResHandlerWAV
	.global	_ZTI16CIwResHandlerWAV
	.section	.rodata
	.align	3
	.set	.LANCHOR0,. + 0
	.type	_ZTV16CIwResHandlerWAV, %object
	.size	_ZTV16CIwResHandlerWAV, 68
_ZTV16CIwResHandlerWAV:
	.word	0
	.word	_ZTI16CIwResHandlerWAV
	.word	_ZN16CIwResHandlerWAVD1Ev
	.word	_ZN16CIwResHandlerWAVD0Ev
	.word	_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX
	.word	_ZN10CIwManaged10ParseCloseEP16CIwTextParserITX
	.word	_ZN10CIwManaged14ParseAttributeEP16CIwTextParserITXPKc
	.word	_ZN10CIwManaged9SerialiseEv
	.word	_ZN10CIwManaged7ResolveEv
	.word	_ZN10CIwManaged15ParseCloseChildEP16CIwTextParserITXPS_
	.word	_ZN10CIwManaged11HandleEventEP8CIwEvent
	.word	_ZN10CIwManaged7SetNameEPKc
	.word	_ZNK16CIwResHandlerWAV12GetClassNameEv
	.word	_ZN10CIwManaged11DebugRenderEv
	.word	_ZN10CIwManaged17DebugAddMenuItemsEP7CIwMenu
	.word	_ZN10CIwManaged8_ReplaceEPS_
	.word	_ZN16CIwResHandlerWAV5BuildERK9CIwStringILi160EE
	.type	_ZTS16CIwResHandlerWAV, %object
	.size	_ZTS16CIwResHandlerWAV, 19
_ZTS16CIwResHandlerWAV:
	.ascii	"16CIwResHandlerWAV\000"
	.space	1
	.type	_ZTI16CIwResHandlerWAV, %object
	.size	_ZTI16CIwResHandlerWAV, 12
_ZTI16CIwResHandlerWAV:
	.word	_ZTVN10__cxxabiv120__si_class_type_infoE+8
	.word	_ZTS16CIwResHandlerWAV
	.word	_ZTI13CIwResHandler
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"CIwResHandlerWAV\000"
	.space	3
.LC1:
	.ascii	"wav\000"
.LC2:
	.ascii	"CIwSoundData\000"
	.space	3
.LC3:
	.ascii	"WAV\000"
	.hidden	_ZTV16CIwResHandlerWAV
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
