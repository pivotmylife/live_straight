	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"pngpread.c"
	.section	.text.png_push_crc_skip,"ax",%progbits
	.align	2
	.global	png_push_crc_skip
	.hidden	png_push_crc_skip
	.type	png_push_crc_skip, %function
png_push_crc_skip:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, #3
	str	r1, [r0, #476]
	str	r3, [r0, #496]
	bx	lr
	.size	png_push_crc_skip, .-png_push_crc_skip
	.section	.text.png_push_restore_buffer,"ax",%progbits
	.align	2
	.global	png_push_restore_buffer
	.hidden	png_push_restore_buffer
	.type	png_push_restore_buffer, %function
png_push_restore_buffer:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	ip, [r0, #480]
	add	r3, r2, ip
	str	r1, [r0, #464]
	str	r3, [r0, #488]
	str	r1, [r0, #468]
	str	r2, [r0, #492]
	bx	lr
	.size	png_push_restore_buffer, .-png_push_restore_buffer
	.section	.text.png_push_have_info,"ax",%progbits
	.align	2
	.global	png_push_have_info
	.hidden	png_push_have_info
	.type	png_push_have_info, %function
png_push_have_info:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, lr}
	ldr	r3, [r0, #444]
	cmp	r3, #0
	movne	lr, pc
	bxne	r3
.L7:
	ldmfd	sp!, {r3, lr}
	bx	lr
	.size	png_push_have_info, .-png_push_have_info
	.section	.text.png_push_have_end,"ax",%progbits
	.align	2
	.global	png_push_have_end
	.hidden	png_push_have_end
	.type	png_push_have_end, %function
png_push_have_end:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, lr}
	ldr	r3, [r0, #452]
	cmp	r3, #0
	movne	lr, pc
	bxne	r3
.L11:
	ldmfd	sp!, {r3, lr}
	bx	lr
	.size	png_push_have_end, .-png_push_have_end
	.section	.text.png_push_have_row,"ax",%progbits
	.align	2
	.global	png_push_have_row
	.hidden	png_push_have_row
	.type	png_push_have_row, %function
png_push_have_row:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	ip, [r0, #448]
	cmp	ip, #0
	stmfd	sp!, {r3, lr}
	ldrneb	r3, [r0, #320]	@ zero_extendqisi2
	ldrne	r2, [r0, #256]
	movne	lr, pc
	bxne	ip
.L14:
	ldmfd	sp!, {r3, lr}
	bx	lr
	.size	png_push_have_row, .-png_push_have_row
	.section	.text.png_get_progressive_ptr,"ax",%progbits
	.align	2
	.global	png_get_progressive_ptr
	.hidden	png_get_progressive_ptr
	.type	png_get_progressive_ptr, %function
png_get_progressive_ptr:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	ldrne	r0, [r0, #112]
	bx	lr
	.size	png_get_progressive_ptr, .-png_get_progressive_ptr
	.section	.text.png_set_progressive_read_fn,"ax",%progbits
	.align	2
	.global	png_set_progressive_read_fn
	.hidden	png_set_progressive_read_fn
	.type	png_set_progressive_read_fn, %function
png_set_progressive_read_fn:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	subs	ip, r0, #0
	stmfd	sp!, {r3, lr}
	beq	.L21
	ldr	lr, [sp, #8]
	str	r2, [ip, #444]
	str	lr, [ip, #452]
	str	r3, [ip, #448]
	ldr	r2, .L22
	bl	png_set_read_fn
.L21:
	ldmfd	sp!, {r3, lr}
	bx	lr
.L23:
	.align	2
.L22:
	.word	png_push_fill_buffer
	.size	png_set_progressive_read_fn, .-png_set_progressive_read_fn
	.section	.text.png_progressive_combine_row,"ax",%progbits
	.align	2
	.global	png_progressive_combine_row
	.hidden	png_progressive_combine_row
	.type	png_progressive_combine_row, %function
png_progressive_combine_row:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, lr}
	subs	r3, r0, #0
	beq	.L26
	cmp	r2, #0
	ldrneb	r2, [r3, #320]	@ zero_extendqisi2
	ldrne	r3, .L27
	ldrne	r2, [r3, r2, asl #2]
	blne	png_combine_row
.L26:
	ldmfd	sp!, {r3, lr}
	bx	lr
.L28:
	.align	2
.L27:
	.word	png_pass_dsp_mask
	.size	png_progressive_combine_row, .-png_progressive_combine_row
	.section	.text.png_push_fill_buffer,"ax",%progbits
	.align	2
	.global	png_push_fill_buffer
	.hidden	png_push_fill_buffer
	.type	png_push_fill_buffer, %function
png_push_fill_buffer:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, lr}
	subs	r4, r0, #0
	mov	r7, r1
	mov	r5, r2
	beq	.L33
	ldr	r6, [r4, #480]
	cmp	r6, #0
	moveq	r0, r1
	bne	.L34
.L32:
	cmp	r5, #0
	beq	.L33
	ldr	r3, [r4, #492]
	cmp	r3, #0
	beq	.L33
	cmp	r5, r3
	movcs	r5, r3
	mov	r2, r5
	ldr	r1, [r4, #464]
	bl	memcpy
	ldr	ip, [r4, #488]
	ldr	r1, [r4, #492]
	ldr	r3, [r4, #464]
	rsb	r0, r5, ip
	add	r2, r3, r5
	rsb	r5, r5, r1
	str	r2, [r4, #464]
	str	r0, [r4, #488]
	str	r5, [r4, #492]
.L33:
	ldmfd	sp!, {r3, r4, r5, r6, r7, lr}
	bx	lr
.L34:
	cmp	r2, r6
	movcc	r6, r2
	mov	r0, r1
	mov	r2, r6
	ldr	r1, [r4, #456]
	bl	memcpy
	ldr	r0, [r4, #480]
	ldr	ip, [r4, #488]
	ldr	r3, [r4, #456]
	rsb	r2, r6, r0
	add	r3, r3, r6
	rsb	r1, r6, ip
	str	r1, [r4, #488]
	str	r2, [r4, #480]
	str	r3, [r4, #456]
	rsb	r5, r6, r5
	add	r0, r7, r6
	b	.L32
	.size	png_push_fill_buffer, .-png_push_fill_buffer
	.section	.text.png_push_handle_unknown,"ax",%progbits
	.align	2
	.global	png_push_handle_unknown
	.hidden	png_push_handle_unknown
	.type	png_push_handle_unknown, %function
png_push_handle_unknown:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, lr}
	ldrb	r3, [r0, #312]	@ zero_extendqisi2
	tst	r3, #32
	mov	r4, r0
	mov	r5, r1
	mov	r7, r2
	beq	.L46
.L36:
	ldr	r1, [r4, #136]
	tst	r1, #32768
	bne	.L47
.L38:
	mov	r3, #3
	str	r7, [r4, #476]
	str	r3, [r4, #496]
	ldmfd	sp!, {r3, r4, r5, r6, r7, lr}
	bx	lr
.L47:
	add	r6, r4, #312
	ldmia	r6, {r0, r1}
	cmp	r7, #0
	mov	r2, #0
	str	r0, [r4, #648]
	strb	r2, [r4, #652]
	str	r7, [r4, #660]
	streq	r7, [r4, #656]
	bne	.L48
.L40:
	ldr	r3, [r4, #568]
	cmp	r3, #0
	beq	.L41
	add	r7, r4, #648
	mov	r0, r4
	mov	r1, r7
	mov	lr, pc
	bx	r3
	cmp	r0, #0
	blt	.L49
	bne	.L43
	ldrb	ip, [r4, #312]	@ zero_extendqisi2
	tst	ip, #32
	beq	.L50
.L44:
	mov	r1, r5
	mov	r2, r7
	mov	r0, r4
	mov	r3, #1
	bl	png_set_unknown_chunks
.L43:
	mov	r0, r4
	ldr	r1, [r4, #656]
	mov	r7, #0
	bl	png_free
	str	r7, [r4, #656]
	b	.L38
.L46:
	add	r1, r0, #312
	bl	png_handle_as_unknown
	cmp	r0, #3
	beq	.L36
	ldr	r0, [r4, #568]
	cmp	r0, #0
	bne	.L36
	mov	r0, r4
	ldr	r1, .L51
	bl	png_chunk_error
	b	.L36
.L48:
	mov	r1, r7
	mov	r0, r4
	bl	png_malloc
	mov	r2, r7
	mov	r1, r0
	str	r0, [r4, #656]
	mov	r0, r4
	bl	png_crc_read
	b	.L40
.L41:
	mov	r1, r5
	mov	r0, r4
	add	r2, r4, #648
	mov	r3, #1
	bl	png_set_unknown_chunks
	b	.L43
.L50:
	mov	r1, r6
	mov	r0, r4
	bl	png_handle_as_unknown
	cmp	r0, #3
	beq	.L44
	mov	r0, r4
	ldr	r1, .L51
	bl	png_chunk_error
	b	.L44
.L49:
	mov	r0, r4
	ldr	r1, .L51+4
	bl	png_chunk_error
	b	.L43
.L52:
	.align	2
.L51:
	.word	.LC0
	.word	.LC1
	.size	png_push_handle_unknown, .-png_push_handle_unknown
	.section	.text.png_push_handle_zTXt,"ax",%progbits
	.align	2
	.global	png_push_handle_zTXt
	.hidden	png_push_handle_zTXt
	.type	png_push_handle_zTXt, %function
png_push_handle_zTXt:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	ldr	r1, [r0, #132]
	and	r3, r1, #17
	cmp	r3, #1
	mov	r4, r0
	mov	r5, r2
	ldrne	r1, .L56
	blne	png_error
.L54:
	mov	r0, r4
	add	r1, r5, #1
	bl	png_malloc
	mov	ip, #0
	str	r0, [r4, #512]
	strb	ip, [r0, r5]
	ldr	r0, [r4, #512]
	mov	r2, #5
	str	r2, [r4, #496]
	str	r0, [r4, #516]
	str	r5, [r4, #508]
	str	r5, [r4, #504]
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L57:
	.align	2
.L56:
	.word	.LC2
	.size	png_push_handle_zTXt, .-png_push_handle_zTXt
	.section	.text.png_push_handle_tEXt,"ax",%progbits
	.align	2
	.global	png_push_handle_tEXt
	.hidden	png_push_handle_tEXt
	.type	png_push_handle_tEXt, %function
png_push_handle_tEXt:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	ldr	r1, [r0, #132]
	and	r3, r1, #17
	cmp	r3, #1
	mov	r4, r0
	mov	r5, r2
	ldrne	r1, .L61
	blne	png_error
.L59:
	mov	r0, r4
	add	r1, r5, #1
	bl	png_malloc
	mov	ip, #0
	str	r0, [r4, #512]
	strb	ip, [r0, r5]
	ldr	r0, [r4, #512]
	mov	r2, #4
	str	r2, [r4, #496]
	str	r0, [r4, #516]
	str	r5, [r4, #508]
	str	r5, [r4, #504]
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L62:
	.align	2
.L61:
	.word	.LC3
	.size	png_push_handle_tEXt, .-png_push_handle_tEXt
	.section	.text.png_push_save_buffer,"ax",%progbits
	.align	2
	.global	png_push_save_buffer
	.hidden	png_push_save_buffer
	.type	png_push_save_buffer, %function
png_push_save_buffer:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	mov	r4, r0
	ldr	r0, [r0, #480]
	cmp	r0, #0
	beq	.L64
	add	r2, r4, #456
	ldmia	r2, {r2, r3}	@ phole ldm
	cmp	r2, r3
	beq	.L64
	orr	ip, r3, r2
	tst	ip, #3
	add	r5, r2, #4
	add	r6, r3, #4
	movne	ip, #0
	moveq	ip, #1
	cmp	r2, r6
	cmpls	r3, r5
	movls	r1, #0
	movhi	r1, #1
	cmp	r0, #3
	movls	ip, #0
	andhi	ip, ip, #1
	ands	ip, ip, r1
	beq	.L65
	mov	r7, r0, lsr #2
	movs	r5, r7, asl #2
	beq	.L66
	ldr	r1, [r2, #0]
	mov	ip, #1
	sub	r6, r7, #1
	cmp	ip, r7
	str	r1, [r3, #0]
	and	r6, r6, #3
	mov	r1, #4
	bcs	.L117
	cmp	r6, #0
	beq	.L67
	cmp	r6, #1
	beq	.L115
	cmp	r6, #2
	ldrne	r1, [r2, #4]
	strne	r1, [r3, #4]
	movne	r1, #8
	ldr	r6, [r2, r1]
	movne	ip, #2
	str	r6, [r3, r1]
	add	ip, ip, #1
	add	r1, r1, #4
.L115:
	ldr	r6, [r2, r1]
	add	ip, ip, #1
	cmp	ip, r7
	str	r6, [r3, r1]
	add	r1, r1, #4
	bcs	.L117
.L67:
	ldr	r6, [r2, r1]
	str	r6, [r3, r1]
	add	r6, r1, #4
	ldr	r8, [r2, r6]
	str	r8, [r3, r6]
	add	r6, r6, #4
	ldr	r8, [r2, r6]
	str	r8, [r3, r6]
	add	r6, r1, #12
	add	ip, ip, #4
	ldr	r8, [r2, r6]
	cmp	ip, r7
	str	r8, [r3, r6]
	add	r1, r1, #16
	bcc	.L67
.L117:
	cmp	r0, r5
	add	r2, r2, r5
	add	r3, r3, r5
	beq	.L68
.L66:
	ldrb	r1, [r2, #0]	@ zero_extendqisi2
	mvn	ip, r5
	add	r6, r5, #1
	add	ip, ip, r0
	cmp	r0, r6
	strb	r1, [r3, #0]
	and	ip, ip, #3
	mov	r1, #1
	bls	.L68
	cmp	ip, #0
	beq	.L69
	cmp	ip, #1
	beq	.L113
	cmp	ip, #2
	ldrneb	r1, [r2, #1]	@ zero_extendqisi2
	strneb	r1, [r3, #1]
	movne	r1, #2
	ldrb	ip, [r2, r1]	@ zero_extendqisi2
	strb	ip, [r3, r1]
	add	r1, r1, #1
.L113:
	ldrb	ip, [r2, r1]	@ zero_extendqisi2
	strb	ip, [r3, r1]
	add	r1, r1, #1
	add	ip, r1, r5
	cmp	r0, ip
	bls	.L68
.L69:
	ldrb	r6, [r2, r1]	@ zero_extendqisi2
	strb	r6, [r3, r1]
	add	ip, r1, #1
	ldrb	r7, [r2, ip]	@ zero_extendqisi2
	strb	r7, [r3, ip]
	add	r6, ip, #1
	ldrb	r7, [r2, r6]	@ zero_extendqisi2
	strb	r7, [r3, r6]
	add	ip, r1, #3
	add	r1, r1, #4
	ldrb	r6, [r2, ip]	@ zero_extendqisi2
	add	r7, r1, r5
	cmp	r0, r7
	strb	r6, [r3, ip]
	bhi	.L69
.L68:
	ldr	r0, [r4, #480]
.L64:
	ldr	r2, [r4, #492]
	ldr	r3, [r4, #484]
	add	r5, r0, r2
	cmp	r5, r3
	bls	.L71
	add	lr, r2, #256
	mvn	r2, lr
	cmp	r2, r0
	bcc	.L118
.L72:
	add	r5, r5, #256
	mov	r1, r5
	mov	r0, r4
	ldr	r6, [r4, #460]
	bl	png_malloc
	ldr	r2, [r4, #480]
	mov	r1, r6
	str	r0, [r4, #460]
	bl	memcpy
	mov	r0, r4
	mov	r1, r6
	bl	png_free
	str	r5, [r4, #484]
	ldr	r2, [r4, #492]
.L71:
	cmp	r2, #0
	beq	.L73
	ldr	r1, [r4, #480]
	ldr	lr, [r4, #460]
	add	r0, lr, r1
	ldr	r1, [r4, #464]
	bl	memcpy
	ldr	r2, [r4, #480]
	ldr	r0, [r4, #492]
	mov	ip, #0
	add	r3, r2, r0
	str	r3, [r4, #480]
	str	ip, [r4, #492]
.L73:
	ldr	ip, [r4, #460]
	mov	r3, #0
	str	r3, [r4, #488]
	str	ip, [r4, #456]
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L118:
	mov	r0, r4
	ldr	r1, .L119
	bl	png_error
	ldr	r0, [r4, #492]
	ldr	r1, [r4, #480]
	add	r5, r0, r1
	b	.L72
.L65:
	ldrb	r6, [r2, ip]	@ zero_extendqisi2
	mov	r1, #1
	sub	r5, r0, #1
	cmp	r0, r1
	strb	r6, [r3, ip]
	and	ip, r5, #3
	bls	.L68
	cmp	ip, #0
	beq	.L70
	cmp	ip, #1
	beq	.L111
	cmp	ip, #2
	ldrneb	ip, [r2, r1]	@ zero_extendqisi2
	strneb	ip, [r3, r1]
	movne	r1, #2
	ldrb	r5, [r2, r1]	@ zero_extendqisi2
	strb	r5, [r3, r1]
	add	r1, r1, #1
.L111:
	ldrb	ip, [r2, r1]	@ zero_extendqisi2
	strb	ip, [r3, r1]
	add	r1, r1, #1
	cmp	r0, r1
	bls	.L68
.L70:
	ldrb	ip, [r2, r1]	@ zero_extendqisi2
	strb	ip, [r3, r1]
	add	ip, r1, #1
	ldrb	r5, [r2, ip]	@ zero_extendqisi2
	strb	r5, [r3, ip]
	add	ip, ip, #1
	ldrb	r5, [r2, ip]	@ zero_extendqisi2
	strb	r5, [r3, ip]
	add	ip, r1, #3
	ldrb	r5, [r2, ip]	@ zero_extendqisi2
	add	r1, r1, #4
	cmp	r0, r1
	strb	r5, [r3, ip]
	bhi	.L70
	b	.L68
.L120:
	.align	2
.L119:
	.word	.LC4
	.size	png_push_save_buffer, .-png_push_save_buffer
	.global	__aeabi_uidiv
	.section	.text.png_read_push_finish_row,"ax",%progbits
	.align	2
	.global	png_read_push_finish_row
	.hidden	png_read_push_finish_row
	.type	png_read_push_finish_row, %function
png_read_push_finish_row:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	ldr	r1, [r0, #256]
	ldr	r2, [r0, #236]
	add	r3, r1, #1
	cmp	r3, r2
	mov	r4, r0
	str	r3, [r0, #256]
	bcc	.L132
	ldrb	ip, [r0, #319]	@ zero_extendqisi2
	cmp	ip, #0
	bne	.L136
.L132:
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L136:
	ldr	r5, [r0, #244]
	mov	r2, #0
	add	r3, r5, #1
	str	r2, [r0, #256]
	ldr	r1, [r0, #260]
	bl	png_memset_check
	ldr	r8, .L140
	ldrb	r5, [r4, #320]	@ zero_extendqisi2
	ldr	r7, .L140+4
	ldr	r9, .L140+8
	ldr	sl, .L140+12
	b	.L135
.L123:
	cmp	r5, #3
	beq	.L137
	cmp	r5, #5
	beq	.L138
.L127:
	cmp	r5, #7
	subhi	r5, r5, #1
	andhi	r5, r5, #255
	strhib	r5, [r4, #320]
	cmp	r5, #6
	bhi	.L132
	ldr	r3, [r4, #228]
.L125:
	ldr	r1, [r8, r5, asl #2]
	sub	r0, r3, #1
	ldr	ip, [r7, r5, asl #2]
	add	r6, r0, r1
	rsb	r0, ip, r6
	bl	__aeabi_uidiv
	ldrb	r1, [r4, #325]	@ zero_extendqisi2
	cmp	r1, #7
	mulls	r1, r0, r1
	movhi	r1, r1, lsr #3
	mulhi	r1, r0, r1
	addls	r1, r1, #7
	ldr	r2, [r4, #140]
	movls	r1, r1, lsr #3
	add	r3, r1, #1
	tst	r2, #2
	mov	r6, r0
	str	r0, [r4, #252]
	str	r3, [r4, #248]
	bne	.L132
	ldrb	r5, [r4, #320]	@ zero_extendqisi2
	ldr	ip, [r4, #232]
	ldr	r1, [r9, r5, asl #2]
	sub	r2, ip, #1
	ldr	r3, [sl, r5, asl #2]
	add	lr, r2, r1
	rsb	r0, r3, lr
	bl	__aeabi_uidiv
	cmp	r6, #0
	str	r0, [r4, #236]
	bne	.L139
.L135:
	add	r0, r5, #1
	and	r5, r0, #255
	cmp	r5, #1
	strb	r5, [r4, #320]
	bne	.L123
	ldr	r3, [r4, #228]
	cmp	r3, #4
	bhi	.L125
.L124:
	add	lr, r5, #1
	and	r5, lr, #255
	strb	r5, [r4, #320]
	b	.L127
.L137:
	ldr	r3, [r4, #228]
	cmp	r3, #2
	bls	.L124
	b	.L125
.L138:
	ldr	r3, [r4, #228]
	cmp	r3, #1
	bhi	.L125
	b	.L124
.L139:
	cmp	r0, #0
	beq	.L135
	b	.L132
.L141:
	.align	2
.L140:
	.word	png_pass_inc
	.word	png_pass_start
	.word	png_pass_yinc
	.word	png_pass_ystart
	.size	png_read_push_finish_row, .-png_read_push_finish_row
	.section	.text.png_push_crc_finish,"ax",%progbits
	.align	2
	.global	png_push_crc_finish
	.hidden	png_push_crc_finish
	.type	png_push_crc_finish, %function
png_push_crc_finish:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	ldr	r5, [r0, #476]
	cmp	r5, #0
	mov	r4, r0
	ldreq	r3, [r0, #488]
	beq	.L145
	ldr	r6, [r0, #480]
	cmp	r6, #0
	beq	.L144
	cmp	r6, r5
	movcs	r6, r5
	ldr	r1, [r0, #456]
	mov	r2, r6
	bl	png_calculate_crc
	ldr	r5, [r4, #476]
	ldr	r3, [r4, #488]
	ldr	r1, [r4, #480]
	ldr	r0, [r4, #456]
	rsb	r5, r6, r5
	add	r2, r0, r6
	rsb	r3, r6, r3
	cmp	r5, #0
	rsb	r6, r6, r1
	str	r6, [r4, #480]
	str	r2, [r4, #456]
	str	r5, [r4, #476]
	str	r3, [r4, #488]
	beq	.L145
.L144:
	ldr	r3, [r4, #492]
	cmp	r3, #0
	beq	.L148
	cmp	r5, r3
	movcs	r5, r3
	mov	r2, r5
	mov	r0, r4
	ldr	r1, [r4, #464]
	bl	png_calculate_crc
	ldr	r2, [r4, #476]
	ldr	r3, [r4, #488]
	ldr	r0, [r4, #492]
	ldr	r1, [r4, #464]
	rsb	ip, r5, r2
	add	r1, r1, r5
	rsb	r3, r5, r3
	cmp	ip, #0
	rsb	r5, r5, r0
	str	r5, [r4, #492]
	str	r1, [r4, #464]
	str	ip, [r4, #476]
	str	r3, [r4, #488]
	beq	.L145
.L148:
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L145:
	cmp	r3, #3
	mov	r0, r4
	bls	.L149
	mov	r1, #0
	bl	png_crc_finish
	mov	ip, #1
	str	ip, [r4, #496]
	b	.L148
.L149:
	ldmfd	sp!, {r4, r5, r6, lr}
	b	png_push_save_buffer
	.size	png_push_crc_finish, .-png_push_crc_finish
	.section	.text.png_push_read_zTXt,"ax",%progbits
	.align	2
	.global	png_push_read_zTXt
	.hidden	png_push_read_zTXt
	.type	png_push_read_zTXt, %function
png_push_read_zTXt:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	r3, [r0, #488]
	sub	sp, sp, #12
	str	r1, [sp, #0]
	cmp	r3, #0
	mov	r4, r0
	ldr	r5, [r0, #508]
	beq	.L152
	cmp	r5, #0
	beq	.L153
	cmp	r5, r3
	movcs	r5, r3
	mov	r2, r5
	ldr	r1, [r0, #516]
	bl	png_crc_read
	ldr	r2, [r4, #508]
	ldr	r0, [r4, #516]
	add	r3, r0, r5
	rsb	r5, r5, r2
	str	r3, [r4, #516]
	str	r5, [r4, #508]
.L152:
	cmp	r5, #0
	beq	.L177
.L171:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L177:
	ldr	r3, [r4, #488]
.L153:
	cmp	r3, #3
	mov	r0, r4
	bls	.L178
	bl	png_push_crc_finish
	ldr	sl, [r4, #512]
	ldrb	r1, [sl, #0]	@ zero_extendqisi2
	cmp	r1, #0
	mov	r3, sl
	beq	.L157
.L158:
	ldrb	ip, [r3, #1]!	@ zero_extendqisi2
	cmp	ip, #0
	bne	.L158
.L157:
	ldr	r2, [r4, #504]
	add	lr, sl, r2
	cmp	lr, r3
	bls	.L175
	ldrb	r6, [r3, #1]	@ zero_extendqisi2
	cmp	r6, #0
	add	r3, r3, #1
	bne	.L175
	add	r0, r3, #1
	rsb	r7, r0, sl
	add	r7, r7, r2
	add	r1, r4, #200
	ldmia	r1, {r1, r2}	@ phole ldm
	rsb	r5, sl, r0
	cmp	r7, #0
	str	r1, [r4, #156]
	str	r2, [r4, #160]
	str	r0, [r4, #144]
	str	r7, [r4, #148]
	str	r5, [sp, #4]
	beq	.L161
	add	r9, r4, #144
	mov	r5, r6
	mov	fp, r6
.L167:
	mov	r1, #1
	mov	r0, r9
	bl	inflate
	cmp	r0, #1
	mov	r7, r0
	bhi	.L179
	ldr	r3, [r4, #160]
	cmp	r3, #0
	movne	r8, r0
	orreq	r8, r0, #1
	ands	r8, r8, #255
	beq	.L163
	ldr	r0, [r4, #204]
	cmp	r5, #0
	add	lr, r0, #1
	rsb	r3, r3, lr
	beq	.L180
	add	r1, r3, r6
	mov	r0, r4
	bl	png_malloc
	mov	r2, r6
	mov	r1, r5
	mov	r8, r0
	bl	memcpy
	mov	r1, r5
	mov	r0, r4
	bl	png_free
	ldr	r0, [r4, #204]
	ldr	r5, [r4, #160]
	ldr	r1, [r4, #200]
	rsb	r2, r5, r0
	add	r0, r8, r6
	bl	memcpy
	ldr	ip, [r4, #160]
	ldr	r3, [r4, #204]
	rsb	r1, ip, r3
	add	r6, r6, r1
	strb	fp, [r8, r6]
	mov	r5, r8
.L165:
	cmp	r7, #1
	beq	.L166
	ldr	r7, [r4, #148]
	add	r2, r4, #200
	ldmia	r2, {r2, lr}	@ phole ldm
	cmp	r7, #0
	str	r2, [r4, #156]
	str	lr, [r4, #160]
	bne	.L167
	mov	r0, r9
	bl	inflateReset
	str	r7, [r4, #148]
.L170:
	mov	r1, #0
	str	r1, [r4, #512]
.L176:
	mov	r1, sl
	mov	r0, r4
	bl	png_free
	mov	r0, r4
	mov	r1, r5
	bl	png_free
	b	.L171
.L178:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	b	png_push_save_buffer
.L166:
	mov	r0, r9
	mov	r7, #0
	bl	inflateReset
	str	r7, [r4, #148]
.L169:
	mov	r7, #0
	mov	r1, sl
	str	r7, [r4, #512]
	mov	r0, r4
	bl	png_free
	mov	r1, #16
	mov	r0, r4
	bl	png_malloc
	ldr	r3, [sp, #4]
	add	ip, r5, r3
	str	ip, [r0, #8]
	str	r7, [r0, #0]
	str	r5, [r0, #4]
	mov	r2, r0
	mov	r3, #1
	mov	r6, r0
	ldr	r1, [sp, #0]
	mov	r0, r4
	bl	png_set_text_2
	mov	r1, r5
	mov	r7, r0
	mov	r0, r4
	bl	png_free
	mov	r0, r4
	mov	r1, r6
	bl	png_free
	cmp	r7, #0
	beq	.L171
	mov	r0, r4
	ldr	r1, .L181
	bl	png_warning
	b	.L171
.L180:
	ldr	r5, [sp, #4]
	mov	r0, r4
	add	r1, r3, r5
	bl	png_malloc
	ldr	r6, [r4, #160]
	mov	r5, r0
	ldr	r3, [sp, #4]
	ldr	r0, [r4, #204]
	ldr	r1, [r4, #200]
	rsb	r2, r6, r0
	add	r0, r5, r3
	bl	memcpy
	mov	r1, sl
	ldr	r2, [sp, #4]
	mov	r0, r5
	bl	memcpy
	ldr	r1, [r4, #204]
	ldr	ip, [r4, #160]
	ldr	r2, [sp, #4]
	rsb	r6, ip, r1
	add	r6, r6, r2
	strb	fp, [r5, r6]
	b	.L165
.L175:
	mov	r3, #0
	str	r3, [r4, #512]
	mov	r0, r4
	mov	r1, sl
	bl	png_free
	b	.L171
.L179:
	mov	r0, r9
	bl	inflateReset
	mov	ip, #0
	str	ip, [r4, #512]
	str	ip, [r4, #148]
	b	.L176
.L163:
	mov	r0, r9
	bl	inflateReset
	str	r8, [r4, #148]
	b	.L170
.L161:
	add	r0, r4, #144
	bl	inflateReset
	mov	r5, r7
	str	r7, [r4, #148]
	b	.L169
.L182:
	.align	2
.L181:
	.word	.LC5
	.size	png_push_read_zTXt, .-png_push_read_zTXt
	.section	.text.png_push_read_tEXt,"ax",%progbits
	.align	2
	.global	png_push_read_tEXt
	.hidden	png_push_read_tEXt
	.type	png_push_read_tEXt, %function
png_push_read_tEXt:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	ldr	r3, [r0, #488]
	cmp	r3, #0
	mov	r4, r0
	mov	r7, r1
	ldr	r5, [r0, #508]
	beq	.L185
	cmp	r5, #0
	beq	.L186
	cmp	r5, r3
	movcs	r5, r3
	mov	r2, r5
	ldr	r1, [r0, #516]
	bl	png_crc_read
	ldr	r2, [r4, #508]
	ldr	r0, [r4, #516]
	add	r3, r0, r5
	rsb	r5, r5, r2
	str	r3, [r4, #516]
	str	r5, [r4, #508]
.L185:
	cmp	r5, #0
	beq	.L195
.L193:
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L195:
	ldr	r3, [r4, #488]
.L186:
	cmp	r3, #3
	mov	r0, r4
	bls	.L196
	bl	png_push_crc_finish
	ldr	r6, [r4, #512]
	ldrb	r1, [r6, #0]	@ zero_extendqisi2
	cmp	r1, #0
	mov	r8, r6
	beq	.L190
.L191:
	ldrb	r5, [r8, #1]!	@ zero_extendqisi2
	cmp	r5, #0
	bne	.L191
.L190:
	ldr	r2, [r4, #504]
	add	lr, r6, r2
	cmp	r8, lr
	mov	r1, #16
	mov	r0, r4
	addcc	r8, r8, #1
	bl	png_malloc
	mvn	r3, #0
	mov	r2, r0
	str	r3, [r0, #0]
	mov	r1, r7
	mov	r3, #1
	mov	r5, r0
	stmib	r0, {r6, r8}	@ phole stm
	mov	r0, r4
	bl	png_set_text_2
	mov	r1, r6
	mov	r7, r0
	mov	r0, r4
	bl	png_free
	mov	r0, r4
	mov	r1, r5
	bl	png_free
	mov	ip, #0
	cmp	r7, #0
	str	ip, [r4, #512]
	beq	.L193
	mov	r0, r4
	ldr	r1, .L197
	bl	png_warning
	b	.L193
.L196:
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	png_push_save_buffer
.L198:
	.align	2
.L197:
	.word	.LC5
	.size	png_push_read_tEXt, .-png_push_read_tEXt
	.section	.text.png_push_read_sig,"ax",%progbits
	.align	2
	.global	png_push_read_sig
	.hidden	png_push_read_sig
	.type	png_push_read_sig, %function
png_push_read_sig:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, lr}
	ldrb	r5, [r0, #328]	@ zero_extendqisi2
	ldr	r7, [r0, #480]
	ldr	r3, [r0, #488]
	rsb	r6, r5, #8
	cmp	r6, r3
	movcs	r6, r3
	add	sl, r5, #32
	cmp	r7, #0
	mov	r4, r0
	mov	r8, r1
	add	sl, r1, sl
	moveq	r7, r6
	beq	.L201
	cmp	r6, r7
	movcc	r7, r6
	mov	r2, r7
	mov	r0, sl
	ldr	r1, [r4, #456]
	bl	memcpy
	ldr	r2, [r4, #488]
	ldr	ip, [r4, #480]
	ldr	r3, [r4, #456]
	rsb	r1, r7, r2
	add	r0, r3, r7
	rsb	r2, r7, ip
	str	r1, [r4, #488]
	str	r2, [r4, #480]
	str	r0, [r4, #456]
	add	sl, sl, r7
	rsb	r7, r7, r6
.L201:
	cmp	r7, #0
	beq	.L202
	ldr	r3, [r4, #492]
	cmp	r3, #0
	beq	.L202
	cmp	r7, r3
	movcs	r7, r3
	mov	r2, r7
	ldr	r1, [r4, #464]
	mov	r0, sl
	bl	memcpy
	ldr	r2, [r4, #488]
	ldr	r1, [r4, #492]
	ldr	r3, [r4, #464]
	rsb	ip, r7, r2
	add	r0, r3, r7
	rsb	r7, r7, r1
	str	ip, [r4, #488]
	str	r7, [r4, #492]
	str	r0, [r4, #464]
.L202:
	ldrb	r0, [r4, #328]	@ zero_extendqisi2
	add	r8, r8, #32
	add	lr, r6, r0
	strb	lr, [r4, #328]
	mov	r0, r8
	mov	r1, r5
	mov	r2, r6
	bl	png_sig_cmp
	cmp	r0, #0
	beq	.L203
	cmp	r5, #3
	bhi	.L204
	mov	r0, r8
	mov	r1, r5
	sub	r2, r6, #4
	bl	png_sig_cmp
	cmp	r0, #0
	bne	.L207
.L204:
	mov	r0, r4
	ldr	r1, .L208
	bl	png_error
	b	.L206
.L203:
	ldrb	ip, [r4, #328]	@ zero_extendqisi2
	cmp	ip, #7
	movhi	ip, #1
	strhi	ip, [r4, #496]
.L206:
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, lr}
	bx	lr
.L207:
	mov	r0, r4
	ldr	r1, .L208+4
	bl	png_error
	b	.L206
.L209:
	.align	2
.L208:
	.word	.LC7
	.word	.LC6
	.size	png_push_read_sig, .-png_push_read_sig
	.section	.text.png_push_process_row,"ax",%progbits
	.align	2
	.global	png_push_process_row
	.hidden	png_push_process_row
	.type	png_push_process_row, %function
png_push_process_row:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	ldrb	r3, [r0, #325]	@ zero_extendqisi2
	ldr	lr, [r0, #252]
	mov	r4, r0
	cmp	r3, #7
	str	lr, [r4, #284]
	mulls	lr, r3, lr
	strb	r3, [r4, #295]
	movhi	r3, r3, lsr #3
	mulhi	lr, r3, lr
	ldrb	ip, [r0, #322]	@ zero_extendqisi2
	addls	lr, lr, #7
	ldrb	r0, [r0, #326]	@ zero_extendqisi2
	ldrb	r1, [r4, #323]	@ zero_extendqisi2
	movls	lr, lr, lsr #3
	strb	r0, [r4, #294]
	strb	r1, [r4, #293]
	str	lr, [r4, #288]
	ldr	r2, [r4, #264]
	strb	ip, [r4, #292]
	ldr	r1, [r4, #260]
	ldrb	ip, [r2, #0]	@ zero_extendqisi2
	sub	sp, sp, #8
	add	r3, r1, #1
	add	r2, r2, #1
	mov	r0, r4
	add	r1, r4, #284
	str	ip, [sp, #0]
	bl	png_read_filter_row
	ldr	r0, [r4, #244]
	ldr	r1, [r4, #260]
	add	r3, r0, #1
	ldr	r2, [r4, #264]
	mov	r0, r4
	bl	png_memcpy_check
	ldr	r3, [r4, #140]
	cmp	r3, #0
	bne	.L213
	ldr	r2, [r4, #136]
	tst	r2, #4194304
	bne	.L213
.L214:
	ldrb	lr, [r4, #319]	@ zero_extendqisi2
	cmp	lr, #0
	beq	.L215
	ldr	r3, [r4, #140]
	tst	r3, #2
	bne	.L283
.L215:
	ldr	ip, [r4, #448]
	cmp	ip, #0
	ldr	r1, [r4, #264]
	beq	.L271
	add	r1, r1, #1
	mov	r0, r4
	ldr	r2, [r4, #256]
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	mov	lr, pc
	bx	ip
.L271:
	mov	r0, r4
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	b	png_read_push_finish_row
.L213:
	mov	r0, r4
	bl	png_do_read_transformations
	b	.L214
.L283:
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #5
	bls	.L284
.L216:
	cmp	r3, #6
	ldrls	pc, [pc, r3, asl #2]
	b	.L272
.L225:
	.word	.L218
	.word	.L219
	.word	.L220
	.word	.L221
	.word	.L222
	.word	.L223
	.word	.L224
.L223:
	ldr	r1, [r4, #264]
	mov	r0, r4
	add	r1, r1, #1
	bl	png_push_have_row
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #5
	bne	.L279
.L224:
	ldr	r2, [r4, #264]
	mov	r0, r4
	add	r1, r2, #1
	bl	png_push_have_row
.L282:
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
.L279:
	cmp	r3, #6
	beq	.L275
.L272:
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	bx	lr
.L219:
	ldr	ip, [r4, #448]
	cmp	ip, #0
	ldr	r1, [r4, #264]
	beq	.L233
	add	r1, r1, #1
	mov	r0, r4
	ldr	r2, [r4, #256]
	mov	r3, #1
	mov	lr, pc
	bx	ip
.L233:
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #1
	bne	.L234
	ldr	ip, [r4, #448]
	cmp	ip, #0
	ldr	r1, [r4, #264]
	addne	r1, r1, #1
	movne	r0, r4
	ldrne	r2, [r4, #256]
	movne	lr, pc
	bxne	ip
.L235:
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #1
	bne	.L234
	ldr	ip, [r4, #448]
	cmp	ip, #0
	ldr	r1, [r4, #264]
	addne	r1, r1, #1
	movne	r0, r4
	ldrne	r2, [r4, #256]
	movne	lr, pc
	bxne	ip
.L236:
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #1
	bne	.L234
	ldr	ip, [r4, #448]
	cmp	ip, #0
	ldr	r1, [r4, #264]
	addne	r1, r1, #1
	movne	r0, r4
	ldrne	r2, [r4, #256]
	movne	lr, pc
	bxne	ip
.L237:
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #1
	bne	.L234
	ldr	ip, [r4, #448]
	cmp	ip, #0
	ldr	r1, [r4, #264]
	addne	r1, r1, #1
	movne	r0, r4
	ldrne	r2, [r4, #256]
	movne	lr, pc
	bxne	ip
.L238:
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #1
	bne	.L234
	ldr	ip, [r4, #448]
	cmp	ip, #0
	ldr	r1, [r4, #264]
	addne	r1, r1, #1
	movne	r0, r4
	ldrne	r2, [r4, #256]
	movne	lr, pc
	bxne	ip
.L239:
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #1
	bne	.L234
	ldr	ip, [r4, #448]
	cmp	ip, #0
	ldr	r1, [r4, #264]
	addne	r1, r1, #1
	movne	r0, r4
	ldrne	r2, [r4, #256]
	movne	lr, pc
	bxne	ip
.L240:
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #1
	bne	.L234
	ldr	ip, [r4, #448]
	cmp	ip, #0
	ldr	r1, [r4, #264]
	addne	r1, r1, #1
	movne	r0, r4
	ldrne	r2, [r4, #256]
	movne	lr, pc
	bxne	ip
.L253:
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
.L234:
	cmp	r3, #2
	bne	.L272
	mov	r1, #0
	mov	r0, r4
	bl	png_push_have_row
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r2, [r4, #320]	@ zero_extendqisi2
	cmp	r2, #2
	bne	.L272
	mov	r1, #0
	mov	r0, r4
	bl	png_push_have_row
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #2
	bne	.L272
	mov	r1, #0
	mov	r0, r4
	bl	png_push_have_row
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r1, [r4, #320]	@ zero_extendqisi2
	cmp	r1, #2
	bne	.L272
.L275:
	mov	r0, r4
	mov	r1, #0
	bl	png_push_have_row
	mov	r0, r4
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	b	png_read_push_finish_row
.L284:
	mov	r0, r4
	bl	png_do_read_interlace
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	b	.L216
.L218:
	ldr	ip, [r4, #448]
	cmp	ip, #0
	ldr	r1, [r4, #264]
	beq	.L242
	add	r1, r1, #1
	mov	r0, r4
	ldr	r2, [r4, #256]
	mov	r3, #0
	mov	lr, pc
	bx	ip
.L242:
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L243
	ldr	ip, [r4, #448]
	cmp	ip, #0
	ldr	r1, [r4, #264]
	addne	r1, r1, #1
	movne	r0, r4
	ldrne	r2, [r4, #256]
	movne	lr, pc
	bxne	ip
.L244:
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L243
	ldr	ip, [r4, #448]
	cmp	ip, #0
	ldr	r1, [r4, #264]
	addne	r1, r1, #1
	movne	r0, r4
	ldrne	r2, [r4, #256]
	movne	lr, pc
	bxne	ip
.L245:
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L243
	ldr	ip, [r4, #448]
	cmp	ip, #0
	ldr	r1, [r4, #264]
	addne	r1, r1, #1
	movne	r0, r4
	ldrne	r2, [r4, #256]
	movne	lr, pc
	bxne	ip
.L246:
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L243
	ldr	ip, [r4, #448]
	cmp	ip, #0
	ldr	r1, [r4, #264]
	addne	r1, r1, #1
	movne	r0, r4
	ldrne	r2, [r4, #256]
	movne	lr, pc
	bxne	ip
.L247:
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L243
	ldr	ip, [r4, #448]
	cmp	ip, #0
	ldr	r1, [r4, #264]
	addne	r1, r1, #1
	movne	r0, r4
	ldrne	r2, [r4, #256]
	movne	lr, pc
	bxne	ip
.L248:
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L243
	ldr	ip, [r4, #448]
	cmp	ip, #0
	ldr	r1, [r4, #264]
	addne	r1, r1, #1
	movne	r0, r4
	ldrne	r2, [r4, #256]
	movne	lr, pc
	bxne	ip
.L249:
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L243
	ldr	ip, [r4, #448]
	cmp	ip, #0
	ldr	r1, [r4, #264]
	addne	r1, r1, #1
	movne	r0, r4
	ldrne	r2, [r4, #256]
	movne	lr, pc
	bxne	ip
.L250:
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
.L243:
	cmp	r3, #2
	beq	.L285
.L251:
	cmp	r3, #4
	beq	.L286
.L252:
	cmp	r3, #6
	bne	.L272
	ldr	ip, [r4, #232]
	cmp	ip, #4
	bhi	.L272
	b	.L275
.L220:
	ldr	ip, [r4, #448]
	cmp	ip, #0
	ldr	r1, [r4, #264]
	beq	.L260
	add	r1, r1, #1
	mov	r0, r4
	ldr	r2, [r4, #256]
	mov	r3, #2
	mov	lr, pc
	bx	ip
.L260:
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #2
	bne	.L229
	ldr	ip, [r4, #448]
	cmp	ip, #0
	ldr	r1, [r4, #264]
	addne	r1, r1, #1
	movne	r0, r4
	ldrne	r2, [r4, #256]
	movne	lr, pc
	bxne	ip
.L261:
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #2
	bne	.L229
	ldr	ip, [r4, #448]
	cmp	ip, #0
	ldr	r1, [r4, #264]
	addne	r1, r1, #1
	movne	r0, r4
	ldrne	r2, [r4, #256]
	movne	lr, pc
	bxne	ip
.L262:
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #2
	bne	.L229
	ldr	ip, [r4, #448]
	cmp	ip, #0
	ldr	r1, [r4, #264]
	addne	r1, r1, #1
	movne	r0, r4
	ldrne	r2, [r4, #256]
	movne	lr, pc
	bxne	ip
.L254:
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #2
	bne	.L229
	ldr	ip, [r4, #448]
	cmp	ip, #0
	movne	r0, r4
	movne	r1, #0
	ldrne	r2, [r4, #256]
	movne	lr, pc
	bxne	ip
.L256:
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #2
	bne	.L229
	ldr	ip, [r4, #448]
	cmp	ip, #0
	movne	r0, r4
	movne	r1, #0
	ldrne	r2, [r4, #256]
	movne	lr, pc
	bxne	ip
.L257:
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #2
	bne	.L229
	ldr	ip, [r4, #448]
	cmp	ip, #0
	movne	r0, r4
	movne	r1, #0
	ldrne	r2, [r4, #256]
	movne	lr, pc
	bxne	ip
.L258:
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #2
	bne	.L229
	ldr	ip, [r4, #448]
	cmp	ip, #0
	movne	r0, r4
	movne	r1, #0
	ldrne	r2, [r4, #256]
	movne	lr, pc
	bxne	ip
.L265:
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
.L229:
	cmp	r3, #4
	bne	.L272
	mov	r1, #0
	mov	r0, r4
	bl	png_push_have_row
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r0, [r4, #320]	@ zero_extendqisi2
	cmp	r0, #4
	beq	.L275
	b	.L272
.L221:
	ldr	ip, [r4, #448]
	cmp	ip, #0
	ldr	r1, [r4, #264]
	beq	.L228
	add	r1, r1, #1
	mov	r0, r4
	ldr	r2, [r4, #256]
	mov	r3, #3
	mov	lr, pc
	bx	ip
.L228:
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #3
	bne	.L229
	ldr	ip, [r4, #448]
	cmp	ip, #0
	ldr	r1, [r4, #264]
	addne	r1, r1, #1
	movne	r0, r4
	ldrne	r2, [r4, #256]
	movne	lr, pc
	bxne	ip
.L230:
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #3
	bne	.L229
	ldr	ip, [r4, #448]
	cmp	ip, #0
	ldr	r1, [r4, #264]
	addne	r1, r1, #1
	movne	r0, r4
	ldrne	r2, [r4, #256]
	movne	lr, pc
	bxne	ip
.L231:
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #3
	bne	.L229
	ldr	ip, [r4, #448]
	cmp	ip, #0
	ldr	r1, [r4, #264]
	beq	.L265
	add	r1, r1, #1
	mov	r0, r4
	ldr	r2, [r4, #256]
	mov	lr, pc
	bx	ip
	b	.L265
.L222:
	ldr	ip, [r4, #264]
	mov	r0, r4
	add	r1, ip, #1
	bl	png_push_have_row
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #4
	bne	.L279
	ldr	lr, [r4, #264]
	mov	r0, r4
	add	r1, lr, #1
	bl	png_push_have_row
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #4
	bne	.L279
	ldr	ip, [r4, #448]
	cmp	ip, #0
	movne	r0, r4
	movne	r1, #0
	ldrne	r2, [r4, #256]
	movne	lr, pc
	bxne	ip
.L267:
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #4
	bne	.L279
	ldr	ip, [r4, #448]
	cmp	ip, #0
	beq	.L282
	mov	r0, r4
	mov	r1, #0
	ldr	r2, [r4, #256]
	mov	lr, pc
	bx	ip
	b	.L282
.L286:
	ldr	r0, [r4, #232]
	cmp	r0, #4
	bhi	.L272
	mov	r1, #0
	mov	r0, r4
	bl	png_push_have_row
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #4
	bne	.L252
	mov	r1, #0
	mov	r0, r4
	bl	png_push_have_row
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	b	.L252
.L285:
	mov	r1, #0
	mov	r0, r4
	bl	png_push_have_row
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #2
	bne	.L251
	mov	r1, #0
	mov	r0, r4
	bl	png_push_have_row
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #2
	bne	.L251
	mov	r1, #0
	mov	r0, r4
	bl	png_push_have_row
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	cmp	r3, #2
	bne	.L251
	mov	r1, #0
	mov	r0, r4
	bl	png_push_have_row
	mov	r0, r4
	bl	png_read_push_finish_row
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	b	.L251
	.size	png_push_process_row, .-png_push_process_row
	.section	.text.png_process_IDAT_data,"ax",%progbits
	.align	2
	.global	png_process_IDAT_data
	.hidden	png_process_IDAT_data
	.type	png_process_IDAT_data, %function
png_process_IDAT_data:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	ldr	r6, [r0, #136]
	mov	r3, r6, lsr #5
	cmp	r2, #0
	moveq	r3, #0
	andne	r3, r3, #1
	cmp	r3, #0
	mov	r4, r0
	mov	r5, r2
	mov	r6, r1
	bne	.L300
.L288:
	str	r5, [r4, #148]
	str	r6, [r4, #144]
	add	r5, r4, #144
	b	.L298
.L302:
	ldrb	ip, [r4, #320]	@ zero_extendqisi2
	cmp	ip, #6
	bhi	.L295
.L296:
	mov	r0, r4
	bl	png_push_process_row
	ldr	r2, [r4, #248]
	ldr	ip, [r4, #264]
	str	r2, [r4, #160]
	str	ip, [r4, #156]
.L298:
	mov	r1, #1
	mov	r0, r5
	bl	inflate
	subs	r3, r0, #0
	ldr	r1, .L304
	mov	r0, r4
	beq	.L289
	cmp	r3, #1
	beq	.L301
	cmn	r3, #5
	beq	.L299
	bl	png_error
.L289:
	ldr	r1, [r4, #160]
	cmp	r1, #0
	bne	.L299
	ldrb	r0, [r4, #319]	@ zero_extendqisi2
	cmp	r0, #0
	bne	.L302
	ldr	r2, [r4, #256]
	ldr	lr, [r4, #236]
	cmp	r2, lr
	bne	.L296
.L295:
	ldr	r3, [r4, #148]
	cmp	r3, #0
	movne	r0, r4
	ldrne	r1, .L304+4
	blne	png_warning
.L297:
	ldr	r0, [r4, #136]
	orr	r1, r0, #32
	str	r1, [r4, #136]
.L299:
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L300:
	ldr	r1, .L304+8
	bl	png_error
	b	.L288
.L301:
	ldr	r1, [r4, #148]
	cmp	r1, #0
	bne	.L303
.L291:
	ldr	r0, [r4, #160]
	cmp	r0, #0
	moveq	r0, r4
	bleq	png_push_process_row
.L292:
	add	r3, r4, #132
	ldmia	r3, {r3, lr}	@ phole ldm
	orr	ip, r3, #8
	orr	r2, lr, #32
	str	r2, [r4, #136]
	str	ip, [r4, #132]
	b	.L299
.L303:
	ldr	r1, .L304+12
	bl	png_error
	b	.L291
.L305:
	.align	2
.L304:
	.word	.LC10
	.word	.LC11
	.word	.LC8
	.word	.LC9
	.size	png_process_IDAT_data, .-png_process_IDAT_data
	.section	.text.png_push_read_IDAT,"ax",%progbits
	.align	2
	.global	png_push_read_IDAT
	.hidden	png_push_read_IDAT
	.type	png_push_read_IDAT, %function
png_push_read_IDAT:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	ldr	r3, [r0, #132]
	tst	r3, #256
	sub	sp, sp, #8
	mov	r4, r0
	ldrne	r5, [r0, #296]
	bne	.L308
	ldr	r3, [r0, #488]
	cmp	r3, #7
	bls	.L322
	ldr	r5, [r0, #480]
	cmp	r5, #0
	bne	.L311
	add	r6, sp, #4
	mov	r0, r6
	mov	r2, #4
.L312:
	ldr	r3, [r4, #492]
	cmp	r3, #0
	beq	.L313
	cmp	r2, r3
	movcc	r5, r2
	movcs	r5, r3
	mov	r2, r5
	ldr	r1, [r4, #464]
	bl	memcpy
	ldr	ip, [r4, #488]
	ldr	r1, [r4, #492]
	ldr	r3, [r4, #464]
	rsb	r2, r5, ip
	add	r0, r3, r5
	rsb	r5, r5, r1
	str	r2, [r4, #488]
	str	r5, [r4, #492]
	str	r0, [r4, #464]
.L313:
	mov	r1, r6
	mov	r0, r4
	bl	png_get_uint_31
	add	r5, r4, #312
	str	r0, [r4, #472]
	mov	r0, r4
	bl	png_reset_crc
	mov	r0, r4
	mov	r1, r5
	mov	r2, #4
	bl	png_crc_read
	ldr	r2, [r4, #132]
	orr	r0, r2, #256
	str	r0, [r4, #132]
	ldr	r1, .L326
	mov	r0, r5
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L314
	ldr	r1, [r4, #136]
	mov	lr, #1
	tst	r1, #32
	str	lr, [r4, #496]
	bne	.L321
	mov	r0, r4
	ldr	r1, .L326+4
	bl	png_error
	b	.L321
.L314:
	ldr	r5, [r4, #472]
	str	r5, [r4, #296]
.L308:
	cmp	r5, #0
	ldreq	r3, [r4, #488]
	bne	.L323
.L318:
	cmp	r3, #3
	mov	r0, r4
	bls	.L322
	mov	r1, #0
	bl	png_crc_finish
	ldr	r3, [r4, #132]
	bic	r0, r3, #256
	orr	ip, r0, #8
	str	ip, [r4, #132]
.L321:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L323:
	ldr	r6, [r4, #480]
	cmp	r6, #0
	beq	.L316
	cmp	r5, r6
	movcc	r6, r5
	mov	r0, r4
	ldr	r1, [r4, #456]
	mov	r2, r6
	bl	png_calculate_crc
	ldr	r5, [r4, #136]
	tst	r5, #32
	beq	.L324
.L317:
	ldr	lr, [r4, #296]
	ldr	r3, [r4, #488]
	ldr	r0, [r4, #480]
	ldr	r2, [r4, #456]
	rsb	r5, r6, lr
	add	ip, r2, r6
	rsb	r3, r6, r3
	cmp	r5, #0
	rsb	r6, r6, r0
	str	r6, [r4, #480]
	str	ip, [r4, #456]
	str	r5, [r4, #296]
	str	r3, [r4, #488]
	beq	.L318
.L316:
	ldr	r3, [r4, #492]
	cmp	r3, #0
	beq	.L321
	cmp	r5, r3
	movcs	r5, r3
	ldr	r1, [r4, #464]
	mov	r0, r4
	mov	r2, r5
	bl	png_calculate_crc
	ldr	r1, [r4, #136]
	tst	r1, #32
	beq	.L325
.L319:
	ldr	r2, [r4, #296]
	ldr	r3, [r4, #488]
	ldr	r0, [r4, #492]
	ldr	r1, [r4, #464]
	rsb	ip, r5, r2
	add	lr, r1, r5
	rsb	r3, r5, r3
	cmp	ip, #0
	rsb	r5, r5, r0
	str	r5, [r4, #492]
	str	lr, [r4, #464]
	str	ip, [r4, #296]
	str	r3, [r4, #488]
	bne	.L321
	b	.L318
.L322:
	bl	png_push_save_buffer
	b	.L321
.L311:
	cmp	r5, #4
	movcs	r5, #4
	add	r6, sp, #4
	ldr	r1, [r4, #456]
	mov	r2, r5
	mov	r0, r6
	bl	memcpy
	ldr	ip, [r4, #488]
	ldr	r2, [r4, #456]
	ldr	r3, [r4, #480]
	rsb	r0, r5, ip
	rsb	r1, r5, r3
	add	ip, r2, r5
	rsbs	r2, r5, #4
	str	r0, [r4, #488]
	str	r1, [r4, #480]
	str	ip, [r4, #456]
	beq	.L313
	add	r0, r6, r5
	b	.L312
.L324:
	mov	r0, r4
	ldr	r1, [r4, #456]
	mov	r2, r6
	bl	png_process_IDAT_data
	b	.L317
.L325:
	mov	r0, r4
	ldr	r1, [r4, #464]
	mov	r2, r5
	bl	png_process_IDAT_data
	b	.L319
.L327:
	.align	2
.L326:
	.word	png_IDAT
	.word	.LC12
	.size	png_push_read_IDAT, .-png_push_read_IDAT
	.section	.text.png_push_read_chunk,"ax",%progbits
	.align	2
	.global	png_push_read_chunk
	.hidden	png_push_read_chunk
	.type	png_push_read_chunk, %function
png_push_read_chunk:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	ldr	r6, [r0, #132]
	tst	r6, #256
	sub	sp, sp, #12
	mov	r4, r0
	mov	r7, r1
	addne	r5, r0, #312
	bne	.L329
	ldr	r3, [r0, #488]
	cmp	r3, #7
	bls	.L395
	ldr	r5, [r0, #480]
	cmp	r5, #0
	bne	.L332
	add	r6, sp, #4
	mov	r0, r6
	mov	r2, #4
.L333:
	ldr	r3, [r4, #492]
	cmp	r3, #0
	beq	.L334
	cmp	r2, r3
	movcc	r5, r2
	movcs	r5, r3
	mov	r2, r5
	ldr	r1, [r4, #464]
	bl	memcpy
	ldr	ip, [r4, #488]
	ldr	r1, [r4, #492]
	ldr	r3, [r4, #464]
	rsb	r2, r5, ip
	add	r0, r3, r5
	rsb	r5, r5, r1
	str	r2, [r4, #488]
	str	r5, [r4, #492]
	str	r0, [r4, #464]
.L334:
	mov	r1, r6
	mov	r0, r4
	bl	png_get_uint_31
	add	r5, r4, #312
	str	r0, [r4, #472]
	mov	r0, r4
	bl	png_reset_crc
	mov	r2, #4
	mov	r0, r4
	mov	r1, r5
	bl	png_crc_read
	mov	r0, r4
	mov	r1, r5
	bl	png_check_chunk_name
	ldr	r6, [r4, #132]
	orr	r6, r6, #256
	str	r6, [r4, #132]
.L329:
	mov	r0, r5
	ldr	r1, .L401
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	bne	.L335
	tst	r6, #8
	orrne	r6, r6, #8192
	strne	r6, [r4, #132]
.L335:
	mov	r0, r5
	ldr	r1, .L401+4
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	bne	.L336
	ldr	r2, [r4, #472]
	cmp	r2, #13
	beq	.L338
	mov	r0, r4
	ldr	r1, .L401+8
	bl	png_error
	ldr	r2, [r4, #472]
.L338:
	ldr	r0, [r4, #488]
	add	lr, r2, #4
	cmp	lr, r0
	bhi	.L396
	mov	r1, r7
	mov	r0, r4
	bl	png_handle_IHDR
	ldr	r3, [r4, #132]
.L340:
	bic	r2, r3, #256
	str	r2, [r4, #132]
.L390:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L336:
	mov	r0, r5
	ldr	r1, .L401+12
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	bne	.L341
	ldr	r2, [r4, #472]
	ldr	ip, [r4, #488]
	add	r1, r2, #4
	cmp	r1, ip
	bhi	.L396
	mov	r0, r4
	mov	r1, r7
	bl	png_handle_IEND
	ldr	r3, [r4, #452]
	mov	r2, #6
	cmp	r3, #0
	str	r2, [r4, #496]
	beq	.L393
	mov	r1, r7
	mov	r0, r4
	mov	lr, pc
	bx	r3
	ldr	r3, [r4, #132]
	b	.L340
.L341:
	mov	r0, r4
	mov	r1, r5
	bl	png_handle_as_unknown
	cmp	r0, #0
	beq	.L344
	ldr	r6, [r4, #472]
	ldr	r3, [r4, #488]
	add	lr, r6, #4
	cmp	lr, r3
	bls	.L345
.L396:
	mov	r0, r4
	bl	png_push_save_buffer
	b	.L390
.L394:
	mov	r0, r4
.L395:
	bl	png_push_save_buffer
	b	.L390
.L332:
	cmp	r5, #4
	movcs	r5, #4
	add	r6, sp, #4
	ldr	r1, [r4, #456]
	mov	r2, r5
	mov	r0, r6
	bl	memcpy
	ldr	ip, [r4, #488]
	ldr	r2, [r4, #456]
	ldr	r3, [r4, #480]
	rsb	r0, r5, ip
	rsb	r1, r5, r3
	add	ip, r2, r5
	rsbs	r2, r5, #4
	str	r0, [r4, #488]
	str	r1, [r4, #480]
	str	ip, [r4, #456]
	beq	.L334
	add	r0, r6, r5
	b	.L333
.L344:
	mov	r0, r5
	ldr	r1, .L401+16
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L397
	mov	r0, r5
	ldr	r1, .L401
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	bne	.L352
	ldr	r3, [r4, #132]
	tst	r3, #1
	beq	.L398
	ldrb	r2, [r4, #322]	@ zero_extendqisi2
	cmp	r2, #3
	beq	.L399
.L354:
	tst	r3, #4
	beq	.L355
	tst	r3, #8192
	bne	.L356
	ldr	r0, [r4, #472]
	cmp	r0, #0
	beq	.L390
.L356:
	tst	r3, #8
	beq	.L355
	mov	r0, r4
	ldr	r1, .L401+20
	bl	png_error
	ldr	r3, [r4, #132]
.L355:
	ldr	r2, [r4, #472]
	orr	lr, r3, #4
	mov	ip, #2
	mov	r1, r7
	str	r2, [r4, #296]
	str	lr, [r4, #132]
	str	ip, [r4, #496]
	mov	r0, r4
	bl	png_push_have_info
	ldr	r3, [r4, #248]
	ldr	r1, [r4, #264]
	str	r3, [r4, #160]
	str	r1, [r4, #156]
	b	.L390
.L345:
	ldr	r1, .L401
	mov	r2, #4
	mov	r0, r5
	bl	memcmp
	cmp	r0, #0
	ldreq	r3, [r4, #132]
	orreq	r3, r3, #4
	streq	r3, [r4, #132]
	mov	r1, r7
	mov	r2, r6
	mov	r0, r4
	bl	png_handle_unknown
	mov	r0, r5
	ldr	r1, .L401+16
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	bne	.L347
	ldr	r0, [r4, #132]
	orr	r3, r0, #2
	str	r3, [r4, #132]
	b	.L340
.L397:
	ldr	r2, [r4, #472]
	ldr	lr, [r4, #488]
	add	ip, r2, #4
	cmp	ip, lr
	bhi	.L396
	mov	r1, r7
	mov	r0, r4
	bl	png_handle_PLTE
	ldr	r3, [r4, #132]
	b	.L340
.L387:
	cmp	r1, r3
	bhi	.L394
	mov	r1, r7
	mov	r0, r4
	bl	png_push_handle_unknown
.L393:
	ldr	r3, [r4, #132]
	b	.L340
.L347:
	mov	r0, r5
	ldr	r1, .L401
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	bne	.L393
	ldr	r3, [r4, #132]
	tst	r3, #1
	beq	.L400
	ldrb	r1, [r4, #322]	@ zero_extendqisi2
	cmp	r1, #3
	bne	.L340
	tst	r3, #2
	bne	.L340
	mov	r0, r4
	ldr	r1, .L401+24
	bl	png_error
	ldr	r3, [r4, #132]
	b	.L340
.L352:
	mov	r0, r5
	ldr	r1, .L401+28
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	bne	.L357
	ldr	r2, [r4, #472]
	ldr	r3, [r4, #488]
	add	r0, r2, #4
	cmp	r0, r3
	bhi	.L396
	mov	r1, r7
	mov	r0, r4
	bl	png_handle_gAMA
	ldr	r3, [r4, #132]
	b	.L340
.L357:
	mov	r0, r5
	ldr	r1, .L401+32
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	bne	.L359
	ldr	r2, [r4, #472]
	ldr	ip, [r4, #488]
	add	r1, r2, #4
	cmp	r1, ip
	bhi	.L396
	mov	r1, r7
	mov	r0, r4
	bl	png_handle_sBIT
	ldr	r3, [r4, #132]
	b	.L340
.L398:
	mov	r0, r4
	ldr	r1, .L401+36
	bl	png_error
	ldr	r3, [r4, #132]
	b	.L354
.L359:
	mov	r0, r5
	ldr	r1, .L401+40
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	bne	.L361
	ldr	r2, [r4, #472]
	ldr	r0, [r4, #488]
	add	lr, r2, #4
	cmp	lr, r0
	bhi	.L396
	mov	r1, r7
	mov	r0, r4
	bl	png_handle_cHRM
	ldr	r3, [r4, #132]
	b	.L340
.L400:
	mov	r0, r4
	ldr	r1, .L401+36
	bl	png_error
	ldr	r3, [r4, #132]
	b	.L340
.L399:
	tst	r3, #2
	bne	.L354
	mov	r0, r4
	ldr	r1, .L401+24
	bl	png_error
	ldr	r3, [r4, #132]
	b	.L354
.L361:
	mov	r0, r5
	ldr	r1, .L401+44
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	bne	.L363
	ldr	r2, [r4, #472]
	ldr	r3, [r4, #488]
	add	r1, r2, #4
	cmp	r1, r3
	bhi	.L396
	mov	r1, r7
	mov	r0, r4
	bl	png_handle_sRGB
	ldr	r3, [r4, #132]
	b	.L340
.L363:
	mov	r0, r5
	ldr	r1, .L401+48
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	bne	.L365
	ldr	r2, [r4, #472]
	ldr	lr, [r4, #488]
	add	ip, r2, #4
	cmp	ip, lr
	bhi	.L396
	mov	r1, r7
	mov	r0, r4
	bl	png_handle_iCCP
	ldr	r3, [r4, #132]
	b	.L340
.L365:
	mov	r0, r5
	ldr	r1, .L401+52
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	bne	.L367
	ldr	r2, [r4, #472]
	ldr	r1, [r4, #488]
	add	r0, r2, #4
	cmp	r0, r1
	bhi	.L396
	mov	r1, r7
	mov	r0, r4
	bl	png_handle_sPLT
	ldr	r3, [r4, #132]
	b	.L340
.L367:
	mov	r0, r5
	ldr	r1, .L401+56
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	bne	.L369
	ldr	r2, [r4, #472]
	ldr	r3, [r4, #488]
	add	ip, r2, #4
	cmp	ip, r3
	bhi	.L396
	mov	r1, r7
	mov	r0, r4
	bl	png_handle_tRNS
	ldr	r3, [r4, #132]
	b	.L340
.L369:
	mov	r0, r5
	ldr	r1, .L401+60
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	bne	.L371
	ldr	r2, [r4, #472]
	ldr	r0, [r4, #488]
	add	lr, r2, #4
	cmp	lr, r0
	bhi	.L396
	mov	r1, r7
	mov	r0, r4
	bl	png_handle_bKGD
	ldr	r3, [r4, #132]
	b	.L340
.L371:
	mov	r0, r5
	ldr	r1, .L401+64
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	bne	.L373
	ldr	r2, [r4, #472]
	ldr	ip, [r4, #488]
	add	r1, r2, #4
	cmp	r1, ip
	bhi	.L396
	mov	r1, r7
	mov	r0, r4
	bl	png_handle_hIST
	ldr	r3, [r4, #132]
	b	.L340
.L373:
	mov	r0, r5
	ldr	r1, .L401+68
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	bne	.L375
	ldr	r2, [r4, #472]
	ldr	r3, [r4, #488]
	add	lr, r2, #4
	cmp	lr, r3
	bhi	.L396
	mov	r1, r7
	mov	r0, r4
	bl	png_handle_pHYs
	ldr	r3, [r4, #132]
	b	.L340
.L375:
	mov	r0, r5
	ldr	r1, .L401+72
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	bne	.L377
	ldr	r2, [r4, #472]
	ldr	r1, [r4, #488]
	add	r0, r2, #4
	cmp	r0, r1
	bhi	.L396
	mov	r1, r7
	mov	r0, r4
	bl	png_handle_oFFs
	ldr	r3, [r4, #132]
	b	.L340
.L377:
	mov	r0, r5
	ldr	r1, .L401+76
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	bne	.L379
	ldr	r2, [r4, #472]
	ldr	lr, [r4, #488]
	add	ip, r2, #4
	cmp	ip, lr
	bhi	.L396
	mov	r1, r7
	mov	r0, r4
	bl	png_handle_pCAL
	ldr	r3, [r4, #132]
	b	.L340
.L379:
	mov	r0, r5
	ldr	r1, .L401+80
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	bne	.L381
	ldr	r2, [r4, #472]
	ldr	r3, [r4, #488]
	add	r0, r2, #4
	cmp	r0, r3
	bhi	.L396
	mov	r1, r7
	mov	r0, r4
	bl	png_handle_sCAL
	ldr	r3, [r4, #132]
	b	.L340
.L381:
	mov	r0, r5
	ldr	r1, .L401+84
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	bne	.L383
	ldr	r2, [r4, #472]
	ldr	ip, [r4, #488]
	add	r1, r2, #4
	cmp	r1, ip
	bhi	.L396
	mov	r1, r7
	mov	r0, r4
	bl	png_handle_tIME
	ldr	r3, [r4, #132]
	b	.L340
.L383:
	mov	r0, r5
	ldr	r1, .L401+88
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	bne	.L385
	ldr	r2, [r4, #472]
	ldr	r0, [r4, #488]
	add	lr, r2, #4
	cmp	lr, r0
	bhi	.L396
	mov	r1, r7
	mov	r0, r4
	bl	png_push_handle_tEXt
	ldr	r3, [r4, #132]
	b	.L340
.L385:
	ldr	r1, .L401+92
	mov	r2, #4
	mov	r0, r5
	bl	memcmp
	ldr	r2, [r4, #472]
	cmp	r0, #0
	ldr	r3, [r4, #488]
	add	r1, r2, #4
	bne	.L387
	cmp	r1, r3
	bhi	.L394
	mov	r1, r7
	mov	r0, r4
	bl	png_push_handle_zTXt
	ldr	r3, [r4, #132]
	b	.L340
.L402:
	.align	2
.L401:
	.word	png_IDAT
	.word	png_IHDR
	.word	.LC13
	.word	png_IEND
	.word	png_PLTE
	.word	.LC16
	.word	.LC15
	.word	png_gAMA
	.word	png_sBIT
	.word	.LC14
	.word	png_cHRM
	.word	png_sRGB
	.word	png_iCCP
	.word	png_sPLT
	.word	png_tRNS
	.word	png_bKGD
	.word	png_hIST
	.word	png_pHYs
	.word	png_oFFs
	.word	png_pCAL
	.word	png_sCAL
	.word	png_tIME
	.word	png_tEXt
	.word	png_zTXt
	.size	png_push_read_chunk, .-png_push_read_chunk
	.section	.text.png_process_some_data,"ax",%progbits
	.align	2
	.global	png_process_some_data
	.hidden	png_process_some_data
	.type	png_process_some_data, %function
png_process_some_data:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	bxeq	lr
	ldr	r3, [r0, #496]
	cmp	r3, #5
	ldrls	pc, [pc, r3, asl #2]
	b	.L405
.L412:
	.word	.L406
	.word	.L407
	.word	.L408
	.word	.L409
	.word	.L410
	.word	.L411
.L411:
	b	png_push_read_zTXt
.L405:
	mov	r1, #0
	str	r1, [r0, #488]
	bx	lr
.L406:
	b	png_push_read_sig
.L407:
	b	png_push_read_chunk
.L408:
	b	png_push_read_IDAT
.L409:
	b	png_push_crc_finish
.L410:
	b	png_push_read_tEXt
	.size	png_process_some_data, .-png_process_some_data
	.section	.text.png_process_data,"ax",%progbits
	.align	2
	.global	png_process_data
	.hidden	png_process_data
	.type	png_process_data, %function
png_process_data:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	cmpne	r1, #0
	stmfd	sp!, {r3, r4, r5, lr}
	mov	r5, r1
	mov	r4, r0
	beq	.L417
	ldr	r0, [r0, #480]
	add	r1, r3, r0
	cmp	r1, #0
	str	r2, [r4, #464]
	str	r2, [r4, #468]
	str	r3, [r4, #492]
	str	r1, [r4, #488]
	beq	.L417
.L418:
	mov	r0, r4
	mov	r1, r5
	bl	png_process_some_data
	ldr	r3, [r4, #488]
	cmp	r3, #0
	bne	.L418
.L417:
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
	.size	png_process_data, .-png_process_data
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"unknown critical chunk\000"
	.space	1
.LC1:
	.ascii	"error in user chunk\000"
.LC2:
	.ascii	"Out of place zTXt\000"
	.space	2
.LC3:
	.ascii	"Out of place tEXt\000"
	.space	2
.LC4:
	.ascii	"Potential overflow of save_buffer\000"
	.space	2
.LC5:
	.ascii	"Insufficient memory to store text chunk.\000"
	.space	3
.LC6:
	.ascii	"Not a PNG file\000"
	.space	1
.LC7:
	.ascii	"PNG file corrupted by ASCII conversion\000"
	.space	1
.LC8:
	.ascii	"Extra compression data\000"
	.space	1
.LC9:
	.ascii	"Extra compressed data\000"
	.space	2
.LC10:
	.ascii	"Decompression Error\000"
.LC11:
	.ascii	"Too much data in IDAT chunks\000"
	.space	3
.LC12:
	.ascii	"Not enough compressed data\000"
	.space	1
.LC13:
	.ascii	"Invalid IHDR length\000"
.LC14:
	.ascii	"Missing IHDR before IDAT\000"
	.space	3
.LC15:
	.ascii	"Missing PLTE before IDAT\000"
	.space	3
.LC16:
	.ascii	"Too many IDAT's found\000"
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
