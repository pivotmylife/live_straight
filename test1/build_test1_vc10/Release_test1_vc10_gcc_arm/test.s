	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"test.cpp"
	.section	.text._ZN7QuTimerD1Ev,"axG",%progbits,_ZN7QuTimerD1Ev,comdat
	.align	2
	.weak	_ZN7QuTimerD1Ev
	.hidden	_ZN7QuTimerD1Ev
	.type	_ZN7QuTimerD1Ev, %function
_ZN7QuTimerD1Ev:
	.fnstart
.LFB2684:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L3
	str	r3, [r0, #0]
	bx	lr
.L4:
	.align	2
.L3:
	.word	_ZTV7QuTimer+8
	.cantunwind
	.fnend
	.size	_ZN7QuTimerD1Ev, .-_ZN7QuTimerD1Ev
	.section	.text._ZN7QuTimer6updateEv,"axG",%progbits,_ZN7QuTimer6updateEv,comdat
	.align	2
	.weak	_ZN7QuTimer6updateEv
	.hidden	_ZN7QuTimer6updateEv
	.type	_ZN7QuTimer6updateEv, %function
_ZN7QuTimer6updateEv:
	.fnstart
.LFB2686:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r1, [r0, #8]
	add	r3, r1, #1
	str	r3, [r0, #8]
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN7QuTimer6updateEv, .-_ZN7QuTimer6updateEv
	.section	.text._ZN7QuTimer5resetEv,"axG",%progbits,_ZN7QuTimer5resetEv,comdat
	.align	2
	.weak	_ZN7QuTimer5resetEv
	.hidden	_ZN7QuTimer5resetEv
	.type	_ZN7QuTimer5resetEv, %function
_ZN7QuTimer5resetEv:
	.fnstart
.LFB2687:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, [r0, #4]
	str	r3, [r0, #8]
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN7QuTimer5resetEv, .-_ZN7QuTimer5resetEv
	.section	.text._ZN22QuSoundStructInterfaceD1Ev,"axG",%progbits,_ZN22QuSoundStructInterfaceD1Ev,comdat
	.align	2
	.weak	_ZN22QuSoundStructInterfaceD1Ev
	.hidden	_ZN22QuSoundStructInterfaceD1Ev
	.type	_ZN22QuSoundStructInterfaceD1Ev, %function
_ZN22QuSoundStructInterfaceD1Ev:
	.fnstart
.LFB2773:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r1, .L11
	ldr	r2, .L11+4
	str	r1, [r0, #4]
	str	r2, [r0, #0]
	bx	lr
.L12:
	.align	2
.L11:
	.word	_ZTV7QuTimer+8
	.word	_ZTV22QuSoundStructInterface+8
	.cantunwind
	.fnend
	.size	_ZN22QuSoundStructInterfaceD1Ev, .-_ZN22QuSoundStructInterfaceD1Ev
	.global	__aeabi_i2f
	.global	__aeabi_fdiv
	.global	__aeabi_fsub
	.global	__aeabi_fmul
	.global	__aeabi_fadd
	.section	.text._ZN22QuSoundStructInterface6updateEv,"axG",%progbits,_ZN22QuSoundStructInterface6updateEv,comdat
	.align	2
	.weak	_ZN22QuSoundStructInterface6updateEv
	.hidden	_ZN22QuSoundStructInterface6updateEv
	.type	_ZN22QuSoundStructInterface6updateEv, %function
_ZN22QuSoundStructInterface6updateEv:
	.fnstart
.LFB2775:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	.save {r4, r5, r6, r7, r8, lr}
	mov	r4, r0
	ldr	r5, [r4, #16]
	ldr	r0, [r0, #12]
	ldr	r3, [r4, #0]
	cmp	r0, r5
	ldr	r6, [r3, #20]
	movge	r5, #1065353216
	bge	.L15
	ldr	r7, [r4, #8]
	rsb	r0, r7, r0
	bl	__aeabi_i2f
	mov	r8, r0
	rsb	r0, r7, r5
	bl	__aeabi_i2f
	mov	r1, r0
	mov	r0, r8
	bl	__aeabi_fdiv
	mov	r5, r0
.L15:
	mov	r1, r5
	mov	r0, #1065353216
	bl	__aeabi_fsub
	ldr	r1, [r4, #24]	@ float
	bl	__aeabi_fmul
	ldr	r1, [r4, #20]	@ float
	mov	r7, r0
	mov	r0, r5
	bl	__aeabi_fmul
	mov	r1, r0
	mov	r0, r7
	bl	__aeabi_fadd
	mov	r1, r0
	mov	r0, r4
	mov	lr, pc
	bx	r6
	add	r0, r4, #4
	ldr	r1, [r4, #4]
	ldr	ip, [r1, #8]
	mov	lr, pc
	bx	ip
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
	.fnend
	.size	_ZN22QuSoundStructInterface6updateEv, .-_ZN22QuSoundStructInterface6updateEv
	.section	.text._ZN23QuSoundManagerInterfaceD1Ev,"axG",%progbits,_ZN23QuSoundManagerInterfaceD1Ev,comdat
	.align	2
	.weak	_ZN23QuSoundManagerInterfaceD1Ev
	.hidden	_ZN23QuSoundManagerInterfaceD1Ev
	.type	_ZN23QuSoundManagerInterfaceD1Ev, %function
_ZN23QuSoundManagerInterfaceD1Ev:
	.fnstart
.LFB2796:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L20
	str	r3, [r0, #0]
	bx	lr
.L21:
	.align	2
.L20:
	.word	_ZTV23QuSoundManagerInterface+8
	.cantunwind
	.fnend
	.size	_ZN23QuSoundManagerInterfaceD1Ev, .-_ZN23QuSoundManagerInterfaceD1Ev
	.section	.text._ZN23QuSoundManagerInterface6updateEv,"axG",%progbits,_ZN23QuSoundManagerInterface6updateEv,comdat
	.align	2
	.weak	_ZN23QuSoundManagerInterface6updateEv
	.hidden	_ZN23QuSoundManagerInterface6updateEv
	.type	_ZN23QuSoundManagerInterface6updateEv, %function
_ZN23QuSoundManagerInterface6updateEv:
	.fnstart
.LFB2799:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN23QuSoundManagerInterface6updateEv, .-_ZN23QuSoundManagerInterface6updateEv
	.section	.text._ZN13QuBaseManagerD1Ev,"axG",%progbits,_ZN13QuBaseManagerD1Ev,comdat
	.align	2
	.weak	_ZN13QuBaseManagerD1Ev
	.hidden	_ZN13QuBaseManagerD1Ev
	.type	_ZN13QuBaseManagerD1Ev, %function
_ZN13QuBaseManagerD1Ev:
	.fnstart
.LFB3201:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L26
	str	r3, [r0, #0]
	bx	lr
.L27:
	.align	2
.L26:
	.word	_ZTV13QuBaseManager+8
	.cantunwind
	.fnend
	.size	_ZN13QuBaseManagerD1Ev, .-_ZN13QuBaseManagerD1Ev
	.section	.text._ZN13QuBaseManager10initializeEv,"axG",%progbits,_ZN13QuBaseManager10initializeEv,comdat
	.align	2
	.weak	_ZN13QuBaseManager10initializeEv
	.hidden	_ZN13QuBaseManager10initializeEv
	.type	_ZN13QuBaseManager10initializeEv, %function
_ZN13QuBaseManager10initializeEv:
	.fnstart
.LFB3203:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN13QuBaseManager10initializeEv, .-_ZN13QuBaseManager10initializeEv
	.section	.text._ZN13QuBaseManager6updateEv,"axG",%progbits,_ZN13QuBaseManager6updateEv,comdat
	.align	2
	.weak	_ZN13QuBaseManager6updateEv
	.hidden	_ZN13QuBaseManager6updateEv
	.type	_ZN13QuBaseManager6updateEv, %function
_ZN13QuBaseManager6updateEv:
	.fnstart
.LFB3204:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN13QuBaseManager6updateEv, .-_ZN13QuBaseManager6updateEv
	.section	.text._ZN13QuBaseManager7keyDownEi,"axG",%progbits,_ZN13QuBaseManager7keyDownEi,comdat
	.align	2
	.weak	_ZN13QuBaseManager7keyDownEi
	.hidden	_ZN13QuBaseManager7keyDownEi
	.type	_ZN13QuBaseManager7keyDownEi, %function
_ZN13QuBaseManager7keyDownEi:
	.fnstart
.LFB3205:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN13QuBaseManager7keyDownEi, .-_ZN13QuBaseManager7keyDownEi
	.section	.text._ZN13QuBaseManager5keyUpEi,"axG",%progbits,_ZN13QuBaseManager5keyUpEi,comdat
	.align	2
	.weak	_ZN13QuBaseManager5keyUpEi
	.hidden	_ZN13QuBaseManager5keyUpEi
	.type	_ZN13QuBaseManager5keyUpEi, %function
_ZN13QuBaseManager5keyUpEi:
	.fnstart
.LFB3206:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN13QuBaseManager5keyUpEi, .-_ZN13QuBaseManager5keyUpEi
	.section	.text._ZN13QuBaseManager10singleDownEi13QuScreenCoord,"axG",%progbits,_ZN13QuBaseManager10singleDownEi13QuScreenCoord,comdat
	.align	2
	.weak	_ZN13QuBaseManager10singleDownEi13QuScreenCoord
	.hidden	_ZN13QuBaseManager10singleDownEi13QuScreenCoord
	.type	_ZN13QuBaseManager10singleDownEi13QuScreenCoord, %function
_ZN13QuBaseManager10singleDownEi13QuScreenCoord:
	.fnstart
.LFB3207:
	@ Function supports interworking.
	@ args = 16, pretend = 8, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	sub	sp, sp, #8
	mov	r1, sp
	stmia	r1, {r2, r3}
	add	sp, sp, #8
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN13QuBaseManager10singleDownEi13QuScreenCoord, .-_ZN13QuBaseManager10singleDownEi13QuScreenCoord
	.section	.text._ZN13QuBaseManager8singleUpEi13QuScreenCoord,"axG",%progbits,_ZN13QuBaseManager8singleUpEi13QuScreenCoord,comdat
	.align	2
	.weak	_ZN13QuBaseManager8singleUpEi13QuScreenCoord
	.hidden	_ZN13QuBaseManager8singleUpEi13QuScreenCoord
	.type	_ZN13QuBaseManager8singleUpEi13QuScreenCoord, %function
_ZN13QuBaseManager8singleUpEi13QuScreenCoord:
	.fnstart
.LFB3208:
	@ Function supports interworking.
	@ args = 16, pretend = 8, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	sub	sp, sp, #8
	mov	r1, sp
	stmia	r1, {r2, r3}
	add	sp, sp, #8
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN13QuBaseManager8singleUpEi13QuScreenCoord, .-_ZN13QuBaseManager8singleUpEi13QuScreenCoord
	.section	.text._ZN13QuBaseManager12singleMotionE13QuScreenCoordS0_,"axG",%progbits,_ZN13QuBaseManager12singleMotionE13QuScreenCoordS0_,comdat
	.align	2
	.weak	_ZN13QuBaseManager12singleMotionE13QuScreenCoordS0_
	.hidden	_ZN13QuBaseManager12singleMotionE13QuScreenCoordS0_
	.type	_ZN13QuBaseManager12singleMotionE13QuScreenCoordS0_, %function
_ZN13QuBaseManager12singleMotionE13QuScreenCoordS0_:
	.fnstart
.LFB3209:
	@ Function supports interworking.
	@ args = 36, pretend = 16, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	sub	sp, sp, #16
	add	r0, sp, #4
	stmia	r0, {r1, r2, r3}
	add	sp, sp, #16
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN13QuBaseManager12singleMotionE13QuScreenCoordS0_, .-_ZN13QuBaseManager12singleMotionE13QuScreenCoordS0_
	.section	.text._ZN13QuBaseManager17singleDeltaMotionEv,"axG",%progbits,_ZN13QuBaseManager17singleDeltaMotionEv,comdat
	.align	2
	.weak	_ZN13QuBaseManager17singleDeltaMotionEv
	.hidden	_ZN13QuBaseManager17singleDeltaMotionEv
	.type	_ZN13QuBaseManager17singleDeltaMotionEv, %function
_ZN13QuBaseManager17singleDeltaMotionEv:
	.fnstart
.LFB3210:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN13QuBaseManager17singleDeltaMotionEv, .-_ZN13QuBaseManager17singleDeltaMotionEv
	.section	.text._ZN13QuBaseManager12screenResizeEii,"axG",%progbits,_ZN13QuBaseManager12screenResizeEii,comdat
	.align	2
	.weak	_ZN13QuBaseManager12screenResizeEii
	.hidden	_ZN13QuBaseManager12screenResizeEii
	.type	_ZN13QuBaseManager12screenResizeEii, %function
_ZN13QuBaseManager12screenResizeEii:
	.fnstart
.LFB3211:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN13QuBaseManager12screenResizeEii, .-_ZN13QuBaseManager12screenResizeEii
	.section	.text._ZN13QuBaseManager10multiTouchEibii,"axG",%progbits,_ZN13QuBaseManager10multiTouchEibii,comdat
	.align	2
	.weak	_ZN13QuBaseManager10multiTouchEibii
	.hidden	_ZN13QuBaseManager10multiTouchEibii
	.type	_ZN13QuBaseManager10multiTouchEibii, %function
_ZN13QuBaseManager10multiTouchEibii:
	.fnstart
.LFB3212:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN13QuBaseManager10multiTouchEibii, .-_ZN13QuBaseManager10multiTouchEibii
	.section	.text._ZN13QuBaseManager11multiMotionEiii,"axG",%progbits,_ZN13QuBaseManager11multiMotionEiii,comdat
	.align	2
	.weak	_ZN13QuBaseManager11multiMotionEiii
	.hidden	_ZN13QuBaseManager11multiMotionEiii
	.type	_ZN13QuBaseManager11multiMotionEiii, %function
_ZN13QuBaseManager11multiMotionEiii:
	.fnstart
.LFB3213:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN13QuBaseManager11multiMotionEiii, .-_ZN13QuBaseManager11multiMotionEiii
	.section	.text._ZN13QuBasicCameraD1Ev,"axG",%progbits,_ZN13QuBasicCameraD1Ev,comdat
	.align	2
	.weak	_ZN13QuBasicCameraD1Ev
	.hidden	_ZN13QuBasicCameraD1Ev
	.type	_ZN13QuBasicCameraD1Ev, %function
_ZN13QuBasicCameraD1Ev:
	.fnstart
.LFB3711:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L52
	str	r3, [r0, #0]
	bx	lr
.L53:
	.align	2
.L52:
	.word	_ZTV13QuBasicCamera+8
	.cantunwind
	.fnend
	.size	_ZN13QuBasicCameraD1Ev, .-_ZN13QuBasicCameraD1Ev
	.section	.text._ZN11QuBoxButton16drawRotatedImageE15QuStupidPointerI11QuBaseImageE15GDeviceRotation,"axG",%progbits,_ZN11QuBoxButton16drawRotatedImageE15QuStupidPointerI11QuBaseImageE15GDeviceRotation,comdat
	.align	2
	.weak	_ZN11QuBoxButton16drawRotatedImageE15QuStupidPointerI11QuBaseImageE15GDeviceRotation
	.hidden	_ZN11QuBoxButton16drawRotatedImageE15QuStupidPointerI11QuBaseImageE15GDeviceRotation
	.type	_ZN11QuBoxButton16drawRotatedImageE15QuStupidPointerI11QuBaseImageE15GDeviceRotation, %function
_ZN11QuBoxButton16drawRotatedImageE15QuStupidPointerI11QuBaseImageE15GDeviceRotation:
	.fnstart
.LFB3849:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN11QuBoxButton16drawRotatedImageE15QuStupidPointerI11QuBaseImageE15GDeviceRotation, .-_ZN11QuBoxButton16drawRotatedImageE15QuStupidPointerI11QuBaseImageE15GDeviceRotation
	.section	.text._ZN11QuBoxButton15drawRotatedRectE7QuColor15GDeviceRotation,"axG",%progbits,_ZN11QuBoxButton15drawRotatedRectE7QuColor15GDeviceRotation,comdat
	.align	2
	.weak	_ZN11QuBoxButton15drawRotatedRectE7QuColor15GDeviceRotation
	.hidden	_ZN11QuBoxButton15drawRotatedRectE7QuColor15GDeviceRotation
	.type	_ZN11QuBoxButton15drawRotatedRectE7QuColor15GDeviceRotation, %function
_ZN11QuBoxButton15drawRotatedRectE7QuColor15GDeviceRotation:
	.fnstart
.LFB3850:
	@ Function supports interworking.
	@ args = 24, pretend = 16, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	sub	sp, sp, #16
	add	r0, sp, #4
	stmia	r0, {r1, r2, r3}
	add	sp, sp, #16
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN11QuBoxButton15drawRotatedRectE7QuColor15GDeviceRotation, .-_ZN11QuBoxButton15drawRotatedRectE7QuColor15GDeviceRotation
	.section	.text._ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE8overflowEi,"axG",%progbits,_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE8overflowEi,comdat
	.align	2
	.weak	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE8overflowEi
	.hidden	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE8overflowEi
	.type	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE8overflowEi, %function
_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE8overflowEi:
	.fnstart
.LFB5769:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mvn	r0, #0
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE8overflowEi, .-_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE8overflowEi
	.section	.text._ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE4syncEv,"axG",%progbits,_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE4syncEv,comdat
	.align	2
	.weak	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE4syncEv
	.hidden	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE4syncEv
	.type	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE4syncEv, %function
_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE4syncEv:
	.fnstart
.LFB5771:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #0
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE4syncEv, .-_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE4syncEv
	.section	.text._ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE,"axG",%progbits,_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE,comdat
	.align	2
	.weak	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE
	.hidden	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE
	.type	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE, %function
_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE:
	.fnstart
.LFB6113:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE, .-_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE
	.section	.text._ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE9showmanycEv,"axG",%progbits,_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE9showmanycEv,comdat
	.align	2
	.weak	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE9showmanycEv
	.hidden	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE9showmanycEv
	.type	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE9showmanycEv, %function
_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE9showmanycEv:
	.fnstart
.LFB6302:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #0
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE9showmanycEv, .-_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE9showmanycEv
	.section	.text._ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv,"axG",%progbits,_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv,comdat
	.align	2
	.weak	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv
	.hidden	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv
	.type	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv, %function
_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv:
	.fnstart
.LFB6304:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, [r0, #12]
	ldr	r3, [r0, #8]
	cmp	r3, r2
	mvneq	r0, #0
	ldrneb	r0, [r3, #0]	@ zero_extendqisi2
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv, .-_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv
	.section	.text._ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE5uflowEv,"axG",%progbits,_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE5uflowEv,comdat
	.align	2
	.weak	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE5uflowEv
	.hidden	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE5uflowEv
	.type	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE5uflowEv, %function
_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE5uflowEv:
	.fnstart
.LFB6305:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, [r0, #8]
	ldr	r1, [r0, #12]
	cmp	r2, r1
	mov	r3, r0
	ldrneb	r0, [r2], #1	@ zero_extendqisi2
	mvneq	r0, #0
	strne	r2, [r3, #8]
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE5uflowEv, .-_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE5uflowEv
	.section	.text._ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi,"axG",%progbits,_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi,comdat
	.align	2
	.weak	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi
	.hidden	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi
	.type	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi, %function
_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi:
	.fnstart
.LFB6306:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r2, [r0, #8]
	mov	r3, r0
	ldr	r0, [r0, #4]
	cmp	r2, r0
	beq	.L75
	cmn	r1, #1
	subeq	r2, r2, #1
	streq	r2, [r3, #8]
	moveq	r1, #0
	beq	.L78
	ldrb	ip, [r2, #-1]	@ zero_extendqisi2
	and	r0, r1, #255
	cmp	ip, r0
	sub	r0, r2, #1
	streq	r0, [r3, #8]
	bne	.L80
.L78:
	mov	r0, r1
	bx	lr
.L80:
	ldr	ip, [r3, #36]
	tst	ip, #16
	strne	r0, [r3, #8]
	strneb	r1, [r2, #-1]
	bne	.L78
.L75:
	mvn	r1, #0
	mov	r0, r1
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi, .-_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi
	.section	.text._ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE6setbufEPci,"axG",%progbits,_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE6setbufEPci,comdat
	.align	2
	.weak	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE6setbufEPci
	.hidden	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE6setbufEPci
	.type	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE6setbufEPci, %function
_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE6setbufEPci:
	.fnstart
.LFB6310:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE6setbufEPci, .-_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE6setbufEPci
	.section	.text._ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE7seekoffElii,"axG",%progbits,_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE7seekoffElii,comdat
	.align	2
	.weak	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE7seekoffElii
	.hidden	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE7seekoffElii
	.type	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE7seekoffElii, %function
_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE7seekoffElii:
	.fnstart
.LFB6311:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r1, #0
	mvn	r2, #0
	str	r1, [r0, #4]
	str	r2, [r0, #0]
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE7seekoffElii, .-_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE7seekoffElii
	.section	.text._ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE7seekposENS_4fposIiEEi,"axG",%progbits,_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE7seekposENS_4fposIiEEi,comdat
	.align	2
	.weak	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE7seekposENS_4fposIiEEi
	.hidden	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE7seekposENS_4fposIiEEi
	.type	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE7seekposENS_4fposIiEEi, %function
_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE7seekposENS_4fposIiEEi:
	.fnstart
.LFB6312:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	sub	sp, sp, #8
	mov	ip, sp
	stmia	ip, {r2, r3}
	mov	r2, #0
	mvn	r3, #0
	str	r2, [r0, #4]
	str	r3, [r0, #0]
	add	sp, sp, #8
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE7seekposENS_4fposIiEEi, .-_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE7seekposENS_4fposIiEEi
	.section	.text._ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE9underflowEv,"axG",%progbits,_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE9underflowEv,comdat
	.align	2
	.weak	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE9underflowEv
	.hidden	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE9underflowEv
	.type	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE9underflowEv, %function
_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE9underflowEv:
	.fnstart
.LFB6313:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mvn	r0, #0
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE9underflowEv, .-_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE9underflowEv
	.section	.text._ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE5uflowEv,"axG",%progbits,_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE5uflowEv,comdat
	.align	2
	.weak	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE5uflowEv
	.hidden	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE5uflowEv
	.type	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE5uflowEv, %function
_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE5uflowEv:
	.fnstart
.LFB6314:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	ldr	r3, [r0, #0]
	mov	r4, r0
	ldr	ip, [r3, #32]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	ldrne	r3, [r4, #8]
	addne	r2, r3, #1
	strne	r2, [r4, #8]
	ldrneb	r0, [r3, #0]	@ zero_extendqisi2
	ldmfd	sp!, {r4, lr}
	bx	lr
	.fnend
	.size	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE5uflowEv, .-_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE5uflowEv
	.section	.text._ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE9pbackfailEi,"axG",%progbits,_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE9pbackfailEi,comdat
	.align	2
	.weak	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE9pbackfailEi
	.hidden	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE9pbackfailEi
	.type	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE9pbackfailEi, %function
_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE9pbackfailEi:
	.fnstart
.LFB6315:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mvn	r0, #0
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE9pbackfailEi, .-_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE9pbackfailEi
	.section	.text._GLOBAL__I__Z13myDevicePausePvS_,"ax",%progbits
	.align	2
	.type	_GLOBAL__I__Z13myDevicePausePvS_, %function
_GLOBAL__I__Z13myDevicePausePvS_:
	.fnstart
.LFB6341:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	ldr	r4, .L99
	ldr	r5, .L99+4
	mov	r0, r4
	bl	_ZN4_STL8ios_base9_Loc_initC1Ev
	add	r6, r4, #4
	ldr	r1, .L99+8
	mov	r2, r5
	mov	r0, r4
	bl	__aeabi_atexit
	mov	r0, r6
	bl	_ZN4_STL8ios_base4InitC1Ev
	mov	r2, r5
	mov	r0, r6
	ldr	r1, .L99+12
	bl	__aeabi_atexit
	mov	r2, #0
	mov	r3, #1
	str	r3, [r4, #20]
	str	r2, [r4, #12]
	str	r2, [r4, #8]
	str	r3, [r4, #16]
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L100:
	.align	2
.L99:
	.word	.LANCHOR0
	.word	__dso_handle
	.word	_ZN4_STL8ios_base9_Loc_initD1Ev
	.word	_ZN4_STL8ios_base4InitD1Ev
	.fnend
	.size	_GLOBAL__I__Z13myDevicePausePvS_, .-_GLOBAL__I__Z13myDevicePausePvS_
	.section	.init_array,"aw",%init_array
	.align	2
	.word	_GLOBAL__I__Z13myDevicePausePvS_(target1)
	.section	.text._ZN13QuBaseManagerD0Ev,"axG",%progbits,_ZN13QuBaseManagerD0Ev,comdat
	.align	2
	.weak	_ZN13QuBaseManagerD0Ev
	.hidden	_ZN13QuBaseManagerD0Ev
	.type	_ZN13QuBaseManagerD0Ev, %function
_ZN13QuBaseManagerD0Ev:
	.fnstart
.LFB3202:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L103
	stmfd	sp!, {r4, lr}
	mov	r4, r0
	str	r3, [r0, #0]
	bl	_ZdlPv
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L104:
	.align	2
.L103:
	.word	_ZTV13QuBaseManager+8
	.cantunwind
	.fnend
	.size	_ZN13QuBaseManagerD0Ev, .-_ZN13QuBaseManagerD0Ev
	.section	.text._ZN7QuTimerD0Ev,"axG",%progbits,_ZN7QuTimerD0Ev,comdat
	.align	2
	.weak	_ZN7QuTimerD0Ev
	.hidden	_ZN7QuTimerD0Ev
	.type	_ZN7QuTimerD0Ev, %function
_ZN7QuTimerD0Ev:
	.fnstart
.LFB2685:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L107
	stmfd	sp!, {r4, lr}
	mov	r4, r0
	str	r3, [r0, #0]
	bl	_ZdlPv
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L108:
	.align	2
.L107:
	.word	_ZTV7QuTimer+8
	.cantunwind
	.fnend
	.size	_ZN7QuTimerD0Ev, .-_ZN7QuTimerD0Ev
	.section	.text._ZN13QuBasicCameraD0Ev,"axG",%progbits,_ZN13QuBasicCameraD0Ev,comdat
	.align	2
	.weak	_ZN13QuBasicCameraD0Ev
	.hidden	_ZN13QuBasicCameraD0Ev
	.type	_ZN13QuBasicCameraD0Ev, %function
_ZN13QuBasicCameraD0Ev:
	.fnstart
.LFB3712:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L111
	stmfd	sp!, {r4, lr}
	mov	r4, r0
	str	r3, [r0, #0]
	bl	_ZdlPv
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L112:
	.align	2
.L111:
	.word	_ZTV13QuBasicCamera+8
	.cantunwind
	.fnend
	.size	_ZN13QuBasicCameraD0Ev, .-_ZN13QuBasicCameraD0Ev
	.section	.text._ZN22QuSoundStructInterfaceD0Ev,"axG",%progbits,_ZN22QuSoundStructInterfaceD0Ev,comdat
	.align	2
	.weak	_ZN22QuSoundStructInterfaceD0Ev
	.hidden	_ZN22QuSoundStructInterfaceD0Ev
	.type	_ZN22QuSoundStructInterfaceD0Ev, %function
_ZN22QuSoundStructInterfaceD0Ev:
	.fnstart
.LFB2774:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r1, .L115
	ldr	r3, .L115+4
	stmfd	sp!, {r4, lr}
	mov	r4, r0
	stmia	r0, {r1, r3}	@ phole stm
	bl	_ZdlPv
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L116:
	.align	2
.L115:
	.word	_ZTV22QuSoundStructInterface+8
	.word	_ZTV7QuTimer+8
	.cantunwind
	.fnend
	.size	_ZN22QuSoundStructInterfaceD0Ev, .-_ZN22QuSoundStructInterfaceD0Ev
	.section	.text._ZN18QuApRawSoundStructD0Ev,"axG",%progbits,_ZN18QuApRawSoundStructD0Ev,comdat
	.align	2
	.weak	_ZN18QuApRawSoundStructD0Ev
	.hidden	_ZN18QuApRawSoundStructD0Ev
	.type	_ZN18QuApRawSoundStructD0Ev, %function
_ZN18QuApRawSoundStructD0Ev:
	.fnstart
.LFB2788:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	ldr	r1, .L119
	mov	r4, r0
	str	r1, [r0, #0]
	ldr	r0, [r0, #36]
	bl	_ZdlPv
	ldr	r3, .L119+4
	ldr	r0, .L119+8
	stmia	r4, {r0, r3}	@ phole stm
	mov	r0, r4
	bl	_ZdlPv
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L120:
	.align	2
.L119:
	.word	_ZTV18QuApRawSoundStruct+8
	.word	_ZTV7QuTimer+8
	.word	_ZTV22QuSoundStructInterface+8
	.cantunwind
	.fnend
	.size	_ZN18QuApRawSoundStructD0Ev, .-_ZN18QuApRawSoundStructD0Ev
	.section	.text._ZN18QuApRawSoundStructD1Ev,"axG",%progbits,_ZN18QuApRawSoundStructD1Ev,comdat
	.align	2
	.weak	_ZN18QuApRawSoundStructD1Ev
	.hidden	_ZN18QuApRawSoundStructD1Ev
	.type	_ZN18QuApRawSoundStructD1Ev, %function
_ZN18QuApRawSoundStructD1Ev:
	.fnstart
.LFB2787:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	ldr	r1, .L123
	mov	r4, r0
	str	r1, [r0, #0]
	ldr	r0, [r0, #36]
	bl	_ZdlPv
	ldr	r0, .L123+4
	ldr	r3, .L123+8
	stmia	r4, {r0, r3}	@ phole stm
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L124:
	.align	2
.L123:
	.word	_ZTV18QuApRawSoundStruct+8
	.word	_ZTV22QuSoundStructInterface+8
	.word	_ZTV7QuTimer+8
	.cantunwind
	.fnend
	.size	_ZN18QuApRawSoundStructD1Ev, .-_ZN18QuApRawSoundStructD1Ev
	.section	.text._ZN23QuSoundManagerInterfaceD0Ev,"axG",%progbits,_ZN23QuSoundManagerInterfaceD0Ev,comdat
	.align	2
	.weak	_ZN23QuSoundManagerInterfaceD0Ev
	.hidden	_ZN23QuSoundManagerInterfaceD0Ev
	.type	_ZN23QuSoundManagerInterfaceD0Ev, %function
_ZN23QuSoundManagerInterfaceD0Ev:
	.fnstart
.LFB2797:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L127
	stmfd	sp!, {r4, lr}
	mov	r4, r0
	str	r3, [r0, #0]
	bl	_ZdlPv
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L128:
	.align	2
.L127:
	.word	_ZTV23QuSoundManagerInterface+8
	.cantunwind
	.fnend
	.size	_ZN23QuSoundManagerInterfaceD0Ev, .-_ZN23QuSoundManagerInterfaceD0Ev
	.section	.text._ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev,"axG",%progbits,_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev,comdat
	.align	2
	.weak	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.hidden	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.type	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev, %function
_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev:
	.fnstart
.LFB4580:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	mov	r4, r0
	ldr	r0, [r0, #0]
	cmp	r0, #0
	blne	free
.L130:
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev, .-_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.section	.text._ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_,"axG",%progbits,_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_,comdat
	.align	2
	.weak	_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_
	.hidden	_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_
	.type	_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_, %function
_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_:
	.fnstart
.LFB5117:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	ldmia	r1, {r0, r5}	@ phole ldm
	ldmia	r2, {r1, r4}	@ phole ldm
	rsb	r5, r0, r5
	rsb	r4, r1, r4
	cmp	r4, r5
	movlt	r2, r4
	movge	r2, r5
	bl	memcmp
	cmp	r0, #0
	movne	r0, r0, lsr #31
	bne	.L134
	cmp	r5, r4
	movge	r0, #0
	movlt	r0, #1
.L134:
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
	.cantunwind
	.fnend
	.size	_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_, .-_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_
	.section	.text._ZN14QuArrayPointerIfE6deInitEv,"axG",%progbits,_ZN14QuArrayPointerIfE6deInitEv,comdat
	.align	2
	.weak	_ZN14QuArrayPointerIfE6deInitEv
	.hidden	_ZN14QuArrayPointerIfE6deInitEv
	.type	_ZN14QuArrayPointerIfE6deInitEv, %function
_ZN14QuArrayPointerIfE6deInitEv:
	.fnstart
.LFB4724:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	ldrb	r3, [r0, #0]	@ zero_extendqisi2
	cmp	r3, #0
	mov	r4, r0
	beq	.L139
	ldr	r0, [r0, #8]
	cmp	r0, #0
	blne	_ZdaPv
.L138:
	mov	r0, #0
	strb	r0, [r4, #0]
	str	r0, [r4, #8]
.L139:
	ldmfd	sp!, {r4, lr}
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN14QuArrayPointerIfE6deInitEv, .-_ZN14QuArrayPointerIfE6deInitEv
	.section	.text._ZN19QuApRawSoundManager6updateEv,"axG",%progbits,_ZN19QuApRawSoundManager6updateEv,comdat
	.align	2
	.weak	_ZN19QuApRawSoundManager6updateEv
	.hidden	_ZN19QuApRawSoundManager6updateEv
	.type	_ZN19QuApRawSoundManager6updateEv, %function
_ZN19QuApRawSoundManager6updateEv:
	.fnstart
.LFB2820:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	ldr	r3, [r0, #4]
	mov	r5, r0
	ldr	r4, [r3, #8]
	b	.L142
.L144:
	ldr	r1, [r4, #28]
	mov	r0, r1
	ldr	r3, [r1, #0]
	ldr	ip, [r3, #24]
	mov	lr, pc
	bx	ip
	mov	r0, r4
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r3, [r5, #4]
	mov	r4, r0
.L142:
	cmp	r3, r4
	bne	.L144
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
	.fnend
	.size	_ZN19QuApRawSoundManager6updateEv, .-_ZN19QuApRawSoundManager6updateEv
	.section	.text._ZN19QuApRawSoundManager16areSoundsPlayingEv,"axG",%progbits,_ZN19QuApRawSoundManager16areSoundsPlayingEv,comdat
	.align	2
	.weak	_ZN19QuApRawSoundManager16areSoundsPlayingEv
	.hidden	_ZN19QuApRawSoundManager16areSoundsPlayingEv
	.type	_ZN19QuApRawSoundManager16areSoundsPlayingEv, %function
_ZN19QuApRawSoundManager16areSoundsPlayingEv:
	.fnstart
.LFB2813:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	ldr	r3, [r0, #4]
	mov	r5, r0
	ldr	r4, [r3, #8]
	b	.L148
.L151:
	ldr	r1, [r4, #28]
	mov	r0, r1
	ldr	r3, [r1, #0]
	ldr	ip, [r3, #12]
	mov	lr, pc
	bx	ip
	cmp	r0, #0
	bne	.L153
	mov	r0, r4
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r3, [r5, #4]
	mov	r4, r0
.L148:
	cmp	r3, r4
	bne	.L151
	mov	r0, #0
.L150:
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L153:
	mov	r0, #1
	b	.L150
	.fnend
	.size	_ZN19QuApRawSoundManager16areSoundsPlayingEv, .-_ZN19QuApRawSoundManager16areSoundsPlayingEv
	.section	.text._ZN4_STL9basic_iosIcNS_11char_traitsIcEEED0Ev,"axG",%progbits,_ZN4_STL9basic_iosIcNS_11char_traitsIcEEED0Ev,comdat
	.align	2
	.weak	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEED0Ev
	.hidden	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEED0Ev
	.type	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEED0Ev, %function
_ZN4_STL9basic_iosIcNS_11char_traitsIcEEED0Ev:
	.fnstart
.LFB5372:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L156
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	str	r3, [r0, #0]
	bl	_ZN4_STL8ios_baseD2Ev
	mov	r0, r4
	bl	_ZdlPv
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L157:
	.align	2
.L156:
	.word	_ZTVN4_STL9basic_iosIcNS_11char_traitsIcEEEE+8
	.fnend
	.size	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEED0Ev, .-_ZN4_STL9basic_iosIcNS_11char_traitsIcEEED0Ev
	.section	.text._ZN4_STL9basic_iosIcNS_11char_traitsIcEEED1Ev,"axG",%progbits,_ZN4_STL9basic_iosIcNS_11char_traitsIcEEED1Ev,comdat
	.align	2
	.weak	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEED1Ev
	.hidden	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEED1Ev
	.type	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEED1Ev, %function
_ZN4_STL9basic_iosIcNS_11char_traitsIcEEED1Ev:
	.fnstart
.LFB5371:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L160
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	str	r3, [r0, #0]
	bl	_ZN4_STL8ios_baseD2Ev
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L161:
	.align	2
.L160:
	.word	_ZTVN4_STL9basic_iosIcNS_11char_traitsIcEEEE+8
	.fnend
	.size	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEED1Ev, .-_ZN4_STL9basic_iosIcNS_11char_traitsIcEEED1Ev
	.section	.text._ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE10_M_xsputncEci,"axG",%progbits,_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE10_M_xsputncEci,comdat
	.align	2
	.weak	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE10_M_xsputncEci
	.hidden	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE10_M_xsputncEci
	.type	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE10_M_xsputncEci, %function
_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE10_M_xsputncEci:
	.fnstart
.LFB5767:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	.save {r4, r5, r6, r7, r8, lr}
	subs	r7, r2, #0
	mov	r4, r0
	mov	r8, r1
	mov	r5, #0
	bgt	.L167
	b	.L164
.L170:
	rsb	r1, r5, r7
	rsb	r6, r0, r6
	cmp	r1, r6
	movcc	r6, r1
	mov	r2, r6
	mov	r1, r8
	bl	memset
	ldr	r0, [r4, #20]
	add	r5, r6, r5
	add	r3, r0, r6
	cmp	r7, r5
	str	r3, [r4, #20]
	ble	.L164
.L167:
	add	r0, r4, #20
	ldmia	r0, {r0, r6}	@ phole ldm
	cmp	r0, r6
	bcc	.L170
	ldr	r2, [r4, #0]
	mov	r0, r4
	mov	r1, r8
	ldr	ip, [r2, #52]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	beq	.L164
	add	r5, r5, #1
	cmp	r7, r5
	bgt	.L167
.L164:
	mov	r0, r5
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
	.fnend
	.size	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE10_M_xsputncEci, .-_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE10_M_xsputncEci
	.section	.text._ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE6xsgetnEPci,"axG",%progbits,_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE6xsgetnEPci,comdat
	.align	2
	.weak	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE6xsgetnEPci
	.hidden	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE6xsgetnEPci
	.type	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE6xsgetnEPci, %function
_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE6xsgetnEPci:
	.fnstart
.LFB6303:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	.save {r4, r5, r6, r7, r8, lr}
	subs	r8, r2, #0
	mov	r5, r0
	mov	r7, r1
	mov	r4, #0
	bgt	.L177
	b	.L173
.L174:
	ldr	r2, [r5, #0]
	ldr	ip, [r2, #36]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	beq	.L173
	add	r4, r4, #1
	cmp	r8, r4
	strb	r0, [r7], #1
	ble	.L173
.L177:
	add	r1, r5, #8
	ldmia	r1, {r1, r3}	@ phole ldm
	cmp	r1, r3
	rsb	r2, r1, r3
	rsb	r6, r4, r8
	mov	r0, r5
	bcs	.L174
	cmp	r6, r2
	movcs	r6, r2
	cmp	r6, #0
	bne	.L180
.L175:
	add	r4, r6, r4
	add	r1, r1, r6
	cmp	r8, r4
	str	r1, [r5, #8]
	add	r7, r7, r6
	bgt	.L177
.L173:
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L180:
	mov	r0, r7
	mov	r2, r6
	bl	memcpy
	ldr	r1, [r5, #8]
	b	.L175
	.fnend
	.size	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE6xsgetnEPci, .-_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE6xsgetnEPci
	.section	.text._ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE6xsputnEPKci,"axG",%progbits,_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE6xsputnEPKci,comdat
	.align	2
	.weak	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE6xsputnEPKci
	.hidden	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE6xsputnEPKci
	.type	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE6xsputnEPKci, %function
_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE6xsputnEPKci:
	.fnstart
.LFB5449:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	.save {r4, r5, r6, r7, r8, lr}
	subs	r8, r2, #0
	mov	r6, r0
	mov	r7, r1
	mov	r4, #0
	bgt	.L187
	b	.L183
.L191:
	cmp	r5, r2
	movcs	r5, r2
	cmp	r5, #0
	bne	.L190
.L185:
	add	r4, r5, r4
	add	r0, r0, r5
	cmp	r8, r4
	str	r0, [r6, #20]
	add	r7, r7, r5
	ble	.L183
.L187:
	add	r0, r6, #20
	ldmia	r0, {r0, r3}	@ phole ldm
	cmp	r0, r3
	rsb	r2, r0, r3
	rsb	r5, r4, r8
	bcc	.L191
	ldrb	r1, [r7, #0]	@ zero_extendqisi2
	ldr	r2, [r6, #0]
	mov	r0, r6
	ldr	ip, [r2, #52]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	beq	.L183
	add	r4, r4, #1
	cmp	r8, r4
	add	r7, r7, #1
	bgt	.L187
.L183:
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L190:
	mov	r1, r7
	mov	r2, r5
	bl	memcpy
	ldr	r0, [r6, #20]
	b	.L185
	.fnend
	.size	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE6xsputnEPKci, .-_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE6xsputnEPKci
	.section	.text._ZN4_STL15basic_streambufIcNS_11char_traitsIcEEED0Ev,"axG",%progbits,_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEED0Ev,comdat
	.align	2
	.weak	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEED0Ev
	.hidden	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEED0Ev
	.type	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEED0Ev, %function
_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEED0Ev:
	.fnstart
.LFB5701:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L195
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	str	r3, [r0], #28
	bl	_ZN4_STL6localeD1Ev
	mov	r0, r4
	bl	_ZdlPv
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L196:
	.align	2
.L195:
	.word	_ZTVN4_STL15basic_streambufIcNS_11char_traitsIcEEEE+8
	.fnend
	.size	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEED0Ev, .-_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEED0Ev
	.section	.text._ZN4_STL15basic_streambufIcNS_11char_traitsIcEEED1Ev,"axG",%progbits,_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEED1Ev,comdat
	.align	2
	.weak	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEED1Ev
	.hidden	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEED1Ev
	.type	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEED1Ev, %function
_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEED1Ev:
	.fnstart
.LFB5700:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L200
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	str	r3, [r0], #28
	bl	_ZN4_STL6localeD1Ev
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L201:
	.align	2
.L200:
	.word	_ZTVN4_STL15basic_streambufIcNS_11char_traitsIcEEEE+8
	.fnend
	.size	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEED1Ev, .-_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEED1Ev
	.section	.text._ZN4_STL9basic_iosIcNS_11char_traitsIcEEE8setstateEi,"axG",%progbits,_ZN4_STL9basic_iosIcNS_11char_traitsIcEEE8setstateEi,comdat
	.align	2
	.weak	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEE8setstateEi
	.hidden	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEE8setstateEi
	.type	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEE8setstateEi, %function
_ZN4_STL9basic_iosIcNS_11char_traitsIcEEE8setstateEi:
	.fnstart
.LFB5090:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	ip, [r0, #8]
	stmfd	sp!, {r3, lr}
	.save {r3, lr}
	ldr	r3, [r0, #88]
	ldr	r2, [r0, #20]
	cmp	r3, #0
	orr	r3, r1, ip
	orreq	r3, r3, #1
	tst	r3, r2
	str	r3, [r0, #8]
	blne	_ZN4_STL8ios_base16_M_throw_failureEv
.L205:
	ldmfd	sp!, {r3, lr}
	bx	lr
	.fnend
	.size	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEE8setstateEi, .-_ZN4_STL9basic_iosIcNS_11char_traitsIcEEE8setstateEi
	.section	.text._ZN12QuDrawObject7disableEv,"axG",%progbits,_ZN12QuDrawObject7disableEv,comdat
	.align	2
	.weak	_ZN12QuDrawObject7disableEv
	.hidden	_ZN12QuDrawObject7disableEv
	.type	_ZN12QuDrawObject7disableEv, %function
_ZN12QuDrawObject7disableEv:
	.fnstart
.LFB2973:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	ldrb	r3, [r0, #120]	@ zero_extendqisi2
	cmp	r3, #0
	mov	r4, r0
	bne	.L213
.L207:
	ldrb	r0, [r4, #8]	@ zero_extendqisi2
	cmp	r0, #0
	bne	.L214
.L208:
	ldrb	r1, [r4, #36]	@ zero_extendqisi2
	cmp	r1, #0
	bne	.L215
.L209:
	ldrb	r2, [r4, #64]	@ zero_extendqisi2
	cmp	r2, #0
	bne	.L216
.L210:
	ldrb	ip, [r4, #92]	@ zero_extendqisi2
	cmp	ip, #0
	ldrne	r0, [r4, #112]
	blne	glDisableClientState
.L212:
	ldmfd	sp!, {r4, lr}
	bx	lr
.L216:
	ldr	r0, [r4, #84]
	bl	glDisableClientState
	b	.L210
.L215:
	ldr	r0, [r4, #56]
	bl	glDisableClientState
	b	.L209
.L214:
	ldr	r0, [r4, #28]
	bl	glDisableClientState
	b	.L208
.L213:
	ldr	r0, [r0, #124]
	bl	_ZN11QuBaseImage6unbindEv
	b	.L207
	.fnend
	.size	_ZN12QuDrawObject7disableEv, .-_ZN12QuDrawObject7disableEv
	.global	__aeabi_f2d
	.global	__aeabi_d2f
	.global	__aeabi_fcmpeq
	.section	.text._ZN13QuBasicCamera18setModelViewMatrixEv,"axG",%progbits,_ZN13QuBasicCamera18setModelViewMatrixEv,comdat
	.align	2
	.weak	_ZN13QuBasicCamera18setModelViewMatrixEv
	.hidden	_ZN13QuBasicCamera18setModelViewMatrixEv
	.type	_ZN13QuBasicCamera18setModelViewMatrixEv, %function
_ZN13QuBasicCamera18setModelViewMatrixEv:
	.fnstart
.LFB3719:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 88
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.pad #92
	sub	sp, sp, #92
	str	r0, [sp, #20]
	mov	r0, #5888
	bl	glMatrixMode
	bl	glLoadIdentity
	ldr	r1, [sp, #20]
	ldr	r1, [r1, #12]	@ float
	mov	r0, r1
	str	r1, [sp, #12]	@ float
	bl	__aeabi_fmul
	mov	r1, #0
	bl	__aeabi_fadd
	bl	__aeabi_f2d
	bl	sqrt
	bl	__aeabi_d2f
	mov	r1, #0
	mov	r4, r0
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	beq	.L218
	mov	r2, #0
	str	r2, [sp, #16]	@ float
	ldr	r8, [sp, #12]	@ float
	mov	r9, #-2147483648
	mov	sl, r2
.L219:
	mov	r1, #0
	mov	r0, sl
	bl	__aeabi_fmul
	mov	r5, r0
	mov	r1, r5
	mov	r0, r8
	bl	__aeabi_fsub
	mov	r1, #-2147483648
	mov	r7, r0
	mov	r0, r8
	bl	__aeabi_fmul
	mov	r1, r0
	mov	r0, r5
	bl	__aeabi_fadd
	ldr	r1, [sp, #16]	@ float
	mov	r6, r0
	mov	r0, r5
	bl	__aeabi_fsub
	mov	r5, r0
	mov	r1, r5
	mov	r0, sl
	bl	__aeabi_fmul
	mov	r1, r6
	mov	fp, r0
	mov	r0, r8
	bl	__aeabi_fmul
	mov	r1, r0
	mov	r0, fp
	bl	__aeabi_fsub
	mov	r1, r9
	mov	fp, r0
	mov	r0, r5
	bl	__aeabi_fmul
	mov	r1, r7
	mov	r9, r0
	mov	r0, r8
	bl	__aeabi_fmul
	mov	r1, r0
	mov	r0, r9
	bl	__aeabi_fadd
	mov	r1, r6
	mov	r9, r0
	ldr	r0, [sp, #16]	@ float
	bl	__aeabi_fmul
	mov	r1, r7
	mov	r4, r0
	mov	r0, sl
	bl	__aeabi_fmul
	mov	r1, r0
	mov	r0, r4
	bl	__aeabi_fsub
	mov	r1, r7
	str	r0, [sp, #8]	@ float
	mov	r0, r7
	bl	__aeabi_fmul
	mov	r1, r6
	mov	r4, r0
	mov	r0, r6
	bl	__aeabi_fmul
	mov	r1, r0
	mov	r0, r4
	bl	__aeabi_fadd
	mov	r1, r5
	mov	r4, r0
	mov	r0, r5
	bl	__aeabi_fmul
	mov	r1, r0
	mov	r0, r4
	bl	__aeabi_fadd
	bl	__aeabi_f2d
	bl	sqrt
	bl	__aeabi_d2f
	mov	r1, #0
	mov	r4, r0
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	bne	.L220
	mov	r0, r7
	mov	r1, r4
	bl	__aeabi_fdiv
	mov	r1, r4
	mov	r7, r0
	mov	r0, r6
	bl	__aeabi_fdiv
	mov	r1, r4
	mov	r6, r0
	mov	r0, r5
	bl	__aeabi_fdiv
	mov	r5, r0
.L220:
	mov	r1, fp
	mov	r0, fp
	bl	__aeabi_fmul
	mov	r1, r9
	mov	r4, r0
	mov	r0, r9
	bl	__aeabi_fmul
	mov	r1, r0
	mov	r0, r4
	bl	__aeabi_fadd
	mov	r4, r0
	ldr	r0, [sp, #8]	@ float
	mov	r1, r0
	bl	__aeabi_fmul
	mov	r1, r0
	mov	r0, r4
	bl	__aeabi_fadd
	bl	__aeabi_f2d
	bl	sqrt
	bl	__aeabi_d2f
	mov	r1, #0
	mov	r4, r0
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	bne	.L222
	mov	r0, fp
	mov	r1, r4
	bl	__aeabi_fdiv
	mov	r1, r4
	mov	fp, r0
	mov	r0, r9
	bl	__aeabi_fdiv
	mov	r1, r4
	mov	r9, r0
	ldr	r0, [sp, #8]	@ float
	bl	__aeabi_fdiv
	str	r0, [sp, #8]	@ float
.L222:
	ldr	r2, [sp, #8]	@ float
	ldr	r1, [sp, #16]	@ float
	mov	r4, #0
	mov	lr, #1065353216
	add	r0, sp, #24
	str	lr, [sp, #84]	@ float
	str	lr, [sp, #4]
	str	r2, [sp, #60]	@ float
	str	r1, [sp, #32]	@ float
	str	r7, [sp, #24]	@ float
	str	r6, [sp, #40]	@ float
	str	r5, [sp, #56]	@ float
	str	fp, [sp, #28]	@ float
	str	r9, [sp, #44]	@ float
	str	sl, [sp, #48]	@ float
	str	r8, [sp, #64]	@ float
	str	r4, [sp, #72]	@ float
	str	r4, [sp, #76]	@ float
	str	r4, [sp, #80]	@ float
	str	r4, [sp, #36]	@ float
	str	r4, [sp, #52]	@ float
	str	r4, [sp, #68]	@ float
	bl	glMultMatrixf
	ldr	ip, [sp, #12]
	mov	r0, #-2147483648
	add	r2, ip, #-2147483648
	mov	r1, r0
	bl	glTranslatef
	ldr	r3, [sp, #20]
	ldr	r0, [r3, #32]
	bl	__aeabi_i2f
	mov	r1, r4
	mov	r2, r4
	ldr	r3, [sp, #4]
	bl	glRotatef
	add	sp, sp, #92
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L218:
	mov	r1, r4
	mov	r0, #0
	bl	__aeabi_fdiv
	mov	r1, r4
	mov	sl, r0
	ldr	r0, [sp, #12]	@ float
	bl	__aeabi_fdiv
	add	r9, sl, #-2147483648
	mov	r8, r0
	str	sl, [sp, #16]	@ float
	b	.L219
	.fnend
	.size	_ZN13QuBasicCamera18setModelViewMatrixEv, .-_ZN13QuBasicCamera18setModelViewMatrixEv
	.global	__aeabi_dadd
	.global	__aeabi_dmul
	.global	__aeabi_ddiv
	.section	.text._ZN13QuBasicCamera19setProjectionMatrixEv,"axG",%progbits,_ZN13QuBasicCamera19setProjectionMatrixEv,comdat
	.align	2
	.weak	_ZN13QuBasicCamera19setProjectionMatrixEv
	.hidden	_ZN13QuBasicCamera19setProjectionMatrixEv
	.type	_ZN13QuBasicCamera19setProjectionMatrixEv, %function
_ZN13QuBasicCamera19setProjectionMatrixEv:
	.fnstart
.LFB3718:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	r5, [r0, #12]	@ float
	.pad #20
	sub	sp, sp, #20
	mov	r9, r0
	mov	r1, #1056964608
	ldr	r0, [r0, #8]	@ float
	bl	__aeabi_fmul
	mov	r1, r5
	bl	__aeabi_fdiv
	bl	__aeabi_f2d
	bl	atan
	mov	r2, #1107296256
	mov	r6, r0
	mov	r7, r1
	mov	r0, r5
	add	r1, r2, #13107200
	bl	__aeabi_fdiv
	mov	sl, #5888
	add	r4, sl, #1
	mov	r8, r0
	mov	r0, r4
	bl	glMatrixMode
	bl	glLoadIdentity
	ldr	r0, [r9, #16]
	bl	__aeabi_i2f
	mov	fp, r0
	ldr	r0, [r9, #20]
	bl	__aeabi_i2f
	mov	r1, r0
	mov	r0, fp
	bl	__aeabi_fdiv
	bl	__aeabi_f2d
	str	r0, [sp, #8]
	str	r1, [sp, #12]
	mov	r0, r4
	bl	glMatrixMode
	bl	glLoadIdentity
	mov	r2, r6
	mov	r3, r7
	mov	r0, r6
	mov	r1, r7
	bl	__aeabi_dadd
	mov	r9, #1124073472
	bl	__aeabi_d2f
	mov	r7, #4784128
	add	r1, r9, #3407872
	bl	__aeabi_fmul
	add	r1, r7, #4048
	add	r1, r1, #1073741835
	bl	__aeabi_fdiv
	mov	r4, #598016
	bl	__aeabi_f2d
	add	ip, r4, #504
	add	r3, ip, #1073741827
	mov	r2, #1610612736
	mov	fp, #1073741824
	bl	__aeabi_dmul
	add	r3, fp, #7733248
	mov	r2, #0
	add	r3, r3, #32768
	bl	__aeabi_ddiv
	bl	tan
	mov	r6, r0
	mov	r9, r1
	mov	r0, r8
	bl	__aeabi_f2d
	mov	r2, r6
	mov	r3, r9
	bl	__aeabi_dmul
	add	r7, r1, #-2147483648
	mov	r4, r0
	mov	r6, r1
	mov	r2, r0
	mov	r3, r7
	add	r1, sp, #8
	ldmia	r1, {r0-r1}
	bl	__aeabi_dmul
	bl	__aeabi_d2f
	mov	r2, r4
	mov	r3, r6
	mov	fp, r0
	add	r1, sp, #8
	ldmia	r1, {r0-r1}
	bl	__aeabi_dmul
	bl	__aeabi_d2f
	mov	r1, r7
	mov	r9, r0
	mov	r0, r4
	bl	__aeabi_d2f
	mov	r1, r6
	mov	r7, r0
	mov	r0, r4
	bl	__aeabi_d2f
	mov	ip, #1107296256
	add	r1, ip, #13107200
	mov	r4, r0
	mov	r0, r5
	str	r8, [sp, #0]	@ float
	bl	__aeabi_fmul
	mov	r1, r9
	mov	r2, r7
	mov	r3, r4
	str	r0, [sp, #4]	@ float
	mov	r0, fp
	bl	glFrustumf
	mov	r0, sl
	bl	glMatrixMode
	mov	r1, #4352
	add	r1, r1, #2
	mov	r0, #3152
	bl	glHint
	mov	r0, #1
	bl	glDepthMask
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
	.fnend
	.size	_ZN13QuBasicCamera19setProjectionMatrixEv, .-_ZN13QuBasicCamera19setProjectionMatrixEv
	.global	__aeabi_f2iz
	.section	.text._ZN18QuApRawSoundStruct9setVolumeEf,"axG",%progbits,_ZN18QuApRawSoundStruct9setVolumeEf,comdat
	.align	2
	.weak	_ZN18QuApRawSoundStruct9setVolumeEf
	.hidden	_ZN18QuApRawSoundStruct9setVolumeEf
	.type	_ZN18QuApRawSoundStruct9setVolumeEf, %function
_ZN18QuApRawSoundStruct9setVolumeEf:
	.fnstart
.LFB2793:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	.save {r4, r5, r6, r7, r8, lr}
	ldr	r5, [r0, #32]
	cmn	r5, #1
	mov	r4, r0
	beq	.L233
	ldr	r6, [r4, #16]
	ldr	r0, [r0, #12]
	cmp	r0, r6
	movge	r6, #1065353216
	blt	.L234
.L232:
	mov	r1, r6
	mov	r0, #1065353216
	bl	__aeabi_fsub
	ldr	r1, [r4, #24]	@ float
	bl	__aeabi_fmul
	ldr	r1, [r4, #20]	@ float
	mov	r7, r0
	mov	r0, r6
	bl	__aeabi_fmul
	mov	r1, r0
	mov	r0, r7
	bl	__aeabi_fadd
	mov	r1, #1124073472
	add	r1, r1, #8388608
	bl	__aeabi_fmul
	bl	__aeabi_f2iz
	mov	r1, #3
	mov	r2, r0
	mov	r0, r5
	bl	s3eSoundChannelSetInt
.L233:
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L234:
	ldr	r7, [r4, #8]
	rsb	r0, r7, r0
	bl	__aeabi_i2f
	mov	r8, r0
	rsb	r0, r7, r6
	bl	__aeabi_i2f
	mov	r1, r0
	mov	r0, r8
	bl	__aeabi_fdiv
	mov	r6, r0
	b	.L232
	.fnend
	.size	_ZN18QuApRawSoundStruct9setVolumeEf, .-_ZN18QuApRawSoundStruct9setVolumeEf
	.section	.text._ZN18QuApRawSoundStruct4stopEv,"axG",%progbits,_ZN18QuApRawSoundStruct4stopEv,comdat
	.align	2
	.weak	_ZN18QuApRawSoundStruct4stopEv
	.hidden	_ZN18QuApRawSoundStruct4stopEv
	.type	_ZN18QuApRawSoundStruct4stopEv, %function
_ZN18QuApRawSoundStruct4stopEv:
	.fnstart
.LFB2791:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, lr}
	.save {r3, lr}
	ldr	r0, [r0, #32]
	cmn	r0, #1
	blne	s3eSoundChannelStop
.L237:
	ldmfd	sp!, {r3, lr}
	bx	lr
	.fnend
	.size	_ZN18QuApRawSoundStruct4stopEv, .-_ZN18QuApRawSoundStruct4stopEv
	.section	.text._ZN18QuApRawSoundStruct9isPlayingEv,"axG",%progbits,_ZN18QuApRawSoundStruct9isPlayingEv,comdat
	.align	2
	.weak	_ZN18QuApRawSoundStruct9isPlayingEv
	.hidden	_ZN18QuApRawSoundStruct9isPlayingEv
	.type	_ZN18QuApRawSoundStruct9isPlayingEv, %function
_ZN18QuApRawSoundStruct9isPlayingEv:
	.fnstart
.LFB2790:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, lr}
	.save {r3, lr}
	ldr	r0, [r0, #32]
	cmn	r0, #1
	moveq	r0, #0
	beq	.L240
	mov	r1, #4
	bl	s3eSoundChannelGetInt
	cmp	r0, #1
	movne	r0, #0
	moveq	r0, #1
.L240:
	ldmfd	sp!, {r3, lr}
	bx	lr
	.fnend
	.size	_ZN18QuApRawSoundStruct9isPlayingEv, .-_ZN18QuApRawSoundStruct9isPlayingEv
	.section	.text._ZN18QuApRawSoundStruct4playEv,"axG",%progbits,_ZN18QuApRawSoundStruct4playEv,comdat
	.align	2
	.weak	_ZN18QuApRawSoundStruct4playEv
	.hidden	_ZN18QuApRawSoundStruct4playEv
	.type	_ZN18QuApRawSoundStruct4playEv, %function
_ZN18QuApRawSoundStruct4playEv:
	.fnstart
.LFB2789:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
	.save {r4, r5, lr}
	.pad #12
	sub	sp, sp, #12
	mov	r4, r0
	bl	s3eSoundGetFreeChannel
	cmn	r0, #1
	str	r0, [r4, #32]
	beq	.L244
	ldrb	r5, [r4, #28]	@ zero_extendqisi2
	mov	r1, #1
	ldr	r2, [r4, #44]
	eor	r5, r5, r1
	bl	s3eSoundChannelSetInt
	ldr	r3, [r4, #40]
	add	r2, r3, r3, lsr #31
	add	r0, r4, #32
	ldmia	r0, {r0, r1}	@ phole ldm
	mov	ip, #0
	mov	r2, r2, asr #1
	mov	r3, r5
	str	ip, [sp, #0]
	bl	s3eSoundChannelPlay
.L244:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, lr}
	bx	lr
	.fnend
	.size	_ZN18QuApRawSoundStruct4playEv, .-_ZN18QuApRawSoundStruct4playEv
	.section	.text._Z8mainTermv,"ax",%progbits
	.align	2
	.global	_Z8mainTermv
	.hidden	_Z8mainTermv
	.type	_Z8mainTermv, %function
_Z8mainTermv:
	.fnstart
.LFB4572:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, lr}
	.save {r3, lr}
	ldr	r1, .L247
	mov	r0, #1
	bl	s3eSurfaceUnRegister
	ldr	r1, .L247+4
	mov	r0, #0
	bl	s3eKeyboardUnRegister
	ldr	r1, .L247+8
	mov	r0, #0
	bl	s3ePointerUnRegister
	ldr	r1, .L247+12
	mov	r0, #1
	bl	s3ePointerUnRegister
	bl	s3eAccelerometerStop
	bl	_Z21IwResManagerTerminatev
	bl	_Z16IwSoundTerminatev
	bl	_Z13IwGxTerminatev
	ldmfd	sp!, {r3, lr}
	bx	lr
.L248:
	.align	2
.L247:
	.word	_Z6resizePvS_
	.word	_Z10keyboardcbP16s3eKeyboardEventPv
	.word	_Z11singleTouchP15s3ePointerEvent
	.word	_Z12singleMotionP21s3ePointerMotionEvent
	.fnend
	.size	_Z8mainTermv, .-_Z8mainTermv
	.section	.text._Z22convertToGlCoordinatesii,"ax",%progbits
	.align	2
	.global	_Z22convertToGlCoordinatesii
	.hidden	_Z22convertToGlCoordinatesii
	.type	_Z22convertToGlCoordinatesii, %function
_Z22convertToGlCoordinatesii:
	.fnstart
.LFB4564:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	.pad #8
	sub	sp, sp, #8
	str	r1, [sp, #0]
	mov	r4, r0
	mov	r1, sp
	str	r2, [sp, #4]
	bl	_Z13IwGLTransformRK10CIwGLPoint
	mov	r0, r4
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	bx	lr
	.fnend
	.size	_Z22convertToGlCoordinatesii, .-_Z22convertToGlCoordinatesii
	.section	.text._ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEED0Ev,"axG",%progbits,_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEED0Ev,comdat
	.align	2
	.weak	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEED0Ev
	.hidden	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEED0Ev
	.type	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEED0Ev, %function
_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEED0Ev:
	.fnstart
.LFB5694:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r1, .L254
	ldr	r3, .L254+4
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	str	r3, [r4, #0]
	str	r1, [r0, #4]!
	bl	_ZN4_STL8ios_baseD2Ev
	mov	r0, r4
	bl	_ZdlPv
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L255:
	.align	2
.L254:
	.word	_ZTVN4_STL9basic_iosIcNS_11char_traitsIcEEEE+8
	.word	_ZTVN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE+12
	.fnend
	.size	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEED0Ev, .-_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEED0Ev
	.section	.text._ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEED1Ev,"axG",%progbits,_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEED1Ev,comdat
	.align	2
	.weak	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEED1Ev
	.hidden	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEED1Ev
	.type	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEED1Ev, %function
_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEED1Ev:
	.fnstart
.LFB5693:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r1, .L258
	ldr	r3, .L258+4
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	str	r3, [r4, #0]
	str	r1, [r0, #4]!
	bl	_ZN4_STL8ios_baseD2Ev
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L259:
	.align	2
.L258:
	.word	_ZTVN4_STL9basic_iosIcNS_11char_traitsIcEEEE+8
	.word	_ZTVN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE+12
	.fnend
	.size	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEED1Ev, .-_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEED1Ev
	.section	.text._ZN4_STL13basic_istreamIcNS_11char_traitsIcEEED0Ev,"axG",%progbits,_ZN4_STL13basic_istreamIcNS_11char_traitsIcEEED0Ev,comdat
	.align	2
	.weak	_ZN4_STL13basic_istreamIcNS_11char_traitsIcEEED0Ev
	.hidden	_ZN4_STL13basic_istreamIcNS_11char_traitsIcEEED0Ev
	.type	_ZN4_STL13basic_istreamIcNS_11char_traitsIcEEED0Ev, %function
_ZN4_STL13basic_istreamIcNS_11char_traitsIcEEED0Ev:
	.fnstart
.LFB5687:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r1, .L262
	ldr	r3, .L262+4
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	str	r3, [r4, #0]
	str	r1, [r0, #8]!
	bl	_ZN4_STL8ios_baseD2Ev
	mov	r0, r4
	bl	_ZdlPv
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L263:
	.align	2
.L262:
	.word	_ZTVN4_STL9basic_iosIcNS_11char_traitsIcEEEE+8
	.word	_ZTVN4_STL13basic_istreamIcNS_11char_traitsIcEEEE+12
	.fnend
	.size	_ZN4_STL13basic_istreamIcNS_11char_traitsIcEEED0Ev, .-_ZN4_STL13basic_istreamIcNS_11char_traitsIcEEED0Ev
	.section	.text._ZN4_STL13basic_istreamIcNS_11char_traitsIcEEED1Ev,"axG",%progbits,_ZN4_STL13basic_istreamIcNS_11char_traitsIcEEED1Ev,comdat
	.align	2
	.weak	_ZN4_STL13basic_istreamIcNS_11char_traitsIcEEED1Ev
	.hidden	_ZN4_STL13basic_istreamIcNS_11char_traitsIcEEED1Ev
	.type	_ZN4_STL13basic_istreamIcNS_11char_traitsIcEEED1Ev, %function
_ZN4_STL13basic_istreamIcNS_11char_traitsIcEEED1Ev:
	.fnstart
.LFB5686:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r1, .L266
	ldr	r3, .L266+4
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	str	r3, [r4, #0]
	str	r1, [r0, #8]!
	bl	_ZN4_STL8ios_baseD2Ev
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L267:
	.align	2
.L266:
	.word	_ZTVN4_STL9basic_iosIcNS_11char_traitsIcEEEE+8
	.word	_ZTVN4_STL13basic_istreamIcNS_11char_traitsIcEEEE+12
	.fnend
	.size	_ZN4_STL13basic_istreamIcNS_11char_traitsIcEEED1Ev, .-_ZN4_STL13basic_istreamIcNS_11char_traitsIcEEED1Ev
	.section	.text._ZN4_STL14basic_iostreamIcNS_11char_traitsIcEEED0Ev,"axG",%progbits,_ZN4_STL14basic_iostreamIcNS_11char_traitsIcEEED0Ev,comdat
	.align	2
	.weak	_ZThn8_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED0Ev
	.hidden	_ZThn8_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED0Ev
	.type	_ZThn8_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED0Ev, %function
_ZThn8_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED0Ev:
	.fnstart
	sub	r0, r0, #8
	b	.LTHUNK5
	.fnend
	.size	_ZThn8_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED0Ev, .-_ZThn8_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED0Ev
	.align	2
	.weak	_ZN4_STL14basic_iostreamIcNS_11char_traitsIcEEED0Ev
	.hidden	_ZN4_STL14basic_iostreamIcNS_11char_traitsIcEEED0Ev
	.type	_ZN4_STL14basic_iostreamIcNS_11char_traitsIcEEED0Ev, %function
_ZN4_STL14basic_iostreamIcNS_11char_traitsIcEEED0Ev:
	.fnstart
.LFB5379:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	ldr	lr, .L270
	ldr	r2, .L270+4
	ldr	ip, [lr, #12]
	mov	r4, r0
	add	r0, r2, #40
	str	r0, [r4, #12]
	str	r2, [r4, #0]
	str	ip, [r4, #8]
	ldr	r1, [ip, #-12]
	ldr	r2, [lr, #4]
	ldr	r0, [lr, #16]
	add	r3, r4, r1
	str	r0, [r3, #8]
	str	r2, [r4, #0]
	ldr	r1, [lr, #8]
	ldr	ip, [r2, #-12]
	ldr	r3, .L270+8
	mov	r0, r4
	str	r1, [r4, ip]
	str	r3, [r0, #12]!
	bl	_ZN4_STL8ios_baseD2Ev
	mov	r0, r4
	bl	_ZdlPv
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L271:
	.align	2
.L270:
	.word	_ZTTN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE
	.word	_ZTVN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE+12
	.word	_ZTVN4_STL9basic_iosIcNS_11char_traitsIcEEEE+8
	.fnend
	.size	_ZN4_STL14basic_iostreamIcNS_11char_traitsIcEEED0Ev, .-_ZN4_STL14basic_iostreamIcNS_11char_traitsIcEEED0Ev
	.section	.text._ZN4_STL14basic_iostreamIcNS_11char_traitsIcEEED1Ev,"axG",%progbits,_ZN4_STL14basic_iostreamIcNS_11char_traitsIcEEED1Ev,comdat
	.align	2
	.weak	_ZThn8_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED1Ev
	.hidden	_ZThn8_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED1Ev
	.type	_ZThn8_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED1Ev, %function
_ZThn8_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED1Ev:
	.fnstart
	sub	r0, r0, #8
	b	.LTHUNK7
	.fnend
	.size	_ZThn8_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED1Ev, .-_ZThn8_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED1Ev
	.align	2
	.weak	_ZN4_STL14basic_iostreamIcNS_11char_traitsIcEEED1Ev
	.hidden	_ZN4_STL14basic_iostreamIcNS_11char_traitsIcEEED1Ev
	.type	_ZN4_STL14basic_iostreamIcNS_11char_traitsIcEEED1Ev, %function
_ZN4_STL14basic_iostreamIcNS_11char_traitsIcEEED1Ev:
	.fnstart
.LFB5378:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	ldr	lr, .L274
	ldr	r2, .L274+4
	ldr	ip, [lr, #12]
	mov	r4, r0
	add	r0, r2, #40
	str	r0, [r4, #12]
	str	r2, [r4, #0]
	str	ip, [r4, #8]
	ldr	r1, [ip, #-12]
	ldr	r2, [lr, #4]
	ldr	r0, [lr, #16]
	add	r3, r4, r1
	str	r0, [r3, #8]
	str	r2, [r4, #0]
	ldr	r1, [lr, #8]
	ldr	ip, [r2, #-12]
	ldr	r3, .L274+8
	mov	r0, r4
	str	r1, [r4, ip]
	str	r3, [r0, #12]!
	bl	_ZN4_STL8ios_baseD2Ev
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L275:
	.align	2
.L274:
	.word	_ZTTN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE
	.word	_ZTVN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE+12
	.word	_ZTVN4_STL9basic_iosIcNS_11char_traitsIcEEEE+8
	.fnend
	.size	_ZN4_STL14basic_iostreamIcNS_11char_traitsIcEEED1Ev, .-_ZN4_STL14basic_iostreamIcNS_11char_traitsIcEEED1Ev
	.section	.text._ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendIPKcEERS5_T_SA_,"axG",%progbits,_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendIPKcEERS5_T_SA_,comdat
	.align	2
	.weak	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendIPKcEERS5_T_SA_
	.hidden	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendIPKcEERS5_T_SA_
	.type	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendIPKcEERS5_T_SA_, %function
_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendIPKcEERS5_T_SA_:
	.fnstart
.LFB5959:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, r2
	stmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, lr}
	.save {r3, r4, r5, r6, r7, r8, sl, lr}
	mov	r6, r1
	mov	r8, r2
	mov	r4, r0
	beq	.L277
	ldmia	r0, {r1, r7}	@ phole ldm
	rsb	r5, r6, r2
	cmn	r5, #1
	rsb	r7, r1, r7
	beq	.L278
	rsb	r3, r5, #-16777216
	add	ip, r3, #16711680
	add	r2, ip, #65280
	add	r3, r2, #254
	cmp	r7, r3
	bls	.L279
.L278:
	ldr	r0, .L291
	bl	_ZN4_STL24__stl_throw_length_errorEPKc
	ldr	r1, [r4, #0]
.L279:
	ldr	r2, [r4, #8]
	sub	r3, r2, #1
	rsb	ip, r1, r3
	add	r2, r5, r7
	cmp	r2, ip
	bls	.L280
	add	sl, r7, #1
	cmp	r5, r7
	addcs	r7, sl, r5
	addcc	r7, sl, r7
	cmp	r7, #0
	moveq	r8, r7
	bne	.L289
.L282:
	ldr	sl, [r4, #4]
	cmp	r1, sl
	moveq	r0, r8
	beq	.L285
	rsb	sl, r1, sl
	mov	r0, r8
	mov	r2, sl
	bl	memmove
	add	r0, r0, sl
.L285:
	mov	r1, r6
	mov	r2, r5
	bl	memmove
	mov	r1, #0
	strb	r1, [r0, r5]
	ldr	r3, [r4, #0]
	cmp	r3, #0
	add	r5, r0, r5
	movne	r0, r3
	blne	free
.L286:
	add	r7, r8, r7
	stmib	r4, {r5, r7}	@ phole stm
	str	r8, [r4, #0]
.L277:
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, lr}
	bx	lr
.L280:
	add	r1, r6, #1
	cmp	r8, r1
	ldr	r0, [r4, #4]
	beq	.L287
	add	r0, r0, #1
	rsb	r2, r1, r8
	bl	memmove
	ldr	r0, [r4, #4]
.L287:
	mov	r2, #0
	strb	r2, [r0, r5]
	ldrb	ip, [r6, #0]	@ zero_extendqisi2
	ldr	r3, [r4, #4]
	strb	ip, [r3, #0]
	ldr	r0, [r4, #4]
	add	r5, r0, r5
	str	r5, [r4, #4]
	b	.L277
.L289:
	mov	r0, r7
	bl	malloc
	subs	r8, r0, #0
	ldrne	r1, [r4, #0]
	bne	.L282
.L290:
	mov	r0, r7
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
	ldr	r1, [r4, #0]
	mov	r8, r0
	b	.L282
.L292:
	.align	2
.L291:
	.word	.LC0
	.fnend
	.size	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendIPKcEERS5_T_SA_, .-_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendIPKcEERS5_T_SA_
	.section	.text._ZNK4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE16_M_append_bufferEv,"axG",%progbits,_ZNK4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE16_M_append_bufferEv,comdat
	.align	2
	.weak	_ZNK4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE16_M_append_bufferEv
	.hidden	_ZNK4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE16_M_append_bufferEv
	.type	_ZNK4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE16_M_append_bufferEv, %function
_ZNK4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE16_M_append_bufferEv:
	.fnstart
.LFB5712:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	ldr	r3, [r0, #16]
	add	r5, r0, #52
	cmp	r5, r3
	mov	r4, r0
	ldr	r2, [r0, #20]
	beq	.L300
.L296:
	ldr	ip, [r4, #24]
	cmp	r2, ip
	beq	.L298
.L299:
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L298:
	add	r1, r5, #8
	str	r1, [r4, #24]
	str	r5, [r4, #16]
	str	r5, [r4, #20]
	b	.L299
.L300:
	cmp	r5, r2
	moveq	r2, r5
	beq	.L296
	add	r0, r0, #40
	mov	r1, r5
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendIPKcEERS5_T_SA_
	add	r0, r4, #60
	str	r0, [r4, #24]
	str	r5, [r4, #20]
	str	r5, [r4, #16]
	b	.L299
	.fnend
	.size	_ZNK4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE16_M_append_bufferEv, .-_ZNK4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE16_M_append_bufferEv
	.section	.text._ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposIiEEi,"axG",%progbits,_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposIiEEi,comdat
	.align	2
	.weak	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposIiEEi
	.hidden	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposIiEEi
	.type	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposIiEEi, %function
_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposIiEEi:
	.fnstart
.LFB6301:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	.save {r4, r5, r6, r7, r8, lr}
	.pad #16
	sub	sp, sp, #16
	add	ip, sp, #8
	ldr	r4, [sp, #40]
	stmia	ip, {r2, r3}
	mov	r3, r4, lsr #3
	ands	r6, r3, #1
	mov	r5, r0
	mov	r7, r2
	ldr	r8, [sp, #12]
	beq	.L302
	ldr	r0, [r1, #36]
	tst	r0, #8
	bne	.L314
.L311:
	mvn	r3, #0
	mov	r1, #0
	str	r3, [r5, #0]
	str	r1, [r5, #4]
.L301:
	mov	r0, r5
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L314:
	ldr	r2, [r1, #8]
	cmp	r2, #0
	beq	.L311
.L302:
	mov	r4, r4, lsr #4
	ands	r4, r4, #1
	bne	.L315
.L304:
	ldr	r2, [r1, #36]
	and	r0, r2, #24
	cmp	r0, #16
	beq	.L316
.L306:
	cmp	r6, #0
	beq	.L307
	cmp	r7, #0
	blt	.L311
	ldr	r2, [r1, #12]
	ldr	r3, [r1, #4]
	rsb	ip, r3, r2
	cmp	ip, r7
	blt	.L311
	add	ip, r3, r7
	str	ip, [r1, #8]
	str	r2, [r1, #12]
	str	r3, [r1, #4]
.L307:
	cmp	r4, #0
	beq	.L310
	cmp	r7, #0
	blt	.L311
	ldr	r3, [r1, #40]
	ldr	r0, [r1, #44]
	rsb	r2, r3, r0
	cmp	r7, r2
	bhi	.L311
	add	r2, r3, r2
	add	r0, r3, r7
	str	r0, [r1, #20]
	str	r2, [r1, #24]
	str	r3, [r1, #16]
.L310:
	stmia	r5, {r7, r8}	@ phole stm
	b	.L301
.L315:
	ldr	r3, [r1, #36]
	tst	r3, #16
	beq	.L311
	ldr	ip, [r1, #20]
	cmp	ip, #0
	bne	.L304
	b	.L311
.L316:
	mov	r0, r1
	str	r1, [sp, #4]
	bl	_ZNK4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE16_M_append_bufferEv
	ldr	r1, [sp, #4]
	b	.L306
	.fnend
	.size	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposIiEEi, .-_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposIiEEi
	.section	.text._ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffElii,"axG",%progbits,_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffElii,comdat
	.align	2
	.weak	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffElii
	.hidden	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffElii
	.type	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffElii, %function
_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffElii:
	.fnstart
.LFB6300:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	.pad #16
	sub	sp, sp, #16
	ldr	ip, [sp, #32]
	and	r4, ip, #24
	cmp	r4, #24
	mov	r4, r0
	beq	.L343
	ands	r6, ip, #8
	movne	r5, #0
	beq	.L344
.L322:
	ldr	r0, [r1, #36]
	tst	r0, #8
	beq	.L320
	ldr	ip, [r1, #8]
	cmp	ip, #0
	beq	.L320
	cmp	r5, #0
	mov	r6, #1
	bne	.L323
.L326:
	ldr	ip, [r1, #36]
	and	r0, ip, #24
	cmp	r0, #16
	beq	.L345
.L340:
	cmp	r3, #2
	beq	.L330
	cmp	r3, #4
	beq	.L331
	cmp	r3, #1
	bne	.L320
	mov	r3, #0
.L332:
	cmp	r6, #0
	add	r2, r3, r2
	beq	.L335
	ldr	r3, [r1, #4]
.L334:
	ldr	r0, [r1, #12]
	rsb	r0, r3, r0
	cmp	r2, r0
	movle	ip, #0
	movgt	ip, #1
	orrs	ip, ip, r2, lsr #31
	bne	.L320
	add	r0, r3, r0
	add	ip, r3, r2
	str	ip, [r1, #8]
	str	r0, [r1, #12]
	str	r3, [r1, #4]
.L335:
	cmp	r5, #0
	beq	.L337
	ldr	r3, [r1, #16]
	ldr	r0, [r1, #24]
	rsb	r0, r3, r0
	cmp	r2, r0
	movle	ip, #0
	movgt	ip, #1
	orrs	ip, ip, r2, lsr #31
	bne	.L320
	add	r0, r3, r0
	add	ip, r3, r2
	str	ip, [r1, #20]
	str	r0, [r1, #24]
	str	r3, [r1, #16]
.L337:
	mov	r1, #0
	str	r2, [r4, #0]
	str	r1, [r4, #4]
	b	.L317
.L344:
	tst	ip, #16
	beq	.L320
	ldr	r0, [r1, #36]
	mov	r5, #1
.L323:
	tst	r0, #16
	beq	.L320
	ldr	r0, [r1, #20]
	cmp	r0, #0
	bne	.L326
.L320:
	mvn	r3, #0
	mov	r2, #0
	str	r3, [r4, #0]
	str	r2, [r4, #4]
.L317:
	mov	r0, r4
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L343:
	cmp	r3, #1
	cmpne	r3, #4
	moveq	r5, #1
	beq	.L322
	b	.L320
.L331:
	add	r0, r1, #44
	ldmda	r0, {r0, r3}
	sub	r3, r3, r0
	b	.L332
.L330:
	cmp	r6, #0
	beq	.L333
	ldr	r3, [r1, #4]
	ldr	r0, [r1, #8]
	rsb	ip, r3, r0
	add	r2, ip, r2
	b	.L334
.L345:
	mov	r0, r1
	str	r1, [sp, #12]
	str	r2, [sp, #8]
	str	r3, [sp, #4]
	bl	_ZNK4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE16_M_append_bufferEv
	ldr	r3, [sp, #4]
	ldr	r2, [sp, #8]
	ldr	r1, [sp, #12]
	b	.L340
.L333:
	add	r0, r1, #20
	ldmda	r0, {r0, r3}
	sub	r3, r3, r0
	add	r2, r3, r2
	b	.L335
	.fnend
	.size	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffElii, .-_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffElii
	.section	.text.T.1792,"ax",%progbits
	.align	2
	.type	T.1792, %function
T.1792:
	.fnstart
.LFB6342:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	cmp	r0, #0
	.pad #12
	sub	sp, sp, #12
	str	r0, [sp, #4]
	beq	.L365
.L374:
	ldr	r3, [sp, #4]
	ldr	fp, [r3, #12]
	cmp	fp, #0
	beq	.L348
.L373:
	ldr	r4, [fp, #12]
	cmp	r4, #0
	beq	.L349
.L372:
	ldr	r5, [r4, #12]
	cmp	r5, #0
	beq	.L350
.L371:
	ldr	r6, [r5, #12]
	cmp	r6, #0
	beq	.L351
.L370:
	ldr	r7, [r6, #12]
	cmp	r7, #0
	beq	.L352
.L369:
	ldr	r8, [r7, #12]
	cmp	r8, #0
	beq	.L353
.L368:
	ldr	sl, [r8, #12]
	cmp	sl, #0
	beq	.L354
.L367:
	ldr	r9, [sl, #12]
	cmp	r9, #0
	beq	.L355
	b	.L366
.L375:
	mov	r9, r3
.L366:
	ldr	r0, [r9, #12]
	bl	T.1792
	ldr	r1, [r9, #8]
	mov	r0, r9
	str	r1, [sp, #0]
	bl	free
	ldr	r3, [sp, #0]
	cmp	r3, #0
	bne	.L375
.L355:
	ldr	r9, [sl, #8]
	mov	r0, sl
	bl	free
	cmp	r9, #0
	movne	sl, r9
	bne	.L367
.L354:
	ldr	sl, [r8, #8]
	mov	r0, r8
	bl	free
	cmp	sl, #0
	movne	r8, sl
	bne	.L368
.L353:
	ldr	r8, [r7, #8]
	mov	r0, r7
	bl	free
	cmp	r8, #0
	movne	r7, r8
	bne	.L369
.L352:
	ldr	r7, [r6, #8]
	mov	r0, r6
	bl	free
	cmp	r7, #0
	movne	r6, r7
	bne	.L370
.L351:
	ldr	r6, [r5, #8]
	mov	r0, r5
	bl	free
	cmp	r6, #0
	movne	r5, r6
	bne	.L371
.L350:
	ldr	r5, [r4, #8]
	mov	r0, r4
	bl	free
	cmp	r5, #0
	movne	r4, r5
	bne	.L372
.L349:
	ldr	r4, [fp, #8]
	mov	r0, fp
	bl	free
	cmp	r4, #0
	movne	fp, r4
	bne	.L373
.L348:
	ldr	r2, [sp, #4]
	ldr	r4, [r2, #8]
	mov	r0, r2
	bl	free
	cmp	r4, #0
	strne	r4, [sp, #4]
	bne	.L374
.L365:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
	.fnend
	.size	T.1792, .-T.1792
	.section	.text._ZN21QuStupidPointerHelper6removeEPv,"axG",%progbits,_ZN21QuStupidPointerHelper6removeEPv,comdat
	.align	2
	.weak	_ZN21QuStupidPointerHelper6removeEPv
	.hidden	_ZN21QuStupidPointerHelper6removeEPv
	.type	_ZN21QuStupidPointerHelper6removeEPv, %function
_ZN21QuStupidPointerHelper6removeEPv:
	.fnstart
.LFB2722:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, lr}
	.save {r3, r4, r5, r6, r7, lr}
	ldr	r4, .L404
	ldr	r3, [r4, #0]
	ldr	r5, [r3, #4]
	cmp	r5, #0
	moveq	r6, r3
	moveq	r7, r3
	beq	.L388
	mov	r1, r3
	mov	r7, r5
	b	.L381
.L401:
	mov	r1, r7
	mov	r7, r2
.L381:
	ldr	r2, [r7, #16]
	cmp	r0, r2
	ldrhi	r2, [r7, #12]
	ldrls	r2, [r7, #8]
	movhi	r7, r1
	cmp	r2, #0
	bne	.L401
	mov	r1, r3
	b	.L385
.L402:
	mov	r1, r5
	mov	r5, r2
.L385:
	ldr	r2, [r5, #16]
	cmp	r0, r2
	ldrcs	r2, [r5, #12]
	ldrcc	r2, [r5, #8]
	movcs	r5, r1
	cmp	r2, #0
	bne	.L402
	cmp	r7, r5
	mov	r6, r7
	moveq	r7, r5
	beq	.L388
	mov	r0, r7
.L389:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r5, r0
	bne	.L389
	ldr	r3, [r4, #0]
	mov	r6, r5
.L388:
	ldr	r2, [r3, #8]
	cmp	r7, r2
	beq	.L403
.L391:
	cmp	r7, r6
	bne	.L400
.L398:
	ldmfd	sp!, {r3, r4, r5, r6, r7, lr}
	bx	lr
.L397:
	mov	r7, r5
.L400:
	mov	r0, r7
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	ip, [r4, #0]
	mov	r5, r0
	add	r3, ip, #12
	mov	r0, r7
	add	r1, ip, #4
	add	r2, ip, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L396:
	ldr	r3, [r4, #4]
	cmp	r5, r6
	sub	lr, r3, #1
	str	lr, [r4, #4]
	bne	.L397
	b	.L398
.L403:
	cmp	r6, r3
	bne	.L391
	ldr	r3, [r4, #4]
	cmp	r3, #0
	ldr	r4, .L404
	beq	.L398
	ldr	r0, [r6, #4]
	bl	T.1792
	ldr	r2, [r4, #0]
	str	r2, [r2, #8]
	ldr	ip, [r4, #0]
	mov	r0, #0
	str	r0, [ip, #4]
	ldr	r1, [r4, #0]
	str	r1, [r1, #12]
	str	r0, [r4, #4]
	b	.L398
.L405:
	.align	2
.L404:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	_ZN21QuStupidPointerHelper6removeEPv, .-_ZN21QuStupidPointerHelper6removeEPv
	.section	.text._ZN18InstructionManager8singleUpEi13QuScreenCoord,"axG",%progbits,_ZN18InstructionManager8singleUpEi13QuScreenCoord,comdat
	.align	2
	.weak	_ZN18InstructionManager8singleUpEi13QuScreenCoord
	.hidden	_ZN18InstructionManager8singleUpEi13QuScreenCoord
	.type	_ZN18InstructionManager8singleUpEi13QuScreenCoord, %function
_ZN18InstructionManager8singleUpEi13QuScreenCoord:
	.fnstart
.LFB3923:
	@ Function supports interworking.
	@ args = 16, pretend = 8, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	.pad #8
	sub	sp, sp, #8
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	.pad #16
	sub	sp, sp, #16
	add	r1, sp, #24
	stmia	r1, {r2, r3}
	mov	r4, r0
	ldmia	r1, {r0, r1, r2, r3}
	mov	ip, sp
	stmia	ip, {r0, r1, r2, r3}
	ldr	r2, [r4, #172]
	ldr	ip, [r4, #180]
	add	r1, r2, ip
	cmp	r1, r0
	ldr	r1, [sp, #4]
	blt	.L411
	cmp	r2, r0
	bgt	.L411
	ldr	r3, [r4, #176]
	ldr	r0, [r4, #184]
	add	r2, r3, r0
	cmp	r2, r1
	blt	.L411
	cmp	r3, r1
	bgt	.L411
	ldr	ip, [r4, #12]
	ldr	r1, [r4, #16]
	cmp	ip, r1
	ldrge	lr, [r4, #20]
	movge	r0, lr
	ldrge	r3, [lr, #0]
	ldrge	ip, [r3, #8]
	movge	lr, pc
	bxge	ip
.L411:
	add	sp, sp, #16
	ldmfd	sp!, {r4, lr}
	add	sp, sp, #8
	bx	lr
	.fnend
	.size	_ZN18InstructionManager8singleUpEi13QuScreenCoord, .-_ZN18InstructionManager8singleUpEi13QuScreenCoord
	.section	.text._ZN23QuAbsScreenIntBoxButton5clickE13QuScreenCoord15GDeviceRotation,"axG",%progbits,_ZN23QuAbsScreenIntBoxButton5clickE13QuScreenCoord15GDeviceRotation,comdat
	.align	2
	.weak	_ZN23QuAbsScreenIntBoxButton5clickE13QuScreenCoord15GDeviceRotation
	.hidden	_ZN23QuAbsScreenIntBoxButton5clickE13QuScreenCoord15GDeviceRotation
	.type	_ZN23QuAbsScreenIntBoxButton5clickE13QuScreenCoord15GDeviceRotation, %function
_ZN23QuAbsScreenIntBoxButton5clickE13QuScreenCoord15GDeviceRotation:
	.fnstart
.LFB3857:
	@ Function supports interworking.
	@ args = 24, pretend = 16, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	sub	sp, sp, #16
	str	r4, [sp, #-4]!
	add	ip, sp, #8
	stmia	ip, {r1, r2, r3}
	ldr	r3, [sp, #24]
	cmp	r3, #2
	mov	r2, r1
	add	r1, sp, #12
	ldmia	r1, {r1, r4, ip}	@ phole ldm
	beq	.L417
	cmp	r3, #3
	rsbeq	r3, r1, ip
	beq	.L419
	cmp	r3, #1
	movne	r3, r2
	rsbeq	r2, r2, r4
	movne	r2, r1
	moveq	r3, r1
.L419:
	ldr	r1, [r0, #4]
	ldr	ip, [r0, #12]
	add	ip, r1, ip
	cmp	ip, r3
	blt	.L420
	cmp	r1, r3
	bgt	.L420
	ldr	r1, [r0, #16]
	ldr	r0, [r0, #8]
	add	r3, r0, r1
	cmp	r3, r2
	blt	.L420
	cmp	r0, r2
	movgt	r0, #0
	movle	r0, #1
	b	.L421
.L420:
	mov	r0, #0
.L421:
	ldmfd	sp!, {r4}
	add	sp, sp, #16
	bx	lr
.L417:
	rsb	r3, r2, r4
	rsb	r2, r1, ip
	b	.L419
	.cantunwind
	.fnend
	.size	_ZN23QuAbsScreenIntBoxButton5clickE13QuScreenCoord15GDeviceRotation, .-_ZN23QuAbsScreenIntBoxButton5clickE13QuScreenCoord15GDeviceRotation
	.section	.text._ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev,"axG",%progbits,_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev,comdat
	.align	2
	.weak	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.hidden	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.type	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev, %function
_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev:
	.fnstart
.LFB5386:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	ldr	r0, [r0, #40]
	ldr	r3, .L427
	cmp	r0, #0
	str	r3, [r4, #0]
	blne	free
.L424:
	ldr	r1, .L427+4
	mov	r0, r4
	str	r1, [r0], #28
	bl	_ZN4_STL6localeD1Ev
	mov	r0, r4
	bl	_ZdlPv
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L428:
	.align	2
.L427:
	.word	_ZTVN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE+8
	.word	_ZTVN4_STL15basic_streambufIcNS_11char_traitsIcEEEE+8
	.fnend
	.size	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev, .-_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.section	.text._ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev,"axG",%progbits,_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev,comdat
	.align	2
	.weak	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.hidden	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.type	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev, %function
_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev:
	.fnstart
.LFB5385:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	ldr	r0, [r0, #40]
	ldr	r3, .L433
	cmp	r0, #0
	str	r3, [r4, #0]
	blne	free
.L430:
	ldr	r1, .L433+4
	mov	r0, r4
	str	r1, [r0], #28
	bl	_ZN4_STL6localeD1Ev
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L434:
	.align	2
.L433:
	.word	_ZTVN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE+8
	.word	_ZTVN4_STL15basic_streambufIcNS_11char_traitsIcEEEE+8
	.fnend
	.size	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev, .-_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.section	.text._ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev,"axG",%progbits,_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev,comdat
	.align	2
	.weak	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	.hidden	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	.type	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev, %function
_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev:
	.fnstart
.LFB5083:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	ldr	r4, [r0, #0]
	mov	r5, r0
	ldr	r0, [r4, #0]
	ldr	r3, [r0, #-12]
	add	r3, r4, r3
	ldr	r2, [r3, #4]
	tst	r2, #8192
	bne	.L439
.L436:
	mov	r0, r5
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L439:
	ldr	r3, [r3, #88]
	cmp	r3, #0
	beq	.L436
	mov	r0, r3
	ldr	r1, [r3, #0]
	ldr	ip, [r1, #20]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	bne	.L436
	ldr	r3, [r4, #0]
	ldr	r0, [r3, #-12]
	add	r0, r4, r0
	ldr	r2, [r0, #8]
	ldr	lr, [r0, #20]
	orr	ip, r2, #1
	tst	ip, lr
	str	ip, [r0, #8]
	beq	.L436
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
	b	.L436
	.fnend
	.size	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev, .-_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	.section	.text._ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE9_M_insertEPNS_18_Rb_tree_node_baseESD_RKS4_SD_,"axG",%progbits,_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE9_M_insertEPNS_18_Rb_tree_node_baseESD_RKS4_SD_,comdat
	.align	2
	.weak	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE9_M_insertEPNS_18_Rb_tree_node_baseESD_RKS4_SD_
	.hidden	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE9_M_insertEPNS_18_Rb_tree_node_baseESD_RKS4_SD_
	.type	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE9_M_insertEPNS_18_Rb_tree_node_baseESD_RKS4_SD_, %function
_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE9_M_insertEPNS_18_Rb_tree_node_baseESD_RKS4_SD_:
	.fnstart
.LFB5753:
	@ Function supports interworking.
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	.save {r4, r5, r6, r7, r8, lr}
	mov	r4, r1
	ldr	r1, [r1, #0]
	cmp	r1, r3
	mov	r7, r3
	mov	r5, r0
	ldr	r8, [sp, #24]
	beq	.L441
	ldr	r3, [sp, #28]
	cmp	r3, #0
	beq	.L451
.L442:
	mov	r0, #24
	bl	malloc
	subs	r6, r0, #0
	beq	.L452
.L449:
	ldr	ip, [r8, #4]
	ldr	lr, [r8, #0]
	str	ip, [r6, #20]
	str	lr, [r6, #16]
	str	r6, [r7, #12]
	ldr	r3, [r4, #0]
	ldr	r0, [r3, #12]
	cmp	r7, r0
	streq	r6, [r3, #12]
.L445:
	mov	r0, #0
	str	r0, [r6, #12]
	str	r0, [r6, #8]
	str	r7, [r6, #4]
	ldr	r3, [r4, #0]
	mov	r0, r6
	add	r1, r3, #4
	bl	_ZN4_STL10_Rb_globalIbE10_RebalanceEPNS_18_Rb_tree_node_baseERS3_
	ldr	r2, [r4, #4]
	add	r1, r2, #1
	str	r1, [r4, #4]
	mov	r0, r5
	str	r6, [r5, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L451:
	cmp	r2, #0
	beq	.L453
.L441:
	mov	r0, #24
	bl	malloc
	subs	r6, r0, #0
	bne	.L443
	mov	r0, #24
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
	mov	r6, r0
.L443:
	ldr	ip, [r8, #4]
	ldr	lr, [r8, #0]
	str	ip, [r6, #20]
	str	lr, [r6, #16]
	str	r6, [r7, #8]
	ldr	r3, [r4, #0]
	cmp	r3, r7
	beq	.L454
	ldr	r2, [r3, #8]
	cmp	r7, r2
	streq	r6, [r3, #8]
	b	.L445
.L454:
	str	r6, [r7, #4]
	ldr	r1, [r4, #0]
	str	r6, [r1, #12]
	b	.L445
.L452:
	mov	r0, #24
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
	mov	r6, r0
	b	.L449
.L453:
	ldr	r2, [r8, #0]
	ldr	r0, [r7, #16]
	cmp	r2, r0
	bcs	.L442
	b	.L441
	.fnend
	.size	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE9_M_insertEPNS_18_Rb_tree_node_baseESD_RKS4_SD_, .-_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE9_M_insertEPNS_18_Rb_tree_node_baseESD_RKS4_SD_
	.section	.text._ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueERKS4_,"axG",%progbits,_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueERKS4_,comdat
	.align	2
	.weak	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueERKS4_
	.hidden	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueERKS4_
	.type	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueERKS4_, %function
_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueERKS4_:
	.fnstart
.LFB5750:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	.save {r4, r5, r6, r7, lr}
	ldr	r3, [r1, #0]
	ldr	r6, [r3, #4]
	cmp	r6, #0
	.pad #20
	sub	sp, sp, #20
	mov	r7, r1
	mov	r4, r0
	mov	r5, r2
	moveq	r6, r3
	beq	.L463
	ldr	r0, [r2, #0]
	b	.L461
.L476:
	mov	r6, r2
.L461:
	ldr	r2, [r6, #16]
	cmp	r0, r2
	ldrcc	r2, [r6, #8]
	ldrcs	r2, [r6, #12]
	movcc	r1, #1
	movcs	r1, #0
	cmp	r2, #0
	bne	.L476
	cmp	r1, #0
	moveq	r0, r6
	bne	.L463
	ldr	r3, [r5, #0]
	ldr	r2, [r0, #16]
	cmp	r2, r3
	movcs	r3, #0
	strcs	r0, [r4, #0]
	strcsb	r3, [r4, #4]
	bcc	.L477
.L455:
	mov	r0, r4
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L463:
	ldr	r3, [r3, #8]
	cmp	r6, r3
	beq	.L478
	mov	r0, r6
	bl	_ZN4_STL10_Rb_globalIbE12_M_decrementEPNS_18_Rb_tree_node_baseE
	ldr	r3, [r5, #0]
	ldr	r2, [r0, #16]
	cmp	r2, r3
	movcs	r3, #0
	strcs	r0, [r4, #0]
	strcsb	r3, [r4, #4]
	bcs	.L455
.L477:
	mov	lr, #0
	mov	r1, r7
	add	r0, sp, #8
	mov	r2, lr
	mov	r3, r6
	stmia	sp, {r5, lr}	@ phole stm
	bl	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE9_M_insertEPNS_18_Rb_tree_node_baseESD_RKS4_SD_
	ldr	r1, [sp, #8]
	mov	r0, #1
	str	r1, [r4, #0]
	strb	r0, [r4, #4]
	b	.L455
.L478:
	mov	r1, r7
	add	r0, sp, #12
	mov	ip, #0
	mov	r2, r6
	mov	r3, r6
	stmia	sp, {r5, ip}	@ phole stm
	bl	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE9_M_insertEPNS_18_Rb_tree_node_baseESD_RKS4_SD_
	ldr	r1, [sp, #12]
	mov	r0, #1
	str	r1, [r4, #0]
	strb	r0, [r4, #4]
	b	.L455
	.fnend
	.size	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueERKS4_, .-_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueERKS4_
	.section	.text._ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS4_NS_16_Nonconst_traitsIS4_EEEERKS4_,"axG",%progbits,_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS4_NS_16_Nonconst_traitsIS4_EEEERKS4_,comdat
	.align	2
	.weak	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS4_NS_16_Nonconst_traitsIS4_EEEERKS4_
	.hidden	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS4_NS_16_Nonconst_traitsIS4_EEEERKS4_
	.type	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS4_NS_16_Nonconst_traitsIS4_EEEERKS4_, %function
_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS4_NS_16_Nonconst_traitsIS4_EEEERKS4_:
	.fnstart
.LFB5435:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	.save {r4, r5, r6, r7, r8, lr}
	ldr	r6, [r1, #0]
	ldr	ip, [r2, #0]
	mov	r8, r2
	ldr	r2, [r6, #8]
	cmp	ip, r2
	.pad #40
	sub	sp, sp, #40
	mov	r5, r1
	mov	r4, r3
	mov	r7, r0
	beq	.L507
	cmp	ip, r6
	beq	.L508
	mov	r0, ip
	bl	_ZN4_STL10_Rb_globalIbE12_M_decrementEPNS_18_Rb_tree_node_baseE
	mov	ip, r0
	ldr	r0, [r8, #0]
	ldr	r3, [r4, #0]
	ldr	r1, [r0, #16]
	cmp	r3, r1
	bcs	.L494
	ldr	r2, [ip, #16]
	cmp	r3, r2
	bls	.L495
	ldr	r3, [ip, #12]
	cmp	r3, #0
	beq	.L506
.L500:
	mov	r2, r0
	mov	r1, r5
	mov	r0, r7
.L504:
	mov	ip, #0
	mov	r3, r2
	stmia	sp, {r4, ip}	@ phole stm
	bl	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE9_M_insertEPNS_18_Rb_tree_node_baseESD_RKS4_SD_
	b	.L479
.L494:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	ip, [r8, #0]
	ldr	r3, [r4, #0]
	ldr	r1, [ip, #16]
	cmp	r1, r3
	bcs	.L497
	ldr	lr, [r5, #0]
	cmp	lr, r0
	beq	.L498
	ldr	r2, [r0, #16]
	cmp	r3, r2
	bcs	.L499
.L498:
	ldr	r2, [ip, #12]
	cmp	r2, #0
	bne	.L500
	mov	r1, r5
	b	.L505
.L507:
	ldr	r3, [r1, #4]
	cmp	r3, #0
	beq	.L509
	ldr	r2, [r4, #0]
	ldr	r3, [ip, #16]
	cmp	r2, r3
	movcc	r2, ip
	bcc	.L504
	bhi	.L485
.L497:
	str	ip, [r7, #0]
.L479:
	mov	r0, r7
	add	sp, sp, #40
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L495:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
.L499:
	add	r0, sp, #8
	mov	r1, r5
	mov	r2, r4
	bl	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueERKS4_
	ldr	r0, [sp, #8]
	str	r0, [r7, #0]
	b	.L479
.L508:
	ldr	r3, [ip, #12]
	ldr	r2, [r4, #0]
	ldr	lr, [r3, #16]
	cmp	lr, r2
	bcc	.L510
	mov	r2, r4
	add	r0, sp, #16
	bl	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueERKS4_
	ldr	r3, [sp, #16]
	str	r3, [r7, #0]
	b	.L479
.L485:
	mov	r0, ip
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r1, [r5, #0]
	cmp	r1, r0
	beq	.L511
	ldr	lr, [r4, #0]
	ldr	r2, [r0, #16]
	cmp	lr, r2
	bcs	.L488
	ldr	ip, [r8, #0]
	ldr	r3, [ip, #12]
	cmp	r3, #0
	bne	.L500
.L506:
	mov	r1, r5
	mov	r2, r3
.L505:
	mov	r3, ip
	mov	r0, r7
	stmia	sp, {r4, ip}	@ phole stm
	bl	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE9_M_insertEPNS_18_Rb_tree_node_baseESD_RKS4_SD_
	b	.L479
.L510:
	mov	r2, #0
	stmia	sp, {r4, ip}	@ phole stm
	bl	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE9_M_insertEPNS_18_Rb_tree_node_baseESD_RKS4_SD_
	b	.L479
.L509:
	add	r0, sp, #32
	mov	r2, r4
	bl	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueERKS4_
	ldr	r0, [sp, #32]
	str	r0, [r7, #0]
	b	.L479
.L488:
	add	r0, sp, #24
	mov	r1, r5
	mov	r2, r4
	bl	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueERKS4_
	ldr	r0, [sp, #24]
	str	r0, [r7, #0]
	b	.L479
.L511:
	ldr	ip, [r8, #0]
	mov	r1, r5
	mov	r3, ip
	mov	r0, r7
	mov	r2, #0
	stmia	sp, {r4, ip}	@ phole stm
	bl	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE9_M_insertEPNS_18_Rb_tree_node_baseESD_RKS4_SD_
	b	.L479
	.fnend
	.size	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS4_NS_16_Nonconst_traitsIS4_EEEERKS4_, .-_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS4_NS_16_Nonconst_traitsIS4_EEEERKS4_
	.section	.text.T.1801,"ax",%progbits
	.align	2
	.type	T.1801, %function
T.1801:
	.fnstart
.LFB6351:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	ldr	r3, .L523
	ldr	r3, [r3, #0]
	ldr	ip, [r3, #4]
	cmp	ip, #0
	.pad #16
	sub	sp, sp, #16
	ldr	r4, [r0, #0]
	moveq	ip, r3
	beq	.L514
	mov	r1, r3
	b	.L517
.L522:
	mov	r1, ip
	mov	ip, r2
.L517:
	ldr	r2, [ip, #16]
	cmp	r4, r2
	ldrhi	r2, [ip, #12]
	ldrls	r2, [ip, #8]
	movhi	ip, r1
	cmp	r2, #0
	bne	.L522
.L514:
	cmp	ip, r3
	beq	.L519
	ldr	r0, [ip, #16]
	cmp	r0, r4
	mov	r0, ip
	bls	.L520
.L519:
	add	r0, sp, #8
	str	ip, [sp, #12]
	ldr	r1, .L523
	mov	ip, #0
	add	r2, sp, #12
	mov	r3, sp
	stmia	sp, {r4, ip}	@ phole stm
	bl	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS4_NS_16_Nonconst_traitsIS4_EEEERKS4_
	ldr	r0, [sp, #8]
.L520:
	add	r0, r0, #20
	add	sp, sp, #16
	ldmfd	sp!, {r4, lr}
	bx	lr
.L524:
	.align	2
.L523:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	T.1801, .-T.1801
	.section	.text._ZN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev,"axG",%progbits,_ZN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev,comdat
	.align	2
	.weak	_ZThn8_N4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.hidden	_ZThn8_N4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.type	_ZThn8_N4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev, %function
_ZThn8_N4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev:
	.fnstart
	sub	r0, r0, #8
	b	.LTHUNK9
	.fnend
	.size	_ZThn8_N4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev, .-_ZThn8_N4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.global	__cxa_end_cleanup
	.align	2
	.weak	_ZN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.hidden	_ZN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.type	_ZN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev, %function
_ZN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev:
	.fnstart
.LFB5006:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	ldr	r3, .L533
	mov	r4, r0
	ldr	r0, [r0, #52]
	ldr	r2, .L533+4
	add	ip, r3, #20
	add	r1, r3, #40
	cmp	r0, #0
	str	r1, [r4, #72]
	str	ip, [r4, #8]
	str	r2, [r4, #12]
	str	r3, [r4, #0]
	blne	free
.L526:
	ldr	r0, .L533+8
	str	r0, [r4, #12]
	add	r0, r4, #40
.LEHB0:
	bl	_ZN4_STL6localeD1Ev
.LEHE0:
	ldr	lr, .L533+12
	ldr	r1, [lr, #4]
	str	r1, [r4, #0]
	ldr	r3, [lr, #16]
	ldr	r0, [r1, #-12]
	ldr	r2, [lr, #24]
	str	r2, [r4, r0]
	str	r3, [r4, #8]
	ldr	ip, [r3, #-12]
	ldr	r2, [lr, #8]
	ldr	r0, [lr, #20]
	add	r1, r4, ip
	str	r0, [r1, #8]
	str	r2, [r4, #0]
	ldr	ip, [lr, #12]
	ldr	r3, [r2, #-12]
	ldr	lr, .L533+16
	mov	r0, r4
	str	ip, [r4, r3]
	str	lr, [r0, #72]!
.LEHB1:
	bl	_ZN4_STL8ios_baseD2Ev
.LEHE1:
	mov	r0, r4
	bl	_ZdlPv
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L531:
.L528:
.L532:
.L529:
	ldr	lr, .L533+12
	ldr	r3, [lr, #4]
	str	r3, [r4, #0]
	ldr	r2, [lr, #16]
	ldr	r1, [r3, #-12]
	ldr	ip, [lr, #24]
	str	ip, [r4, r1]
	str	r2, [r4, #8]
	ldr	r3, [r2, #-12]
	ldr	ip, [lr, #20]
	ldr	r2, [lr, #8]
	add	r1, r4, r3
	str	ip, [r1, #8]
	str	r2, [r4, #0]
	ldr	r1, [lr, #12]
	ldr	r3, [r2, #-12]
	ldr	ip, .L533+16
	mov	lr, r4
	str	r1, [r4, r3]
	str	ip, [lr, #72]!
	mov	r4, r0
	mov	r0, lr
	bl	_ZN4_STL8ios_baseD2Ev
	mov	r0, r4
.LEHB2:
	bl	__cxa_end_cleanup
.LEHE2:
.L534:
	.align	2
.L533:
	.word	_ZTVN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE+12
	.word	_ZTVN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE+8
	.word	_ZTVN4_STL15basic_streambufIcNS_11char_traitsIcEEEE+8
	.word	_ZTTN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.word	_ZTVN4_STL9basic_iosIcNS_11char_traitsIcEEEE+8
	.global	__gxx_personality_v0
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA5006:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5006-.LLSDACSB5006
.LLSDACSB5006:
	.uleb128 .LEHB0-.LFB5006
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L531-.LFB5006
	.uleb128 0x0
	.uleb128 .LEHB1-.LFB5006
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB2-.LFB5006
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE5006:
	.fnend
	.size	_ZN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev, .-_ZN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.section	.text._ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1EPKcRKS4_,"axG",%progbits,_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1EPKcRKS4_,comdat
	.align	2
	.weak	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1EPKcRKS4_
	.hidden	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1EPKcRKS4_
	.type	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1EPKcRKS4_, %function
_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1EPKcRKS4_:
	.fnstart
.LFB4950:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r3, #0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	.save {r4, r5, r6, r7, r8, lr}
	str	r3, [r0, #8]
	str	r3, [r0, #0]
	str	r3, [r0, #4]
	mov	r4, r0
	mov	r0, r1
	mov	r5, r1
	bl	strlen
	add	r7, r5, r0
	rsb	r6, r5, r7
	adds	r8, r6, #1
	beq	.L538
	mov	r0, r8
	bl	malloc
	cmp	r0, #0
	beq	.L548
.L539:
	add	r8, r0, r8
	str	r8, [r4, #8]
	str	r0, [r4, #0]
	str	r0, [r4, #4]
	b	.L545
.L538:
	ldr	r0, .L549
.LEHB3:
	bl	_ZN4_STL24__stl_throw_length_errorEPKc
	ldr	r0, [r4, #0]
.L545:
	cmp	r5, r7
	beq	.L541
	mov	r1, r5
	mov	r2, r6
	bl	memmove
	add	r0, r0, r6
.L541:
	mov	r1, #0
	str	r0, [r4, #4]
	strb	r1, [r0, #0]
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L548:
	mov	r0, r8
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
.LEHE3:
	b	.L539
.L547:
.L543:
	ldr	r3, [r4, #0]
	cmp	r3, #0
	mov	r4, r0
	movne	r0, r3
	blne	free
.L544:
	mov	r0, r4
.LEHB4:
	bl	__cxa_end_cleanup
.LEHE4:
.L550:
	.align	2
.L549:
	.word	.LC0
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA4950:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4950-.LLSDACSB4950
.LLSDACSB4950:
	.uleb128 .LEHB3-.LFB4950
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L547-.LFB4950
	.uleb128 0x0
	.uleb128 .LEHB4-.LFB4950
	.uleb128 .LEHE4-.LEHB4
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE4950:
	.fnend
	.size	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1EPKcRKS4_, .-_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1EPKcRKS4_
	.section	.text._ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_,"axG",%progbits,_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_,comdat
	.align	2
	.weak	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
	.hidden	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
	.type	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_, %function
_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_:
	.fnstart
.LFB4583:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	.save {r4, r5, r6, r7, r8, lr}
	mov	r3, #0
	str	r3, [r0, #0]
	str	r3, [r0, #4]
	ldmia	r1, {r5, r7}	@ phole ldm
	rsb	r6, r5, r7
	adds	r8, r6, #1
	mov	r4, r0
	str	r3, [r0, #8]
	beq	.L554
	mov	r0, r8
	bl	malloc
	cmp	r0, #0
	beq	.L564
.L555:
	add	r8, r0, r8
	str	r8, [r4, #8]
	str	r0, [r4, #0]
	str	r0, [r4, #4]
	b	.L561
.L554:
	ldr	r0, .L565
.LEHB5:
	bl	_ZN4_STL24__stl_throw_length_errorEPKc
	ldr	r0, [r4, #0]
.L561:
	cmp	r5, r7
	beq	.L557
	mov	r1, r5
	mov	r2, r6
	bl	memmove
	add	r0, r0, r6
.L557:
	mov	r1, #0
	str	r0, [r4, #4]
	strb	r1, [r0, #0]
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L564:
	mov	r0, r8
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
.LEHE5:
	b	.L555
.L563:
.L559:
	ldr	r3, [r4, #0]
	cmp	r3, #0
	mov	r4, r0
	movne	r0, r3
	blne	free
.L560:
	mov	r0, r4
.LEHB6:
	bl	__cxa_end_cleanup
.LEHE6:
.L566:
	.align	2
.L565:
	.word	.LC0
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA4583:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4583-.LLSDACSB4583
.LLSDACSB4583:
	.uleb128 .LEHB5-.LFB4583
	.uleb128 .LEHE5-.LEHB5
	.uleb128 .L563-.LFB4583
	.uleb128 0x0
	.uleb128 .LEHB6-.LFB4583
	.uleb128 .LEHE6-.LEHB6
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE4583:
	.fnend
	.size	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_, .-_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
	.section	.text._ZN19QuApRawSoundManager8getSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE,"axG",%progbits,_ZN19QuApRawSoundManager8getSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE,comdat
	.align	2
	.weak	_ZN19QuApRawSoundManager8getSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.hidden	_ZN19QuApRawSoundManager8getSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.type	_ZN19QuApRawSoundManager8getSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE, %function
_ZN19QuApRawSoundManager8getSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE:
	.fnstart
.LFB2817:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	.save {r4, r5, r6, r7, lr}
	.pad #20
	sub	sp, sp, #20
	ldr	r3, [r1, #0]
	add	r4, sp, #4
	mov	r6, r1
	mov	r5, r0
	mov	r1, r2
	mov	r0, r4
	ldr	r7, [r3, #12]
.LEHB7:
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
.LEHE7:
	mov	r0, r5
	mov	r1, r6
	mov	r2, r4
.LEHB8:
	mov	lr, pc
	bx	r7
.LEHE8:
	ldr	r0, [sp, #4]
	cmp	r0, #0
	blne	free
.L567:
	mov	r0, r5
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L572:
.L569:
	mov	r5, r0
	mov	r0, r4
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r5
.LEHB9:
	bl	__cxa_end_cleanup
.LEHE9:
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA2817:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2817-.LLSDACSB2817
.LLSDACSB2817:
	.uleb128 .LEHB7-.LFB2817
	.uleb128 .LEHE7-.LEHB7
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB8-.LFB2817
	.uleb128 .LEHE8-.LEHB8
	.uleb128 .L572-.LFB2817
	.uleb128 0x0
	.uleb128 .LEHB9-.LFB2817
	.uleb128 .LEHE9-.LEHB9
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE2817:
	.fnend
	.size	_ZN19QuApRawSoundManager8getSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE, .-_ZN19QuApRawSoundManager8getSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.section	.text._ZN4_STL9basic_iosIcNS_11char_traitsIcEEE5imbueERKNS_6localeE,"axG",%progbits,_ZN4_STL9basic_iosIcNS_11char_traitsIcEEE5imbueERKNS_6localeE,comdat
	.align	2
	.weak	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEE5imbueERKNS_6localeE
	.hidden	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEE5imbueERKNS_6localeE
	.type	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEE5imbueERKNS_6localeE, %function
_ZN4_STL9basic_iosIcNS_11char_traitsIcEEE5imbueERKNS_6localeE:
	.fnstart
.LFB5703:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, lr}
	mov	r5, r1
	.pad #16
	sub	sp, sp, #16
	mov	r9, r0
	mov	r6, r2
.LEHB10:
	bl	_ZN4_STL8ios_base5imbueERKNS_6localeE
.LEHE10:
	ldr	r4, [r5, #88]
	cmp	r4, #0
	beq	.L575
	ldr	r3, [r4, #0]
	mov	r0, r4
	mov	r1, r6
.LEHB11:
	ldr	ip, [r3, #56]
	mov	lr, pc
	bx	ip
	add	r7, r4, #28
	add	r4, sp, #12
	mov	r0, r4
	mov	r1, r7
	bl	_ZN4_STL6localeC1ERKS0_
.LEHE11:
	mov	r0, r7
	mov	r1, r6
.LEHB12:
	bl	_ZN4_STL6localeaSERKS0_
.LEHE12:
	mov	r0, r4
.LEHB13:
	bl	_ZN4_STL6localeD1Ev
.L575:
	mov	r0, r6
	ldr	r1, .L619
	bl	_ZNK4_STL6locale12_M_get_facetERKNS0_2idE
	str	r0, [r5, #64]
	ldr	r1, .L619+4
	mov	r0, r6
	bl	_ZNK4_STL6locale12_M_get_facetERKNS0_2idE
	str	r0, [r5, #68]
	mov	r1, r0
	ldr	r2, [r0, #0]
	mov	r0, sp
	mov	sl, sp
	add	r4, r5, #72
	ldr	ip, [r2, #16]
	mov	lr, pc
	bx	ip
.LEHE13:
	cmp	sl, r4
	beq	.L578
	ldmia	sp, {r4, r6}	@ phole ldm
	ldr	r8, [r5, #72]
	cmp	r4, r6
	mov	r1, r8
	beq	.L579
	ldr	ip, [r5, #76]
	cmp	r8, ip
	beq	.L581
	rsb	r3, r6, r4
	mvn	lr, r3
	ands	r3, lr, #3
	beq	.L612
	ldrb	r1, [r4], #1	@ zero_extendqisi2
	strb	r1, [r8], #1
	ldr	lr, [r5, #76]
	cmp	r8, lr
	beq	.L601
	cmp	r3, #1
	beq	.L612
	cmp	r3, #2
	beq	.L613
	ldrb	r3, [r4], #1	@ zero_extendqisi2
	strb	r3, [r8], #1
	ldr	r2, [r5, #76]
	cmp	r8, r2
	beq	.L601
.L613:
	ldrb	lr, [r4], #1	@ zero_extendqisi2
	strb	lr, [r8], #1
	ldr	ip, [r5, #76]
	cmp	r8, ip
	beq	.L601
.L612:
	ldrb	r2, [r4], #1	@ zero_extendqisi2
	strb	r2, [r8], #1
	cmp	r6, r4
	mov	r3, r4
	mov	r2, r8
	beq	.L579
	ldr	r1, [r5, #76]
	cmp	r8, r1
	beq	.L601
	ldrb	ip, [r4], #1	@ zero_extendqisi2
	strb	ip, [r8], #1
	ldr	r1, [r5, #76]
	cmp	r8, r1
	beq	.L601
	ldrb	lr, [r4, #0]	@ zero_extendqisi2
	strb	lr, [r8, #0]
	ldr	r4, [r5, #76]
	add	r8, r2, #2
	cmp	r8, r4
	add	r4, r3, #2
	beq	.L601
	ldrb	r8, [r3, #2]	@ zero_extendqisi2
	strb	r8, [r2, #2]
	ldr	r1, [r5, #76]
	add	r8, r2, #3
	cmp	r8, r1
	add	r4, r3, #3
	bne	.L612
.L601:
	ldr	r1, [r5, #72]
.L581:
	rsb	r7, r4, r6
	cmn	r7, #1
	rsb	r8, r1, r8
	beq	.L595
	rsb	lr, r7, #-16777216
	add	ip, lr, #16711680
	add	r2, ip, #65280
	add	r3, r2, #254
	cmp	r8, r3
	bls	.L583
.L595:
	ldr	r0, .L619+8
.LEHB14:
	bl	_ZN4_STL24__stl_throw_length_errorEPKc
	ldr	r1, [r5, #72]
.L583:
	ldr	lr, [r5, #80]
	sub	ip, lr, #1
	rsb	r3, r1, ip
	add	r2, r8, r7
	cmp	r2, r3
	bls	.L584
	add	sl, r8, #1
	cmp	r8, r7
	addcs	r8, sl, r8
	addcc	r8, sl, r7
	cmp	r8, #0
	moveq	r6, r8
	beq	.L586
	mov	r0, r8
	bl	malloc
	subs	r6, r0, #0
	beq	.L618
.L587:
	ldr	r1, [r5, #72]
.L586:
	ldr	sl, [r5, #76]
	cmp	r1, sl
	moveq	r0, r6
	beq	.L589
	rsb	sl, r1, sl
	mov	r0, r6
	mov	r2, sl
	bl	memmove
	add	r0, r0, sl
.L589:
	mov	r1, r4
	mov	r2, r7
	bl	memmove
	mov	r1, #0
	strb	r1, [r0, r7]
	ldr	r3, [r5, #72]
	cmp	r3, #0
	add	r7, r0, r7
	movne	r0, r3
	blne	free
.L590:
	add	r8, r6, r8
	str	r8, [r5, #80]
	str	r7, [r5, #76]
	str	r6, [r5, #72]
.L578:
	ldr	r0, [sp, #0]
	cmp	r0, #0
	blne	free
.L574:
	mov	r0, r9
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L579:
	ldr	r3, [r5, #76]
	cmp	r8, r3
	beq	.L578
	ldrb	lr, [r3, #0]	@ zero_extendqisi2
	strb	lr, [r8, #0]
	ldr	r1, [r5, #76]
	rsb	ip, r3, r8
	add	r0, r1, ip
	str	r0, [r5, #76]
	b	.L578
.L584:
	add	r1, r4, #1
	cmp	r6, r1
	ldr	r0, [r5, #76]
	beq	.L591
	add	r0, r0, #1
	rsb	r2, r1, r6
	bl	memmove
	ldr	r0, [r5, #76]
.L591:
	mov	ip, #0
	strb	ip, [r0, r7]
	ldrb	r2, [r4, #0]	@ zero_extendqisi2
	ldr	r3, [r5, #76]
	strb	r2, [r3, #0]
	ldr	r0, [r5, #76]
	add	r7, r0, r7
	str	r7, [r5, #76]
	b	.L578
.L618:
	mov	r0, r8
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
.LEHE14:
	mov	r6, r0
	b	.L587
.L599:
.L593:
	mov	r5, r0
	mov	r0, sp
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
.L594:
	mov	r0, r9
	bl	_ZN4_STL6localeD1Ev
	mov	r0, r5
.LEHB15:
	bl	__cxa_end_cleanup
.LEHE15:
.L600:
	mov	r5, r0
	b	.L594
.L598:
.L577:
	mov	r5, r0
	mov	r0, r4
	bl	_ZN4_STL6localeD1Ev
	b	.L594
.L620:
	.align	2
.L619:
	.word	_ZN4_STL5ctypeIcE2idE
	.word	_ZN4_STL8numpunctIcE2idE
	.word	.LC0
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA5703:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5703-.LLSDACSB5703
.LLSDACSB5703:
	.uleb128 .LEHB10-.LFB5703
	.uleb128 .LEHE10-.LEHB10
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB11-.LFB5703
	.uleb128 .LEHE11-.LEHB11
	.uleb128 .L600-.LFB5703
	.uleb128 0x0
	.uleb128 .LEHB12-.LFB5703
	.uleb128 .LEHE12-.LEHB12
	.uleb128 .L598-.LFB5703
	.uleb128 0x0
	.uleb128 .LEHB13-.LFB5703
	.uleb128 .LEHE13-.LEHB13
	.uleb128 .L600-.LFB5703
	.uleb128 0x0
	.uleb128 .LEHB14-.LFB5703
	.uleb128 .LEHE14-.LEHB14
	.uleb128 .L599-.LFB5703
	.uleb128 0x0
	.uleb128 .LEHB15-.LFB5703
	.uleb128 .LEHE15-.LEHB15
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE5703:
	.fnend
	.size	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEE5imbueERKNS_6localeE, .-_ZN4_STL9basic_iosIcNS_11char_traitsIcEEE5imbueERKNS_6localeE
	.section	.text._ZN4_STL9basic_iosIcNS_11char_traitsIcEEE4initEPNS_15basic_streambufIcS2_EE,"axG",%progbits,_ZN4_STL9basic_iosIcNS_11char_traitsIcEEE4initEPNS_15basic_streambufIcS2_EE,comdat
	.align	2
	.weak	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEE4initEPNS_15basic_streambufIcS2_EE
	.hidden	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEE4initEPNS_15basic_streambufIcS2_EE
	.type	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEE4initEPNS_15basic_streambufIcS2_EE, %function
_ZN4_STL9basic_iosIcNS_11char_traitsIcEEE4initEPNS_15basic_streambufIcS2_EE:
	.fnstart
.LFB5387:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	.save {r4, r5, r6, r7, lr}
	cmp	r1, #0
	mov	r4, r0
	movne	r3, #0
	.pad #12
	sub	sp, sp, #12
	mov	r7, r1
	str	r1, [r4, #88]
	strne	r3, [r0, #8]
	beq	.L629
.L623:
	add	r6, sp, #4
	mov	r0, r6
.LEHB16:
	bl	_ZN4_STL6localeC1Ev
.LEHE16:
	mov	r0, sp
	mov	r1, r4
	mov	r2, r6
	mov	r5, sp
.LEHB17:
	bl	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEE5imbueERKNS_6localeE
	mov	r0, sp
	bl	_ZN4_STL6localeD1Ev
.LEHE17:
	mov	r0, r6
.LEHB18:
	bl	_ZN4_STL6localeD1Ev
	mov	r2, #4096
	mov	r0, #0
	add	r1, r2, #8
	mov	r3, #6
	rsbs	r7, r7, #1
	movcc	r7, #0
	str	r1, [r4, #4]
	str	r0, [r4, #28]
	str	r0, [r4, #92]
	str	r0, [r4, #20]
	ldr	ip, [r4, #64]
	str	r7, [r4, #8]
	str	r3, [r4, #24]
	mov	r0, ip
	ldr	r2, [ip, #0]
	mov	r1, #32
	ldr	ip, [r2, #24]
	mov	lr, pc
	bx	ip
	strb	r0, [r4, #84]
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L629:
	ldr	r1, [r0, #20]
	mov	r3, #1
	tst	r1, #1
	str	r3, [r0, #8]
	beq	.L623
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
.LEHE18:
	b	.L623
.L627:
.L625:
	mov	r4, r0
	mov	r0, r6
	bl	_ZN4_STL6localeD1Ev
	mov	r0, r4
.LEHB19:
	bl	__cxa_end_cleanup
.LEHE19:
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA5387:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5387-.LLSDACSB5387
.LLSDACSB5387:
	.uleb128 .LEHB16-.LFB5387
	.uleb128 .LEHE16-.LEHB16
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB17-.LFB5387
	.uleb128 .LEHE17-.LEHB17
	.uleb128 .L627-.LFB5387
	.uleb128 0x0
	.uleb128 .LEHB18-.LFB5387
	.uleb128 .LEHE18-.LEHB18
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB19-.LFB5387
	.uleb128 .LEHE19-.LEHB19
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE5387:
	.fnend
	.size	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEE4initEPNS_15basic_streambufIcS2_EE, .-_ZN4_STL9basic_iosIcNS_11char_traitsIcEEE4initEPNS_15basic_streambufIcS2_EE
	.section	.text._ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i,"axG",%progbits,_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i,comdat
	.align	2
	.weak	_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i
	.hidden	_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i
	.type	_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i, %function
_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i:
	.fnstart
.LFB5114:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	.save {r4, r5, r6, r7, lr}
	ldr	r5, [r0, #0]
	cmp	r5, #0
	.pad #28
	sub	sp, sp, #28
	mov	r6, r0
	mov	r4, r1
	mov	r7, r2
	beq	.L631
	ldr	r3, .L666
	ldr	r1, [r3, #0]
	ldr	ip, [r1, #4]
	cmp	ip, #0
	moveq	ip, r1
	beq	.L635
	mov	r2, r1
	b	.L636
.L659:
	mov	r2, ip
	mov	ip, r3
.L636:
	ldr	r3, [ip, #16]
	cmp	r5, r3
	ldrhi	r3, [ip, #12]
	ldrls	r3, [ip, #8]
	movhi	ip, r2
	cmp	r3, #0
	bne	.L659
.L635:
	cmp	ip, r1
	beq	.L638
	ldr	r0, [ip, #16]
	cmp	r5, r0
	mov	r3, ip
	bcs	.L639
.L638:
	add	r3, sp, #4
	str	ip, [sp, #20]
	add	r0, sp, #16
	mov	ip, #0
	ldr	r1, .L666
	add	r2, sp, #20
	stmib	sp, {r5, ip}	@ phole stm
	bl	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS4_NS_16_Nonconst_traitsIS4_EEEERKS4_
	ldr	r3, [sp, #16]
.L639:
	ldr	r1, [r3, #20]
	sub	r2, r1, #1
	cmp	r2, #0
	str	r2, [r3, #20]
	beq	.L660
.L631:
	cmp	r4, #0
	stmia	r6, {r4, r7}	@ phole stm
	beq	.L657
	ldr	r5, .L666
	str	r4, [sp, #12]
	ldr	r0, [r5, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	beq	.L644
	mov	r1, r0
	b	.L648
.L661:
	mov	r1, r3
	mov	r3, r2
.L648:
	ldr	r2, [r3, #16]
	cmp	r4, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L661
	cmp	r0, r3
	beq	.L644
	ldr	r1, [r3, #16]
	cmp	r4, r1
	bcc	.L644
.L649:
	cmp	r3, r0
	beq	.L662
.L651:
	ldr	r0, [r3, #20]
	add	ip, r0, #1
	str	ip, [r3, #20]
.L657:
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L644:
	mov	r3, r0
	b	.L649
.L660:
	ldr	r0, [r6, #0]
	bl	_ZN21QuStupidPointerHelper6removeEPv
	ldr	r3, [r6, #4]
	cmp	r3, #1
	ldr	r3, [r6, #0]
	beq	.L663
	cmp	r3, #0
	beq	.L631
	ldr	r0, [r3, #-4]
	add	lr, r0, r0, asl #1
	add	r0, r3, lr, asl #4
	cmp	r3, r0
	bne	.L658
	b	.L641
.L664:
	mov	r0, r5
.L658:
	sub	r5, r0, #48
	ldr	r2, [r0, #-48]
	mov	r0, r5
	ldr	ip, [r2, #0]
	mov	lr, pc
	bx	ip
	ldr	r0, [r6, #0]
	cmp	r0, r5
	bne	.L664
.L641:
	sub	r0, r0, #8
	bl	_ZdaPv
	b	.L631
.L663:
	cmp	r3, #0
	beq	.L631
	mov	r0, r3
	ldr	ip, [r3, #0]
	ldr	ip, [ip, #4]
	mov	lr, pc
	bx	ip
	b	.L631
.L662:
	add	r0, sp, #12
	bl	T.1801
	mov	r3, #0
	str	r3, [r0, #0]
	ldr	r3, [r5, #0]
	ldr	r2, [r3, #4]
	cmp	r2, #0
	beq	.L651
	ldr	ip, [sp, #12]
	mov	r0, r3
	b	.L656
.L665:
	mov	r0, r2
	mov	r2, r1
.L656:
	ldr	r1, [r2, #16]
	cmp	ip, r1
	ldrhi	r1, [r2, #12]
	ldrls	r1, [r2, #8]
	movhi	r2, r0
	cmp	r1, #0
	bne	.L665
	cmp	r3, r2
	beq	.L651
	ldr	lr, [r2, #16]
	cmp	ip, lr
	movcs	r3, r2
	b	.L651
.L667:
	.align	2
.L666:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i, .-_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i
	.section	.text._ZN4_STL4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructEED1Ev,"axG",%progbits,_ZN4_STL4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructEED1Ev,comdat
	.align	2
	.weak	_ZN4_STL4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructEED1Ev
	.hidden	_ZN4_STL4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructEED1Ev
	.type	_ZN4_STL4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructEED1Ev, %function
_ZN4_STL4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructEED1Ev:
	.fnstart
.LFB4648:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	mov	r4, r0
	mov	r1, #0
	add	r0, r0, #12
	ldr	r2, [r4, #16]
.LEHB20:
	bl	_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i
.LEHE20:
	ldr	r0, [r4, #0]
	cmp	r0, #0
	blne	free
.L670:
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L673:
.L671:
	mov	r5, r0
	mov	r0, r4
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r5
.LEHB21:
	bl	__cxa_end_cleanup
.LEHE21:
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA4648:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4648-.LLSDACSB4648
.LLSDACSB4648:
	.uleb128 .LEHB20-.LFB4648
	.uleb128 .LEHE20-.LEHB20
	.uleb128 .L673-.LFB4648
	.uleb128 0x0
	.uleb128 .LEHB21-.LFB4648
	.uleb128 .LEHE21-.LEHB21
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE4648:
	.fnend
	.size	_ZN4_STL4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructEED1Ev, .-_ZN4_STL4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructEED1Ev
	.section	.text._ZN15QuStupidPointerI23QuSoundManagerInterfaceE3setEPS0_i,"axG",%progbits,_ZN15QuStupidPointerI23QuSoundManagerInterfaceE3setEPS0_i,comdat
	.align	2
	.weak	_ZN15QuStupidPointerI23QuSoundManagerInterfaceE3setEPS0_i
	.hidden	_ZN15QuStupidPointerI23QuSoundManagerInterfaceE3setEPS0_i
	.type	_ZN15QuStupidPointerI23QuSoundManagerInterfaceE3setEPS0_i, %function
_ZN15QuStupidPointerI23QuSoundManagerInterfaceE3setEPS0_i:
	.fnstart
.LFB5290:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	.save {r4, r5, r6, r7, lr}
	ldr	r5, [r0, #0]
	cmp	r5, #0
	.pad #28
	sub	sp, sp, #28
	mov	r6, r0
	mov	r4, r1
	mov	r7, r2
	beq	.L675
	ldr	r3, .L710
	ldr	r1, [r3, #0]
	ldr	ip, [r1, #4]
	cmp	ip, #0
	moveq	ip, r1
	beq	.L679
	mov	r2, r1
	b	.L680
.L703:
	mov	r2, ip
	mov	ip, r3
.L680:
	ldr	r3, [ip, #16]
	cmp	r5, r3
	ldrhi	r3, [ip, #12]
	ldrls	r3, [ip, #8]
	movhi	ip, r2
	cmp	r3, #0
	bne	.L703
.L679:
	cmp	ip, r1
	beq	.L682
	ldr	r0, [ip, #16]
	cmp	r5, r0
	mov	r3, ip
	bcs	.L683
.L682:
	add	r3, sp, #4
	str	ip, [sp, #20]
	add	r0, sp, #16
	mov	ip, #0
	ldr	r1, .L710
	add	r2, sp, #20
	stmib	sp, {r5, ip}	@ phole stm
	bl	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS4_NS_16_Nonconst_traitsIS4_EEEERKS4_
	ldr	r3, [sp, #16]
.L683:
	ldr	r1, [r3, #20]
	sub	r2, r1, #1
	cmp	r2, #0
	str	r2, [r3, #20]
	beq	.L704
.L675:
	cmp	r4, #0
	stmia	r6, {r4, r7}	@ phole stm
	beq	.L701
	ldr	r5, .L710
	str	r4, [sp, #12]
	ldr	r0, [r5, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	beq	.L688
	mov	r1, r0
	b	.L692
.L705:
	mov	r1, r3
	mov	r3, r2
.L692:
	ldr	r2, [r3, #16]
	cmp	r4, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L705
	cmp	r0, r3
	beq	.L688
	ldr	r1, [r3, #16]
	cmp	r4, r1
	bcc	.L688
.L693:
	cmp	r3, r0
	beq	.L706
.L695:
	ldr	r2, [r3, #20]
	add	ip, r2, #1
	str	ip, [r3, #20]
.L701:
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L688:
	mov	r3, r0
	b	.L693
.L704:
	ldr	r0, [r6, #0]
	bl	_ZN21QuStupidPointerHelper6removeEPv
	ldr	r3, [r6, #4]
	cmp	r3, #1
	ldr	r3, [r6, #0]
	beq	.L707
	cmp	r3, #0
	beq	.L675
	ldr	lr, [r3, #-4]
	add	r0, r3, lr, asl #2
	cmp	r3, r0
	bne	.L702
	b	.L685
.L708:
	mov	r0, r5
.L702:
	sub	r5, r0, #4
	ldr	r2, [r0, #-4]
	mov	r0, r5
	ldr	ip, [r2, #0]
	mov	lr, pc
	bx	ip
	ldr	r0, [r6, #0]
	cmp	r0, r5
	bne	.L708
.L685:
	sub	r0, r0, #8
	bl	_ZdaPv
	b	.L675
.L707:
	cmp	r3, #0
	beq	.L675
	mov	r0, r3
	ldr	ip, [r3, #0]
	ldr	ip, [ip, #4]
	mov	lr, pc
	bx	ip
	b	.L675
.L706:
	add	r0, sp, #12
	bl	T.1801
	mov	r3, #0
	str	r3, [r0, #0]
	ldr	r3, [r5, #0]
	ldr	r2, [r3, #4]
	cmp	r2, #0
	beq	.L695
	ldr	ip, [sp, #12]
	mov	r0, r3
	b	.L700
.L709:
	mov	r0, r2
	mov	r2, r1
.L700:
	ldr	r1, [r2, #16]
	cmp	ip, r1
	ldrhi	r1, [r2, #12]
	ldrls	r1, [r2, #8]
	movhi	r2, r0
	cmp	r1, #0
	bne	.L709
	cmp	r3, r2
	beq	.L695
	ldr	r0, [r2, #16]
	cmp	ip, r0
	movcs	r3, r2
	b	.L695
.L711:
	.align	2
.L710:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	_ZN15QuStupidPointerI23QuSoundManagerInterfaceE3setEPS0_i, .-_ZN15QuStupidPointerI23QuSoundManagerInterfaceE3setEPS0_i
	.section	.text._ZN15QuStupidPointerI10QuPngImageE3setEPS0_i,"axG",%progbits,_ZN15QuStupidPointerI10QuPngImageE3setEPS0_i,comdat
	.align	2
	.weak	_ZN15QuStupidPointerI10QuPngImageE3setEPS0_i
	.hidden	_ZN15QuStupidPointerI10QuPngImageE3setEPS0_i
	.type	_ZN15QuStupidPointerI10QuPngImageE3setEPS0_i, %function
_ZN15QuStupidPointerI10QuPngImageE3setEPS0_i:
	.fnstart
.LFB5221:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	.save {r4, r5, r6, r7, lr}
	ldr	r5, [r0, #0]
	cmp	r5, #0
	.pad #28
	sub	sp, sp, #28
	mov	r6, r0
	mov	r4, r1
	mov	r7, r2
	beq	.L713
	ldr	r3, .L748
	ldr	r1, [r3, #0]
	ldr	ip, [r1, #4]
	cmp	ip, #0
	moveq	ip, r1
	beq	.L717
	mov	r2, r1
	b	.L718
.L741:
	mov	r2, ip
	mov	ip, r3
.L718:
	ldr	r3, [ip, #16]
	cmp	r5, r3
	ldrhi	r3, [ip, #12]
	ldrls	r3, [ip, #8]
	movhi	ip, r2
	cmp	r3, #0
	bne	.L741
.L717:
	cmp	ip, r1
	beq	.L720
	ldr	r0, [ip, #16]
	cmp	r5, r0
	mov	r3, ip
	bcs	.L721
.L720:
	add	r3, sp, #4
	str	ip, [sp, #20]
	add	r0, sp, #16
	mov	ip, #0
	ldr	r1, .L748
	add	r2, sp, #20
	stmib	sp, {r5, ip}	@ phole stm
	bl	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS4_NS_16_Nonconst_traitsIS4_EEEERKS4_
	ldr	r3, [sp, #16]
.L721:
	ldr	r1, [r3, #20]
	sub	r2, r1, #1
	cmp	r2, #0
	str	r2, [r3, #20]
	beq	.L742
.L713:
	cmp	r4, #0
	stmia	r6, {r4, r7}	@ phole stm
	beq	.L739
	ldr	r5, .L748
	str	r4, [sp, #12]
	ldr	r0, [r5, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	beq	.L726
	mov	r1, r0
	b	.L730
.L743:
	mov	r1, r3
	mov	r3, r2
.L730:
	ldr	r2, [r3, #16]
	cmp	r4, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L743
	cmp	r0, r3
	beq	.L726
	ldr	r1, [r3, #16]
	cmp	r4, r1
	bcc	.L726
.L731:
	cmp	r3, r0
	beq	.L744
.L733:
	ldr	r0, [r3, #20]
	add	ip, r0, #1
	str	ip, [r3, #20]
.L739:
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L726:
	mov	r3, r0
	b	.L731
.L742:
	ldr	r0, [r6, #0]
	bl	_ZN21QuStupidPointerHelper6removeEPv
	ldr	r3, [r6, #4]
	cmp	r3, #1
	ldr	r3, [r6, #0]
	beq	.L745
	cmp	r3, #0
	beq	.L713
	ldr	r0, [r3, #-4]
	add	lr, r0, r0, asl #1
	add	r0, r3, lr, asl #4
	cmp	r3, r0
	bne	.L740
	b	.L723
.L746:
	mov	r0, r5
.L740:
	sub	r5, r0, #48
	ldr	r2, [r0, #-48]
	mov	r0, r5
	ldr	ip, [r2, #4]
	mov	lr, pc
	bx	ip
	ldr	r0, [r6, #0]
	cmp	r0, r5
	bne	.L746
.L723:
	sub	r0, r0, #8
	bl	_ZdaPv
	b	.L713
.L745:
	cmp	r3, #0
	beq	.L713
	mov	r0, r3
	ldr	ip, [r3, #0]
	ldr	ip, [ip, #8]
	mov	lr, pc
	bx	ip
	b	.L713
.L744:
	add	r0, sp, #12
	bl	T.1801
	mov	r3, #0
	str	r3, [r0, #0]
	ldr	r3, [r5, #0]
	ldr	r2, [r3, #4]
	cmp	r2, #0
	beq	.L733
	ldr	ip, [sp, #12]
	mov	r0, r3
	b	.L738
.L747:
	mov	r0, r2
	mov	r2, r1
.L738:
	ldr	r1, [r2, #16]
	cmp	r1, ip
	ldrcc	r1, [r2, #12]
	ldrcs	r1, [r2, #8]
	movcc	r2, r0
	cmp	r1, #0
	bne	.L747
	cmp	r3, r2
	beq	.L733
	ldr	lr, [r2, #16]
	cmp	lr, ip
	movls	r3, r2
	b	.L733
.L749:
	.align	2
.L748:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	_ZN15QuStupidPointerI10QuPngImageE3setEPS0_i, .-_ZN15QuStupidPointerI10QuPngImageE3setEPS0_i
	.section	.text._ZN15QuStupidPointerI22QuSoundStructInterfaceE3setEPS0_i,"axG",%progbits,_ZN15QuStupidPointerI22QuSoundStructInterfaceE3setEPS0_i,comdat
	.align	2
	.weak	_ZN15QuStupidPointerI22QuSoundStructInterfaceE3setEPS0_i
	.hidden	_ZN15QuStupidPointerI22QuSoundStructInterfaceE3setEPS0_i
	.type	_ZN15QuStupidPointerI22QuSoundStructInterfaceE3setEPS0_i, %function
_ZN15QuStupidPointerI22QuSoundStructInterfaceE3setEPS0_i:
	.fnstart
.LFB5124:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	.save {r4, r5, r6, r7, lr}
	ldr	r5, [r0, #0]
	cmp	r5, #0
	.pad #28
	sub	sp, sp, #28
	mov	r6, r0
	mov	r4, r1
	mov	r7, r2
	beq	.L751
	ldr	r3, .L786
	ldr	r1, [r3, #0]
	ldr	ip, [r1, #4]
	cmp	ip, #0
	moveq	ip, r1
	beq	.L755
	mov	r2, r1
	b	.L756
.L779:
	mov	r2, ip
	mov	ip, r3
.L756:
	ldr	r3, [ip, #16]
	cmp	r5, r3
	ldrhi	r3, [ip, #12]
	ldrls	r3, [ip, #8]
	movhi	ip, r2
	cmp	r3, #0
	bne	.L779
.L755:
	cmp	ip, r1
	beq	.L758
	ldr	r0, [ip, #16]
	cmp	r5, r0
	mov	r3, ip
	bcs	.L759
.L758:
	add	r3, sp, #4
	str	ip, [sp, #20]
	add	r0, sp, #16
	mov	ip, #0
	ldr	r1, .L786
	add	r2, sp, #20
	stmib	sp, {r5, ip}	@ phole stm
	bl	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS4_NS_16_Nonconst_traitsIS4_EEEERKS4_
	ldr	r3, [sp, #16]
.L759:
	ldr	r1, [r3, #20]
	sub	r2, r1, #1
	cmp	r2, #0
	str	r2, [r3, #20]
	beq	.L780
.L751:
	cmp	r4, #0
	stmia	r6, {r4, r7}	@ phole stm
	beq	.L777
	ldr	r5, .L786
	str	r4, [sp, #12]
	ldr	r0, [r5, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	beq	.L764
	mov	r1, r0
	b	.L768
.L781:
	mov	r1, r3
	mov	r3, r2
.L768:
	ldr	r2, [r3, #16]
	cmp	r4, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L781
	cmp	r0, r3
	beq	.L764
	ldr	r1, [r3, #16]
	cmp	r4, r1
	bcc	.L764
.L769:
	cmp	r3, r0
	beq	.L782
.L771:
	ldr	r2, [r3, #20]
	add	ip, r2, #1
	str	ip, [r3, #20]
.L777:
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L764:
	mov	r3, r0
	b	.L769
.L780:
	ldr	r0, [r6, #0]
	bl	_ZN21QuStupidPointerHelper6removeEPv
	ldr	r3, [r6, #4]
	cmp	r3, #1
	ldr	r3, [r6, #0]
	beq	.L783
	cmp	r3, #0
	beq	.L751
	ldr	lr, [r3, #-4]
	add	r0, r3, lr, asl #5
	cmp	r3, r0
	bne	.L778
	b	.L761
.L784:
	mov	r0, r5
.L778:
	sub	r5, r0, #32
	ldr	r2, [r0, #-32]
	mov	r0, r5
	ldr	ip, [r2, #0]
	mov	lr, pc
	bx	ip
	ldr	r0, [r6, #0]
	cmp	r0, r5
	bne	.L784
.L761:
	sub	r0, r0, #8
	bl	_ZdaPv
	b	.L751
.L783:
	cmp	r3, #0
	beq	.L751
	mov	r0, r3
	ldr	ip, [r3, #0]
	ldr	ip, [ip, #4]
	mov	lr, pc
	bx	ip
	b	.L751
.L782:
	add	r0, sp, #12
	bl	T.1801
	mov	r3, #0
	str	r3, [r0, #0]
	ldr	r3, [r5, #0]
	ldr	r2, [r3, #4]
	cmp	r2, #0
	beq	.L771
	ldr	ip, [sp, #12]
	mov	r0, r3
	b	.L776
.L785:
	mov	r0, r2
	mov	r2, r1
.L776:
	ldr	r1, [r2, #16]
	cmp	ip, r1
	ldrhi	r1, [r2, #12]
	ldrls	r1, [r2, #8]
	movhi	r2, r0
	cmp	r1, #0
	bne	.L785
	cmp	r3, r2
	beq	.L771
	ldr	r0, [r2, #16]
	cmp	ip, r0
	movcs	r3, r2
	b	.L771
.L787:
	.align	2
.L786:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	_ZN15QuStupidPointerI22QuSoundStructInterfaceE3setEPS0_i, .-_ZN15QuStupidPointerI22QuSoundStructInterfaceE3setEPS0_i
	.section	.text._ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i,"axG",%progbits,_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i,comdat
	.align	2
	.weak	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
	.hidden	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
	.type	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i, %function
_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i:
	.fnstart
.LFB5153:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	.save {r4, r5, r6, r7, lr}
	ldr	r5, [r0, #0]
	cmp	r5, #0
	.pad #28
	sub	sp, sp, #28
	mov	r6, r0
	mov	r4, r1
	mov	r7, r2
	beq	.L789
	ldr	r3, .L824
	ldr	r1, [r3, #0]
	ldr	ip, [r1, #4]
	cmp	ip, #0
	moveq	ip, r1
	beq	.L793
	mov	r2, r1
	b	.L794
.L817:
	mov	r2, ip
	mov	ip, r3
.L794:
	ldr	r3, [ip, #16]
	cmp	r5, r3
	ldrhi	r3, [ip, #12]
	ldrls	r3, [ip, #8]
	movhi	ip, r2
	cmp	r3, #0
	bne	.L817
.L793:
	cmp	ip, r1
	beq	.L796
	ldr	r0, [ip, #16]
	cmp	r5, r0
	mov	r3, ip
	bcs	.L797
.L796:
	add	r3, sp, #4
	str	ip, [sp, #20]
	add	r0, sp, #16
	mov	ip, #0
	ldr	r1, .L824
	add	r2, sp, #20
	stmib	sp, {r5, ip}	@ phole stm
	bl	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS4_NS_16_Nonconst_traitsIS4_EEEERKS4_
	ldr	r3, [sp, #16]
.L797:
	ldr	r1, [r3, #20]
	sub	r2, r1, #1
	cmp	r2, #0
	str	r2, [r3, #20]
	beq	.L818
.L789:
	cmp	r4, #0
	stmia	r6, {r4, r7}	@ phole stm
	beq	.L815
	ldr	r5, .L824
	str	r4, [sp, #12]
	ldr	r0, [r5, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	beq	.L802
	mov	r1, r0
	b	.L806
.L819:
	mov	r1, r3
	mov	r3, r2
.L806:
	ldr	r2, [r3, #16]
	cmp	r4, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L819
	cmp	r0, r3
	beq	.L802
	ldr	r1, [r3, #16]
	cmp	r4, r1
	bcc	.L802
.L807:
	cmp	r3, r0
	beq	.L820
.L809:
	ldr	r0, [r3, #20]
	add	ip, r0, #1
	str	ip, [r3, #20]
.L815:
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L802:
	mov	r3, r0
	b	.L807
.L818:
	ldr	r0, [r6, #0]
	bl	_ZN21QuStupidPointerHelper6removeEPv
	ldr	r3, [r6, #4]
	cmp	r3, #1
	ldr	r3, [r6, #0]
	beq	.L821
	cmp	r3, #0
	beq	.L789
	ldr	r0, [r3, #-4]
	rsb	lr, r0, r0, asl #3
	add	r0, r3, lr, asl #2
	cmp	r3, r0
	bne	.L816
	b	.L799
.L822:
	mov	r0, r5
.L816:
	sub	r5, r0, #28
	ldr	r2, [r0, #-28]
	mov	r0, r5
	ldr	ip, [r2, #4]
	mov	lr, pc
	bx	ip
	ldr	r0, [r6, #0]
	cmp	r0, r5
	bne	.L822
.L799:
	sub	r0, r0, #8
	bl	_ZdaPv
	b	.L789
.L821:
	cmp	r3, #0
	beq	.L789
	mov	r0, r3
	ldr	ip, [r3, #0]
	ldr	ip, [ip, #8]
	mov	lr, pc
	bx	ip
	b	.L789
.L820:
	add	r0, sp, #12
	bl	T.1801
	mov	r3, #0
	str	r3, [r0, #0]
	ldr	r3, [r5, #0]
	ldr	r2, [r3, #4]
	cmp	r2, #0
	beq	.L809
	ldr	ip, [sp, #12]
	mov	r0, r3
	b	.L814
.L823:
	mov	r0, r2
	mov	r2, r1
.L814:
	ldr	r1, [r2, #16]
	cmp	r1, ip
	ldrcc	r1, [r2, #12]
	ldrcs	r1, [r2, #8]
	movcc	r2, r0
	cmp	r1, #0
	bne	.L823
	cmp	r3, r2
	beq	.L809
	ldr	lr, [r2, #16]
	cmp	lr, ip
	movls	r3, r2
	b	.L809
.L825:
	.align	2
.L824:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i, .-_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
	.section	.text._ZN12QuDrawObjectC2Ev,"axG",%progbits,_ZN12QuDrawObjectC2Ev,comdat
	.align	2
	.weak	_ZN12QuDrawObjectC2Ev
	.hidden	_ZN12QuDrawObjectC2Ev
	.type	_ZN12QuDrawObjectC2Ev, %function
_ZN12QuDrawObjectC2Ev:
	.fnstart
.LFB2950:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r2, .L846
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	mov	r1, #0
	mov	r4, r0
	mov	r3, #4
	str	r2, [r4, #0]
	str	r1, [r0, #124]!
	str	r3, [r4, #96]
	strb	r1, [r4, #8]
	str	r3, [r4, #12]
	strb	r1, [r4, #36]
	str	r3, [r4, #40]
	strb	r1, [r4, #64]
	str	r3, [r4, #68]
	strb	r1, [r4, #92]
	mov	r2, #1
.LEHB22:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE22:
	mov	r0, #0
	mov	r1, #5
	strb	r0, [r4, #120]
	str	r0, [r4, #4]
	str	r1, [r4, #132]
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L842:
.L833:
.L843:
.L835:
.L844:
.L837:
.L845:
.L839:
	mov	r5, r0
	add	r0, r4, #92
	bl	_ZN14QuArrayPointerIfE6deInitEv
	add	r0, r4, #64
	bl	_ZN14QuArrayPointerIfE6deInitEv
	add	r0, r4, #36
	bl	_ZN14QuArrayPointerIfE6deInitEv
	add	r0, r4, #8
	bl	_ZN14QuArrayPointerIfE6deInitEv
	mov	r0, r5
.LEHB23:
	bl	__cxa_end_cleanup
.LEHE23:
.L847:
	.align	2
.L846:
	.word	_ZTV12QuDrawObject+8
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA2950:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2950-.LLSDACSB2950
.LLSDACSB2950:
	.uleb128 .LEHB22-.LFB2950
	.uleb128 .LEHE22-.LEHB22
	.uleb128 .L842-.LFB2950
	.uleb128 0x0
	.uleb128 .LEHB23-.LFB2950
	.uleb128 .LEHE23-.LEHB23
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE2950:
	.fnend
	.size	_ZN12QuDrawObjectC2Ev, .-_ZN12QuDrawObjectC2Ev
	.section	.text._ZN4_STL4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageEED1Ev,"axG",%progbits,_ZN4_STL4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageEED1Ev,comdat
	.align	2
	.weak	_ZN4_STL4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageEED1Ev
	.hidden	_ZN4_STL4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageEED1Ev
	.type	_ZN4_STL4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageEED1Ev, %function
_ZN4_STL4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageEED1Ev:
	.fnstart
.LFB4763:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	mov	r4, r0
	mov	r1, #0
	add	r0, r0, #12
	ldr	r2, [r4, #16]
.LEHB24:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE24:
	ldr	r0, [r4, #0]
	cmp	r0, #0
	blne	free
.L851:
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L854:
.L852:
	mov	r5, r0
	mov	r0, r4
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r5
.LEHB25:
	bl	__cxa_end_cleanup
.LEHE25:
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA4763:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4763-.LLSDACSB4763
.LLSDACSB4763:
	.uleb128 .LEHB24-.LFB4763
	.uleb128 .LEHE24-.LEHB24
	.uleb128 .L854-.LFB4763
	.uleb128 0x0
	.uleb128 .LEHB25-.LFB4763
	.uleb128 .LEHE25-.LEHB25
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE4763:
	.fnend
	.size	_ZN4_STL4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageEED1Ev, .-_ZN4_STL4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageEED1Ev
	.section	.text._ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE,"axG",%progbits,_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE,comdat
	.align	2
	.weak	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
	.hidden	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
	.type	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE, %function
_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE:
	.fnstart
.LFB5546:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	subs	r5, r1, #0
	.pad #20
	sub	sp, sp, #20
	mov	r9, r0
	beq	.L883
	ldr	fp, .L894
	mov	sl, #0
.L887:
	mov	r0, r9
	ldr	r1, [r5, #12]
.LEHB26:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE26:
	ldr	r4, [r5, #28]
	cmp	r4, #0
	ldr	r8, [r5, #8]
	ldr	r7, [r5, #32]
	beq	.L857
	ldr	r1, [fp, #0]
	ldr	ip, [r1, #4]
	cmp	ip, #0
	moveq	ip, r1
	beq	.L861
	mov	r2, r1
	b	.L862
.L890:
	mov	r2, ip
	mov	ip, r3
.L862:
	ldr	r3, [ip, #16]
	cmp	r4, r3
	ldrhi	r3, [ip, #12]
	ldrls	r3, [ip, #8]
	movhi	ip, r2
	cmp	r3, #0
	bne	.L890
.L861:
	cmp	ip, r1
	beq	.L864
	ldr	r3, [ip, #16]
	cmp	r4, r3
	mov	r3, ip
	bcs	.L865
.L864:
	add	r0, sp, #8
	ldr	r1, .L894
	add	r2, sp, #12
	mov	r3, sp
	str	ip, [sp, #12]
	stmia	sp, {r4, sl}	@ phole stm
.LEHB27:
	bl	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS4_NS_16_Nonconst_traitsIS4_EEEERKS4_
	ldr	r3, [sp, #8]
.L865:
	ldr	r0, [r3, #20]
	sub	r2, r0, #1
	cmp	r2, #0
	str	r2, [r3, #20]
	beq	.L891
.L857:
	ldr	r0, [r5, #16]
	cmp	r0, #0
	str	r7, [r5, #32]
	str	sl, [r5, #28]
	blne	free
.L878:
	cmp	r5, #0
	movne	r0, r5
	blne	free
.L880:
	subs	r5, r8, #0
	bne	.L887
.L883:
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L891:
	ldr	r0, [r5, #28]
	bl	_ZN21QuStupidPointerHelper6removeEPv
.LEHE27:
	ldr	r1, [r5, #32]
	cmp	r1, #1
	beq	.L892
	ldr	r0, [r5, #28]
	cmp	r0, #0
	beq	.L857
	ldr	ip, [r0, #-4]
	add	r4, ip, ip, asl #2
	add	r4, r0, r4, asl #2
	b	.L871
.L893:
	sub	r0, r4, #8
	mov	r1, #0
	ldr	r2, [r4, #-4]
	sub	r6, r4, #20
.LEHB28:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE28:
	ldr	r0, [r4, #-20]
	cmp	r0, #0
	blne	free
.L875:
	ldr	r0, [r5, #28]
	mov	r4, r6
.L871:
	cmp	r0, r4
	bne	.L893
	sub	r0, r0, #8
	bl	_ZdaPv
	b	.L857
.L892:
	ldr	r4, [r5, #28]
	cmp	r4, #0
	beq	.L857
	add	r0, r4, #12
	mov	r1, #0
	ldr	r2, [r4, #16]
.LEHB29:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE29:
	ldr	r0, [r4, #0]
	cmp	r0, #0
	blne	free
.L869:
	mov	r0, r4
	bl	_ZdlPv
	b	.L857
.L886:
	mov	r7, r0
.L881:
	add	r0, r5, #16
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r7
.LEHB30:
	bl	__cxa_end_cleanup
.LEHE30:
.L885:
.L870:
	mov	r7, r0
	mov	r0, r4
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	b	.L881
.L884:
.L876:
	mov	r7, r0
	mov	r0, r6
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	b	.L881
.L895:
	.align	2
.L894:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA5546:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5546-.LLSDACSB5546
.LLSDACSB5546:
	.uleb128 .LEHB26-.LFB5546
	.uleb128 .LEHE26-.LEHB26
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB27-.LFB5546
	.uleb128 .LEHE27-.LEHB27
	.uleb128 .L886-.LFB5546
	.uleb128 0x0
	.uleb128 .LEHB28-.LFB5546
	.uleb128 .LEHE28-.LEHB28
	.uleb128 .L884-.LFB5546
	.uleb128 0x0
	.uleb128 .LEHB29-.LFB5546
	.uleb128 .LEHE29-.LEHB29
	.uleb128 .L885-.LFB5546
	.uleb128 0x0
	.uleb128 .LEHB30-.LFB5546
	.uleb128 .LEHE30-.LEHB30
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE5546:
	.fnend
	.size	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE, .-_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
	.section	.text._ZN15QuStupidPointerI13QuBaseManagerE3setEPS0_i,"axG",%progbits,_ZN15QuStupidPointerI13QuBaseManagerE3setEPS0_i,comdat
	.align	2
	.weak	_ZN15QuStupidPointerI13QuBaseManagerE3setEPS0_i
	.hidden	_ZN15QuStupidPointerI13QuBaseManagerE3setEPS0_i
	.type	_ZN15QuStupidPointerI13QuBaseManagerE3setEPS0_i, %function
_ZN15QuStupidPointerI13QuBaseManagerE3setEPS0_i:
	.fnstart
.LFB5291:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	.save {r4, r5, r6, r7, lr}
	ldr	r5, [r0, #0]
	cmp	r5, #0
	.pad #28
	sub	sp, sp, #28
	mov	r6, r0
	mov	r4, r1
	mov	r7, r2
	beq	.L897
	ldr	r3, .L932
	ldr	r1, [r3, #0]
	ldr	ip, [r1, #4]
	cmp	ip, #0
	moveq	ip, r1
	beq	.L901
	mov	r2, r1
	b	.L902
.L925:
	mov	r2, ip
	mov	ip, r3
.L902:
	ldr	r3, [ip, #16]
	cmp	r5, r3
	ldrhi	r3, [ip, #12]
	ldrls	r3, [ip, #8]
	movhi	ip, r2
	cmp	r3, #0
	bne	.L925
.L901:
	cmp	ip, r1
	beq	.L904
	ldr	r0, [ip, #16]
	cmp	r5, r0
	mov	r3, ip
	bcs	.L905
.L904:
	add	r3, sp, #4
	str	ip, [sp, #20]
	add	r0, sp, #16
	mov	ip, #0
	ldr	r1, .L932
	add	r2, sp, #20
	stmib	sp, {r5, ip}	@ phole stm
	bl	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS4_NS_16_Nonconst_traitsIS4_EEEERKS4_
	ldr	r3, [sp, #16]
.L905:
	ldr	r1, [r3, #20]
	sub	r2, r1, #1
	cmp	r2, #0
	str	r2, [r3, #20]
	beq	.L926
.L897:
	cmp	r4, #0
	stmia	r6, {r4, r7}	@ phole stm
	beq	.L923
	ldr	r5, .L932
	str	r4, [sp, #12]
	ldr	r0, [r5, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	beq	.L910
	mov	r1, r0
	b	.L914
.L927:
	mov	r1, r3
	mov	r3, r2
.L914:
	ldr	r2, [r3, #16]
	cmp	r4, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L927
	cmp	r0, r3
	beq	.L910
	ldr	r1, [r3, #16]
	cmp	r4, r1
	bcc	.L910
.L915:
	cmp	r3, r0
	beq	.L928
.L917:
	ldr	r2, [r3, #20]
	add	ip, r2, #1
	str	ip, [r3, #20]
.L923:
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L910:
	mov	r3, r0
	b	.L915
.L926:
	ldr	r0, [r6, #0]
	bl	_ZN21QuStupidPointerHelper6removeEPv
	ldr	r3, [r6, #4]
	cmp	r3, #1
	ldr	r3, [r6, #0]
	beq	.L929
	cmp	r3, #0
	beq	.L897
	ldr	lr, [r3, #-4]
	add	r0, r3, lr, asl #2
	cmp	r3, r0
	bne	.L924
	b	.L907
.L930:
	mov	r0, r5
.L924:
	sub	r5, r0, #4
	ldr	r2, [r0, #-4]
	mov	r0, r5
	ldr	ip, [r2, #0]
	mov	lr, pc
	bx	ip
	ldr	r0, [r6, #0]
	cmp	r0, r5
	bne	.L930
.L907:
	sub	r0, r0, #8
	bl	_ZdaPv
	b	.L897
.L929:
	cmp	r3, #0
	beq	.L897
	mov	r0, r3
	ldr	ip, [r3, #0]
	ldr	ip, [ip, #4]
	mov	lr, pc
	bx	ip
	b	.L897
.L928:
	add	r0, sp, #12
	bl	T.1801
	mov	r3, #0
	str	r3, [r0, #0]
	ldr	r3, [r5, #0]
	ldr	r2, [r3, #4]
	cmp	r2, #0
	beq	.L917
	ldr	ip, [sp, #12]
	mov	r0, r3
	b	.L922
.L931:
	mov	r0, r2
	mov	r2, r1
.L922:
	ldr	r1, [r2, #16]
	cmp	ip, r1
	ldrhi	r1, [r2, #12]
	ldrls	r1, [r2, #8]
	movhi	r2, r0
	cmp	r1, #0
	bne	.L931
	cmp	r3, r2
	beq	.L917
	ldr	r0, [r2, #16]
	cmp	ip, r0
	movcs	r3, r2
	b	.L917
.L933:
	.align	2
.L932:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	_ZN15QuStupidPointerI13QuBaseManagerE3setEPS0_i, .-_ZN15QuStupidPointerI13QuBaseManagerE3setEPS0_i
	.section	.text._ZN15QuStupidPointerI13QuBaseManagerED1Ev,"axG",%progbits,_ZN15QuStupidPointerI13QuBaseManagerED1Ev,comdat
	.align	2
	.weak	_ZN15QuStupidPointerI13QuBaseManagerED1Ev
	.hidden	_ZN15QuStupidPointerI13QuBaseManagerED1Ev
	.type	_ZN15QuStupidPointerI13QuBaseManagerED1Ev, %function
_ZN15QuStupidPointerI13QuBaseManagerED1Ev:
	.fnstart
.LFB4894:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r1, #0
	mov	r4, r0
	ldr	r2, [r0, #4]
	bl	_ZN15QuStupidPointerI13QuBaseManagerE3setEPS0_i
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
	.fnend
	.size	_ZN15QuStupidPointerI13QuBaseManagerED1Ev, .-_ZN15QuStupidPointerI13QuBaseManagerED1Ev
	.section	.text._ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7reserveEj,"axG",%progbits,_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7reserveEj,comdat
	.align	2
	.weak	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7reserveEj
	.hidden	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7reserveEj
	.type	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7reserveEj, %function
_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7reserveEj:
	.fnstart
.LFB6154:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, lr}
	.save {r3, r4, r5, r6, r7, lr}
	ldr	ip, [r0, #8]
	ldr	r2, [r0, #0]
	mov	r4, r0
	sub	r0, ip, #1
	rsb	r3, r2, r0
	cmp	r1, r3
	bcc	.L945
	cmn	r1, #1
	beq	.L946
	add	r6, r1, #1
	mov	r0, r6
	bl	malloc
	subs	r5, r0, #0
	beq	.L947
.L941:
	ldr	r1, [r4, #0]
.L940:
	ldr	r7, [r4, #4]
	cmp	r7, r1
	moveq	r7, r5
	beq	.L943
	rsb	r7, r1, r7
	mov	r2, r7
	mov	r0, r5
	bl	memmove
	add	r7, r0, r7
.L943:
	mov	r1, #0
	strb	r1, [r7, #0]
	ldr	r0, [r4, #0]
	cmp	r0, r1
	blne	free
.L944:
	add	r6, r5, r6
	str	r6, [r4, #8]
	stmia	r4, {r5, r7}	@ phole stm
.L945:
	ldmfd	sp!, {r3, r4, r5, r6, r7, lr}
	bx	lr
.L946:
	ldr	r0, .L948
	mov	r6, #0
	bl	_ZN4_STL24__stl_throw_length_errorEPKc
	mov	r5, r6
	ldr	r1, [r4, #0]
	b	.L940
.L947:
	mov	r0, r6
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
	mov	r5, r0
	b	.L941
.L949:
	.align	2
.L948:
	.word	.LC0
	.fnend
	.size	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7reserveEj, .-_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7reserveEj
	.section	.text._ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi,"axG",%progbits,_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi,comdat
	.align	2
	.weak	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi
	.hidden	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi
	.type	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi, %function
_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi:
	.fnstart
.LFB6309:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmn	r1, #1
	stmfd	sp!, {r3, r4, r5, r6, r7, lr}
	.save {r3, r4, r5, r6, r7, lr}
	mov	r4, r1
	mov	r5, r0
	moveq	r4, #0
	bne	.L961
.L952:
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, r6, r7, lr}
	bx	lr
.L961:
	ldr	r3, [r0, #36]
	tst	r3, #16
	beq	.L953
	tst	r3, #8
	ldr	r2, [r0, #24]
	ldr	r3, [r0, #20]
	beq	.L962
	cmp	r3, r2
	beq	.L963
.L956:
	strb	r4, [r3, #0]
	ldr	ip, [r5, #20]
	add	r0, ip, #1
	str	r0, [r5, #20]
	b	.L952
.L963:
	ldr	r2, [r0, #44]
	ldr	r1, [r0, #48]
	add	r3, r2, #1
	cmp	r3, r1
	ldmib	r0, {r6, r7}	@ phole ldm
	beq	.L964
.L957:
	mov	r1, #0
	strb	r1, [r3, #0]
	ldr	r2, [r5, #44]
	strb	r4, [r2, #0]
	add	r2, r5, #40
	ldmia	r2, {r2, r3}	@ phole ldm
	rsb	ip, r6, r7
	add	r1, r3, #1
	add	r0, r2, ip
	str	r1, [r5, #20]
	str	r0, [r5, #8]
	str	r2, [r5, #16]
	str	r1, [r5, #44]
	str	r2, [r5, #4]
	str	r1, [r5, #12]
	str	r1, [r5, #24]
	b	.L952
.L962:
	cmp	r3, r2
	bne	.L956
	bl	_ZNK4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE16_M_append_bufferEv
	ldr	r3, [r5, #20]
	ldr	r2, [r5, #24]
	cmp	r3, r2
	bne	.L956
.L953:
	mvn	r4, #0
	b	.L952
.L964:
	ldr	lr, [r0, #40]!
	cmp	r2, lr
	rsbne	r1, lr, r2
	moveq	r1, #1
	rsb	ip, lr, r2
	add	r1, r1, ip
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7reserveEj
	ldr	r3, [r5, #44]
	add	r3, r3, #1
	b	.L957
	.fnend
	.size	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi, .-_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi
	.section	.text._ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendEjc,"axG",%progbits,_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendEjc,comdat
	.align	2
	.weak	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendEjc
	.hidden	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendEjc
	.type	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendEjc, %function
_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendEjc:
	.fnstart
.LFB6334:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmn	r1, #1
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	mov	r5, r1
	mov	r4, r0
	mov	r6, r2
	beq	.L966
	rsb	r2, r1, #-16777216
	add	ip, r2, #16711680
	ldr	r1, [r0, #4]
	ldr	r2, [r0, #0]
	add	r0, ip, #65280
	rsb	r1, r2, r1
	add	r3, r0, #254
	cmp	r3, r1
	bcc	.L966
.L967:
	ldr	r0, [r4, #8]
	sub	r3, r0, #1
	rsb	r0, r2, r3
	add	ip, r1, r5
	cmp	ip, r0
	bls	.L968
	cmp	r1, r5
	addcs	r1, r1, r1
	addcc	r1, r1, r5
	mov	r0, r4
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE7reserveEj
.L968:
	cmp	r5, #0
	beq	.L969
	ldr	r1, [r4, #4]
	add	lr, r5, r1
	add	r0, r1, #1
	rsb	r2, r0, lr
	mov	r1, r6
	bl	memset
	ldr	r3, [r4, #4]
	mov	ip, #0
	strb	ip, [r3, r5]
	ldr	r2, [r4, #4]
	strb	r6, [r2, #0]
	ldr	r1, [r4, #4]
	add	r5, r1, r5
	str	r5, [r4, #4]
.L969:
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L966:
	ldr	r0, .L971
	bl	_ZN4_STL24__stl_throw_length_errorEPKc
	ldmia	r4, {r2, r3}	@ phole ldm
	rsb	r1, r2, r3
	b	.L967
.L972:
	.align	2
.L971:
	.word	.LC0
	.fnend
	.size	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendEjc, .-_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendEjc
	.section	.text._ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE6xsputnEPKci,"axG",%progbits,_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE6xsputnEPKci,comdat
	.align	2
	.weak	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE6xsputnEPKci
	.hidden	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE6xsputnEPKci
	.type	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE6xsputnEPKci, %function
_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE6xsputnEPKci:
	.fnstart
.LFB6307:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, lr}
	.save {r3, r4, r5, r6, r7, r8, sl, lr}
	ldr	ip, [r0, #36]
	mov	r3, ip, lsr #4
	cmp	r2, #0
	movle	r3, #0
	andgt	r3, r3, #1
	cmp	r3, #0
	mov	r5, r2
	mov	r4, r0
	mov	r6, r1
	moveq	r5, r3
	beq	.L975
	ldr	r7, [r0, #16]
	ldr	r0, [r0, #40]
	cmp	r7, r0
	movne	r8, #0
	beq	.L990
.L977:
	tst	ip, #8
	beq	.L981
	add	r2, r6, r5
	mov	r1, r6
	add	r0, r4, #40
	ldmib	r4, {r3, r6}
	sub	r6, r6, r3
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendIPKcEERS5_T_SA_
	add	r3, r4, #40
	ldmia	r3, {r3, ip}	@ phole ldm
	add	r6, r3, r6
	str	ip, [r4, #20]
	str	r6, [r4, #8]
	str	r3, [r4, #16]
	str	r3, [r4, #4]
	str	ip, [r4, #12]
	str	ip, [r4, #24]
	add	r5, r5, r8
.L975:
	mov	r0, r5
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, lr}
	bx	lr
.L981:
	add	r3, r4, #52
	cmp	r3, r7
	ldrne	r7, [r4, #20]
	beq	.L991
.L985:
	ldr	lr, [r4, #24]
	cmp	lr, r7
	beq	.L987
.L989:
	add	sl, r4, #40
.L986:
	add	r2, r6, r5
	mov	r1, r6
	mov	r0, sl
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendIPKcEERS5_T_SA_
	add	r5, r5, r8
	b	.L975
.L990:
	ldr	r0, [r4, #20]
	ldr	r8, [r4, #44]
	rsb	r8, r0, r8
	cmp	r5, r8
	bge	.L978
	cmp	r5, #0
	beq	.L979
	bl	memcpy
	ldr	r0, [r4, #20]
.L979:
	add	r1, r0, r5
	str	r1, [r4, #20]
	b	.L975
.L987:
	add	sl, r3, #8
	str	sl, [r4, #24]
	str	r3, [r4, #16]
	str	r3, [r4, #20]
	b	.L989
.L978:
	cmp	r8, #0
	bne	.L992
.L980:
	add	r7, r4, #52
	add	r2, r4, #60
	str	r2, [r4, #24]
	str	r7, [r4, #16]
	str	r7, [r4, #20]
	rsb	r5, r8, r5
	add	r6, r6, r8
	b	.L977
.L991:
	ldr	r2, [r4, #20]
	cmp	r2, r7
	beq	.L985
	add	sl, r4, #40
	mov	r0, sl
	mov	r1, r7
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendIPKcEERS5_T_SA_
	add	r0, r4, #60
	str	r0, [r4, #24]
	str	r7, [r4, #20]
	str	r7, [r4, #16]
	b	.L986
.L992:
	mov	r2, r8
	bl	memcpy
	ldr	ip, [r4, #36]
	b	.L980
	.fnend
	.size	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE6xsputnEPKci, .-_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE6xsputnEPKci
	.section	.text._ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE10_M_xsputncEci,"axG",%progbits,_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE10_M_xsputncEci,comdat
	.align	2
	.weak	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE10_M_xsputncEci
	.hidden	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE10_M_xsputncEci
	.type	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE10_M_xsputncEci, %function
_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE10_M_xsputncEci:
	.fnstart
.LFB6308:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, lr}
	.save {r3, r4, r5, r6, r7, r8, sl, lr}
	ldr	r3, [r0, #36]
	mov	r4, r0
	mov	r0, r3, lsr #4
	cmp	r2, #0
	movle	r0, #0
	andgt	r0, r0, #1
	cmp	r0, #0
	mov	r5, r2
	mov	r7, r1
	moveq	r5, r0
	beq	.L995
	ldr	r6, [r4, #16]
	ldr	ip, [r4, #40]
	cmp	r6, ip
	movne	r8, #0
	beq	.L1007
.L997:
	tst	r3, #8
	beq	.L999
	mov	r1, r5
	mov	r2, r7
	add	r0, r4, #40
	ldmib	r4, {r3, r6}
	sub	r6, r6, r3
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendEjc
	ldr	ip, [r4, #40]
	ldr	r0, [r4, #44]
	add	r6, ip, r6
	str	r0, [r4, #20]
	str	r6, [r4, #8]
	str	ip, [r4, #16]
	str	ip, [r4, #4]
	str	r0, [r4, #12]
	str	r0, [r4, #24]
	add	r5, r5, r8
.L995:
	mov	r0, r5
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, lr}
	bx	lr
.L999:
	add	r3, r4, #52
	cmp	r6, r3
	ldrne	r6, [r4, #20]
	beq	.L1008
.L1002:
	ldr	lr, [r4, #24]
	cmp	r6, lr
	beq	.L1004
.L1006:
	add	sl, r4, #40
.L1003:
	mov	r1, r5
	mov	r0, sl
	mov	r2, r7
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendEjc
	add	r5, r5, r8
	b	.L995
.L1007:
	ldr	r0, [r4, #20]
	ldr	r8, [r4, #44]
	rsb	r8, r0, r8
	cmp	r5, r8
	blt	.L1009
	mov	r2, r8
	bl	memset
	add	r6, r4, #52
	add	r2, r4, #60
	str	r2, [r4, #24]
	str	r6, [r4, #16]
	str	r6, [r4, #20]
	ldr	r3, [r4, #36]
	rsb	r5, r8, r5
	b	.L997
.L1004:
	add	sl, r3, #8
	str	sl, [r4, #24]
	str	r3, [r4, #16]
	str	r3, [r4, #20]
	b	.L1006
.L1009:
	bl	memset
	ldr	r1, [r4, #20]
	add	r3, r1, r5
	str	r3, [r4, #20]
	b	.L995
.L1008:
	ldr	r2, [r4, #20]
	cmp	r6, r2
	beq	.L1002
	add	sl, r4, #40
	mov	r0, sl
	mov	r1, r6
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendIPKcEERS5_T_SA_
	add	r3, r4, #60
	str	r3, [r4, #24]
	str	r6, [r4, #20]
	str	r6, [r4, #16]
	b	.L1003
	.fnend
	.size	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE10_M_xsputncEci, .-_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE10_M_xsputncEci
	.section	.text.T.1796,"ax",%progbits
	.align	2
	.type	T.1796, %function
T.1796:
	.fnstart
.LFB6346:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	.save {r4, r5, r6, r7, r8, lr}
	ldr	r4, .L1040
	.pad #8
	sub	sp, sp, #8
	str	r4, [sp, #0]
	ldr	r2, [r4, #0]
	ldr	r3, [r2, #-12]
	add	r3, r4, r3
	ldr	r1, [r3, #8]
	cmp	r1, #0
	mov	r7, r0
	mov	r6, r4
	sub	r8, r2, #12
	movne	r3, #0
	bne	.L1012
	ldr	r0, [r3, #88]
	cmp	r0, #0
	beq	.L1035
.L1013:
	ldr	r5, [r3, #92]
	cmp	r5, #0
	beq	.L1014
	ldr	r1, [r5, #0]
	ldr	r2, [r1, #-12]
	add	ip, r5, r2
	ldr	r2, [ip, #88]
	cmp	r2, #0
	beq	.L1014
	mov	r0, r2
	ldr	r3, [r2, #0]
.LEHB31:
	ldr	ip, [r3, #20]
	mov	lr, pc
	bx	ip
.LEHE31:
	cmn	r0, #1
	beq	.L1015
.L1033:
	ldr	r1, [r4, #0]
	ldr	r3, [r1, #-12]
	sub	r8, r1, #12
	add	r3, r6, r3
.L1014:
	ldr	ip, [r3, #8]
	rsbs	r3, ip, #1
	movcc	r3, #0
.L1012:
	cmp	r3, #0
	strb	r3, [sp, #4]
	beq	.L1019
	mov	r0, r7
	bl	strlen
	ldr	r3, [r8, #0]
	add	r2, r6, r3
	ldr	r8, [r2, #28]
	cmp	r0, r8
	mov	r5, r0
	bge	.L1020
	subs	r8, r8, r0
	beq	.L1020
	ldr	lr, [r2, #4]
	and	r3, lr, #7
	cmp	r3, #1
	ldr	r3, [r2, #88]
	beq	.L1036
	ldrb	r1, [r2, #84]	@ zero_extendqisi2
	mov	r0, r3
	mov	r2, r8
	ldr	r3, [r3, #0]
.LEHB32:
	ldr	ip, [r3, #48]
	mov	lr, pc
	bx	ip
	cmp	r8, r0
	beq	.L1037
.L1024:
	ldr	lr, [r4, #0]
	ldr	r2, [lr, #-12]
	mov	r7, #0
	add	r6, r6, r2
	str	r7, [r6, #28]
	ldr	r7, .L1040
.L1030:
	ldr	r2, [r4, #0]
	ldr	r0, [r2, #-12]
	add	r0, r7, r0
	ldr	r1, [r0, #88]
	ldr	lr, [r0, #8]
	cmp	r1, #0
	ldr	ip, [r0, #20]
	orr	r4, lr, #4
	orreq	r4, lr, #5
	tst	r4, ip
	str	r4, [r0, #8]
	bne	.L1038
.L1019:
	ldr	r4, [sp, #0]
	ldr	r1, [r4, #0]
	ldr	r3, [r1, #-12]
	add	r3, r4, r3
	ldr	ip, [r3, #4]
	tst	ip, #8192
	bne	.L1039
.L1031:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L1020:
	add	r0, r4, r3
	ldr	lr, [r0, #88]
	mov	r1, r7
	mov	r0, lr
	ldr	r3, [lr, #0]
	mov	r2, r5
	ldr	r7, .L1040
	ldr	ip, [r3, #44]
	mov	lr, pc
	bx	ip
.LEHE32:
	ldr	ip, [r4, #0]
	ldr	r1, [ip, #-12]
	mov	r2, #0
	add	r6, r6, r1
	cmp	r5, r0
	str	r2, [r6, #28]
	bne	.L1030
	ldr	r4, [sp, #0]
	ldr	r1, [r4, #0]
	ldr	r3, [r1, #-12]
	add	r3, r4, r3
	ldr	ip, [r3, #4]
	tst	ip, #8192
	beq	.L1031
.L1039:
	ldr	r3, [r3, #88]
	cmp	r3, #0
	beq	.L1031
	mov	r0, r3
	ldr	r2, [r3, #0]
.LEHB33:
	ldr	ip, [r2, #20]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	bne	.L1031
	ldr	r1, [r4, #0]
	ldr	r0, [r1, #-12]
	add	r0, r4, r0
	ldr	r3, [r0, #8]
	ldr	ip, [r0, #20]
	orr	lr, r3, #1
	tst	lr, ip
	str	lr, [r0, #8]
	beq	.L1031
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
.LEHE33:
	b	.L1031
.L1038:
.LEHB34:
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
	b	.L1019
.L1036:
	mov	r1, r7
	mov	r0, r3
	ldr	ip, [r3, #0]
	mov	r2, r5
	ldr	ip, [ip, #44]
	mov	lr, pc
	bx	ip
	cmp	r5, r0
	bne	.L1024
	ldr	ip, [r4, #0]
	ldr	r2, .L1040
	ldr	r1, [ip, #-12]
	add	r0, r2, r1
	ldr	lr, [r0, #88]
	ldrb	r1, [r0, #84]	@ zero_extendqisi2
	ldr	r3, [lr, #0]
	mov	r0, lr
	mov	r2, r8
	ldr	ip, [r3, #48]
	mov	lr, pc
	bx	ip
	cmp	r8, r0
	bne	.L1024
.L1025:
	ldr	r3, [r4, #0]
	ldr	ip, [r3, #-12]
	mov	r4, #0
	add	r6, r6, ip
	str	r4, [r6, #28]
	b	.L1019
.L1037:
	ldr	r3, [r4, #0]
	ldr	r2, .L1040
	ldr	r1, [r3, #-12]
	add	r0, r2, r1
	ldr	lr, [r0, #88]
	mov	r1, r7
	mov	r0, lr
	ldr	ip, [lr, #0]
	mov	r2, r5
	ldr	ip, [ip, #44]
	mov	lr, pc
	bx	ip
.LEHE34:
	cmp	r5, r0
	bne	.L1024
	b	.L1025
.L1015:
	ldr	r2, [r5, #0]
	ldr	r0, [r2, #-12]
	add	r0, r5, r0
	ldr	ip, [r0, #8]
	ldr	r8, [r0, #20]
	orr	lr, ip, #1
	tst	lr, r8
	str	lr, [r0, #8]
	beq	.L1033
.LEHB35:
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
	b	.L1033
.L1035:
	mov	r0, r3
	mov	r1, #1
	bl	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEE8setstateEi
.LEHE35:
	ldr	r8, [r4, #0]
	ldr	r5, [r8, #-12]
	sub	r8, r8, #12
	add	r3, r4, r5
	b	.L1013
.L1032:
.L1027:
	mov	r6, r0
	mov	r0, sp
	bl	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	mov	r0, r6
.LEHB36:
	bl	__cxa_end_cleanup
.LEHE36:
.L1041:
	.align	2
.L1040:
	.word	_ZN4_STL4coutE
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA6346:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6346-.LLSDACSB6346
.LLSDACSB6346:
	.uleb128 .LEHB31-.LFB6346
	.uleb128 .LEHE31-.LEHB31
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB32-.LFB6346
	.uleb128 .LEHE32-.LEHB32
	.uleb128 .L1032-.LFB6346
	.uleb128 0x0
	.uleb128 .LEHB33-.LFB6346
	.uleb128 .LEHE33-.LEHB33
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB34-.LFB6346
	.uleb128 .LEHE34-.LEHB34
	.uleb128 .L1032-.LFB6346
	.uleb128 0x0
	.uleb128 .LEHB35-.LFB6346
	.uleb128 .LEHE35-.LEHB35
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB36-.LFB6346
	.uleb128 .LEHE36-.LEHB36
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE6346:
	.fnend
	.size	T.1796, .-T.1796
	.section	.text._ZN12QuDrawObjectD0Ev,"axG",%progbits,_ZN12QuDrawObjectD0Ev,comdat
	.align	2
	.weak	_ZN12QuDrawObjectD0Ev
	.hidden	_ZN12QuDrawObjectD0Ev
	.type	_ZN12QuDrawObjectD0Ev, %function
_ZN12QuDrawObjectD0Ev:
	.fnstart
.LFB2955:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	ldr	r3, .L1067
	mov	r4, r0
	mov	r1, #0
	str	r3, [r0], #124
	ldr	r2, [r4, #128]
.LEHB37:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE37:
	ldrb	r0, [r4, #92]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L1044
	ldr	r0, [r4, #100]
	cmp	r0, #0
	blne	_ZdaPv
.L1048:
	mov	r1, #0
	strb	r1, [r4, #92]
	str	r1, [r4, #100]
.L1044:
	ldrb	r2, [r4, #64]	@ zero_extendqisi2
	cmp	r2, #0
	beq	.L1049
	ldr	r0, [r4, #72]
	cmp	r0, #0
	blne	_ZdaPv
.L1050:
	mov	ip, #0
	strb	ip, [r4, #64]
	str	ip, [r4, #72]
.L1049:
	ldrb	lr, [r4, #36]	@ zero_extendqisi2
	cmp	lr, #0
	beq	.L1053
	ldr	r0, [r4, #44]
	cmp	r0, #0
	blne	_ZdaPv
.L1054:
	mov	r3, #0
	strb	r3, [r4, #36]
	str	r3, [r4, #44]
.L1053:
	ldrb	r0, [r4, #8]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L1057
	ldr	r0, [r4, #16]
	cmp	r0, #0
	blne	_ZdaPv
.L1058:
	mov	r1, #0
	strb	r1, [r4, #8]
	str	r1, [r4, #16]
.L1057:
	mov	r0, r4
	bl	_ZdlPv
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L1062:
.L1046:
.L1063:
.L1051:
.L1064:
.L1055:
.L1065:
.L1059:
	mov	r5, r0
	add	r0, r4, #92
	bl	_ZN14QuArrayPointerIfE6deInitEv
	add	r0, r4, #64
	bl	_ZN14QuArrayPointerIfE6deInitEv
	add	r0, r4, #36
	bl	_ZN14QuArrayPointerIfE6deInitEv
	add	r0, r4, #8
	bl	_ZN14QuArrayPointerIfE6deInitEv
	mov	r0, r5
.LEHB38:
	bl	__cxa_end_cleanup
.LEHE38:
.L1068:
	.align	2
.L1067:
	.word	_ZTV12QuDrawObject+8
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA2955:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2955-.LLSDACSB2955
.LLSDACSB2955:
	.uleb128 .LEHB37-.LFB2955
	.uleb128 .LEHE37-.LEHB37
	.uleb128 .L1062-.LFB2955
	.uleb128 0x0
	.uleb128 .LEHB38-.LFB2955
	.uleb128 .LEHE38-.LEHB38
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE2955:
	.fnend
	.size	_ZN12QuDrawObjectD0Ev, .-_ZN12QuDrawObjectD0Ev
	.section	.text._ZN12QuDrawObjectD1Ev,"axG",%progbits,_ZN12QuDrawObjectD1Ev,comdat
	.align	2
	.weak	_ZN12QuDrawObjectD1Ev
	.hidden	_ZN12QuDrawObjectD1Ev
	.type	_ZN12QuDrawObjectD1Ev, %function
_ZN12QuDrawObjectD1Ev:
	.fnstart
.LFB2954:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	ldr	r3, .L1094
	mov	r4, r0
	mov	r1, #0
	str	r3, [r0], #124
	ldr	r2, [r4, #128]
.LEHB39:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE39:
	ldrb	r0, [r4, #92]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L1071
	ldr	r0, [r4, #100]
	cmp	r0, #0
	blne	_ZdaPv
.L1075:
	mov	r1, #0
	strb	r1, [r4, #92]
	str	r1, [r4, #100]
.L1071:
	ldrb	r2, [r4, #64]	@ zero_extendqisi2
	cmp	r2, #0
	beq	.L1076
	ldr	r0, [r4, #72]
	cmp	r0, #0
	blne	_ZdaPv
.L1077:
	mov	ip, #0
	strb	ip, [r4, #64]
	str	ip, [r4, #72]
.L1076:
	ldrb	lr, [r4, #36]	@ zero_extendqisi2
	cmp	lr, #0
	beq	.L1080
	ldr	r0, [r4, #44]
	cmp	r0, #0
	blne	_ZdaPv
.L1081:
	mov	r3, #0
	strb	r3, [r4, #36]
	str	r3, [r4, #44]
.L1080:
	ldrb	r0, [r4, #8]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L1084
	ldr	r0, [r4, #16]
	cmp	r0, #0
	blne	_ZdaPv
.L1085:
	mov	r1, #0
	strb	r1, [r4, #8]
	str	r1, [r4, #16]
.L1084:
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L1089:
.L1073:
.L1090:
.L1078:
.L1091:
.L1082:
.L1092:
.L1086:
	mov	r5, r0
	add	r0, r4, #92
	bl	_ZN14QuArrayPointerIfE6deInitEv
	add	r0, r4, #64
	bl	_ZN14QuArrayPointerIfE6deInitEv
	add	r0, r4, #36
	bl	_ZN14QuArrayPointerIfE6deInitEv
	add	r0, r4, #8
	bl	_ZN14QuArrayPointerIfE6deInitEv
	mov	r0, r5
.LEHB40:
	bl	__cxa_end_cleanup
.LEHE40:
.L1095:
	.align	2
.L1094:
	.word	_ZTV12QuDrawObject+8
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA2954:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2954-.LLSDACSB2954
.LLSDACSB2954:
	.uleb128 .LEHB39-.LFB2954
	.uleb128 .LEHE39-.LEHB39
	.uleb128 .L1089-.LFB2954
	.uleb128 0x0
	.uleb128 .LEHB40-.LFB2954
	.uleb128 .LEHE40-.LEHB40
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE2954:
	.fnend
	.size	_ZN12QuDrawObjectD1Ev, .-_ZN12QuDrawObjectD1Ev
	.section	.text._ZN12QuDrawObjectD2Ev,"axG",%progbits,_ZN12QuDrawObjectD2Ev,comdat
	.align	2
	.weak	_ZN12QuDrawObjectD2Ev
	.hidden	_ZN12QuDrawObjectD2Ev
	.type	_ZN12QuDrawObjectD2Ev, %function
_ZN12QuDrawObjectD2Ev:
	.fnstart
.LFB2953:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	ldr	r3, .L1121
	mov	r4, r0
	mov	r1, #0
	str	r3, [r0], #124
	ldr	r2, [r4, #128]
.LEHB41:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE41:
	ldrb	r0, [r4, #92]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L1098
	ldr	r0, [r4, #100]
	cmp	r0, #0
	blne	_ZdaPv
.L1102:
	mov	r1, #0
	strb	r1, [r4, #92]
	str	r1, [r4, #100]
.L1098:
	ldrb	r2, [r4, #64]	@ zero_extendqisi2
	cmp	r2, #0
	beq	.L1103
	ldr	r0, [r4, #72]
	cmp	r0, #0
	blne	_ZdaPv
.L1104:
	mov	ip, #0
	strb	ip, [r4, #64]
	str	ip, [r4, #72]
.L1103:
	ldrb	lr, [r4, #36]	@ zero_extendqisi2
	cmp	lr, #0
	beq	.L1107
	ldr	r0, [r4, #44]
	cmp	r0, #0
	blne	_ZdaPv
.L1108:
	mov	r3, #0
	strb	r3, [r4, #36]
	str	r3, [r4, #44]
.L1107:
	ldrb	r0, [r4, #8]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L1111
	ldr	r0, [r4, #16]
	cmp	r0, #0
	blne	_ZdaPv
.L1112:
	mov	r1, #0
	strb	r1, [r4, #8]
	str	r1, [r4, #16]
.L1111:
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L1116:
.L1100:
.L1117:
.L1105:
.L1118:
.L1109:
.L1119:
.L1113:
	mov	r5, r0
	add	r0, r4, #92
	bl	_ZN14QuArrayPointerIfE6deInitEv
	add	r0, r4, #64
	bl	_ZN14QuArrayPointerIfE6deInitEv
	add	r0, r4, #36
	bl	_ZN14QuArrayPointerIfE6deInitEv
	add	r0, r4, #8
	bl	_ZN14QuArrayPointerIfE6deInitEv
	mov	r0, r5
.LEHB42:
	bl	__cxa_end_cleanup
.LEHE42:
.L1122:
	.align	2
.L1121:
	.word	_ZTV12QuDrawObject+8
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA2953:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2953-.LLSDACSB2953
.LLSDACSB2953:
	.uleb128 .LEHB41-.LFB2953
	.uleb128 .LEHE41-.LEHB41
	.uleb128 .L1116-.LFB2953
	.uleb128 0x0
	.uleb128 .LEHB42-.LFB2953
	.uleb128 .LEHE42-.LEHB42
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE2953:
	.fnend
	.size	_ZN12QuDrawObjectD2Ev, .-_ZN12QuDrawObjectD2Ev
	.section	.text._ZN17QuImageDrawObjectD0Ev,"axG",%progbits,_ZN17QuImageDrawObjectD0Ev,comdat
	.align	2
	.weak	_ZN17QuImageDrawObjectD0Ev
	.hidden	_ZN17QuImageDrawObjectD0Ev
	.type	_ZN17QuImageDrawObjectD0Ev, %function
_ZN17QuImageDrawObjectD0Ev:
	.fnstart
.LFB2983:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L1125
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	str	r3, [r0, #0]
	bl	_ZN12QuDrawObjectD2Ev
	mov	r0, r4
	bl	_ZdlPv
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L1126:
	.align	2
.L1125:
	.word	_ZTV17QuImageDrawObject+8
	.fnend
	.size	_ZN17QuImageDrawObjectD0Ev, .-_ZN17QuImageDrawObjectD0Ev
	.section	.text._ZN21QuRectangleDrawObjectIiED0Ev,"axG",%progbits,_ZN21QuRectangleDrawObjectIiED0Ev,comdat
	.align	2
	.weak	_ZN21QuRectangleDrawObjectIiED0Ev
	.hidden	_ZN21QuRectangleDrawObjectIiED0Ev
	.type	_ZN21QuRectangleDrawObjectIiED0Ev, %function
_ZN21QuRectangleDrawObjectIiED0Ev:
	.fnstart
.LFB3847:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L1129
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	str	r3, [r0, #0]
	bl	_ZN12QuDrawObjectD2Ev
	mov	r0, r4
	bl	_ZdlPv
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L1130:
	.align	2
.L1129:
	.word	_ZTV21QuRectangleDrawObjectIiE+8
	.fnend
	.size	_ZN21QuRectangleDrawObjectIiED0Ev, .-_ZN21QuRectangleDrawObjectIiED0Ev
	.section	.text._ZN21QuRectangleDrawObjectIiED1Ev,"axG",%progbits,_ZN21QuRectangleDrawObjectIiED1Ev,comdat
	.align	2
	.weak	_ZN21QuRectangleDrawObjectIiED1Ev
	.hidden	_ZN21QuRectangleDrawObjectIiED1Ev
	.type	_ZN21QuRectangleDrawObjectIiED1Ev, %function
_ZN21QuRectangleDrawObjectIiED1Ev:
	.fnstart
.LFB3846:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L1133
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	str	r3, [r0, #0]
	bl	_ZN12QuDrawObjectD2Ev
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L1134:
	.align	2
.L1133:
	.word	_ZTV21QuRectangleDrawObjectIiE+8
	.fnend
	.size	_ZN21QuRectangleDrawObjectIiED1Ev, .-_ZN21QuRectangleDrawObjectIiED1Ev
	.section	.text._ZN17QuImageDrawObjectD1Ev,"axG",%progbits,_ZN17QuImageDrawObjectD1Ev,comdat
	.align	2
	.weak	_ZN17QuImageDrawObjectD1Ev
	.hidden	_ZN17QuImageDrawObjectD1Ev
	.type	_ZN17QuImageDrawObjectD1Ev, %function
_ZN17QuImageDrawObjectD1Ev:
	.fnstart
.LFB2982:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	ldr	r3, .L1137
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	str	r3, [r0, #0]
	bl	_ZN12QuDrawObjectD2Ev
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L1138:
	.align	2
.L1137:
	.word	_ZTV17QuImageDrawObject+8
	.fnend
	.size	_ZN17QuImageDrawObjectD1Ev, .-_ZN17QuImageDrawObjectD1Ev
	.section	.text._ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_,"axG",%progbits,_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_,comdat
	.align	2
	.weak	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_
	.hidden	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_
	.type	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_, %function
_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_:
	.fnstart
.LFB5859:
	@ Function supports interworking.
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, lr}
	mov	r4, r1
	ldr	r1, [r1, #0]
	cmp	r1, r3
	mov	r6, r3
	mov	r8, r0
	ldr	r7, [sp, #32]
	beq	.L1140
	ldr	r3, [sp, #36]
	cmp	r3, #0
	beq	.L1164
.L1141:
	mov	r0, #36
	bl	malloc
	subs	sl, r0, #0
	beq	.L1165
.L1154:
	add	r9, sl, #16
	mov	r0, r9
	mov	r1, r7
	mov	r5, sl
.LEHB43:
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
.LEHE43:
	mov	r0, sl
	mov	lr, #0
	str	lr, [r0, #28]!
	add	r1, r7, #12
	ldmia	r1, {r1, r2}	@ phole ldm
.LEHB44:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE44:
	str	sl, [r6, #12]
	ldr	r1, [r4, #0]
	ldr	r3, [r1, #12]
	cmp	r6, r3
	streq	sl, [r1, #12]
.L1147:
	mov	lr, #0
	str	r6, [r5, #4]
	str	lr, [r5, #12]
	str	lr, [r5, #8]
	ldr	ip, [r4, #0]
	mov	r0, r5
	add	r1, ip, #4
.LEHB45:
	bl	_ZN4_STL10_Rb_globalIbE10_RebalanceEPNS_18_Rb_tree_node_baseERS3_
.LEHE45:
	ldr	r0, [r4, #4]
	add	r2, r0, #1
	str	r2, [r4, #4]
	mov	r0, r8
	str	r5, [r8, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L1164:
	cmp	r2, #0
	beq	.L1166
.L1140:
	mov	r0, #36
	bl	malloc
	subs	sl, r0, #0
	beq	.L1167
.L1142:
	add	r9, sl, #16
	mov	r0, r9
	mov	r1, r7
	mov	r5, sl
.LEHB46:
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
.LEHE46:
	mov	r2, #0
	mov	r0, sl
	str	r2, [r0, #28]!
	add	r1, r7, #12
	ldmia	r1, {r1, r2}	@ phole ldm
.LEHB47:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE47:
.L1145:
	str	sl, [r6, #8]
	ldr	r3, [r4, #0]
	cmp	r3, r6
	beq	.L1168
	ldr	ip, [r3, #8]
	cmp	r6, ip
	streq	sl, [r3, #8]
	b	.L1147
.L1168:
	str	sl, [r6, #4]
	ldr	r0, [r4, #0]
	str	sl, [r0, #12]
	b	.L1147
.L1167:
	mov	r0, #36
.LEHB48:
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
	mov	sl, r0
	b	.L1142
.L1165:
	mov	r0, #36
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
	mov	sl, r0
	b	.L1154
.L1166:
	add	r0, r4, #8
	mov	r1, r7
	add	r2, r6, #16
	bl	_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_
	cmp	r0, #0
	beq	.L1141
	b	.L1140
.L1156:
.L1163:
.L1149:
	mov	r4, r0
	mov	r0, r9
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
.L1150:
	mov	r0, r4
	bl	__cxa_end_cleanup
.LEHE48:
.L1157:
.L1162:
	mov	r4, r0
	b	.L1150
.L1158:
	b	.L1163
.L1144:
.L1159:
	b	.L1162
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA5859:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5859-.LLSDACSB5859
.LLSDACSB5859:
	.uleb128 .LEHB43-.LFB5859
	.uleb128 .LEHE43-.LEHB43
	.uleb128 .L1157-.LFB5859
	.uleb128 0x0
	.uleb128 .LEHB44-.LFB5859
	.uleb128 .LEHE44-.LEHB44
	.uleb128 .L1156-.LFB5859
	.uleb128 0x0
	.uleb128 .LEHB45-.LFB5859
	.uleb128 .LEHE45-.LEHB45
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB46-.LFB5859
	.uleb128 .LEHE46-.LEHB46
	.uleb128 .L1159-.LFB5859
	.uleb128 0x0
	.uleb128 .LEHB47-.LFB5859
	.uleb128 .LEHE47-.LEHB47
	.uleb128 .L1158-.LFB5859
	.uleb128 0x0
	.uleb128 .LEHB48-.LFB5859
	.uleb128 .LEHE48-.LEHB48
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE5859:
	.fnend
	.size	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_, .-_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_
	.section	.text._ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_,"axG",%progbits,_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_,comdat
	.align	2
	.weak	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_
	.hidden	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_
	.type	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_, %function
_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_:
	.fnstart
.LFB5791:
	@ Function supports interworking.
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, lr}
	mov	r4, r1
	ldr	r1, [r1, #0]
	cmp	r1, r3
	mov	r6, r3
	mov	r8, r0
	ldr	r7, [sp, #32]
	beq	.L1170
	ldr	r3, [sp, #36]
	cmp	r3, #0
	beq	.L1194
.L1171:
	mov	r0, #36
	bl	malloc
	subs	sl, r0, #0
	beq	.L1195
.L1184:
	add	r9, sl, #16
	mov	r0, r9
	mov	r1, r7
	mov	r5, sl
.LEHB49:
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
.LEHE49:
	mov	r0, sl
	mov	lr, #0
	str	lr, [r0, #28]!
	add	r1, r7, #12
	ldmia	r1, {r1, r2}	@ phole ldm
.LEHB50:
	bl	_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i
.LEHE50:
	str	sl, [r6, #12]
	ldr	r1, [r4, #0]
	ldr	r3, [r1, #12]
	cmp	r6, r3
	streq	sl, [r1, #12]
.L1177:
	mov	lr, #0
	str	r6, [r5, #4]
	str	lr, [r5, #12]
	str	lr, [r5, #8]
	ldr	ip, [r4, #0]
	mov	r0, r5
	add	r1, ip, #4
.LEHB51:
	bl	_ZN4_STL10_Rb_globalIbE10_RebalanceEPNS_18_Rb_tree_node_baseERS3_
.LEHE51:
	ldr	r0, [r4, #4]
	add	r2, r0, #1
	str	r2, [r4, #4]
	mov	r0, r8
	str	r5, [r8, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L1194:
	cmp	r2, #0
	beq	.L1196
.L1170:
	mov	r0, #36
	bl	malloc
	subs	sl, r0, #0
	beq	.L1197
.L1172:
	add	r9, sl, #16
	mov	r0, r9
	mov	r1, r7
	mov	r5, sl
.LEHB52:
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
.LEHE52:
	mov	r2, #0
	mov	r0, sl
	str	r2, [r0, #28]!
	add	r1, r7, #12
	ldmia	r1, {r1, r2}	@ phole ldm
.LEHB53:
	bl	_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i
.LEHE53:
.L1175:
	str	sl, [r6, #8]
	ldr	r3, [r4, #0]
	cmp	r3, r6
	beq	.L1198
	ldr	ip, [r3, #8]
	cmp	r6, ip
	streq	sl, [r3, #8]
	b	.L1177
.L1198:
	str	sl, [r6, #4]
	ldr	r0, [r4, #0]
	str	sl, [r0, #12]
	b	.L1177
.L1197:
	mov	r0, #36
.LEHB54:
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
	mov	sl, r0
	b	.L1172
.L1195:
	mov	r0, #36
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
	mov	sl, r0
	b	.L1184
.L1196:
	add	r0, r4, #8
	mov	r1, r7
	add	r2, r6, #16
	bl	_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_
	cmp	r0, #0
	beq	.L1171
	b	.L1170
.L1186:
.L1193:
.L1179:
	mov	r4, r0
	mov	r0, r9
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
.L1180:
	mov	r0, r4
	bl	__cxa_end_cleanup
.LEHE54:
.L1187:
.L1192:
	mov	r4, r0
	b	.L1180
.L1188:
	b	.L1193
.L1174:
.L1189:
	b	.L1192
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA5791:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5791-.LLSDACSB5791
.LLSDACSB5791:
	.uleb128 .LEHB49-.LFB5791
	.uleb128 .LEHE49-.LEHB49
	.uleb128 .L1187-.LFB5791
	.uleb128 0x0
	.uleb128 .LEHB50-.LFB5791
	.uleb128 .LEHE50-.LEHB50
	.uleb128 .L1186-.LFB5791
	.uleb128 0x0
	.uleb128 .LEHB51-.LFB5791
	.uleb128 .LEHE51-.LEHB51
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB52-.LFB5791
	.uleb128 .LEHE52-.LEHB52
	.uleb128 .L1189-.LFB5791
	.uleb128 0x0
	.uleb128 .LEHB53-.LFB5791
	.uleb128 .LEHE53-.LEHB53
	.uleb128 .L1188-.LFB5791
	.uleb128 0x0
	.uleb128 .LEHB54-.LFB5791
	.uleb128 .LEHE54-.LEHB54
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE5791:
	.fnend
	.size	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_, .-_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_
	.section	.text.T.1802,"ax",%progbits
	.align	2
	.type	T.1802, %function
T.1802:
	.fnstart
.LFB6352:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	ldr	ip, [r0, #0]
	cmp	ip, #0
	.pad #16
	sub	sp, sp, #16
	mov	r4, r0
	mov	r6, r1
	beq	.L1200
	ldr	r3, .L1219
	ldr	r2, [r3, #0]
	ldr	r3, [r2, #4]
	cmp	r3, #0
	mov	r5, r2
	beq	.L1202
.L1205:
	ldr	r1, [r3, #16]
	cmp	ip, r1
	movls	r5, r3
	ldrhi	r3, [r3, #12]
	ldrls	r3, [r3, #8]
	cmp	r3, #0
	bne	.L1205
.L1202:
	cmp	r5, r2
	beq	.L1207
	ldr	r0, [r5, #16]
	cmp	ip, r0
	mov	r3, r5
	bcs	.L1208
.L1207:
	mov	r3, sp
	str	ip, [sp, #0]
	add	r0, sp, #8
	mov	ip, #0
	ldr	r1, .L1219
	add	r2, sp, #12
	str	r5, [sp, #12]
	str	ip, [sp, #4]
	bl	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS4_NS_16_Nonconst_traitsIS4_EEEERKS4_
	ldr	r3, [sp, #8]
.L1208:
	ldr	r1, [r3, #20]
	sub	r2, r1, #1
	cmp	r2, #0
	str	r2, [r3, #20]
	beq	.L1216
.L1200:
	mov	r0, #0
	stmia	r4, {r0, r6}	@ phole stm
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L1216:
	ldr	r0, [r4, #0]
	bl	_ZN21QuStupidPointerHelper6removeEPv
	ldr	r3, [r4, #4]
	cmp	r3, #1
	beq	.L1217
	ldr	r3, [r4, #0]
	cmp	r3, #0
	beq	.L1200
	ldr	lr, [r3, #-4]
	add	r0, r3, lr, asl #6
	cmp	r3, r0
	beq	.L1211
	b	.L1215
.L1218:
	mov	r0, r5
.L1215:
	sub	r5, r0, #64
	mov	r0, r5
	bl	_ZN9PngLoader5abortEv
	mov	r0, r5
	bl	_ZN9PngLoader15deleteImageDataEv
	ldr	r0, [r4, #0]
	cmp	r0, r5
	bne	.L1218
.L1211:
	sub	r0, r0, #8
	bl	_ZdaPv
	b	.L1200
.L1217:
	ldr	r5, [r4, #0]
	cmp	r5, #0
	beq	.L1200
	mov	r0, r5
	bl	_ZN9PngLoader5abortEv
	mov	r0, r5
	bl	_ZN9PngLoader15deleteImageDataEv
	mov	r0, r5
	bl	_ZdlPv
	b	.L1200
.L1220:
	.align	2
.L1219:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	T.1802, .-T.1802
	.section	.text._ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_,"axG",%progbits,_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_,comdat
	.align	2
	.weak	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_
	.hidden	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_
	.type	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_, %function
_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_:
	.fnstart
.LFB5788:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	r9, [r1, #0]
	ldr	r6, [r9, #4]
	cmp	r6, #0
	.pad #20
	sub	sp, sp, #20
	mov	fp, r1
	mov	r5, r0
	mov	r4, r2
	beq	.L1246
	ldr	sl, [r2, #0]
	ldr	r8, [r2, #4]
	rsb	r8, sl, r8
	b	.L1222
.L1247:
	cmp	r8, r7
	blt	.L1226
.L1225:
	ldr	r3, [r6, #12]
	cmp	r3, #0
	mov	r2, #0
	beq	.L1228
.L1248:
	mov	r6, r3
.L1222:
	add	r1, r6, #16
	ldmia	r1, {r1, r7}	@ phole ldm
	rsb	r7, r1, r7
	cmp	r7, r8
	movlt	r2, r7
	movge	r2, r8
	mov	r0, sl
	bl	memcmp
	cmp	r0, #0
	beq	.L1247
	bge	.L1225
.L1226:
	ldr	r3, [r6, #8]
	cmp	r3, #0
	mov	r2, #1
	bne	.L1248
.L1228:
	cmp	r2, #0
	moveq	r7, r6
	beq	.L1231
.L1230:
	ldr	r3, [r9, #8]
	cmp	r6, r3
	beq	.L1249
	mov	r0, r6
	bl	_ZN4_STL10_Rb_globalIbE12_M_decrementEPNS_18_Rb_tree_node_baseE
	mov	r7, r0
.L1231:
	ldr	r0, [r7, #16]
	ldmia	r4, {r1, r2}	@ phole ldm
	ldr	sl, [r7, #20]
	rsb	r8, r1, r2
	rsb	sl, r0, sl
	cmp	r8, sl
	movlt	r2, r8
	movge	r2, sl
	bl	memcmp
	cmp	r0, #0
	bne	.L1237
	cmp	sl, r8
	blt	.L1239
.L1238:
	mov	r1, #0
	str	r7, [r5, #0]
	strb	r1, [r5, #4]
.L1221:
	mov	r0, r5
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L1237:
	bge	.L1238
.L1239:
	mov	lr, #0
	mov	r3, r6
	add	r0, sp, #8
	mov	r1, fp
	mov	r2, lr
	stmia	sp, {r4, lr}	@ phole stm
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_
	ldr	r0, [sp, #8]
	mov	r3, #1
	str	r0, [r5, #0]
	strb	r3, [r5, #4]
	b	.L1221
.L1249:
	mov	r1, fp
	add	r0, sp, #12
	mov	ip, #0
	mov	r2, r6
	mov	r3, r6
	stmia	sp, {r4, ip}	@ phole stm
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_
	ldr	r1, [sp, #12]
	mov	r0, #1
	str	r1, [r5, #0]
	strb	r0, [r5, #4]
	b	.L1221
.L1246:
	mov	r6, r9
	b	.L1230
	.fnend
	.size	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_, .-_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_
	.section	.text._ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_,"axG",%progbits,_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_,comdat
	.align	2
	.weak	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_
	.hidden	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_
	.type	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_, %function
_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_:
	.fnstart
.LFB5856:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	r9, [r1, #0]
	ldr	r6, [r9, #4]
	cmp	r6, #0
	.pad #20
	sub	sp, sp, #20
	mov	fp, r1
	mov	r5, r0
	mov	r4, r2
	beq	.L1275
	ldr	sl, [r2, #0]
	ldr	r8, [r2, #4]
	rsb	r8, sl, r8
	b	.L1251
.L1276:
	cmp	r8, r7
	blt	.L1255
.L1254:
	ldr	r3, [r6, #12]
	cmp	r3, #0
	mov	r2, #0
	beq	.L1257
.L1277:
	mov	r6, r3
.L1251:
	add	r1, r6, #16
	ldmia	r1, {r1, r7}	@ phole ldm
	rsb	r7, r1, r7
	cmp	r7, r8
	movlt	r2, r7
	movge	r2, r8
	mov	r0, sl
	bl	memcmp
	cmp	r0, #0
	beq	.L1276
	bge	.L1254
.L1255:
	ldr	r3, [r6, #8]
	cmp	r3, #0
	mov	r2, #1
	bne	.L1277
.L1257:
	cmp	r2, #0
	moveq	r7, r6
	beq	.L1260
.L1259:
	ldr	r3, [r9, #8]
	cmp	r6, r3
	beq	.L1278
	mov	r0, r6
	bl	_ZN4_STL10_Rb_globalIbE12_M_decrementEPNS_18_Rb_tree_node_baseE
	mov	r7, r0
.L1260:
	ldr	r0, [r7, #16]
	ldmia	r4, {r1, r2}	@ phole ldm
	ldr	sl, [r7, #20]
	rsb	r8, r1, r2
	rsb	sl, r0, sl
	cmp	r8, sl
	movlt	r2, r8
	movge	r2, sl
	bl	memcmp
	cmp	r0, #0
	bne	.L1266
	cmp	sl, r8
	blt	.L1268
.L1267:
	mov	r1, #0
	str	r7, [r5, #0]
	strb	r1, [r5, #4]
.L1250:
	mov	r0, r5
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L1266:
	bge	.L1267
.L1268:
	mov	lr, #0
	mov	r3, r6
	add	r0, sp, #8
	mov	r1, fp
	mov	r2, lr
	stmia	sp, {r4, lr}	@ phole stm
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_
	ldr	r0, [sp, #8]
	mov	r3, #1
	str	r0, [r5, #0]
	strb	r3, [r5, #4]
	b	.L1250
.L1278:
	mov	r1, fp
	add	r0, sp, #12
	mov	ip, #0
	mov	r2, r6
	mov	r3, r6
	stmia	sp, {r4, ip}	@ phole stm
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_
	ldr	r1, [sp, #12]
	mov	r0, #1
	str	r1, [r5, #0]
	strb	r0, [r5, #4]
	b	.L1250
.L1275:
	mov	r6, r9
	b	.L1259
	.fnend
	.size	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_, .-_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_
	.section	.text._ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE3putEc,"axG",%progbits,_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE3putEc,comdat
	.align	2
	.weak	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE3putEc
	.hidden	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE3putEc
	.type	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE3putEc, %function
_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE3putEc:
	.fnstart
.LFB5092:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	.pad #8
	sub	sp, sp, #8
	str	r0, [sp, #0]
	ldr	r2, [r0, #0]
	ldr	r3, [r2, #-12]
	add	r3, r0, r3
	mov	r4, r0
	ldr	r0, [r3, #8]
	cmp	r0, #0
	mov	r5, r1
	movne	r3, #0
	bne	.L1281
	ldr	r1, [r3, #88]
	cmp	r1, #0
	beq	.L1298
.L1282:
	ldr	r6, [r3, #92]
	cmp	r6, #0
	beq	.L1283
	ldr	r1, [r6, #0]
	ldr	r0, [r1, #-12]
	add	ip, r6, r0
	ldr	r1, [ip, #88]
	cmp	r1, #0
	beq	.L1283
	mov	r0, r1
	ldr	r2, [r1, #0]
.LEHB55:
	ldr	ip, [r2, #20]
	mov	lr, pc
	bx	ip
.LEHE55:
	cmn	r0, #1
	beq	.L1284
.L1297:
	ldr	r2, [r4, #0]
	ldr	lr, [r2, #-12]
	add	r3, r4, lr
.L1283:
	ldr	ip, [r3, #8]
	rsbs	r3, ip, #1
	movcc	r3, #0
.L1281:
	cmp	r3, #0
	strb	r3, [sp, #4]
	beq	.L1288
	ldr	r1, [r2, #-12]
	add	r3, r4, r1
	ldr	r3, [r3, #88]
	ldr	r2, [r3, #20]
	ldr	r0, [r3, #24]
	cmp	r2, r0
	strccb	r5, [r2], #1
	strcc	r2, [r3, #20]
	bcs	.L1299
.L1290:
	ldr	r5, [sp, #0]
	ldr	r2, [r5, #0]
	ldr	ip, [r2, #-12]
	add	r3, r5, ip
	ldr	r1, [r3, #4]
	tst	r1, #8192
	bne	.L1300
.L1292:
	mov	r0, r4
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L1299:
	mov	r1, r5
	mov	r0, r3
	ldr	r5, [r3, #0]
.LEHB56:
	ldr	ip, [r5, #52]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	bne	.L1290
	ldr	r2, [r4, #0]
.L1288:
	ldr	r0, [r2, #-12]
	add	r0, r4, r0
	ldr	r3, [r0, #8]
	ldr	r2, [r0, #20]
	orr	ip, r3, #1
	tst	ip, r2
	str	ip, [r0, #8]
	beq	.L1290
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
.LEHE56:
	ldr	r5, [sp, #0]
	ldr	r2, [r5, #0]
	ldr	ip, [r2, #-12]
	add	r3, r5, ip
	ldr	r1, [r3, #4]
	tst	r1, #8192
	beq	.L1292
.L1300:
	ldr	r3, [r3, #88]
	cmp	r3, #0
	beq	.L1292
	mov	r0, r3
	ldr	r3, [r3, #0]
.LEHB57:
	ldr	ip, [r3, #20]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	bne	.L1292
	ldr	r2, [r5, #0]
	ldr	r0, [r2, #-12]
	add	r0, r5, r0
	ldr	ip, [r0, #8]
	ldr	r1, [r0, #20]
	orr	lr, ip, #1
	tst	lr, r1
	str	lr, [r0, #8]
	beq	.L1292
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
	b	.L1292
.L1284:
	ldr	r1, [r6, #0]
	ldr	r0, [r1, #-12]
	add	r0, r6, r0
	ldr	r3, [r0, #8]
	ldr	ip, [r0, #20]
	orr	lr, r3, #1
	tst	lr, ip
	str	lr, [r0, #8]
	beq	.L1297
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
	b	.L1297
.L1298:
	mov	r0, r3
	mov	r1, #1
	bl	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEE8setstateEi
.LEHE57:
	ldr	r2, [r4, #0]
	ldr	r6, [r2, #-12]
	add	r3, r4, r6
	b	.L1282
.L1296:
.L1294:
	mov	r4, r0
	mov	r0, sp
	bl	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	mov	r0, r4
.LEHB58:
	bl	__cxa_end_cleanup
.LEHE58:
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA5092:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5092-.LLSDACSB5092
.LLSDACSB5092:
	.uleb128 .LEHB55-.LFB5092
	.uleb128 .LEHE55-.LEHB55
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB56-.LFB5092
	.uleb128 .LEHE56-.LEHB56
	.uleb128 .L1296-.LFB5092
	.uleb128 0x0
	.uleb128 .LEHB57-.LFB5092
	.uleb128 .LEHE57-.LEHB57
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB58-.LFB5092
	.uleb128 .LEHE58-.LEHB58
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE5092:
	.fnend
	.size	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE3putEc, .-_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE3putEc
	.section	.text._ZN4_STL4endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_,"axG",%progbits,_ZN4_STL4endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_,comdat
	.align	2
	.weak	_ZN4_STL4endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.hidden	_ZN4_STL4endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.type	_ZN4_STL4endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_, %function
_ZN4_STL4endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_:
	.fnstart
.LFB4624:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	ldr	r0, [r0, #0]
	ldr	r3, [r0, #-12]
	add	lr, r4, r3
	ldr	ip, [lr, #64]
	mov	r1, #10
	mov	r0, ip
	ldr	r2, [ip, #0]
	ldr	ip, [r2, #24]
	mov	lr, pc
	bx	ip
	mov	r1, r0
	mov	r0, r4
	bl	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE3putEc
	ldr	r1, [r4, #0]
	ldr	r0, [r1, #-12]
	add	r3, r4, r0
	ldr	r3, [r3, #88]
	cmp	r3, #0
	beq	.L1302
	mov	r0, r3
	ldr	r1, [r3, #0]
	ldr	ip, [r1, #20]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	beq	.L1305
.L1302:
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L1305:
	ldr	r3, [r4, #0]
	ldr	r0, [r3, #-12]
	add	r0, r4, r0
	ldr	lr, [r0, #8]
	ldr	r2, [r0, #20]
	orr	ip, lr, #1
	tst	ip, r2
	str	ip, [r0, #8]
	beq	.L1302
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
	b	.L1302
	.fnend
	.size	_ZN4_STL4endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_, .-_ZN4_STL4endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	.section	.text._Z13myDevicePausePvS_,"ax",%progbits
	.align	2
	.global	_Z13myDevicePausePvS_
	.hidden	_Z13myDevicePausePvS_
	.type	_Z13myDevicePausePvS_, %function
_Z13myDevicePausePvS_:
	.fnstart
.LFB4561:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	ldr	r4, .L1311
	bl	s3eDeviceUnYield
	bl	s3eDeviceBacklightOn
	ldr	r0, .L1311+4
	bl	T.1796
	ldr	r1, [r4, #0]
	ldr	r0, [r1, #-12]
	add	r3, r4, r0
	ldr	ip, [r3, #64]
	mov	r1, #10
	mov	r0, ip
	ldr	r2, [ip, #0]
	ldr	ip, [r2, #24]
	mov	lr, pc
	bx	ip
	mov	r1, r0
	mov	r0, r4
	bl	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE3putEc
	ldr	r1, [r4, #0]
	ldr	r0, [r1, #-12]
	add	r3, r4, r0
	ldr	r3, [r3, #88]
	cmp	r3, #0
	beq	.L1307
	mov	r0, r3
	ldr	r2, [r3, #0]
	ldr	ip, [r2, #20]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	beq	.L1310
.L1307:
	mov	r0, #0
	ldmfd	sp!, {r4, lr}
	bx	lr
.L1310:
	ldr	r1, [r4, #0]
	ldr	r0, [r1, #-12]
	add	r0, r4, r0
	ldr	r3, [r0, #8]
	ldr	ip, [r0, #20]
	orr	lr, r3, #1
	tst	lr, ip
	str	lr, [r0, #8]
	beq	.L1307
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
	b	.L1307
.L1312:
	.align	2
.L1311:
	.word	_ZN4_STL4coutE
	.word	.LC1
	.fnend
	.size	_Z13myDevicePausePvS_, .-_Z13myDevicePausePvS_
	.section	.text._ZN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev,"axG",%progbits,_ZN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev,comdat
	.align	2
	.weak	_ZThn8_N4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.hidden	_ZThn8_N4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.type	_ZThn8_N4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev, %function
_ZThn8_N4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev:
	.fnstart
	sub	r0, r0, #8
	b	.LTHUNK11
	.fnend
	.size	_ZThn8_N4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev, .-_ZThn8_N4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.align	2
	.weak	_ZN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.hidden	_ZN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.type	_ZN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev, %function
_ZN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev:
	.fnstart
.LFB5005:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	ldr	r3, .L1321
	mov	r4, r0
	ldr	r0, [r0, #52]
	ldr	r2, .L1321+4
	add	ip, r3, #20
	add	r1, r3, #40
	cmp	r0, #0
	str	r1, [r4, #72]
	str	ip, [r4, #8]
	str	r2, [r4, #12]
	str	r3, [r4, #0]
	blne	free
.L1314:
	ldr	r0, .L1321+8
	str	r0, [r4, #12]
	add	r0, r4, #40
.LEHB59:
	bl	_ZN4_STL6localeD1Ev
.LEHE59:
	ldr	lr, .L1321+12
	ldr	r1, [lr, #4]
	str	r1, [r4, #0]
	ldr	r3, [lr, #16]
	ldr	r0, [r1, #-12]
	ldr	r2, [lr, #24]
	str	r2, [r4, r0]
	str	r3, [r4, #8]
	ldr	ip, [r3, #-12]
	ldr	r2, [lr, #8]
	ldr	r0, [lr, #20]
	add	r1, r4, ip
	str	r0, [r1, #8]
	str	r2, [r4, #0]
	ldr	ip, [lr, #12]
	ldr	r3, [r2, #-12]
	ldr	lr, .L1321+16
	mov	r0, r4
	str	ip, [r4, r3]
	str	lr, [r0, #72]!
.LEHB60:
	bl	_ZN4_STL8ios_baseD2Ev
.LEHE60:
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L1319:
.L1316:
.L1320:
.L1317:
	ldr	lr, .L1321+12
	ldr	r3, [lr, #4]
	str	r3, [r4, #0]
	ldr	r2, [lr, #16]
	ldr	r1, [r3, #-12]
	ldr	ip, [lr, #24]
	str	ip, [r4, r1]
	str	r2, [r4, #8]
	ldr	r3, [r2, #-12]
	ldr	ip, [lr, #20]
	ldr	r2, [lr, #8]
	add	r1, r4, r3
	str	ip, [r1, #8]
	str	r2, [r4, #0]
	ldr	r1, [lr, #12]
	ldr	r3, [r2, #-12]
	ldr	ip, .L1321+16
	mov	lr, r4
	str	r1, [r4, r3]
	str	ip, [lr, #72]!
	mov	r4, r0
	mov	r0, lr
	bl	_ZN4_STL8ios_baseD2Ev
	mov	r0, r4
.LEHB61:
	bl	__cxa_end_cleanup
.LEHE61:
.L1322:
	.align	2
.L1321:
	.word	_ZTVN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE+12
	.word	_ZTVN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE+8
	.word	_ZTVN4_STL15basic_streambufIcNS_11char_traitsIcEEEE+8
	.word	_ZTTN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.word	_ZTVN4_STL9basic_iosIcNS_11char_traitsIcEEEE+8
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA5005:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5005-.LLSDACSB5005
.LLSDACSB5005:
	.uleb128 .LEHB59-.LFB5005
	.uleb128 .LEHE59-.LEHB59
	.uleb128 .L1319-.LFB5005
	.uleb128 0x0
	.uleb128 .LEHB60-.LFB5005
	.uleb128 .LEHE60-.LEHB60
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB61-.LFB5005
	.uleb128 .LEHE61-.LEHB61
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE5005:
	.fnend
	.size	_ZN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev, .-_ZN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.section	.text._ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE6setbufEPci,"axG",%progbits,_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE6setbufEPci,comdat
	.align	2
	.weak	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE6setbufEPci
	.hidden	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE6setbufEPci
	.type	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE6setbufEPci, %function
_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE6setbufEPci:
	.fnstart
.LFB6299:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	subs	r9, r2, #0
	.pad #12
	sub	sp, sp, #12
	mov	r4, r0
	ble	.L1324
	ldr	r3, [r0, #40]
	ldr	r6, [r0, #16]
	cmp	r6, r3
	moveq	r2, #1
	movne	r7, #0
	strne	r7, [sp, #4]
	ldreq	r7, [r0, #20]
	streq	r2, [sp, #4]
	ldr	r0, [r0, #4]
	rsbeq	r7, r6, r7
	ldr	r2, [r4, #36]
	cmp	r3, r0
	ldreq	r8, [r4, #8]
	movne	r8, #0
	and	r2, r2, #24
	movne	fp, r8
	rsbeq	r8, r3, r8
	moveq	fp, #1
	cmp	r2, #16
	mov	r5, r3
	beq	.L1341
.L1329:
	ldr	ip, [r4, #48]
	sub	r1, ip, #1
	rsb	r3, r5, r1
	cmp	r9, r3
	ldrcc	r3, [r4, #44]
	bcc	.L1333
	add	r9, r9, #1
	mov	r0, r9
	bl	malloc
	subs	r6, r0, #0
	beq	.L1342
.L1334:
	add	r1, r4, #40
	ldmia	r1, {r1, sl}	@ phole ldm
	cmp	r1, sl
	mov	r5, r6
	moveq	sl, r6
	beq	.L1336
	rsb	sl, r1, sl
	mov	r2, sl
	mov	r0, r6
	bl	memmove
	add	sl, r0, sl
.L1336:
	mov	lr, #0
	strb	lr, [sl, #0]
	ldr	r0, [r4, #40]
	cmp	r0, lr
	mov	r3, sl
	beq	.L1337
	str	sl, [sp, #0]
	bl	free
	ldr	r3, [sp, #0]
.L1337:
	add	r9, r6, r9
	str	sl, [r4, #44]
	str	r9, [r4, #48]
	str	r6, [r4, #40]
.L1333:
	cmp	fp, #0
	rsb	r0, r5, r3
	addne	r2, r5, r0
	addne	r8, r5, r8
	strne	r8, [r4, #8]
	strne	r2, [r4, #12]
	strne	r5, [r4, #4]
	ldr	r2, [sp, #4]
	cmp	r2, #0
	addne	r7, r5, r7
	addne	r0, r5, r0
	strne	r0, [r4, #24]
	strne	r7, [r4, #20]
	strne	r5, [r4, #16]
.L1324:
	mov	r0, r4
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L1341:
	add	r1, r4, #52
	cmp	r6, r1
	ldr	r2, [r4, #20]
	beq	.L1343
.L1331:
	ldr	ip, [r4, #24]
	cmp	ip, r2
	bne	.L1329
	add	r2, r1, #8
	str	r2, [r4, #24]
	str	r1, [r4, #16]
	str	r1, [r4, #20]
	mov	r5, r3
	b	.L1329
.L1343:
	cmp	r6, r2
	beq	.L1331
	add	r0, r4, #40
	mov	r1, r6
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendIPKcEERS5_T_SA_
	add	r3, r4, #60
	str	r6, [r4, #20]
	str	r3, [r4, #24]
	str	r6, [r4, #16]
	ldr	r5, [r4, #40]
	b	.L1329
.L1342:
	mov	r0, r9
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
	mov	r6, r0
	b	.L1334
	.fnend
	.size	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE6setbufEPci, .-_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE6setbufEPci
	.section	.text._ZN23QuSoundManagerInterface9playSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE,"axG",%progbits,_ZN23QuSoundManagerInterface9playSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE,comdat
	.align	2
	.weak	_ZN23QuSoundManagerInterface9playSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.hidden	_ZN23QuSoundManagerInterface9playSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.type	_ZN23QuSoundManagerInterface9playSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE, %function
_ZN23QuSoundManagerInterface9playSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE:
	.fnstart
.LFB2798:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	.save {r4, r5, r6, r7, lr}
	.pad #28
	sub	sp, sp, #28
	ldr	r3, [r0, #0]
	add	r4, sp, #4
	mov	r7, r0
	add	r5, sp, #16
	mov	r0, r4
	ldr	r6, [r3, #16]
.LEHB62:
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
.LEHE62:
	mov	r1, r7
	mov	r0, r5
	mov	r2, r4
.LEHB63:
	mov	lr, pc
	bx	r6
.LEHE63:
	ldr	r2, [sp, #16]
	mov	r0, r2
	ldr	r1, [r2, #0]
.LEHB64:
	ldr	ip, [r1, #8]
	mov	lr, pc
	bx	ip
.LEHE64:
	mov	r0, r5
	mov	r1, #0
	ldr	r2, [sp, #20]
.LEHB65:
	bl	_ZN15QuStupidPointerI22QuSoundStructInterfaceE3setEPS0_i
.LEHE65:
	ldr	r0, [sp, #4]
	cmp	r0, #0
	blne	free
.L1352:
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L1354:
	mov	r6, r0
.L1351:
	mov	r0, r4
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r6
.LEHB66:
	bl	__cxa_end_cleanup
.LEHE66:
.L1353:
.L1346:
	mov	r6, r0
	mov	r1, #0
	mov	r0, r5
	ldr	r2, [sp, #20]
	bl	_ZN15QuStupidPointerI22QuSoundStructInterfaceE3setEPS0_i
	b	.L1351
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA2798:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2798-.LLSDACSB2798
.LLSDACSB2798:
	.uleb128 .LEHB62-.LFB2798
	.uleb128 .LEHE62-.LEHB62
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB63-.LFB2798
	.uleb128 .LEHE63-.LEHB63
	.uleb128 .L1354-.LFB2798
	.uleb128 0x0
	.uleb128 .LEHB64-.LFB2798
	.uleb128 .LEHE64-.LEHB64
	.uleb128 .L1353-.LFB2798
	.uleb128 0x0
	.uleb128 .LEHB65-.LFB2798
	.uleb128 .LEHE65-.LEHB65
	.uleb128 .L1354-.LFB2798
	.uleb128 0x0
	.uleb128 .LEHB66-.LFB2798
	.uleb128 .LEHE66-.LEHB66
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE2798:
	.fnend
	.size	_ZN23QuSoundManagerInterface9playSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE, .-_ZN23QuSoundManagerInterface9playSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.section	.text._ZN18InstructionManagerD0Ev,"axG",%progbits,_ZN18InstructionManagerD0Ev,comdat
	.align	2
	.weak	_ZN18InstructionManagerD0Ev
	.hidden	_ZN18InstructionManagerD0Ev
	.type	_ZN18InstructionManagerD0Ev, %function
_ZN18InstructionManagerD0Ev:
	.fnstart
.LFB6234:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	mov	r4, r0
	ldr	r0, [r0, #260]
	ldr	r3, .L1378
	cmp	r0, #0
	str	r3, [r4, #0]
.LEHB67:
	blne	s3eFileClose
.LEHE67:
.L1357:
	ldr	r1, .L1378+4
	ldr	lr, .L1378+8
	ldr	r2, .L1378+12
	mov	r0, r4
	mov	ip, #0
	str	lr, [r0, #28]!
	str	ip, [r4, #260]
	str	r2, [r4, #168]
	str	r1, [r4, #188]
	str	r1, [r4, #224]
.LEHB68:
	bl	_ZN12QuDrawObjectD2Ev
.LEHE68:
	add	r0, r4, #20
	mov	r1, #0
	ldr	r2, [r4, #24]
.LEHB69:
	bl	_ZN15QuStupidPointerI22QuSoundStructInterfaceE3setEPS0_i
.LEHE69:
	ldr	r1, .L1378+16
	ldr	r0, .L1378+20
	stmia	r4, {r0, r1}	@ phole stm
	mov	r0, r4
	bl	_ZdlPv
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L1370:
.L1360:
.L1371:
.L1361:
.L1372:
.L1364:
	ldr	r3, .L1378+4
	ldr	r5, .L1378+12
	str	r3, [r4, #188]
	str	r5, [r4, #168]
	str	r3, [r4, #224]
	mov	r5, r0
	add	r0, r4, #28
	bl	_ZN17QuImageDrawObjectD1Ev
.L1365:
	add	r0, r4, #20
	mov	r1, #0
	ldr	r2, [r4, #24]
	bl	_ZN15QuStupidPointerI22QuSoundStructInterfaceE3setEPS0_i
.L1367:
.L1375:
.L1368:
	ldr	ip, .L1378+20
	ldr	r2, .L1378+16
	str	ip, [r4, #0]
	str	r2, [r4, #4]
	mov	r0, r5
.LEHB70:
	bl	__cxa_end_cleanup
.LEHE70:
.L1374:
	mov	r5, r0
	b	.L1367
.L1373:
	mov	r5, r0
	b	.L1365
.L1379:
	.align	2
.L1378:
	.word	_ZTV18InstructionManager+8
	.word	_ZTV13QuBasicCamera+8
	.word	_ZTV17QuImageDrawObject+8
	.word	_ZTV23QuAbsScreenIntBoxButton+8
	.word	_ZTV7QuTimer+8
	.word	_ZTV13QuBaseManager+8
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA6234:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6234-.LLSDACSB6234
.LLSDACSB6234:
	.uleb128 .LEHB67-.LFB6234
	.uleb128 .LEHE67-.LEHB67
	.uleb128 .L1370-.LFB6234
	.uleb128 0x0
	.uleb128 .LEHB68-.LFB6234
	.uleb128 .LEHE68-.LEHB68
	.uleb128 .L1373-.LFB6234
	.uleb128 0x0
	.uleb128 .LEHB69-.LFB6234
	.uleb128 .LEHE69-.LEHB69
	.uleb128 .L1374-.LFB6234
	.uleb128 0x0
	.uleb128 .LEHB70-.LFB6234
	.uleb128 .LEHE70-.LEHB70
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE6234:
	.fnend
	.size	_ZN18InstructionManagerD0Ev, .-_ZN18InstructionManagerD0Ev
	.section	.text._ZN12QuDrawObject6enableEv,"axG",%progbits,_ZN12QuDrawObject6enableEv,comdat
	.align	2
	.weak	_ZN12QuDrawObject6enableEv
	.hidden	_ZN12QuDrawObject6enableEv
	.type	_ZN12QuDrawObject6enableEv, %function
_ZN12QuDrawObject6enableEv:
	.fnstart
.LFB2972:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	ldrb	r3, [r0, #120]	@ zero_extendqisi2
	cmp	r3, #0
	mov	r4, r0
	beq	.L1381
	ldr	r2, [r0, #124]
	mov	r0, r2
	ldr	r1, [r2, #0]
	ldr	ip, [r1, #0]
	mov	lr, pc
	bx	ip
	cmp	r0, #0
	beq	.L1406
.L1381:
	ldrb	r0, [r4, #8]	@ zero_extendqisi2
	cmp	r0, #0
	bne	.L1407
.L1383:
	ldrb	r2, [r4, #36]	@ zero_extendqisi2
	cmp	r2, #0
	bne	.L1408
.L1389:
	ldrb	r2, [r4, #64]	@ zero_extendqisi2
	cmp	r2, #0
	bne	.L1409
.L1395:
	ldrb	r2, [r4, #92]	@ zero_extendqisi2
	cmp	r2, #0
	bne	.L1410
.L1406:
	ldmfd	sp!, {r4, lr}
	bx	lr
.L1410:
	ldr	r0, [r4, #112]
	bl	glEnableClientState
	ldr	r0, [r4, #112]
	sub	r1, r0, #32768
	sub	ip, r1, #116
	cmp	ip, #4
	ldrls	pc, [pc, ip, asl #2]
	b	.L1406
.L1405:
	.word	.L1401
	.word	.L1402
	.word	.L1403
	.word	.L1406
	.word	.L1404
.L1409:
	ldr	r0, [r4, #84]
	bl	glEnableClientState
	ldr	r0, [r4, #84]
	sub	r1, r0, #32768
	sub	ip, r1, #116
	cmp	ip, #4
	ldrls	pc, [pc, ip, asl #2]
	b	.L1395
.L1400:
	.word	.L1396
	.word	.L1397
	.word	.L1398
	.word	.L1395
	.word	.L1399
.L1408:
	ldr	r0, [r4, #56]
	bl	glEnableClientState
	ldr	r3, [r4, #56]
	sub	r1, r3, #32768
	sub	ip, r1, #116
	cmp	ip, #4
	ldrls	pc, [pc, ip, asl #2]
	b	.L1389
.L1394:
	.word	.L1390
	.word	.L1391
	.word	.L1392
	.word	.L1389
	.word	.L1393
.L1407:
	ldr	r0, [r4, #28]
	bl	glEnableClientState
	ldr	r1, [r4, #28]
	sub	r3, r1, #32768
	sub	ip, r3, #116
	cmp	ip, #4
	ldrls	pc, [pc, ip, asl #2]
	b	.L1383
.L1388:
	.word	.L1384
	.word	.L1385
	.word	.L1386
	.word	.L1383
	.word	.L1387
.L1387:
	ldr	r0, [r4, #24]
	ldr	r2, [r4, #20]
	ldr	ip, [r4, #12]
	ldr	r3, [r4, #16]
	mov	lr, #5120
	add	r3, r3, r0, asl #2
	mul	r2, ip, r2
	add	r1, lr, #6
	ldr	r0, [r4, #32]
	bl	glTexCoordPointer
	b	.L1383
.L1386:
	ldr	r0, [r4, #12]
	add	r2, r4, #20
	ldmia	r2, {r2, lr}	@ phole ldm
	ldr	r3, [r4, #16]
	mov	r1, #5120
	mul	r2, r0, r2
	add	r3, r3, lr, asl #2
	add	r1, r1, #6
	ldr	r0, [r4, #32]
	bl	glColorPointer
	b	.L1383
.L1385:
	add	r0, r4, #16
	ldmia	r0, {r0, r1}	@ phole ldm
	ldr	ip, [r4, #12]
	ldr	r2, [r4, #24]
	mov	lr, #5120
	add	r2, r0, r2, asl #2
	mul	r1, ip, r1
	add	r0, lr, #6
	bl	glNormalPointer
	b	.L1383
.L1384:
	ldr	r3, [r4, #12]
	ldr	r0, [r4, #24]
	ldr	r2, [r4, #20]
	ldr	lr, [r4, #16]
	mov	r1, #5120
	mul	r2, r3, r2
	add	r1, r1, #6
	add	r3, lr, r0, asl #2
	ldr	r0, [r4, #32]
	bl	glVertexPointer
	b	.L1383
.L1393:
	ldr	r0, [r4, #52]
	add	r1, r4, #44
	ldmia	r1, {r1, r2}	@ phole ldm
	ldr	ip, [r4, #40]
	mov	lr, #5120
	add	r3, r1, r0, asl #2
	mul	r2, ip, r2
	add	r1, lr, #6
	ldr	r0, [r4, #60]
	bl	glTexCoordPointer
	b	.L1389
.L1392:
	ldr	r3, [r4, #40]
	ldr	r0, [r4, #52]
	ldr	r2, [r4, #48]
	ldr	lr, [r4, #44]
	mov	r1, #5120
	mul	r2, r3, r2
	add	r1, r1, #6
	add	r3, lr, r0, asl #2
	ldr	r0, [r4, #60]
	bl	glColorPointer
	b	.L1389
.L1391:
	ldr	r1, [r4, #48]
	ldr	ip, [r4, #40]
	ldr	r2, [r4, #52]
	ldr	r3, [r4, #44]
	mov	lr, #5120
	mul	r1, ip, r1
	add	r2, r3, r2, asl #2
	add	r0, lr, #6
	bl	glNormalPointer
	b	.L1389
.L1390:
	ldr	r0, [r4, #40]
	ldr	r1, [r4, #52]
	ldr	r2, [r4, #48]
	ldr	r3, [r4, #44]
	mov	lr, #5120
	mul	r2, r0, r2
	add	r3, r3, r1, asl #2
	ldr	r0, [r4, #60]
	add	r1, lr, #6
	bl	glVertexPointer
	b	.L1389
.L1399:
	ldr	r1, [r4, #80]
	ldr	r2, [r4, #76]
	ldr	ip, [r4, #68]
	ldr	r3, [r4, #72]
	mov	lr, #5120
	add	r3, r3, r1, asl #2
	mul	r2, ip, r2
	add	r1, lr, #6
	ldr	r0, [r4, #88]
	bl	glTexCoordPointer
	b	.L1395
.L1398:
	ldr	r3, [r4, #68]
	ldr	r0, [r4, #80]
	ldr	r2, [r4, #76]
	ldr	lr, [r4, #72]
	mov	r1, #5120
	mul	r2, r3, r2
	add	r1, r1, #6
	add	r3, lr, r0, asl #2
	ldr	r0, [r4, #88]
	bl	glColorPointer
	b	.L1395
.L1397:
	ldr	r1, [r4, #76]
	ldr	ip, [r4, #68]
	ldr	r2, [r4, #80]
	ldr	r3, [r4, #72]
	mov	lr, #5120
	mul	r1, ip, r1
	add	r2, r3, r2, asl #2
	add	r0, lr, #6
	bl	glNormalPointer
	b	.L1395
.L1396:
	ldr	r1, [r4, #68]
	ldr	r0, [r4, #80]
	ldr	r2, [r4, #76]
	ldr	r3, [r4, #72]
	mov	lr, #5120
	mul	r2, r1, r2
	add	r3, r3, r0, asl #2
	add	r1, lr, #6
	ldr	r0, [r4, #88]
	bl	glVertexPointer
	b	.L1395
.L1404:
	ldr	r1, [r4, #108]
	ldr	r2, [r4, #104]
	ldr	ip, [r4, #96]
	ldr	r3, [r4, #100]
	mov	lr, #5120
	add	r3, r3, r1, asl #2
	mul	r2, ip, r2
	ldr	r0, [r4, #116]
	add	r1, lr, #6
	bl	glTexCoordPointer
	b	.L1406
.L1403:
	ldr	r3, [r4, #96]
	ldr	r0, [r4, #108]
	ldr	r2, [r4, #104]
	ldr	lr, [r4, #100]
	mov	r1, #5120
	mul	r2, r3, r2
	add	r1, r1, #6
	add	r3, lr, r0, asl #2
	ldr	r0, [r4, #116]
	bl	glColorPointer
	b	.L1406
.L1402:
	ldr	r3, [r4, #100]
	ldr	r1, [r4, #104]
	ldr	ip, [r4, #96]
	ldr	r2, [r4, #108]
	mov	lr, #5120
	mul	r1, ip, r1
	add	r2, r3, r2, asl #2
	add	r0, lr, #6
	bl	glNormalPointer
	b	.L1406
.L1401:
	ldr	r1, [r4, #96]
	ldr	r0, [r4, #108]
	ldr	r2, [r4, #104]
	ldr	r3, [r4, #100]
	mov	lr, #5120
	mul	r2, r1, r2
	add	r3, r3, r0, asl #2
	add	r1, lr, #6
	ldr	r0, [r4, #116]
	bl	glVertexPointer
	b	.L1406
	.fnend
	.size	_ZN12QuDrawObject6enableEv, .-_ZN12QuDrawObject6enableEv
	.section	.text.T.1795,"ax",%progbits
	.align	2
	.type	T.1795, %function
T.1795:
	.fnstart
.LFB6345:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	.save {r4, r5, r6, r7, lr}
	.pad #20
	sub	sp, sp, #20
	mov	r6, #0
	add	r5, sp, #16
	str	r6, [r5, #-8]!
	ldr	r2, [r1, #4]
	mov	r4, r0
	ldr	r1, [r1, #0]
	mov	r0, r5
.LEHB71:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE71:
	mov	r2, r6
	mov	r0, r4
	mov	r1, r5
	mov	r3, r6
	str	r6, [sp, #0]
.LEHB72:
	bl	_ZN12QuDrawObject9loadImageE15QuStupidPointerI11QuBaseImageEPfii
.LEHE72:
	mov	r6, r0
	mov	r1, #0
	mov	r0, r5
	ldr	r2, [sp, #12]
.LEHB73:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
	cmp	r6, #0
	moveq	r0, r6
	beq	.L1415
	mov	r0, #48
	bl	_Znaj
.LEHE73:
	ldr	r7, .L1424
	mov	r5, r0
	ldmia	r7!, {r0, r1, r2, r3}
	mov	ip, r5
	stmia	ip!, {r0, r1, r2, r3}
	ldmia	r7!, {r0, r1, r2, r3}
	mov	r6, ip
	stmia	r6!, {r0, r1, r2, r3}
	ldmia	r7, {r0, r1, r2, r3}
	stmia	r6, {r0, r1, r2, r3}
	ldrb	r3, [r4, #8]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L1423
.L1421:
	mov	r0, #32768
	add	lr, r0, #116
	mov	r2, #0
	mov	r0, #1
	mov	r1, #3
	str	r5, [r4, #16]
	str	r2, [r4, #24]
	str	lr, [r4, #28]
	str	r1, [r4, #32]
	strb	r0, [r4, #136]
	str	r2, [r4, #20]
	strb	r0, [r4, #8]
.L1415:
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L1423:
	add	r0, r4, #8
	bl	_ZN14QuArrayPointerIfE6deInitEv
	b	.L1421
.L1422:
.L1416:
	mov	r4, r0
	mov	r1, #0
	mov	r0, r5
	ldr	r2, [sp, #12]
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
	mov	r0, r4
.LEHB74:
	bl	__cxa_end_cleanup
.LEHE74:
.L1425:
	.align	2
.L1424:
	.word	.LANCHOR1
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA6345:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6345-.LLSDACSB6345
.LLSDACSB6345:
	.uleb128 .LEHB71-.LFB6345
	.uleb128 .LEHE71-.LEHB71
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB72-.LFB6345
	.uleb128 .LEHE72-.LEHB72
	.uleb128 .L1422-.LFB6345
	.uleb128 0x0
	.uleb128 .LEHB73-.LFB6345
	.uleb128 .LEHE73-.LEHB73
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB74-.LFB6345
	.uleb128 .LEHE74-.LEHB74
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE6345:
	.fnend
	.size	T.1795, .-T.1795
	.section	.text._ZN23QuAbsScreenIntBoxButton16drawRotatedImageE15QuStupidPointerI11QuBaseImageE15GDeviceRotation,"axG",%progbits,_ZN23QuAbsScreenIntBoxButton16drawRotatedImageE15QuStupidPointerI11QuBaseImageE15GDeviceRotation,comdat
	.align	2
	.weak	_ZN23QuAbsScreenIntBoxButton16drawRotatedImageE15QuStupidPointerI11QuBaseImageE15GDeviceRotation
	.hidden	_ZN23QuAbsScreenIntBoxButton16drawRotatedImageE15QuStupidPointerI11QuBaseImageE15GDeviceRotation
	.type	_ZN23QuAbsScreenIntBoxButton16drawRotatedImageE15QuStupidPointerI11QuBaseImageE15GDeviceRotation, %function
_ZN23QuAbsScreenIntBoxButton16drawRotatedImageE15QuStupidPointerI11QuBaseImageE15GDeviceRotation:
	.fnstart
.LFB3858:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 296
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, lr}
	.pad #296
	sub	sp, sp, #296
	mov	r4, r0
	ldr	sl, .L1448
	add	r0, sp, #4
	mov	r6, r1
	add	r5, sp, #296
.LEHB75:
	bl	_ZN12QuDrawObjectC2Ev
.LEHE75:
	mov	r3, #0
	str	sl, [sp, #4]
	strb	r3, [sp, #140]
	str	r3, [r5, #-8]!
	ldr	r2, [r6, #4]
	mov	r0, r5
	ldr	r1, [r6, #0]
.LEHB76:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE76:
	add	r0, sp, #4
	mov	r1, r5
.LEHB77:
	bl	T.1795
.LEHE77:
	mov	r0, r5
	mov	r1, #0
	ldr	r2, [sp, #292]
.LEHB78:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
	ldr	r0, [r4, #4]
	bl	__aeabi_i2f
	mov	r6, r0
	ldr	r0, [r4, #8]
	bl	__aeabi_i2f
	mov	r5, r0
	ldr	r0, [r4, #12]
	bl	__aeabi_i2f
	mov	r8, r0
	ldr	r0, [r4, #16]
	bl	__aeabi_i2f
	mov	r7, #0
	add	ip, sp, #248
	str	r7, [ip], #4
	add	r3, ip, #8
	str	r7, [r3], #4
	add	r9, r3, #8
	str	r7, [r9], #4
	add	r2, r9, #8
	str	r7, [r2, #0]
	mov	r1, r0
	mov	r0, r5
	str	r6, [sp, #240]	@ float
	bl	__aeabi_fadd
	mov	r1, r8
	mov	r7, r0
	mov	r0, r6
	str	r7, [sp, #244]	@ float
	bl	__aeabi_fadd
	mov	ip, r0
	mov	r0, #0
	str	ip, [sp, #204]	@ float
	str	r0, [sp, #200]	@ float
	str	r6, [sp, #192]	@ float
	str	r7, [sp, #196]	@ float
	add	r9, sp, #192
	ldmia	r9!, {r0, r1, r2, r3}
	add	r8, sp, #144
	mov	lr, #0
	str	ip, [sp, #228]	@ float
	str	ip, [sp, #252]	@ float
	str	ip, [sp, #276]	@ float
	mov	ip, r8
	str	lr, [sp, #212]	@ float
	str	lr, [sp, #224]	@ float
	str	lr, [sp, #236]	@ float
	str	r7, [sp, #208]	@ float
	str	r6, [sp, #216]	@ float
	str	r5, [sp, #232]	@ float
	str	r7, [sp, #256]	@ float
	str	r6, [sp, #264]	@ float
	str	r5, [sp, #268]	@ float
	str	r5, [sp, #280]	@ float
	str	r5, [sp, #220]	@ float
	stmia	ip!, {r0, r1, r2, r3}
	ldmia	r9!, {r0, r1, r2, r3}
	mov	lr, ip
	stmia	lr!, {r0, r1, r2, r3}
	ldmia	r9, {r0, r1, r2, r3}
	stmia	lr, {r0, r1, r2, r3}
	mov	r0, #48
	bl	_Znaj
	mov	r5, r0
	ldmia	r8!, {r0, r1, r2, r3}
	mov	ip, r5
	stmia	ip!, {r0, r1, r2, r3}
	ldmia	r8!, {r0, r1, r2, r3}
	mov	lr, ip
	stmia	lr!, {r0, r1, r2, r3}
	ldmia	r8, {r0, r1, r2, r3}
	stmia	lr, {r0, r1, r2, r3}
	ldrb	r1, [sp, #12]	@ zero_extendqisi2
	cmp	r1, #0
	bne	.L1446
.L1441:
	mov	r1, #32768
	mov	r0, #0
	add	r3, r1, #116
	str	r5, [sp, #20]
	mov	r2, #3
	mov	r5, #1
	str	r0, [sp, #28]
	str	r3, [sp, #32]
	str	r2, [sp, #36]
	strb	r5, [sp, #12]
	str	r0, [sp, #24]
	bl	glPushMatrix
	add	r5, r4, #20
	ldr	ip, [r4, #20]
	mov	r0, r5
	ldr	ip, [ip, #8]
	mov	lr, pc
	bx	ip
	mov	r0, r5
	ldr	r5, [r4, #20]
	ldr	ip, [r5, #12]
	mov	lr, pc
	bx	ip
	mov	r0, #2896
	bl	glDisable
	mov	lr, #3040
	add	r0, lr, #2
	bl	glEnable
	mov	r0, #768
	mov	r2, r0
	add	r1, r2, #3
	add	r0, r0, #2
	bl	glBlendFunc
	ldr	r0, [r4, #44]
	bl	__aeabi_i2f
	mov	r1, #-1090519040
	bl	__aeabi_fmul
	mov	r5, r0
	ldr	r0, [r4, #48]
	bl	__aeabi_i2f
	mov	r1, #-1090519040
	bl	__aeabi_fmul
	mov	r2, #0
	mov	r1, r0
	mov	r0, r5
	bl	glTranslatef
	add	r0, sp, #296
	ldr	r3, [r0, #-292]!
	ldr	ip, [r3, #8]
	mov	lr, pc
	bx	ip
	ldr	r2, [sp, #8]
	cmp	r2, #0
	ble	.L1437
	ldrb	r1, [sp, #12]	@ zero_extendqisi2
	cmp	r1, #0
	bne	.L1447
.L1437:
	add	r0, sp, #296
	ldr	ip, [r0, #-292]!
	ldr	ip, [ip, #12]
	mov	lr, pc
	bx	ip
	bl	glPopMatrix
.LEHE78:
	add	r0, sp, #296
	str	sl, [r0, #-292]!
.LEHB79:
	bl	_ZN12QuDrawObjectD2Ev
.LEHE79:
	add	sp, sp, #296
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L1447:
	ldr	r0, [sp, #136]
	mov	r1, #0
.LEHB80:
	bl	glDrawArrays
.LEHE80:
	b	.L1437
.L1446:
	ldr	r0, [sp, #20]
	cmp	r0, #0
	beq	.L1441
	bl	_ZdaPv
	b	.L1441
.L1443:
	mov	r4, r0
.L1439:
	add	r0, sp, #4
	bl	_ZN17QuImageDrawObjectD1Ev
	mov	r0, r4
.LEHB81:
	bl	__cxa_end_cleanup
.LEHE81:
.L1442:
.L1430:
	mov	r4, r0
	mov	r1, #0
	mov	r0, r5
	ldr	r2, [sp, #292]
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
	b	.L1439
.L1449:
	.align	2
.L1448:
	.word	_ZTV17QuImageDrawObject+8
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA3858:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3858-.LLSDACSB3858
.LLSDACSB3858:
	.uleb128 .LEHB75-.LFB3858
	.uleb128 .LEHE75-.LEHB75
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB76-.LFB3858
	.uleb128 .LEHE76-.LEHB76
	.uleb128 .L1443-.LFB3858
	.uleb128 0x0
	.uleb128 .LEHB77-.LFB3858
	.uleb128 .LEHE77-.LEHB77
	.uleb128 .L1442-.LFB3858
	.uleb128 0x0
	.uleb128 .LEHB78-.LFB3858
	.uleb128 .LEHE78-.LEHB78
	.uleb128 .L1443-.LFB3858
	.uleb128 0x0
	.uleb128 .LEHB79-.LFB3858
	.uleb128 .LEHE79-.LEHB79
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB80-.LFB3858
	.uleb128 .LEHE80-.LEHB80
	.uleb128 .L1443-.LFB3858
	.uleb128 0x0
	.uleb128 .LEHB81-.LFB3858
	.uleb128 .LEHE81-.LEHB81
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE3858:
	.fnend
	.size	_ZN23QuAbsScreenIntBoxButton16drawRotatedImageE15QuStupidPointerI11QuBaseImageE15GDeviceRotation, .-_ZN23QuAbsScreenIntBoxButton16drawRotatedImageE15QuStupidPointerI11QuBaseImageE15GDeviceRotation
	.section	.text._ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueENS_17_Rb_tree_iteratorISC_NS_16_Nonconst_traitsISC_EEEERKSC_,"axG",%progbits,_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueENS_17_Rb_tree_iteratorISC_NS_16_Nonconst_traitsISC_EEEERKSC_,comdat
	.align	2
	.weak	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueENS_17_Rb_tree_iteratorISC_NS_16_Nonconst_traitsISC_EEEERKSC_
	.hidden	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueENS_17_Rb_tree_iteratorISC_NS_16_Nonconst_traitsISC_EEEERKSC_
	.type	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueENS_17_Rb_tree_iteratorISC_NS_16_Nonconst_traitsISC_EEEERKSC_, %function
_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueENS_17_Rb_tree_iteratorISC_NS_16_Nonconst_traitsISC_EEEERKSC_:
	.fnstart
.LFB5552:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	ip, [r1, #0]
	ldr	r4, [r2, #0]
	mov	r7, r2
	ldr	r2, [ip, #8]
	cmp	r4, r2
	.pad #52
	sub	sp, sp, #52
	mov	r6, r1
	mov	r9, r0
	mov	r5, r3
	beq	.L1485
	cmp	r4, ip
	beq	.L1486
	mov	r0, r4
	bl	_ZN4_STL10_Rb_globalIbE12_M_decrementEPNS_18_Rb_tree_node_baseE
	str	r0, [sp, #12]
	ldr	r4, [r7, #0]
	ldr	r8, [r5, #0]
	ldr	r1, [r4, #16]
	ldr	fp, [r5, #4]
	ldr	sl, [r4, #20]
	rsb	fp, r8, fp
	rsb	sl, r1, sl
	cmp	sl, fp
	movlt	r2, sl
	movge	r2, fp
	mov	r0, r8
	bl	memcmp
	cmp	r0, #0
	bne	.L1465
	cmp	fp, sl
	blt	.L1467
.L1466:
	mov	r0, r4
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	mov	sl, #0
	cmp	sl, #0
	mov	fp, #1
	mov	r8, r0
	beq	.L1487
.L1473:
	cmp	fp, #0
	ldrne	r4, [r7, #0]
	bne	.L1474
.L1476:
	mov	r1, r6
	mov	r2, r5
	add	r0, sp, #16
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_
	ldr	r1, [sp, #16]
	str	r1, [r9, #0]
.L1450:
	mov	r0, r9
	add	sp, sp, #52
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L1465:
	bge	.L1466
.L1467:
	ldr	r3, [sp, #12]
	add	r0, r3, #16
	ldmia	r0, {r0, lr}	@ phole ldm
	rsb	sl, r0, lr
	cmp	fp, sl
	movlt	r2, fp
	movge	r2, sl
	mov	r1, r8
	bl	memcmp
	cmp	r0, #0
	bne	.L1468
	cmp	fp, sl
	bgt	.L1470
.L1469:
	mov	r0, r4
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	mov	sl, #1
	cmp	sl, #0
	mov	fp, #0
	mov	r8, r0
	bne	.L1473
.L1487:
	ldr	r4, [r7, #0]
	add	r7, r6, #8
	mov	r0, r7
	add	r1, r4, #16
	mov	r2, r5
	bl	_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_
	cmp	r0, #0
	beq	.L1474
	ldr	r0, [r6, #0]
	cmp	r0, r8
	beq	.L1475
	mov	r0, r7
	mov	r1, r5
	add	r2, r8, #16
	bl	_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_
	cmp	r0, #0
	beq	.L1476
.L1475:
	ldr	r2, [r4, #12]
	cmp	r2, #0
	movne	r2, r8
	bne	.L1482
	mov	r1, r6
	mov	r3, r4
	mov	r0, r9
	str	r5, [sp, #0]
	str	r4, [sp, #4]
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_
	b	.L1450
.L1474:
	str	r4, [r9, #0]
	b	.L1450
.L1468:
	bge	.L1469
.L1470:
	ldr	ip, [sp, #12]
	ldr	r2, [ip, #12]
	cmp	r2, #0
	beq	.L1484
.L1471:
	mov	r2, r4
.L1482:
	mov	ip, #0
	mov	r1, r6
	mov	r0, r9
	mov	r3, r2
	stmia	sp, {r5, ip}	@ phole stm
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_
	b	.L1450
.L1484:
	mov	r1, r6
.L1483:
	mov	r3, ip
	mov	r0, r9
	stmia	sp, {r5, ip}	@ phole stm
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_
	b	.L1450
.L1485:
	ldr	r3, [r1, #4]
	cmp	r3, #0
	bne	.L1452
	add	r0, sp, #40
	mov	r2, r5
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_
	ldr	r0, [sp, #40]
	str	r0, [r9, #0]
	b	.L1450
.L1452:
	add	r8, r1, #8
	add	sl, r4, #16
	mov	r0, r8
	mov	r1, r5
	mov	r2, sl
	bl	_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_
	subs	fp, r0, #0
	bne	.L1471
	mov	r1, sl
	mov	r0, r8
	mov	r2, r5
	bl	_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_
	cmp	r0, #0
	beq	.L1474
	mov	r0, r4
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r1, [r6, #0]
	cmp	r1, r0
	mov	r4, r0
	beq	.L1488
	mov	r0, r8
	mov	r1, r5
	add	r2, r4, #16
	bl	_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_
	cmp	r0, #0
	beq	.L1459
	ldr	ip, [r7, #0]
	ldr	r2, [ip, #12]
	cmp	r2, #0
	beq	.L1484
	mov	r2, r4
	mov	r1, r6
	mov	r0, r9
	mov	r3, r4
	stmia	sp, {r5, fp}	@ phole stm
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_
	b	.L1450
.L1486:
	ldr	r7, [r4, #12]
	add	r0, r1, #8
	mov	r2, r3
	add	r1, r7, #16
	bl	_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_
	cmp	r0, #0
	mov	r1, r6
	bne	.L1489
	mov	r2, r5
	add	r0, sp, #24
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_
	ldr	r2, [sp, #24]
	str	r2, [r9, #0]
	b	.L1450
.L1489:
	mov	r3, r7
	mov	r0, r9
	mov	r2, #0
	str	r5, [sp, #0]
	str	r4, [sp, #4]
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_
	b	.L1450
.L1459:
	mov	r1, r6
	mov	r2, r5
	add	r0, sp, #32
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_
	ldr	ip, [sp, #32]
	str	ip, [r9, #0]
	b	.L1450
.L1488:
	ldr	ip, [r7, #0]
	mov	r1, r6
	mov	r2, fp
	b	.L1483
	.fnend
	.size	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueENS_17_Rb_tree_iteratorISC_NS_16_Nonconst_traitsISC_EEEERKSC_, .-_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueENS_17_Rb_tree_iteratorISC_NS_16_Nonconst_traitsISC_EEEERKSC_
	.section	.text._ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_,"axG",%progbits,_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_,comdat
	.align	2
	.weak	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_
	.hidden	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_
	.type	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_, %function
_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_:
	.fnstart
.LFB4760:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	r9, [r0, #0]
	ldr	r4, [r9, #4]
	cmp	r4, #0
	.pad #44
	sub	sp, sp, #44
	mov	fp, r0
	mov	sl, r1
	moveq	r4, r9
	beq	.L1496
	ldr	r8, [r1, #0]
	ldr	r6, [r1, #4]
	mov	r7, r9
	rsb	r6, r8, r6
.L1497:
	add	r0, r4, #16
	ldmia	r0, {r0, r5}	@ phole ldm
	rsb	r5, r0, r5
	cmp	r6, r5
	movlt	r2, r6
	movge	r2, r5
	mov	r1, r8
	bl	memcmp
	cmp	r0, #0
	bne	.L1492
	cmp	r5, r6
	blt	.L1494
.L1493:
	ldr	r3, [r4, #8]
	cmp	r3, #0
	beq	.L1496
.L1519:
	mov	r7, r4
	mov	r4, r3
	b	.L1497
.L1492:
	bge	.L1493
.L1494:
	ldr	r3, [r4, #12]
	cmp	r3, #0
	mov	r4, r7
	bne	.L1519
.L1496:
	cmp	r4, r9
	beq	.L1499
	ldr	r0, [sl, #0]
	ldr	r1, [r4, #16]
	ldr	r3, [sl, #4]
	ldr	r2, [r4, #20]
	rsb	r6, r0, r3
	rsb	r5, r1, r2
	cmp	r5, r6
	movlt	r2, r5
	movge	r2, r6
	bl	memcmp
	cmp	r0, #0
	mov	r0, r4
	bne	.L1500
	cmp	r6, r5
	blt	.L1499
.L1501:
	add	r0, r0, #28
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L1500:
	bge	.L1501
.L1499:
	mov	r1, #0
	add	r6, sp, #40
	str	r1, [r6, #-16]!
	mov	r2, #1
	mov	r0, r6
	add	r5, sp, #4
.LEHB82:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE82:
	mov	r1, sl
	mov	r0, r5
.LEHB83:
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
.LEHE83:
	mov	ip, #0
	add	r0, r5, #12
	add	r1, sp, #24
	ldmia	r1, {r1, r2}	@ phole ldm
	str	ip, [sp, #16]
.LEHB84:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE84:
.L1504:
	mov	r1, fp
	add	r0, sp, #36
	add	r2, sp, #32
	mov	r3, r5
	str	r4, [sp, #32]
.LEHB85:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueENS_17_Rb_tree_iteratorISC_NS_16_Nonconst_traitsISC_EEEERKSC_
.LEHE85:
	add	r0, r5, #12
	mov	r1, #0
	ldr	r2, [sp, #20]
	ldr	r4, [sp, #36]
.LEHB86:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE86:
	ldr	r0, [sp, #4]
	cmp	r0, #0
	blne	free
.L1507:
	mov	r0, r6
	mov	r1, #0
	ldr	r2, [sp, #28]
.LEHB87:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE87:
	mov	r0, r4
	b	.L1501
.L1516:
	mov	r4, r0
.L1510:
	mov	r0, r6
	mov	r1, #0
	ldr	r2, [sp, #28]
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
	mov	r0, r4
.LEHB88:
	bl	__cxa_end_cleanup
.LEHE88:
.L1514:
.L1518:
.L1508:
	mov	r4, r0
	mov	r0, r5
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	b	.L1510
.L1515:
.L1509:
	mov	r4, r0
	mov	r0, r5
	bl	_ZN4_STL4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageEED1Ev
	b	.L1510
.L1513:
	b	.L1518
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA4760:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4760-.LLSDACSB4760
.LLSDACSB4760:
	.uleb128 .LEHB82-.LFB4760
	.uleb128 .LEHE82-.LEHB82
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB83-.LFB4760
	.uleb128 .LEHE83-.LEHB83
	.uleb128 .L1516-.LFB4760
	.uleb128 0x0
	.uleb128 .LEHB84-.LFB4760
	.uleb128 .LEHE84-.LEHB84
	.uleb128 .L1514-.LFB4760
	.uleb128 0x0
	.uleb128 .LEHB85-.LFB4760
	.uleb128 .LEHE85-.LEHB85
	.uleb128 .L1515-.LFB4760
	.uleb128 0x0
	.uleb128 .LEHB86-.LFB4760
	.uleb128 .LEHE86-.LEHB86
	.uleb128 .L1513-.LFB4760
	.uleb128 0x0
	.uleb128 .LEHB87-.LFB4760
	.uleb128 .LEHE87-.LEHB87
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB88-.LFB4760
	.uleb128 .LEHE88-.LEHB88
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE4760:
	.fnend
	.size	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_, .-_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_
	.section	.text._ZN14QuImageManager11setFlyImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE,"axG",%progbits,_ZN14QuImageManager11setFlyImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE,comdat
	.align	2
	.weak	_ZN14QuImageManager11setFlyImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.hidden	_ZN14QuImageManager11setFlyImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.type	_ZN14QuImageManager11setFlyImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE, %function
_ZN14QuImageManager11setFlyImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE:
	.fnstart
.LFB3193:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.pad #52
	sub	sp, sp, #52
	str	r1, [sp, #0]
	mov	r9, r0
	mov	r1, r2
	add	r0, sp, #20
	mov	fp, r2
.LEHB89:
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
	ldr	r3, [sp, #0]
	ldr	sl, [r3, #0]
	ldr	r4, [sl, #4]
	cmp	r4, #0
	ldr	r8, [sp, #20]
	beq	.L1521
	ldr	r0, [sp, #24]
	str	r0, [sp, #4]
	mov	r7, sl
	rsb	r6, r8, r0
.L1527:
	add	r0, r4, #16
	ldmia	r0, {r0, r5}	@ phole ldm
	rsb	r5, r0, r5
	cmp	r6, r5
	movlt	r2, r6
	movge	r2, r5
	mov	r1, r8
	bl	memcmp
	cmp	r0, #0
	bne	.L1522
	cmp	r5, r6
	blt	.L1524
.L1523:
	ldr	r3, [r4, #8]
	cmp	r3, #0
	beq	.L1526
.L1568:
	mov	r7, r4
	mov	r4, r3
	b	.L1527
.L1522:
	bge	.L1523
.L1524:
	ldr	r3, [r4, #12]
	cmp	r3, #0
	mov	r4, r7
	bne	.L1568
.L1526:
	cmp	r4, sl
	beq	.L1528
	add	r1, r4, #16
	ldmia	r1, {r1, r2}	@ phole ldm
	ldr	ip, [sp, #4]
	rsb	r5, r1, r2
	rsb	r6, r8, ip
	cmp	r5, r6
	movlt	r2, r5
	movge	r2, r6
	mov	r0, r8
	bl	memcmp
	cmp	r0, #0
	bne	.L1529
	cmp	r6, r5
	blt	.L1521
.L1530:
	cmp	r8, #0
	movne	r0, r8
	blne	free
.L1531:
	cmp	r4, sl
	beq	.L1532
	ldr	r0, [sp, #0]
	mov	r1, fp
.L1567:
	bl	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_
	mov	ip, r0
	mov	r0, #0
	str	r0, [r9, #0]
	ldr	r2, [ip, #4]
	mov	r0, r9
	ldr	r1, [ip, #0]
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
	mov	r0, r9
	add	sp, sp, #52
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L1529:
	bge	.L1530
.L1521:
	mov	r4, sl
.L1528:
	mov	sl, r4
	b	.L1530
.L1532:
	ldr	r4, [sp, #0]
	mov	r1, fp
	add	r0, r4, #12
	bl	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_
	mov	r1, fp
	mov	r8, r0
	ldr	r0, [sp, #0]
	bl	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_
	mov	r7, r0
	mov	r0, #48
	bl	_Znwj
.LEHE89:
	ldr	r3, .L1570
	mov	r5, r0
	str	r3, [r5], #28
	mov	r1, #0
	mvn	r6, #0
	str	r1, [r0, #28]
	strb	r1, [r0, #4]
	mov	r4, r0
	str	r6, [r0, #8]
	mov	r1, #1
	mov	r0, r5
.LEHB90:
	bl	T.1802
.LEHE90:
	mov	lr, #0
	str	lr, [r4, #44]
	str	lr, [r4, #36]
	str	lr, [r4, #40]
	mov	r0, #8
	bl	malloc
	cmp	r0, #0
	beq	.L1569
.L1537:
	mov	r6, #0
	add	r2, r0, #8
	add	r5, sp, #48
	str	r2, [r4, #44]
	str	r0, [r4, #36]
	str	r0, [r4, #40]
	strb	r6, [r0, #0]
	str	r6, [r5, #-8]!
	mov	r1, r4
	mov	r0, r5
	mov	r2, #1
	add	r4, sp, #48
.LEHB91:
	bl	_ZN15QuStupidPointerI10QuPngImageE3setEPS0_i
.LEHE91:
	str	r6, [r4, #-16]!
	add	r1, sp, #40
	ldmia	r1, {r1, r2}	@ phole ldm
	mov	r0, r4
.LEHB92:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE92:
	mov	r0, r7
	add	r1, sp, #32
	ldmia	r1, {r1, r2}	@ phole ldm
.LEHB93:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
	mov	r0, r8
	ldmia	r7, {r1, r2}	@ phole ldm
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE93:
	mov	r0, r4
	mov	r1, #0
	ldr	r2, [sp, #36]
.LEHB94:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE94:
	mov	r0, r5
	ldr	r2, [sp, #44]
	mov	r1, #0
.LEHB95:
	bl	_ZN15QuStupidPointerI10QuPngImageE3setEPS0_i
	mov	r1, fp
	ldr	r0, [sp, #0]
	bl	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_
	add	r4, sp, #8
	ldr	r5, [r0, #0]
	mov	r1, fp
	mov	r0, r4
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
.LEHE95:
	mov	r0, r5
	mov	r1, r4
.LEHB96:
	bl	_ZN10QuPngImage9loadImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
.LEHE96:
	ldr	r0, [sp, #8]
	cmp	r0, #0
	blne	free
.L1554:
	mov	r0, r5
.LEHB97:
	bl	_ZN10QuPngImage18finishLoadingImageEv
.LEHE97:
	ldr	r0, [sp, #0]
	mov	r1, fp
	b	.L1567
.L1569:
	mov	r0, #8
.LEHB98:
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
.LEHE98:
	b	.L1537
.L1561:
	mov	r6, r0
.L1550:
	mov	r0, r5
	mov	r1, #0
	ldr	r2, [sp, #44]
	bl	_ZN15QuStupidPointerI10QuPngImageE3setEPS0_i
	mov	r0, r6
.LEHB99:
	bl	__cxa_end_cleanup
.LEHE99:
.L1560:
.L1548:
	mov	r6, r0
	mov	r1, #0
	mov	r0, r4
	ldr	r2, [sp, #36]
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
	b	.L1550
.L1558:
	mov	r6, r0
.L1543:
.L1562:
.L1545:
	ldr	r7, .L1570+4
	mov	r0, r4
	str	r7, [r4, #0]
	bl	_ZN11QuBaseImage7destroyEv
	mov	r0, r4
	bl	_ZdlPv
	mov	r0, r6
.LEHB100:
	bl	__cxa_end_cleanup
.L1559:
.L1553:
	mov	r8, r0
	mov	r0, r4
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r8
	bl	__cxa_end_cleanup
.LEHE100:
.L1556:
.L1539:
	ldr	r3, [r4, #36]
	cmp	r3, #0
	mov	r6, r0
	movne	r0, r3
	blne	free
.L1540:
.L1557:
.L1541:
	mov	r0, r5
	ldr	r1, [r4, #32]
	bl	T.1802
	b	.L1543
.L1571:
	.align	2
.L1570:
	.word	_ZTV10QuPngImage+8
	.word	_ZTV11QuBaseImage+8
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA3193:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3193-.LLSDACSB3193
.LLSDACSB3193:
	.uleb128 .LEHB89-.LFB3193
	.uleb128 .LEHE89-.LEHB89
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB90-.LFB3193
	.uleb128 .LEHE90-.LEHB90
	.uleb128 .L1558-.LFB3193
	.uleb128 0x0
	.uleb128 .LEHB91-.LFB3193
	.uleb128 .LEHE91-.LEHB91
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB92-.LFB3193
	.uleb128 .LEHE92-.LEHB92
	.uleb128 .L1561-.LFB3193
	.uleb128 0x0
	.uleb128 .LEHB93-.LFB3193
	.uleb128 .LEHE93-.LEHB93
	.uleb128 .L1560-.LFB3193
	.uleb128 0x0
	.uleb128 .LEHB94-.LFB3193
	.uleb128 .LEHE94-.LEHB94
	.uleb128 .L1561-.LFB3193
	.uleb128 0x0
	.uleb128 .LEHB95-.LFB3193
	.uleb128 .LEHE95-.LEHB95
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB96-.LFB3193
	.uleb128 .LEHE96-.LEHB96
	.uleb128 .L1559-.LFB3193
	.uleb128 0x0
	.uleb128 .LEHB97-.LFB3193
	.uleb128 .LEHE97-.LEHB97
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB98-.LFB3193
	.uleb128 .LEHE98-.LEHB98
	.uleb128 .L1556-.LFB3193
	.uleb128 0x0
	.uleb128 .LEHB99-.LFB3193
	.uleb128 .LEHE99-.LEHB99
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB100-.LFB3193
	.uleb128 .LEHE100-.LEHB100
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE3193:
	.fnend
	.size	_ZN14QuImageManager11setFlyImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE, .-_ZN14QuImageManager11setFlyImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.section	.text._ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueENS_17_Rb_tree_iteratorISC_NS_16_Nonconst_traitsISC_EEEERKSC_,"axG",%progbits,_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueENS_17_Rb_tree_iteratorISC_NS_16_Nonconst_traitsISC_EEEERKSC_,comdat
	.align	2
	.weak	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueENS_17_Rb_tree_iteratorISC_NS_16_Nonconst_traitsISC_EEEERKSC_
	.hidden	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueENS_17_Rb_tree_iteratorISC_NS_16_Nonconst_traitsISC_EEEERKSC_
	.type	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueENS_17_Rb_tree_iteratorISC_NS_16_Nonconst_traitsISC_EEEERKSC_, %function
_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueENS_17_Rb_tree_iteratorISC_NS_16_Nonconst_traitsISC_EEEERKSC_:
	.fnstart
.LFB5475:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	ip, [r1, #0]
	ldr	r4, [r2, #0]
	mov	r7, r2
	ldr	r2, [ip, #8]
	cmp	r4, r2
	.pad #52
	sub	sp, sp, #52
	mov	r6, r1
	mov	r9, r0
	mov	r5, r3
	beq	.L1607
	cmp	r4, ip
	beq	.L1608
	mov	r0, r4
	bl	_ZN4_STL10_Rb_globalIbE12_M_decrementEPNS_18_Rb_tree_node_baseE
	str	r0, [sp, #12]
	ldr	r4, [r7, #0]
	ldr	r8, [r5, #0]
	ldr	r1, [r4, #16]
	ldr	fp, [r5, #4]
	ldr	sl, [r4, #20]
	rsb	fp, r8, fp
	rsb	sl, r1, sl
	cmp	sl, fp
	movlt	r2, sl
	movge	r2, fp
	mov	r0, r8
	bl	memcmp
	cmp	r0, #0
	bne	.L1587
	cmp	fp, sl
	blt	.L1589
.L1588:
	mov	r0, r4
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	mov	sl, #0
	cmp	sl, #0
	mov	fp, #1
	mov	r8, r0
	beq	.L1609
.L1595:
	cmp	fp, #0
	ldrne	r4, [r7, #0]
	bne	.L1596
.L1598:
	mov	r1, r6
	mov	r2, r5
	add	r0, sp, #16
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_
	ldr	r1, [sp, #16]
	str	r1, [r9, #0]
.L1572:
	mov	r0, r9
	add	sp, sp, #52
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L1587:
	bge	.L1588
.L1589:
	ldr	r3, [sp, #12]
	add	r0, r3, #16
	ldmia	r0, {r0, lr}	@ phole ldm
	rsb	sl, r0, lr
	cmp	fp, sl
	movlt	r2, fp
	movge	r2, sl
	mov	r1, r8
	bl	memcmp
	cmp	r0, #0
	bne	.L1590
	cmp	fp, sl
	bgt	.L1592
.L1591:
	mov	r0, r4
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	mov	sl, #1
	cmp	sl, #0
	mov	fp, #0
	mov	r8, r0
	bne	.L1595
.L1609:
	ldr	r4, [r7, #0]
	add	r7, r6, #8
	mov	r0, r7
	add	r1, r4, #16
	mov	r2, r5
	bl	_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_
	cmp	r0, #0
	beq	.L1596
	ldr	r0, [r6, #0]
	cmp	r0, r8
	beq	.L1597
	mov	r0, r7
	mov	r1, r5
	add	r2, r8, #16
	bl	_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_
	cmp	r0, #0
	beq	.L1598
.L1597:
	ldr	r2, [r4, #12]
	cmp	r2, #0
	movne	r2, r8
	bne	.L1604
	mov	r1, r6
	mov	r3, r4
	mov	r0, r9
	str	r5, [sp, #0]
	str	r4, [sp, #4]
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_
	b	.L1572
.L1596:
	str	r4, [r9, #0]
	b	.L1572
.L1590:
	bge	.L1591
.L1592:
	ldr	ip, [sp, #12]
	ldr	r2, [ip, #12]
	cmp	r2, #0
	beq	.L1606
.L1593:
	mov	r2, r4
.L1604:
	mov	ip, #0
	mov	r1, r6
	mov	r0, r9
	mov	r3, r2
	stmia	sp, {r5, ip}	@ phole stm
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_
	b	.L1572
.L1606:
	mov	r1, r6
.L1605:
	mov	r3, ip
	mov	r0, r9
	stmia	sp, {r5, ip}	@ phole stm
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_
	b	.L1572
.L1607:
	ldr	r3, [r1, #4]
	cmp	r3, #0
	bne	.L1574
	add	r0, sp, #40
	mov	r2, r5
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_
	ldr	r0, [sp, #40]
	str	r0, [r9, #0]
	b	.L1572
.L1574:
	add	r8, r1, #8
	add	sl, r4, #16
	mov	r0, r8
	mov	r1, r5
	mov	r2, sl
	bl	_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_
	subs	fp, r0, #0
	bne	.L1593
	mov	r1, sl
	mov	r0, r8
	mov	r2, r5
	bl	_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_
	cmp	r0, #0
	beq	.L1596
	mov	r0, r4
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r1, [r6, #0]
	cmp	r1, r0
	mov	r4, r0
	beq	.L1610
	mov	r0, r8
	mov	r1, r5
	add	r2, r4, #16
	bl	_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_
	cmp	r0, #0
	beq	.L1581
	ldr	ip, [r7, #0]
	ldr	r2, [ip, #12]
	cmp	r2, #0
	beq	.L1606
	mov	r2, r4
	mov	r1, r6
	mov	r0, r9
	mov	r3, r4
	stmia	sp, {r5, fp}	@ phole stm
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_
	b	.L1572
.L1608:
	ldr	r7, [r4, #12]
	add	r0, r1, #8
	mov	r2, r3
	add	r1, r7, #16
	bl	_ZNK4_STL4lessINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEEEclERKS6_S9_
	cmp	r0, #0
	mov	r1, r6
	bne	.L1611
	mov	r2, r5
	add	r0, sp, #24
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_
	ldr	r2, [sp, #24]
	str	r2, [r9, #0]
	b	.L1572
.L1611:
	mov	r3, r7
	mov	r0, r9
	mov	r2, #0
	str	r5, [sp, #0]
	str	r4, [sp, #4]
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE9_M_insertEPNS_18_Rb_tree_node_baseESK_RKSC_SK_
	b	.L1572
.L1581:
	mov	r1, r6
	mov	r2, r5
	add	r0, sp, #32
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueERKSC_
	ldr	ip, [sp, #32]
	str	ip, [r9, #0]
	b	.L1572
.L1610:
	ldr	ip, [r7, #0]
	mov	r1, r6
	mov	r2, fp
	b	.L1605
	.fnend
	.size	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueENS_17_Rb_tree_iteratorISC_NS_16_Nonconst_traitsISC_EEEERKSC_, .-_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueENS_17_Rb_tree_iteratorISC_NS_16_Nonconst_traitsISC_EEEERKSC_
	.section	.text._ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_,"axG",%progbits,_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_,comdat
	.align	2
	.weak	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_
	.hidden	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_
	.type	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_, %function
_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_:
	.fnstart
.LFB4645:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	r9, [r0, #0]
	ldr	r4, [r9, #4]
	cmp	r4, #0
	.pad #44
	sub	sp, sp, #44
	mov	fp, r0
	mov	sl, r1
	moveq	r4, r9
	beq	.L1618
	ldr	r8, [r1, #0]
	ldr	r6, [r1, #4]
	mov	r7, r9
	rsb	r6, r8, r6
.L1619:
	add	r0, r4, #16
	ldmia	r0, {r0, r5}	@ phole ldm
	rsb	r5, r0, r5
	cmp	r6, r5
	movlt	r2, r6
	movge	r2, r5
	mov	r1, r8
	bl	memcmp
	cmp	r0, #0
	bne	.L1614
	cmp	r5, r6
	blt	.L1616
.L1615:
	ldr	r3, [r4, #8]
	cmp	r3, #0
	beq	.L1618
.L1640:
	mov	r7, r4
	mov	r4, r3
	b	.L1619
.L1614:
	bge	.L1615
.L1616:
	ldr	r3, [r4, #12]
	cmp	r3, #0
	mov	r4, r7
	bne	.L1640
.L1618:
	cmp	r4, r9
	beq	.L1621
	ldr	r0, [sl, #0]
	ldr	r1, [r4, #16]
	ldr	r3, [sl, #4]
	ldr	r2, [r4, #20]
	rsb	r6, r0, r3
	rsb	r5, r1, r2
	cmp	r5, r6
	movlt	r2, r5
	movge	r2, r6
	bl	memcmp
	cmp	r0, #0
	mov	r0, r4
	bne	.L1622
	cmp	r6, r5
	blt	.L1621
.L1623:
	add	r0, r0, #28
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L1622:
	bge	.L1623
.L1621:
	mov	r1, #0
	add	r6, sp, #40
	str	r1, [r6, #-16]!
	mov	r2, #1
	mov	r0, r6
	add	r5, sp, #4
.LEHB101:
	bl	_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i
.LEHE101:
	mov	r1, sl
	mov	r0, r5
.LEHB102:
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
.LEHE102:
	mov	ip, #0
	add	r0, r5, #12
	add	r1, sp, #24
	ldmia	r1, {r1, r2}	@ phole ldm
	str	ip, [sp, #16]
.LEHB103:
	bl	_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i
.LEHE103:
.L1626:
	mov	r1, fp
	add	r0, sp, #36
	add	r2, sp, #32
	mov	r3, r5
	str	r4, [sp, #32]
.LEHB104:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE13insert_uniqueENS_17_Rb_tree_iteratorISC_NS_16_Nonconst_traitsISC_EEEERKSC_
.LEHE104:
	add	r0, r5, #12
	mov	r1, #0
	ldr	r2, [sp, #20]
	ldr	r4, [sp, #36]
.LEHB105:
	bl	_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i
.LEHE105:
	ldr	r0, [sp, #4]
	cmp	r0, #0
	blne	free
.L1628:
	mov	r0, r6
	mov	r1, #0
	ldr	r2, [sp, #28]
.LEHB106:
	bl	_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i
.LEHE106:
	mov	r0, r4
	b	.L1623
.L1637:
	mov	r4, r0
.L1631:
	mov	r0, r6
	mov	r1, #0
	ldr	r2, [sp, #28]
	bl	_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i
	mov	r0, r4
.LEHB107:
	bl	__cxa_end_cleanup
.LEHE107:
.L1635:
.L1639:
.L1629:
	mov	r4, r0
	mov	r0, r5
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	b	.L1631
.L1636:
.L1630:
	mov	r4, r0
	mov	r0, r5
	bl	_ZN4_STL4pairIKNS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructEED1Ev
	b	.L1631
.L1634:
	b	.L1639
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA4645:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4645-.LLSDACSB4645
.LLSDACSB4645:
	.uleb128 .LEHB101-.LFB4645
	.uleb128 .LEHE101-.LEHB101
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB102-.LFB4645
	.uleb128 .LEHE102-.LEHB102
	.uleb128 .L1637-.LFB4645
	.uleb128 0x0
	.uleb128 .LEHB103-.LFB4645
	.uleb128 .LEHE103-.LEHB103
	.uleb128 .L1635-.LFB4645
	.uleb128 0x0
	.uleb128 .LEHB104-.LFB4645
	.uleb128 .LEHE104-.LEHB104
	.uleb128 .L1636-.LFB4645
	.uleb128 0x0
	.uleb128 .LEHB105-.LFB4645
	.uleb128 .LEHE105-.LEHB105
	.uleb128 .L1634-.LFB4645
	.uleb128 0x0
	.uleb128 .LEHB106-.LFB4645
	.uleb128 .LEHE106-.LEHB106
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB107-.LFB4645
	.uleb128 .LEHE107-.LEHB107
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE4645:
	.fnend
	.size	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_, .-_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_
	.section	.text._ZN19QuApRawSoundManager9loadSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE,"axG",%progbits,_ZN19QuApRawSoundManager9loadSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE,comdat
	.align	2
	.weak	_ZN19QuApRawSoundManager9loadSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.hidden	_ZN19QuApRawSoundManager9loadSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.type	_ZN19QuApRawSoundManager9loadSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE, %function
_ZN19QuApRawSoundManager9loadSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE:
	.fnstart
.LFB2816:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 96
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.pad #100
	sub	sp, sp, #100
	mov	fp, r1
	mov	r9, r0
	mov	r1, r2
	add	r0, sp, #68
.LEHB108:
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
.LEHE108:
	ldr	sl, [fp, #4]
	ldr	r4, [sl, #4]
	cmp	r4, #0
	beq	.L1642
	ldr	r1, [sp, #72]
	ldr	r8, [sp, #68]
	str	r1, [sp, #4]
	mov	r7, sl
	rsb	r6, r8, r1
.L1648:
	add	r0, r4, #16
	ldmia	r0, {r0, r5}	@ phole ldm
	rsb	r5, r0, r5
	cmp	r6, r5
	movlt	r2, r6
	movge	r2, r5
	mov	r1, r8
	bl	memcmp
	cmp	r0, #0
	bne	.L1643
	cmp	r5, r6
	blt	.L1645
.L1644:
	ldr	r3, [r4, #8]
	cmp	r3, #0
	beq	.L1647
.L1786:
	mov	r7, r4
	mov	r4, r3
	b	.L1648
.L1643:
	bge	.L1644
.L1645:
	ldr	r3, [r4, #12]
	cmp	r3, #0
	mov	r4, r7
	bne	.L1786
.L1647:
	cmp	sl, r4
	beq	.L1642
	ldr	r1, [r4, #16]
	ldr	r0, [r4, #20]
	ldr	r2, [sp, #4]
	rsb	r5, r1, r0
	rsb	r6, r8, r2
	cmp	r5, r6
	movlt	r2, r5
	movge	r2, r6
	mov	r0, r8
	bl	memcmp
	cmp	r0, #0
	bne	.L1649
	cmp	r6, r5
	bge	.L1650
.L1642:
	mov	r4, sl
.L1650:
	cmp	sl, r4
	beq	.L1651
	add	r0, fp, #4
	add	r1, sp, #68
.LEHB109:
	bl	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_
.L1785:
	ldmia	r0, {r1, r2}	@ phole ldm
	mov	r0, #0
	str	r0, [r9, #0]
	mov	r0, r9
	bl	_ZN15QuStupidPointerI22QuSoundStructInterfaceE3setEPS0_i
	ldr	r0, [sp, #68]
	cmp	r0, #0
	blne	free
.L1641:
	mov	r0, r9
	add	sp, sp, #100
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L1649:
	blt	.L1642
	b	.L1650
.L1651:
	add	fp, fp, #4
	mov	r0, fp
	add	r1, sp, #68
	bl	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_
	add	r7, sp, #56
	str	r0, [sp, #4]
	add	r1, sp, #68
	mov	r0, r7
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
.LEHE109:
	mov	r0, #48
.LEHB110:
	bl	_Znwj
.LEHE110:
	ldr	r6, .L1807
	ldr	ip, .L1807+4
	mov	r3, #0
	mov	r1, #1065353216
	add	sl, sp, #44
	mov	lr, #0
	str	r1, [r0, #20]	@ float
	mov	r4, r0
	strb	r3, [r0, #28]
	str	lr, [r0, #24]	@ float
	stmia	r0, {r6, ip}	@ phole stm
	str	r3, [r0, #8]
	str	r3, [r0, #12]
	str	r3, [r0, #16]
	mov	r1, r7
	mov	r0, sl
.LEHB111:
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
.LEHE111:
	ldr	r0, [sp, #44]
	ldr	r1, .L1807+8
.LEHB112:
	bl	s3eFileOpen
	mov	r6, r0
	bl	s3eFileGetSize
	cmn	r0, #1
	str	r0, [r4, #40]
	beq	.L1787
.L1655:
	bl	_Znaj
	str	r0, [r4, #36]
	ldr	r1, [r4, #40]
	mov	r2, #1
	mov	r3, r6
	bl	s3eFileRead
	mov	r0, r6
	bl	s3eFileClose
.LEHE112:
	ldr	r0, [sp, #44]
	cmp	r0, #0
	blne	free
.L1686:
	mov	r5, #43776
	add	lr, r5, #224
	mvn	r1, #0
	add	r5, sp, #96
	mov	r2, #1
	mov	ip, #0
	str	r1, [r4, #32]
	str	lr, [r4, #44]
	strb	r2, [r4, #29]
	str	ip, [r5, #-8]!
	mov	r1, r4
	mov	r0, r5
.LEHB113:
	bl	_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i
.LEHE113:
	ldr	r0, [sp, #4]
	add	r1, sp, #88
	ldmia	r1, {r1, r2}	@ phole ldm
.LEHB114:
	bl	_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i
.LEHE114:
	mov	r0, r5
	mov	r1, #0
	ldr	r2, [sp, #92]
.LEHB115:
	bl	_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i
.LEHE115:
	ldr	r0, [sp, #56]
	cmp	r0, #0
	blne	free
.L1694:
	mov	r0, fp
	add	r1, sp, #68
.LEHB116:
	bl	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_
.LEHE116:
	b	.L1785
.L1787:
	ldr	r0, .L1807+12
.LEHB117:
	bl	T.1796
	ldr	r8, .L1807+16
	str	r8, [sp, #80]
	ldr	r0, [r8, #0]
	ldr	r5, [r0, #-12]
	add	r0, r8, r5
	ldr	r3, [r0, #8]
	cmp	r3, #0
	mov	r5, r8
	strne	r8, [sp, #28]
	movne	r3, #0
	bne	.L1657
	ldr	r2, [r0, #88]
	cmp	r2, #0
	str	r8, [sp, #28]
	beq	.L1788
.L1658:
	ldr	r1, [r0, #92]
	cmp	r1, #0
	str	r1, [sp, #8]
	beq	.L1659
	ldr	ip, [r1, #0]
	ldr	r2, [ip, #-12]
	add	lr, r1, r2
	ldr	r3, [lr, #88]
	cmp	r3, #0
	beq	.L1659
	mov	r0, r3
	ldr	r1, [r3, #0]
	ldr	ip, [r1, #20]
	mov	lr, pc
	bx	ip
.LEHE117:
	cmn	r0, #1
	beq	.L1660
.L1783:
	ldr	r3, [r8, #0]
	ldr	lr, [r3, #-12]
	add	r0, r5, lr
.L1659:
	ldr	r2, [r0, #8]
	rsbs	r3, r2, #1
	movcc	r3, #0
.L1657:
	cmp	r3, #0
	strb	r3, [sp, #84]
	beq	.L1664
	ldr	r2, [r8, #0]
	ldr	lr, [r2, #-12]
	mov	r2, #0
	add	r5, r5, lr
	ldr	lr, [r5, #28]
	str	r2, [r5, #28]
	ldr	r3, [sp, #44]
	ldr	ip, [r8, #0]
	ldr	r1, [sp, #48]
	ldr	r0, [ip, #-12]
	rsb	ip, r3, r1
	str	ip, [sp, #24]
	ldr	r1, [r5, #4]
	ldr	r3, [sp, #24]
	and	r1, r1, #1
	cmp	r3, lr
	str	r1, [sp, #32]
	ldr	r5, [sp, #28]
	movcc	r1, r3
	ldr	ip, [sp, #32]
	rsbcc	lr, r1, lr
	add	r0, r5, r0
	ldr	r5, [r0, #88]
	strcs	r2, [sp, #20]
	strcc	lr, [sp, #20]
	cmp	ip, #0
	beq	.L1789
.L1667:
	ldr	ip, [r5, #0]
	mov	r0, r5
	ldr	r1, [sp, #44]
	ldr	r2, [sp, #24]
.LEHB118:
	ldr	ip, [ip, #44]
	mov	lr, pc
	bx	ip
.LEHE118:
	ldr	r3, [sp, #24]
	cmp	r3, r0
	movne	r2, #0
	moveq	r2, #1
	str	r2, [sp, #8]
.L1673:
	ldr	r0, [sp, #32]
	cmp	r0, #0
	beq	.L1675
	ldr	r1, [sp, #8]
	cmp	r1, #0
	beq	.L1664
	ldr	ip, [r8, #0]
	ldr	r0, [sp, #28]
	ldr	r3, [ip, #-12]
	ldr	ip, [sp, #20]
	add	r2, r0, r3
	ldrb	lr, [r2, #84]	@ zero_extendqisi2
	cmp	ip, #0
	str	lr, [sp, #12]
	beq	.L1676
	ldr	r2, [r5, #20]
	ldr	r0, [r5, #24]
	sub	r3, ip, #1
	and	lr, r3, #3
	mov	r1, #1
	cmp	r2, r0
	str	lr, [sp, #24]
	str	r1, [sp, #8]
	bcs	.L1790
	ldr	r1, [sp, #12]
	mov	ip, r2
	strb	r1, [ip], #1
	str	ip, [r5, #20]
.L1708:
	ldr	lr, [sp, #20]
	mov	r3, #1
	cmp	lr, #1
	str	r3, [sp, #16]
	bls	.L1769
	ldr	ip, [sp, #24]
	cmp	ip, #0
	beq	.L1679
	cmp	ip, r3
	beq	.L1757
	cmp	ip, #2
	beq	.L1758
	ldr	r0, [sp, #8]
	cmp	r0, #0
	beq	.L1711
	add	r2, r5, #20
	ldmia	r2, {r2, r3}	@ phole ldm
	cmp	r2, r3
	bcs	.L1791
	ldr	lr, [sp, #12]
	mov	r1, r2
	strb	lr, [r1], #1
	str	r1, [r5, #20]
.L1711:
	ldr	r3, [sp, #16]
	add	r0, r3, #1
	str	r0, [sp, #16]
.L1758:
	ldr	r2, [sp, #8]
	cmp	r2, #0
	beq	.L1715
	add	r2, r5, #20
	ldmia	r2, {r2, ip}	@ phole ldm
	cmp	r2, ip
	bcs	.L1792
	ldr	r0, [sp, #12]
	mov	lr, r2
	strb	r0, [lr], #1
	str	lr, [r5, #20]
.L1715:
	ldr	ip, [sp, #16]
	add	r2, ip, #1
	str	r2, [sp, #16]
.L1757:
	ldr	r3, [sp, #8]
	cmp	r3, #0
	beq	.L1719
	ldr	r2, [r5, #20]
	ldr	r1, [r5, #24]
	cmp	r2, r1
	bcs	.L1793
	ldr	ip, [sp, #12]
	mov	r0, r2
	strb	ip, [r0], #1
	str	r0, [r5, #20]
.L1719:
	add	r1, sp, #16
	ldmia	r1, {r1, r2}	@ phole ldm
	add	r3, r1, #1
	cmp	r2, r3
	str	r3, [sp, #16]
	bls	.L1769
.L1679:
	ldr	r2, [sp, #8]
	cmp	r2, #0
	beq	.L1677
	add	r2, r5, #20
	ldmia	r2, {r2, ip}	@ phole ldm
	cmp	r2, ip
	bcs	.L1678
	ldr	r1, [sp, #12]
	mov	lr, r2
	strb	r1, [lr], #1
	str	lr, [r5, #20]
.L1677:
	ldr	ip, [sp, #16]
	ldr	lr, [sp, #8]
	add	r0, ip, #1
	cmp	lr, #0
	str	r0, [sp, #16]
	beq	.L1723
	add	r2, r5, #20
	ldmia	r2, {r2, lr}	@ phole ldm
	cmp	r2, lr
	bcs	.L1794
	mov	r0, r2
	ldr	r2, [sp, #12]
	strb	r2, [r0], #1
	str	r0, [r5, #20]
.L1723:
	ldr	r1, [sp, #8]
	cmp	r1, #0
	beq	.L1726
	add	r2, r5, #20
	ldmia	r2, {r2, lr}	@ phole ldm
	cmp	r2, lr
	bcs	.L1795
	mov	r0, r2
	ldr	r2, [sp, #12]
	strb	r2, [r0], #1
	str	r0, [r5, #20]
.L1726:
	ldr	r1, [sp, #8]
	cmp	r1, #0
	beq	.L1729
	add	r2, r5, #20
	ldmia	r2, {r2, lr}	@ phole ldm
	cmp	r2, lr
	bcs	.L1796
	mov	r0, r2
	ldr	r2, [sp, #12]
	strb	r2, [r0], #1
	str	r0, [r5, #20]
.L1729:
	add	ip, sp, #16
	ldmia	ip, {ip, lr}	@ phole ldm
	add	r1, ip, #3
	cmp	lr, r1
	str	r1, [sp, #16]
	bhi	.L1679
.L1769:
	ldr	r5, [sp, #8]
	cmp	r5, #0
	bne	.L1676
.L1664:
	ldr	r1, [r8, #0]
	ldr	r5, [sp, #28]
	ldr	r0, [r1, #-12]
	add	r0, r5, r0
	ldr	lr, [r0, #88]
	ldr	ip, [r0, #8]
	cmp	lr, #0
	ldr	r2, [r0, #20]
	orr	r3, ip, #4
	orreq	r3, ip, #5
	tst	r3, r2
	str	r3, [r0, #8]
	bne	.L1797
.L1676:
	ldr	r5, [sp, #80]
	ldr	ip, [r5, #0]
	ldr	r3, [ip, #-12]
	add	r3, r5, r3
	ldr	r2, [r3, #4]
	tst	r2, #8192
	bne	.L1798
.L1681:
	ldr	r0, .L1807+16
.LEHB119:
	bl	_ZN4_STL4endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	ldr	r0, [r4, #40]
	b	.L1655
.L1798:
	ldr	r3, [r3, #88]
	cmp	r3, #0
	beq	.L1681
	mov	r0, r3
	ldr	r1, [r3, #0]
	ldr	ip, [r1, #20]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	bne	.L1681
	ldr	r3, [r5, #0]
	ldr	r0, [r3, #-12]
	add	r0, r5, r0
	ldr	r2, [r0, #8]
	ldr	r5, [r0, #20]
	orr	lr, r2, #1
	tst	lr, r5
	str	lr, [r0, #8]
	beq	.L1681
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
.LEHE119:
	b	.L1681
.L1797:
.LEHB120:
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
	b	.L1676
.L1675:
	ldr	r1, [sp, #8]
	cmp	r1, #0
	bne	.L1676
	b	.L1664
.L1789:
	ldr	r3, [sp, #20]
	ldrb	r0, [r0, #84]	@ zero_extendqisi2
	cmp	r3, #0
	str	r0, [sp, #12]
	beq	.L1667
	ldr	r0, [sp, #20]
	add	r2, r5, #20
	ldmia	r2, {r2, r3}	@ phole ldm
	sub	lr, r0, #1
	and	ip, lr, #3
	mov	r1, #1
	cmp	r2, r3
	str	ip, [sp, #36]
	str	r1, [sp, #8]
	bcs	.L1799
	mov	r1, r2
	ldr	r2, [sp, #12]
	strb	r2, [r1], #1
	str	r1, [r5, #20]
.L1732:
	ldr	lr, [sp, #20]
	mov	r2, #1
	cmp	lr, #1
	str	r2, [sp, #16]
	bls	.L1776
	ldr	r3, [sp, #36]
	cmp	r3, #0
	beq	.L1672
	cmp	r3, r2
	beq	.L1760
	cmp	r3, #2
	beq	.L1761
	ldr	ip, [sp, #8]
	cmp	ip, #0
	beq	.L1735
	add	r2, r5, #20
	ldmia	r2, {r2, r3}	@ phole ldm
	cmp	r2, r3
	bcs	.L1800
	ldr	lr, [sp, #12]
	mov	r0, r2
	strb	lr, [r0], #1
	str	r0, [r5, #20]
.L1735:
	ldr	r3, [sp, #16]
	add	ip, r3, #1
	str	ip, [sp, #16]
.L1761:
	ldr	r2, [sp, #8]
	cmp	r2, #0
	beq	.L1739
	ldr	r2, [r5, #20]
	ldr	r1, [r5, #24]
	cmp	r2, r1
	bcs	.L1801
	ldr	r3, [sp, #12]
	mov	r0, r2
	strb	r3, [r0], #1
	str	r0, [r5, #20]
.L1739:
	ldr	r1, [sp, #16]
	add	r2, r1, #1
	str	r2, [sp, #16]
.L1760:
	ldr	ip, [sp, #8]
	cmp	ip, #0
	beq	.L1743
	add	r2, r5, #20
	ldmia	r2, {r2, lr}	@ phole ldm
	cmp	r2, lr
	bcs	.L1802
	ldr	r1, [sp, #12]
	mov	r0, r2
	strb	r1, [r0], #1
	str	r0, [r5, #20]
.L1743:
	add	r3, sp, #16
	ldmia	r3, {r3, ip}	@ phole ldm
	add	lr, r3, #1
	cmp	ip, lr
	str	lr, [sp, #16]
	bhi	.L1672
	b	.L1776
.L1806:
	mov	lr, r2
	ldr	r2, [sp, #12]
	strb	r2, [lr], #1
	str	lr, [r5, #20]
	ldr	r2, [sp, #8]
.L1670:
	ldr	r1, [sp, #16]
	cmp	r2, #0
	add	r0, r1, #1
	str	r0, [sp, #16]
	beq	.L1747
	ldr	r2, [r5, #20]
	ldr	r0, [r5, #24]
	cmp	r2, r0
	bcs	.L1803
	ldr	ip, [sp, #12]
	mov	lr, r2
	strb	ip, [lr], #1
	str	lr, [r5, #20]
.L1747:
	ldr	r3, [sp, #8]
	cmp	r3, #0
	beq	.L1750
	ldr	r2, [r5, #20]
	ldr	r0, [r5, #24]
	cmp	r2, r0
	bcs	.L1804
	ldr	ip, [sp, #12]
	mov	lr, r2
	strb	ip, [lr], #1
	str	lr, [r5, #20]
.L1750:
	ldr	r3, [sp, #8]
	cmp	r3, #0
	beq	.L1753
	ldr	r2, [r5, #20]
	ldr	r0, [r5, #24]
	cmp	r2, r0
	bcs	.L1805
	ldr	ip, [sp, #12]
	mov	lr, r2
	strb	ip, [lr], #1
	str	lr, [r5, #20]
.L1753:
	ldr	r2, [sp, #16]
	ldr	r0, [sp, #20]
	add	r3, r2, #3
	cmp	r0, r3
	str	r3, [sp, #16]
	bls	.L1776
.L1672:
	ldr	r2, [sp, #8]
	cmp	r2, #0
	beq	.L1670
	ldr	r2, [r5, #20]
	ldr	r1, [r5, #24]
	cmp	r2, r1
	bcc	.L1806
	ldr	ip, [r5, #0]
	mov	r0, r5
	ldr	r1, [sp, #12]
	ldr	ip, [ip, #52]
	mov	lr, pc
	bx	ip
	ldr	r3, [sp, #8]
	cmn	r0, #1
	moveq	r3, #0
	str	r3, [sp, #8]
	mov	r2, r3
	b	.L1670
.L1776:
	ldr	lr, [sp, #8]
	cmp	lr, #0
	beq	.L1673
	b	.L1667
.L1805:
	ldr	r2, [r5, #0]
	mov	r0, r5
	ldr	r1, [sp, #12]
	ldr	ip, [r2, #52]
	mov	lr, pc
	bx	ip
	ldr	r1, [sp, #8]
	cmn	r0, #1
	moveq	r1, #0
	str	r1, [sp, #8]
	b	.L1753
.L1803:
	ldr	r2, [r5, #0]
	mov	r0, r5
	ldr	r1, [sp, #12]
	ldr	ip, [r2, #52]
	mov	lr, pc
	bx	ip
	ldr	r1, [sp, #8]
	cmn	r0, #1
	moveq	r1, #0
	str	r1, [sp, #8]
	b	.L1747
.L1804:
	ldr	r2, [r5, #0]
	mov	r0, r5
	ldr	r1, [sp, #12]
	ldr	ip, [r2, #52]
	mov	lr, pc
	bx	ip
.LEHE120:
	ldr	r1, [sp, #8]
	cmn	r0, #1
	moveq	r1, #0
	str	r1, [sp, #8]
	b	.L1750
.L1660:
	ldr	ip, [sp, #8]
	ldr	r1, [ip, #0]
	ldr	r0, [r1, #-12]
	add	r0, ip, r0
	ldr	r2, [r0, #8]
	ldr	lr, [r0, #20]
	orr	r3, r2, #1
	tst	r3, lr
	str	r3, [r0, #8]
	beq	.L1783
.LEHB121:
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
	b	.L1783
.L1788:
	mov	r1, #1
	bl	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEE8setstateEi
.LEHE121:
	ldr	r3, [r8, #0]
	ldr	ip, [r3, #-12]
	add	r0, r8, ip
	b	.L1658
.L1791:
	ldr	r2, [r5, #0]
	mov	r0, r5
	ldr	r1, [sp, #12]
.LEHB122:
	ldr	ip, [r2, #52]
	mov	lr, pc
	bx	ip
	ldr	ip, [sp, #8]
	cmn	r0, #1
	moveq	ip, #0
	str	ip, [sp, #8]
	b	.L1711
.L1790:
	ldr	r2, [r5, #0]
	mov	r0, r5
	ldr	r1, [sp, #12]
	ldr	ip, [r2, #52]
	mov	lr, pc
	bx	ip
	adds	r3, r0, #1
	movne	r3, #1
	str	r3, [sp, #8]
	b	.L1708
.L1792:
	ldr	r3, [r5, #0]
	mov	r0, r5
	ldr	r1, [sp, #12]
	ldr	ip, [r3, #52]
	mov	lr, pc
	bx	ip
	ldr	r1, [sp, #8]
	cmn	r0, #1
	moveq	r1, #0
	str	r1, [sp, #8]
	b	.L1715
.L1793:
	ldr	r2, [r5, #0]
	mov	r0, r5
	ldr	r1, [sp, #12]
	ldr	ip, [r2, #52]
	mov	lr, pc
	bx	ip
	ldr	lr, [sp, #8]
	cmn	r0, #1
	moveq	lr, #0
	str	lr, [sp, #8]
	b	.L1719
.L1801:
	ldr	ip, [r5, #0]
	mov	r0, r5
	ldr	r1, [sp, #12]
	ldr	ip, [ip, #52]
	mov	lr, pc
	bx	ip
	ldr	lr, [sp, #8]
	cmn	r0, #1
	moveq	lr, #0
	str	lr, [sp, #8]
	b	.L1739
.L1802:
	ldr	r3, [r5, #0]
	mov	r0, r5
	ldr	r1, [sp, #12]
	ldr	ip, [r3, #52]
	mov	lr, pc
	bx	ip
	ldr	r2, [sp, #8]
	cmn	r0, #1
	moveq	r2, #0
	str	r2, [sp, #8]
	b	.L1743
.L1799:
	ldr	r3, [r5, #0]
	mov	r0, r5
	ldr	r1, [sp, #12]
	ldr	ip, [r3, #52]
	mov	lr, pc
	bx	ip
	adds	r0, r0, #1
	movne	r0, #1
	str	r0, [sp, #8]
	b	.L1732
.L1800:
	ldr	r2, [r5, #0]
	mov	r0, r5
	ldr	r1, [sp, #12]
	ldr	ip, [r2, #52]
	mov	lr, pc
	bx	ip
	ldr	r1, [sp, #8]
	cmn	r0, #1
	moveq	r1, #0
	str	r1, [sp, #8]
	b	.L1735
.L1678:
	ldr	r3, [r5, #0]
	mov	r0, r5
	ldr	r1, [sp, #12]
	ldr	ip, [r3, #52]
	mov	lr, pc
	bx	ip
	ldr	r2, [sp, #8]
	cmn	r0, #1
	moveq	r2, #0
	str	r2, [sp, #8]
	b	.L1677
.L1794:
	ldr	ip, [r5, #0]
	mov	r0, r5
	ldr	r1, [sp, #12]
	ldr	ip, [ip, #52]
	mov	lr, pc
	bx	ip
	ldr	r3, [sp, #8]
	cmn	r0, #1
	moveq	r3, #0
	str	r3, [sp, #8]
	b	.L1723
.L1795:
	ldr	ip, [r5, #0]
	mov	r0, r5
	ldr	r1, [sp, #12]
	ldr	ip, [ip, #52]
	mov	lr, pc
	bx	ip
	ldr	r3, [sp, #8]
	cmn	r0, #1
	moveq	r3, #0
	str	r3, [sp, #8]
	b	.L1726
.L1796:
	ldr	ip, [r5, #0]
	mov	r0, r5
	ldr	r1, [sp, #12]
	ldr	ip, [ip, #52]
	mov	lr, pc
	bx	ip
.LEHE122:
	ldr	r3, [sp, #8]
	cmn	r0, #1
	moveq	r3, #0
	str	r3, [sp, #8]
	b	.L1729
.L1699:
.L1683:
	mov	r6, r0
	add	r0, sp, #80
	bl	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
.L1685:
	mov	r0, sl
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
.L1688:
.L1703:
.L1690:
	ldr	r9, .L1807+20
	ldr	r8, .L1807+4
	str	r9, [r4, #0]
	str	r8, [r4, #4]
	mov	r0, r4
	bl	_ZdlPv
.L1695:
	mov	r0, r7
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
.L1696:
	add	r0, sp, #68
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r6
.LEHB123:
	bl	__cxa_end_cleanup
.LEHE123:
.L1705:
	mov	r6, r0
	b	.L1696
.L1704:
	mov	r6, r0
	b	.L1695
.L1701:
	mov	r6, r0
	b	.L1688
.L1700:
	mov	r6, r0
	b	.L1685
.L1702:
.L1692:
	mov	r6, r0
	mov	r1, #0
	mov	r0, r5
	ldr	r2, [sp, #92]
	bl	_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i
	b	.L1695
.L1808:
	.align	2
.L1807:
	.word	_ZTV18QuApRawSoundStruct+8
	.word	_ZTV7QuTimer+8
	.word	.LC2
	.word	.LC3
	.word	_ZN4_STL4coutE
	.word	_ZTV22QuSoundStructInterface+8
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA2816:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2816-.LLSDACSB2816
.LLSDACSB2816:
	.uleb128 .LEHB108-.LFB2816
	.uleb128 .LEHE108-.LEHB108
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB109-.LFB2816
	.uleb128 .LEHE109-.LEHB109
	.uleb128 .L1705-.LFB2816
	.uleb128 0x0
	.uleb128 .LEHB110-.LFB2816
	.uleb128 .LEHE110-.LEHB110
	.uleb128 .L1704-.LFB2816
	.uleb128 0x0
	.uleb128 .LEHB111-.LFB2816
	.uleb128 .LEHE111-.LEHB111
	.uleb128 .L1701-.LFB2816
	.uleb128 0x0
	.uleb128 .LEHB112-.LFB2816
	.uleb128 .LEHE112-.LEHB112
	.uleb128 .L1700-.LFB2816
	.uleb128 0x0
	.uleb128 .LEHB113-.LFB2816
	.uleb128 .LEHE113-.LEHB113
	.uleb128 .L1704-.LFB2816
	.uleb128 0x0
	.uleb128 .LEHB114-.LFB2816
	.uleb128 .LEHE114-.LEHB114
	.uleb128 .L1702-.LFB2816
	.uleb128 0x0
	.uleb128 .LEHB115-.LFB2816
	.uleb128 .LEHE115-.LEHB115
	.uleb128 .L1704-.LFB2816
	.uleb128 0x0
	.uleb128 .LEHB116-.LFB2816
	.uleb128 .LEHE116-.LEHB116
	.uleb128 .L1705-.LFB2816
	.uleb128 0x0
	.uleb128 .LEHB117-.LFB2816
	.uleb128 .LEHE117-.LEHB117
	.uleb128 .L1700-.LFB2816
	.uleb128 0x0
	.uleb128 .LEHB118-.LFB2816
	.uleb128 .LEHE118-.LEHB118
	.uleb128 .L1699-.LFB2816
	.uleb128 0x0
	.uleb128 .LEHB119-.LFB2816
	.uleb128 .LEHE119-.LEHB119
	.uleb128 .L1700-.LFB2816
	.uleb128 0x0
	.uleb128 .LEHB120-.LFB2816
	.uleb128 .LEHE120-.LEHB120
	.uleb128 .L1699-.LFB2816
	.uleb128 0x0
	.uleb128 .LEHB121-.LFB2816
	.uleb128 .LEHE121-.LEHB121
	.uleb128 .L1700-.LFB2816
	.uleb128 0x0
	.uleb128 .LEHB122-.LFB2816
	.uleb128 .LEHE122-.LEHB122
	.uleb128 .L1699-.LFB2816
	.uleb128 0x0
	.uleb128 .LEHB123-.LFB2816
	.uleb128 .LEHE123-.LEHB123
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE2816:
	.fnend
	.size	_ZN19QuApRawSoundManager9loadSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE, .-_ZN19QuApRawSoundManager9loadSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.section	.text._ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE,"axG",%progbits,_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE,comdat
	.align	2
	.weak	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
	.hidden	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
	.type	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE, %function
_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE:
	.fnstart
.LFB5461:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.pad #20
	sub	sp, sp, #20
	str	r1, [sp, #12]
	mov	r6, r0
.L1810:
	ldr	r0, [sp, #12]
	cmp	r0, #0
	beq	.L1877
	ldr	r0, [sp, #12]
	ldr	r3, [r0, #12]
	str	r3, [sp, #8]
.L1811:
	ldr	r3, [sp, #8]
	cmp	r3, #0
	beq	.L1878
	ldr	r2, [sp, #8]
	ldr	r1, [r2, #12]
	str	r1, [sp, #4]
.L1812:
	ldr	r3, [sp, #4]
	cmp	r3, #0
	beq	.L1879
	ldr	r4, [sp, #4]
	ldr	fp, [r4, #12]
.L1813:
	cmp	fp, #0
	beq	.L1880
	ldr	r9, [fp, #12]
.L1814:
	cmp	r9, #0
	beq	.L1881
	ldr	sl, [r9, #12]
	cmp	sl, #0
	beq	.L1815
.L1872:
	ldr	r8, [sl, #12]
	cmp	r8, #0
	beq	.L1816
.L1871:
	ldr	r7, [r8, #12]
	cmp	r7, #0
	beq	.L1817
.L1870:
	ldr	r4, [r7, #12]
	cmp	r4, #0
	bne	.L1869
	b	.L1818
.L1882:
	mov	r4, r5
.L1869:
	mov	r0, r6
	ldr	r1, [r4, #12]
.LEHB124:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE124:
	add	r0, r4, #28
	mov	r1, #0
	ldr	r2, [r4, #32]
	ldr	r5, [r4, #8]
.LEHB125:
	bl	_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i
.LEHE125:
	ldr	r0, [r4, #16]
	cmp	r0, #0
	blne	free
.L1820:
	cmp	r4, #0
	movne	r0, r4
	blne	free
.L1822:
	cmp	r5, #0
	bne	.L1882
.L1818:
	add	r0, r7, #28
	mov	r1, #0
	ldr	r2, [r7, #32]
	ldr	r4, [r7, #8]
.LEHB126:
	bl	_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i
.LEHE126:
	ldr	r0, [r7, #16]
	cmp	r0, #0
	blne	free
.L1825:
	cmp	r7, #0
	movne	r0, r7
	blne	free
.L1827:
	cmp	r4, #0
	movne	r7, r4
	bne	.L1870
.L1817:
	add	r0, r8, #28
	mov	r1, #0
	ldr	r2, [r8, #32]
	ldr	r4, [r8, #8]
.LEHB127:
	bl	_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i
.LEHE127:
	ldr	r0, [r8, #16]
	cmp	r0, #0
	blne	free
.L1830:
	cmp	r8, #0
	movne	r0, r8
	blne	free
.L1832:
	cmp	r4, #0
	movne	r8, r4
	bne	.L1871
.L1816:
	add	r0, sl, #28
	mov	r1, #0
	ldr	r2, [sl, #32]
	ldr	r4, [sl, #8]
.LEHB128:
	bl	_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i
.LEHE128:
	ldr	r0, [sl, #16]
	cmp	r0, #0
	blne	free
.L1835:
	cmp	sl, #0
	movne	r0, sl
	blne	free
.L1837:
	cmp	r4, #0
	movne	sl, r4
	bne	.L1872
.L1815:
	add	r0, r9, #28
	mov	r1, #0
	ldr	r2, [r9, #32]
	ldr	r4, [r9, #8]
.LEHB129:
	bl	_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i
.LEHE129:
	ldr	r0, [r9, #16]
	cmp	r0, #0
	blne	free
.L1840:
	mov	r0, r9
	bl	free
	mov	r9, r4
	b	.L1814
.L1864:
.L1841:
	mov	sl, r0
	add	r0, r9, #16
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, sl
.LEHB130:
	bl	__cxa_end_cleanup
.LEHE130:
.L1881:
	mov	r1, r9
	add	r0, fp, #28
	ldr	r2, [fp, #32]
	ldr	r4, [fp, #8]
.LEHB131:
	bl	_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i
.LEHE131:
	ldr	r0, [fp, #16]
	cmp	r0, #0
	blne	free
.L1844:
	mov	r0, fp
	bl	free
	mov	fp, r4
	b	.L1813
.L1863:
.L1845:
	mov	r9, r0
	add	r0, fp, #16
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r9
.LEHB132:
	bl	__cxa_end_cleanup
.L1865:
.L1836:
	mov	r8, r0
	add	r0, sl, #16
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r8
	bl	__cxa_end_cleanup
.L1866:
.L1831:
	mov	r7, r0
	add	r0, r8, #16
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r7
	bl	__cxa_end_cleanup
.L1867:
.L1826:
	mov	r6, r0
	add	r0, r7, #16
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r6
	bl	__cxa_end_cleanup
.L1868:
.L1821:
	mov	r5, r0
	add	r0, r4, #16
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r5
	bl	__cxa_end_cleanup
.LEHE132:
.L1880:
	ldr	ip, [sp, #4]
	mov	r1, fp
	add	r0, ip, #28
	ldr	r2, [ip, #32]
	ldr	r4, [ip, #8]
.LEHB133:
	bl	_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i
.LEHE133:
	ldr	lr, [sp, #4]
	ldr	r0, [lr, #16]
	cmp	r0, #0
	blne	free
.L1848:
	ldr	r0, [sp, #4]
	bl	free
	str	r4, [sp, #4]
	b	.L1812
.L1862:
.L1849:
	ldr	r3, [sp, #4]
	mov	fp, r0
	add	r0, r3, #16
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, fp
.LEHB134:
	bl	__cxa_end_cleanup
.LEHE134:
.L1879:
	ldr	ip, [sp, #8]
	mov	r1, r3
	add	r0, ip, #28
	ldr	r2, [ip, #32]
	ldr	r4, [ip, #8]
.LEHB135:
	bl	_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i
.LEHE135:
	ldr	r1, [sp, #8]
	ldr	r0, [r1, #16]
	cmp	r0, #0
	blne	free
.L1852:
	ldr	r0, [sp, #8]
	bl	free
	str	r4, [sp, #8]
	b	.L1811
.L1861:
.L1853:
	ldr	r2, [sp, #8]
	mov	r4, r0
	add	r0, r2, #16
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r4
.LEHB136:
	bl	__cxa_end_cleanup
.LEHE136:
.L1878:
	ldr	lr, [sp, #12]
	mov	r1, r3
	add	r0, lr, #28
	ldr	r2, [lr, #32]
	ldr	r4, [lr, #8]
.LEHB137:
	bl	_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i
.LEHE137:
	ldr	r3, [sp, #12]
	ldr	r0, [r3, #16]
	cmp	r0, #0
	blne	free
.L1856:
	ldr	r0, [sp, #12]
	bl	free
	str	r4, [sp, #12]
	b	.L1810
.L1860:
.L1857:
	ldr	r6, [sp, #12]
	mov	r5, r0
	add	r0, r6, #16
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r5
.LEHB138:
	bl	__cxa_end_cleanup
.LEHE138:
.L1877:
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA5461:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5461-.LLSDACSB5461
.LLSDACSB5461:
	.uleb128 .LEHB124-.LFB5461
	.uleb128 .LEHE124-.LEHB124
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB125-.LFB5461
	.uleb128 .LEHE125-.LEHB125
	.uleb128 .L1868-.LFB5461
	.uleb128 0x0
	.uleb128 .LEHB126-.LFB5461
	.uleb128 .LEHE126-.LEHB126
	.uleb128 .L1867-.LFB5461
	.uleb128 0x0
	.uleb128 .LEHB127-.LFB5461
	.uleb128 .LEHE127-.LEHB127
	.uleb128 .L1866-.LFB5461
	.uleb128 0x0
	.uleb128 .LEHB128-.LFB5461
	.uleb128 .LEHE128-.LEHB128
	.uleb128 .L1865-.LFB5461
	.uleb128 0x0
	.uleb128 .LEHB129-.LFB5461
	.uleb128 .LEHE129-.LEHB129
	.uleb128 .L1864-.LFB5461
	.uleb128 0x0
	.uleb128 .LEHB130-.LFB5461
	.uleb128 .LEHE130-.LEHB130
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB131-.LFB5461
	.uleb128 .LEHE131-.LEHB131
	.uleb128 .L1863-.LFB5461
	.uleb128 0x0
	.uleb128 .LEHB132-.LFB5461
	.uleb128 .LEHE132-.LEHB132
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB133-.LFB5461
	.uleb128 .LEHE133-.LEHB133
	.uleb128 .L1862-.LFB5461
	.uleb128 0x0
	.uleb128 .LEHB134-.LFB5461
	.uleb128 .LEHE134-.LEHB134
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB135-.LFB5461
	.uleb128 .LEHE135-.LEHB135
	.uleb128 .L1861-.LFB5461
	.uleb128 0x0
	.uleb128 .LEHB136-.LFB5461
	.uleb128 .LEHE136-.LEHB136
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB137-.LFB5461
	.uleb128 .LEHE137-.LEHB137
	.uleb128 .L1860-.LFB5461
	.uleb128 0x0
	.uleb128 .LEHB138-.LFB5461
	.uleb128 .LEHE138-.LEHB138
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE5461:
	.fnend
	.size	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE, .-_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
	.section	.text._ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev,"axG",%progbits,_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev,comdat
	.align	2
	.weak	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev
	.hidden	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev
	.type	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev, %function
_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev:
	.fnstart
.LFB2806:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	ldr	r3, [r0, #4]
	cmp	r3, #0
	mov	r4, r0
	bne	.L1890
.L1884:
	ldr	r0, [r4, #0]
	cmp	r0, #0
	blne	free
.L1887:
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L1890:
	ldr	r2, [r0, #0]
	ldr	r1, [r2, #4]
.LEHB139:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE139:
	ldr	lr, [r4, #0]
	str	lr, [lr, #8]
	ldr	ip, [r4, #0]
	mov	r0, #0
	str	r0, [ip, #4]
	ldr	r1, [r4, #0]
	str	r1, [r1, #12]
	str	r0, [r4, #4]
	b	.L1884
.L1889:
.L1885:
	ldr	r3, [r4, #0]
	cmp	r3, #0
	mov	r4, r0
	movne	r0, r3
	blne	free
.L1886:
	mov	r0, r4
.LEHB140:
	bl	__cxa_end_cleanup
.LEHE140:
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA2806:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2806-.LLSDACSB2806
.LLSDACSB2806:
	.uleb128 .LEHB139-.LFB2806
	.uleb128 .LEHE139-.LEHB139
	.uleb128 .L1889-.LFB2806
	.uleb128 0x0
	.uleb128 .LEHB140-.LFB2806
	.uleb128 .LEHE140-.LEHB140
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE2806:
	.fnend
	.size	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev, .-_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev
	.section	.text._ZN19QuApRawSoundManager11deleteSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE,"axG",%progbits,_ZN19QuApRawSoundManager11deleteSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE,comdat
	.align	2
	.weak	_ZN19QuApRawSoundManager11deleteSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.hidden	_ZN19QuApRawSoundManager11deleteSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.type	_ZN19QuApRawSoundManager11deleteSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE, %function
_ZN19QuApRawSoundManager11deleteSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE:
	.fnstart
.LFB2814:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	add	r2, r0, #4
	.pad #28
	sub	sp, sp, #28
	mov	r5, r0
	mov	r0, r2
	str	r2, [sp, #4]
	mov	r6, r1
.LEHB141:
	bl	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEEixERSD_
	ldr	r3, .L1936
	ldr	r3, [r3, #0]
	ldr	ip, [r3, #4]
	cmp	ip, #0
	ldr	r4, [r0, #0]
	moveq	ip, r3
	beq	.L1895
	mov	r1, r3
	b	.L1896
.L1931:
	mov	r1, ip
	mov	ip, r2
.L1896:
	ldr	r2, [ip, #16]
	cmp	r4, r2
	ldrhi	r2, [ip, #12]
	ldrls	r2, [ip, #8]
	movhi	ip, r1
	cmp	r2, #0
	bne	.L1931
.L1895:
	cmp	ip, r3
	beq	.L1898
	ldr	r0, [ip, #16]
	cmp	r4, r0
	bcc	.L1898
.L1899:
	ldr	r1, [r5, #4]
	str	r1, [sp, #0]
	ldr	r4, [r1, #4]
	cmp	r4, #0
	moveq	r7, r1
	mov	fp, r1
	moveq	r9, r7
	moveq	r2, r7
	beq	.L1914
	ldr	r7, [r6, #4]
	ldr	sl, [r6, #0]
	mov	r9, r1
	rsb	r7, sl, r7
	mov	r6, r4
.L1906:
	add	r0, r6, #16
	ldmia	r0, {r0, r8}	@ phole ldm
	rsb	r8, r0, r8
	cmp	r7, r8
	movlt	r2, r7
	movge	r2, r8
	mov	r1, sl
	bl	memcmp
	cmp	r0, #0
	bne	.L1901
	cmp	r8, r7
	blt	.L1903
.L1902:
	ldr	r3, [r6, #8]
	cmp	r3, #0
	beq	.L1905
.L1932:
	mov	r9, r6
	mov	r6, r3
	b	.L1906
.L1898:
	str	ip, [sp, #20]
	add	r0, sp, #16
	mov	ip, #0
	ldr	r1, .L1936
	add	r2, sp, #20
	add	r3, sp, #8
	str	r4, [sp, #8]
	str	ip, [sp, #12]
	bl	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS4_NS_16_Nonconst_traitsIS4_EEEERKS4_
	b	.L1899
.L1901:
	bge	.L1902
.L1903:
	ldr	r3, [r6, #12]
	cmp	r3, #0
	mov	r6, r9
	bne	.L1932
.L1905:
	mov	r9, r6
	b	.L1912
.L1933:
	cmp	r7, r8
	blt	.L1909
.L1908:
	ldr	r3, [r4, #12]
	cmp	r3, #0
	mov	r4, fp
	beq	.L1911
.L1934:
	mov	fp, r4
	mov	r4, r3
.L1912:
	add	r1, r4, #16
	ldmia	r1, {r1, lr}	@ phole ldm
	rsb	r8, r1, lr
	cmp	r7, r8
	movlt	r2, r7
	movge	r2, r8
	mov	r0, sl
	bl	memcmp
	cmp	r0, #0
	beq	.L1933
	bge	.L1908
.L1909:
	ldr	r3, [r4, #8]
	cmp	r3, #0
	bne	.L1934
.L1911:
	cmp	r6, r4
	moveq	r7, r6
	ldreq	r2, [sp, #0]
	beq	.L1914
	mov	r0, r6
.L1915:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r4, r0
	bne	.L1915
	ldr	r2, [r5, #4]
	str	r2, [sp, #0]
	mov	r7, r6
	mov	r9, r4
.L1914:
	ldr	r3, [r2, #8]
	cmp	r7, r3
	beq	.L1935
.L1917:
	cmp	r7, r9
	bne	.L1930
.L1927:
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L1926:
	mov	r7, r6
.L1930:
	mov	r0, r7
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r4, [r5, #4]
	mov	r6, r0
	add	r1, r4, #4
	add	r2, r4, #8
	add	r3, r4, #12
	mov	r0, r7
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
.LEHE141:
	mov	r4, r0
	mov	r1, #0
	add	r0, r0, #28
	ldr	r2, [r4, #32]
.LEHB142:
	bl	_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i
.LEHE142:
	ldr	r0, [r4, #16]
	cmp	r0, #0
	blne	free
.L1923:
	cmp	r4, #0
	movne	r0, r4
	blne	free
.L1925:
	ldr	r2, [r5, #8]
	cmp	r6, r9
	sub	lr, r2, #1
	str	lr, [r5, #8]
	bne	.L1926
	b	.L1927
.L1935:
	cmp	r9, r2
	bne	.L1917
	ldr	r2, [r5, #8]
	cmp	r2, #0
	beq	.L1927
	ldr	r0, [sp, #4]
	ldr	r1, [r9, #4]
.LEHB143:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
	ldr	r3, [r5, #4]
	str	r3, [r3, #8]
	ldr	r1, [r5, #4]
	mov	r0, #0
	str	r0, [r1, #4]
	ldr	ip, [r5, #4]
	str	ip, [ip, #12]
	str	r0, [r5, #8]
	b	.L1927
.L1928:
.L1924:
	mov	r5, r0
	add	r0, r4, #16
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r5
	bl	__cxa_end_cleanup
.LEHE143:
.L1937:
	.align	2
.L1936:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA2814:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2814-.LLSDACSB2814
.LLSDACSB2814:
	.uleb128 .LEHB141-.LFB2814
	.uleb128 .LEHE141-.LEHB141
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB142-.LFB2814
	.uleb128 .LEHE142-.LEHB142
	.uleb128 .L1928-.LFB2814
	.uleb128 0x0
	.uleb128 .LEHB143-.LFB2814
	.uleb128 .LEHE143-.LEHB143
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE2814:
	.fnend
	.size	_ZN19QuApRawSoundManager11deleteSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE, .-_ZN19QuApRawSoundManager11deleteSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.section	.text._ZN19QuApRawSoundManagerD0Ev,"axG",%progbits,_ZN19QuApRawSoundManagerD0Ev,comdat
	.align	2
	.weak	_ZN19QuApRawSoundManagerD0Ev
	.hidden	_ZN19QuApRawSoundManagerD0Ev
	.type	_ZN19QuApRawSoundManagerD0Ev, %function
_ZN19QuApRawSoundManagerD0Ev:
	.fnstart
.LFB2812:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	ldr	r3, .L1956
	mov	r5, r0
	str	r3, [r0, #0]
	ldr	r0, [r0, #4]
	ldr	r4, [r0, #8]
	b	.L1940
.L1942:
	add	r0, r4, #28
	mov	r1, #0
	ldr	r2, [r4, #32]
.LEHB144:
	bl	_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i
	mov	r0, r4
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
.LEHE144:
	mov	r4, r0
	ldr	r0, [r5, #4]
.L1940:
	cmp	r4, r0
	bne	.L1942
	ldr	r1, [r5, #8]
	cmp	r1, #0
	bne	.L1955
.L1945:
	cmp	r0, #0
	blne	free
.L1948:
	ldr	r3, .L1956+4
	mov	r0, r5
	str	r3, [r5, #0]
	bl	_ZdlPv
	mov	r0, r5
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L1955:
	ldr	r1, [r4, #4]
	add	r0, r5, #4
.LEHB145:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE145:
	ldr	lr, [r5, #4]
	str	lr, [lr, #8]
	ldr	ip, [r5, #4]
	mov	r0, #0
	str	r0, [ip, #4]
	ldr	r2, [r5, #4]
	str	r2, [r2, #12]
	str	r0, [r5, #8]
	ldr	r0, [r5, #4]
	b	.L1945
.L1951:
.L1946:
	ldr	r3, [r5, #4]
	cmp	r3, #0
	mov	r4, r0
	movne	r0, r3
	blne	free
.L1953:
.L1949:
	ldr	r1, .L1956+4
	mov	r0, r4
	str	r1, [r5, #0]
.LEHB146:
	bl	__cxa_end_cleanup
.LEHE146:
.L1952:
.L1944:
	mov	r4, r0
	add	r0, r5, #4
	bl	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev
	b	.L1949
.L1957:
	.align	2
.L1956:
	.word	_ZTV19QuApRawSoundManager+8
	.word	_ZTV23QuSoundManagerInterface+8
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA2812:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2812-.LLSDACSB2812
.LLSDACSB2812:
	.uleb128 .LEHB144-.LFB2812
	.uleb128 .LEHE144-.LEHB144
	.uleb128 .L1952-.LFB2812
	.uleb128 0x0
	.uleb128 .LEHB145-.LFB2812
	.uleb128 .LEHE145-.LEHB145
	.uleb128 .L1951-.LFB2812
	.uleb128 0x0
	.uleb128 .LEHB146-.LFB2812
	.uleb128 .LEHE146-.LEHB146
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE2812:
	.fnend
	.size	_ZN19QuApRawSoundManagerD0Ev, .-_ZN19QuApRawSoundManagerD0Ev
	.section	.text._ZN19QuApRawSoundManagerD1Ev,"axG",%progbits,_ZN19QuApRawSoundManagerD1Ev,comdat
	.align	2
	.weak	_ZN19QuApRawSoundManagerD1Ev
	.hidden	_ZN19QuApRawSoundManagerD1Ev
	.type	_ZN19QuApRawSoundManagerD1Ev, %function
_ZN19QuApRawSoundManagerD1Ev:
	.fnstart
.LFB2811:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	ldr	r3, .L1976
	mov	r5, r0
	str	r3, [r0, #0]
	ldr	r0, [r0, #4]
	ldr	r4, [r0, #8]
	b	.L1960
.L1962:
	add	r0, r4, #28
	mov	r1, #0
	ldr	r2, [r4, #32]
.LEHB147:
	bl	_ZN15QuStupidPointerI18QuApRawSoundStructE3setEPS0_i
	mov	r0, r4
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
.LEHE147:
	mov	r4, r0
	ldr	r0, [r5, #4]
.L1960:
	cmp	r4, r0
	bne	.L1962
	ldr	r1, [r5, #8]
	cmp	r1, #0
	bne	.L1975
.L1965:
	cmp	r0, #0
	blne	free
.L1968:
	ldr	r3, .L1976+4
	mov	r0, r5
	str	r3, [r5, #0]
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L1975:
	ldr	r1, [r4, #4]
	add	r0, r5, #4
.LEHB148:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI18QuApRawSoundStructEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE148:
	ldr	lr, [r5, #4]
	str	lr, [lr, #8]
	ldr	ip, [r5, #4]
	mov	r0, #0
	str	r0, [ip, #4]
	ldr	r2, [r5, #4]
	str	r2, [r2, #12]
	str	r0, [r5, #8]
	ldr	r0, [r5, #4]
	b	.L1965
.L1971:
.L1966:
	ldr	r3, [r5, #4]
	cmp	r3, #0
	mov	r4, r0
	movne	r0, r3
	blne	free
.L1973:
.L1969:
	ldr	r1, .L1976+4
	mov	r0, r4
	str	r1, [r5, #0]
.LEHB149:
	bl	__cxa_end_cleanup
.LEHE149:
.L1972:
.L1964:
	mov	r4, r0
	add	r0, r5, #4
	bl	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI18QuApRawSoundStructENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev
	b	.L1969
.L1977:
	.align	2
.L1976:
	.word	_ZTV19QuApRawSoundManager+8
	.word	_ZTV23QuSoundManagerInterface+8
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA2811:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2811-.LLSDACSB2811
.LLSDACSB2811:
	.uleb128 .LEHB147-.LFB2811
	.uleb128 .LEHE147-.LEHB147
	.uleb128 .L1972-.LFB2811
	.uleb128 0x0
	.uleb128 .LEHB148-.LFB2811
	.uleb128 .LEHE148-.LEHB148
	.uleb128 .L1971-.LFB2811
	.uleb128 0x0
	.uleb128 .LEHB149-.LFB2811
	.uleb128 .LEHE149-.LEHB149
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE2811:
	.fnend
	.size	_ZN19QuApRawSoundManagerD1Ev, .-_ZN19QuApRawSoundManagerD1Ev
	.section	.text._ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE,"axG",%progbits,_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE,comdat
	.align	2
	.weak	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
	.hidden	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
	.type	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE, %function
_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE:
	.fnstart
.LFB5534:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.pad #20
	sub	sp, sp, #20
	str	r1, [sp, #12]
	mov	r6, r0
.L1979:
	ldr	r0, [sp, #12]
	cmp	r0, #0
	beq	.L2055
	ldr	r0, [sp, #12]
	ldr	r3, [r0, #12]
	str	r3, [sp, #8]
.L1980:
	ldr	r3, [sp, #8]
	cmp	r3, #0
	beq	.L2056
	ldr	r2, [sp, #8]
	ldr	r1, [r2, #12]
	str	r1, [sp, #4]
.L1981:
	ldr	r3, [sp, #4]
	cmp	r3, #0
	beq	.L2057
	ldr	r4, [sp, #4]
	ldr	fp, [r4, #12]
.L1982:
	cmp	fp, #0
	beq	.L2058
	ldr	r9, [fp, #12]
.L1983:
	cmp	r9, #0
	beq	.L2059
	ldr	sl, [r9, #12]
	cmp	sl, #0
	beq	.L1984
.L2050:
	ldr	r8, [sl, #12]
	cmp	r8, #0
	beq	.L1985
.L2049:
	ldr	r7, [r8, #12]
	cmp	r7, #0
	beq	.L1986
.L2048:
	ldr	r4, [r7, #12]
	cmp	r4, #0
	bne	.L2047
	b	.L1987
.L2060:
	mov	r4, r5
.L2047:
	mov	r0, r6
	ldr	r1, [r4, #12]
.LEHB150:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE150:
	add	r0, r4, #28
	mov	r1, #0
	ldr	r2, [r4, #32]
	ldr	r5, [r4, #8]
.LEHB151:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE151:
	ldr	r0, [r4, #16]
	cmp	r0, #0
	blne	free
.L1990:
	cmp	r4, #0
	movne	r0, r4
	blne	free
.L1992:
	cmp	r5, #0
	bne	.L2060
.L1987:
	add	r0, r7, #28
	mov	r1, #0
	ldr	r2, [r7, #32]
	ldr	r4, [r7, #8]
.LEHB152:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE152:
	ldr	r0, [r7, #16]
	cmp	r0, #0
	blne	free
.L1996:
	cmp	r7, #0
	movne	r0, r7
	blne	free
.L1998:
	cmp	r4, #0
	movne	r7, r4
	bne	.L2048
.L1986:
	add	r0, r8, #28
	mov	r1, #0
	ldr	r2, [r8, #32]
	ldr	r4, [r8, #8]
.LEHB153:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE153:
	ldr	r0, [r8, #16]
	cmp	r0, #0
	blne	free
.L2002:
	cmp	r8, #0
	movne	r0, r8
	blne	free
.L2004:
	cmp	r4, #0
	movne	r8, r4
	bne	.L2049
.L1985:
	add	r0, sl, #28
	mov	r1, #0
	ldr	r2, [sl, #32]
	ldr	r4, [sl, #8]
.LEHB154:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE154:
	ldr	r0, [sl, #16]
	cmp	r0, #0
	blne	free
.L2008:
	cmp	sl, #0
	movne	r0, sl
	blne	free
.L2010:
	cmp	r4, #0
	movne	sl, r4
	bne	.L2050
.L1984:
	add	r0, r9, #28
	mov	r1, #0
	ldr	r2, [r9, #32]
	ldr	r4, [r9, #8]
.LEHB155:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE155:
	ldr	r0, [r9, #16]
	cmp	r0, #0
	blne	free
.L2014:
	mov	r0, r9
	bl	free
	mov	r9, r4
	b	.L1983
.L2042:
.L2015:
	mov	sl, r0
	add	r0, r9, #16
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, sl
.LEHB156:
	bl	__cxa_end_cleanup
.LEHE156:
.L2059:
	mov	r1, r9
	add	r0, fp, #28
	ldr	r2, [fp, #32]
	ldr	r4, [fp, #8]
.LEHB157:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE157:
	ldr	r0, [fp, #16]
	cmp	r0, #0
	blne	free
.L2019:
	mov	r0, fp
	bl	free
	mov	fp, r4
	b	.L1982
.L2041:
.L2020:
	mov	r9, r0
	add	r0, fp, #16
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r9
.LEHB158:
	bl	__cxa_end_cleanup
.L2043:
.L2009:
	mov	r8, r0
	add	r0, sl, #16
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r8
	bl	__cxa_end_cleanup
.L2044:
.L2003:
	mov	r7, r0
	add	r0, r8, #16
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r7
	bl	__cxa_end_cleanup
.L2045:
.L1997:
	mov	r6, r0
	add	r0, r7, #16
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r6
	bl	__cxa_end_cleanup
.L2046:
.L1991:
	mov	r5, r0
	add	r0, r4, #16
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r5
	bl	__cxa_end_cleanup
.LEHE158:
.L2058:
	ldr	ip, [sp, #4]
	mov	r1, fp
	add	r0, ip, #28
	ldr	r2, [ip, #32]
	ldr	r4, [ip, #8]
.LEHB159:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE159:
	ldr	lr, [sp, #4]
	ldr	r0, [lr, #16]
	cmp	r0, #0
	blne	free
.L2024:
	ldr	r0, [sp, #4]
	bl	free
	str	r4, [sp, #4]
	b	.L1981
.L2040:
.L2025:
	ldr	r3, [sp, #4]
	mov	fp, r0
	add	r0, r3, #16
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, fp
.LEHB160:
	bl	__cxa_end_cleanup
.LEHE160:
.L2057:
	ldr	ip, [sp, #8]
	mov	r1, r3
	add	r0, ip, #28
	ldr	r2, [ip, #32]
	ldr	r4, [ip, #8]
.LEHB161:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE161:
	ldr	r1, [sp, #8]
	ldr	r0, [r1, #16]
	cmp	r0, #0
	blne	free
.L2029:
	ldr	r0, [sp, #8]
	bl	free
	str	r4, [sp, #8]
	b	.L1980
.L2039:
.L2030:
	ldr	r2, [sp, #8]
	mov	r4, r0
	add	r0, r2, #16
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r4
.LEHB162:
	bl	__cxa_end_cleanup
.LEHE162:
.L2056:
	ldr	lr, [sp, #12]
	mov	r1, r3
	add	r0, lr, #28
	ldr	r2, [lr, #32]
	ldr	r4, [lr, #8]
.LEHB163:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE163:
	ldr	r3, [sp, #12]
	ldr	r0, [r3, #16]
	cmp	r0, #0
	blne	free
.L2034:
	ldr	r0, [sp, #12]
	bl	free
	str	r4, [sp, #12]
	b	.L1979
.L2038:
.L2035:
	ldr	r6, [sp, #12]
	mov	r5, r0
	add	r0, r6, #16
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r5
.LEHB164:
	bl	__cxa_end_cleanup
.LEHE164:
.L2055:
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA5534:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5534-.LLSDACSB5534
.LLSDACSB5534:
	.uleb128 .LEHB150-.LFB5534
	.uleb128 .LEHE150-.LEHB150
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB151-.LFB5534
	.uleb128 .LEHE151-.LEHB151
	.uleb128 .L2046-.LFB5534
	.uleb128 0x0
	.uleb128 .LEHB152-.LFB5534
	.uleb128 .LEHE152-.LEHB152
	.uleb128 .L2045-.LFB5534
	.uleb128 0x0
	.uleb128 .LEHB153-.LFB5534
	.uleb128 .LEHE153-.LEHB153
	.uleb128 .L2044-.LFB5534
	.uleb128 0x0
	.uleb128 .LEHB154-.LFB5534
	.uleb128 .LEHE154-.LEHB154
	.uleb128 .L2043-.LFB5534
	.uleb128 0x0
	.uleb128 .LEHB155-.LFB5534
	.uleb128 .LEHE155-.LEHB155
	.uleb128 .L2042-.LFB5534
	.uleb128 0x0
	.uleb128 .LEHB156-.LFB5534
	.uleb128 .LEHE156-.LEHB156
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB157-.LFB5534
	.uleb128 .LEHE157-.LEHB157
	.uleb128 .L2041-.LFB5534
	.uleb128 0x0
	.uleb128 .LEHB158-.LFB5534
	.uleb128 .LEHE158-.LEHB158
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB159-.LFB5534
	.uleb128 .LEHE159-.LEHB159
	.uleb128 .L2040-.LFB5534
	.uleb128 0x0
	.uleb128 .LEHB160-.LFB5534
	.uleb128 .LEHE160-.LEHB160
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB161-.LFB5534
	.uleb128 .LEHE161-.LEHB161
	.uleb128 .L2039-.LFB5534
	.uleb128 0x0
	.uleb128 .LEHB162-.LFB5534
	.uleb128 .LEHE162-.LEHB162
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB163-.LFB5534
	.uleb128 .LEHE163-.LEHB163
	.uleb128 .L2038-.LFB5534
	.uleb128 0x0
	.uleb128 .LEHB164-.LFB5534
	.uleb128 .LEHE164-.LEHB164
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE5534:
	.fnend
	.size	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE, .-_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
	.section	.text._ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev,"axG",%progbits,_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev,comdat
	.align	2
	.weak	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev
	.hidden	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev
	.type	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev, %function
_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev:
	.fnstart
.LFB3184:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	ldr	r3, [r0, #4]
	cmp	r3, #0
	mov	r4, r0
	bne	.L2068
.L2062:
	ldr	r0, [r4, #0]
	cmp	r0, #0
	blne	free
.L2065:
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L2068:
	ldr	r2, [r0, #0]
	ldr	r1, [r2, #4]
.LEHB165:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE165:
	ldr	lr, [r4, #0]
	str	lr, [lr, #8]
	ldr	ip, [r4, #0]
	mov	r0, #0
	str	r0, [ip, #4]
	ldr	r1, [r4, #0]
	str	r1, [r1, #12]
	str	r0, [r4, #4]
	b	.L2062
.L2067:
.L2063:
	ldr	r3, [r4, #0]
	cmp	r3, #0
	mov	r4, r0
	movne	r0, r3
	blne	free
.L2064:
	mov	r0, r4
.LEHB166:
	bl	__cxa_end_cleanup
.LEHE166:
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA3184:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3184-.LLSDACSB3184
.LLSDACSB3184:
	.uleb128 .LEHB165-.LFB3184
	.uleb128 .LEHE165-.LEHB165
	.uleb128 .L2067-.LFB3184
	.uleb128 0x0
	.uleb128 .LEHB166-.LFB3184
	.uleb128 .LEHE166-.LEHB166
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE3184:
	.fnend
	.size	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev, .-_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev
	.section	.text._ZN14QuImageManagerD1Ev,"axG",%progbits,_ZN14QuImageManagerD1Ev,comdat
	.align	2
	.weak	_ZN14QuImageManagerD1Ev
	.hidden	_ZN14QuImageManagerD1Ev
	.type	_ZN14QuImageManagerD1Ev, %function
_ZN14QuImageManagerD1Ev:
	.fnstart
.LFB3192:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	ldr	r3, [r0, #28]
	cmp	r3, #0
	mov	r4, r0
	bne	.L2092
.L2070:
	ldr	r0, [r4, #24]
	cmp	r0, #0
	blne	free
.L2073:
	ldr	r3, [r4, #16]
	cmp	r3, #0
	bne	.L2093
.L2075:
	ldr	r0, [r4, #12]
	cmp	r0, #0
	blne	free
.L2079:
	ldr	r3, [r4, #4]
	cmp	r3, #0
	bne	.L2094
.L2081:
	ldr	r0, [r4, #0]
	cmp	r0, #0
	blne	free
.L2085:
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L2092:
	ldr	r0, [r0, #24]
	ldr	r1, [r0, #4]
	add	r0, r4, #24
.LEHB167:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE167:
	ldr	lr, [r4, #24]
	str	lr, [lr, #8]
	ldr	ip, [r4, #24]
	mov	r1, #0
	str	r1, [ip, #4]
	ldr	r2, [r4, #24]
	str	r2, [r2, #12]
	str	r1, [r4, #28]
	b	.L2070
.L2094:
	ldr	r0, [r4, #0]
	ldr	r1, [r0, #4]
	mov	r0, r4
.LEHB168:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE168:
	ldr	lr, [r4, #0]
	str	lr, [lr, #8]
	ldr	ip, [r4, #0]
	mov	r1, #0
	str	r1, [ip, #4]
	ldr	r2, [r4, #0]
	str	r2, [r2, #12]
	str	r1, [r4, #4]
	b	.L2081
.L2093:
	ldr	r0, [r4, #12]
	ldr	r1, [r0, #4]
	add	r0, r4, #12
.LEHB169:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE169:
	ldr	lr, [r4, #12]
	str	lr, [lr, #8]
	ldr	ip, [r4, #12]
	mov	r1, #0
	str	r1, [ip, #4]
	ldr	r2, [r4, #12]
	str	r2, [r2, #12]
	str	r1, [r4, #16]
	b	.L2075
.L2089:
.L2071:
	ldr	r3, [r4, #24]
	cmp	r3, #0
	mov	r5, r0
	movne	r0, r3
	blne	free
.L2072:
.L2090:
.L2076:
	add	r0, r4, #12
	bl	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev
.L2091:
.L2082:
	mov	r0, r4
	bl	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev
	mov	r0, r5
.LEHB170:
	bl	__cxa_end_cleanup
.L2087:
.L2083:
	ldr	r3, [r4, #0]
	cmp	r3, #0
	mov	r4, r0
	movne	r0, r3
	blne	free
.L2084:
	mov	r0, r4
	bl	__cxa_end_cleanup
.LEHE170:
.L2088:
.L2077:
	ldr	r3, [r4, #12]
	cmp	r3, #0
	mov	r5, r0
	beq	.L2082
	mov	r0, r3
	bl	free
	b	.L2082
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA3192:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3192-.LLSDACSB3192
.LLSDACSB3192:
	.uleb128 .LEHB167-.LFB3192
	.uleb128 .LEHE167-.LEHB167
	.uleb128 .L2089-.LFB3192
	.uleb128 0x0
	.uleb128 .LEHB168-.LFB3192
	.uleb128 .LEHE168-.LEHB168
	.uleb128 .L2087-.LFB3192
	.uleb128 0x0
	.uleb128 .LEHB169-.LFB3192
	.uleb128 .LEHE169-.LEHB169
	.uleb128 .L2088-.LFB3192
	.uleb128 0x0
	.uleb128 .LEHB170-.LFB3192
	.uleb128 .LEHE170-.LEHB170
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE3192:
	.fnend
	.size	_ZN14QuImageManagerD1Ev, .-_ZN14QuImageManagerD1Ev
	.section	.text._ZN15QuStupidPointerI14QuImageManagerE3setEPS0_i,"axG",%progbits,_ZN15QuStupidPointerI14QuImageManagerE3setEPS0_i,comdat
	.align	2
	.weak	_ZN15QuStupidPointerI14QuImageManagerE3setEPS0_i
	.hidden	_ZN15QuStupidPointerI14QuImageManagerE3setEPS0_i
	.type	_ZN15QuStupidPointerI14QuImageManagerE3setEPS0_i, %function
_ZN15QuStupidPointerI14QuImageManagerE3setEPS0_i:
	.fnstart
.LFB5289:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	.save {r4, r5, r6, r7, r8, sl, lr}
	ldr	r4, [r0, #0]
	cmp	r4, #0
	.pad #28
	sub	sp, sp, #28
	mov	r6, r0
	mov	r5, r1
	mov	sl, r2
	beq	.L2096
	ldr	r3, .L2152
	ldr	r1, [r3, #0]
	ldr	ip, [r1, #4]
	cmp	ip, #0
	moveq	ip, r1
	beq	.L2100
	mov	r2, r1
	b	.L2101
.L2144:
	mov	r2, ip
	mov	ip, r3
.L2101:
	ldr	r3, [ip, #16]
	cmp	r4, r3
	ldrhi	r3, [ip, #12]
	ldrls	r3, [ip, #8]
	movhi	ip, r2
	cmp	r3, #0
	bne	.L2144
.L2100:
	cmp	ip, r1
	beq	.L2103
	ldr	r0, [ip, #16]
	cmp	r4, r0
	mov	r3, ip
	bcs	.L2104
.L2103:
	add	r3, sp, #4
	str	ip, [sp, #20]
	add	r0, sp, #16
	mov	ip, #0
	ldr	r1, .L2152
	add	r2, sp, #20
	stmib	sp, {r4, ip}	@ phole stm
.LEHB171:
	bl	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS4_NS_16_Nonconst_traitsIS4_EEEERKS4_
	ldr	r3, [sp, #16]
.L2104:
	ldr	r8, [r3, #20]
	sub	r8, r8, #1
	cmp	r8, #0
	str	r8, [r3, #20]
	beq	.L2145
.L2096:
	cmp	r5, #0
	stmia	r6, {r5, sl}	@ phole stm
	beq	.L2138
	ldr	r4, .L2152
	str	r5, [sp, #12]
	ldr	r0, [r4, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	beq	.L2125
	mov	r1, r0
	b	.L2129
.L2146:
	mov	r1, r3
	mov	r3, r2
.L2129:
	ldr	r2, [r3, #16]
	cmp	r5, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L2146
	cmp	r0, r3
	beq	.L2125
	ldr	ip, [r3, #16]
	cmp	r5, ip
	bcc	.L2125
.L2130:
	cmp	r3, r0
	beq	.L2147
.L2132:
	ldr	ip, [r3, #20]
	add	r1, ip, #1
	str	r1, [r3, #20]
.L2138:
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	bx	lr
.L2125:
	mov	r3, r0
	b	.L2130
.L2145:
	ldr	r0, [r6, #0]
	bl	_ZN21QuStupidPointerHelper6removeEPv
.LEHE171:
	ldr	r1, [r6, #4]
	cmp	r1, #1
	beq	.L2148
	ldr	r0, [r6, #0]
	cmp	r0, #0
	beq	.L2096
	ldr	r2, [r0, #-4]
	add	r4, r2, r2, asl #3
	add	r4, r0, r4, asl #2
	b	.L2106
.L2108:
	ldr	r0, [r4, #-12]
	cmp	r0, #0
	blne	free
.L2111:
	ldr	r1, [r4, #-20]
	cmp	r1, #0
	add	r0, r7, #12
	bne	.L2149
.L2113:
	ldr	r0, [r4, #-24]
	cmp	r0, #0
	blne	free
.L2117:
	ldr	ip, [r4, #-32]
	cmp	ip, #0
	bne	.L2150
.L2119:
	ldr	r0, [r4, #-36]
	cmp	r0, #0
	blne	free
.L2123:
	ldr	r0, [r6, #0]
	mov	r4, r7
.L2106:
	cmp	r0, r4
	beq	.L2107
	ldr	r7, [r4, #-8]
	cmp	r7, #0
	sub	r0, r4, #12
	sub	r7, r4, #36
	beq	.L2108
	ldr	lr, [r4, #-12]
	ldr	r1, [lr, #4]
.LEHB172:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI6QuFontEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE172:
	ldr	ip, [r4, #-12]
	str	ip, [ip, #8]
	ldr	r0, [r4, #-12]
	str	r8, [r0, #4]
	ldr	r3, [r4, #-12]
	str	r3, [r3, #12]
	str	r8, [r4, #-8]
	b	.L2108
.L2148:
	ldr	r4, [r6, #0]
	cmp	r4, #0
	beq	.L2096
	mov	r0, r4
.LEHB173:
	bl	_ZN14QuImageManagerD1Ev
.LEHE173:
	mov	r0, r4
	bl	_ZdlPv
	b	.L2096
.L2150:
	ldr	r2, [r4, #-36]
	mov	r0, r7
	ldr	r1, [r2, #4]
.LEHB174:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE174:
	ldr	r3, [r4, #-36]
	str	r3, [r3, #8]
	ldr	lr, [r4, #-36]
	str	r8, [lr, #4]
	ldr	r1, [r4, #-36]
	str	r1, [r1, #12]
	str	r8, [r4, #-32]
	b	.L2119
.L2149:
	ldr	r2, [r4, #-24]
	ldr	r1, [r2, #4]
.LEHB175:
	bl	_ZN4_STL8_Rb_treeINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEENS_4pairIKS6_15QuStupidPointerI11QuBaseImageEEENS_10_Select1stISC_EENS_4lessIS6_EENS4_ISC_EEE8_M_eraseEPNS_13_Rb_tree_nodeISC_EE
.LEHE175:
	ldr	r0, [r4, #-24]
	str	r0, [r0, #8]
	ldr	r3, [r4, #-24]
	str	r8, [r3, #4]
	ldr	lr, [r4, #-24]
	str	lr, [lr, #12]
	str	r8, [r4, #-20]
	b	.L2113
.L2107:
	sub	r0, r0, #8
	bl	_ZdaPv
	b	.L2096
.L2147:
	add	r0, sp, #12
.LEHB176:
	bl	T.1801
.LEHE176:
	mov	r2, #0
	str	r2, [r0, #0]
	ldr	r3, [r4, #0]
	ldr	r2, [r3, #4]
	cmp	r2, #0
	beq	.L2132
	ldr	ip, [sp, #12]
	mov	r0, r3
	b	.L2137
.L2151:
	mov	r0, r2
	mov	r2, r1
.L2137:
	ldr	r1, [r2, #16]
	cmp	r1, ip
	ldrcc	r1, [r2, #12]
	ldrcs	r1, [r2, #8]
	movcc	r2, r0
	cmp	r1, #0
	bne	.L2151
	cmp	r3, r2
	beq	.L2132
	ldr	r0, [r2, #16]
	cmp	r0, ip
	movls	r3, r2
	b	.L2132
.L2141:
.L2109:
	ldr	r3, [r7, #24]
	cmp	r3, #0
	mov	r4, r0
	movne	r0, r3
	blne	free
.L2110:
.L2142:
.L2114:
	add	r0, r7, #12
	bl	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev
.L2143:
.L2120:
	mov	r0, r7
	bl	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev
	mov	r0, r4
.LEHB177:
	bl	__cxa_end_cleanup
.L2140:
.L2115:
	ldr	r3, [r7, #12]
	cmp	r3, #0
	mov	r4, r0
	beq	.L2120
	mov	r0, r3
	bl	free
	b	.L2120
.L2139:
.L2121:
	ldr	r3, [r7, #0]
	cmp	r3, #0
	mov	r4, r0
	movne	r0, r3
	blne	free
.L2122:
	mov	r0, r4
	bl	__cxa_end_cleanup
.LEHE177:
.L2153:
	.align	2
.L2152:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA5289:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5289-.LLSDACSB5289
.LLSDACSB5289:
	.uleb128 .LEHB171-.LFB5289
	.uleb128 .LEHE171-.LEHB171
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB172-.LFB5289
	.uleb128 .LEHE172-.LEHB172
	.uleb128 .L2141-.LFB5289
	.uleb128 0x0
	.uleb128 .LEHB173-.LFB5289
	.uleb128 .LEHE173-.LEHB173
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB174-.LFB5289
	.uleb128 .LEHE174-.LEHB174
	.uleb128 .L2139-.LFB5289
	.uleb128 0x0
	.uleb128 .LEHB175-.LFB5289
	.uleb128 .LEHE175-.LEHB175
	.uleb128 .L2140-.LFB5289
	.uleb128 0x0
	.uleb128 .LEHB176-.LFB5289
	.uleb128 .LEHE176-.LEHB176
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB177-.LFB5289
	.uleb128 .LEHE177-.LEHB177
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE5289:
	.fnend
	.size	_ZN15QuStupidPointerI14QuImageManagerE3setEPS0_i, .-_ZN15QuStupidPointerI14QuImageManagerE3setEPS0_i
	.section	.text._ZN8QuGlobal6getRefEv,"axG",%progbits,_ZN8QuGlobal6getRefEv,comdat
	.align	2
	.weak	_ZN8QuGlobal6getRefEv
	.hidden	_ZN8QuGlobal6getRefEv
	.type	_ZN8QuGlobal6getRefEv, %function
_ZN8QuGlobal6getRefEv:
	.fnstart
.LFB3679:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	.save {r4, r5, r6, r7, r8, lr}
	ldr	r5, .L2170
	ldr	r4, [r5, #0]
	cmp	r4, #0
	beq	.L2169
.L2155:
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L2169:
	mov	r0, #60
.LEHB178:
	bl	_Znwj
.LEHE178:
	mov	r1, #0
	add	r8, r0, #4
	mov	r6, r0
	strb	r1, [r0, #0]
	strb	r1, [r0, #1]
	str	r1, [r0, #4]
	mov	r2, #1
	mov	r0, r8
	mov	r4, r6
.LEHB179:
	bl	_ZN15QuStupidPointerI13QuBaseManagerE3setEPS0_i
.LEHE179:
	mov	r1, #0
	add	r7, r6, #12
	str	r1, [r6, #12]
	mov	r0, r7
	mov	r2, #1
.LEHB180:
	bl	_ZN15QuStupidPointerI14QuImageManagerE3setEPS0_i
.LEHE180:
	mov	r1, #0
	mov	r0, r6
	str	r1, [r0, #20]!
	mov	r2, #1
.LEHB181:
	bl	_ZN15QuStupidPointerI23QuSoundManagerInterfaceE3setEPS0_i
.LEHE181:
	mov	r3, #0
	mov	r2, #0
	mov	r0, #30
	mov	r1, #1065353216
	str	r0, [r6, #40]
	str	r3, [r6, #48]	@ float
	str	r1, [r6, #52]	@ float
	str	r2, [r6, #56]
	str	r2, [r6, #36]
	str	r3, [r6, #44]	@ float
	str	r6, [r5, #0]
	b	.L2155
.L2168:
	mov	r4, r0
.L2164:
	mov	r0, r6
	bl	_ZdlPv
	mov	r0, r4
.LEHB182:
	bl	__cxa_end_cleanup
.LEHE182:
.L2166:
.L2161:
	mov	r4, r0
	mov	r1, #0
	mov	r0, r7
	ldr	r2, [r6, #16]
	bl	_ZN15QuStupidPointerI14QuImageManagerE3setEPS0_i
.L2163:
	mov	r0, r8
	bl	_ZN15QuStupidPointerI13QuBaseManagerED1Ev
	b	.L2164
.L2167:
	mov	r4, r0
	b	.L2163
.L2171:
	.align	2
.L2170:
	.word	_ZN8QuGlobal5mSelfE
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA3679:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3679-.LLSDACSB3679
.LLSDACSB3679:
	.uleb128 .LEHB178-.LFB3679
	.uleb128 .LEHE178-.LEHB178
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB179-.LFB3679
	.uleb128 .LEHE179-.LEHB179
	.uleb128 .L2168-.LFB3679
	.uleb128 0x0
	.uleb128 .LEHB180-.LFB3679
	.uleb128 .LEHE180-.LEHB180
	.uleb128 .L2167-.LFB3679
	.uleb128 0x0
	.uleb128 .LEHB181-.LFB3679
	.uleb128 .LEHE181-.LEHB181
	.uleb128 .L2166-.LFB3679
	.uleb128 0x0
	.uleb128 .LEHB182-.LFB3679
	.uleb128 .LEHE182-.LEHB182
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE3679:
	.fnend
	.size	_ZN8QuGlobal6getRefEv, .-_ZN8QuGlobal6getRefEv
	.section	.text._ZN18InstructionManager6updateEv,"axG",%progbits,_ZN18InstructionManager6updateEv,comdat
	.align	2
	.weak	_ZN18InstructionManager6updateEv
	.hidden	_ZN18InstructionManager6updateEv
	.type	_ZN18InstructionManager6updateEv, %function
_ZN18InstructionManager6updateEv:
	.fnstart
.LFB3922:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	mov	r4, r0
	mov	r0, #16640
	bl	glClear
	add	r5, r4, #224
	ldr	ip, [r4, #4]
	add	r0, r4, #4
	ldr	ip, [ip, #8]
	mov	lr, pc
	bx	ip
	ldr	r2, [r4, #224]
	mov	r0, r5
	ldr	ip, [r2, #8]
	mov	lr, pc
	bx	ip
	ldr	r1, [r4, #224]
	mov	r0, r5
	ldr	ip, [r1, #12]
	mov	lr, pc
	bx	ip
	bl	_ZN8QuGlobal6getRefEv
	ldr	r0, [r0, #28]
	bl	__aeabi_i2f
	mov	r6, r0
	bl	_ZN8QuGlobal6getRefEv
	ldr	r0, [r0, #32]
	bl	__aeabi_i2f
	mov	r2, #1065353216
	mov	r1, r0
	mov	r0, r6
	bl	glScalef
	add	r6, r4, #28
	mov	r0, #2896
	bl	glDisable
	ldr	r3, [r4, #28]
	mov	r0, r6
	ldr	ip, [r3, #8]
	mov	lr, pc
	bx	ip
	ldr	r2, [r4, #32]
	cmp	r2, #0
	ble	.L2173
	ldrb	r0, [r4, #36]	@ zero_extendqisi2
	cmp	r0, #0
	ldrne	r0, [r4, #160]
	movne	r1, #0
	blne	glDrawArrays
.L2173:
	mov	r0, r6
	ldr	r2, [r4, #28]
	ldr	ip, [r2, #12]
	mov	lr, pc
	bx	ip
	mov	r0, r5
	ldr	r1, [r4, #224]
	ldr	ip, [r1, #8]
	mov	lr, pc
	bx	ip
	ldr	r3, [r4, #224]
	mov	r0, r5
	ldr	ip, [r3, #12]
	mov	lr, pc
	bx	ip
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
	.fnend
	.size	_ZN18InstructionManager6updateEv, .-_ZN18InstructionManager6updateEv
	.section	.text._ZN23QuAbsScreenIntBoxButton6getBoxE15GDeviceRotation,"axG",%progbits,_ZN23QuAbsScreenIntBoxButton6getBoxE15GDeviceRotation,comdat
	.align	2
	.weak	_ZN23QuAbsScreenIntBoxButton6getBoxE15GDeviceRotation
	.hidden	_ZN23QuAbsScreenIntBoxButton6getBoxE15GDeviceRotation
	.type	_ZN23QuAbsScreenIntBoxButton6getBoxE15GDeviceRotation, %function
_ZN23QuAbsScreenIntBoxButton6getBoxE15GDeviceRotation:
	.fnstart
.LFB3864:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.pad #12
	sub	sp, sp, #12
	mov	r4, r1
	mov	r6, r2
	ldmib	r1, {r8, sl}	@ phole ldm
	mov	r5, r0
	bl	_ZN8QuGlobal6getRefEv
	ldr	r0, [r0, #28]
	str	r0, [sp, #4]
	bl	_ZN8QuGlobal6getRefEv
	ldr	r7, [r4, #16]
	ldr	fp, [r0, #32]
	ldr	r4, [r4, #12]
	bl	_ZN8QuGlobal6getRefEv
	ldr	r9, [r0, #28]
	bl	_ZN8QuGlobal6getRefEv
	cmp	r6, #2
	ldr	r3, [r0, #32]
	beq	.L2180
	cmp	r6, #3
	beq	.L2181
	cmp	r6, #1
	beq	.L2179
	mov	r2, r8
	cmp	r6, #2
	mov	r8, sl
	mov	sl, r2
	beq	.L2185
	cmp	r6, #3
	beq	.L2186
	cmp	r6, #1
	movne	r3, r4
	movne	r4, r7
	movne	r7, r3
	beq	.L2184
.L2187:
	str	sl, [r5, #0]
	str	r8, [r5, #4]
	str	r7, [r5, #8]
	str	r4, [r5, #12]
	mov	r0, r5
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L2179:
	ldr	r1, [sp, #4]
	rsb	r8, r8, r1
.L2184:
	rsb	r4, r4, r9
	b	.L2187
.L2180:
	ldr	lr, [sp, #4]
	rsb	ip, r8, lr
	rsb	r8, sl, fp
	mov	sl, ip
.L2185:
	rsb	r9, r4, r9
	rsb	r4, r7, r3
	mov	r7, r9
	b	.L2187
.L2181:
	rsb	sl, sl, fp
.L2186:
	rsb	r7, r7, r3
	b	.L2187
	.fnend
	.size	_ZN23QuAbsScreenIntBoxButton6getBoxE15GDeviceRotation, .-_ZN23QuAbsScreenIntBoxButton6getBoxE15GDeviceRotation
	.section	.text._Z10keyboardcbP16s3eKeyboardEventPv,"ax",%progbits
	.align	2
	.global	_Z10keyboardcbP16s3eKeyboardEventPv
	.hidden	_Z10keyboardcbP16s3eKeyboardEventPv
	.type	_Z10keyboardcbP16s3eKeyboardEventPv, %function
_Z10keyboardcbP16s3eKeyboardEventPv:
	.fnstart
.LFB4565:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	ldr	r6, [r0, #4]
	cmp	r6, #0
	.pad #16
	sub	sp, sp, #16
	mov	r5, r0
	beq	.L2191
.LEHB183:
	bl	_ZN8QuGlobal6getRefEv
	add	r4, sp, #16
	mov	ip, r0
	mov	r0, #0
	str	r0, [r4, #-8]!
	ldr	r2, [ip, #8]
	mov	r0, r4
	ldr	r1, [ip, #4]
	bl	_ZN15QuStupidPointerI13QuBaseManagerE3setEPS0_i
.LEHE183:
	ldr	r2, [sp, #8]
	ldr	r1, [r5, #0]
	mov	r0, r2
	ldr	r3, [r2, #0]
.LEHB184:
	ldr	ip, [r3, #16]
	mov	lr, pc
	bx	ip
.LEHE184:
	mov	r0, r4
	mov	r1, #0
	ldr	r2, [sp, #12]
.LEHB185:
	bl	_ZN15QuStupidPointerI13QuBaseManagerE3setEPS0_i
.L2197:
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L2191:
	bl	_ZN8QuGlobal6getRefEv
	add	r4, sp, #16
	str	r6, [r4, #-16]!
	mov	ip, r0
	ldr	r2, [r0, #8]
	ldr	r1, [ip, #4]
	mov	r0, sp
	bl	_ZN15QuStupidPointerI13QuBaseManagerE3setEPS0_i
.LEHE185:
	ldr	r2, [sp, #0]
	ldr	r1, [r5, #0]
	mov	r0, r2
	ldr	r3, [r2, #0]
.LEHB186:
	ldr	ip, [r3, #20]
	mov	lr, pc
	bx	ip
.LEHE186:
	mov	r0, sp
	mov	r1, #0
	ldr	r2, [sp, #4]
.LEHB187:
	bl	_ZN15QuStupidPointerI13QuBaseManagerE3setEPS0_i
.LEHE187:
	b	.L2197
.L2199:
.L2193:
	mov	r5, r0
	mov	r0, r4
	bl	_ZN15QuStupidPointerI13QuBaseManagerED1Ev
	mov	r0, r5
.LEHB188:
	bl	__cxa_end_cleanup
.LEHE188:
.L2198:
.L2196:
	mov	r4, r0
	mov	r0, sp
	bl	_ZN15QuStupidPointerI13QuBaseManagerED1Ev
	mov	r0, r4
.LEHB189:
	bl	__cxa_end_cleanup
.LEHE189:
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA4565:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4565-.LLSDACSB4565
.LLSDACSB4565:
	.uleb128 .LEHB183-.LFB4565
	.uleb128 .LEHE183-.LEHB183
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB184-.LFB4565
	.uleb128 .LEHE184-.LEHB184
	.uleb128 .L2199-.LFB4565
	.uleb128 0x0
	.uleb128 .LEHB185-.LFB4565
	.uleb128 .LEHE185-.LEHB185
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB186-.LFB4565
	.uleb128 .LEHE186-.LEHB186
	.uleb128 .L2198-.LFB4565
	.uleb128 0x0
	.uleb128 .LEHB187-.LFB4565
	.uleb128 .LEHE187-.LEHB187
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB188-.LFB4565
	.uleb128 .LEHE188-.LEHB188
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB189-.LFB4565
	.uleb128 .LEHE189-.LEHB189
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE4565:
	.fnend
	.size	_Z10keyboardcbP16s3eKeyboardEventPv, .-_Z10keyboardcbP16s3eKeyboardEventPv
	.section	.text._Z12singleMotionP21s3ePointerMotionEvent,"ax",%progbits
	.align	2
	.global	_Z12singleMotionP21s3ePointerMotionEvent
	.hidden	_Z12singleMotionP21s3ePointerMotionEvent
	.type	_Z12singleMotionP21s3ePointerMotionEvent, %function
_Z12singleMotionP21s3ePointerMotionEvent:
	.fnstart
.LFB4567:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.pad #52
	sub	sp, sp, #52
	ldr	r8, [r0, #4]
	ldr	sl, [r0, #0]
.LEHB190:
	bl	_ZN8QuGlobal6getRefEv
	mov	r4, r0
	bl	_ZN8QuGlobal6getRefEv
	bl	_ZN8QuGlobal17getDeviceRotationEv
	cmp	r0, #4
	beq	.L2221
.L2204:
	cmp	r0, #0
	cmpne	r0, #2
	ldreq	r5, [r4, #32]
	ldreq	r6, [r4, #28]
	ldrne	r5, [r4, #28]
	ldrne	r6, [r4, #32]
	bl	_ZN8QuGlobal6getRefEv
	bl	_ZN8QuGlobal17getDeviceRotationEv
	ldr	r1, .L2222
	add	r3, r1, r0, asl #2
	ldr	r3, [r3, #48]
	cmp	r3, #2
	beq	.L2210
	cmp	r3, #3
	rsbeq	r7, r8, r5
	beq	.L2212
	cmp	r3, #1
	movne	r7, sl
	rsbeq	sl, sl, r6
	movne	sl, r8
	moveq	r7, r8
.L2212:
	cmp	r3, #1
	cmpne	r3, #3
	moveq	r8, r5
	movne	r8, r6
	movne	r6, r5
	bl	_ZN8QuGlobal6getRefEv
	add	r5, sp, #48
	mov	fp, #0
	str	fp, [r5, #-8]!
	mov	r4, r0
	ldr	r2, [r0, #8]
	ldr	r1, [r4, #4]
	mov	r0, r5
	bl	_ZN15QuStupidPointerI13QuBaseManagerE3setEPS0_i
.LEHE190:
	ldr	lr, [sp, #40]
	ldr	r0, [lr, #0]
	add	r9, sp, #48
	rsb	sl, sl, r6
	ldr	fp, [r0, #32]
	str	r7, [r9, #-24]!
	str	r6, [sp, #36]
	str	r8, [sp, #32]
	str	sl, [sp, #28]
	ldmia	r9, {r0, r1, r2, r3}
	add	r9, sp, #4
	stmia	r9, {r0, r1, r2, r3}
	ldr	r4, .L2222+4
	ldr	r2, [r4, #20]
	str	r2, [sp, #0]
	add	ip, r4, #8
	mov	r0, lr
	ldmia	ip, {r1, r2, r3}
.LEHB191:
	mov	lr, pc
	bx	fp
.LEHE191:
	mov	r0, r5
	mov	r1, #0
	ldr	r2, [sp, #44]
.LEHB192:
	bl	_ZN15QuStupidPointerI13QuBaseManagerE3setEPS0_i
	str	r7, [r4, #8]
	str	r6, [r4, #20]
	str	r8, [r4, #16]
	str	sl, [r4, #12]
	add	sp, sp, #52
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L2210:
	rsb	r7, sl, r6
	rsb	sl, r8, r5
	b	.L2212
.L2221:
	bl	_ZN8QuGlobal6getRefEv
	bl	_ZN8QuGlobal17getDeviceRotationEv
.LEHE192:
	b	.L2204
.L2219:
.L2216:
	mov	r6, r0
	mov	r0, r5
	bl	_ZN15QuStupidPointerI13QuBaseManagerED1Ev
	mov	r0, r6
.LEHB193:
	bl	__cxa_end_cleanup
.LEHE193:
.L2223:
	.align	2
.L2222:
	.word	.LANCHOR1
	.word	.LANCHOR0
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA4567:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4567-.LLSDACSB4567
.LLSDACSB4567:
	.uleb128 .LEHB190-.LFB4567
	.uleb128 .LEHE190-.LEHB190
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB191-.LFB4567
	.uleb128 .LEHE191-.LEHB191
	.uleb128 .L2219-.LFB4567
	.uleb128 0x0
	.uleb128 .LEHB192-.LFB4567
	.uleb128 .LEHE192-.LEHB192
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB193-.LFB4567
	.uleb128 .LEHE193-.LEHB193
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE4567:
	.fnend
	.size	_Z12singleMotionP21s3ePointerMotionEvent, .-_Z12singleMotionP21s3ePointerMotionEvent
	.section	.text._Z12apUpdateLoopv,"ax",%progbits
	.align	2
	.global	_Z12apUpdateLoopv
	.hidden	_Z12apUpdateLoopv
	.type	_Z12apUpdateLoopv, %function
_Z12apUpdateLoopv:
	.fnstart
.LFB4570:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	.save {r4, r5, r6, r7, lr}
	.pad #20
	sub	sp, sp, #20
	bl	s3eAccelerometerGetX
	bl	__aeabi_i2f
	mov	r2, #1140850688
	add	r1, r2, #7995392
	bl	__aeabi_fdiv
	mov	r4, r0
	bl	s3eAccelerometerGetY
	bl	__aeabi_i2f
	mov	r1, #1140850688
	add	r1, r1, #7995392
	bl	__aeabi_fdiv
	mov	r5, r0
	bl	s3eAccelerometerGetZ
	mov	r7, r0
	mov	r0, #11
	bl	s3eSurfaceGetInt
	cmp	r0, #2
	addeq	r4, r4, #-2147483648
	addeq	r6, r5, #-2147483648
	beq	.L2229
	cmp	r0, #3
	addeq	r5, r5, #-2147483648
	addeq	r6, r4, #-2147483648
	moveq	r4, r5
	beq	.L2229
	cmp	r0, #1
	moveq	r6, r4
	movne	r6, r5
	moveq	r4, r5
.L2229:
	bl	_ZN8QuGlobal6getRefEv
	mov	r5, r0
	mov	r0, r7
	str	r4, [sp, #4]	@ float
	str	r6, [sp, #8]	@ float
	bl	__aeabi_i2f
	mov	ip, #1140850688
	add	r1, ip, #7995392
	bl	__aeabi_fdiv
	str	r0, [sp, #12]	@ float
	add	r3, sp, #4
	mov	r0, r5
	ldmia	r3, {r1, r2, r3}
	bl	_ZN8QuGlobal8setAccelE9QuVector3IfE
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
	.fnend
	.size	_Z12apUpdateLoopv, .-_Z12apUpdateLoopv
	.section	.text._Z10yieldCyclev,"ax",%progbits
	.align	2
	.global	_Z10yieldCyclev
	.hidden	_Z10yieldCyclev
	.type	_Z10yieldCyclev, %function
_Z10yieldCyclev:
	.fnstart
.LFB4569:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	bl	s3eTimerGetMs
	ldr	r4, .L2238
	ldr	ip, [r4, #24]
	rsb	r1, ip, r0
	str	r1, [r4, #24]
	bl	_ZN8QuGlobal6getRefEv
	ldr	r2, [r4, #24]
	ldr	r3, [r0, #40]
	cmp	r2, r3
	bgt	.L2237
	bl	_ZN8QuGlobal6getRefEv
	ldr	r2, [r4, #24]
	ldr	r0, [r0, #40]
	rsb	r0, r2, r0
	bl	s3eDeviceYield
.L2235:
	bl	s3eTimerGetMs
	str	r0, [r4, #24]
	ldmfd	sp!, {r4, lr}
	bx	lr
.L2237:
	mov	r0, #0
	bl	s3eDeviceYield
	b	.L2235
.L2239:
	.align	2
.L2238:
	.word	.LANCHOR0
	.fnend
	.size	_Z10yieldCyclev, .-_Z10yieldCyclev
	.section	.text._Z6isQuitv,"ax",%progbits
	.align	2
	.global	_Z6isQuitv
	.hidden	_Z6isQuitv
	.type	_Z6isQuitv, %function
_Z6isQuitv:
	.fnstart
.LFB4568:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, lr}
	.save {r3, lr}
	bl	_ZN8QuGlobal6getRefEv
	ldrb	r3, [r0, #1]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L2241
.L2245:
	mov	r0, #1
.L2242:
	ldmfd	sp!, {r3, lr}
	bx	lr
.L2241:
	bl	s3eDeviceCheckQuitRequest
	cmp	r0, #0
	beq	.L2246
.L2243:
	ldr	r0, .L2247
	bl	T.1796
	ldr	r0, .L2247+4
	bl	_ZN4_STL4endlIcNS_11char_traitsIcEEEERNS_13basic_ostreamIT_T0_EES7_
	b	.L2245
.L2246:
	mov	r0, #1
	bl	s3eKeyboardGetState
	tst	r0, #2
	bne	.L2243
	mov	r0, #210
	bl	s3eKeyboardGetState
	ands	r0, r0, #2
	beq	.L2242
	b	.L2243
.L2248:
	.align	2
.L2247:
	.word	.LC4
	.word	_ZN4_STL4coutE
	.fnend
	.size	_Z6isQuitv, .-_Z6isQuitv
	.section	.text._Z21setGameGlobalRotation23s3eSurfaceBlitDirection,"ax",%progbits
	.align	2
	.global	_Z21setGameGlobalRotation23s3eSurfaceBlitDirection
	.hidden	_Z21setGameGlobalRotation23s3eSurfaceBlitDirection
	.type	_Z21setGameGlobalRotation23s3eSurfaceBlitDirection, %function
_Z21setGameGlobalRotation23s3eSurfaceBlitDirection:
	.fnstart
.LFB4562:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	sub	r0, r0, #1
	cmp	r0, #2
	ldrls	r3, .L2253
	addls	r0, r3, r0, asl #2
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	ldrls	r4, [r0, #64]
	movhi	r4, #0
	bl	_ZN8QuGlobal6getRefEv
	mov	r1, r4
	bl	_ZN8QuGlobal17setDeviceRotationE15GDeviceRotation
	ldmfd	sp!, {r4, lr}
	bx	lr
.L2254:
	.align	2
.L2253:
	.word	.LANCHOR1
	.fnend
	.size	_Z21setGameGlobalRotation23s3eSurfaceBlitDirection, .-_Z21setGameGlobalRotation23s3eSurfaceBlitDirection
	.global	__aeabi_fcmple
	.section	.text.T.1800,"ax",%progbits
	.align	2
	.type	T.1800, %function
T.1800:
	.fnstart
.LFB6350:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, lr}
	ldr	r5, .L2266
	mov	r4, r0
	str	r5, [r0, #0]
	cmn	r2, #1
	mov	r0, #0
	.pad #8
	sub	sp, sp, #8
	str	r0, [r4, #32]
	mov	r5, r3
	str	r1, [r4, #16]
	strne	r2, [r4, #20]
	beq	.L2265
.L2257:
	bl	_ZN8QuGlobal6getRefEv
	ldr	r0, [r0, #32]
	bl	__aeabi_i2f
	mov	r7, r0
	bl	_ZN8QuGlobal6getRefEv
	mov	r8, r0
	ldr	r0, [r4, #20]
	bl	__aeabi_i2f
	mov	r9, r0
	ldr	r0, [r4, #16]
	bl	__aeabi_i2f
	mov	r1, r0
	mov	r0, r9
	bl	__aeabi_fdiv
	mov	r6, r0
	ldr	r0, [r8, #28]
	bl	__aeabi_i2f
	mov	r1, r0
	mov	r0, r7
	bl	__aeabi_fdiv
	mov	r1, r6
	bl	__aeabi_fcmple
	cmp	r0, #0
	beq	.L2264
	bl	_ZN8QuGlobal6getRefEv
	ldr	r0, [r0, #28]
	bl	__aeabi_i2f
	mov	r8, r0
	bl	_ZN8QuGlobal6getRefEv
	ldr	r0, [r0, #28]
	bl	__aeabi_i2f
	mov	r1, r0
	mov	r0, r6
	bl	__aeabi_fmul
	mov	r7, r0
	bl	_ZN8QuGlobal6getRefEv
	ldr	r0, [r0, #32]
	bl	__aeabi_i2f
	mov	r1, r7
	bl	__aeabi_fsub
	mov	r1, #1056964608
	bl	__aeabi_fmul
	bl	__aeabi_f2iz
	mov	r9, #0
	mov	sl, r0
.L2260:
	mov	r0, r8
	bl	__aeabi_f2iz
	mov	r6, r0
	mov	r0, r7
	bl	__aeabi_f2iz
	mov	r2, r6
	mov	r3, r0
	mov	r1, sl
	mov	r0, r9
	bl	glViewport
	add	r2, r4, #16
	ldmia	r2, {r2, r3}	@ phole ldm
	mov	r0, #1124073472
	add	ip, r0, #16384000
	str	ip, [r4, #12]	@ float
	str	r5, [r4, #8]	@ float
	str	r2, [r4, #24]
	str	r3, [r4, #28]
	mov	r0, r4
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L2264:
	bl	_ZN8QuGlobal6getRefEv
	ldr	r0, [r0, #32]
	bl	__aeabi_i2f
	mov	r7, r0
	bl	_ZN8QuGlobal6getRefEv
	ldr	r0, [r0, #32]
	bl	__aeabi_i2f
	mov	r1, r6
	bl	__aeabi_fdiv
	mov	r8, r0
	bl	_ZN8QuGlobal6getRefEv
	ldr	r0, [r0, #28]
	bl	__aeabi_i2f
	mov	r1, r8
	bl	__aeabi_fsub
	mov	r1, #1056964608
	bl	__aeabi_fmul
	bl	__aeabi_f2iz
	mov	sl, #0
	mov	r9, r0
	b	.L2260
.L2265:
	str	r1, [sp, #4]
	bl	_ZN8QuGlobal6getRefEv
	ldr	r1, [sp, #4]
	ldr	r0, [r0, #32]
	mul	r0, r1, r0
	bl	__aeabi_i2f
	mov	r6, r0
	bl	_ZN8QuGlobal6getRefEv
	ldr	r0, [r0, #28]
	bl	__aeabi_i2f
	mov	r1, r0
	mov	r0, r6
	bl	__aeabi_fdiv
	bl	__aeabi_f2iz
	str	r0, [r4, #20]
	b	.L2257
.L2267:
	.align	2
.L2266:
	.word	_ZTV13QuBasicCamera+8
	.fnend
	.size	T.1800, .-T.1800
	.section	.text._ZN23QuAbsScreenIntBoxButton15drawRotatedRectE7QuColor15GDeviceRotation,"axG",%progbits,_ZN23QuAbsScreenIntBoxButton15drawRotatedRectE7QuColor15GDeviceRotation,comdat
	.align	2
	.weak	_ZN23QuAbsScreenIntBoxButton15drawRotatedRectE7QuColor15GDeviceRotation
	.hidden	_ZN23QuAbsScreenIntBoxButton15drawRotatedRectE7QuColor15GDeviceRotation
	.type	_ZN23QuAbsScreenIntBoxButton15drawRotatedRectE7QuColor15GDeviceRotation, %function
_ZN23QuAbsScreenIntBoxButton15drawRotatedRectE7QuColor15GDeviceRotation:
	.fnstart
.LFB3859:
	@ Function supports interworking.
	@ args = 24, pretend = 16, frame = 360
	@ frame_needed = 0, uses_anonymous_args = 0
	.pad #16
	sub	sp, sp, #16
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.pad #364
	sub	sp, sp, #364
	add	ip, sp, #404
	stmia	ip, {r1, r2, r3}
	ldr	r5, [sp, #420]
	mov	sl, r0
	ldr	r4, [sp, #404]	@ float
	ldr	r6, [sp, #408]	@ float
	ldr	r7, [sp, #412]	@ float
	ldr	r8, [sp, #416]	@ float
.LEHB194:
	bl	_ZN8QuGlobal6getRefEv
	ldr	fp, [r0, #28]
	bl	_ZN8QuGlobal6getRefEv
	ldr	r9, [r0, #32]
	bl	_ZN8QuGlobal6getRefEv
	ldr	r0, [r0, #32]
	bl	__aeabi_i2f
	mov	r1, fp
	mov	r3, r0
	mov	r2, r9
	add	r0, sp, #308
	bl	T.1800
.LEHE194:
	cmp	r5, #1
	cmpne	r5, #3
	ldreq	r3, [sp, #324]
	streq	r3, [sp, #336]
	ldreq	r3, [sp, #328]
	streq	r3, [sp, #332]
	cmp	r5, #0
	cmpne	r5, #2
	ldreq	r3, [sp, #328]
	ldr	r0, .L2303
	streq	r3, [sp, #336]
	ldreq	r3, [sp, #324]
	add	r5, r0, r5, asl #2
	streq	r3, [sp, #332]
	ldr	r3, [r5, #76]
	mov	r0, #2896
	str	r3, [sp, #340]
.LEHB195:
	bl	glDisable
	add	sl, sl, #4
	ldmia	sl, {r0, r1, r2, r3}
	add	r5, sp, #344
	stmia	r5, {r0, r1, r2, r3}
	add	r0, sp, #12
	bl	_ZN12QuDrawObjectC2Ev
.LEHE195:
	ldmia	r5, {r0, r1, r2, r3}
	add	lr, sp, #148
	ldr	sl, .L2303+4
	stmia	lr, {r0, r1, r2, r3}
	mov	r5, #4
	str	r5, [sp, #16]
	str	sl, [sp, #12]
	bl	__aeabi_i2f
	mov	r5, r0
	ldr	r0, [sp, #152]
	bl	__aeabi_i2f
	mov	sl, r0
	ldr	r0, [sp, #156]
	bl	__aeabi_i2f
	mov	fp, r0
	ldr	r0, [sp, #160]
	bl	__aeabi_i2f
	mov	r3, #0
	add	r2, sp, #268
	str	r3, [r2], #4
	add	ip, r2, #8
	str	r3, [ip], #4
	mov	r1, r0
	add	r0, ip, #8
	str	r3, [r0], #4
	add	r9, r0, #8
	str	r3, [r9, #0]
	mov	r0, sl
	str	r5, [sp, #260]	@ float
	bl	__aeabi_fadd
	mov	r1, fp
	mov	r9, r0
	mov	r0, r5
	str	r9, [sp, #264]	@ float
	bl	__aeabi_fadd
	mov	r2, #0
	str	r5, [sp, #212]	@ float
	str	r2, [sp, #220]	@ float
	str	r9, [sp, #216]	@ float
	str	r0, [sp, #224]	@ float
	add	r1, sp, #212
	mov	fp, r1
	mov	ip, r0
	ldmia	fp!, {r0, r1, r2, r3}
	mov	lr, #0
	str	fp, [sp, #4]
	add	fp, sp, #164
	str	lr, [sp, #232]	@ float
	str	r5, [sp, #236]	@ float
	str	lr, [sp, #244]	@ float
	str	ip, [sp, #248]	@ float
	str	lr, [sp, #256]	@ float
	str	ip, [sp, #272]	@ float
	str	r5, [sp, #284]	@ float
	str	r9, [sp, #228]	@ float
	str	sl, [sp, #252]	@ float
	str	r9, [sp, #276]	@ float
	str	sl, [sp, #288]	@ float
	str	ip, [sp, #296]	@ float
	mov	ip, fp
	str	sl, [sp, #240]	@ float
	stmia	ip!, {r0, r1, r2, r3}
	str	sl, [sp, #300]	@ float
	ldr	r5, [sp, #4]
	ldmia	r5!, {r0, r1, r2, r3}
	mov	lr, ip
	stmia	lr!, {r0, r1, r2, r3}
	ldmia	r5, {r0, r1, r2, r3}
	stmia	lr, {r0, r1, r2, r3}
	mov	r0, #48
.LEHB196:
	bl	_Znaj
.LEHE196:
	mov	lr, fp
	mov	r5, r0
	ldmia	lr!, {r0, r1, r2, r3}
	mov	sl, r5
	stmia	sl!, {r0, r1, r2, r3}
	ldmia	lr!, {r0, r1, r2, r3}
	mov	ip, sl
	stmia	ip!, {r0, r1, r2, r3}
	ldmia	lr, {r0, r1, r2, r3}
	stmia	ip, {r0, r1, r2, r3}
	ldrb	r1, [sp, #20]	@ zero_extendqisi2
	cmp	r1, #0
	bne	.L2299
.L2290:
	ldr	r0, [sp, #16]
	mov	ip, #32768
	mov	r2, #0
	add	r1, ip, #116
	str	r5, [sp, #28]
	mov	r3, #1
	mov	r5, #3
	mov	r0, r0, asl #4
	str	r2, [sp, #36]
	str	r1, [sp, #40]
	str	r5, [sp, #44]
	strb	r3, [sp, #20]
	str	r2, [sp, #32]
.LEHB197:
	bl	_Znaj
	ldr	lr, [sp, #16]
	cmp	lr, #0
	mov	r5, r0
	ble	.L2277
	mov	r3, r0
	mov	r2, #0
.L2278:
	str	r4, [r3, #0]	@ float
	str	r6, [r3, #4]	@ float
	str	r7, [r3, #8]	@ float
	str	r8, [r3, #12]	@ float
	ldr	r1, [sp, #16]
	add	r2, r2, #1
	cmp	r2, r1
	add	r3, r3, #16
	blt	.L2278
.L2277:
	cmp	r5, #0
	beq	.L2300
	ldrb	r4, [sp, #104]	@ zero_extendqisi2
	cmp	r4, #0
	bne	.L2301
.L2291:
	mov	lr, #32768
	mov	r2, #0
	add	ip, lr, #118
	mov	r0, #4
	mov	r3, #1
	str	r5, [sp, #112]
	str	r2, [sp, #120]
	str	ip, [sp, #124]
	str	r0, [sp, #128]
	strb	r3, [sp, #104]
	str	r2, [sp, #116]
.L2280:
	bl	glPushMatrix
	add	r0, sp, #308
	ldr	r1, [sp, #308]
	ldr	ip, [r1, #8]
	mov	lr, pc
	bx	ip
	add	r0, sp, #308
	ldr	r4, [sp, #308]
	ldr	ip, [r4, #12]
	mov	lr, pc
	bx	ip
	ldr	r0, [sp, #332]
	bl	__aeabi_i2f
	mov	r1, #-1090519040
	bl	__aeabi_fmul
	mov	r4, r0
	ldr	r0, [sp, #336]
	bl	__aeabi_i2f
	mov	r1, #-1090519040
	bl	__aeabi_fmul
	mov	r2, #0
	mov	r1, r0
	mov	r0, r4
	bl	glTranslatef
	add	r0, sp, #360
	ldr	r2, [r0, #-348]!
	ldr	ip, [r2, #8]
	mov	lr, pc
	bx	ip
	ldr	r2, [sp, #16]
	cmp	r2, #0
	ble	.L2283
	ldrb	r3, [sp, #20]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L2302
.L2283:
	add	r0, sp, #360
	ldr	ip, [r0, #-348]!
	ldr	ip, [ip, #12]
	mov	lr, pc
	bx	ip
	bl	glPopMatrix
.LEHE197:
	ldr	lr, .L2303+4
	add	r0, sp, #360
	str	lr, [r0, #-348]!
.LEHB198:
	bl	_ZN12QuDrawObjectD2Ev
.LEHE198:
	add	sp, sp, #364
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	add	sp, sp, #16
	bx	lr
.L2302:
	ldr	r0, [sp, #144]
	mov	r1, #0
.LEHB199:
	bl	glDrawArrays
.LEHE199:
	b	.L2283
.L2299:
	ldr	r0, [sp, #28]
	cmp	r0, #0
	beq	.L2290
	bl	_ZdaPv
	b	.L2290
.L2301:
	ldr	r0, [sp, #112]
	cmp	r0, #0
	beq	.L2291
	bl	_ZdaPv
	b	.L2291
.L2300:
	add	r0, sp, #104
	bl	_ZN14QuArrayPointerIfE6deInitEv
	b	.L2280
.L2294:
	mov	r4, r0
.L2287:
	mov	r0, r4
.LEHB200:
	bl	__cxa_end_cleanup
.LEHE200:
.L2293:
.L2285:
	mov	r4, r0
	add	r0, sp, #12
	bl	_ZN21QuRectangleDrawObjectIiED1Ev
	b	.L2287
.L2292:
.L2276:
	mov	r4, r0
	add	r0, sp, #12
	bl	_ZN12QuDrawObjectD2Ev
	b	.L2287
.L2304:
	.align	2
.L2303:
	.word	.LANCHOR1
	.word	_ZTV21QuRectangleDrawObjectIiE+8
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA3859:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3859-.LLSDACSB3859
.LLSDACSB3859:
	.uleb128 .LEHB194-.LFB3859
	.uleb128 .LEHE194-.LEHB194
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB195-.LFB3859
	.uleb128 .LEHE195-.LEHB195
	.uleb128 .L2294-.LFB3859
	.uleb128 0x0
	.uleb128 .LEHB196-.LFB3859
	.uleb128 .LEHE196-.LEHB196
	.uleb128 .L2292-.LFB3859
	.uleb128 0x0
	.uleb128 .LEHB197-.LFB3859
	.uleb128 .LEHE197-.LEHB197
	.uleb128 .L2293-.LFB3859
	.uleb128 0x0
	.uleb128 .LEHB198-.LFB3859
	.uleb128 .LEHE198-.LEHB198
	.uleb128 .L2294-.LFB3859
	.uleb128 0x0
	.uleb128 .LEHB199-.LFB3859
	.uleb128 .LEHE199-.LEHB199
	.uleb128 .L2293-.LFB3859
	.uleb128 0x0
	.uleb128 .LEHB200-.LFB3859
	.uleb128 .LEHE200-.LEHB200
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE3859:
	.fnend
	.size	_ZN23QuAbsScreenIntBoxButton15drawRotatedRectE7QuColor15GDeviceRotation, .-_ZN23QuAbsScreenIntBoxButton15drawRotatedRectE7QuColor15GDeviceRotation
	.section	.text._Z8mainInitv,"ax",%progbits
	.align	2
	.global	_Z8mainInitv
	.hidden	_Z8mainInitv
	.type	_Z8mainInitv, %function
_Z8mainInitv:
	.fnstart
.LFB4571:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
.LEHB201:
	bl	_Z8IwGxInitv
	mov	r0, #5888
	bl	glMatrixMode
	mov	r0, #33792
	add	r0, r0, #192
	bl	glActiveTexture
	bl	_Z11IwSoundInitv
	bl	_Z16IwResManagerInitv
	ldr	r3, .L2313
	mov	r0, #212
	ldr	r5, [r3, #0]
	bl	_Znwj
.LEHE201:
	mov	r4, r0
.LEHB202:
	bl	_ZN16CIwResHandlerWAVC1Ev
.LEHE202:
	mov	r1, r4
	mov	r0, r5
.LEHB203:
	bl	_ZN13CIwResManager10AddHandlerEP13CIwResHandler
	bl	s3eAccelerometerStart
	mov	r0, #0
	ldr	r1, .L2313+4
	mov	r2, r0
	bl	s3eKeyboardRegister
	mov	r0, #0
	ldr	r1, .L2313+8
	mov	r2, r0
	bl	s3ePointerRegister
	ldr	r1, .L2313+12
	mov	r2, #0
	mov	r0, #1
	bl	s3ePointerRegister
	ldr	r1, .L2313+16
	mov	r2, #0
	mov	r0, #1
	bl	s3eSurfaceRegister
	bl	_ZN8QuGlobal6getRefEv
	mov	r5, r0
	mov	r0, #0
	bl	s3eSurfaceGetInt
	mov	r4, r0
	mov	r0, #1
	bl	s3eSurfaceGetInt
	mov	r1, r4
	mov	r2, r0
	mov	r0, r5
	bl	_ZN8QuGlobal13setScreenSizeEii
	bl	_Z12apUpdateLoopv
	mov	r0, #2
	bl	_Z10IwGLGetInt12IwGLProperty
	ldr	r1, .L2313+20
	add	r3, r1, r0, asl #2
	ldr	ip, [r3, #92]
	sub	r2, ip, #1
	cmp	r2, #2
	addls	r1, r1, r2, asl #2
	ldrls	r4, [r1, #64]
	movhi	r4, #0
	bl	_ZN8QuGlobal6getRefEv
	mov	r1, r4
	bl	_ZN8QuGlobal17setDeviceRotationE15GDeviceRotation
	mov	r0, #7
	mov	r1, #0
	bl	_Z10IwGLSetInt12IwGLPropertyi
	mov	r0, #0
	ldr	r1, .L2313+24
	mov	r2, r0
	bl	s3eDeviceRegister
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L2311:
.L2307:
	mov	r5, r0
	mov	r0, r4
	bl	_ZdlPv
	mov	r0, r5
	bl	__cxa_end_cleanup
.LEHE203:
.L2314:
	.align	2
.L2313:
	.word	g_IwResManager
	.word	_Z10keyboardcbP16s3eKeyboardEventPv
	.word	_Z11singleTouchP15s3ePointerEvent
	.word	_Z12singleMotionP21s3ePointerMotionEvent
	.word	_Z6resizePvS_
	.word	.LANCHOR1
	.word	_Z13myDevicePausePvS_
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA4571:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4571-.LLSDACSB4571
.LLSDACSB4571:
	.uleb128 .LEHB201-.LFB4571
	.uleb128 .LEHE201-.LEHB201
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB202-.LFB4571
	.uleb128 .LEHE202-.LEHB202
	.uleb128 .L2311-.LFB4571
	.uleb128 0x0
	.uleb128 .LEHB203-.LFB4571
	.uleb128 .LEHE203-.LEHB203
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE4571:
	.fnend
	.size	_Z8mainInitv, .-_Z8mainInitv
	.section	.text._Z6resizePvS_,"ax",%progbits
	.align	2
	.global	_Z6resizePvS_
	.hidden	_Z6resizePvS_
	.type	_Z6resizePvS_, %function
_Z6resizePvS_:
	.fnstart
.LFB4563:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	ldrb	r3, [r0, #0]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L2319
	ldr	r1, [r0, #12]
	sub	r0, r1, #1
	cmp	r0, #2
	ldrls	r2, .L2320
	addls	r0, r2, r0, asl #2
	ldrls	r4, [r0, #64]
	movhi	r4, #0
	bl	_ZN8QuGlobal6getRefEv
	mov	r1, r4
	bl	_ZN8QuGlobal17setDeviceRotationE15GDeviceRotation
.L2319:
	ldmfd	sp!, {r4, lr}
	bx	lr
.L2321:
	.align	2
.L2320:
	.word	.LANCHOR1
	.fnend
	.size	_Z6resizePvS_, .-_Z6resizePvS_
	.section	.text._Z7runGamev,"ax",%progbits
	.align	2
	.global	_Z7runGamev
	.hidden	_Z7runGamev
	.type	_Z7runGamev, %function
_Z7runGamev:
	.fnstart
.LFB4573:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.pad #28
	sub	sp, sp, #28
.LEHB204:
	bl	_Z6isQuitv
	subs	r7, r0, #0
	mov	r6, #0
	ldr	sl, .L2381
	add	r9, sp, #16
	add	fp, sp, #20
	add	r8, sp, #8
	bne	.L2376
.L2358:
	bl	s3eKeyboardUpdate
	bl	s3ePointerUpdate
	bl	_Z12apUpdateLoopv
	bl	_ZN8QuGlobal6getRefEv
	ldr	r5, [r0, #36]
	mov	r4, r0
	add	r0, r5, #1
	str	r0, [r4, #36]
	ldr	r2, [r4, #20]
	mov	r0, r2
	ldr	r1, [r2, #0]
	ldr	ip, [r1, #28]
	mov	lr, pc
	bx	ip
	ldr	r4, [r4, #12]
	ldr	r3, [r4, #16]
	cmp	r3, #0
	beq	.L2324
	ldr	lr, [r4, #12]
	ldr	ip, [lr, #8]
	mov	r1, #100
	ldr	r0, [ip, #28]
	bl	_ZN10QuPngImage9pumpImageEi
	cmp	r0, #0
	beq	.L2324
	ldr	r0, [r4, #12]
	add	r1, r0, #4
	add	r2, r0, #8
	add	r3, r0, #12
	ldr	r0, [r0, #8]
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
.LEHE204:
	mov	r5, r0
	mov	r1, r7
	add	r0, r0, #28
	ldr	r2, [r5, #32]
.LEHB205:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE205:
	ldr	r0, [r5, #16]
	cmp	r0, #0
	blne	free
.L2329:
	cmp	r5, #0
	movne	r0, r5
	blne	free
.L2331:
	ldr	r3, [r4, #16]
	sub	r7, r3, #1
	str	r7, [r4, #16]
.L2324:
.LEHB206:
	bl	_ZN8QuGlobal6getRefEv
.LEHE206:
	str	r6, [sp, #8]
	ldr	r4, [r0, #4]
	ldr	r3, [r0, #8]
	cmp	r4, #0
	str	r3, [sp, #12]
	str	r4, [sp, #8]
	moveq	r3, r6
	beq	.L2333
	ldr	ip, [sl, #0]
	ldr	r0, [ip, #4]
	cmp	r0, #0
	beq	.L2334
	mov	r1, ip
	mov	r3, r0
	b	.L2338
.L2377:
	mov	r1, r3
	mov	r3, r2
.L2338:
	ldr	r2, [r3, #16]
	cmp	r4, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L2377
	cmp	ip, r3
	beq	.L2334
	ldr	r1, [r3, #16]
	cmp	r4, r1
	bcc	.L2334
.L2339:
	cmp	r3, ip
	mov	r2, r3
	beq	.L2375
.L2341:
	ldr	lr, [r2, #20]
	add	r4, lr, #1
	str	r4, [r2, #20]
	ldr	r3, [sp, #8]
.L2333:
	mov	r0, r3
	ldr	r7, [r3, #0]
.LEHB207:
	ldr	ip, [r7, #12]
	mov	lr, pc
	bx	ip
.LEHE207:
	mov	r0, r8
	mov	r1, #0
	ldr	r2, [sp, #12]
.LEHB208:
	bl	_ZN15QuStupidPointerI13QuBaseManagerE3setEPS0_i
	bl	_Z15IwGLSwapBuffersv
	bl	_Z10yieldCyclev
	bl	_Z6isQuitv
.LEHE208:
	subs	r7, r0, #0
	beq	.L2358
.L2376:
	ldr	r5, .L2381+4
	ldr	r4, [r5, #0]
	cmp	r4, #0
	beq	.L2359
	ldrb	r0, [r4, #0]	@ zero_extendqisi2
	cmp	r0, #0
	bne	.L2378
.L2360:
	add	r0, r4, #20
	mov	r1, #0
	ldr	r2, [r4, #24]
.LEHB209:
	bl	_ZN15QuStupidPointerI23QuSoundManagerInterfaceE3setEPS0_i
.LEHE209:
	add	r0, r4, #12
	mov	r1, #0
	ldr	r2, [r4, #16]
.LEHB210:
	bl	_ZN15QuStupidPointerI14QuImageManagerE3setEPS0_i
.LEHE210:
	add	r0, r4, #4
	mov	r1, #0
	ldr	r2, [r4, #8]
.LEHB211:
	bl	_ZN15QuStupidPointerI13QuBaseManagerE3setEPS0_i
	mov	r0, r4
	bl	_ZdlPv
.L2359:
	mov	r1, #0
	str	r1, [r5, #0]
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L2379:
	ldr	r2, [r0, #16]
	cmp	r4, r2
	movls	ip, r0
	ldrhi	r0, [r0, #12]
	ldrls	r0, [r0, #8]
.L2375:
	cmp	r0, #0
	bne	.L2379
	cmp	r3, ip
	beq	.L2347
	ldr	r5, [ip, #16]
	cmp	r4, r5
	mov	r3, ip
	bcs	.L2348
.L2347:
	mov	r3, sp
	mov	r0, r9
	ldr	r1, .L2381
	mov	r2, fp
	str	ip, [sp, #20]
	stmia	sp, {r4, r6}	@ phole stm
	bl	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS4_NS_16_Nonconst_traitsIS4_EEEERKS4_
	ldr	r3, [sp, #16]
.L2348:
	str	r6, [r3, #20]
	ldr	r0, [sl, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	beq	.L2349
	mov	r1, r0
	b	.L2353
.L2380:
	mov	r1, r3
	mov	r3, r2
.L2353:
	ldr	r2, [r3, #16]
	cmp	r4, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L2380
	cmp	r0, r3
	beq	.L2349
	ldr	ip, [r3, #16]
	cmp	r4, ip
	bcs	.L2354
.L2349:
	mov	r3, r0
.L2354:
	mov	r2, r3
	b	.L2341
.L2334:
	mov	r3, ip
	b	.L2339
.L2378:
	add	r0, r4, #20
	ldr	r2, [r4, #24]
	mov	r1, #0
	bl	_ZN15QuStupidPointerI23QuSoundManagerInterfaceE3setEPS0_i
	add	r0, r4, #12
	ldr	r2, [r4, #16]
	mov	r1, #0
	bl	_ZN15QuStupidPointerI14QuImageManagerE3setEPS0_i
	add	r0, r4, #4
	ldr	r2, [r4, #8]
	mov	r1, #0
	bl	_ZN15QuStupidPointerI13QuBaseManagerE3setEPS0_i
.LEHE211:
	mov	r3, #0
	strb	r3, [r4, #0]
	ldr	r4, [r5, #0]
	cmp	r4, r3
	bne	.L2360
	b	.L2359
.L2371:
.L2356:
	mov	r8, r0
	add	r0, sp, #8
	bl	_ZN15QuStupidPointerI13QuBaseManagerED1Ev
	mov	r0, r8
.LEHB212:
	bl	__cxa_end_cleanup
.L2368:
.L2330:
	mov	r6, r0
	add	r0, r5, #16
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	mov	r0, r6
	bl	__cxa_end_cleanup
.LEHE212:
.L2370:
	mov	r5, r0
.L2366:
	add	r0, r4, #4
	bl	_ZN15QuStupidPointerI13QuBaseManagerED1Ev
	mov	r0, r5
.LEHB213:
	bl	__cxa_end_cleanup
.LEHE213:
.L2369:
.L2363:
	mov	r5, r0
	mov	r1, #0
	add	r0, r4, #12
	ldr	r2, [r4, #16]
	bl	_ZN15QuStupidPointerI14QuImageManagerE3setEPS0_i
	b	.L2366
.L2382:
	.align	2
.L2381:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.word	_ZN8QuGlobal5mSelfE
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA4573:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4573-.LLSDACSB4573
.LLSDACSB4573:
	.uleb128 .LEHB204-.LFB4573
	.uleb128 .LEHE204-.LEHB204
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB205-.LFB4573
	.uleb128 .LEHE205-.LEHB205
	.uleb128 .L2368-.LFB4573
	.uleb128 0x0
	.uleb128 .LEHB206-.LFB4573
	.uleb128 .LEHE206-.LEHB206
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB207-.LFB4573
	.uleb128 .LEHE207-.LEHB207
	.uleb128 .L2371-.LFB4573
	.uleb128 0x0
	.uleb128 .LEHB208-.LFB4573
	.uleb128 .LEHE208-.LEHB208
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB209-.LFB4573
	.uleb128 .LEHE209-.LEHB209
	.uleb128 .L2369-.LFB4573
	.uleb128 0x0
	.uleb128 .LEHB210-.LFB4573
	.uleb128 .LEHE210-.LEHB210
	.uleb128 .L2370-.LFB4573
	.uleb128 0x0
	.uleb128 .LEHB211-.LFB4573
	.uleb128 .LEHE211-.LEHB211
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB212-.LFB4573
	.uleb128 .LEHE212-.LEHB212
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB213-.LFB4573
	.uleb128 .LEHE213-.LEHB213
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE4573:
	.fnend
	.size	_Z7runGamev, .-_Z7runGamev
	.section	.text._Z15INITIALIZE_GAMEI18InstructionManager19QuApRawSoundManagerEiv,"axG",%progbits,_Z15INITIALIZE_GAMEI18InstructionManager19QuApRawSoundManagerEiv,comdat
	.align	2
	.weak	_Z15INITIALIZE_GAMEI18InstructionManager19QuApRawSoundManagerEiv
	.hidden	_Z15INITIALIZE_GAMEI18InstructionManager19QuApRawSoundManagerEiv
	.type	_Z15INITIALIZE_GAMEI18InstructionManager19QuApRawSoundManagerEiv, %function
_Z15INITIALIZE_GAMEI18InstructionManager19QuApRawSoundManagerEiv:
	.fnstart
.LFB5026:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 64
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.pad #68
	sub	sp, sp, #68
.LEHB214:
	bl	_Z8mainInitv
	bl	_ZN8QuGlobal6getRefEv
	str	r0, [sp, #4]
	mov	r0, #264
	bl	_Znwj
.LEHE214:
	ldr	fp, .L2508
	mov	r4, r0
	add	r9, r0, #20
	ldr	r0, .L2508+4
	mov	r1, #0
	mov	r3, #10
	str	r0, [r4, #0]
	str	r3, [r4, #16]
	str	fp, [r4, #4]
	str	r1, [r4, #8]
	str	r1, [r4, #12]
	str	r1, [r4, #20]
	mov	r0, r9
	mov	r2, #1
.LEHB215:
	bl	_ZN15QuStupidPointerI22QuSoundStructInterfaceE3setEPS0_i
.LEHE215:
	add	sl, r4, #28
	mov	r0, sl
.LEHB216:
	bl	_ZN12QuDrawObjectC2Ev
.LEHE216:
	ldr	r8, .L2508+8
	ldr	r2, .L2508+12
	mov	r1, #0
	str	r2, [r4, #28]
	str	r1, [r4, #184]
	strb	r1, [r4, #164]
	str	r8, [r4, #168]
	str	r1, [r4, #172]
	str	r1, [r4, #176]
	str	r1, [r4, #180]
	add	r5, r4, #188
.LEHB217:
	bl	_ZN8QuGlobal6getRefEv
	ldr	r6, [r0, #28]
	bl	_ZN8QuGlobal6getRefEv
	ldr	r7, [r0, #32]
	bl	_ZN8QuGlobal6getRefEv
	ldr	r0, [r0, #32]
	bl	__aeabi_i2f
	mov	r1, r6
	mov	r3, r0
	mov	r2, r7
	mov	r0, r5
	bl	T.1800
.LEHE217:
	add	r5, r4, #224
.LEHB218:
	bl	_ZN8QuGlobal6getRefEv
	ldr	r6, [r0, #28]
	bl	_ZN8QuGlobal6getRefEv
	ldr	r7, [r0, #32]
	bl	_ZN8QuGlobal6getRefEv
	ldr	r0, [r0, #32]
	bl	__aeabi_i2f
	mov	r1, r6
	mov	r3, r0
	mov	r2, r7
	mov	r0, r5
	bl	T.1800
.LEHE218:
	add	r5, sp, #12
	mov	r0, r5
	ldr	r1, .L2508+16
	add	r2, sp, #60
.LEHB219:
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1EPKcRKS4_
.LEHE219:
	ldr	r0, [sp, #12]
	ldr	r1, .L2508+20
.LEHB220:
	bl	s3eFileOpen
.LEHE220:
	str	r0, [r4, #260]
	ldr	r0, [sp, #12]
	cmp	r0, #0
	blne	free
.L2390:
	ldr	r5, .L2508+24
	mov	r6, #1
	str	r6, [sp, #52]
	str	r4, [sp, #48]
	str	r4, [sp, #56]
	ldr	r0, [r5, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	beq	.L2402
	mov	r1, r0
	b	.L2406
.L2501:
	mov	r1, r3
	mov	r3, r2
.L2406:
	ldr	r2, [r3, #16]
	cmp	r4, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L2501
	cmp	r0, r3
	beq	.L2402
	ldr	r7, [r3, #16]
	cmp	r4, r7
	bcc	.L2402
.L2407:
	cmp	r3, r0
	beq	.L2502
.L2409:
	ldr	lr, [r3, #20]
	add	ip, lr, #1
	add	r4, sp, #48
	str	ip, [r3, #20]
	ldr	r0, [sp, #4]
	mov	r1, r4
.LEHB221:
	bl	_ZN8QuGlobal10setManagerE15QuStupidPointerI13QuBaseManagerE
.LEHE221:
	mov	r1, #0
	ldr	r2, [sp, #52]
	mov	r0, r4
.LEHB222:
	bl	_ZN15QuStupidPointerI13QuBaseManagerE3setEPS0_i
	bl	_ZN8QuGlobal6getRefEv
	mov	r7, r0
	mov	r0, #16
	bl	_Znwj
	mov	r4, r0
	ldr	r0, .L2508+28
	mov	r3, #0
	stmia	r4, {r0, r3}	@ phole stm
	mov	r0, #36
	bl	malloc
	cmp	r0, #0
	beq	.L2503
.L2422:
	mov	r1, #0
	stmib	r4, {r0, r1}	@ phole stm
	strb	r1, [r0, #0]
	ldr	r6, [r4, #4]
	str	r1, [r6, #4]
	ldr	r5, [r4, #4]
	str	r5, [r5, #8]
	ldr	r2, [r4, #4]
	add	r6, sp, #64
	str	r2, [r2, #12]
	str	r1, [r6, #-24]!
	mov	r2, #1
	mov	r0, r6
	mov	r1, r4
	bl	_ZN15QuStupidPointerI23QuSoundManagerInterfaceE3setEPS0_i
.LEHE222:
	mov	r0, #36
	add	r8, r7, #12
.LEHB223:
	bl	_Znwj
	mov	ip, #0
	str	ip, [r0, #0]
	mov	r4, r0
	mov	r0, #36
	bl	malloc
	cmp	r0, #0
	beq	.L2504
.L2433:
	mov	lr, #0
	stmia	r4, {r0, lr}	@ phole stm
	strb	lr, [r0, #0]
	ldr	r1, [r4, #0]
	str	lr, [r1, #4]
	ldr	r0, [r4, #0]
	str	r0, [r0, #8]
	ldr	r3, [r4, #0]
	mov	r0, #36
	str	r3, [r3, #12]
	str	lr, [r4, #12]
	bl	malloc
	cmp	r0, #0
	beq	.L2505
.L2440:
	mov	r5, #0
	str	r0, [r4, #12]
	str	r5, [r4, #16]
	strb	r5, [r0, #0]
	ldr	lr, [r4, #12]
	str	r5, [lr, #4]
	ldr	ip, [r4, #12]
	str	ip, [ip, #8]
	ldr	r2, [r4, #12]
	mov	r0, #36
	str	r2, [r2, #12]
	str	r5, [r4, #24]
	bl	malloc
	cmp	r0, #0
	beq	.L2506
.L2447:
	mov	r3, #0
	str	r0, [r4, #24]
	str	r3, [r4, #28]
	strb	r3, [r0, #0]
	ldr	r5, [r4, #24]
	str	r3, [r5, #4]
	ldr	r1, [r4, #24]
	str	r1, [r1, #8]
	ldr	r0, [r4, #24]
	add	r5, sp, #64
	str	r0, [r0, #12]
	str	r3, [r5, #-40]!
	mov	r1, r4
	mov	r0, r5
	mov	r2, #1
	bl	_ZN15QuStupidPointerI14QuImageManagerE3setEPS0_i
.LEHE223:
	mov	r0, r8
	add	r1, sp, #24
	ldmia	r1, {r1, r2}	@ phole ldm
.LEHB224:
	bl	_ZN15QuStupidPointerI14QuImageManagerE3setEPS0_i
.LEHE224:
	mov	r0, r5
	mov	r1, #0
	ldr	r2, [sp, #28]
.LEHB225:
	bl	_ZN15QuStupidPointerI14QuImageManagerE3setEPS0_i
	add	r0, r7, #20
	add	r1, sp, #40
	ldmia	r1, {r1, r2}	@ phole ldm
	bl	_ZN15QuStupidPointerI23QuSoundManagerInterfaceE3setEPS0_i
.LEHE225:
	mov	r4, #1
	strb	r4, [r7, #0]
	mov	r1, #0
	ldr	r2, [sp, #44]
	mov	r0, r6
.LEHB226:
	bl	_ZN15QuStupidPointerI23QuSoundManagerInterfaceE3setEPS0_i
	bl	_ZN8QuGlobal6getRefEv
	add	r4, sp, #64
	mov	r3, r0
	mov	r0, #0
	str	r0, [r4, #-32]!
	ldr	r2, [r3, #8]
	mov	r0, r4
	ldr	r1, [r3, #4]
	bl	_ZN15QuStupidPointerI13QuBaseManagerE3setEPS0_i
.LEHE226:
	ldr	ip, [sp, #32]
	mov	r0, ip
	ldr	r2, [ip, #0]
.LEHB227:
	ldr	ip, [r2, #8]
	mov	lr, pc
	bx	ip
.LEHE227:
	mov	r0, r4
	mov	r1, #0
	ldr	r2, [sp, #36]
.LEHB228:
	bl	_ZN15QuStupidPointerI13QuBaseManagerE3setEPS0_i
	bl	_Z7runGamev
	bl	_Z8mainTermv
.LEHE228:
	mov	r0, #0
	add	sp, sp, #68
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L2402:
	mov	r3, r0
	b	.L2407
.L2506:
	mov	r0, #36
.LEHB229:
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
.LEHE229:
	b	.L2447
.L2503:
	mov	r0, #36
.LEHB230:
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
.LEHE230:
	b	.L2422
.L2504:
	mov	r0, #36
.LEHB231:
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
.LEHE231:
	b	.L2433
.L2505:
	mov	r0, #36
.LEHB232:
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
.LEHE232:
	b	.L2440
.L2502:
	add	r0, sp, #56
.LEHB233:
	bl	T.1801
.LEHE233:
	mov	r4, #0
	str	r4, [r0, #0]
	ldr	r3, [r5, #0]
	ldr	r2, [r3, #4]
	cmp	r2, #0
	beq	.L2409
	ldr	ip, [sp, #56]
	mov	r0, r3
	b	.L2414
.L2507:
	mov	r0, r2
	mov	r2, r1
.L2414:
	ldr	r1, [r2, #16]
	cmp	r1, ip
	ldrcc	r1, [r2, #12]
	ldrcs	r1, [r2, #8]
	movcc	r2, r0
	cmp	r1, #0
	bne	.L2507
	cmp	r3, r2
	beq	.L2409
	ldr	r8, [r2, #16]
	cmp	r8, ip
	movls	r3, r2
	b	.L2409
.L2468:
.L2449:
.L2469:
.L2452:
.L2474:
.L2454:
	mov	r7, r0
	add	r0, r4, #12
	bl	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev
.L2475:
.L2455:
	mov	r0, r4
	bl	_ZN4_STL3mapINS_12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEE15QuStupidPointerI11QuBaseImageENS_4lessIS6_EENS4_INS_4pairIKS6_S9_EEEEED1Ev
.L2477:
.L2457:
	mov	r0, r4
	bl	_ZdlPv
.L2462:
	mov	r0, r6
	mov	r1, #0
	ldr	r2, [sp, #44]
	bl	_ZN15QuStupidPointerI23QuSoundManagerInterfaceE3setEPS0_i
	mov	r0, r7
.LEHB234:
	bl	__cxa_end_cleanup
.LEHE234:
.L2490:
	mov	r7, r0
	b	.L2462
.L2492:
.L2418:
	mov	r9, r0
	mov	r0, r4
	bl	_ZN15QuStupidPointerI13QuBaseManagerED1Ev
	mov	r0, r9
.LEHB235:
	bl	__cxa_end_cleanup
.LEHE235:
.L2485:
	mov	r6, r0
.L2397:
	mov	r0, sl
	bl	_ZN17QuImageDrawObjectD1Ev
.L2398:
	mov	r0, r9
	mov	r1, #0
	ldr	r2, [r4, #24]
	bl	_ZN15QuStupidPointerI22QuSoundStructInterfaceE3setEPS0_i
.L2400:
.L2488:
.L2401:
.L2493:
.L2416:
	ldr	r5, .L2508+32
	mov	r0, r4
	stmia	r4, {r5, fp}	@ phole stm
	bl	_ZdlPv
	mov	r0, r6
.LEHB236:
	bl	__cxa_end_cleanup
.LEHE236:
.L2486:
	mov	r6, r0
	b	.L2398
.L2487:
	mov	r6, r0
	b	.L2400
.L2489:
.L2465:
	mov	r7, r0
	mov	r0, r4
	bl	_ZN15QuStupidPointerI13QuBaseManagerED1Ev
	mov	r0, r7
.LEHB237:
	bl	__cxa_end_cleanup
.LEHE237:
.L2482:
	mov	r6, r0
.L2392:
.L2483:
.L2394:
	ldr	r3, .L2508+36
	str	r3, [r4, #224]
.L2395:
	str	r8, [r4, #168]
	str	r3, [r4, #188]
	b	.L2397
.L2484:
	ldr	r3, .L2508+36
	mov	r6, r0
	b	.L2395
.L2481:
.L2391:
	mov	r6, r0
	mov	r0, r5
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	b	.L2392
.L2476:
.L2459:
	mov	r7, r0
	mov	r1, #0
	mov	r0, r5
	ldr	r2, [sp, #28]
	bl	_ZN15QuStupidPointerI14QuImageManagerE3setEPS0_i
	b	.L2462
.L2470:
.L2442:
.L2471:
.L2445:
	mov	r7, r0
	b	.L2455
.L2472:
.L2435:
.L2473:
.L2438:
	mov	r7, r0
	b	.L2457
.L2478:
.L2424:
.L2479:
.L2427:
.L2480:
.L2429:
.L2491:
.L2431:
	ldr	fp, .L2508+40
	mov	sl, r0
	str	fp, [r4, #0]
	mov	r0, r4
	bl	_ZdlPv
	mov	r0, sl
.LEHB238:
	bl	__cxa_end_cleanup
.LEHE238:
.L2509:
	.align	2
.L2508:
	.word	_ZTV7QuTimer+8
	.word	_ZTV18InstructionManager+8
	.word	_ZTV23QuAbsScreenIntBoxButton+8
	.word	_ZTV17QuImageDrawObject+8
	.word	.LC5
	.word	.LC6
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.word	_ZTV19QuApRawSoundManager+8
	.word	_ZTV13QuBaseManager+8
	.word	_ZTV13QuBasicCamera+8
	.word	_ZTV23QuSoundManagerInterface+8
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA5026:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE5026-.LLSDACSB5026
.LLSDACSB5026:
	.uleb128 .LEHB214-.LFB5026
	.uleb128 .LEHE214-.LEHB214
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB215-.LFB5026
	.uleb128 .LEHE215-.LEHB215
	.uleb128 .L2487-.LFB5026
	.uleb128 0x0
	.uleb128 .LEHB216-.LFB5026
	.uleb128 .LEHE216-.LEHB216
	.uleb128 .L2486-.LFB5026
	.uleb128 0x0
	.uleb128 .LEHB217-.LFB5026
	.uleb128 .LEHE217-.LEHB217
	.uleb128 .L2485-.LFB5026
	.uleb128 0x0
	.uleb128 .LEHB218-.LFB5026
	.uleb128 .LEHE218-.LEHB218
	.uleb128 .L2484-.LFB5026
	.uleb128 0x0
	.uleb128 .LEHB219-.LFB5026
	.uleb128 .LEHE219-.LEHB219
	.uleb128 .L2482-.LFB5026
	.uleb128 0x0
	.uleb128 .LEHB220-.LFB5026
	.uleb128 .LEHE220-.LEHB220
	.uleb128 .L2481-.LFB5026
	.uleb128 0x0
	.uleb128 .LEHB221-.LFB5026
	.uleb128 .LEHE221-.LEHB221
	.uleb128 .L2492-.LFB5026
	.uleb128 0x0
	.uleb128 .LEHB222-.LFB5026
	.uleb128 .LEHE222-.LEHB222
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB223-.LFB5026
	.uleb128 .LEHE223-.LEHB223
	.uleb128 .L2490-.LFB5026
	.uleb128 0x0
	.uleb128 .LEHB224-.LFB5026
	.uleb128 .LEHE224-.LEHB224
	.uleb128 .L2476-.LFB5026
	.uleb128 0x0
	.uleb128 .LEHB225-.LFB5026
	.uleb128 .LEHE225-.LEHB225
	.uleb128 .L2490-.LFB5026
	.uleb128 0x0
	.uleb128 .LEHB226-.LFB5026
	.uleb128 .LEHE226-.LEHB226
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB227-.LFB5026
	.uleb128 .LEHE227-.LEHB227
	.uleb128 .L2489-.LFB5026
	.uleb128 0x0
	.uleb128 .LEHB228-.LFB5026
	.uleb128 .LEHE228-.LEHB228
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB229-.LFB5026
	.uleb128 .LEHE229-.LEHB229
	.uleb128 .L2468-.LFB5026
	.uleb128 0x0
	.uleb128 .LEHB230-.LFB5026
	.uleb128 .LEHE230-.LEHB230
	.uleb128 .L2478-.LFB5026
	.uleb128 0x0
	.uleb128 .LEHB231-.LFB5026
	.uleb128 .LEHE231-.LEHB231
	.uleb128 .L2472-.LFB5026
	.uleb128 0x0
	.uleb128 .LEHB232-.LFB5026
	.uleb128 .LEHE232-.LEHB232
	.uleb128 .L2470-.LFB5026
	.uleb128 0x0
	.uleb128 .LEHB233-.LFB5026
	.uleb128 .LEHE233-.LEHB233
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB234-.LFB5026
	.uleb128 .LEHE234-.LEHB234
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB235-.LFB5026
	.uleb128 .LEHE235-.LEHB235
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB236-.LFB5026
	.uleb128 .LEHE236-.LEHB236
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB237-.LFB5026
	.uleb128 .LEHE237-.LEHB237
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB238-.LFB5026
	.uleb128 .LEHE238-.LEHB238
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE5026:
	.fnend
	.size	_Z15INITIALIZE_GAMEI18InstructionManager19QuApRawSoundManagerEiv, .-_Z15INITIALIZE_GAMEI18InstructionManager19QuApRawSoundManagerEiv
	.section	.text.main,"ax",%progbits
	.align	2
	.global	main
	.hidden	main
	.type	main, %function
main:
	.fnstart
.LFB4575:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, lr}
	.save {r3, lr}
	bl	_Z15INITIALIZE_GAMEI18InstructionManager19QuApRawSoundManagerEiv
	mov	r0, #0
	ldmfd	sp!, {r3, lr}
	bx	lr
	.fnend
	.size	main, .-main
	.section	.text._ZN18InstructionManagerD1Ev,"axG",%progbits,_ZN18InstructionManagerD1Ev,comdat
	.align	2
	.weak	_ZN18InstructionManagerD1Ev
	.hidden	_ZN18InstructionManagerD1Ev
	.type	_ZN18InstructionManagerD1Ev, %function
_ZN18InstructionManagerD1Ev:
	.fnstart
.LFB6233:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	.save {r4, r5, r6, r7, r8, lr}
	mov	r4, r0
	ldr	r0, [r0, #260]
	ldr	r3, .L2551
	cmp	r0, #0
	.pad #16
	sub	sp, sp, #16
	str	r3, [r4, #0]
.LEHB239:
	blne	s3eFileClose
.LEHE239:
.L2513:
	ldr	r1, .L2551+4
	ldr	lr, .L2551+8
	ldr	r2, .L2551+12
	mov	r0, r4
	mov	ip, #0
	str	lr, [r0, #28]!
	str	ip, [r4, #260]
	str	r2, [r4, #168]
	str	r1, [r4, #188]
	str	r1, [r4, #224]
.LEHB240:
	bl	_ZN12QuDrawObjectD2Ev
.LEHE240:
	add	r0, r4, #20
	mov	r1, #0
	ldr	r2, [r4, #24]
.LEHB241:
	bl	_ZN15QuStupidPointerI22QuSoundStructInterfaceE3setEPS0_i
.LEHE241:
	ldr	lr, .L2551+16
	ldr	r0, .L2551+20
	stmia	r4, {r0, lr}	@ phole stm
	mov	r0, r4
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L2540:
.L2516:
.L2541:
.L2517:
.L2542:
.L2520:
	ldr	r6, .L2551+4
	ldr	r5, .L2551+12
	str	r6, [r4, #188]
	str	r5, [r4, #168]
	str	r6, [r4, #224]
	mov	r5, r0
	add	r0, r4, #28
	bl	_ZN17QuImageDrawObjectD1Ev
.L2521:
	ldr	lr, [r4, #20]
	cmp	lr, #0
	add	r7, r4, #20
	ldr	r6, [r4, #24]
	beq	.L2522
	ldr	r0, .L2551+24
	ldr	r2, [r0, #0]
	ldr	r3, [r2, #4]
	mov	ip, r2
.L2523:
	cmp	r3, #0
	beq	.L2548
	ldr	r8, [r3, #16]
	cmp	lr, r8
	ldrhi	r8, [r3, #12]
	ldrls	r8, [r3, #8]
	movhi	r3, ip
	mov	ip, r3
	mov	r3, r8
	b	.L2523
.L2544:
	mov	r5, r0
.L2537:
.L2545:
.L2538:
	ldr	r7, .L2551+20
	ldr	r6, .L2551+16
	str	r7, [r4, #0]
	str	r6, [r4, #4]
	mov	r0, r5
.LEHB242:
	bl	__cxa_end_cleanup
.LEHE242:
.L2543:
	mov	r5, r0
	b	.L2521
.L2548:
	cmp	ip, r2
	beq	.L2529
	ldr	r3, [ip, #16]
	cmp	lr, r3
	mov	r3, ip
	bcs	.L2530
.L2529:
	mov	r3, sp
	str	ip, [sp, #12]
	add	r0, sp, #8
	mov	ip, #0
	ldr	r1, .L2551+24
	add	r2, sp, #12
	str	lr, [sp, #0]
	str	ip, [sp, #4]
	bl	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS4_NS_16_Nonconst_traitsIS4_EEEERKS4_
	ldr	r3, [sp, #8]
.L2530:
	ldr	r2, [r3, #20]
	sub	r1, r2, #1
	cmp	r1, #0
	str	r1, [r3, #20]
	beq	.L2549
.L2522:
	mov	r2, #0
	str	r2, [r4, #20]
	str	r6, [r4, #24]
	b	.L2537
.L2549:
	ldr	r0, [r4, #20]
	bl	_ZN21QuStupidPointerHelper6removeEPv
	ldr	r0, [r4, #24]
	cmp	r0, #1
	ldr	r3, [r4, #20]
	beq	.L2550
	cmp	r3, #0
	beq	.L2522
	ldr	lr, [r3, #-4]
	add	r8, r3, lr, asl #5
.L2535:
	ldr	ip, [r7, #0]
	cmp	ip, r8
	sub	r8, r8, #32
	beq	.L2534
	ldr	r1, [r8, #0]
	mov	r0, r8
	ldr	ip, [r1, #0]
	mov	lr, pc
	bx	ip
	b	.L2535
.L2550:
	cmp	r3, #0
	beq	.L2522
	mov	r0, r3
	ldr	r7, [r3, #0]
	ldr	ip, [r7, #4]
	mov	lr, pc
	bx	ip
	b	.L2522
.L2534:
	ldr	r8, [r4, #20]
	sub	r0, r8, #8
	bl	_ZdaPv
	b	.L2522
.L2552:
	.align	2
.L2551:
	.word	_ZTV18InstructionManager+8
	.word	_ZTV13QuBasicCamera+8
	.word	_ZTV17QuImageDrawObject+8
	.word	_ZTV23QuAbsScreenIntBoxButton+8
	.word	_ZTV7QuTimer+8
	.word	_ZTV13QuBaseManager+8
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA6233:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE6233-.LLSDACSB6233
.LLSDACSB6233:
	.uleb128 .LEHB239-.LFB6233
	.uleb128 .LEHE239-.LEHB239
	.uleb128 .L2540-.LFB6233
	.uleb128 0x0
	.uleb128 .LEHB240-.LFB6233
	.uleb128 .LEHE240-.LEHB240
	.uleb128 .L2543-.LFB6233
	.uleb128 0x0
	.uleb128 .LEHB241-.LFB6233
	.uleb128 .LEHE241-.LEHB241
	.uleb128 .L2544-.LFB6233
	.uleb128 0x0
	.uleb128 .LEHB242-.LFB6233
	.uleb128 .LEHE242-.LEHB242
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE6233:
	.fnend
	.size	_ZN18InstructionManagerD1Ev, .-_ZN18InstructionManagerD1Ev
	.section	.text._ZN18InstructionManager10initializeEv,"axG",%progbits,_ZN18InstructionManager10initializeEv,comdat
	.align	2
	.weak	_ZN18InstructionManager10initializeEv
	.hidden	_ZN18InstructionManager10initializeEv
	.type	_ZN18InstructionManager10initializeEv, %function
_ZN18InstructionManager10initializeEv:
	.fnstart
.LFB3921:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 304
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.pad #324
	sub	sp, sp, #324
	mov	r4, r0
.LEHB243:
	bl	_ZN8QuGlobal6getRefEv
.LEHE243:
	add	r7, sp, #236
	ldr	r8, [r0, #12]
	ldr	r1, .L2711
	mov	r0, r7
	add	r2, sp, #316
.LEHB244:
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1EPKcRKS4_
.LEHE244:
	add	r6, sp, #212
	mov	r0, r6
	mov	r1, r7
.LEHB245:
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
.LEHE245:
	add	r5, sp, #288
	mov	r1, r8
	mov	r0, r5
	mov	r2, r6
.LEHB246:
	bl	_ZN14QuImageManager11setFlyImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
.LEHE246:
	ldr	r0, [sp, #212]
	cmp	r0, #0
	blne	free
.L2557:
	mov	r0, r5
.LEHB247:
	bl	_ZN10QuPngImage18finishLoadingImageEv
.LEHE247:
	add	r0, r4, #28
	mov	r1, r5
.LEHB248:
	bl	T.1795
.LEHE248:
	mov	r0, r5
	mov	r1, #0
	ldr	r2, [sp, #292]
.LEHB249:
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
.LEHE249:
	ldr	r0, [sp, #236]
	cmp	r0, #0
	blne	free
.L2568:
.L2566:
.LEHB250:
	bl	_ZN8QuGlobal6getRefEv
	mov	r6, r0
	ldr	r0, [r0, #28]
	bl	__aeabi_i2f
	mov	r1, #1056964608
	bl	__aeabi_fmul
	bl	__aeabi_f2iz
	mov	r5, r0
	ldr	r0, [r6, #32]
	bl	__aeabi_i2f
	ldr	r1, .L2711+4
	mov	r8, r0
	bl	__aeabi_fmul
	bl	__aeabi_f2iz
	add	sl, r5, r5, lsr #31
	sub	r9, r5, sl, asr #1
	add	r1, r0, r0, lsr #31
	mov	r2, r1, asr #1
	mov	r6, r0
	ldr	r1, .L2711+8
	str	r9, [r4, #172]
	mov	r0, r8
	rsb	r7, r2, #0
	bl	__aeabi_fmul
	bl	__aeabi_f2iz
	add	r0, r7, r0
	str	r5, [r4, #180]
	str	r6, [r4, #184]
	str	r0, [r4, #176]
	bl	_ZN8QuGlobal6getRefEv
.LEHE250:
	ldr	r5, [r0, #20]
	add	sl, sp, #224
	ldr	r3, [r5, #0]
	mov	r0, sl
	ldr	r1, .L2711+12
	add	r2, sp, #312
	ldr	r6, [r3, #12]
	add	r7, r4, #20
.LEHB251:
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1EPKcRKS4_
.LEHE251:
	add	r8, sp, #280
	mov	r1, r5
	mov	r0, r8
	mov	r2, sl
.LEHB252:
	mov	lr, pc
	bx	r6
.LEHE252:
	ldr	lr, [r4, #20]
	cmp	lr, #0
	add	r5, sp, #280
	ldmia	r5, {r5, r9}	@ phole ldm
	beq	.L2571
	ldr	ip, .L2711+16
	ldr	r1, [ip, #0]
	ldr	ip, [r1, #4]
	cmp	ip, #0
	moveq	ip, r1
	beq	.L2575
	mov	r2, r1
	b	.L2576
.L2698:
	mov	r2, ip
	mov	ip, r3
.L2576:
	ldr	r3, [ip, #16]
	cmp	lr, r3
	ldrhi	r3, [ip, #12]
	ldrls	r3, [ip, #8]
	movhi	ip, r2
	cmp	r3, #0
	bne	.L2698
.L2575:
	cmp	ip, r1
	beq	.L2578
	ldr	r3, [ip, #16]
	cmp	lr, r3
	mov	r3, ip
	bcs	.L2579
.L2578:
	str	lr, [sp, #272]
	add	r0, sp, #304
	mov	lr, #0
	ldr	r1, .L2711+16
	add	r2, sp, #308
	add	r3, sp, #272
	str	lr, [sp, #276]
	str	ip, [sp, #308]
.LEHB253:
	bl	_ZN4_STL8_Rb_treeIPvNS_4pairIKS1_iEENS_10_Select1stIS4_EENS_4lessIS1_EENS_9allocatorIS4_EEE13insert_uniqueENS_17_Rb_tree_iteratorIS4_NS_16_Nonconst_traitsIS4_EEEERKS4_
.LEHE253:
	ldr	r3, [sp, #304]
.L2579:
	ldr	r2, [r3, #20]
	sub	r0, r2, #1
	cmp	r0, #0
	str	r0, [r3, #20]
	beq	.L2699
.L2571:
	cmp	r5, #0
	str	r9, [r4, #24]
	str	r5, [r4, #20]
	beq	.L2585
	ldr	r6, .L2711+16
	str	r5, [sp, #300]
	ldr	r0, [r6, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	beq	.L2586
	mov	r1, r0
	b	.L2590
.L2700:
	mov	r1, r3
	mov	r3, r2
.L2590:
	ldr	r2, [r3, #16]
	cmp	r5, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L2700
	cmp	r0, r3
	beq	.L2586
	ldr	r9, [r3, #16]
	cmp	r5, r9
	bcc	.L2586
.L2591:
	cmp	r3, r0
	beq	.L2701
.L2593:
	ldr	r1, [r3, #20]
	add	r2, r1, #1
	str	r2, [r3, #20]
.L2585:
	mov	r0, r8
	mov	r1, #0
	ldr	r2, [sp, #284]
.LEHB254:
	bl	_ZN15QuStupidPointerI22QuSoundStructInterfaceE3setEPS0_i
.LEHE254:
	ldr	r0, [sp, #224]
	cmp	r0, #0
	blne	free
.L2601:
	add	r7, sp, #20
	ldr	r5, .L2711+20
	add	r0, r7, #72
.LEHB255:
	bl	_ZN4_STL8ios_baseC2Ev
.LEHE255:
	ldr	r9, .L2711+24
	ldr	r3, [r5, #8]
	mov	r1, #0
	str	r3, [sp, #20]
	str	r9, [sp, #92]
	strb	r1, [sp, #176]
	str	r1, [sp, #180]
	str	r1, [sp, #184]
	ldr	r6, [r5, #12]
	ldr	ip, [r3, #-12]
	str	r6, [r7, ip]
	ldr	sl, [sp, #20]
	str	r1, [sp, #24]
	ldr	r8, [sl, #-12]
	add	r0, r7, r8
.LEHB256:
	bl	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEE4initEPNS_15basic_streambufIcS2_EE
.LEHE256:
	ldr	r8, [r5, #16]
	str	r8, [sp, #28]
	ldr	r2, [r5, #20]
	ldr	r1, [r8, #-12]
	add	r8, sp, #28
	str	r2, [r8, r1]
	ldr	r0, [sp, #28]
	ldr	lr, [r0, #-12]
	mov	r1, #0
	add	r0, r8, lr
.LEHB257:
	bl	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEE4initEPNS_15basic_streambufIcS2_EE
.LEHE257:
.L2611:
	ldr	r2, [r5, #4]
	add	r6, sp, #320
	str	r2, [r6, #-300]!
	ldr	lr, [r5, #24]
	ldr	r0, [r2, #-12]
	ldr	r3, [r5, #28]
	str	lr, [r6, r0]
	ldr	ip, [sp, #20]
	str	r3, [sp, #28]
	ldr	sl, [ip, #-12]
	mov	r1, #0
	add	r0, r6, sl
.LEHB258:
	bl	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEE4initEPNS_15basic_streambufIcS2_EE
.LEHE258:
	ldr	lr, .L2711+28
	ldr	sl, .L2711+32
	mov	r3, #0
	add	r2, lr, #20
	add	ip, lr, #40
	add	r0, sp, #60
	str	ip, [sp, #92]
	str	r2, [sp, #28]
	str	r3, [sp, #56]
	str	lr, [sp, #20]
	str	sl, [sp, #32]
	str	r3, [sp, #36]
	str	r3, [sp, #40]
	str	r3, [sp, #44]
	str	r3, [sp, #48]
	str	r3, [sp, #52]
.LEHB259:
	bl	_ZN4_STL6localeC1Ev
.LEHE259:
	ldr	lr, .L2711+36
	mov	r1, #0
	mov	r3, #24
	mov	r0, #8
	str	lr, [sp, #32]
	str	r3, [sp, #68]
	str	r1, [sp, #80]
	str	r1, [sp, #72]
	str	r1, [sp, #76]
	bl	malloc
	cmp	r0, #0
	beq	.L2702
.L2618:
	add	r1, r0, #8
	add	ip, sp, #20
	mov	r2, #0
	str	r1, [sp, #80]
	str	r0, [sp, #72]
	str	r0, [sp, #76]
	add	r1, ip, #12
	strb	r2, [r0, #0]
	add	r0, ip, #72
.LEHB260:
	bl	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEE4initEPNS_15basic_streambufIcS2_EE
.LEHE260:
	add	r5, sp, #28
	ldr	r3, [sp, #28]
	str	r5, [sp, #264]
	ldr	r2, [r3, #-12]
	add	r6, r5, r2
	ldr	r0, [r6, #8]
	cmp	r0, #0
	sub	r1, r3, #12
	movne	r3, #0
	bne	.L2629
	add	r0, r8, r2
	ldr	lr, [r0, #88]
	cmp	lr, #0
	beq	.L2703
.L2630:
	ldr	r6, [r0, #92]
	cmp	r6, #0
	beq	.L2631
	ldr	lr, [r6, #0]
	ldr	r0, [lr, #-12]
	add	ip, r6, r0
	ldr	r3, [ip, #88]
	cmp	r3, #0
	beq	.L2631
	mov	r0, r3
	ldr	r2, [r3, #0]
.LEHB261:
	ldr	ip, [r2, #20]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	beq	.L2632
.L2695:
	ldr	lr, [sp, #28]
	ldr	r2, [lr, #-12]
	sub	r1, lr, #12
.L2631:
	add	r2, r5, r2
	ldr	r6, [r2, #8]
	rsbs	r3, r6, #1
	movcc	r3, #0
.L2629:
	cmp	r3, #0
	strb	r3, [sp, #268]
	bne	.L2704
.L2636:
	ldr	r0, [r1, #0]
	add	r0, r8, r0
	ldr	ip, [r0, #8]
	ldr	r1, [r0, #20]
	orr	lr, ip, #1
	tst	lr, r1
	str	lr, [r0, #8]
	bne	.L2705
.L2640:
	ldr	r5, [sp, #264]
	ldr	r6, [r5, #0]
	ldr	r3, [r6, #-12]
	add	r3, r5, r3
	ldr	r2, [r3, #4]
	tst	r2, #8192
	bne	.L2706
.L2642:
	add	r0, sp, #20
	ldr	r3, [sp, #48]
	add	r1, r0, #64
	cmp	r1, r3
	ldrne	r1, [sp, #52]
	beq	.L2707
.L2648:
	ldr	ip, [sp, #56]
	cmp	ip, r1
	beq	.L2659
.L2649:
	add	r5, sp, #200
	mov	r0, r5
	add	r1, sp, #72
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
.LEHE261:
	add	r6, sp, #188
	mov	r0, r6
	mov	r1, r5
.LEHB262:
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
.LEHE262:
	ldr	r0, [r4, #260]
	ldr	r1, [sp, #188]
.LEHB263:
	bl	s3eFilePrintf
.LEHE263:
	ldr	r0, [sp, #188]
	cmp	r0, #0
	blne	free
.L2652:
	ldr	r0, [sp, #200]
	cmp	r0, #0
	blne	free
.L2654:
	add	r0, sp, #20
.LEHB264:
	bl	_ZN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	ldr	r0, [r4, #260]
	cmp	r0, #0
	blne	s3eFileClose
.LEHE264:
.L2657:
	mov	lr, #0
	str	lr, [r4, #260]
	add	sp, sp, #324
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L2586:
	mov	r3, r0
	b	.L2591
.L2705:
.LEHB265:
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
.LEHE265:
	ldr	r5, [sp, #264]
	ldr	r6, [r5, #0]
	ldr	r3, [r6, #-12]
	add	r3, r5, r3
	ldr	r2, [r3, #4]
	tst	r2, #8192
	beq	.L2642
.L2706:
	ldr	r3, [r3, #88]
	cmp	r3, #0
	beq	.L2642
	mov	r0, r3
	ldr	r1, [r3, #0]
.LEHB266:
	ldr	ip, [r1, #20]
	mov	lr, pc
	bx	ip
	cmn	r0, #1
	bne	.L2642
	ldr	r2, [r5, #0]
	ldr	r0, [r2, #-12]
	add	r0, r5, r0
	ldr	ip, [r0, #8]
	ldr	lr, [r0, #20]
	orr	r5, ip, #1
	tst	r5, lr
	str	r5, [r0, #8]
	beq	.L2642
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
.LEHE266:
	b	.L2642
.L2699:
	ldr	r0, [r4, #20]
.LEHB267:
	bl	_ZN21QuStupidPointerHelper6removeEPv
	ldr	r1, [r4, #24]
	cmp	r1, #1
	ldr	r3, [r4, #20]
	beq	.L2708
	cmp	r3, #0
	beq	.L2571
	ldr	r6, [r3, #-4]
	add	r6, r3, r6, asl #5
	b	.L2584
.L2709:
	ldr	r3, [r6, #-32]!
	mov	r0, r6
	ldr	ip, [r3, #0]
	mov	lr, pc
	bx	ip
.LEHE267:
.L2584:
	ldr	ip, [r7, #0]
	cmp	ip, r6
	bne	.L2709
	ldr	lr, [r4, #20]
	sub	r0, lr, #8
	bl	_ZdaPv
	b	.L2571
.L2707:
	ldr	r2, [sp, #52]
	cmp	r1, r2
	beq	.L2648
	add	r0, r0, #52
.LEHB268:
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEE6appendIPKcEERS5_T_SA_
.LEHE268:
.L2659:
	add	r5, sp, #20
	add	r6, r5, #64
	add	r1, r5, #72
	str	r6, [sp, #52]
	str	r1, [sp, #56]
	str	r6, [sp, #48]
	b	.L2649
.L2704:
	ldr	fp, [r1, #0]
	add	r6, sp, #296
	add	sl, r5, fp
	add	r1, sl, #32
	mov	r0, r6
.LEHB269:
	bl	_ZN4_STL6localeC1ERKS0_
.LEHE269:
	mov	r0, r6
	ldr	r1, .L2711+40
.LEHB270:
	bl	_ZNK4_STL6locale12_M_use_facetERKNS0_2idE
	ldr	r3, [sp, #28]
	ldr	fp, [r3, #-12]
	add	r2, r8, fp
	ldr	r1, [r2, #88]
	subs	ip, r1, #0
	movne	ip, #1
	ldrb	lr, [r2, #84]	@ zero_extendqisi2
	str	r1, [sp, #256]
	strb	ip, [sp, #260]
	mov	r1, r0
	ldr	ip, [r0, #0]
	mvn	r0, #-1073741824
	add	sl, r5, fp
	sub	r5, r0, #802816
	str	sl, [sp, #0]
	sub	fp, r5, #147
	mov	sl, #536870912
	str	sl, [sp, #8]
	str	fp, [sp, #12]
	str	lr, [sp, #4]
	add	r3, sp, #256
	add	r0, sp, #248
	ldmia	r3, {r2, r3}
	ldr	ip, [ip, #20]
	mov	lr, pc
	bx	ip
.LEHE270:
	ldrb	r5, [sp, #252]	@ zero_extendqisi2
	mov	r0, r6
	eor	r5, r5, #1
.LEHB271:
	bl	_ZN4_STL6localeD1Ev
.LEHE271:
	cmp	r5, #0
	beq	.L2640
	ldr	r6, [sp, #28]
	sub	r1, r6, #12
	b	.L2636
.L2708:
	cmp	r3, #0
	beq	.L2571
	mov	r0, r3
	ldr	r7, [r3, #0]
.LEHB272:
	ldr	ip, [r7, #4]
	mov	lr, pc
	bx	ip
.LEHE272:
	b	.L2571
.L2702:
	mov	r0, #8
.LEHB273:
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
.LEHE273:
	b	.L2618
.L2701:
	add	r0, sp, #300
.LEHB274:
	bl	T.1801
.LEHE274:
	mov	r5, #0
	str	r5, [r0, #0]
	ldr	r3, [r6, #0]
	ldr	r2, [r3, #4]
	cmp	r2, #0
	beq	.L2593
	ldr	ip, [sp, #300]
	mov	r0, r3
	b	.L2598
.L2710:
	mov	r0, r2
	mov	r2, r1
.L2598:
	ldr	r1, [r2, #16]
	cmp	r1, ip
	ldrcc	r1, [r2, #12]
	ldrcs	r1, [r2, #8]
	movcc	r2, r0
	cmp	r1, #0
	bne	.L2710
	cmp	r3, r2
	beq	.L2593
	ldr	r0, [r2, #16]
	cmp	r0, ip
	movls	r3, r2
	b	.L2593
.L2632:
	ldr	r0, [r6, #0]
	ldr	ip, [r0, #-12]
	add	r0, r6, ip
	ldr	r3, [r0, #8]
	ldr	r6, [r0, #20]
	orr	r1, r3, #1
	tst	r1, r6
	str	r1, [r0, #8]
	beq	.L2695
.LEHB275:
	bl	_ZN4_STL8ios_base16_M_throw_failureEv
	b	.L2695
.L2703:
	mov	r1, #1
	bl	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEE8setstateEi
.LEHE275:
	ldr	r3, [sp, #28]
	ldr	r2, [r3, #-12]
	sub	r1, r3, #12
	add	r0, r8, r2
	b	.L2630
.L2670:
.L2625:
	mov	r4, r0
	add	r0, sp, #32
	bl	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
.L2626:
	ldr	r1, [r5, #4]
	ldr	r3, [r5, #16]
	ldr	r2, [r1, #-12]
	ldr	ip, [r5, #24]
	str	ip, [r6, r2]
	str	r3, [sp, #28]
	ldr	fp, [r5, #8]
	ldr	lr, [r3, #-12]
	ldr	r6, [r5, #20]
	str	r6, [r8, lr]
	str	fp, [sp, #20]
	ldr	sl, [r5, #12]
	ldr	r0, [fp, #-12]
	str	sl, [r7, r0]
.L2672:
.L2627:
	add	r0, sp, #92
	str	r9, [sp, #92]
	bl	_ZN4_STL8ios_baseD2Ev
	mov	r0, r4
.LEHB276:
	bl	__cxa_end_cleanup
.LEHE276:
.L2671:
	mov	r4, r0
	b	.L2626
.L2666:
.L2613:
	ldr	r6, [r5, #16]
	str	r6, [sp, #28]
	ldr	r4, [r5, #20]
	ldr	fp, [r6, #-12]
	str	r4, [r8, fp]
.L2694:
.L2667:
.L2614:
.L2668:
.L2615:
	ldr	r8, [r5, #8]
	str	r8, [sp, #20]
	ldr	r1, [r5, #12]
	ldr	r5, [r8, #-12]
	mov	r4, r0
	str	r1, [r7, r5]
	b	.L2627
.L2664:
	b	.L2694
.L2663:
.L2620:
	ldr	r3, [sp, #72]
	cmp	r3, #0
	mov	r4, r0
	movne	r0, r3
	blne	free
.L2621:
.L2669:
.L2622:
	add	r0, sp, #60
	str	sl, [sp, #32]
	bl	_ZN4_STL6localeD1Ev
	b	.L2626
.L2678:
.L2602:
	mov	r4, r0
	mov	r1, #0
	mov	r0, r8
	ldr	r2, [sp, #284]
	bl	_ZN15QuStupidPointerI22QuSoundStructInterfaceE3setEPS0_i
.L2604:
	mov	r0, sl
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
.L2696:
	mov	r0, r4
.LEHB277:
	bl	__cxa_end_cleanup
.LEHE277:
.L2673:
.L2651:
	mov	r4, r0
	mov	r0, r6
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
.L2655:
	mov	r0, r5
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
.L2658:
	add	r0, sp, #20
	bl	_ZN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	b	.L2696
.L2674:
	mov	r4, r0
	b	.L2655
.L2665:
.L2609:
	mov	r4, r0
	b	.L2627
.L2662:
	mov	r4, r0
.L2646:
	add	r0, sp, #264
	bl	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEE6sentryD1Ev
	b	.L2658
.L2682:
	mov	r4, r0
.L2567:
	mov	r0, r7
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	b	.L2696
.L2683:
.L2697:
.L2606:
	mov	r4, r0
	b	.L2696
.L2675:
	mov	r4, r0
	b	.L2658
.L2679:
	mov	r4, r0
	b	.L2604
.L2680:
	b	.L2697
.L2681:
.L2562:
	mov	r4, r0
	mov	r1, #0
	mov	r0, r5
	ldr	r2, [sp, #292]
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
	b	.L2567
.L2676:
.L2559:
	mov	r4, r0
	mov	r1, #0
	mov	r0, r5
	ldr	r2, [sp, #292]
	bl	_ZN15QuStupidPointerI11QuBaseImageE3setEPS0_i
	b	.L2567
.L2677:
.L2556:
	mov	r4, r0
	mov	r0, r6
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	b	.L2567
.L2661:
.L2639:
	mov	r4, r0
	mov	r0, r6
	bl	_ZN4_STL6localeD1Ev
	b	.L2646
.L2712:
	.align	2
.L2711:
	.word	.LC7
	.word	1050253722
	.word	1045220557
	.word	.LC8
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.word	_ZTTN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.word	_ZTVN4_STL9basic_iosIcNS_11char_traitsIcEEEE+8
	.word	_ZTVN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE+12
	.word	_ZTVN4_STL15basic_streambufIcNS_11char_traitsIcEEEE+8
	.word	_ZTVN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE+8
	.word	_ZN4_STL7num_putIcNS_19ostreambuf_iteratorIcNS_11char_traitsIcEEEEE2idE
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA3921:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE3921-.LLSDACSB3921
.LLSDACSB3921:
	.uleb128 .LEHB243-.LFB3921
	.uleb128 .LEHE243-.LEHB243
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB244-.LFB3921
	.uleb128 .LEHE244-.LEHB244
	.uleb128 .L2683-.LFB3921
	.uleb128 0x0
	.uleb128 .LEHB245-.LFB3921
	.uleb128 .LEHE245-.LEHB245
	.uleb128 .L2682-.LFB3921
	.uleb128 0x0
	.uleb128 .LEHB246-.LFB3921
	.uleb128 .LEHE246-.LEHB246
	.uleb128 .L2677-.LFB3921
	.uleb128 0x0
	.uleb128 .LEHB247-.LFB3921
	.uleb128 .LEHE247-.LEHB247
	.uleb128 .L2676-.LFB3921
	.uleb128 0x0
	.uleb128 .LEHB248-.LFB3921
	.uleb128 .LEHE248-.LEHB248
	.uleb128 .L2681-.LFB3921
	.uleb128 0x0
	.uleb128 .LEHB249-.LFB3921
	.uleb128 .LEHE249-.LEHB249
	.uleb128 .L2682-.LFB3921
	.uleb128 0x0
	.uleb128 .LEHB250-.LFB3921
	.uleb128 .LEHE250-.LEHB250
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB251-.LFB3921
	.uleb128 .LEHE251-.LEHB251
	.uleb128 .L2680-.LFB3921
	.uleb128 0x0
	.uleb128 .LEHB252-.LFB3921
	.uleb128 .LEHE252-.LEHB252
	.uleb128 .L2679-.LFB3921
	.uleb128 0x0
	.uleb128 .LEHB253-.LFB3921
	.uleb128 .LEHE253-.LEHB253
	.uleb128 .L2678-.LFB3921
	.uleb128 0x0
	.uleb128 .LEHB254-.LFB3921
	.uleb128 .LEHE254-.LEHB254
	.uleb128 .L2679-.LFB3921
	.uleb128 0x0
	.uleb128 .LEHB255-.LFB3921
	.uleb128 .LEHE255-.LEHB255
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB256-.LFB3921
	.uleb128 .LEHE256-.LEHB256
	.uleb128 .L2665-.LFB3921
	.uleb128 0x0
	.uleb128 .LEHB257-.LFB3921
	.uleb128 .LEHE257-.LEHB257
	.uleb128 .L2664-.LFB3921
	.uleb128 0x0
	.uleb128 .LEHB258-.LFB3921
	.uleb128 .LEHE258-.LEHB258
	.uleb128 .L2666-.LFB3921
	.uleb128 0x0
	.uleb128 .LEHB259-.LFB3921
	.uleb128 .LEHE259-.LEHB259
	.uleb128 .L2671-.LFB3921
	.uleb128 0x0
	.uleb128 .LEHB260-.LFB3921
	.uleb128 .LEHE260-.LEHB260
	.uleb128 .L2670-.LFB3921
	.uleb128 0x0
	.uleb128 .LEHB261-.LFB3921
	.uleb128 .LEHE261-.LEHB261
	.uleb128 .L2675-.LFB3921
	.uleb128 0x0
	.uleb128 .LEHB262-.LFB3921
	.uleb128 .LEHE262-.LEHB262
	.uleb128 .L2674-.LFB3921
	.uleb128 0x0
	.uleb128 .LEHB263-.LFB3921
	.uleb128 .LEHE263-.LEHB263
	.uleb128 .L2673-.LFB3921
	.uleb128 0x0
	.uleb128 .LEHB264-.LFB3921
	.uleb128 .LEHE264-.LEHB264
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB265-.LFB3921
	.uleb128 .LEHE265-.LEHB265
	.uleb128 .L2662-.LFB3921
	.uleb128 0x0
	.uleb128 .LEHB266-.LFB3921
	.uleb128 .LEHE266-.LEHB266
	.uleb128 .L2675-.LFB3921
	.uleb128 0x0
	.uleb128 .LEHB267-.LFB3921
	.uleb128 .LEHE267-.LEHB267
	.uleb128 .L2678-.LFB3921
	.uleb128 0x0
	.uleb128 .LEHB268-.LFB3921
	.uleb128 .LEHE268-.LEHB268
	.uleb128 .L2675-.LFB3921
	.uleb128 0x0
	.uleb128 .LEHB269-.LFB3921
	.uleb128 .LEHE269-.LEHB269
	.uleb128 .L2662-.LFB3921
	.uleb128 0x0
	.uleb128 .LEHB270-.LFB3921
	.uleb128 .LEHE270-.LEHB270
	.uleb128 .L2661-.LFB3921
	.uleb128 0x0
	.uleb128 .LEHB271-.LFB3921
	.uleb128 .LEHE271-.LEHB271
	.uleb128 .L2662-.LFB3921
	.uleb128 0x0
	.uleb128 .LEHB272-.LFB3921
	.uleb128 .LEHE272-.LEHB272
	.uleb128 .L2678-.LFB3921
	.uleb128 0x0
	.uleb128 .LEHB273-.LFB3921
	.uleb128 .LEHE273-.LEHB273
	.uleb128 .L2663-.LFB3921
	.uleb128 0x0
	.uleb128 .LEHB274-.LFB3921
	.uleb128 .LEHE274-.LEHB274
	.uleb128 .L2678-.LFB3921
	.uleb128 0x0
	.uleb128 .LEHB275-.LFB3921
	.uleb128 .LEHE275-.LEHB275
	.uleb128 .L2675-.LFB3921
	.uleb128 0x0
	.uleb128 .LEHB276-.LFB3921
	.uleb128 .LEHE276-.LEHB276
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB277-.LFB3921
	.uleb128 .LEHE277-.LEHB277
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE3921:
	.fnend
	.size	_ZN18InstructionManager10initializeEv, .-_ZN18InstructionManager10initializeEv
	.section	.text._Z11singleTouchP15s3ePointerEvent,"ax",%progbits
	.align	2
	.global	_Z11singleTouchP15s3ePointerEvent
	.hidden	_Z11singleTouchP15s3ePointerEvent
	.type	_Z11singleTouchP15s3ePointerEvent, %function
_Z11singleTouchP15s3ePointerEvent:
	.fnstart
.LFB4566:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, lr}
	.pad #48
	sub	sp, sp, #48
	mov	r5, r0
	add	r7, r0, #8
	ldmia	r7, {r7, r8}	@ phole ldm
.LEHB278:
	bl	_ZN8QuGlobal6getRefEv
	mov	r4, r0
	bl	_ZN8QuGlobal6getRefEv
	bl	_ZN8QuGlobal17getDeviceRotationEv
	cmp	r0, #4
	beq	.L2752
.L2715:
	cmp	r0, #0
	cmpne	r0, #2
	ldreq	r9, [r4, #32]
	ldreq	r6, [r4, #28]
	ldrne	r9, [r4, #28]
	ldrne	r6, [r4, #32]
	bl	_ZN8QuGlobal6getRefEv
	bl	_ZN8QuGlobal17getDeviceRotationEv
	ldr	r1, .L2756
	add	r3, r1, r0, asl #2
	ldr	r3, [r3, #48]
	cmp	r3, #2
	beq	.L2721
	cmp	r3, #3
	rsbeq	sl, r8, r9
	beq	.L2723
	cmp	r3, #1
	movne	sl, r7
	rsbeq	r7, r7, r6
	movne	r7, r8
	moveq	sl, r8
.L2723:
	cmp	r3, #1
	cmpne	r3, #3
	ldr	r0, .L2756+4
	movne	r8, r6
	movne	r6, r9
	moveq	r8, r9
	rsb	r7, r7, r6
	str	sl, [r0, #8]
	str	r6, [r0, #20]
	str	r8, [r0, #16]
	str	r7, [r0, #12]
	ldr	r9, [r5, #4]
	cmp	r9, #0
	beq	.L2726
	bl	_ZN8QuGlobal6getRefEv
.LEHE278:
	mov	r2, #0
	str	r2, [sp, #36]
	ldmib	r0, {r2, r4}	@ phole ldm
	cmp	r2, #0
	str	r4, [sp, #40]
	str	r2, [sp, #36]
	beq	.L2728
	ldr	r4, .L2756+8
	str	r2, [sp, #44]
	ldr	ip, [r4, #0]
	ldr	r3, [ip, #4]
	cmp	r3, #0
	beq	.L2729
	mov	r0, ip
	b	.L2733
.L2753:
	mov	r0, r3
	mov	r3, r1
.L2733:
	ldr	r1, [r3, #16]
	cmp	r2, r1
	ldrhi	r1, [r3, #12]
	ldrls	r1, [r3, #8]
	movhi	r3, r0
	cmp	r1, #0
	bne	.L2753
	cmp	ip, r3
	beq	.L2729
	ldr	lr, [r3, #16]
	cmp	r2, lr
	bcc	.L2729
.L2734:
	cmp	r3, ip
	beq	.L2754
.L2736:
	ldr	r4, [r3, #20]
	add	r0, r4, #1
	str	r0, [r3, #20]
	ldr	r2, [sp, #36]
.L2728:
	ldr	r1, [r2, #0]
	ldr	ip, [r1, #24]
	str	r6, [sp, #24]
	str	r8, [sp, #20]
	add	r4, sp, #20
	ldmia	r4, {r0, r1}
	str	r7, [sp, #16]
	str	sl, [sp, #12]
	mov	r4, sp
	ldr	lr, [r5, #0]
	stmia	r4, {r0, r1}
	add	r3, sp, #12
	mov	r0, r2
	mov	r1, lr
	ldmia	r3, {r2, r3}
.LEHB279:
	mov	lr, pc
	bx	ip
.LEHE279:
	add	r0, sp, #36
	mov	r1, #0
	ldr	r2, [sp, #40]
.LEHB280:
	bl	_ZN15QuStupidPointerI13QuBaseManagerE3setEPS0_i
.L2747:
	add	sp, sp, #48
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L2726:
	bl	_ZN8QuGlobal6getRefEv
	add	r4, sp, #48
	str	r9, [r4, #-20]!
	mov	ip, r0
	ldr	r2, [r0, #8]
	ldr	r1, [ip, #4]
	mov	r0, r4
	bl	_ZN15QuStupidPointerI13QuBaseManagerE3setEPS0_i
.LEHE280:
	ldr	r2, [sp, #28]
	ldr	r0, [r2, #0]
	ldr	ip, [r0, #28]
	str	r6, [sp, #24]
	str	r8, [sp, #20]
	str	r7, [sp, #16]
	str	sl, [sp, #12]
	add	r3, sp, #20
	ldmia	r3, {r0, r1}
	ldr	lr, [r5, #0]
	mov	r5, sp
	stmia	r5, {r0, r1}
	add	r3, sp, #12
	mov	r0, r2
	mov	r1, lr
	ldmia	r3, {r2, r3}
.LEHB281:
	mov	lr, pc
	bx	ip
.LEHE281:
	mov	r0, r4
	mov	r1, #0
	ldr	r2, [sp, #32]
.LEHB282:
	bl	_ZN15QuStupidPointerI13QuBaseManagerE3setEPS0_i
	b	.L2747
.L2729:
	mov	r3, ip
	b	.L2734
.L2721:
	rsb	sl, r7, r6
	rsb	r7, r8, r9
	b	.L2723
.L2752:
	bl	_ZN8QuGlobal6getRefEv
	bl	_ZN8QuGlobal17getDeviceRotationEv
	b	.L2715
.L2754:
	add	r0, sp, #44
	bl	T.1801
.LEHE282:
	mov	ip, #0
	str	ip, [r0, #0]
	ldr	r3, [r4, #0]
	ldr	r2, [r3, #4]
	cmp	r2, #0
	beq	.L2736
	ldr	ip, [sp, #44]
	mov	r0, r3
	b	.L2741
.L2755:
	mov	r0, r2
	mov	r2, r1
.L2741:
	ldr	r1, [r2, #16]
	cmp	r1, ip
	ldrcc	r1, [r2, #12]
	ldrcs	r1, [r2, #8]
	movcc	r2, r0
	cmp	r1, #0
	bne	.L2755
	cmp	r3, r2
	beq	.L2736
	ldr	r1, [r2, #16]
	cmp	r1, ip
	movls	r3, r2
	b	.L2736
.L2748:
.L2746:
	mov	r6, r0
	mov	r0, r4
	bl	_ZN15QuStupidPointerI13QuBaseManagerED1Ev
	mov	r0, r6
.LEHB283:
	bl	__cxa_end_cleanup
.LEHE283:
.L2749:
.L2743:
	mov	r5, r0
	add	r0, sp, #36
	bl	_ZN15QuStupidPointerI13QuBaseManagerED1Ev
	mov	r0, r5
.LEHB284:
	bl	__cxa_end_cleanup
.LEHE284:
.L2757:
	.align	2
.L2756:
	.word	.LANCHOR1
	.word	.LANCHOR0
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA4566:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE4566-.LLSDACSB4566
.LLSDACSB4566:
	.uleb128 .LEHB278-.LFB4566
	.uleb128 .LEHE278-.LEHB278
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB279-.LFB4566
	.uleb128 .LEHE279-.LEHB279
	.uleb128 .L2749-.LFB4566
	.uleb128 0x0
	.uleb128 .LEHB280-.LFB4566
	.uleb128 .LEHE280-.LEHB280
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB281-.LFB4566
	.uleb128 .LEHE281-.LEHB281
	.uleb128 .L2748-.LFB4566
	.uleb128 0x0
	.uleb128 .LEHB282-.LFB4566
	.uleb128 .LEHE282-.LEHB282
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB283-.LFB4566
	.uleb128 .LEHE283-.LEHB283
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB284-.LFB4566
	.uleb128 .LEHE284-.LEHB284
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE4566:
	.fnend
	.size	_Z11singleTouchP15s3ePointerEvent, .-_Z11singleTouchP15s3ePointerEvent
	.section	.text._ZN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev,"axG",%progbits,_ZN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev,comdat
	.align	2
	.weak	_ZTv0_n12_N4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.hidden	_ZTv0_n12_N4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.type	_ZTv0_n12_N4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev, %function
_ZTv0_n12_N4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev:
	.fnstart
.LFB6385:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r1, [r0, #0]
	ldr	r3, [r1, #-12]
	add	r0, r0, r3
	b	.LTHUNK10
	.cantunwind
	.fnend
	.size	_ZTv0_n12_N4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev, .-_ZTv0_n12_N4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.section	.text._ZN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev,"axG",%progbits,_ZN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev,comdat
	.align	2
	.weak	_ZTv0_n12_N4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.hidden	_ZTv0_n12_N4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.type	_ZTv0_n12_N4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev, %function
_ZTv0_n12_N4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev:
	.fnstart
.LFB6374:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r1, [r0, #0]
	ldr	r3, [r1, #-12]
	add	r0, r0, r3
	b	.LTHUNK8
	.cantunwind
	.fnend
	.size	_ZTv0_n12_N4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev, .-_ZTv0_n12_N4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.section	.text._ZN4_STL14basic_iostreamIcNS_11char_traitsIcEEED1Ev,"axG",%progbits,_ZN4_STL14basic_iostreamIcNS_11char_traitsIcEEED1Ev,comdat
	.align	2
	.weak	_ZTv0_n12_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED1Ev
	.hidden	_ZTv0_n12_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED1Ev
	.type	_ZTv0_n12_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED1Ev, %function
_ZTv0_n12_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED1Ev:
	.fnstart
.LFB6367:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r1, [r0, #0]
	ldr	r3, [r1, #-12]
	add	r0, r0, r3
	b	.LTHUNK6
	.cantunwind
	.fnend
	.size	_ZTv0_n12_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED1Ev, .-_ZTv0_n12_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED1Ev
	.section	.text._ZN4_STL14basic_iostreamIcNS_11char_traitsIcEEED0Ev,"axG",%progbits,_ZN4_STL14basic_iostreamIcNS_11char_traitsIcEEED0Ev,comdat
	.align	2
	.weak	_ZTv0_n12_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED0Ev
	.hidden	_ZTv0_n12_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED0Ev
	.type	_ZTv0_n12_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED0Ev, %function
_ZTv0_n12_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED0Ev:
	.fnstart
.LFB6365:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r1, [r0, #0]
	ldr	r3, [r1, #-12]
	add	r0, r0, r3
	b	.LTHUNK4
	.cantunwind
	.fnend
	.size	_ZTv0_n12_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED0Ev, .-_ZTv0_n12_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED0Ev
	.section	.text._ZN4_STL13basic_istreamIcNS_11char_traitsIcEEED1Ev,"axG",%progbits,_ZN4_STL13basic_istreamIcNS_11char_traitsIcEEED1Ev,comdat
	.align	2
	.weak	_ZTv0_n12_N4_STL13basic_istreamIcNS_11char_traitsIcEEED1Ev
	.hidden	_ZTv0_n12_N4_STL13basic_istreamIcNS_11char_traitsIcEEED1Ev
	.type	_ZTv0_n12_N4_STL13basic_istreamIcNS_11char_traitsIcEEED1Ev, %function
_ZTv0_n12_N4_STL13basic_istreamIcNS_11char_traitsIcEEED1Ev:
	.fnstart
.LFB6364:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r1, [r0, #0]
	ldr	r3, [r1, #-12]
	add	r0, r0, r3
	b	.LTHUNK3
	.cantunwind
	.fnend
	.size	_ZTv0_n12_N4_STL13basic_istreamIcNS_11char_traitsIcEEED1Ev, .-_ZTv0_n12_N4_STL13basic_istreamIcNS_11char_traitsIcEEED1Ev
	.section	.text._ZN4_STL13basic_istreamIcNS_11char_traitsIcEEED0Ev,"axG",%progbits,_ZN4_STL13basic_istreamIcNS_11char_traitsIcEEED0Ev,comdat
	.align	2
	.weak	_ZTv0_n12_N4_STL13basic_istreamIcNS_11char_traitsIcEEED0Ev
	.hidden	_ZTv0_n12_N4_STL13basic_istreamIcNS_11char_traitsIcEEED0Ev
	.type	_ZTv0_n12_N4_STL13basic_istreamIcNS_11char_traitsIcEEED0Ev, %function
_ZTv0_n12_N4_STL13basic_istreamIcNS_11char_traitsIcEEED0Ev:
	.fnstart
.LFB6363:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r1, [r0, #0]
	ldr	r3, [r1, #-12]
	add	r0, r0, r3
	b	.LTHUNK2
	.cantunwind
	.fnend
	.size	_ZTv0_n12_N4_STL13basic_istreamIcNS_11char_traitsIcEEED0Ev, .-_ZTv0_n12_N4_STL13basic_istreamIcNS_11char_traitsIcEEED0Ev
	.section	.text._ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEED1Ev,"axG",%progbits,_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEED1Ev,comdat
	.align	2
	.weak	_ZTv0_n12_N4_STL13basic_ostreamIcNS_11char_traitsIcEEED1Ev
	.hidden	_ZTv0_n12_N4_STL13basic_ostreamIcNS_11char_traitsIcEEED1Ev
	.type	_ZTv0_n12_N4_STL13basic_ostreamIcNS_11char_traitsIcEEED1Ev, %function
_ZTv0_n12_N4_STL13basic_ostreamIcNS_11char_traitsIcEEED1Ev:
	.fnstart
.LFB6362:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r1, [r0, #0]
	ldr	r3, [r1, #-12]
	add	r0, r0, r3
	b	.LTHUNK1
	.cantunwind
	.fnend
	.size	_ZTv0_n12_N4_STL13basic_ostreamIcNS_11char_traitsIcEEED1Ev, .-_ZTv0_n12_N4_STL13basic_ostreamIcNS_11char_traitsIcEEED1Ev
	.section	.text._ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEED0Ev,"axG",%progbits,_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEED0Ev,comdat
	.align	2
	.weak	_ZTv0_n12_N4_STL13basic_ostreamIcNS_11char_traitsIcEEED0Ev
	.hidden	_ZTv0_n12_N4_STL13basic_ostreamIcNS_11char_traitsIcEEED0Ev
	.type	_ZTv0_n12_N4_STL13basic_ostreamIcNS_11char_traitsIcEEED0Ev, %function
_ZTv0_n12_N4_STL13basic_ostreamIcNS_11char_traitsIcEEED0Ev:
	.fnstart
.LFB6361:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r1, [r0, #0]
	ldr	r3, [r1, #-12]
	add	r0, r0, r3
	b	.LTHUNK0
	.cantunwind
	.fnend
	.size	_ZTv0_n12_N4_STL13basic_ostreamIcNS_11char_traitsIcEEED0Ev, .-_ZTv0_n12_N4_STL13basic_ostreamIcNS_11char_traitsIcEEED0Ev
	.hidden	lastMousePosition
	.global	lastMousePosition
	.hidden	_ZTV18InstructionManager
	.weak	_ZTV18InstructionManager
	.section	.rodata._ZTV18InstructionManager,"aG",%progbits,_ZTV18InstructionManager,comdat
	.align	3
	.type	_ZTV18InstructionManager, %object
	.size	_ZTV18InstructionManager, 60
_ZTV18InstructionManager:
	.word	0
	.word	_ZTI18InstructionManager
	.word	_ZN18InstructionManagerD1Ev
	.word	_ZN18InstructionManagerD0Ev
	.word	_ZN18InstructionManager10initializeEv
	.word	_ZN18InstructionManager6updateEv
	.word	_ZN13QuBaseManager7keyDownEi
	.word	_ZN13QuBaseManager5keyUpEi
	.word	_ZN13QuBaseManager10singleDownEi13QuScreenCoord
	.word	_ZN18InstructionManager8singleUpEi13QuScreenCoord
	.word	_ZN13QuBaseManager12singleMotionE13QuScreenCoordS0_
	.word	_ZN13QuBaseManager17singleDeltaMotionEv
	.word	_ZN13QuBaseManager12screenResizeEii
	.word	_ZN13QuBaseManager10multiTouchEibii
	.word	_ZN13QuBaseManager11multiMotionEiii
	.hidden	_ZTI18InstructionManager
	.weak	_ZTI18InstructionManager
	.section	.rodata._ZTI18InstructionManager,"aG",%progbits,_ZTI18InstructionManager,comdat
	.align	2
	.type	_ZTI18InstructionManager, %object
	.size	_ZTI18InstructionManager, 12
_ZTI18InstructionManager:
	.word	_ZTVN10__cxxabiv120__si_class_type_infoE+8
	.word	_ZTS18InstructionManager
	.word	_ZTI13QuBaseManager
	.hidden	_ZTS18InstructionManager
	.weak	_ZTS18InstructionManager
	.section	.rodata._ZTS18InstructionManager,"aG",%progbits,_ZTS18InstructionManager,comdat
	.align	2
	.type	_ZTS18InstructionManager, %object
	.size	_ZTS18InstructionManager, 21
_ZTS18InstructionManager:
	.ascii	"18InstructionManager\000"
	.hidden	_ZTI13QuBaseManager
	.weak	_ZTI13QuBaseManager
	.section	.rodata._ZTI13QuBaseManager,"aG",%progbits,_ZTI13QuBaseManager,comdat
	.align	2
	.type	_ZTI13QuBaseManager, %object
	.size	_ZTI13QuBaseManager, 8
_ZTI13QuBaseManager:
	.word	_ZTVN10__cxxabiv117__class_type_infoE+8
	.word	_ZTS13QuBaseManager
	.hidden	_ZTS13QuBaseManager
	.weak	_ZTS13QuBaseManager
	.section	.rodata._ZTS13QuBaseManager,"aG",%progbits,_ZTS13QuBaseManager,comdat
	.align	2
	.type	_ZTS13QuBaseManager, %object
	.size	_ZTS13QuBaseManager, 16
_ZTS13QuBaseManager:
	.ascii	"13QuBaseManager\000"
	.hidden	_ZTTN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak	_ZTTN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.section	.rodata._ZTTN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE,"aG",%progbits,_ZTVN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE,comdat
	.align	3
	.type	_ZTTN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE, %object
	.size	_ZTTN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE, 40
_ZTTN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.word	_ZTVN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE+12
	.word	_ZTCN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_14basic_iostreamIcS2_EE+12
	.word	_ZTCN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_istreamIcS2_EE+12
	.word	_ZTCN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_istreamIcS2_EE+32
	.word	_ZTCN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE8_NS_13basic_ostreamIcS2_EE+12
	.word	_ZTCN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE8_NS_13basic_ostreamIcS2_EE+32
	.word	_ZTCN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_14basic_iostreamIcS2_EE+52
	.word	_ZTCN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_14basic_iostreamIcS2_EE+32
	.word	_ZTVN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE+52
	.word	_ZTVN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE+32
	.hidden	_ZTVN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak	_ZTVN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.section	.rodata._ZTVN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE,"aG",%progbits,_ZTVN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE,comdat
	.align	3
	.type	_ZTVN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE, %object
	.size	_ZTVN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE, 60
_ZTVN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.word	72
	.word	0
	.word	_ZTIN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.word	_ZN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.word	_ZN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.word	64
	.word	-8
	.word	_ZTIN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.word	_ZThn8_N4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.word	_ZThn8_N4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.word	-72
	.word	-72
	.word	_ZTIN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.word	_ZTv0_n12_N4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.word	_ZTv0_n12_N4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.hidden	_ZTCN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_14basic_iostreamIcS2_EE
	.weak	_ZTCN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_14basic_iostreamIcS2_EE
	.section	.rodata._ZTCN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_14basic_iostreamIcS2_EE,"aG",%progbits,_ZTVN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE,comdat
	.align	3
	.type	_ZTCN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_14basic_iostreamIcS2_EE, %object
	.size	_ZTCN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_14basic_iostreamIcS2_EE, 60
_ZTCN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_14basic_iostreamIcS2_EE:
	.word	72
	.word	0
	.word	_ZTIN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE
	.word	_ZN4_STL14basic_iostreamIcNS_11char_traitsIcEEED1Ev
	.word	_ZN4_STL14basic_iostreamIcNS_11char_traitsIcEEED0Ev
	.word	64
	.word	-8
	.word	_ZTIN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE
	.word	_ZThn8_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED1Ev
	.word	_ZThn8_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED0Ev
	.word	-72
	.word	-72
	.word	_ZTIN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE
	.word	_ZTv0_n12_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED1Ev
	.word	_ZTv0_n12_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED0Ev
	.hidden	_ZTCN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_istreamIcS2_EE
	.weak	_ZTCN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_istreamIcS2_EE
	.section	.rodata._ZTCN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_istreamIcS2_EE,"aG",%progbits,_ZTVN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE,comdat
	.align	3
	.type	_ZTCN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_istreamIcS2_EE, %object
	.size	_ZTCN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_istreamIcS2_EE, 40
_ZTCN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE0_NS_13basic_istreamIcS2_EE:
	.word	72
	.word	0
	.word	_ZTIN4_STL13basic_istreamIcNS_11char_traitsIcEEEE
	.word	_ZN4_STL13basic_istreamIcNS_11char_traitsIcEEED1Ev
	.word	_ZN4_STL13basic_istreamIcNS_11char_traitsIcEEED0Ev
	.word	-72
	.word	-72
	.word	_ZTIN4_STL13basic_istreamIcNS_11char_traitsIcEEEE
	.word	_ZTv0_n12_N4_STL13basic_istreamIcNS_11char_traitsIcEEED1Ev
	.word	_ZTv0_n12_N4_STL13basic_istreamIcNS_11char_traitsIcEEED0Ev
	.hidden	_ZTCN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE8_NS_13basic_ostreamIcS2_EE
	.weak	_ZTCN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE8_NS_13basic_ostreamIcS2_EE
	.section	.rodata._ZTCN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE8_NS_13basic_ostreamIcS2_EE,"aG",%progbits,_ZTVN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE,comdat
	.align	3
	.type	_ZTCN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE8_NS_13basic_ostreamIcS2_EE, %object
	.size	_ZTCN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE8_NS_13basic_ostreamIcS2_EE, 40
_ZTCN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE8_NS_13basic_ostreamIcS2_EE:
	.word	64
	.word	0
	.word	_ZTIN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE
	.word	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEED1Ev
	.word	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEED0Ev
	.word	-64
	.word	-64
	.word	_ZTIN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE
	.word	_ZTv0_n12_N4_STL13basic_ostreamIcNS_11char_traitsIcEEED1Ev
	.word	_ZTv0_n12_N4_STL13basic_ostreamIcNS_11char_traitsIcEEED0Ev
	.hidden	_ZTIN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak	_ZTIN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.section	.rodata._ZTIN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE,"aG",%progbits,_ZTIN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE,comdat
	.align	2
	.type	_ZTIN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE, %object
	.size	_ZTIN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE, 12
_ZTIN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.word	_ZTVN10__cxxabiv120__si_class_type_infoE+8
	.word	_ZTSN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.word	_ZTIN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE
	.hidden	_ZTIN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE
	.weak	_ZTIN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE
	.section	.rodata._ZTIN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE,"aG",%progbits,_ZTIN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE,comdat
	.align	2
	.type	_ZTIN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE, %object
	.size	_ZTIN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE, 32
_ZTIN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE:
	.word	_ZTVN10__cxxabiv121__vmi_class_type_infoE+8
	.word	_ZTSN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE
	.word	2
	.word	2
	.word	_ZTIN4_STL13basic_istreamIcNS_11char_traitsIcEEEE
	.word	2
	.word	_ZTIN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE
	.word	2050
	.hidden	_ZTIN4_STL13basic_istreamIcNS_11char_traitsIcEEEE
	.weak	_ZTIN4_STL13basic_istreamIcNS_11char_traitsIcEEEE
	.section	.rodata._ZTIN4_STL13basic_istreamIcNS_11char_traitsIcEEEE,"aG",%progbits,_ZTIN4_STL13basic_istreamIcNS_11char_traitsIcEEEE,comdat
	.align	2
	.type	_ZTIN4_STL13basic_istreamIcNS_11char_traitsIcEEEE, %object
	.size	_ZTIN4_STL13basic_istreamIcNS_11char_traitsIcEEEE, 24
_ZTIN4_STL13basic_istreamIcNS_11char_traitsIcEEEE:
	.word	_ZTVN10__cxxabiv121__vmi_class_type_infoE+8
	.word	_ZTSN4_STL13basic_istreamIcNS_11char_traitsIcEEEE
	.word	0
	.word	1
	.word	_ZTIN4_STL9basic_iosIcNS_11char_traitsIcEEEE
	.word	-3069
	.hidden	_ZTIN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE
	.weak	_ZTIN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE
	.section	.rodata._ZTIN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE,"aG",%progbits,_ZTIN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE,comdat
	.align	2
	.type	_ZTIN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE, %object
	.size	_ZTIN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE, 24
_ZTIN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE:
	.word	_ZTVN10__cxxabiv121__vmi_class_type_infoE+8
	.word	_ZTSN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE
	.word	0
	.word	1
	.word	_ZTIN4_STL9basic_iosIcNS_11char_traitsIcEEEE
	.word	-3069
	.hidden	_ZTSN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak	_ZTSN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.section	.rodata._ZTSN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE,"aG",%progbits,_ZTSN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE,comdat
	.align	2
	.type	_ZTSN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE, %object
	.size	_ZTSN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE, 68
_ZTSN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.ascii	"N4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_"
	.ascii	"9allocatorIcEEEE\000"
	.hidden	_ZTSN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE
	.weak	_ZTSN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE
	.section	.rodata._ZTSN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE,"aG",%progbits,_ZTSN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE,comdat
	.align	2
	.type	_ZTSN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE, %object
	.size	_ZTSN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE, 47
_ZTSN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE:
	.ascii	"N4_STL14basic_iostreamIcNS_11char_traitsIcEEEE\000"
	.hidden	_ZTSN4_STL13basic_istreamIcNS_11char_traitsIcEEEE
	.weak	_ZTSN4_STL13basic_istreamIcNS_11char_traitsIcEEEE
	.section	.rodata._ZTSN4_STL13basic_istreamIcNS_11char_traitsIcEEEE,"aG",%progbits,_ZTSN4_STL13basic_istreamIcNS_11char_traitsIcEEEE,comdat
	.align	2
	.type	_ZTSN4_STL13basic_istreamIcNS_11char_traitsIcEEEE, %object
	.size	_ZTSN4_STL13basic_istreamIcNS_11char_traitsIcEEEE, 46
_ZTSN4_STL13basic_istreamIcNS_11char_traitsIcEEEE:
	.ascii	"N4_STL13basic_istreamIcNS_11char_traitsIcEEEE\000"
	.hidden	_ZTIN4_STL9basic_iosIcNS_11char_traitsIcEEEE
	.weak	_ZTIN4_STL9basic_iosIcNS_11char_traitsIcEEEE
	.section	.rodata._ZTIN4_STL9basic_iosIcNS_11char_traitsIcEEEE,"aG",%progbits,_ZTIN4_STL9basic_iosIcNS_11char_traitsIcEEEE,comdat
	.align	2
	.type	_ZTIN4_STL9basic_iosIcNS_11char_traitsIcEEEE, %object
	.size	_ZTIN4_STL9basic_iosIcNS_11char_traitsIcEEEE, 12
_ZTIN4_STL9basic_iosIcNS_11char_traitsIcEEEE:
	.word	_ZTVN10__cxxabiv120__si_class_type_infoE+8
	.word	_ZTSN4_STL9basic_iosIcNS_11char_traitsIcEEEE
	.word	_ZTIN4_STL8ios_baseE
	.hidden	_ZTSN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE
	.weak	_ZTSN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE
	.section	.rodata._ZTSN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE,"aG",%progbits,_ZTSN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE,comdat
	.align	2
	.type	_ZTSN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE, %object
	.size	_ZTSN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE, 46
_ZTSN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE:
	.ascii	"N4_STL13basic_ostreamIcNS_11char_traitsIcEEEE\000"
	.hidden	_ZTSN4_STL9basic_iosIcNS_11char_traitsIcEEEE
	.weak	_ZTSN4_STL9basic_iosIcNS_11char_traitsIcEEEE
	.section	.rodata._ZTSN4_STL9basic_iosIcNS_11char_traitsIcEEEE,"aG",%progbits,_ZTSN4_STL9basic_iosIcNS_11char_traitsIcEEEE,comdat
	.align	2
	.type	_ZTSN4_STL9basic_iosIcNS_11char_traitsIcEEEE, %object
	.size	_ZTSN4_STL9basic_iosIcNS_11char_traitsIcEEEE, 41
_ZTSN4_STL9basic_iosIcNS_11char_traitsIcEEEE:
	.ascii	"N4_STL9basic_iosIcNS_11char_traitsIcEEEE\000"
	.hidden	_ZTVN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE
	.weak	_ZTVN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE
	.section	.rodata._ZTVN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE,"aG",%progbits,_ZTVN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE,comdat
	.align	3
	.type	_ZTVN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE, %object
	.size	_ZTVN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE, 40
_ZTVN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE:
	.word	4
	.word	0
	.word	_ZTIN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE
	.word	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEED1Ev
	.word	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEED0Ev
	.word	-4
	.word	-4
	.word	_ZTIN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE
	.word	_ZTv0_n12_N4_STL13basic_ostreamIcNS_11char_traitsIcEEED1Ev
	.word	_ZTv0_n12_N4_STL13basic_ostreamIcNS_11char_traitsIcEEED0Ev
	.hidden	_ZTTN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE
	.weak	_ZTTN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE
	.section	.rodata._ZTTN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE,"aG",%progbits,_ZTVN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE,comdat
	.align	3
	.type	_ZTTN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE, %object
	.size	_ZTTN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE, 8
_ZTTN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE:
	.word	_ZTVN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE+12
	.word	_ZTVN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE+32
	.hidden	_ZTVN4_STL13basic_istreamIcNS_11char_traitsIcEEEE
	.weak	_ZTVN4_STL13basic_istreamIcNS_11char_traitsIcEEEE
	.section	.rodata._ZTVN4_STL13basic_istreamIcNS_11char_traitsIcEEEE,"aG",%progbits,_ZTVN4_STL13basic_istreamIcNS_11char_traitsIcEEEE,comdat
	.align	3
	.type	_ZTVN4_STL13basic_istreamIcNS_11char_traitsIcEEEE, %object
	.size	_ZTVN4_STL13basic_istreamIcNS_11char_traitsIcEEEE, 40
_ZTVN4_STL13basic_istreamIcNS_11char_traitsIcEEEE:
	.word	8
	.word	0
	.word	_ZTIN4_STL13basic_istreamIcNS_11char_traitsIcEEEE
	.word	_ZN4_STL13basic_istreamIcNS_11char_traitsIcEEED1Ev
	.word	_ZN4_STL13basic_istreamIcNS_11char_traitsIcEEED0Ev
	.word	-8
	.word	-8
	.word	_ZTIN4_STL13basic_istreamIcNS_11char_traitsIcEEEE
	.word	_ZTv0_n12_N4_STL13basic_istreamIcNS_11char_traitsIcEEED1Ev
	.word	_ZTv0_n12_N4_STL13basic_istreamIcNS_11char_traitsIcEEED0Ev
	.hidden	_ZTTN4_STL13basic_istreamIcNS_11char_traitsIcEEEE
	.weak	_ZTTN4_STL13basic_istreamIcNS_11char_traitsIcEEEE
	.section	.rodata._ZTTN4_STL13basic_istreamIcNS_11char_traitsIcEEEE,"aG",%progbits,_ZTVN4_STL13basic_istreamIcNS_11char_traitsIcEEEE,comdat
	.align	3
	.type	_ZTTN4_STL13basic_istreamIcNS_11char_traitsIcEEEE, %object
	.size	_ZTTN4_STL13basic_istreamIcNS_11char_traitsIcEEEE, 8
_ZTTN4_STL13basic_istreamIcNS_11char_traitsIcEEEE:
	.word	_ZTVN4_STL13basic_istreamIcNS_11char_traitsIcEEEE+12
	.word	_ZTVN4_STL13basic_istreamIcNS_11char_traitsIcEEEE+32
	.hidden	_ZTVN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE
	.weak	_ZTVN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE
	.section	.rodata._ZTVN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE,"aG",%progbits,_ZTVN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE,comdat
	.align	3
	.type	_ZTVN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE, %object
	.size	_ZTVN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE, 60
_ZTVN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE:
	.word	12
	.word	0
	.word	_ZTIN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE
	.word	_ZN4_STL14basic_iostreamIcNS_11char_traitsIcEEED1Ev
	.word	_ZN4_STL14basic_iostreamIcNS_11char_traitsIcEEED0Ev
	.word	4
	.word	-8
	.word	_ZTIN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE
	.word	_ZThn8_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED1Ev
	.word	_ZThn8_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED0Ev
	.word	-12
	.word	-12
	.word	_ZTIN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE
	.word	_ZTv0_n12_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED1Ev
	.word	_ZTv0_n12_N4_STL14basic_iostreamIcNS_11char_traitsIcEEED0Ev
	.hidden	_ZTTN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE
	.weak	_ZTTN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE
	.section	.rodata._ZTTN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE,"aG",%progbits,_ZTVN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE,comdat
	.align	3
	.type	_ZTTN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE, %object
	.size	_ZTTN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE, 28
_ZTTN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE:
	.word	_ZTVN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE+12
	.word	_ZTCN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE0_NS_13basic_istreamIcS2_EE+12
	.word	_ZTCN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE0_NS_13basic_istreamIcS2_EE+32
	.word	_ZTCN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE8_NS_13basic_ostreamIcS2_EE+12
	.word	_ZTCN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE8_NS_13basic_ostreamIcS2_EE+32
	.word	_ZTVN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE+52
	.word	_ZTVN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE+32
	.hidden	_ZTCN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE0_NS_13basic_istreamIcS2_EE
	.weak	_ZTCN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE0_NS_13basic_istreamIcS2_EE
	.section	.rodata._ZTCN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE0_NS_13basic_istreamIcS2_EE,"aG",%progbits,_ZTVN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE,comdat
	.align	3
	.type	_ZTCN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE0_NS_13basic_istreamIcS2_EE, %object
	.size	_ZTCN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE0_NS_13basic_istreamIcS2_EE, 40
_ZTCN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE0_NS_13basic_istreamIcS2_EE:
	.word	12
	.word	0
	.word	_ZTIN4_STL13basic_istreamIcNS_11char_traitsIcEEEE
	.word	_ZN4_STL13basic_istreamIcNS_11char_traitsIcEEED1Ev
	.word	_ZN4_STL13basic_istreamIcNS_11char_traitsIcEEED0Ev
	.word	-12
	.word	-12
	.word	_ZTIN4_STL13basic_istreamIcNS_11char_traitsIcEEEE
	.word	_ZTv0_n12_N4_STL13basic_istreamIcNS_11char_traitsIcEEED1Ev
	.word	_ZTv0_n12_N4_STL13basic_istreamIcNS_11char_traitsIcEEED0Ev
	.hidden	_ZTCN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE8_NS_13basic_ostreamIcS2_EE
	.weak	_ZTCN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE8_NS_13basic_ostreamIcS2_EE
	.section	.rodata._ZTCN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE8_NS_13basic_ostreamIcS2_EE,"aG",%progbits,_ZTVN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE,comdat
	.align	3
	.type	_ZTCN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE8_NS_13basic_ostreamIcS2_EE, %object
	.size	_ZTCN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE8_NS_13basic_ostreamIcS2_EE, 40
_ZTCN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE8_NS_13basic_ostreamIcS2_EE:
	.word	4
	.word	0
	.word	_ZTIN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE
	.word	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEED1Ev
	.word	_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEED0Ev
	.word	-4
	.word	-4
	.word	_ZTIN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE
	.word	_ZTv0_n12_N4_STL13basic_ostreamIcNS_11char_traitsIcEEED1Ev
	.word	_ZTv0_n12_N4_STL13basic_ostreamIcNS_11char_traitsIcEEED0Ev
	.hidden	_ZTVN4_STL9basic_iosIcNS_11char_traitsIcEEEE
	.weak	_ZTVN4_STL9basic_iosIcNS_11char_traitsIcEEEE
	.section	.rodata._ZTVN4_STL9basic_iosIcNS_11char_traitsIcEEEE,"aG",%progbits,_ZTVN4_STL9basic_iosIcNS_11char_traitsIcEEEE,comdat
	.align	3
	.type	_ZTVN4_STL9basic_iosIcNS_11char_traitsIcEEEE, %object
	.size	_ZTVN4_STL9basic_iosIcNS_11char_traitsIcEEEE, 16
_ZTVN4_STL9basic_iosIcNS_11char_traitsIcEEEE:
	.word	0
	.word	_ZTIN4_STL9basic_iosIcNS_11char_traitsIcEEEE
	.word	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEED1Ev
	.word	_ZN4_STL9basic_iosIcNS_11char_traitsIcEEED0Ev
	.hidden	_ZTVN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak	_ZTVN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.section	.rodata._ZTVN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE,"aG",%progbits,_ZTVN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE,comdat
	.align	3
	.type	_ZTVN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE, %object
	.size	_ZTVN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE, 68
_ZTVN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.word	0
	.word	_ZTIN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.word	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.word	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.word	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE6setbufEPci
	.word	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekoffElii
	.word	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE7seekposENS_4fposIiEEi
	.word	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE4syncEv
	.word	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE9showmanycEv
	.word	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE6xsgetnEPci
	.word	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9underflowEv
	.word	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE5uflowEv
	.word	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE9pbackfailEi
	.word	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE6xsputnEPKci
	.word	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE10_M_xsputncEci
	.word	_ZN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEE8overflowEi
	.word	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE
	.hidden	_ZTIN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak	_ZTIN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.section	.rodata._ZTIN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE,"aG",%progbits,_ZTIN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE,comdat
	.align	2
	.type	_ZTIN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE, %object
	.size	_ZTIN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE, 12
_ZTIN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.word	_ZTVN10__cxxabiv120__si_class_type_infoE+8
	.word	_ZTSN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.word	_ZTIN4_STL15basic_streambufIcNS_11char_traitsIcEEEE
	.hidden	_ZTSN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.weak	_ZTSN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.section	.rodata._ZTSN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE,"aG",%progbits,_ZTSN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE,comdat
	.align	2
	.type	_ZTSN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE, %object
	.size	_ZTSN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE, 65
_ZTSN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE:
	.ascii	"N4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9al"
	.ascii	"locatorIcEEEE\000"
	.hidden	_ZTIN4_STL15basic_streambufIcNS_11char_traitsIcEEEE
	.weak	_ZTIN4_STL15basic_streambufIcNS_11char_traitsIcEEEE
	.section	.rodata._ZTIN4_STL15basic_streambufIcNS_11char_traitsIcEEEE,"aG",%progbits,_ZTIN4_STL15basic_streambufIcNS_11char_traitsIcEEEE,comdat
	.align	2
	.type	_ZTIN4_STL15basic_streambufIcNS_11char_traitsIcEEEE, %object
	.size	_ZTIN4_STL15basic_streambufIcNS_11char_traitsIcEEEE, 8
_ZTIN4_STL15basic_streambufIcNS_11char_traitsIcEEEE:
	.word	_ZTVN10__cxxabiv117__class_type_infoE+8
	.word	_ZTSN4_STL15basic_streambufIcNS_11char_traitsIcEEEE
	.hidden	_ZTSN4_STL15basic_streambufIcNS_11char_traitsIcEEEE
	.weak	_ZTSN4_STL15basic_streambufIcNS_11char_traitsIcEEEE
	.section	.rodata._ZTSN4_STL15basic_streambufIcNS_11char_traitsIcEEEE,"aG",%progbits,_ZTSN4_STL15basic_streambufIcNS_11char_traitsIcEEEE,comdat
	.align	2
	.type	_ZTSN4_STL15basic_streambufIcNS_11char_traitsIcEEEE, %object
	.size	_ZTSN4_STL15basic_streambufIcNS_11char_traitsIcEEEE, 48
_ZTSN4_STL15basic_streambufIcNS_11char_traitsIcEEEE:
	.ascii	"N4_STL15basic_streambufIcNS_11char_traitsIcEEEE\000"
	.hidden	_ZTVN4_STL15basic_streambufIcNS_11char_traitsIcEEEE
	.weak	_ZTVN4_STL15basic_streambufIcNS_11char_traitsIcEEEE
	.section	.rodata._ZTVN4_STL15basic_streambufIcNS_11char_traitsIcEEEE,"aG",%progbits,_ZTVN4_STL15basic_streambufIcNS_11char_traitsIcEEEE,comdat
	.align	3
	.type	_ZTVN4_STL15basic_streambufIcNS_11char_traitsIcEEEE, %object
	.size	_ZTVN4_STL15basic_streambufIcNS_11char_traitsIcEEEE, 68
_ZTVN4_STL15basic_streambufIcNS_11char_traitsIcEEEE:
	.word	0
	.word	_ZTIN4_STL15basic_streambufIcNS_11char_traitsIcEEEE
	.word	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEED1Ev
	.word	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEED0Ev
	.word	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE6setbufEPci
	.word	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE7seekoffElii
	.word	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE7seekposENS_4fposIiEEi
	.word	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE4syncEv
	.word	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE9showmanycEv
	.word	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE6xsgetnEPci
	.word	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE9underflowEv
	.word	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE5uflowEv
	.word	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE9pbackfailEi
	.word	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE6xsputnEPKci
	.word	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE10_M_xsputncEci
	.word	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE8overflowEi
	.word	_ZN4_STL15basic_streambufIcNS_11char_traitsIcEEE5imbueERKNS_6localeE
	.hidden	_ZN4_STL7num_putIcNS_19ostreambuf_iteratorIcNS_11char_traitsIcEEEEE2idE
	.weak	_ZN4_STL7num_putIcNS_19ostreambuf_iteratorIcNS_11char_traitsIcEEEEE2idE
	.section	.bss._ZN4_STL7num_putIcNS_19ostreambuf_iteratorIcNS_11char_traitsIcEEEEE2idE,"awG",%nobits,_ZN4_STL7num_putIcNS_19ostreambuf_iteratorIcNS_11char_traitsIcEEEEE2idE,comdat
	.align	2
	.type	_ZN4_STL7num_putIcNS_19ostreambuf_iteratorIcNS_11char_traitsIcEEEEE2idE, %object
	.size	_ZN4_STL7num_putIcNS_19ostreambuf_iteratorIcNS_11char_traitsIcEEEEE2idE, 4
_ZN4_STL7num_putIcNS_19ostreambuf_iteratorIcNS_11char_traitsIcEEEEE2idE:
	.space	4
	.hidden	_ZTV13QuBaseManager
	.weak	_ZTV13QuBaseManager
	.section	.rodata._ZTV13QuBaseManager,"aG",%progbits,_ZTV13QuBaseManager,comdat
	.align	3
	.type	_ZTV13QuBaseManager, %object
	.size	_ZTV13QuBaseManager, 60
_ZTV13QuBaseManager:
	.word	0
	.word	_ZTI13QuBaseManager
	.word	_ZN13QuBaseManagerD1Ev
	.word	_ZN13QuBaseManagerD0Ev
	.word	_ZN13QuBaseManager10initializeEv
	.word	_ZN13QuBaseManager6updateEv
	.word	_ZN13QuBaseManager7keyDownEi
	.word	_ZN13QuBaseManager5keyUpEi
	.word	_ZN13QuBaseManager10singleDownEi13QuScreenCoord
	.word	_ZN13QuBaseManager8singleUpEi13QuScreenCoord
	.word	_ZN13QuBaseManager12singleMotionE13QuScreenCoordS0_
	.word	_ZN13QuBaseManager17singleDeltaMotionEv
	.word	_ZN13QuBaseManager12screenResizeEii
	.word	_ZN13QuBaseManager10multiTouchEibii
	.word	_ZN13QuBaseManager11multiMotionEiii
	.hidden	_ZTV7QuTimer
	.weak	_ZTV7QuTimer
	.section	.rodata._ZTV7QuTimer,"aG",%progbits,_ZTV7QuTimer,comdat
	.align	3
	.type	_ZTV7QuTimer, %object
	.size	_ZTV7QuTimer, 24
_ZTV7QuTimer:
	.word	0
	.word	_ZTI7QuTimer
	.word	_ZN7QuTimerD1Ev
	.word	_ZN7QuTimerD0Ev
	.word	_ZN7QuTimer6updateEv
	.word	_ZN7QuTimer5resetEv
	.hidden	_ZTI7QuTimer
	.weak	_ZTI7QuTimer
	.section	.rodata._ZTI7QuTimer,"aG",%progbits,_ZTI7QuTimer,comdat
	.align	2
	.type	_ZTI7QuTimer, %object
	.size	_ZTI7QuTimer, 8
_ZTI7QuTimer:
	.word	_ZTVN10__cxxabiv117__class_type_infoE+8
	.word	_ZTS7QuTimer
	.hidden	_ZTS7QuTimer
	.weak	_ZTS7QuTimer
	.section	.rodata._ZTS7QuTimer,"aG",%progbits,_ZTS7QuTimer,comdat
	.align	2
	.type	_ZTS7QuTimer, %object
	.size	_ZTS7QuTimer, 9
_ZTS7QuTimer:
	.ascii	"7QuTimer\000"
	.hidden	_ZTV17QuImageDrawObject
	.weak	_ZTV17QuImageDrawObject
	.section	.rodata._ZTV17QuImageDrawObject,"aG",%progbits,_ZTV17QuImageDrawObject,comdat
	.align	3
	.type	_ZTV17QuImageDrawObject, %object
	.size	_ZTV17QuImageDrawObject, 24
_ZTV17QuImageDrawObject:
	.word	0
	.word	_ZTI17QuImageDrawObject
	.word	_ZN17QuImageDrawObjectD1Ev
	.word	_ZN17QuImageDrawObjectD0Ev
	.word	_ZN12QuDrawObject6enableEv
	.word	_ZN12QuDrawObject7disableEv
	.hidden	_ZTI17QuImageDrawObject
	.weak	_ZTI17QuImageDrawObject
	.section	.rodata._ZTI17QuImageDrawObject,"aG",%progbits,_ZTI17QuImageDrawObject,comdat
	.align	2
	.type	_ZTI17QuImageDrawObject, %object
	.size	_ZTI17QuImageDrawObject, 12
_ZTI17QuImageDrawObject:
	.word	_ZTVN10__cxxabiv120__si_class_type_infoE+8
	.word	_ZTS17QuImageDrawObject
	.word	_ZTI12QuDrawObject
	.hidden	_ZTS17QuImageDrawObject
	.weak	_ZTS17QuImageDrawObject
	.section	.rodata._ZTS17QuImageDrawObject,"aG",%progbits,_ZTS17QuImageDrawObject,comdat
	.align	2
	.type	_ZTS17QuImageDrawObject, %object
	.size	_ZTS17QuImageDrawObject, 20
_ZTS17QuImageDrawObject:
	.ascii	"17QuImageDrawObject\000"
	.hidden	_ZTI12QuDrawObject
	.weak	_ZTI12QuDrawObject
	.section	.rodata._ZTI12QuDrawObject,"aG",%progbits,_ZTI12QuDrawObject,comdat
	.align	2
	.type	_ZTI12QuDrawObject, %object
	.size	_ZTI12QuDrawObject, 8
_ZTI12QuDrawObject:
	.word	_ZTVN10__cxxabiv117__class_type_infoE+8
	.word	_ZTS12QuDrawObject
	.hidden	_ZTS12QuDrawObject
	.weak	_ZTS12QuDrawObject
	.section	.rodata._ZTS12QuDrawObject,"aG",%progbits,_ZTS12QuDrawObject,comdat
	.align	2
	.type	_ZTS12QuDrawObject, %object
	.size	_ZTS12QuDrawObject, 15
_ZTS12QuDrawObject:
	.ascii	"12QuDrawObject\000"
	.hidden	_ZTV12QuDrawObject
	.weak	_ZTV12QuDrawObject
	.section	.rodata._ZTV12QuDrawObject,"aG",%progbits,_ZTV12QuDrawObject,comdat
	.align	3
	.type	_ZTV12QuDrawObject, %object
	.size	_ZTV12QuDrawObject, 24
_ZTV12QuDrawObject:
	.word	0
	.word	_ZTI12QuDrawObject
	.word	_ZN12QuDrawObjectD1Ev
	.word	_ZN12QuDrawObjectD0Ev
	.word	_ZN12QuDrawObject6enableEv
	.word	_ZN12QuDrawObject7disableEv
	.hidden	_ZTV23QuAbsScreenIntBoxButton
	.weak	_ZTV23QuAbsScreenIntBoxButton
	.section	.rodata._ZTV23QuAbsScreenIntBoxButton,"aG",%progbits,_ZTV23QuAbsScreenIntBoxButton,comdat
	.align	3
	.type	_ZTV23QuAbsScreenIntBoxButton, %object
	.size	_ZTV23QuAbsScreenIntBoxButton, 24
_ZTV23QuAbsScreenIntBoxButton:
	.word	0
	.word	_ZTI23QuAbsScreenIntBoxButton
	.word	_ZN23QuAbsScreenIntBoxButton5clickE13QuScreenCoord15GDeviceRotation
	.word	_ZN23QuAbsScreenIntBoxButton16drawRotatedImageE15QuStupidPointerI11QuBaseImageE15GDeviceRotation
	.word	_ZN23QuAbsScreenIntBoxButton15drawRotatedRectE7QuColor15GDeviceRotation
	.word	_ZN23QuAbsScreenIntBoxButton6getBoxE15GDeviceRotation
	.hidden	_ZTI23QuAbsScreenIntBoxButton
	.weak	_ZTI23QuAbsScreenIntBoxButton
	.section	.rodata._ZTI23QuAbsScreenIntBoxButton,"aG",%progbits,_ZTI23QuAbsScreenIntBoxButton,comdat
	.align	2
	.type	_ZTI23QuAbsScreenIntBoxButton, %object
	.size	_ZTI23QuAbsScreenIntBoxButton, 24
_ZTI23QuAbsScreenIntBoxButton:
	.word	_ZTVN10__cxxabiv121__vmi_class_type_infoE+8
	.word	_ZTS23QuAbsScreenIntBoxButton
	.word	0
	.word	1
	.word	_ZTI11QuBoxButton
	.word	0
	.hidden	_ZTS23QuAbsScreenIntBoxButton
	.weak	_ZTS23QuAbsScreenIntBoxButton
	.section	.rodata._ZTS23QuAbsScreenIntBoxButton,"aG",%progbits,_ZTS23QuAbsScreenIntBoxButton,comdat
	.align	2
	.type	_ZTS23QuAbsScreenIntBoxButton, %object
	.size	_ZTS23QuAbsScreenIntBoxButton, 26
_ZTS23QuAbsScreenIntBoxButton:
	.ascii	"23QuAbsScreenIntBoxButton\000"
	.hidden	_ZTI11QuBoxButton
	.weak	_ZTI11QuBoxButton
	.section	.rodata._ZTI11QuBoxButton,"aG",%progbits,_ZTI11QuBoxButton,comdat
	.align	2
	.type	_ZTI11QuBoxButton, %object
	.size	_ZTI11QuBoxButton, 8
_ZTI11QuBoxButton:
	.word	_ZTVN10__cxxabiv117__class_type_infoE+8
	.word	_ZTS11QuBoxButton
	.hidden	_ZTS11QuBoxButton
	.weak	_ZTS11QuBoxButton
	.section	.rodata._ZTS11QuBoxButton,"aG",%progbits,_ZTS11QuBoxButton,comdat
	.align	2
	.type	_ZTS11QuBoxButton, %object
	.size	_ZTS11QuBoxButton, 14
_ZTS11QuBoxButton:
	.ascii	"11QuBoxButton\000"
	.hidden	_ZTV21QuRectangleDrawObjectIiE
	.weak	_ZTV21QuRectangleDrawObjectIiE
	.section	.rodata._ZTV21QuRectangleDrawObjectIiE,"aG",%progbits,_ZTV21QuRectangleDrawObjectIiE,comdat
	.align	3
	.type	_ZTV21QuRectangleDrawObjectIiE, %object
	.size	_ZTV21QuRectangleDrawObjectIiE, 24
_ZTV21QuRectangleDrawObjectIiE:
	.word	0
	.word	_ZTI21QuRectangleDrawObjectIiE
	.word	_ZN21QuRectangleDrawObjectIiED1Ev
	.word	_ZN21QuRectangleDrawObjectIiED0Ev
	.word	_ZN12QuDrawObject6enableEv
	.word	_ZN12QuDrawObject7disableEv
	.hidden	_ZTI21QuRectangleDrawObjectIiE
	.weak	_ZTI21QuRectangleDrawObjectIiE
	.section	.rodata._ZTI21QuRectangleDrawObjectIiE,"aG",%progbits,_ZTI21QuRectangleDrawObjectIiE,comdat
	.align	2
	.type	_ZTI21QuRectangleDrawObjectIiE, %object
	.size	_ZTI21QuRectangleDrawObjectIiE, 12
_ZTI21QuRectangleDrawObjectIiE:
	.word	_ZTVN10__cxxabiv120__si_class_type_infoE+8
	.word	_ZTS21QuRectangleDrawObjectIiE
	.word	_ZTI12QuDrawObject
	.hidden	_ZTS21QuRectangleDrawObjectIiE
	.weak	_ZTS21QuRectangleDrawObjectIiE
	.section	.rodata._ZTS21QuRectangleDrawObjectIiE,"aG",%progbits,_ZTS21QuRectangleDrawObjectIiE,comdat
	.align	2
	.type	_ZTS21QuRectangleDrawObjectIiE, %object
	.size	_ZTS21QuRectangleDrawObjectIiE, 27
_ZTS21QuRectangleDrawObjectIiE:
	.ascii	"21QuRectangleDrawObjectIiE\000"
	.hidden	_ZTV11QuBoxButton
	.weak	_ZTV11QuBoxButton
	.section	.rodata._ZTV11QuBoxButton,"aG",%progbits,_ZTV11QuBoxButton,comdat
	.align	3
	.type	_ZTV11QuBoxButton, %object
	.size	_ZTV11QuBoxButton, 24
_ZTV11QuBoxButton:
	.word	0
	.word	_ZTI11QuBoxButton
	.word	__cxa_pure_virtual
	.word	_ZN11QuBoxButton16drawRotatedImageE15QuStupidPointerI11QuBaseImageE15GDeviceRotation
	.word	_ZN11QuBoxButton15drawRotatedRectE7QuColor15GDeviceRotation
	.word	__cxa_pure_virtual
	.hidden	_ZTV13QuBasicCamera
	.weak	_ZTV13QuBasicCamera
	.section	.rodata._ZTV13QuBasicCamera,"aG",%progbits,_ZTV13QuBasicCamera,comdat
	.align	3
	.type	_ZTV13QuBasicCamera, %object
	.size	_ZTV13QuBasicCamera, 24
_ZTV13QuBasicCamera:
	.word	0
	.word	_ZTI13QuBasicCamera
	.word	_ZN13QuBasicCameraD1Ev
	.word	_ZN13QuBasicCameraD0Ev
	.word	_ZN13QuBasicCamera19setProjectionMatrixEv
	.word	_ZN13QuBasicCamera18setModelViewMatrixEv
	.hidden	_ZTI13QuBasicCamera
	.weak	_ZTI13QuBasicCamera
	.section	.rodata._ZTI13QuBasicCamera,"aG",%progbits,_ZTI13QuBasicCamera,comdat
	.align	2
	.type	_ZTI13QuBasicCamera, %object
	.size	_ZTI13QuBasicCamera, 8
_ZTI13QuBasicCamera:
	.word	_ZTVN10__cxxabiv117__class_type_infoE+8
	.word	_ZTS13QuBasicCamera
	.hidden	_ZTS13QuBasicCamera
	.weak	_ZTS13QuBasicCamera
	.section	.rodata._ZTS13QuBasicCamera,"aG",%progbits,_ZTS13QuBasicCamera,comdat
	.align	2
	.type	_ZTS13QuBasicCamera, %object
	.size	_ZTS13QuBasicCamera, 16
_ZTS13QuBasicCamera:
	.ascii	"13QuBasicCamera\000"
	.hidden	_ZTV19QuApRawSoundManager
	.weak	_ZTV19QuApRawSoundManager
	.section	.rodata._ZTV19QuApRawSoundManager,"aG",%progbits,_ZTV19QuApRawSoundManager,comdat
	.align	3
	.type	_ZTV19QuApRawSoundManager, %object
	.size	_ZTV19QuApRawSoundManager, 40
_ZTV19QuApRawSoundManager:
	.word	0
	.word	_ZTI19QuApRawSoundManager
	.word	_ZN19QuApRawSoundManagerD1Ev
	.word	_ZN19QuApRawSoundManagerD0Ev
	.word	_ZN19QuApRawSoundManager16areSoundsPlayingEv
	.word	_ZN19QuApRawSoundManager9loadSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.word	_ZN19QuApRawSoundManager8getSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.word	_ZN23QuSoundManagerInterface9playSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.word	_ZN19QuApRawSoundManager11deleteSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.word	_ZN19QuApRawSoundManager6updateEv
	.hidden	_ZTI19QuApRawSoundManager
	.weak	_ZTI19QuApRawSoundManager
	.section	.rodata._ZTI19QuApRawSoundManager,"aG",%progbits,_ZTI19QuApRawSoundManager,comdat
	.align	2
	.type	_ZTI19QuApRawSoundManager, %object
	.size	_ZTI19QuApRawSoundManager, 12
_ZTI19QuApRawSoundManager:
	.word	_ZTVN10__cxxabiv120__si_class_type_infoE+8
	.word	_ZTS19QuApRawSoundManager
	.word	_ZTI23QuSoundManagerInterface
	.hidden	_ZTS19QuApRawSoundManager
	.weak	_ZTS19QuApRawSoundManager
	.section	.rodata._ZTS19QuApRawSoundManager,"aG",%progbits,_ZTS19QuApRawSoundManager,comdat
	.align	2
	.type	_ZTS19QuApRawSoundManager, %object
	.size	_ZTS19QuApRawSoundManager, 22
_ZTS19QuApRawSoundManager:
	.ascii	"19QuApRawSoundManager\000"
	.hidden	_ZTI23QuSoundManagerInterface
	.weak	_ZTI23QuSoundManagerInterface
	.section	.rodata._ZTI23QuSoundManagerInterface,"aG",%progbits,_ZTI23QuSoundManagerInterface,comdat
	.align	2
	.type	_ZTI23QuSoundManagerInterface, %object
	.size	_ZTI23QuSoundManagerInterface, 8
_ZTI23QuSoundManagerInterface:
	.word	_ZTVN10__cxxabiv117__class_type_infoE+8
	.word	_ZTS23QuSoundManagerInterface
	.hidden	_ZTS23QuSoundManagerInterface
	.weak	_ZTS23QuSoundManagerInterface
	.section	.rodata._ZTS23QuSoundManagerInterface,"aG",%progbits,_ZTS23QuSoundManagerInterface,comdat
	.align	2
	.type	_ZTS23QuSoundManagerInterface, %object
	.size	_ZTS23QuSoundManagerInterface, 26
_ZTS23QuSoundManagerInterface:
	.ascii	"23QuSoundManagerInterface\000"
	.hidden	_ZTV18QuApRawSoundStruct
	.weak	_ZTV18QuApRawSoundStruct
	.section	.rodata._ZTV18QuApRawSoundStruct,"aG",%progbits,_ZTV18QuApRawSoundStruct,comdat
	.align	3
	.type	_ZTV18QuApRawSoundStruct, %object
	.size	_ZTV18QuApRawSoundStruct, 36
_ZTV18QuApRawSoundStruct:
	.word	0
	.word	_ZTI18QuApRawSoundStruct
	.word	_ZN18QuApRawSoundStructD1Ev
	.word	_ZN18QuApRawSoundStructD0Ev
	.word	_ZN18QuApRawSoundStruct4playEv
	.word	_ZN18QuApRawSoundStruct9isPlayingEv
	.word	_ZN18QuApRawSoundStruct4stopEv
	.word	_ZN18QuApRawSoundStruct9setVolumeEf
	.word	_ZN22QuSoundStructInterface6updateEv
	.hidden	_ZTI18QuApRawSoundStruct
	.weak	_ZTI18QuApRawSoundStruct
	.section	.rodata._ZTI18QuApRawSoundStruct,"aG",%progbits,_ZTI18QuApRawSoundStruct,comdat
	.align	2
	.type	_ZTI18QuApRawSoundStruct, %object
	.size	_ZTI18QuApRawSoundStruct, 12
_ZTI18QuApRawSoundStruct:
	.word	_ZTVN10__cxxabiv120__si_class_type_infoE+8
	.word	_ZTS18QuApRawSoundStruct
	.word	_ZTI22QuSoundStructInterface
	.hidden	_ZTS18QuApRawSoundStruct
	.weak	_ZTS18QuApRawSoundStruct
	.section	.rodata._ZTS18QuApRawSoundStruct,"aG",%progbits,_ZTS18QuApRawSoundStruct,comdat
	.align	2
	.type	_ZTS18QuApRawSoundStruct, %object
	.size	_ZTS18QuApRawSoundStruct, 21
_ZTS18QuApRawSoundStruct:
	.ascii	"18QuApRawSoundStruct\000"
	.hidden	_ZTI22QuSoundStructInterface
	.weak	_ZTI22QuSoundStructInterface
	.section	.rodata._ZTI22QuSoundStructInterface,"aG",%progbits,_ZTI22QuSoundStructInterface,comdat
	.align	2
	.type	_ZTI22QuSoundStructInterface, %object
	.size	_ZTI22QuSoundStructInterface, 8
_ZTI22QuSoundStructInterface:
	.word	_ZTVN10__cxxabiv117__class_type_infoE+8
	.word	_ZTS22QuSoundStructInterface
	.hidden	_ZTS22QuSoundStructInterface
	.weak	_ZTS22QuSoundStructInterface
	.section	.rodata._ZTS22QuSoundStructInterface,"aG",%progbits,_ZTS22QuSoundStructInterface,comdat
	.align	2
	.type	_ZTS22QuSoundStructInterface, %object
	.size	_ZTS22QuSoundStructInterface, 25
_ZTS22QuSoundStructInterface:
	.ascii	"22QuSoundStructInterface\000"
	.hidden	_ZTV22QuSoundStructInterface
	.weak	_ZTV22QuSoundStructInterface
	.section	.rodata._ZTV22QuSoundStructInterface,"aG",%progbits,_ZTV22QuSoundStructInterface,comdat
	.align	3
	.type	_ZTV22QuSoundStructInterface, %object
	.size	_ZTV22QuSoundStructInterface, 36
_ZTV22QuSoundStructInterface:
	.word	0
	.word	_ZTI22QuSoundStructInterface
	.word	_ZN22QuSoundStructInterfaceD1Ev
	.word	_ZN22QuSoundStructInterfaceD0Ev
	.word	__cxa_pure_virtual
	.word	__cxa_pure_virtual
	.word	__cxa_pure_virtual
	.word	__cxa_pure_virtual
	.word	_ZN22QuSoundStructInterface6updateEv
	.hidden	_ZTV23QuSoundManagerInterface
	.weak	_ZTV23QuSoundManagerInterface
	.section	.rodata._ZTV23QuSoundManagerInterface,"aG",%progbits,_ZTV23QuSoundManagerInterface,comdat
	.align	3
	.type	_ZTV23QuSoundManagerInterface, %object
	.size	_ZTV23QuSoundManagerInterface, 40
_ZTV23QuSoundManagerInterface:
	.word	0
	.word	_ZTI23QuSoundManagerInterface
	.word	_ZN23QuSoundManagerInterfaceD1Ev
	.word	_ZN23QuSoundManagerInterfaceD0Ev
	.word	__cxa_pure_virtual
	.word	__cxa_pure_virtual
	.word	__cxa_pure_virtual
	.word	_ZN23QuSoundManagerInterface9playSoundEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.word	__cxa_pure_virtual
	.word	_ZN23QuSoundManagerInterface6updateEv
	.set	.LTHUNK0,_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEED0Ev
	.set	.LTHUNK1,_ZN4_STL13basic_ostreamIcNS_11char_traitsIcEEED1Ev
	.set	.LTHUNK2,_ZN4_STL13basic_istreamIcNS_11char_traitsIcEEED0Ev
	.set	.LTHUNK3,_ZN4_STL13basic_istreamIcNS_11char_traitsIcEEED1Ev
	.set	.LTHUNK4,_ZN4_STL14basic_iostreamIcNS_11char_traitsIcEEED0Ev
	.set	.LTHUNK5,_ZN4_STL14basic_iostreamIcNS_11char_traitsIcEEED0Ev
	.set	.LTHUNK6,_ZN4_STL14basic_iostreamIcNS_11char_traitsIcEEED1Ev
	.set	.LTHUNK7,_ZN4_STL14basic_iostreamIcNS_11char_traitsIcEEED1Ev
	.set	.LTHUNK8,_ZN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.set	.LTHUNK9,_ZN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED0Ev
	.set	.LTHUNK10,_ZN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.set	.LTHUNK11,_ZN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEED1Ev
	.section	.rodata
	.align	2
	.set	.LANCHOR1,. + 0
	.type	_ZL15GUI_RECT_COORDS, %object
	.size	_ZL15GUI_RECT_COORDS, 48
_ZL15GUI_RECT_COORDS:
	.word	-1090519040
	.word	1056964608
	.word	0
	.word	1056964608
	.word	1056964608
	.word	0
	.word	-1090519040
	.word	-1090519040
	.word	0
	.word	1056964608
	.word	-1090519040
	.word	0
	.type	_ZL26DEVICE_ROTATION_TO_INVERSE, %object
	.size	_ZL26DEVICE_ROTATION_TO_INVERSE, 16
_ZL26DEVICE_ROTATION_TO_INVERSE:
	.word	0
	.word	3
	.word	2
	.word	1
	.type	CSWTCH.1786, %object
	.size	CSWTCH.1786, 12
CSWTCH.1786:
	.word	1
	.word	2
	.word	3
	.type	_ZL24DEVICE_ROTATION_TO_ANGLE, %object
	.size	_ZL24DEVICE_ROTATION_TO_ANGLE, 16
_ZL24DEVICE_ROTATION_TO_ANGLE:
	.word	0
	.word	270
	.word	180
	.word	90
	.type	_ZL35GL_ROTATE_TO_SURFACE_BLIT_DIRECTION, %object
	.size	_ZL35GL_ROTATE_TO_SURFACE_BLIT_DIRECTION, 16
_ZL35GL_ROTATE_TO_SURFACE_BLIT_DIRECTION:
	.word	0
	.word	1
	.word	2
	.word	3
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"basic_string\000"
	.space	3
.LC1:
	.ascii	"paused\000"
	.space	1
.LC2:
	.ascii	"rb\000"
	.space	1
.LC3:
	.ascii	"failed to open \000"
.LC4:
	.ascii	"hard quit\000"
	.space	2
.LC5:
	.ascii	"accel.txt\000"
	.space	2
.LC6:
	.ascii	"w\000"
	.space	2
.LC7:
	.ascii	"images/title.png\000"
	.space	3
.LC8:
	.ascii	"sounds/SELECT.raw\000"
	.bss
	.align	2
	.set	.LANCHOR0,. + 0
	.type	_ZN4_STLL8_LocInitE, %object
	.size	_ZN4_STLL8_LocInitE, 1
_ZN4_STLL8_LocInitE:
	.space	1
	.space	3
	.type	_ZN4_STLL8_IosInitE, %object
	.size	_ZN4_STLL8_IosInitE, 1
_ZN4_STLL8_IosInitE:
	.space	1
	.space	3
	.type	lastMousePosition, %object
	.size	lastMousePosition, 16
lastMousePosition:
	.space	16
	.type	_ZZ10yieldCyclevE4time, %object
	.size	_ZZ10yieldCyclevE4time, 4
_ZZ10yieldCyclevE4time:
	.space	4
	.hidden	_ZTV7QuTimer
	.hidden	_ZTVN4_STL15basic_streambufIcNS_11char_traitsIcEEEE
	.hidden	_ZTVN4_STL13basic_ostreamIcNS_11char_traitsIcEEEE
	.hidden	_ZTVN4_STL13basic_istreamIcNS_11char_traitsIcEEEE
	.hidden	_ZTVN4_STL15basic_stringbufIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.hidden	_ZTVN4_STL14basic_iostreamIcNS_11char_traitsIcEEEE
	.hidden	_ZTVN4_STL9basic_iosIcNS_11char_traitsIcEEEE
	.hidden	_ZTVN4_STL18basic_stringstreamIcNS_11char_traitsIcEENS_9allocatorIcEEEE
	.hidden	_ZTV21QuRectangleDrawObjectIiE
	.hidden	_ZTV23QuAbsScreenIntBoxButton
	.hidden	_ZTV18InstructionManager
	.hidden	_ZTV13QuBaseManager
	.hidden	_ZTV11QuBoxButton
	.hidden	_ZTV13QuBasicCamera
	.hidden	_ZTV17QuImageDrawObject
	.hidden	_ZTV12QuDrawObject
	.hidden	_ZTV19QuApRawSoundManager
	.hidden	_ZTV23QuSoundManagerInterface
	.hidden	_ZTV18QuApRawSoundStruct
	.hidden	_ZTV22QuSoundStructInterface
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
