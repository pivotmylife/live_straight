	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"zutil.c"
	.section	.text.zlibVersion,"ax",%progbits
	.align	2
	.global	zlibVersion
	.hidden	zlibVersion
	.type	zlibVersion, %function
zlibVersion:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L3
	bx	lr
.L4:
	.align	2
.L3:
	.word	.LC0
	.size	zlibVersion, .-zlibVersion
	.section	.text.zlibCompileFlags,"ax",%progbits
	.align	2
	.global	zlibCompileFlags
	.hidden	zlibCompileFlags
	.type	zlibCompileFlags, %function
zlibCompileFlags:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #85
	bx	lr
	.size	zlibCompileFlags, .-zlibCompileFlags
	.section	.text.zError,"ax",%progbits
	.align	2
	.global	zError
	.hidden	zError
	.type	zError, %function
zError:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L9
	rsb	r0, r0, #2
	ldr	r0, [r3, r0, asl #2]
	bx	lr
.L10:
	.align	2
.L9:
	.word	.LANCHOR0
	.size	zError, .-zError
	.section	.text.zcfree,"ax",%progbits
	.align	2
	.global	zcfree
	.hidden	zcfree
	.type	zcfree, %function
zcfree:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, lr}
	mov	r0, r1
	bl	free
	ldmfd	sp!, {r3, lr}
	bx	lr
	.size	zcfree, .-zcfree
	.section	.text.zcalloc,"ax",%progbits
	.align	2
	.global	zcalloc
	.hidden	zcalloc
	.type	zcalloc, %function
zcalloc:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, lr}
	mul	r0, r1, r2
	bl	malloc
	ldmfd	sp!, {r3, lr}
	bx	lr
	.size	zcalloc, .-zcalloc
	.hidden	z_errmsg
	.global	z_errmsg
	.section	.rodata
	.align	2
	.set	.LANCHOR0,. + 0
	.type	z_errmsg, %object
	.size	z_errmsg, 40
z_errmsg:
	.word	.LC1
	.word	.LC2
	.word	.LC3
	.word	.LC4
	.word	.LC5
	.word	.LC6
	.word	.LC7
	.word	.LC8
	.word	.LC9
	.word	.LC3
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"1.2.3\000"
	.space	2
.LC1:
	.ascii	"need dictionary\000"
.LC2:
	.ascii	"stream end\000"
	.space	1
.LC3:
	.ascii	"\000"
	.space	3
.LC4:
	.ascii	"file error\000"
	.space	1
.LC5:
	.ascii	"stream error\000"
	.space	3
.LC6:
	.ascii	"data error\000"
	.space	1
.LC7:
	.ascii	"insufficient memory\000"
.LC8:
	.ascii	"buffer error\000"
	.space	3
.LC9:
	.ascii	"incompatible version\000"
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
