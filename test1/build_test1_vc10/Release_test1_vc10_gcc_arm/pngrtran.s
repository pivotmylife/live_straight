	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"pngrtran.c"
	.section	.text.png_set_strip_16,"ax",%progbits
	.align	2
	.global	png_set_strip_16
	.hidden	png_set_strip_16
	.type	png_set_strip_16, %function
png_set_strip_16:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	ldrne	r3, [r0, #140]
	orrne	r3, r3, #1024
	strne	r3, [r0, #140]
	bx	lr
	.size	png_set_strip_16, .-png_set_strip_16
	.section	.text.png_set_strip_alpha,"ax",%progbits
	.align	2
	.global	png_set_strip_alpha
	.hidden	png_set_strip_alpha
	.type	png_set_strip_alpha, %function
png_set_strip_alpha:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	ldrne	r3, [r0, #136]
	orrne	r3, r3, #4194304
	strne	r3, [r0, #136]
	bx	lr
	.size	png_set_strip_alpha, .-png_set_strip_alpha
	.global	__aeabi_dmul
	.global	__aeabi_dsub
	.global	__aeabi_dcmpgt
	.global	__aeabi_d2f
	.section	.text.png_set_gamma,"ax",%progbits
	.align	2
	.global	png_set_gamma
	.hidden	png_set_gamma
	.type	png_set_gamma, %function
png_set_gamma:
	@ Function supports interworking.
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	subs	r4, r0, #0
	mov	r5, r2
	mov	r6, r3
	add	r8, sp, #24
	ldmia	r8, {r7-r8}
	beq	.L11
	mov	r2, r7
	mov	r3, r8
	mov	r0, r5
	mov	r1, r6
	bl	__aeabi_dmul
	mov	ip, #1069547520
	add	r3, ip, #3145728
	mov	r2, #0
	bl	__aeabi_dsub
	bic	r3, r1, #-2147483648
	mov	r1, r3
	adr	r3, .L14
	ldmia	r3, {r2-r3}
	bl	__aeabi_dcmpgt
	cmp	r0, #0
	beq	.L13
.L9:
	ldr	r1, [r4, #140]
	orr	r0, r1, #8192
	str	r0, [r4, #140]
.L10:
	mov	r1, r8
	mov	r0, r7
	bl	__aeabi_d2f
	mov	r1, r6
	str	r0, [r4, #376]	@ float
	mov	r0, r5
	bl	__aeabi_d2f
	str	r0, [r4, #380]	@ float
.L11:
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L13:
	ldrb	r3, [r4, #322]	@ zero_extendqisi2
	tst	r3, #4
	bne	.L9
	cmp	r3, #3
	bne	.L10
	b	.L9
.L15:
	.align	3
.L14:
	.word	-1717986918
	.word	1068079513
	.size	png_set_gamma, .-png_set_gamma
	.section	.text.png_set_expand,"ax",%progbits
	.align	2
	.global	png_set_expand
	.hidden	png_set_expand
	.type	png_set_expand, %function
png_set_expand:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	bxeq	lr
	add	r1, r0, #136
	ldmia	r1, {r1, r3}	@ phole ldm
	orr	ip, r3, #33554432
	orr	r3, ip, #4096
	bic	r2, r1, #64
	str	r2, [r0, #136]
	str	r3, [r0, #140]
	bx	lr
	.size	png_set_expand, .-png_set_expand
	.section	.text.png_set_palette_to_rgb,"ax",%progbits
	.align	2
	.global	png_set_palette_to_rgb
	.hidden	png_set_palette_to_rgb
	.type	png_set_palette_to_rgb, %function
png_set_palette_to_rgb:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	bxeq	lr
	add	r1, r0, #136
	ldmia	r1, {r1, r3}	@ phole ldm
	orr	ip, r3, #33554432
	orr	r3, ip, #4096
	bic	r2, r1, #64
	str	r2, [r0, #136]
	str	r3, [r0, #140]
	bx	lr
	.size	png_set_palette_to_rgb, .-png_set_palette_to_rgb
	.section	.text.png_set_expand_gray_1_2_4_to_8,"ax",%progbits
	.align	2
	.global	png_set_expand_gray_1_2_4_to_8
	.hidden	png_set_expand_gray_1_2_4_to_8
	.type	png_set_expand_gray_1_2_4_to_8, %function
png_set_expand_gray_1_2_4_to_8:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	bxeq	lr
	add	r1, r0, #136
	ldmia	r1, {r1, ip}	@ phole ldm
	orr	r3, ip, #4096
	bic	r2, r1, #64
	str	r2, [r0, #136]
	str	r3, [r0, #140]
	bx	lr
	.size	png_set_expand_gray_1_2_4_to_8, .-png_set_expand_gray_1_2_4_to_8
	.section	.text.png_set_gray_1_2_4_to_8,"ax",%progbits
	.align	2
	.global	png_set_gray_1_2_4_to_8
	.hidden	png_set_gray_1_2_4_to_8
	.type	png_set_gray_1_2_4_to_8, %function
png_set_gray_1_2_4_to_8:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	ldrne	r3, [r0, #140]
	orrne	r3, r3, #33554432
	orrne	r3, r3, #4096
	strne	r3, [r0, #140]
	bx	lr
	.size	png_set_gray_1_2_4_to_8, .-png_set_gray_1_2_4_to_8
	.section	.text.png_set_tRNS_to_alpha,"ax",%progbits
	.align	2
	.global	png_set_tRNS_to_alpha
	.hidden	png_set_tRNS_to_alpha
	.type	png_set_tRNS_to_alpha, %function
png_set_tRNS_to_alpha:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	add	r1, r0, #136
	ldmia	r1, {r1, r3}	@ phole ldm
	orr	ip, r3, #33554432
	orr	r3, ip, #4096
	bic	r2, r1, #64
	str	r2, [r0, #136]
	str	r3, [r0, #140]
	bx	lr
	.size	png_set_tRNS_to_alpha, .-png_set_tRNS_to_alpha
	.section	.text.png_set_gray_to_rgb,"ax",%progbits
	.align	2
	.global	png_set_gray_to_rgb
	.hidden	png_set_gray_to_rgb
	.type	png_set_gray_to_rgb, %function
png_set_gray_to_rgb:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	add	r1, r0, #136
	ldmia	r1, {r1, ip}	@ phole ldm
	orr	r3, ip, #16384
	bic	r2, r1, #64
	str	r2, [r0, #136]
	str	r3, [r0, #140]
	bx	lr
	.size	png_set_gray_to_rgb, .-png_set_gray_to_rgb
	.section	.text.png_set_read_user_transform_fn,"ax",%progbits
	.align	2
	.global	png_set_read_user_transform_fn
	.hidden	png_set_read_user_transform_fn
	.type	png_set_read_user_transform_fn, %function
png_set_read_user_transform_fn:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	ldrne	r3, [r0, #140]
	orrne	r3, r3, #1048576
	strne	r1, [r0, #116]
	strne	r3, [r0, #140]
	bx	lr
	.size	png_set_read_user_transform_fn, .-png_set_read_user_transform_fn
	.section	.text.png_read_transform_info,"ax",%progbits
	.align	2
	.global	png_read_transform_info
	.hidden	png_read_transform_info
	.type	png_read_transform_info, %function
png_read_transform_info:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	ldr	r3, [r0, #140]
	tst	r3, #4096
	mov	r5, r0
	mov	r4, r1
	beq	.L68
	ldrb	r2, [r1, #25]	@ zero_extendqisi2
	cmp	r2, #3
	beq	.L69
	mov	ip, #308
	add	r1, ip, #2
	ldrh	r0, [r0, r1]
	cmp	r0, #0
	beq	.L40
	tst	r3, #33554432
	orrne	r2, r2, #4
	strneb	r2, [r4, #25]
	ldrneb	r2, [r4, #25]	@ zero_extendqisi2
.L40:
	ldrb	r0, [r4, #24]	@ zero_extendqisi2
	cmp	r0, #7
	mov	r3, #0	@ movhi
	movls	r0, #8
	strlsb	r0, [r4, #24]
	strh	r3, [r4, #22]	@ movhi
	ldr	r3, [r5, #140]
	b	.L36
.L69:
	mov	r1, #308
	add	r0, r1, #2
	ldrh	r2, [r5, r0]
	cmp	r2, #0
	beq	.L38
	tst	r3, #33554432
	movne	r3, #6
	strneb	r3, [r4, #25]
	bne	.L39
.L38:
	mov	r3, #2
	strb	r3, [r4, #25]
.L39:
	mov	r2, #8
	mov	ip, #0	@ movhi
	strb	r2, [r4, #24]
	strh	ip, [r4, #22]	@ movhi
	ldr	r3, [r5, #140]
.L68:
	ldrb	r2, [r4, #25]	@ zero_extendqisi2
.L36:
	tst	r3, #128
	beq	.L42
	bic	r2, r2, #4
	mov	lr, #0	@ movhi
	strb	r2, [r4, #25]
	strh	lr, [r4, #22]	@ movhi
	mov	r2, #10
	add	r0, r4, #90
	add	r1, r5, #340
	bl	memcpy
	ldr	r3, [r5, #140]
	ldrb	r2, [r4, #25]	@ zero_extendqisi2
.L42:
	tst	r3, #8192
	ldrne	r3, [r5, #376]	@ float
	strne	r3, [r4, #40]	@ float
	ldrne	r3, [r5, #592]
	strne	r3, [r4, #252]
	ldrne	r3, [r5, #140]
	tst	r3, #1024
	ldrb	r1, [r4, #24]	@ zero_extendqisi2
	beq	.L44
	cmp	r1, #16
	moveq	r1, #8
	streqb	r1, [r4, #24]
	ldreq	r3, [r5, #140]
.L44:
	tst	r3, #16384
	orrne	r2, r2, #2
	strneb	r2, [r4, #25]
	ldrne	r3, [r5, #140]
	ldrneb	r2, [r4, #25]	@ zero_extendqisi2
	tst	r3, #6291456
	bicne	r2, r2, #2
	strneb	r2, [r4, #25]
	ldrne	r3, [r5, #140]
	ldrneb	r2, [r4, #25]	@ zero_extendqisi2
	tst	r3, #64
	beq	.L47
	cmp	r2, #2
	cmpne	r2, #6
	beq	.L70
.L47:
	tst	r3, #4
	beq	.L48
	cmp	r1, #7
	movls	r1, #8
	strlsb	r1, [r4, #24]
.L48:
	cmp	r2, #3
	beq	.L51
	tst	r2, #2
	movne	r3, #3
	strneb	r3, [r4, #29]
	beq	.L51
.L50:
	ldr	r0, [r5, #136]
	tst	r0, #4194304
	andne	r2, r2, #251
	strneb	r2, [r4, #25]
	tst	r2, #4
	ldrneb	r0, [r4, #29]	@ zero_extendqisi2
	addne	r0, r0, #1
	strneb	r0, [r4, #29]
	ldr	r3, [r5, #140]
	tst	r3, #32768
	beq	.L55
	ldrb	r2, [r4, #25]	@ zero_extendqisi2
	cmp	r2, #2
	cmpne	r2, #0
	beq	.L71
.L55:
	tst	r3, #1048576
	bne	.L56
	ldrb	r2, [r4, #29]	@ zero_extendqisi2
	mov	r3, r1
.L57:
	mul	r3, r2, r3
	ldr	r2, [r4, #0]
	and	r0, r3, #255
	cmp	r0, #7
	strb	r0, [r4, #30]
	mulls	r0, r2, r0
	movhi	r0, r0, lsr #3
	mulhi	r0, r2, r0
	addls	r0, r0, #7
	movls	r0, r0, lsr #3
	str	r0, [r4, #12]
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L51:
	mov	r3, #1
	strb	r3, [r4, #29]
	b	.L50
.L56:
	ldrb	r3, [r5, #128]	@ zero_extendqisi2
	cmp	r1, r3
	strccb	r3, [r4, #24]
	movcs	r3, r1
	ldrb	r2, [r5, #129]	@ zero_extendqisi2
	ldrb	r1, [r4, #29]	@ zero_extendqisi2
	cmp	r1, r2
	movcs	r2, r1
	strccb	r2, [r4, #29]
	ldrccb	r3, [r4, #24]	@ zero_extendqisi2
	b	.L57
.L71:
	ldrb	r3, [r4, #29]	@ zero_extendqisi2
	add	ip, r3, #1
	strb	ip, [r4, #29]
	ldr	r3, [r5, #140]
	tst	r3, #16777216
	orrne	r2, r2, #4
	strneb	r2, [r4, #25]
	ldrne	r3, [r5, #140]
	b	.L55
.L70:
	ldr	ip, [r5, #520]
	cmp	ip, #0
	beq	.L47
	cmp	r1, #8
	moveq	r2, #3
	streqb	r2, [r4, #25]
	ldreq	r3, [r5, #140]
	b	.L47
	.size	png_read_transform_info, .-png_read_transform_info
	.section	.text.png_do_unpack,"ax",%progbits
	.align	2
	.global	png_do_unpack
	.hidden	png_do_unpack
	.type	png_do_unpack, %function
png_do_unpack:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldrb	r3, [r0, #9]	@ zero_extendqisi2
	cmp	r3, #7
	stmfd	sp!, {r4, r5, r6}
	bhi	.L89
	cmp	r3, #2
	ldr	r2, [r0, #0]
	beq	.L76
	cmp	r3, #4
	beq	.L77
	cmp	r3, #1
	beq	.L195
.L74:
	ldrb	r1, [r0, #10]	@ zero_extendqisi2
	mul	r2, r1, r2
	mov	ip, r1, asl #3
	str	r2, [r0, #4]
	mov	r2, #8
	strb	r2, [r0, #9]
	strb	ip, [r0, #11]
.L89:
	ldmfd	sp!, {r4, r5, r6}
	bx	lr
.L76:
	cmp	r2, #0
	beq	.L74
	sub	r5, r2, #1
	add	r3, r2, #3
	mvn	r6, r3
	add	r3, r1, r5, lsr #2
	ldrb	r4, [r3, #0]	@ zero_extendqisi2
	and	ip, r6, #3
	mov	ip, ip, asl #1
	mov	r6, r4, asr ip
	cmp	ip, #6
	add	r4, r1, r5
	and	r1, r6, #3
	strb	r1, [r4, #0]
	and	r5, r5, #3
	addne	ip, ip, #2
	beq	.L196
.L159:
	mov	r1, #1
	cmp	r2, r1
	bls	.L74
	cmp	r5, #0
	beq	.L83
	cmp	r5, r1
	beq	.L145
	cmp	r5, #2
	beq	.L146
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	mov	r5, r6, asr ip
	and	r6, r5, #3
	cmp	ip, #6
	strb	r6, [r4, #-1]
	addne	ip, ip, #2
	subeq	r3, r3, #1
	moveq	ip, #0
	add	r1, r1, #1
.L146:
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	mov	r5, r6, asr ip
	and	r6, r5, #3
	rsb	r5, r1, #0
	cmp	ip, #6
	strb	r6, [r4, r5]
	addne	ip, ip, #2
	subeq	r3, r3, #1
	moveq	ip, #0
	add	r1, r1, #1
.L145:
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	mov	r5, r6, asr ip
	cmp	ip, #6
	and	r6, r5, #3
	rsb	r5, r1, #0
	add	r1, r1, #1
	addne	ip, ip, #2
	subeq	r3, r3, #1
	moveq	ip, #0
	cmp	r2, r1
	strb	r6, [r4, r5]
	bhi	.L83
	b	.L74
.L166:
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	mov	r5, r6, asr ip
	and	r6, r5, #3
	mvn	r5, r1
	cmp	ip, #6
	strb	r6, [r4, r5]
	subeq	r3, r3, #1
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	addne	ip, ip, #2
	moveq	ip, #0
	mov	r5, r6, asr ip
	cmp	ip, #6
	and	r6, r5, #3
	rsb	r5, r1, r4
	add	r1, r1, #3
	addne	ip, ip, #2
	subeq	r3, r3, #1
	moveq	ip, #0
	cmp	r2, r1
	strb	r6, [r5, #-2]
	bls	.L74
.L83:
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	mov	r5, r6, asr ip
	and	r6, r5, #3
	rsb	r5, r1, #0
	cmp	ip, #6
	strb	r6, [r4, r5]
	subeq	r3, r3, #1
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	addne	ip, ip, #2
	moveq	ip, #0
	mov	r5, r6, asr ip
	add	r1, r1, #1
	and	r6, r5, #3
	cmp	ip, #6
	rsb	r5, r1, #0
	strb	r6, [r4, r5]
	addne	ip, ip, #2
	subeq	r3, r3, #1
	moveq	ip, #0
	b	.L166
.L195:
	cmp	r2, #0
	beq	.L74
	sub	r5, r2, #1
	add	r3, r1, r5, lsr #3
	add	r4, r2, #7
	mvn	ip, r4
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	and	ip, ip, #7
	mov	r6, r6, asr ip
	add	r4, r1, r5
	cmp	ip, #7
	and	r1, r6, #1
	strb	r1, [r4, #0]
	and	r5, r5, #3
	addne	ip, ip, #1
	beq	.L197
.L183:
	mov	r1, #1
	cmp	r2, r1
	bls	.L74
	cmp	r5, #0
	beq	.L80
	cmp	r5, r1
	beq	.L151
	cmp	r5, #2
	beq	.L152
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	mov	r5, r6, asr ip
	and	r6, r5, r1
	cmp	ip, #7
	strb	r6, [r4, #-1]
	addne	ip, ip, r1
	subeq	r3, r3, #1
	moveq	ip, #0
	add	r1, r1, #1
.L152:
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	mov	r5, r6, asr ip
	and	r6, r5, #1
	rsb	r5, r1, #0
	cmp	ip, #7
	strb	r6, [r4, r5]
	addne	ip, ip, #1
	subeq	r3, r3, #1
	moveq	ip, #0
	add	r1, r1, #1
.L151:
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	mov	r5, r6, asr ip
	cmp	ip, #7
	and	r6, r5, #1
	rsb	r5, r1, #0
	add	r1, r1, #1
	addne	ip, ip, #1
	subeq	r3, r3, #1
	moveq	ip, #0
	cmp	r2, r1
	strb	r6, [r4, r5]
	bhi	.L80
	b	.L74
.L190:
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	mov	r5, r6, asr ip
	and	r6, r5, #1
	mvn	r5, r1
	cmp	ip, #7
	strb	r6, [r4, r5]
	subeq	r3, r3, #1
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	addne	ip, ip, #1
	moveq	ip, #0
	mov	r5, r6, asr ip
	cmp	ip, #7
	and	r6, r5, #1
	rsb	r5, r1, r4
	add	r1, r1, #3
	addne	ip, ip, #1
	subeq	r3, r3, #1
	moveq	ip, #0
	cmp	r2, r1
	strb	r6, [r5, #-2]
	bls	.L74
.L80:
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	mov	r5, r6, asr ip
	and	r6, r5, #1
	rsb	r5, r1, #0
	cmp	ip, #7
	strb	r6, [r4, r5]
	subeq	r3, r3, #1
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	addne	ip, ip, #1
	moveq	ip, #0
	mov	r5, r6, asr ip
	add	r1, r1, #1
	and	r6, r5, #1
	cmp	ip, #7
	rsb	r5, r1, #0
	strb	r6, [r4, r5]
	addne	ip, ip, #1
	subeq	r3, r3, #1
	moveq	ip, #0
	b	.L190
.L77:
	tst	r2, #1
	movne	r4, #4
	moveq	r4, #0
	cmp	r2, #0
	beq	.L74
	sub	r5, r2, #1
	add	ip, r1, r5, lsr #1
	ldrb	r6, [ip, #0]	@ zero_extendqisi2
	mov	r6, r6, asr r4
	cmp	r4, #4
	add	r4, r1, r5
	and	r1, r6, #15
	strb	r1, [r4, #0]
	and	r5, r5, #3
	beq	.L198
.L171:
	mov	r1, #1
	cmp	r2, r1
	bls	.L74
	cmp	r5, #0
	beq	.L88
	cmp	r5, r1
	beq	.L148
	cmp	r5, #2
	beq	.L149
	ldrb	r5, [ip, #0]	@ zero_extendqisi2
	mov	r5, r5, asr r3
	cmp	r3, #4
	and	r3, r5, #15
	strb	r3, [r4, #-1]
	subeq	ip, ip, #1
	moveq	r3, #0
	movne	r3, #4
	add	r1, r1, #1
.L149:
	ldrb	r5, [ip, #0]	@ zero_extendqisi2
	mov	r5, r5, asr r3
	cmp	r3, #4
	and	r5, r5, #15
	rsb	r3, r1, #0
	strb	r5, [r4, r3]
	subeq	ip, ip, #1
	moveq	r3, #0
	movne	r3, #4
	add	r1, r1, #1
.L148:
	ldrb	r5, [ip, #0]	@ zero_extendqisi2
	mov	r5, r5, asr r3
	cmp	r3, #4
	and	r5, r5, #15
	rsb	r3, r1, #0
	add	r1, r1, #1
	strb	r5, [r4, r3]
	subeq	ip, ip, #1
	moveq	r3, #0
	movne	r3, #4
	cmp	r2, r1
	bhi	.L88
	b	.L74
.L178:
	ldrb	r5, [ip, #0]	@ zero_extendqisi2
	mov	r5, r5, asr r3
	cmp	r3, #4
	and	r5, r5, #15
	mvn	r3, r1
	strb	r5, [r4, r3]
	subeq	ip, ip, #1
	ldrb	r5, [ip, #0]	@ zero_extendqisi2
	moveq	r3, #0
	movne	r3, #4
	mov	r5, r5, asr r3
	cmp	r3, #4
	and	r5, r5, #15
	rsb	r3, r1, r4
	add	r1, r1, #3
	strb	r5, [r3, #-2]
	subeq	ip, ip, #1
	moveq	r3, #0
	movne	r3, #4
	cmp	r2, r1
	bls	.L74
.L88:
	ldrb	r5, [ip, #0]	@ zero_extendqisi2
	mov	r5, r5, asr r3
	cmp	r3, #4
	and	r5, r5, #15
	rsb	r3, r1, #0
	strb	r5, [r4, r3]
	subeq	ip, ip, #1
	ldrb	r5, [ip, #0]	@ zero_extendqisi2
	movne	r3, #4
	moveq	r3, #0
	mov	r5, r5, asr r3
	add	r1, r1, #1
	cmp	r3, #4
	and	r5, r5, #15
	rsb	r3, r1, #0
	strb	r5, [r4, r3]
	movne	r3, #4
	subeq	ip, ip, #1
	moveq	r3, #0
	b	.L178
.L196:
	sub	r3, r3, #1
	mov	ip, #0
	b	.L159
.L198:
	sub	ip, ip, #1
	mov	r3, #0
	b	.L171
.L197:
	sub	r3, r3, #1
	mov	ip, #0
	b	.L183
	.size	png_do_unpack, .-png_do_unpack
	.global	__aeabi_uidivmod
	.section	.text.png_do_unshift,"ax",%progbits
	.align	2
	.global	png_do_unshift
	.hidden	png_do_unshift
	.type	png_do_unshift, %function
png_do_unshift:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	ldrb	r3, [r0, #8]	@ zero_extendqisi2
	cmp	r3, #3
	sub	sp, sp, #16
	mov	r4, r1
	beq	.L217
	tst	r3, #2
	ldr	sl, [r0, #0]
	ldrb	ip, [r0, #9]	@ zero_extendqisi2
	bne	.L302
	ldrb	r7, [r2, #3]	@ zero_extendqisi2
	mov	r1, #1
	rsb	r6, r7, ip
	str	r6, [sp, #0]
	mov	r5, #2
	mov	r6, r1
.L202:
	tst	r3, #4
	ldrneb	r2, [r2, #4]	@ zero_extendqisi2
	addne	r6, sp, #16
	addne	r3, r6, r1, asl #2
	rsbne	r2, r2, ip
	strne	r2, [r3, #-16]
	addne	r6, r1, #1
	add	r2, sp, #16
	ldr	r8, [r2, #-16]!
	mov	r6, r6, asl #2
	moveq	r5, r1
	sub	r1, r6, #4
	cmp	r8, #0
	mov	r3, r1, lsr #2
	mov	r1, #0
	mov	r8, r1
	and	r7, r3, #3
	movgt	r1, #1
	ble	.L303
.L289:
	mov	r3, #4
	cmp	r3, r6
	beq	.L290
	cmp	r7, #0
	beq	.L207
	cmp	r7, #1
	beq	.L285
	cmp	r7, #2
	beq	.L286
	ldr	r7, [r2, r3]
	cmp	r7, #0
	strle	r8, [r2, r3]
	movgt	r1, #1
	add	r3, r3, #4
.L286:
	ldr	r7, [r2, r3]
	cmp	r7, #0
	strle	r8, [r2, r3]
	movgt	r1, #1
	add	r3, r3, #4
.L285:
	ldr	r7, [r2, r3]
	cmp	r7, #0
	strle	r8, [r2, r3]
	add	r3, r3, #4
	movgt	r1, #1
	cmp	r3, r6
	beq	.L290
.L207:
	ldr	r7, [r2, r3]
	cmp	r7, #0
	strle	r8, [r2, r3]
	add	r3, r3, #4
	ldr	r9, [r2, r3]
	movgt	r1, #1
	cmp	r9, #0
	movgt	r1, #1
	ble	.L304
.L297:
	add	r7, r3, #4
	ldr	r9, [r2, r7]
	cmp	r9, #0
	strle	r8, [r2, r7]
	add	r7, r3, #8
	ldr	r9, [r2, r7]
	movgt	r1, #1
	add	r3, r3, #12
	cmp	r9, #0
	strle	r8, [r2, r7]
	movgt	r1, #1
	cmp	r3, r6
	bne	.L207
.L290:
	cmp	r1, #0
	beq	.L217
	sub	ip, ip, #2
	cmp	ip, #14
	ldrls	pc, [pc, ip, asl #2]
	b	.L217
.L212:
	.word	.L208
	.word	.L217
	.word	.L209
	.word	.L217
	.word	.L217
	.word	.L217
	.word	.L210
	.word	.L217
	.word	.L217
	.word	.L217
	.word	.L217
	.word	.L217
	.word	.L217
	.word	.L217
	.word	.L211
.L211:
	muls	sl, r5, sl
	beq	.L217
	ldrb	ip, [r4, #0]	@ zero_extendqisi2
	ldrb	r1, [r4, #1]	@ zero_extendqisi2
	add	r2, r1, ip, asl #8
	mov	r0, r2, asl #16
	ldr	r3, [sp, #0]
	mov	r6, r0, lsr #16
	mov	r1, r6, asr r3
	mov	ip, r1, asl #16
	mov	r2, ip, lsr #24
	sub	r6, sl, #1
	mov	r0, ip, lsr #16
	cmp	sl, #1
	strb	r2, [r4, #0]
	strb	r0, [r4, #1]
	and	r2, r6, #1
	add	r4, r4, #2
	mov	r6, #1
	bls	.L217
	cmp	r2, #0
	beq	.L216
	ldrb	r2, [r4, #0]	@ zero_extendqisi2
	ldrb	ip, [r4, #1]	@ zero_extendqisi2
	add	r1, ip, r2, asl #8
	mov	lr, r1, asl #16
	mov	r0, r6
	mov	r1, r5
	mov	r6, lr, lsr #16
	bl	__aeabi_uidivmod
	add	r3, sp, #16
	add	r0, r3, r1, asl #2
	ldr	ip, [r0, #-16]
	mov	r2, r6, asr ip
	mov	r6, #2
	mov	r3, r2, asl #16
	mov	r1, r3, lsr #24
	mov	r0, r3, lsr #16
	cmp	sl, r6
	strb	r1, [r4, #0]
	strb	r0, [r4, #1]
	add	r4, r4, r6
	bls	.L217
.L216:
	ldrb	r3, [r4, #1]	@ zero_extendqisi2
	ldrb	r8, [r4, #0]	@ zero_extendqisi2
	add	ip, r3, r8, asl #8
	mov	lr, ip, asl #16
	mov	r0, r6
	mov	r1, r5
	mov	r7, lr, lsr #16
	bl	__aeabi_uidivmod
	add	r2, sp, #16
	add	r1, r2, r1, asl #2
	ldr	r0, [r1, #-16]
	mov	r8, r7, asr r0
	mov	r3, r8, asl #16
	mov	ip, r3, lsr #24
	mov	r7, r3, lsr #16
	strb	ip, [r4, #0]
	strb	r7, [r4, #1]
	add	r7, r4, #2
	ldrb	r0, [r4, #2]	@ zero_extendqisi2
	ldrb	r2, [r7, #1]	@ zero_extendqisi2
	add	r6, r6, #1
	add	r1, r2, r0, asl #8
	mov	r8, r1, asl #16
	mov	r0, r6
	mov	r1, r5
	bl	__aeabi_uidivmod
	add	r3, sp, #16
	add	ip, r3, r1, asl #2
	ldr	r2, [ip, #-16]
	mov	r8, r8, lsr #16
	mov	r0, r8, asr r2
	add	r6, r6, #1
	mov	r8, r0, asl #16
	mov	r3, r8, lsr #24
	cmp	sl, r6
	mov	r8, r8, lsr #16
	strb	r3, [r4, #2]
	strb	r8, [r7, #1]
	add	r4, r7, #2
	bhi	.L216
.L217:
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L302:
	ldrb	r5, [r2, #0]	@ zero_extendqisi2
	ldrb	r1, [r2, #2]	@ zero_extendqisi2
	ldrb	r8, [r2, #1]	@ zero_extendqisi2
	rsb	r7, r5, ip
	rsb	r6, r8, ip
	rsb	r5, r1, ip
	mov	r1, #3
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	str	r7, [sp, #0]
	mov	r5, #4
	mov	r6, r1
	b	.L202
.L210:
	muls	sl, r5, sl
	beq	.L217
	ldrb	r1, [r4, #0]	@ zero_extendqisi2
	ldr	ip, [sp, #0]
	mov	r2, r1, asr ip
	sub	r6, sl, #1
	cmp	sl, #1
	and	r3, r6, #3
	strb	r2, [r4, #0]
	mov	r6, #1
	bls	.L217
	cmp	r3, #0
	beq	.L215
	cmp	r3, #1
	beq	.L282
	cmp	r3, #2
	beq	.L283
	mov	r0, r6
	mov	r1, r5
	bl	__aeabi_uidivmod
	add	ip, sp, #16
	add	r2, ip, r1, asl #2
	ldr	r3, [r2, #-16]
	ldrb	r1, [r4, #1]	@ zero_extendqisi2
	mov	r6, r1, asr r3
	strb	r6, [r4, #1]
	mov	r6, #2
.L283:
	mov	r0, r6
	mov	r1, r5
	bl	__aeabi_uidivmod
	add	ip, sp, #16
	add	r2, ip, r1, asl #2
	ldr	r3, [r2, #-16]
	ldrb	r1, [r4, r6]	@ zero_extendqisi2
	mov	r0, r1, asr r3
	strb	r0, [r4, r6]
	add	r6, r6, #1
.L282:
	mov	r0, r6
	mov	r1, r5
	bl	__aeabi_uidivmod
	add	ip, sp, #16
	add	r2, ip, r1, asl #2
	ldr	r3, [r2, #-16]
	ldrb	r1, [r4, r6]	@ zero_extendqisi2
	mov	r0, r1, asr r3
	strb	r0, [r4, r6]
	add	r6, r6, #1
	cmp	sl, r6
	bls	.L217
.L215:
	mov	r0, r6
	mov	r1, r5
	bl	__aeabi_uidivmod
	add	r3, sp, #16
	add	r8, r3, r1, asl #2
	ldr	r7, [r8, #-16]
	ldrb	r1, [r4, r6]	@ zero_extendqisi2
	mov	ip, r1, asr r7
	add	r7, r6, #1
	strb	ip, [r4, r6]
	mov	r0, r7
	mov	r1, r5
	bl	__aeabi_uidivmod
	add	r2, sp, #16
	add	r0, r2, r1, asl #2
	ldrb	r3, [r4, r7]	@ zero_extendqisi2
	ldr	r8, [r0, #-16]
	mov	r1, r3, asr r8
	add	r8, r7, #1
	strb	r1, [r4, r7]
	mov	r0, r8
	mov	r1, r5
	bl	__aeabi_uidivmod
	add	ip, sp, #16
	add	r2, ip, r1, asl #2
	ldr	r7, [r2, #-16]
	ldrb	r0, [r4, r8]	@ zero_extendqisi2
	mov	r3, r0, asr r7
	add	r7, r6, #3
	strb	r3, [r4, r8]
	mov	r0, r7
	mov	r1, r5
	bl	__aeabi_uidivmod
	add	ip, sp, #16
	add	r1, ip, r1, asl #2
	ldr	r0, [r1, #-16]
	ldrb	r2, [r4, r7]	@ zero_extendqisi2
	mov	r3, r2, asr r0
	add	r6, r6, #4
	cmp	sl, r6
	strb	r3, [r4, r7]
	bhi	.L215
	b	.L217
.L209:
	ldr	r2, [sp, #0]
	mov	ip, #240
	mov	r1, ip, asr r2
	mov	ip, #15
	bic	r3, r1, #15
	orr	r1, r3, ip, asr r2
	ldr	ip, [r0, #4]
	cmp	ip, #0
	and	r1, r1, #255
	beq	.L217
	ldrb	r0, [r4, #0]	@ zero_extendqisi2
	and	r3, r1, r0, asr r2
	cmp	ip, #1
	sub	r0, ip, #1
	strb	r3, [r4, #0]
	and	r0, r0, #3
	mov	r3, #1
	bls	.L217
	cmp	r0, #0
	beq	.L214
	cmp	r0, #1
	beq	.L280
	cmp	r0, #2
	ldrneb	r3, [r4, #1]	@ zero_extendqisi2
	andne	r3, r1, r3, asr r2
	strneb	r3, [r4, #1]
	movne	r3, #2
	ldrb	r0, [r4, r3]	@ zero_extendqisi2
	and	r0, r1, r0, asr r2
	strb	r0, [r4, r3]
	add	r3, r3, #1
.L280:
	ldrb	r0, [r4, r3]	@ zero_extendqisi2
	and	r0, r1, r0, asr r2
	strb	r0, [r4, r3]
	add	r3, r3, #1
	cmp	ip, r3
	bls	.L217
.L214:
	ldrb	r5, [r4, r3]	@ zero_extendqisi2
	and	r0, r1, r5, asr r2
	strb	r0, [r4, r3]
	add	r0, r3, #1
	ldrb	r5, [r4, r0]	@ zero_extendqisi2
	and	r5, r1, r5, asr r2
	strb	r5, [r4, r0]
	add	r0, r0, #1
	ldrb	r5, [r4, r0]	@ zero_extendqisi2
	and	r5, r1, r5, asr r2
	strb	r5, [r4, r0]
	add	r0, r3, #3
	ldrb	r5, [r4, r0]	@ zero_extendqisi2
	and	r5, r1, r5, asr r2
	add	r3, r3, #4
	cmp	ip, r3
	strb	r5, [r4, r0]
	bhi	.L214
	b	.L217
.L208:
	ldr	r1, [r0, #4]
	cmp	r1, #0
	beq	.L217
	ldrb	ip, [r4, #0]	@ zero_extendqisi2
	mov	r3, ip, lsr #1
	and	r0, r3, #85
	sub	r2, r1, #1
	cmp	r1, #1
	strb	r0, [r4, #0]
	and	r2, r2, #3
	mov	r3, #1
	bls	.L217
	cmp	r2, #0
	beq	.L213
	cmp	r2, #1
	beq	.L278
	cmp	r2, #2
	ldrneb	r3, [r4, #1]	@ zero_extendqisi2
	movne	r3, r3, lsr #1
	andne	r3, r3, #85
	strneb	r3, [r4, #1]
	movne	r3, #2
	ldrb	ip, [r4, r3]	@ zero_extendqisi2
	mov	r2, ip, lsr #1
	and	r0, r2, #85
	strb	r0, [r4, r3]
	add	r3, r3, #1
.L278:
	ldrb	ip, [r4, r3]	@ zero_extendqisi2
	mov	r2, ip, lsr #1
	and	r0, r2, #85
	strb	r0, [r4, r3]
	add	r3, r3, #1
	cmp	r1, r3
	bls	.L217
.L213:
	ldrb	r0, [r4, r3]	@ zero_extendqisi2
	mov	ip, r0, lsr #1
	and	r2, ip, #85
	strb	r2, [r4, r3]
	add	r2, r3, #1
	ldrb	r0, [r4, r2]	@ zero_extendqisi2
	mov	ip, r0, lsr #1
	and	r0, ip, #85
	strb	r0, [r4, r2]
	add	r2, r2, #1
	ldrb	ip, [r4, r2]	@ zero_extendqisi2
	mov	r0, ip, lsr #1
	and	ip, r0, #85
	strb	ip, [r4, r2]
	add	r2, r3, #3
	ldrb	r0, [r4, r2]	@ zero_extendqisi2
	add	r3, r3, #4
	mov	ip, r0, lsr #1
	and	r0, ip, #85
	cmp	r1, r3
	strb	r0, [r4, r2]
	bhi	.L213
	b	.L217
.L303:
	str	r1, [r2, #0]
	b	.L289
.L304:
	str	r8, [r2, r3]
	b	.L297
	.size	png_do_unshift, .-png_do_unshift
	.section	.text.png_do_chop,"ax",%progbits
	.align	2
	.global	png_do_chop
	.hidden	png_do_chop
	.type	png_do_chop, %function
png_do_chop:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldrb	r3, [r0, #9]	@ zero_extendqisi2
	cmp	r3, #16
	str	r4, [sp, #-4]!
	beq	.L325
.L309:
	ldmfd	sp!, {r4}
	bx	lr
.L325:
	ldrb	r3, [r0, #10]	@ zero_extendqisi2
	ldr	r2, [r0, #0]
	muls	ip, r2, r3
	beq	.L307
	sub	r2, ip, #1
	cmp	ip, #1
	and	r2, r2, #3
	mov	r3, #1
	bls	.L324
	cmp	r2, #0
	beq	.L308
	cmp	r2, #1
	beq	.L322
	cmp	r2, #2
	movne	r3, #2
	ldrneb	r2, [r1, r3]	@ zero_extendqisi2
	strneb	r2, [r1, #1]
	ldrb	r2, [r1, r3, asl #1]	@ zero_extendqisi2
	strb	r2, [r1, r3]
	add	r3, r3, #1
.L322:
	ldrb	r2, [r1, r3, asl #1]	@ zero_extendqisi2
	strb	r2, [r1, r3]
	add	r3, r3, #1
	cmp	ip, r3
	bls	.L324
.L308:
	ldrb	r2, [r1, r3, asl #1]	@ zero_extendqisi2
	strb	r2, [r1, r3]
	add	r2, r3, #1
	ldrb	r4, [r1, r2, asl #1]	@ zero_extendqisi2
	strb	r4, [r1, r2]
	add	r2, r2, #1
	ldrb	r4, [r1, r2, asl #1]	@ zero_extendqisi2
	strb	r4, [r1, r2]
	add	r2, r3, #3
	ldrb	r4, [r1, r2, asl #1]	@ zero_extendqisi2
	add	r3, r3, #4
	cmp	ip, r3
	strb	r4, [r1, r2]
	bhi	.L308
.L324:
	ldrb	r3, [r0, #10]	@ zero_extendqisi2
	ldr	r2, [r0, #0]
.L307:
	mul	r2, r3, r2
	mov	r1, r3, asl #3
	mov	ip, #8
	str	r2, [r0, #4]
	strb	ip, [r0, #9]
	strb	r1, [r0, #11]
	b	.L309
	.size	png_do_chop, .-png_do_chop
	.section	.text.png_do_read_swap_alpha,"ax",%progbits
	.align	2
	.global	png_do_read_swap_alpha
	.hidden	png_do_read_swap_alpha
	.type	png_do_read_swap_alpha, %function
png_do_read_swap_alpha:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
	ldrb	r3, [r0, #8]	@ zero_extendqisi2
	cmp	r3, #6
	ldr	ip, [r0, #0]
	beq	.L392
	cmp	r3, #4
	beq	.L393
.L335:
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
	bx	lr
.L393:
	ldrb	r3, [r0, #9]	@ zero_extendqisi2
	cmp	r3, #8
	beq	.L394
	cmp	ip, #0
	ldr	r3, [r0, #4]
	beq	.L335
	add	r1, r1, r3
	ldrb	r4, [r1, #-1]	@ zero_extendqisi2
	ldrb	r6, [r1, #-3]	@ zero_extendqisi2
	ldrb	r0, [r1, #-2]	@ zero_extendqisi2
	ldrb	r5, [r1, #-4]	@ zero_extendqisi2
	mov	r3, r1
	sub	r2, ip, #1
	cmp	ip, #1
	strb	r6, [r1, #-1]
	strb	r5, [r1, #-2]
	strb	r4, [r1, #-3]
	and	r2, r2, #3
	strb	r0, [r3, #-4]!
	mov	r0, #1
	bls	.L335
	cmp	r2, #0
	beq	.L334
	cmp	r2, #1
	beq	.L390
	cmp	r2, #2
	beq	.L391
	ldrb	r2, [r3, #-1]	@ zero_extendqisi2
	ldrb	r4, [r3, #-3]	@ zero_extendqisi2
	ldrb	r0, [r3, #-4]	@ zero_extendqisi2
	ldrb	r5, [r3, #-2]	@ zero_extendqisi2
	strb	r4, [r3, #-1]
	strb	r0, [r3, #-2]
	strb	r2, [r3, #-3]
	strb	r5, [r3, #-4]!
	mov	r0, #2
.L391:
	ldrb	r1, [r3, #-1]	@ zero_extendqisi2
	ldrb	r5, [r3, #-3]	@ zero_extendqisi2
	ldrb	r2, [r3, #-2]	@ zero_extendqisi2
	ldrb	r4, [r3, #-4]	@ zero_extendqisi2
	strb	r5, [r3, #-1]
	strb	r4, [r3, #-2]
	strb	r1, [r3, #-3]
	strb	r2, [r3, #-4]!
	add	r0, r0, #1
.L390:
	ldrb	r1, [r3, #-1]	@ zero_extendqisi2
	ldrb	r5, [r3, #-3]	@ zero_extendqisi2
	ldrb	r2, [r3, #-2]	@ zero_extendqisi2
	ldrb	r4, [r3, #-4]	@ zero_extendqisi2
	add	r0, r0, #1
	cmp	ip, r0
	strb	r5, [r3, #-1]
	strb	r4, [r3, #-2]
	strb	r1, [r3, #-3]
	strb	r2, [r3, #-4]!
	bls	.L335
.L334:
	ldrb	r2, [r3, #-1]	@ zero_extendqisi2
	ldrb	r5, [r3, #-3]	@ zero_extendqisi2
	ldrb	r7, [r3, #-2]	@ zero_extendqisi2
	ldrb	r4, [r3, #-4]	@ zero_extendqisi2
	mov	r1, r3
	strb	r2, [r3, #-3]
	strb	r5, [r3, #-1]
	strb	r4, [r3, #-2]
	strb	r7, [r1, #-4]!
	ldrb	r5, [r1, #-1]	@ zero_extendqisi2
	ldrb	r7, [r1, #-3]	@ zero_extendqisi2
	ldrb	r4, [r1, #-2]	@ zero_extendqisi2
	ldrb	r6, [r1, #-4]	@ zero_extendqisi2
	mov	r2, r1
	strb	r7, [r1, #-1]
	strb	r6, [r1, #-2]
	strb	r5, [r1, #-3]
	strb	r4, [r2, #-4]!
	ldrb	r1, [r2, #-1]	@ zero_extendqisi2
	ldrb	r6, [r2, #-2]	@ zero_extendqisi2
	ldrb	r5, [r2, #-3]	@ zero_extendqisi2
	ldrb	r4, [r2, #-4]	@ zero_extendqisi2
	strb	r5, [r2, #-1]
	strb	r6, [r2, #-4]
	strb	r4, [r2, #-2]
	strb	r1, [r2, #-3]
	sub	r2, r3, #12
	ldrb	r1, [r2, #-1]	@ zero_extendqisi2
	ldrb	r6, [r2, #-2]	@ zero_extendqisi2
	ldrb	r5, [r2, #-3]	@ zero_extendqisi2
	ldrb	r4, [r2, #-4]	@ zero_extendqisi2
	add	r0, r0, #4
	cmp	ip, r0
	strb	r6, [r2, #-4]
	strb	r5, [r2, #-1]
	strb	r4, [r2, #-2]
	strb	r1, [r2, #-3]
	sub	r3, r3, #16
	bhi	.L334
	b	.L335
.L392:
	ldrb	r3, [r0, #9]	@ zero_extendqisi2
	cmp	r3, #8
	beq	.L395
	cmp	ip, #0
	ldr	r3, [r0, #4]
	beq	.L335
	add	r1, r1, r3
	ldrb	r4, [r1, #-1]	@ zero_extendqisi2
	ldrb	r9, [r1, #-3]	@ zero_extendqisi2
	ldrb	sl, [r1, #-4]	@ zero_extendqisi2
	ldrb	r8, [r1, #-5]	@ zero_extendqisi2
	ldrb	r7, [r1, #-6]	@ zero_extendqisi2
	ldrb	r6, [r1, #-7]	@ zero_extendqisi2
	ldrb	r0, [r1, #-2]	@ zero_extendqisi2
	ldrb	r5, [r1, #-8]	@ zero_extendqisi2
	mov	r3, r1
	sub	r2, ip, #1
	cmp	ip, #1
	strb	r9, [r1, #-1]
	strb	sl, [r1, #-2]
	strb	r8, [r1, #-3]
	strb	r7, [r1, #-4]
	strb	r6, [r1, #-5]
	strb	r5, [r1, #-6]
	strb	r4, [r1, #-7]
	and	r2, r2, #3
	strb	r0, [r3, #-8]!
	mov	r0, #1
	bls	.L335
	cmp	r2, #0
	beq	.L331
	cmp	r2, #1
	beq	.L386
	cmp	r2, #2
	beq	.L387
	ldrb	r2, [r3, #-1]	@ zero_extendqisi2
	ldrb	r8, [r3, #-3]	@ zero_extendqisi2
	ldrb	r7, [r3, #-4]	@ zero_extendqisi2
	ldrb	r6, [r3, #-5]	@ zero_extendqisi2
	ldrb	r5, [r3, #-6]	@ zero_extendqisi2
	ldrb	r4, [r3, #-7]	@ zero_extendqisi2
	ldrb	r0, [r3, #-8]	@ zero_extendqisi2
	ldrb	sl, [r3, #-2]	@ zero_extendqisi2
	strb	r0, [r3, #-6]
	strb	r8, [r3, #-1]
	strb	r7, [r3, #-2]
	strb	r6, [r3, #-3]
	strb	r5, [r3, #-4]
	strb	r4, [r3, #-5]
	strb	r2, [r3, #-7]
	strb	sl, [r3, #-8]!
	mov	r0, #2
.L387:
	ldrb	r1, [r3, #-1]	@ zero_extendqisi2
	ldrb	sl, [r3, #-3]	@ zero_extendqisi2
	ldrb	r8, [r3, #-4]	@ zero_extendqisi2
	ldrb	r7, [r3, #-5]	@ zero_extendqisi2
	ldrb	r6, [r3, #-6]	@ zero_extendqisi2
	ldrb	r5, [r3, #-7]	@ zero_extendqisi2
	ldrb	r2, [r3, #-2]	@ zero_extendqisi2
	ldrb	r4, [r3, #-8]	@ zero_extendqisi2
	strb	sl, [r3, #-1]
	strb	r8, [r3, #-2]
	strb	r7, [r3, #-3]
	strb	r6, [r3, #-4]
	strb	r5, [r3, #-5]
	strb	r4, [r3, #-6]
	strb	r1, [r3, #-7]
	strb	r2, [r3, #-8]!
	add	r0, r0, #1
.L386:
	ldrb	r1, [r3, #-1]	@ zero_extendqisi2
	ldrb	sl, [r3, #-3]	@ zero_extendqisi2
	ldrb	r8, [r3, #-4]	@ zero_extendqisi2
	ldrb	r7, [r3, #-5]	@ zero_extendqisi2
	ldrb	r6, [r3, #-6]	@ zero_extendqisi2
	ldrb	r5, [r3, #-7]	@ zero_extendqisi2
	ldrb	r2, [r3, #-2]	@ zero_extendqisi2
	ldrb	r4, [r3, #-8]	@ zero_extendqisi2
	add	r0, r0, #1
	cmp	ip, r0
	strb	sl, [r3, #-1]
	strb	r8, [r3, #-2]
	strb	r7, [r3, #-3]
	strb	r6, [r3, #-4]
	strb	r5, [r3, #-5]
	strb	r4, [r3, #-6]
	strb	r1, [r3, #-7]
	strb	r2, [r3, #-8]!
	bls	.L335
.L331:
	ldrb	r2, [r3, #-1]	@ zero_extendqisi2
	ldrb	sl, [r3, #-3]	@ zero_extendqisi2
	ldrb	r8, [r3, #-4]	@ zero_extendqisi2
	ldrb	r7, [r3, #-5]	@ zero_extendqisi2
	ldrb	r6, [r3, #-6]	@ zero_extendqisi2
	ldrb	r5, [r3, #-7]	@ zero_extendqisi2
	ldrb	fp, [r3, #-2]	@ zero_extendqisi2
	ldrb	r4, [r3, #-8]	@ zero_extendqisi2
	mov	r1, r3
	strb	r2, [r3, #-7]
	strb	sl, [r3, #-1]
	strb	r8, [r3, #-2]
	strb	r7, [r3, #-3]
	strb	r6, [r3, #-4]
	strb	r5, [r3, #-5]
	strb	r4, [r3, #-6]
	strb	fp, [r1, #-8]!
	ldrb	r4, [r1, #-1]	@ zero_extendqisi2
	ldrb	r9, [r1, #-3]	@ zero_extendqisi2
	ldrb	sl, [r1, #-4]	@ zero_extendqisi2
	ldrb	r8, [r1, #-5]	@ zero_extendqisi2
	ldrb	r7, [r1, #-6]	@ zero_extendqisi2
	ldrb	r6, [r1, #-7]	@ zero_extendqisi2
	ldrb	r5, [r1, #-8]	@ zero_extendqisi2
	ldrb	fp, [r1, #-2]	@ zero_extendqisi2
	mov	r2, r1
	strb	r8, [r1, #-3]
	strb	r7, [r1, #-4]
	strb	r6, [r1, #-5]
	strb	r5, [r1, #-6]
	strb	r4, [r1, #-7]
	strb	r9, [r1, #-1]
	strb	sl, [r1, #-2]
	strb	fp, [r2, #-8]!
	ldrb	r1, [r2, #-3]	@ zero_extendqisi2
	ldrb	r9, [r2, #-1]	@ zero_extendqisi2
	ldrb	sl, [r2, #-2]	@ zero_extendqisi2
	strb	r1, [r2, #-1]
	ldrb	r8, [r2, #-4]	@ zero_extendqisi2
	ldrb	r7, [r2, #-5]	@ zero_extendqisi2
	ldrb	r6, [r2, #-6]	@ zero_extendqisi2
	ldrb	r5, [r2, #-7]	@ zero_extendqisi2
	ldrb	r4, [r2, #-8]	@ zero_extendqisi2
	strb	r8, [r2, #-2]
	strb	sl, [r2, #-8]
	strb	r7, [r2, #-3]
	strb	r6, [r2, #-4]
	strb	r5, [r2, #-5]
	strb	r4, [r2, #-6]
	strb	r9, [r2, #-7]
	sub	r2, r3, #24
	ldrb	r1, [r2, #-1]	@ zero_extendqisi2
	ldrb	r9, [r2, #-2]	@ zero_extendqisi2
	ldrb	sl, [r2, #-3]	@ zero_extendqisi2
	ldrb	r8, [r2, #-4]	@ zero_extendqisi2
	ldrb	r7, [r2, #-5]	@ zero_extendqisi2
	ldrb	r6, [r2, #-6]	@ zero_extendqisi2
	ldrb	r5, [r2, #-7]	@ zero_extendqisi2
	ldrb	r4, [r2, #-8]	@ zero_extendqisi2
	add	r0, r0, #4
	cmp	ip, r0
	strb	r9, [r2, #-8]
	strb	sl, [r2, #-1]
	strb	r8, [r2, #-2]
	strb	r7, [r2, #-3]
	strb	r6, [r2, #-4]
	strb	r5, [r2, #-5]
	strb	r4, [r2, #-6]
	strb	r1, [r2, #-7]
	sub	r3, r3, #32
	bhi	.L331
	b	.L335
.L394:
	cmp	ip, #0
	ldr	r3, [r0, #4]
	beq	.L335
	add	r1, r1, r3
	ldrb	r0, [r1, #-1]	@ zero_extendqisi2
	ldrb	r4, [r1, #-2]	@ zero_extendqisi2
	mov	r3, r1
	sub	r2, ip, #1
	cmp	ip, #1
	strb	r4, [r1, #-1]
	and	r2, r2, #3
	strb	r0, [r3, #-2]!
	mov	r0, #1
	bls	.L335
	cmp	r2, #0
	beq	.L333
	cmp	r2, #1
	beq	.L388
	cmp	r2, #2
	ldrneb	r2, [r3, #-1]	@ zero_extendqisi2
	ldrneb	r1, [r3, #-2]	@ zero_extendqisi2
	strneb	r1, [r3, #-1]
	strneb	r2, [r3, #-2]!
	ldrb	r2, [r3, #-1]	@ zero_extendqisi2
	ldrb	r1, [r3, #-2]	@ zero_extendqisi2
	movne	r0, #2
	strb	r1, [r3, #-1]
	strb	r2, [r3, #-2]!
	add	r0, r0, #1
.L388:
	ldrb	r2, [r3, #-1]	@ zero_extendqisi2
	ldrb	r1, [r3, #-2]	@ zero_extendqisi2
	add	r0, r0, #1
	cmp	ip, r0
	strb	r1, [r3, #-1]
	strb	r2, [r3, #-2]!
	bls	.L335
.L333:
	ldrb	r5, [r3, #-1]	@ zero_extendqisi2
	ldrb	r2, [r3, #-2]	@ zero_extendqisi2
	mov	r1, r3
	strb	r2, [r3, #-1]
	strb	r5, [r1, #-2]!
	ldrb	r4, [r1, #-1]	@ zero_extendqisi2
	ldrb	r5, [r1, #-2]	@ zero_extendqisi2
	mov	r2, r1
	strb	r5, [r1, #-1]
	strb	r4, [r2, #-2]!
	ldrb	r4, [r2, #-1]	@ zero_extendqisi2
	ldrb	r1, [r2, #-2]	@ zero_extendqisi2
	strb	r4, [r2, #-2]
	strb	r1, [r2, #-1]
	sub	r2, r3, #6
	ldrb	r4, [r2, #-1]	@ zero_extendqisi2
	ldrb	r1, [r2, #-2]	@ zero_extendqisi2
	add	r0, r0, #4
	cmp	ip, r0
	strb	r4, [r2, #-2]
	strb	r1, [r2, #-1]
	sub	r3, r3, #8
	bhi	.L333
	b	.L335
.L395:
	cmp	ip, #0
	ldr	r3, [r0, #4]
	beq	.L335
	add	r1, r1, r3
	ldrb	r6, [r1, #-2]	@ zero_extendqisi2
	ldrb	r5, [r1, #-3]	@ zero_extendqisi2
	ldrb	r0, [r1, #-1]	@ zero_extendqisi2
	ldrb	r4, [r1, #-4]	@ zero_extendqisi2
	mov	r3, r1
	sub	r2, ip, #1
	cmp	ip, #1
	strb	r6, [r1, #-1]
	strb	r5, [r1, #-2]
	strb	r4, [r1, #-3]
	and	r2, r2, #3
	strb	r0, [r3, #-4]!
	mov	r0, #1
	bls	.L335
	cmp	r2, #0
	beq	.L330
	cmp	r2, #1
	beq	.L384
	cmp	r2, #2
	beq	.L385
	ldrb	r0, [r3, #-3]	@ zero_extendqisi2
	ldrb	r4, [r3, #-2]	@ zero_extendqisi2
	ldrb	r2, [r3, #-1]	@ zero_extendqisi2
	ldrb	r1, [r3, #-4]	@ zero_extendqisi2
	strb	r0, [r3, #-2]
	strb	r4, [r3, #-1]
	strb	r1, [r3, #-3]
	strb	r2, [r3, #-4]!
	mov	r0, #2
.L385:
	ldrb	r5, [r3, #-2]	@ zero_extendqisi2
	ldrb	r4, [r3, #-3]	@ zero_extendqisi2
	ldrb	r2, [r3, #-1]	@ zero_extendqisi2
	ldrb	r1, [r3, #-4]	@ zero_extendqisi2
	strb	r5, [r3, #-1]
	strb	r4, [r3, #-2]
	strb	r1, [r3, #-3]
	strb	r2, [r3, #-4]!
	add	r0, r0, #1
.L384:
	ldrb	r5, [r3, #-2]	@ zero_extendqisi2
	ldrb	r4, [r3, #-3]	@ zero_extendqisi2
	ldrb	r2, [r3, #-1]	@ zero_extendqisi2
	ldrb	r1, [r3, #-4]	@ zero_extendqisi2
	add	r0, r0, #1
	cmp	ip, r0
	strb	r5, [r3, #-1]
	strb	r4, [r3, #-2]
	strb	r1, [r3, #-3]
	strb	r2, [r3, #-4]!
	bls	.L335
.L330:
	ldrb	r5, [r3, #-2]	@ zero_extendqisi2
	ldrb	r4, [r3, #-3]	@ zero_extendqisi2
	ldrb	r7, [r3, #-1]	@ zero_extendqisi2
	ldrb	r2, [r3, #-4]	@ zero_extendqisi2
	mov	r1, r3
	strb	r2, [r3, #-3]
	strb	r5, [r3, #-1]
	strb	r4, [r3, #-2]
	strb	r7, [r1, #-4]!
	ldrb	r6, [r1, #-3]	@ zero_extendqisi2
	ldrb	r7, [r1, #-2]	@ zero_extendqisi2
	ldrb	r4, [r1, #-1]	@ zero_extendqisi2
	ldrb	r5, [r1, #-4]	@ zero_extendqisi2
	mov	r2, r1
	strb	r7, [r1, #-1]
	strb	r6, [r1, #-2]
	strb	r5, [r1, #-3]
	strb	r4, [r2, #-4]!
	ldrb	r6, [r2, #-1]	@ zero_extendqisi2
	ldrb	r5, [r2, #-2]	@ zero_extendqisi2
	ldrb	r4, [r2, #-3]	@ zero_extendqisi2
	ldrb	r1, [r2, #-4]	@ zero_extendqisi2
	strb	r5, [r2, #-1]
	strb	r6, [r2, #-4]
	strb	r4, [r2, #-2]
	strb	r1, [r2, #-3]
	sub	r2, r3, #12
	ldrb	r6, [r2, #-1]	@ zero_extendqisi2
	ldrb	r5, [r2, #-2]	@ zero_extendqisi2
	ldrb	r4, [r2, #-3]	@ zero_extendqisi2
	ldrb	r1, [r2, #-4]	@ zero_extendqisi2
	add	r0, r0, #4
	cmp	ip, r0
	strb	r6, [r2, #-4]
	strb	r5, [r2, #-1]
	strb	r4, [r2, #-2]
	strb	r1, [r2, #-3]
	sub	r3, r3, #16
	bhi	.L330
	b	.L335
	.size	png_do_read_swap_alpha, .-png_do_read_swap_alpha
	.section	.text.png_do_read_invert_alpha,"ax",%progbits
	.align	2
	.global	png_do_read_invert_alpha
	.hidden	png_do_read_invert_alpha
	.type	png_do_read_invert_alpha, %function
png_do_read_invert_alpha:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	str	r4, [sp, #-4]!
	ldrb	r3, [r0, #8]	@ zero_extendqisi2
	cmp	r3, #6
	ldr	r2, [r0, #0]
	beq	.L462
	cmp	r3, #4
	beq	.L463
.L405:
	ldmfd	sp!, {r4}
	bx	lr
.L463:
	ldrb	ip, [r0, #9]	@ zero_extendqisi2
	cmp	ip, #8
	beq	.L464
	cmp	r2, #0
	ldr	r3, [r0, #4]
	beq	.L405
	add	r3, r1, r3
	ldrb	r0, [r3, #-1]	@ zero_extendqisi2
	ldrb	r1, [r3, #-2]	@ zero_extendqisi2
	mvn	ip, r0
	cmp	r2, #1
	mvn	r0, r1
	sub	r1, r2, #1
	strb	r0, [r3, #-2]
	strb	ip, [r3, #-1]
	and	r1, r1, #3
	sub	r3, r3, #4
	mov	r0, #1
	bls	.L405
	cmp	r1, #0
	beq	.L404
	cmp	r1, #1
	beq	.L460
	cmp	r1, #2
	beq	.L461
	ldrb	r1, [r3, #-1]	@ zero_extendqisi2
	ldrb	ip, [r3, #-2]	@ zero_extendqisi2
	mvn	r0, r1
	mvn	r1, ip
	strb	r0, [r3, #-1]
	strb	r1, [r3, #-2]
	mov	r0, #2
	sub	r3, r3, #4
.L461:
	ldrb	ip, [r3, #-1]	@ zero_extendqisi2
	ldrb	r1, [r3, #-2]	@ zero_extendqisi2
	mvn	ip, ip
	mvn	r1, r1
	strb	ip, [r3, #-1]
	strb	r1, [r3, #-2]
	add	r0, r0, #1
	sub	r3, r3, #4
.L460:
	ldrb	ip, [r3, #-1]	@ zero_extendqisi2
	ldrb	r1, [r3, #-2]	@ zero_extendqisi2
	add	r0, r0, #1
	mvn	ip, ip
	mvn	r1, r1
	cmp	r2, r0
	strb	ip, [r3, #-1]
	strb	r1, [r3, #-2]
	sub	r3, r3, #4
	bls	.L405
.L404:
	ldrb	r1, [r3, #-1]	@ zero_extendqisi2
	ldrb	r4, [r3, #-2]	@ zero_extendqisi2
	mvn	ip, r1
	mvn	r1, r4
	strb	r1, [r3, #-2]
	strb	ip, [r3, #-1]
	sub	r1, r3, #4
	ldrb	r4, [r1, #-1]	@ zero_extendqisi2
	ldrb	ip, [r1, #-2]	@ zero_extendqisi2
	mvn	r4, r4
	mvn	ip, ip
	strb	r4, [r1, #-1]
	strb	ip, [r1, #-2]
	sub	r1, r1, #4
	ldrb	ip, [r1, #-1]	@ zero_extendqisi2
	ldrb	r4, [r1, #-2]	@ zero_extendqisi2
	mvn	ip, ip
	mvn	r4, r4
	strb	r4, [r1, #-2]
	strb	ip, [r1, #-1]
	sub	r1, r3, #12
	ldrb	ip, [r1, #-1]	@ zero_extendqisi2
	ldrb	r4, [r1, #-2]	@ zero_extendqisi2
	add	r0, r0, #4
	mvn	ip, ip
	mvn	r4, r4
	cmp	r2, r0
	strb	r4, [r1, #-2]
	strb	ip, [r1, #-1]
	sub	r3, r3, #16
	bhi	.L404
	b	.L405
.L462:
	ldrb	r3, [r0, #9]	@ zero_extendqisi2
	cmp	r3, #8
	beq	.L465
	cmp	r2, #0
	ldr	r3, [r0, #4]
	beq	.L405
	add	r1, r1, r3
	ldrb	r0, [r1, #-1]	@ zero_extendqisi2
	ldrb	r3, [r1, #-2]	@ zero_extendqisi2
	mvn	ip, r0
	cmp	r2, #1
	mvn	r0, r3
	sub	r3, r2, #1
	strb	r0, [r1, #-2]
	strb	ip, [r1, #-1]
	and	r3, r3, #3
	sub	r1, r1, #8
	mov	r0, #1
	bls	.L405
	cmp	r3, #0
	beq	.L401
	cmp	r3, #1
	beq	.L456
	cmp	r3, #2
	beq	.L457
	ldrb	r3, [r1, #-1]	@ zero_extendqisi2
	ldrb	ip, [r1, #-2]	@ zero_extendqisi2
	mvn	r0, r3
	mvn	r3, ip
	strb	r0, [r1, #-1]
	strb	r3, [r1, #-2]
	mov	r0, #2
	sub	r1, r1, #8
.L457:
	ldrb	ip, [r1, #-1]	@ zero_extendqisi2
	ldrb	r3, [r1, #-2]	@ zero_extendqisi2
	mvn	ip, ip
	mvn	r3, r3
	strb	ip, [r1, #-1]
	strb	r3, [r1, #-2]
	add	r0, r0, #1
	sub	r1, r1, #8
.L456:
	ldrb	ip, [r1, #-1]	@ zero_extendqisi2
	ldrb	r3, [r1, #-2]	@ zero_extendqisi2
	add	r0, r0, #1
	mvn	ip, ip
	mvn	r3, r3
	cmp	r2, r0
	strb	ip, [r1, #-1]
	strb	r3, [r1, #-2]
	sub	r1, r1, #8
	bls	.L405
.L401:
	ldrb	r3, [r1, #-1]	@ zero_extendqisi2
	ldrb	r4, [r1, #-2]	@ zero_extendqisi2
	mvn	ip, r3
	mvn	r3, r4
	strb	r3, [r1, #-2]
	strb	ip, [r1, #-1]
	sub	r3, r1, #8
	ldrb	r4, [r3, #-1]	@ zero_extendqisi2
	ldrb	ip, [r3, #-2]	@ zero_extendqisi2
	mvn	r4, r4
	mvn	ip, ip
	strb	r4, [r3, #-1]
	strb	ip, [r3, #-2]
	sub	r3, r3, #8
	ldrb	ip, [r3, #-1]	@ zero_extendqisi2
	ldrb	r4, [r3, #-2]	@ zero_extendqisi2
	mvn	ip, ip
	mvn	r4, r4
	strb	r4, [r3, #-2]
	strb	ip, [r3, #-1]
	sub	r3, r1, #24
	ldrb	ip, [r3, #-1]	@ zero_extendqisi2
	ldrb	r4, [r3, #-2]	@ zero_extendqisi2
	add	r0, r0, #4
	mvn	ip, ip
	mvn	r4, r4
	cmp	r2, r0
	strb	r4, [r3, #-2]
	strb	ip, [r3, #-1]
	sub	r1, r1, #32
	bhi	.L401
	b	.L405
.L464:
	cmp	r2, #0
	ldr	r3, [r0, #4]
	beq	.L405
	add	r3, r1, r3
	ldrb	ip, [r3, #-1]	@ zero_extendqisi2
	sub	r0, r2, #1
	mvn	r1, ip
	cmp	r2, #1
	strb	r1, [r3, #-1]
	and	r0, r0, #3
	sub	r3, r3, #2
	mov	r1, #1
	bls	.L405
	cmp	r0, #0
	beq	.L403
	cmp	r0, #1
	beq	.L458
	cmp	r0, #2
	ldrneb	r1, [r3, #-1]	@ zero_extendqisi2
	mvnne	r1, r1
	strneb	r1, [r3, #-1]
	subne	r3, r3, #2
	ldrb	ip, [r3, #-1]	@ zero_extendqisi2
	movne	r1, #2
	mvn	r0, ip
	strb	r0, [r3, #-1]
	add	r1, r1, #1
	sub	r3, r3, #2
.L458:
	ldrb	ip, [r3, #-1]	@ zero_extendqisi2
	add	r1, r1, #1
	mvn	r0, ip
	cmp	r2, r1
	strb	r0, [r3, #-1]
	sub	r3, r3, #2
	bls	.L405
.L403:
	ldrb	ip, [r3, #-1]	@ zero_extendqisi2
	mvn	r0, ip
	strb	r0, [r3, #-1]
	sub	r0, r3, #2
	ldrb	ip, [r0, #-1]	@ zero_extendqisi2
	mvn	ip, ip
	strb	ip, [r0, #-1]
	sub	r0, r0, #2
	ldrb	ip, [r0, #-1]	@ zero_extendqisi2
	mvn	ip, ip
	strb	ip, [r0, #-1]
	sub	r0, r3, #6
	ldrb	ip, [r0, #-1]	@ zero_extendqisi2
	add	r1, r1, #4
	mvn	ip, ip
	cmp	r2, r1
	strb	ip, [r0, #-1]
	sub	r3, r3, #8
	bhi	.L403
	b	.L405
.L465:
	cmp	r2, #0
	ldr	r3, [r0, #4]
	beq	.L405
	add	ip, r1, r3
	ldrb	r0, [ip, #-1]	@ zero_extendqisi2
	cmp	r2, #1
	mvn	r1, r0
	sub	r0, r2, #1
	strb	r1, [ip, #-1]
	and	r0, r0, #3
	sub	r3, ip, #4
	mov	r1, #1
	bls	.L405
	cmp	r0, #0
	beq	.L400
	cmp	r0, #1
	beq	.L454
	cmp	r0, #2
	ldrneb	r1, [r3, #-1]	@ zero_extendqisi2
	mvnne	r1, r1
	strneb	r1, [r3, #-1]
	subne	r3, r3, #4
	ldrb	ip, [r3, #-1]	@ zero_extendqisi2
	movne	r1, #2
	mvn	r0, ip
	strb	r0, [r3, #-1]
	add	r1, r1, #1
	sub	r3, r3, #4
.L454:
	ldrb	ip, [r3, #-1]	@ zero_extendqisi2
	add	r1, r1, #1
	mvn	r0, ip
	cmp	r2, r1
	strb	r0, [r3, #-1]
	sub	r3, r3, #4
	bls	.L405
.L400:
	ldrb	ip, [r3, #-1]	@ zero_extendqisi2
	mvn	r0, ip
	strb	r0, [r3, #-1]
	sub	r0, r3, #4
	ldrb	ip, [r0, #-1]	@ zero_extendqisi2
	mvn	ip, ip
	strb	ip, [r0, #-1]
	sub	r0, r0, #4
	ldrb	ip, [r0, #-1]	@ zero_extendqisi2
	mvn	ip, ip
	strb	ip, [r0, #-1]
	sub	r0, r3, #12
	ldrb	ip, [r0, #-1]	@ zero_extendqisi2
	add	r1, r1, #4
	mvn	ip, ip
	cmp	r2, r1
	strb	ip, [r0, #-1]
	sub	r3, r3, #16
	bhi	.L400
	b	.L405
	.size	png_do_read_invert_alpha, .-png_do_read_invert_alpha
	.section	.text.png_do_read_filler,"ax",%progbits
	.align	2
	.global	png_do_read_filler
	.hidden	png_do_read_filler
	.type	png_do_read_filler, %function
png_do_read_filler:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldrb	ip, [r0, #8]	@ zero_extendqisi2
	stmfd	sp!, {r4, r5, r6, r7, r8, sl}
	cmp	ip, #0
	mov	r4, r2, lsr #8
	and	r4, r4, #255
	and	r2, r2, #255
	ldr	r5, [r0, #0]
	bne	.L467
	ldrb	ip, [r0, #9]	@ zero_extendqisi2
	cmp	ip, #8
	beq	.L612
	cmp	ip, #16
	beq	.L613
.L491:
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl}
	bx	lr
.L467:
	cmp	ip, #2
	bne	.L491
	ldrb	r6, [r0, #9]	@ zero_extendqisi2
	cmp	r6, #8
	beq	.L614
	cmp	r6, #16
	bne	.L491
	tst	r3, #128
	beq	.L486
	mov	r8, r5, asl #1
	add	r3, r8, r5
	add	r1, r1, r3, asl #1
	cmp	r5, #1
	add	r8, r1, r8
	bls	.L487
	strb	r4, [r8, #-1]
	strb	r2, [r8, #-2]
	ldrb	r3, [r1, #-1]	@ zero_extendqisi2
	strb	r3, [r8, #-3]
	ldrb	r7, [r1, #-2]	@ zero_extendqisi2
	strb	r7, [r8, #-4]
	ldrb	r6, [r1, #-3]	@ zero_extendqisi2
	strb	r6, [r8, #-5]
	ldrb	r3, [r1, #-4]	@ zero_extendqisi2
	strb	r3, [r8, #-6]
	ldrb	r7, [r1, #-5]	@ zero_extendqisi2
	strb	r7, [r8, #-7]
	ldrb	r7, [r1, #-6]!	@ zero_extendqisi2
	sub	r6, r5, #2
	mov	r3, r8
	cmp	ip, r5
	strb	r7, [r3, #-8]!
	and	r6, r6, #3
	beq	.L607
	cmp	r6, #0
	beq	.L488
	cmp	r6, #1
	beq	.L600
	cmp	r6, #2
	beq	.L601
	strb	r4, [r3, #-1]
	strb	r2, [r3, #-2]
	ldrb	ip, [r1, #-1]	@ zero_extendqisi2
	strb	ip, [r3, #-3]
	ldrb	r6, [r1, #-2]	@ zero_extendqisi2
	strb	r6, [r3, #-4]
	ldrb	ip, [r1, #-3]	@ zero_extendqisi2
	strb	ip, [r3, #-5]
	ldrb	r6, [r1, #-4]	@ zero_extendqisi2
	strb	r6, [r3, #-6]
	ldrb	ip, [r1, #-5]	@ zero_extendqisi2
	strb	ip, [r3, #-7]
	ldrb	r6, [r1, #-6]!	@ zero_extendqisi2
	strb	r6, [r3, #-8]!
	mov	ip, #3
.L601:
	strb	r4, [r3, #-1]
	strb	r2, [r3, #-2]
	ldrb	r6, [r1, #-1]	@ zero_extendqisi2
	strb	r6, [r3, #-3]
	ldrb	r6, [r1, #-2]	@ zero_extendqisi2
	strb	r6, [r3, #-4]
	ldrb	r6, [r1, #-3]	@ zero_extendqisi2
	strb	r6, [r3, #-5]
	ldrb	r6, [r1, #-4]	@ zero_extendqisi2
	strb	r6, [r3, #-6]
	ldrb	r6, [r1, #-5]	@ zero_extendqisi2
	strb	r6, [r3, #-7]
	ldrb	r6, [r1, #-6]!	@ zero_extendqisi2
	strb	r6, [r3, #-8]!
	add	ip, ip, #1
.L600:
	strb	r4, [r3, #-1]
	strb	r2, [r3, #-2]
	ldrb	r6, [r1, #-1]	@ zero_extendqisi2
	strb	r6, [r3, #-3]
	ldrb	r6, [r1, #-2]	@ zero_extendqisi2
	strb	r6, [r3, #-4]
	ldrb	r6, [r1, #-3]	@ zero_extendqisi2
	strb	r6, [r3, #-5]
	ldrb	r6, [r1, #-4]	@ zero_extendqisi2
	strb	r6, [r3, #-6]
	ldrb	r6, [r1, #-5]	@ zero_extendqisi2
	strb	r6, [r3, #-7]
	add	ip, ip, #1
	ldrb	r6, [r1, #-6]!	@ zero_extendqisi2
	cmp	ip, r5
	strb	r6, [r3, #-8]!
	beq	.L607
.L488:
	strb	r4, [r3, #-1]
	strb	r2, [r3, #-2]
	ldrb	r7, [r1, #-1]	@ zero_extendqisi2
	strb	r7, [r3, #-3]
	ldrb	r6, [r1, #-2]	@ zero_extendqisi2
	strb	r6, [r3, #-4]
	ldrb	sl, [r1, #-3]	@ zero_extendqisi2
	strb	sl, [r3, #-5]
	ldrb	r7, [r1, #-4]	@ zero_extendqisi2
	strb	r7, [r3, #-6]
	ldrb	r6, [r1, #-5]	@ zero_extendqisi2
	strb	r6, [r3, #-7]
	mov	r7, r1
	ldrb	sl, [r7, #-6]!	@ zero_extendqisi2
	mov	r6, r3
	strb	sl, [r6, #-8]!
	strb	r4, [r6, #-1]
	strb	r2, [r6, #-2]
	ldrb	sl, [r7, #-1]	@ zero_extendqisi2
	strb	sl, [r6, #-3]
	ldrb	sl, [r7, #-2]	@ zero_extendqisi2
	strb	sl, [r6, #-4]
	ldrb	sl, [r7, #-3]	@ zero_extendqisi2
	strb	sl, [r6, #-5]
	ldrb	sl, [r7, #-4]	@ zero_extendqisi2
	strb	sl, [r6, #-6]
	ldrb	sl, [r7, #-5]	@ zero_extendqisi2
	strb	sl, [r6, #-7]
	ldrb	r7, [r7, #-6]	@ zero_extendqisi2
	strb	r7, [r6, #-8]!
	strb	r4, [r6, #-1]
	strb	r2, [r6, #-2]
	sub	r7, r1, #12
	ldrb	sl, [r7, #-1]	@ zero_extendqisi2
	strb	sl, [r6, #-3]
	ldrb	sl, [r7, #-2]	@ zero_extendqisi2
	strb	sl, [r6, #-4]
	ldrb	sl, [r7, #-3]	@ zero_extendqisi2
	strb	sl, [r6, #-5]
	ldrb	sl, [r7, #-4]	@ zero_extendqisi2
	strb	sl, [r6, #-6]
	ldrb	sl, [r7, #-5]	@ zero_extendqisi2
	strb	sl, [r6, #-7]
	ldrb	sl, [r7, #-6]	@ zero_extendqisi2
	sub	r7, r3, #24
	strb	sl, [r6, #-8]
	strb	r4, [r7, #-1]
	strb	r2, [r7, #-2]
	sub	r6, r1, #18
	ldrb	sl, [r6, #-1]	@ zero_extendqisi2
	strb	sl, [r7, #-3]
	ldrb	sl, [r6, #-2]	@ zero_extendqisi2
	strb	sl, [r7, #-4]
	ldrb	sl, [r6, #-3]	@ zero_extendqisi2
	strb	sl, [r7, #-5]
	ldrb	sl, [r6, #-4]	@ zero_extendqisi2
	strb	sl, [r7, #-6]
	ldrb	sl, [r6, #-5]	@ zero_extendqisi2
	strb	sl, [r7, #-7]
	add	ip, ip, #4
	ldrb	r6, [r6, #-6]	@ zero_extendqisi2
	cmp	ip, r5
	strb	r6, [r7, #-8]
	sub	r3, r3, #32
	sub	r1, r1, #24
	bne	.L488
.L607:
	rsb	ip, r5, #1
	add	r8, r8, ip, asl #3
.L487:
	mov	r5, r5, asl #3
	strb	r2, [r8, #-2]
	strb	r4, [r8, #-1]
	b	.L609
.L612:
	tst	r3, #128
	beq	.L469
	add	r1, r1, r5
	cmp	r5, #1
	add	r7, r1, r5
	bls	.L470
	strb	r2, [r7, #-1]
	mov	r3, #2
	ldrb	r6, [r1, #-1]	@ zero_extendqisi2
	sub	r4, r5, #2
	mov	ip, r7
	cmp	r3, r5
	strb	r6, [ip, #-2]!
	and	r4, r4, #3
	beq	.L604
	cmp	r4, #0
	beq	.L471
	cmp	r4, #1
	beq	.L588
	cmp	r4, #2
	strneb	r2, [ip, #-1]
	ldrneb	r3, [r1, #-2]	@ zero_extendqisi2
	strneb	r3, [ip, #-2]!
	movne	r3, #3
	strb	r2, [ip, #-1]
	rsb	r4, r3, #0
	ldrb	r4, [r1, r4]	@ zero_extendqisi2
	strb	r4, [ip, #-2]!
	add	r3, r3, #1
.L588:
	strb	r2, [ip, #-1]
	rsb	r4, r3, #0
	ldrb	r4, [r1, r4]	@ zero_extendqisi2
	add	r3, r3, #1
	cmp	r3, r5
	strb	r4, [ip, #-2]!
	beq	.L604
.L471:
	strb	r2, [ip, #-1]
	rsb	r4, r3, #0
	ldrb	r6, [r1, r4]	@ zero_extendqisi2
	mov	r4, ip
	strb	r6, [r4, #-2]!
	add	r6, r3, #1
	strb	r2, [r4, #-1]
	rsb	r8, r6, #0
	ldrb	r8, [r1, r8]	@ zero_extendqisi2
	strb	r8, [r4, #-2]!
	strb	r2, [r4, #-1]
	mvn	r6, r6
	ldrb	r8, [r1, r6]	@ zero_extendqisi2
	sub	r6, ip, #6
	strb	r8, [r4, #-2]
	strb	r2, [r6, #-1]
	rsb	r4, r3, r1
	ldrb	r4, [r4, #-3]	@ zero_extendqisi2
	add	r3, r3, #4
	cmp	r3, r5
	strb	r4, [r6, #-2]
	sub	ip, ip, #8
	bne	.L471
.L604:
	rsb	r3, r5, #1
	add	r7, r7, r3, asl #1
.L470:
	strb	r2, [r7, #-1]
	mov	r5, r5, asl #1
.L608:
	mov	r2, #2
	mov	r1, #16
	str	r5, [r0, #4]
	strb	r2, [r0, #10]
	strb	r1, [r0, #11]
	b	.L491
.L613:
	tst	r3, #128
	beq	.L475
	cmp	r5, #1
	add	r8, r1, r5, asl #2
	add	r1, r1, r5, asl #1
	bls	.L476
	strb	r4, [r8, #-1]
	strb	r2, [r8, #-2]
	ldrb	r3, [r1, #-1]	@ zero_extendqisi2
	strb	r3, [r8, #-3]
	mov	r6, #2
	ldrb	r7, [r1, #-2]!	@ zero_extendqisi2
	sub	ip, r5, #2
	mov	r3, r8
	cmp	r6, r5
	strb	r7, [r3, #-4]!
	and	ip, ip, #3
	beq	.L605
	cmp	ip, #0
	beq	.L477
	cmp	ip, #1
	beq	.L592
	cmp	ip, #2
	beq	.L593
	strb	r4, [r3, #-1]
	strb	r2, [r3, #-2]
	ldrb	r6, [r1, #-1]	@ zero_extendqisi2
	strb	r6, [r3, #-3]
	ldrb	ip, [r1, #-2]!	@ zero_extendqisi2
	strb	ip, [r3, #-4]!
	mov	r6, #3
.L593:
	strb	r4, [r3, #-1]
	strb	r2, [r3, #-2]
	ldrb	ip, [r1, #-1]	@ zero_extendqisi2
	strb	ip, [r3, #-3]
	ldrb	ip, [r1, #-2]!	@ zero_extendqisi2
	strb	ip, [r3, #-4]!
	add	r6, r6, #1
.L592:
	strb	r4, [r3, #-1]
	strb	r2, [r3, #-2]
	ldrb	ip, [r1, #-1]	@ zero_extendqisi2
	strb	ip, [r3, #-3]
	add	r6, r6, #1
	ldrb	ip, [r1, #-2]!	@ zero_extendqisi2
	cmp	r6, r5
	strb	ip, [r3, #-4]!
	beq	.L605
.L477:
	strb	r4, [r3, #-1]
	strb	r2, [r3, #-2]
	ldrb	ip, [r1, #-1]	@ zero_extendqisi2
	strb	ip, [r3, #-3]
	mov	r7, r1
	ldrb	sl, [r7, #-2]!	@ zero_extendqisi2
	mov	ip, r3
	strb	sl, [ip, #-4]!
	strb	r4, [ip, #-1]
	strb	r2, [ip, #-2]
	ldrb	sl, [r7, #-1]	@ zero_extendqisi2
	strb	sl, [ip, #-3]
	ldrb	sl, [r7, #-2]!	@ zero_extendqisi2
	strb	sl, [ip, #-4]!
	strb	r4, [ip, #-1]
	strb	r2, [ip, #-2]
	ldrb	sl, [r7, #-1]	@ zero_extendqisi2
	strb	sl, [ip, #-3]
	ldrb	sl, [r7, #-2]	@ zero_extendqisi2
	sub	r7, r3, #12
	strb	sl, [ip, #-4]
	strb	r4, [r7, #-1]
	strb	r2, [r7, #-2]
	sub	ip, r1, #6
	ldrb	sl, [ip, #-1]	@ zero_extendqisi2
	strb	sl, [r7, #-3]
	add	r6, r6, #4
	ldrb	ip, [ip, #-2]	@ zero_extendqisi2
	cmp	r6, r5
	strb	ip, [r7, #-4]
	sub	r3, r3, #16
	sub	r1, r1, #8
	bne	.L477
.L605:
	rsb	r1, r5, #1
	add	r8, r8, r1, asl #2
.L476:
	strb	r2, [r8, #-2]
	strb	r4, [r8, #-1]
	mov	r5, r5, asl #2
.L610:
	mov	r3, #2
	mov	r2, #32
	str	r5, [r0, #4]
	strb	r3, [r0, #10]
	strb	r2, [r0, #11]
	b	.L491
.L469:
	cmp	r5, #0
	beq	.L473
	add	r1, r1, r5
	add	ip, r1, r5
	ldrb	r6, [r1, #-1]	@ zero_extendqisi2
	mov	r3, ip
	sub	r4, r5, #1
	cmp	r5, #1
	strb	r6, [ip, #-1]
	and	r4, r4, #3
	strb	r2, [r3, #-2]!
	mov	ip, #2
	bls	.L473
	cmp	r4, #0
	beq	.L474
	cmp	r4, #1
	beq	.L590
	cmp	r4, #2
	ldrneb	ip, [r1, #-2]	@ zero_extendqisi2
	strneb	ip, [r3, #-1]
	strneb	r2, [r3, #-2]!
	movne	ip, #3
	rsb	r4, ip, #0
	ldrb	r6, [r1, r4]	@ zero_extendqisi2
	strb	r6, [r3, #-1]
	strb	r2, [r3, #-2]!
	add	ip, ip, #1
.L590:
	rsb	r6, ip, #0
	add	ip, ip, #1
	ldrb	r4, [r1, r6]	@ zero_extendqisi2
	sub	r6, ip, #1
	cmp	r5, r6
	strb	r4, [r3, #-1]
	strb	r2, [r3, #-2]!
	bls	.L473
.L474:
	rsb	r7, ip, #0
	ldrb	r8, [r1, r7]	@ zero_extendqisi2
	mov	r6, r3
	strb	r8, [r3, #-1]
	add	r7, ip, #1
	strb	r2, [r6, #-2]!
	rsb	r4, r7, #0
	ldrb	r8, [r1, r4]	@ zero_extendqisi2
	mov	r4, r6
	strb	r8, [r6, #-1]
	strb	r2, [r4, #-2]!
	mvn	r7, r7
	ldrb	r6, [r1, r7]	@ zero_extendqisi2
	strb	r2, [r4, #-2]
	strb	r6, [r4, #-1]
	rsb	r4, ip, r1
	add	ip, ip, #4
	ldrb	r6, [r4, #-3]	@ zero_extendqisi2
	sub	r4, ip, #1
	cmp	r5, r4
	sub	r4, r3, #6
	strb	r6, [r4, #-1]
	strb	r2, [r4, #-2]
	sub	r3, r3, #8
	bhi	.L474
.L473:
	mov	r5, r5, asl #1
	b	.L608
.L475:
	cmp	r5, #0
	beq	.L478
	add	ip, r1, r5, asl #1
	ldrb	r6, [ip, #-1]	@ zero_extendqisi2
	add	r1, r1, r5, asl #2
	strb	r6, [r1, #-1]
	ldrb	r7, [ip, #-2]!	@ zero_extendqisi2
	mov	r3, r1
	sub	r6, r5, #1
	cmp	r5, #1
	strb	r7, [r1, #-2]
	strb	r4, [r1, #-3]
	mov	r7, #1
	and	r1, r6, #3
	strb	r2, [r3, #-4]!
	bls	.L478
	cmp	r1, #0
	beq	.L479
	cmp	r1, #1
	beq	.L594
	cmp	r1, #2
	beq	.L595
	ldrb	r1, [ip, #-1]	@ zero_extendqisi2
	strb	r1, [r3, #-1]
	ldrb	r7, [ip, #-2]!	@ zero_extendqisi2
	strb	r4, [r3, #-3]
	strb	r7, [r3, #-2]
	strb	r2, [r3, #-4]!
	mov	r7, #2
.L595:
	ldrb	r1, [ip, #-1]	@ zero_extendqisi2
	strb	r1, [r3, #-1]
	ldrb	r1, [ip, #-2]!	@ zero_extendqisi2
	strb	r4, [r3, #-3]
	strb	r1, [r3, #-2]
	strb	r2, [r3, #-4]!
	add	r7, r7, #1
.L594:
	ldrb	r1, [ip, #-1]	@ zero_extendqisi2
	strb	r1, [r3, #-1]
	add	r7, r7, #1
	ldrb	r1, [ip, #-2]!	@ zero_extendqisi2
	cmp	r5, r7
	strb	r1, [r3, #-2]
	strb	r4, [r3, #-3]
	strb	r2, [r3, #-4]!
	bls	.L478
.L479:
	ldrb	r8, [ip, #-1]	@ zero_extendqisi2
	strb	r8, [r3, #-1]
	mov	r8, ip
	ldrb	r1, [r8, #-2]!	@ zero_extendqisi2
	mov	r6, r3
	strb	r1, [r3, #-2]
	strb	r4, [r3, #-3]
	strb	r2, [r6, #-4]!
	ldrb	sl, [r8, #-1]	@ zero_extendqisi2
	strb	sl, [r6, #-1]
	ldrb	sl, [r8, #-2]!	@ zero_extendqisi2
	mov	r1, r6
	strb	sl, [r6, #-2]
	strb	r4, [r6, #-3]
	strb	r2, [r1, #-4]!
	ldrb	r6, [r8, #-1]	@ zero_extendqisi2
	strb	r6, [r1, #-1]
	ldrb	r6, [r8, #-2]	@ zero_extendqisi2
	strb	r2, [r1, #-4]
	strb	r6, [r1, #-2]
	strb	r4, [r1, #-3]
	sub	r6, ip, #6
	ldrb	r8, [r6, #-1]	@ zero_extendqisi2
	sub	r1, r3, #12
	strb	r8, [r1, #-1]
	add	r7, r7, #4
	ldrb	r6, [r6, #-2]	@ zero_extendqisi2
	cmp	r5, r7
	strb	r6, [r1, #-2]
	strb	r2, [r1, #-4]
	strb	r4, [r1, #-3]
	sub	r3, r3, #16
	sub	ip, ip, #8
	bhi	.L479
.L478:
	mov	r5, r5, asl #2
	b	.L610
.L614:
	tst	r3, #128
	beq	.L481
	add	r7, r5, r5, asl #1
	add	r1, r1, r7
	cmp	r5, #1
	add	r6, r1, r5
	bls	.L482
	strb	r2, [r6, #-1]
	ldrb	r4, [r1, #-1]	@ zero_extendqisi2
	strb	r4, [r6, #-2]
	ldrb	ip, [r1, #-2]	@ zero_extendqisi2
	rsb	r7, r7, #3
	strb	ip, [r6, #-3]
	rsb	r3, r7, #-16777216
	add	r8, r3, #16711680
	mov	r3, r1
	add	r4, r8, #65280
	ldrb	r8, [r3, #-3]!	@ zero_extendqisi2
	add	ip, r4, #253
	add	r7, r7, r1
	add	r4, ip, ip, asl #1
	cmp	r3, r7
	mov	ip, r6
	strb	r8, [ip, #-4]!
	and	r4, r4, #3
	beq	.L606
	cmp	r4, #0
	beq	.L483
	cmp	r4, #1
	beq	.L596
	cmp	r4, #2
	beq	.L597
	strb	r2, [ip, #-1]
	ldrb	r4, [r3, #-1]	@ zero_extendqisi2
	strb	r4, [ip, #-2]
	ldrb	r4, [r3, #-2]	@ zero_extendqisi2
	strb	r4, [ip, #-3]
	ldrb	r3, [r3, #-3]	@ zero_extendqisi2
	strb	r3, [ip, #-4]!
	sub	r3, r1, #6
.L597:
	strb	r2, [ip, #-1]
	ldrb	r1, [r3, #-1]	@ zero_extendqisi2
	strb	r1, [ip, #-2]
	ldrb	r1, [r3, #-2]	@ zero_extendqisi2
	strb	r1, [ip, #-3]
	ldrb	r1, [r3, #-3]!	@ zero_extendqisi2
	strb	r1, [ip, #-4]!
.L596:
	strb	r2, [ip, #-1]
	ldrb	r1, [r3, #-1]	@ zero_extendqisi2
	strb	r1, [ip, #-2]
	ldrb	r1, [r3, #-2]	@ zero_extendqisi2
	strb	r1, [ip, #-3]
	ldrb	r1, [r3, #-3]!	@ zero_extendqisi2
	cmp	r3, r7
	strb	r1, [ip, #-4]!
	beq	.L606
.L483:
	strb	r2, [ip, #-1]
	ldrb	r4, [r3, #-1]	@ zero_extendqisi2
	strb	r4, [ip, #-2]
	ldrb	r1, [r3, #-2]	@ zero_extendqisi2
	strb	r1, [ip, #-3]
	mov	r4, r3
	ldrb	r8, [r4, #-3]!	@ zero_extendqisi2
	mov	r1, ip
	strb	r8, [r1, #-4]!
	strb	r2, [r1, #-1]
	ldrb	r8, [r4, #-1]	@ zero_extendqisi2
	strb	r8, [r1, #-2]
	ldrb	r8, [r4, #-2]	@ zero_extendqisi2
	strb	r8, [r1, #-3]
	ldrb	r4, [r4, #-3]	@ zero_extendqisi2
	strb	r4, [r1, #-4]!
	strb	r2, [r1, #-1]
	sub	r4, r3, #6
	ldrb	r8, [r4, #-1]	@ zero_extendqisi2
	strb	r8, [r1, #-2]
	ldrb	r8, [r4, #-2]	@ zero_extendqisi2
	strb	r8, [r1, #-3]
	ldrb	r8, [r4, #-3]	@ zero_extendqisi2
	sub	r4, ip, #12
	strb	r8, [r1, #-4]
	strb	r2, [r4, #-1]
	sub	r1, r3, #9
	ldrb	r8, [r1, #-1]	@ zero_extendqisi2
	strb	r8, [r4, #-2]
	ldrb	r8, [r1, #-2]	@ zero_extendqisi2
	strb	r8, [r4, #-3]
	sub	r3, r3, #12
	ldrb	r1, [r1, #-3]	@ zero_extendqisi2
	cmp	r3, r7
	strb	r1, [r4, #-4]
	sub	ip, ip, #16
	bne	.L483
.L606:
	rsb	r3, r5, #1
	add	r6, r6, r3, asl #2
.L482:
	mov	r5, r5, asl #2
	strb	r2, [r6, #-1]
	b	.L611
.L481:
	cmp	r5, #0
	beq	.L484
	add	r3, r5, r5, asl #1
	add	r1, r1, r3
	ldrb	r4, [r1, #-1]	@ zero_extendqisi2
	add	r6, r1, r5
	strb	r4, [r6, #-1]
	ldrb	r8, [r1, #-2]	@ zero_extendqisi2
	strb	r8, [r6, #-2]
	mov	r4, r1
	ldrb	r8, [r4, #-3]!	@ zero_extendqisi2
	mov	r3, r6
	sub	r7, r5, #1
	cmp	r5, #1
	strb	r8, [r6, #-3]
	and	r7, r7, #3
	strb	r2, [r3, #-4]!
	mov	r6, #1
	bls	.L484
	cmp	r7, #0
	beq	.L485
	cmp	r7, #1
	beq	.L598
	cmp	r7, #2
	beq	.L599
	ldrb	r6, [r4, #-1]	@ zero_extendqisi2
	strb	r6, [r3, #-1]
	ldrb	r6, [r4, #-2]	@ zero_extendqisi2
	strb	r6, [r3, #-2]
	ldrb	r6, [r4, #-3]	@ zero_extendqisi2
	strb	r6, [r3, #-3]
	strb	r2, [r3, #-4]!
	mov	r6, ip
	sub	r4, r1, #6
.L599:
	ldrb	ip, [r4, #-1]	@ zero_extendqisi2
	strb	ip, [r3, #-1]
	ldrb	r1, [r4, #-2]	@ zero_extendqisi2
	strb	r1, [r3, #-2]
	ldrb	ip, [r4, #-3]!	@ zero_extendqisi2
	strb	ip, [r3, #-3]
	strb	r2, [r3, #-4]!
	add	r6, r6, #1
.L598:
	ldrb	r1, [r4, #-1]	@ zero_extendqisi2
	strb	r1, [r3, #-1]
	ldrb	ip, [r4, #-2]	@ zero_extendqisi2
	strb	ip, [r3, #-2]
	add	r6, r6, #1
	ldrb	r1, [r4, #-3]!	@ zero_extendqisi2
	cmp	r5, r6
	strb	r1, [r3, #-3]
	strb	r2, [r3, #-4]!
	bls	.L484
.L485:
	ldrb	r1, [r4, #-1]	@ zero_extendqisi2
	strb	r1, [r3, #-1]
	ldrb	ip, [r4, #-2]	@ zero_extendqisi2
	strb	ip, [r3, #-2]
	mov	r1, r4
	ldrb	r7, [r1, #-3]!	@ zero_extendqisi2
	mov	ip, r3
	strb	r7, [r3, #-3]
	strb	r2, [ip, #-4]!
	ldrb	r7, [r1, #-1]	@ zero_extendqisi2
	strb	r7, [ip, #-1]
	ldrb	r7, [r1, #-2]	@ zero_extendqisi2
	strb	r7, [ip, #-2]
	ldrb	r7, [r1, #-3]	@ zero_extendqisi2
	mov	r1, ip
	strb	r7, [ip, #-3]
	strb	r2, [r1, #-4]!
	sub	ip, r4, #6
	ldrb	r7, [ip, #-1]	@ zero_extendqisi2
	strb	r7, [r1, #-1]
	ldrb	r7, [ip, #-2]	@ zero_extendqisi2
	strb	r7, [r1, #-2]
	ldrb	ip, [ip, #-3]	@ zero_extendqisi2
	strb	r2, [r1, #-4]
	strb	ip, [r1, #-3]
	sub	ip, r4, #9
	ldrb	r7, [ip, #-1]	@ zero_extendqisi2
	sub	r1, r3, #12
	strb	r7, [r1, #-1]
	ldrb	r7, [ip, #-2]	@ zero_extendqisi2
	strb	r7, [r1, #-2]
	add	r6, r6, #4
	ldrb	ip, [ip, #-3]	@ zero_extendqisi2
	cmp	r5, r6
	strb	ip, [r1, #-3]
	strb	r2, [r1, #-4]
	sub	r3, r3, #16
	sub	r4, r4, #12
	bhi	.L485
.L484:
	mov	r5, r5, asl #2
.L611:
	mov	r3, #4
	mov	r2, #32
	str	r5, [r0, #4]
	strb	r3, [r0, #10]
	strb	r2, [r0, #11]
	b	.L491
.L486:
	cmp	r5, #0
	beq	.L489
	mov	ip, r5, asl #1
	add	r7, ip, r5
	add	r1, r1, r7, asl #1
	ldrb	r6, [r1, #-1]	@ zero_extendqisi2
	add	ip, r1, ip
	strb	r6, [ip, #-1]
	ldrb	r3, [r1, #-2]	@ zero_extendqisi2
	strb	r3, [ip, #-2]
	ldrb	r7, [r1, #-3]	@ zero_extendqisi2
	strb	r7, [ip, #-3]
	ldrb	r6, [r1, #-4]	@ zero_extendqisi2
	strb	r6, [ip, #-4]
	ldrb	r3, [r1, #-5]	@ zero_extendqisi2
	strb	r3, [ip, #-5]
	ldrb	r7, [r1, #-6]!	@ zero_extendqisi2
	mov	r3, ip
	sub	r6, r5, #1
	cmp	r5, #1
	strb	r7, [ip, #-6]
	strb	r4, [ip, #-7]
	mov	r7, #1
	and	ip, r6, #3
	strb	r2, [r3, #-8]!
	bls	.L489
	cmp	ip, #0
	beq	.L490
	cmp	ip, #1
	beq	.L602
	cmp	ip, #2
	beq	.L603
	ldrb	ip, [r1, #-1]	@ zero_extendqisi2
	strb	ip, [r3, #-1]
	ldrb	r7, [r1, #-2]	@ zero_extendqisi2
	strb	r7, [r3, #-2]
	ldrb	ip, [r1, #-3]	@ zero_extendqisi2
	strb	ip, [r3, #-3]
	ldrb	r7, [r1, #-4]	@ zero_extendqisi2
	strb	r7, [r3, #-4]
	ldrb	ip, [r1, #-5]	@ zero_extendqisi2
	strb	ip, [r3, #-5]
	ldrb	r7, [r1, #-6]!	@ zero_extendqisi2
	strb	r4, [r3, #-7]
	strb	r7, [r3, #-6]
	strb	r2, [r3, #-8]!
	mov	r7, #2
.L603:
	ldrb	ip, [r1, #-1]	@ zero_extendqisi2
	strb	ip, [r3, #-1]
	ldrb	ip, [r1, #-2]	@ zero_extendqisi2
	strb	ip, [r3, #-2]
	ldrb	ip, [r1, #-3]	@ zero_extendqisi2
	strb	ip, [r3, #-3]
	ldrb	ip, [r1, #-4]	@ zero_extendqisi2
	strb	ip, [r3, #-4]
	ldrb	ip, [r1, #-5]	@ zero_extendqisi2
	strb	ip, [r3, #-5]
	ldrb	ip, [r1, #-6]!	@ zero_extendqisi2
	strb	r4, [r3, #-7]
	strb	ip, [r3, #-6]
	strb	r2, [r3, #-8]!
	add	r7, r7, #1
.L602:
	ldrb	ip, [r1, #-1]	@ zero_extendqisi2
	strb	ip, [r3, #-1]
	ldrb	ip, [r1, #-2]	@ zero_extendqisi2
	strb	ip, [r3, #-2]
	ldrb	ip, [r1, #-3]	@ zero_extendqisi2
	strb	ip, [r3, #-3]
	ldrb	ip, [r1, #-4]	@ zero_extendqisi2
	strb	ip, [r3, #-4]
	ldrb	ip, [r1, #-5]	@ zero_extendqisi2
	strb	ip, [r3, #-5]
	add	r7, r7, #1
	ldrb	ip, [r1, #-6]!	@ zero_extendqisi2
	cmp	r5, r7
	strb	ip, [r3, #-6]
	strb	r4, [r3, #-7]
	strb	r2, [r3, #-8]!
	bls	.L489
.L490:
	ldrb	ip, [r1, #-1]	@ zero_extendqisi2
	strb	ip, [r3, #-1]
	ldrb	r6, [r1, #-2]	@ zero_extendqisi2
	strb	r6, [r3, #-2]
	ldrb	r8, [r1, #-3]	@ zero_extendqisi2
	strb	r8, [r3, #-3]
	ldrb	ip, [r1, #-4]	@ zero_extendqisi2
	strb	ip, [r3, #-4]
	ldrb	r6, [r1, #-5]	@ zero_extendqisi2
	strb	r6, [r3, #-5]
	mov	ip, r1
	ldrb	r8, [ip, #-6]!	@ zero_extendqisi2
	mov	r6, r3
	strb	r8, [r3, #-6]
	strb	r4, [r3, #-7]
	strb	r2, [r6, #-8]!
	ldrb	r8, [ip, #-1]	@ zero_extendqisi2
	strb	r8, [r6, #-1]
	ldrb	r8, [ip, #-2]	@ zero_extendqisi2
	strb	r8, [r6, #-2]
	ldrb	r8, [ip, #-3]	@ zero_extendqisi2
	strb	r8, [r6, #-3]
	ldrb	r8, [ip, #-4]	@ zero_extendqisi2
	strb	r8, [r6, #-4]
	ldrb	r8, [ip, #-5]	@ zero_extendqisi2
	strb	r8, [r6, #-5]
	ldrb	r8, [ip, #-6]	@ zero_extendqisi2
	mov	ip, r6
	strb	r8, [r6, #-6]
	strb	r4, [r6, #-7]
	strb	r2, [ip, #-8]!
	sub	r6, r1, #12
	ldrb	r8, [r6, #-1]	@ zero_extendqisi2
	strb	r8, [ip, #-1]
	ldrb	r8, [r6, #-2]	@ zero_extendqisi2
	strb	r8, [ip, #-2]
	ldrb	r8, [r6, #-3]	@ zero_extendqisi2
	strb	r8, [ip, #-3]
	ldrb	r8, [r6, #-4]	@ zero_extendqisi2
	strb	r8, [ip, #-4]
	ldrb	r8, [r6, #-5]	@ zero_extendqisi2
	strb	r8, [ip, #-5]
	ldrb	r6, [r6, #-6]	@ zero_extendqisi2
	strb	r2, [ip, #-8]
	strb	r6, [ip, #-6]
	strb	r4, [ip, #-7]
	sub	r6, r1, #18
	ldrb	r8, [r6, #-1]	@ zero_extendqisi2
	sub	ip, r3, #24
	strb	r8, [ip, #-1]
	ldrb	r8, [r6, #-2]	@ zero_extendqisi2
	strb	r8, [ip, #-2]
	ldrb	r8, [r6, #-3]	@ zero_extendqisi2
	strb	r8, [ip, #-3]
	ldrb	r8, [r6, #-4]	@ zero_extendqisi2
	strb	r8, [ip, #-4]
	ldrb	r8, [r6, #-5]	@ zero_extendqisi2
	strb	r8, [ip, #-5]
	add	r7, r7, #4
	ldrb	r6, [r6, #-6]	@ zero_extendqisi2
	cmp	r5, r7
	strb	r6, [ip, #-6]
	strb	r2, [ip, #-8]
	strb	r4, [ip, #-7]
	sub	r1, r1, #24
	sub	r3, r3, #32
	bhi	.L490
.L489:
	mov	r5, r5, asl #3
.L609:
	mov	r2, #4
	mov	r1, #64
	str	r5, [r0, #4]
	strb	r2, [r0, #10]
	strb	r1, [r0, #11]
	b	.L491
	.size	png_do_read_filler, .-png_do_read_filler
	.section	.text.png_do_gray_to_rgb,"ax",%progbits
	.align	2
	.global	png_do_gray_to_rgb
	.hidden	png_do_gray_to_rgb
	.type	png_do_gray_to_rgb, %function
png_do_gray_to_rgb:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldrb	r3, [r0, #9]	@ zero_extendqisi2
	cmp	r3, #7
	stmfd	sp!, {r4, r5, r6}
	ldr	ip, [r0, #0]
	bls	.L627
	ldrb	r2, [r0, #8]	@ zero_extendqisi2
	tst	r2, #2
	bne	.L627
	cmp	r2, #0
	bne	.L617
	cmp	r3, #8
	beq	.L680
	cmp	ip, #0
	beq	.L619
	mov	r3, ip, asl #1
	sub	r2, r3, #1
	add	r1, r1, r2
	mov	r2, r1
	ldrb	r4, [r2], #-2	@ zero_extendqisi2
	add	r1, r1, ip, asl #2
	strb	r4, [r1, #0]
	ldrb	r5, [r2, #1]	@ zero_extendqisi2
	strb	r5, [r1, #-1]
	ldrb	r3, [r2, #2]	@ zero_extendqisi2
	strb	r3, [r1, #-2]
	ldrb	r4, [r2, #1]	@ zero_extendqisi2
	strb	r4, [r1, #-3]
	ldrb	r5, [r2, #2]	@ zero_extendqisi2
	strb	r5, [r1, #-4]
	ldrb	r3, [r2, #1]	@ zero_extendqisi2
	sub	r4, ip, #1
	cmp	ip, #1
	strb	r3, [r1, #-5]
	and	r4, r4, #3
	sub	r2, r2, #2
	sub	r3, r1, #6
	mov	r5, #1
	bls	.L679
	cmp	r4, #0
	beq	.L621
	cmp	r4, #1
	beq	.L672
	cmp	r4, #2
	beq	.L673
	ldrb	r5, [r2, #2]	@ zero_extendqisi2
	strb	r5, [r3, #0]
	ldrb	r4, [r2, #1]	@ zero_extendqisi2
	strb	r4, [r3, #-1]
	ldrb	r5, [r2, #2]	@ zero_extendqisi2
	strb	r5, [r3, #-2]
	ldrb	r4, [r2, #1]	@ zero_extendqisi2
	strb	r4, [r3, #-3]
	ldrb	r5, [r2, #2]	@ zero_extendqisi2
	strb	r5, [r3, #-4]
	ldrb	r4, [r2, #1]	@ zero_extendqisi2
	mov	r5, #2
	strb	r4, [r3, #-5]
	sub	r2, r2, #2
	sub	r3, r1, #12
.L673:
	ldrb	r1, [r2, #2]	@ zero_extendqisi2
	strb	r1, [r3, #0]
	ldrb	r1, [r2, #1]	@ zero_extendqisi2
	strb	r1, [r3, #-1]
	ldrb	r1, [r2, #2]	@ zero_extendqisi2
	strb	r1, [r3, #-2]
	ldrb	r1, [r2, #1]	@ zero_extendqisi2
	strb	r1, [r3, #-3]
	ldrb	r1, [r2, #2]	@ zero_extendqisi2
	strb	r1, [r3, #-4]
	ldrb	r1, [r2, #1]	@ zero_extendqisi2
	add	r5, r5, #1
	strb	r1, [r3, #-5]
	sub	r2, r2, #2
	sub	r3, r3, #6
.L672:
	ldrb	r1, [r2, #2]	@ zero_extendqisi2
	strb	r1, [r3, #0]
	ldrb	r1, [r2, #1]	@ zero_extendqisi2
	strb	r1, [r3, #-1]
	ldrb	r1, [r2, #2]	@ zero_extendqisi2
	strb	r1, [r3, #-2]
	ldrb	r1, [r2, #1]	@ zero_extendqisi2
	strb	r1, [r3, #-3]
	ldrb	r1, [r2, #2]	@ zero_extendqisi2
	strb	r1, [r3, #-4]
	add	r5, r5, #1
	ldrb	r1, [r2, #1]	@ zero_extendqisi2
	cmp	ip, r5
	strb	r1, [r3, #-5]
	sub	r2, r2, #2
	sub	r3, r3, #6
	bls	.L679
.L621:
	ldrb	r4, [r2, #2]	@ zero_extendqisi2
	strb	r4, [r3, #0]
	ldrb	r6, [r2, #1]	@ zero_extendqisi2
	strb	r6, [r3, #-1]
	ldrb	r1, [r2, #2]	@ zero_extendqisi2
	strb	r1, [r3, #-2]
	ldrb	r4, [r2, #1]	@ zero_extendqisi2
	strb	r4, [r3, #-3]
	ldrb	r6, [r2, #2]	@ zero_extendqisi2
	strb	r6, [r3, #-4]
	ldrb	r1, [r2, #1]	@ zero_extendqisi2
	strb	r1, [r3, #-5]
	mov	r1, r2
	ldrb	r4, [r1], #-2	@ zero_extendqisi2
	strb	r4, [r3, #-6]
	ldrb	r6, [r1, #1]	@ zero_extendqisi2
	sub	r4, r3, #6
	strb	r6, [r4, #-1]
	ldrb	r6, [r1, #2]	@ zero_extendqisi2
	strb	r6, [r4, #-2]
	ldrb	r6, [r1, #1]	@ zero_extendqisi2
	strb	r6, [r4, #-3]
	ldrb	r6, [r1, #2]	@ zero_extendqisi2
	strb	r6, [r4, #-4]
	ldrb	r6, [r1, #1]	@ zero_extendqisi2
	strb	r6, [r4, #-5]
	ldrb	r4, [r1], #-2	@ zero_extendqisi2
	strb	r4, [r3, #-12]
	ldrb	r6, [r1, #1]	@ zero_extendqisi2
	sub	r4, r3, #12
	strb	r6, [r4, #-1]
	ldrb	r6, [r1, #2]	@ zero_extendqisi2
	strb	r6, [r4, #-2]
	ldrb	r6, [r1, #1]	@ zero_extendqisi2
	strb	r6, [r4, #-3]
	ldrb	r6, [r1, #2]	@ zero_extendqisi2
	strb	r6, [r4, #-4]
	ldrb	r1, [r1, #1]	@ zero_extendqisi2
	strb	r1, [r4, #-5]
	sub	r1, r2, #6
	ldrb	r4, [r1, #2]	@ zero_extendqisi2
	strb	r4, [r3, #-18]
	ldrb	r6, [r1, #1]	@ zero_extendqisi2
	sub	r4, r3, #18
	strb	r6, [r4, #-1]
	ldrb	r6, [r1, #2]	@ zero_extendqisi2
	strb	r6, [r4, #-2]
	ldrb	r6, [r1, #1]	@ zero_extendqisi2
	strb	r6, [r4, #-3]
	ldrb	r6, [r1, #2]	@ zero_extendqisi2
	strb	r6, [r4, #-4]
	add	r5, r5, #4
	ldrb	r1, [r1, #1]	@ zero_extendqisi2
	cmp	ip, r5
	strb	r1, [r4, #-5]
	sub	r3, r3, #24
	sub	r2, r2, #8
	bhi	.L621
.L679:
	ldrb	r3, [r0, #9]	@ zero_extendqisi2
	ldrb	r2, [r0, #8]	@ zero_extendqisi2
	b	.L619
.L617:
	cmp	r2, #4
	beq	.L681
.L619:
	ldrb	r1, [r0, #10]	@ zero_extendqisi2
	add	r1, r1, #2
	and	r1, r1, #255
	mul	r3, r1, r3
	and	r3, r3, #255
	cmp	r3, #7
	mulls	ip, r3, ip
	strb	r3, [r0, #11]
	movhi	r3, r3, lsr #3
	mulhi	ip, r3, ip
	addls	ip, ip, #7
	orr	r2, r2, #2
	movls	ip, ip, lsr #3
	strb	r2, [r0, #8]
	strb	r1, [r0, #10]
	str	ip, [r0, #4]
.L627:
	ldmfd	sp!, {r4, r5, r6}
	bx	lr
.L680:
	cmp	ip, #0
	beq	.L619
	sub	r5, ip, #1
	add	r1, r1, r5
	ldrb	r3, [r1, #0]	@ zero_extendqisi2
	add	r4, r1, ip, asl #1
	strb	r3, [r4, #0]
	ldrb	r2, [r1, #0]	@ zero_extendqisi2
	strb	r2, [r4, #-1]
	ldrb	r3, [r1, #0]	@ zero_extendqisi2
	cmp	ip, #1
	strb	r3, [r4, #-2]
	and	r5, r5, #3
	sub	r3, r4, #3
	mvn	r2, #0
	bls	.L679
	cmp	r5, #0
	beq	.L620
	cmp	r5, #1
	beq	.L670
	cmp	r5, #2
	beq	.L671
	ldrb	r5, [r1, #-1]	@ zero_extendqisi2
	strb	r5, [r3, #0]
	ldrb	r2, [r1, #-1]	@ zero_extendqisi2
	strb	r2, [r3, #-1]
	ldrb	r5, [r1, #-1]	@ zero_extendqisi2
	mvn	r2, #1
	strb	r5, [r3, #-2]
	sub	r3, r4, #6
.L671:
	ldrb	r4, [r1, r2]	@ zero_extendqisi2
	strb	r4, [r3, #0]
	ldrb	r5, [r1, r2]	@ zero_extendqisi2
	strb	r5, [r3, #-1]
	ldrb	r4, [r1, r2]	@ zero_extendqisi2
	sub	r2, r2, #1
	strb	r4, [r3, #-2]
	sub	r3, r3, #3
.L670:
	ldrb	r4, [r1, r2]	@ zero_extendqisi2
	strb	r4, [r3, #0]
	ldrb	r5, [r1, r2]	@ zero_extendqisi2
	strb	r5, [r3, #-1]
	ldrb	r4, [r1, r2]	@ zero_extendqisi2
	sub	r2, r2, #1
	rsb	r5, r2, #0
	cmp	ip, r5
	strb	r4, [r3, #-2]
	sub	r3, r3, #3
	bls	.L679
.L620:
	ldrb	r5, [r1, r2]	@ zero_extendqisi2
	strb	r5, [r3, #0]
	ldrb	r6, [r1, r2]	@ zero_extendqisi2
	strb	r6, [r3, #-1]
	ldrb	r4, [r1, r2]	@ zero_extendqisi2
	strb	r4, [r3, #-2]
	sub	r4, r2, #1
	ldrb	r5, [r1, r4]	@ zero_extendqisi2
	strb	r5, [r3, #-3]
	ldrb	r6, [r1, r4]	@ zero_extendqisi2
	sub	r5, r3, #3
	strb	r6, [r5, #-1]
	ldrb	r6, [r1, r4]	@ zero_extendqisi2
	strb	r6, [r5, #-2]
	sub	r4, r4, #1
	ldrb	r5, [r1, r4]	@ zero_extendqisi2
	strb	r5, [r3, #-6]
	ldrb	r6, [r1, r4]	@ zero_extendqisi2
	sub	r5, r3, #6
	strb	r6, [r5, #-1]
	ldrb	r4, [r1, r4]	@ zero_extendqisi2
	strb	r4, [r5, #-2]
	sub	r4, r2, #3
	ldrb	r5, [r1, r4]	@ zero_extendqisi2
	strb	r5, [r3, #-9]
	ldrb	r6, [r1, r4]	@ zero_extendqisi2
	sub	r5, r3, #9
	strb	r6, [r5, #-1]
	sub	r2, r2, #4
	ldrb	r4, [r1, r4]	@ zero_extendqisi2
	rsb	r6, r2, #0
	cmp	ip, r6
	strb	r4, [r5, #-2]
	sub	r3, r3, #12
	bhi	.L620
	b	.L679
.L681:
	cmp	r3, #8
	beq	.L682
	cmp	ip, #0
	beq	.L619
	mov	r3, ip, asl #2
	sub	r4, r3, #1
	add	r1, r1, r4
	ldrb	r2, [r1, #0]	@ zero_extendqisi2
	add	r3, r1, r3
	strb	r2, [r3, #0]
	ldrb	r4, [r1, #-1]	@ zero_extendqisi2
	strb	r4, [r3, #-1]
	ldrb	r2, [r1, #-2]	@ zero_extendqisi2
	strb	r2, [r3, #-2]
	ldrb	r4, [r1, #-3]	@ zero_extendqisi2
	strb	r4, [r3, #-3]
	ldrb	r2, [r1, #-2]	@ zero_extendqisi2
	strb	r2, [r3, #-4]
	ldrb	r4, [r1, #-3]	@ zero_extendqisi2
	strb	r4, [r3, #-5]
	ldrb	r2, [r1, #-2]	@ zero_extendqisi2
	strb	r2, [r3, #-6]
	ldrb	r4, [r1, #-3]	@ zero_extendqisi2
	sub	r2, ip, #1
	cmp	ip, #1
	strb	r4, [r3, #-7]
	and	r4, r2, #1
	sub	r3, r3, #8
	sub	r2, r1, #4
	mov	r1, #1
	bls	.L679
	cmp	r4, #0
	beq	.L624
	ldrb	r4, [r2, #0]	@ zero_extendqisi2
	strb	r4, [r3, #0]
	ldrb	r1, [r2, #-1]	@ zero_extendqisi2
	strb	r1, [r3, #-1]
	ldrb	r4, [r2, #-2]	@ zero_extendqisi2
	strb	r4, [r3, #-2]
	ldrb	r1, [r2, #-3]	@ zero_extendqisi2
	strb	r1, [r3, #-3]
	ldrb	r4, [r2, #-2]	@ zero_extendqisi2
	strb	r4, [r3, #-4]
	ldrb	r1, [r2, #-3]	@ zero_extendqisi2
	strb	r1, [r3, #-5]
	ldrb	r4, [r2, #-2]	@ zero_extendqisi2
	strb	r4, [r3, #-6]
	mov	r1, #2
	ldrb	r4, [r2, #-3]	@ zero_extendqisi2
	cmp	ip, r1
	strb	r4, [r3, #-7]
	sub	r2, r2, #4
	sub	r3, r3, #8
	bls	.L679
.L624:
	ldrb	r4, [r2, #0]	@ zero_extendqisi2
	strb	r4, [r3, #0]
	ldrb	r4, [r2, #-1]	@ zero_extendqisi2
	strb	r4, [r3, #-1]
	ldrb	r4, [r2, #-2]	@ zero_extendqisi2
	strb	r4, [r3, #-2]
	ldrb	r4, [r2, #-3]	@ zero_extendqisi2
	strb	r4, [r3, #-3]
	ldrb	r4, [r2, #-2]	@ zero_extendqisi2
	strb	r4, [r3, #-4]
	ldrb	r4, [r2, #-3]	@ zero_extendqisi2
	strb	r4, [r3, #-5]
	ldrb	r4, [r2, #-2]	@ zero_extendqisi2
	strb	r4, [r3, #-6]
	ldrb	r4, [r2, #-3]	@ zero_extendqisi2
	strb	r4, [r3, #-7]
	ldrb	r4, [r2, #-4]	@ zero_extendqisi2
	strb	r4, [r3, #-8]
	sub	r2, r2, #4
	ldrb	r4, [r2, #-1]	@ zero_extendqisi2
	sub	r3, r3, #8
	strb	r4, [r3, #-1]
	ldrb	r4, [r2, #-2]	@ zero_extendqisi2
	strb	r4, [r3, #-2]
	ldrb	r4, [r2, #-3]	@ zero_extendqisi2
	strb	r4, [r3, #-3]
	ldrb	r4, [r2, #-2]	@ zero_extendqisi2
	strb	r4, [r3, #-4]
	ldrb	r4, [r2, #-3]	@ zero_extendqisi2
	strb	r4, [r3, #-5]
	ldrb	r4, [r2, #-2]	@ zero_extendqisi2
	strb	r4, [r3, #-6]
	add	r1, r1, #2
	ldrb	r4, [r2, #-3]	@ zero_extendqisi2
	cmp	ip, r1
	strb	r4, [r3, #-7]
	sub	r2, r2, #4
	sub	r3, r3, #8
	bhi	.L624
	b	.L679
.L682:
	cmp	ip, #0
	beq	.L619
	mov	r3, ip, asl #1
	sub	r2, r3, #1
	add	r5, r1, r2
	ldrb	r4, [r5, #0]	@ zero_extendqisi2
	add	r3, r5, r3
	strb	r4, [r3, #0]
	ldrb	r1, [r5, #-1]	@ zero_extendqisi2
	strb	r1, [r3, #-1]
	ldrb	r4, [r5, #-1]	@ zero_extendqisi2
	strb	r4, [r3, #-2]
	ldrb	r4, [r5, #-1]	@ zero_extendqisi2
	sub	r1, ip, #1
	cmp	ip, #1
	strb	r4, [r3, #-3]
	sub	r2, r5, #2
	and	r1, r1, #3
	sub	r3, r3, #4
	mov	r5, #1
	bls	.L679
	cmp	r1, #0
	beq	.L623
	cmp	r1, #1
	beq	.L674
	cmp	r1, #2
	beq	.L675
	ldrb	r1, [r2, #0]	@ zero_extendqisi2
	strb	r1, [r3, #0]
	ldrb	r5, [r2, #-1]	@ zero_extendqisi2
	strb	r5, [r3, #-1]
	ldrb	r1, [r2, #-1]	@ zero_extendqisi2
	strb	r1, [r3, #-2]
	ldrb	r5, [r2, #-1]	@ zero_extendqisi2
	sub	r2, r2, #2
	strb	r5, [r3, #-3]
	sub	r3, r3, #4
	mov	r5, #2
.L675:
	ldrb	r1, [r2, #0]	@ zero_extendqisi2
	strb	r1, [r3, #0]
	ldrb	r1, [r2, #-1]	@ zero_extendqisi2
	strb	r1, [r3, #-1]
	ldrb	r1, [r2, #-1]	@ zero_extendqisi2
	strb	r1, [r3, #-2]
	ldrb	r1, [r2, #-1]	@ zero_extendqisi2
	add	r5, r5, #1
	strb	r1, [r3, #-3]
	sub	r2, r2, #2
	sub	r3, r3, #4
.L674:
	ldrb	r1, [r2, #0]	@ zero_extendqisi2
	strb	r1, [r3, #0]
	ldrb	r1, [r2, #-1]	@ zero_extendqisi2
	strb	r1, [r3, #-1]
	ldrb	r1, [r2, #-1]	@ zero_extendqisi2
	strb	r1, [r3, #-2]
	add	r5, r5, #1
	ldrb	r1, [r2, #-1]	@ zero_extendqisi2
	cmp	ip, r5
	strb	r1, [r3, #-3]
	sub	r2, r2, #2
	sub	r3, r3, #4
	bls	.L679
.L623:
	ldrb	r4, [r2, #0]	@ zero_extendqisi2
	strb	r4, [r3, #0]
	ldrb	r1, [r2, #-1]	@ zero_extendqisi2
	strb	r1, [r3, #-1]
	ldrb	r6, [r2, #-1]	@ zero_extendqisi2
	strb	r6, [r3, #-2]
	ldrb	r4, [r2, #-1]	@ zero_extendqisi2
	strb	r4, [r3, #-3]
	ldrb	r1, [r2, #-2]	@ zero_extendqisi2
	strb	r1, [r3, #-4]
	sub	r4, r2, #2
	ldrb	r6, [r4, #-1]	@ zero_extendqisi2
	sub	r1, r3, #4
	strb	r6, [r1, #-1]
	ldrb	r6, [r4, #-1]	@ zero_extendqisi2
	strb	r6, [r1, #-2]
	ldrb	r6, [r4, #-1]	@ zero_extendqisi2
	strb	r6, [r1, #-3]
	ldrb	r6, [r4, #-2]	@ zero_extendqisi2
	strb	r6, [r1, #-4]
	sub	r4, r4, #2
	ldrb	r6, [r4, #-1]	@ zero_extendqisi2
	sub	r1, r1, #4
	strb	r6, [r1, #-1]
	ldrb	r6, [r4, #-1]	@ zero_extendqisi2
	strb	r6, [r1, #-2]
	ldrb	r4, [r4, #-1]	@ zero_extendqisi2
	strb	r4, [r1, #-3]
	ldrb	r1, [r2, #-6]	@ zero_extendqisi2
	strb	r1, [r3, #-12]
	sub	r4, r2, #6
	ldrb	r6, [r4, #-1]	@ zero_extendqisi2
	sub	r1, r3, #12
	strb	r6, [r1, #-1]
	ldrb	r6, [r4, #-1]	@ zero_extendqisi2
	strb	r6, [r1, #-2]
	add	r5, r5, #4
	ldrb	r4, [r4, #-1]	@ zero_extendqisi2
	cmp	ip, r5
	strb	r4, [r1, #-3]
	sub	r3, r3, #16
	sub	r2, r2, #8
	bhi	.L623
	b	.L679
	.size	png_do_gray_to_rgb, .-png_do_gray_to_rgb
	.section	.text.png_do_rgb_to_gray,"ax",%progbits
	.align	2
	.global	png_do_rgb_to_gray
	.hidden	png_do_rgb_to_gray
	.type	png_do_rgb_to_gray, %function
png_do_rgb_to_gray:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
	ldrb	ip, [r1, #8]	@ zero_extendqisi2
	ands	r5, ip, #2
	mov	r3, r0
	sub	sp, sp, #32
	ldr	r4, [r1, #0]
	moveq	r0, r5
	beq	.L685
	mov	r6, #580
	add	r5, r6, #2
	ldrh	r6, [r3, r5]
	str	r6, [sp, #16]
	mov	r5, #584
	ldrh	r5, [r3, r5]
	mov	r0, #584
	str	r5, [sp, #12]
	add	r0, r0, #2
	ldrh	r0, [r3, r0]
	cmp	ip, #2
	str	r0, [sp, #4]
	movne	r0, #0
	beq	.L807
.L687:
	cmp	ip, #6
	beq	.L777
.L806:
	ldrb	r6, [r1, #9]	@ zero_extendqisi2
.L710:
	ldrb	r2, [r1, #10]	@ zero_extendqisi2
	sub	r3, r2, #2
	and	r3, r3, #255
	mul	r6, r3, r6
	and	r6, r6, #255
	cmp	r6, #7
	mulls	r4, r6, r4
	strb	r6, [r1, #11]
	movhi	r6, r6, lsr #3
	mulhi	r4, r6, r4
	addls	r4, r4, #7
	bic	r2, ip, #2
	movls	r4, r4, lsr #3
	strb	r2, [r1, #8]
	strb	r3, [r1, #10]
	str	r4, [r1, #4]
.L685:
	add	sp, sp, #32
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
	bx	lr
.L807:
	ldrb	r6, [r1, #9]	@ zero_extendqisi2
	cmp	r6, #8
	beq	.L808
	ldr	r0, [r3, #404]
	cmp	r0, #0
	beq	.L702
	ldr	r0, [r3, #400]
	cmp	r0, #0
	beq	.L702
	cmp	r4, #0
	beq	.L691
	mov	fp, #0
	mov	r5, r2
	mov	ip, r2
	mov	r6, fp
	str	fp, [sp, #20]
	str	r4, [sp, #8]
	str	r1, [sp, #24]
	str	r2, [sp, #28]
	b	.L706
.L705:
	add	r5, r5, #2
.L706:
	ldrb	r8, [ip, #0]	@ zero_extendqisi2
	ldrb	r2, [ip, #1]	@ zero_extendqisi2
	ldrb	r4, [ip, #2]	@ zero_extendqisi2
	ldrb	fp, [ip, #3]	@ zero_extendqisi2
	ldrb	r9, [ip, #4]	@ zero_extendqisi2
	ldrb	r0, [ip, #5]	@ zero_extendqisi2
	orr	r1, fp, r4, asl #8
	orr	sl, r2, r8, asl #8
	orr	fp, r0, r9, asl #8
	mov	r8, fp, asl #16
	mov	r2, sl, asl #16
	mov	sl, r1, asl #16
	mov	r7, r2, lsr #24
	mov	r1, sl, lsr #16
	mov	r2, r2, lsr #16
	mov	r0, r8, lsr #16
	mov	r9, r8, lsr #24
	mov	r4, sl, lsr #24
	cmp	r2, r0
	cmpeq	r2, r1
	mov	fp, r9, asl #1
	mov	r7, r7, asl #1
	mov	r4, r4, asl #1
	and	r9, r2, #255
	and	sl, r1, #255
	and	r8, r0, #255
	add	ip, ip, #6
	beq	.L704
	ldr	r2, [r3, #372]
	mov	r9, r9, asr r2
	mov	sl, sl, asr r2
	ldr	r1, [r3, #404]
	mov	r8, r8, asr r2
	ldr	r0, [r1, r9, asl #2]
	ldr	r9, [sp, #16]
	ldrh	r0, [r7, r0]
	mul	r0, r9, r0
	ldr	r7, [r1, sl, asl #2]
	ldrh	r4, [r4, r7]
	ldr	r7, [sp, #12]
	mla	r0, r7, r4, r0
	ldr	r9, [r1, r8, asl #2]
	ldr	r8, [sp, #4]
	ldrh	r4, [fp, r9]
	mla	r0, r8, r4, r0
	mov	r0, r0, lsr #15
	mov	r4, r0, asl #16
	mov	r1, r4, lsr #16
	and	r9, r1, #255
	mov	r2, r9, asr r2
	ldr	r1, [r3, #400]
	mov	r0, r4, lsr #24
	ldr	r9, [r1, r2, asl #2]
	mov	r1, r0, asl #1
	ldrh	r2, [r1, r9]
	mov	r9, #1
	str	r9, [sp, #20]
.L704:
	ldr	r4, [sp, #8]
	add	r6, r6, #1
	mov	r1, r2, lsr #8
	cmp	r4, r6
	strb	r1, [r5, #0]
	strb	r2, [r5, #1]
	bhi	.L705
	add	r0, sp, #20
	ldmia	r0, {r0, r1}	@ phole ldm
	ldr	r4, [sp, #8]
	ldr	r2, [sp, #28]
	ldrb	ip, [r1, #8]	@ zero_extendqisi2
	b	.L687
.L777:
	ldrb	r6, [r1, #9]	@ zero_extendqisi2
	cmp	r6, #8
	beq	.L809
	ldr	r5, [r3, #404]
	cmp	r5, #0
	beq	.L721
	ldr	r5, [r3, #400]
	cmp	r5, #0
	beq	.L721
	cmp	r4, #0
	beq	.L710
	mov	ip, r2
	mov	r5, #0
	str	r0, [sp, #8]
	str	r4, [sp, #20]
	str	r1, [sp, #24]
	b	.L724
.L784:
	add	r2, r2, #4
	add	ip, ip, #8
.L724:
	ldrb	r8, [ip, #0]	@ zero_extendqisi2
	ldrb	r6, [ip, #1]	@ zero_extendqisi2
	ldrb	r4, [ip, #2]	@ zero_extendqisi2
	ldrb	fp, [ip, #3]	@ zero_extendqisi2
	ldrb	r9, [ip, #4]	@ zero_extendqisi2
	ldrb	sl, [ip, #5]	@ zero_extendqisi2
	orr	r1, r6, r8, asl #8
	orr	r0, fp, r4, asl #8
	orr	r4, sl, r9, asl #8
	mov	r8, r4, asl #16
	mov	r9, r1, asl #16
	mov	sl, r0, asl #16
	mov	r1, r9, lsr #16
	mov	r0, sl, lsr #16
	mov	r4, r8, lsr #16
	mov	r7, r9, lsr #24
	mov	r6, sl, lsr #24
	mov	fp, r8, lsr #24
	cmp	r1, r4
	cmpeq	r1, r0
	mov	r7, r7, asl #1
	mov	r6, r6, asl #1
	mov	fp, fp, asl #1
	and	r9, r1, #255
	and	sl, r0, #255
	and	r8, r4, #255
	beq	.L723
	ldr	r1, [r3, #372]
	mov	r9, r9, asr r1
	mov	sl, sl, asr r1
	ldr	r0, [r3, #404]
	mov	r8, r8, asr r1
	ldr	r4, [r0, r9, asl #2]
	ldrh	r4, [r7, r4]
	ldr	r7, [sp, #16]
	mul	r4, r7, r4
	ldr	r9, [r0, sl, asl #2]
	ldrh	r6, [r6, r9]
	ldr	r9, [sp, #12]
	mla	r4, r9, r6, r4
	ldr	r0, [r0, r8, asl #2]
	ldr	r7, [sp, #4]
	ldrh	r6, [fp, r0]
	mla	r4, r7, r6, r4
	mov	r4, r4, lsr #15
	mov	r7, r4, asl #16
	mov	r0, r7, lsr #16
	and	r6, r0, #255
	mov	r1, r6, asr r1
	ldr	r0, [r3, #400]
	mov	r6, r7, lsr #24
	ldr	r7, [sp, #8]
	ldr	r1, [r0, r1, asl #2]
	mov	r4, r6, asl #1
	orr	r0, r7, #1
	ldrh	r1, [r4, r1]
	str	r0, [sp, #8]
.L723:
	mov	r6, r1, lsr #8
	strb	r6, [r2, #0]
	strb	r1, [r2, #1]
	ldrb	r0, [ip, #6]	@ zero_extendqisi2
	strb	r0, [r2, #2]
	ldr	r7, [sp, #20]
	add	r5, r5, #1
	ldrb	r1, [ip, #7]	@ zero_extendqisi2
	cmp	r7, r5
	strb	r1, [r2, #3]
	bhi	.L784
	ldr	r1, [sp, #24]
	ldr	r0, [sp, #8]
	ldr	r4, [sp, #20]
	ldrb	ip, [r1, #8]	@ zero_extendqisi2
	ldrb	r6, [r1, #9]	@ zero_extendqisi2
	b	.L710
.L808:
	ldr	sl, [r3, #388]
	cmp	sl, #0
	beq	.L692
	ldr	r5, [r3, #392]
	cmp	r5, #0
	beq	.L692
	cmp	r4, #0
	beq	.L691
	mov	r0, #0
	tst	r4, #1
	add	r7, r2, #1
	mov	ip, r2
	mov	r8, r0
	beq	.L773
	ldr	fp, [sp, #16]
	b	.L696
.L810:
	mul	r6, fp, r6
	ldr	r0, [sp, #12]
	mla	r6, r0, r5, r6
	ldr	r5, [sp, #4]
	mla	sl, r5, sl, r6
	ldr	r6, [r3, #388]
	ldrb	r0, [r6, sl, lsr #15]	@ zero_extendqisi2
	strb	r0, [r7, #-1]
	mov	r0, #1
.L694:
	add	r8, r8, #1
	cmp	r4, r8
	bls	.L804
	ldr	r5, [r3, #392]
	ldrb	r6, [ip, #0]	@ zero_extendqisi2
	ldrb	r9, [ip, #1]	@ zero_extendqisi2
	ldrb	sl, [ip, #2]	@ zero_extendqisi2
	ldrb	r6, [r5, r6]	@ zero_extendqisi2
	ldrb	sl, [r5, sl]	@ zero_extendqisi2
	ldrb	r5, [r5, r9]	@ zero_extendqisi2
	cmp	r6, r5
	cmpeq	r6, sl
	add	r7, r7, #1
	add	ip, ip, #3
	beq	.L751
	mul	r6, fp, r6
	ldr	r9, [sp, #12]
	mla	r6, r9, r5, r6
	ldr	r0, [sp, #4]
	mla	sl, r0, sl, r6
	ldr	r9, [r3, #388]
	ldrb	r6, [r9, sl, lsr #15]	@ zero_extendqisi2
	strb	r6, [r7, #-1]
	mov	r0, #1
.L788:
	ldr	r5, [r3, #392]
	add	r8, r8, #1
	add	r7, r7, #1
.L696:
	ldrb	r6, [ip, #0]	@ zero_extendqisi2
	ldrb	r9, [ip, #1]	@ zero_extendqisi2
	ldrb	sl, [ip, #2]	@ zero_extendqisi2
	ldrb	r6, [r5, r6]	@ zero_extendqisi2
	ldrb	sl, [r5, sl]	@ zero_extendqisi2
	ldrb	r5, [r5, r9]	@ zero_extendqisi2
	cmp	r6, r5
	cmpeq	r6, sl
	add	ip, ip, #3
	bne	.L810
	ldrb	r5, [ip, #-1]	@ zero_extendqisi2
	strb	r5, [r7, #-1]
	b	.L694
.L751:
	ldrb	r5, [ip, #-1]	@ zero_extendqisi2
	strb	r5, [r7, #-1]
	b	.L788
.L702:
	cmp	r4, #0
	beq	.L691
	mov	r0, #0
	mov	r6, r2
	mov	ip, r2
	mov	r7, r0
	ldr	fp, [sp, #4]
	ldr	sl, [sp, #12]
	ldr	r9, [sp, #16]
	str	r3, [sp, #8]
	str	r1, [sp, #20]
	str	r2, [sp, #24]
	b	.L709
.L811:
	add	r6, r6, #2
.L709:
	ldrb	r2, [ip, #0]	@ zero_extendqisi2
	ldrb	r3, [ip, #1]	@ zero_extendqisi2
	orr	r3, r3, r2, asl #8
	mul	r1, r9, r3
	ldrb	r5, [ip, #2]	@ zero_extendqisi2
	ldrb	r8, [ip, #3]	@ zero_extendqisi2
	orr	r2, r8, r5, asl #8
	mla	r1, sl, r2, r1
	ldrb	r8, [ip, #4]	@ zero_extendqisi2
	ldrb	r5, [ip, #5]	@ zero_extendqisi2
	orr	r5, r5, r8, asl #8
	mla	r1, fp, r5, r1
	mov	r1, r1, lsr #15
	cmp	r3, r2
	cmpeq	r3, r5
	mov	r1, r1, asl #16
	add	r7, r7, #1
	movne	r0, #1
	mov	r3, r1, lsr #24
	mov	r2, r1, lsr #16
	cmp	r4, r7
	strb	r3, [r6, #0]
	strb	r2, [r6, #1]
	add	ip, ip, #6
	bhi	.L811
	ldr	r3, [sp, #8]
	add	r1, sp, #20
	ldmia	r1, {r1, r2}	@ phole ldm
.L804:
	ldrb	ip, [r1, #8]	@ zero_extendqisi2
	b	.L687
.L809:
	ldr	sl, [r3, #388]
	cmp	sl, #0
	beq	.L714
	ldr	r7, [r3, #392]
	cmp	r7, #0
	beq	.L714
	cmp	r4, #0
	beq	.L710
	tst	r4, #1
	mov	r5, r2
	mov	ip, r2
	mov	r6, #0
	beq	.L778
	mov	fp, r6
	str	r1, [sp, #8]
	b	.L716
.L779:
	ldrb	r7, [ip, #4]	@ zero_extendqisi2
	ldrb	sl, [r2, #1]	@ zero_extendqisi2
	ldr	ip, [r3, #392]
	ldrb	r6, [r2, #2]	@ zero_extendqisi2
	ldrb	r1, [ip, r7]	@ zero_extendqisi2
	ldrb	r6, [ip, r6]	@ zero_extendqisi2
	ldrb	ip, [ip, sl]	@ zero_extendqisi2
	cmp	r1, ip
	cmpeq	r1, r6
	add	r5, r5, #2
	ldr	r7, [r3, #388]
	orrne	r0, r0, #1
.L739:
	ldr	sl, [sp, #16]
	mul	r1, sl, r1
	ldr	r9, [sp, #12]
	mla	r1, r9, ip, r1
	ldr	r8, [sp, #4]
	mla	r1, r8, r6, r1
	ldrb	sl, [r7, r1, lsr #15]	@ zero_extendqisi2
	strb	sl, [r5, #0]
	ldrb	r1, [r2, #3]	@ zero_extendqisi2
	strb	r1, [r5, #1]
	ldr	r7, [r3, #392]
	ldr	sl, [r3, #388]
	add	fp, fp, #1
	add	r5, r5, #2
	add	ip, r2, #4
.L716:
	ldrb	r8, [ip, #0]	@ zero_extendqisi2
	ldrb	r1, [ip, #2]	@ zero_extendqisi2
	ldrb	r6, [r7, r1]	@ zero_extendqisi2
	ldrb	r1, [r7, r8]	@ zero_extendqisi2
	ldr	r8, [sp, #16]
	mul	r8, r1, r8
	ldrb	r9, [ip, #1]	@ zero_extendqisi2
	ldrb	r7, [r7, r9]	@ zero_extendqisi2
	ldr	r9, [sp, #12]
	mla	r8, r9, r7, r8
	ldr	r9, [sp, #4]
	mla	r8, r9, r6, r8
	cmp	r1, r7
	cmpeq	r1, r6
	ldrb	r1, [sl, r8, lsr #15]	@ zero_extendqisi2
	strb	r1, [r5, #0]
	add	fp, fp, #1
	ldrb	r6, [ip, #3]	@ zero_extendqisi2
	orrne	r0, r0, #1
	cmp	r4, fp
	add	r2, ip, #4
	strb	r6, [r5, #1]
	bhi	.L779
.L805:
	ldr	r1, [sp, #8]
	ldrb	ip, [r1, #8]	@ zero_extendqisi2
	b	.L806
.L721:
	cmp	r4, #0
	beq	.L710
	mov	r3, r2
	mov	r5, #0
	ldr	fp, [sp, #12]
	ldr	r9, [sp, #16]
	str	r1, [sp, #8]
	b	.L726
.L785:
	add	r2, r2, #4
	add	r3, r3, #8
.L726:
	ldrb	r7, [r3, #0]	@ zero_extendqisi2
	ldrb	r1, [r3, #1]	@ zero_extendqisi2
	orr	r1, r1, r7, asl #8
	mul	r7, r9, r1
	ldrb	sl, [r3, #2]	@ zero_extendqisi2
	ldrb	r8, [r3, #3]	@ zero_extendqisi2
	orr	r8, r8, sl, asl #8
	mla	r7, fp, r8, r7
	ldrb	r6, [r3, #4]	@ zero_extendqisi2
	ldrb	ip, [r3, #5]	@ zero_extendqisi2
	orr	ip, ip, r6, asl #8
	ldr	r6, [sp, #4]
	mla	r7, r6, ip, r7
	mov	r6, r7, lsr #15
	mov	r7, r6, asl #16
	mov	r6, r7, lsr #24
	mov	r7, r7, lsr #16
	strb	r6, [r2, #0]
	strb	r7, [r2, #1]
	cmp	r1, r8
	cmpeq	r1, ip
	ldrb	r1, [r3, #6]	@ zero_extendqisi2
	strb	r1, [r2, #2]
	add	r5, r5, #1
	ldrb	ip, [r3, #7]	@ zero_extendqisi2
	orrne	r0, r0, #1
	cmp	r4, r5
	strb	ip, [r2, #3]
	bhi	.L785
	b	.L805
.L778:
	ldrb	ip, [r2, #0]	@ zero_extendqisi2
	ldrb	r6, [r2, #1]	@ zero_extendqisi2
	ldrb	r5, [r2, #2]	@ zero_extendqisi2
	ldrb	r9, [r7, ip]	@ zero_extendqisi2
	ldrb	fp, [r7, r5]	@ zero_extendqisi2
	ldrb	r6, [r7, r6]	@ zero_extendqisi2
	ldr	r7, [sp, #16]
	cmp	r9, r6
	cmpeq	r9, fp
	mul	r9, r7, r9
	ldr	r8, [sp, #12]
	mla	r6, r8, r6, r9
	ldr	ip, [sp, #4]
	mla	fp, ip, fp, r6
	ldrb	r5, [sl, fp, lsr #15]	@ zero_extendqisi2
	ldrb	sl, [r2, #3]	@ zero_extendqisi2
	strb	r5, [r2, #0]
	strb	sl, [r2, #1]
	mov	r9, #1
	ldr	r7, [r3, #392]
	ldr	sl, [r3, #388]
	orrne	r0, r0, #1
	add	ip, r2, #4
	add	r5, r2, #2
	mov	fp, r9
	str	r1, [sp, #8]
	b	.L716
.L773:
	ldrb	ip, [r2, #2]	@ zero_extendqisi2
	ldrb	r6, [r2, #0]	@ zero_extendqisi2
	ldrb	fp, [r2, #1]	@ zero_extendqisi2
	ldrb	r8, [r5, ip]	@ zero_extendqisi2
	ldrb	r6, [r5, r6]	@ zero_extendqisi2
	ldrb	r5, [r5, fp]	@ zero_extendqisi2
	cmp	r6, r5
	cmpeq	r6, r8
	add	ip, r2, #3
	bne	.L786
	ldrb	sl, [ip, #-1]	@ zero_extendqisi2
	strb	sl, [r7, #-1]
.L787:
	add	r7, r7, #1
	mov	r8, #1
	ldr	r5, [r3, #392]
	ldr	fp, [sp, #16]
	b	.L696
.L786:
	ldr	r0, [sp, #16]
	mul	r6, r0, r6
	ldr	fp, [sp, #12]
	mla	r5, fp, r5, r6
	ldr	r9, [sp, #4]
	mla	r8, r9, r8, r5
	ldrb	r0, [sl, r8, lsr #15]	@ zero_extendqisi2
	strb	r0, [r7, #-1]
	mov	r0, #1
	b	.L787
.L691:
	mov	r0, #0
	b	.L710
.L692:
	cmp	r4, #0
	beq	.L691
	ldrb	r5, [r2, #0]	@ zero_extendqisi2
	ldrb	r7, [r2, #1]	@ zero_extendqisi2
	ldrb	r8, [r2, #2]	@ zero_extendqisi2
	sub	ip, r4, #1
	cmp	r5, r7
	cmpeq	r5, r8
	and	r6, ip, #3
	mov	r0, #0
	add	ip, r2, #3
	beq	.L753
	ldr	r0, [sp, #16]
	mul	r0, r5, r0
	ldr	r5, [sp, #12]
	mla	r0, r5, r7, r0
	ldr	r7, [sp, #4]
	mla	r0, r7, r8, r0
	mov	r0, r0, lsr #15
	strb	r0, [r2, #0]
	mov	r0, #1
.L789:
	mov	r5, #1
	cmp	r4, r5
	bls	.L804
	cmp	r6, #0
	beq	.L803
	cmp	r6, r5
	beq	.L771
	cmp	r6, #2
	beq	.L772
	ldrb	r6, [ip, #0]	@ zero_extendqisi2
	ldrb	r7, [ip, #1]	@ zero_extendqisi2
	ldrb	r8, [ip, #2]	@ zero_extendqisi2
	cmp	r6, r7
	cmpeq	r6, r8
	add	ip, ip, #3
	bne	.L791
	ldrb	r7, [ip, #-1]	@ zero_extendqisi2
	strb	r7, [r2, r5]
.L792:
	add	r5, r5, #1
.L772:
	ldrb	r6, [ip, #0]	@ zero_extendqisi2
	ldrb	r7, [ip, #1]	@ zero_extendqisi2
	ldrb	r8, [ip, #2]	@ zero_extendqisi2
	cmp	r6, r7
	cmpeq	r6, r8
	add	ip, ip, #3
	bne	.L793
	ldrb	r7, [ip, #-1]	@ zero_extendqisi2
	strb	r7, [r2, r5]
.L794:
	add	r5, r5, #1
.L771:
	ldrb	r6, [ip, #0]	@ zero_extendqisi2
	ldrb	r7, [ip, #1]	@ zero_extendqisi2
	ldrb	r8, [ip, #2]	@ zero_extendqisi2
	cmp	r6, r7
	cmpeq	r6, r8
	add	ip, ip, #3
	bne	.L795
	ldrb	r6, [ip, #-1]	@ zero_extendqisi2
	strb	r6, [r2, r5]
.L796:
	add	r5, r5, #1
	cmp	r4, r5
	bls	.L804
.L803:
	ldr	fp, [sp, #16]
	b	.L699
.L812:
	mul	r7, fp, r7
	ldr	r0, [sp, #12]
	mla	r0, sl, r0, r7
	ldr	r7, [sp, #4]
	mla	r0, r7, r8, r0
	mov	r9, r0, lsr #15
	strb	r9, [r2, r5]
	mov	r0, #1
.L797:
	ldrb	r7, [ip, #3]	@ zero_extendqisi2
	ldrb	r8, [r6, #2]	@ zero_extendqisi2
	ldrb	sl, [r6, #1]	@ zero_extendqisi2
	cmp	r7, sl
	cmpeq	r7, r8
	mulne	r7, fp, r7
	ldrne	r0, [sp, #12]
	mlane	r7, r0, sl, r7
	ldrne	r0, [sp, #4]
	mlane	r0, r8, r0, r7
	add	r6, ip, #6
	ldreqb	r7, [r6, #-1]	@ zero_extendqisi2
	movne	r0, r0, lsr #15
	add	r9, r5, #1
	strneb	r0, [r2, r9]
	streqb	r7, [r2, r9]
	ldrb	r7, [ip, #6]	@ zero_extendqisi2
	ldrb	r8, [r6, #2]	@ zero_extendqisi2
	ldrb	r6, [r6, #1]	@ zero_extendqisi2
	movne	r0, #1
	cmp	r7, r6
	cmpeq	r7, r8
	mulne	r7, fp, r7
	ldrne	r0, [sp, #12]
	mlane	r0, r6, r0, r7
	ldrne	r6, [sp, #4]
	mlane	r0, r6, r8, r0
	add	ip, ip, #9
	ldreqb	r6, [ip, #-1]	@ zero_extendqisi2
	add	sl, r5, #2
	movne	r0, r0, lsr #15
	add	r5, r5, #3
	strneb	r0, [r2, sl]
	streqb	r6, [r2, sl]
	movne	r0, #1
	cmp	r4, r5
	bls	.L804
.L699:
	ldrb	r9, [ip, #0]	@ zero_extendqisi2
	ldrb	r8, [ip, #1]	@ zero_extendqisi2
	ldrb	r7, [ip, #2]	@ zero_extendqisi2
	cmp	r9, r8
	cmpeq	r9, r7
	mulne	r0, fp, r9
	ldrne	r9, [sp, #12]
	mlane	r0, r9, r8, r0
	ldrne	r8, [sp, #4]
	mlane	r0, r8, r7, r0
	add	ip, ip, #3
	ldreqb	r9, [ip, #-1]	@ zero_extendqisi2
	movne	r0, r0, lsr #15
	strneb	r0, [r2, r5]
	streqb	r9, [r2, r5]
	ldrb	r7, [ip, #0]	@ zero_extendqisi2
	ldrb	sl, [ip, #1]	@ zero_extendqisi2
	ldrb	r8, [ip, #2]	@ zero_extendqisi2
	movne	r0, #1
	cmp	r7, sl
	cmpeq	r7, r8
	add	r5, r5, #1
	add	r6, ip, #3
	bne	.L812
	ldrb	sl, [r6, #-1]	@ zero_extendqisi2
	strb	sl, [r2, r5]
	b	.L797
.L714:
	cmp	r4, #0
	beq	.L710
	tst	r4, #1
	mov	ip, r2
	mov	r3, r2
	mov	r9, #0
	beq	.L781
	ldr	fp, [sp, #4]
	add	r8, sp, #12
	ldmia	r8, {r8, sl}	@ phole ldm
	str	r1, [sp, #8]
	b	.L718
.L782:
	ldrb	r3, [r3, #4]	@ zero_extendqisi2
	ldrb	r5, [r2, #1]	@ zero_extendqisi2
	ldrb	r1, [r2, #2]	@ zero_extendqisi2
	cmp	r3, r5
	cmpeq	r3, r1
	add	ip, ip, #2
	orrne	r0, r0, #1
.L745:
	mul	r3, sl, r3
	mla	r5, r8, r5, r3
	mla	r1, fp, r1, r5
	mov	r3, r1, lsr #15
	strb	r3, [ip, #0]
	ldrb	r1, [r2, #3]	@ zero_extendqisi2
	add	r9, r9, #1
	strb	r1, [ip, #1]
	add	r3, r2, #4
	add	ip, ip, #2
.L718:
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	mul	r7, sl, r1
	ldrb	r5, [r3, #1]	@ zero_extendqisi2
	mla	r7, r8, r5, r7
	ldrb	r6, [r3, #2]	@ zero_extendqisi2
	mla	r7, fp, r6, r7
	mov	r7, r7, lsr #15
	strb	r7, [ip, #0]
	cmp	r1, r5
	cmpeq	r1, r6
	add	r9, r9, #1
	ldrb	r1, [r3, #3]	@ zero_extendqisi2
	orrne	r0, r0, #1
	cmp	r4, r9
	add	r2, r3, #4
	strb	r1, [ip, #1]
	bhi	.L782
	b	.L805
.L781:
	ldrb	r8, [r2, #0]	@ zero_extendqisi2
	ldrb	r5, [r2, #1]	@ zero_extendqisi2
	ldr	r9, [sp, #16]
	ldrb	ip, [r2, #2]	@ zero_extendqisi2
	cmp	r8, r5
	cmpeq	r8, ip
	mul	r8, r9, r8
	ldr	r6, [sp, #12]
	mla	r5, r6, r5, r8
	ldr	r7, [sp, #4]
	mla	ip, r7, ip, r5
	ldrb	r3, [r2, #3]	@ zero_extendqisi2
	mov	fp, ip, lsr #15
	strb	r3, [r2, #1]
	strb	fp, [r2, #0]
	orrne	r0, r0, #1
	add	r3, r2, #4
	mov	r9, #1
	add	ip, r2, #2
	ldr	fp, [sp, #4]
	add	r8, sp, #12
	ldmia	r8, {r8, sl}	@ phole ldm
	str	r1, [sp, #8]
	b	.L718
.L753:
	ldrb	r5, [ip, #-1]	@ zero_extendqisi2
	strb	r5, [r2, #0]
	b	.L789
.L795:
	ldr	r0, [sp, #16]
	mul	r0, r6, r0
	ldr	r9, [sp, #12]
	mla	r0, r9, r7, r0
	ldr	r6, [sp, #4]
	mla	r0, r6, r8, r0
	mov	r0, r0, lsr #15
	strb	r0, [r2, r5]
	mov	r0, #1
	b	.L796
.L793:
	ldr	r0, [sp, #16]
	mul	r0, r6, r0
	ldr	r9, [sp, #12]
	mla	r0, r9, r7, r0
	ldr	r6, [sp, #4]
	mla	r0, r6, r8, r0
	mov	r8, r0, lsr #15
	strb	r8, [r2, r5]
	mov	r0, #1
	b	.L794
.L791:
	ldr	r0, [sp, #16]
	mul	r0, r6, r0
	ldr	r9, [sp, #12]
	mla	r0, r9, r7, r0
	ldr	r6, [sp, #4]
	mla	r0, r6, r8, r0
	mov	r8, r0, lsr #15
	strb	r8, [r2, r5]
	mov	r0, r5
	b	.L792
	.size	png_do_rgb_to_gray, .-png_do_rgb_to_gray
	.section	.text.png_build_grayscale_palette,"ax",%progbits
	.align	2
	.global	png_build_grayscale_palette
	.hidden	png_build_grayscale_palette
	.type	png_build_grayscale_palette, %function
png_build_grayscale_palette:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
	cmp	r1, #0
	sub	sp, sp, #16
	beq	.L816
	sub	r0, r0, #1
	cmp	r0, #7
	bhi	.L816
	ldr	ip, .L833
	ldr	r3, [ip, r0, asl #2]
	add	r2, ip, r0, asl #2
	cmp	r3, #0
	ldr	r2, [r2, #32]
	ble	.L816
	add	r3, r3, r3, asl #1
	str	r3, [sp, #8]
	ldr	r0, [sp, #8]
	sub	ip, r3, #3
	mov	r3, #3
	add	sl, ip, ip, asl #1
	cmp	r3, r0
	mov	r0, #0
	and	ip, sl, r3
	strb	r0, [r1, #2]
	strb	r0, [r1, r0]
	strb	r0, [r1, #1]
	mov	sl, r2
	beq	.L816
	cmp	ip, #0
	beq	.L815
	cmp	ip, #1
	beq	.L829
	cmp	ip, #2
	beq	.L830
	mov	r3, r1
	and	ip, r2, #255
	strb	ip, [r3, #3]!
	mov	sl, r2, asl #1
	strb	ip, [r3, #2]
	strb	ip, [r3, #1]
	mov	r3, #6
.L830:
	and	r4, sl, #255
	mov	r0, r1
	strb	r4, [r0, r3]!
	strb	r4, [r0, #2]
	strb	r4, [r0, #1]
	add	sl, sl, r2
	add	r3, r3, #3
.L829:
	and	ip, sl, #255
	mov	r0, r1
	strb	ip, [r0, r3]!
	ldr	r4, [sp, #8]
	add	r3, r3, #3
	cmp	r3, r4
	strb	ip, [r0, #2]
	strb	ip, [r0, #1]
	add	sl, sl, r2
	beq	.L816
.L815:
	add	r8, sl, r2
	add	r7, r8, r2
	add	r4, r7, r2
	str	r4, [sp, #4]
	and	sl, sl, #255
	mov	r5, r1
	and	r8, r8, #255
	mov	r4, r1
	add	fp, r3, #3
	add	r9, r3, #6
	ldr	r0, [sp, #4]
	str	r9, [sp, #12]
	strb	sl, [r5, r3]!
	strb	r8, [r4, fp]!
	ldr	fp, [sp, #12]
	and	r6, r0, #255
	and	r7, r7, #255
	mov	ip, r1
	mov	r0, r1
	add	r9, r3, #9
	strb	r7, [ip, fp]!
	strb	r6, [r0, r9]!
	ldr	r9, [sp, #8]
	strb	sl, [r5, #2]
	strb	sl, [r5, #1]
	strb	r8, [r4, #2]
	strb	r7, [ip, #2]
	strb	r6, [r0, #2]
	ldr	fp, [sp, #4]
	add	r3, r3, #12
	cmp	r3, r9
	strb	r8, [r4, #1]
	add	sl, fp, r2
	strb	r7, [ip, #1]
	strb	r6, [r0, #1]
	bne	.L815
.L816:
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
	bx	lr
.L834:
	.align	2
.L833:
	.word	.LANCHOR0
	.size	png_build_grayscale_palette, .-png_build_grayscale_palette
	.section	.text.png_do_gamma,"ax",%progbits
	.align	2
	.global	png_do_gamma
	.hidden	png_do_gamma
	.type	png_do_gamma, %function
png_do_gamma:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
	ldrb	ip, [r0, #9]	@ zero_extendqisi2
	subs	r4, r2, #0
	movne	r4, #1
	cmp	ip, #8
	movhi	r4, #0
	sub	sp, sp, #8
	cmp	r4, #0
	ldr	r5, [r0, #0]
	ldr	r4, [sp, #40]
	bne	.L836
	subs	r6, r3, #0
	movne	r6, #1
	cmp	ip, #16
	movne	r6, #0
	cmp	r6, #0
	beq	.L862
.L836:
	ldrb	r6, [r0, #8]	@ zero_extendqisi2
	cmp	r6, #6
	ldrls	pc, [pc, r6, asl #2]
	b	.L862
.L842:
	.word	.L838
	.word	.L862
	.word	.L839
	.word	.L862
	.word	.L840
	.word	.L862
	.word	.L841
.L838:
	cmp	ip, #2
	beq	.L993
.L855:
	cmp	ip, #4
	beq	.L994
	cmp	ip, #8
	beq	.L995
	cmp	ip, #16
	beq	.L996
.L862:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
	bx	lr
.L841:
	cmp	ip, #8
	beq	.L847
	cmp	r5, #0
	beq	.L862
	ldrb	r6, [r1, #1]	@ zero_extendqisi2
	mov	r2, r6, asr r4
	ldrb	ip, [r1, #3]	@ zero_extendqisi2
	mov	ip, ip, asr r4
	ldrb	r0, [r1, #5]	@ zero_extendqisi2
	mov	r0, r0, asr r4
	ldr	r2, [r3, r2, asl #2]
	ldrb	r6, [r1, #0]	@ zero_extendqisi2
	add	r2, r2, r6, asl #1
	ldrh	r2, [r2, #0]
	mov	r6, r2, lsr #8
	strb	r6, [r1, #0]
	strb	r2, [r1, #1]
	ldr	r2, [r3, ip, asl #2]
	ldrb	ip, [r1, #2]	@ zero_extendqisi2
	add	r2, r2, ip, asl #1
	ldrh	r2, [r2, #0]
	mov	ip, r2, lsr #8
	strb	r2, [r1, #3]
	strb	ip, [r1, #2]
	ldr	ip, [r3, r0, asl #2]
	ldrb	r0, [r1, #4]	@ zero_extendqisi2
	add	r2, ip, r0, asl #1
	ldrh	r2, [r2, #0]
	sub	r0, r5, #1
	mov	ip, r2, lsr #8
	cmp	r5, #1
	strb	r2, [r1, #5]
	strb	ip, [r1, #4]
	and	r2, r0, #1
	add	r1, r1, #8
	mov	r0, #1
	bls	.L862
	cmp	r2, #0
	beq	.L850
	ldrb	r6, [r1, #1]	@ zero_extendqisi2
	mov	r2, r6, asr r4
	ldrb	ip, [r1, #3]	@ zero_extendqisi2
	mov	ip, ip, asr r4
	ldrb	r0, [r1, #5]	@ zero_extendqisi2
	mov	r0, r0, asr r4
	ldr	r2, [r3, r2, asl #2]
	ldrb	r6, [r1, #0]	@ zero_extendqisi2
	add	r2, r2, r6, asl #1
	ldrh	r2, [r2, #0]
	mov	r6, r2, lsr #8
	strb	r6, [r1, #0]
	strb	r2, [r1, #1]
	ldr	r2, [r3, ip, asl #2]
	ldrb	ip, [r1, #2]	@ zero_extendqisi2
	add	r2, r2, ip, asl #1
	ldrh	r2, [r2, #0]
	mov	ip, r2, lsr #8
	strb	r2, [r1, #3]
	strb	ip, [r1, #2]
	ldr	ip, [r3, r0, asl #2]
	ldrb	r0, [r1, #4]	@ zero_extendqisi2
	add	r2, ip, r0, asl #1
	ldrh	r2, [r2, #0]
	mov	r0, #2
	mov	ip, r2, lsr #8
	cmp	r5, r0
	strb	ip, [r1, #4]
	strb	r2, [r1, #5]
	add	r1, r1, #8
	bls	.L862
.L850:
	ldrb	ip, [r1, #1]	@ zero_extendqisi2
	mov	r2, ip, asr r4
	ldrb	r7, [r1, #3]	@ zero_extendqisi2
	mov	r7, r7, asr r4
	ldrb	r6, [r1, #5]	@ zero_extendqisi2
	mov	r6, r6, asr r4
	ldrb	ip, [r1, #0]	@ zero_extendqisi2
	ldr	r2, [r3, r2, asl #2]
	mov	ip, ip, asl #1
	ldrh	r2, [ip, r2]
	mov	ip, r2, lsr #8
	strb	ip, [r1, #0]
	strb	r2, [r1, #1]
	ldrb	ip, [r1, #2]	@ zero_extendqisi2
	ldr	r2, [r3, r7, asl #2]
	mov	ip, ip, asl #1
	ldrh	r2, [ip, r2]
	mov	ip, r2, lsr #8
	strb	ip, [r1, #2]
	strb	r2, [r1, #3]
	ldrb	ip, [r1, #4]	@ zero_extendqisi2
	ldr	r2, [r3, r6, asl #2]
	mov	r6, ip, asl #1
	ldrh	r2, [r6, r2]
	mov	ip, r2, lsr #8
	strb	ip, [r1, #4]
	strb	r2, [r1, #5]
	add	r2, r1, #8
	ldrb	r6, [r2, #1]	@ zero_extendqisi2
	mov	ip, r6, asr r4
	ldrb	r6, [r1, #8]	@ zero_extendqisi2
	ldr	ip, [r3, ip, asl #2]
	mov	r6, r6, asl #1
	ldrh	ip, [r6, ip]
	mov	r6, ip, lsr #8
	strb	r6, [r1, #8]
	ldrb	r1, [r2, #3]	@ zero_extendqisi2
	mov	r1, r1, asr r4
	ldrb	r6, [r2, #5]	@ zero_extendqisi2
	mov	r6, r6, asr r4
	strb	ip, [r2, #1]
	ldrb	ip, [r2, #2]	@ zero_extendqisi2
	ldr	r1, [r3, r1, asl #2]
	mov	ip, ip, asl #1
	ldrh	r1, [ip, r1]
	mov	ip, r1, lsr #8
	strb	ip, [r2, #2]
	strb	r1, [r2, #3]
	ldrb	ip, [r2, #4]	@ zero_extendqisi2
	ldr	r1, [r3, r6, asl #2]
	mov	ip, ip, asl #1
	ldrh	r1, [ip, r1]
	add	r0, r0, #2
	mov	ip, r1, lsr #8
	cmp	r5, r0
	strb	ip, [r2, #4]
	strb	r1, [r2, #5]
	add	r1, r2, #8
	bhi	.L850
	b	.L862
.L839:
	cmp	ip, #8
	beq	.L843
	cmp	r5, #0
	beq	.L862
	ldrb	r6, [r1, #1]	@ zero_extendqisi2
	mov	r2, r6, asr r4
	ldrb	ip, [r1, #3]	@ zero_extendqisi2
	mov	ip, ip, asr r4
	ldrb	r0, [r1, #5]	@ zero_extendqisi2
	mov	r0, r0, asr r4
	ldr	r2, [r3, r2, asl #2]
	ldrb	r6, [r1, #0]	@ zero_extendqisi2
	add	r2, r2, r6, asl #1
	ldrh	r2, [r2, #0]
	mov	r6, r2, lsr #8
	strb	r6, [r1, #0]
	strb	r2, [r1, #1]
	ldr	r2, [r3, ip, asl #2]
	ldrb	ip, [r1, #2]	@ zero_extendqisi2
	add	r2, r2, ip, asl #1
	ldrh	r2, [r2, #0]
	mov	ip, r2, lsr #8
	strb	r2, [r1, #3]
	strb	ip, [r1, #2]
	ldr	ip, [r3, r0, asl #2]
	ldrb	r0, [r1, #4]	@ zero_extendqisi2
	add	r2, ip, r0, asl #1
	ldrh	r2, [r2, #0]
	sub	r0, r5, #1
	mov	ip, r2, lsr #8
	cmp	r5, #1
	strb	ip, [r1, #4]
	strb	r2, [r1, #5]
	and	ip, r0, #1
	add	r2, r1, #6
	mov	r0, #1
	bls	.L862
	cmp	ip, #0
	beq	.L846
	ldrb	r7, [r2, #1]	@ zero_extendqisi2
	mov	r0, r7, asr r4
	ldrb	r6, [r2, #3]	@ zero_extendqisi2
	mov	r6, r6, asr r4
	ldrb	ip, [r2, #5]	@ zero_extendqisi2
	mov	ip, ip, asr r4
	ldr	r0, [r3, r0, asl #2]
	ldrb	r7, [r2, #0]	@ zero_extendqisi2
	add	r0, r0, r7, asl #1
	ldrh	r0, [r0, #0]
	mov	r7, r0, lsr #8
	strb	r7, [r2, #0]
	strb	r0, [r2, #1]
	ldr	r0, [r3, r6, asl #2]
	ldrb	r6, [r2, #2]	@ zero_extendqisi2
	add	r0, r0, r6, asl #1
	ldrh	r0, [r0, #0]
	mov	r6, r0, lsr #8
	strb	r6, [r2, #2]
	strb	r0, [r2, #3]
	ldr	r0, [r3, ip, asl #2]
	ldrb	ip, [r2, #4]	@ zero_extendqisi2
	add	r6, r0, ip, asl #1
	ldrh	ip, [r6, #0]
	mov	r0, #2
	mov	r6, ip, lsr #8
	cmp	r5, r0
	strb	ip, [r2, #5]
	strb	r6, [r2, #4]
	add	r2, r1, #12
	bls	.L862
.L846:
	ldrb	ip, [r2, #1]	@ zero_extendqisi2
	mov	r1, ip, asr r4
	ldrb	r7, [r2, #3]	@ zero_extendqisi2
	mov	r7, r7, asr r4
	ldrb	r6, [r2, #5]	@ zero_extendqisi2
	mov	r6, r6, asr r4
	ldrb	r8, [r2, #0]	@ zero_extendqisi2
	ldr	r1, [r3, r1, asl #2]
	mov	ip, r8, asl #1
	ldrh	r8, [ip, r1]
	mov	ip, r8, lsr #8
	strb	ip, [r2, #0]
	strb	r8, [r2, #1]
	ldrb	r1, [r2, #2]	@ zero_extendqisi2
	ldr	r8, [r3, r7, asl #2]
	mov	ip, r1, asl #1
	ldrh	r1, [ip, r8]
	mov	r7, r1, lsr #8
	strb	r7, [r2, #2]
	strb	r1, [r2, #3]
	ldrb	ip, [r2, #4]	@ zero_extendqisi2
	ldr	r8, [r3, r6, asl #2]
	mov	r6, ip, asl #1
	ldrh	r1, [r6, r8]
	mov	r7, r1, lsr #8
	strb	r7, [r2, #4]
	strb	r1, [r2, #5]
	add	r1, r2, #6
	ldrb	ip, [r1, #1]	@ zero_extendqisi2
	mov	r6, ip, asr r4
	ldrb	r8, [r2, #6]	@ zero_extendqisi2
	ldr	ip, [r3, r6, asl #2]
	mov	r7, r8, asl #1
	ldrh	ip, [r7, ip]
	mov	r6, ip, lsr #8
	strb	r6, [r2, #6]
	ldrb	r8, [r1, #3]	@ zero_extendqisi2
	mov	r8, r8, asr r4
	ldrb	r7, [r1, #5]	@ zero_extendqisi2
	mov	r7, r7, asr r4
	strb	ip, [r1, #1]
	ldrb	r6, [r1, #2]	@ zero_extendqisi2
	ldr	ip, [r3, r8, asl #2]
	mov	r6, r6, asl #1
	ldrh	ip, [r6, ip]
	mov	r6, ip, lsr #8
	strb	r6, [r1, #2]
	strb	ip, [r1, #3]
	ldrb	r6, [r1, #4]	@ zero_extendqisi2
	ldr	ip, [r3, r7, asl #2]
	mov	r6, r6, asl #1
	ldrh	ip, [r6, ip]
	add	r0, r0, #2
	mov	r6, ip, lsr #8
	cmp	r5, r0
	strb	r6, [r1, #4]
	add	r2, r2, #12
	strb	ip, [r1, #5]
	bhi	.L846
	b	.L862
.L840:
	cmp	ip, #8
	beq	.L851
	cmp	r5, #0
	beq	.L862
	ldrb	ip, [r1, #1]	@ zero_extendqisi2
	mov	r2, ip, asr r4
	ldrb	r0, [r1, #0]	@ zero_extendqisi2
	ldr	ip, [r3, r2, asl #2]
	add	r2, ip, r0, asl #1
	ldrh	r2, [r2, #0]
	sub	r0, r5, #1
	mov	ip, r2, lsr #8
	cmp	r5, #1
	strb	r2, [r1, #1]
	strb	ip, [r1, #0]
	and	r2, r0, #3
	add	r1, r1, #4
	mov	r0, #1
	bls	.L862
	cmp	r2, #0
	beq	.L854
	cmp	r2, #1
	beq	.L971
	cmp	r2, #2
	beq	.L972
	ldrb	r2, [r1, #1]	@ zero_extendqisi2
	mov	ip, r2, asr r4
	ldrb	r0, [r1, #0]	@ zero_extendqisi2
	ldr	r2, [r3, ip, asl #2]
	add	ip, r2, r0, asl #1
	ldrh	r2, [ip, #0]
	mov	r0, r2, lsr #8
	strb	r0, [r1, #0]
	strb	r2, [r1, #1]
	mov	r0, #2
	add	r1, r1, #4
.L972:
	ldrb	ip, [r1, #1]	@ zero_extendqisi2
	mov	r2, ip, asr r4
	ldrb	ip, [r1, #0]	@ zero_extendqisi2
	ldr	r2, [r3, r2, asl #2]
	add	r2, r2, ip, asl #1
	ldrh	r2, [r2, #0]
	mov	ip, r2, lsr #8
	strb	ip, [r1, #0]
	strb	r2, [r1, #1]
	add	r0, r0, #1
	add	r1, r1, #4
.L971:
	ldrb	ip, [r1, #1]	@ zero_extendqisi2
	mov	r2, ip, asr r4
	ldrb	ip, [r1, #0]	@ zero_extendqisi2
	ldr	r2, [r3, r2, asl #2]
	add	r2, r2, ip, asl #1
	ldrh	r2, [r2, #0]
	add	r0, r0, #1
	mov	ip, r2, lsr #8
	cmp	r5, r0
	strb	ip, [r1, #0]
	strb	r2, [r1, #1]
	add	r1, r1, #4
	bls	.L862
.L854:
	ldrb	r7, [r1, #1]	@ zero_extendqisi2
	mov	r2, r7, asr r4
	ldrb	r6, [r1, #0]	@ zero_extendqisi2
	ldr	r7, [r3, r2, asl #2]
	mov	ip, r6, asl #1
	ldrh	r2, [ip, r7]
	mov	r6, r2, lsr #8
	strb	r6, [r1, #0]
	strb	r2, [r1, #1]
	add	r2, r1, #4
	ldrb	r7, [r2, #1]	@ zero_extendqisi2
	mov	ip, r7, asr r4
	ldrb	r6, [r1, #4]	@ zero_extendqisi2
	ldr	r7, [r3, ip, asl #2]
	mov	r6, r6, asl #1
	ldrh	ip, [r6, r7]
	mov	r6, ip, lsr #8
	strb	r6, [r1, #4]
	strb	ip, [r2, #1]
	add	ip, r2, #4
	ldrb	r7, [ip, #1]	@ zero_extendqisi2
	mov	r6, r7, asr r4
	ldrb	r7, [r2, #4]	@ zero_extendqisi2
	ldr	r6, [r3, r6, asl #2]
	mov	r7, r7, asl #1
	ldrh	r6, [r7, r6]
	mov	r7, r6, lsr #8
	strb	r7, [r2, #4]
	strb	r6, [ip, #1]
	add	r2, r1, #12
	ldrb	r6, [r2, #1]	@ zero_extendqisi2
	mov	ip, r6, asr r4
	ldrb	r6, [r1, #12]	@ zero_extendqisi2
	ldr	ip, [r3, ip, asl #2]
	mov	r6, r6, asl #1
	ldrh	ip, [r6, ip]
	add	r0, r0, #4
	mov	r6, ip, lsr #8
	cmp	r5, r0
	strb	r6, [r1, #12]
	strb	ip, [r2, #1]
	add	r1, r1, #16
	bhi	.L854
	b	.L862
.L996:
	cmp	r5, #0
	beq	.L862
	ldrb	ip, [r1, #1]	@ zero_extendqisi2
	mov	r2, ip, asr r4
	ldrb	r0, [r1, #0]	@ zero_extendqisi2
	ldr	ip, [r3, r2, asl #2]
	add	r2, ip, r0, asl #1
	ldrh	r2, [r2, #0]
	sub	r0, r5, #1
	mov	ip, r2, lsr #8
	cmp	r5, #1
	strb	r2, [r1, #1]
	strb	ip, [r1, #0]
	and	r2, r0, #3
	add	r1, r1, #2
	mov	r0, #1
	bls	.L862
	cmp	r2, #0
	beq	.L861
	cmp	r2, #1
	beq	.L965
	cmp	r2, #2
	beq	.L966
	ldrb	ip, [r1, #1]	@ zero_extendqisi2
	mov	r2, ip, asr r4
	ldrb	r0, [r1, #0]	@ zero_extendqisi2
	ldr	ip, [r3, r2, asl #2]
	add	r2, ip, r0, asl #1
	ldrh	r2, [r2, #0]
	mov	ip, r2, lsr #8
	strb	ip, [r1, #0]
	strb	r2, [r1, #1]
	mov	r0, #2
	add	r1, r1, #2
.L966:
	ldrb	ip, [r1, #1]	@ zero_extendqisi2
	mov	r2, ip, asr r4
	ldrb	ip, [r1, #0]	@ zero_extendqisi2
	ldr	r2, [r3, r2, asl #2]
	add	r2, r2, ip, asl #1
	ldrh	r2, [r2, #0]
	mov	ip, r2, lsr #8
	strb	ip, [r1, #0]
	strb	r2, [r1, #1]
	add	r0, r0, #1
	add	r1, r1, #2
.L965:
	ldrb	ip, [r1, #1]	@ zero_extendqisi2
	mov	r2, ip, asr r4
	ldrb	ip, [r1, #0]	@ zero_extendqisi2
	ldr	r2, [r3, r2, asl #2]
	add	r2, r2, ip, asl #1
	ldrh	r2, [r2, #0]
	add	r0, r0, #1
	mov	ip, r2, lsr #8
	cmp	r5, r0
	strb	ip, [r1, #0]
	strb	r2, [r1, #1]
	add	r1, r1, #2
	bls	.L862
.L861:
	ldrb	r7, [r1, #1]	@ zero_extendqisi2
	mov	r2, r7, asr r4
	ldrb	r6, [r1, #0]	@ zero_extendqisi2
	ldr	r7, [r3, r2, asl #2]
	mov	ip, r6, asl #1
	ldrh	r2, [ip, r7]
	mov	r6, r2, lsr #8
	strb	r6, [r1, #0]
	strb	r2, [r1, #1]
	add	r2, r1, #2
	ldrb	r7, [r2, #1]	@ zero_extendqisi2
	mov	ip, r7, asr r4
	ldrb	r6, [r1, #2]	@ zero_extendqisi2
	ldr	r7, [r3, ip, asl #2]
	mov	r6, r6, asl #1
	ldrh	ip, [r6, r7]
	mov	r6, ip, lsr #8
	strb	r6, [r1, #2]
	strb	ip, [r2, #1]
	add	ip, r2, #2
	ldrb	r7, [ip, #1]	@ zero_extendqisi2
	mov	r6, r7, asr r4
	ldrb	r7, [r2, #2]	@ zero_extendqisi2
	ldr	r6, [r3, r6, asl #2]
	mov	r7, r7, asl #1
	ldrh	r6, [r7, r6]
	mov	r7, r6, lsr #8
	strb	r7, [r2, #2]
	strb	r6, [ip, #1]
	add	r2, r1, #6
	ldrb	r6, [r2, #1]	@ zero_extendqisi2
	mov	ip, r6, asr r4
	ldrb	r6, [r1, #6]	@ zero_extendqisi2
	ldr	ip, [r3, ip, asl #2]
	mov	r6, r6, asl #1
	ldrh	ip, [r6, ip]
	add	r0, r0, #4
	mov	r6, ip, lsr #8
	cmp	r5, r0
	strb	r6, [r1, #6]
	strb	ip, [r2, #1]
	add	r1, r1, #8
	bhi	.L861
	b	.L862
.L994:
	cmp	r5, #0
	beq	.L862
	ldrb	r3, [r1, #0]	@ zero_extendqisi2
	and	r0, r3, #240
	and	ip, r3, #15
	orr	r0, r0, r0, lsr #4
	ldrb	r0, [r2, r0]	@ zero_extendqisi2
	orr	r3, ip, ip, asl #4
	ldrb	ip, [r2, r3]	@ zero_extendqisi2
	bic	r3, r0, #15
	sub	r0, r5, #1
	orr	r3, r3, ip, lsr #4
	cmp	r5, #2
	mov	ip, r0, lsr #1
	strb	r3, [r1, #0]
	and	r0, ip, #3
	mov	r3, #1
	bls	.L862
	cmp	r0, #0
	beq	.L859
	cmp	r0, #1
	beq	.L961
	cmp	r0, #2
	beq	.L962
	ldrb	r4, [r1, #1]	@ zero_extendqisi2
	and	ip, r4, #240
	and	r3, r4, #15
	orr	r0, ip, ip, lsr #4
	ldrb	ip, [r2, r0]	@ zero_extendqisi2
	orr	r4, r3, r3, asl #4
	ldrb	r0, [r2, r4]	@ zero_extendqisi2
	bic	r3, ip, #15
	orr	r4, r3, r0, lsr #4
	strb	r4, [r1, #1]
	mov	r3, #2
.L962:
	ldrb	r0, [r1, r3]	@ zero_extendqisi2
	and	r4, r0, #240
	orr	ip, r4, r4, lsr #4
	and	r0, r0, #15
	ldrb	r4, [r2, ip]	@ zero_extendqisi2
	orr	r0, r0, r0, asl #4
	ldrb	ip, [r2, r0]	@ zero_extendqisi2
	bic	r0, r4, #15
	orr	r0, r0, ip, lsr #4
	strb	r0, [r1, r3]
	add	r3, r3, #1
.L961:
	ldrb	r0, [r1, r3]	@ zero_extendqisi2
	and	r4, r0, #240
	orr	ip, r4, r4, lsr #4
	and	r0, r0, #15
	ldrb	r4, [r2, ip]	@ zero_extendqisi2
	orr	r0, r0, r0, asl #4
	ldrb	ip, [r2, r0]	@ zero_extendqisi2
	bic	r0, r4, #15
	orr	r0, r0, ip, lsr #4
	strb	r0, [r1, r3]
	add	r3, r3, #1
	cmp	r5, r3, asl #1
	bls	.L862
.L859:
	ldrb	r0, [r1, r3]	@ zero_extendqisi2
	and	r4, r0, #240
	and	r6, r0, #15
	orr	ip, r4, r4, lsr #4
	ldrb	r4, [r2, ip]	@ zero_extendqisi2
	orr	r0, r6, r6, asl #4
	ldrb	ip, [r2, r0]	@ zero_extendqisi2
	bic	r6, r4, #15
	orr	r0, r6, ip, lsr #4
	strb	r0, [r1, r3]
	add	r0, r3, #1
	ldrb	ip, [r1, r0]	@ zero_extendqisi2
	and	r6, ip, #240
	orr	r4, r6, r6, lsr #4
	and	ip, ip, #15
	ldrb	r6, [r2, r4]	@ zero_extendqisi2
	orr	ip, ip, ip, asl #4
	ldrb	r4, [r2, ip]	@ zero_extendqisi2
	bic	ip, r6, #15
	orr	r6, ip, r4, lsr #4
	strb	r6, [r1, r0]
	add	r0, r0, #1
	ldrb	ip, [r1, r0]	@ zero_extendqisi2
	and	r6, ip, #240
	orr	r4, r6, r6, lsr #4
	and	ip, ip, #15
	ldrb	r6, [r2, r4]	@ zero_extendqisi2
	orr	ip, ip, ip, asl #4
	ldrb	r4, [r2, ip]	@ zero_extendqisi2
	bic	ip, r6, #15
	orr	r6, ip, r4, lsr #4
	strb	r6, [r1, r0]
	add	r0, r3, #3
	ldrb	ip, [r1, r0]	@ zero_extendqisi2
	and	r6, ip, #240
	orr	r4, r6, r6, lsr #4
	and	ip, ip, #15
	ldrb	r6, [r2, r4]	@ zero_extendqisi2
	orr	ip, ip, ip, asl #4
	ldrb	r4, [r2, ip]	@ zero_extendqisi2
	add	r3, r3, #4
	bic	ip, r6, #15
	orr	ip, ip, r4, lsr #4
	cmp	r5, r3, asl #1
	strb	ip, [r1, r0]
	bhi	.L859
	b	.L862
.L993:
	cmp	r5, #0
	beq	.L862
	stmia	sp, {r0, r3}	@ phole stm
	mov	ip, #0
	mov	fp, r4
.L857:
	ldrb	r3, [r1, ip]	@ zero_extendqisi2
	and	r6, r3, #192
	and	r4, r3, #48
	and	r0, r3, #3
	mov	sl, r6, asr #4
	and	r3, r3, #12
	mov	r7, r4, asr #2
	orr	r9, sl, r6, lsr #2
	mov	r8, r0, asl #4
	orr	sl, r7, r4, asl #2
	mov	r7, r3, asl #2
	orr	r8, r8, r0, asl #6
	orr	r7, r7, r3, asl #4
	orr	r9, r9, r6
	orr	sl, sl, r4
	orr	r8, r8, r0
	orr	r7, r7, r3
	orr	r6, r9, r6, lsr #6
	orr	r4, sl, r4, lsr #4
	orr	r8, r8, r0, asl #2
	orr	r7, r7, r3, lsr #2
	ldrb	r6, [r2, r6]	@ zero_extendqisi2
	ldrb	r4, [r2, r4]	@ zero_extendqisi2
	ldrb	r8, [r2, r8]	@ zero_extendqisi2
	ldrb	r0, [r2, r7]	@ zero_extendqisi2
	bic	r3, r6, #63
	mov	r4, r4, lsr #2
	and	r4, r4, #48
	orr	r3, r3, r8, lsr #6
	mov	r0, r0, lsr #4
	orr	r3, r3, r4
	and	r4, r0, #12
	orr	r3, r3, r4
	strb	r3, [r1, ip]
	add	ip, ip, #1
	cmp	r5, ip, asl #2
	bhi	.L857
	ldmia	sp, {r0, r3}	@ phole ldm
	mov	r4, fp
	ldrb	ip, [r0, #9]	@ zero_extendqisi2
	b	.L855
.L847:
	cmp	r5, #0
	beq	.L862
	ldrb	r3, [r1, #0]	@ zero_extendqisi2
	ldrb	ip, [r2, r3]	@ zero_extendqisi2
	ldrb	r0, [r1, #1]	@ zero_extendqisi2
	strb	ip, [r1, #0]
	ldrb	r3, [r2, r0]	@ zero_extendqisi2
	ldrb	ip, [r1, #2]	@ zero_extendqisi2
	strb	r3, [r1, #1]
	ldrb	r0, [r2, ip]	@ zero_extendqisi2
	sub	r3, r5, #1
	cmp	r5, #1
	strb	r0, [r1, #2]
	and	r3, r3, #3
	add	r1, r1, #4
	mov	r0, #1
	bls	.L862
	cmp	r3, #0
	beq	.L849
	cmp	r3, #1
	beq	.L973
	cmp	r3, #2
	beq	.L974
	ldrb	ip, [r1, #0]	@ zero_extendqisi2
	ldrb	r0, [r2, ip]	@ zero_extendqisi2
	ldrb	r3, [r1, #1]	@ zero_extendqisi2
	strb	r0, [r1, #0]
	ldrb	ip, [r2, r3]	@ zero_extendqisi2
	ldrb	r0, [r1, #2]	@ zero_extendqisi2
	strb	ip, [r1, #1]
	ldrb	r3, [r2, r0]	@ zero_extendqisi2
	mov	r0, #2
	strb	r3, [r1, #2]
	add	r1, r1, #4
.L974:
	ldrb	ip, [r1, #0]	@ zero_extendqisi2
	ldrb	r3, [r2, ip]	@ zero_extendqisi2
	ldrb	ip, [r1, #1]	@ zero_extendqisi2
	strb	r3, [r1, #0]
	ldrb	r3, [r2, ip]	@ zero_extendqisi2
	ldrb	ip, [r1, #2]	@ zero_extendqisi2
	strb	r3, [r1, #1]
	ldrb	r3, [r2, ip]	@ zero_extendqisi2
	add	r0, r0, #1
	strb	r3, [r1, #2]
	add	r1, r1, #4
.L973:
	ldrb	ip, [r1, #0]	@ zero_extendqisi2
	ldrb	r3, [r2, ip]	@ zero_extendqisi2
	ldrb	ip, [r1, #1]	@ zero_extendqisi2
	strb	r3, [r1, #0]
	ldrb	r3, [r2, ip]	@ zero_extendqisi2
	ldrb	ip, [r1, #2]	@ zero_extendqisi2
	strb	r3, [r1, #1]
	add	r0, r0, #1
	ldrb	r3, [r2, ip]	@ zero_extendqisi2
	cmp	r5, r0
	strb	r3, [r1, #2]
	add	r1, r1, #4
	bls	.L862
.L849:
	ldrb	ip, [r1, #0]	@ zero_extendqisi2
	ldrb	r3, [r2, ip]	@ zero_extendqisi2
	ldrb	ip, [r1, #1]	@ zero_extendqisi2
	strb	r3, [r1, #0]
	ldrb	r3, [r2, ip]	@ zero_extendqisi2
	ldrb	ip, [r1, #2]	@ zero_extendqisi2
	strb	r3, [r1, #1]
	ldrb	r3, [r2, ip]	@ zero_extendqisi2
	ldrb	ip, [r1, #4]	@ zero_extendqisi2
	strb	r3, [r1, #2]
	ldrb	r3, [r2, ip]	@ zero_extendqisi2
	strb	r3, [r1, #4]
	add	r3, r1, #4
	ldrb	ip, [r3, #1]	@ zero_extendqisi2
	ldrb	ip, [r2, ip]	@ zero_extendqisi2
	strb	ip, [r3, #1]
	ldrb	ip, [r3, #2]	@ zero_extendqisi2
	ldrb	ip, [r2, ip]	@ zero_extendqisi2
	strb	ip, [r3, #2]
	ldrb	ip, [r3, #4]	@ zero_extendqisi2
	ldrb	ip, [r2, ip]	@ zero_extendqisi2
	strb	ip, [r3, #4]
	add	r3, r3, #4
	ldrb	ip, [r3, #1]	@ zero_extendqisi2
	ldrb	ip, [r2, ip]	@ zero_extendqisi2
	strb	ip, [r3, #1]
	ldrb	ip, [r3, #2]	@ zero_extendqisi2
	ldrb	ip, [r2, ip]	@ zero_extendqisi2
	strb	ip, [r3, #2]
	ldrb	ip, [r1, #12]	@ zero_extendqisi2
	ldrb	r3, [r2, ip]	@ zero_extendqisi2
	strb	r3, [r1, #12]
	add	r3, r1, #12
	ldrb	ip, [r3, #1]	@ zero_extendqisi2
	ldrb	ip, [r2, ip]	@ zero_extendqisi2
	strb	ip, [r3, #1]
	ldrb	ip, [r3, #2]	@ zero_extendqisi2
	add	r0, r0, #4
	ldrb	ip, [r2, ip]	@ zero_extendqisi2
	cmp	r5, r0
	strb	ip, [r3, #2]
	add	r1, r1, #16
	bhi	.L849
	b	.L862
.L851:
	cmp	r5, #0
	beq	.L862
	ldrb	ip, [r1, #0]	@ zero_extendqisi2
	ldrb	r3, [r2, ip]	@ zero_extendqisi2
	sub	r0, r5, #1
	cmp	r5, #1
	strb	r3, [r1, #0]
	and	ip, r0, #3
	mov	r3, #2
	mov	r0, #1
	bls	.L862
	cmp	ip, #0
	beq	.L853
	cmp	ip, #1
	beq	.L969
	cmp	ip, #2
	ldrneb	r0, [r1, #2]	@ zero_extendqisi2
	ldrneb	r0, [r2, r0]	@ zero_extendqisi2
	strneb	r0, [r1, #2]
	movne	r0, r3
	movne	r3, #4
	ldrb	ip, [r1, r3]	@ zero_extendqisi2
	ldrb	ip, [r2, ip]	@ zero_extendqisi2
	add	r0, r0, #1
	strb	ip, [r1, r3]
	add	r3, r3, #2
.L969:
	ldrb	ip, [r1, r3]	@ zero_extendqisi2
	add	r0, r0, #1
	ldrb	ip, [r2, ip]	@ zero_extendqisi2
	cmp	r5, r0
	strb	ip, [r1, r3]
	add	r3, r3, #2
	bls	.L862
.L853:
	ldrb	r4, [r1, r3]	@ zero_extendqisi2
	ldrb	ip, [r2, r4]	@ zero_extendqisi2
	strb	ip, [r1, r3]
	add	ip, r3, #2
	ldrb	r4, [r1, ip]	@ zero_extendqisi2
	ldrb	r4, [r2, r4]	@ zero_extendqisi2
	strb	r4, [r1, ip]
	add	ip, ip, #2
	ldrb	r4, [r1, ip]	@ zero_extendqisi2
	ldrb	r4, [r2, r4]	@ zero_extendqisi2
	strb	r4, [r1, ip]
	add	ip, r3, #6
	ldrb	r4, [r1, ip]	@ zero_extendqisi2
	add	r0, r0, #4
	ldrb	r4, [r2, r4]	@ zero_extendqisi2
	cmp	r5, r0
	strb	r4, [r1, ip]
	add	r3, r3, #8
	bhi	.L853
	b	.L862
.L843:
	cmp	r5, #0
	beq	.L862
	ldrb	ip, [r1, #0]	@ zero_extendqisi2
	ldrb	r0, [r2, ip]	@ zero_extendqisi2
	ldrb	r3, [r1, #1]	@ zero_extendqisi2
	strb	r0, [r1, #0]
	ldrb	ip, [r2, r3]	@ zero_extendqisi2
	ldrb	r0, [r1, #2]	@ zero_extendqisi2
	strb	ip, [r1, #1]
	ldrb	r3, [r2, r0]	@ zero_extendqisi2
	sub	ip, r5, #1
	cmp	r5, #1
	strb	r3, [r1, #2]
	and	ip, ip, #3
	add	r3, r1, #3
	mov	r0, #1
	bls	.L862
	cmp	ip, #0
	beq	.L845
	cmp	ip, #1
	beq	.L967
	cmp	ip, #2
	beq	.L968
	ldrb	ip, [r3, #0]	@ zero_extendqisi2
	ldrb	r0, [r2, ip]	@ zero_extendqisi2
	ldrb	ip, [r3, #1]	@ zero_extendqisi2
	strb	r0, [r3, #0]
	ldrb	r0, [r2, ip]	@ zero_extendqisi2
	ldrb	ip, [r3, #2]	@ zero_extendqisi2
	strb	r0, [r3, #1]
	ldrb	r0, [r2, ip]	@ zero_extendqisi2
	strb	r0, [r3, #2]
	add	r3, r1, #6
	mov	r0, #2
.L968:
	ldrb	ip, [r3, #0]	@ zero_extendqisi2
	ldrb	r1, [r2, ip]	@ zero_extendqisi2
	ldrb	ip, [r3, #1]	@ zero_extendqisi2
	strb	r1, [r3, #0]
	ldrb	r1, [r2, ip]	@ zero_extendqisi2
	ldrb	ip, [r3, #2]	@ zero_extendqisi2
	strb	r1, [r3, #1]
	ldrb	r1, [r2, ip]	@ zero_extendqisi2
	add	r0, r0, #1
	strb	r1, [r3, #2]
	add	r3, r3, #3
.L967:
	ldrb	ip, [r3, #0]	@ zero_extendqisi2
	ldrb	r1, [r2, ip]	@ zero_extendqisi2
	ldrb	ip, [r3, #1]	@ zero_extendqisi2
	strb	r1, [r3, #0]
	ldrb	r1, [r2, ip]	@ zero_extendqisi2
	ldrb	ip, [r3, #2]	@ zero_extendqisi2
	strb	r1, [r3, #1]
	add	r0, r0, #1
	ldrb	r1, [r2, ip]	@ zero_extendqisi2
	cmp	r5, r0
	strb	r1, [r3, #2]
	add	r3, r3, #3
	bls	.L862
.L845:
	ldrb	ip, [r3, #0]	@ zero_extendqisi2
	ldrb	r1, [r2, ip]	@ zero_extendqisi2
	ldrb	ip, [r3, #1]	@ zero_extendqisi2
	strb	r1, [r3, #0]
	ldrb	r1, [r2, ip]	@ zero_extendqisi2
	ldrb	ip, [r3, #2]	@ zero_extendqisi2
	strb	r1, [r3, #1]
	ldrb	r1, [r2, ip]	@ zero_extendqisi2
	ldrb	ip, [r3, #3]	@ zero_extendqisi2
	strb	r1, [r3, #2]
	ldrb	r1, [r2, ip]	@ zero_extendqisi2
	strb	r1, [r3, #3]
	add	r1, r3, #3
	ldrb	ip, [r1, #1]	@ zero_extendqisi2
	ldrb	ip, [r2, ip]	@ zero_extendqisi2
	strb	ip, [r1, #1]
	ldrb	ip, [r1, #2]	@ zero_extendqisi2
	ldrb	ip, [r2, ip]	@ zero_extendqisi2
	strb	ip, [r1, #2]
	ldrb	ip, [r3, #6]	@ zero_extendqisi2
	ldrb	r1, [r2, ip]	@ zero_extendqisi2
	strb	r1, [r3, #6]
	add	r1, r3, #6
	ldrb	ip, [r1, #1]	@ zero_extendqisi2
	ldrb	ip, [r2, ip]	@ zero_extendqisi2
	strb	ip, [r1, #1]
	ldrb	ip, [r1, #2]	@ zero_extendqisi2
	ldrb	ip, [r2, ip]	@ zero_extendqisi2
	strb	ip, [r1, #2]
	ldrb	ip, [r3, #9]	@ zero_extendqisi2
	ldrb	r1, [r2, ip]	@ zero_extendqisi2
	strb	r1, [r3, #9]
	add	r1, r3, #9
	ldrb	ip, [r1, #1]	@ zero_extendqisi2
	ldrb	ip, [r2, ip]	@ zero_extendqisi2
	strb	ip, [r1, #1]
	ldrb	ip, [r1, #2]	@ zero_extendqisi2
	add	r0, r0, #4
	ldrb	ip, [r2, ip]	@ zero_extendqisi2
	cmp	r5, r0
	strb	ip, [r1, #2]
	add	r3, r3, #12
	bhi	.L845
	b	.L862
.L995:
	cmp	r5, #0
	beq	.L862
	ldrb	r0, [r1, #0]	@ zero_extendqisi2
	ldrb	r3, [r2, r0]	@ zero_extendqisi2
	sub	ip, r5, #1
	cmp	r5, #1
	strb	r3, [r1, #0]
	and	r0, ip, #3
	mov	r3, #1
	bls	.L862
	cmp	r0, #0
	beq	.L860
	cmp	r0, #1
	beq	.L963
	cmp	r0, #2
	ldrneb	r3, [r1, #1]	@ zero_extendqisi2
	ldrneb	r3, [r2, r3]	@ zero_extendqisi2
	strneb	r3, [r1, #1]
	movne	r3, #2
	ldrb	r0, [r1, r3]	@ zero_extendqisi2
	ldrb	ip, [r2, r0]	@ zero_extendqisi2
	strb	ip, [r1, r3]
	add	r3, r3, #1
.L963:
	ldrb	r0, [r1, r3]	@ zero_extendqisi2
	ldrb	ip, [r2, r0]	@ zero_extendqisi2
	strb	ip, [r1, r3]
	add	r3, r3, #1
	cmp	r5, r3
	bls	.L862
.L860:
	ldrb	ip, [r1, r3]	@ zero_extendqisi2
	ldrb	r0, [r2, ip]	@ zero_extendqisi2
	strb	r0, [r1, r3]
	add	r0, r3, #1
	ldrb	ip, [r1, r0]	@ zero_extendqisi2
	ldrb	ip, [r2, ip]	@ zero_extendqisi2
	strb	ip, [r1, r0]
	add	r0, r0, #1
	ldrb	ip, [r1, r0]	@ zero_extendqisi2
	ldrb	ip, [r2, ip]	@ zero_extendqisi2
	strb	ip, [r1, r0]
	add	r0, r3, #3
	ldrb	ip, [r1, r0]	@ zero_extendqisi2
	add	r3, r3, #4
	ldrb	ip, [r2, ip]	@ zero_extendqisi2
	cmp	r5, r3
	strb	ip, [r1, r0]
	bhi	.L860
	b	.L862
	.size	png_do_gamma, .-png_do_gamma
	.section	.text.png_do_expand_palette,"ax",%progbits
	.align	2
	.global	png_do_expand_palette
	.hidden	png_do_expand_palette
	.type	png_do_expand_palette, %function
png_do_expand_palette:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldrb	ip, [r0, #8]	@ zero_extendqisi2
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
	cmp	ip, #3
	ldr	r7, [sp, #32]
	ldr	r4, [r0, #0]
	beq	.L1173
.L1023:
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
	bx	lr
.L1173:
	ldrb	sl, [r0, #9]	@ zero_extendqisi2
	cmp	sl, #7
	bls	.L1174
	cmp	sl, #8
	bne	.L1023
.L1015:
	cmp	r3, #0
	beq	.L1016
	cmp	r4, #0
	moveq	fp, r4
	beq	.L1017
	sub	r8, r4, #1
	add	ip, r1, r8
	ldrb	r5, [ip, #0]	@ zero_extendqisi2
	mov	fp, r4, asl #2
	add	r1, r1, fp
	cmp	r5, r7
	sub	r1, r1, #2
	and	r6, r8, #1
	mvn	r9, #0
	blt	.L1030
	strb	r9, [r1, #1]
.L1119:
	ldrb	r5, [ip, #0]	@ zero_extendqisi2
	add	r8, r5, r5, asl #1
	add	r5, r8, r2
	ldrb	r8, [r5, #2]	@ zero_extendqisi2
	mov	r5, r1
	strb	r8, [r5], #-4
	ldrb	r8, [ip, #0]	@ zero_extendqisi2
	add	r8, r8, r8, asl #1
	add	r8, r8, r2
	ldrb	r8, [r8, #1]	@ zero_extendqisi2
	strb	r8, [r1, #-1]!
	ldrb	r8, [ip, #0]	@ zero_extendqisi2
	add	r8, r8, r8, asl #1
	ldrb	r8, [r8, r2]	@ zero_extendqisi2
	cmp	r4, #1
	strb	r8, [r1, #-1]
	mvn	r1, #0
	bls	.L1017
	cmp	r6, #0
	beq	.L1020
	ldrb	sl, [ip, r1]	@ zero_extendqisi2
	cmp	sl, r7
	ldrltb	sl, [r3, sl]	@ zero_extendqisi2
	strgeb	r9, [r5, #1]
	strltb	sl, [r5, #1]
	ldrb	r6, [ip, r1]	@ zero_extendqisi2
	add	r8, r6, r6, asl #1
	add	sl, r8, r2
	ldrb	r6, [sl, #2]	@ zero_extendqisi2
	strb	r6, [r5, #0]
	ldrb	r8, [ip, r1]	@ zero_extendqisi2
	add	sl, r8, r8, asl #1
	add	r6, sl, r2
	ldrb	r8, [r6, #1]	@ zero_extendqisi2
	mov	r6, r5
	strb	r8, [r6, #-1]!
	ldrb	sl, [ip, r1]	@ zero_extendqisi2
	sub	r1, r1, #1
	add	r8, sl, sl, asl #1
	ldrb	sl, [r8, r2]	@ zero_extendqisi2
	rsb	r8, r1, #0
	cmp	r4, r8
	strb	sl, [r6, #-1]
	sub	r5, r5, #4
	bhi	.L1020
	b	.L1017
.L1175:
	strb	r9, [r5, #1]
.L1122:
	ldrb	r8, [ip, r1]	@ zero_extendqisi2
	add	sl, r8, r8, asl #1
	add	r8, sl, r2
	ldrb	sl, [r8, #2]	@ zero_extendqisi2
	mov	r6, r5
	strb	sl, [r6], #-1
	ldrb	r8, [ip, r1]	@ zero_extendqisi2
	add	sl, r8, r8, asl #1
	add	r8, sl, r2
	ldrb	sl, [r8, #1]	@ zero_extendqisi2
	strb	sl, [r5, #-1]
	ldrb	r8, [ip, r1]	@ zero_extendqisi2
	sub	r1, r1, #1
	add	sl, r8, r8, asl #1
	ldrb	r8, [sl, r2]	@ zero_extendqisi2
	rsb	sl, r1, #0
	cmp	r4, sl
	strb	r8, [r6, #-1]
	sub	r5, r5, #4
	bls	.L1017
.L1020:
	ldrb	r6, [ip, r1]	@ zero_extendqisi2
	cmp	r6, r7
	ldrltb	r6, [r3, r6]	@ zero_extendqisi2
	strgeb	r9, [r5, #1]
	strltb	r6, [r5, #1]
	ldrb	sl, [ip, r1]	@ zero_extendqisi2
	add	r8, sl, sl, asl #1
	add	sl, r8, r2
	ldrb	r8, [sl, #2]	@ zero_extendqisi2
	mov	r6, r5
	strb	r8, [r6], #-1
	ldrb	sl, [ip, r1]	@ zero_extendqisi2
	add	r8, sl, sl, asl #1
	add	sl, r8, r2
	ldrb	r8, [sl, #1]	@ zero_extendqisi2
	strb	r8, [r5, #-1]
	ldrb	sl, [ip, r1]	@ zero_extendqisi2
	add	r8, sl, sl, asl #1
	ldrb	sl, [r8, r2]	@ zero_extendqisi2
	strb	sl, [r6, #-1]
	sub	r1, r1, #1
	ldrb	r6, [ip, r1]	@ zero_extendqisi2
	cmp	r6, r7
	sub	r5, r5, #4
	bge	.L1175
	ldrb	r8, [r3, r6]	@ zero_extendqisi2
	strb	r8, [r5, #1]
	b	.L1122
.L1174:
	cmp	sl, #2
	beq	.L1002
	cmp	sl, #4
	beq	.L1003
	cmp	sl, #1
	beq	.L1176
.L1000:
	mov	ip, #8
	strb	ip, [r0, #11]
	strb	ip, [r0, #9]
	str	r4, [r0, #4]
	b	.L1015
.L1017:
	mov	r1, #4
	mov	ip, #8
	mov	r2, #32
	mov	r3, #6
	strb	r1, [r0, #10]
	strb	ip, [r0, #9]
	strb	r2, [r0, #11]
	str	fp, [r0, #4]
	strb	r3, [r0, #8]
	b	.L1023
.L1030:
	ldrb	r5, [r3, r5]	@ zero_extendqisi2
	strb	r5, [r1, #1]
	b	.L1119
.L1003:
	cmp	r4, #0
	beq	.L1000
	sub	r5, r4, #1
	add	ip, r1, r5, lsr #1
	ldrb	sl, [ip, #0]	@ zero_extendqisi2
	and	r6, r4, #1
	mov	r6, r6, asl #2
	mov	r8, sl, asr r6
	cmp	r6, #4
	and	sl, r8, #15
	add	r8, r1, r5
	strb	sl, [r8, #0]
	addne	r6, r6, #4
	and	sl, r5, #3
	beq	.L1177
.L1135:
	mov	r5, #1
	cmp	r4, r5
	bls	.L1000
	cmp	sl, #0
	beq	.L1014
	cmp	sl, r5
	beq	.L1105
	cmp	sl, #2
	beq	.L1106
	ldrb	r9, [ip, #0]	@ zero_extendqisi2
	mov	sl, r9, asr r6
	and	r9, sl, #15
	cmp	r6, #4
	strb	r9, [r8, #-1]
	addne	r6, r6, #4
	subeq	ip, ip, #1
	moveq	r6, #0
	add	r5, r5, #1
.L1106:
	ldrb	sl, [ip, #0]	@ zero_extendqisi2
	mov	r9, sl, asr r6
	rsb	sl, r5, #0
	and	r9, r9, #15
	cmp	r6, #4
	strb	r9, [r8, sl]
	addne	r6, r6, #4
	subeq	ip, ip, #1
	moveq	r6, #0
	add	r5, r5, #1
.L1105:
	ldrb	sl, [ip, #0]	@ zero_extendqisi2
	mov	r9, sl, asr r6
	cmp	r6, #4
	rsb	sl, r5, #0
	add	r5, r5, #1
	and	r9, r9, #15
	addne	r6, r6, #4
	subeq	ip, ip, #1
	moveq	r6, #0
	cmp	r4, r5
	strb	r9, [r8, sl]
	bhi	.L1014
	b	.L1000
.L1142:
	ldrb	sl, [ip, #0]	@ zero_extendqisi2
	mov	r9, sl, asr r6
	mvn	sl, r5
	and	r9, r9, #15
	cmp	r6, #4
	strb	r9, [r8, sl]
	subeq	ip, ip, #1
	ldrb	sl, [ip, #0]	@ zero_extendqisi2
	addne	r6, r6, #4
	moveq	r6, #0
	mov	r9, sl, asr r6
	cmp	r6, #4
	rsb	sl, r5, r8
	add	r5, r5, #3
	and	r9, r9, #15
	addne	r6, r6, #4
	subeq	ip, ip, #1
	moveq	r6, #0
	cmp	r4, r5
	strb	r9, [sl, #-2]
	bls	.L1000
.L1014:
	ldrb	sl, [ip, #0]	@ zero_extendqisi2
	mov	r9, sl, asr r6
	rsb	sl, r5, #0
	and	r9, r9, #15
	cmp	r6, #4
	strb	r9, [r8, sl]
	subeq	ip, ip, #1
	ldrb	sl, [ip, #0]	@ zero_extendqisi2
	addne	r6, r6, #4
	moveq	r6, #0
	mov	r9, sl, asr r6
	add	r5, r5, #1
	and	r9, r9, #15
	cmp	r6, #4
	rsb	sl, r5, #0
	strb	r9, [r8, sl]
	addne	r6, r6, #4
	subeq	ip, ip, #1
	moveq	r6, #0
	b	.L1142
.L1002:
	cmp	r4, #0
	beq	.L1000
	sub	r5, r4, #1
	add	ip, r4, #3
	mvn	r8, ip
	add	ip, r1, r5, lsr #2
	ldrb	sl, [ip, #0]	@ zero_extendqisi2
	and	r6, r8, #3
	mov	r6, r6, asl #1
	mov	r8, sl, asr r6
	cmp	r6, #6
	and	sl, r8, #3
	add	r8, r1, r5
	strb	sl, [r8, #0]
	addne	r6, r6, #2
	and	sl, r5, #3
	beq	.L1178
.L1123:
	mov	r5, #1
	cmp	r4, r5
	bls	.L1000
	cmp	sl, #0
	beq	.L1011
	cmp	sl, r5
	beq	.L1102
	cmp	sl, #2
	beq	.L1103
	ldrb	r9, [ip, #0]	@ zero_extendqisi2
	mov	sl, r9, asr r6
	and	r9, sl, #3
	cmp	r6, #6
	strb	r9, [r8, #-1]
	addne	r6, r6, #2
	subeq	ip, ip, #1
	moveq	r6, #0
	add	r5, r5, #1
.L1103:
	ldrb	sl, [ip, #0]	@ zero_extendqisi2
	mov	r9, sl, asr r6
	rsb	sl, r5, #0
	and	r9, r9, #3
	cmp	r6, #6
	strb	r9, [r8, sl]
	addne	r6, r6, #2
	subeq	ip, ip, #1
	moveq	r6, #0
	add	r5, r5, #1
.L1102:
	ldrb	sl, [ip, #0]	@ zero_extendqisi2
	mov	r9, sl, asr r6
	cmp	r6, #6
	rsb	sl, r5, #0
	add	r5, r5, #1
	and	r9, r9, #3
	addne	r6, r6, #2
	subeq	ip, ip, #1
	moveq	r6, #0
	cmp	r4, r5
	strb	r9, [r8, sl]
	bhi	.L1011
	b	.L1000
.L1130:
	ldrb	sl, [ip, #0]	@ zero_extendqisi2
	mov	r9, sl, asr r6
	mvn	sl, r5
	and	r9, r9, #3
	cmp	r6, #6
	strb	r9, [r8, sl]
	subeq	ip, ip, #1
	ldrb	sl, [ip, #0]	@ zero_extendqisi2
	addne	r6, r6, #2
	moveq	r6, #0
	mov	r9, sl, asr r6
	cmp	r6, #6
	rsb	sl, r5, r8
	add	r5, r5, #3
	and	r9, r9, #3
	addne	r6, r6, #2
	subeq	ip, ip, #1
	moveq	r6, #0
	cmp	r4, r5
	strb	r9, [sl, #-2]
	bls	.L1000
.L1011:
	ldrb	sl, [ip, #0]	@ zero_extendqisi2
	mov	r9, sl, asr r6
	rsb	sl, r5, #0
	and	r9, r9, #3
	cmp	r6, #6
	strb	r9, [r8, sl]
	subeq	ip, ip, #1
	ldrb	sl, [ip, #0]	@ zero_extendqisi2
	addne	r6, r6, #2
	moveq	r6, #0
	mov	r9, sl, asr r6
	add	r5, r5, #1
	and	r9, r9, #3
	cmp	r6, #6
	rsb	sl, r5, #0
	strb	r9, [r8, sl]
	addne	r6, r6, #2
	subeq	ip, ip, #1
	moveq	r6, #0
	b	.L1130
.L1176:
	cmp	r4, #0
	beq	.L1000
	sub	r5, r4, #1
	add	ip, r1, r5, lsr #3
	add	r9, r4, #7
	ldrb	r6, [ip, #0]	@ zero_extendqisi2
	mvn	r8, r9
	and	r8, r8, #7
	mov	r9, r6, asr r8
	ands	r6, r9, #1
	and	r9, r5, #3
	add	r5, r1, r5
	streqb	r6, [r5, #0]
	strneb	sl, [r5, #0]
.L1147:
	cmp	r8, #7
	addne	r8, r8, #1
	subeq	ip, ip, #1
	moveq	r8, #0
	cmp	r4, #1
	mvn	r6, #0
	bls	.L1000
	cmp	r9, #0
	beq	.L1008
	cmp	r9, #1
	beq	.L1108
	cmp	r9, #2
	beq	.L1109
	ldrb	r9, [ip, #0]	@ zero_extendqisi2
	mov	r9, r9, asr r8
	ands	r9, r9, #1
	streqb	r9, [r5, r6]
	strneb	sl, [r5, r6]
	cmp	r8, #7
	addne	r8, r8, #1
	subeq	ip, ip, #1
	moveq	r8, #0
	sub	r6, r6, #1
.L1109:
	ldrb	r9, [ip, #0]	@ zero_extendqisi2
	mov	r9, r9, asr r8
	ands	r9, r9, #1
	streqb	r9, [r5, r6]
	strneb	sl, [r5, r6]
	cmp	r8, #7
	addne	r8, r8, #1
	subeq	ip, ip, #1
	moveq	r8, #0
	sub	r6, r6, #1
.L1108:
	ldrb	r9, [ip, #0]	@ zero_extendqisi2
	mov	r9, r9, asr r8
	ands	r9, r9, #1
	streqb	r9, [r5, r6]
	strneb	sl, [r5, r6]
	sub	r6, r6, #1
	cmp	r8, #7
	rsb	r9, r6, #0
	addne	r8, r8, #1
	subeq	ip, ip, #1
	moveq	r8, #0
	cmp	r4, r9
	bls	.L1000
.L1008:
	ldrb	r9, [ip, #0]	@ zero_extendqisi2
	mov	fp, r9, asr r8
	ands	r9, fp, #1
	streqb	r9, [r5, r6]
	strneb	sl, [r5, r6]
	cmp	r8, #7
	subeq	ip, ip, #1
	ldrb	fp, [ip, #0]	@ zero_extendqisi2
	moveq	r8, #0
	addne	r8, r8, #1
	mov	r9, fp, asr r8
	sub	r6, r6, #1
	ands	fp, r9, #1
	streqb	fp, [r5, r6]
	strneb	sl, [r5, r6]
.L1162:
	cmp	r8, #7
	subeq	ip, ip, #1
	ldrb	fp, [ip, #0]	@ zero_extendqisi2
	addne	r8, r8, #1
	moveq	r8, #0
	mov	r9, fp, asr r8
	ands	fp, r9, #1
	sub	r9, r6, #1
	streqb	fp, [r5, r9]
	strneb	sl, [r5, r9]
	cmp	r8, #7
	subeq	ip, ip, #1
	ldrb	r9, [ip, #0]	@ zero_extendqisi2
	addne	r8, r8, #1
	moveq	r8, #0
	mov	fp, r9, asr r8
	sub	r9, r6, #2
	ands	fp, fp, #1
	sub	r6, r6, #3
	streqb	fp, [r5, r9]
	strneb	sl, [r5, r9]
	cmp	r8, #7
	rsb	r9, r6, #0
	addne	r8, r8, #1
	subeq	ip, ip, #1
	moveq	r8, #0
	cmp	r4, r9
	bhi	.L1008
	b	.L1000
.L1016:
	cmp	r4, #0
	add	r7, r4, r4, asl #1
	beq	.L1021
	sub	r3, r4, #1
	add	ip, r1, r3
	ldrb	r5, [ip, #0]	@ zero_extendqisi2
	add	r6, r5, r5, asl #1
	add	r5, r6, r2
	ldrb	r5, [r5, #2]	@ zero_extendqisi2
	sub	r6, r7, #1
	add	r1, r1, r6
	strb	r5, [r1, #0]
	ldrb	r5, [ip, #0]	@ zero_extendqisi2
	add	r6, r5, r5, asl #1
	add	r5, r6, r2
	ldrb	r6, [r5, #1]	@ zero_extendqisi2
	strb	r6, [r1, #-1]
	ldrb	r5, [ip, #0]	@ zero_extendqisi2
	add	r6, r5, r5, asl #1
	ldrb	r5, [r6, r2]	@ zero_extendqisi2
	cmp	r4, #1
	strb	r5, [r1, #-2]
	and	r6, r3, #1
	mvn	r5, #0
	sub	r3, r1, #3
	bls	.L1021
	cmp	r6, #0
	beq	.L1022
	ldrb	r5, [ip, #-1]	@ zero_extendqisi2
	add	r5, r5, r5, asl #1
	add	r5, r5, r2
	ldrb	r5, [r5, #2]	@ zero_extendqisi2
	strb	r5, [r3, #0]
	ldrb	r5, [ip, #-1]	@ zero_extendqisi2
	add	r5, r5, r5, asl #1
	add	r5, r5, r2
	ldrb	r5, [r5, #1]	@ zero_extendqisi2
	strb	r5, [r3, #-1]
	ldrb	r5, [ip, #-1]	@ zero_extendqisi2
	add	r5, r5, r5, asl #1
	ldrb	r5, [r5, r2]	@ zero_extendqisi2
	cmp	r4, #2
	strb	r5, [r3, #-2]
	sub	r3, r1, #6
	mvn	r5, #1
	bls	.L1021
.L1022:
	ldrb	r8, [ip, r5]	@ zero_extendqisi2
	add	r6, r8, r8, asl #1
	add	r1, r6, r2
	ldrb	r8, [r1, #2]	@ zero_extendqisi2
	strb	r8, [r3, #0]
	ldrb	r6, [ip, r5]	@ zero_extendqisi2
	add	r1, r6, r6, asl #1
	add	r8, r1, r2
	ldrb	r6, [r8, #1]	@ zero_extendqisi2
	strb	r6, [r3, #-1]
	ldrb	r1, [ip, r5]	@ zero_extendqisi2
	add	r8, r1, r1, asl #1
	ldrb	r6, [r8, r2]	@ zero_extendqisi2
	strb	r6, [r3, #-2]
	sub	r5, r5, #1
	ldrb	r1, [ip, r5]	@ zero_extendqisi2
	add	r8, r1, r1, asl #1
	add	r6, r8, r2
	ldrb	r1, [r6, #2]	@ zero_extendqisi2
	strb	r1, [r3, #-3]
	ldrb	r8, [ip, r5]	@ zero_extendqisi2
	add	r6, r8, r8, asl #1
	add	r1, r6, r2
	ldrb	r8, [r1, #1]	@ zero_extendqisi2
	sub	r1, r3, #3
	strb	r8, [r1, #-1]
	ldrb	r6, [ip, r5]	@ zero_extendqisi2
	sub	r5, r5, #1
	add	r8, r6, r6, asl #1
	ldrb	r6, [r8, r2]	@ zero_extendqisi2
	rsb	r8, r5, #0
	cmp	r4, r8
	strb	r6, [r1, #-2]
	sub	r3, r3, #6
	bhi	.L1022
.L1021:
	mov	r1, #3
	mov	r3, #8
	mov	ip, #24
	mov	r2, #2
	strb	r1, [r0, #10]
	strb	r3, [r0, #9]
	strb	ip, [r0, #11]
	str	r7, [r0, #4]
	strb	r2, [r0, #8]
	b	.L1023
.L1178:
	sub	ip, ip, #1
	mov	r6, #0
	b	.L1123
.L1177:
	sub	ip, ip, #1
	mov	r6, #0
	b	.L1135
	.size	png_do_expand_palette, .-png_do_expand_palette
	.section	.text.png_do_expand,"ax",%progbits
	.align	2
	.global	png_do_expand
	.hidden	png_do_expand
	.type	png_do_expand, %function
png_do_expand:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
	ldrb	ip, [r0, #8]	@ zero_extendqisi2
	cmp	ip, #0
	sub	sp, sp, #16
	ldr	r3, [r0, #0]
	bne	.L1180
	ldrb	ip, [r0, #9]	@ zero_extendqisi2
	cmp	r2, #0
	moveq	r4, r2
	ldrneh	r4, [r2, #8]
	cmp	ip, #7
	bhi	.L1183
	cmp	ip, #2
	beq	.L1186
	cmp	ip, #4
	beq	.L1187
	cmp	ip, #1
	beq	.L1480
.L1184:
	mov	ip, #8
	strb	ip, [r0, #9]
	strb	ip, [r0, #11]
	str	r3, [r0, #4]
.L1183:
	cmp	r2, #0
	beq	.L1222
	cmp	ip, #8
	beq	.L1481
	cmp	ip, #16
	beq	.L1482
.L1203:
	ldrb	r1, [r0, #9]	@ zero_extendqisi2
	mov	r2, r1, asl #1
	and	r2, r2, #255
	cmp	r2, #7
	mulls	r3, r2, r3
	strb	r2, [r0, #11]
	movhi	r2, r2, lsr #3
	mulhi	r3, r2, r3
	addls	r3, r3, #7
	mov	ip, #4
	mov	r1, #2
	movls	r3, r3, lsr #3
	strb	ip, [r0, #8]
	strb	r1, [r0, #10]
	str	r3, [r0, #4]
.L1222:
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
	bx	lr
.L1180:
	subs	r4, r2, #0
	movne	r4, #1
	cmp	ip, #2
	movne	r4, #0
	cmp	r4, #0
	beq	.L1222
	ldrb	ip, [r0, #9]	@ zero_extendqisi2
	cmp	ip, #8
	beq	.L1483
	cmp	ip, #16
	beq	.L1484
.L1213:
	mov	ip, ip, asl #2
	and	r1, ip, #255
	cmp	r1, #7
	mulls	r3, r1, r3
	strb	r1, [r0, #11]
	movhi	r1, r1, lsr #3
	mulhi	r3, r1, r3
	mov	r2, #6
	addls	r3, r3, #7
	strb	r2, [r0, #8]
	movls	r3, r3, lsr #3
	mov	r2, #4
	strb	r2, [r0, #10]
	str	r3, [r0, #4]
	b	.L1222
.L1186:
	and	ip, r4, #3
	add	r4, ip, ip, asl #2
	cmp	r3, #0
	add	r4, r4, r4, asl #4
	beq	.L1184
	sub	r8, r3, #1
	add	ip, r3, #3
	mvn	r6, ip
	add	ip, r1, r8, lsr #2
	ldrb	r7, [ip, #0]	@ zero_extendqisi2
	and	r5, r6, #3
	mov	r5, r5, asl #1
	mov	r6, r7, asr r5
	and	r6, r6, #3
	mov	r7, r6, asl #4
	orr	r7, r7, r6, asl #2
	orr	r7, r7, r6
	orr	r6, r7, r6, asl #6
	cmp	r5, #6
	add	r7, r1, r8
	strb	r6, [r7, #0]
	and	r8, r8, #3
	addne	r5, r5, #2
	beq	.L1485
.L1398:
	mov	r6, #1
	cmp	r3, r6
	bls	.L1184
	cmp	r8, #0
	beq	.L1195
	cmp	r8, r6
	beq	.L1349
	cmp	r8, #2
	beq	.L1350
	ldrb	sl, [ip, #0]	@ zero_extendqisi2
	mov	r8, sl, asr r5
	and	r8, r8, #3
	mov	sl, r8, asl #4
	orr	sl, sl, r8, asl #2
	orr	sl, sl, r8
	orr	r8, sl, r8, asl #6
	cmp	r5, #6
	strb	r8, [r7, #-1]
	addne	r5, r5, #2
	subeq	ip, ip, #1
	moveq	r5, #0
	add	r6, r6, #1
.L1350:
	ldrb	sl, [ip, #0]	@ zero_extendqisi2
	mov	r8, sl, asr r5
	and	r8, r8, #3
	mov	sl, r8, asl #4
	orr	sl, sl, r8, asl #2
	orr	sl, sl, r8
	orr	r8, sl, r8, asl #6
	rsb	sl, r6, #0
	cmp	r5, #6
	strb	r8, [r7, sl]
	addne	r5, r5, #2
	subeq	ip, ip, #1
	moveq	r5, #0
	add	r6, r6, #1
.L1349:
	ldrb	sl, [ip, #0]	@ zero_extendqisi2
	mov	r8, sl, asr r5
	and	r8, r8, #3
	mov	sl, r8, asl #4
	orr	sl, sl, r8, asl #2
	orr	sl, sl, r8
	cmp	r5, #6
	orr	r8, sl, r8, asl #6
	rsb	sl, r6, #0
	add	r6, r6, #1
	addne	r5, r5, #2
	subeq	ip, ip, #1
	moveq	r5, #0
	cmp	r3, r6
	strb	r8, [r7, sl]
	bhi	.L1195
	b	.L1184
.L1484:
	ldrh	r7, [r2, #4]
	ldrh	r9, [r2, #6]
	and	r8, r7, #255
	ldrh	r5, [r2, #2]
	mov	r4, r9, lsr #8
	str	r8, [sp, #4]
	and	r8, r9, #255
	str	r8, [sp, #8]
	str	r4, [sp, #12]
	cmp	r3, #0
	and	r6, r5, #255
	mov	r7, r7, lsr #8
	mov	r5, r5, lsr #8
	ldr	r9, [r0, #4]
	beq	.L1213
	add	sl, r9, r1
	sub	r9, sl, #2
	ldrb	r4, [r9, #-4]	@ zero_extendqisi2
	add	fp, r1, r3, asl #3
	cmp	r4, r5
	sub	r2, r3, #1
	sub	r8, fp, #3
	and	sl, r2, #1
	mvn	r4, #0
	beq	.L1486
.L1337:
	strb	r4, [r8, #2]
	strb	r4, [r8, #1]
.L1461:
	ldrb	r2, [r9, #1]	@ zero_extendqisi2
	mov	ip, r8
	strb	r2, [ip], #-8
	mov	r2, r9
	ldrb	fp, [r2], #-6	@ zero_extendqisi2
	strb	fp, [r8, #-1]!
	mov	r1, r9
	ldrb	r9, [r1, #-1]!	@ zero_extendqisi2
	strb	r9, [r8, #-1]!
	ldrb	r9, [r1, #-1]!	@ zero_extendqisi2
	strb	r9, [r8, #-1]!
	ldrb	r9, [r1, #-1]!	@ zero_extendqisi2
	strb	r9, [r8, #-1]!
	ldrb	r9, [r1, #-1]	@ zero_extendqisi2
	mov	r1, #1
	cmp	r3, r1
	strb	r9, [r8, #-1]
	bls	.L1462
	cmp	sl, #0
	beq	.L1219
	ldrb	r8, [r2, #-4]	@ zero_extendqisi2
	cmp	r8, r5
	beq	.L1463
.L1339:
	strb	r4, [ip, #2]
	strb	r4, [ip, #1]
.L1464:
	ldrb	r8, [r2, #1]	@ zero_extendqisi2
	strb	r8, [ip, #0]
	ldrb	sl, [r2, #0]	@ zero_extendqisi2
	mov	r8, ip
	strb	sl, [r8, #-1]!
	mov	sl, r2
	ldrb	r9, [sl, #-1]!	@ zero_extendqisi2
	strb	r9, [r8, #-1]!
	ldrb	r9, [sl, #-1]!	@ zero_extendqisi2
	strb	r9, [r8, #-1]!
	ldrb	r9, [sl, #-1]!	@ zero_extendqisi2
	strb	r9, [r8, #-1]!
	add	r1, r1, #1
	ldrb	sl, [sl, #-1]	@ zero_extendqisi2
	cmp	r3, r1
	strb	sl, [r8, #-1]
	sub	ip, ip, #8
	sub	r2, r2, #6
	bhi	.L1219
.L1462:
	ldrb	ip, [r0, #9]	@ zero_extendqisi2
	b	.L1213
.L1498:
	sub	ip, ip, #1
	mov	r8, #0
.L1405:
	ldrb	sl, [ip, #0]	@ zero_extendqisi2
	mov	r5, sl, asr r8
	and	r5, r5, #3
	mov	sl, r5, asl #4
	orr	sl, sl, r5, asl #2
	orr	sl, sl, r5
	orr	r5, sl, r5, asl #6
	mvn	sl, r6
	cmp	r8, #6
	strb	r5, [r7, sl]
	subeq	ip, ip, #1
	ldrb	sl, [ip, #0]	@ zero_extendqisi2
	addne	r8, r8, #2
	moveq	r8, #0
	mov	r5, sl, asr r8
	and	r5, r5, #3
	mov	sl, r5, asl #4
	orr	sl, sl, r5, asl #2
	orr	sl, sl, r5
	orr	r5, sl, r5, asl #6
	cmp	r8, #6
	rsb	sl, r6, r7
	add	r6, r6, #3
	strb	r5, [sl, #-2]
	subeq	ip, ip, #1
	addne	r5, r8, #2
	moveq	r5, #0
	cmp	r3, r6
	bls	.L1184
.L1195:
	ldrb	sl, [ip, #0]	@ zero_extendqisi2
	mov	r8, sl, asr r5
	and	r8, r8, #3
	mov	sl, r8, asl #4
	orr	sl, sl, r8, asl #2
	orr	sl, sl, r8
	orr	r8, sl, r8, asl #6
	rsb	sl, r6, #0
	cmp	r5, #6
	strb	r8, [r7, sl]
	subeq	ip, ip, #1
	ldrb	sl, [ip, #0]	@ zero_extendqisi2
	addne	r8, r5, #2
	moveq	r8, #0
	mov	r5, sl, asr r8
	and	r5, r5, #3
	mov	sl, r5, asl #4
	orr	sl, sl, r5, asl #2
	orr	sl, sl, r5
	add	r6, r6, #1
	orr	r5, sl, r5, asl #6
	cmp	r8, #6
	rsb	sl, r6, #0
	strb	r5, [r7, sl]
	addne	r8, r8, #2
	bne	.L1405
	b	.L1498
.L1499:
	ldrb	r8, [r2, #-3]	@ zero_extendqisi2
	cmp	r8, r6
	bne	.L1217
	ldrb	sl, [r2, #-2]	@ zero_extendqisi2
	cmp	sl, r7
	bne	.L1217
	ldrb	r9, [r2, #-1]	@ zero_extendqisi2
	ldr	r8, [sp, #4]
	cmp	r9, r8
	bne	.L1217
	ldrb	r9, [r2, #0]	@ zero_extendqisi2
	ldr	sl, [sp, #12]
	cmp	r9, sl
	bne	.L1217
	ldrb	r8, [r2, #1]	@ zero_extendqisi2
	ldr	r9, [sp, #8]
	cmp	r8, r9
	moveq	r8, #0
	streqb	r8, [ip, #2]
	streqb	r8, [ip, #1]
	beq	.L1218
.L1217:
	strb	r4, [ip, #2]
	strb	r4, [ip, #1]
.L1218:
	ldrb	r8, [r2, #1]	@ zero_extendqisi2
	mov	sl, ip
	strb	r8, [sl], #-1
	mov	r8, r2
	ldrb	r9, [r8], #-1	@ zero_extendqisi2
	strb	r9, [ip, #-1]
	ldrb	r9, [r2, #-1]	@ zero_extendqisi2
	strb	r9, [sl, #-1]
	ldrb	r9, [r8, #-1]	@ zero_extendqisi2
	sub	sl, sl, #1
	strb	r9, [sl, #-1]
	sub	r8, r8, #1
	ldrb	r9, [r8, #-1]	@ zero_extendqisi2
	sub	sl, sl, #1
	strb	r9, [sl, #-1]
	ldrb	r8, [r8, #-2]	@ zero_extendqisi2
	strb	r8, [sl, #-2]
	sub	r2, r2, #6
	ldrb	sl, [r2, #-4]	@ zero_extendqisi2
	cmp	sl, r5
	add	r1, r1, #1
	sub	ip, ip, #8
	beq	.L1488
.L1342:
	strb	r4, [ip, #2]
	strb	r4, [ip, #1]
.L1465:
	ldrb	sl, [r2, #1]	@ zero_extendqisi2
	mov	r8, ip
	strb	sl, [r8], #-1
	mov	sl, r2
	ldrb	r9, [sl], #-1	@ zero_extendqisi2
	strb	r9, [ip, #-1]
	ldrb	r9, [r2, #-1]	@ zero_extendqisi2
	strb	r9, [r8, #-1]
	ldrb	r9, [sl, #-1]	@ zero_extendqisi2
	sub	r8, r8, #1
	strb	r9, [r8, #-1]
	sub	sl, sl, #1
	ldrb	r9, [sl, #-1]	@ zero_extendqisi2
	sub	r8, r8, #1
	strb	r9, [r8, #-1]
	add	r1, r1, #1
	ldrb	sl, [sl, #-2]	@ zero_extendqisi2
	cmp	r3, r1
	strb	sl, [r8, #-2]
	sub	ip, ip, #8
	sub	r2, r2, #6
	bls	.L1462
.L1219:
	ldrb	r9, [r2, #-4]	@ zero_extendqisi2
	cmp	r9, r5
	bne	.L1217
	b	.L1499
.L1481:
	cmp	r3, #0
	and	r4, r4, #255
	beq	.L1203
	sub	r7, r3, #1
	add	r2, r1, r7
	ldrb	r5, [r2, #0]	@ zero_extendqisi2
	add	r1, r1, r3, asl #1
	cmp	r5, r4
	sub	r1, r1, #2
	mvn	r5, #0
	and	r6, r7, #3
	strneb	r5, [r1, #1]
	mov	r7, #0
	beq	.L1490
.L1375:
	ldrb	ip, [r2, #0]	@ zero_extendqisi2
	cmp	r3, #1
	strb	ip, [r1], #-2
	mvn	ip, #0
	bls	.L1203
	cmp	r6, #0
	beq	.L1206
	cmp	r6, #1
	beq	.L1345
	cmp	r6, #2
	beq	.L1346
	ldrb	r8, [r2, ip]	@ zero_extendqisi2
	cmp	r8, r4
	strneb	r5, [r1, #1]
	streqb	r7, [r1, #1]
	ldrb	r6, [r2, ip]	@ zero_extendqisi2
	strb	r6, [r1], #-2
	sub	ip, ip, #1
.L1346:
	ldrb	r8, [r2, ip]	@ zero_extendqisi2
	cmp	r8, r4
	strneb	r5, [r1, #1]
	streqb	r7, [r1, #1]
	ldrb	r6, [r2, ip]	@ zero_extendqisi2
	strb	r6, [r1], #-2
	sub	ip, ip, #1
.L1345:
	ldrb	r6, [r2, ip]	@ zero_extendqisi2
	cmp	r6, r4
	strneb	r5, [r1, #1]
	streqb	r7, [r1, #1]
	ldrb	r6, [r2, ip]	@ zero_extendqisi2
	sub	ip, ip, #1
	rsb	r8, ip, #0
	cmp	r3, r8
	strb	r6, [r1], #-2
	bhi	.L1206
	b	.L1203
.L1382:
	ldrb	sl, [r2, ip]	@ zero_extendqisi2
	mov	r6, r1
	strb	sl, [r6], #-2
	sub	r8, ip, #1
	ldrb	sl, [r2, r8]	@ zero_extendqisi2
	cmp	sl, r4
	strneb	r5, [r6, #1]
	streqb	r7, [r6, #1]
	ldrb	r6, [r2, r8]	@ zero_extendqisi2
	strb	r6, [r1, #-2]
	sub	r6, ip, #2
	ldrb	r8, [r2, r6]	@ zero_extendqisi2
	cmp	r8, r4
	sub	r8, r1, #4
	strneb	r5, [r8, #1]
	streqb	r7, [r8, #1]
	sub	ip, ip, #3
	ldrb	r6, [r2, r6]	@ zero_extendqisi2
	rsb	r8, ip, #0
	cmp	r3, r8
	strb	r6, [r1, #-4]
	sub	r1, r1, #6
	bls	.L1203
.L1206:
	ldrb	r8, [r2, ip]	@ zero_extendqisi2
	cmp	r8, r4
	strneb	r5, [r1, #1]
	streqb	r7, [r1, #1]
	ldrb	r6, [r2, ip]	@ zero_extendqisi2
	strb	r6, [r1], #-2
	sub	ip, ip, #1
	ldrb	sl, [r2, ip]	@ zero_extendqisi2
	cmp	sl, r4
	strneb	r5, [r1, #1]
	streqb	r7, [r1, #1]
	b	.L1382
.L1482:
	mov	r2, r4, lsr #8
	cmp	r3, #0
	and	r5, r2, #255
	and	r7, r4, #255
	ldr	ip, [r0, #4]
	beq	.L1203
	sub	r4, r3, #1
	add	r8, r1, ip, asl #1
	mov	r6, #0
	sub	ip, ip, #1
	ands	r4, r4, #3
	add	r1, r1, ip
	sub	r2, r8, #3
	mvn	ip, #0
	mov	r8, r6
	beq	.L1209
	ldrb	r6, [r1, #-1]	@ zero_extendqisi2
	cmp	r6, r5
	beq	.L1387
.L1247:
	strb	ip, [r2, #2]
	strb	ip, [r2, #1]
.L1388:
	ldrb	r6, [r1, #0]	@ zero_extendqisi2
	strb	r6, [r2, #0]
	ldrb	r6, [r1, #-1]	@ zero_extendqisi2
	cmp	r4, #1
	strb	r6, [r2, #-1]
	sub	r1, r1, #2
	sub	r2, r2, #4
	mov	r6, #1
	beq	.L1209
	cmp	r4, #2
	beq	.L1347
	ldrb	r4, [r1, #-1]	@ zero_extendqisi2
	cmp	r4, r5
	beq	.L1389
.L1248:
	strb	ip, [r2, #2]
	strb	ip, [r2, #1]
.L1390:
	ldrb	r4, [r1, #0]	@ zero_extendqisi2
	strb	r4, [r2, #0]
	ldrb	r4, [r1, #-1]	@ zero_extendqisi2
	add	r6, r6, #1
	strb	r4, [r2, #-1]
	sub	r1, r1, #2
	sub	r2, r2, #4
.L1347:
	ldrb	r4, [r1, #-1]	@ zero_extendqisi2
	cmp	r4, r5
	beq	.L1391
.L1250:
	strb	ip, [r2, #2]
	strb	ip, [r2, #1]
.L1392:
	ldrb	r4, [r1, #0]	@ zero_extendqisi2
	strb	r4, [r2, #0]
	ldrb	r4, [r1, #-1]	@ zero_extendqisi2
	add	r6, r6, #1
	strb	r4, [r2, #-1]
	sub	r1, r1, #2
	sub	r2, r2, #4
	b	.L1209
.L1207:
	strb	ip, [r2, #2]
	strb	ip, [r2, #1]
.L1208:
	ldrb	r4, [r1, #0]	@ zero_extendqisi2
	strb	r4, [r2, #0]
	add	r6, r6, #1
	ldrb	r4, [r1, #-1]	@ zero_extendqisi2
	cmp	r3, r6
	strb	r4, [r2, #-1]
	sub	r2, r2, #4
	bls	.L1203
	sub	r4, r1, #2
	ldrb	r9, [r4, #-1]	@ zero_extendqisi2
	cmp	r9, r5
	beq	.L1491
.L1253:
	strb	ip, [r2, #2]
	strb	ip, [r2, #1]
.L1393:
	ldrb	r9, [r4, #0]	@ zero_extendqisi2
	strb	r9, [r2, #0]
	ldrb	sl, [r4, #-1]	@ zero_extendqisi2
	strb	sl, [r2, #-1]
	sub	sl, r4, #2
	ldrb	r1, [sl, #-1]	@ zero_extendqisi2
	cmp	r1, r5
	sub	r1, r2, #4
	beq	.L1394
.L1254:
	strb	ip, [r1, #2]
	strb	ip, [r1, #1]
.L1395:
	ldrb	r9, [r4, #-2]	@ zero_extendqisi2
	strb	r9, [r2, #-4]
	ldrb	sl, [sl, #-1]	@ zero_extendqisi2
	strb	sl, [r1, #-1]
	sub	sl, r4, #4
	ldrb	r1, [sl, #-1]	@ zero_extendqisi2
	cmp	r1, r5
	sub	r1, r2, #8
	beq	.L1396
.L1255:
	strb	ip, [r1, #2]
	strb	ip, [r1, #1]
.L1397:
	ldrb	r9, [r4, #-4]	@ zero_extendqisi2
	strb	r9, [r2, #-8]
	ldrb	sl, [sl, #-1]	@ zero_extendqisi2
	add	r6, r6, #3
	strb	sl, [r1, #-1]
	sub	r2, r2, #12
	sub	r1, r4, #6
.L1209:
	ldrb	r4, [r1, #-1]	@ zero_extendqisi2
	cmp	r4, r5
	bne	.L1207
	ldrb	r4, [r1, #0]	@ zero_extendqisi2
	cmp	r4, r7
	streqb	r8, [r2, #2]
	streqb	r8, [r2, #1]
	bne	.L1207
	b	.L1208
.L1483:
	cmp	r3, #0
	ldrb	r8, [r2, #6]	@ zero_extendqisi2
	ldrb	r5, [r2, #2]	@ zero_extendqisi2
	ldrb	r7, [r2, #4]	@ zero_extendqisi2
	ldr	r2, [r0, #4]
	beq	.L1213
	add	r4, r2, r1
	sub	r2, r4, #2
	ldrb	r6, [r2, #-1]	@ zero_extendqisi2
	add	sl, r1, r3, asl #2
	sub	fp, r3, #1
	cmp	r6, r5
	sub	ip, sl, #2
	mvn	r6, #0
	and	sl, fp, #3
	mov	fp, #0
	beq	.L1492
.L1320:
	strb	r6, [ip, #1]
.L1448:
	ldrb	r9, [r2, #1]	@ zero_extendqisi2
	mov	r1, ip
	strb	r9, [r1], #-4
	ldrb	r4, [r2, #0]	@ zero_extendqisi2
	strb	r4, [ip, #-1]!
	mov	r4, #1
	ldrb	r9, [r2, #-1]	@ zero_extendqisi2
	cmp	r3, r4
	strb	r9, [ip, #-1]
	sub	r2, r2, #3
	bls	.L1462
	cmp	sl, #0
	beq	.L1477
	cmp	sl, r4
	beq	.L1358
	cmp	sl, #2
	beq	.L1359
	ldrb	ip, [r2, #-1]	@ zero_extendqisi2
	cmp	ip, r5
	beq	.L1450
.L1322:
	strb	r6, [r1, #1]
.L1451:
	ldrb	ip, [r2, #1]	@ zero_extendqisi2
	strb	ip, [r1, #0]
	ldrb	sl, [r2, #0]	@ zero_extendqisi2
	mov	ip, r1
	strb	sl, [ip, #-1]!
	ldrb	sl, [r2, #-1]	@ zero_extendqisi2
	strb	sl, [ip, #-1]
	add	r4, r4, #1
	sub	r1, r1, #4
	sub	r2, r2, #3
.L1359:
	ldrb	sl, [r2, #-1]	@ zero_extendqisi2
	cmp	sl, r5
	beq	.L1452
.L1325:
	strb	r6, [r1, #1]
.L1453:
	ldrb	ip, [r2, #1]	@ zero_extendqisi2
	strb	ip, [r1, #0]
	ldrb	sl, [r2, #0]	@ zero_extendqisi2
	mov	ip, r1
	strb	sl, [ip, #-1]!
	ldrb	sl, [r2, #-1]	@ zero_extendqisi2
	strb	sl, [ip, #-1]
	add	r4, r4, #1
	sub	r1, r1, #4
	sub	r2, r2, #3
.L1358:
	ldrb	sl, [r2, #-1]	@ zero_extendqisi2
	cmp	sl, r5
	beq	.L1454
.L1328:
	strb	r6, [r1, #1]
.L1455:
	ldrb	ip, [r2, #1]	@ zero_extendqisi2
	strb	ip, [r1, #0]
	ldrb	sl, [r2, #0]	@ zero_extendqisi2
	mov	ip, r1
	strb	sl, [ip, #-1]!
	add	r4, r4, #1
	ldrb	sl, [r2, #-1]	@ zero_extendqisi2
	cmp	r3, r4
	strb	sl, [ip, #-1]
	sub	r1, r1, #4
	sub	r2, r2, #3
	bls	.L1462
.L1477:
	mov	r9, r0
.L1216:
	ldrb	r0, [r2, #-1]	@ zero_extendqisi2
	cmp	r0, r5
	beq	.L1493
.L1214:
	strb	r6, [r1, #1]
.L1215:
	ldrb	ip, [r2, #1]	@ zero_extendqisi2
	mov	r0, r1
	strb	ip, [r0], #-1
	ldrb	sl, [r2, #0]	@ zero_extendqisi2
	strb	sl, [r1, #-1]
	ldrb	ip, [r2, #-1]	@ zero_extendqisi2
	strb	ip, [r0, #-1]
	sub	r0, r2, #3
	ldrb	sl, [r0, #-1]	@ zero_extendqisi2
	cmp	sl, r5
	add	r4, r4, #1
	sub	r1, r1, #4
	beq	.L1494
.L1331:
	strb	r6, [r1, #1]
.L1456:
	ldrb	ip, [r0, #1]	@ zero_extendqisi2
	mov	r2, r1
	strb	ip, [r2], #-1
	ldrb	sl, [r0, #0]	@ zero_extendqisi2
	strb	sl, [r1, #-1]
	ldrb	ip, [r0, #-1]	@ zero_extendqisi2
	strb	ip, [r2, #-1]
	sub	r2, r0, #3
	ldrb	sl, [r2, #-1]	@ zero_extendqisi2
	cmp	sl, r5
	sub	ip, r1, #4
	beq	.L1457
.L1333:
	strb	r6, [ip, #1]
.L1458:
	ldrb	sl, [r2, #1]	@ zero_extendqisi2
	strb	sl, [r1, #-4]
	ldrb	sl, [r0, #-3]	@ zero_extendqisi2
	strb	sl, [ip, #-1]
	ldrb	r2, [r2, #-1]	@ zero_extendqisi2
	strb	r2, [ip, #-2]
	sub	r2, r0, #6
	ldrb	ip, [r2, #-1]	@ zero_extendqisi2
	cmp	ip, r5
	sub	ip, r1, #8
	beq	.L1459
.L1335:
	strb	r6, [ip, #1]
.L1460:
	ldrb	sl, [r2, #1]	@ zero_extendqisi2
	strb	sl, [r1, #-8]
	ldrb	sl, [r0, #-6]	@ zero_extendqisi2
	strb	sl, [ip, #-1]
	add	r4, r4, #3
	ldrb	r2, [r2, #-1]	@ zero_extendqisi2
	cmp	r3, r4
	strb	r2, [ip, #-2]
	sub	r1, r1, #12
	sub	r2, r0, #9
	bhi	.L1216
	mov	r0, r9
	ldrb	ip, [r0, #9]	@ zero_extendqisi2
	b	.L1213
.L1459:
	ldrb	sl, [r0, #-6]	@ zero_extendqisi2
	cmp	sl, r7
	bne	.L1335
	ldrb	sl, [r2, #1]	@ zero_extendqisi2
	cmp	sl, r8
	bne	.L1335
	strb	fp, [ip, #1]
	b	.L1460
.L1457:
	ldrb	sl, [r0, #-3]	@ zero_extendqisi2
	cmp	sl, r7
	bne	.L1333
	ldrb	sl, [r2, #1]	@ zero_extendqisi2
	cmp	sl, r8
	bne	.L1333
	strb	fp, [ip, #1]
	b	.L1458
.L1494:
	ldrb	r2, [r2, #-3]	@ zero_extendqisi2
	cmp	r2, r7
	bne	.L1331
	ldrb	sl, [r0, #1]	@ zero_extendqisi2
	cmp	sl, r8
	bne	.L1331
	strb	fp, [r1, #1]
	b	.L1456
.L1493:
	ldrb	ip, [r2, #0]	@ zero_extendqisi2
	cmp	ip, r7
	bne	.L1214
	ldrb	r0, [r2, #1]	@ zero_extendqisi2
	cmp	r0, r8
	moveq	sl, fp
	streqb	sl, [r1, #1]
	bne	.L1214
	b	.L1215
.L1488:
	ldrb	r9, [r2, #-3]	@ zero_extendqisi2
	cmp	r9, r6
	bne	.L1342
	ldrb	sl, [r2, #-2]	@ zero_extendqisi2
	cmp	sl, r7
	bne	.L1342
	ldrb	r8, [r2, #-1]	@ zero_extendqisi2
	ldr	r9, [sp, #4]
	cmp	r8, r9
	bne	.L1342
	ldrb	r8, [r2, #0]	@ zero_extendqisi2
	ldr	sl, [sp, #12]
	cmp	r8, sl
	bne	.L1342
	ldrb	r9, [r2, #1]	@ zero_extendqisi2
	ldr	r8, [sp, #8]
	cmp	r9, r8
	bne	.L1342
	mov	sl, #0
	strb	sl, [ip, #2]
	strb	sl, [ip, #1]
	b	.L1465
.L1396:
	ldrb	r9, [r4, #-4]	@ zero_extendqisi2
	cmp	r9, r7
	bne	.L1255
	strb	r8, [r1, #2]
	strb	r8, [r1, #1]
	b	.L1397
.L1394:
	ldrb	r9, [r4, #-2]	@ zero_extendqisi2
	cmp	r9, r7
	bne	.L1254
	strb	r8, [r1, #2]
	strb	r8, [r1, #1]
	b	.L1395
.L1491:
	ldrb	r9, [r1, #-2]	@ zero_extendqisi2
	cmp	r9, r7
	bne	.L1253
	strb	r8, [r2, #2]
	strb	r8, [r2, #1]
	b	.L1393
.L1480:
	and	r4, r4, #1
	mov	ip, r4, asl #8
	rsb	r4, r4, ip
	mov	r4, r4, asl #16
	cmp	r3, #0
	mov	r4, r4, lsr #16
	beq	.L1184
	sub	r7, r3, #1
	add	ip, r1, r7, lsr #3
	add	r6, r3, #7
	ldrb	sl, [ip, #0]	@ zero_extendqisi2
	mvn	r5, r6
	and	r5, r5, #7
	mov	r8, sl, asr r5
	and	sl, r7, #3
	ands	r6, r8, #1
	add	r7, r1, r7
	mvn	r8, #0
	streqb	r6, [r7, #0]
	bne	.L1495
.L1422:
	cmp	r5, #7
	addne	r5, r5, #1
	subeq	ip, ip, #1
	moveq	r5, #0
	cmp	r3, #1
	mvn	r6, #0
	bls	.L1184
	cmp	sl, #0
	beq	.L1192
	cmp	sl, #1
	beq	.L1355
	cmp	sl, #2
	beq	.L1356
	ldrb	sl, [ip, #0]	@ zero_extendqisi2
	mov	sl, sl, asr r5
	ands	sl, sl, #1
	streqb	sl, [r7, r6]
	strneb	r8, [r7, r6]
	cmp	r5, #7
	addne	r5, r5, #1
	subeq	ip, ip, #1
	moveq	r5, #0
	sub	r6, r6, #1
.L1356:
	ldrb	sl, [ip, #0]	@ zero_extendqisi2
	mov	sl, sl, asr r5
	ands	sl, sl, #1
	streqb	sl, [r7, r6]
	strneb	r8, [r7, r6]
	cmp	r5, #7
	addne	r5, r5, #1
	subeq	ip, ip, #1
	moveq	r5, #0
	sub	r6, r6, #1
.L1355:
	ldrb	sl, [ip, #0]	@ zero_extendqisi2
	mov	sl, sl, asr r5
	ands	sl, sl, #1
	streqb	sl, [r7, r6]
	strneb	r8, [r7, r6]
	sub	r6, r6, #1
	cmp	r5, #7
	rsb	sl, r6, #0
	addne	r5, r5, #1
	subeq	ip, ip, #1
	moveq	r5, #0
	cmp	r3, sl
	bls	.L1184
.L1192:
	ldrb	sl, [ip, #0]	@ zero_extendqisi2
	mov	r9, sl, asr r5
	ands	sl, r9, #1
	streqb	sl, [r7, r6]
	strneb	r8, [r7, r6]
	cmp	r5, #7
	subeq	ip, ip, #1
	ldrb	r9, [ip, #0]	@ zero_extendqisi2
	moveq	r5, #0
	addne	r5, r5, #1
	mov	sl, r9, asr r5
	sub	r6, r6, #1
	ands	r9, sl, #1
	streqb	r9, [r7, r6]
	strneb	r8, [r7, r6]
.L1437:
	cmp	r5, #7
	subeq	ip, ip, #1
	ldrb	sl, [ip, #0]	@ zero_extendqisi2
	addne	r5, r5, #1
	moveq	r5, #0
	mov	r9, sl, asr r5
	ands	sl, r9, #1
	sub	r9, r6, #1
	streqb	sl, [r7, r9]
	strneb	r8, [r7, r9]
	cmp	r5, #7
	subeq	ip, ip, #1
	ldrb	sl, [ip, #0]	@ zero_extendqisi2
	addne	r5, r5, #1
	moveq	r5, #0
	mov	r9, sl, asr r5
	ands	sl, r9, #1
	sub	r9, r6, #2
	sub	r6, r6, #3
	streqb	sl, [r7, r9]
	strneb	r8, [r7, r9]
	rsb	sl, r6, #0
	cmp	r5, #7
	addne	r5, r5, #1
	subeq	ip, ip, #1
	moveq	r5, #0
	cmp	r3, sl
	bhi	.L1192
	b	.L1184
.L1187:
	tst	r3, #1
	and	r4, r4, #15
	movne	r7, #4
	moveq	r7, #0
	cmp	r3, #0
	add	r4, r4, r4, asl #4
	beq	.L1184
	sub	r6, r3, #1
	add	r5, r1, r6, lsr #1
	ldrb	r8, [r5, #0]	@ zero_extendqisi2
	mov	r8, r8, asr r7
	and	r8, r8, #15
	orr	r8, r8, r8, asl #4
	cmp	r7, #4
	add	r7, r1, r6
	strb	r8, [r7, #0]
	and	r8, r6, #3
	beq	.L1496
.L1410:
	mov	r6, #1
	cmp	r3, r6
	bls	.L1184
	cmp	r8, #0
	beq	.L1200
	cmp	r8, r6
	beq	.L1352
	cmp	r8, #2
	beq	.L1353
	ldrb	r8, [r5, #0]	@ zero_extendqisi2
	mov	r8, r8, asr ip
	and	r8, r8, #15
	orr	r8, r8, r8, asl #4
	cmp	ip, #4
	strb	r8, [r7, #-1]
	subeq	r5, r5, #1
	moveq	ip, #0
	movne	ip, #4
	add	r6, r6, #1
.L1353:
	ldrb	r8, [r5, #0]	@ zero_extendqisi2
	mov	r8, r8, asr ip
	and	r8, r8, #15
	cmp	ip, #4
	orr	r8, r8, r8, asl #4
	rsb	ip, r6, #0
	strb	r8, [r7, ip]
	subeq	r5, r5, #1
	moveq	ip, #0
	movne	ip, #4
	add	r6, r6, #1
.L1352:
	ldrb	r8, [r5, #0]	@ zero_extendqisi2
	mov	r8, r8, asr ip
	and	r8, r8, #15
	cmp	ip, #4
	orr	r8, r8, r8, asl #4
	rsb	ip, r6, #0
	add	r6, r6, #1
	strb	r8, [r7, ip]
	subeq	r5, r5, #1
	moveq	ip, #0
	movne	ip, #4
	cmp	r3, r6
	bhi	.L1200
	b	.L1184
.L1500:
	sub	r5, r5, #1
	mov	r8, #0
.L1417:
	ldrb	ip, [r5, #0]	@ zero_extendqisi2
	mov	ip, ip, asr r8
	and	ip, ip, #15
	cmp	r8, #4
	orr	ip, ip, ip, asl #4
	mvn	r8, r6
	strb	ip, [r7, r8]
	subeq	r5, r5, #1
	ldrb	ip, [r5, #0]	@ zero_extendqisi2
	moveq	r8, #0
	movne	r8, #4
	mov	ip, ip, asr r8
	and	ip, ip, #15
	cmp	r8, #4
	orr	ip, ip, ip, asl #4
	rsb	r8, r6, r7
	add	r6, r6, #3
	strb	ip, [r8, #-2]
	subeq	r5, r5, #1
	moveq	ip, #0
	movne	ip, #4
	cmp	r3, r6
	bls	.L1184
.L1200:
	ldrb	r8, [r5, #0]	@ zero_extendqisi2
	mov	r8, r8, asr ip
	and	r8, r8, #15
	orr	r8, r8, r8, asl #4
	cmp	ip, #4
	rsb	ip, r6, #0
	strb	r8, [r7, ip]
	subeq	r5, r5, #1
	ldrb	ip, [r5, #0]	@ zero_extendqisi2
	movne	r8, #4
	moveq	r8, #0
	mov	ip, ip, asr r8
	add	r6, r6, #1
	and	ip, ip, #15
	cmp	r8, #4
	orr	ip, ip, ip, asl #4
	rsb	r8, r6, #0
	strb	ip, [r7, r8]
	movne	r8, #4
	bne	.L1417
	b	.L1500
.L1495:
	strb	r8, [r7, #0]
	b	.L1422
.L1492:
	ldrb	r4, [r2, #0]	@ zero_extendqisi2
	cmp	r4, r7
	bne	.L1320
	ldrb	r1, [r2, #1]	@ zero_extendqisi2
	cmp	r1, r8
	bne	.L1320
	strb	fp, [ip, #1]
	b	.L1448
.L1450:
	ldrb	sl, [r2, #0]	@ zero_extendqisi2
	cmp	sl, r7
	bne	.L1322
	ldrb	ip, [r2, #1]	@ zero_extendqisi2
	cmp	ip, r8
	bne	.L1322
	strb	fp, [r1, #1]
	b	.L1451
.L1454:
	ldrb	ip, [r2, #0]	@ zero_extendqisi2
	cmp	ip, r7
	bne	.L1328
	ldrb	sl, [r2, #1]	@ zero_extendqisi2
	cmp	sl, r8
	bne	.L1328
	strb	fp, [r1, #1]
	b	.L1455
.L1452:
	ldrb	ip, [r2, #0]	@ zero_extendqisi2
	cmp	ip, r7
	bne	.L1325
	ldrb	sl, [r2, #1]	@ zero_extendqisi2
	cmp	sl, r8
	bne	.L1325
	strb	fp, [r1, #1]
	b	.L1453
.L1496:
	sub	r5, r5, #1
	mov	ip, #0
	b	.L1410
.L1485:
	sub	ip, ip, #1
	mov	r5, #0
	b	.L1398
.L1486:
	ldrb	fp, [r9, #-3]	@ zero_extendqisi2
	cmp	fp, r6
	bne	.L1337
	ldrb	r1, [r9, #-2]	@ zero_extendqisi2
	cmp	r1, r7
	bne	.L1337
	ldrb	r2, [r9, #-1]	@ zero_extendqisi2
	ldr	ip, [sp, #4]
	cmp	r2, ip
	bne	.L1337
	ldrb	r1, [r9, #0]	@ zero_extendqisi2
	ldr	fp, [sp, #12]
	cmp	r1, fp
	bne	.L1337
	ldrb	r2, [r9, #1]	@ zero_extendqisi2
	ldr	ip, [sp, #8]
	cmp	r2, ip
	bne	.L1337
	mov	fp, #0
	strb	fp, [r8, #2]
	strb	fp, [r8, #1]
	b	.L1461
.L1463:
	ldrb	sl, [r2, #-3]	@ zero_extendqisi2
	cmp	sl, r6
	bne	.L1339
	ldrb	r9, [r2, #-2]	@ zero_extendqisi2
	cmp	r9, r7
	bne	.L1339
	ldrb	r8, [r2, #-1]	@ zero_extendqisi2
	ldr	sl, [sp, #4]
	cmp	r8, sl
	bne	.L1339
	ldrb	r9, [r2, #0]	@ zero_extendqisi2
	ldr	r8, [sp, #12]
	cmp	r9, r8
	bne	.L1339
	ldrb	r9, [r2, #1]	@ zero_extendqisi2
	ldr	sl, [sp, #8]
	cmp	r9, sl
	bne	.L1339
	mov	r8, #0
	strb	r8, [ip, #2]
	strb	r8, [ip, #1]
	b	.L1464
.L1490:
	strb	r7, [r1, #1]
	b	.L1375
.L1387:
	ldrb	r6, [r1, #0]	@ zero_extendqisi2
	cmp	r6, r7
	bne	.L1247
	strb	r8, [r2, #2]
	strb	r8, [r2, #1]
	b	.L1388
.L1391:
	ldrb	r4, [r1, #0]	@ zero_extendqisi2
	cmp	r4, r7
	bne	.L1250
	strb	r8, [r2, #2]
	strb	r8, [r2, #1]
	b	.L1392
.L1389:
	ldrb	r4, [r1, #0]	@ zero_extendqisi2
	cmp	r4, r7
	bne	.L1248
	strb	r8, [r2, #2]
	strb	r8, [r2, #1]
	b	.L1390
	.size	png_do_expand, .-png_do_expand
	.section	.text.png_do_dither,"ax",%progbits
	.align	2
	.global	png_do_dither
	.hidden	png_do_dither
	.type	png_do_dither, %function
png_do_dither:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, r5, r6, r7, r8, sl}
	ldrb	ip, [r0, #8]	@ zero_extendqisi2
	subs	r4, r2, #0
	movne	r4, #1
	cmp	ip, #2
	movne	r5, #0
	andeq	r5, r4, #1
	cmp	r5, #0
	ldr	r5, [r0, #0]
	beq	.L1502
	ldrb	r6, [r0, #9]	@ zero_extendqisi2
	cmp	r6, #8
	beq	.L1559
.L1502:
	cmp	ip, #6
	movne	r4, #0
	andeq	r4, r4, #1
	cmp	r4, #0
	beq	.L1508
	ldrb	r4, [r0, #9]	@ zero_extendqisi2
	cmp	r4, #8
	beq	.L1560
.L1508:
	subs	r2, r3, #0
	movne	r2, #1
	cmp	ip, #3
	movne	r2, #0
	cmp	r2, #0
	beq	.L1514
	ldrb	ip, [r0, #9]	@ zero_extendqisi2
	cmp	ip, #8
	beq	.L1561
.L1514:
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl}
	bx	lr
.L1561:
	cmp	r5, #0
	beq	.L1514
	ldrb	ip, [r1, #0]	@ zero_extendqisi2
	ldrb	r2, [r3, ip]	@ zero_extendqisi2
	sub	r0, r5, #1
	cmp	r5, #1
	strb	r2, [r1, #0]
	and	r0, r0, #3
	mov	r2, #1
	bls	.L1514
	cmp	r0, #0
	beq	.L1513
	cmp	r0, #1
	beq	.L1555
	cmp	r0, #2
	ldrneb	r2, [r1, #1]	@ zero_extendqisi2
	ldrneb	r2, [r3, r2]	@ zero_extendqisi2
	strneb	r2, [r1, #1]
	movne	r2, #2
	ldrb	ip, [r1, r2]	@ zero_extendqisi2
	ldrb	r0, [r3, ip]	@ zero_extendqisi2
	strb	r0, [r1, r2]
	add	r2, r2, #1
.L1555:
	ldrb	ip, [r1, r2]	@ zero_extendqisi2
	ldrb	r0, [r3, ip]	@ zero_extendqisi2
	strb	r0, [r1, r2]
	add	r2, r2, #1
	cmp	r5, r2
	bls	.L1514
.L1513:
	ldrb	ip, [r1, r2]	@ zero_extendqisi2
	ldrb	r0, [r3, ip]	@ zero_extendqisi2
	strb	r0, [r1, r2]
	add	r0, r2, #1
	ldrb	ip, [r1, r0]	@ zero_extendqisi2
	ldrb	ip, [r3, ip]	@ zero_extendqisi2
	strb	ip, [r1, r0]
	add	r0, r0, #1
	ldrb	ip, [r1, r0]	@ zero_extendqisi2
	ldrb	ip, [r3, ip]	@ zero_extendqisi2
	strb	ip, [r1, r0]
	add	r0, r2, #3
	ldrb	ip, [r1, r0]	@ zero_extendqisi2
	add	r2, r2, #4
	ldrb	ip, [r3, ip]	@ zero_extendqisi2
	cmp	r5, r2
	strb	ip, [r1, r0]
	bhi	.L1513
	b	.L1514
.L1559:
	cmp	r5, #0
	beq	.L1503
	ldrb	r6, [r1, #1]	@ zero_extendqisi2
	ldrb	ip, [r1, #0]	@ zero_extendqisi2
	mov	r3, r6, lsr #3
	mov	r4, ip, lsr #3
	mov	r6, r3, asl #5
	ldrb	ip, [r1, #2]	@ zero_extendqisi2
	orr	r3, r6, r4, asl #10
	orr	r6, r3, ip, lsr #3
	ldrb	r3, [r2, r6]	@ zero_extendqisi2
	sub	r4, r5, #1
	cmp	r5, #1
	strb	r3, [r1, #0]
	and	r4, r4, #3
	add	r3, r1, #3
	mov	ip, #1
	bls	.L1557
	cmp	r4, #0
	beq	.L1504
	cmp	r4, #1
	beq	.L1551
	cmp	r4, #2
	beq	.L1552
	ldrb	ip, [r3, #1]	@ zero_extendqisi2
	ldrb	r7, [r3, #0]	@ zero_extendqisi2
	mov	r6, ip, lsr #3
	mov	r4, r7, lsr #3
	ldrb	ip, [r3, #2]	@ zero_extendqisi2
	mov	r3, r6, asl #5
	orr	r7, r3, r4, asl #10
	orr	r4, r7, ip, lsr #3
	ldrb	r3, [r2, r4]	@ zero_extendqisi2
	mov	ip, #2
	strb	r3, [r1, #1]
	add	r3, r1, #6
.L1552:
	ldrb	r4, [r3, #1]	@ zero_extendqisi2
	ldrb	r7, [r3, #0]	@ zero_extendqisi2
	mov	r6, r4, lsr #3
	mov	r7, r7, lsr #3
	mov	r4, r6, asl #5
	ldrb	r6, [r3, #2]	@ zero_extendqisi2
	orr	r4, r4, r7, asl #10
	orr	r7, r4, r6, lsr #3
	ldrb	r4, [r2, r7]	@ zero_extendqisi2
	add	r3, r3, #3
	strb	r4, [r1, ip]
	add	ip, ip, #1
.L1551:
	ldrb	r4, [r3, #1]	@ zero_extendqisi2
	ldrb	r7, [r3, #0]	@ zero_extendqisi2
	mov	r6, r4, lsr #3
	mov	r4, r6, asl #5
	mov	r7, r7, lsr #3
	ldrb	r6, [r3, #2]	@ zero_extendqisi2
	orr	r4, r4, r7, asl #10
	orr	r4, r4, r6, lsr #3
	ldrb	r4, [r2, r4]	@ zero_extendqisi2
	strb	r4, [r1, ip]
	add	ip, ip, #1
	cmp	r5, ip
	add	r3, r3, #3
	bls	.L1557
.L1504:
	ldrb	r6, [r3, #1]	@ zero_extendqisi2
	ldrb	sl, [r3, #0]	@ zero_extendqisi2
	mov	r8, r6, lsr #3
	mov	r4, r8, asl #5
	ldrb	r6, [r3, #2]	@ zero_extendqisi2
	mov	r7, sl, lsr #3
	orr	sl, r4, r7, asl #10
	orr	r8, sl, r6, lsr #3
	ldrb	r7, [r2, r8]	@ zero_extendqisi2
	strb	r7, [r1, ip]
	add	r4, r3, #3
	ldrb	r6, [r4, #1]	@ zero_extendqisi2
	ldrb	sl, [r3, #3]	@ zero_extendqisi2
	mov	r8, r6, lsr #3
	mov	r7, sl, lsr #3
	ldrb	r6, [r4, #2]	@ zero_extendqisi2
	mov	sl, r8, asl #5
	orr	r8, sl, r7, asl #10
	orr	r4, r8, r6, lsr #3
	ldrb	r7, [r2, r4]	@ zero_extendqisi2
	add	r4, ip, #1
	strb	r7, [r1, r4]
	add	r6, r3, #6
	ldrb	sl, [r6, #1]	@ zero_extendqisi2
	ldrb	r8, [r3, #6]	@ zero_extendqisi2
	mov	sl, sl, lsr #3
	ldrb	r7, [r6, #2]	@ zero_extendqisi2
	mov	r8, r8, lsr #3
	mov	r6, sl, asl #5
	orr	r6, r6, r8, asl #10
	orr	r8, r6, r7, lsr #3
	ldrb	r6, [r2, r8]	@ zero_extendqisi2
	add	r7, r4, #1
	strb	r6, [r1, r7]
	add	r4, r3, #9
	ldrb	r8, [r4, #1]	@ zero_extendqisi2
	ldrb	r7, [r3, #9]	@ zero_extendqisi2
	mov	r8, r8, lsr #3
	ldrb	r6, [r4, #2]	@ zero_extendqisi2
	mov	r7, r7, lsr #3
	mov	r4, r8, asl #5
	orr	r4, r4, r7, asl #10
	orr	r4, r4, r6, lsr #3
	ldrb	r6, [r2, r4]	@ zero_extendqisi2
	add	r4, ip, #3
	add	ip, ip, #4
	cmp	r5, ip
	strb	r6, [r1, r4]
	add	r3, r3, #12
	bhi	.L1504
.L1557:
	ldrb	r6, [r0, #9]	@ zero_extendqisi2
.L1503:
	cmp	r6, #7
	mulls	r5, r6, r5
	strb	r6, [r0, #11]
	movhi	r6, r6, lsr #3
	mulhi	r5, r6, r5
	addls	r5, r5, #7
	mov	r2, #3
	mov	r1, #1
	movls	r5, r5, lsr #3
	strb	r2, [r0, #8]
	strb	r1, [r0, #10]
	str	r5, [r0, #4]
	b	.L1514
.L1560:
	cmp	r5, #0
	beq	.L1509
	ldrb	r3, [r1, #1]	@ zero_extendqisi2
	ldrb	r4, [r1, #0]	@ zero_extendqisi2
	mov	ip, r3, lsr #3
	mov	r4, r4, lsr #3
	mov	r3, ip, asl #5
	ldrb	ip, [r1, #2]	@ zero_extendqisi2
	orr	r3, r3, r4, asl #10
	orr	r4, r3, ip, lsr #3
	ldrb	r3, [r2, r4]	@ zero_extendqisi2
	sub	ip, r5, #1
	cmp	r5, #1
	strb	r3, [r1, #0]
	and	r4, ip, #3
	add	r3, r1, #4
	mov	ip, #1
	bls	.L1558
	cmp	r4, #0
	beq	.L1510
	cmp	r4, #1
	beq	.L1553
	cmp	r4, #2
	beq	.L1554
	ldrb	ip, [r3, #1]	@ zero_extendqisi2
	ldrb	r4, [r3, #0]	@ zero_extendqisi2
	mov	r7, ip, lsr #3
	mov	r6, r4, lsr #3
	mov	ip, r7, asl #5
	ldrb	r4, [r3, #2]	@ zero_extendqisi2
	orr	r7, ip, r6, asl #10
	orr	r6, r7, r4, lsr #3
	ldrb	ip, [r2, r6]	@ zero_extendqisi2
	add	r3, r3, #4
	strb	ip, [r1, #1]
	mov	ip, #2
.L1554:
	ldrb	r4, [r3, #1]	@ zero_extendqisi2
	ldrb	r7, [r3, #0]	@ zero_extendqisi2
	mov	r6, r4, lsr #3
	mov	r7, r7, lsr #3
	mov	r4, r6, asl #5
	ldrb	r6, [r3, #2]	@ zero_extendqisi2
	orr	r4, r4, r7, asl #10
	orr	r7, r4, r6, lsr #3
	ldrb	r6, [r2, r7]	@ zero_extendqisi2
	add	r3, r3, #4
	strb	r6, [r1, ip]
	add	ip, ip, #1
.L1553:
	ldrb	r4, [r3, #1]	@ zero_extendqisi2
	ldrb	r7, [r3, #0]	@ zero_extendqisi2
	mov	r6, r4, lsr #3
	mov	r4, r6, asl #5
	mov	r7, r7, lsr #3
	ldrb	r6, [r3, #2]	@ zero_extendqisi2
	orr	r4, r4, r7, asl #10
	orr	r4, r4, r6, lsr #3
	ldrb	r4, [r2, r4]	@ zero_extendqisi2
	strb	r4, [r1, ip]
	add	ip, ip, #1
	cmp	r5, ip
	add	r3, r3, #4
	bls	.L1558
.L1510:
	ldrb	r6, [r3, #1]	@ zero_extendqisi2
	ldrb	sl, [r3, #0]	@ zero_extendqisi2
	mov	r8, r6, lsr #3
	mov	r4, r8, asl #5
	ldrb	r6, [r3, #2]	@ zero_extendqisi2
	mov	r7, sl, lsr #3
	orr	sl, r4, r7, asl #10
	orr	r8, sl, r6, lsr #3
	ldrb	r4, [r2, r8]	@ zero_extendqisi2
	strb	r4, [r1, ip]
	add	r4, r3, #4
	ldrb	r6, [r4, #1]	@ zero_extendqisi2
	ldrb	r7, [r3, #4]	@ zero_extendqisi2
	mov	sl, r6, lsr #3
	mov	r8, r7, lsr #3
	mov	r6, sl, asl #5
	ldrb	r7, [r4, #2]	@ zero_extendqisi2
	orr	sl, r6, r8, asl #10
	orr	r6, sl, r7, lsr #3
	ldrb	sl, [r2, r6]	@ zero_extendqisi2
	add	r6, ip, #1
	strb	sl, [r1, r6]
	add	r7, r4, #4
	ldrb	sl, [r7, #1]	@ zero_extendqisi2
	ldrb	r8, [r4, #4]	@ zero_extendqisi2
	mov	r4, sl, lsr #3
	ldrb	r7, [r7, #2]	@ zero_extendqisi2
	mov	r8, r8, lsr #3
	mov	r4, r4, asl #5
	orr	r4, r4, r8, asl #10
	orr	r4, r4, r7, lsr #3
	ldrb	r7, [r2, r4]	@ zero_extendqisi2
	add	r6, r6, #1
	strb	r7, [r1, r6]
	add	r4, r3, #12
	ldrb	r8, [r4, #1]	@ zero_extendqisi2
	ldrb	r7, [r3, #12]	@ zero_extendqisi2
	mov	r8, r8, lsr #3
	ldrb	r6, [r4, #2]	@ zero_extendqisi2
	mov	r7, r7, lsr #3
	mov	r4, r8, asl #5
	orr	r4, r4, r7, asl #10
	orr	r4, r4, r6, lsr #3
	ldrb	r6, [r2, r4]	@ zero_extendqisi2
	add	r4, ip, #3
	add	ip, ip, #4
	cmp	r5, ip
	strb	r6, [r1, r4]
	add	r3, r3, #16
	bhi	.L1510
.L1558:
	ldrb	r4, [r0, #9]	@ zero_extendqisi2
.L1509:
	cmp	r4, #7
	mulls	r5, r4, r5
	strb	r4, [r0, #11]
	movhi	r4, r4, lsr #3
	mulhi	r5, r4, r5
	addls	r5, r5, #7
	mov	r2, #3
	mov	r1, #1
	movls	r5, r5, lsr #3
	strb	r2, [r0, #8]
	strb	r1, [r0, #10]
	str	r5, [r0, #4]
	b	.L1514
	.size	png_do_dither, .-png_do_dither
	.section	.text.png_do_read_intrapixel,"ax",%progbits
	.align	2
	.global	png_do_read_intrapixel
	.hidden	png_do_read_intrapixel
	.type	png_do_read_intrapixel, %function
png_do_read_intrapixel:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl}
	ldrb	r3, [r0, #8]	@ zero_extendqisi2
	tst	r3, #2
	beq	.L1571
	ldrb	r2, [r0, #9]	@ zero_extendqisi2
	cmp	r2, #8
	ldr	ip, [r0, #0]
	beq	.L1592
	cmp	r2, #16
	beq	.L1593
.L1571:
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl}
	bx	lr
.L1592:
	cmp	r3, #2
	moveq	r2, #3
	beq	.L1566
	cmp	r3, #6
	bne	.L1571
	mov	r2, #4
.L1566:
	cmp	ip, #0
	beq	.L1571
	mov	r0, r1
	ldrb	r3, [r1, #0]	@ zero_extendqisi2
	ldrb	r4, [r0, #1]!	@ zero_extendqisi2
	add	r4, r4, r3
	mov	r3, r1
	strb	r4, [r3], r2
	ldrb	r4, [r0, #0]	@ zero_extendqisi2
	ldrb	r0, [r1, #2]!	@ zero_extendqisi2
	cmp	ip, #1
	add	r4, r4, r0
	sub	r0, ip, #1
	strb	r4, [r1, #0]
	and	r1, r0, #3
	mov	r4, #1
	bls	.L1571
	cmp	r1, #0
	beq	.L1567
	cmp	r1, #1
	beq	.L1590
	cmp	r1, #2
	beq	.L1591
	mov	r5, r3
	ldrb	r4, [r5, #1]!	@ zero_extendqisi2
	ldrb	r0, [r3, #0]	@ zero_extendqisi2
	add	r1, r4, r0
	strb	r1, [r3, #0]
	mov	r1, r3
	ldrb	r4, [r5, #0]	@ zero_extendqisi2
	ldrb	r0, [r1, #2]!	@ zero_extendqisi2
	add	r5, r4, r0
	strb	r5, [r1, #0]
	add	r3, r3, r2
	mov	r4, #2
.L1591:
	mov	r0, r3
	ldrb	r5, [r0, #1]!	@ zero_extendqisi2
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	add	r1, r5, r1
	strb	r1, [r3, #0]
	mov	r1, r3
	ldrb	r5, [r0, #0]	@ zero_extendqisi2
	ldrb	r0, [r1, #2]!	@ zero_extendqisi2
	add	r0, r5, r0
	strb	r0, [r1, #0]
	add	r4, r4, #1
	add	r3, r3, r2
.L1590:
	mov	r0, r3
	ldrb	r5, [r0, #1]!	@ zero_extendqisi2
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	add	r1, r5, r1
	strb	r1, [r3, #0]
	mov	r1, r3
	ldrb	r5, [r0, #0]	@ zero_extendqisi2
	ldrb	r0, [r1, #2]!	@ zero_extendqisi2
	add	r4, r4, #1
	add	r0, r5, r0
	cmp	ip, r4
	strb	r0, [r1, #0]
	add	r3, r3, r2
	bls	.L1571
.L1567:
	ldrb	r1, [r3, #2]	@ zero_extendqisi2
	ldrb	r6, [r3, #1]	@ zero_extendqisi2
	ldrb	r0, [r3, #0]	@ zero_extendqisi2
	add	r1, r6, r1
	add	r0, r6, r0
	strb	r1, [r3, #2]
	strb	r0, [r3, #0]
	add	r1, r3, r2
	ldrb	r0, [r3, r2]	@ zero_extendqisi2
	ldrb	r5, [r1, #1]	@ zero_extendqisi2
	add	r6, r5, r0
	strb	r6, [r3, r2]
	ldrb	r0, [r1, #2]	@ zero_extendqisi2
	ldrb	r5, [r1, #1]	@ zero_extendqisi2
	add	r6, r5, r0
	strb	r6, [r1, #2]
	add	r0, r1, r2
	ldrb	r5, [r3, r2, asl #1]	@ zero_extendqisi2
	ldrb	r6, [r0, #1]	@ zero_extendqisi2
	add	r5, r6, r5
	strb	r5, [r3, r2, asl #1]
	ldrb	r3, [r0, #2]	@ zero_extendqisi2
	ldrb	r5, [r0, #1]	@ zero_extendqisi2
	add	r3, r5, r3
	strb	r3, [r0, #2]
	add	r3, r0, r2
	ldrb	r5, [r3, #1]	@ zero_extendqisi2
	ldrb	r0, [r1, r2, asl #1]	@ zero_extendqisi2
	add	r0, r5, r0
	strb	r0, [r1, r2, asl #1]
	ldrb	r1, [r3, #2]	@ zero_extendqisi2
	ldrb	r0, [r3, #1]	@ zero_extendqisi2
	add	r4, r4, #4
	add	r1, r0, r1
	cmp	ip, r4
	strb	r1, [r3, #2]
	add	r3, r3, r2
	bhi	.L1567
	b	.L1571
.L1593:
	cmp	r3, #2
	moveq	r4, #6
	beq	.L1569
	cmp	r3, #6
	bne	.L1571
	mov	r4, #8
.L1569:
	cmp	ip, #0
	beq	.L1571
	mov	r5, r1
	mov	r6, r1
	mov	r0, r1
	ldrb	r8, [r5, #4]!	@ zero_extendqisi2
	ldrb	sl, [r6, #1]!	@ zero_extendqisi2
	ldrb	r3, [r0, #5]!	@ zero_extendqisi2
	ldrb	r9, [r1, #2]	@ zero_extendqisi2
	ldrb	r7, [r1, #3]	@ zero_extendqisi2
	ldrb	r2, [r1, #0]	@ zero_extendqisi2
	orr	r7, r7, r9, asl #8
	orr	r2, sl, r2, asl #8
	orr	sl, r3, r8, asl #8
	add	r3, r7, sl
	add	r8, r2, #65536
	add	r2, r8, r7
	add	r7, r3, #65536
	mov	r8, r7, asl #16
	mov	sl, r2, asl #16
	mov	r2, sl, lsr #16
	mov	r3, r8, lsr #16
	sub	r7, ip, #1
	mov	sl, r2, lsr #8
	mov	r8, r3, lsr #8
	cmp	ip, #1
	strb	sl, [r1], r4
	and	r7, r7, #1
	strb	r2, [r6, #0]
	strb	r8, [r5, #0]
	strb	r3, [r0, #0]
	mov	r5, #1
	bls	.L1571
	cmp	r7, #0
	beq	.L1570
	mov	r7, r1
	mov	r6, r1
	mov	r0, r1
	ldrb	sl, [r7, #1]!	@ zero_extendqisi2
	ldrb	r8, [r6, #4]!	@ zero_extendqisi2
	ldrb	r9, [r1, #2]	@ zero_extendqisi2
	ldrb	r5, [r1, #3]	@ zero_extendqisi2
	ldrb	r2, [r1, #0]	@ zero_extendqisi2
	ldrb	r3, [r0, #5]!	@ zero_extendqisi2
	orr	r5, r5, r9, asl #8
	orr	r2, sl, r2, asl #8
	orr	r3, r3, r8, asl #8
	add	r3, r5, r3
	add	sl, r2, #65536
	add	r2, sl, r5
	add	r5, r3, #65536
	mov	sl, r5, asl #16
	mov	r8, r2, asl #16
	mov	r2, r8, lsr #16
	mov	r3, sl, lsr #16
	mov	r5, #2
	mov	sl, r2, lsr #8
	mov	r8, r3, lsr #8
	cmp	ip, r5
	strb	sl, [r1], r4
	strb	r2, [r7, #0]
	strb	r8, [r6, #0]
	strb	r3, [r0, #0]
	bls	.L1571
.L1570:
	ldrb	r0, [r1, #1]	@ zero_extendqisi2
	ldrb	r8, [r1, #0]	@ zero_extendqisi2
	ldrb	r6, [r1, #4]	@ zero_extendqisi2
	ldrb	r3, [r1, #5]	@ zero_extendqisi2
	ldrb	sl, [r1, #2]	@ zero_extendqisi2
	ldrb	r7, [r1, #3]	@ zero_extendqisi2
	orr	r2, r0, r8, asl #8
	orr	r0, r7, sl, asl #8
	orr	r7, r3, r6, asl #8
	add	sl, r0, r7
	add	r2, r2, #65536
	add	r8, r2, r0
	add	r6, sl, #65536
	mov	r0, r8, asl #16
	mov	r3, r6, asl #16
	mov	r2, r0, lsr #16
	mov	r8, r3, lsr #16
	mov	r7, r2, lsr #8
	mov	sl, r8, lsr #8
	strb	r7, [r1, #0]
	strb	r2, [r1, #1]
	strb	sl, [r1, #4]
	strb	r8, [r1, #5]
	add	r3, r1, r4
	ldrb	sl, [r1, r4]	@ zero_extendqisi2
	ldrb	r7, [r3, #4]	@ zero_extendqisi2
	ldrb	r6, [r3, #1]	@ zero_extendqisi2
	ldrb	r2, [r3, #5]	@ zero_extendqisi2
	ldrb	r8, [r3, #2]	@ zero_extendqisi2
	ldrb	r0, [r3, #3]	@ zero_extendqisi2
	orr	r2, r2, r7, asl #8
	orr	r0, r0, r8, asl #8
	orr	r6, r6, sl, asl #8
	add	r7, r0, r2
	add	r6, r6, #65536
	add	r0, r6, r0
	add	r2, r7, #65536
	mov	r6, r2, asl #16
	mov	r7, r0, asl #16
	mov	r0, r7, lsr #16
	mov	r2, r6, lsr #16
	add	r5, r5, #2
	mov	r7, r0, lsr #8
	mov	r6, r2, lsr #8
	cmp	ip, r5
	strb	r7, [r1, r4]
	strb	r0, [r3, #1]
	strb	r6, [r3, #4]
	strb	r2, [r3, #5]
	add	r1, r3, r4
	bhi	.L1570
	b	.L1571
	.size	png_do_read_intrapixel, .-png_do_read_intrapixel
	.global	__aeabi_f2d
	.global	__aeabi_fmul
	.global	__aeabi_ddiv
	.global	__aeabi_i2d
	.global	__aeabi_dadd
	.global	__aeabi_d2uiz
	.global	__aeabi_ui2d
	.section	.text.png_build_gamma_table,"ax",%progbits
	.align	2
	.global	png_build_gamma_table
	.hidden	png_build_gamma_table
	.type	png_build_gamma_table, %function
png_build_gamma_table:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldrb	r3, [r0, #323]	@ zero_extendqisi2
	cmp	r3, #8
	sub	sp, sp, #52
	mov	r4, r0
	bhi	.L1595
	ldr	r5, [r0, #380]	@ float
	mov	r0, r5
	bl	__aeabi_f2d
	adr	r3, .L1733
	ldmia	r3, {r2-r3}
	bl	__aeabi_dcmpgt
	cmp	r0, #0
	bne	.L1596
	mov	r1, #1069547520
	mov	r0, #0
	add	r1, r1, #3145728
	stmia	sp, {r0-r1}
.L1597:
	mov	r0, r4
	mov	r1, #256
	bl	png_malloc
	mov	r5, #0
	mov	r7, r0
	str	r0, [r4, #384]
	b	.L1599
.L1727:
	bl	__aeabi_i2d
	mov	ip, #1073741824
	add	r7, ip, #7274496
	add	r3, r7, #57344
	mov	r2, #0
	bl	__aeabi_ddiv
	ldmia	sp, {r2-r3}
	ldr	r7, [r4, #384]
	bl	pow
	mov	r3, #1073741824
	add	ip, r3, #7274496
	add	r3, ip, #57344
	mov	r2, #0
	bl	__aeabi_dmul
	mov	r3, #1069547520
	mov	r2, #0
	add	r3, r3, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	strb	r0, [r7, r6]
	mov	r0, r8
	bl	__aeabi_i2d
	mov	ip, #1073741824
	add	r6, ip, #7274496
	add	r3, r6, #57344
	mov	r2, #0
	bl	__aeabi_ddiv
	mov	r7, #1073741824
	ldmia	sp, {r2-r3}
	ldr	r6, [r4, #384]
	bl	pow
	add	r3, r7, #7274496
	mov	r2, #0
	add	r3, r3, #57344
	bl	__aeabi_dmul
	mov	ip, #1069547520
	add	r3, ip, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	mov	r7, #1073741824
	strb	r0, [r6, r8]
	mov	r0, sl
	bl	__aeabi_i2d
	add	r3, r7, #7274496
	mov	r2, #0
	add	r3, r3, #57344
	bl	__aeabi_ddiv
	ldmia	sp, {r2-r3}
	ldr	r7, [r4, #384]
	bl	pow
	mov	ip, #1073741824
	add	r8, ip, #7274496
	add	r3, r8, #57344
	mov	r2, #0
	mov	r6, #1069547520
	bl	__aeabi_dmul
	add	r3, r6, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	strb	r0, [r7, sl]
	mov	r0, r9
	bl	__aeabi_i2d
	mov	r3, #1073741824
	add	ip, r3, #7274496
	add	r3, ip, #57344
	mov	r2, #0
	mov	r8, #1073741824
	bl	__aeabi_ddiv
	add	r6, r8, #7274496
	ldmia	sp, {r2-r3}
	ldr	r7, [r4, #384]
	mov	sl, #1069547520
	bl	pow
	mov	r2, #0
	add	r3, r6, #57344
	bl	__aeabi_dmul
	mov	r2, #0
	add	r3, sl, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	strb	r0, [r7, r9]
	ldr	r7, [r4, #384]
	add	r5, r5, #5
.L1599:
	mov	r0, r5
	bl	__aeabi_i2d
	mov	r3, #1073741824
	add	ip, r3, #7274496
	add	r3, ip, #57344
	mov	r2, #0
	mov	r9, #1073741824
	bl	__aeabi_ddiv
	add	r8, r9, #7274496
	ldmia	sp, {r2-r3}
	bl	pow
	mov	r6, #1069547520
	add	r3, r8, #57344
	mov	r2, #0
	bl	__aeabi_dmul
	add	r3, r6, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	add	r6, r5, #1
	cmp	r6, #256
	strb	r0, [r7, r5]
	add	sl, r5, #3
	add	r9, r5, #4
	add	r8, r6, #1
	mov	r0, r6
	bne	.L1727
	ldr	r3, [r4, #140]
	bic	r1, r3, #-16777216
	bic	lr, r1, #10420224
	bic	r2, lr, #65280
	bic	r0, r2, #127
	cmp	r0, #0
	bne	.L1728
.L1637:
	add	sp, sp, #52
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L1734:
	.align	3
.L1733:
	.word	-1598689907
	.word	1051772663
.L1595:
	ldrb	r3, [r0, #322]	@ zero_extendqisi2
	tst	r3, #2
	ldreqb	r3, [r0, #411]	@ zero_extendqisi2
	beq	.L1608
	ldrb	fp, [r0, #408]	@ zero_extendqisi2
	ldrb	r0, [r0, #409]	@ zero_extendqisi2
	ldrb	r2, [r4, #410]	@ zero_extendqisi2
	cmp	r0, fp
	movge	r3, r0
	movlt	r3, fp
	cmp	r2, r3
	movgt	r3, r2
	ble	.L1608
.L1609:
	rsb	r3, r3, #16
.L1726:
	str	r3, [sp, #40]
	ldr	r1, [r4, #140]
	tst	r1, #1024
	beq	.L1611
	ldr	r5, [sp, #40]
	cmp	r5, #4
	movle	r2, #5
	strle	r2, [sp, #40]
	bgt	.L1611
.L1612:
	ldr	r9, [sp, #40]
	mov	r3, #1
	rsb	r6, r9, #8
	mov	sl, r3, asl r6
	str	sl, [sp, #36]
	and	r3, r9, #255
	ldr	r9, [sp, #36]
	mov	sl, r9, asl #2
	str	sl, [sp, #44]
.L1614:
	ldr	r5, [r4, #380]	@ float
	str	r3, [r4, #372]
	mov	r0, r5
	bl	__aeabi_f2d
	adr	r3, .L1735
	ldmia	r3, {r2-r3}
	bl	__aeabi_dcmpgt
	cmp	r0, #0
	bne	.L1615
	mov	fp, #1069547520
	mov	r0, #0
	add	r1, fp, #3145728
	stmia	sp, {r0-r1}
.L1616:
	mov	r0, r4
	ldr	r1, [sp, #44]
	bl	png_malloc
	ldr	r3, [r4, #140]
	ands	fp, r3, #1152
	mov	r5, r0
	str	r0, [r4, #396]
	bne	.L1617
	ldr	r0, [sp, #36]
	cmp	r0, #0
	ble	.L1619
	ldr	r3, .L1735+8
	ldr	ip, [sp, #40]
	add	r7, r3, ip, asl #2
	ldr	r6, [r7, #64]
	str	r6, [sp, #12]
	mov	r9, fp
.L1628:
	mov	r1, #512
	mov	r0, r4
	bl	png_malloc
	str	r0, [r5, r9, asl #2]
	mov	r7, fp, lsr #4
	ldr	r2, [r4, #396]
	mvn	r5, #-1090519040
	mov	r0, r7
	ldr	r8, [r2, r9, asl #2]
	bl	__aeabi_ui2d
	sub	ip, r5, #1048576
	sub	r3, ip, #31
	mov	r2, #0
	bl	__aeabi_ddiv
	ldmia	sp, {r2-r3}
	bl	pow
	mvn	r3, #-1090519040
	sub	r6, r3, #1048576
	sub	r3, r6, #31
	mov	r2, #0
	mov	sl, #1069547520
	bl	__aeabi_dmul
	add	r3, sl, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	strh	r0, [r8, #0]	@ movhi
	add	r0, r7, #256
	bl	__aeabi_ui2d
	mvn	ip, #-1090519040
	sub	r3, ip, #1048576
	ldr	r5, [r4, #396]
	mov	r2, #0
	sub	r3, r3, #31
	mvn	r6, #-1090519040
	bl	__aeabi_ddiv
	sub	sl, r6, #1048576
	ldmia	sp, {r2-r3}
	ldr	r8, [r5, r9, asl #2]
	bl	pow
	mov	r5, #1069547520
	sub	r3, sl, #31
	mov	r2, #0
	bl	__aeabi_dmul
	add	r3, r5, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	mov	r5, #2
	mov	sl, r9, asl r5
	strh	r0, [r5, r8]	@ movhi
.L1626:
	ldr	lr, [r4, #396]
	add	r0, r7, r5, asl #8
	ldr	r6, [lr, sl]
	bl	__aeabi_ui2d
	mvn	ip, #-1090519040
	sub	r3, ip, #1048576
	mov	r2, #0
	sub	r3, r3, #31
	bl	__aeabi_ddiv
	mvn	r8, #-1090519040
	ldmia	sp, {r2-r3}
	bl	pow
	sub	ip, r8, #1048576
	sub	r3, ip, #31
	mov	r2, #0
	bl	__aeabi_dmul
	mov	r3, #1069547520
	mov	r2, #0
	add	r3, r3, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	mov	r8, r5, asl #1
	strh	r0, [r8, r6]	@ movhi
	add	r6, r5, #1
	add	r0, r7, r6, asl #8
	bl	__aeabi_ui2d
	mvn	ip, #-1090519040
	sub	r3, ip, #1048576
	ldr	r5, [r4, #396]
	mov	r2, #0
	sub	r3, r3, #31
	bl	__aeabi_ddiv
	ldr	r8, [r5, sl]
	ldmia	sp, {r2-r3}
	mvn	r5, #-1090519040
	bl	pow
	sub	ip, r5, #1048576
	sub	r3, ip, #31
	mov	r2, #0
	bl	__aeabi_dmul
	mov	r3, #1069547520
	mov	r2, #0
	add	r3, r3, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	add	r5, r6, #1
	cmp	r5, #256
	mov	r6, r6, asl #1
	strh	r0, [r6, r8]	@ movhi
	bne	.L1626
	ldr	r1, [sp, #36]
	add	r9, r9, #1
	ldr	r0, [sp, #12]
	cmp	r9, r1
	add	fp, fp, r0
	bge	.L1627
	ldr	r5, [r4, #396]
	b	.L1628
.L1608:
	cmp	r3, #0
	bne	.L1609
	b	.L1726
.L1596:
	ldr	r1, [r4, #376]	@ float
	mov	r0, r5
	bl	__aeabi_fmul
	bl	__aeabi_f2d
	mov	r5, #1069547520
	mov	r2, r0
	mov	r3, r1
	mov	r0, #0
	add	r1, r5, #3145728
	bl	__aeabi_ddiv
	stmia	sp, {r0-r1}
	b	.L1597
.L1611:
	ldr	r2, [sp, #40]
	cmp	r2, #8
	movgt	sl, #8
	strgt	sl, [sp, #40]
	bgt	.L1612
	ldr	lr, [sp, #40]
	cmp	lr, #0
	bge	.L1612
	mov	ip, #0
	mov	r6, #1024
	mov	r9, #256
	str	r6, [sp, #44]
	str	ip, [sp, #40]
	str	r9, [sp, #36]
	mov	r6, #8
	mov	r3, ip
	b	.L1614
.L1736:
	.align	3
.L1735:
	.word	-1598689907
	.word	1051772663
	.word	.LANCHOR0
.L1729:
	ldr	r5, [r4, #396]
	bl	png_malloc
	str	r0, [r5, r8, asl #2]
	mov	r1, #512
	mov	r0, r4
	ldr	r8, [r4, #396]
	bl	png_malloc
	str	r0, [r8, sl, asl #2]
	mov	r1, #512
	mov	r0, r4
	ldr	r5, [r4, #396]
	bl	png_malloc
	add	r2, r7, #3
	str	r0, [r5, r2, asl #2]
	ldr	r5, [r4, #396]
	add	r7, r7, #4
.L1621:
	mov	r1, #512
	mov	r0, r4
	bl	png_malloc
	add	r8, r7, #1
	cmp	r8, fp
	str	r0, [r5, r7, asl #2]
	mov	r1, #512
	add	sl, r8, #1
	mov	r0, r4
	blt	.L1729
.L1620:
	mov	lr, #1069547520
	ldmia	sp, {r2-r3}
	mov	r0, #0
	add	r1, lr, #3145728
	bl	__aeabi_ddiv
	ldr	ip, [sp, #40]
	mov	r7, #255
	mov	r7, r7, asr ip
	mov	r9, r9, asl #8
	str	r0, [sp, #24]
	str	r1, [sp, #28]
	mov	r5, #0
	str	r9, [sp, #20]
	mov	r8, r5
.L1624:
	mov	r0, r8
	bl	__aeabi_i2d
	mov	r3, #1069547520
	mov	r2, #0
	add	r3, r3, #2097152
	mov	sl, #1061158912
	bl	__aeabi_dadd
	add	r3, sl, #3145728
	mov	r2, #0
	bl	__aeabi_dmul
	add	r3, sp, #24
	ldmia	r3, {r2-r3}
	bl	pow
	mov	sl, r0
	ldr	r0, [sp, #20]
	mov	fp, r1
	bl	__aeabi_ui2d
	mov	r2, r0
	mov	r3, r1
	mov	r0, sl
	mov	r1, fp
	bl	__aeabi_dmul
	bl	__aeabi_d2uiz
	cmp	r5, r0
	bhi	.L1622
	mov	lr, r5, lsr r6
	ldr	r2, [r4, #396]
	mov	r3, r8, asl #24
	orr	ip, r8, r3, lsr #16
	and	r1, r5, r7
	ldr	r1, [r2, r1, asl #2]
	mov	r3, ip, asl #16
	rsb	r2, r5, r0
	add	r5, r5, #1
	mov	r3, r3, lsr #16
	mov	ip, lr, asl #1
	cmp	r0, r5
	strh	r3, [ip, r1]	@ movhi
	and	r2, r2, #3
	bcc	.L1622
	cmp	r2, #0
	beq	.L1724
	cmp	r2, #1
	beq	.L1712
	cmp	r2, #2
	beq	.L1713
	mov	lr, r5, lsr r6
	ldr	r1, [r4, #396]
	and	ip, r5, r7
	ldr	r1, [r1, ip, asl #2]
	mov	r2, lr, asl #1
	strh	r3, [r2, r1]	@ movhi
	add	r5, r5, #1
.L1713:
	mov	r2, r5, lsr r6
	ldr	r1, [r4, #396]
	and	ip, r5, r7
	ldr	lr, [r1, ip, asl #2]
	mov	r2, r2, asl #1
	strh	r3, [r2, lr]	@ movhi
	add	r5, r5, #1
.L1712:
	mov	r2, r5, lsr r6
	ldr	r1, [r4, #396]
	and	ip, r5, r7
	ldr	lr, [r1, ip, asl #2]
	add	r5, r5, #1
	mov	r2, r2, asl #1
	cmp	r0, r5
	strh	r3, [r2, lr]	@ movhi
	bcc	.L1622
.L1724:
	str	r8, [sp, #16]
	str	r0, [sp, #12]
.L1623:
	add	r1, r5, #3
	mov	sl, r1, lsr r6
	mov	r9, r5, lsr r6
	add	r2, r5, #1
	mov	r0, r2, lsr r6
	add	ip, r2, #1
	mov	lr, ip, lsr r6
	str	sl, [sp, #0]
	ldr	fp, [r4, #396]
	and	r8, r5, r7
	ldr	sl, [fp, r8, asl #2]
	mov	r9, r9, asl #1
	strh	r3, [r9, sl]	@ movhi
	ldr	sl, [r4, #396]
	and	fp, r2, r7
	ldr	r2, [sl, fp, asl #2]
	mov	r8, r0, asl #1
	strh	r3, [r8, r2]	@ movhi
	ldr	fp, [r4, #396]
	and	r0, ip, r7
	ldr	r2, [fp, r0, asl #2]
	mov	r8, lr, asl #1
	strh	r3, [r8, r2]	@ movhi
	ldr	fp, [r4, #396]
	ldr	lr, [sp, #0]
	ldr	r0, [sp, #12]
	and	ip, r1, r7
	ldr	r8, [fp, ip, asl #2]
	add	r5, r5, #4
	mov	fp, lr, asl #1
	cmp	r0, r5
	strh	r3, [fp, r8]	@ movhi
	bcs	.L1623
	ldr	r8, [sp, #16]
.L1622:
	add	r8, r8, #1
	cmp	r8, #256
	bne	.L1624
	ldr	r3, [sp, #20]
	cmp	r5, r3
	bcs	.L1627
	mov	ip, r5, lsr r6
	ldr	fp, [r4, #396]
	and	r2, r5, r7
	ldr	r0, [fp, r2, asl #2]
	ldr	sl, [sp, #20]
	add	r3, r5, #1
	add	r1, r0, ip, asl #1
	mvn	lr, r5
	mvn	fp, #0	@ movhi
	cmp	r3, sl
	strh	fp, [r1, #0]	@ movhi
	and	r2, lr, #3
	bcs	.L1627
	cmp	r2, #0
	beq	.L1640
	cmp	r2, #1
	beq	.L1710
	cmp	r2, #2
	beq	.L1711
	mov	ip, r3, lsr r6
	ldr	r2, [r4, #396]
	and	lr, r3, r7
	ldr	sl, [r2, lr, asl #2]
	add	fp, sl, ip, asl #1
	mvn	sl, #0	@ movhi
	strh	sl, [fp, #0]	@ movhi
	add	r3, r3, #1
.L1711:
	mov	r1, r3, lsr r6
	ldr	ip, [r4, #396]
	and	r2, r3, r7
	ldr	fp, [ip, r2, asl #2]
	add	r0, fp, r1, asl #1
	mvn	fp, #0	@ movhi
	strh	fp, [r0, #0]	@ movhi
	add	r3, r3, #1
.L1710:
	mov	r1, r3, lsr r6
	ldr	lr, [r4, #396]
	and	ip, r3, r7
	ldr	r2, [lr, ip, asl #2]
	ldr	r0, [sp, #20]
	add	r3, r3, #1
	add	ip, r2, r1, asl #1
	mvn	lr, #0	@ movhi
	cmp	r3, r0
	strh	lr, [ip, #0]	@ movhi
	bcc	.L1640
.L1627:
	ldr	r3, [r4, #140]
.L1619:
	bic	lr, r3, #-16777216
	bic	ip, lr, #10420224
	bic	r3, ip, #65280
	bic	r2, r3, #127
	cmp	r2, #0
	beq	.L1637
	ldr	r0, [r4, #376]	@ float
	bl	__aeabi_f2d
	mov	sl, #1069547520
	mov	r2, r0
	mov	r3, r1
	mov	r0, #0
	add	r1, sl, #3145728
	bl	__aeabi_ddiv
	stmia	sp, {r0-r1}
	mov	r0, r4
	ldr	r1, [sp, #44]
	bl	png_malloc
	ldr	sl, [sp, #36]
	cmp	sl, #0
	mov	r5, r0
	str	r0, [r4, #404]
	ble	.L1629
	ldr	fp, [sp, #40]
	ldr	r6, .L1735+8
	add	r7, r6, fp, asl #2
	ldr	r9, [r7, #64]
	mov	fp, #0
	str	r9, [sp, #12]
	mov	r9, fp
.L1631:
	mov	r1, #512
	mov	r0, r4
	bl	png_malloc
	str	r0, [r5, r9, asl #2]
	mov	r7, fp, lsr #4
	ldr	r1, [r4, #404]
	mvn	r5, #-1090519040
	sub	r6, r5, #1048576
	mov	r0, r7
	ldr	r8, [r1, r9, asl #2]
	bl	__aeabi_ui2d
	sub	r3, r6, #31
	mov	r2, #0
	bl	__aeabi_ddiv
	mvn	sl, #-1090519040
	ldmia	sp, {r2-r3}
	bl	pow
	sub	ip, sl, #1048576
	sub	r3, ip, #31
	mov	r2, #0
	bl	__aeabi_dmul
	mov	r3, #1069547520
	mov	r2, #0
	add	r3, r3, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	mvn	r6, #-1090519040
	strh	r0, [r8, #0]	@ movhi
	sub	sl, r6, #1048576
	add	r0, r7, #256
	bl	__aeabi_ui2d
	ldr	r5, [r4, #404]
	sub	r3, sl, #31
	mov	r2, #0
	bl	__aeabi_ddiv
	ldmia	sp, {r2-r3}
	ldr	r8, [r5, r9, asl #2]
	bl	pow
	mvn	ip, #-1090519040
	sub	r3, ip, #1048576
	mov	r2, #0
	sub	r3, r3, #31
	mov	r5, #1069547520
	bl	__aeabi_dmul
	add	r3, r5, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	mov	r5, #2
	bl	__aeabi_d2uiz
	mov	sl, r9, asl r5
	strh	r0, [r5, r8]	@ movhi
	mov	r8, r7
.L1630:
	ldr	r2, [r4, #404]
	add	r0, r8, r5, asl #8
	ldr	r6, [r2, sl]
	bl	__aeabi_ui2d
	mvn	ip, #-1090519040
	sub	r3, ip, #1048576
	mov	r2, #0
	sub	r3, r3, #31
	bl	__aeabi_ddiv
	mvn	r7, #-1090519040
	ldmia	sp, {r2-r3}
	bl	pow
	sub	ip, r7, #1048576
	sub	r3, ip, #31
	mov	r2, #0
	bl	__aeabi_dmul
	mov	r3, #1069547520
	mov	r2, #0
	add	r3, r3, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	mov	r7, r5, asl #1
	strh	r0, [r7, r6]	@ movhi
	add	r6, r5, #1
	ldr	ip, [r4, #404]
	mvn	r5, #-1090519040
	add	r0, r8, r6, asl #8
	ldr	r7, [ip, sl]
	bl	__aeabi_ui2d
	sub	r3, r5, #1048576
	mov	r2, #0
	sub	r3, r3, #31
	bl	__aeabi_ddiv
	ldmia	sp, {r2-r3}
	bl	pow
	mvn	ip, #-1090519040
	sub	r5, ip, #1048576
	sub	r3, r5, #31
	mov	r2, #0
	bl	__aeabi_dmul
	mov	r3, #1069547520
	mov	r2, #0
	add	r3, r3, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	add	r5, r6, #1
	mov	ip, r6, asl #1
	cmp	r5, #256
	strh	r0, [ip, r7]	@ movhi
	bne	.L1630
	ldr	lr, [sp, #36]
	add	r9, r9, #1
	ldr	r0, [sp, #12]
	cmp	r9, lr
	add	fp, fp, r0
	ldrlt	r5, [r4, #404]
	blt	.L1631
.L1629:
	ldr	r0, [r4, #380]	@ float
	bl	__aeabi_f2d
	adr	r3, .L1737
	ldmia	r3, {r2-r3}
	mov	r5, r0
	mov	r6, r1
	bl	__aeabi_dcmpgt
	cmp	r0, #0
	bne	.L1730
	ldr	r0, [r4, #376]	@ float
	bl	__aeabi_f2d
	stmia	sp, {r0-r1}
.L1634:
	ldr	r1, [sp, #44]
	mov	r0, r4
	bl	png_malloc
	ldr	r3, [sp, #36]
	cmp	r3, #0
	mov	r5, r0
	str	r0, [r4, #400]
	ble	.L1637
	ldr	r8, .L1737+8
	ldr	sl, [sp, #40]
	add	fp, r8, sl, asl #2
	ldr	r9, [fp, #64]
	mov	fp, #0
	str	r9, [sp, #12]
	mov	r9, fp
.L1636:
	mov	r1, #512
	mov	r0, r4
	bl	png_malloc
	str	r0, [r5, r9, asl #2]
	mov	r7, fp, lsr #4
	ldr	r2, [r4, #400]
	mvn	r5, #-1090519040
	sub	sl, r5, #1048576
	mov	r0, r7
	ldr	r6, [r2, r9, asl #2]
	bl	__aeabi_ui2d
	sub	r3, sl, #31
	mov	r2, #0
	bl	__aeabi_ddiv
	mvn	r8, #-1090519040
	ldmia	sp, {r2-r3}
	bl	pow
	sub	r3, r8, #1048576
	mov	r2, #0
	sub	r3, r3, #31
	bl	__aeabi_dmul
	mov	ip, #1069547520
	add	r3, ip, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	mvn	sl, #-1090519040
	strh	r0, [r6, #0]	@ movhi
	sub	r8, sl, #1048576
	add	r0, r7, #256
	bl	__aeabi_ui2d
	ldr	r5, [r4, #400]
	sub	r3, r8, #31
	mov	r2, #0
	bl	__aeabi_ddiv
	ldmia	sp, {r2-r3}
	ldr	r6, [r5, r9, asl #2]
	bl	pow
	mvn	r3, #-1090519040
	sub	ip, r3, #1048576
	sub	r3, ip, #31
	mov	r2, #0
	mov	r5, #1069547520
	bl	__aeabi_dmul
	add	r3, r5, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	mov	r5, #2
	mov	sl, r9, asl r5
	strh	r0, [r5, r6]	@ movhi
	mov	r8, r7
.L1635:
	ldr	lr, [r4, #400]
	add	r0, r8, r5, asl #8
	ldr	r6, [lr, sl]
	bl	__aeabi_ui2d
	mvn	r3, #-1090519040
	sub	ip, r3, #1048576
	sub	r3, ip, #31
	mov	r2, #0
	bl	__aeabi_ddiv
	mvn	r7, #-1090519040
	ldmia	sp, {r2-r3}
	bl	pow
	sub	r3, r7, #1048576
	mov	r2, #0
	sub	r3, r3, #31
	bl	__aeabi_dmul
	mov	ip, #1069547520
	add	r3, ip, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	mov	r7, r5, asl #1
	strh	r0, [r7, r6]	@ movhi
	add	r6, r5, #1
	add	r0, r8, r6, asl #8
	bl	__aeabi_ui2d
	mvn	r3, #-1090519040
	sub	ip, r3, #1048576
	ldr	r5, [r4, #400]
	sub	r3, ip, #31
	mov	r2, #0
	bl	__aeabi_ddiv
	ldr	r7, [r5, sl]
	ldmia	sp, {r2-r3}
	mvn	r5, #-1090519040
	bl	pow
	sub	r3, r5, #1048576
	mov	r2, #0
	sub	r3, r3, #31
	bl	__aeabi_dmul
	mov	ip, #1069547520
	mov	r2, #0
	add	r3, ip, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	add	r5, r6, #1
	cmp	r5, #256
	mov	r6, r6, asl #1
	strh	r0, [r6, r7]	@ movhi
	bne	.L1635
	ldr	r1, [sp, #36]
	add	r9, r9, #1
	ldr	r0, [sp, #12]
	cmp	r9, r1
	add	fp, fp, r0
	bge	.L1637
	ldr	r5, [r4, #400]
	b	.L1636
.L1617:
	ldr	r2, [sp, #36]
	cmp	r2, #0
	ble	.L1620
	sub	r8, r2, #1
	ands	r8, r8, #3
	mov	r7, #0
	moveq	fp, r2
	beq	.L1621
	mov	r0, r4
	mov	r1, #512
	bl	png_malloc
	str	r0, [r5, r7, asl #2]
	cmp	r8, #1
	mov	r7, #1
	ldr	r5, [r4, #396]
	ldreq	fp, [sp, #36]
	beq	.L1621
	cmp	r8, #2
	beq	.L1714
	mov	r0, r4
	mov	r1, #512
	bl	png_malloc
	str	r0, [r5, r7, asl #2]
	ldr	r5, [r4, #396]
	add	r7, r7, r7
.L1714:
	mov	r0, r4
	mov	r1, #512
	bl	png_malloc
	str	r0, [r5, r7, asl #2]
	ldr	fp, [sp, #36]
	add	r7, r7, #1
	ldr	r5, [r4, #396]
	b	.L1621
.L1640:
	mov	r1, r3, lsr r6
	add	r2, r3, #1
	mov	r8, r2, lsr r6
	add	r0, r2, #1
	mov	r5, r0, lsr r6
	add	lr, r3, #3
	mov	ip, lr, lsr r6
	ldr	fp, [r4, #396]
	and	r9, r3, r7
	ldr	r9, [fp, r9, asl #2]
	mov	sl, r1, asl #1
	mvn	fp, #0	@ movhi
	strh	fp, [sl, r9]	@ movhi
	ldr	sl, [r4, #396]
	and	r1, r2, r7
	ldr	r2, [sl, r1, asl #2]
	mov	r8, r8, asl #1
	strh	fp, [r8, r2]	@ movhi
	ldr	r1, [r4, #396]
	and	r0, r0, r7
	ldr	r2, [r1, r0, asl #2]
	mov	r5, r5, asl #1
	strh	fp, [r5, r2]	@ movhi
	ldr	r2, [r4, #396]
	and	r1, lr, r7
	ldr	r0, [sp, #20]
	ldr	lr, [r2, r1, asl #2]
	add	r3, r3, #4
	mov	r1, ip, asl #1
	cmp	r3, r0
	strh	fp, [r1, lr]	@ movhi
	bcc	.L1640
	b	.L1627
.L1615:
	ldr	r1, [r4, #376]	@ float
	mov	r0, r5
	bl	__aeabi_fmul
	bl	__aeabi_f2d
	mov	r3, r1
	mov	r1, #1069547520
	mov	r2, r0
	add	r1, r1, #3145728
	mov	r0, #0
	bl	__aeabi_ddiv
	stmia	sp, {r0-r1}
	b	.L1616
.L1728:
	ldr	r0, [r4, #376]	@ float
	bl	__aeabi_f2d
	mov	r5, #1069547520
	mov	r2, r0
	mov	r3, r1
	mov	r0, #0
	add	r1, r5, #3145728
	bl	__aeabi_ddiv
	stmia	sp, {r0-r1}
	mov	r0, r4
	mov	r1, r6
	bl	png_malloc
	mov	r5, #0
	mov	r7, r0
	str	r0, [r4, #392]
	b	.L1602
.L1738:
	.align	3
.L1737:
	.word	-1598689907
	.word	1051772663
	.word	.LANCHOR0
.L1731:
	bl	__aeabi_i2d
	mov	ip, #1073741824
	add	r7, ip, #7274496
	add	r3, r7, #57344
	mov	r2, #0
	bl	__aeabi_ddiv
	ldmia	sp, {r2-r3}
	ldr	r7, [r4, #392]
	bl	pow
	mov	r3, #1073741824
	add	ip, r3, #7274496
	add	r3, ip, #57344
	mov	r2, #0
	bl	__aeabi_dmul
	mov	r3, #1069547520
	mov	r2, #0
	add	r3, r3, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	strb	r0, [r7, r6]
	mov	r0, r8
	bl	__aeabi_i2d
	mov	ip, #1073741824
	add	r6, ip, #7274496
	add	r3, r6, #57344
	mov	r2, #0
	bl	__aeabi_ddiv
	mov	r7, #1073741824
	ldmia	sp, {r2-r3}
	ldr	r6, [r4, #392]
	bl	pow
	add	r3, r7, #7274496
	mov	r2, #0
	add	r3, r3, #57344
	bl	__aeabi_dmul
	mov	ip, #1069547520
	add	r3, ip, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	mov	r7, #1073741824
	strb	r0, [r6, r8]
	mov	r0, sl
	bl	__aeabi_i2d
	add	r3, r7, #7274496
	mov	r2, #0
	add	r3, r3, #57344
	bl	__aeabi_ddiv
	ldmia	sp, {r2-r3}
	ldr	r7, [r4, #392]
	bl	pow
	mov	ip, #1073741824
	add	r6, ip, #7274496
	add	r3, r6, #57344
	mov	r2, #0
	mov	r8, #1069547520
	bl	__aeabi_dmul
	add	r3, r8, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	strb	r0, [r7, sl]
	mov	r0, r9
	bl	__aeabi_i2d
	mov	r3, #1073741824
	add	ip, r3, #7274496
	add	r3, ip, #57344
	mov	r2, #0
	mov	r6, #1073741824
	bl	__aeabi_ddiv
	add	r8, r6, #7274496
	ldmia	sp, {r2-r3}
	ldr	sl, [r4, #392]
	mov	r7, #1069547520
	bl	pow
	mov	r2, #0
	add	r3, r8, #57344
	bl	__aeabi_dmul
	add	r3, r7, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	strb	r0, [sl, r9]
	ldr	r7, [r4, #392]
	add	r5, r5, #5
.L1602:
	mov	r0, r5
	bl	__aeabi_i2d
	mov	r3, #1073741824
	add	ip, r3, #7274496
	add	r3, ip, #57344
	mov	r2, #0
	mov	r6, #1073741824
	bl	__aeabi_ddiv
	add	r8, r6, #7274496
	ldmia	sp, {r2-r3}
	bl	pow
	mov	r9, #1069547520
	add	r3, r8, #57344
	mov	r2, #0
	bl	__aeabi_dmul
	add	r3, r9, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	add	r6, r5, #1
	cmp	r6, #256
	strb	r0, [r7, r5]
	add	sl, r5, #3
	add	r9, r5, #4
	add	r8, r6, #1
	mov	r0, r6
	bne	.L1731
	mov	r1, r6
	mov	r0, r4
	bl	png_malloc
	str	r0, [r4, #388]
	mov	r7, r0
	ldr	r0, [r4, #380]	@ float
	bl	__aeabi_f2d
	adr	r3, .L1739
	ldmia	r3, {r2-r3}
	mov	r5, r0
	mov	r6, r1
	bl	__aeabi_dcmpgt
	cmp	r0, #0
	beq	.L1715
	mov	lr, #1069547520
	mov	r2, r5
	mov	r3, r6
	mov	r0, #0
	add	r1, lr, #3145728
	bl	__aeabi_ddiv
	stmia	sp, {r0-r1}
.L1605:
	mov	r5, #0
	b	.L1606
.L1732:
	bl	__aeabi_i2d
	mov	r3, #1073741824
	add	r7, r3, #7274496
	add	r3, r7, #57344
	mov	r2, #0
	bl	__aeabi_ddiv
	ldmia	sp, {r2-r3}
	ldr	r7, [r4, #388]
	bl	pow
	mov	ip, #1073741824
	add	r3, ip, #7274496
	mov	r2, #0
	add	r3, r3, #57344
	bl	__aeabi_dmul
	mov	ip, #1069547520
	add	r3, ip, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	strb	r0, [r7, r6]
	mov	r0, r8
	bl	__aeabi_i2d
	mov	r3, #1073741824
	add	r6, r3, #7274496
	add	r3, r6, #57344
	mov	r2, #0
	bl	__aeabi_ddiv
	mov	r7, #1073741824
	ldmia	sp, {r2-r3}
	ldr	r6, [r4, #388]
	bl	pow
	add	ip, r7, #7274496
	add	r3, ip, #57344
	mov	r2, #0
	bl	__aeabi_dmul
	mov	r3, #1069547520
	mov	r2, #0
	add	r3, r3, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	mov	r7, #1073741824
	strb	r0, [r6, r8]
	mov	r0, sl
	bl	__aeabi_i2d
	add	ip, r7, #7274496
	add	r3, ip, #57344
	mov	r2, #0
	bl	__aeabi_ddiv
	ldmia	sp, {r2-r3}
	ldr	r7, [r4, #388]
	bl	pow
	mov	r3, #1073741824
	add	r6, r3, #7274496
	add	r3, r6, #57344
	mov	r2, #0
	mov	r8, #1069547520
	bl	__aeabi_dmul
	add	r3, r8, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	strb	r0, [r7, sl]
	mov	r0, r9
	bl	__aeabi_i2d
	mov	ip, #1073741824
	add	r3, ip, #7274496
	mov	r2, #0
	add	r3, r3, #57344
	mov	r6, #1073741824
	bl	__aeabi_ddiv
	add	r8, r6, #7274496
	ldmia	sp, {r2-r3}
	ldr	sl, [r4, #388]
	mov	r7, #1069547520
	bl	pow
	mov	r2, #0
	add	r3, r8, #57344
	bl	__aeabi_dmul
	add	r3, r7, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	strb	r0, [sl, r9]
	ldr	r7, [r4, #388]
	add	r5, r5, #5
.L1606:
	mov	r0, r5
	bl	__aeabi_i2d
	mov	ip, #1073741824
	add	r3, ip, #7274496
	mov	r2, #0
	add	r3, r3, #57344
	mov	r6, #1073741824
	bl	__aeabi_ddiv
	add	r8, r6, #7274496
	ldmia	sp, {r2-r3}
	bl	pow
	mov	r9, #1069547520
	add	r3, r8, #57344
	mov	r2, #0
	bl	__aeabi_dmul
	add	r3, r9, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	add	r6, r5, #1
	cmp	r6, #256
	strb	r0, [r7, r5]
	add	sl, r5, #3
	add	r9, r5, #4
	add	r8, r6, #1
	mov	r0, r6
	bne	.L1732
	b	.L1637
.L1715:
	ldr	r0, [r4, #376]	@ float
	bl	__aeabi_f2d
	stmia	sp, {r0-r1}
	b	.L1605
.L1730:
	mov	r1, #1069547520
	mov	r2, r5
	mov	r3, r6
	mov	r0, #0
	add	r1, r1, #3145728
	bl	__aeabi_ddiv
	stmia	sp, {r0-r1}
	b	.L1634
.L1740:
	.align	3
.L1739:
	.word	-1598689907
	.word	1051772663
	.size	png_build_gamma_table, .-png_build_gamma_table
	.global	__aeabi_dcmplt
	.global	__aeabi_fcmpeq
	.section	.text.png_init_read_transformations,"ax",%progbits
	.align	2
	.global	png_init_read_transformations
	.hidden	png_init_read_transformations
	.type	png_init_read_transformations, %function
png_init_read_transformations:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	mov	r4, r0
	ldr	r0, [r0, #140]
	tst	r0, #256
	sub	sp, sp, #28
	ldrb	r5, [r4, #322]	@ zero_extendqisi2
	beq	.L1742
	tst	r5, #2
	beq	.L1918
.L1742:
	ldr	r1, [r4, #140]
	bic	r2, r1, #15936
	bic	r3, r2, #63
	mov	lr, r3, asl #17
	mov	ip, #16384
	mov	r6, lr, lsr #17
	add	r2, ip, #128
	cmp	r6, r2
	beq	.L1919
.L1743:
	and	r1, r1, #4352
	cmp	r1, #4352
	beq	.L1920
.L1744:
	add	r0, r4, #348
	add	r0, r0, #2
	add	r1, r4, #340
	mov	r2, #10
	bl	memcpy
	cmp	r5, #3
	beq	.L1754
.L1915:
	ldr	r6, [r4, #140]
.L1755:
	bic	ip, r6, #-16777216
	bic	r1, ip, #10420224
	bic	r3, r1, #57088
	bic	r2, r3, #255
	cmp	r2, #0
	beq	.L1762
	ldr	r0, [r4, #376]	@ float
	mov	r1, #0
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	beq	.L1921
.L1762:
	cmp	r5, #3
	movne	r5, #0
	moveq	r5, #1
	ands	ip, r5, r6, lsr #7
	bne	.L1922
.L1782:
	ands	r6, r5, r6, lsr #3
	beq	.L1801
	ldrb	lr, [r4, #408]	@ zero_extendqisi2
	ldrb	r1, [r4, #409]	@ zero_extendqisi2
	rsb	r0, lr, #8
	ldrb	r2, [r4, #410]	@ zero_extendqisi2
	rsb	r1, r1, #8
	cmp	r0, #8
	mov	r3, #308
	rsb	r2, r2, #8
	ldrh	r6, [r4, r3]
	movhi	r0, #0
	cmp	r1, #8
	movhi	r1, #0
	cmp	r2, #8
	movhi	r2, #0
	cmp	r6, #0
	beq	.L1801
	ldr	lr, [r4, #304]
	ldrb	r3, [lr, #0]	@ zero_extendqisi2
	mov	r7, r3, asr r0
	strb	r7, [lr, #0]
	ldr	r3, [r4, #304]
	ldrb	ip, [r3, #1]	@ zero_extendqisi2
	mov	r5, ip, asr r1
	strb	r5, [r3, #1]
	ldr	ip, [r4, #304]
	ldrb	r7, [ip, #2]	@ zero_extendqisi2
	mov	r7, r7, asr r2
	add	r6, r6, r6, asl #1
	mov	r3, #3
	sub	lr, r6, #3
	add	r5, lr, lr, asl #1
	cmp	r3, r6
	strb	r7, [ip, #2]
	and	r5, r5, r3
	beq	.L1801
	cmp	r5, #0
	beq	.L1800
	cmp	r5, #1
	beq	.L1874
	cmp	r5, #2
	beq	.L1875
	ldr	r3, [r4, #304]
	ldrb	lr, [r3, #3]!	@ zero_extendqisi2
	mov	r5, lr, asr r0
	strb	r5, [r3, #0]
	ldr	ip, [r4, #304]
	add	r3, ip, #3
	ldrb	lr, [r3, #1]	@ zero_extendqisi2
	mov	r5, lr, asr r1
	strb	r5, [r3, #1]
	ldr	ip, [r4, #304]
	add	r3, ip, #3
	ldrb	lr, [r3, #2]	@ zero_extendqisi2
	mov	r5, lr, asr r2
	strb	r5, [r3, #2]
	mov	r3, #6
.L1875:
	ldr	r5, [r4, #304]
	add	ip, r3, r5
	ldrb	lr, [ip, #0]	@ zero_extendqisi2
	mov	r5, lr, asr r0
	strb	r5, [ip, #0]
	ldr	lr, [r4, #304]
	add	ip, r3, lr
	ldrb	r5, [ip, #1]	@ zero_extendqisi2
	mov	lr, r5, asr r1
	strb	lr, [ip, #1]
	ldr	r5, [r4, #304]
	add	ip, r3, r5
	ldrb	lr, [ip, #2]	@ zero_extendqisi2
	mov	r5, lr, asr r2
	strb	r5, [ip, #2]
	add	r3, r3, #3
.L1874:
	ldr	r5, [r4, #304]
	add	ip, r3, r5
	ldrb	lr, [ip, #0]	@ zero_extendqisi2
	mov	r5, lr, asr r0
	strb	r5, [ip, #0]
	ldr	lr, [r4, #304]
	add	ip, r3, lr
	ldrb	r5, [ip, #1]	@ zero_extendqisi2
	mov	lr, r5, asr r1
	strb	lr, [ip, #1]
	ldr	r5, [r4, #304]
	add	ip, r3, r5
	ldrb	lr, [ip, #2]	@ zero_extendqisi2
	mov	r5, lr, asr r2
	add	r3, r3, #3
	cmp	r3, r6
	strb	r5, [ip, #2]
	beq	.L1801
.L1800:
	ldr	ip, [r4, #304]
	ldrb	r7, [r3, ip]	@ zero_extendqisi2
	mov	lr, r7, asr r0
	strb	lr, [r3, ip]
	ldr	r5, [r4, #304]
	add	ip, r3, r5
	ldrb	r7, [ip, #1]	@ zero_extendqisi2
	mov	lr, r7, asr r1
	strb	lr, [ip, #1]
	ldr	r5, [r4, #304]
	add	ip, r3, r5
	ldrb	r7, [ip, #2]	@ zero_extendqisi2
	mov	lr, r7, asr r2
	strb	lr, [ip, #2]
	ldr	ip, [r4, #304]
	add	lr, r3, #3
	ldrb	r5, [lr, ip]	@ zero_extendqisi2
	mov	r7, r5, asr r0
	strb	r7, [lr, ip]
	ldr	r5, [r4, #304]
	add	ip, lr, r5
	ldrb	r7, [ip, #1]	@ zero_extendqisi2
	mov	r7, r7, asr r1
	strb	r7, [ip, #1]
	ldr	r5, [r4, #304]
	add	ip, lr, r5
	ldrb	r7, [ip, #2]	@ zero_extendqisi2
	mov	lr, r7, asr r2
	strb	lr, [ip, #2]
	ldr	ip, [r4, #304]
	add	lr, r3, #6
	ldrb	r5, [lr, ip]	@ zero_extendqisi2
	mov	r7, r5, asr r0
	strb	r7, [lr, ip]
	ldr	r5, [r4, #304]
	add	ip, lr, r5
	ldrb	r7, [ip, #1]	@ zero_extendqisi2
	mov	r7, r7, asr r1
	strb	r7, [ip, #1]
	ldr	r5, [r4, #304]
	add	ip, lr, r5
	ldrb	r7, [ip, #2]	@ zero_extendqisi2
	mov	lr, r7, asr r2
	strb	lr, [ip, #2]
	ldr	ip, [r4, #304]
	add	lr, r3, #9
	ldrb	r5, [lr, ip]	@ zero_extendqisi2
	mov	r7, r5, asr r0
	strb	r7, [lr, ip]
	ldr	r5, [r4, #304]
	add	ip, lr, r5
	ldrb	r7, [ip, #1]	@ zero_extendqisi2
	mov	r7, r7, asr r1
	strb	r7, [ip, #1]
	ldr	r5, [r4, #304]
	add	ip, lr, r5
	ldrb	r5, [ip, #2]	@ zero_extendqisi2
	mov	r5, r5, asr r2
	add	r3, r3, #12
	cmp	r3, r6
	strb	r5, [ip, #2]
	bne	.L1800
.L1801:
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L1922:
	mov	r8, #308
	add	r5, r8, #2
	ldrh	r8, [r4, r5]
	cmp	r8, #0
	ldr	r2, [r4, #304]
	ldrb	r7, [r4, #342]	@ zero_extendqisi2
	ldrb	r5, [r4, #344]	@ zero_extendqisi2
	ldrb	ip, [r4, #346]	@ zero_extendqisi2
	beq	.L1793
	ldr	r3, [r4, #420]
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	sub	r1, r8, #1
	cmp	r3, #0
	and	r0, r1, #1
	beq	.L1923
	cmp	r3, #255
	beq	.L1841
	ldrb	r1, [r2, #0]	@ zero_extendqisi2
	mul	r1, r3, r1
	rsb	lr, r3, #255
	add	r6, r1, #128
	mla	lr, r7, lr, r6
	mov	r3, lr, asl #16
	mov	r1, r3, lsr #24
	add	r6, r1, r3, lsr #16
	mov	lr, r6, asr #8
	strb	lr, [r2, #0]
	ldr	r3, [r4, #420]
	ldrb	r6, [r2, #1]	@ zero_extendqisi2
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	mul	r6, r1, r6
	rsb	r3, r1, #255
	add	lr, r6, #128
	mla	r3, r5, r3, lr
	mov	r6, r3, asl #16
	mov	r1, r6, lsr #24
	add	lr, r1, r6, lsr #16
	mov	r1, lr, asr #8
	strb	r1, [r2, #1]
	ldr	r3, [r4, #420]
	ldrb	lr, [r2, #2]	@ zero_extendqisi2
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	mul	lr, r6, lr
	rsb	r3, r6, #255
	add	r1, lr, #128
	mla	r3, ip, r3, r1
	mov	lr, r3, asl #16
	mov	r6, lr, lsr #24
	add	r3, r6, lr, lsr #16
	mov	r1, r3, asr #8
	strb	r1, [r2, #2]
.L1841:
	mov	r3, #1
	cmp	r8, r3
	mov	r1, #3
	ble	.L1899
	cmp	r0, #0
	beq	.L1796
	ldr	r0, [r4, #420]
	ldrb	r0, [r0, r3]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L1891
	cmp	r0, #255
	beq	.L1848
	ldrb	r6, [r2, r1]	@ zero_extendqisi2
	mul	r6, r0, r6
	rsb	sl, r0, #255
	add	lr, r6, #128
	mla	sl, r7, sl, lr
	mov	r0, sl, asl #16
	mov	r6, r0, lsr #24
	add	lr, r6, r0, lsr #16
	mov	sl, lr, asr #8
	mov	lr, r2
	strb	sl, [lr, r1]!
	ldr	r0, [r4, #420]
	ldrb	sl, [lr, #1]	@ zero_extendqisi2
	ldrb	r6, [r0, r3]	@ zero_extendqisi2
	mul	sl, r6, sl
	rsb	r6, r6, #255
	add	r0, sl, #128
	mla	r6, r5, r6, r0
	mov	r0, r6, asl #16
	mov	sl, r0, lsr #24
	add	r6, sl, r0, lsr #16
	mov	sl, r6, asr #8
	strb	sl, [lr, #1]
	ldr	r0, [r4, #420]
	ldrb	sl, [lr, #2]	@ zero_extendqisi2
	ldrb	r6, [r0, r3]	@ zero_extendqisi2
	mul	sl, r6, sl
	rsb	r0, r6, #255
	add	sl, sl, #128
	mla	r0, ip, r0, sl
	mov	r6, r0, asl #16
	mov	sl, r6, lsr #24
	add	r0, sl, r6, lsr #16
	mov	r6, r0, asr #8
	strb	r6, [lr, #2]
.L1848:
	add	r3, r3, #1
	cmp	r8, r3
	add	r1, r1, #3
	ble	.L1899
.L1796:
	ldr	lr, [r4, #420]
	ldrb	r0, [lr, r3]	@ zero_extendqisi2
	cmp	r0, #0
	bne	.L1794
	add	r0, r2, r1
	strb	r5, [r0, #1]
	strb	ip, [r0, #2]
	strb	r7, [r2, r1]
.L1795:
	ldr	r6, [r4, #420]
	add	r3, r3, #1
	ldrb	r0, [r6, r3]	@ zero_extendqisi2
	cmp	r0, #0
	add	r1, r1, #3
	beq	.L1891
	cmp	r0, #255
	rsb	sl, r0, #255
	mov	r6, r2
	beq	.L1848
	ldrb	r9, [r2, r1]	@ zero_extendqisi2
	mul	r0, r9, r0
	add	r0, r0, #128
	mla	r0, sl, r7, r0
	mov	lr, r0, asl #16
	mov	sl, lr, lsr #24
	add	r0, sl, lr, lsr #16
	mov	sl, r0, asr #8
	strb	sl, [r6, r1]!
	ldr	lr, [r4, #420]
	ldrb	sl, [r6, #1]	@ zero_extendqisi2
	ldrb	r0, [lr, r3]	@ zero_extendqisi2
	mul	sl, r0, sl
	rsb	r0, r0, #255
	add	lr, sl, #128
	mla	r0, r5, r0, lr
	mov	lr, r0, asl #16
	mov	sl, lr, lsr #24
	add	r0, sl, lr, lsr #16
	mov	sl, r0, asr #8
	strb	sl, [r6, #1]
	ldr	lr, [r4, #420]
	ldrb	sl, [r6, #2]	@ zero_extendqisi2
	ldrb	r0, [lr, r3]	@ zero_extendqisi2
	mul	sl, r0, sl
	rsb	lr, r0, #255
	add	sl, sl, #128
	mla	lr, ip, lr, sl
	mov	r0, lr, asl #16
	mov	sl, r0, lsr #24
	add	lr, sl, r0, lsr #16
	add	r3, r3, #1
	mov	r0, lr, asr #8
	cmp	r8, r3
	strb	r0, [r6, #2]
	add	r1, r1, #3
	bgt	.L1796
.L1899:
	ldr	r6, [r4, #140]
.L1793:
	bic	ip, r6, #128
	orr	r6, ip, #262144
	str	r6, [r4, #140]
	mov	r5, #1
	b	.L1782
.L1918:
	ldr	r1, [r4, #132]
	orr	r3, r1, #2048
	mov	r1, r0
	and	r1, r1, #4352
	cmp	r1, #4352
	str	r3, [r4, #132]
	bne	.L1744
.L1920:
	tst	r5, #2
	beq	.L1924
	cmp	r5, #3
	beq	.L1925
	add	r6, r4, #348
	add	r0, r6, #2
	add	r1, r4, #340
	mov	r2, #10
	bl	memcpy
	b	.L1915
.L1925:
	ldrb	r1, [r4, #340]	@ zero_extendqisi2
	ldr	r6, [r4, #304]
	add	lr, r1, r1, asl #1
	ldrb	ip, [lr, r6]	@ zero_extendqisi2
	mov	r2, #340
	add	r3, r2, #2
	strh	ip, [r4, r3]	@ movhi
	add	r3, lr, r6
	ldrb	r1, [r3, #1]	@ zero_extendqisi2
	mov	r2, #344
	strh	r1, [r4, r2]	@ movhi
	mov	r6, #344
	ldrb	lr, [r3, #2]	@ zero_extendqisi2
	add	ip, r6, #2
	tst	r0, #524288
	strh	lr, [r4, ip]	@ movhi
	beq	.L1752
	tst	r0, #33554432
	bne	.L1752
	mov	r0, #308
	add	lr, r0, #2
	ldrh	r0, [r4, lr]
	cmp	r0, #0
	beq	.L1752
	ldr	r3, [r4, #420]
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	sub	r6, r0, #1
	mvn	ip, r2
	cmp	r0, #1
	strb	ip, [r3, #0]
	and	r2, r6, #3
	mov	r3, #1
	ble	.L1752
	cmp	r2, #0
	beq	.L1753
	cmp	r2, #1
	beq	.L1882
	cmp	r2, #2
	ldrne	r3, [r4, #420]
	ldrneb	r2, [r3, #1]!	@ zero_extendqisi2
	mvnne	r2, r2
	strneb	r2, [r3, #0]
	ldr	ip, [r4, #420]
	movne	r3, #2
	add	r2, r3, ip
	ldrb	r6, [r2, #0]	@ zero_extendqisi2
	mvn	r1, r6
	strb	r1, [r2, #0]
	add	r3, r3, #1
.L1882:
	ldr	r6, [r4, #420]
	add	r2, r3, r6
	ldrb	r1, [r2, #0]	@ zero_extendqisi2
	add	r3, r3, #1
	mvn	lr, r1
	cmp	r0, r3
	strb	lr, [r2, #0]
	ble	.L1752
.L1753:
	ldr	r2, [r4, #420]
	ldrb	r1, [r3, r2]	@ zero_extendqisi2
	mvn	r6, r1
	strb	r6, [r3, r2]
	ldr	r1, [r4, #420]
	add	r2, r3, #1
	ldrb	lr, [r2, r1]	@ zero_extendqisi2
	mvn	ip, lr
	strb	ip, [r2, r1]
	ldr	r1, [r4, #420]
	add	r2, r2, #1
	ldrb	r6, [r2, r1]	@ zero_extendqisi2
	mvn	lr, r6
	strb	lr, [r2, r1]
	ldr	lr, [r4, #420]
	add	r1, r3, #3
	ldrb	ip, [r1, lr]	@ zero_extendqisi2
	add	r3, r3, #4
	mvn	r6, ip
	cmp	r0, r3
	strb	r6, [r1, lr]
	bgt	.L1753
.L1752:
	add	r0, r4, #348
	add	r0, r0, #2
	add	r1, r4, #340
	mov	r2, #10
	bl	memcpy
.L1754:
	mov	ip, #308
	add	r3, ip, #2
	ldrh	r6, [r4, r3]
	cmp	r6, #0
	beq	.L1915
	ldr	r1, [r4, #376]	@ float
	ldr	r0, [r4, #380]	@ float
	bl	__aeabi_fmul
	bl	__aeabi_f2d
	mov	ip, #1069547520
	add	r3, ip, #3145728
	mov	r2, #0
	bl	__aeabi_dsub
	bic	r3, r1, #-2147483648
	mov	r1, r3
	adr	r3, .L1931
	ldmia	r3, {r2-r3}
	bl	__aeabi_dcmplt
	cmp	r0, #0
	beq	.L1915
	cmp	r6, #0
	ble	.L1758
	ldr	r2, [r4, #420]
	ldrb	r3, [r2, #0]	@ zero_extendqisi2
	sub	r0, r3, #1
	and	r1, r0, #255
	mov	r3, #1
	cmp	r1, #253
	movhi	r1, #0
	movls	r1, #1
	sub	lr, r6, #1
	cmp	r3, r6
	and	r0, lr, #3
	bge	.L1884
	cmp	r0, #0
	beq	.L1761
	cmp	r0, #1
	beq	.L1880
	cmp	r0, #2
	beq	.L1881
	ldrb	lr, [r2, #1]	@ zero_extendqisi2
	sub	r3, lr, #1
	and	ip, r3, #255
	cmp	ip, #253
	movls	r1, #1
	mov	r3, #2
.L1881:
	ldrb	lr, [r2, r3]	@ zero_extendqisi2
	sub	ip, lr, #1
	and	r0, ip, #255
	cmp	r0, #253
	movls	r1, #1
	add	r3, r3, #1
.L1880:
	ldrb	lr, [r2, r3]	@ zero_extendqisi2
	sub	ip, lr, #1
	and	r0, ip, #255
	cmp	r0, #253
	add	r3, r3, #1
	movls	r1, #1
	cmp	r3, r6
	bge	.L1884
.L1761:
	add	r0, r3, #1
	ldrb	lr, [r2, r3]	@ zero_extendqisi2
	ldrb	ip, [r2, r0]	@ zero_extendqisi2
	add	r7, r0, #1
	sub	r8, lr, #1
	and	r8, r8, #255
	ldrb	lr, [r2, r7]	@ zero_extendqisi2
	sub	ip, ip, #1
	add	r7, r3, #3
	and	ip, ip, #255
	cmp	r8, #253
	ldrb	r7, [r2, r7]	@ zero_extendqisi2
	movls	r1, #1
	sub	r0, lr, #1
	cmp	ip, #253
	and	lr, r0, #255
	movhi	ip, r1
	sub	r1, r7, #1
	and	r1, r1, #255
	movls	ip, #1
	cmp	lr, #253
	movhi	lr, ip
	movls	lr, #1
	add	r3, r3, #4
	cmp	r1, #253
	movhi	r1, lr
	movls	r1, #1
	cmp	r3, r6
	blt	.L1761
.L1884:
	cmp	r1, #0
	bne	.L1915
.L1758:
	ldr	r6, [r4, #140]
	bic	r6, r6, #8192
	str	r6, [r4, #140]
	b	.L1755
.L1919:
	mov	r3, #340
	add	lr, r3, #2
	mov	ip, #344
	ldrh	r6, [r4, lr]
	ldrh	r3, [r4, ip]
	cmp	r3, r6
	bne	.L1743
	mov	ip, #344
	add	r6, ip, #2
	ldrh	r2, [r4, r6]
	cmp	r2, r3
	ldreq	r3, [r4, #132]
	moveq	ip, #348
	orreq	r3, r3, #2048
	streqh	r2, [r4, ip]	@ movhi
	streq	r3, [r4, #132]
	b	.L1743
.L1891:
	add	r6, r2, r1
	strb	r5, [r6, #1]
	strb	ip, [r6, #2]
	strb	r7, [r2, r1]
	b	.L1848
.L1923:
	strb	ip, [r2, #2]
	strb	r5, [r2, #1]
	strb	r7, [r2, #0]
	b	.L1841
.L1932:
	.align	3
.L1931:
	.word	-1717986918
	.word	1068079513
.L1921:
	mov	r0, r4
	bl	png_build_gamma_table
	ldr	r6, [r4, #140]
	tst	r6, #128
	beq	.L1764
	cmp	r5, #3
	beq	.L1926
	ldrb	r8, [r4, #323]	@ zero_extendqisi2
	mvn	r7, #0
	mvn	r0, r7, asl r8
	bl	__aeabi_ui2d
	ldrb	r3, [r4, #332]	@ zero_extendqisi2
	cmp	r3, #2
	mov	r5, r0
	mov	r6, r1
	beq	.L1785
	cmp	r3, #3
	beq	.L1786
	cmp	r3, #1
	beq	.L1784
	mov	sl, #1069547520
	mov	r2, #0
	add	r3, sl, #3145728
	stmia	sp, {r2-r3}
	str	r2, [sp, #8]
	str	r3, [sp, #12]
.L1787:
	mov	r8, #348
	ldrh	r0, [r4, r8]
	bl	__aeabi_ui2d
	mov	r2, r5
	mov	r3, r6
	bl	__aeabi_ddiv
	add	r3, sp, #8
	ldmia	r3, {r2-r3}
	bl	pow
	mov	r2, r5
	mov	r3, r6
	bl	__aeabi_dmul
	mov	ip, #1069547520
	add	r3, ip, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	mov	r7, #356
	add	r7, r7, #2
	strh	r0, [r4, r7]	@ movhi
	ldrh	r0, [r4, r8]
	bl	__aeabi_ui2d
	mov	r2, r5
	mov	r3, r6
	bl	__aeabi_ddiv
	ldmia	sp, {r2-r3}
	bl	pow
	mov	sl, #1069547520
	mov	r2, r5
	mov	r3, r6
	bl	__aeabi_dmul
	add	r3, sl, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	mov	r3, #340
	add	r3, r3, #2
	mov	sl, #344
	ldrh	r2, [r4, r3]
	ldrh	r1, [r4, sl]
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
	cmp	r1, r2
	strh	r0, [r4, r8]	@ movhi
	bne	.L1788
	add	ip, sl, #2
	ldrh	r8, [r4, ip]
	cmp	r8, r1
	beq	.L1927
.L1788:
	mov	r0, r2
	bl	__aeabi_ui2d
	mov	r2, r5
	mov	r3, r6
	bl	__aeabi_ddiv
	add	r3, sp, #8
	ldmia	r3, {r2-r3}
	bl	pow
	mov	sl, #1069547520
	mov	r2, r5
	mov	r3, r6
	bl	__aeabi_dmul
	add	r3, sl, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	mov	sl, #344
	mov	ip, #352
	strh	r0, [r4, ip]	@ movhi
	ldrh	r0, [r4, sl]
	bl	__aeabi_ui2d
	mov	r2, r5
	mov	r3, r6
	bl	__aeabi_ddiv
	add	r3, sp, #8
	ldmia	r3, {r2-r3}
	bl	pow
	mov	r2, r5
	mov	r3, r6
	bl	__aeabi_dmul
	mov	r3, #1069547520
	mov	r2, #0
	add	r3, r3, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	mov	r8, #352
	add	ip, r8, #2
	add	r7, sl, #2
	strh	r0, [r4, ip]	@ movhi
	ldrh	r0, [r4, r7]
	bl	__aeabi_ui2d
	mov	r2, r5
	mov	r3, r6
	bl	__aeabi_ddiv
	add	r3, sp, #8
	ldmia	r3, {r2-r3}
	bl	pow
	mov	r2, r5
	mov	r3, r6
	bl	__aeabi_dmul
	mov	r3, #1069547520
	mov	r2, #0
	add	r3, r3, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	mov	r8, #340
	mov	ip, #356
	add	r8, r8, #2
	strh	r0, [r4, ip]	@ movhi
	ldrh	r0, [r4, r8]
	bl	__aeabi_ui2d
	mov	r2, r5
	mov	r3, r6
	bl	__aeabi_ddiv
	ldmia	sp, {r2-r3}
	bl	pow
	mov	r2, r5
	mov	r3, r6
	bl	__aeabi_dmul
	mov	r3, #1069547520
	mov	r2, #0
	add	r3, r3, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	strh	r0, [r4, r8]	@ movhi
	ldrh	r0, [r4, sl]
	bl	__aeabi_ui2d
	mov	r2, r5
	mov	r3, r6
	bl	__aeabi_ddiv
	ldmia	sp, {r2-r3}
	bl	pow
	mov	r2, r5
	mov	r3, r6
	bl	__aeabi_dmul
	mov	ip, #1069547520
	add	r3, ip, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	strh	r0, [r4, sl]	@ movhi
	ldrh	r0, [r4, r7]
	bl	__aeabi_ui2d
	mov	r2, r5
	mov	r3, r6
	bl	__aeabi_ddiv
	ldmia	sp, {r2-r3}
	bl	pow
	mov	r2, r5
	mov	r3, r6
	bl	__aeabi_dmul
	mov	r3, #1069547520
	mov	r2, #0
	add	r3, r3, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	strh	r0, [r4, r7]	@ movhi
	b	.L1801
.L1924:
	ldrb	r3, [r4, #323]	@ zero_extendqisi2
	sub	lr, r3, #1
	cmp	lr, #15
	ldrls	pc, [pc, lr, asl #2]
	b	.L1744
.L1750:
	.word	.L1746
	.word	.L1747
	.word	.L1744
	.word	.L1748
	.word	.L1744
	.word	.L1744
	.word	.L1744
	.word	.L1749
	.word	.L1744
	.word	.L1744
	.word	.L1744
	.word	.L1744
	.word	.L1744
	.word	.L1744
	.word	.L1744
	.word	.L1749
.L1794:
	cmp	r0, #255
	rsb	sl, r0, #255
	mov	r6, r2
	beq	.L1795
	ldrb	r9, [r2, r1]	@ zero_extendqisi2
	mul	r0, r9, r0
	add	r0, r0, #128
	mla	r0, sl, r7, r0
	mov	lr, r0, asl #16
	mov	sl, lr, lsr #24
	add	r0, sl, lr, lsr #16
	mov	sl, r0, asr #8
	strb	sl, [r6, r1]!
	ldr	lr, [r4, #420]
	ldrb	sl, [r6, #1]	@ zero_extendqisi2
	ldrb	r0, [lr, r3]	@ zero_extendqisi2
	mul	sl, r0, sl
	rsb	r0, r0, #255
	add	lr, sl, #128
	mla	r0, r5, r0, lr
	mov	lr, r0, asl #16
	mov	sl, lr, lsr #24
	add	r0, sl, lr, lsr #16
	mov	lr, r0, asr #8
	strb	lr, [r6, #1]
	ldr	sl, [r4, #420]
	ldrb	lr, [r6, #2]	@ zero_extendqisi2
	ldrb	r0, [sl, r3]	@ zero_extendqisi2
	mul	lr, r0, lr
	rsb	r0, r0, #255
	add	sl, lr, #128
	mla	r0, ip, r0, sl
	mov	lr, r0, asl #16
	mov	sl, lr, lsr #24
	add	r0, sl, lr, lsr #16
	mov	lr, r0, asr #8
	strb	lr, [r6, #2]
	b	.L1795
.L1764:
	cmp	r5, #3
	bne	.L1801
	mov	r0, #308
	ldrh	r1, [r4, r0]
	cmp	r1, #0
	ldr	r3, [r4, #304]
	beq	.L1791
	ldr	r0, [r4, #384]
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	ldrb	r2, [r0, r6]	@ zero_extendqisi2
	strb	r2, [r3, #0]
	ldrb	ip, [r3, #1]	@ zero_extendqisi2
	ldr	lr, [r4, #384]
	ldrb	r6, [lr, ip]	@ zero_extendqisi2
	strb	r6, [r3, #1]
	ldrb	r2, [r3, #2]	@ zero_extendqisi2
	ldr	r0, [r4, #384]
	add	r1, r1, r1, asl #1
	ldrb	ip, [r0, r2]	@ zero_extendqisi2
	sub	lr, r1, #3
	add	r6, lr, lr, asl #1
	cmp	r5, r1
	strb	ip, [r3, #2]
	and	r2, r6, #3
	beq	.L1889
	cmp	r2, #0
	beq	.L1792
	cmp	r2, #1
	beq	.L1877
	cmp	r2, #2
	beq	.L1878
	ldr	r5, [r4, #384]
	ldrb	r2, [r3, #3]	@ zero_extendqisi2
	ldrb	ip, [r5, r2]	@ zero_extendqisi2
	mov	r5, r3
	strb	ip, [r5, #3]!
	ldr	r6, [r4, #384]
	ldrb	r0, [r5, #1]	@ zero_extendqisi2
	ldrb	lr, [r6, r0]	@ zero_extendqisi2
	strb	lr, [r5, #1]
	ldrb	r2, [r5, #2]	@ zero_extendqisi2
	ldr	ip, [r4, #384]
	ldrb	r6, [ip, r2]	@ zero_extendqisi2
	strb	r6, [r5, #2]
	mov	r5, #6
.L1878:
	ldr	r6, [r4, #384]
	ldrb	r2, [r3, r5]	@ zero_extendqisi2
	ldrb	ip, [r6, r2]	@ zero_extendqisi2
	mov	r6, r3
	strb	ip, [r6, r5]!
	ldr	r0, [r4, #384]
	ldrb	lr, [r6, #1]	@ zero_extendqisi2
	ldrb	r2, [r0, lr]	@ zero_extendqisi2
	strb	r2, [r6, #1]
	ldrb	r0, [r6, #2]	@ zero_extendqisi2
	ldr	ip, [r4, #384]
	ldrb	lr, [ip, r0]	@ zero_extendqisi2
	strb	lr, [r6, #2]
	add	r5, r5, #3
.L1877:
	ldr	r6, [r4, #384]
	ldrb	r2, [r3, r5]	@ zero_extendqisi2
	ldrb	ip, [r6, r2]	@ zero_extendqisi2
	mov	r6, r3
	strb	ip, [r6, r5]!
	ldr	r0, [r4, #384]
	ldrb	lr, [r6, #1]	@ zero_extendqisi2
	ldrb	r2, [r0, lr]	@ zero_extendqisi2
	strb	r2, [r6, #1]
	ldrb	r0, [r6, #2]	@ zero_extendqisi2
	ldr	ip, [r4, #384]
	add	r5, r5, #3
	ldrb	lr, [ip, r0]	@ zero_extendqisi2
	cmp	r5, r1
	strb	lr, [r6, #2]
	beq	.L1889
.L1792:
	ldr	r6, [r4, #384]
	ldrb	r2, [r3, r5]	@ zero_extendqisi2
	ldrb	ip, [r6, r2]	@ zero_extendqisi2
	mov	r6, r3
	strb	ip, [r6, r5]!
	ldr	r0, [r4, #384]
	ldrb	lr, [r6, #1]	@ zero_extendqisi2
	ldrb	r2, [r0, lr]	@ zero_extendqisi2
	strb	r2, [r6, #1]
	ldrb	r0, [r6, #2]	@ zero_extendqisi2
	ldr	ip, [r4, #384]
	ldrb	lr, [ip, r0]	@ zero_extendqisi2
	strb	lr, [r6, #2]
	add	ip, r5, #3
	ldr	r6, [r4, #384]
	ldrb	r2, [r3, ip]	@ zero_extendqisi2
	ldrb	r0, [r6, r2]	@ zero_extendqisi2
	mov	r6, r3
	strb	r0, [r6, ip]!
	ldr	lr, [r4, #384]
	ldrb	r0, [r6, #1]	@ zero_extendqisi2
	ldrb	r2, [lr, r0]	@ zero_extendqisi2
	strb	r2, [r6, #1]
	ldrb	lr, [r6, #2]	@ zero_extendqisi2
	ldr	ip, [r4, #384]
	ldrb	r0, [ip, lr]	@ zero_extendqisi2
	strb	r0, [r6, #2]
	add	ip, r5, #6
	ldr	lr, [r4, #384]
	ldrb	r2, [r3, ip]	@ zero_extendqisi2
	ldrb	r6, [lr, r2]	@ zero_extendqisi2
	mov	lr, r3
	strb	r6, [lr, ip]!
	ldrb	r6, [lr, #1]	@ zero_extendqisi2
	ldr	r0, [r4, #384]
	ldrb	r2, [r0, r6]	@ zero_extendqisi2
	strb	r2, [lr, #1]
	ldrb	r0, [lr, #2]	@ zero_extendqisi2
	ldr	ip, [r4, #384]
	ldrb	r6, [ip, r0]	@ zero_extendqisi2
	add	ip, r5, #9
	strb	r6, [lr, #2]
	ldr	lr, [r4, #384]
	ldrb	r2, [r3, ip]	@ zero_extendqisi2
	ldrb	r0, [lr, r2]	@ zero_extendqisi2
	mov	r2, r3
	strb	r0, [r2, ip]!
	ldr	r6, [r4, #384]
	ldrb	lr, [r2, #1]	@ zero_extendqisi2
	ldrb	r0, [r6, lr]	@ zero_extendqisi2
	strb	r0, [r2, #1]
	ldrb	r6, [r2, #2]	@ zero_extendqisi2
	ldr	ip, [r4, #384]
	add	r5, r5, #12
	ldrb	lr, [ip, r6]	@ zero_extendqisi2
	cmp	r5, r1
	strb	lr, [r2, #2]
	bne	.L1792
.L1889:
	ldr	r6, [r4, #140]
.L1791:
	bic	r6, r6, #8192
	str	r6, [r4, #140]
	mov	r5, #1
	b	.L1782
.L1749:
	mov	r3, #348
	ldrh	lr, [r4, r3]
	mov	r2, #344
	mov	r1, #340
	add	ip, r2, #2
	add	r6, r1, #2
	mov	r0, #344
	strh	lr, [r4, r6]	@ movhi
	strh	lr, [r4, ip]	@ movhi
	strh	lr, [r4, r0]	@ movhi
	b	.L1744
.L1748:
	mov	lr, #348
	ldrh	r1, [r4, lr]
	add	r2, r1, r1, asl #4
	mov	r3, r2, asl #16
	mov	r6, #344
	mov	ip, #340
	mov	r3, r3, lsr #16
	tst	r0, #33554432
	add	r2, r6, #2
	add	r1, ip, #2
	mov	r0, #344
	strh	r3, [r4, r1]	@ movhi
	strh	r3, [r4, lr]	@ movhi
	strh	r3, [r4, r2]	@ movhi
	strh	r3, [r4, r0]	@ movhi
	bne	.L1744
	mov	r0, #432
	ldrh	r3, [r4, r0]
	add	r3, r3, r3, asl #4
	b	.L1914
.L1747:
	mov	r6, #348
	ldrh	ip, [r4, r6]
	add	r1, ip, ip, asl #2
	add	lr, r1, r1, asl #4
	mov	r3, lr, asl #16
	mov	r2, #344
	mov	ip, #340
	mov	lr, r3, lsr #16
	tst	r0, #33554432
	add	r2, r2, #2
	add	r1, ip, #2
	mov	r0, #344
	strh	lr, [r4, r1]	@ movhi
	strh	lr, [r4, r6]	@ movhi
	strh	lr, [r4, r2]	@ movhi
	strh	lr, [r4, r0]	@ movhi
	bne	.L1744
	mov	r0, #432
	ldrh	r6, [r4, r0]
	add	r3, r6, r6, asl #2
	add	r3, r3, r3, asl #4
	b	.L1914
.L1746:
	mov	ip, #348
	ldrh	r1, [r4, ip]
	rsb	r6, r1, r1, asl #8
	mov	r3, r6, asl #16
	mov	r2, #344
	mov	lr, #340
	mov	r6, r3, lsr #16
	tst	r0, #33554432
	add	r2, r2, #2
	add	r1, lr, #2
	mov	r0, #344
	strh	r6, [r4, r1]	@ movhi
	strh	r6, [r4, ip]	@ movhi
	strh	r6, [r4, r2]	@ movhi
	strh	r6, [r4, r0]	@ movhi
	bne	.L1744
	mov	r0, #432
	ldrh	ip, [r4, r0]
	rsb	r3, ip, ip, asl #8
.L1914:
	mov	r3, r3, asl #16
	mov	lr, r3, lsr #16
	mov	r2, #428
	mov	r1, #424
	strh	lr, [r4, r0]	@ movhi
	add	ip, r2, #2
	add	r6, r1, #2
	mov	r0, #428
	strh	lr, [r4, r6]	@ movhi
	strh	lr, [r4, ip]	@ movhi
	strh	lr, [r4, r0]	@ movhi
	b	.L1744
.L1785:
	ldr	r7, [r4, #376]	@ float
.L1916:
	mov	r0, r7
	bl	__aeabi_f2d
	mov	ip, #1069547520
	mov	r2, r0
	mov	r3, r1
	mov	r0, #0
	add	r1, ip, #3145728
	bl	__aeabi_ddiv
	str	r0, [sp, #8]
	str	r1, [sp, #12]
	mov	r0, r7
	ldr	r1, [r4, #380]	@ float
	bl	__aeabi_fmul
	bl	__aeabi_f2d
	mov	r3, r1
	mov	r1, #1069547520
	mov	r2, r0
	add	r1, r1, #3145728
	mov	r0, #0
	bl	__aeabi_ddiv
	stmia	sp, {r0-r1}
	b	.L1787
.L1926:
	ldrb	r3, [r4, #332]	@ zero_extendqisi2
	mov	r2, #308
	cmp	r3, #2
	ldrh	r7, [r4, r2]
	ldr	r5, [r4, #304]
	beq	.L1928
	cmp	r3, #3
	beq	.L1771
	cmp	r3, #1
	beq	.L1929
	mov	r8, #1069547520
	mov	r2, #0
	add	r3, r8, #3145728
	str	r2, [sp, #16]
	str	r3, [sp, #20]
.L1772:
	mov	r0, #340
	add	ip, r0, #2
	ldrh	r0, [r4, ip]
	ldrb	sl, [r4, #344]	@ zero_extendqisi2
	ldrb	r8, [r4, #346]	@ zero_extendqisi2
	and	r6, r0, #255
.L1776:
	bl	__aeabi_ui2d
	mov	r3, #1073741824
	add	ip, r3, #7274496
	add	r3, ip, #57344
	mov	r2, #0
	bl	__aeabi_ddiv
	add	r3, sp, #16
	ldmia	r3, {r2-r3}
	bl	pow
	mov	r3, #1073741824
	add	ip, r3, #7274496
	add	r3, ip, #57344
	mov	r2, #0
	bl	__aeabi_dmul
	mov	r3, #1069547520
	add	r3, r3, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	mov	r2, #344
	mov	ip, r0
	ldrh	r0, [r4, r2]
	and	r2, ip, #255
	str	r2, [sp, #0]
	bl	__aeabi_ui2d
	mov	r3, #1073741824
	add	ip, r3, #7274496
	add	r3, ip, #57344
	mov	r2, #0
	bl	__aeabi_ddiv
	add	r3, sp, #16
	ldmia	r3, {r2-r3}
	bl	pow
	mov	r3, #1073741824
	add	ip, r3, #7274496
	add	r3, ip, #57344
	mov	r2, #0
	bl	__aeabi_dmul
	mov	r3, #1069547520
	add	r3, r3, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	mov	ip, r0
	mov	r0, #344
	add	r2, r0, #2
	ldrh	r0, [r4, r2]
	and	r2, ip, #255
	str	r2, [sp, #8]
	bl	__aeabi_ui2d
	mov	r3, #1073741824
	add	ip, r3, #7274496
	add	r3, ip, #57344
	mov	r2, #0
	bl	__aeabi_ddiv
	add	r3, sp, #16
	ldmia	r3, {r2-r3}
	bl	pow
	mov	r3, #1073741824
	add	ip, r3, #7274496
	add	r3, ip, #57344
	mov	r2, #0
	bl	__aeabi_dmul
	mov	r3, #1069547520
	mov	r2, #0
	add	r3, r3, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	and	r0, r0, #255
	str	r0, [sp, #16]
.L1767:
	cmp	r7, #0
	beq	.L1777
	mov	r1, #308
	add	r1, r1, #2
	ldrh	r0, [r4, r1]
	sub	lr, r7, #1
	cmp	r0, #0
	and	r0, lr, #1
	ble	.L1820
	ldr	ip, [r4, #420]
	ldrb	r3, [ip, #0]	@ zero_extendqisi2
	cmp	r3, #255
	beq	.L1820
	cmp	r3, #0
	streqb	r8, [r5, #2]
	streqb	sl, [r5, #1]
	streqb	r6, [r5, #0]
	beq	.L1892
	ldr	fp, [r4, #392]
	ldrb	lr, [r5, #0]	@ zero_extendqisi2
	ldrb	r9, [fp, lr]	@ zero_extendqisi2
	mul	r9, r3, r9
	add	r2, r9, #128
	ldr	r9, [sp, #0]
	rsb	r3, r3, #255
	mla	r3, r9, r3, r2
	mov	fp, r3, asl #16
	mov	ip, fp, lsr #24
	add	lr, ip, fp, lsr #16
	mov	r2, lr, asr #8
	ldr	ip, [r4, #388]
	and	r3, r2, #255
	ldrb	fp, [ip, r3]	@ zero_extendqisi2
	strb	fp, [r5, #0]
	ldrb	lr, [r5, #1]	@ zero_extendqisi2
	ldr	ip, [r4, #420]
	ldr	r2, [r4, #392]
	ldrb	r3, [ip, #0]	@ zero_extendqisi2
	ldrb	fp, [r2, lr]	@ zero_extendqisi2
	mul	fp, r3, fp
	add	lr, fp, #128
	ldr	fp, [sp, #8]
	rsb	r3, r3, #255
	mla	r3, fp, r3, lr
	mov	ip, r3, asl #16
	mov	r2, ip, lsr #24
	add	lr, r2, ip, lsr #16
	mov	r3, lr, asr #8
	ldr	r2, [r4, #388]
	and	ip, r3, #255
	ldrb	lr, [r2, ip]	@ zero_extendqisi2
	strb	lr, [r5, #1]
	ldrb	r3, [r5, #2]	@ zero_extendqisi2
	ldr	lr, [r4, #420]
	ldr	ip, [r4, #392]
	ldrb	r2, [ip, r3]	@ zero_extendqisi2
	ldrb	r3, [lr, #0]	@ zero_extendqisi2
	mul	r2, r3, r2
	ldr	ip, [sp, #16]
	rsb	lr, r3, #255
	add	r2, r2, #128
	mla	lr, ip, lr, r2
	mov	r3, lr, asl #16
	mov	r2, r3, lsr #24
	add	lr, r2, r3, lsr #16
	mov	ip, lr, asr #8
	ldr	r2, [r4, #388]
	and	r3, ip, #255
	ldrb	lr, [r2, r3]	@ zero_extendqisi2
	strb	lr, [r5, #2]
.L1892:
	mov	r3, #1
	cmp	r7, r3
	mov	r2, #3
	ble	.L1777
	cmp	r0, #0
	beq	.L1781
	ldrh	r0, [r4, r1]
	cmp	r0, r3
	ble	.L1827
	ldr	ip, [r4, #420]
	ldrb	r0, [ip, r3]	@ zero_extendqisi2
	cmp	r0, #255
	beq	.L1827
	cmp	r0, #0
	beq	.L1896
	ldr	fp, [r4, #392]
	ldrb	lr, [r5, r2]	@ zero_extendqisi2
	ldrb	ip, [fp, lr]	@ zero_extendqisi2
	mul	ip, r0, ip
	ldr	fp, [sp, #0]
	add	r9, ip, #128
	rsb	lr, r0, #255
	mla	lr, fp, lr, r9
	mov	r9, lr, asl #16
	mov	ip, r9, lsr #24
	add	r0, ip, r9, lsr #16
	ldr	lr, [r4, #388]
	mov	fp, r0, asr #8
	and	r9, fp, #255
	ldrb	r0, [lr, r9]	@ zero_extendqisi2
	strb	r0, [r5, r2]
	add	r0, r5, #3
	ldr	lr, [r4, #420]
	ldr	fp, [r4, #392]
	ldrb	ip, [r0, #1]	@ zero_extendqisi2
	ldrb	r9, [fp, ip]	@ zero_extendqisi2
	ldrb	ip, [lr, r3]	@ zero_extendqisi2
	mul	r9, ip, r9
	ldr	fp, [sp, #8]
	add	lr, r9, #128
	rsb	ip, ip, #255
	mla	ip, fp, ip, lr
	mov	fp, ip, asl #16
	mov	r9, fp, lsr #24
	add	lr, r9, fp, lsr #16
	mov	ip, lr, asr #8
	ldr	r9, [r4, #388]
	and	fp, ip, #255
	ldrb	lr, [r9, fp]	@ zero_extendqisi2
	strb	lr, [r0, #1]
	ldrb	ip, [r0, #2]	@ zero_extendqisi2
	ldr	fp, [r4, #420]
	ldr	r9, [r4, #392]
	ldrb	lr, [r9, ip]	@ zero_extendqisi2
	ldrb	ip, [fp, r3]	@ zero_extendqisi2
	mul	lr, ip, lr
	ldr	fp, [sp, #16]
	rsb	ip, ip, #255
	add	r9, lr, #128
	mla	ip, fp, ip, r9
	b	.L1917
.L1930:
	ldr	lr, [r4, #420]
	ldrb	r0, [lr, r3]	@ zero_extendqisi2
	cmp	r0, #255
	beq	.L1778
	cmp	r0, #0
	bne	.L1779
	add	r0, r5, r2
	strb	sl, [r0, #1]
	strb	r8, [r0, #2]
	strb	r6, [r5, r2]
.L1780:
	ldrh	lr, [r4, r1]
	add	r3, r3, #1
	cmp	lr, r3
	add	r2, r2, #3
	ble	.L1827
	ldr	ip, [r4, #420]
	ldrb	r0, [ip, r3]	@ zero_extendqisi2
	cmp	r0, #255
	beq	.L1827
	cmp	r0, #0
	beq	.L1898
	ldr	fp, [r4, #392]
	ldrb	ip, [r5, r2]	@ zero_extendqisi2
	ldrb	lr, [fp, ip]	@ zero_extendqisi2
	mul	lr, r0, lr
	ldr	fp, [sp, #0]
	add	r9, lr, #128
	rsb	r0, r0, #255
	mla	r0, fp, r0, r9
	mov	lr, r0, asl #16
	mov	ip, lr, lsr #24
	add	r9, ip, lr, lsr #16
	mov	fp, r9, asr #8
	ldr	lr, [r4, #388]
	and	r0, fp, #255
	ldrb	ip, [lr, r0]	@ zero_extendqisi2
	mov	r0, r5
	strb	ip, [r0, r2]!
	ldr	lr, [r4, #420]
	ldr	r9, [r4, #392]
	ldrb	fp, [r0, #1]	@ zero_extendqisi2
	ldrb	ip, [r9, fp]	@ zero_extendqisi2
	ldrb	r9, [lr, r3]	@ zero_extendqisi2
	mul	ip, r9, ip
	ldr	fp, [sp, #8]
	rsb	lr, r9, #255
	add	ip, ip, #128
	mla	ip, lr, fp, ip
	mov	fp, ip, asl #16
	mov	r9, fp, lsr #24
	add	lr, r9, fp, lsr #16
	mov	ip, lr, asr #8
	ldr	r9, [r4, #388]
	and	fp, ip, #255
	ldrb	lr, [r9, fp]	@ zero_extendqisi2
	strb	lr, [r0, #1]
	ldrb	ip, [r0, #2]	@ zero_extendqisi2
	ldr	fp, [r4, #420]
	ldr	r9, [r4, #392]
	ldrb	lr, [r9, ip]	@ zero_extendqisi2
	ldrb	r9, [fp, r3]	@ zero_extendqisi2
	mul	lr, r9, lr
	ldr	fp, [sp, #16]
	rsb	r9, r9, #255
	add	ip, lr, #128
	mla	ip, r9, fp, ip
.L1917:
	mov	lr, ip, asl #16
	mov	r9, lr, lsr #24
	add	ip, r9, lr, lsr #16
	mov	lr, ip, asr #8
	ldr	r9, [r4, #388]
	and	ip, lr, #255
	ldrb	lr, [r9, ip]	@ zero_extendqisi2
	strb	lr, [r0, #2]
.L1897:
	add	r3, r3, #1
	cmp	r7, r3
	add	r2, r2, #3
	ble	.L1777
.L1781:
	ldrh	ip, [r4, r1]
	cmp	ip, r3
	bgt	.L1930
.L1778:
	ldr	ip, [r4, #384]
	ldrb	r0, [r5, r2]	@ zero_extendqisi2
	ldrb	lr, [ip, r0]	@ zero_extendqisi2
	mov	r0, r5
	strb	lr, [r0, r2]!
	ldr	r9, [r4, #384]
	ldrb	ip, [r0, #1]	@ zero_extendqisi2
	ldrb	lr, [r9, ip]	@ zero_extendqisi2
	strb	lr, [r0, #1]
	ldrb	ip, [r0, #2]	@ zero_extendqisi2
	ldr	r9, [r4, #384]
	ldrb	lr, [r9, ip]	@ zero_extendqisi2
	strb	lr, [r0, #2]
	b	.L1780
.L1827:
	ldr	lr, [r4, #384]
	ldrb	r0, [r5, r2]	@ zero_extendqisi2
	ldrb	ip, [lr, r0]	@ zero_extendqisi2
	mov	lr, r5
	strb	ip, [lr, r2]!
	ldr	r9, [r4, #384]
	ldrb	r0, [lr, #1]	@ zero_extendqisi2
	ldrb	ip, [r9, r0]	@ zero_extendqisi2
	strb	ip, [lr, #1]
	ldrb	r0, [lr, #2]	@ zero_extendqisi2
	ldr	r9, [r4, #384]
	ldrb	ip, [r9, r0]	@ zero_extendqisi2
	strb	ip, [lr, #2]
	b	.L1897
.L1777:
	ldr	r6, [r4, #140]
	bic	r5, r6, #8320
	orr	r6, r5, #262144
	str	r6, [r4, #140]
	mov	r5, #1
	b	.L1782
.L1898:
	add	r0, r5, r2
	strb	sl, [r0, #1]
	strb	r8, [r0, #2]
	strb	r6, [r5, r2]
	b	.L1897
.L1779:
	ldr	fp, [r4, #392]
	ldrb	ip, [r5, r2]	@ zero_extendqisi2
	ldrb	lr, [fp, ip]	@ zero_extendqisi2
	mul	lr, r0, lr
	ldr	fp, [sp, #0]
	add	r9, lr, #128
	rsb	r0, r0, #255
	mla	r0, fp, r0, r9
	mov	lr, r0, asl #16
	mov	ip, lr, lsr #24
	add	r9, ip, lr, lsr #16
	mov	fp, r9, asr #8
	ldr	lr, [r4, #388]
	and	r0, fp, #255
	ldrb	ip, [lr, r0]	@ zero_extendqisi2
	mov	lr, r5
	strb	ip, [lr, r2]!
	ldr	r0, [r4, #420]
	ldr	r9, [r4, #392]
	ldrb	fp, [lr, #1]	@ zero_extendqisi2
	ldrb	ip, [r9, fp]	@ zero_extendqisi2
	ldrb	r9, [r0, r3]	@ zero_extendqisi2
	mul	ip, r9, ip
	ldr	fp, [sp, #8]
	rsb	r0, r9, #255
	add	ip, ip, #128
	mla	ip, r0, fp, ip
	mov	fp, ip, asl #16
	mov	r9, fp, lsr #24
	add	r0, r9, fp, lsr #16
	mov	ip, r0, asr #8
	ldr	r9, [r4, #388]
	and	fp, ip, #255
	ldrb	r0, [r9, fp]	@ zero_extendqisi2
	strb	r0, [lr, #1]
	ldrb	ip, [lr, #2]	@ zero_extendqisi2
	ldr	fp, [r4, #420]
	ldr	r9, [r4, #392]
	ldrb	r0, [fp, r3]	@ zero_extendqisi2
	ldrb	ip, [r9, ip]	@ zero_extendqisi2
	mul	ip, r0, ip
	ldr	fp, [sp, #16]
	rsb	r9, r0, #255
	add	r0, ip, #128
	mla	r0, r9, fp, r0
	mov	ip, r0, asl #16
	mov	r9, ip, lsr #24
	add	r0, r9, ip, lsr #16
	mov	ip, r0, asr #8
	ldr	r9, [r4, #388]
	and	r0, ip, #255
	ldrb	ip, [r9, r0]	@ zero_extendqisi2
	strb	ip, [lr, #2]
	b	.L1780
.L1784:
	mov	r2, #1069547520
	ldr	r0, [r4, #380]	@ float
	add	ip, r2, #3145728
	mov	fp, #0
	stmia	sp, {fp-ip}
	bl	__aeabi_f2d
	str	r0, [sp, #8]
	str	r1, [sp, #12]
	b	.L1787
.L1786:
	ldr	r7, [r4, #336]	@ float
	b	.L1916
.L1820:
	ldr	r2, [r4, #384]
	ldrb	r3, [r5, #0]	@ zero_extendqisi2
	ldrb	lr, [r2, r3]	@ zero_extendqisi2
	strb	lr, [r5, #0]
	ldrb	r3, [r5, #1]	@ zero_extendqisi2
	ldr	ip, [r4, #384]
	ldrb	lr, [ip, r3]	@ zero_extendqisi2
	strb	lr, [r5, #1]
	ldrb	ip, [r5, #2]	@ zero_extendqisi2
	ldr	r2, [r4, #384]
	ldrb	r3, [r2, ip]	@ zero_extendqisi2
	strb	r3, [r5, #2]
	b	.L1892
.L1927:
	cmp	r0, r8
	bne	.L1788
	ldrh	r1, [r4, r7]
	mov	r2, #352
	strh	r0, [r4, r3]	@ movhi
	mov	r3, #352
	add	r2, r2, #2
	add	lr, r3, #4
	strh	r1, [r4, r3]	@ movhi
	strh	r1, [r4, lr]	@ movhi
	strh	r1, [r4, r2]	@ movhi
	strh	r0, [r4, ip]	@ movhi
	strh	r0, [r4, sl]	@ movhi
	b	.L1801
.L1928:
	mov	lr, #344
	add	r6, lr, #2
	ldrh	r9, [r4, r6]
	add	r3, r2, #32
	ldr	r2, [r4, #392]
	add	ip, r3, #2
	ldrb	r8, [r2, r9]	@ zero_extendqisi2
	add	r1, ip, #2
	ldrh	sl, [r4, ip]
	ldr	r3, [r4, #384]
	ldrh	lr, [r4, r1]
	str	r8, [sp, #16]
	ldrb	r8, [r3, r9]	@ zero_extendqisi2
	ldrb	r9, [r2, sl]	@ zero_extendqisi2
	str	r9, [sp, #0]
	ldrb	r0, [r2, lr]	@ zero_extendqisi2
	str	r0, [sp, #8]
	ldrb	r6, [r3, sl]	@ zero_extendqisi2
	ldrb	sl, [r3, lr]	@ zero_extendqisi2
	b	.L1767
.L1896:
	add	r0, r5, #3
	strb	sl, [r0, #1]
	strb	r8, [r0, #2]
	strb	r6, [r5, r2]
	b	.L1897
.L1929:
	ldr	r0, [r4, #380]	@ float
	bl	__aeabi_f2d
	str	r0, [sp, #16]
	str	r1, [sp, #20]
	b	.L1772
.L1771:
	ldr	sl, [r4, #336]	@ float
	mov	r0, sl
	bl	__aeabi_f2d
	mov	r3, r1
	mov	r1, #1069547520
	mov	r2, r0
	add	r1, r1, #3145728
	mov	r0, #0
	bl	__aeabi_ddiv
	str	r0, [sp, #16]
	str	r1, [sp, #20]
	mov	r0, sl
	ldr	r1, [r4, #380]	@ float
	bl	__aeabi_fmul
	bl	__aeabi_f2d
	mov	r6, #1069547520
	mov	r2, r0
	mov	r3, r1
	mov	r0, #0
	add	r1, r6, #3145728
	bl	__aeabi_ddiv
	mov	r8, #1069547520
	mov	r2, #0
	add	r3, r8, #3145728
	stmia	sp, {r0-r1}
	bl	__aeabi_dsub
	bic	sl, r1, #-2147483648
	mov	r1, sl
	adr	r3, .L1933
	ldmia	r3, {r2-r3}
	bl	__aeabi_dcmplt
	cmp	r0, #0
	bne	.L1772
	mov	lr, #340
	add	r8, lr, #2
	ldrh	r0, [r4, r8]
	bl	__aeabi_ui2d
	mov	ip, #1073741824
	add	r3, ip, #7274496
	mov	r2, #0
	add	r3, r3, #57344
	mov	r6, #1073741824
	bl	__aeabi_ddiv
	add	sl, r6, #7274496
	ldmia	sp, {r2-r3}
	bl	pow
	add	r3, sl, #57344
	mov	r2, #0
	bl	__aeabi_dmul
	mov	ip, #1069547520
	add	r3, ip, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	mov	r3, #344
	mov	r6, r0
	mov	sl, #1073741824
	ldrh	r0, [r4, r3]
	bl	__aeabi_ui2d
	add	ip, sl, #7274496
	add	r3, ip, #57344
	mov	r2, #0
	bl	__aeabi_ddiv
	ldmia	sp, {r2-r3}
	bl	pow
	mov	r3, #1073741824
	add	sl, r3, #7274496
	add	r3, sl, #57344
	mov	r2, #0
	bl	__aeabi_dmul
	mov	ip, #1069547520
	add	r3, ip, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	mov	sl, r0
	mov	r0, #344
	add	r3, r0, #2
	ldrh	r0, [r4, r3]
	bl	__aeabi_ui2d
	mov	ip, #1073741824
	add	r3, ip, #7274496
	mov	r2, #0
	add	r3, r3, #57344
	bl	__aeabi_ddiv
	ldmia	sp, {r2-r3}
	bl	pow
	mov	ip, #1073741824
	add	r3, ip, #7274496
	mov	r2, #0
	add	r3, r3, #57344
	bl	__aeabi_dmul
	mov	ip, #1069547520
	add	r3, ip, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	mov	r3, r0
	and	r6, r6, #255
	ldrh	r0, [r4, r8]
	and	sl, sl, #255
	and	r8, r3, #255
	b	.L1776
.L1934:
	.align	3
.L1933:
	.word	-1717986918
	.word	1068079513
	.size	png_init_read_transformations, .-png_init_read_transformations
	.section	.text.png_do_background,"ax",%progbits
	.align	2
	.global	png_do_background
	.hidden	png_do_background
	.type	png_do_background, %function
png_do_background:
	@ Function supports interworking.
	@ args = 32, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	subs	r5, r3, #0
	sub	sp, sp, #28
	mov	r7, r1
	mov	r4, r0
	ldr	ip, [sp, #68]
	ldr	sl, [sp, #72]
	ldr	r9, [sp, #76]
	ldr	r1, [sp, #80]
	ldr	r3, [sp, #92]
	ldr	r6, [r0, #0]
	beq	.L2066
	ldrb	r0, [r0, #8]	@ zero_extendqisi2
	ands	r8, r0, #4
	bne	.L2580
.L1937:
	cmp	r0, #6
	ldrls	pc, [pc, r0, asl #2]
	b	.L1938
.L1943:
	.word	.L1939
	.word	.L1938
	.word	.L1940
	.word	.L1938
	.word	.L1941
	.word	.L1938
	.word	.L1942
.L2062:
	ldr	r4, [sp, #4]
.L2577:
	ldrb	r0, [r4, #8]	@ zero_extendqisi2
	and	r8, r0, #4
.L1938:
	cmp	r8, #0
	beq	.L2066
	ldrb	r2, [r4, #10]	@ zero_extendqisi2
	ldrb	r1, [r4, #9]	@ zero_extendqisi2
	sub	r3, r2, #1
	and	r2, r3, #255
	mul	r1, r2, r1
	and	r3, r1, #255
	cmp	r3, #7
	mulls	r6, r3, r6
	strb	r3, [r4, #11]
	movhi	r3, r3, lsr #3
	mulhi	r6, r3, r6
	addls	r6, r6, #7
	bic	ip, r0, #4
	movls	r6, r6, lsr #3
	strb	ip, [r4, #8]
	strb	r2, [r4, #10]
	str	r6, [r4, #4]
.L2066:
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L2580:
	cmp	r2, #0
	cmpne	r0, #3
	bne	.L1937
	b	.L2066
.L1942:
	ldrb	r2, [r4, #9]	@ zero_extendqisi2
	cmp	r2, #8
	beq	.L2581
	ldr	r9, [sp, #84]
	cmp	r9, #0
	cmpne	r1, #0
	beq	.L2053
	ldr	sl, [sp, #88]
	cmp	sl, #0
	beq	.L2053
	cmp	r6, #0
	beq	.L1938
	mov	ip, #65536
	sub	fp, ip, #1
	mov	r2, r7
	str	fp, [sp, #16]
	add	r7, r7, #6
	mov	ip, #0
	str	r4, [sp, #20]
	mov	fp, sl
	b	.L2058
.L2584:
	ldrh	r4, [r5, #2]
	mov	r0, r4, lsr #8
	strb	r0, [r7, #-6]
	ldrh	r8, [r5, #2]
	strb	r8, [r7, #-5]
	ldrh	r4, [r5, #4]
	mov	r0, r4, lsr #8
	strb	r0, [r7, #-4]
	ldrh	r8, [r5, #4]
	strb	r8, [r7, #-3]
	ldrh	r4, [r5, #6]
	mov	r0, r4, lsr #8
	strb	r0, [r7, #-2]
	ldrh	r8, [r5, #6]
	strb	r8, [r7, #-1]
.L2055:
	add	ip, ip, #1
	cmp	r6, ip
	add	r7, r7, #6
	bls	.L2582
	add	r2, r2, #8
.L2058:
	ldrb	r4, [r2, #6]	@ zero_extendqisi2
	ldrb	r0, [r2, #7]	@ zero_extendqisi2
	add	r8, r0, r4, asl #8
	mov	r0, r8, asl #16
	ldr	r4, [sp, #16]
	mov	r0, r0, lsr #16
	cmp	r0, r4
	beq	.L2583
	cmp	r0, #0
	beq	.L2584
	ldrb	r8, [r2, #1]	@ zero_extendqisi2
	mov	sl, r8, asr r3
	ldr	r9, [sp, #64]
	rsb	r4, r0, #65280
	ldrh	r8, [r9, #2]
	add	r4, r4, #255
	mul	r8, r4, r8
	ldrb	r9, [r2, #0]	@ zero_extendqisi2
	ldr	sl, [fp, sl, asl #2]
	mov	r9, r9, asl #1
	ldrh	sl, [r9, sl]
	add	r9, r8, #32768
	mla	r9, r0, sl, r9
	add	sl, r9, r9, lsr #16
	mov	r8, sl, lsr #16
	mov	r8, r8, asl #16
	mov	r9, r8, lsr #16
	and	sl, r9, #255
	mov	sl, sl, asr r3
	ldr	r9, [sp, #84]
	mov	r8, r8, lsr #24
	ldr	sl, [r9, sl, asl #2]
	mov	r8, r8, asl #1
	ldrh	r9, [r8, sl]
	mov	sl, r9, lsr #8
	strb	sl, [r7, #-6]
	strb	r9, [r7, #-5]
	ldrb	r8, [r2, #3]	@ zero_extendqisi2
	mov	sl, r8, asr r3
	ldr	r9, [sp, #64]
	ldrh	r8, [r9, #4]
	mul	r8, r4, r8
	ldrb	r9, [r2, #2]	@ zero_extendqisi2
	ldr	sl, [fp, sl, asl #2]
	mov	r9, r9, asl #1
	ldrh	sl, [r9, sl]
	add	r9, r8, #32768
	mla	r9, r0, sl, r9
	add	sl, r9, r9, lsr #16
	mov	r8, sl, lsr #16
	mov	r8, r8, asl #16
	mov	r9, r8, lsr #16
	and	sl, r9, #255
	mov	sl, sl, asr r3
	ldr	r9, [sp, #84]
	mov	r8, r8, lsr #24
	ldr	sl, [r9, sl, asl #2]
	mov	r9, r8, asl #1
	ldrh	r8, [r9, sl]
	mov	sl, r8, lsr #8
	strb	r8, [r7, #-3]
	strb	sl, [r7, #-4]
	ldrb	r9, [r2, #5]	@ zero_extendqisi2
	mov	r8, r9, asr r3
	ldr	r9, [sp, #64]
	ldrh	sl, [r9, #6]
	mul	r4, sl, r4
	ldrb	sl, [r2, #4]	@ zero_extendqisi2
	ldr	r8, [fp, r8, asl #2]
	mov	sl, sl, asl #1
	ldrh	r8, [sl, r8]
	add	r4, r4, #32768
	mla	r4, r0, r8, r4
	add	sl, r4, r4, lsr #16
	mov	r0, sl, lsr #16
	mov	sl, r0, asl #16
	mov	r4, sl, lsr #16
	and	r8, r4, #255
	mov	r4, r8, asr r3
	mov	r8, sl, lsr #24
	ldr	sl, [sp, #84]
	ldr	r4, [sl, r4, asl #2]
	mov	r0, r8, asl #1
	ldrh	r8, [r0, r4]
	mov	r4, r8, lsr #8
	strb	r4, [r7, #-2]
	strb	r8, [r7, #-1]
	b	.L2055
.L1939:
	ldrb	sl, [r4, #9]	@ zero_extendqisi2
	sub	sl, sl, #1
	cmp	sl, #15
	ldrls	pc, [pc, sl, asl #2]
	b	.L1938
.L1949:
	.word	.L1944
	.word	.L1945
	.word	.L1938
	.word	.L1946
	.word	.L1938
	.word	.L1938
	.word	.L1938
	.word	.L1947
	.word	.L1938
	.word	.L1938
	.word	.L1938
	.word	.L1938
	.word	.L1938
	.word	.L1938
	.word	.L1938
	.word	.L1948
.L1940:
	ldrb	sl, [r4, #9]	@ zero_extendqisi2
	cmp	sl, #8
	beq	.L2585
	cmp	r1, #0
	beq	.L2586
	cmp	r6, #0
	beq	.L1938
	add	r7, r7, #6
	mov	r0, #0
	str	r6, [sp, #20]
	str	r4, [sp, #12]
	str	r5, [sp, #8]
	b	.L2006
.L2004:
	mov	r4, r4, asr r3
	mov	r6, r6, asr r3
	mov	sl, sl, asr r3
	ldr	r4, [r1, r4, asl #2]
	mov	ip, ip, asl #1
	ldrh	ip, [ip, r4]
	mov	r4, ip, lsr #8
	strb	r4, [r7, #-6]
	strb	ip, [r7, #-5]
	ldr	ip, [r1, r6, asl #2]
	mov	r5, r5, asl #1
	ldrh	ip, [r5, ip]
	mov	r4, ip, lsr #8
	strb	r4, [r7, #-4]
	strb	ip, [r7, #-3]
	ldr	r6, [r1, sl, asl #2]
	mov	r8, r8, asl #1
	ldrh	ip, [r8, r6]
	mov	r4, ip, lsr #8
	strb	r4, [r7, #-2]
	strb	ip, [r7, #-1]
.L2005:
	ldr	r8, [sp, #20]
	add	r0, r0, #1
	cmp	r8, r0
	add	r7, r7, #6
	bls	.L2587
.L2006:
	ldrb	ip, [r7, #-6]	@ zero_extendqisi2
	ldrb	r4, [r7, #-5]	@ zero_extendqisi2
	ldrh	r5, [r2, #2]
	add	sl, r4, ip, asl #8
	mov	r6, sl, asl #16
	cmp	r5, r6, lsr #16
	ldrb	r8, [r7, #-2]	@ zero_extendqisi2
	ldrb	r5, [r7, #-4]	@ zero_extendqisi2
	ldrb	r6, [r7, #-3]	@ zero_extendqisi2
	ldrb	sl, [r7, #-1]	@ zero_extendqisi2
	bne	.L2004
	ldrh	r9, [r2, #4]
	add	fp, r6, r5, asl #8
	str	r9, [sp, #4]
	mov	r9, fp, asl #16
	ldr	fp, [sp, #4]
	cmp	fp, r9, lsr #16
	bne	.L2004
	ldrh	r9, [r2, #6]
	add	fp, sl, r8, asl #8
	str	r9, [sp, #4]
	mov	r9, fp, asl #16
	ldr	fp, [sp, #4]
	cmp	fp, r9, lsr #16
	bne	.L2004
	ldr	r4, [sp, #8]
	ldrh	r6, [r4, #2]
	mov	ip, r6, lsr #8
	strb	ip, [r7, #-6]
	ldrh	r8, [r4, #2]
	strb	r8, [r7, #-5]
	ldrh	r6, [r4, #4]
	mov	ip, r6, lsr #8
	strb	ip, [r7, #-4]
	ldrh	r8, [r4, #4]
	strb	r8, [r7, #-3]
	ldrh	r6, [r4, #6]
	mov	ip, r6, lsr #8
	strb	ip, [r7, #-2]
	ldrh	r8, [r4, #6]
	strb	r8, [r7, #-1]
	b	.L2005
.L1941:
	ldrb	r2, [r4, #9]	@ zero_extendqisi2
	cmp	r2, #8
	beq	.L2588
	ldr	ip, [sp, #84]
	cmp	ip, #0
	cmpne	r1, #0
	beq	.L2026
	ldr	r2, [sp, #88]
	cmp	r2, #0
	beq	.L2026
	cmp	r6, #0
	beq	.L1938
	mov	sl, #65536
	tst	r6, #1
	sub	r8, sl, #1
	add	r0, r7, #2
	mov	r2, r7
	mov	sl, #0
	beq	.L2403
	str	r4, [sp, #4]
	b	.L2031
.L2592:
	ldrh	ip, [r5, #8]
	mov	r4, ip, lsr #8
	strb	r4, [r0, #-2]
	ldrh	ip, [r5, #8]
	strb	ip, [r0, #-1]
.L2028:
	add	sl, sl, #1
	cmp	r6, sl
	add	r0, r0, #2
	bls	.L2589
	add	ip, r2, #4
	ldrb	r7, [ip, #2]	@ zero_extendqisi2
	ldrb	r4, [ip, #3]	@ zero_extendqisi2
	add	r4, r4, r7, asl #8
	mov	r4, r4, asl #16
	mov	r4, r4, lsr #16
	cmp	r4, r8
	beq	.L2590
	cmp	r4, #0
	beq	.L2537
	ldrb	r7, [ip, #1]	@ zero_extendqisi2
	mov	r7, r7, asr r3
	ldr	r2, [sp, #64]
	rsb	fp, r4, #65280
	ldrh	r9, [r2, #8]
	add	r2, fp, #255
	mul	r2, r9, r2
	ldr	fp, [sp, #88]
	ldrb	r9, [ip, #0]	@ zero_extendqisi2
	ldr	r7, [fp, r7, asl #2]
	mov	r9, r9, asl #1
	ldrh	r7, [r9, r7]
	add	r2, r2, #32768
	mla	r2, r4, r7, r2
	add	r7, r2, r2, lsr #16
	mov	r2, r7, lsr #16
	mov	r7, r2, asl #16
	mov	r4, r7, lsr #16
	and	r4, r4, #255
	mov	r4, r4, asr r3
	mov	r2, r7, lsr #24
	ldr	r7, [sp, #84]
	ldr	r4, [r7, r4, asl #2]
.L2578:
	mov	r2, r2, asl #1
	ldrh	r2, [r2, r4]
	mov	r4, r2, lsr #8
	strb	r4, [r0, #-2]
	strb	r2, [r0, #-1]
.L2538:
	add	sl, sl, #1
	add	r0, r0, #2
	add	r2, ip, #4
.L2031:
	ldrb	r4, [r2, #2]	@ zero_extendqisi2
	ldrb	ip, [r2, #3]	@ zero_extendqisi2
	add	ip, ip, r4, asl #8
	mov	r4, ip, asl #16
	mov	ip, r4, lsr #16
	cmp	ip, r8
	beq	.L2591
	cmp	ip, #0
	beq	.L2592
	ldrb	r7, [r2, #1]	@ zero_extendqisi2
	mov	r7, r7, asr r3
	ldr	r9, [sp, #64]
	rsb	r4, ip, #65280
	ldrh	fp, [r9, #8]
	add	r4, r4, #255
	mul	r4, fp, r4
	ldr	fp, [sp, #88]
	ldrb	r9, [r2, #0]	@ zero_extendqisi2
	ldr	r7, [fp, r7, asl #2]
	mov	r9, r9, asl #1
	ldrh	r7, [r9, r7]
	add	r4, r4, #32768
	mla	r4, ip, r7, r4
	add	r7, r4, r4, lsr #16
	mov	ip, r7, lsr #16
	mov	r7, ip, asl #16
	mov	r4, r7, lsr #16
	and	r4, r4, #255
	mov	r4, r4, asr r3
	mov	ip, r7, lsr #24
	ldr	r7, [sp, #84]
	ldr	r4, [r7, r4, asl #2]
	mov	ip, ip, asl #1
	ldrh	ip, [ip, r4]
	mov	r4, ip, lsr #8
	strb	r4, [r0, #-2]
	strb	ip, [r0, #-1]
	b	.L2028
.L2026:
	cmp	r6, #0
	beq	.L1938
	mov	r1, #65536
	tst	r6, #1
	sub	r9, r1, #1
	mov	sl, r7
	mov	r8, r7
	mov	fp, #0
	bne	.L2593
	ldrb	r3, [r7, #3]	@ zero_extendqisi2
	ldrb	r8, [r7, #2]	@ zero_extendqisi2
	add	r0, r3, r8, asl #8
	mov	r1, r0, asl #16
	mov	r3, r1, lsr #16
	cmp	r3, r9
	beq	.L2539
	cmp	r3, #0
	beq	.L2540
	ldrb	r1, [r7, #0]	@ zero_extendqisi2
	ldrb	ip, [r7, #1]	@ zero_extendqisi2
	add	r0, ip, r1, asl #8
	mov	r2, r0, asl #16
	mov	sl, r2, lsr #16
	mul	sl, r3, sl
	ldr	fp, [sp, #64]
	rsb	r3, r3, #65280
	ldrh	ip, [fp, #8]
	add	r8, sl, #32768
	add	r0, r3, #255
	mla	r0, ip, r0, r8
	add	r1, r0, r0, lsr #16
	mov	r2, r1, lsr #16
	mov	fp, r2, asl #16
	mov	sl, fp, lsr #24
	mov	ip, fp, lsr #16
	strb	sl, [r7, #0]
	strb	ip, [r7, #1]
.L2541:
	add	sl, r7, #2
	add	r8, r7, #4
	mov	fp, #1
	ldr	r7, [sp, #64]
	b	.L2036
.L2596:
	ldrh	r0, [r5, #8]
	mov	r3, r0, lsr #8
	strb	r3, [sl, #0]
	ldrh	r1, [r5, #8]
	strb	r1, [sl, #1]
.L2033:
	add	fp, fp, #1
	cmp	r6, fp
	bls	.L2577
	add	r8, r8, #4
	ldrb	r2, [r8, #2]	@ zero_extendqisi2
	ldrb	r0, [r8, #3]	@ zero_extendqisi2
	add	ip, r0, r2, asl #8
	mov	r3, ip, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, r9
	add	sl, sl, #2
	beq	.L2594
	cmp	r3, #0
	beq	.L2542
	ldrb	r1, [r8, #0]	@ zero_extendqisi2
	ldrb	r0, [r8, #1]	@ zero_extendqisi2
	add	r2, r0, r1, asl #8
	mov	ip, r2, asl #16
	mov	r0, ip, lsr #16
	mul	r0, r3, r0
	ldrh	ip, [r7, #8]
	rsb	r1, r3, #65280
	add	r2, r0, #32768
	add	r0, r1, #255
	mla	r0, ip, r0, r2
	add	r3, r0, r0, lsr #16
	mov	r1, r3, lsr #16
	mov	ip, r1, asl #16
	mov	r2, ip, lsr #24
	mov	r0, ip, lsr #16
	strb	r2, [sl, #0]
	strb	r0, [sl, #1]
.L2543:
	add	fp, fp, #1
	add	r8, r8, #4
	add	sl, sl, #2
.L2036:
	ldrb	r2, [r8, #2]	@ zero_extendqisi2
	ldrb	ip, [r8, #3]	@ zero_extendqisi2
	add	r0, ip, r2, asl #8
	mov	r3, r0, asl #16
	mov	r3, r3, lsr #16
	cmp	r3, r9
	beq	.L2595
	cmp	r3, #0
	beq	.L2596
	ldrb	r1, [r8, #0]	@ zero_extendqisi2
	ldrb	r0, [r8, #1]	@ zero_extendqisi2
	add	r2, r0, r1, asl #8
	mov	ip, r2, asl #16
	mov	r0, ip, lsr #16
	mul	r0, r3, r0
	ldrh	r1, [r7, #8]
	rsb	r3, r3, #65280
	add	r2, r0, #32768
	add	ip, r3, #255
	mla	ip, r1, ip, r2
	add	r0, ip, ip, lsr #16
	mov	r3, r0, lsr #16
	mov	r1, r3, asl #16
	mov	r2, r1, lsr #24
	mov	ip, r1, lsr #16
	strb	r2, [sl, #0]
	strb	ip, [sl, #1]
	b	.L2033
.L2053:
	cmp	r6, #0
	beq	.L1938
	mov	r3, #65536
	sub	r9, r3, #1
	mov	r8, r7
	mov	sl, #0
	str	r4, [sp, #4]
	b	.L2063
.L2598:
	ldrh	r1, [r5, #2]
	mov	r2, r1, lsr #8
	strb	r2, [r7, #0]
	ldrh	r3, [r5, #2]
	strb	r3, [r7, #1]
	ldrh	r4, [r5, #4]
	mov	r0, r4, lsr #8
	strb	r0, [r7, #2]
	ldrh	ip, [r5, #4]
	strb	ip, [r7, #3]
	ldrh	r1, [r5, #6]
	mov	r2, r1, lsr #8
	strb	r2, [r7, #4]
	ldrh	r3, [r5, #6]
	strb	r3, [r7, #5]
.L2060:
	add	sl, sl, #1
	cmp	r6, sl
	bls	.L2062
	add	r8, r8, #8
	add	r7, r7, #6
.L2063:
	ldrb	r4, [r8, #6]	@ zero_extendqisi2
	ldrb	r0, [r8, #7]	@ zero_extendqisi2
	add	ip, r0, r4, asl #8
	mov	r1, ip, asl #16
	mov	r3, r1, lsr #16
	rsb	r2, r3, #65280
	cmp	r3, r9
	add	r2, r2, #255
	beq	.L2597
	cmp	r3, #0
	beq	.L2598
	ldrb	fp, [r8, #0]	@ zero_extendqisi2
	ldrb	r1, [r8, #1]	@ zero_extendqisi2
	add	r0, r1, fp, asl #8
	mov	r4, r0, asl #16
	mov	ip, r4, lsr #16
	mul	ip, r3, ip
	ldrh	fp, [r5, #2]
	add	r4, ip, #32768
	mla	r4, fp, r2, r4
	ldrb	r0, [r8, #2]	@ zero_extendqisi2
	ldrb	r1, [r8, #3]	@ zero_extendqisi2
	add	fp, r1, r0, asl #8
	mov	r0, fp, asl #16
	mov	r0, r0, lsr #16
	mul	r0, r3, r0
	add	ip, r4, r4, lsr #16
	mov	r1, ip, lsr #16
	mov	fp, r1, asl #16
	mov	r4, fp, lsr #24
	mov	ip, fp, lsr #16
	ldrb	r1, [r8, #4]	@ zero_extendqisi2
	ldrb	fp, [r8, #5]	@ zero_extendqisi2
	strb	r4, [r7, #0]
	strb	ip, [r7, #1]
	ldrh	ip, [r5, #4]
	add	r4, r0, #32768
	mla	r4, ip, r2, r4
	add	r1, fp, r1, asl #8
	mov	ip, r1, asl #16
	mov	r1, ip, lsr #16
	mul	r3, r1, r3
	add	r0, r4, r4, lsr #16
	mov	ip, r0, lsr #16
	mov	r1, ip, asl #16
	mov	r4, r1, lsr #24
	mov	r0, r1, lsr #16
	strb	r4, [r7, #2]
	strb	r0, [r7, #3]
	ldrh	r1, [r5, #6]
	add	ip, r3, #32768
	mla	r2, r1, r2, ip
	add	r2, r2, r2, lsr #16
	mov	r3, r2, lsr #16
	mov	r4, r3, asl #16
	mov	r0, r4, lsr #24
	mov	ip, r4, lsr #16
	strb	r0, [r7, #4]
	strb	ip, [r7, #5]
	b	.L2060
.L2597:
	mov	r0, r7
	mov	r1, r8
	mov	r2, #6
	bl	memcpy
	b	.L2060
.L2587:
	ldr	r4, [sp, #12]
	ldrb	r0, [r4, #8]	@ zero_extendqisi2
	mov	r6, r8
	and	r8, r0, #4
	b	.L1938
.L2593:
	ldr	r7, [sp, #64]
	b	.L2036
.L1944:
	cmp	r6, #0
	beq	.L1938
	ldrb	r3, [r7, #0]	@ zero_extendqisi2
	ldrh	r1, [r2, #8]
	mov	r0, #32512
	sub	ip, r6, #1
	cmp	r1, r3, lsr #7
	add	r0, r0, #127
	and	ip, ip, #3
	beq	.L2599
.L2077:
	mov	r1, #1
	cmp	r6, r1
	mov	r3, #6
	bls	.L2577
	cmp	ip, #0
	beq	.L1953
	cmp	ip, r1
	beq	.L2350
	cmp	ip, #2
	beq	.L2351
	ldrb	ip, [r7, #0]	@ zero_extendqisi2
	mov	sl, ip, asr r3
	ldrh	r8, [r2, #8]
	and	sl, sl, r1
	cmp	sl, r8
	andeq	ip, ip, #191
	streqb	ip, [r7, #0]
	ldreqh	r8, [r5, #8]
	orreq	ip, ip, r8, asl r3
	streqb	ip, [r7, #0]
	cmp	r3, #0
	subne	r3, r3, #1
	addeq	r7, r7, #1
	moveq	r3, #7
	add	r1, r1, #1
.L2351:
	ldrb	ip, [r7, #0]	@ zero_extendqisi2
	mov	sl, ip, asr r3
	ldrh	r8, [r2, #8]
	and	sl, sl, #1
	cmp	sl, r8
	beq	.L2600
.L2084:
	cmp	r3, #0
	subne	r3, r3, #1
	addeq	r7, r7, #1
	moveq	r3, #7
	add	r1, r1, #1
.L2350:
	ldrb	ip, [r7, #0]	@ zero_extendqisi2
	mov	sl, ip, asr r3
	ldrh	r8, [r2, #8]
	and	sl, sl, #1
	cmp	sl, r8
	beq	.L2601
.L2088:
	cmp	r3, #0
	add	r1, r1, #1
	subne	r3, r3, #1
	addeq	r7, r7, #1
	moveq	r3, #7
	cmp	r6, r1
	bhi	.L1953
	b	.L2577
.L1950:
	cmp	r3, #0
	addeq	r7, r7, #1
	ldrb	ip, [r7, #0]	@ zero_extendqisi2
	moveq	r3, #7
	subne	r3, r3, #1
	mov	sl, ip, asr r3
	ldrh	r8, [r2, #8]
	and	sl, sl, #1
	cmp	sl, r8
	add	r1, r1, #1
	beq	.L2602
.L2092:
	cmp	r3, #0
	addeq	r7, r7, #1
	ldrb	ip, [r7, #0]	@ zero_extendqisi2
	subne	r3, r3, #1
	moveq	r3, #7
	mov	sl, ip, asr r3
	ldrh	r8, [r2, #8]
	and	sl, sl, #1
	cmp	sl, r8
	beq	.L2603
.L2095:
	cmp	r3, #0
	addeq	r7, r7, #1
	ldrb	ip, [r7, #0]	@ zero_extendqisi2
	subne	r3, r3, #1
	moveq	r3, #7
	mov	sl, ip, asr r3
	ldrh	r8, [r2, #8]
	and	sl, sl, #1
	cmp	sl, r8
	beq	.L2604
.L2098:
	cmp	r3, #0
	add	r1, r1, #3
	subne	r3, r3, #1
	addeq	r7, r7, #1
	moveq	r3, #7
	cmp	r6, r1
	bls	.L2577
.L1953:
	ldrb	ip, [r7, #0]	@ zero_extendqisi2
	mov	sl, ip, asr r3
	ldrh	r8, [r2, #8]
	and	sl, sl, #1
	cmp	sl, r8
	bne	.L1950
	rsb	r8, r3, #7
	and	sl, ip, r0, asr r8
	strb	sl, [r7, #0]
	ldrh	r8, [r5, #8]
	orr	ip, sl, r8, asl r3
	strb	ip, [r7, #0]
	b	.L1950
.L1945:
	cmp	ip, #0
	beq	.L2605
	cmp	r6, #0
	beq	.L1938
	ldrb	r1, [r7, #0]	@ zero_extendqisi2
	ldrh	r0, [r2, #8]
	mov	r3, r1, asr #6
	cmp	r3, r0
	mov	r8, #16128
	sub	r0, r6, #1
	add	r8, r8, #63
	and	r0, r0, #1
	beq	.L2606
	and	r3, r3, #3
	mov	sl, r3, asl #4
	orr	sl, sl, r3, asl #2
	orr	sl, sl, r3
	orr	r3, sl, r3, asl #6
	ldrb	r3, [ip, r3]	@ zero_extendqisi2
	and	r1, r1, r8
	and	r3, r3, #192
	orr	r3, r3, r1
	strb	r3, [r7, #0]
.L2424:
	mov	r1, #1
	cmp	r6, r1
	mov	r3, #4
	bls	.L2577
	cmp	r0, #0
	beq	.L2557
	ldrb	r0, [r7, #0]	@ zero_extendqisi2
	mov	sl, r0, asr r3
	ldrh	fp, [r2, #8]
	and	r9, sl, #3
	cmp	r9, fp
	beq	.L2426
	mov	sl, r9
	mov	r9, r9, asl r3
	orr	r9, r9, sl, asl #2
	orr	r9, r9, sl
	orr	sl, r9, sl, asl #6
	ldrb	sl, [ip, sl]	@ zero_extendqisi2
	and	r0, r0, r8, lsr #2
	mov	sl, sl, lsr #6
	orr	r0, r0, sl, asl r3
	strb	r0, [r7, #0]
.L2427:
	cmp	r3, #0
	add	r1, r1, #1
	subne	r3, r3, #2
	addeq	r7, r7, #1
	moveq	r3, #6
	cmp	r6, r1
	bls	.L2577
.L2557:
	mov	fp, r4
	b	.L1960
.L1956:
	rsb	sl, r3, #6
	and	r9, r0, r8, asr sl
	and	r4, r4, #3
	mov	sl, r4, asl #4
	orr	sl, sl, r4, asl #2
	orr	sl, sl, r4
	orr	r4, sl, r4, asl #6
	ldrb	sl, [ip, r4]	@ zero_extendqisi2
	mov	r4, sl, lsr #6
	orr	r0, r9, r4, asl r3
	strb	r0, [r7, #0]
.L1957:
	cmp	r3, #0
	addeq	r7, r7, #1
	ldrb	r0, [r7, #0]	@ zero_extendqisi2
	moveq	r3, #6
	subne	r3, r3, #2
	mov	r4, r0, asr r3
	ldrh	sl, [r2, #8]
	and	r9, r4, #3
	cmp	r9, sl
	add	r1, r1, #1
	beq	.L2607
	rsb	sl, r3, #6
	and	r0, r0, r8, asr sl
	and	r4, r4, #3
	mov	sl, r4, asl #4
	orr	sl, sl, r4, asl #2
	orr	sl, sl, r4
	orr	r4, sl, r4, asl #6
	ldrb	r4, [ip, r4]	@ zero_extendqisi2
	mov	r4, r4, lsr #6
	orr	r0, r0, r4, asl r3
	strb	r0, [r7, #0]
.L2430:
	cmp	r3, #0
	add	r1, r1, #1
	subne	r3, r3, #2
	addeq	r7, r7, #1
	moveq	r3, #6
	cmp	r6, r1
	bls	.L2579
.L1960:
	ldrb	r0, [r7, #0]	@ zero_extendqisi2
	mov	r4, r0, asr r3
	ldrh	sl, [r2, #8]
	and	r9, r4, #3
	cmp	r9, sl
	bne	.L1956
	rsb	sl, r3, #6
	and	r9, r0, r8, asr sl
	strb	r9, [r7, #0]
	ldrh	r4, [r5, #8]
	orr	r0, r9, r4, asl r3
	strb	r0, [r7, #0]
	b	.L1957
.L1948:
	cmp	r1, #0
	beq	.L2608
	cmp	r6, #0
	beq	.L1938
	ldrb	r0, [r7], #2	@ zero_extendqisi2
	ldrb	ip, [r7, #-1]	@ zero_extendqisi2
	ldrh	r8, [r2, #8]
	add	sl, ip, r0, asl #8
	mov	sl, sl, asl #16
	cmp	r8, sl, lsr #16
	sub	r8, r6, #1
	and	r8, r8, #1
	beq	.L2609
	mov	ip, ip, asr r3
	ldr	ip, [r1, ip, asl #2]
	add	r0, ip, r0, asl #1
	ldrh	r0, [r0, #0]
	mov	ip, r0, lsr #8
	strb	ip, [r7, #-2]
	strb	r0, [r7, #-1]
.L2482:
	mov	r0, #1
	cmp	r6, r0
	add	r7, r7, #2
	bls	.L2577
	cmp	r8, #0
	beq	.L1987
	ldrb	ip, [r7, #-2]	@ zero_extendqisi2
	ldrb	r8, [r7, #-1]	@ zero_extendqisi2
	ldrh	sl, [r2, #8]
	add	r9, r8, ip, asl #8
	mov	r9, r9, asl #16
	cmp	sl, r9, lsr #16
	beq	.L2390
	mov	r8, r8, asr r3
	ldr	r8, [r1, r8, asl #2]
	add	ip, r8, ip, asl #1
	ldrh	ip, [ip, #0]
	mov	r8, ip, lsr #8
	strb	r8, [r7, #-2]
	strb	ip, [r7, #-1]
.L2486:
	add	r0, r0, #1
	cmp	r6, r0
	add	r7, r7, #2
	bls	.L2577
.L1987:
	ldrb	ip, [r7, #-2]	@ zero_extendqisi2
	ldrb	r8, [r7, #-1]	@ zero_extendqisi2
	ldrh	sl, [r2, #8]
	add	r9, r8, ip, asl #8
	mov	r9, r9, asl #16
	cmp	sl, r9, lsr #16
	movne	r8, r8, asr r3
	ldreqh	ip, [r5, #8]
	ldrne	r8, [r1, r8, asl #2]
	moveq	ip, ip, lsr #8
	movne	ip, ip, asl #1
	streqb	ip, [r7, #-2]
	ldrneh	ip, [ip, r8]
	ldreqh	ip, [r5, #8]
	movne	r8, ip, lsr #8
	strneb	r8, [r7, #-2]
	strb	ip, [r7, #-1]
	ldrb	ip, [r7], #2	@ zero_extendqisi2
	ldrb	r8, [r7, #-1]	@ zero_extendqisi2
	ldrh	sl, [r2, #8]
	add	r9, r8, ip, asl #8
	mov	r9, r9, asl #16
	cmp	sl, r9, lsr #16
	add	r0, r0, #1
	beq	.L2390
	mov	r8, r8, asr r3
	ldr	r8, [r1, r8, asl #2]
	mov	ip, ip, asl #1
	ldrh	ip, [ip, r8]
	add	r0, r0, #1
	mov	r8, ip, lsr #8
	cmp	r6, r0
	strb	r8, [r7, #-2]
	strb	ip, [r7, #-1]
	add	r7, r7, #2
	bhi	.L1987
	b	.L2577
.L1947:
	cmp	ip, #0
	beq	.L2610
	cmp	r6, #0
	beq	.L1938
	ldrh	r1, [r2, #8]
	ldrb	r3, [r7, #0]	@ zero_extendqisi2
	sub	r0, r6, #1
	cmp	r1, r3
	and	r1, r0, #3
	beq	.L2611
	ldrb	r0, [ip, r3]	@ zero_extendqisi2
	strb	r0, [r7, #0]
.L2468:
	mov	r3, #1
	cmp	r6, r3
	bls	.L2577
	cmp	r1, #0
	beq	.L1980
	cmp	r1, r3
	beq	.L2361
	cmp	r1, #2
	beq	.L2362
	ldrb	r1, [r7, r3]	@ zero_extendqisi2
	ldrh	r8, [r2, #8]
	cmp	r8, r1
	ldrneb	r1, [ip, r1]	@ zero_extendqisi2
	ldreqh	r1, [r5, #8]
	strb	r1, [r7, r3]
	add	r3, r3, #1
.L2362:
	ldrb	r1, [r7, r3]	@ zero_extendqisi2
	ldrh	r0, [r2, #8]
	cmp	r0, r1
	ldrneb	r1, [ip, r1]	@ zero_extendqisi2
	ldreqh	r1, [r5, #8]
	strb	r1, [r7, r3]
	add	r3, r3, #1
.L2361:
	ldrb	r8, [r7, r3]	@ zero_extendqisi2
	ldrh	r1, [r2, #8]
	cmp	r1, r8
	ldrneb	r8, [ip, r8]	@ zero_extendqisi2
	ldreqh	r8, [r5, #8]
	strb	r8, [r7, r3]
	add	r3, r3, #1
	cmp	r6, r3
	bhi	.L1980
	b	.L2577
.L2180:
	ldrb	r0, [ip, r1]	@ zero_extendqisi2
	strb	r0, [r7, r3]
.L2476:
	add	r1, r3, #1
	ldrb	r0, [r7, r1]	@ zero_extendqisi2
	ldrh	r8, [r2, #8]
	cmp	r8, r0
	ldrneb	r0, [ip, r0]	@ zero_extendqisi2
	ldreqh	r0, [r5, #8]
	strb	r0, [r7, r1]
	add	r1, r3, #2
	ldrb	r0, [r7, r1]	@ zero_extendqisi2
	ldrh	r8, [r2, #8]
	cmp	r8, r0
	ldrneb	r0, [ip, r0]	@ zero_extendqisi2
	ldreqh	r0, [r5, #8]
	add	r3, r3, #3
	cmp	r6, r3
	strb	r0, [r7, r1]
	bls	.L2577
.L1980:
	ldrb	r1, [r7, r3]	@ zero_extendqisi2
	ldrh	r0, [r2, #8]
	cmp	r0, r1
	ldreqh	r1, [r5, #8]
	ldrneb	r1, [ip, r1]	@ zero_extendqisi2
	strb	r1, [r7, r3]
	add	r3, r3, #1
	ldrb	r1, [r7, r3]	@ zero_extendqisi2
	ldrh	r8, [r2, #8]
	cmp	r8, r1
	bne	.L2180
	ldrh	r8, [r5, #8]
	strb	r8, [r7, r3]
	b	.L2476
.L1946:
	cmp	ip, #0
	beq	.L2612
	cmp	r6, #0
	beq	.L1938
	ldrb	r1, [r7, #0]	@ zero_extendqisi2
	ldrh	r0, [r2, #8]
	mov	r3, r1, asr #4
	cmp	r3, r0
	mov	r8, #3840
	sub	r0, r6, #1
	add	r8, r8, #15
	and	r0, r0, #1
	beq	.L2613
	orr	r3, r3, r3, asl #4
	ldrb	r3, [ip, r3]	@ zero_extendqisi2
	and	r1, r1, r8
	and	r3, r3, #240
	orr	r3, r3, r1
	strb	r3, [r7, #0]
.L2446:
	mov	r1, #1
	cmp	r6, r1
	mov	r3, #0
	bls	.L2577
	cmp	r0, r3
	beq	.L2561
	ldrb	r0, [r7, #0]	@ zero_extendqisi2
	ldrh	sl, [r2, #8]
	and	r9, r0, #15
	cmp	r9, sl
	andeq	r0, r0, #240
	streqb	r0, [r7, #0]
	movne	sl, r9
	orrne	sl, sl, sl, asl #4
	ldrneb	sl, [ip, sl]	@ zero_extendqisi2
	ldreqh	sl, [r5, #8]
	andne	r0, r0, #240
	orrne	r0, r0, sl, lsr #4
	orreq	r0, r0, sl
	add	r1, r1, #1
	cmp	r3, #0
	strb	r0, [r7, #0]
	subne	r3, r3, #4
	addeq	r7, r7, #1
	moveq	r3, #4
	cmp	r6, r1
	bls	.L2577
.L2561:
	mov	fp, r4
	b	.L1971
.L2142:
	rsb	sl, r3, #4
	and	r0, r0, r8, asr sl
	and	r4, r4, #15
	orr	r4, r4, r4, asl #4
	ldrb	r4, [ip, r4]	@ zero_extendqisi2
	mov	r4, r4, lsr #4
	orr	r0, r0, r4, asl r3
	strb	r0, [r7, #0]
.L2452:
	cmp	r3, #0
	add	r1, r1, #1
	subne	r3, r3, #4
	addeq	r7, r7, #1
	moveq	r3, #4
	cmp	r6, r1
	bls	.L2579
.L1971:
	ldrb	r0, [r7, #0]	@ zero_extendqisi2
	mov	r4, r0, asr r3
	ldrh	sl, [r2, #8]
	and	r9, r4, #15
	cmp	r9, sl
	rsbeq	r4, r3, #4
	andeq	r0, r0, r8, asr r4
	rsbne	sl, r3, #4
	andne	r0, r0, r8, asr sl
	andne	r4, r4, #15
	streqb	r0, [r7, #0]
	orrne	r4, r4, r4, asl #4
	ldrneb	r4, [ip, r4]	@ zero_extendqisi2
	ldreqh	r4, [r5, #8]
	movne	r4, r4, lsr #4
	orr	r9, r0, r4, asl r3
	cmp	r3, #0
	strb	r9, [r7, #0]
	addeq	r7, r7, #1
	ldrb	r0, [r7, #0]	@ zero_extendqisi2
	moveq	r3, #4
	subne	r3, r3, #4
	mov	r4, r0, asr r3
	ldrh	sl, [r2, #8]
	and	r9, r4, #15
	cmp	r9, sl
	add	r1, r1, #1
	bne	.L2142
	rsb	r4, r3, #4
	and	r0, r0, r8, asr r4
	strb	r0, [r7, #0]
	ldrh	r4, [r5, #8]
	orr	r0, r0, r4, asl r3
	strb	r0, [r7, #0]
	b	.L2452
.L2390:
	ldrh	ip, [r5, #8]
	mov	r8, ip, lsr #8
	strb	r8, [r7, #-2]
	ldrh	ip, [r5, #8]
	strb	ip, [r7, #-1]
	b	.L2486
.L2539:
	mov	r0, r7
	mov	r1, r7
	mov	r2, #2
	bl	memcpy
	b	.L2541
.L2583:
	ldrb	r4, [r2, #1]	@ zero_extendqisi2
	mov	r0, r4, asr r3
	ldrb	r8, [r2, #0]	@ zero_extendqisi2
	ldr	r0, [r1, r0, asl #2]
	mov	r4, r8, asl #1
	ldrh	r8, [r4, r0]
	mov	r4, r8, lsr #8
	strb	r4, [r7, #-6]
	strb	r8, [r7, #-5]
	ldrb	r4, [r2, #3]	@ zero_extendqisi2
	mov	r0, r4, asr r3
	ldrb	r8, [r2, #2]	@ zero_extendqisi2
	ldr	r0, [r1, r0, asl #2]
	mov	r4, r8, asl #1
	ldrh	r8, [r4, r0]
	mov	r4, r8, lsr #8
	strb	r4, [r7, #-4]
	strb	r8, [r7, #-3]
	ldrb	r4, [r2, #5]	@ zero_extendqisi2
	mov	r0, r4, asr r3
	ldrb	r8, [r2, #4]	@ zero_extendqisi2
	ldr	r0, [r1, r0, asl #2]
	mov	r4, r8, asl #1
	ldrh	r8, [r4, r0]
	mov	r4, r8, lsr #8
	strb	r4, [r7, #-2]
	strb	r8, [r7, #-1]
	b	.L2055
.L2542:
	ldrh	r2, [r5, #8]
	mov	ip, r2, lsr #8
	strb	ip, [sl, #0]
	ldrh	r3, [r5, #8]
	strb	r3, [sl, #1]
	b	.L2543
.L2537:
	ldrh	r4, [r5, #8]
	mov	r2, r4, lsr #8
	strb	r2, [r0, #-2]
	ldrh	r4, [r5, #8]
	strb	r4, [r0, #-1]
	b	.L2538
.L2591:
	ldrb	r4, [r2, #1]	@ zero_extendqisi2
	mov	ip, r4, asr r3
	ldrb	r4, [r2, #0]	@ zero_extendqisi2
	ldr	ip, [r1, ip, asl #2]
	mov	r4, r4, asl #1
	ldrh	ip, [r4, ip]
	mov	r4, ip, lsr #8
	strb	r4, [r0, #-2]
	strb	ip, [r0, #-1]
	b	.L2028
.L2590:
	ldrb	r4, [ip, #1]	@ zero_extendqisi2
	mov	r4, r4, asr r3
	ldrb	r2, [r2, #4]	@ zero_extendqisi2
	ldr	r4, [r1, r4, asl #2]
	b	.L2578
.L2595:
	mov	r0, sl
	mov	r1, r8
	mov	r2, #2
	bl	memcpy
	b	.L2033
.L2581:
	cmp	r9, #0
	cmpne	sl, #0
	beq	.L2040
	cmp	ip, #0
	beq	.L2040
	cmp	r6, #0
	beq	.L1938
	tst	r6, #1
	add	r3, r7, #3
	mov	r2, r7
	mov	r0, #0
	beq	.L2407
	str	r4, [sp, #8]
	ldr	fp, [sp, #64]
	b	.L2045
.L2617:
	ldrh	r1, [r5, #2]
	strb	r1, [r3, #-3]
	ldrh	r4, [r5, #4]
	strb	r4, [r3, #-2]
	ldrh	r8, [r5, #6]
	strb	r8, [r3, #-1]
.L2042:
	add	r0, r0, #1
	cmp	r6, r0
	add	r3, r3, #3
	bls	.L2614
	add	r1, r2, #4
	ldrb	r4, [r1, #3]	@ zero_extendqisi2
	cmp	r4, #255
	beq	.L2615
	cmp	r4, #0
	beq	.L2547
	rsb	r7, r4, #255
	mov	r2, r7, asl #16
	ldrh	r7, [fp, #2]
	mov	r2, r2, lsr #16
	mul	r7, r2, r7
	ldrb	r8, [r1, #0]	@ zero_extendqisi2
	ldrb	r8, [r9, r8]	@ zero_extendqisi2
	add	r7, r7, #128
	mla	r7, r8, r4, r7
	mov	r7, r7, asl #16
	mov	r8, r7, lsr #24
	add	r7, r8, r7, lsr #16
	mov	r7, r7, asr #8
	and	r8, r7, #255
	ldrb	r7, [sl, r8]	@ zero_extendqisi2
	strb	r7, [r3, #-3]
	ldrh	r7, [fp, #4]
	mul	r7, r2, r7
	ldrb	r8, [r1, #1]	@ zero_extendqisi2
	ldrb	r8, [r9, r8]	@ zero_extendqisi2
	add	r7, r7, #128
	mla	r7, r8, r4, r7
	mov	r7, r7, asl #16
	mov	r8, r7, lsr #24
	add	r7, r8, r7, lsr #16
	mov	r7, r7, asr #8
	and	r8, r7, #255
	ldrb	r7, [sl, r8]	@ zero_extendqisi2
	strb	r7, [r3, #-2]
	ldrh	r8, [fp, #6]
	mul	r2, r8, r2
	ldrb	r7, [r1, #2]	@ zero_extendqisi2
	ldrb	r7, [r9, r7]	@ zero_extendqisi2
	add	r2, r2, #128
	mla	r2, r7, r4, r2
	mov	r8, r2, asl #16
	mov	r4, r8, lsr #24
	add	r2, r4, r8, lsr #16
	mov	r8, r2, asr #8
	and	r2, r8, #255
	ldrb	r4, [sl, r2]	@ zero_extendqisi2
	strb	r4, [r3, #-1]
.L2548:
	add	r0, r0, #1
	add	r3, r3, #3
	add	r2, r1, #4
.L2045:
	ldrb	r1, [r2, #3]	@ zero_extendqisi2
	cmp	r1, #255
	beq	.L2616
	cmp	r1, #0
	beq	.L2617
	rsb	r7, r1, #255
	mov	r4, r7, asl #16
	ldrh	r7, [fp, #2]
	mov	r4, r4, lsr #16
	mul	r7, r4, r7
	ldrb	r8, [r2, #0]	@ zero_extendqisi2
	ldrb	r8, [r9, r8]	@ zero_extendqisi2
	add	r7, r7, #128
	mla	r7, r8, r1, r7
	mov	r7, r7, asl #16
	mov	r8, r7, lsr #24
	add	r7, r8, r7, lsr #16
	mov	r7, r7, asr #8
	and	r8, r7, #255
	ldrb	r7, [sl, r8]	@ zero_extendqisi2
	strb	r7, [r3, #-3]
	ldrh	r7, [fp, #4]
	mul	r7, r4, r7
	ldrb	r8, [r2, #1]	@ zero_extendqisi2
	ldrb	r8, [r9, r8]	@ zero_extendqisi2
	add	r7, r7, #128
	mla	r7, r8, r1, r7
	mov	r7, r7, asl #16
	mov	r8, r7, lsr #24
	add	r7, r8, r7, lsr #16
	mov	r7, r7, asr #8
	and	r8, r7, #255
	ldrb	r7, [sl, r8]	@ zero_extendqisi2
	strb	r7, [r3, #-2]
	ldrh	r8, [fp, #6]
	mul	r4, r8, r4
	ldrb	r7, [r2, #2]	@ zero_extendqisi2
	ldrb	r7, [r9, r7]	@ zero_extendqisi2
	add	r8, r4, #128
	mla	r1, r7, r1, r8
	mov	r1, r1, asl #16
	mov	r4, r1, lsr #24
	add	r8, r4, r1, lsr #16
	mov	r1, r8, asr #8
	and	r4, r1, #255
	ldrb	r8, [sl, r4]	@ zero_extendqisi2
	strb	r8, [r3, #-1]
	b	.L2042
.L2585:
	cmp	ip, #0
	beq	.L2618
	cmp	r6, #0
	beq	.L1938
	mov	r3, r7
	ldrb	r0, [r3], #2	@ zero_extendqisi2
	ldrh	r8, [r2, #2]
	sub	r1, r6, #1
	cmp	r8, r0
	and	r1, r1, #3
	add	r7, r7, #1
	beq	.L2619
.L2225:
	ldrb	r0, [ip, r0]	@ zero_extendqisi2
	strb	r0, [r3, #-2]
	ldrb	r8, [r7, #0]	@ zero_extendqisi2
	ldrb	r0, [ip, r8]	@ zero_extendqisi2
	strb	r0, [r7, #0]
	ldrb	r8, [r3, #0]	@ zero_extendqisi2
	ldrb	r0, [ip, r8]	@ zero_extendqisi2
	strb	r0, [r3, #0]
.L2487:
	mov	r8, #1
	cmp	r6, r8
	add	r7, r7, #3
	add	r3, r3, #3
	bls	.L2577
	cmp	r1, #0
	beq	.L1998
	cmp	r1, r8
	beq	.L2369
	cmp	r1, #2
	beq	.L2370
	ldrb	r1, [r3, #-2]	@ zero_extendqisi2
	ldrh	r0, [r2, #2]
	cmp	r0, r1
	beq	.L2489
.L2228:
	ldrb	r1, [ip, r1]	@ zero_extendqisi2
	strb	r1, [r3, #-2]
	ldrb	r0, [r7, #0]	@ zero_extendqisi2
	ldrb	r1, [ip, r0]	@ zero_extendqisi2
	strb	r1, [r7, #0]
	ldrb	r0, [r3, #0]	@ zero_extendqisi2
	ldrb	r1, [ip, r0]	@ zero_extendqisi2
	strb	r1, [r3, #0]
.L2490:
	add	r8, r8, #1
	add	r7, r7, #3
	add	r3, r3, #3
.L2370:
	ldrb	r1, [r3, #-2]	@ zero_extendqisi2
	ldrh	r0, [r2, #2]
	cmp	r0, r1
	beq	.L2491
.L2232:
	ldrb	r1, [ip, r1]	@ zero_extendqisi2
	strb	r1, [r3, #-2]
	ldrb	r0, [r7, #0]	@ zero_extendqisi2
	ldrb	r1, [ip, r0]	@ zero_extendqisi2
	strb	r1, [r7, #0]
	ldrb	r0, [r3, #0]	@ zero_extendqisi2
	ldrb	r1, [ip, r0]	@ zero_extendqisi2
	strb	r1, [r3, #0]
.L2492:
	add	r8, r8, #1
	add	r7, r7, #3
	add	r3, r3, #3
.L2369:
	ldrb	r1, [r3, #-2]	@ zero_extendqisi2
	ldrh	r0, [r2, #2]
	cmp	r0, r1
	beq	.L2493
.L2236:
	ldrb	r1, [ip, r1]	@ zero_extendqisi2
	strb	r1, [r3, #-2]
	ldrb	r0, [r7, #0]	@ zero_extendqisi2
	ldrb	r1, [ip, r0]	@ zero_extendqisi2
	strb	r1, [r7, #0]
	ldrb	r0, [r3, #0]	@ zero_extendqisi2
	ldrb	r1, [ip, r0]	@ zero_extendqisi2
	strb	r1, [r3, #0]
.L2494:
	add	r8, r8, #1
	cmp	r6, r8
	add	r7, r7, #3
	add	r3, r3, #3
	bhi	.L1998
	b	.L2577
.L1996:
	ldrb	r0, [ip, r1]	@ zero_extendqisi2
	strb	r0, [r3, #-2]
	ldrb	r1, [r7, #0]	@ zero_extendqisi2
	ldrb	sl, [ip, r1]	@ zero_extendqisi2
	strb	sl, [r7, #0]
	ldrb	r0, [r3, #0]	@ zero_extendqisi2
	ldrb	r1, [ip, r0]	@ zero_extendqisi2
	strb	r1, [r3, #0]
.L1997:
	add	r3, r3, #3
	ldrb	r0, [r3, #-2]	@ zero_extendqisi2
	ldrh	sl, [r2, #2]
	cmp	sl, r0
	add	r8, r8, #1
	add	r1, r7, #3
	beq	.L2620
.L2240:
	ldrb	r7, [ip, r0]	@ zero_extendqisi2
	strb	r7, [r3, #-2]
	ldrb	sl, [r1, #0]	@ zero_extendqisi2
	ldrb	r0, [ip, sl]	@ zero_extendqisi2
	strb	r0, [r1, #0]
	ldrb	r7, [r3, #0]	@ zero_extendqisi2
	ldrb	sl, [ip, r7]	@ zero_extendqisi2
	strb	sl, [r3, #0]
.L2495:
	add	r0, r3, #3
	ldrh	sl, [r2, #2]
	ldrb	r7, [r0, #-2]	@ zero_extendqisi2
	cmp	sl, r7
	add	sl, r1, #3
	beq	.L2496
.L2243:
	ldrb	r7, [ip, r7]	@ zero_extendqisi2
	strb	r7, [r0, #-2]
	ldrb	r0, [r1, #3]	@ zero_extendqisi2
	ldrb	sl, [ip, r0]	@ zero_extendqisi2
	strb	sl, [r1, #3]
	ldrb	r7, [r3, #3]	@ zero_extendqisi2
	ldrb	r0, [ip, r7]	@ zero_extendqisi2
	strb	r0, [r3, #3]
.L2497:
	add	r0, r3, #6
	ldrh	sl, [r2, #2]
	ldrb	r7, [r0, #-2]	@ zero_extendqisi2
	cmp	sl, r7
	add	sl, r1, #6
	beq	.L2498
.L2246:
	ldrb	r7, [ip, r7]	@ zero_extendqisi2
	strb	r7, [r0, #-2]
	ldrb	r7, [r1, #6]	@ zero_extendqisi2
	ldrb	r0, [ip, r7]	@ zero_extendqisi2
	strb	r0, [r1, #6]
	ldrb	r7, [r3, #6]	@ zero_extendqisi2
	ldrb	r0, [ip, r7]	@ zero_extendqisi2
	strb	r0, [r3, #6]
.L2499:
	add	r8, r8, #3
	cmp	r6, r8
	add	r7, r1, #9
	add	r3, r3, #9
	bls	.L2577
.L1998:
	ldrb	r1, [r3, #-2]	@ zero_extendqisi2
	ldrh	sl, [r2, #2]
	cmp	sl, r1
	bne	.L1996
	ldrb	sl, [r3, #-1]	@ zero_extendqisi2
	ldrh	r0, [r2, #4]
	cmp	sl, r0
	bne	.L1996
	ldrb	sl, [r7, #1]	@ zero_extendqisi2
	ldrh	r0, [r2, #6]
	cmp	sl, r0
	bne	.L1996
	ldrh	sl, [r5, #2]
	strb	sl, [r3, #-2]
	ldrh	r0, [r5, #4]
	strb	r0, [r3, #-1]
	ldrh	r1, [r5, #6]
	strb	r1, [r7, #1]
	b	.L1997
.L2588:
	cmp	r9, #0
	cmpne	sl, #0
	beq	.L2013
	cmp	ip, #0
	beq	.L2013
	cmp	r6, #0
	beq	.L1938
	sub	r1, r6, #1
	ands	r0, r1, #3
	mov	r2, #0
	mov	r1, r7
	beq	.L2018
	ldrb	r3, [r7, #1]	@ zero_extendqisi2
	cmp	r3, #255
	beq	.L2500
	cmp	r3, #0
	beq	.L2501
	ldr	fp, [sp, #64]
	ldrh	r8, [fp, #8]
	rsb	r2, r3, #255
	mul	r2, r8, r2
	ldrb	r1, [r7, #0]	@ zero_extendqisi2
	ldrb	r8, [r9, r1]	@ zero_extendqisi2
	add	r2, r2, #128
	mla	r3, r8, r3, r2
	mov	r1, r3, asl #16
	mov	r2, r1, lsr #24
	add	r8, r2, r1, lsr #16
	mov	r3, r8, asr #8
	and	r1, r3, #255
	ldrb	r2, [sl, r1]	@ zero_extendqisi2
	strb	r2, [r7, #0]
.L2502:
	cmp	r0, #1
	mov	r2, #1
	add	r1, r7, #2
	beq	.L2018
	cmp	r0, #2
	beq	.L2373
	ldrb	r3, [r1, #1]	@ zero_extendqisi2
	cmp	r3, #255
	beq	.L2503
	cmp	r3, #0
	beq	.L2504
	ldr	r0, [sp, #64]
	ldrh	r8, [r0, #8]
	rsb	r0, r3, #255
	mul	r8, r0, r8
	ldrb	r0, [r1, #0]	@ zero_extendqisi2
	ldrb	r0, [r9, r0]	@ zero_extendqisi2
	add	r8, r8, #128
	mla	r3, r0, r3, r8
	mov	r3, r3, asl #16
	mov	r0, r3, lsr #24
	add	r8, r0, r3, lsr #16
	mov	r3, r8, asr #8
	and	r8, r3, #255
	ldrb	r0, [sl, r8]	@ zero_extendqisi2
	strb	r0, [r7, r2]
.L2505:
	add	r2, r2, #1
	add	r1, r1, #2
.L2373:
	ldrb	r3, [r1, #1]	@ zero_extendqisi2
	cmp	r3, #255
	beq	.L2506
	cmp	r3, #0
	beq	.L2507
	ldr	fp, [sp, #64]
	ldrh	r8, [fp, #8]
	rsb	r0, r3, #255
	mul	r8, r0, r8
	ldrb	r0, [r1, #0]	@ zero_extendqisi2
	ldrb	r0, [r9, r0]	@ zero_extendqisi2
	add	r8, r8, #128
	mla	r3, r0, r3, r8
	mov	r8, r3, asl #16
	mov	r0, r8, lsr #24
	add	r3, r0, r8, lsr #16
	mov	r0, r3, asr #8
	and	r8, r0, #255
	ldrb	r3, [sl, r8]	@ zero_extendqisi2
	strb	r3, [r7, r2]
.L2508:
	add	r2, r2, #1
	add	r1, r1, #2
	b	.L2018
.L2623:
	ldrh	r8, [r5, #8]
	strb	r8, [r7, r2]
.L2015:
	add	r2, r2, #1
	cmp	r6, r2
	bls	.L2577
	add	r3, r1, #2
	ldrb	r0, [r3, #1]	@ zero_extendqisi2
	cmp	r0, #255
	beq	.L2621
	cmp	r0, #0
	beq	.L2509
	ldr	r1, [sp, #64]
	ldrh	r8, [r1, #8]
	rsb	r1, r0, #255
	mul	r1, r8, r1
	ldrb	r8, [r3, #0]	@ zero_extendqisi2
	ldrb	r8, [r9, r8]	@ zero_extendqisi2
	add	r1, r1, #128
	mla	r1, r8, r0, r1
	mov	r8, r1, asl #16
	mov	r0, r8, lsr #24
	add	r1, r0, r8, lsr #16
	mov	r0, r1, asr #8
	and	r8, r0, #255
	ldrb	r1, [sl, r8]	@ zero_extendqisi2
	strb	r1, [r7, r2]
.L2510:
	ldrb	r1, [r3, #3]	@ zero_extendqisi2
	cmp	r1, #255
	add	r0, r2, #1
	beq	.L2511
	cmp	r1, #0
	beq	.L2512
	ldr	r8, [sp, #64]
	ldrh	fp, [r8, #8]
	rsb	r8, r1, #255
	mul	fp, r8, fp
	ldrb	r8, [r3, #2]	@ zero_extendqisi2
	ldrb	r8, [r9, r8]	@ zero_extendqisi2
	add	fp, fp, #128
	mla	r1, r8, r1, fp
	mov	r1, r1, asl #16
	mov	r8, r1, lsr #24
	add	r1, r8, r1, lsr #16
	mov	r8, r1, asr #8
	and	r1, r8, #255
	ldrb	r8, [sl, r1]	@ zero_extendqisi2
	strb	r8, [r7, r0]
.L2513:
	ldrb	r1, [r3, #5]	@ zero_extendqisi2
	cmp	r1, #255
	add	r0, r2, #2
	beq	.L2514
	cmp	r1, #0
	beq	.L2515
	ldr	r8, [sp, #64]
	ldrh	fp, [r8, #8]
	rsb	r8, r1, #255
	mul	fp, r8, fp
	ldrb	r8, [r3, #4]	@ zero_extendqisi2
	ldrb	r8, [r9, r8]	@ zero_extendqisi2
	add	fp, fp, #128
	mla	r1, r8, r1, fp
	mov	r1, r1, asl #16
	mov	r8, r1, lsr #24
	add	r1, r8, r1, lsr #16
	mov	r8, r1, asr #8
	and	r1, r8, #255
	ldrb	r8, [sl, r1]	@ zero_extendqisi2
	strb	r8, [r7, r0]
.L2516:
	add	r2, r2, #3
	add	r1, r3, #6
.L2018:
	ldrb	r3, [r1, #1]	@ zero_extendqisi2
	cmp	r3, #255
	beq	.L2622
	cmp	r3, #0
	beq	.L2623
	ldr	fp, [sp, #64]
	ldrh	r8, [fp, #8]
	rsb	r0, r3, #255
	mul	r0, r8, r0
	ldrb	r8, [r1, #0]	@ zero_extendqisi2
	ldrb	r8, [r9, r8]	@ zero_extendqisi2
	add	r0, r0, #128
	mla	r3, r8, r3, r0
	mov	r8, r3, asl #16
	mov	r0, r8, lsr #24
	add	r3, r0, r8, lsr #16
	mov	r8, r3, asr #8
	and	r3, r8, #255
	ldrb	r0, [sl, r3]	@ zero_extendqisi2
	strb	r0, [r7, r2]
	b	.L2015
.L2403:
	ldrb	sl, [r7, #3]	@ zero_extendqisi2
	ldrb	ip, [r7, #2]	@ zero_extendqisi2
	add	r2, sl, ip, asl #8
	mov	sl, r2, asl #16
	mov	r2, sl, lsr #16
	cmp	r2, r8
	beq	.L2534
	cmp	r2, #0
	beq	.L2535
	ldrb	sl, [r7, #1]	@ zero_extendqisi2
	mov	r9, sl, asr r3
	ldr	ip, [sp, #64]
	rsb	fp, r2, #65280
	ldrh	sl, [ip, #8]
	add	ip, fp, #255
	mul	fp, sl, ip
	ldr	ip, [sp, #88]
	ldr	sl, [ip, r9, asl #2]
	ldrb	r9, [r7, #0]	@ zero_extendqisi2
	add	ip, sl, r9, asl #1
	ldrh	r9, [ip, #0]
	add	sl, fp, #32768
	mla	r2, r9, r2, sl
	add	r2, r2, r2, lsr #16
	mov	ip, r2, lsr #16
	mov	ip, ip, asl #16
	mov	sl, ip, lsr #16
	and	r9, sl, #255
	mov	r2, r9, asr r3
	ldr	r9, [sp, #84]
	ldr	sl, [r9, r2, asl #2]
	mov	ip, ip, lsr #24
	add	r2, sl, ip, asl #1
	ldrh	sl, [r2, #0]
	mov	r2, sl, lsr #8
	strb	r2, [r0, #-2]
	strb	sl, [r0, #-1]
.L2536:
	add	r0, r0, #2
	add	r2, r7, #4
	mov	sl, #1
	str	r4, [sp, #4]
	b	.L2031
.L2589:
	ldr	r4, [sp, #4]
	ldrb	r0, [r4, #8]	@ zero_extendqisi2
	and	r8, r0, #4
	b	.L1938
.L2582:
	ldr	r4, [sp, #20]
	ldrb	r0, [r4, #8]	@ zero_extendqisi2
	and	r8, r0, #4
	b	.L1938
.L2594:
	mov	r0, sl
	mov	r1, r8
	mov	r2, #2
	bl	memcpy
	b	.L2543
.L2547:
	ldrh	r2, [r5, #2]
	strb	r2, [r3, #-3]
	ldrh	r8, [r5, #4]
	strb	r8, [r3, #-2]
	ldrh	r4, [r5, #6]
	strb	r4, [r3, #-1]
	b	.L2548
.L2616:
	ldrb	r1, [r2, #0]	@ zero_extendqisi2
	ldrb	r4, [ip, r1]	@ zero_extendqisi2
	strb	r4, [r3, #-3]
	ldrb	r8, [r2, #1]	@ zero_extendqisi2
	ldrb	r1, [ip, r8]	@ zero_extendqisi2
	strb	r1, [r3, #-2]
	ldrb	r4, [r2, #2]	@ zero_extendqisi2
	ldrb	r8, [ip, r4]	@ zero_extendqisi2
	strb	r8, [r3, #-1]
	b	.L2042
.L2607:
	rsb	r4, r3, #6
	and	r0, r0, r8, asr r4
	strb	r0, [r7, #0]
	ldrh	r4, [r5, #8]
	orr	r0, r0, r4, asl r3
	strb	r0, [r7, #0]
	b	.L2430
.L2540:
	ldrh	sl, [r5, #8]
	mov	r3, sl, lsr #8
	strb	r3, [r7, #0]
	ldrh	r8, [r5, #8]
	strb	r8, [r7, #1]
	b	.L2541
.L2013:
	cmp	r6, #0
	beq	.L1938
	sub	ip, r6, #1
	ands	r2, ip, #3
	mov	r3, r7
	mov	r1, #0
	beq	.L2576
	ldrb	r3, [r7, #1]	@ zero_extendqisi2
	cmp	r3, #255
	beq	.L2519
	cmp	r3, #0
	beq	.L2518
	ldrb	r8, [r7, #0]	@ zero_extendqisi2
	mul	r8, r3, r8
	ldr	r9, [sp, #64]
	ldrh	r0, [r9, #8]
	add	r1, r8, #128
	rsb	ip, r3, #255
	mla	ip, r0, ip, r1
	mov	r0, ip, asl #16
	mov	r8, r0, lsr #24
	add	r3, r8, r0, lsr #16
	mov	ip, r3, asr #8
	strb	ip, [r7, #0]
.L2519:
	cmp	r2, #1
	mov	r1, #1
	add	r3, r7, #2
	beq	.L2576
	cmp	r2, #2
	beq	.L2374
	ldrb	r2, [r3, #1]	@ zero_extendqisi2
	cmp	r2, #255
	beq	.L2520
	cmp	r2, #0
	beq	.L2521
	ldrb	r0, [r3, #0]	@ zero_extendqisi2
	mul	r0, r2, r0
	ldr	sl, [sp, #64]
	ldrh	ip, [sl, #8]
	add	r8, r0, #128
	rsb	r2, r2, #255
	mla	r2, ip, r2, r8
	mov	r8, r2, asl #16
	mov	r0, r8, lsr #24
	add	ip, r0, r8, lsr #16
	mov	r2, ip, asr #8
	strb	r2, [r7, r1]
.L2522:
	add	r1, r1, #1
	add	r3, r3, #2
.L2374:
	ldrb	r2, [r3, #1]	@ zero_extendqisi2
	cmp	r2, #255
	beq	.L2523
	cmp	r2, #0
	beq	.L2524
	ldrb	r0, [r3, #0]	@ zero_extendqisi2
	mul	r0, r2, r0
	ldr	fp, [sp, #64]
	ldrh	ip, [fp, #8]
	add	r8, r0, #128
	rsb	r2, r2, #255
	mla	r2, ip, r2, r8
	mov	r8, r2, asl #16
	mov	r0, r8, lsr #24
	add	r2, r0, r8, lsr #16
	mov	ip, r2, asr #8
	strb	ip, [r7, r1]
.L2525:
	add	r1, r1, #1
	add	r3, r3, #2
	ldr	ip, [sp, #64]
	b	.L2023
.L2626:
	ldrh	r0, [r5, #8]
	strb	r0, [r7, r1]
.L2020:
	add	r1, r1, #1
	cmp	r6, r1
	bls	.L2577
	add	r2, r3, #2
	ldrb	r0, [r2, #1]	@ zero_extendqisi2
	cmp	r0, #255
	beq	.L2624
	cmp	r0, #0
	beq	.L2526
	ldrb	r8, [r2, #0]	@ zero_extendqisi2
	mul	r8, r0, r8
	ldrh	r3, [ip, #8]
	add	r8, r8, #128
	rsb	r0, r0, #255
	mla	r3, r0, r3, r8
	mov	r3, r3, asl #16
	mov	r0, r3, lsr #24
	add	r8, r0, r3, lsr #16
	mov	r3, r8, asr #8
	strb	r3, [r7, r1]
.L2527:
	ldrb	r3, [r2, #3]	@ zero_extendqisi2
	cmp	r3, #255
	add	r0, r1, #1
	beq	.L2528
	cmp	r3, #0
	beq	.L2529
	ldrb	r8, [r2, #2]	@ zero_extendqisi2
	mul	r8, r3, r8
	ldrh	sl, [ip, #8]
	add	r8, r8, #128
	rsb	r3, r3, #255
	mla	r3, sl, r3, r8
	mov	r3, r3, asl #16
	mov	r8, r3, lsr #24
	add	r3, r8, r3, lsr #16
	mov	r8, r3, asr #8
	strb	r8, [r7, r0]
.L2530:
	ldrb	r3, [r2, #5]	@ zero_extendqisi2
	cmp	r3, #255
	add	r0, r1, #2
	beq	.L2531
	cmp	r3, #0
	beq	.L2532
	ldrb	r8, [r2, #4]	@ zero_extendqisi2
	mul	r8, r3, r8
	ldrh	sl, [ip, #8]
	add	r8, r8, #128
	rsb	r3, r3, #255
	mla	r3, sl, r3, r8
	mov	r3, r3, asl #16
	mov	r8, r3, lsr #24
	add	r3, r8, r3, lsr #16
	mov	r3, r3, asr #8
	strb	r3, [r7, r0]
.L2533:
	add	r1, r1, #3
	add	r3, r2, #6
.L2023:
	ldrb	r2, [r3, #1]	@ zero_extendqisi2
	cmp	r2, #255
	beq	.L2625
	cmp	r2, #0
	beq	.L2626
	ldrb	r0, [r3, #0]	@ zero_extendqisi2
	mul	r0, r2, r0
	ldrh	r8, [ip, #8]
	add	r0, r0, #128
	rsb	r2, r2, #255
	mla	r2, r8, r2, r0
	mov	r8, r2, asl #16
	mov	r0, r8, lsr #24
	add	r2, r0, r8, lsr #16
	mov	r8, r2, asr #8
	strb	r8, [r7, r1]
	b	.L2020
.L2040:
	cmp	r6, #0
	beq	.L1938
	tst	r6, #1
	add	r3, r7, #3
	mov	r2, r7
	mov	ip, #0
	bne	.L2050
	ldrb	r2, [r7, #3]	@ zero_extendqisi2
	cmp	r2, #255
	beq	.L2549
	cmp	r2, #0
	beq	.L2550
	ldrb	r0, [r7, #0]	@ zero_extendqisi2
	mul	r0, r2, r0
	rsb	r8, r2, #255
	ldrh	ip, [r5, #2]
	mov	r1, r8, asl #16
	mov	r8, r1, lsr #16
	add	r0, r0, #128
	mla	r0, ip, r8, r0
	mov	r0, r0, asl #16
	mov	ip, r0, lsr #24
	add	r0, ip, r0, lsr #16
	mov	ip, r0, asr #8
	strb	ip, [r3, #-3]
	ldrb	r0, [r7, #1]	@ zero_extendqisi2
	mul	r0, r2, r0
	ldrh	ip, [r5, #4]
	add	r0, r0, #128
	mla	r0, ip, r8, r0
	mov	r0, r0, asl #16
	mov	ip, r0, lsr #24
	add	r0, ip, r0, lsr #16
	mov	ip, r0, asr #8
	strb	ip, [r3, #-2]
	ldrb	r0, [r7, #2]	@ zero_extendqisi2
	mul	r2, r0, r2
	ldrh	ip, [r5, #6]
	add	r2, r2, #128
	mla	r2, ip, r8, r2
	mov	r8, r2, asl #16
	mov	r1, r8, lsr #24
	add	r0, r1, r8, lsr #16
	mov	ip, r0, asr #8
	strb	ip, [r3, #-1]
.L2551:
	add	r3, r3, #3
	add	r2, r7, #4
	mov	ip, #1
	b	.L2050
.L2629:
	ldrh	r8, [r5, #2]
	strb	r8, [r3, #-3]
	ldrh	r1, [r5, #4]
	strb	r1, [r3, #-2]
	ldrh	r0, [r5, #6]
	strb	r0, [r3, #-1]
.L2047:
	add	ip, ip, #1
	cmp	r6, ip
	add	r3, r3, #3
	bls	.L2577
	add	r1, r2, #4
	ldrb	r0, [r1, #3]	@ zero_extendqisi2
	cmp	r0, #255
	beq	.L2627
	cmp	r0, #0
	beq	.L2552
	ldrb	r7, [r1, #0]	@ zero_extendqisi2
	mul	r7, r0, r7
	rsb	r8, r0, #255
	mov	r2, r8, asl #16
	ldrh	r8, [r5, #2]
	mov	r2, r2, lsr #16
	add	r7, r7, #128
	mla	r7, r8, r2, r7
	mov	r7, r7, asl #16
	mov	r8, r7, lsr #24
	add	r7, r8, r7, lsr #16
	mov	r8, r7, asr #8
	strb	r8, [r3, #-3]
	ldrb	r7, [r1, #1]	@ zero_extendqisi2
	mul	r7, r0, r7
	ldrh	r8, [r5, #4]
	add	r7, r7, #128
	mla	r7, r8, r2, r7
	mov	r7, r7, asl #16
	mov	r8, r7, lsr #24
	add	r7, r8, r7, lsr #16
	mov	r8, r7, asr #8
	strb	r8, [r3, #-2]
	ldrb	r7, [r1, #2]	@ zero_extendqisi2
	mul	r0, r7, r0
	ldrh	r7, [r5, #6]
	add	r8, r0, #128
	mla	r2, r7, r2, r8
	mov	r2, r2, asl #16
	mov	r0, r2, lsr #24
	add	r8, r0, r2, lsr #16
	mov	r0, r8, asr #8
	strb	r0, [r3, #-1]
.L2553:
	add	ip, ip, #1
	add	r3, r3, #3
	add	r2, r1, #4
.L2050:
	ldrb	r1, [r2, #3]	@ zero_extendqisi2
	cmp	r1, #255
	beq	.L2628
	cmp	r1, #0
	beq	.L2629
	ldrb	r7, [r2, #0]	@ zero_extendqisi2
	mul	r7, r1, r7
	rsb	r8, r1, #255
	mov	r0, r8, asl #16
	ldrh	r8, [r5, #2]
	mov	r0, r0, lsr #16
	add	r7, r7, #128
	mla	r7, r8, r0, r7
	mov	r7, r7, asl #16
	mov	r8, r7, lsr #24
	add	r7, r8, r7, lsr #16
	mov	r8, r7, asr #8
	strb	r8, [r3, #-3]
	ldrb	r7, [r2, #1]	@ zero_extendqisi2
	mul	r7, r1, r7
	ldrh	r8, [r5, #4]
	add	r7, r7, #128
	mla	r7, r8, r0, r7
	mov	r7, r7, asl #16
	mov	r8, r7, lsr #24
	add	r7, r8, r7, lsr #16
	mov	r8, r7, asr #8
	strb	r8, [r3, #-2]
	ldrb	r7, [r2, #2]	@ zero_extendqisi2
	mul	r1, r7, r1
	ldrh	r7, [r5, #6]
	add	r8, r1, #128
	mla	r0, r7, r0, r8
	mov	r0, r0, asl #16
	mov	r1, r0, lsr #24
	add	r8, r1, r0, lsr #16
	mov	r0, r8, asr #8
	strb	r0, [r3, #-1]
	b	.L2047
.L2552:
	ldrh	r8, [r5, #2]
	strb	r8, [r3, #-3]
	ldrh	r2, [r5, #4]
	strb	r2, [r3, #-2]
	ldrh	r0, [r5, #6]
	strb	r0, [r3, #-1]
	b	.L2553
.L2628:
	ldrb	r8, [r2, #0]	@ zero_extendqisi2
	strb	r8, [r3, #-3]
	ldrb	r1, [r2, #1]	@ zero_extendqisi2
	strb	r1, [r3, #-2]
	ldrb	r0, [r2, #2]	@ zero_extendqisi2
	strb	r0, [r3, #-1]
	b	.L2047
.L2627:
	ldrb	r8, [r2, #4]	@ zero_extendqisi2
	strb	r8, [r3, #-3]
	ldrb	r2, [r1, #1]	@ zero_extendqisi2
	strb	r2, [r3, #-2]
	ldrb	r0, [r1, #2]	@ zero_extendqisi2
	strb	r0, [r3, #-1]
	b	.L2553
.L2615:
	ldrb	r2, [r2, #4]	@ zero_extendqisi2
	ldrb	r8, [ip, r2]	@ zero_extendqisi2
	strb	r8, [r3, #-3]
	ldrb	r4, [r1, #1]	@ zero_extendqisi2
	ldrb	r2, [ip, r4]	@ zero_extendqisi2
	strb	r2, [r3, #-2]
	ldrb	r8, [r1, #2]	@ zero_extendqisi2
	ldrb	r4, [ip, r8]	@ zero_extendqisi2
	strb	r4, [r3, #-1]
	b	.L2548
.L2532:
	ldrh	r3, [r5, #8]
	strb	r3, [r7, r0]
	b	.L2533
.L2529:
	ldrh	r3, [r5, #8]
	strb	r3, [r7, r0]
	b	.L2530
.L2526:
	ldrh	r3, [r5, #8]
	strb	r3, [r7, r1]
	b	.L2527
.L2498:
	ldrb	fp, [r0, #-1]	@ zero_extendqisi2
	ldrh	r9, [r2, #4]
	cmp	fp, r9
	bne	.L2246
	ldrb	fp, [sl, #1]	@ zero_extendqisi2
	ldrh	r9, [r2, #6]
	cmp	fp, r9
	bne	.L2246
	ldrh	r7, [r5, #2]
	strb	r7, [r0, #-2]
	ldrh	r7, [r5, #4]
	strb	r7, [r0, #-1]
	ldrh	r0, [r5, #6]
	strb	r0, [sl, #1]
	b	.L2499
.L2496:
	ldrb	fp, [r0, #-1]	@ zero_extendqisi2
	ldrh	r9, [r2, #4]
	cmp	fp, r9
	bne	.L2243
	ldrb	fp, [sl, #1]	@ zero_extendqisi2
	ldrh	r9, [r2, #6]
	cmp	fp, r9
	bne	.L2243
	ldrh	r7, [r5, #2]
	strb	r7, [r0, #-2]
	ldrh	r7, [r5, #4]
	strb	r7, [r0, #-1]
	ldrh	r0, [r5, #6]
	strb	r0, [sl, #1]
	b	.L2497
.L2620:
	ldrb	sl, [r3, #-1]	@ zero_extendqisi2
	ldrh	r7, [r2, #4]
	cmp	sl, r7
	bne	.L2240
	ldrb	sl, [r1, #1]	@ zero_extendqisi2
	ldrh	r7, [r2, #6]
	cmp	sl, r7
	bne	.L2240
	ldrh	sl, [r5, #2]
	strb	sl, [r3, #-2]
	ldrh	r7, [r5, #4]
	strb	r7, [r3, #-1]
	ldrh	r0, [r5, #6]
	strb	r0, [r1, #1]
	b	.L2495
.L2625:
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	strb	r2, [r7, r1]
	b	.L2020
.L2528:
	ldrb	r8, [r2, #2]	@ zero_extendqisi2
	strb	r8, [r7, r0]
	b	.L2530
.L2624:
	ldrb	r0, [r3, #2]	@ zero_extendqisi2
	strb	r0, [r7, r1]
	b	.L2527
.L2531:
	ldrb	r8, [r2, #4]	@ zero_extendqisi2
	strb	r8, [r7, r0]
	b	.L2533
.L2614:
	ldr	r4, [sp, #8]
	ldrb	r0, [r4, #8]	@ zero_extendqisi2
	and	r8, r0, #4
	b	.L1938
.L2407:
	ldrb	r2, [r7, #3]	@ zero_extendqisi2
	cmp	r2, #255
	beq	.L2544
	cmp	r2, #0
	beq	.L2545
	rsb	fp, r2, #255
	mov	r1, fp, asl #16
	ldr	fp, [sp, #64]
	ldrh	r8, [fp, #2]
	mov	r1, r1, lsr #16
	mul	r8, r1, r8
	ldrb	r0, [r7, #0]	@ zero_extendqisi2
	ldrb	r0, [r9, r0]	@ zero_extendqisi2
	add	r8, r8, #128
	mla	r8, r0, r2, r8
	mov	r8, r8, asl #16
	mov	r0, r8, lsr #24
	add	r0, r0, r8, lsr #16
	mov	r8, r0, asr #8
	and	r0, r8, #255
	ldrb	r8, [sl, r0]	@ zero_extendqisi2
	strb	r8, [r3, #-3]
	ldrh	r8, [fp, #4]
	mul	r8, r1, r8
	ldrb	r0, [r7, #1]	@ zero_extendqisi2
	ldrb	r0, [r9, r0]	@ zero_extendqisi2
	add	r8, r8, #128
	mla	r8, r0, r2, r8
	mov	r8, r8, asl #16
	mov	r0, r8, lsr #24
	add	r0, r0, r8, lsr #16
	mov	r0, r0, asr #8
	and	r8, r0, #255
	ldrb	r0, [sl, r8]	@ zero_extendqisi2
	strb	r0, [r3, #-2]
	ldrh	r8, [fp, #6]
	mul	r1, r8, r1
	ldrb	r0, [r7, #2]	@ zero_extendqisi2
	ldrb	r8, [r9, r0]	@ zero_extendqisi2
	add	fp, r1, #128
	mla	r2, r8, r2, fp
	mov	fp, r2, asl #16
	mov	r1, fp, lsr #24
	add	r2, r1, fp, lsr #16
	mov	r0, r2, asr #8
	and	r8, r0, #255
	ldrb	fp, [sl, r8]	@ zero_extendqisi2
	strb	fp, [r3, #-1]
.L2546:
	add	r3, r3, #3
	add	r2, r7, #4
	mov	r0, #1
	str	r4, [sp, #8]
	ldr	fp, [sp, #64]
	b	.L2045
.L2586:
	cmp	r6, #0
	beq	.L1938
	sub	r3, r6, #1
	ands	r0, r3, #3
	mov	r3, r7
	beq	.L2009
	ldrb	ip, [r7, #0]	@ zero_extendqisi2
	ldrb	r1, [r7, #1]	@ zero_extendqisi2
	ldrh	r3, [r2, #2]
	add	r8, r1, ip, asl #8
	mov	r1, r8, asl #16
	cmp	r3, r1, lsr #16
	ldrb	r8, [r7, #4]	@ zero_extendqisi2
	ldrb	r1, [r7, #2]	@ zero_extendqisi2
	ldrb	r3, [r7, #3]	@ zero_extendqisi2
	ldrb	ip, [r7, #5]	@ zero_extendqisi2
	beq	.L2630
.L2264:
	cmp	r0, #1
	add	r3, r7, #6
	mov	r1, #1
	beq	.L2009
	cmp	r0, #2
	beq	.L2372
	ldrb	ip, [r3, #0]	@ zero_extendqisi2
	ldrb	r0, [r3, #1]	@ zero_extendqisi2
	add	r8, r0, ip, asl #8
	ldrh	r0, [r2, #2]
	mov	r7, r8, asl #16
	cmp	r0, r7, lsr #16
	ldrb	ip, [r3, #2]	@ zero_extendqisi2
	ldrb	r0, [r3, #3]	@ zero_extendqisi2
	ldrb	r8, [r3, #4]	@ zero_extendqisi2
	ldrb	r7, [r3, #5]	@ zero_extendqisi2
	beq	.L2631
.L2266:
	add	r1, r1, #1
	add	r3, r3, #6
.L2372:
	ldrb	r7, [r3, #0]	@ zero_extendqisi2
	ldrb	r0, [r3, #1]	@ zero_extendqisi2
	add	ip, r0, r7, asl #8
	ldrh	r7, [r2, #2]
	mov	r8, ip, asl #16
	cmp	r7, r8, lsr #16
	ldrb	ip, [r3, #2]	@ zero_extendqisi2
	ldrb	r0, [r3, #3]	@ zero_extendqisi2
	ldrb	r8, [r3, #4]	@ zero_extendqisi2
	ldrb	r7, [r3, #5]	@ zero_extendqisi2
	beq	.L2632
.L2269:
	add	r1, r1, #1
	add	r3, r3, #6
	b	.L2009
.L2007:
	add	r1, r1, #1
	cmp	r6, r1
	bls	.L2577
	add	r0, r3, #6
	ldrb	r8, [r3, #6]	@ zero_extendqisi2
	ldrb	ip, [r0, #1]	@ zero_extendqisi2
	ldrh	sl, [r2, #2]
	add	r7, ip, r8, asl #8
	mov	r3, r7, asl #16
	cmp	sl, r3, lsr #16
	ldrb	ip, [r0, #2]	@ zero_extendqisi2
	ldrb	r3, [r0, #3]	@ zero_extendqisi2
	ldrb	r8, [r0, #4]	@ zero_extendqisi2
	ldrb	r7, [r0, #5]	@ zero_extendqisi2
	beq	.L2633
.L2273:
	add	r3, r0, #6
	ldrb	r7, [r0, #6]	@ zero_extendqisi2
	ldrb	sl, [r3, #1]	@ zero_extendqisi2
	ldrh	ip, [r2, #2]
	add	r8, sl, r7, asl #8
	mov	sl, r8, asl #16
	cmp	ip, sl, lsr #16
	ldrb	r7, [r3, #2]	@ zero_extendqisi2
	ldrb	ip, [r3, #3]	@ zero_extendqisi2
	ldrb	sl, [r3, #4]	@ zero_extendqisi2
	ldrb	r8, [r3, #5]	@ zero_extendqisi2
	beq	.L2634
.L2275:
	add	r3, r0, #12
	ldrb	r8, [r0, #12]	@ zero_extendqisi2
	ldrb	ip, [r3, #1]	@ zero_extendqisi2
	ldrh	sl, [r2, #2]
	add	r7, ip, r8, asl #8
	mov	r8, r7, asl #16
	cmp	sl, r8, lsr #16
	ldrb	r7, [r3, #2]	@ zero_extendqisi2
	ldrb	ip, [r3, #3]	@ zero_extendqisi2
	ldrb	sl, [r3, #4]	@ zero_extendqisi2
	ldrb	r8, [r3, #5]	@ zero_extendqisi2
	beq	.L2635
.L2277:
	add	r1, r1, #3
	add	r3, r0, #18
.L2009:
	ldrb	ip, [r3, #0]	@ zero_extendqisi2
	ldrb	r0, [r3, #1]	@ zero_extendqisi2
	add	r8, r0, ip, asl #8
	ldrh	r0, [r2, #2]
	mov	r7, r8, asl #16
	cmp	r0, r7, lsr #16
	ldrb	ip, [r3, #2]	@ zero_extendqisi2
	ldrb	r0, [r3, #3]	@ zero_extendqisi2
	ldrb	r8, [r3, #4]	@ zero_extendqisi2
	ldrb	r7, [r3, #5]	@ zero_extendqisi2
	bne	.L2007
	add	r0, r0, ip, asl #8
	ldrh	sl, [r2, #4]
	mov	ip, r0, asl #16
	cmp	sl, ip, lsr #16
	bne	.L2007
	add	r0, r7, r8, asl #8
	ldrh	r8, [r2, #6]
	mov	ip, r0, asl #16
	cmp	r8, ip, lsr #16
	bne	.L2007
	ldrh	r0, [r5, #2]
	mov	ip, r0, lsr #8
	strb	ip, [r3, #0]
	ldrh	r8, [r5, #2]
	strb	r8, [r3, #1]
	ldrh	r0, [r5, #4]
	mov	ip, r0, lsr #8
	strb	ip, [r3, #2]
	ldrh	r8, [r5, #4]
	strb	r8, [r3, #3]
	ldrh	r0, [r5, #6]
	mov	ip, r0, lsr #8
	strb	ip, [r3, #4]
	ldrh	r8, [r5, #6]
	strb	r8, [r3, #5]
	b	.L2007
.L2579:
	mov	r4, fp
	b	.L2577
.L2512:
	ldrh	r1, [r5, #8]
	strb	r1, [r7, r0]
	b	.L2513
.L2509:
	ldrh	r0, [r5, #8]
	strb	r0, [r7, r2]
	b	.L2510
.L2515:
	ldrh	r1, [r5, #8]
	strb	r1, [r7, r0]
	b	.L2516
.L2535:
	ldrh	sl, [r5, #8]
	mov	ip, sl, lsr #8
	strb	ip, [r0, #-2]
	ldrh	r2, [r5, #8]
	strb	r2, [r0, #-1]
	b	.L2536
.L2602:
	rsb	sl, r3, #7
	and	ip, ip, r0, asr sl
	strb	ip, [r7, #0]
	ldrh	r8, [r5, #8]
	orr	sl, ip, r8, asl r3
	strb	sl, [r7, #0]
	b	.L2092
.L2604:
	rsb	r8, r3, #7
	and	ip, ip, r0, asr r8
	strb	ip, [r7, #0]
	ldrh	r8, [r5, #8]
	orr	ip, ip, r8, asl r3
	strb	ip, [r7, #0]
	b	.L2098
.L2603:
	rsb	r8, r3, #7
	and	sl, ip, r0, asr r8
	strb	sl, [r7, #0]
	ldrh	r8, [r5, #8]
	orr	ip, sl, r8, asl r3
	strb	ip, [r7, #0]
	b	.L2095
.L2622:
	ldrb	r3, [r1, #0]	@ zero_extendqisi2
	ldrb	r0, [ip, r3]	@ zero_extendqisi2
	strb	r0, [r7, r2]
	b	.L2015
.L2576:
	ldr	ip, [sp, #64]
	b	.L2023
.L2621:
	ldrb	r1, [r1, #2]	@ zero_extendqisi2
	ldrb	r8, [ip, r1]	@ zero_extendqisi2
	strb	r8, [r7, r2]
	b	.L2510
.L2514:
	ldrb	r1, [r3, #4]	@ zero_extendqisi2
	ldrb	r8, [ip, r1]	@ zero_extendqisi2
	strb	r8, [r7, r0]
	b	.L2516
.L2511:
	ldrb	r1, [r3, #2]	@ zero_extendqisi2
	ldrb	r8, [ip, r1]	@ zero_extendqisi2
	strb	r8, [r7, r0]
	b	.L2513
.L2534:
	ldrb	r2, [r7, #1]	@ zero_extendqisi2
	mov	sl, r2, asr r3
	ldrb	r2, [r7, #0]	@ zero_extendqisi2
	ldr	ip, [r1, sl, asl #2]
	add	sl, ip, r2, asl #1
	ldrh	ip, [sl, #0]
	mov	r2, ip, lsr #8
	strb	r2, [r0, #-2]
	strb	ip, [r0, #-1]
	b	.L2536
.L2635:
	add	ip, ip, r7, asl #8
	ldrh	r9, [r2, #4]
	mov	r7, ip, asl #16
	cmp	r9, r7, lsr #16
	bne	.L2277
	add	r7, r8, sl, asl #8
	ldrh	ip, [r2, #6]
	mov	r8, r7, asl #16
	cmp	ip, r8, lsr #16
	bne	.L2277
	ldrh	r7, [r5, #2]
	mov	ip, r7, lsr #8
	strb	ip, [r0, #12]
	ldrh	r8, [r5, #2]
	strb	r8, [r3, #1]
	ldrh	r7, [r5, #4]
	mov	ip, r7, lsr #8
	strb	ip, [r3, #2]
	ldrh	r8, [r5, #4]
	strb	r8, [r3, #3]
	ldrh	r7, [r5, #6]
	mov	ip, r7, lsr #8
	strb	ip, [r3, #4]
	ldrh	r8, [r5, #6]
	strb	r8, [r3, #5]
	b	.L2277
.L2634:
	add	ip, ip, r7, asl #8
	ldrh	r9, [r2, #4]
	mov	ip, ip, asl #16
	cmp	r9, ip, lsr #16
	bne	.L2275
	add	r7, r8, sl, asl #8
	ldrh	sl, [r2, #6]
	mov	r8, r7, asl #16
	cmp	sl, r8, lsr #16
	bne	.L2275
	ldrh	ip, [r5, #2]
	mov	r7, ip, lsr #8
	strb	r7, [r0, #6]
	ldrh	sl, [r5, #2]
	strb	sl, [r3, #1]
	ldrh	r8, [r5, #4]
	mov	ip, r8, lsr #8
	strb	ip, [r3, #2]
	ldrh	r7, [r5, #4]
	strb	r7, [r3, #3]
	ldrh	sl, [r5, #6]
	mov	r8, sl, lsr #8
	strb	r8, [r3, #4]
	ldrh	ip, [r5, #6]
	strb	ip, [r3, #5]
	b	.L2275
.L2633:
	add	r3, r3, ip, asl #8
	ldrh	sl, [r2, #4]
	mov	r3, r3, asl #16
	cmp	sl, r3, lsr #16
	bne	.L2273
	add	ip, r7, r8, asl #8
	ldrh	r8, [r2, #6]
	mov	r7, ip, asl #16
	cmp	r8, r7, lsr #16
	bne	.L2273
	ldrh	r7, [r5, #2]
	mov	r8, r7, lsr #8
	strb	r8, [r0, #0]
	ldrh	r3, [r5, #2]
	strb	r3, [r0, #1]
	ldrh	sl, [r5, #4]
	mov	ip, sl, lsr #8
	strb	ip, [r0, #2]
	ldrh	r7, [r5, #4]
	strb	r7, [r0, #3]
	ldrh	r8, [r5, #6]
	mov	r3, r8, lsr #8
	strb	r3, [r0, #4]
	ldrh	sl, [r5, #6]
	strb	sl, [r0, #5]
	b	.L2273
.L2619:
	ldrb	sl, [r3, #-1]	@ zero_extendqisi2
	ldrh	r8, [r2, #4]
	cmp	sl, r8
	bne	.L2225
	ldrb	sl, [r7, #1]	@ zero_extendqisi2
	ldrh	r8, [r2, #6]
	cmp	sl, r8
	bne	.L2225
	ldrh	r0, [r5, #2]
	strb	r0, [r3, #-2]
	ldrh	r8, [r5, #4]
	strb	r8, [r3, #-1]
	ldrh	r0, [r5, #6]
	strb	r0, [r7, #1]
	b	.L2487
.L2493:
	ldrb	sl, [r3, #-1]	@ zero_extendqisi2
	ldrh	r0, [r2, #4]
	cmp	sl, r0
	bne	.L2236
	ldrb	sl, [r7, #1]	@ zero_extendqisi2
	ldrh	r0, [r2, #6]
	cmp	sl, r0
	bne	.L2236
	ldrh	r1, [r5, #2]
	strb	r1, [r3, #-2]
	ldrh	r0, [r5, #4]
	strb	r0, [r3, #-1]
	ldrh	r1, [r5, #6]
	strb	r1, [r7, #1]
	b	.L2494
.L2491:
	ldrb	sl, [r3, #-1]	@ zero_extendqisi2
	ldrh	r0, [r2, #4]
	cmp	sl, r0
	bne	.L2232
	ldrb	sl, [r7, #1]	@ zero_extendqisi2
	ldrh	r0, [r2, #6]
	cmp	sl, r0
	bne	.L2232
	ldrh	r1, [r5, #2]
	strb	r1, [r3, #-2]
	ldrh	r0, [r5, #4]
	strb	r0, [r3, #-1]
	ldrh	r1, [r5, #6]
	strb	r1, [r7, #1]
	b	.L2492
.L2489:
	ldrb	sl, [r3, #-1]	@ zero_extendqisi2
	ldrh	r0, [r2, #4]
	cmp	sl, r0
	bne	.L2228
	ldrb	sl, [r7, #1]	@ zero_extendqisi2
	ldrh	r0, [r2, #6]
	cmp	sl, r0
	bne	.L2228
	ldrh	r1, [r5, #2]
	strb	r1, [r3, #-2]
	ldrh	r0, [r5, #4]
	strb	r0, [r3, #-1]
	ldrh	r1, [r5, #6]
	strb	r1, [r7, #1]
	b	.L2490
.L2524:
	ldrh	r2, [r5, #8]
	strb	r2, [r7, r1]
	b	.L2525
.L2550:
	ldrh	r0, [r5, #2]
	strb	r0, [r3, #-3]
	ldrh	ip, [r5, #4]
	strb	ip, [r3, #-2]
	ldrh	r2, [r5, #6]
	strb	r2, [r3, #-1]
	b	.L2551
.L2518:
	ldrh	r3, [r5, #8]
	strb	r3, [r7, #0]
	b	.L2519
.L2521:
	ldrh	r2, [r5, #8]
	strb	r2, [r7, r1]
	b	.L2522
.L2599:
	and	r3, r3, #127
	strb	r3, [r7, #0]
	ldrh	r1, [r5, #8]
	orr	r8, r3, r1, asl #7
	strb	r8, [r7, #0]
	b	.L2077
.L2618:
	cmp	r6, #0
	beq	.L2066
	sub	r3, r6, #1
	ands	r1, r3, #3
	mov	r3, r7
	beq	.L2001
	ldrb	ip, [r7, #0]	@ zero_extendqisi2
	ldrh	r3, [r2, #2]
	cmp	ip, r3
	beq	.L2636
.L2249:
	cmp	r1, #1
	add	r3, r7, #3
	mov	ip, #1
	beq	.L2001
	cmp	r1, #2
	beq	.L2371
	ldrb	r7, [r3, #0]	@ zero_extendqisi2
	ldrh	r1, [r2, #2]
	cmp	r7, r1
	beq	.L2637
.L2251:
	add	ip, ip, #1
	add	r3, r3, #3
.L2371:
	ldrb	r8, [r3, #0]	@ zero_extendqisi2
	ldrh	r1, [r2, #2]
	cmp	r8, r1
	beq	.L2638
.L2254:
	add	ip, ip, #1
	add	r3, r3, #3
	b	.L2001
.L1999:
	add	r1, ip, #1
	cmp	r6, r1
	bls	.L2577
	ldrb	ip, [r3, #3]	@ zero_extendqisi2
	ldrh	r7, [r2, #2]
	cmp	ip, r7
	add	r3, r3, #3
	beq	.L2639
.L2258:
	ldrb	r0, [r3, #3]	@ zero_extendqisi2
	ldrh	r8, [r2, #2]
	cmp	r0, r8
	add	r0, r3, #3
	beq	.L2640
.L2260:
	ldrh	r0, [r2, #2]
	ldrb	ip, [r3, #6]	@ zero_extendqisi2
	cmp	ip, r0
	add	r0, r3, #6
	beq	.L2641
.L2262:
	add	ip, r1, #3
	add	r3, r3, #9
.L2001:
	ldrb	r7, [r3, #0]	@ zero_extendqisi2
	ldrh	r8, [r2, #2]
	cmp	r7, r8
	bne	.L1999
	ldrb	r0, [r3, #1]	@ zero_extendqisi2
	ldrh	r1, [r2, #4]
	cmp	r0, r1
	bne	.L1999
	ldrb	r8, [r3, #2]	@ zero_extendqisi2
	ldrh	r0, [r2, #6]
	cmp	r8, r0
	bne	.L1999
	ldrh	r0, [r5, #2]
	strb	r0, [r3, #0]
	ldrh	r8, [r5, #4]
	strb	r8, [r3, #1]
	ldrh	r1, [r5, #6]
	strb	r1, [r3, #2]
	b	.L1999
.L2523:
	ldrb	r8, [r3, #0]	@ zero_extendqisi2
	strb	r8, [r7, r1]
	b	.L2525
.L2520:
	ldrb	r8, [r3, #0]	@ zero_extendqisi2
	strb	r8, [r7, r1]
	b	.L2522
.L2549:
	ldrb	r2, [r7, #0]	@ zero_extendqisi2
	strb	r2, [r3, #-3]
	ldrb	r8, [r7, #1]	@ zero_extendqisi2
	strb	r8, [r3, #-2]
	ldrb	r1, [r7, #2]	@ zero_extendqisi2
	strb	r1, [r3, #-1]
	b	.L2551
.L2504:
	ldrh	r3, [r5, #8]
	strb	r3, [r7, r2]
	b	.L2505
.L2545:
	ldrh	r0, [r5, #2]
	strb	r0, [r3, #-3]
	ldrh	r8, [r5, #4]
	strb	r8, [r3, #-2]
	ldrh	r2, [r5, #6]
	strb	r2, [r3, #-1]
	b	.L2546
.L2501:
	ldrh	r3, [r5, #8]
	strb	r3, [r7, #0]
	b	.L2502
.L2507:
	ldrh	r3, [r5, #8]
	strb	r3, [r7, r2]
	b	.L2508
.L2600:
	rsb	r8, r3, #7
	and	sl, ip, r0, asr r8
	strb	sl, [r7, #0]
	ldrh	r8, [r5, #8]
	orr	ip, sl, r8, asl r3
	strb	ip, [r7, #0]
	b	.L2084
.L2601:
	rsb	r8, r3, #7
	and	ip, ip, r0, asr r8
	strb	ip, [r7, #0]
	ldrh	r8, [r5, #8]
	orr	ip, ip, r8, asl r3
	strb	ip, [r7, #0]
	b	.L2088
.L2611:
	ldrh	r3, [r5, #8]
	strb	r3, [r7, #0]
	b	.L2468
.L2606:
	and	r1, r1, #63
	strb	r1, [r7, #0]
	ldrh	r3, [r5, #8]
	orr	r1, r1, r3, asl #6
	strb	r1, [r7, #0]
	b	.L2424
.L2609:
	ldrh	r0, [r5, #8]
	mov	ip, r0, lsr #8
	strb	ip, [r7, #-2]
	ldrh	r0, [r5, #8]
	strb	r0, [r7, #-1]
	b	.L2482
.L2613:
	and	r1, r1, #15
	strb	r1, [r7, #0]
	ldrh	r3, [r5, #8]
	orr	r1, r1, r3, asl #4
	strb	r1, [r7, #0]
	b	.L2446
.L2426:
	and	r0, r0, #207
	strb	r0, [r7, #0]
	ldrh	sl, [r5, #8]
	orr	r0, r0, sl, asl r3
	strb	r0, [r7, #0]
	b	.L2427
.L2500:
	ldrb	r1, [r7, #0]	@ zero_extendqisi2
	ldrb	r2, [ip, r1]	@ zero_extendqisi2
	strb	r2, [r7, #0]
	b	.L2502
.L2506:
	ldrb	r0, [r1, #0]	@ zero_extendqisi2
	ldrb	r8, [ip, r0]	@ zero_extendqisi2
	strb	r8, [r7, r2]
	b	.L2508
.L2503:
	ldrb	r0, [r1, #0]	@ zero_extendqisi2
	ldrb	r8, [ip, r0]	@ zero_extendqisi2
	strb	r8, [r7, r2]
	b	.L2505
.L2544:
	ldrb	r1, [r7, #0]	@ zero_extendqisi2
	ldrb	r0, [ip, r1]	@ zero_extendqisi2
	strb	r0, [r3, #-3]
	ldrb	r8, [r7, #1]	@ zero_extendqisi2
	ldrb	r2, [ip, r8]	@ zero_extendqisi2
	strb	r2, [r3, #-2]
	ldrb	fp, [r7, #2]	@ zero_extendqisi2
	ldrb	r1, [ip, fp]	@ zero_extendqisi2
	strb	r1, [r3, #-1]
	b	.L2546
.L2640:
	ldrb	r7, [r0, #1]	@ zero_extendqisi2
	ldrh	ip, [r2, #4]
	cmp	r7, ip
	bne	.L2260
	ldrb	r8, [r0, #2]	@ zero_extendqisi2
	ldrh	r7, [r2, #6]
	cmp	r8, r7
	bne	.L2260
	ldrh	r7, [r5, #2]
	strb	r7, [r3, #3]
	ldrh	r8, [r5, #4]
	strb	r8, [r0, #1]
	ldrh	ip, [r5, #6]
	strb	ip, [r0, #2]
	b	.L2260
.L2639:
	ldrb	ip, [r3, #1]	@ zero_extendqisi2
	ldrh	r0, [r2, #4]
	cmp	ip, r0
	bne	.L2258
	ldrb	r8, [r3, #2]	@ zero_extendqisi2
	ldrh	r7, [r2, #6]
	cmp	r8, r7
	bne	.L2258
	ldrh	r7, [r5, #2]
	strb	r7, [r3, #0]
	ldrh	ip, [r5, #4]
	strb	ip, [r3, #1]
	ldrh	r0, [r5, #6]
	strb	r0, [r3, #2]
	b	.L2258
.L2641:
	ldrb	r8, [r0, #1]	@ zero_extendqisi2
	ldrh	r7, [r2, #4]
	cmp	r8, r7
	bne	.L2262
	ldrb	r8, [r0, #2]	@ zero_extendqisi2
	ldrh	ip, [r2, #6]
	cmp	r8, ip
	bne	.L2262
	ldrh	ip, [r5, #2]
	strb	ip, [r3, #6]
	ldrh	r8, [r5, #4]
	strb	r8, [r0, #1]
	ldrh	r7, [r5, #6]
	strb	r7, [r0, #2]
	b	.L2262
.L2630:
	add	r3, r3, r1, asl #8
	ldrh	sl, [r2, #4]
	mov	r3, r3, asl #16
	cmp	sl, r3, lsr #16
	bne	.L2264
	add	r1, ip, r8, asl #8
	ldrh	ip, [r2, #6]
	mov	r8, r1, asl #16
	cmp	ip, r8, lsr #16
	bne	.L2264
	ldrh	r3, [r5, #2]
	mov	r1, r3, lsr #8
	strb	r1, [r7, #0]
	ldrh	ip, [r5, #2]
	strb	ip, [r7, #1]
	ldrh	r8, [r5, #4]
	mov	r3, r8, lsr #8
	strb	r3, [r7, #2]
	ldrh	r1, [r5, #4]
	strb	r1, [r7, #3]
	ldrh	ip, [r5, #6]
	mov	r8, ip, lsr #8
	strb	r8, [r7, #4]
	ldrh	r3, [r5, #6]
	strb	r3, [r7, #5]
	b	.L2264
.L2631:
	add	r0, r0, ip, asl #8
	ldrh	sl, [r2, #4]
	mov	ip, r0, asl #16
	cmp	sl, ip, lsr #16
	bne	.L2266
	add	ip, r7, r8, asl #8
	ldrh	r7, [r2, #6]
	mov	r8, ip, asl #16
	cmp	r7, r8, lsr #16
	bne	.L2266
	ldrh	r0, [r5, #2]
	mov	ip, r0, lsr #8
	strb	ip, [r3, #0]
	ldrh	r8, [r5, #2]
	strb	r8, [r3, #1]
	ldrh	r7, [r5, #4]
	mov	r0, r7, lsr #8
	strb	r0, [r3, #2]
	ldrh	ip, [r5, #4]
	strb	ip, [r3, #3]
	ldrh	r8, [r5, #6]
	mov	r7, r8, lsr #8
	strb	r7, [r3, #4]
	ldrh	r0, [r5, #6]
	strb	r0, [r3, #5]
	b	.L2266
.L2632:
	add	r0, r0, ip, asl #8
	ldrh	sl, [r2, #4]
	mov	ip, r0, asl #16
	cmp	sl, ip, lsr #16
	bne	.L2269
	add	ip, r7, r8, asl #8
	ldrh	r8, [r2, #6]
	mov	r7, ip, asl #16
	cmp	r8, r7, lsr #16
	bne	.L2269
	ldrh	r0, [r5, #2]
	mov	ip, r0, lsr #8
	strb	ip, [r3, #0]
	ldrh	r7, [r5, #2]
	strb	r7, [r3, #1]
	ldrh	r8, [r5, #4]
	mov	r0, r8, lsr #8
	strb	r0, [r3, #2]
	ldrh	ip, [r5, #4]
	strb	ip, [r3, #3]
	ldrh	r7, [r5, #6]
	mov	r8, r7, lsr #8
	strb	r8, [r3, #4]
	ldrh	r0, [r5, #6]
	strb	r0, [r3, #5]
	b	.L2269
.L2608:
	cmp	r6, #0
	beq	.L1938
	sub	r3, r6, #1
	ands	r0, r3, #3
	mov	r3, r7
	beq	.L1990
	ldrb	r3, [r7, #1]	@ zero_extendqisi2
	ldrb	ip, [r7, #0]	@ zero_extendqisi2
	add	r8, r3, ip, asl #8
	ldrh	r3, [r2, #8]
	mov	r1, r8, asl #16
	cmp	r3, r1, lsr #16
	ldreqh	r3, [r5, #8]
	moveq	r3, r3, lsr #8
	streqb	r3, [r7, #0]
	ldreqh	r3, [r5, #8]
	streqb	r3, [r7, #1]
	cmp	r0, #1
	add	r3, r7, #2
	mov	r1, #1
	beq	.L1990
	cmp	r0, #2
	beq	.L2367
	ldrb	r8, [r3, #0]	@ zero_extendqisi2
	ldrb	r0, [r3, #1]	@ zero_extendqisi2
	add	ip, r0, r8, asl #8
	ldrh	r0, [r2, #8]
	mov	r8, ip, asl #16
	cmp	r0, r8, lsr #16
	ldreqh	r0, [r5, #8]
	moveq	r0, r0, lsr #8
	streqb	r0, [r3, #0]
	ldreqh	r0, [r5, #8]
	add	r1, r1, #1
	streqb	r0, [r3, #1]
	add	r3, r3, #2
.L2367:
	ldrb	r8, [r3, #0]	@ zero_extendqisi2
	ldrb	r0, [r3, #1]	@ zero_extendqisi2
	add	ip, r0, r8, asl #8
	ldrh	r8, [r2, #8]
	mov	ip, ip, asl #16
	cmp	r8, ip, lsr #16
	ldreqh	r8, [r5, #8]
	moveq	r8, r8, lsr #8
	streqb	r8, [r3, #0]
	ldreqh	r8, [r5, #8]
	add	r1, r1, #1
	streqb	r8, [r3, #1]
	add	r3, r3, #2
	b	.L1990
.L2219:
	add	r3, r0, #2
	ldrb	ip, [r3, #1]	@ zero_extendqisi2
	ldrb	r8, [r0, #2]	@ zero_extendqisi2
	add	r7, ip, r8, asl #8
	ldrh	r8, [r2, #8]
	mov	r7, r7, asl #16
	cmp	r8, r7, lsr #16
	ldreqh	r8, [r5, #8]
	moveq	r8, r8, lsr #8
	streqb	r8, [r0, #2]
	ldreqh	r8, [r5, #8]
	streqb	r8, [r3, #1]
	add	r3, r0, #4
	ldrb	ip, [r3, #1]	@ zero_extendqisi2
	ldrb	r8, [r0, #4]	@ zero_extendqisi2
	add	r7, ip, r8, asl #8
	ldrh	ip, [r2, #8]
	mov	r7, r7, asl #16
	cmp	ip, r7, lsr #16
	ldreqh	ip, [r5, #8]
	moveq	ip, ip, lsr #8
	streqb	ip, [r0, #4]
	ldreqh	ip, [r5, #8]
	add	r1, r1, #3
	streqb	ip, [r3, #1]
	add	r3, r0, #6
.L1990:
	ldrb	ip, [r3, #0]	@ zero_extendqisi2
	ldrb	r0, [r3, #1]	@ zero_extendqisi2
	add	r8, r0, ip, asl #8
	ldrh	r0, [r2, #8]
	mov	ip, r8, asl #16
	cmp	r0, ip, lsr #16
	ldreqh	r0, [r5, #8]
	moveq	r0, r0, lsr #8
	streqb	r0, [r3, #0]
	ldreqh	r0, [r5, #8]
	add	r1, r1, #1
	streqb	r0, [r3, #1]
	cmp	r6, r1
	bls	.L2577
	add	r0, r3, #2
	ldrb	r8, [r3, #2]	@ zero_extendqisi2
	ldrb	ip, [r0, #1]	@ zero_extendqisi2
	add	r7, ip, r8, asl #8
	ldrh	r8, [r2, #8]
	mov	r7, r7, asl #16
	cmp	r8, r7, lsr #16
	bne	.L2219
	ldrh	r8, [r5, #8]
	mov	ip, r8, lsr #8
	strb	ip, [r3, #2]
	ldrh	r7, [r5, #8]
	strb	r7, [r0, #1]
	b	.L2219
.L2612:
	cmp	r6, #0
	beq	.L1938
	ldrb	r3, [r7, #0]	@ zero_extendqisi2
	ldrh	r8, [r2, #8]
	mov	ip, #3840
	sub	r1, r6, #1
	cmp	r8, r3, lsr #4
	add	r0, ip, #15
	and	ip, r1, #3
	beq	.L2642
.L2145:
	mov	r1, #1
	cmp	r6, r1
	mov	r3, #0
	bls	.L2577
	cmp	ip, r3
	beq	.L1975
	cmp	ip, r1
	beq	.L2358
	cmp	ip, #2
	beq	.L2359
	ldrb	ip, [r7, #0]	@ zero_extendqisi2
	ldrh	r8, [r2, #8]
	and	sl, ip, #15
	cmp	sl, r8
	andeq	ip, ip, #240
	streqb	ip, [r7, #0]
	ldreqh	r8, [r5, #8]
	orreq	ip, ip, r8
	streqb	ip, [r7, #0]
	cmp	r3, #0
	subne	r3, r3, #4
	addeq	r7, r7, #1
	moveq	r3, #4
	add	r1, r1, #1
.L2359:
	ldrb	ip, [r7, #0]	@ zero_extendqisi2
	mov	sl, ip, asr r3
	ldrh	r8, [r2, #8]
	and	sl, sl, #15
	cmp	sl, r8
	beq	.L2643
.L2152:
	cmp	r3, #0
	subne	r3, r3, #4
	addeq	r7, r7, #1
	moveq	r3, #4
	add	r1, r1, #1
.L2358:
	ldrb	ip, [r7, #0]	@ zero_extendqisi2
	mov	sl, ip, asr r3
	ldrh	r8, [r2, #8]
	and	sl, sl, #15
	cmp	sl, r8
	beq	.L2644
.L2156:
	cmp	r3, #0
	add	r1, r1, #1
	subne	r3, r3, #4
	addeq	r7, r7, #1
	moveq	r3, #4
	cmp	r6, r1
	bhi	.L1975
	b	.L2577
.L1972:
	cmp	r3, #0
	addeq	r7, r7, #1
	ldrb	ip, [r7, #0]	@ zero_extendqisi2
	moveq	r3, #4
	subne	r3, r3, #4
	mov	sl, ip, asr r3
	ldrh	r8, [r2, #8]
	and	sl, sl, #15
	cmp	sl, r8
	add	r1, r1, #1
	beq	.L2645
.L2160:
	cmp	r3, #0
	addeq	r7, r7, #1
	ldrb	ip, [r7, #0]	@ zero_extendqisi2
	subne	r3, r3, #4
	moveq	r3, #4
	mov	sl, ip, asr r3
	ldrh	r8, [r2, #8]
	and	sl, sl, #15
	cmp	sl, r8
	beq	.L2646
.L2163:
	cmp	r3, #0
	addeq	r7, r7, #1
	ldrb	ip, [r7, #0]	@ zero_extendqisi2
	subne	r3, r3, #4
	moveq	r3, #4
	mov	sl, ip, asr r3
	ldrh	r8, [r2, #8]
	and	sl, sl, #15
	cmp	sl, r8
	beq	.L2647
.L2166:
	cmp	r3, #0
	add	r1, r1, #3
	subne	r3, r3, #4
	addeq	r7, r7, #1
	moveq	r3, #4
	cmp	r6, r1
	bls	.L2577
.L1975:
	ldrb	ip, [r7, #0]	@ zero_extendqisi2
	mov	sl, ip, asr r3
	ldrh	r8, [r2, #8]
	and	sl, sl, #15
	cmp	sl, r8
	bne	.L1972
	rsb	r8, r3, #4
	and	sl, ip, r0, asr r8
	strb	sl, [r7, #0]
	ldrh	r8, [r5, #8]
	orr	ip, sl, r8, asl r3
	strb	ip, [r7, #0]
	b	.L1972
.L2610:
	cmp	r6, #0
	beq	.L1938
	ldrb	ip, [r7, #0]	@ zero_extendqisi2
	ldrh	r8, [r2, #8]
	sub	r3, r6, #1
	cmp	ip, r8
	and	r1, r3, #3
	beq	.L2648
.L2186:
	mov	r3, #1
	cmp	r6, r3
	bls	.L2577
	cmp	r1, #0
	beq	.L1982
	cmp	r1, r3
	beq	.L2364
	cmp	r1, #2
	beq	.L2365
	ldrb	r0, [r7, r3]	@ zero_extendqisi2
	ldrh	r1, [r2, #8]
	cmp	r0, r1
	ldreqh	r1, [r5, #8]
	streqb	r1, [r7, r3]
	add	r3, r3, #1
.L2365:
	ldrb	ip, [r7, r3]	@ zero_extendqisi2
	ldrh	r8, [r2, #8]
	cmp	ip, r8
	ldreqh	r8, [r5, #8]
	streqb	r8, [r7, r3]
	add	r3, r3, #1
.L2364:
	ldrb	r0, [r7, r3]	@ zero_extendqisi2
	ldrh	r1, [r2, #8]
	cmp	r0, r1
	ldreqh	r1, [r5, #8]
	streqb	r1, [r7, r3]
	add	r3, r3, #1
	cmp	r6, r3
	bhi	.L1982
	b	.L2577
.L2197:
	add	ip, r1, #1
	ldrb	sl, [r7, ip]	@ zero_extendqisi2
	ldrh	r8, [r2, #8]
	cmp	sl, r8
	add	r0, r1, #2
	add	r3, r1, #3
	ldreqh	r1, [r5, #8]
	streqb	r1, [r7, ip]
	ldrh	r8, [r2, #8]
	ldrb	ip, [r7, r0]	@ zero_extendqisi2
	cmp	ip, r8
	ldreqh	r8, [r5, #8]
	streqb	r8, [r7, r0]
	cmp	r6, r3
	bls	.L2577
.L1982:
	ldrh	r8, [r2, #8]
	ldrb	ip, [r7, r3]	@ zero_extendqisi2
	cmp	ip, r8
	ldreqh	r8, [r5, #8]
	streqb	r8, [r7, r3]
	add	r1, r3, #1
	ldrb	sl, [r7, r1]	@ zero_extendqisi2
	ldrh	r0, [r2, #8]
	cmp	sl, r0
	ldreqh	sl, [r5, #8]
	streqb	sl, [r7, r1]
	b	.L2197
.L2605:
	cmp	r6, #0
	beq	.L1938
	ldrb	r3, [r7, #0]	@ zero_extendqisi2
	ldrh	r8, [r2, #8]
	mov	ip, #16128
	sub	r1, r6, #1
	cmp	r8, r3, lsr #6
	add	r0, ip, #63
	and	ip, r1, #3
	beq	.L2649
.L2111:
	mov	r1, #1
	cmp	r6, r1
	mov	r3, #4
	bls	.L2577
	cmp	ip, #0
	beq	.L1964
	cmp	ip, r1
	beq	.L2354
	cmp	ip, #2
	beq	.L2355
	ldrb	ip, [r7, #0]	@ zero_extendqisi2
	mov	sl, ip, asr r3
	ldrh	r8, [r2, #8]
	and	sl, sl, #3
	cmp	sl, r8
	andeq	ip, ip, #207
	streqb	ip, [r7, #0]
	ldreqh	r8, [r5, #8]
	orreq	ip, ip, r8, asl r3
	streqb	ip, [r7, #0]
	cmp	r3, #0
	subne	r3, r3, #2
	addeq	r7, r7, #1
	moveq	r3, #6
	add	r1, r1, #1
.L2355:
	ldrb	ip, [r7, #0]	@ zero_extendqisi2
	mov	sl, ip, asr r3
	ldrh	r8, [r2, #8]
	and	sl, sl, #3
	cmp	sl, r8
	beq	.L2650
.L2118:
	cmp	r3, #0
	subne	r3, r3, #2
	addeq	r7, r7, #1
	moveq	r3, #6
	add	r1, r1, #1
.L2354:
	ldrb	ip, [r7, #0]	@ zero_extendqisi2
	mov	sl, ip, asr r3
	ldrh	r8, [r2, #8]
	and	sl, sl, #3
	cmp	sl, r8
	beq	.L2651
.L2122:
	cmp	r3, #0
	add	r1, r1, #1
	subne	r3, r3, #2
	addeq	r7, r7, #1
	moveq	r3, #6
	cmp	r6, r1
	bhi	.L1964
	b	.L2577
.L1961:
	cmp	r3, #0
	addeq	r7, r7, #1
	ldrb	ip, [r7, #0]	@ zero_extendqisi2
	moveq	r3, #6
	subne	r3, r3, #2
	mov	sl, ip, asr r3
	ldrh	r8, [r2, #8]
	and	sl, sl, #3
	cmp	sl, r8
	add	r1, r1, #1
	beq	.L2652
.L2126:
	cmp	r3, #0
	addeq	r7, r7, #1
	ldrb	ip, [r7, #0]	@ zero_extendqisi2
	subne	r3, r3, #2
	moveq	r3, #6
	mov	sl, ip, asr r3
	ldrh	r8, [r2, #8]
	and	sl, sl, #3
	cmp	sl, r8
	beq	.L2653
.L2129:
	cmp	r3, #0
	addeq	r7, r7, #1
	ldrb	ip, [r7, #0]	@ zero_extendqisi2
	subne	r3, r3, #2
	moveq	r3, #6
	mov	sl, ip, asr r3
	ldrh	r8, [r2, #8]
	and	sl, sl, #3
	cmp	sl, r8
	beq	.L2654
.L2132:
	cmp	r3, #0
	add	r1, r1, #3
	subne	r3, r3, #2
	addeq	r7, r7, #1
	moveq	r3, #6
	cmp	r6, r1
	bls	.L2577
.L1964:
	ldrb	ip, [r7, #0]	@ zero_extendqisi2
	mov	sl, ip, asr r3
	ldrh	r8, [r2, #8]
	and	sl, sl, #3
	cmp	sl, r8
	bne	.L1961
	rsb	r8, r3, #6
	and	sl, ip, r0, asr r8
	strb	sl, [r7, #0]
	ldrh	r8, [r5, #8]
	orr	ip, sl, r8, asl r3
	strb	ip, [r7, #0]
	b	.L1961
.L2647:
	rsb	r8, r3, #4
	and	ip, ip, r0, asr r8
	strb	ip, [r7, #0]
	ldrh	r8, [r5, #8]
	orr	ip, ip, r8, asl r3
	strb	ip, [r7, #0]
	b	.L2166
.L2646:
	rsb	r8, r3, #4
	and	sl, ip, r0, asr r8
	strb	sl, [r7, #0]
	ldrh	r8, [r5, #8]
	orr	ip, sl, r8, asl r3
	strb	ip, [r7, #0]
	b	.L2163
.L2645:
	rsb	sl, r3, #4
	and	ip, ip, r0, asr sl
	strb	ip, [r7, #0]
	ldrh	r8, [r5, #8]
	orr	sl, ip, r8, asl r3
	strb	sl, [r7, #0]
	b	.L2160
.L2654:
	rsb	r8, r3, #6
	and	ip, ip, r0, asr r8
	strb	ip, [r7, #0]
	ldrh	r8, [r5, #8]
	orr	ip, ip, r8, asl r3
	strb	ip, [r7, #0]
	b	.L2132
.L2653:
	rsb	r8, r3, #6
	and	sl, ip, r0, asr r8
	strb	sl, [r7, #0]
	ldrh	r8, [r5, #8]
	orr	ip, sl, r8, asl r3
	strb	ip, [r7, #0]
	b	.L2129
.L2652:
	rsb	sl, r3, #6
	and	ip, ip, r0, asr sl
	strb	ip, [r7, #0]
	ldrh	r8, [r5, #8]
	orr	sl, ip, r8, asl r3
	strb	sl, [r7, #0]
	b	.L2126
.L2637:
	ldrb	r8, [r3, #1]	@ zero_extendqisi2
	ldrh	r0, [r2, #4]
	cmp	r8, r0
	bne	.L2251
	ldrb	r7, [r3, #2]	@ zero_extendqisi2
	ldrh	r1, [r2, #6]
	cmp	r7, r1
	bne	.L2251
	ldrh	r7, [r5, #2]
	strb	r7, [r3, #0]
	ldrh	r0, [r5, #4]
	strb	r0, [r3, #1]
	ldrh	r8, [r5, #6]
	strb	r8, [r3, #2]
	b	.L2251
.L2636:
	ldrb	r8, [r7, #1]	@ zero_extendqisi2
	ldrh	r0, [r2, #4]
	cmp	r8, r0
	bne	.L2249
	ldrb	ip, [r7, #2]	@ zero_extendqisi2
	ldrh	r3, [r2, #6]
	cmp	ip, r3
	bne	.L2249
	ldrh	ip, [r5, #2]
	strb	ip, [r7, #0]
	ldrh	r0, [r5, #4]
	strb	r0, [r7, #1]
	ldrh	r8, [r5, #6]
	strb	r8, [r7, #2]
	b	.L2249
.L2638:
	ldrb	r0, [r3, #1]	@ zero_extendqisi2
	ldrh	r7, [r2, #4]
	cmp	r0, r7
	bne	.L2254
	ldrb	r8, [r3, #2]	@ zero_extendqisi2
	ldrh	r1, [r2, #6]
	cmp	r8, r1
	bne	.L2254
	ldrh	r8, [r5, #2]
	strb	r8, [r3, #0]
	ldrh	r7, [r5, #4]
	strb	r7, [r3, #1]
	ldrh	r0, [r5, #6]
	strb	r0, [r3, #2]
	b	.L2254
.L2642:
	and	r3, r3, #15
	strb	r3, [r7, #0]
	ldrh	r1, [r5, #8]
	orr	r8, r3, r1, asl #4
	strb	r8, [r7, #0]
	b	.L2145
.L2649:
	and	r3, r3, #63
	strb	r3, [r7, #0]
	ldrh	r1, [r5, #8]
	orr	r8, r3, r1, asl #6
	strb	r8, [r7, #0]
	b	.L2111
.L2648:
	ldrh	ip, [r5, #8]
	strb	ip, [r7, #0]
	b	.L2186
.L2651:
	rsb	r8, r3, #6
	and	ip, ip, r0, asr r8
	strb	ip, [r7, #0]
	ldrh	r8, [r5, #8]
	orr	ip, ip, r8, asl r3
	strb	ip, [r7, #0]
	b	.L2122
.L2650:
	rsb	r8, r3, #6
	and	sl, ip, r0, asr r8
	strb	sl, [r7, #0]
	ldrh	r8, [r5, #8]
	orr	ip, sl, r8, asl r3
	strb	ip, [r7, #0]
	b	.L2118
.L2643:
	rsb	r8, r3, #4
	and	sl, ip, r0, asr r8
	strb	sl, [r7, #0]
	ldrh	r8, [r5, #8]
	orr	ip, sl, r8, asl r3
	strb	ip, [r7, #0]
	b	.L2152
.L2644:
	rsb	r8, r3, #4
	and	ip, ip, r0, asr r8
	strb	ip, [r7, #0]
	ldrh	r8, [r5, #8]
	orr	ip, ip, r8, asl r3
	strb	ip, [r7, #0]
	b	.L2156
	.size	png_do_background, .-png_do_background
	.section	.text.png_set_rgb_to_gray_fixed,"ax",%progbits
	.align	2
	.global	png_set_rgb_to_gray_fixed
	.hidden	png_set_rgb_to_gray_fixed
	.type	png_set_rgb_to_gray_fixed, %function
png_set_rgb_to_gray_fixed:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	subs	r4, r0, #0
	beq	.L2665
	cmp	r1, #2
	beq	.L2659
	cmp	r1, #3
	beq	.L2660
	cmp	r1, #1
	ldreq	r0, [r4, #140]
	orreq	r1, r0, #6291456
	streq	r1, [r4, #140]
.L2657:
	ldrb	r1, [r4, #322]	@ zero_extendqisi2
	cmp	r1, #3
	ldreq	r1, [r4, #140]
	orreq	r1, r1, #4096
	streq	r1, [r4, #140]
	orrs	r1, r3, r2
	bmi	.L2666
.L2662:
	mov	r0, #99328
	add	r1, r0, #668
	add	ip, r1, #3
	add	r0, r3, r2
	cmp	r0, ip
	bgt	.L2664
	ldr	ip, .L2667
	mov	r0, r2, asl #15
	mov	r1, r3, asl #15
	mov	r2, r0, lsr #5
	umull	r3, r2, ip, r2
	mov	r0, r1, lsr #5
	umull	r3, r0, ip, r0
	mov	r2, r2, lsr #7
	mov	r0, r0, lsr #7
	rsb	r1, r2, #32768
	rsb	ip, r0, r1
	mov	r3, ip, asl #16
	mov	ip, r3, lsr #16
.L2663:
	mov	r1, #584
	add	r1, r1, #2
	mov	r3, #580
	add	r3, r3, #2
	strh	ip, [r4, r1]	@ movhi
	mov	r1, #584
	strh	r2, [r4, r3]	@ movhi
	strh	r0, [r4, r1]	@ movhi
.L2665:
	ldmfd	sp!, {r4, lr}
	bx	lr
.L2660:
	ldr	ip, [r4, #140]
	ldrb	r1, [r4, #322]	@ zero_extendqisi2
	orr	r0, ip, #2097152
	str	r0, [r4, #140]
	cmp	r1, #3
	ldreq	r1, [r4, #140]
	orreq	r1, r1, #4096
	streq	r1, [r4, #140]
	orrs	r1, r3, r2
	bpl	.L2662
.L2666:
	mov	r2, #23296
	mov	ip, #2352
	mov	r3, #6912
	add	r0, r2, #138
	add	ip, ip, #14
	add	r2, r3, #56
	b	.L2663
.L2659:
	ldr	r1, [r4, #140]
	orr	ip, r1, #4194304
	str	ip, [r4, #140]
	b	.L2657
.L2664:
	mov	r0, r4
	ldr	r1, .L2667+4
	bl	png_warning
	mov	r2, #23296
	mov	ip, #2352
	mov	r3, #6912
	add	r0, r2, #138
	add	ip, ip, #14
	add	r2, r3, #56
	b	.L2663
.L2668:
	.align	2
.L2667:
	.word	175921861
	.word	.LC0
	.size	png_set_rgb_to_gray_fixed, .-png_set_rgb_to_gray_fixed
	.section	.text.png_set_background,"ax",%progbits
	.align	2
	.global	png_set_background
	.hidden	png_set_background
	.type	png_set_background, %function
png_set_background:
	@ Function supports interworking.
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	subs	r4, r0, #0
	mov	r5, r2
	mov	r6, r3
	beq	.L2674
	cmp	r2, #0
	beq	.L2675
	ldr	r2, [r4, #140]
	orr	r0, r2, #128
	str	r0, [r4, #140]
	mov	r2, #10
	add	r0, r4, #340
	bl	memcpy
	add	r1, sp, #16
	ldmia	r1, {r0-r1}
	bl	__aeabi_d2f
	ldr	r3, [r4, #140]
	cmp	r6, #0
	movne	r6, #256
	orr	r6, r6, r3
	str	r6, [r4, #140]
	str	r0, [r4, #336]	@ float
	strb	r5, [r4, #332]
.L2674:
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L2675:
	ldr	r1, .L2676
	bl	png_warning
	b	.L2674
.L2677:
	.align	2
.L2676:
	.word	.LC1
	.size	png_set_background, .-png_set_background
	.section	.text.png_set_crc_action,"ax",%progbits
	.align	2
	.global	png_set_crc_action
	.hidden	png_set_crc_action
	.type	png_set_crc_action, %function
png_set_crc_action:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	subs	r4, r0, #0
	sub	sp, sp, #8
	beq	.L2691
	sub	r1, r1, #2
	cmp	r1, #3
	ldrls	pc, [pc, r1, asl #2]
	b	.L2680
.L2685:
	.word	.L2681
	.word	.L2682
	.word	.L2683
	.word	.L2684
.L2681:
	mov	r0, r4
	ldr	r1, .L2692
	str	r2, [sp, #4]
	bl	png_warning
	ldr	r2, [sp, #4]
.L2680:
	ldr	ip, [r4, #136]
	bic	r0, ip, #3072
	str	r0, [r4, #136]
.L2684:
	sub	r2, r2, #1
	cmp	r2, #4
	ldrls	pc, [pc, r2, asl #2]
	b	.L2686
.L2690:
	.word	.L2687
	.word	.L2686
	.word	.L2688
	.word	.L2689
	.word	.L2691
.L2686:
	ldr	r2, [r4, #136]
	bic	ip, r2, #768
	str	ip, [r4, #136]
.L2691:
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	bx	lr
.L2689:
	ldr	r2, [r4, #136]
	orr	ip, r2, #768
	str	ip, [r4, #136]
	b	.L2691
.L2682:
	ldr	ip, [r4, #136]
	bic	r0, ip, #3072
	orr	r3, r0, #1024
	str	r3, [r4, #136]
	b	.L2684
.L2683:
	ldr	r3, [r4, #136]
	orr	r1, r3, #3072
	str	r1, [r4, #136]
	b	.L2684
.L2687:
	ldr	r0, [r4, #136]
	bic	r3, r0, #768
	orr	r1, r3, #512
	str	r1, [r4, #136]
	b	.L2691
.L2688:
	ldr	r0, [r4, #136]
	bic	r3, r0, #768
	orr	r1, r3, #256
	str	r1, [r4, #136]
	b	.L2691
.L2693:
	.align	2
.L2692:
	.word	.LC2
	.size	png_set_crc_action, .-png_set_crc_action
	.section	.text.png_set_dither,"ax",%progbits
	.align	2
	.global	png_set_dither
	.hidden	png_set_dither
	.type	png_set_dither, %function
png_set_dither:
	@ Function supports interworking.
	@ args = 8, pretend = 0, frame = 64
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	subs	r4, r0, #0
	sub	sp, sp, #68
	str	r1, [sp, #32]
	str	r2, [sp, #28]
	mov	r7, r3
	ldr	r5, [sp, #104]
	beq	.L2765
	ldr	r2, [r4, #140]
	ldr	r1, [sp, #108]
	orr	r3, r2, #64
	cmp	r1, #0
	str	r3, [r4, #140]
	beq	.L2841
.L2696:
	ldr	r3, [sp, #28]
	cmp	r3, r7
	ble	.L2699
	cmp	r5, #0
	beq	.L2700
	mov	r1, r3
	mov	r0, r4
	bl	png_malloc
	str	r0, [r4, #624]
	mov	r3, #0
.L2701:
	ldr	lr, [sp, #28]
	cmp	r3, lr
	bge	.L2842
	ldr	r6, [r4, #624]
	strb	r3, [r6, r3]
	add	r3, r3, #1
	b	.L2701
.L2844:
	mov	r0, r4
	mov	r1, r5
	bl	png_free
.L2765:
	add	sp, sp, #68
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L2842:
	sub	r2, lr, #1
.L2703:
	cmp	r2, r7
	blt	.L2706
	mov	r3, #0
	mov	ip, #1
	b	.L2707
.L2705:
	ldr	r0, [r4, #624]
	ldrb	lr, [r0, r3]	@ zero_extendqisi2
	ldrb	r6, [r0, r1]	@ zero_extendqisi2
	mov	sl, lr, asl #1
	mov	r8, r6, asl #1
	ldrh	sl, [sl, r5]
	ldrh	r8, [r8, r5]
	cmp	sl, r8
	strccb	r6, [r0, r3]
	ldrcc	r3, [r4, #624]
	movcc	ip, #0
	strccb	lr, [r3, r1]
	mov	r3, r1
.L2707:
	cmp	r2, r3
	add	r1, r3, #1
	bgt	.L2705
	cmp	ip, #0
	bne	.L2706
	sub	r2, r2, #1
	b	.L2703
.L2700:
	ldr	r1, [sp, #28]
	mov	r0, r4
	bl	png_malloc
	str	r0, [r4, #628]
	ldr	r1, [sp, #28]
	mov	r0, r4
	bl	png_malloc
	str	r0, [r4, #632]
.L2724:
	ldr	ip, [sp, #28]
	cmp	r5, ip
	bge	.L2843
	ldr	r1, [r4, #628]
	and	r8, r5, #255
	strb	r8, [r1, r5]
	ldr	r6, [r4, #632]
	strb	r8, [r6, r5]
	add	r5, r5, #1
	b	.L2724
.L2750:
	mov	r1, r8
	mov	r0, r4
	ldr	r7, [sp, #24]
	bl	png_free
	mov	r0, r4
	ldr	r1, [r4, #632]
	bl	png_free
	ldr	r1, [r4, #628]
	mov	r0, r4
	bl	png_free
	mov	r1, #0
	str	r1, [r4, #628]
	str	r1, [r4, #632]
	str	r7, [sp, #28]
.L2699:
	ldr	ip, [r4, #304]
	cmp	ip, #0
	ldreq	ip, [sp, #32]
	streq	ip, [r4, #304]
	ldr	r7, [sp, #108]
	ldr	r0, [sp, #28]
	mov	r3, #308
	cmp	r7, #0
	strh	r0, [r4, r3]	@ movhi
	beq	.L2765
	mov	r1, #32768
	mov	r0, r4
	bl	png_malloc
	mov	r2, #32768
	mov	r1, #0
	str	r0, [r4, #520]
	bl	memset
	mov	r1, #32768
	mov	r0, r4
	bl	png_malloc
	mov	r2, #32768
	mov	r1, #255
	mov	r5, r0
	bl	memset
	mov	r2, #0
	str	r2, [sp, #40]
	mov	r8, r2
.L2753:
	ldr	r0, [sp, #28]
	cmp	r0, r8
	ble	.L2844
	ldr	r0, [sp, #32]
	ldr	r3, [sp, #40]
	ldrb	r1, [r0, r3]!	@ zero_extendqisi2
	ldrb	r7, [r0, #1]	@ zero_extendqisi2
	mov	sl, r7, lsr #3
	ldrb	lr, [r0, #2]	@ zero_extendqisi2
	str	sl, [sp, #16]
	ldr	r9, [sp, #16]
	mov	fp, r1, lsr #3
	mov	r2, lr, lsr #3
	rsb	r6, fp, #0
	mov	r7, #0
	rsb	lr, r9, #0
	rsb	r0, r2, #0
	str	fp, [sp, #48]
	str	r6, [sp, #44]
	str	r7, [sp, #36]
	str	lr, [sp, #52]
	str	r0, [sp, #20]
.L2763:
	ldr	sl, [sp, #36]
	ldr	r1, [sp, #48]
	cmp	sl, r1
	ldrle	lr, [sp, #48]
	ldrle	r0, [sp, #36]
	ldr	fp, [sp, #36]
	ldrgt	r7, [sp, #44]
	ldr	r9, [sp, #52]
	rsble	lr, r0, lr
	mov	r6, fp, asl #10
	strgt	r7, [sp, #12]
	strle	lr, [sp, #12]
	str	r6, [sp, #24]
	str	r9, [sp, #8]
	mov	sl, #0
.L2762:
	ldr	r7, [sp, #16]
	cmp	sl, r7
	movle	lr, r7
	ldrgt	ip, [sp, #8]
	rsble	ip, sl, lr
	ldr	r6, [sp, #24]
	ldr	lr, [sp, #12]
	ldr	r0, [sp, #12]
	orr	r1, r6, sl, asl #5
	cmp	r0, ip
	movlt	r0, ip
	add	ip, ip, lr
	ldrb	fp, [r5, r1]	@ zero_extendqisi2
	add	r7, r2, ip
	cmp	r2, r0
	addge	r9, r7, r2
	addlt	r9, r7, r0
	cmp	r9, fp
	strltb	r9, [r5, r1]
	ldrlt	r9, [r4, #520]
	strltb	r8, [r9, r1]
	mov	r6, #3
	ldr	r7, [sp, #20]
	mov	r3, #1
	cmp	r6, #0
	add	lr, r7, r3
	beq	.L2761
	cmp	r6, r3
	beq	.L2811
	cmp	r6, #2
	beq	.L2812
	cmp	r3, r2
	movgt	r7, lr
	orr	r6, r3, r1
	suble	r7, r2, #1
	ldrb	r9, [r5, r6]	@ zero_extendqisi2
	add	fp, r7, ip
	cmp	r7, r0
	addge	r7, fp, r7
	addlt	r7, fp, r0
	cmp	r7, r9
	strltb	r7, [r5, r6]
	ldrlt	r7, [r4, #520]
	strltb	r8, [r7, r6]
	add	r3, r3, #1
	add	lr, lr, #1
.L2812:
	cmp	r3, r2
	rsble	r7, r3, r2
	movgt	r7, lr
	orr	r6, r3, r1
	ldrb	r9, [r5, r6]	@ zero_extendqisi2
	add	fp, r7, ip
	cmp	r7, r0
	addge	r7, fp, r7
	addlt	r7, fp, r0
	cmp	r7, r9
	strltb	r7, [r5, r6]
	ldrlt	r7, [r4, #520]
	strltb	r8, [r7, r6]
	add	r3, r3, #1
	add	lr, lr, #1
.L2811:
	cmp	r3, r2
	rsble	r7, r3, r2
	movgt	r7, lr
	orr	r6, r3, r1
	ldrb	r9, [r5, r6]	@ zero_extendqisi2
	add	fp, r7, ip
	cmp	r7, r0
	addge	r7, fp, r7
	addlt	r7, fp, r0
	cmp	r7, r9
	strltb	r7, [r5, r6]
	ldrlt	r7, [r4, #520]
	add	r3, r3, #1
	strltb	r8, [r7, r6]
	cmp	r3, #32
	add	lr, lr, #1
	beq	.L2825
.L2761:
	cmp	r3, r2
	movgt	r7, lr
	rsble	r7, r3, r2
	orr	r6, r3, r1
	ldrb	r9, [r5, r6]	@ zero_extendqisi2
	add	fp, r7, ip
	cmp	r7, r0
	addge	r7, fp, r7
	addlt	r7, fp, r0
	cmp	r7, r9
	strltb	r7, [r5, r6]
	ldrlt	r7, [r4, #520]
	add	r3, r3, #1
	strltb	r8, [r7, r6]
	cmp	r3, r2
	add	lr, lr, #1
	rsble	r7, r3, r2
	movgt	r7, lr
.L2826:
	orr	r6, r3, r1
	ldrb	r9, [r5, r6]	@ zero_extendqisi2
	add	fp, r7, ip
	cmp	r7, r0
	addge	r7, fp, r7
	addlt	r7, fp, r0
	cmp	r7, r9
	strltb	r7, [r5, r6]
	ldrlt	r7, [r4, #520]
	strltb	r8, [r7, r6]
	add	r6, r3, #1
	cmp	r6, r2
	add	r7, lr, #1
	rsble	r7, r6, r2
	orr	r6, r6, r1
	ldrb	r9, [r5, r6]	@ zero_extendqisi2
	add	fp, r7, ip
	cmp	r7, r0
	addge	r7, fp, r7
	addlt	r7, fp, r0
	cmp	r7, r9
	strltb	r7, [r5, r6]
	ldrlt	r7, [r4, #520]
	strltb	r8, [r7, r6]
	add	r6, r3, #2
	cmp	r6, r2
	add	r7, lr, #2
	rsble	r7, r6, r2
	orr	r6, r6, r1
	ldrb	r9, [r5, r6]	@ zero_extendqisi2
	add	fp, r7, ip
	cmp	r7, r0
	addge	r7, fp, r7
	addlt	r7, fp, r0
	cmp	r7, r9
	strltb	r7, [r5, r6]
	ldrlt	r7, [r4, #520]
	add	r3, r3, #3
	strltb	r8, [r7, r6]
	cmp	r3, #32
	add	lr, lr, #3
	bne	.L2761
.L2825:
	ldr	r0, [sp, #8]
	add	sl, sl, #1
	add	r7, r0, #1
	cmp	sl, #32
	str	r7, [sp, #8]
	bne	.L2762
	ldr	lr, [sp, #36]
	ldr	r3, [sp, #44]
	add	lr, lr, #1
	add	r0, r3, #1
	cmp	lr, #32
	str	lr, [sp, #36]
	str	r0, [sp, #44]
	bne	.L2763
	ldr	r1, [sp, #40]
	add	r2, r1, #3
	add	r8, r8, #1
	str	r2, [sp, #40]
	b	.L2753
.L2841:
	ldr	r1, [sp, #28]
	bl	png_malloc
	str	r0, [r4, #524]
	ldr	r3, [sp, #108]
.L2697:
	ldr	ip, [sp, #28]
	cmp	r3, ip
	bge	.L2696
	ldr	r0, [r4, #524]
	strb	r3, [r0, r3]
	add	r3, r3, #1
	b	.L2697
.L2706:
	ldr	r0, [sp, #108]
	cmp	r0, #0
	bne	.L2708
	ldr	r6, [sp, #28]
	mov	r5, r0
	mov	r3, #3
	add	fp, sp, #60
	mov	sl, r0
	mov	r8, r7
.L2709:
	cmp	r5, r8
	bge	.L2845
	ldr	r2, [r4, #624]
	ldrb	r7, [r2, r5]	@ zero_extendqisi2
	cmp	r8, r7
	bgt	.L2715
	add	r2, r2, r6
.L2716:
	ldrb	r9, [r2, #-1]!	@ zero_extendqisi2
	cmp	r8, r9
	sub	r6, r6, #1
	ble	.L2716
	ldr	r9, [sp, #32]
	mla	r9, r3, r6, r9
	ldr	lr, [sp, #32]
	mov	r2, #3
	add	r7, lr, sl
	mov	r1, r9
	mov	r0, fp
	str	r3, [sp, #4]
	bl	memcpy
	mov	r2, #3
	mov	r1, r7
	mov	r0, r9
	bl	memcpy
	mov	r1, fp
	mov	r0, r7
	mov	r2, #3
	bl	memcpy
	ldr	r0, [r4, #524]
	strb	r5, [r0, r6]
	ldr	r1, [r4, #524]
	strb	r6, [r1, r5]
	ldr	r3, [sp, #4]
.L2715:
	add	r5, r5, #1
	add	sl, sl, #3
	b	.L2709
.L2843:
	mov	lr, #3072
	add	r1, lr, #4
	mov	r0, r4
	bl	png_malloc
	mov	r3, #0
	mov	r8, r0
	mov	r0, #3072
	add	r2, r0, #4
	mov	sl, r3
.L2726:
	str	sl, [r8, r3]
	add	r3, r3, #4
	cmp	r3, r2
	bne	.L2726
	ldr	fp, [sp, #32]
	mov	r6, #3072
	add	r3, r6, #4
	mov	r2, #96
	add	r5, fp, #3
	str	r3, [sp, #16]
	ldr	r6, [sp, #28]
	str	r2, [sp, #12]
	str	r5, [sp, #40]
	str	r7, [sp, #24]
.L2727:
	mov	lr, sl
	sub	sl, r6, #1
	mov	fp, #0
	str	sl, [sp, #36]
	ldr	r7, [sp, #40]
	mov	r3, fp
	mov	r0, lr
	mov	r5, r4
.L2751:
	ldr	r2, [sp, #36]
	cmp	r3, r2
	bge	.L2846
	add	r4, r3, #1
	cmp	r6, r4
	ble	.L2728
	ldrb	sl, [r7, #-2]	@ zero_extendqisi2
	ldrb	lr, [r7, #1]	@ zero_extendqisi2
	ldrb	ip, [r7, #-3]	@ zero_extendqisi2
	ldrb	r1, [r7, #0]	@ zero_extendqisi2
	rsb	lr, lr, sl
	ldrb	r2, [r7, #-1]	@ zero_extendqisi2
	ldrb	r3, [r7, #2]	@ zero_extendqisi2
	rsb	sl, r1, ip
	cmp	lr, #0
	rsb	r3, r3, r2
	rsblt	lr, lr, #0
	cmp	sl, #0
	rsblt	sl, sl, #0
	cmp	r3, #0
	rsblt	r3, r3, #0
	add	r2, lr, sl
	ldr	r1, [sp, #12]
	add	sl, r2, r3
	mvn	lr, r4
	add	ip, lr, r6
	cmp	r1, sl
	and	r3, ip, #1
	bge	.L2847
.L2803:
	add	r9, r4, #1
	cmp	r6, r9
	add	sl, r7, #3
	ble	.L2728
	cmp	r3, #0
	beq	.L2839
	ldrb	r3, [r7, #-1]	@ zero_extendqisi2
	ldrb	lr, [r7, #-2]	@ zero_extendqisi2
	ldrb	r1, [r7, #-3]	@ zero_extendqisi2
	ldrb	ip, [sl, #1]	@ zero_extendqisi2
	ldrb	r2, [sl, #0]	@ zero_extendqisi2
	str	r3, [sp, #8]
	rsb	r2, r2, r1
	rsb	ip, ip, lr
	ldrb	r3, [sl, #2]	@ zero_extendqisi2
	ldr	r1, [sp, #8]
	cmp	ip, #0
	rsblt	ip, ip, #0
	str	r3, [sp, #20]
	cmp	r2, #0
	rsb	r3, r3, r1
	rsblt	r2, r2, #0
	cmp	r3, #0
	rsblt	r3, r3, #0
	add	lr, ip, r2
	ldr	r1, [sp, #12]
	add	r3, lr, r3
	cmp	r1, r3
	bge	.L2848
.L2805:
	add	r9, r9, #1
	cmp	r6, r9
	add	sl, sl, #3
	ble	.L2728
.L2839:
	str	r4, [sp, #20]
	b	.L2731
.L2729:
	ldrb	r4, [r7, #-2]	@ zero_extendqisi2
	ldrb	lr, [r3, #1]	@ zero_extendqisi2
	ldrb	r1, [sl, #3]	@ zero_extendqisi2
	ldrb	ip, [r7, #-3]	@ zero_extendqisi2
	rsb	lr, lr, r4
	ldrb	sl, [r3, #2]	@ zero_extendqisi2
	ldrb	r2, [r7, #-1]	@ zero_extendqisi2
	rsb	r4, r1, ip
	cmp	lr, #0
	rsb	r2, sl, r2
	rsblt	lr, lr, #0
	cmp	r4, #0
	rsblt	r4, r4, #0
	cmp	r2, #0
	rsblt	r2, r2, #0
	add	ip, lr, r4
	ldr	sl, [sp, #12]
	add	r4, ip, r2
	cmp	sl, r4
	add	r9, r9, #1
	bge	.L2849
.L2808:
	add	r9, r9, #1
	cmp	r6, r9
	add	sl, r3, #3
	ble	.L2850
.L2731:
	ldrb	r4, [sl, #2]	@ zero_extendqisi2
	ldrb	lr, [r7, #-2]	@ zero_extendqisi2
	ldrb	r1, [r7, #-3]	@ zero_extendqisi2
	ldrb	ip, [sl, #1]	@ zero_extendqisi2
	ldrb	r2, [sl, #0]	@ zero_extendqisi2
	str	r4, [sp, #44]
	rsb	ip, ip, lr
	ldrb	r4, [r7, #-1]	@ zero_extendqisi2
	ldr	lr, [sp, #44]
	rsb	r2, r2, r1
	cmp	ip, #0
	rsb	r1, lr, r4
	rsblt	ip, ip, #0
	cmp	r2, #0
	rsblt	r2, r2, #0
	cmp	r1, #0
	rsblt	r1, r1, #0
	add	lr, ip, r2
	ldr	r2, [sp, #12]
	add	r4, lr, r1
	cmp	r2, r4
	add	r3, sl, #3
	blt	.L2729
	mov	r0, r5
	mov	r1, #8
	str	r3, [sp, #4]
	bl	png_malloc_warn
	cmp	r0, #0
	ldr	r3, [sp, #4]
	beq	.L2840
	ldr	r1, [r8, r4, asl #2]
	strb	r9, [r0, #5]
	str	r1, [r0, #0]
	strb	fp, [r0, #4]
	str	r0, [r8, r4, asl #2]
	b	.L2729
.L2849:
	mov	r0, r5
	mov	r1, #8
	str	r3, [sp, #4]
	bl	png_malloc_warn
	cmp	r0, #0
	ldr	r3, [sp, #4]
	beq	.L2840
	ldr	ip, [r8, r4, asl #2]
	strb	fp, [r0, #4]
	str	ip, [r0, #0]
	strb	r9, [r0, #5]
	str	r0, [r8, r4, asl #2]
	b	.L2808
.L2850:
	ldr	r4, [sp, #20]
.L2728:
	cmp	r0, #0
	add	fp, fp, #1
	add	r7, r7, #3
	mov	r3, r4
	bne	.L2751
.L2840:
	mov	r4, r5
	mov	sl, #0
.L2733:
	mov	r5, #0
.L2749:
	ldr	r1, [r8, r5]
	cmp	r1, #0
	beq	.L2747
.L2766:
	ldr	sl, [r1, #0]
	mov	r0, r4
	bl	png_free
	subs	r1, sl, #0
	bne	.L2766
.L2747:
	mov	r3, #0
	str	r3, [r8, r5]
	ldr	r0, [sp, #16]
	add	r5, r5, #4
	cmp	r5, r0
	bne	.L2749
	ldr	ip, [sp, #24]
	cmp	r6, ip
	ble	.L2750
	ldr	r2, [sp, #12]
	add	fp, r2, #96
	str	fp, [sp, #12]
	b	.L2727
.L2847:
	mov	r0, r5
	mov	r1, #8
	str	r3, [sp, #4]
	bl	png_malloc_warn
	cmp	r0, #0
	ldr	r3, [sp, #4]
	beq	.L2840
	ldr	r2, [r8, sl, asl #2]
	strb	fp, [r0, #4]
	str	r2, [r0, #0]
	strb	r4, [r0, #5]
	str	r0, [r8, sl, asl #2]
	b	.L2803
.L2846:
	cmp	r0, #0
	mov	r4, r5
	mov	sl, r0
	mov	r5, r0
	beq	.L2733
	mov	r9, #0
.L2746:
	ldr	r7, [r8, r9, asl #2]
	cmp	r7, #0
	bne	.L2767
	b	.L2734
.L2739:
	ldr	lr, [r4, #632]
	ldrb	r2, [lr, r6]	@ zero_extendqisi2
	strb	r1, [r3, r2]
	ldr	r3, [r4, #632]
	ldr	ip, [r4, #628]
	ldrb	lr, [r3, r6]	@ zero_extendqisi2
	ldrb	r0, [ip, r5]	@ zero_extendqisi2
	strb	lr, [r3, r0]
	ldr	r1, [r4, #628]
	strb	r6, [r1, r5]
	ldr	r2, [r4, #632]
	strb	r5, [r2, r6]
.L2735:
	ldr	r1, [sp, #24]
	cmp	r6, r1
	ble	.L2733
	ldr	r7, [r7, #0]
	cmp	r7, #0
	beq	.L2734
.L2767:
	ldr	r3, [r4, #628]
	ldrb	r2, [r7, #4]	@ zero_extendqisi2
	ldrb	r0, [r3, r2]	@ zero_extendqisi2
	cmp	r6, r0
	ble	.L2735
	ldrb	fp, [r7, #5]	@ zero_extendqisi2
	ldrb	r1, [r3, fp]	@ zero_extendqisi2
	cmp	r6, r1
	ble	.L2735
	tst	r6, #1
	movne	r5, r2
	moveq	r5, fp
	ldr	ip, [sp, #32]
	ldrb	r0, [r3, r5]	@ zero_extendqisi2
	moveq	fp, r2
	mov	r1, ip
	mov	r2, #3
	sub	r6, r6, #1
	mla	r1, r2, r6, r1
	mla	r0, r2, r0, ip
	bl	memcpy
	ldr	r3, [sp, #108]
	cmp	r3, #0
	ldr	r3, [r4, #628]
	ldreq	r2, [sp, #108]
	ldrneb	r1, [r3, r5]	@ zero_extendqisi2
	ldreqb	r1, [r3, r5]	@ zero_extendqisi2
	beq	.L2740
	b	.L2739
.L2853:
	ldrb	r0, [r3, fp]	@ zero_extendqisi2
	strb	r0, [r2, ip]
	ldr	r3, [r4, #628]
	ldr	lr, [r4, #524]
	ldrb	r1, [r3, r5]	@ zero_extendqisi2
	ldrb	r0, [r2, lr]	@ zero_extendqisi2
	add	lr, r2, lr
.L2741:
	cmp	r0, r6
	streqb	r1, [lr, #0]
	ldreq	r3, [r4, #628]
	ldreqb	r1, [r3, r5]	@ zero_extendqisi2
	add	r2, r2, #1
.L2740:
	ldr	ip, [sp, #28]
	cmp	r2, ip
	bge	.L2739
	ldr	ip, [r4, #524]
	ldrb	r0, [r2, ip]	@ zero_extendqisi2
	cmp	r0, r1
	add	lr, r2, ip
	bne	.L2741
	b	.L2853
.L2734:
	ldr	r7, [sp, #12]
	add	r9, r9, #1
	cmp	r7, r9
	bge	.L2746
	b	.L2733
.L2845:
	mov	r7, r8
	mov	r3, #0
.L2718:
	ldr	r2, [sp, #28]
	cmp	r3, r2
	bge	.L2852
	ldr	r8, [r4, #524]
	ldrb	r1, [r3, r8]	@ zero_extendqisi2
	cmp	r7, r1
	add	r8, r3, r8
	bgt	.L2719
	mov	r5, #3
	mul	r1, r5, r1
	ldr	lr, [sp, #32]
	add	r0, lr, r1
	ldrb	r5, [lr, r1]	@ zero_extendqisi2
	ldrb	r6, [r0, #1]	@ zero_extendqisi2
	ldrb	ip, [lr, #1]	@ zero_extendqisi2
	ldrb	r1, [lr, #0]	@ zero_extendqisi2
	ldrb	lr, [r0, #2]	@ zero_extendqisi2
	ldr	r0, [sp, #32]
	rsb	ip, ip, r6
	ldrb	r2, [r0, #2]	@ zero_extendqisi2
	rsb	r1, r1, r5
	cmp	ip, #0
	rsb	r2, r2, lr
	rsblt	ip, ip, #0
	cmp	r1, #0
	rsblt	r1, r1, #0
	cmp	r2, #0
	add	ip, ip, r1
	rsblt	r2, r2, #0
	add	ip, ip, r2
	mov	r1, #1
	mov	r2, r0
	mov	r0, #0
	b	.L2720
.L2722:
	ldrb	fp, [r2, #4]	@ zero_extendqisi2
	ldrb	r9, [r2, #3]	@ zero_extendqisi2
	rsb	fp, fp, r6
	ldrb	sl, [r2, #5]	@ zero_extendqisi2
	rsb	r9, r9, r5
	cmp	fp, #0
	rsb	sl, sl, lr
	rsblt	fp, fp, #0
	cmp	r9, #0
	rsblt	r9, r9, #0
	cmp	sl, #0
	rsblt	sl, sl, #0
	add	r9, fp, r9
	add	sl, r9, sl
	cmp	ip, sl
	movgt	r0, r1
	movgt	ip, sl
	add	r1, r1, #1
	add	r2, r2, #3
.L2720:
	cmp	r1, r7
	blt	.L2722
	strb	r0, [r8, #0]
.L2719:
	add	r3, r3, #1
	b	.L2718
.L2708:
	mov	r6, #0
	ldr	r8, [sp, #28]
	ldr	r1, [r4, #624]
	ldr	r9, [sp, #32]
	mov	r5, r6
	mov	sl, #3
.L2710:
	cmp	r5, r7
	bge	.L2714
	ldrb	r3, [r1, r5]	@ zero_extendqisi2
	cmp	r7, r3
	bgt	.L2711
	add	r1, r1, r8
.L2712:
	ldrb	ip, [r1, #-1]!	@ zero_extendqisi2
	cmp	r7, ip
	sub	r8, r8, #1
	ble	.L2712
	mla	r1, sl, r8, r9
	add	r0, r9, r6
	mov	r2, #3
	bl	memcpy
	ldr	r1, [r4, #624]
.L2711:
	add	r5, r5, #1
	add	r6, r6, #3
	b	.L2710
.L2852:
	ldr	r1, [r4, #624]
.L2714:
	mov	r0, r4
	bl	png_free
	mov	r3, #0
	str	r3, [r4, #624]
	str	r7, [sp, #28]
	b	.L2699
.L2848:
	mov	r0, r5
	mov	r1, #8
	str	r3, [sp, #4]
	bl	png_malloc_warn
	cmp	r0, #0
	ldr	r3, [sp, #4]
	beq	.L2840
	ldr	r1, [r8, r3, asl #2]
	strb	fp, [r0, #4]
	str	r1, [r0, #0]
	strb	r9, [r0, #5]
	str	r0, [r8, r3, asl #2]
	b	.L2805
	.size	png_set_dither, .-png_set_dither
	.global	__aeabi_d2iz
	.section	.text.png_set_rgb_to_gray,"ax",%progbits
	.align	2
	.global	png_set_rgb_to_gray
	.hidden	png_set_rgb_to_gray
	.type	png_set_rgb_to_gray, %function
png_set_rgb_to_gray:
	@ Function supports interworking.
	@ args = 8, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	subs	r4, r0, #0
	beq	.L2864
	cmp	r1, #2
	beq	.L2858
	cmp	r1, #3
	beq	.L2859
	cmp	r1, #1
	ldreq	r0, [r4, #140]
	orreq	r1, r0, #6291456
	streq	r1, [r4, #140]
.L2856:
	ldrb	r0, [r4, #322]	@ zero_extendqisi2
	cmp	r0, #3
	ldreq	r0, [r4, #140]
	orreq	r0, r0, #4096
	streq	r0, [r4, #140]
	mov	r1, r3
	mov	r0, r2
	bl	__aeabi_d2f
	bl	__aeabi_f2d
	mov	r3, #1073741824
	add	ip, r3, #16252928
	add	r3, ip, #27136
	mov	r2, #0
	mov	r5, #1069547520
	bl	__aeabi_dmul
	add	r3, r5, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2iz
	mov	r5, r0
	add	r1, sp, #16
	ldmia	r1, {r0-r1}
	bl	__aeabi_d2f
	bl	__aeabi_f2d
	mov	r3, #1073741824
	add	ip, r3, #16252928
	add	r3, ip, #27136
	mov	r2, #0
	bl	__aeabi_dmul
	mov	r3, #1069547520
	mov	r2, #0
	add	r3, r3, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2iz
	orrs	r2, r0, r5
	bmi	.L2865
	mov	ip, #99328
	add	r1, ip, #668
	add	lr, r1, #3
	add	r2, r0, r5
	cmp	r2, lr
	bgt	.L2863
	ldr	ip, .L2866
	mov	r5, r5, asl #15
	mov	r3, r0, asl #15
	mov	r5, r5, lsr #5
	umull	r2, r5, ip, r5
	mov	r1, r3, lsr #5
	umull	r2, r1, ip, r1
	mov	r0, r5, lsr #7
	mov	r1, r1, lsr #7
	rsb	lr, r0, #32768
	rsb	r2, r1, lr
	mov	r3, r2, asl #16
	mov	ip, r3, lsr #16
.L2862:
	mov	lr, #584
	mov	r3, #580
	add	r2, lr, #2
	add	r3, r3, #2
	strh	ip, [r4, r2]	@ movhi
	mov	ip, #584
	strh	r0, [r4, r3]	@ movhi
	strh	r1, [r4, ip]	@ movhi
.L2864:
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L2863:
	mov	r0, r4
	ldr	r1, .L2866+4
	bl	png_warning
.L2865:
	mov	lr, #2352
	mov	r2, #23296
	mov	r0, #6912
	add	ip, lr, #14
	add	r1, r2, #138
	add	r0, r0, #56
	b	.L2862
.L2858:
	ldr	ip, [r4, #140]
	orr	r5, ip, #4194304
	str	r5, [r4, #140]
	b	.L2856
.L2859:
	ldr	r1, [r4, #140]
	orr	lr, r1, #2097152
	str	lr, [r4, #140]
	b	.L2856
.L2867:
	.align	2
.L2866:
	.word	175921861
	.word	.LC0
	.size	png_set_rgb_to_gray, .-png_set_rgb_to_gray
	.section	.text.png_do_read_transformations,"ax",%progbits
	.align	2
	.global	png_do_read_transformations
	.hidden	png_do_read_transformations
	.type	png_do_read_transformations, %function
png_do_read_transformations:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 64
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	r3, [r0, #264]
	cmp	r3, #0
	sub	sp, sp, #100
	mov	r4, r0
	beq	.L3271
.L2869:
	ldr	r0, [r4, #136]
	tst	r0, #64
	beq	.L3272
.L2870:
	ldr	r3, [r4, #140]
	tst	r3, #4096
	beq	.L2871
	ldrb	r2, [r4, #292]	@ zero_extendqisi2
	cmp	r2, #3
	beq	.L3273
	mov	lr, #308
	add	r2, lr, #2
	ldrh	r0, [r4, r2]
	cmp	r0, #0
	beq	.L2873
	tst	r3, #33554432
	bne	.L3274
.L2873:
	ldr	ip, [r4, #264]
	add	r0, r4, #284
	add	r1, ip, #1
	mov	r2, #0
	bl	png_do_expand
.L2871:
	ldr	r3, [r4, #136]
	ands	r2, r3, #4194304
	bne	.L3275
.L2874:
	ldr	r3, [r4, #140]
	tst	r3, #6291456
	mov	r0, r3
	bne	.L3276
.L2875:
	tst	r3, #16384
	beq	.L2878
	ldr	r2, [r4, #132]
	tst	r2, #2048
	beq	.L3277
.L2878:
	tst	r3, #128
	beq	.L2879
	mov	r1, #308
	add	r2, r1, #2
	ldrh	ip, [r4, r2]
	cmp	ip, #0
	bne	.L2880
	ldrb	r2, [r4, #322]	@ zero_extendqisi2
	tst	r2, #4
	bne	.L2880
.L2879:
	tst	r3, #8192
	beq	.L2881
	tst	r3, #128
	ldreqb	r2, [r4, #322]	@ zero_extendqisi2
	beq	.L2883
	mov	ip, #308
	add	r1, ip, #2
	ldrh	r2, [r4, r1]
	cmp	r2, #0
	beq	.L3278
.L2881:
	tst	r3, #1024
	beq	.L2884
	ldrb	r2, [r4, #293]	@ zero_extendqisi2
	cmp	r2, #16
	ldr	r1, [r4, #264]
	beq	.L3279
.L2884:
	tst	r3, #64
	bne	.L3280
.L2887:
	tst	r3, #32
	bne	.L3281
.L2904:
	tst	r3, #8
	bne	.L3282
.L2905:
	tst	r3, #4
	bne	.L3283
.L2906:
	tst	r3, #1
	bne	.L3284
.L2922:
	tst	r0, #65536
	bne	.L3285
.L2923:
	tst	r0, #16384
	beq	.L2924
	ldr	r3, [r4, #132]
	tst	r3, #2048
	bne	.L3286
.L2924:
	tst	r0, #32768
	bne	.L3287
	tst	r0, #524288
	bne	.L3288
.L2926:
	tst	r0, #131072
	bne	.L3289
.L2934:
	tst	r0, #16
	bne	.L3290
.L2942:
	tst	r0, #1048576
	beq	.L2950
	ldr	r3, [r4, #116]
	cmp	r3, #0
	beq	.L2944
	ldr	r1, [r4, #264]
	mov	r0, r4
	add	r2, r1, #1
	add	r1, r4, #284
	mov	lr, pc
	bx	r3
.L2944:
	ldrb	r1, [r4, #128]	@ zero_extendqisi2
	ldrb	r0, [r4, #129]	@ zero_extendqisi2
	cmp	r1, #0
	strneb	r1, [r4, #293]
	cmp	r0, #0
	ldreqb	r0, [r4, #294]	@ zero_extendqisi2
	ldrb	r2, [r4, #293]	@ zero_extendqisi2
	strneb	r0, [r4, #294]
	mul	r0, r2, r0
	ldr	ip, [r4, #284]
	and	r3, r0, #255
	cmp	r3, #7
	strb	r3, [r4, #295]
	mulls	r3, ip, r3
	movhi	r3, r3, lsr #3
	mulhi	r3, ip, r3
	addls	r3, r3, #7
	movls	r3, r3, lsr #3
	str	r3, [r4, #288]
.L2950:
	add	sp, sp, #100
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L3278:
	ldrb	r2, [r4, #322]	@ zero_extendqisi2
	tst	r2, #4
	bne	.L2881
.L2883:
	cmp	r2, #3
	beq	.L2881
	ldr	r0, [r4, #264]
	ldr	lr, [r4, #372]
	ldr	r3, [r4, #396]
	ldr	r2, [r4, #384]
	add	r1, r0, #1
	add	r0, r4, #284
	str	lr, [sp, #0]
	bl	png_do_gamma
	ldr	r3, [r4, #140]
	mov	r0, r3
	b	.L2881
.L2880:
	ldr	r3, [r4, #264]
	ldr	r7, [r4, #384]
	ldr	r6, [r4, #388]
	add	r5, r4, #392
	ldmia	r5, {r5, lr}	@ phole ldm
	add	r8, r4, #400
	ldmia	r8, {r8, sl}	@ phole ldm
	ldr	r9, [r4, #372]
	add	r0, r4, #348
	add	ip, r0, #2
	add	r1, r3, #1
	add	r0, r4, #284
	add	r3, r4, #340
	add	r2, r4, #424
	str	ip, [sp, #0]
	str	r7, [sp, #4]
	str	r6, [sp, #8]
	str	r5, [sp, #12]
	str	lr, [sp, #16]
	str	r8, [sp, #20]
	str	sl, [sp, #24]
	str	r9, [sp, #28]
	bl	png_do_background
	ldr	r3, [r4, #140]
	mov	r0, r3
	b	.L2879
.L3285:
	ldr	lr, [r4, #264]
	add	r0, r4, #284
	add	r1, lr, #1
	bl	png_do_packswap
	ldr	r0, [r4, #140]
	b	.L2923
.L3284:
	ldr	r2, [r4, #264]
	add	r0, r4, #284
	add	r1, r2, #1
	bl	png_do_bgr
	ldr	r0, [r4, #140]
	b	.L2922
.L3283:
	ldrb	r1, [r4, #293]	@ zero_extendqisi2
	cmp	r1, #7
	ldr	ip, [r4, #264]
	bhi	.L2906
	cmp	r1, #2
	add	ip, ip, #1
	ldr	r2, [r4, #284]
	beq	.L2909
	cmp	r1, #4
	beq	.L2910
	cmp	r1, #1
	beq	.L3291
.L2907:
	ldrb	r1, [r4, #294]	@ zero_extendqisi2
	mul	r2, r1, r2
	mov	r0, #8
	mov	ip, r1, asl #3
	strb	r0, [r4, #293]
	strb	ip, [r4, #295]
	str	r2, [r4, #288]
	mov	r0, r3
	b	.L2906
.L3282:
	ldr	r1, [r4, #264]
	add	r0, r4, #412
	add	r2, r0, #1
	add	r1, r1, #1
	add	r0, r4, #284
	bl	png_do_unshift
	ldr	r3, [r4, #140]
	mov	r0, r3
	b	.L2905
.L3281:
	ldr	lr, [r4, #264]
	add	r0, r4, #284
	add	r1, lr, #1
	bl	png_do_invert
	ldr	r3, [r4, #140]
	mov	r0, r3
	b	.L2904
.L3280:
	ldr	r2, [r4, #520]
	ldrb	ip, [r4, #292]	@ zero_extendqisi2
	subs	r5, r2, #0
	movne	r5, #1
	cmp	ip, #2
	movne	r3, #0
	andeq	r3, r5, #1
	cmp	r3, #0
	ldr	r1, [r4, #524]
	ldr	r3, [r4, #264]
	ldr	r0, [r4, #284]
	beq	.L2888
	ldrb	r6, [r4, #293]	@ zero_extendqisi2
	cmp	r6, #8
	beq	.L3292
.L2888:
	cmp	ip, #6
	movne	r5, #0
	andeq	r5, r5, #1
	cmp	r5, #0
	beq	.L2894
	ldrb	r5, [r4, #293]	@ zero_extendqisi2
	cmp	r5, #8
	beq	.L3293
.L2894:
	subs	r2, r1, #0
	movne	r2, #1
	cmp	ip, #3
	movne	r2, #0
	cmp	r2, #0
	beq	.L3185
	ldrb	ip, [r4, #293]	@ zero_extendqisi2
	cmp	ip, #8
	beq	.L2900
.L3185:
	ldr	r0, [r4, #288]
.L2893:
	cmp	r0, #0
	beq	.L2903
.L3270:
	ldr	r3, [r4, #140]
	mov	r0, r3
	b	.L2887
.L3287:
	ldr	r1, [r4, #264]
	mov	r2, #328
	add	r0, r2, #2
	ldrh	r2, [r4, r0]
	add	r1, r1, #1
	add	r0, r4, #284
	ldr	r3, [r4, #136]
	bl	png_do_read_filler
	ldr	r0, [r4, #140]
	tst	r0, #524288
	beq	.L2926
.L3288:
	ldrb	r1, [r4, #292]	@ zero_extendqisi2
	ldr	r3, [r4, #264]
	cmp	r1, #6
	add	r3, r3, #1
	ldr	r2, [r4, #284]
	beq	.L3294
	cmp	r1, #4
	bne	.L2926
	ldrb	ip, [r4, #293]	@ zero_extendqisi2
	cmp	ip, #8
	beq	.L3295
	cmp	r2, #0
	ldr	r1, [r4, #288]
	beq	.L2926
	add	r3, r3, r1
	ldrb	r0, [r3, #-1]	@ zero_extendqisi2
	ldrb	r1, [r3, #-2]	@ zero_extendqisi2
	mvn	ip, r0
	cmp	r2, #1
	mvn	r0, r1
	sub	r1, r2, #1
	strb	r0, [r3, #-2]
	strb	ip, [r3, #-1]
	and	r1, r1, #3
	sub	r3, r3, #4
	mov	r0, #1
	bls	.L3195
	cmp	r1, #0
	beq	.L2933
	cmp	r1, #1
	beq	.L3163
	cmp	r1, #2
	beq	.L3164
	ldrb	r1, [r3, #-1]	@ zero_extendqisi2
	ldrb	ip, [r3, #-2]	@ zero_extendqisi2
	mvn	r0, r1
	mvn	r1, ip
	strb	r0, [r3, #-1]
	strb	r1, [r3, #-2]
	mov	r0, #2
	sub	r3, r3, #4
.L3164:
	ldrb	ip, [r3, #-1]	@ zero_extendqisi2
	ldrb	r1, [r3, #-2]	@ zero_extendqisi2
	mvn	ip, ip
	mvn	r1, r1
	strb	ip, [r3, #-1]
	strb	r1, [r3, #-2]
	add	r0, r0, #1
	sub	r3, r3, #4
.L3163:
	ldrb	ip, [r3, #-1]	@ zero_extendqisi2
	ldrb	r1, [r3, #-2]	@ zero_extendqisi2
	add	r0, r0, #1
	mvn	ip, ip
	mvn	r1, r1
	cmp	r2, r0
	strb	ip, [r3, #-1]
	strb	r1, [r3, #-2]
	sub	r3, r3, #4
	bls	.L3195
.L2933:
	ldrb	r1, [r3, #-1]	@ zero_extendqisi2
	ldrb	r5, [r3, #-2]	@ zero_extendqisi2
	mvn	ip, r1
	mvn	r1, r5
	strb	r1, [r3, #-2]
	strb	ip, [r3, #-1]
	sub	r1, r3, #4
	ldrb	r5, [r1, #-1]	@ zero_extendqisi2
	ldrb	ip, [r1, #-2]	@ zero_extendqisi2
	mvn	r5, r5
	mvn	ip, ip
	strb	r5, [r1, #-1]
	strb	ip, [r1, #-2]
	sub	r1, r1, #4
	ldrb	ip, [r1, #-1]	@ zero_extendqisi2
	ldrb	r5, [r1, #-2]	@ zero_extendqisi2
	mvn	ip, ip
	mvn	r5, r5
	strb	r5, [r1, #-2]
	strb	ip, [r1, #-1]
	sub	r1, r3, #12
	ldrb	ip, [r1, #-1]	@ zero_extendqisi2
	ldrb	r5, [r1, #-2]	@ zero_extendqisi2
	add	r0, r0, #4
	mvn	ip, ip
	mvn	r5, r5
	cmp	r2, r0
	strb	r5, [r1, #-2]
	strb	ip, [r1, #-1]
	sub	r3, r3, #16
	bhi	.L2933
.L3195:
	ldr	r0, [r4, #140]
.L3302:
	tst	r0, #131072
	beq	.L2934
.L3289:
	ldrb	r3, [r4, #292]	@ zero_extendqisi2
	ldr	r2, [r4, #264]
	cmp	r3, #6
	add	r2, r2, #1
	ldr	r1, [r4, #284]
	beq	.L3296
	cmp	r3, #4
	bne	.L2934
	ldrb	r3, [r4, #293]	@ zero_extendqisi2
	cmp	r3, #8
	beq	.L3297
	cmp	r1, #0
	ldr	r3, [r4, #288]
	beq	.L2934
	add	r2, r2, r3
	ldrb	r5, [r2, #-1]	@ zero_extendqisi2
	ldrb	r7, [r2, #-3]	@ zero_extendqisi2
	ldrb	ip, [r2, #-2]	@ zero_extendqisi2
	ldrb	r6, [r2, #-4]	@ zero_extendqisi2
	mov	r3, r2
	sub	r0, r1, #1
	cmp	r1, #1
	strb	r7, [r2, #-1]
	strb	r6, [r2, #-2]
	strb	r5, [r2, #-3]
	strb	ip, [r3, #-4]!
	and	r2, r0, #3
	mov	ip, #1
	bls	.L3199
	cmp	r2, #0
	beq	.L2941
	cmp	r2, #1
	beq	.L3155
	cmp	r2, #2
	beq	.L3156
	ldrb	r2, [r3, #-1]	@ zero_extendqisi2
	ldrb	r5, [r3, #-3]	@ zero_extendqisi2
	ldrb	ip, [r3, #-4]	@ zero_extendqisi2
	ldrb	r6, [r3, #-2]	@ zero_extendqisi2
	strb	r5, [r3, #-1]
	strb	ip, [r3, #-2]
	strb	r2, [r3, #-3]
	strb	r6, [r3, #-4]!
	mov	ip, #2
.L3156:
	ldrb	r0, [r3, #-1]	@ zero_extendqisi2
	ldrb	r6, [r3, #-3]	@ zero_extendqisi2
	ldrb	r2, [r3, #-2]	@ zero_extendqisi2
	ldrb	r5, [r3, #-4]	@ zero_extendqisi2
	strb	r6, [r3, #-1]
	strb	r5, [r3, #-2]
	strb	r0, [r3, #-3]
	strb	r2, [r3, #-4]!
	add	ip, ip, #1
.L3155:
	ldrb	r0, [r3, #-1]	@ zero_extendqisi2
	ldrb	r6, [r3, #-3]	@ zero_extendqisi2
	ldrb	r2, [r3, #-2]	@ zero_extendqisi2
	ldrb	r5, [r3, #-4]	@ zero_extendqisi2
	add	ip, ip, #1
	cmp	r1, ip
	strb	r6, [r3, #-1]
	strb	r5, [r3, #-2]
	strb	r0, [r3, #-3]
	strb	r2, [r3, #-4]!
	bls	.L3199
.L2941:
	ldrb	r2, [r3, #-1]	@ zero_extendqisi2
	ldrb	r6, [r3, #-3]	@ zero_extendqisi2
	ldrb	r8, [r3, #-2]	@ zero_extendqisi2
	ldrb	r5, [r3, #-4]	@ zero_extendqisi2
	mov	r0, r3
	strb	r2, [r3, #-3]
	strb	r6, [r3, #-1]
	strb	r5, [r3, #-2]
	strb	r8, [r0, #-4]!
	ldrb	r6, [r0, #-1]	@ zero_extendqisi2
	ldrb	r8, [r0, #-3]	@ zero_extendqisi2
	ldrb	r5, [r0, #-2]	@ zero_extendqisi2
	ldrb	r7, [r0, #-4]	@ zero_extendqisi2
	mov	r2, r0
	strb	r8, [r0, #-1]
	strb	r7, [r0, #-2]
	strb	r6, [r0, #-3]
	strb	r5, [r2, #-4]!
	ldrb	r0, [r2, #-1]	@ zero_extendqisi2
	ldrb	r7, [r2, #-2]	@ zero_extendqisi2
	ldrb	r6, [r2, #-3]	@ zero_extendqisi2
	ldrb	r5, [r2, #-4]	@ zero_extendqisi2
	strb	r6, [r2, #-1]
	strb	r7, [r2, #-4]
	strb	r5, [r2, #-2]
	strb	r0, [r2, #-3]
	sub	r2, r3, #12
	ldrb	r0, [r2, #-1]	@ zero_extendqisi2
	ldrb	r7, [r2, #-2]	@ zero_extendqisi2
	ldrb	r6, [r2, #-3]	@ zero_extendqisi2
	ldrb	r5, [r2, #-4]	@ zero_extendqisi2
	add	ip, ip, #4
	cmp	r1, ip
	strb	r7, [r2, #-4]
	strb	r6, [r2, #-1]
	strb	r5, [r2, #-2]
	strb	r0, [r2, #-3]
	sub	r3, r3, #16
	bhi	.L2941
.L3199:
	ldr	r0, [r4, #140]
.L3300:
	tst	r0, #16
	beq	.L2942
.L3290:
	ldr	lr, [r4, #264]
	add	r0, r4, #284
	add	r1, lr, #1
	bl	png_do_swap
	ldr	r0, [r4, #140]
	b	.L2942
.L3272:
	mov	r0, r4
	ldr	r1, .L3306
	bl	png_error
	b	.L2870
.L3276:
	ldr	r2, [r4, #264]
	mov	r0, r4
	add	r2, r2, #1
	add	r1, r4, #284
	bl	png_do_rgb_to_gray
	cmp	r0, #0
	beq	.L3269
	ldr	r3, [r4, #140]
	and	r2, r3, #6291456
	mov	lr, #1
	cmp	r2, #4194304
	strb	lr, [r4, #580]
	mov	r0, r3
	beq	.L3298
.L2877:
	cmp	r2, #2097152
	bne	.L2875
	mov	r0, r4
	ldr	r1, .L3306+12
	bl	png_error
.L3269:
	ldr	r3, [r4, #140]
	mov	r0, r3
	b	.L2875
.L3275:
	ldr	r0, [r4, #264]
	orr	r2, r2, #128
	add	r1, r0, #1
	add	r0, r4, #284
	bl	png_do_strip_filler
	b	.L2874
.L3286:
	ldr	ip, [r4, #264]
	add	r0, r4, #284
	add	r1, ip, #1
	bl	png_do_gray_to_rgb
	ldr	r0, [r4, #140]
	b	.L2924
.L3277:
	ldr	r1, [r4, #264]
	add	r0, r4, #284
	add	r1, r1, #1
	bl	png_do_gray_to_rgb
	ldr	r3, [r4, #140]
	mov	r0, r3
	b	.L2878
.L3279:
	ldrb	r2, [r4, #294]	@ zero_extendqisi2
	ldr	r0, [r4, #284]
	muls	r5, r0, r2
	beq	.L2885
	sub	r3, r5, #1
	and	r0, r3, #3
	cmp	r5, #1
	mov	r3, #2
	add	r2, r1, r3
	bls	.L3182
	cmp	r0, #0
	beq	.L2886
	cmp	r0, #1
	beq	.L3180
	cmp	r0, #2
	ldrneb	r3, [r2, #1]	@ zero_extendqisi2
	strneb	r3, [r1, #2]
	addne	r2, r2, #2
	ldrb	r0, [r2, #1]	@ zero_extendqisi2
	movne	r3, #3
	strb	r0, [r1, r3]
	add	r2, r2, #2
	add	r3, r3, #1
.L3180:
	ldrb	r0, [r2, #1]	@ zero_extendqisi2
	strb	r0, [r1, r3]
	add	r3, r3, #1
	sub	ip, r3, #1
	cmp	r5, ip
	add	r2, r2, #2
	bls	.L3182
.L2886:
	ldrb	r0, [r2, #1]	@ zero_extendqisi2
	strb	r0, [r1, r3]
	add	ip, r2, #2
	ldrb	r6, [ip, #1]	@ zero_extendqisi2
	add	r0, r3, #1
	strb	r6, [r1, r0]
	ldrb	ip, [ip, #3]	@ zero_extendqisi2
	add	r6, r0, #1
	strb	ip, [r1, r6]
	add	r0, r3, #3
	add	r3, r3, #4
	ldrb	ip, [r2, #7]	@ zero_extendqisi2
	sub	r6, r3, #1
	cmp	r5, r6
	strb	ip, [r1, r0]
	add	r2, r2, #8
	bhi	.L2886
.L3182:
	ldr	r3, [r4, #140]
	ldrb	r2, [r4, #294]	@ zero_extendqisi2
	ldr	r0, [r4, #284]
.L2885:
	mul	r0, r2, r0
	mov	r1, #8
	mov	r2, r2, asl #3
	str	r0, [r4, #288]
	strb	r1, [r4, #293]
	strb	r2, [r4, #295]
	mov	r0, r3
	b	.L2884
.L3273:
	mov	r3, #308
	add	ip, r3, #2
	ldr	r1, [r4, #264]
	ldrh	lr, [r4, ip]
	ldr	r2, [r4, #304]
	ldr	r3, [r4, #420]
	add	r1, r1, #1
	add	r0, r4, #284
	str	lr, [sp, #0]
	bl	png_do_expand_palette
	b	.L2871
.L3274:
	ldr	r1, [r4, #264]
	add	r0, r4, #284
	add	r1, r1, #1
	add	r2, r4, #424
	bl	png_do_expand
	b	.L2871
.L2903:
	mov	r0, r4
	ldr	r1, .L3306+4
	bl	png_error
	b	.L3270
.L3296:
	ldrb	r3, [r4, #293]	@ zero_extendqisi2
	cmp	r3, #8
	beq	.L3299
	cmp	r1, #0
	ldr	r3, [r4, #288]
	beq	.L2934
	add	r2, r2, r3
	ldrb	ip, [r2, #-1]	@ zero_extendqisi2
	ldrb	r9, [r2, #-3]	@ zero_extendqisi2
	ldrb	sl, [r2, #-4]	@ zero_extendqisi2
	ldrb	r8, [r2, #-5]	@ zero_extendqisi2
	ldrb	r7, [r2, #-6]	@ zero_extendqisi2
	ldrb	r6, [r2, #-7]	@ zero_extendqisi2
	ldrb	fp, [r2, #-2]	@ zero_extendqisi2
	ldrb	r5, [r2, #-8]	@ zero_extendqisi2
	mov	r3, r2
	sub	r0, r1, #1
	cmp	r1, #1
	strb	r9, [r2, #-1]
	strb	sl, [r2, #-2]
	strb	r8, [r2, #-3]
	strb	r7, [r2, #-4]
	strb	r6, [r2, #-5]
	strb	r5, [r2, #-6]
	strb	ip, [r2, #-7]
	and	r2, r0, #3
	strb	fp, [r3, #-8]!
	mov	r0, #1
	bls	.L3199
	cmp	r2, #0
	beq	.L3242
	cmp	r2, #1
	beq	.L3151
	cmp	r2, #2
	beq	.L3152
	ldrb	r2, [r3, #-1]	@ zero_extendqisi2
	ldrb	sl, [r3, #-3]	@ zero_extendqisi2
	ldrb	r8, [r3, #-4]	@ zero_extendqisi2
	ldrb	r7, [r3, #-5]	@ zero_extendqisi2
	ldrb	r6, [r3, #-6]	@ zero_extendqisi2
	ldrb	r5, [r3, #-7]	@ zero_extendqisi2
	ldrb	r9, [r3, #-2]	@ zero_extendqisi2
	ldrb	ip, [r3, #-8]	@ zero_extendqisi2
	strb	sl, [r3, #-1]
	strb	r8, [r3, #-2]
	strb	r7, [r3, #-3]
	strb	r6, [r3, #-4]
	strb	r5, [r3, #-5]
	strb	ip, [r3, #-6]
	strb	r2, [r3, #-7]
	strb	r9, [r3, #-8]!
	mov	r0, #2
.L3152:
	ldrb	ip, [r3, #-1]	@ zero_extendqisi2
	ldrb	r9, [r3, #-3]	@ zero_extendqisi2
	ldrb	sl, [r3, #-4]	@ zero_extendqisi2
	ldrb	r8, [r3, #-5]	@ zero_extendqisi2
	ldrb	r7, [r3, #-6]	@ zero_extendqisi2
	ldrb	r6, [r3, #-7]	@ zero_extendqisi2
	ldrb	r2, [r3, #-2]	@ zero_extendqisi2
	ldrb	r5, [r3, #-8]	@ zero_extendqisi2
	strb	r9, [r3, #-1]
	strb	sl, [r3, #-2]
	strb	r8, [r3, #-3]
	strb	r7, [r3, #-4]
	strb	r6, [r3, #-5]
	strb	r5, [r3, #-6]
	strb	ip, [r3, #-7]
	strb	r2, [r3, #-8]!
	add	r0, r0, #1
.L3151:
	ldrb	ip, [r3, #-1]	@ zero_extendqisi2
	ldrb	r9, [r3, #-3]	@ zero_extendqisi2
	ldrb	sl, [r3, #-4]	@ zero_extendqisi2
	ldrb	r8, [r3, #-5]	@ zero_extendqisi2
	ldrb	r7, [r3, #-6]	@ zero_extendqisi2
	ldrb	r6, [r3, #-7]	@ zero_extendqisi2
	ldrb	r2, [r3, #-2]	@ zero_extendqisi2
	ldrb	r5, [r3, #-8]	@ zero_extendqisi2
	add	r0, r0, #1
	cmp	r1, r0
	strb	r9, [r3, #-1]
	strb	sl, [r3, #-2]
	strb	r8, [r3, #-3]
	strb	r7, [r3, #-4]
	strb	r6, [r3, #-5]
	strb	r5, [r3, #-6]
	strb	ip, [r3, #-7]
	strb	r2, [r3, #-8]!
	bls	.L3199
.L3242:
	str	r4, [sp, #36]
	mov	fp, r1
.L2938:
	ldrb	r2, [r3, #-1]	@ zero_extendqisi2
	ldrb	r8, [r3, #-3]	@ zero_extendqisi2
	ldrb	r7, [r3, #-4]	@ zero_extendqisi2
	ldrb	r6, [r3, #-5]	@ zero_extendqisi2
	ldrb	r5, [r3, #-6]	@ zero_extendqisi2
	ldrb	r4, [r3, #-7]	@ zero_extendqisi2
	ldrb	r9, [r3, #-2]	@ zero_extendqisi2
	ldrb	ip, [r3, #-8]	@ zero_extendqisi2
	mov	r1, r3
	strb	r2, [r3, #-7]
	strb	r8, [r3, #-1]
	strb	r7, [r3, #-2]
	strb	r6, [r3, #-3]
	strb	r5, [r3, #-4]
	strb	r4, [r3, #-5]
	strb	ip, [r3, #-6]
	strb	r9, [r1, #-8]!
	ldrb	r4, [r1, #-1]	@ zero_extendqisi2
	ldrb	sl, [r1, #-4]	@ zero_extendqisi2
	ldrb	r8, [r1, #-5]	@ zero_extendqisi2
	ldrb	r7, [r1, #-6]	@ zero_extendqisi2
	ldrb	r6, [r1, #-7]	@ zero_extendqisi2
	ldrb	r9, [r1, #-3]	@ zero_extendqisi2
	ldrb	ip, [r1, #-2]	@ zero_extendqisi2
	ldrb	r5, [r1, #-8]	@ zero_extendqisi2
	mov	r2, r1
	strb	r7, [r1, #-4]
	strb	r6, [r1, #-5]
	strb	r5, [r1, #-6]
	strb	r9, [r1, #-1]
	strb	sl, [r1, #-2]
	strb	r8, [r1, #-3]
	strb	r4, [r1, #-7]
	strb	ip, [r2, #-8]!
	ldrb	r4, [r2, #-3]	@ zero_extendqisi2
	ldrb	sl, [r2, #-1]	@ zero_extendqisi2
	ldrb	r8, [r2, #-2]	@ zero_extendqisi2
	strb	r4, [r2, #-1]
	ldrb	r7, [r2, #-4]	@ zero_extendqisi2
	ldrb	r6, [r2, #-5]	@ zero_extendqisi2
	ldrb	r5, [r2, #-6]	@ zero_extendqisi2
	ldrb	ip, [r2, #-7]	@ zero_extendqisi2
	ldrb	r1, [r2, #-8]	@ zero_extendqisi2
	strb	r7, [r2, #-2]
	strb	r8, [r2, #-8]
	strb	r6, [r2, #-3]
	strb	r5, [r2, #-4]
	strb	ip, [r2, #-5]
	strb	r1, [r2, #-6]
	strb	sl, [r2, #-7]
	sub	r2, r3, #24
	ldrb	r1, [r2, #-1]	@ zero_extendqisi2
	ldrb	sl, [r2, #-2]	@ zero_extendqisi2
	ldrb	r8, [r2, #-3]	@ zero_extendqisi2
	ldrb	r7, [r2, #-4]	@ zero_extendqisi2
	ldrb	r6, [r2, #-5]	@ zero_extendqisi2
	ldrb	r5, [r2, #-6]	@ zero_extendqisi2
	ldrb	r4, [r2, #-7]	@ zero_extendqisi2
	ldrb	ip, [r2, #-8]	@ zero_extendqisi2
	add	r0, r0, #4
	cmp	fp, r0
	strb	sl, [r2, #-8]
	strb	r8, [r2, #-1]
	strb	r7, [r2, #-2]
	strb	r6, [r2, #-3]
	strb	r5, [r2, #-4]
	strb	r4, [r2, #-5]
	strb	ip, [r2, #-6]
	strb	r1, [r2, #-7]
	sub	r3, r3, #32
	bhi	.L2938
	ldr	r4, [sp, #36]
	ldr	r0, [r4, #140]
	b	.L3300
.L3294:
	ldrb	ip, [r4, #293]	@ zero_extendqisi2
	cmp	ip, #8
	beq	.L3301
	cmp	r2, #0
	ldr	r1, [r4, #288]
	beq	.L2926
	add	r3, r3, r1
	ldrb	r0, [r3, #-1]	@ zero_extendqisi2
	ldrb	r1, [r3, #-2]	@ zero_extendqisi2
	mvn	ip, r0
	cmp	r2, #1
	mvn	r0, r1
	sub	r1, r2, #1
	strb	r0, [r3, #-2]
	strb	ip, [r3, #-1]
	and	r1, r1, #3
	sub	r3, r3, #8
	mov	r0, #1
	bls	.L3195
	cmp	r1, #0
	beq	.L2930
	cmp	r1, #1
	beq	.L3159
	cmp	r1, #2
	beq	.L3160
	ldrb	r0, [r3, #-1]	@ zero_extendqisi2
	ldrb	r1, [r3, #-2]	@ zero_extendqisi2
	mvn	ip, r0
	mvn	r1, r1
	strb	ip, [r3, #-1]
	strb	r1, [r3, #-2]
	mov	r0, #2
	sub	r3, r3, #8
.L3160:
	ldrb	ip, [r3, #-1]	@ zero_extendqisi2
	ldrb	r1, [r3, #-2]	@ zero_extendqisi2
	mvn	ip, ip
	mvn	r1, r1
	strb	ip, [r3, #-1]
	strb	r1, [r3, #-2]
	add	r0, r0, #1
	sub	r3, r3, #8
.L3159:
	ldrb	ip, [r3, #-1]	@ zero_extendqisi2
	ldrb	r1, [r3, #-2]	@ zero_extendqisi2
	add	r0, r0, #1
	mvn	ip, ip
	mvn	r1, r1
	cmp	r2, r0
	strb	ip, [r3, #-1]
	strb	r1, [r3, #-2]
	sub	r3, r3, #8
	bls	.L3195
.L2930:
	ldrb	r1, [r3, #-1]	@ zero_extendqisi2
	ldrb	r5, [r3, #-2]	@ zero_extendqisi2
	mvn	ip, r1
	mvn	r1, r5
	strb	r1, [r3, #-2]
	strb	ip, [r3, #-1]
	sub	r1, r3, #8
	ldrb	r5, [r1, #-1]	@ zero_extendqisi2
	ldrb	ip, [r1, #-2]	@ zero_extendqisi2
	mvn	r5, r5
	mvn	ip, ip
	strb	r5, [r1, #-1]
	strb	ip, [r1, #-2]
	sub	r1, r1, #8
	ldrb	ip, [r1, #-1]	@ zero_extendqisi2
	ldrb	r5, [r1, #-2]	@ zero_extendqisi2
	mvn	ip, ip
	mvn	r5, r5
	strb	r5, [r1, #-2]
	strb	ip, [r1, #-1]
	sub	r1, r3, #24
	ldrb	ip, [r1, #-1]	@ zero_extendqisi2
	ldrb	r5, [r1, #-2]	@ zero_extendqisi2
	add	r0, r0, #4
	mvn	ip, ip
	mvn	r5, r5
	cmp	r2, r0
	strb	r5, [r1, #-2]
	strb	ip, [r1, #-1]
	sub	r3, r3, #32
	bhi	.L2930
	ldr	r0, [r4, #140]
	b	.L3302
.L3271:
	ldrb	ip, [r0, #320]	@ zero_extendqisi2
	add	r5, sp, #44
	ldr	r3, [r0, #256]
	mov	r1, #50
	mov	r0, r5
	ldr	r2, .L3306+8
	str	ip, [sp, #0]
	bl	snprintf
	mov	r0, r4
	mov	r1, r5
	bl	png_error
	b	.L2869
.L2900:
	cmp	r0, #0
	beq	.L3185
	ldrb	ip, [r3, #1]	@ zero_extendqisi2
	ldrb	r2, [r1, ip]	@ zero_extendqisi2
	cmp	r0, #1
	sub	ip, r0, #1
	strb	r2, [r3, #1]
	and	ip, ip, #3
	mov	r2, #2
	bls	.L3185
	cmp	ip, #0
	beq	.L2902
	cmp	ip, #1
	beq	.L3178
	cmp	ip, #2
	ldrneb	r2, [r3, #2]	@ zero_extendqisi2
	ldrneb	r2, [r1, r2]	@ zero_extendqisi2
	strneb	r2, [r3, #2]
	movne	r2, #3
	ldrb	ip, [r3, r2]	@ zero_extendqisi2
	ldrb	ip, [r1, ip]	@ zero_extendqisi2
	strb	ip, [r3, r2]
	add	r2, r2, #1
.L3178:
	ldrb	ip, [r3, r2]	@ zero_extendqisi2
	ldrb	ip, [r1, ip]	@ zero_extendqisi2
	strb	ip, [r3, r2]
	add	r2, r2, #1
	sub	ip, r2, #1
	cmp	r0, ip
	bls	.L3185
.L2902:
	ldrb	ip, [r3, r2]	@ zero_extendqisi2
	ldrb	r6, [r1, ip]	@ zero_extendqisi2
	strb	r6, [r3, r2]
	add	r6, r2, #1
	ldrb	r5, [r3, r6]	@ zero_extendqisi2
	ldrb	ip, [r1, r5]	@ zero_extendqisi2
	strb	ip, [r3, r6]
	add	ip, r6, #1
	ldrb	r5, [r3, ip]	@ zero_extendqisi2
	ldrb	r6, [r1, r5]	@ zero_extendqisi2
	strb	r6, [r3, ip]
	add	ip, r2, #3
	ldrb	r5, [r3, ip]	@ zero_extendqisi2
	add	r2, r2, #4
	ldrb	r5, [r1, r5]	@ zero_extendqisi2
	sub	r6, r2, #1
	cmp	r0, r6
	strb	r5, [r3, ip]
	bhi	.L2902
	b	.L3185
.L3292:
	cmp	r0, #0
	beq	.L2889
	ldrb	r6, [r3, #2]	@ zero_extendqisi2
	ldrb	ip, [r3, #1]	@ zero_extendqisi2
	mov	r1, r6, lsr #3
	mov	r5, ip, lsr #3
	mov	r6, r1, asl #5
	ldrb	ip, [r3, #3]	@ zero_extendqisi2
	orr	r1, r6, r5, asl #10
	orr	r6, r1, ip, lsr #3
	ldrb	ip, [r2, r6]	@ zero_extendqisi2
	sub	r5, r0, #1
	cmp	r0, #1
	strb	ip, [r3, #1]
	and	r5, r5, #3
	mov	ip, #2
	add	r1, r3, #3
	bls	.L3183
	cmp	r5, #0
	beq	.L2890
	cmp	r5, #1
	beq	.L3174
	cmp	r5, #2
	beq	.L3175
	ldrb	ip, [r1, #2]	@ zero_extendqisi2
	ldrb	r7, [r1, #1]	@ zero_extendqisi2
	mov	r6, ip, lsr #3
	mov	r5, r7, lsr #3
	ldrb	ip, [r1, #3]	@ zero_extendqisi2
	mov	r7, r6, asl #5
	orr	r6, r7, r5, asl #10
	orr	r1, r6, ip, lsr #3
	ldrb	r7, [r2, r1]	@ zero_extendqisi2
	strb	r7, [r3, #2]
	mov	ip, #3
	add	r1, r3, #6
.L3175:
	ldrb	r5, [r1, #2]	@ zero_extendqisi2
	ldrb	r7, [r1, #1]	@ zero_extendqisi2
	mov	r6, r5, lsr #3
	mov	r7, r7, lsr #3
	mov	r5, r6, asl #5
	ldrb	r6, [r1, #3]!	@ zero_extendqisi2
	orr	r5, r5, r7, asl #10
	orr	r7, r5, r6, lsr #3
	ldrb	r5, [r2, r7]	@ zero_extendqisi2
	strb	r5, [r3, ip]
	add	ip, ip, #1
.L3174:
	ldrb	r5, [r1, #2]	@ zero_extendqisi2
	ldrb	r7, [r1, #1]	@ zero_extendqisi2
	mov	r6, r5, lsr #3
	mov	r5, r6, asl #5
	mov	r7, r7, lsr #3
	ldrb	r6, [r1, #3]!	@ zero_extendqisi2
	orr	r5, r5, r7, asl #10
	orr	r5, r5, r6, lsr #3
	ldrb	r6, [r2, r5]	@ zero_extendqisi2
	strb	r6, [r3, ip]
	add	ip, ip, #1
	sub	r5, ip, #1
	cmp	r0, r5
	bls	.L3183
.L2890:
	ldrb	r5, [r1, #2]	@ zero_extendqisi2
	ldrb	r7, [r1, #1]	@ zero_extendqisi2
	mov	sl, r5, lsr #3
	mov	r5, r1
	mov	r8, r7, lsr #3
	mov	r6, sl, asl #5
	ldrb	r7, [r5, #3]!	@ zero_extendqisi2
	orr	sl, r6, r8, asl #10
	orr	r8, sl, r7, lsr #3
	ldrb	r7, [r2, r8]	@ zero_extendqisi2
	strb	r7, [r3, ip]
	ldrb	r6, [r5, #2]	@ zero_extendqisi2
	ldrb	sl, [r5, #1]	@ zero_extendqisi2
	mov	r8, r6, lsr #3
	mov	r7, sl, lsr #3
	ldrb	r6, [r5, #3]	@ zero_extendqisi2
	mov	sl, r8, asl #5
	orr	r5, sl, r7, asl #10
	orr	r8, r5, r6, lsr #3
	ldrb	r7, [r2, r8]	@ zero_extendqisi2
	add	r6, ip, #1
	strb	r7, [r3, r6]
	add	r5, r1, #6
	ldrb	sl, [r5, #2]	@ zero_extendqisi2
	ldrb	r8, [r5, #1]	@ zero_extendqisi2
	mov	sl, sl, lsr #3
	ldrb	r7, [r5, #3]	@ zero_extendqisi2
	mov	r8, r8, lsr #3
	mov	r5, sl, asl #5
	orr	r5, r5, r8, asl #10
	orr	r5, r5, r7, lsr #3
	ldrb	r7, [r2, r5]	@ zero_extendqisi2
	add	r6, r6, #1
	strb	r7, [r3, r6]
	add	r5, r1, #9
	ldrb	r8, [r5, #2]	@ zero_extendqisi2
	ldrb	r7, [r5, #1]	@ zero_extendqisi2
	mov	r8, r8, lsr #3
	ldrb	r6, [r5, #3]	@ zero_extendqisi2
	mov	r7, r7, lsr #3
	mov	r5, r8, asl #5
	orr	r5, r5, r7, asl #10
	orr	r7, r5, r6, lsr #3
	add	r5, ip, #3
	add	ip, ip, #4
	ldrb	r6, [r2, r7]	@ zero_extendqisi2
	sub	r7, ip, #1
	cmp	r0, r7
	strb	r6, [r3, r5]
	add	r1, r1, #12
	bhi	.L2890
.L3183:
	ldrb	r6, [r4, #293]	@ zero_extendqisi2
.L2889:
	cmp	r6, #7
	mulls	r0, r6, r0
	strb	r6, [r4, #295]
	movhi	r6, r6, lsr #3
	mulhi	r0, r6, r0
	addls	r0, r0, #7
	mov	r3, #3
	mov	r2, #1
	movls	r0, r0, lsr #3
	strb	r3, [r4, #292]
	strb	r2, [r4, #294]
	str	r0, [r4, #288]
	b	.L2893
.L3293:
	cmp	r0, #0
	beq	.L2895
	ldrb	r1, [r3, #2]	@ zero_extendqisi2
	ldrb	r5, [r3, #1]	@ zero_extendqisi2
	mov	ip, r1, lsr #3
	mov	r5, r5, lsr #3
	mov	r1, ip, asl #5
	ldrb	ip, [r3, #3]	@ zero_extendqisi2
	orr	r1, r1, r5, asl #10
	orr	r5, r1, ip, lsr #3
	ldrb	r1, [r2, r5]	@ zero_extendqisi2
	sub	ip, r0, #1
	cmp	r0, #1
	strb	r1, [r3, #1]
	and	r5, ip, #3
	add	r1, r3, #4
	mov	ip, #2
	bls	.L3184
	cmp	r5, #0
	beq	.L2896
	cmp	r5, #1
	beq	.L3176
	cmp	r5, #2
	beq	.L3177
	ldrb	ip, [r1, #2]	@ zero_extendqisi2
	ldrb	r5, [r1, #1]	@ zero_extendqisi2
	mov	r7, ip, lsr #3
	mov	r6, r5, lsr #3
	mov	ip, r7, asl #5
	ldrb	r5, [r1, #3]	@ zero_extendqisi2
	orr	r7, ip, r6, asl #10
	orr	r6, r7, r5, lsr #3
	ldrb	ip, [r2, r6]	@ zero_extendqisi2
	add	r1, r1, #4
	strb	ip, [r3, #2]
	mov	ip, #3
.L3177:
	ldrb	r5, [r1, #2]	@ zero_extendqisi2
	ldrb	r7, [r1, #1]	@ zero_extendqisi2
	mov	r6, r5, lsr #3
	mov	r7, r7, lsr #3
	mov	r5, r6, asl #5
	ldrb	r6, [r1, #3]	@ zero_extendqisi2
	orr	r5, r5, r7, asl #10
	orr	r7, r5, r6, lsr #3
	ldrb	r6, [r2, r7]	@ zero_extendqisi2
	add	r1, r1, #4
	strb	r6, [r3, ip]
	add	ip, ip, #1
.L3176:
	ldrb	r5, [r1, #2]	@ zero_extendqisi2
	ldrb	r7, [r1, #1]	@ zero_extendqisi2
	mov	r6, r5, lsr #3
	mov	r5, r6, asl #5
	mov	r7, r7, lsr #3
	ldrb	r6, [r1, #3]	@ zero_extendqisi2
	orr	r5, r5, r7, asl #10
	orr	r5, r5, r6, lsr #3
	ldrb	r5, [r2, r5]	@ zero_extendqisi2
	strb	r5, [r3, ip]
	add	ip, ip, #1
	sub	r5, ip, #1
	cmp	r0, r5
	add	r1, r1, #4
	bls	.L3184
.L2896:
	ldrb	r6, [r1, #2]	@ zero_extendqisi2
	ldrb	sl, [r1, #1]	@ zero_extendqisi2
	mov	r8, r6, lsr #3
	mov	r5, r8, asl #5
	ldrb	r6, [r1, #3]	@ zero_extendqisi2
	mov	r7, sl, lsr #3
	orr	sl, r5, r7, asl #10
	orr	r8, sl, r6, lsr #3
	ldrb	r5, [r2, r8]	@ zero_extendqisi2
	strb	r5, [r3, ip]
	add	r5, r1, #4
	ldrb	r6, [r5, #2]	@ zero_extendqisi2
	ldrb	r7, [r5, #1]	@ zero_extendqisi2
	mov	sl, r6, lsr #3
	mov	r8, r7, lsr #3
	mov	r6, sl, asl #5
	ldrb	r7, [r5, #3]	@ zero_extendqisi2
	orr	sl, r6, r8, asl #10
	orr	r6, sl, r7, lsr #3
	ldrb	r7, [r2, r6]	@ zero_extendqisi2
	add	r6, ip, #1
	strb	r7, [r3, r6]
	add	r5, r5, #4
	ldrb	sl, [r5, #2]	@ zero_extendqisi2
	ldrb	r8, [r5, #1]	@ zero_extendqisi2
	mov	sl, sl, lsr #3
	ldrb	r7, [r5, #3]	@ zero_extendqisi2
	mov	r8, r8, lsr #3
	mov	r5, sl, asl #5
	orr	r5, r5, r8, asl #10
	orr	r5, r5, r7, lsr #3
	ldrb	r7, [r2, r5]	@ zero_extendqisi2
	add	r6, r6, #1
	strb	r7, [r3, r6]
	add	r5, r1, #12
	ldrb	r8, [r5, #2]	@ zero_extendqisi2
	ldrb	r7, [r5, #1]	@ zero_extendqisi2
	mov	r8, r8, lsr #3
	ldrb	r6, [r5, #3]	@ zero_extendqisi2
	mov	r7, r7, lsr #3
	mov	r5, r8, asl #5
	orr	r5, r5, r7, asl #10
	orr	r6, r5, r6, lsr #3
	add	r5, ip, #3
	add	ip, ip, #4
	ldrb	r6, [r2, r6]	@ zero_extendqisi2
	sub	r7, ip, #1
	cmp	r0, r7
	strb	r6, [r3, r5]
	add	r1, r1, #16
	bhi	.L2896
.L3184:
	ldrb	r5, [r4, #293]	@ zero_extendqisi2
.L2895:
	cmp	r5, #7
	mulls	r0, r5, r0
	strb	r5, [r4, #295]
	movhi	r5, r5, lsr #3
	mulhi	r0, r5, r0
	addls	r0, r0, #7
	mov	r3, #3
	mov	r2, #1
	movls	r0, r0, lsr #3
	strb	r3, [r4, #292]
	strb	r2, [r4, #294]
	str	r0, [r4, #288]
	b	.L2893
.L2909:
	cmp	r2, #0
	beq	.L2907
	sub	r0, r2, #1
	add	r3, r2, #3
	mvn	r5, r3
	add	r3, ip, r0, lsr #2
	and	r1, r5, #3
	ldrb	r5, [r3, #0]	@ zero_extendqisi2
	mov	r1, r1, asl #1
	mov	r5, r5, asr r1
	add	ip, ip, r0
	and	r5, r5, #3
	cmp	r1, #6
	strb	r5, [ip, #0]
	addne	r1, r1, #2
	and	r5, r0, #3
	beq	.L3303
.L3200:
	mov	r0, #1
	cmp	r2, r0
	bls	.L3214
	cmp	r5, #0
	beq	.L2916
	cmp	r5, r0
	beq	.L3166
	cmp	r5, #2
	beq	.L3167
	ldrb	r5, [r3, #0]	@ zero_extendqisi2
	mov	r6, r5, asr r1
	and	r5, r6, #3
	cmp	r1, #6
	strb	r5, [ip, #-1]
	addne	r1, r1, #2
	subeq	r3, r3, #1
	moveq	r1, #0
	add	r0, r0, #1
.L3167:
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	mov	r5, r6, asr r1
	and	r6, r5, #3
	rsb	r5, r0, #0
	cmp	r1, #6
	strb	r6, [ip, r5]
	addne	r1, r1, #2
	subeq	r3, r3, #1
	moveq	r1, #0
	add	r0, r0, #1
.L3166:
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	mov	r5, r6, asr r1
	cmp	r1, #6
	and	r6, r5, #3
	rsb	r5, r0, #0
	add	r0, r0, #1
	addne	r1, r1, #2
	subeq	r3, r3, #1
	moveq	r1, #0
	cmp	r2, r0
	strb	r6, [ip, r5]
	bhi	.L2916
	b	.L3214
.L3208:
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	mov	r5, r6, asr r1
	and	r6, r5, #3
	mvn	r5, r0
	cmp	r1, #6
	strb	r6, [ip, r5]
	subeq	r3, r3, #1
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	addne	r1, r1, #2
	moveq	r1, #0
	mov	r5, r6, asr r1
	cmp	r1, #6
	and	r6, r5, #3
	rsb	r5, r0, ip
	add	r0, r0, #3
	addne	r1, r1, #2
	subeq	r3, r3, #1
	moveq	r1, #0
	cmp	r2, r0
	strb	r6, [r5, #-2]
	bls	.L3214
.L2916:
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	mov	r5, r6, asr r1
	and	r6, r5, #3
	rsb	r5, r0, #0
	cmp	r1, #6
	strb	r6, [ip, r5]
	subeq	r3, r3, #1
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	addne	r1, r1, #2
	moveq	r1, #0
	mov	r5, r6, asr r1
	add	r0, r0, #1
	and	r6, r5, #3
	cmp	r1, #6
	rsb	r5, r0, #0
	strb	r6, [ip, r5]
	addne	r1, r1, #2
	subeq	r3, r3, #1
	moveq	r1, #0
	b	.L3208
.L3214:
	ldr	r3, [r4, #140]
	b	.L2907
.L3291:
	cmp	r2, #0
	beq	.L2907
	sub	r0, r2, #1
	add	r3, ip, r0, lsr #3
	add	r5, r2, #7
	mvn	r1, r5
	ldrb	r5, [r3, #0]	@ zero_extendqisi2
	and	r1, r1, #7
	mov	r5, r5, asr r1
	add	ip, ip, r0
	and	r5, r5, #1
	cmp	r1, #7
	strb	r5, [ip, #0]
	addne	r1, r1, #1
	and	r5, r0, #3
	beq	.L3304
.L3226:
	mov	r0, #1
	cmp	r2, r0
	bls	.L3214
	cmp	r5, #0
	beq	.L2913
	cmp	r5, r0
	beq	.L3172
	cmp	r5, #2
	beq	.L3173
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	mov	r5, r6, asr r1
	and	r6, r5, r0
	cmp	r1, #7
	strb	r6, [ip, #-1]
	addne	r1, r1, r0
	subeq	r3, r3, #1
	moveq	r1, #0
	add	r0, r0, #1
.L3173:
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	mov	r5, r6, asr r1
	and	r6, r5, #1
	rsb	r5, r0, #0
	cmp	r1, #7
	strb	r6, [ip, r5]
	addne	r1, r1, #1
	subeq	r3, r3, #1
	moveq	r1, #0
	add	r0, r0, #1
.L3172:
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	mov	r5, r6, asr r1
	cmp	r1, #7
	and	r6, r5, #1
	rsb	r5, r0, #0
	add	r0, r0, #1
	addne	r1, r1, #1
	subeq	r3, r3, #1
	moveq	r1, #0
	cmp	r2, r0
	strb	r6, [ip, r5]
	bhi	.L2913
	b	.L3214
.L3234:
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	mov	r5, r6, asr r1
	and	r6, r5, #1
	mvn	r5, r0
	cmp	r1, #7
	strb	r6, [ip, r5]
	subeq	r3, r3, #1
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	addne	r1, r1, #1
	moveq	r1, #0
	mov	r5, r6, asr r1
	cmp	r1, #7
	and	r6, r5, #1
	rsb	r5, r0, ip
	add	r0, r0, #3
	addne	r1, r1, #1
	subeq	r3, r3, #1
	moveq	r1, #0
	cmp	r2, r0
	strb	r6, [r5, #-2]
	bls	.L3214
.L2913:
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	mov	r5, r6, asr r1
	and	r6, r5, #1
	rsb	r5, r0, #0
	cmp	r1, #7
	strb	r6, [ip, r5]
	subeq	r3, r3, #1
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	addne	r1, r1, #1
	moveq	r1, #0
	mov	r5, r6, asr r1
	add	r0, r0, #1
	and	r6, r5, #1
	cmp	r1, #7
	rsb	r5, r0, #0
	strb	r6, [ip, r5]
	addne	r1, r1, #1
	subeq	r3, r3, #1
	moveq	r1, #0
	b	.L3234
.L2910:
	tst	r2, #1
	movne	r5, #4
	moveq	r5, #0
	cmp	r2, #0
	beq	.L2907
	sub	r0, r2, #1
	add	r3, ip, r0, lsr #1
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	mov	r6, r6, asr r5
	cmp	r5, #4
	add	ip, ip, r0
	and	r5, r6, #15
	strb	r5, [ip, #0]
	and	r5, r0, #3
	beq	.L3305
.L3213:
	mov	r0, #1
	cmp	r2, r0
	bls	.L3214
	cmp	r5, #0
	beq	.L2921
	cmp	r5, r0
	beq	.L3169
	cmp	r5, #2
	beq	.L3170
	ldrb	r5, [r3, #0]	@ zero_extendqisi2
	mov	r5, r5, asr r1
	cmp	r1, #4
	and	r1, r5, #15
	strb	r1, [ip, #-1]
	subeq	r3, r3, #1
	moveq	r1, #0
	movne	r1, #4
	add	r0, r0, #1
.L3170:
	ldrb	r5, [r3, #0]	@ zero_extendqisi2
	mov	r5, r5, asr r1
	cmp	r1, #4
	and	r5, r5, #15
	rsb	r1, r0, #0
	strb	r5, [ip, r1]
	subeq	r3, r3, #1
	moveq	r1, #0
	movne	r1, #4
	add	r0, r0, #1
.L3169:
	ldrb	r5, [r3, #0]	@ zero_extendqisi2
	mov	r5, r5, asr r1
	cmp	r1, #4
	and	r5, r5, #15
	rsb	r1, r0, #0
	add	r0, r0, #1
	strb	r5, [ip, r1]
	subeq	r3, r3, #1
	moveq	r1, #0
	movne	r1, #4
	cmp	r2, r0
	bhi	.L2921
	b	.L3214
.L3221:
	ldrb	r5, [r3, #0]	@ zero_extendqisi2
	mov	r5, r5, asr r1
	cmp	r1, #4
	and	r5, r5, #15
	mvn	r1, r0
	strb	r5, [ip, r1]
	subeq	r3, r3, #1
	ldrb	r5, [r3, #0]	@ zero_extendqisi2
	moveq	r1, #0
	movne	r1, #4
	mov	r5, r5, asr r1
	cmp	r1, #4
	and	r5, r5, #15
	rsb	r1, r0, ip
	add	r0, r0, #3
	strb	r5, [r1, #-2]
	subeq	r3, r3, #1
	moveq	r1, #0
	movne	r1, #4
	cmp	r2, r0
	bls	.L3214
.L2921:
	ldrb	r5, [r3, #0]	@ zero_extendqisi2
	mov	r5, r5, asr r1
	cmp	r1, #4
	and	r5, r5, #15
	rsb	r1, r0, #0
	strb	r5, [ip, r1]
	subeq	r3, r3, #1
	ldrb	r5, [r3, #0]	@ zero_extendqisi2
	movne	r1, #4
	moveq	r1, #0
	mov	r5, r5, asr r1
	add	r0, r0, #1
	cmp	r1, #4
	and	r5, r5, #15
	rsb	r1, r0, #0
	strb	r5, [ip, r1]
	movne	r1, #4
	subeq	r3, r3, #1
	moveq	r1, #0
	b	.L3221
.L3307:
	.align	2
.L3306:
	.word	.LC4
	.word	.LC6
	.word	.LC3
	.word	.LC5
.L3299:
	cmp	r1, #0
	ldr	r3, [r4, #288]
	beq	.L2934
	add	r2, r2, r3
	ldrb	r7, [r2, #-2]	@ zero_extendqisi2
	ldrb	r6, [r2, #-3]	@ zero_extendqisi2
	ldrb	ip, [r2, #-1]	@ zero_extendqisi2
	ldrb	r5, [r2, #-4]	@ zero_extendqisi2
	mov	r3, r2
	sub	r0, r1, #1
	cmp	r1, #1
	strb	r7, [r2, #-1]
	strb	r6, [r2, #-2]
	strb	r5, [r2, #-3]
	strb	ip, [r3, #-4]!
	and	r2, r0, #3
	mov	ip, #1
	bls	.L3199
	cmp	r2, #0
	beq	.L2937
	cmp	r2, #1
	beq	.L3149
	cmp	r2, #2
	beq	.L3150
	ldrb	ip, [r3, #-3]	@ zero_extendqisi2
	ldrb	r5, [r3, #-2]	@ zero_extendqisi2
	ldrb	r6, [r3, #-1]	@ zero_extendqisi2
	ldrb	r0, [r3, #-4]	@ zero_extendqisi2
	strb	ip, [r3, #-2]
	strb	r5, [r3, #-1]
	strb	r0, [r3, #-3]
	strb	r6, [r3, #-4]!
	mov	ip, #2
.L3150:
	ldrb	r6, [r3, #-2]	@ zero_extendqisi2
	ldrb	r5, [r3, #-3]	@ zero_extendqisi2
	ldrb	r2, [r3, #-1]	@ zero_extendqisi2
	ldrb	r0, [r3, #-4]	@ zero_extendqisi2
	strb	r6, [r3, #-1]
	strb	r5, [r3, #-2]
	strb	r0, [r3, #-3]
	strb	r2, [r3, #-4]!
	add	ip, ip, #1
.L3149:
	ldrb	r6, [r3, #-2]	@ zero_extendqisi2
	ldrb	r5, [r3, #-3]	@ zero_extendqisi2
	ldrb	r2, [r3, #-1]	@ zero_extendqisi2
	ldrb	r0, [r3, #-4]	@ zero_extendqisi2
	add	ip, ip, #1
	cmp	r1, ip
	strb	r6, [r3, #-1]
	strb	r5, [r3, #-2]
	strb	r0, [r3, #-3]
	strb	r2, [r3, #-4]!
	bls	.L3199
.L2937:
	ldrb	r6, [r3, #-2]	@ zero_extendqisi2
	ldrb	r5, [r3, #-3]	@ zero_extendqisi2
	ldrb	r8, [r3, #-1]	@ zero_extendqisi2
	ldrb	r2, [r3, #-4]	@ zero_extendqisi2
	mov	r0, r3
	strb	r2, [r3, #-3]
	strb	r6, [r3, #-1]
	strb	r5, [r3, #-2]
	strb	r8, [r0, #-4]!
	ldrb	r7, [r0, #-3]	@ zero_extendqisi2
	ldrb	r8, [r0, #-2]	@ zero_extendqisi2
	ldrb	r5, [r0, #-1]	@ zero_extendqisi2
	ldrb	r6, [r0, #-4]	@ zero_extendqisi2
	mov	r2, r0
	strb	r8, [r0, #-1]
	strb	r7, [r0, #-2]
	strb	r6, [r0, #-3]
	strb	r5, [r2, #-4]!
	ldrb	r7, [r2, #-1]	@ zero_extendqisi2
	ldrb	r6, [r2, #-2]	@ zero_extendqisi2
	ldrb	r5, [r2, #-3]	@ zero_extendqisi2
	ldrb	r0, [r2, #-4]	@ zero_extendqisi2
	strb	r6, [r2, #-1]
	strb	r7, [r2, #-4]
	strb	r5, [r2, #-2]
	strb	r0, [r2, #-3]
	sub	r2, r3, #12
	ldrb	r7, [r2, #-1]	@ zero_extendqisi2
	ldrb	r6, [r2, #-2]	@ zero_extendqisi2
	ldrb	r5, [r2, #-3]	@ zero_extendqisi2
	ldrb	r0, [r2, #-4]	@ zero_extendqisi2
	add	ip, ip, #4
	cmp	r1, ip
	strb	r7, [r2, #-4]
	strb	r6, [r2, #-1]
	strb	r5, [r2, #-2]
	strb	r0, [r2, #-3]
	sub	r3, r3, #16
	bhi	.L2937
	ldr	r0, [r4, #140]
	b	.L3300
.L3301:
	cmp	r2, #0
	ldr	r1, [r4, #288]
	beq	.L2926
	add	r3, r3, r1
	ldrb	ip, [r3, #-1]	@ zero_extendqisi2
	sub	r0, r2, #1
	mvn	r1, ip
	cmp	r2, #1
	strb	r1, [r3, #-1]
	and	r0, r0, #3
	sub	r3, r3, #4
	mov	r1, #1
	bls	.L3195
	cmp	r0, #0
	beq	.L2929
	cmp	r0, #1
	beq	.L3157
	cmp	r0, #2
	ldrneb	r1, [r3, #-1]	@ zero_extendqisi2
	mvnne	r1, r1
	strneb	r1, [r3, #-1]
	subne	r3, r3, #4
	ldrb	r0, [r3, #-1]	@ zero_extendqisi2
	movne	r1, #2
	mvn	ip, r0
	strb	ip, [r3, #-1]
	add	r1, r1, #1
	sub	r3, r3, #4
.L3157:
	ldrb	r0, [r3, #-1]	@ zero_extendqisi2
	add	r1, r1, #1
	mvn	ip, r0
	cmp	r2, r1
	strb	ip, [r3, #-1]
	sub	r3, r3, #4
	bls	.L3195
.L2929:
	ldrb	ip, [r3, #-1]	@ zero_extendqisi2
	mvn	r0, ip
	strb	r0, [r3, #-1]
	sub	r0, r3, #4
	ldrb	ip, [r0, #-1]	@ zero_extendqisi2
	mvn	ip, ip
	strb	ip, [r0, #-1]
	sub	r0, r0, #4
	ldrb	ip, [r0, #-1]	@ zero_extendqisi2
	mvn	ip, ip
	strb	ip, [r0, #-1]
	sub	r0, r3, #12
	ldrb	ip, [r0, #-1]	@ zero_extendqisi2
	add	r1, r1, #4
	mvn	ip, ip
	cmp	r2, r1
	strb	ip, [r0, #-1]
	sub	r3, r3, #16
	bhi	.L2929
	ldr	r0, [r4, #140]
	b	.L3302
.L3297:
	cmp	r1, #0
	ldr	r3, [r4, #288]
	beq	.L2934
	add	r2, r2, r3
	ldrb	ip, [r2, #-1]	@ zero_extendqisi2
	ldrb	r5, [r2, #-2]	@ zero_extendqisi2
	mov	r3, r2
	sub	r0, r1, #1
	cmp	r1, #1
	strb	r5, [r2, #-1]
	strb	ip, [r3, #-2]!
	and	r2, r0, #3
	mov	ip, #1
	bls	.L3199
	cmp	r2, #0
	beq	.L2940
	cmp	r2, #1
	beq	.L3153
	cmp	r2, #2
	ldrneb	r2, [r3, #-1]	@ zero_extendqisi2
	ldrneb	r0, [r3, #-2]	@ zero_extendqisi2
	strneb	r0, [r3, #-1]
	strneb	r2, [r3, #-2]!
	ldrb	r2, [r3, #-1]	@ zero_extendqisi2
	ldrb	r0, [r3, #-2]	@ zero_extendqisi2
	movne	ip, #2
	strb	r0, [r3, #-1]
	strb	r2, [r3, #-2]!
	add	ip, ip, #1
.L3153:
	ldrb	r2, [r3, #-1]	@ zero_extendqisi2
	ldrb	r0, [r3, #-2]	@ zero_extendqisi2
	add	ip, ip, #1
	cmp	r1, ip
	strb	r0, [r3, #-1]
	strb	r2, [r3, #-2]!
	bls	.L3199
.L2940:
	ldrb	r6, [r3, #-1]	@ zero_extendqisi2
	ldrb	r2, [r3, #-2]	@ zero_extendqisi2
	mov	r0, r3
	strb	r2, [r3, #-1]
	strb	r6, [r0, #-2]!
	ldrb	r5, [r0, #-1]	@ zero_extendqisi2
	ldrb	r6, [r0, #-2]	@ zero_extendqisi2
	mov	r2, r0
	strb	r6, [r0, #-1]
	strb	r5, [r2, #-2]!
	ldrb	r5, [r2, #-1]	@ zero_extendqisi2
	ldrb	r0, [r2, #-2]	@ zero_extendqisi2
	strb	r5, [r2, #-2]
	strb	r0, [r2, #-1]
	sub	r2, r3, #6
	ldrb	r5, [r2, #-1]	@ zero_extendqisi2
	ldrb	r0, [r2, #-2]	@ zero_extendqisi2
	add	ip, ip, #4
	cmp	r1, ip
	strb	r5, [r2, #-2]
	strb	r0, [r2, #-1]
	sub	r3, r3, #8
	bhi	.L2940
	ldr	r0, [r4, #140]
	b	.L3300
.L3295:
	cmp	r2, #0
	ldr	r1, [r4, #288]
	beq	.L2926
	add	r3, r3, r1
	ldrb	ip, [r3, #-1]	@ zero_extendqisi2
	sub	r0, r2, #1
	mvn	r1, ip
	cmp	r2, #1
	strb	r1, [r3, #-1]
	and	r0, r0, #3
	sub	r3, r3, #2
	mov	r1, #1
	bls	.L3195
	cmp	r0, #0
	beq	.L2932
	cmp	r0, #1
	beq	.L3161
	cmp	r0, #2
	ldrneb	r1, [r3, #-1]	@ zero_extendqisi2
	mvnne	r1, r1
	strneb	r1, [r3, #-1]
	subne	r3, r3, #2
	ldrb	ip, [r3, #-1]	@ zero_extendqisi2
	movne	r1, #2
	mvn	r0, ip
	strb	r0, [r3, #-1]
	add	r1, r1, #1
	sub	r3, r3, #2
.L3161:
	ldrb	ip, [r3, #-1]	@ zero_extendqisi2
	add	r1, r1, #1
	mvn	r0, ip
	cmp	r2, r1
	strb	r0, [r3, #-1]
	sub	r3, r3, #2
	bls	.L3195
.L2932:
	ldrb	ip, [r3, #-1]	@ zero_extendqisi2
	mvn	r0, ip
	strb	r0, [r3, #-1]
	sub	r0, r3, #2
	ldrb	ip, [r0, #-1]	@ zero_extendqisi2
	mvn	ip, ip
	strb	ip, [r0, #-1]
	sub	r0, r0, #2
	ldrb	ip, [r0, #-1]	@ zero_extendqisi2
	mvn	ip, ip
	strb	ip, [r0, #-1]
	sub	r0, r3, #6
	ldrb	ip, [r0, #-1]	@ zero_extendqisi2
	add	r1, r1, #4
	mvn	ip, ip
	cmp	r2, r1
	strb	ip, [r0, #-1]
	sub	r3, r3, #8
	bhi	.L2932
	ldr	r0, [r4, #140]
	b	.L3302
.L3298:
	mov	r0, r4
	ldr	r1, .L3306+12
	bl	png_warning
	ldr	r3, [r4, #140]
	mov	r0, r3
	and	r2, r3, #6291456
	b	.L2877
.L3303:
	sub	r3, r3, #1
	mov	r1, #0
	b	.L3200
.L3305:
	sub	r3, r3, #1
	mov	r1, #0
	b	.L3213
.L3304:
	sub	r3, r3, #1
	mov	r1, #0
	b	.L3226
	.size	png_do_read_transformations, .-png_do_read_transformations
	.section	.rodata
	.align	2
	.set	.LANCHOR0,. + 0
	.type	CSWTCH.78, %object
	.size	CSWTCH.78, 32
CSWTCH.78:
	.word	2
	.word	4
	.word	0
	.word	16
	.word	0
	.word	0
	.word	0
	.word	256
	.type	CSWTCH.79, %object
	.size	CSWTCH.79, 32
CSWTCH.79:
	.word	255
	.word	85
	.word	0
	.word	17
	.word	0
	.word	0
	.word	0
	.word	1
	.type	png_gamma_shift, %object
	.size	png_gamma_shift, 36
png_gamma_shift:
	.word	16
	.word	33
	.word	66
	.word	132
	.word	272
	.word	584
	.word	1360
	.word	4080
	.word	0
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"ignoring out of range rgb_to_gray coefficients\000"
	.space	1
.LC1:
	.ascii	"Application must supply a known background gamma\000"
	.space	3
.LC2:
	.ascii	"Can't discard critical data on CRC error.\000"
	.space	2
.LC3:
	.ascii	"NULL row buffer for row %ld, pass %d\000"
	.space	3
.LC4:
	.ascii	"Uninitialized row\000"
	.space	2
.LC5:
	.ascii	"png_do_rgb_to_gray found nongray pixel\000"
	.space	1
.LC6:
	.ascii	"png_do_dither returned rowbytes=0\000"
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
