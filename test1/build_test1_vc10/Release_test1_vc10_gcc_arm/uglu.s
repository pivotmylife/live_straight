	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"uglu.c"
	.section	.text.__cosx,"ax",%progbits
	.align	2
	.global	__cosx
	.hidden	__cosx
	.type	__cosx, %function
__cosx:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	bge	.L2
	ldr	ip, .L24
	mvn	r2, r0
	umull	r1, ip, r2, ip
	add	r1, r0, #409600
	add	r0, r1, #2160
	add	r0, r0, #14
	mov	r3, ip, lsr #18
	cmp	r0, #0
	and	r3, r3, #3
	bge	.L2
	cmp	r3, #0
	beq	.L8
	cmp	r3, #1
	beq	.L21
	cmp	r3, #2
	addne	r0, r0, #409600
	addne	r0, r0, #2160
	addne	r0, r0, #14
	add	ip, r0, #409600
	add	r0, ip, #2160
	add	r0, r0, #14
.L21:
	add	r1, r0, #409600
	add	r3, r1, #2160
	add	r0, r3, #14
	cmp	r0, #0
	bge	.L2
.L8:
	add	ip, r0, #1638400
	add	r3, ip, #8640
	add	r0, r3, #56
	cmp	r0, #0
	blt	.L8
.L2:
	add	r1, r0, r0, asl #1
	rsb	r3, r1, r1, asl #5
	add	ip, r0, r3, asl #3
	rsb	r0, ip, ip, asl #3
	mov	r1, r0, asl #1
	mov	r3, r1, asr #16
	add	ip, r3, #16384
	mov	r3, ip, asr #4
	tst	r3, #1024
	mvnne	r2, r3
	movne	r2, r2, asl #22
	moveq	r2, r3, asl #22
	mov	r0, r2, lsr #22
	ldr	r1, .L24+4
	mov	r2, r0, asl #1
	ldrh	r0, [r2, r1]
	tst	r3, #2048
	rsbne	r0, r0, #0
	bx	lr
.L25:
	.align	2
.L24:
	.word	-1560700667
	.word	.LANCHOR0
	.size	__cosx, .-__cosx
	.section	.text.__sinx,"ax",%progbits
	.align	2
	.global	__sinx
	.hidden	__sinx
	.type	__sinx, %function
__sinx:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	bge	.L27
	ldr	ip, .L48
	mvn	r2, r0
	umull	r1, ip, r2, ip
	add	r1, r0, #409600
	add	r0, r1, #2160
	add	r0, r0, #14
	mov	r3, ip, lsr #18
	cmp	r0, #0
	and	r3, r3, #3
	bge	.L27
	cmp	r3, #0
	beq	.L33
	cmp	r3, #1
	beq	.L46
	cmp	r3, #2
	addne	r0, r0, #409600
	addne	r0, r0, #2160
	addne	r0, r0, #14
	add	r1, r0, #409600
	add	r0, r1, #2160
	add	r0, r0, #14
.L46:
	add	r3, r0, #409600
	add	ip, r3, #2160
	add	r0, ip, #14
	cmp	r0, #0
	bge	.L27
.L33:
	add	ip, r0, #1638400
	add	r3, ip, #8640
	add	r0, r3, #56
	cmp	r0, #0
	blt	.L33
.L27:
	add	r3, r0, r0, asl #1
	rsb	ip, r3, r3, asl #5
	add	r1, r0, ip, asl #3
	rsb	r3, r1, r1, asl #3
	mov	ip, r3, asl #1
	mov	r3, ip, asr #20
	tst	r3, #1024
	mvnne	r2, r3
	movne	r2, r2, asl #22
	moveq	r2, r3, asl #22
	mov	r0, r2, lsr #22
	ldr	r1, .L48+4
	mov	r2, r0, asl #1
	ldrh	r0, [r2, r1]
	tst	r3, #2048
	rsbne	r0, r0, #0
	bx	lr
.L49:
	.align	2
.L48:
	.word	-1560700667
	.word	.LANCHOR0
	.size	__sinx, .-__sinx
	.global	__aeabi_ldivmod
	.section	.text.__sqrtx,"ax",%progbits
	.align	2
	.global	__sqrtx
	.hidden	__sqrtx
	.type	__sqrtx, %function
__sqrtx:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	mov	r5, r0
	mov	r6, r5, asr #31
	add	r2, r0, #65536
	mov	r4, r6, asl #16
	orr	r4, r4, r5, lsr #16
	mov	r6, r2, asr #1
	mov	r5, r5, asl #16
	mov	r2, r6
	mov	r3, r2, asr #31
	mov	r0, r5
	mov	r1, r4
	bl	__aeabi_ldivmod
	add	ip, r6, r0
	mov	r6, ip, asr #1
	mov	r2, r6
	mov	r3, r2, asr #31
	mov	r0, r5
	mov	r1, r4
	bl	__aeabi_ldivmod
	add	r1, r6, r0
	mov	r6, r1, asr #1
	mov	r2, r6
	mov	r3, r2, asr #31
	mov	r0, r5
	mov	r1, r4
	bl	__aeabi_ldivmod
	add	ip, r6, r0
	mov	r6, ip, asr #1
	mov	r2, r6
	mov	r3, r2, asr #31
	mov	r0, r5
	mov	r1, r4
	bl	__aeabi_ldivmod
	add	r1, r6, r0
	mov	r6, r1, asr #1
	mov	r2, r6
	mov	r3, r2, asr #31
	mov	r0, r5
	mov	r1, r4
	bl	__aeabi_ldivmod
	add	r6, r6, r0
	mov	r6, r6, asr #1
	mov	r0, r5
	mov	r1, r4
	mov	r2, r6
	mov	r3, r2, asr #31
	bl	__aeabi_ldivmod
	add	r0, r6, r0
	mov	r0, r0, asr #1
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
	.size	__sqrtx, .-__sqrtx
	.global	__aeabi_fsub
	.global	__aeabi_fmul
	.global	__aeabi_fdiv
	.global	__aeabi_f2d
	.global	__aeabi_fcmpeq
	.global	__aeabi_d2f
	.global	__aeabi_fadd
	.section	.text.ugluPerspectivef,"ax",%progbits
	.align	2
	.global	ugluPerspectivef
	.hidden	ugluPerspectivef
	.type	ugluPerspectivef, %function
ugluPerspectivef:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 72
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	mov	r5, r0
	sub	sp, sp, #76
	mov	r0, r3
	mov	r8, r1
	mov	r1, r2
	mov	r4, r2
	mov	r7, r3
	bl	__aeabi_fsub
	mov	r1, #1056964608
	mov	r6, r0
	mov	r0, r5
	bl	__aeabi_fmul
	mov	r3, #4784128
	add	r2, r3, #4048
	add	r1, r2, #1073741835
	bl	__aeabi_fmul
	mov	r1, #1124073472
	add	r1, r1, #3407872
	bl	__aeabi_fdiv
	bl	__aeabi_f2d
	stmia	sp, {r0-r1}
	bl	sin
	mov	r5, #0
	mov	sl, r0
	mov	fp, r1
	mov	r0, r6
	mov	r1, r5
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	bne	.L54
	mov	r0, sl
	mov	r1, fp
	bl	__aeabi_d2f
	mov	r1, r5
	mov	sl, r0
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	bne	.L54
	mov	r0, r8
	mov	r1, r5
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	beq	.L55
.L54:
	add	sp, sp, #76
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L55:
	ldmia	sp, {r0-r1}
	bl	cos
	bl	__aeabi_d2f
	mov	r1, sl
	bl	__aeabi_fdiv
	add	r9, sp, #8
	mov	r2, #64
	mov	sl, r0
	mov	r1, #0
	mov	r0, r9
	bl	memset
	mov	r1, r8
	mov	r0, sl
	bl	__aeabi_fdiv
	mov	r1, r4
	str	r0, [sp, #8]	@ float
	mov	r0, r7
	str	sl, [sp, #28]	@ float
	bl	__aeabi_fadd
	mov	r1, r6
	add	r0, r0, #-2147483648
	bl	__aeabi_fdiv
	mov	r2, #-1090519040
	add	ip, r2, #8388608
	str	r0, [sp, #48]	@ float
	mov	r1, #-1073741824
	mov	r0, r7
	str	ip, [sp, #52]	@ float
	bl	__aeabi_fmul
	mov	r1, r4
	bl	__aeabi_fmul
	mov	r1, r6
	bl	__aeabi_fdiv
	str	r0, [sp, #64]	@ float
	mov	r0, r9
	str	r5, [sp, #68]	@ float
	bl	glMultMatrixf
	b	.L54
	.size	ugluPerspectivef, .-ugluPerspectivef
	.global	__aeabi_idiv
	.section	.text.ugluPerspectivex,"ax",%progbits
	.align	2
	.global	ugluPerspectivex
	.hidden	ugluPerspectivex
	.type	ugluPerspectivex, %function
ugluPerspectivex:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 64
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	mov	r4, #568
	add	ip, r4, #3
	smull	r4, r5, r0, ip
	mov	r0, r4, lsr #16
	orr	r0, r0, r5, asl #16
	cmp	r0, #0
	mov	r5, r3
	rsb	r6, r2, r3
	sub	sp, sp, #68
	mov	r4, r2
	mov	r9, r1
	mov	ip, r0
	movge	r3, r0
	bge	.L58
	ldr	r3, .L99
	mvn	r1, r0
	umull	r2, r1, r3, r1
	add	r2, r0, #409600
	add	r3, r2, #2160
	add	r3, r3, #14
	mov	r2, #409600
	mov	r1, r1, lsr #18
	add	r2, r2, #2160
	cmp	r3, #0
	and	r1, r1, #3
	add	r2, r2, #14
	bge	.L58
	cmp	r1, #0
	beq	.L59
	cmp	r1, #1
	beq	.L96
	cmp	r1, #2
	addne	r3, r0, #823296
	movne	r2, #823296
	addne	r3, r3, #252
	addne	r2, r2, #252
	add	r3, r3, #409600
	add	r2, r2, #409600
	add	r3, r3, #2160
	add	r2, r2, #2160
	add	r3, r3, #14
	add	r2, r2, #14
.L96:
	add	r2, r2, #409600
	add	r2, r2, #2160
	add	r3, r3, #409600
	add	r2, r2, #14
	add	r3, r3, #2160
	cmn	r2, r0
	add	r3, r3, #14
	bpl	.L58
.L59:
	add	r2, r2, #1638400
	add	r2, r2, #8640
	add	r3, r3, #1638400
	add	r2, r2, #56
	add	r3, r3, #8640
	cmn	r2, r0
	add	r3, r3, #56
	bmi	.L59
.L58:
	add	r2, r3, r3, asl #1
	rsb	r2, r2, r2, asl #5
	add	r3, r3, r2, asl #3
	rsb	r3, r3, r3, asl #3
	mov	r2, r3, asl #1
	mov	r3, r2, asr #20
	tst	r3, #1024
	mvnne	r1, r3
	movne	r1, r1, asl #22
	moveq	r1, r3, asl #22
	mov	r1, r1, lsr #22
	ldr	r2, .L99+4
	mov	r1, r1, asl #1
	ldrh	r1, [r1, r2]
	tst	r3, #2048
	rsbne	r1, r1, #0
	cmp	r6, #0
	cmpne	r1, #0
	beq	.L69
	cmp	r9, #0
	bne	.L98
.L69:
	add	sp, sp, #68
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L98:
	cmp	r0, #0
	bge	.L64
	ldr	r3, .L99
	mvn	sl, r0
	umull	ip, sl, r3, sl
	add	ip, r0, #409600
	add	fp, ip, #2160
	add	ip, fp, #14
	mov	r8, #409600
	mov	lr, sl, lsr #18
	add	r7, r8, #2160
	cmp	ip, #0
	and	lr, lr, #3
	add	r3, r7, #14
	bge	.L64
	cmp	lr, #0
	beq	.L65
	cmp	lr, #1
	beq	.L94
	cmp	lr, #2
	addne	ip, r0, #823296
	movne	r3, #823296
	addne	ip, ip, #252
	addne	r3, r3, #252
	add	lr, r3, #409600
	add	fp, ip, #409600
	add	ip, fp, #2160
	add	sl, lr, #2160
	add	ip, ip, #14
	add	r3, sl, #14
.L94:
	add	sl, r3, #409600
	add	r3, sl, #2160
	add	r8, ip, #409600
	add	r3, r3, #14
	add	r7, r8, #2160
	cmn	r3, r0
	add	ip, r7, #14
	bpl	.L64
.L65:
	add	sl, r3, #1638400
	add	r8, sl, #8640
	add	lr, ip, #1638400
	add	r3, r8, #56
	add	r7, lr, #8640
	cmn	r3, r0
	add	ip, r7, #56
	bmi	.L65
.L64:
	add	r8, ip, ip, asl #1
	rsb	r7, r8, r8, asl #5
	add	fp, ip, r7, asl #3
	rsb	r3, fp, fp, asl #3
	mov	lr, r3, asl #1
	mov	sl, lr, asr #16
	add	ip, sl, #16384
	mov	fp, ip, asr #4
	tst	fp, #1024
	mvnne	r0, fp
	moveq	r0, fp, asl #22
	movne	r0, r0, asl #22
	mov	r8, r0, lsr #22
	mov	r7, r8, asl #1
	ldrh	r0, [r7, r2]
	tst	fp, #2048
	rsbne	r0, r0, #0
	bl	__aeabi_idiv
	mov	r1, #0
	mov	r7, r0
	mov	r2, #64
	mov	r0, sp
	bl	memset
	mov	fp, r7
	mov	ip, fp, asr #31
	mov	sl, ip, asl #16
	mov	r2, r9
	mov	r3, r2, asr #31
	orr	r1, sl, fp, lsr #16
	mov	r0, fp, asl #16
	bl	__aeabi_ldivmod
	add	fp, r5, r4
	mov	ip, fp, asr #31
	mov	r9, r6
	mov	sl, r9, asr #31
	mov	r1, ip, asl #16
	str	r0, [sp, #0]
	mov	r2, r9
	mov	r3, sl
	orr	r1, r1, fp, lsr #16
	mov	r0, fp, asl #16
	str	r7, [sp, #20]
	bl	__aeabi_ldivmod
	smull	r2, r3, r5, r4
	mov	r5, r2
	mov	r6, r3
	mov	ip, r5, lsr #16
	orr	r1, ip, r6, asl #16
	mov	r4, r6, asr #16
	mov	r5, r4
	mov	r4, r1
	rsbs	r4, r4, #0
	rsc	r5, r5, #0
	adds	r4, r4, r4
	adc	r5, r5, r5
	mov	r3, r5, asl #16
	mov	r2, #-2147483648
	rsb	lr, r0, #0
	mov	ip, r2, asr #15
	orr	r1, r3, r4, lsr #16
	mov	r2, r9
	mov	r3, sl
	mov	r0, r4, asl #16
	str	lr, [sp, #40]
	str	ip, [sp, #44]
	bl	__aeabi_ldivmod
	mov	r1, #0
	str	r0, [sp, #56]
	mov	r0, sp
	mov	r8, sp
	str	r1, [sp, #60]
	bl	glMultMatrixx
	b	.L69
.L100:
	.align	2
.L99:
	.word	-1560700667
	.word	.LANCHOR0
	.size	ugluPerspectivex, .-ugluPerspectivex
	.section	.text.ugluLookAtf,"ax",%progbits
	.align	2
	.global	ugluLookAtf
	.hidden	ugluLookAtf
	.type	ugluLookAtf, %function
ugluLookAtf:
	@ Function supports interworking.
	@ args = 20, pretend = 0, frame = 80
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	sub	sp, sp, #84
	str	r0, [sp, #8]	@ float
	str	r1, [sp, #12]	@ float
	mov	r0, r3
	ldr	r1, [sp, #8]	@ float
	mov	fp, r2
	bl	__aeabi_fsub
	ldr	r1, [sp, #12]	@ float
	mov	r6, r0
	ldr	r0, [sp, #120]	@ float
	bl	__aeabi_fsub
	mov	r1, fp
	mov	r5, r0
	ldr	r0, [sp, #124]	@ float
	bl	__aeabi_fsub
	mov	r1, r6
	mov	r4, r0
	mov	r0, r6
	bl	__aeabi_fmul
	mov	r1, r5
	mov	r8, r0
	mov	r0, r5
	bl	__aeabi_fmul
	mov	r1, r0
	mov	r0, r8
	bl	__aeabi_fadd
	mov	r1, r4
	mov	r7, r0
	mov	r0, r4
	bl	__aeabi_fmul
	mov	r1, r0
	mov	r0, r7
	bl	__aeabi_fadd
	bl	__aeabi_f2d
	ldr	sl, [sp, #132]	@ float
	ldr	r7, [sp, #136]	@ float
	bl	sqrt
	bl	__aeabi_d2f
	mov	r1, #0
	mov	r8, r0
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	bne	.L102
	mov	r1, r8
	mov	r0, #1065353216
	bl	__aeabi_fdiv
	mov	r9, r0
	mov	r1, r9
	mov	r0, r6
	bl	__aeabi_fmul
	mov	r1, r9
	mov	r6, r0
	mov	r0, r5
	bl	__aeabi_fmul
	mov	r1, r9
	mov	r5, r0
	mov	r0, r4
	bl	__aeabi_fmul
	mov	r4, r0
.L102:
	mov	r0, r7
	mov	r1, r5
	bl	__aeabi_fmul
	mov	r1, r4
	mov	r8, r0
	mov	r0, sl
	bl	__aeabi_fmul
	mov	r1, r0
	mov	r0, r8
	bl	__aeabi_fsub
	mov	r1, r4
	mov	r8, r0
	ldr	r0, [sp, #128]	@ float
	bl	__aeabi_fmul
	mov	r1, r6
	mov	r9, r0
	mov	r0, r7
	bl	__aeabi_fmul
	mov	r1, r0
	mov	r0, r9
	bl	__aeabi_fsub
	mov	r1, r6
	mov	r7, r0
	mov	r0, sl
	bl	__aeabi_fmul
	mov	r1, r5
	mov	sl, r0
	ldr	r0, [sp, #128]	@ float
	bl	__aeabi_fmul
	mov	r1, r0
	mov	r0, sl
	bl	__aeabi_fsub
	mov	r1, r8
	mov	sl, r0
	mov	r0, r8
	bl	__aeabi_fmul
	mov	r1, r7
	mov	r9, r0
	mov	r0, r7
	bl	__aeabi_fmul
	mov	r1, r0
	mov	r0, r9
	bl	__aeabi_fadd
	mov	r1, sl
	mov	r9, r0
	mov	r0, sl
	bl	__aeabi_fmul
	mov	r1, r0
	mov	r0, r9
	bl	__aeabi_fadd
	bl	__aeabi_f2d
	bl	sqrt
	bl	__aeabi_d2f
	mov	r1, #0
	mov	r9, r0
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	bne	.L103
	mov	r1, r9
	mov	r0, #1065353216
	bl	__aeabi_fdiv
	mov	r9, r0
	mov	r1, r9
	mov	r0, r8
	bl	__aeabi_fmul
	mov	r1, r9
	mov	r8, r0
	mov	r0, r7
	bl	__aeabi_fmul
	mov	r1, r9
	mov	r7, r0
	mov	r0, sl
	bl	__aeabi_fmul
	mov	sl, r0
.L103:
	add	r9, sp, #16
	mov	r2, #64
	mov	r1, #0
	mov	r0, r9
	bl	memset
	mov	r3, #1065353216
	mov	r1, r7
	mov	r0, r4
	str	r3, [sp, #76]	@ float
	str	r8, [sp, #16]	@ float
	str	sl, [sp, #48]	@ float
	str	r7, [sp, #32]	@ float
	bl	__aeabi_fmul
	mov	r1, sl
	mov	ip, r0
	mov	r0, r5
	str	ip, [sp, #4]
	bl	__aeabi_fmul
	ldr	r2, [sp, #4]
	mov	r1, r0
	mov	r0, r2
	bl	__aeabi_fsub
	mov	r1, sl
	str	r0, [sp, #20]	@ float
	mov	r0, r6
	bl	__aeabi_fmul
	mov	r1, r8
	mov	sl, r0
	mov	r0, r4
	bl	__aeabi_fmul
	mov	r1, r0
	mov	r0, sl
	bl	__aeabi_fsub
	mov	r1, r8
	str	r0, [sp, #36]	@ float
	mov	r0, r5
	bl	__aeabi_fmul
	mov	r1, r7
	mov	r8, r0
	mov	r0, r6
	bl	__aeabi_fmul
	mov	r1, r0
	mov	r0, r8
	bl	__aeabi_fsub
	add	r6, r6, #-2147483648
	str	r0, [sp, #52]	@ float
	add	r5, r5, #-2147483648
	mov	r0, r9
	add	r4, r4, #-2147483648
	str	r6, [sp, #24]	@ float
	str	r5, [sp, #40]	@ float
	str	r4, [sp, #56]	@ float
	bl	glMultMatrixf
	add	r1, sp, #8
	ldmia	r1, {r1, r3}	@ phole ldm
	add	r0, r1, #-2147483648
	add	r2, fp, #-2147483648
	add	r1, r3, #-2147483648
	bl	glTranslatef
	add	sp, sp, #84
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
	.size	ugluLookAtf, .-ugluLookAtf
	.global	__aeabi_i2f
	.global	__aeabi_f2iz
	.section	.text.ugluLookAtx,"ax",%progbits
	.align	2
	.global	ugluLookAtx
	.hidden	ugluLookAtx
	.type	ugluLookAtx, %function
ugluLookAtx:
	@ Function supports interworking.
	@ args = 20, pretend = 0, frame = 168
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	sub	sp, sp, #172
	ldr	fp, [sp, #208]
	ldr	r7, [sp, #212]
	rsb	r8, r1, fp
	str	r8, [sp, #84]
	rsb	r5, r2, r7
	str	r2, [sp, #100]
	ldr	r2, [sp, #84]
	rsb	r4, r0, r3
	str	r4, [sp, #80]
	str	r1, [sp, #96]
	mov	r1, r2
	mov	r2, r1, asr #31
	str	r1, [sp, #16]
	str	r2, [sp, #20]
	ldr	sl, [sp, #80]
	add	r3, sp, #16
	ldmia	r3, {r3, lr}	@ phole ldm
	mov	r7, sl
	mov	r8, r7, asr #31
	mov	sl, r5
	mov	fp, sl, asr #31
	mul	lr, r1, lr
	str	r5, [sp, #88]
	umull	r1, r2, r7, r7
	umull	r5, r6, r3, r3
	mul	ip, r7, r8
	umull	r3, r4, sl, sl
	mul	r9, sl, fp
	add	r6, r6, lr, asl #1
	add	r4, r4, r9, asl #1
	add	lr, r2, ip, asl #1
	str	r0, [sp, #92]
	mov	r9, r1, lsr #16
	mov	r0, r5, lsr #16
	orr	ip, r0, r6, asl #16
	orr	r1, r9, lr, asl #16
	mov	r6, r3, lsr #16
	add	r5, ip, r1
	orr	r2, r6, r4, asl #16
	add	r0, r5, r2
	mov	r1, r0
	mov	r2, r1, asr #31
	mov	lr, r2, asl #16
	add	r3, r0, #65536
	orr	r4, lr, r1, lsr #16
	mov	r5, r1, asl #16
	mov	r9, r3, asr #1
	mov	r2, r9
	mov	r3, r2, asr #31
	mov	r1, r4
	mov	r0, r5
	bl	__aeabi_ldivmod
	add	r1, r9, r0
	mov	r6, r1, asr #1
	mov	r2, r6
	mov	r3, r2, asr #31
	mov	r1, r4
	mov	r0, r5
	bl	__aeabi_ldivmod
	add	ip, r6, r0
	mov	r9, ip, asr #1
	mov	r2, r9
	mov	r3, r2, asr #31
	mov	r1, r4
	mov	r0, r5
	bl	__aeabi_ldivmod
	add	r1, r9, r0
	mov	r6, r1, asr #1
	mov	r2, r6
	mov	r3, r2, asr #31
	mov	r1, r4
	mov	r0, r5
	bl	__aeabi_ldivmod
	add	ip, r6, r0
	mov	r9, ip, asr #1
	mov	r2, r9
	mov	r3, r2, asr #31
	mov	r1, r4
	mov	r0, r5
	bl	__aeabi_ldivmod
	add	r1, r9, r0
	mov	r6, r1, asr #1
	mov	r2, r6
	mov	r3, r2, asr #31
	mov	r1, r4
	mov	r0, r5
	bl	__aeabi_ldivmod
	add	r0, r6, r0
	mov	r0, r0, asr #1
	bl	__aeabi_i2f
	mov	r1, #0
	mov	r4, r0
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	bne	.L106
	mov	r0, #1191182336
	mov	r1, r4
	add	r0, r0, #8388608
	bl	__aeabi_fdiv
	bl	__aeabi_f2iz
	ldr	r1, [sp, #88]
	ldr	r6, [sp, #84]
	mul	r1, r0, r1
	ldr	r2, [sp, #80]
	mul	r6, r0, r6
	mul	r2, r0, r2
	str	r1, [sp, #88]
	mov	sl, r1
	mov	fp, sl, asr #31
	mov	r0, r6
	mov	r1, r0, asr #31
	str	r0, [sp, #16]
	str	r1, [sp, #20]
	str	r2, [sp, #80]
	str	r6, [sp, #84]
	mov	r7, r2
	mov	r8, r7, asr #31
.L106:
	ldr	ip, [sp, #216]
	ldr	r1, [sp, #220]
	mov	r3, ip
	mov	r4, r3, asr #31
	mov	r2, r1, asr #31
	ldr	r5, [sp, #224]
	str	r3, [sp, #8]
	str	r4, [sp, #12]
	ldr	r9, [sp, #16]
	mul	r4, r7, r2
	mov	r6, r5, asr #31
	mul	r9, r6, r9
	mul	r3, r7, r6
	str	r4, [sp, #48]
	ldr	r4, [sp, #20]
	mla	r3, r5, r8, r3
	mla	r4, r5, r4, r9
	str	r3, [sp, #76]
	str	r4, [sp, #68]
	umull	r3, r4, r7, r5
	ldr	r6, [sp, #12]
	ldr	r0, [sp, #16]
	mul	r0, r6, r0
	ldr	r6, [sp, #16]
	str	r3, [sp, #40]
	str	r4, [sp, #44]
	umull	r3, r4, r6, r5
	ldr	ip, [sp, #12]
	str	r3, [sp, #24]
	str	r4, [sp, #28]
	mul	ip, sl, ip
	umull	r3, r4, sl, r1
	mul	lr, sl, r2
	ldr	r2, [sp, #8]
	str	r3, [sp, #32]
	str	r4, [sp, #36]
	mla	r2, fp, r2, ip
	umull	r3, r4, r7, r1
	mla	lr, r1, fp, lr
	str	r0, [sp, #56]
	ldr	ip, [sp, #48]
	ldr	r0, [sp, #8]
	str	r2, [sp, #4]
	mov	r2, r4
	ldr	r4, [sp, #8]
	umull	r5, r6, sl, r0
	mla	ip, r1, r8, ip
	ldr	r9, [sp, #16]
	mov	r1, r3
	ldr	r0, [sp, #20]
	ldr	r3, [sp, #56]
	str	lr, [sp, #72]
	mov	lr, r4
	mla	r0, r4, r0, r3
	umull	r3, r4, r9, lr
	ldr	lr, [sp, #28]
	str	r3, [sp, #48]
	str	r4, [sp, #52]
	add	r3, sp, #68
	ldmia	r3, {r3, r9}	@ phole ldm
	add	r4, r3, lr
	ldr	lr, [sp, #36]
	add	r3, r9, lr
	ldr	r9, [sp, #44]
	ldr	lr, [sp, #76]
	str	r4, [sp, #28]
	ldr	r4, [sp, #4]
	str	r3, [sp, #36]
	add	r3, lr, r9
	str	r3, [sp, #44]
	add	r6, r4, r6
	ldr	lr, [sp, #24]
	ldr	r4, [sp, #52]
	add	r2, ip, r2
	ldr	r9, [sp, #32]
	add	ip, r0, r4
	ldr	r3, [sp, #40]
	mov	r0, lr, lsr #16
	mov	lr, r5, lsr #16
	orr	lr, lr, r6, asl #16
	ldr	r6, [sp, #44]
	str	ip, [sp, #52]
	ldr	r5, [sp, #36]
	mov	ip, r9, lsr #16
	mov	r9, r3, lsr #16
	ldr	r3, [sp, #48]
	ldr	r4, [sp, #28]
	orr	r9, r9, r6, asl #16
	orr	ip, ip, r5, asl #16
	mov	r5, r3, lsr #16
	rsb	r3, r9, lr
	ldr	lr, [sp, #52]
	orr	r0, r0, r4, asl #16
	mov	r4, r1, lsr #16
	orr	r6, r4, r2, asl #16
	rsb	r1, ip, r0
	orr	r0, r5, lr, asl #16
	rsb	r2, r0, r6
	str	r1, [sp, #68]
	mov	r5, r1
	mov	r6, r5, asr #31
	mov	r0, r3
	mov	r1, r0, asr #31
	str	r0, [sp, #40]
	str	r1, [sp, #44]
	mul	r1, r0, r1
	str	r2, [sp, #76]
	str	r3, [sp, #72]
	ldr	r3, [sp, #76]
	str	r5, [sp, #24]
	str	r6, [sp, #28]
	mov	r2, r3
	mov	r3, r2, asr #31
	ldr	ip, [sp, #28]
	str	r2, [sp, #32]
	str	r3, [sp, #36]
	ldr	r4, [sp, #40]
	ldr	lr, [sp, #24]
	mul	ip, r5, ip
	ldr	r5, [sp, #32]
	str	r1, [sp, #48]
	umull	r1, r2, r4, r4
	umull	r3, r4, lr, lr
	ldr	lr, [sp, #36]
	mov	r0, r5
	mul	lr, r5, lr
	umull	r5, r6, r0, r0
	ldr	r9, [sp, #48]
	add	r4, r4, ip, asl #1
	add	r2, r2, r9, asl #1
	mov	ip, r3, lsr #16
	mov	r0, r1, lsr #16
	orr	r9, ip, r4, asl #16
	add	r6, r6, lr, asl #1
	mov	r1, r5, lsr #16
	orr	lr, r0, r2, asl #16
	add	r0, lr, r9
	orr	r2, r1, r6, asl #16
	add	r4, r0, r2
	mov	r1, r4
	mov	r2, r1, asr #31
	add	lr, r4, #65536
	mov	r3, r2, asl #16
	mov	r9, lr, asr #1
	orr	r4, r3, r1, lsr #16
	mov	r5, r1, asl #16
	mov	r2, r9
	mov	r3, r2, asr #31
	mov	r1, r4
	mov	r0, r5
	bl	__aeabi_ldivmod
	add	ip, r9, r0
	mov	r6, ip, asr #1
	mov	r2, r6
	mov	r3, r2, asr #31
	mov	r1, r4
	mov	r0, r5
	bl	__aeabi_ldivmod
	add	r1, r6, r0
	mov	r9, r1, asr #1
	mov	r2, r9
	mov	r3, r2, asr #31
	mov	r1, r4
	mov	r0, r5
	bl	__aeabi_ldivmod
	add	ip, r9, r0
	mov	r6, ip, asr #1
	mov	r2, r6
	mov	r3, r2, asr #31
	mov	r1, r4
	mov	r0, r5
	bl	__aeabi_ldivmod
	add	r1, r6, r0
	mov	r9, r1, asr #1
	mov	r2, r9
	mov	r3, r2, asr #31
	mov	r1, r4
	mov	r0, r5
	bl	__aeabi_ldivmod
	add	ip, r9, r0
	mov	r6, ip, asr #1
	mov	r2, r6
	mov	r3, r2, asr #31
	mov	r1, r4
	mov	r0, r5
	bl	__aeabi_ldivmod
	add	r9, r6, r0
	mov	r0, r9, asr #1
	bl	__aeabi_i2f
	mov	r1, #0
	mov	r4, r0
	bl	__aeabi_fcmpeq
	cmp	r0, #0
	bne	.L107
	mov	r5, #1191182336
	mov	r1, r4
	add	r0, r5, #8388608
	bl	__aeabi_fdiv
	bl	__aeabi_f2iz
	ldr	r1, [sp, #76]
	add	r2, sp, #68
	ldmia	r2, {r2, r3}	@ phole ldm
	mul	r1, r0, r1
	mul	r2, r0, r2
	mul	r3, r0, r3
	mov	r4, r1
	mov	r5, r4, asr #31
	str	r1, [sp, #76]
	str	r2, [sp, #68]
	mov	r0, r3
	mov	r1, r0, asr #31
	str	r3, [sp, #72]
	mov	r3, r2, asr #31
	str	r4, [sp, #32]
	str	r5, [sp, #36]
	str	r0, [sp, #40]
	str	r1, [sp, #44]
	str	r2, [sp, #24]
	str	r3, [sp, #28]
.L107:
	mov	r1, #0
	mov	r2, #64
	add	r0, sp, #104
	bl	memset
	ldr	ip, [sp, #32]
	add	r1, sp, #20
	ldmia	r1, {r1, r5}	@ phole ldm
	ldr	lr, [sp, #40]
	ldr	r9, [sp, #16]
	ldr	r0, [sp, #40]
	mul	r1, ip, r1
	mov	r2, ip
	ldr	ip, [sp, #20]
	mul	r2, r8, r2
	mul	lr, r8, lr
	mov	r8, r5
	mul	r0, fp, r0
	mul	ip, r5, ip
	umull	r4, r5, r8, r9
	add	r6, sp, #40
	ldmia	r6, {r6, r9}	@ phole ldm
	str	r4, [sp, #48]
	str	r5, [sp, #52]
	mla	r9, sl, r9, r0
	umull	r4, r5, r6, r7
	ldr	r0, [sp, #16]
	ldr	r6, [sp, #36]
	ldr	r3, [sp, #24]
	mla	r6, r0, r6, r1
	ldr	r1, [sp, #36]
	mul	r3, fp, r3
	mla	r1, r7, r1, r2
	ldr	fp, [sp, #28]
	str	r1, [sp, #8]
	mla	fp, sl, fp, r3
	ldr	r8, [sp, #28]
	ldr	r1, [sp, #44]
	ldr	r2, [sp, #32]
	mla	ip, r0, r8, ip
	mla	lr, r7, r1, lr
	umull	r0, r1, r2, r7
	str	r4, [sp, #56]
	str	r5, [sp, #60]
	str	fp, [sp, #4]
	ldr	r4, [sp, #24]
	ldr	fp, [sp, #40]
	mov	r8, r1
	umull	r1, r2, r4, sl
	umull	r4, r5, fp, sl
	ldr	r3, [sp, #32]
	mov	r7, r0
	ldr	r0, [sp, #16]
	mov	sl, r4
	mov	fp, r5
	umull	r4, r5, r3, r0
	add	r5, r6, r5
	ldr	r6, [sp, #52]
	ldr	r3, [sp, #4]
	ldr	r0, [sp, #8]
	add	fp, r9, fp
	add	r9, ip, r6
	add	r6, sp, #56
	ldmia	r6, {r6, ip}	@ phole ldm
	add	r2, r3, r2
	ldr	r3, [sp, #48]
	add	r8, r0, r8
	add	r0, lr, ip
	str	r0, [sp, #60]
	mov	ip, r4, lsr #16
	mov	r0, r1, lsr #16
	mov	r1, r3, lsr #16
	mov	r3, r6, lsr #16
	str	r9, [sp, #52]
	orr	r6, ip, r5, asl #16
	str	r3, [sp, #16]
	mov	r9, sl, lsr #16
	ldr	r5, [sp, #52]
	ldr	r4, [sp, #60]
	mov	lr, r7, lsr #16
	orr	ip, r0, r2, asl #16
	orr	r9, r9, fp, asl #16
	orr	lr, lr, r8, asl #16
	orr	r0, r1, r5, asl #16
	rsb	lr, ip, lr
	ldr	r2, [sp, #84]
	ldr	ip, [sp, #80]
	orr	r1, r3, r4, asl #16
	rsb	r9, r6, r9
	ldr	r4, [sp, #68]
	ldr	r6, [sp, #88]
	rsb	r3, r1, r0
	str	r4, [sp, #104]
	rsb	r1, ip, #0
	ldr	r4, [sp, #76]
	rsb	ip, r2, #0
	rsb	r2, r6, #0
	ldr	r6, [sp, #72]
	add	r0, sp, #104
	mov	r5, #65536
	str	r5, [sp, #164]
	str	r6, [sp, #120]
	str	r9, [sp, #108]
	str	r1, [sp, #112]
	str	r2, [sp, #144]
	str	r4, [sp, #136]
	str	lr, [sp, #124]
	str	r3, [sp, #140]
	str	ip, [sp, #128]
	bl	glMultMatrixx
	add	r5, sp, #92
	ldmia	r5, {r5, r6, r9}	@ phole ldm
	rsb	r0, r5, #0
	rsb	r1, r6, #0
	rsb	r2, r9, #0
	bl	glTranslatex
	add	sp, sp, #172
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
	.size	ugluLookAtx, .-ugluLookAtx
	.section	.rodata
	.align	2
	.set	.LANCHOR0,. + 0
	.type	__gl_sin_tab, %object
	.size	__gl_sin_tab, 2048
__gl_sin_tab:
	.short	0
	.short	100
	.short	201
	.short	301
	.short	402
	.short	503
	.short	603
	.short	704
	.short	805
	.short	905
	.short	1006
	.short	1106
	.short	1207
	.short	1308
	.short	1408
	.short	1509
	.short	1609
	.short	1710
	.short	1811
	.short	1911
	.short	2012
	.short	2112
	.short	2213
	.short	2313
	.short	2414
	.short	2515
	.short	2615
	.short	2716
	.short	2816
	.short	2917
	.short	3017
	.short	3118
	.short	3218
	.short	3319
	.short	3419
	.short	3520
	.short	3620
	.short	3721
	.short	3821
	.short	3922
	.short	4022
	.short	4123
	.short	4223
	.short	4323
	.short	4424
	.short	4524
	.short	4625
	.short	4725
	.short	4825
	.short	4926
	.short	5026
	.short	5126
	.short	5227
	.short	5327
	.short	5427
	.short	5527
	.short	5628
	.short	5728
	.short	5828
	.short	5928
	.short	6029
	.short	6129
	.short	6229
	.short	6329
	.short	6429
	.short	6529
	.short	6630
	.short	6730
	.short	6830
	.short	6930
	.short	7030
	.short	7130
	.short	7230
	.short	7330
	.short	7430
	.short	7530
	.short	7630
	.short	7730
	.short	7830
	.short	7930
	.short	8029
	.short	8129
	.short	8229
	.short	8329
	.short	8429
	.short	8529
	.short	8628
	.short	8728
	.short	8828
	.short	8928
	.short	9027
	.short	9127
	.short	9226
	.short	9326
	.short	9426
	.short	9525
	.short	9625
	.short	9724
	.short	9824
	.short	9923
	.short	10023
	.short	10122
	.short	10222
	.short	10321
	.short	10420
	.short	10520
	.short	10619
	.short	10718
	.short	10818
	.short	10917
	.short	11016
	.short	11115
	.short	11214
	.short	11313
	.short	11413
	.short	11512
	.short	11611
	.short	11710
	.short	11809
	.short	11908
	.short	12007
	.short	12106
	.short	12204
	.short	12303
	.short	12402
	.short	12501
	.short	12600
	.short	12698
	.short	12797
	.short	12896
	.short	12994
	.short	13093
	.short	13192
	.short	13290
	.short	13389
	.short	13487
	.short	13586
	.short	13684
	.short	13782
	.short	13881
	.short	13979
	.short	14077
	.short	14176
	.short	14274
	.short	14372
	.short	14470
	.short	14568
	.short	14666
	.short	14765
	.short	14863
	.short	14961
	.short	15059
	.short	15156
	.short	15254
	.short	15352
	.short	15450
	.short	15548
	.short	15645
	.short	15743
	.short	15841
	.short	15938
	.short	16036
	.short	16134
	.short	16231
	.short	16329
	.short	16426
	.short	16523
	.short	16621
	.short	16718
	.short	16815
	.short	16913
	.short	17010
	.short	17107
	.short	17204
	.short	17301
	.short	17398
	.short	17495
	.short	17592
	.short	17689
	.short	17786
	.short	17883
	.short	17980
	.short	18076
	.short	18173
	.short	18270
	.short	18366
	.short	18463
	.short	18559
	.short	18656
	.short	18752
	.short	18849
	.short	18945
	.short	19041
	.short	19138
	.short	19234
	.short	19330
	.short	19426
	.short	19522
	.short	19618
	.short	19714
	.short	19810
	.short	19906
	.short	20002
	.short	20098
	.short	20193
	.short	20289
	.short	20385
	.short	20480
	.short	20576
	.short	20672
	.short	20767
	.short	20862
	.short	20958
	.short	21053
	.short	21148
	.short	21244
	.short	21339
	.short	21434
	.short	21529
	.short	21624
	.short	21719
	.short	21814
	.short	21909
	.short	22004
	.short	22098
	.short	22193
	.short	22288
	.short	22382
	.short	22477
	.short	22571
	.short	22666
	.short	22760
	.short	22854
	.short	22949
	.short	23043
	.short	23137
	.short	23231
	.short	23325
	.short	23419
	.short	23513
	.short	23607
	.short	23701
	.short	23795
	.short	23889
	.short	23982
	.short	24076
	.short	24169
	.short	24263
	.short	24356
	.short	24450
	.short	24543
	.short	24636
	.short	24730
	.short	24823
	.short	24916
	.short	25009
	.short	25102
	.short	25195
	.short	25288
	.short	25380
	.short	25473
	.short	25566
	.short	25659
	.short	25751
	.short	25844
	.short	25936
	.short	26028
	.short	26121
	.short	26213
	.short	26305
	.short	26397
	.short	26489
	.short	26581
	.short	26673
	.short	26765
	.short	26857
	.short	26949
	.short	27041
	.short	27132
	.short	27224
	.short	27315
	.short	27407
	.short	27498
	.short	27589
	.short	27681
	.short	27772
	.short	27863
	.short	27954
	.short	28045
	.short	28136
	.short	28227
	.short	28317
	.short	28408
	.short	28499
	.short	28589
	.short	28680
	.short	28770
	.short	28861
	.short	28951
	.short	29041
	.short	29131
	.short	29222
	.short	29312
	.short	29402
	.short	29491
	.short	29581
	.short	29671
	.short	29761
	.short	29850
	.short	29940
	.short	30029
	.short	30119
	.short	30208
	.short	30297
	.short	30387
	.short	30476
	.short	30565
	.short	30654
	.short	30743
	.short	30831
	.short	30920
	.short	31009
	.short	31098
	.short	31186
	.short	31275
	.short	31363
	.short	31451
	.short	31539
	.short	31628
	.short	31716
	.short	31804
	.short	31892
	.short	31980
	.short	32067
	.short	32155
	.short	32243
	.short	32330
	.short	32418
	.short	32505
	.short	32593
	.short	32680
	.short	32767
	.short	-32682
	.short	-32595
	.short	-32508
	.short	-32421
	.short	-32334
	.short	-32248
	.short	-32161
	.short	-32074
	.short	-31988
	.short	-31901
	.short	-31815
	.short	-31729
	.short	-31643
	.short	-31557
	.short	-31471
	.short	-31385
	.short	-31299
	.short	-31213
	.short	-31127
	.short	-31042
	.short	-30956
	.short	-30871
	.short	-30785
	.short	-30700
	.short	-30615
	.short	-30530
	.short	-30445
	.short	-30360
	.short	-30275
	.short	-30190
	.short	-30106
	.short	-30021
	.short	-29936
	.short	-29852
	.short	-29768
	.short	-29683
	.short	-29599
	.short	-29515
	.short	-29431
	.short	-29347
	.short	-29263
	.short	-29180
	.short	-29096
	.short	-29012
	.short	-28929
	.short	-28845
	.short	-28762
	.short	-28679
	.short	-28596
	.short	-28512
	.short	-28430
	.short	-28347
	.short	-28264
	.short	-28181
	.short	-28098
	.short	-28016
	.short	-27933
	.short	-27851
	.short	-27769
	.short	-27687
	.short	-27604
	.short	-27522
	.short	-27441
	.short	-27359
	.short	-27277
	.short	-27195
	.short	-27114
	.short	-27032
	.short	-26951
	.short	-26870
	.short	-26788
	.short	-26707
	.short	-26626
	.short	-26545
	.short	-26465
	.short	-26384
	.short	-26303
	.short	-26223
	.short	-26142
	.short	-26062
	.short	-25981
	.short	-25901
	.short	-25821
	.short	-25741
	.short	-25661
	.short	-25581
	.short	-25502
	.short	-25422
	.short	-25343
	.short	-25263
	.short	-25184
	.short	-25105
	.short	-25025
	.short	-24946
	.short	-24867
	.short	-24789
	.short	-24710
	.short	-24631
	.short	-24553
	.short	-24474
	.short	-24396
	.short	-24317
	.short	-24239
	.short	-24161
	.short	-24083
	.short	-24005
	.short	-23928
	.short	-23850
	.short	-23772
	.short	-23695
	.short	-23617
	.short	-23540
	.short	-23463
	.short	-23386
	.short	-23309
	.short	-23232
	.short	-23155
	.short	-23078
	.short	-23002
	.short	-22925
	.short	-22849
	.short	-22773
	.short	-22696
	.short	-22620
	.short	-22544
	.short	-22468
	.short	-22393
	.short	-22317
	.short	-22241
	.short	-22166
	.short	-22090
	.short	-22015
	.short	-21940
	.short	-21865
	.short	-21790
	.short	-21715
	.short	-21640
	.short	-21566
	.short	-21491
	.short	-21416
	.short	-21342
	.short	-21268
	.short	-21194
	.short	-21120
	.short	-21046
	.short	-20972
	.short	-20898
	.short	-20825
	.short	-20751
	.short	-20678
	.short	-20604
	.short	-20531
	.short	-20458
	.short	-20385
	.short	-20312
	.short	-20239
	.short	-20167
	.short	-20094
	.short	-20022
	.short	-19949
	.short	-19877
	.short	-19805
	.short	-19733
	.short	-19661
	.short	-19589
	.short	-19518
	.short	-19446
	.short	-19374
	.short	-19303
	.short	-19232
	.short	-19161
	.short	-19090
	.short	-19019
	.short	-18948
	.short	-18877
	.short	-18807
	.short	-18736
	.short	-18666
	.short	-18595
	.short	-18525
	.short	-18455
	.short	-18385
	.short	-18315
	.short	-18246
	.short	-18176
	.short	-18107
	.short	-18037
	.short	-17968
	.short	-17899
	.short	-17830
	.short	-17761
	.short	-17692
	.short	-17623
	.short	-17555
	.short	-17486
	.short	-17418
	.short	-17350
	.short	-17281
	.short	-17213
	.short	-17145
	.short	-17078
	.short	-17010
	.short	-16942
	.short	-16875
	.short	-16808
	.short	-16740
	.short	-16673
	.short	-16606
	.short	-16539
	.short	-16473
	.short	-16406
	.short	-16339
	.short	-16273
	.short	-16207
	.short	-16140
	.short	-16074
	.short	-16008
	.short	-15943
	.short	-15877
	.short	-15811
	.short	-15746
	.short	-15680
	.short	-15615
	.short	-15550
	.short	-15485
	.short	-15420
	.short	-15355
	.short	-15291
	.short	-15226
	.short	-15162
	.short	-15097
	.short	-15033
	.short	-14969
	.short	-14905
	.short	-14841
	.short	-14778
	.short	-14714
	.short	-14651
	.short	-14587
	.short	-14524
	.short	-14461
	.short	-14398
	.short	-14335
	.short	-14272
	.short	-14210
	.short	-14147
	.short	-14085
	.short	-14023
	.short	-13960
	.short	-13898
	.short	-13836
	.short	-13775
	.short	-13713
	.short	-13651
	.short	-13590
	.short	-13529
	.short	-13468
	.short	-13407
	.short	-13346
	.short	-13285
	.short	-13224
	.short	-13164
	.short	-13103
	.short	-13043
	.short	-12983
	.short	-12923
	.short	-12863
	.short	-12803
	.short	-12743
	.short	-12684
	.short	-12624
	.short	-12565
	.short	-12506
	.short	-12447
	.short	-12388
	.short	-12329
	.short	-12270
	.short	-12212
	.short	-12153
	.short	-12095
	.short	-12037
	.short	-11979
	.short	-11921
	.short	-11863
	.short	-11805
	.short	-11748
	.short	-11690
	.short	-11633
	.short	-11576
	.short	-11519
	.short	-11462
	.short	-11405
	.short	-11348
	.short	-11292
	.short	-11236
	.short	-11179
	.short	-11123
	.short	-11067
	.short	-11011
	.short	-10955
	.short	-10900
	.short	-10844
	.short	-10789
	.short	-10734
	.short	-10679
	.short	-10624
	.short	-10569
	.short	-10514
	.short	-10459
	.short	-10405
	.short	-10351
	.short	-10296
	.short	-10242
	.short	-10188
	.short	-10135
	.short	-10081
	.short	-10027
	.short	-9974
	.short	-9921
	.short	-9867
	.short	-9814
	.short	-9762
	.short	-9709
	.short	-9656
	.short	-9604
	.short	-9551
	.short	-9499
	.short	-9447
	.short	-9395
	.short	-9343
	.short	-9291
	.short	-9240
	.short	-9188
	.short	-9137
	.short	-9086
	.short	-9035
	.short	-8984
	.short	-8933
	.short	-8882
	.short	-8832
	.short	-8782
	.short	-8731
	.short	-8681
	.short	-8631
	.short	-8581
	.short	-8532
	.short	-8482
	.short	-8433
	.short	-8383
	.short	-8334
	.short	-8285
	.short	-8236
	.short	-8187
	.short	-8139
	.short	-8090
	.short	-8042
	.short	-7994
	.short	-7946
	.short	-7898
	.short	-7850
	.short	-7802
	.short	-7755
	.short	-7707
	.short	-7660
	.short	-7613
	.short	-7566
	.short	-7519
	.short	-7472
	.short	-7426
	.short	-7379
	.short	-7333
	.short	-7287
	.short	-7241
	.short	-7195
	.short	-7149
	.short	-7103
	.short	-7058
	.short	-7012
	.short	-6967
	.short	-6922
	.short	-6877
	.short	-6832
	.short	-6788
	.short	-6743
	.short	-6699
	.short	-6655
	.short	-6611
	.short	-6567
	.short	-6523
	.short	-6479
	.short	-6435
	.short	-6392
	.short	-6349
	.short	-6306
	.short	-6263
	.short	-6220
	.short	-6177
	.short	-6135
	.short	-6092
	.short	-6050
	.short	-6008
	.short	-5966
	.short	-5924
	.short	-5882
	.short	-5840
	.short	-5799
	.short	-5758
	.short	-5717
	.short	-5675
	.short	-5635
	.short	-5594
	.short	-5553
	.short	-5513
	.short	-5472
	.short	-5432
	.short	-5392
	.short	-5352
	.short	-5313
	.short	-5273
	.short	-5234
	.short	-5194
	.short	-5155
	.short	-5116
	.short	-5077
	.short	-5038
	.short	-5000
	.short	-4961
	.short	-4923
	.short	-4885
	.short	-4847
	.short	-4809
	.short	-4771
	.short	-4733
	.short	-4696
	.short	-4659
	.short	-4621
	.short	-4584
	.short	-4547
	.short	-4511
	.short	-4474
	.short	-4438
	.short	-4401
	.short	-4365
	.short	-4329
	.short	-4293
	.short	-4257
	.short	-4222
	.short	-4186
	.short	-4151
	.short	-4116
	.short	-4081
	.short	-4046
	.short	-4011
	.short	-3977
	.short	-3942
	.short	-3908
	.short	-3874
	.short	-3840
	.short	-3806
	.short	-3772
	.short	-3739
	.short	-3705
	.short	-3672
	.short	-3639
	.short	-3606
	.short	-3573
	.short	-3540
	.short	-3508
	.short	-3475
	.short	-3443
	.short	-3411
	.short	-3379
	.short	-3347
	.short	-3316
	.short	-3284
	.short	-3253
	.short	-3221
	.short	-3190
	.short	-3159
	.short	-3129
	.short	-3098
	.short	-3068
	.short	-3037
	.short	-3007
	.short	-2977
	.short	-2947
	.short	-2917
	.short	-2888
	.short	-2858
	.short	-2829
	.short	-2800
	.short	-2771
	.short	-2742
	.short	-2713
	.short	-2685
	.short	-2656
	.short	-2628
	.short	-2600
	.short	-2572
	.short	-2544
	.short	-2516
	.short	-2489
	.short	-2461
	.short	-2434
	.short	-2407
	.short	-2380
	.short	-2353
	.short	-2327
	.short	-2300
	.short	-2274
	.short	-2248
	.short	-2222
	.short	-2196
	.short	-2170
	.short	-2144
	.short	-2119
	.short	-2094
	.short	-2068
	.short	-2043
	.short	-2019
	.short	-1994
	.short	-1969
	.short	-1945
	.short	-1921
	.short	-1897
	.short	-1873
	.short	-1849
	.short	-1825
	.short	-1802
	.short	-1778
	.short	-1755
	.short	-1732
	.short	-1709
	.short	-1686
	.short	-1664
	.short	-1641
	.short	-1619
	.short	-1597
	.short	-1575
	.short	-1553
	.short	-1531
	.short	-1510
	.short	-1489
	.short	-1467
	.short	-1446
	.short	-1425
	.short	-1404
	.short	-1384
	.short	-1363
	.short	-1343
	.short	-1323
	.short	-1303
	.short	-1283
	.short	-1263
	.short	-1244
	.short	-1224
	.short	-1205
	.short	-1186
	.short	-1167
	.short	-1148
	.short	-1129
	.short	-1111
	.short	-1092
	.short	-1074
	.short	-1056
	.short	-1038
	.short	-1021
	.short	-1003
	.short	-986
	.short	-968
	.short	-951
	.short	-934
	.short	-917
	.short	-901
	.short	-884
	.short	-868
	.short	-851
	.short	-835
	.short	-819
	.short	-804
	.short	-788
	.short	-773
	.short	-757
	.short	-742
	.short	-727
	.short	-712
	.short	-697
	.short	-683
	.short	-669
	.short	-654
	.short	-640
	.short	-626
	.short	-612
	.short	-599
	.short	-585
	.short	-572
	.short	-559
	.short	-546
	.short	-533
	.short	-520
	.short	-508
	.short	-495
	.short	-483
	.short	-471
	.short	-459
	.short	-447
	.short	-436
	.short	-424
	.short	-413
	.short	-402
	.short	-391
	.short	-380
	.short	-369
	.short	-358
	.short	-348
	.short	-338
	.short	-328
	.short	-318
	.short	-308
	.short	-298
	.short	-289
	.short	-279
	.short	-270
	.short	-261
	.short	-252
	.short	-244
	.short	-235
	.short	-227
	.short	-218
	.short	-210
	.short	-202
	.short	-195
	.short	-187
	.short	-179
	.short	-172
	.short	-165
	.short	-158
	.short	-151
	.short	-144
	.short	-138
	.short	-131
	.short	-125
	.short	-119
	.short	-113
	.short	-107
	.short	-102
	.short	-96
	.short	-91
	.short	-86
	.short	-81
	.short	-76
	.short	-71
	.short	-66
	.short	-62
	.short	-58
	.short	-54
	.short	-50
	.short	-46
	.short	-42
	.short	-39
	.short	-36
	.short	-32
	.short	-29
	.short	-27
	.short	-24
	.short	-21
	.short	-19
	.short	-17
	.short	-15
	.short	-13
	.short	-11
	.short	-9
	.short	-8
	.short	-6
	.short	-5
	.short	-4
	.short	-3
	.short	-3
	.short	-2
	.short	-2
	.short	-2
	.short	-2
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
