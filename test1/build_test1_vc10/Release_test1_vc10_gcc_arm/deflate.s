	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"deflate.c"
	.section	.text.deflateSetHeader,"ax",%progbits
	.align	2
	.global	deflateSetHeader
	.hidden	deflateSetHeader
	.type	deflateSetHeader, %function
deflateSetHeader:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	bne	.L6
.L2:
	mvn	r0, #1
	bx	lr
.L6:
	ldr	r3, [r0, #28]
	cmp	r3, #0
	beq	.L2
	ldr	r2, [r3, #24]
	cmp	r2, #2
	streq	r1, [r3, #28]
	moveq	r0, #0
	bne	.L2
	bx	lr
	.size	deflateSetHeader, .-deflateSetHeader
	.section	.text.deflatePrime,"ax",%progbits
	.align	2
	.global	deflatePrime
	.hidden	deflatePrime
	.type	deflatePrime, %function
deflatePrime:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	bne	.L11
.L8:
	mvn	r0, #1
	bx	lr
.L11:
	ldr	ip, [r0, #28]
	cmp	ip, #0
	beq	.L8
	mvn	r3, #0
	bic	r2, r2, r3, asl r1
	mov	r3, #5760
	add	r3, r3, #60
	str	r1, [ip, r3]
	ldr	r1, [r0, #28]
	mov	r0, #5760
	add	r3, r0, #56
	strh	r2, [r1, r3]	@ movhi
	mov	r0, #0
	bx	lr
	.size	deflatePrime, .-deflatePrime
	.section	.text.deflateTune,"ax",%progbits
	.align	2
	.global	deflateTune
	.hidden	deflateTune
	.type	deflateTune, %function
deflateTune:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	bne	.L16
.L13:
	mvn	r0, #1
	bx	lr
.L16:
	ldr	r0, [r0, #28]
	cmp	r0, #0
	beq	.L13
	ldr	ip, [sp, #0]
	str	r1, [r0, #140]
	str	ip, [r0, #124]
	str	r2, [r0, #128]
	str	r3, [r0, #144]
	mov	r0, #0
	bx	lr
	.size	deflateTune, .-deflateTune
	.section	.text.deflateEnd,"ax",%progbits
	.align	2
	.global	deflateEnd
	.hidden	deflateEnd
	.type	deflateEnd, %function
deflateEnd:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	subs	r4, r0, #0
	bne	.L27
.L18:
	mvn	r0, #1
.L25:
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L27:
	ldr	r3, [r4, #28]
	cmp	r3, #0
	beq	.L18
	ldr	r5, [r3, #4]
	cmp	r5, #69
	cmpne	r5, #42
	beq	.L19
	cmp	r5, #91
	cmpne	r5, #73
	bne	.L28
.L19:
	ldr	r1, [r3, #8]
	cmp	r1, #0
	beq	.L20
	ldr	r0, [r4, #40]
	ldr	ip, [r4, #36]
	mov	lr, pc
	bx	ip
	ldr	r3, [r4, #28]
.L20:
	ldr	r1, [r3, #68]
	cmp	r1, #0
	beq	.L21
	ldr	r0, [r4, #40]
	ldr	ip, [r4, #36]
	mov	lr, pc
	bx	ip
	ldr	r3, [r4, #28]
.L21:
	ldr	r1, [r3, #64]
	cmp	r1, #0
	beq	.L22
	ldr	r0, [r4, #40]
	ldr	ip, [r4, #36]
	mov	lr, pc
	bx	ip
	ldr	r3, [r4, #28]
.L22:
	ldr	r1, [r3, #56]
	cmp	r1, #0
	beq	.L23
	ldr	r0, [r4, #40]
	ldr	ip, [r4, #36]
	mov	lr, pc
	bx	ip
	ldr	r3, [r4, #28]
.L23:
	mov	r1, r3
	ldr	r0, [r4, #40]
	ldr	ip, [r4, #36]
	mov	lr, pc
	bx	ip
	mov	r3, #0
	cmp	r5, #113
	str	r3, [r4, #28]
	mvneq	r0, #2
	movne	r0, r3
	b	.L25
.L28:
	cmp	r5, #113
	cmpne	r5, #103
	beq	.L19
	mov	r0, #664
	add	r2, r0, #2
	cmp	r5, r2
	bne	.L18
	b	.L19
	.size	deflateEnd, .-deflateEnd
	.section	.text.deflateCopy,"ax",%progbits
	.align	2
	.global	deflateCopy
	.hidden	deflateCopy
	.type	deflateCopy, %function
deflateCopy:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	cmpne	r1, #0
	stmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, lr}
	mov	r4, r0
	movne	r5, #0
	moveq	r5, #1
	bne	.L37
.L30:
	mvn	r0, #1
.L32:
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, lr}
	bx	lr
.L37:
	ldr	r6, [r1, #28]
	cmp	r6, #0
	beq	.L30
	mov	lr, r1
	ldmia	lr!, {r0, r1, r2, r3}
	mov	r7, r4
	stmia	r7!, {r0, r1, r2, r3}
	ldmia	lr!, {r0, r1, r2, r3}
	mov	ip, r7
	stmia	ip!, {r0, r1, r2, r3}
	ldmia	lr!, {r0, r1, r2, r3}
	stmia	ip!, {r0, r1, r2, r3}
	ldmia	lr, {r0, r1}
	stmia	ip, {r0, r1}
	mov	r2, #5824
	ldr	r0, [r4, #40]
	mov	r1, #1
	ldr	ip, [r4, #32]
	mov	lr, pc
	bx	ip
	subs	r7, r0, #0
	beq	.L36
	mov	r1, r6
	mov	r2, #5824
	str	r7, [r4, #28]
	bl	memcpy
	str	r4, [r7, #0]
	ldr	r1, [r7, #44]
	mov	r2, #2
	ldr	r0, [r4, #40]
	ldr	ip, [r4, #32]
	mov	lr, pc
	bx	ip
	str	r0, [r7, #56]
	ldr	r1, [r7, #44]
	mov	r2, #2
	ldr	r0, [r4, #40]
	ldr	ip, [r4, #32]
	mov	lr, pc
	bx	ip
	str	r0, [r7, #64]
	ldr	r1, [r7, #76]
	mov	r2, #2
	ldr	r0, [r4, #40]
	ldr	ip, [r4, #32]
	mov	lr, pc
	bx	ip
	mov	r8, #5760
	str	r0, [r7, #68]
	add	r8, r8, #28
	ldr	r0, [r4, #40]
	ldr	r1, [r7, r8]
	mov	r2, #4
	ldr	ip, [r4, #32]
	mov	lr, pc
	bx	ip
	mov	sl, r0
	ldr	r0, [r7, #56]
	cmp	r0, #0
	str	sl, [r7, #8]
	beq	.L33
	ldr	r1, [r7, #64]
	cmp	r1, #0
	beq	.L33
	ldr	r2, [r7, #68]
	cmp	r2, #0
	beq	.L33
	cmp	sl, #0
	beq	.L33
	ldr	lr, [r7, #44]
	ldr	r1, [r6, #56]
	mov	r2, lr, asl #1
	bl	memcpy
	ldr	r0, [r7, #44]
	ldr	r1, [r6, #64]
	mov	r2, r0, asl #1
	ldr	r0, [r7, #64]
	bl	memcpy
	ldr	r1, [r7, #76]
	ldr	r0, [r7, #68]
	mov	r2, r1, asl #1
	ldr	r1, [r6, #68]
	bl	memcpy
	ldr	r1, [r6, #8]
	add	r0, r7, #8
	ldmia	r0, {r0, r2}	@ phole ldm
	bl	memcpy
	ldr	r3, [r7, r8]
	ldr	r2, [r6, #8]
	ldr	r8, [r6, #16]
	ldr	ip, [r7, #8]
	mov	r0, #5760
	rsb	r4, r2, r8
	add	r6, r3, r3, asl #1
	bic	r8, r3, #1
	mov	r1, r0
	add	r3, r7, #2432
	add	r2, r7, #2672
	add	r6, ip, r6
	add	r0, r0, #36
	add	ip, ip, r4
	add	sl, sl, r8
	add	r1, r1, #24
	add	r3, r3, #8
	add	r2, r2, #12
	add	r4, r7, #148
	str	sl, [r7, r0]
	str	r6, [r7, r1]
	str	r2, [r7, #2864]
	str	ip, [r7, #16]
	str	r4, [r7, #2840]
	str	r3, [r7, #2852]
	mov	r0, r5
	b	.L32
.L33:
	mov	r0, r4
	bl	deflateEnd
.L36:
	mvn	r0, #3
	b	.L32
	.size	deflateCopy, .-deflateCopy
	.section	.text.flush_pending,"ax",%progbits
	.align	2
	.type	flush_pending, %function
flush_pending:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	ldr	r3, [r0, #28]
	ldr	r2, [r0, #16]
	ldr	r5, [r3, #20]
	cmp	r5, r2
	movcs	r5, r2
	cmp	r5, #0
	mov	r4, r0
	beq	.L40
	ldr	r1, [r3, #16]
	mov	r2, r5
	ldr	r0, [r0, #12]
	bl	memcpy
	ldr	r1, [r4, #12]
	add	r3, r1, r5
	str	r3, [r4, #12]
	ldr	r0, [r4, #28]
	ldr	r2, [r0, #16]
	add	ip, r2, r5
	str	ip, [r0, #16]
	ldr	r1, [r4, #20]
	ldr	r3, [r4, #16]
	add	r2, r1, r5
	rsb	r0, r5, r3
	str	r2, [r4, #20]
	str	r0, [r4, #16]
	ldr	r1, [r4, #28]
	ldr	ip, [r1, #20]
	rsb	r5, r5, ip
	str	r5, [r1, #20]
	ldr	r3, [r4, #28]
	ldr	r0, [r3, #20]
	cmp	r0, #0
	ldreq	r0, [r3, #8]
	streq	r0, [r3, #16]
.L40:
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
	.size	flush_pending, .-flush_pending
	.section	.text.fill_window,"ax",%progbits
	.align	2
	.type	fill_window, %function
fill_window:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	r5, [r0, #44]
	mov	r4, r0
	sub	r9, r5, #260
	mov	r0, #260
	sub	sp, sp, #20
	sub	r9, r9, #2
	add	r1, r0, #1
	mov	r2, r5, asl #1
	mov	r8, r5, asl #16
	ldr	sl, [r4, #116]
	mov	r8, r8, lsr #16
	str	r1, [sp, #8]
	str	r2, [sp, #12]
	str	r9, [sp, #4]
	mov	r2, r5
	mov	r1, r9
	b	.L57
.L137:
	cmp	r6, #0
	bne	.L135
.L53:
	add	sl, sl, r6
	cmp	sl, #2
	str	sl, [r4, #116]
	bls	.L56
	ldr	r0, [r4, #108]
	ldr	r2, [r4, #56]
	ldrb	ip, [r0, r2]!	@ zero_extendqisi2
	str	ip, [r4, #72]
	ldr	r1, [r4, #88]
	ldrb	r3, [r0, #1]	@ zero_extendqisi2
	eor	r2, r3, ip, asl r1
	ldr	r0, [r4, #84]
	ldr	r1, [sp, #8]
	and	ip, r2, r0
	cmp	sl, r1
	str	ip, [r4, #72]
	bhi	.L58
.L56:
	ldr	r1, [r4, #0]
	ldr	ip, [r1, #4]
	cmp	ip, #0
	beq	.L58
	ldr	r2, [r4, #44]
	ldr	r1, [sp, #4]
.L57:
	ldr	r3, [r4, #108]
	ldr	r6, [r4, #60]
	add	r7, r1, r2
	rsb	sl, sl, r6
	cmp	r3, r7
	rsb	r6, r3, sl
	bcs	.L136
	ldr	r7, [r4, #0]
	ldr	r3, [r7, #4]
	cmp	r3, #0
	beq	.L58
.L140:
	cmp	r6, r3
	movcs	r6, r3
	ldr	fp, [r4, #56]
	ldr	r9, [r4, #108]
	ldr	sl, [r4, #116]
	movcs	r3, #0
	bcc	.L137
.L51:
	str	r3, [r7, #4]
	ldr	r2, [r7, #28]
	ldr	r3, [r2, #24]
	cmp	r3, #1
	beq	.L138
	cmp	r3, #2
	beq	.L139
.L55:
	add	lr, sl, r9
	ldr	r1, [r7, #0]
	add	r0, fp, lr
	mov	r2, r6
	bl	memcpy
	ldr	r3, [r7, #0]
	ldr	r0, [r7, #8]
	add	sl, r3, r6
	add	r1, r0, r6
	str	r1, [r7, #8]
	str	sl, [r7, #0]
	ldr	sl, [r4, #116]
	b	.L53
.L136:
	ldr	r0, [r4, #56]
	mov	r2, r5
	add	r1, r0, r5
	bl	memcpy
	ldr	r3, [r4, #112]
	ldr	r7, [r4, #108]
	ldr	r2, [r4, #92]
	rsb	ip, r5, r3
	rsb	r0, r5, r7
	rsb	r1, r5, r2
	ldr	r3, [r4, #68]
	ldr	r2, [r4, #76]
	str	ip, [r4, #112]
	str	r0, [r4, #108]
	str	r1, [r4, #92]
	add	r3, r3, r2, asl #1
	ldrh	r7, [r3, #-2]
	rsb	r0, r8, r7
	cmp	r5, r7
	mov	ip, r0, asl #16
	sub	r1, r2, #1
	mov	r0, ip, lsr #16
	movhi	r0, #0	@ movhi
	subs	r2, r2, #1
	and	r1, r1, #3
	strh	r0, [r3, #-2]!	@ movhi
	beq	.L119
	cmp	r1, #0
	beq	.L45
	cmp	r1, #1
	beq	.L99
	cmp	r1, #2
	beq	.L100
	ldrh	ip, [r3, #-2]
	rsb	r7, r8, ip
	cmp	r5, ip
	mov	r1, r7, asl #16
	mov	r0, r1, lsr #16
	movhi	r0, #0	@ movhi
	strh	r0, [r3, #-2]!	@ movhi
	sub	r2, r2, #1
.L100:
	ldrh	r7, [r3, #-2]
	rsb	r0, r8, r7
	cmp	r5, r7
	mov	r1, r0, asl #16
	mov	lr, r1, lsr #16
	movhi	lr, #0	@ movhi
	strh	lr, [r3, #-2]!	@ movhi
	sub	r2, r2, #1
.L99:
	ldrh	r1, [r3, #-2]
	rsb	r0, r8, r1
	cmp	r5, r1
	mov	lr, r0, asl #16
	mov	ip, lr, lsr #16
	movhi	ip, #0	@ movhi
	subs	r2, r2, #1
	strh	ip, [r3, #-2]!	@ movhi
	beq	.L119
.L45:
	ldrh	r0, [r3, #-2]
	rsb	lr, r8, r0
	cmp	r5, r0
	mov	r7, lr, asl #16
	mov	ip, r7, lsr #16
	movhi	ip, #0	@ movhi
	strh	ip, [r3, #-2]!	@ movhi
	ldrh	r1, [r3, #-2]
	rsb	r0, r8, r1
	cmp	r5, r1
	mov	lr, r0, asl #16
	mov	r0, lr, lsr #16
	mov	lr, r3
	movhi	r0, #0	@ movhi
	strh	r0, [lr, #-2]!	@ movhi
	ldrh	r1, [lr, #-2]
	rsb	ip, r8, r1
	cmp	r5, r1
	mov	r0, ip, asl #16
	mov	r7, r0, lsr #16
	movhi	r7, #0	@ movhi
	strh	r7, [lr, #-2]	@ movhi
	sub	lr, r3, #4
	ldrh	r1, [lr, #-2]
	rsb	ip, r8, r1
	cmp	r5, r1
	sub	r2, r2, #1
	mov	r0, ip, asl #16
	mov	r7, r0, lsr #16
	movhi	r7, #0	@ movhi
	subs	r2, r2, #3
	strh	r7, [lr, #-2]	@ movhi
	sub	r3, r3, #6
	bne	.L45
.L119:
	ldr	r3, [r4, #64]
	ldr	lr, [sp, #12]
	add	r3, lr, r3
	ldrh	r7, [r3, #-2]
	rsb	ip, r8, r7
	cmp	r5, r7
	mov	r2, ip, asl #16
	mov	r0, r2, lsr #16
	sub	r1, r5, #1
	movhi	r0, #0	@ movhi
	subs	r2, r5, #1
	and	r1, r1, #3
	strh	r0, [r3, #-2]!	@ movhi
	beq	.L106
	cmp	r1, #0
	beq	.L48
	cmp	r1, #1
	beq	.L96
	cmp	r1, #2
	beq	.L97
	ldrh	ip, [r3, #-2]
	rsb	r0, r8, ip
	cmp	r5, ip
	mov	lr, r0, asl #16
	mov	r7, lr, lsr #16
	movhi	r7, #0	@ movhi
	strh	r7, [r3, #-2]!	@ movhi
	sub	r2, r2, #1
.L97:
	ldrh	r0, [r3, #-2]
	rsb	lr, r8, r0
	cmp	r5, r0
	mov	r7, lr, asl #16
	mov	r1, r7, lsr #16
	movhi	r1, #0	@ movhi
	strh	r1, [r3, #-2]!	@ movhi
	sub	r2, r2, #1
.L96:
	ldrh	lr, [r3, #-2]
	rsb	r7, r8, lr
	cmp	r5, lr
	mov	r1, r7, asl #16
	mov	ip, r1, lsr #16
	movhi	ip, #0	@ movhi
	subs	r2, r2, #1
	strh	ip, [r3, #-2]!	@ movhi
	beq	.L106
.L48:
	ldrh	ip, [r3, #-2]
	rsb	r0, r8, ip
	cmp	r5, ip
	mov	lr, r0, asl #16
	mov	r7, lr, lsr #16
	movhi	r7, #0	@ movhi
	strh	r7, [r3, #-2]!	@ movhi
	ldrh	r1, [r3, #-2]
	rsb	ip, r8, r1
	cmp	r5, r1
	mov	r0, ip, asl #16
	mov	r1, r3
	mov	r0, r0, lsr #16
	movhi	r0, #0	@ movhi
	strh	r0, [r1, #-2]!	@ movhi
	ldrh	lr, [r1, #-2]
	rsb	ip, r8, lr
	cmp	r5, lr
	mov	r7, ip, asl #16
	mov	r0, r7, lsr #16
	movhi	r0, #0	@ movhi
	strh	r0, [r1, #-2]	@ movhi
	sub	r1, r3, #4
	ldrh	lr, [r1, #-2]
	rsb	ip, r8, lr
	cmp	r5, lr
	sub	r2, r2, #1
	mov	r7, ip, asl #16
	mov	r0, r7, lsr #16
	movhi	r0, #0	@ movhi
	subs	r2, r2, #3
	strh	r0, [r1, #-2]	@ movhi
	sub	r3, r3, #6
	bne	.L48
.L106:
	ldr	r7, [r4, #0]
	ldr	r3, [r7, #4]
	cmp	r3, #0
	add	r6, r6, r5
	bne	.L140
.L58:
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L138:
	ldr	r0, [r7, #48]
	ldr	r1, [r7, #0]
	mov	r2, r6
	bl	adler32
	str	r0, [r7, #48]
	b	.L55
.L139:
	ldr	r0, [r7, #48]
	ldr	r1, [r7, #0]
	mov	r2, r6
	bl	crc32
	str	r0, [r7, #48]
	b	.L55
.L135:
	rsb	r3, r6, r3
	b	.L51
	.size	fill_window, .-fill_window
	.section	.text.deflateSetDictionary,"ax",%progbits
	.align	2
	.global	deflateSetDictionary
	.hidden	deflateSetDictionary
	.type	deflateSetDictionary, %function
deflateSetDictionary:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, lr}
	subs	r5, r0, #0
	mov	r4, r1
	mov	r6, r2
	bne	.L156
.L142:
	mvn	r0, #1
.L150:
	ldmfd	sp!, {r3, r4, r5, r6, r7, lr}
	bx	lr
.L156:
	ldr	r7, [r5, #28]
	cmp	r7, #0
	cmpne	r1, #0
	beq	.L142
	ldr	r3, [r7, #24]
	cmp	r3, #2
	beq	.L142
	cmp	r3, #1
	beq	.L157
	cmp	r3, #0
	bne	.L144
.L145:
	cmp	r6, #2
	bls	.L146
	ldr	ip, [r7, #44]
	sub	r1, ip, #260
	sub	r5, r1, #2
	cmp	r6, r5
	rsbhi	r6, r5, r6
	movls	r5, r6
	addhi	r4, r4, r6
	mov	r2, r5
	mov	r1, r4
	ldr	r0, [r7, #56]
	bl	memcpy
	str	r5, [r7, #108]
	str	r5, [r7, #92]
	ldr	r1, [r7, #56]
	ldrb	r0, [r1, #0]	@ zero_extendqisi2
	str	r0, [r7, #72]
	ldr	ip, [r7, #88]
	ldrb	r2, [r1, #1]	@ zero_extendqisi2
	eor	r3, r2, r0, asl ip
	ldr	r0, [r7, #84]
	sub	r5, r5, #3
	and	r3, r3, r0
	tst	r5, #1
	str	r3, [r7, #72]
	mov	r2, #0
	beq	.L149
	ldrb	lr, [r1, #2]	@ zero_extendqisi2
	eor	r3, lr, r3, asl ip
	ldr	r1, [r7, #68]
	and	r0, r3, r0
	str	r0, [r7, #72]
	add	lr, r1, r0, asl #1
	ldr	r3, [r7, #64]
	ldrh	ip, [lr, #0]
	strh	ip, [r3, #0]	@ movhi
	ldr	r0, [r7, #72]
	ldr	lr, [r7, #68]
	add	r3, lr, r0, asl #1
	strh	r2, [r3, #0]	@ movhi
	mov	r2, #1
	cmp	r5, r2
	bcc	.L146
	ldr	r3, [r7, #72]
	ldr	r1, [r7, #56]
	add	r0, r7, #84
	ldmia	r0, {r0, ip}	@ phole ldm
	b	.L149
.L158:
	ldr	r1, [r7, #56]
	add	ip, r3, r1
	ldrb	r0, [ip, #2]	@ zero_extendqisi2
	ldr	lr, [r7, #72]
	ldr	r2, [r7, #88]
	eor	r1, r0, lr, asl r2
	ldr	ip, [r7, #84]
	and	r2, r1, ip
	str	r2, [r7, #72]
	ldr	lr, [r7, #52]
	ldr	ip, [r7, #68]
	mov	r2, r2, asl #1
	ldrh	r2, [r2, ip]
	and	r0, r3, lr
	ldr	r1, [r7, #64]
	mov	lr, r0, asl #1
	strh	r2, [lr, r1]	@ movhi
	ldr	r0, [r7, #72]
	ldr	r2, [r7, #68]
	mov	ip, r0, asl #1
	strh	r3, [ip, r2]	@ movhi
	add	r2, r3, #1
	ldr	r1, [r7, #56]
	ldr	r3, [r7, #72]
	add	r0, r7, #84
	ldmia	r0, {r0, ip}	@ phole ldm
.L149:
	add	lr, r2, r1
	ldrb	r1, [lr, #2]	@ zero_extendqisi2
	eor	r3, r1, r3, asl ip
	and	r3, r3, r0
	str	r3, [r7, #72]
	ldr	lr, [r7, #52]
	ldr	ip, [r7, #68]
	mov	r3, r3, asl #1
	ldr	r1, [r7, #64]
	ldrh	r3, [r3, ip]
	and	r0, r2, lr
	mov	lr, r0, asl #1
	strh	r3, [lr, r1]	@ movhi
	ldr	r0, [r7, #72]
	add	r3, r2, #1
	ldr	r1, [r7, #68]
	mov	lr, r0, asl #1
	cmp	r5, r3
	strh	r2, [lr, r1]	@ movhi
	bcs	.L158
.L146:
	mov	r0, #0
	b	.L150
.L157:
	ldr	r3, [r7, #4]
	cmp	r3, #42
	bne	.L142
.L144:
	ldr	r0, [r5, #48]
	mov	r1, r4
	mov	r2, r6
	bl	adler32
	str	r0, [r5, #48]
	b	.L145
	.size	deflateSetDictionary, .-deflateSetDictionary
	.section	.text.deflate,"ax",%progbits
	.align	2
	.global	deflate
	.hidden	deflate
	.type	deflate, %function
deflate:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, fp, lr}
	subs	r4, r0, #0
	mov	r5, r1
	bne	.L256
.L160:
	mvn	r0, #1
.L164:
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L256:
	ldr	r6, [r4, #28]
	rsbs	r3, r6, #1
	movcc	r3, #0
	cmp	r1, #4
	orrgt	r3, r3, #1
	cmp	r3, #0
	bne	.L160
	cmp	r1, #0
	blt	.L160
	ldr	r1, [r4, #12]
	cmp	r1, #0
	beq	.L161
	ldr	r2, [r4, #0]
	cmp	r2, #0
	beq	.L257
.L162:
	mov	r1, #664
	ldr	r3, [r6, #4]
	subs	r7, r5, #4
	movne	r7, #1
	add	r8, r1, #2
	cmp	r3, r8
	movne	r8, #0
	andeq	r8, r7, #1
	cmp	r8, #0
	bne	.L161
	ldr	r2, [r4, #16]
	cmp	r2, #0
	beq	.L254
	cmp	r3, #42
	ldr	sl, [r6, #40]
	str	r4, [r6, #0]
	str	r5, [r6, #40]
	beq	.L258
.L166:
	cmp	r3, #69
	beq	.L259
.L193:
	cmp	r3, #73
	beq	.L260
.L204:
	cmp	r3, #91
	beq	.L261
.L172:
	cmp	r3, #103
	beq	.L262
.L252:
	ldr	r3, [r6, #20]
.L225:
	cmp	r3, #0
	bne	.L263
	ldr	r2, [r4, #4]
	cmp	r5, sl
	cmple	r2, #0
	movne	r3, r2
	bne	.L231
	cmp	r7, #0
	bne	.L254
.L231:
	ldr	r1, [r6, #4]
	mov	r2, #664
	add	ip, r2, #2
	cmp	r1, ip
	beq	.L264
	cmp	r3, #0
	bne	.L234
.L233:
	ldr	r3, [r6, #116]
	cmp	r3, #0
	bne	.L234
	cmp	r5, #0
	beq	.L235
	add	r2, r3, #664
	add	ip, r2, #2
	cmp	r1, ip
	beq	.L236
.L234:
	ldr	r0, [r6, #132]
	ldr	r1, .L281
	add	lr, r0, r0, asl #1
	add	r3, r1, lr, asl #2
	mov	r0, r6
	mov	r1, r5
	ldr	ip, [r3, #8]
	mov	lr, pc
	bx	ip
	sub	r8, r0, #2
	cmp	r8, #1
	movls	r8, #664
	addls	r8, r8, #2
	strls	r8, [r6, #4]
	cmp	r0, #0
	cmpne	r0, #2
	movne	r8, #0
	moveq	r8, #1
	bne	.L238
	ldr	r0, [r4, #16]
	cmp	r0, #0
	bne	.L235
.L255:
	mvn	ip, #0
	str	ip, [r6, #40]
	b	.L164
.L262:
	ldr	r0, [r6, #28]
.L224:
	ldr	r2, [r0, #44]
	cmp	r2, #0
	moveq	r2, #113
	streq	r2, [r6, #4]
	beq	.L252
	ldr	r3, [r6, #20]
	ldr	r1, [r6, #12]
	add	r0, r3, #2
	cmp	r0, r1
	bhi	.L265
.L227:
	ldr	lr, [r4, #48]
	ldr	r1, [r6, #8]
	add	r2, r3, #1
	strb	lr, [r1, r3]
	str	r2, [r6, #20]
	ldr	r0, [r4, #48]
	ldr	r1, [r6, #8]
	mov	ip, r0, lsr #8
	add	lr, r2, #1
	mov	r0, #0
	strb	ip, [r1, r2]
	str	lr, [r6, #20]
	mov	r1, r0
	mov	r2, r0
	bl	crc32
	str	r0, [r4, #48]
	mov	r3, #113
	str	r3, [r6, #4]
	ldr	r3, [r6, #20]
	b	.L225
.L235:
	mov	r0, #0
	b	.L164
.L238:
	cmp	r0, #1
	bne	.L236
	cmp	r5, #1
	mov	r0, r6
	beq	.L266
	mov	r1, r8
	mov	r2, r8
	mov	r3, r8
	bl	_tr_stored_block
	cmp	r5, #3
	beq	.L267
.L240:
	mov	r0, r4
	bl	flush_pending
	ldr	r0, [r4, #16]
	cmp	r0, #0
	beq	.L255
.L236:
	cmp	r7, #0
	bne	.L235
	ldr	r3, [r6, #24]
	cmp	r3, #0
	movle	r0, #1
	ble	.L164
	cmp	r3, #2
	beq	.L268
	ldr	r1, [r4, #48]
	ldr	ip, [r6, #20]
	ldr	r0, [r6, #8]
	mov	r2, r1, lsr #24
	strb	r2, [r0, ip]
	add	r3, ip, #1
	ldr	r0, [r6, #8]
	add	ip, r3, #1
	mov	lr, r1, lsr #16
	strb	lr, [r0, r3]
	str	ip, [r6, #20]
	ldrh	lr, [r4, #48]
	ldr	r1, [r6, #8]
	mov	r2, lr, lsr #8
	strb	r2, [r1, ip]
	add	r3, ip, #1
	ldr	r0, [r6, #8]
	add	ip, r3, #1
	strb	lr, [r0, r3]
	str	ip, [r6, #20]
.L243:
	mov	r0, r4
	bl	flush_pending
	ldr	r1, [r6, #24]
	ldr	r3, [r6, #20]
	cmp	r1, #0
	rsbgt	r1, r1, #0
	strgt	r1, [r6, #24]
	rsbs	r0, r3, #1
	movcc	r0, #0
	b	.L164
.L263:
	mov	r0, r4
	bl	flush_pending
	ldr	r0, [r4, #16]
	cmp	r0, #0
	ldrne	r3, [r4, #4]
	bne	.L231
	b	.L255
.L258:
	ldr	r9, [r6, #24]
	cmp	r9, #2
	beq	.L269
	ldr	r2, [r6, #136]
	cmp	r2, #1
	ldr	r2, [r6, #48]
	ble	.L270
.L187:
	mov	r3, #0
.L189:
	mov	lr, r2, asl #12
	ldr	r1, [r6, #108]
	sub	r2, lr, #30720
	orr	r0, r3, r2
	mov	r3, #138412032
	cmp	r1, #0
	add	ip, r3, #135168
	orrne	r0, r0, #32
	add	lr, ip, #133
	umull	r1, lr, r0, lr
	rsb	r1, lr, r0
	add	r3, lr, r1, lsr #1
	mov	r2, r3, lsr #4
	rsb	ip, r2, r2, asl #5
	ldr	r3, [r6, #20]
	add	r2, ip, #31
	ldr	r1, [r6, #8]
	mov	lr, r2, lsr #8
	mov	r0, #113
	str	r0, [r6, #4]
	strb	lr, [r1, r3]
	ldr	ip, [r6, #8]
	add	lr, r3, #1
	strb	r2, [ip, lr]
	ldr	r0, [r6, #108]
	add	r3, lr, #1
	cmp	r0, #0
	str	r3, [r6, #20]
	beq	.L192
	ldr	ip, [r4, #48]
	ldr	r1, [r6, #8]
	mov	lr, ip, lsr #24
	strb	lr, [r1, r3]
	add	r2, r3, #1
	ldr	r0, [r6, #8]
	add	lr, r2, #1
	mov	r3, ip, lsr #16
	strb	r3, [r0, r2]
	str	lr, [r6, #20]
	ldrh	r2, [r4, #48]
	ldr	r1, [r6, #8]
	mov	ip, r2, lsr #8
	strb	ip, [r1, lr]
	add	r3, lr, #1
	ldr	r0, [r6, #8]
	add	ip, r3, #1
	strb	r2, [r0, r3]
	str	ip, [r6, #20]
.L192:
	mov	r0, #0
	mov	r1, r0
	mov	r2, r0
	bl	adler32
	str	r0, [r4, #48]
	ldr	r3, [r6, #4]
	b	.L166
.L270:
	ldr	r3, [r6, #132]
	cmp	r3, #1
	ble	.L187
	cmp	r3, #5
	movle	r3, #64
	ble	.L189
	cmp	r3, #6
	moveq	r3, #128
	movne	r3, #192
	b	.L189
.L259:
	ldr	r0, [r6, #28]
.L186:
	ldr	r3, [r0, #16]
	cmp	r3, #0
	beq	.L194
	ldr	r1, [r6, #32]
	ldrh	ip, [r0, #20]
	ldr	r3, [r6, #20]
	cmp	r1, ip
	movcs	r2, r3
	bcs	.L199
	mov	ip, r0
	mov	r2, r3
	b	.L200
.L196:
	ldr	r0, [ip, #16]
	ldr	ip, [r6, #8]
	ldrb	r0, [r0, r1]	@ zero_extendqisi2
	strb	r0, [ip, r2]
	ldr	r1, [r6, #32]
	add	r2, r2, #1
	add	r1, r1, #1
	str	r2, [r6, #20]
	str	r1, [r6, #32]
	ldr	r0, [r6, #28]
	ldrh	ip, [r0, #20]
	cmp	ip, r1
	bls	.L199
	mov	ip, r0
.L200:
	ldr	r8, [r6, #12]
	cmp	r8, r2
	bne	.L196
	ldr	lr, [r0, #44]
	cmp	lr, #0
	beq	.L197
	cmp	r2, r3
	bhi	.L271
.L197:
	mov	r0, r4
	bl	flush_pending
	ldr	r3, [r6, #20]
	ldr	r2, [r6, #12]
	cmp	r3, r2
	ldrne	r1, [r6, #32]
	ldrne	ip, [r6, #28]
	movne	r2, r3
	bne	.L196
.L272:
	ldr	r0, [r6, #28]
	mov	r2, r3
.L199:
	ldr	r1, [r0, #44]
	cmp	r1, #0
	beq	.L201
	cmp	r3, r2
	bcc	.L273
.L201:
	ldr	r3, [r0, #20]
	ldr	r2, [r6, #32]
	cmp	r2, r3
	ldrne	r3, [r6, #4]
	bne	.L193
	mov	ip, #0
	add	r3, ip, #73
	str	ip, [r6, #32]
	str	r3, [r6, #4]
.L203:
	ldr	r2, [r0, #28]
	cmp	r2, #0
	mov	ip, r0
	beq	.L205
	ldr	r3, [r6, #20]
	mov	r2, r3
	b	.L211
.L206:
	ldr	r0, [ip, #28]
	ldr	r2, [r6, #32]
	ldrb	r8, [r0, r2]	@ zero_extendqisi2
	ldr	r0, [r6, #8]
	add	ip, r2, #1
	cmp	r8, #0
	add	r2, r1, #1
	str	ip, [r6, #32]
	strb	r8, [r0, r1]
	str	r2, [r6, #20]
	beq	.L210
	ldr	ip, [r6, #28]
.L211:
	ldr	r1, [r6, #12]
	cmp	r1, r2
	ldrne	r1, [r6, #20]
	bne	.L206
	ldr	r8, [ip, #44]
	cmp	r8, #0
	beq	.L207
	cmp	r3, r2
	bcc	.L274
.L207:
	mov	r0, r4
	bl	flush_pending
	ldr	r3, [r6, #20]
	ldr	ip, [r6, #12]
	cmp	r3, ip
	mov	r1, r3
	beq	.L208
	ldr	ip, [r6, #28]
	b	.L206
.L264:
	cmp	r3, #0
	beq	.L233
.L254:
	ldr	r0, .L281+4
	ldr	r3, [r0, #28]
	mvn	r0, #4
	str	r3, [r4, #24]
	b	.L164
.L208:
	mov	r2, r3
	mov	r8, #1
.L210:
	ldr	ip, [r6, #28]
	ldr	r1, [ip, #44]
	cmp	r1, #0
	beq	.L212
	cmp	r3, r2
	bcc	.L275
.L212:
	cmp	r8, #0
	ldrne	r3, [r6, #4]
	bne	.L204
	mov	r3, #91
	str	r8, [r6, #32]
	str	r3, [r6, #4]
	ldr	r0, [r6, #28]
.L214:
	ldr	r1, [r0, #36]
	cmp	r1, #0
	mov	ip, r0
	beq	.L215
	ldr	r3, [r6, #20]
	mov	r2, r3
	b	.L221
.L216:
	ldr	r0, [ip, #36]
	ldr	r2, [r6, #32]
	ldrb	r8, [r0, r2]	@ zero_extendqisi2
	ldr	r0, [r6, #8]
	add	ip, r2, #1
	cmp	r8, #0
	add	r2, r1, #1
	str	ip, [r6, #32]
	strb	r8, [r0, r1]
	str	r2, [r6, #20]
	beq	.L220
	ldr	ip, [r6, #28]
.L221:
	ldr	r1, [r6, #12]
	cmp	r1, r2
	ldrne	r1, [r6, #20]
	bne	.L216
	ldr	r8, [ip, #44]
	cmp	r8, #0
	beq	.L217
	cmp	r3, r2
	bcc	.L276
.L217:
	mov	r0, r4
	bl	flush_pending
	ldr	r3, [r6, #20]
	ldr	ip, [r6, #12]
	cmp	r3, ip
	mov	r1, r3
	ldrne	ip, [r6, #28]
	bne	.L216
.L218:
	mov	r2, r3
	mov	r8, #1
.L220:
	ldr	ip, [r6, #28]
	ldr	r1, [ip, #44]
	cmp	r1, #0
	beq	.L222
	cmp	r3, r2
	bcc	.L277
.L222:
	cmp	r8, #0
	moveq	r3, #103
	ldrne	r3, [r6, #4]
	streq	r3, [r6, #4]
	ldreq	r0, [r6, #28]
	beq	.L224
	b	.L172
.L260:
	ldr	r0, [r6, #28]
	b	.L203
.L257:
	ldr	ip, [r4, #4]
	cmp	ip, #0
	beq	.L162
.L161:
	ldr	r3, .L281+4
	ldr	r0, [r3, #16]
	str	r0, [r4, #24]
	mvn	r0, #1
	b	.L164
.L271:
	ldr	r0, [r6, #8]
	rsb	r2, r3, r2
	add	r1, r3, r0
	ldr	r0, [r4, #48]
	bl	crc32
	str	r0, [r4, #48]
	b	.L197
.L261:
	ldr	r0, [r6, #28]
	b	.L214
.L276:
	ldr	r0, [r6, #8]
	rsb	r2, r3, r2
	add	r1, r3, r0
	ldr	r0, [r4, #48]
	bl	crc32
	str	r0, [r4, #48]
	b	.L217
.L274:
	ldr	r0, [r6, #8]
	rsb	r2, r3, r2
	add	r1, r3, r0
	ldr	r0, [r4, #48]
	bl	crc32
	str	r0, [r4, #48]
	b	.L207
.L268:
	ldr	r2, [r6, #20]
	ldr	lr, [r4, #48]
	ldr	r1, [r6, #8]
	add	r3, r2, #1
	strb	lr, [r1, r2]
	str	r3, [r6, #20]
	ldr	r0, [r4, #48]
	ldr	ip, [r6, #8]
	add	r2, r3, #1
	mov	lr, r0, lsr #8
	strb	lr, [ip, r3]
	str	r2, [r6, #20]
	ldr	r1, [r6, #8]
	ldrh	r0, [r4, #50]
	add	r3, r2, #1
	strb	r0, [r1, r2]
	str	r3, [r6, #20]
	ldr	ip, [r6, #8]
	ldrb	lr, [r4, #51]	@ zero_extendqisi2
	add	r2, r3, #1
	strb	lr, [ip, r3]
	str	r2, [r6, #20]
	ldr	r1, [r6, #8]
	ldr	r0, [r4, #8]
	add	r3, r2, #1
	strb	r0, [r1, r2]
	str	r3, [r6, #20]
	ldr	ip, [r4, #8]
	ldr	lr, [r6, #8]
	add	r2, r3, #1
	mov	r0, ip, lsr #8
	strb	r0, [lr, r3]
	str	r2, [r6, #20]
	ldr	r1, [r6, #8]
	ldrh	ip, [r4, #10]
	add	r3, r2, #1
	strb	ip, [r1, r2]
	str	r3, [r6, #20]
	ldr	lr, [r6, #8]
	ldrb	r0, [r4, #11]	@ zero_extendqisi2
	add	r2, r3, #1
	strb	r0, [lr, r3]
	str	r2, [r6, #20]
	b	.L243
.L215:
	mov	r3, #103
	str	r3, [r6, #4]
	b	.L224
.L205:
	mov	r2, #91
	str	r2, [r6, #4]
	b	.L214
.L275:
	ldr	lr, [r6, #8]
	rsb	r2, r3, r2
	add	r1, r3, lr
	ldr	r0, [r4, #48]
	bl	crc32
	str	r0, [r4, #48]
	b	.L212
.L269:
	mov	r1, r8
	mov	r2, r8
	mov	r0, r8
	bl	crc32
	str	r0, [r4, #48]
	ldr	ip, [r6, #20]
	ldr	r1, [r6, #8]
	mov	r0, #31
	strb	r0, [r1, ip]
	ldr	r3, [r6, #8]
	add	r2, ip, #1
	mvn	r1, #116
	strb	r1, [r3, r2]
	ldr	r0, [r6, #8]
	add	r2, r2, #1
	add	ip, r1, #125
	strb	ip, [r0, r2]
	ldr	r3, [r6, #28]
	add	r2, r2, #1
	cmp	r3, #0
	str	r2, [r6, #20]
	beq	.L278
	ldr	r0, [r3, #44]
	ldr	r1, [r3, #0]
	ldr	fp, [r3, #16]
	cmp	r0, #0
	ldr	lr, [r3, #28]
	movne	r0, #2
	moveq	r0, #0
	ldr	r3, [r3, #36]
	cmp	r1, #0
	addne	r0, r0, #1
	cmp	fp, #0
	movne	r1, #4
	moveq	r1, #0
	cmp	lr, #0
	add	ip, r1, r0
	movne	lr, #8
	moveq	lr, #0
	cmp	r3, #0
	movne	r0, #16
	moveq	r0, #0
	add	r1, lr, ip
	ldr	lr, [r6, #8]
	add	ip, r0, r1
	strb	ip, [lr, r2]
	add	r2, r2, #1
	str	r2, [r6, #20]
	ldr	r3, [r6, #28]
	ldr	r1, [r6, #8]
	ldr	lr, [r3, #4]
	strb	lr, [r1, r2]
	add	lr, r2, #1
	str	lr, [r6, #20]
	ldr	ip, [r6, #28]
	ldr	r0, [ip, #4]
	ldr	r3, [r6, #8]
	mov	r1, r0, lsr #8
	strb	r1, [r3, lr]
	add	r2, lr, #1
	str	r2, [r6, #20]
	ldr	ip, [r6, #28]
	ldr	r0, [r6, #8]
	ldrh	r1, [ip, #6]
	strb	r1, [r0, r2]
	add	r0, r2, #1
	str	r0, [r6, #20]
	ldr	lr, [r6, #28]
	ldr	r3, [r6, #8]
	ldrb	ip, [lr, #7]	@ zero_extendqisi2
	strb	ip, [r3, r0]
	ldr	r3, [r6, #132]
	add	r2, r0, #1
	cmp	r3, #9
	str	r2, [r6, #20]
	ldr	r1, [r6, #8]
	moveq	r8, r9
	beq	.L182
	ldr	r0, [r6, #136]
	cmp	r0, #1
	ble	.L279
.L183:
	mov	r8, #4
.L182:
	strb	r8, [r1, r2]
	add	r2, r2, #1
	str	r2, [r6, #20]
	ldr	r1, [r6, #28]
	ldr	r3, [r6, #8]
	ldr	ip, [r1, #12]
	strb	ip, [r3, r2]
	add	r2, r2, #1
	str	r2, [r6, #20]
	ldr	r0, [r6, #28]
	ldr	lr, [r0, #16]
	cmp	lr, #0
	beq	.L184
	ldr	lr, [r0, #20]
	ldr	r0, [r6, #8]
	strb	lr, [r0, r2]
	add	r0, r2, #1
	str	r0, [r6, #20]
	ldr	r3, [r6, #28]
	ldr	r1, [r3, #20]
	ldr	ip, [r6, #8]
	mov	lr, r1, lsr #8
	strb	lr, [ip, r0]
	add	r2, r0, #1
	str	r2, [r6, #20]
	ldr	r0, [r6, #28]
.L184:
	ldr	ip, [r0, #44]
	cmp	ip, #0
	beq	.L185
	ldr	r0, [r4, #48]
	ldr	r1, [r6, #8]
	bl	crc32
	str	r0, [r4, #48]
	ldr	r0, [r6, #28]
.L185:
	mov	r3, #0
	add	r1, r3, #69
	str	r3, [r6, #32]
	str	r1, [r6, #4]
	b	.L186
.L273:
	ldr	lr, [r6, #8]
	rsb	r2, r3, r2
	add	r1, r3, lr
	ldr	r0, [r4, #48]
	bl	crc32
	str	r0, [r4, #48]
	ldr	r0, [r6, #28]
	b	.L201
.L277:
	ldr	lr, [r6, #8]
	rsb	r2, r3, r2
	add	r1, r3, lr
	ldr	r0, [r4, #48]
	bl	crc32
	str	r0, [r4, #48]
	b	.L222
.L194:
	mov	r1, #73
	str	r1, [r6, #4]
	b	.L203
.L265:
	mov	r0, r4
	bl	flush_pending
	ldr	r3, [r6, #20]
	ldr	r2, [r6, #12]
	add	ip, r3, #2
	cmp	ip, r2
	bhi	.L225
	b	.L227
.L267:
	ldr	r1, [r6, #76]
	ldr	lr, [r6, #68]
	add	r3, lr, r1, asl #1
	strh	r8, [r3, #-2]	@ movhi
	ldr	r2, [r6, #76]
	sub	ip, r2, #-2147483647
	mov	r1, r8
	mov	r2, ip, asl #1
	ldr	r0, [r6, #68]
	bl	memset
	b	.L240
.L266:
	bl	_tr_align
	b	.L240
.L278:
	ldr	lr, [r6, #8]
	strb	r3, [lr, r2]
	ldr	r0, [r6, #8]
	add	ip, r2, #1
	strb	r3, [r0, ip]
	ldr	r1, [r6, #8]
	add	lr, ip, #1
	strb	r3, [r1, lr]
	ldr	r0, [r6, #8]
	add	r2, lr, #1
	strb	r3, [r0, r2]
	ldr	ip, [r6, #8]
	add	lr, r2, #1
	strb	r3, [ip, lr]
	ldr	r1, [r6, #132]
	add	r2, lr, #1
	cmp	r1, #9
	str	r2, [r6, #20]
	ldr	r0, [r6, #8]
	moveq	r3, r9
	beq	.L170
	ldr	ip, [r6, #136]
	cmp	ip, #1
	ble	.L280
.L171:
	mov	r3, #4
.L170:
	strb	r3, [r0, r2]
	add	r2, r2, #1
	ldr	r1, [r6, #8]
	add	lr, r2, #1
	mov	r0, #3
	mov	r3, #113
	strb	r0, [r1, r2]
	str	lr, [r6, #20]
	str	r3, [r6, #4]
	b	.L252
.L279:
	cmp	r3, #1
	bgt	.L182
	b	.L183
.L280:
	cmp	r1, #1
	bgt	.L170
	b	.L171
.L282:
	.align	2
.L281:
	.word	.LANCHOR0
	.word	z_errmsg
	.size	deflate, .-deflate
	.section	.text.deflateParams,"ax",%progbits
	.align	2
	.global	deflateParams
	.hidden	deflateParams
	.type	deflateParams, %function
deflateParams:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	cmp	r0, #0
	sub	sp, sp, #8
	mov	r4, r1
	bne	.L292
.L284:
	mvn	r0, #1
.L290:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L292:
	ldr	r5, [r0, #28]
	cmp	r5, #0
	beq	.L284
	cmn	r1, #1
	moveq	r3, #0
	moveq	r4, #6
	beq	.L286
	cmp	r4, #9
	movls	r3, #0
	movhi	r3, #1
.L286:
	orrs	r3, r3, r2, lsr #31
	bne	.L284
	cmp	r2, #4
	bgt	.L284
	ldr	r6, .L294
	ldr	r3, [r5, #132]
	mov	r1, #12
	mla	ip, r1, r3, r6
	mla	r1, r4, r1, r6
	ldr	ip, [ip, #8]
	ldr	r1, [r1, #8]
	cmp	r1, ip
	beq	.L287
	ldr	r1, [r0, #8]
	cmp	r1, #0
	bne	.L293
.L287:
	mov	r0, #0
.L288:
	cmp	r4, r3
	beq	.L289
	mov	r1, #12
	mul	r1, r4, r1
	add	r3, r1, r6
	ldrh	ip, [r1, r6]
	ldrh	r1, [r3, #6]
	ldrh	r6, [r3, #2]
	ldrh	r3, [r3, #4]
	str	r6, [r5, #128]
	str	ip, [r5, #140]
	str	r3, [r5, #144]
	str	r1, [r5, #124]
	str	r4, [r5, #132]
.L289:
	str	r2, [r5, #136]
	b	.L290
.L293:
	mov	r1, #1
	str	r2, [sp, #4]
	bl	deflate
	ldr	r3, [r5, #132]
	ldr	r2, [sp, #4]
	b	.L288
.L295:
	.align	2
.L294:
	.word	.LANCHOR0
	.size	deflateParams, .-deflateParams
	.section	.text.deflateBound,"ax",%progbits
	.align	2
	.global	deflateBound
	.hidden	deflateBound
	.type	deflateBound, %function
deflateBound:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	add	ip, r1, #11
	add	r2, r1, #7
	stmfd	sp!, {r3, lr}
	add	r3, ip, r2, lsr #3
	subs	ip, r0, #0
	add	r2, r1, #63
	add	r0, r3, r2, lsr #6
	beq	.L297
	ldr	r2, [ip, #28]
	cmp	r2, #0
	beq	.L297
	ldr	r3, [r2, #48]
	cmp	r3, #15
	beq	.L299
.L297:
	ldmfd	sp!, {r3, lr}
	bx	lr
.L299:
	ldr	ip, [r2, #80]
	cmp	ip, #15
	bne	.L297
	mov	r0, r1
	bl	compressBound
	b	.L297
	.size	deflateBound, .-deflateBound
	.section	.text.deflateReset,"ax",%progbits
	.align	2
	.global	deflateReset
	.hidden	deflateReset
	.type	deflateReset, %function
deflateReset:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	subs	r4, r0, #0
	bne	.L309
.L301:
	mvn	r0, #1
.L307:
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L309:
	ldr	r5, [r4, #28]
	cmp	r5, #0
	beq	.L301
	ldr	r3, [r4, #32]
	cmp	r3, #0
	beq	.L301
	ldr	r0, [r4, #36]
	cmp	r0, #0
	beq	.L301
	mov	r3, #0
	mov	r2, #2
	str	r2, [r4, #44]
	str	r3, [r4, #20]
	str	r3, [r4, #8]
	str	r3, [r4, #24]
	ldr	r2, [r5, #24]
	ldr	r1, [r5, #8]
	cmp	r2, r3
	rsblt	r2, r2, #0
	str	r3, [r5, #20]
	str	r1, [r5, #16]
	strlt	r2, [r5, #24]
	blt	.L303
	moveq	r3, #113
	bne	.L303
.L304:
	mov	r0, #0
	cmp	r2, #2
	str	r3, [r5, #4]
	mov	r1, r0
	mov	r2, r0
	beq	.L310
	bl	adler32
.L306:
	mov	r6, #0
	str	r0, [r4, #48]
	mov	r0, r5
	str	r6, [r5, #40]
	bl	_tr_init
	ldr	r2, [r5, #44]
	ldr	r1, [r5, #76]
	ldr	r3, [r5, #68]
	mov	ip, r2, asl #1
	add	r0, r3, r1, asl #1
	mov	r2, #0	@ movhi
	str	ip, [r5, #60]
	strh	r2, [r0, #-2]	@ movhi
	ldr	r1, [r5, #76]
	sub	r3, r1, #-2147483647
	mov	r2, r3, asl #1
	mov	r1, r6
	ldr	r0, [r5, #68]
	bl	memset
	ldr	ip, [r5, #132]
	ldr	r2, .L311
	add	r0, ip, ip, asl #1
	mov	r1, r0, asl #2
	add	r3, r1, r2
	ldrh	r0, [r1, r2]
	ldrh	ip, [r3, #2]
	ldrh	r2, [r3, #6]
	ldrh	r1, [r3, #4]
	mov	r3, #2
	str	r0, [r5, #140]
	str	ip, [r5, #128]
	str	r1, [r5, #144]
	str	r2, [r5, #124]
	str	r3, [r5, #96]
	str	r6, [r5, #72]
	str	r6, [r5, #108]
	str	r6, [r5, #92]
	str	r6, [r5, #116]
	str	r3, [r5, #120]
	str	r6, [r5, #104]
	mov	r0, r6
	b	.L307
.L303:
	mov	r3, #42
	b	.L304
.L310:
	bl	crc32
	b	.L306
.L312:
	.align	2
.L311:
	.word	.LANCHOR0
	.size	deflateReset, .-deflateReset
	.global	__aeabi_uidiv
	.section	.text.deflateInit2_,"ax",%progbits
	.align	2
	.global	deflateInit2_
	.hidden	deflateInit2_
	.type	deflateInit2_, %function
deflateInit2_:
	@ Function supports interworking.
	@ args = 16, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	sub	sp, sp, #12
	ldr	ip, [sp, #56]
	cmp	ip, #0
	mov	r4, r0
	add	r5, sp, #48
	ldmia	r5, {r5, r6}	@ phole ldm
	ldr	r0, [sp, #60]
	bne	.L329
.L314:
	mvn	r0, #5
.L324:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L329:
	ldrb	ip, [ip, #0]	@ zero_extendqisi2
	cmp	ip, #49
	cmpeq	r0, #56
	moveq	r0, #0
	movne	r0, #1
	bne	.L314
	cmp	r4, #0
	bne	.L330
.L315:
	mvn	r0, #1
	b	.L324
.L330:
	ldr	ip, [r4, #32]
	cmp	ip, #0
	str	r0, [r4, #24]
	ldreq	r0, .L331
	streq	r0, [r4, #32]
	ldr	r0, [r4, #36]
	streq	ip, [r4, #40]
	cmp	r0, #0
	ldreq	r0, .L331+4
	streq	r0, [r4, #36]
	cmn	r1, #1
	movne	r9, r1
	moveq	r9, #6
	cmp	r3, #0
	rsblt	r3, r3, #0
	movlt	r8, #0
	blt	.L320
	cmp	r3, #15
	movle	r8, #1
	subgt	r3, r3, #16
	movgt	r8, #2
.L320:
	sub	r1, r5, #1
	cmp	r1, #8
	cmpls	r2, #8
	bne	.L315
	cmp	r3, #7
	movgt	r2, #0
	movle	r2, #1
	cmp	r3, #15
	orrgt	r2, r2, #1
	cmp	r2, #0
	bne	.L315
	cmp	r9, #9
	movle	r0, #0
	movgt	r0, #1
	orrs	r2, r0, r9, lsr #31
	bne	.L315
	mov	ip, r6, lsr #31
	cmp	r6, #4
	orrgt	ip, ip, #1
	cmp	ip, #0
	bne	.L315
	cmp	r3, #8
	moveq	r3, #9
	ldr	r0, [r4, #40]
	mov	r1, #1
	stmia	sp, {r3, ip}	@ phole stm
	mov	r2, #5824
	ldr	ip, [r4, #32]
	mov	lr, pc
	bx	ip
	subs	r7, r0, #0
	ldmia	sp, {r3, ip}	@ phole ldm
	beq	.L328
	add	r1, r5, #7
	mov	sl, #1
	mov	fp, sl, asl r3
	mov	r2, sl, asl r1
	sub	lr, fp, #1
	sub	r0, r2, #1
	str	r7, [r4, #28]
	str	ip, [r7, #28]
	str	lr, [r7, #52]
	str	r3, [r7, #48]
	str	r0, [r7, #84]
	str	r1, [r7, #80]
	str	r2, [r7, #76]
	mov	r1, #3
	add	r0, r5, #9
	str	r8, [r7, #24]
	str	r4, [r7, #0]
	str	fp, [r7, #44]
	bl	__aeabi_uidiv
	str	r0, [r7, #88]
	mov	r1, fp
	mov	r2, #2
	ldr	r0, [r4, #40]
	ldr	ip, [r4, #32]
	mov	lr, pc
	bx	ip
	str	r0, [r7, #56]
	ldr	r1, [r7, #44]
	mov	r2, #2
	ldr	r0, [r4, #40]
	ldr	ip, [r4, #32]
	mov	lr, pc
	bx	ip
	str	r0, [r7, #64]
	ldr	r1, [r7, #76]
	mov	r2, #2
	ldr	r0, [r4, #40]
	ldr	ip, [r4, #32]
	mov	lr, pc
	bx	ip
	add	r3, r5, #6
	mov	r1, sl, asl r3
	mov	r5, #5760
	add	r5, r5, #28
	str	r1, [r7, r5]
	str	r0, [r7, #68]
	mov	r2, #4
	ldr	r0, [r4, #40]
	ldr	ip, [r4, #32]
	mov	lr, pc
	bx	ip
	ldr	r3, [r7, r5]
	ldr	r1, [r7, #56]
	mov	ip, r3, asl #2
	cmp	r1, #0
	str	ip, [r7, #12]
	str	r0, [r7, #8]
	beq	.L325
	ldr	ip, [r7, #64]
	cmp	ip, #0
	beq	.L325
	ldr	r2, [r7, #68]
	cmp	r2, #0
	beq	.L325
	cmp	r0, #0
	beq	.L325
	mov	r1, #3
	mla	r1, r3, r1, r0
	bic	ip, r3, #1
	mov	r2, #5760
	add	r0, r0, ip
	mov	r3, r2
	add	r2, r2, #36
	add	r3, r3, #24
	str	r0, [r7, r2]
	mov	ip, #8
	mov	r0, r4
	str	r1, [r7, r3]
	strb	ip, [r7, #36]
	str	r9, [r7, #132]
	str	r6, [r7, #136]
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	b	deflateReset
.L325:
	ldr	r1, .L331+8
	mov	lr, #664
	ldr	r3, [r1, #24]
	add	r0, lr, #2
	str	r0, [r7, #4]
	str	r3, [r4, #24]
	mov	r0, r4
	bl	deflateEnd
.L328:
	mvn	r0, #3
	b	.L324
.L332:
	.align	2
.L331:
	.word	zcalloc
	.word	zcfree
	.word	z_errmsg
	.size	deflateInit2_, .-deflateInit2_
	.section	.text.deflateInit_,"ax",%progbits
	.align	2
	.global	deflateInit_
	.hidden	deflateInit_
	.type	deflateInit_, %function
deflateInit_:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r2, #0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	mov	r4, r0
	mov	r5, r1
	bne	.L346
.L334:
	mvn	r0, #5
.L341:
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L346:
	ldrb	r2, [r2, #0]	@ zero_extendqisi2
	cmp	r2, #49
	cmpeq	r3, #56
	moveq	r3, #0
	movne	r3, #1
	bne	.L334
	cmp	r0, #0
	bne	.L347
.L335:
	mvn	r0, #1
	b	.L341
.L347:
	ldr	r0, [r0, #32]
	cmp	r0, #0
	str	r3, [r4, #24]
	ldreq	r3, .L348
	streq	r3, [r4, #32]
	ldr	r3, [r4, #36]
	streq	r0, [r4, #40]
	cmp	r3, #0
	ldreq	r3, .L348+4
	streq	r3, [r4, #36]
	cmn	r1, #1
	addeq	r5, r1, #7
	beq	.L339
	cmp	r5, #9
	movle	r1, #0
	movgt	r1, #1
	orrs	r3, r1, r5, lsr #31
	bne	.L335
.L339:
	ldr	r0, [r4, #40]
	mov	r1, #1
	mov	r2, #5824
	ldr	ip, [r4, #32]
	mov	lr, pc
	bx	ip
	subs	r6, r0, #0
	beq	.L345
	mov	r2, #1
	mov	r8, #32512
	add	lr, r8, #255
	mov	ip, #15
	mov	r1, #32768
	add	r0, r2, #4
	mov	r8, #0
	str	r6, [r4, #28]
	str	ip, [r6, #80]
	str	ip, [r6, #48]
	str	lr, [r6, #84]
	str	lr, [r6, #52]
	str	r2, [r6, #24]
	str	r1, [r6, #44]
	str	r1, [r6, #76]
	str	r0, [r6, #88]
	str	r4, [r6, #0]
	str	r8, [r6, #28]
	mov	r2, #2
	ldr	r0, [r4, #40]
	ldr	ip, [r4, #32]
	mov	lr, pc
	bx	ip
	str	r0, [r6, #56]
	ldr	r1, [r6, #44]
	mov	r2, #2
	ldr	r0, [r4, #40]
	ldr	ip, [r4, #32]
	mov	lr, pc
	bx	ip
	str	r0, [r6, #64]
	ldr	r1, [r6, #76]
	mov	r2, #2
	ldr	r0, [r4, #40]
	ldr	ip, [r4, #32]
	mov	lr, pc
	bx	ip
	mov	r7, #5760
	add	r7, r7, #28
	mov	r1, #16384
	str	r0, [r6, #68]
	str	r1, [r6, r7]
	mov	r2, #4
	ldr	r0, [r4, #40]
	ldr	ip, [r4, #32]
	mov	lr, pc
	bx	ip
	ldr	r3, [r6, r7]
	ldr	r2, [r6, #56]
	mov	ip, r3, asl #2
	cmp	r2, r8
	str	ip, [r6, #12]
	str	r0, [r6, #8]
	beq	.L342
	ldr	r1, [r6, #64]
	cmp	r1, r8
	beq	.L342
	ldr	lr, [r6, #68]
	cmp	lr, r8
	beq	.L342
	cmp	r0, r8
	beq	.L342
	mov	lr, #3
	mla	lr, r3, lr, r0
	bic	ip, r3, #1
	mov	r2, #5760
	mov	r1, r2
	add	r0, r0, ip
	add	r2, r2, #36
	add	r3, r1, #24
	str	r0, [r6, r2]
	mov	r1, #8
	mov	r0, r4
	str	lr, [r6, r3]
	strb	r1, [r6, #36]
	str	r5, [r6, #132]
	str	r8, [r6, #136]
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	deflateReset
.L342:
	ldr	ip, .L348+8
	mov	r2, #664
	ldr	r3, [ip, #24]
	add	r0, r2, #2
	str	r0, [r6, #4]
	str	r3, [r4, #24]
	mov	r0, r4
	bl	deflateEnd
.L345:
	mvn	r0, #3
	b	.L341
.L349:
	.align	2
.L348:
	.word	zcalloc
	.word	zcfree
	.word	z_errmsg
	.size	deflateInit_, .-deflateInit_
	.section	.text.deflate_stored,"ax",%progbits
	.align	2
	.type	deflate_stored, %function
deflate_stored:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, lr}
	ldr	r6, [r0, #12]
	mov	r5, #65280
	sub	r6, r6, #5
	add	r2, r5, #254
	mov	r4, r0
	mov	r0, #65536
	sub	r3, r0, #1
	cmp	r6, r2
	movhi	r6, r3
	mov	r7, r1
	mov	r5, #0
.L373:
	ldr	r3, [r4, #116]
	cmp	r3, #1
	bls	.L376
.L352:
	ldr	r1, [r4, #108]
	ldr	r0, [r4, #92]
	add	r3, r3, r1
	cmp	r3, #0
	str	r3, [r4, #108]
	str	r5, [r4, #116]
	mov	r1, r0
	add	r2, r6, r0
	beq	.L355
	cmp	r3, r2
	bcc	.L356
.L355:
	cmp	r0, #0
	ldrge	r1, [r4, #56]
	rsb	sl, r2, r3
	addge	r1, r0, r1
	movlt	r1, #0
	str	sl, [r4, #116]
	str	r2, [r4, #108]
	mov	r3, #0
	rsb	r2, r0, r2
	mov	r0, r4
	bl	_tr_flush_block
	ldr	r8, [r4, #108]
	str	r8, [r4, #92]
	ldr	r8, [r4, #0]
	ldr	r2, [r8, #28]
	ldr	r3, [r8, #16]
	ldr	sl, [r2, #20]
	cmp	r3, sl
	movcc	sl, r3
	cmp	sl, #0
	beq	.L359
	ldr	r1, [r2, #16]
	ldr	r0, [r8, #12]
	mov	r2, sl
	bl	memcpy
	ldr	r0, [r8, #12]
	add	r3, r0, sl
	str	r3, [r8, #12]
	ldr	ip, [r8, #28]
	ldr	r2, [ip, #16]
	add	r1, r2, sl
	str	r1, [ip, #16]
	ldr	r0, [r8, #20]
	ldr	r3, [r8, #16]
	add	r2, r0, sl
	rsb	ip, sl, r3
	str	ip, [r8, #16]
	str	r2, [r8, #20]
	ldr	r0, [r8, #28]
	ldr	r1, [r0, #20]
	rsb	sl, sl, r1
	str	sl, [r0, #20]
	ldr	r3, [r8, #28]
	ldr	r2, [r3, #20]
	cmp	r2, #0
	ldreq	r2, [r3, #8]
	ldr	ip, [r4, #0]
	streq	r2, [r3, #16]
	ldr	r3, [ip, #16]
.L359:
	cmp	r3, #0
	beq	.L353
	ldr	r3, [r4, #108]
	ldr	r1, [r4, #92]
.L356:
	ldr	r2, [r4, #44]
	sub	ip, r2, #260
	sub	lr, ip, #2
	rsb	r2, r1, r3
	cmp	r2, lr
	bcc	.L373
	cmp	r1, #0
	ldrge	r3, [r4, #56]
	movlt	r1, #0
	addge	r1, r1, r3
	mov	r0, r4
	mov	r3, #0
	bl	_tr_flush_block
	ldr	r8, [r4, #108]
	str	r8, [r4, #92]
	ldr	r8, [r4, #0]
	ldr	r2, [r8, #28]
	ldr	r3, [r8, #16]
	ldr	sl, [r2, #20]
	cmp	r3, sl
	movcc	sl, r3
	cmp	sl, #0
	beq	.L363
	ldr	r1, [r2, #16]
	ldr	r0, [r8, #12]
	mov	r2, sl
	bl	memcpy
	ldr	ip, [r8, #12]
	add	r3, ip, sl
	str	r3, [r8, #12]
	ldr	r1, [r8, #28]
	ldr	r0, [r1, #16]
	add	r2, r0, sl
	str	r2, [r1, #16]
	add	r3, r8, #16
	ldmia	r3, {r3, ip}	@ phole ldm
	add	r0, ip, sl
	rsb	r1, sl, r3
	str	r1, [r8, #16]
	str	r0, [r8, #20]
	ldr	ip, [r8, #28]
	ldr	r2, [ip, #20]
	rsb	sl, sl, r2
	str	sl, [ip, #20]
	ldr	r3, [r8, #28]
	ldr	r0, [r3, #20]
	cmp	r0, #0
	ldreq	r0, [r3, #8]
	ldr	r1, [r4, #0]
	streq	r0, [r3, #16]
	ldr	r3, [r1, #16]
.L363:
	cmp	r3, #0
	bne	.L373
.L353:
	mov	r0, #0
.L368:
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, lr}
	bx	lr
.L376:
	mov	r0, r4
	bl	fill_window
	ldr	r3, [r4, #116]
	rsbs	r2, r3, #1
	movcc	r2, #0
	cmp	r7, #0
	movne	r1, #0
	andeq	r1, r2, #1
	cmp	r1, #0
	bne	.L353
	cmp	r2, #0
	beq	.L352
	ldr	r3, [r4, #92]
	cmp	r3, #0
	ldrge	r1, [r4, #56]
	ldr	lr, [r4, #108]
	addge	r1, r3, r1
	rsb	r2, r3, lr
	mov	r0, r4
	cmp	r7, #4
	movne	r3, #0
	moveq	r3, #1
	bl	_tr_flush_block
	ldr	r0, [r4, #108]
	str	r0, [r4, #92]
	ldr	r0, [r4, #0]
	bl	flush_pending
	ldr	r1, [r4, #0]
	ldr	r2, [r1, #16]
	cmp	r2, #0
	bne	.L367
	cmp	r7, #4
	moveq	r0, #2
	bne	.L353
	b	.L368
.L367:
	cmp	r7, #4
	moveq	r0, #3
	movne	r0, #1
	b	.L368
	.size	deflate_stored, .-deflate_stored
	.section	.text.deflate_slow,"ax",%progbits
	.align	2
	.type	deflate_slow, %function
deflate_slow:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 80
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	mov	r3, #5760
	sub	sp, sp, #84
	mov	r4, r0
	mov	r0, #5760
	str	r1, [sp, #20]
	add	r2, r3, #24
	add	r1, r0, #28
	str	r2, [sp, #8]
	str	r1, [sp, #12]
	mov	r6, #5760
	mov	r8, r6
	mov	r7, #260
	ldr	r2, [r4, #116]
	add	r7, r7, #1
	add	r6, r6, #32
	add	r8, r8, #36
	mov	r5, #0
.L439:
	cmp	r2, r7
	bls	.L477
.L379:
	ldr	ip, [r4, #108]
	ldr	r5, [r4, #56]
	add	r9, ip, r5
	ldrb	r1, [r9, #2]	@ zero_extendqisi2
	ldr	sl, [r4, #72]
	ldr	r3, [r4, #88]
	eor	r2, r1, sl, asl r3
	ldr	r0, [r4, #84]
	and	r5, r2, r0
	str	r5, [r4, #72]
	ldr	r1, [r4, #68]
	ldr	sl, [r4, #52]
	mov	r9, r5, asl #1
	ldrh	r5, [r9, r1]
	and	r3, sl, ip
	ldr	r0, [r4, #64]
	mov	r2, r3, asl #1
	strh	r5, [r2, r0]	@ movhi
	add	r1, r4, #68
	ldmia	r1, {r1, r9}	@ phole ldm
	mov	sl, r9, asl #1
	ldrh	r9, [r4, #108]
	strh	r9, [sl, r1]	@ movhi
	ldr	r0, [r4, #96]
	ldr	ip, [r4, #112]
	mov	sl, #2
	cmp	r5, #0
	str	ip, [r4, #100]
	str	r0, [r4, #120]
	str	sl, [r4, #96]
	beq	.L471
.L440:
	ldr	r2, [r4, #128]
	cmp	r0, r2
	bcs	.L471
	ldr	r2, [r4, #44]
	ldr	r3, [r4, #108]
	sub	r1, r2, #260
	sub	r1, r1, #2
	rsb	ip, r5, r3
	cmp	ip, r1
	bhi	.L383
	ldr	fp, [r4, #136]
	sub	sl, fp, #2
	cmp	sl, #1
	str	fp, [sp, #24]
	bls	.L384
	ldr	sl, [r4, #56]
	str	sl, [sp, #16]
	cmp	r3, r1
	add	fp, sl, r3
	ldr	r9, [r4, #124]
	str	fp, [sp, #28]
	addhi	fp, r3, #260
	addhi	fp, fp, #2
	rsbhi	fp, r2, fp
	movls	r2, #0
	ldr	sl, [r4, #144]
	strls	r2, [sp, #32]
	strhi	fp, [sp, #32]
	ldr	ip, [sp, #16]
	ldr	r1, [r4, #140]
	ldr	fp, [sp, #28]
	add	r2, r3, #256
	add	r2, r2, #2
	cmp	r0, r1
	add	r1, fp, r0
	add	fp, ip, r2
	str	fp, [sp, #40]
	ldr	r2, [r4, #64]
	ldrb	fp, [r1, #-1]	@ zero_extendqisi2
	str	r2, [sp, #36]
	ldr	ip, [r4, #52]
	ldr	r1, [sp, #28]
	str	ip, [sp, #56]
	ldrb	ip, [r1, r0]	@ zero_extendqisi2
	ldr	r2, [r4, #116]
	ldr	r1, [sp, #40]
	str	r2, [sp, #52]
	rsb	r2, r1, #256
	ldr	r1, [sp, #40]
	str	r2, [sp, #48]
	sub	r2, r1, #256
	ldr	r1, [sp, #52]
	str	r2, [sp, #44]
	movcs	r9, r9, lsr #2
	cmp	sl, r1
	movcc	r1, sl
	ldr	r2, [sp, #48]
	str	r1, [sp, #72]
	ldr	r1, [sp, #44]
	add	sl, r2, #2
	sub	r2, r1, #2
	str	sl, [sp, #48]
	str	r2, [sp, #44]
	mov	r1, r9
	str	r5, [sp, #60]
	ldr	r9, [sp, #32]
	mov	r2, r5
	str	r4, [sp, #32]
	str	r3, [sp, #64]
	ldr	sl, [sp, #16]
	str	r8, [sp, #68]
	str	r7, [sp, #16]
	ldr	r8, [sp, #56]
	ldr	r7, [sp, #36]
	str	r6, [sp, #36]
	b	.L397
.L390:
	and	r4, r2, r8
	mov	r2, r4, asl #1
	ldrh	r2, [r2, r7]
	cmp	r2, r9
	bls	.L456
.L479:
	subs	r1, r1, #1
	beq	.L456
	mov	r0, ip
	mov	fp, r5
	mov	ip, r3
.L397:
	add	r4, sl, r2
	ldrb	r3, [r4, r0]	@ zero_extendqisi2
	cmp	r3, ip
	movne	r3, ip
	mov	r6, r0
	movne	r5, fp
	movne	ip, r0
	bne	.L390
	add	ip, r0, r4
	ldrb	r5, [ip, #-1]	@ zero_extendqisi2
	cmp	r5, fp
	movne	r5, fp
	movne	ip, r0
	bne	.L390
	ldrb	ip, [r4, #0]	@ zero_extendqisi2
	ldr	fp, [sp, #28]
	str	ip, [sp, #56]
	ldrb	ip, [fp, #0]	@ zero_extendqisi2
	ldr	fp, [sp, #56]
	cmp	fp, ip
	beq	.L478
.L392:
	and	r4, r2, r8
	mov	r2, r4, asl #1
	ldrh	r2, [r2, r7]
	cmp	r2, r9
	mov	ip, r0
	bhi	.L479
.L456:
	mov	r2, r6
	ldr	r5, [sp, #60]
	ldr	r4, [sp, #32]
	ldr	r3, [sp, #64]
	ldr	r7, [sp, #16]
	ldr	r6, [sp, #36]
	ldr	r8, [sp, #68]
.L396:
	ldr	r9, [sp, #52]
	cmp	r2, r9
	movcs	r2, r9
	cmp	r2, #5
	str	r2, [r4, #96]
	bls	.L405
.L472:
	ldr	r0, [r4, #120]
.L383:
	cmp	r0, #2
	bls	.L408
.L481:
	ldr	r2, [r4, #96]
	cmp	r2, r0
	bhi	.L408
	ldr	fp, [r4, #100]
	sub	sl, r3, #1
	ldr	r9, [r4, r6]
	rsb	ip, fp, sl
	mov	r1, ip, asl #16
	ldr	fp, [r4, r8]
	mov	r2, r9, asl #1
	mov	r1, r1, lsr #16
	ldr	r9, [r4, #116]
	ldr	sl, .L490
	strh	r1, [r2, fp]	@ movhi
	sub	r0, r0, #3
	ldr	ip, [sp, #8]
	and	r0, r0, #255
	ldrb	r2, [sl, r0]	@ zero_extendqisi2
	ldr	sl, [r4, r6]
	ldr	fp, [r4, ip]
	add	r2, r2, #292
	add	ip, sl, #1
	strb	r0, [fp, sl]
	add	r2, r2, #1
	str	ip, [r4, r6]
	add	r2, r4, r2, asl #2
	ldrh	r0, [r2, #4]
	sub	r1, r1, #1
	mov	fp, r1, asl #16
	cmp	fp, #16711680
	add	r3, r3, r9
	add	r9, r0, #1
	strh	r9, [r2, #4]	@ movhi
	ldrhi	r2, .L490+4
	ldrls	r9, .L490+4
	addhi	fp, r2, fp, lsr #23
	ldrlsb	r2, [r9, fp, lsr #16]	@ zero_extendqisi2
	ldrhib	r2, [fp, #256]	@ zero_extendqisi2
	add	r1, r2, #608
	add	r0, r1, #2
	mov	r2, r0, asl #2
	ldrh	fp, [r2, r4]
	add	r9, fp, #1
	strh	r9, [r2, r4]	@ movhi
	add	r1, r4, #116
	ldmia	r1, {r1, fp}	@ phole ldm
	add	r0, r1, #1
	rsb	r9, fp, r0
	sub	r1, fp, #2
	str	r9, [r4, #116]
	str	r1, [r4, #120]
	ldr	r9, [sp, #12]
	ldr	r2, [r4, #108]
	ldr	r0, [r4, r9]
	sub	r3, r3, #3
.L412:
	add	r2, r2, #1
	cmp	r3, r2
	str	r2, [r4, #108]
	bcc	.L411
	ldr	fp, [r4, #56]
	add	r1, r2, fp
	ldrb	r5, [r1, #2]	@ zero_extendqisi2
	ldr	sl, [r4, #72]
	ldr	r9, [r4, #88]
	eor	fp, r5, sl, asl r9
	ldr	r9, [r4, #84]
	and	r1, fp, r9
	str	r1, [r4, #72]
	ldr	sl, [r4, #52]
	ldr	r5, [r4, #68]
	mov	fp, r1, asl #1
	ldrh	r5, [fp, r5]
	and	r2, r2, sl
	ldr	fp, [r4, #64]
	mov	r1, r2, asl #1
	strh	r5, [r1, fp]	@ movhi
	add	r2, r4, #68
	ldmia	r2, {r2, sl}	@ phole ldm
	mov	r1, sl, asl #1
	ldrh	sl, [r4, #108]
	strh	sl, [r1, r2]	@ movhi
	ldr	r1, [r4, #120]
	ldr	r2, [r4, #108]
.L411:
	sub	r1, r1, #1
	cmp	r1, #0
	str	r1, [r4, #120]
	bne	.L412
	sub	r3, r0, #1
	add	r2, r2, #1
	mov	fp, #2
	cmp	ip, r3
	str	r1, [r4, #104]
	str	fp, [r4, #96]
	str	r2, [r4, #108]
	beq	.L480
	ldr	r2, [r4, #116]
.L482:
	cmp	r2, r7
	bhi	.L379
.L477:
	mov	r0, r4
	bl	fill_window
	ldr	r3, [r4, #116]
	ldr	ip, [sp, #20]
	cmp	r3, r7
	cmpls	ip, #0
	beq	.L380
	cmp	r3, #0
	beq	.L381
	cmp	r3, #2
	bhi	.L379
	ldr	r0, [r4, #96]
	ldr	ip, [r4, #112]
	mov	sl, #2
	cmp	r5, #0
	str	ip, [r4, #100]
	str	r0, [r4, #120]
	str	sl, [r4, #96]
	bne	.L440
.L471:
	cmp	r0, #2
	ldr	r3, [r4, #108]
	bhi	.L481
.L408:
	ldr	r0, [r4, #104]
	cmp	r0, #0
	beq	.L418
	ldr	sl, [r4, r6]
	ldr	r0, [r4, #56]
	ldr	r1, [r4, r8]
	add	ip, r3, r0
	mov	r9, sl, asl #1
	mov	r3, #0	@ movhi
	ldrb	sl, [ip, #-1]	@ zero_extendqisi2
	strh	r3, [r9, r1]	@ movhi
	ldr	ip, [sp, #8]
	ldr	r2, [r4, r6]
	ldr	r0, [r4, ip]
	add	ip, r2, #1
	strb	sl, [r0, r2]
	add	r3, sl, #36
	str	ip, [r4, r6]
	add	r9, r4, r3, asl #2
	ldrh	r1, [r9, #4]
	add	r1, r1, #1
	strh	r1, [r9, #4]	@ movhi
	ldr	r9, [sp, #12]
	ldr	r2, [r4, r9]
	sub	sl, r2, #1
	cmp	ip, sl
	beq	.L444
.L474:
	ldr	r3, [r4, #108]
	ldr	sl, [r4, #0]
.L419:
	ldr	r2, [r4, #116]
	add	r1, r3, #1
	sub	r2, r2, #1
	str	r1, [r4, #108]
	str	r2, [r4, #116]
	ldr	r0, [sl, #16]
	cmp	r0, #0
	bne	.L439
.L380:
	mov	r0, #0
.L428:
	add	sp, sp, #84
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L480:
	ldr	lr, [r4, #92]
	cmp	lr, #0
	ldrge	r1, [r4, #56]
	rsb	r2, lr, r2
	addge	r1, lr, r1
	mov	r0, r4
	mov	r3, #0
	bl	_tr_flush_block
	ldr	r0, [r4, #108]
	ldr	sl, [r4, #0]
	str	r0, [r4, #92]
	ldr	r2, [sl, #28]
	ldr	r3, [sl, #16]
	ldr	r9, [r2, #20]
	cmp	r3, r9
	movcc	r9, r3
	cmp	r9, #0
	beq	.L416
	ldr	r1, [r2, #16]
	ldr	r0, [sl, #12]
	mov	r2, r9
	bl	memcpy
	ldr	r3, [sl, #12]
	add	r1, r3, r9
	str	r1, [sl, #12]
	ldr	ip, [sl, #28]
	ldr	r2, [ip, #16]
	add	r0, r2, r9
	str	r0, [ip, #16]
	add	r1, sl, #16
	ldmia	r1, {r1, r3}	@ phole ldm
	add	r2, r3, r9
	rsb	ip, r9, r1
	str	ip, [sl, #16]
	str	r2, [sl, #20]
	ldr	r3, [sl, #28]
	ldr	r0, [r3, #20]
	rsb	r9, r9, r0
	str	r9, [r3, #20]
	ldr	r1, [sl, #28]
	ldr	r2, [r1, #20]
	cmp	r2, #0
	ldreq	r2, [r1, #8]
	ldr	ip, [r4, #0]
	streq	r2, [r1, #16]
	ldr	r3, [ip, #16]
.L416:
	cmp	r3, #0
	beq	.L380
	ldr	r2, [r4, #116]
	b	.L482
.L418:
	ldr	r0, [r4, #116]
	add	r3, r3, #1
	sub	r2, r0, #1
	mov	ip, #1
	str	ip, [r4, #104]
	str	r3, [r4, #108]
	str	r2, [r4, #116]
	b	.L439
.L444:
	ldr	r9, [r4, #92]
	cmp	r9, #0
	ldrge	r1, [r4, #56]
	ldr	lr, [r4, #108]
	addge	r1, r9, r1
	movlt	r1, #0
	rsb	r2, r9, lr
	mov	r3, #0
	mov	r0, r4
	bl	_tr_flush_block
	ldr	r3, [r4, #108]
	ldr	sl, [r4, #0]
	str	r3, [r4, #92]
	ldr	r2, [sl, #28]
	ldr	r1, [sl, #16]
	ldr	r9, [r2, #20]
	cmp	r9, r1
	movcs	r9, r1
	cmp	r9, #0
	beq	.L419
	ldr	r1, [r2, #16]
	ldr	r0, [sl, #12]
	mov	r2, r9
	bl	memcpy
	ldr	ip, [sl, #12]
	add	r3, ip, r9
	str	r3, [sl, #12]
	ldr	r1, [sl, #28]
	ldr	r2, [r1, #16]
	add	r0, r2, r9
	str	r0, [r1, #16]
	add	r3, sl, #16
	ldmia	r3, {r3, ip}	@ phole ldm
	add	r2, ip, r9
	rsb	r1, r9, r3
	str	r2, [sl, #20]
	str	r1, [sl, #16]
	ldr	ip, [sl, #28]
	ldr	r0, [ip, #20]
	rsb	r9, r9, r0
	str	r9, [ip, #20]
	ldr	r3, [sl, #28]
	ldr	r2, [r3, #20]
	cmp	r2, #0
	ldreq	r2, [r3, #8]
	streq	r2, [r3, #16]
	b	.L474
.L384:
	cmp	fp, #3
	mov	sl, fp
	bne	.L472
	cmp	ip, #1
	bne	.L472
	ldr	r1, [r4, #56]
	ldrb	r2, [r1, r3]	@ zero_extendqisi2
	ldrb	ip, [r1, r5]	@ zero_extendqisi2
	add	r0, r3, #256
	cmp	ip, r2
	add	ip, r0, #2
	add	r0, r1, ip
	add	r2, r1, r3
	add	r1, r1, r5
	beq	.L483
.L400:
	mov	r2, #2
	str	r2, [r4, #96]
.L489:
	cmp	r2, #5
	bhi	.L472
.L405:
	ldr	fp, [sp, #24]
	cmp	fp, #1
	bne	.L484
.L406:
	mov	r1, #2
	str	r1, [r4, #96]
	ldr	r0, [r4, #120]
	b	.L383
.L478:
	ldrb	ip, [r4, #1]	@ zero_extendqisi2
	ldr	fp, [sp, #28]
	str	ip, [sp, #56]
	ldrb	ip, [fp, #1]	@ zero_extendqisi2
	ldr	fp, [sp, #56]
	cmp	fp, ip
	bne	.L392
	ldr	fp, [sp, #28]
	add	ip, fp, #2
	str	ip, [sp, #4]
	mvn	fp, ip
	ldr	ip, [sp, #40]
	add	fp, fp, ip
	tst	fp, #8
	str	r4, [sp, #76]
	ldr	fp, [sp, #4]
	bne	.L442
	str	r5, [sp, #28]
	str	r3, [sp, #56]
	mov	r3, r4
.L394:
	ldrb	r4, [fp, #1]	@ zero_extendqisi2
	ldrb	r5, [r3, #3]	@ zero_extendqisi2
	cmp	r4, r5
	add	ip, fp, #1
	bne	.L470
.L485:
	ldrb	ip, [r3, #4]	@ zero_extendqisi2
	ldrb	r4, [fp, #2]	@ zero_extendqisi2
	cmp	r4, ip
	add	ip, fp, #2
	bne	.L470
	ldrb	r5, [fp, #3]	@ zero_extendqisi2
	ldrb	r4, [r3, #5]	@ zero_extendqisi2
	cmp	r5, r4
	add	ip, fp, #3
	bne	.L470
	ldrb	ip, [r3, #6]	@ zero_extendqisi2
	ldrb	r5, [fp, #4]	@ zero_extendqisi2
	cmp	r5, ip
	add	ip, fp, #4
	bne	.L470
	ldrb	r4, [fp, #5]	@ zero_extendqisi2
	ldrb	r5, [r3, #7]	@ zero_extendqisi2
	cmp	r4, r5
	add	ip, fp, #5
	bne	.L470
	ldrb	ip, [r3, #8]	@ zero_extendqisi2
	ldrb	r4, [fp, #6]	@ zero_extendqisi2
	cmp	r4, ip
	add	ip, fp, #6
	bne	.L470
	ldrb	r5, [fp, #7]	@ zero_extendqisi2
	ldrb	r4, [r3, #9]	@ zero_extendqisi2
	cmp	r5, r4
	add	ip, fp, #7
	bne	.L470
	ldrb	ip, [r3, #10]	@ zero_extendqisi2
	ldrb	r5, [fp, #8]	@ zero_extendqisi2
	cmp	r5, ip
	add	ip, fp, #8
	bne	.L470
	ldr	r4, [sp, #40]
	cmp	r4, ip
	add	r3, r3, #8
	bls	.L470
	ldrb	r5, [ip, #1]	@ zero_extendqisi2
	mov	r4, ip
	ldrb	ip, [r3, #3]	@ zero_extendqisi2
	cmp	r5, ip
	add	ip, fp, #9
	bne	.L470
	ldrb	ip, [r3, #4]	@ zero_extendqisi2
	ldrb	fp, [r4, #2]	@ zero_extendqisi2
	cmp	fp, ip
	add	ip, r4, #2
	bne	.L470
	ldrb	ip, [r3, #5]	@ zero_extendqisi2
	ldrb	fp, [r4, #3]	@ zero_extendqisi2
	cmp	fp, ip
	add	ip, r4, #3
	bne	.L470
	ldrb	ip, [r3, #6]	@ zero_extendqisi2
	ldrb	fp, [r4, #4]	@ zero_extendqisi2
	cmp	fp, ip
	add	ip, r4, #4
	bne	.L470
	ldrb	ip, [r3, #7]	@ zero_extendqisi2
	ldrb	fp, [r4, #5]	@ zero_extendqisi2
	cmp	fp, ip
	add	ip, r4, #5
	bne	.L470
	ldrb	ip, [r3, #8]	@ zero_extendqisi2
	ldrb	fp, [r4, #6]	@ zero_extendqisi2
	cmp	fp, ip
	add	ip, r4, #6
	bne	.L470
	ldrb	ip, [r3, #9]	@ zero_extendqisi2
	ldrb	fp, [r4, #7]	@ zero_extendqisi2
	cmp	fp, ip
	add	ip, r4, #7
	bne	.L470
	ldrb	ip, [r3, #10]	@ zero_extendqisi2
	ldrb	fp, [r4, #8]	@ zero_extendqisi2
	cmp	fp, ip
	add	ip, r4, #8
	bne	.L470
	add	fp, r4, #8
	add	r3, r3, #8
	ldrb	r4, [fp, #1]	@ zero_extendqisi2
	ldrb	r5, [r3, #3]	@ zero_extendqisi2
	cmp	r4, r5
	add	ip, fp, #1
	beq	.L485
.L470:
	ldr	r5, [sp, #28]
	ldr	r3, [sp, #56]
.L393:
	add	r4, sp, #44
	ldmia	r4, {r4, fp}	@ phole ldm
	add	ip, fp, ip
	cmp	r0, ip
	str	r4, [sp, #28]
	bge	.L392
	ldr	fp, [sp, #72]
	ldr	r6, [sp, #32]
	cmp	ip, fp
	str	r2, [r6, #112]
	bge	.L486
	ldr	r0, [sp, #44]
	add	r3, r0, ip
	ldrb	r5, [r3, #-1]	@ zero_extendqisi2
	mov	r6, ip
	ldrb	r3, [r0, ip]	@ zero_extendqisi2
	b	.L390
.L381:
	ldr	r6, [r4, #104]
	cmp	r6, #0
	beq	.L422
	mov	r2, #5760
	add	r2, r2, #32
	mov	r1, #5760
	ldr	ip, [r4, r2]
	ldr	r5, [r4, #108]
	ldr	r6, [r4, #56]
	add	sl, r1, #36
	ldr	r1, [r4, sl]
	add	r0, r5, r6
	mov	lr, ip, asl #1
	ldrb	r0, [r0, #-1]	@ zero_extendqisi2
	strh	r3, [lr, r1]	@ movhi
	mov	sl, #5760
	ldr	ip, [r4, r2]
	add	r5, sl, #24
	ldr	r6, [r4, r5]
	add	r1, ip, #1
	strb	r0, [r6, ip]
	add	lr, r0, #36
	str	r1, [r4, r2]
	add	r5, r4, lr, asl #2
	ldrh	sl, [r5, #4]
	add	r1, sl, #1
	strh	r1, [r5, #4]	@ movhi
	str	r3, [r4, #104]
.L422:
	ldr	r0, [r4, #92]
	cmp	r0, #0
	ldrge	r1, [r4, #56]
	ldr	lr, [r4, #108]
	ldr	sl, [sp, #20]
	addge	r1, r0, r1
	movlt	r1, #0
	rsb	r2, r0, lr
	cmp	sl, #4
	movne	r3, #0
	moveq	r3, #1
	mov	r0, r4
	bl	_tr_flush_block
	ldr	r3, [r4, #108]
	ldr	r5, [r4, #0]
	str	r3, [r4, #92]
	ldr	r2, [r5, #28]
	ldr	r3, [r5, #16]
	ldr	r6, [r2, #20]
	cmp	r3, r6
	movcc	r6, r3
	cmp	r6, #0
	bne	.L487
	cmp	r3, #0
	bne	.L427
.L488:
	ldr	fp, [sp, #20]
	cmp	fp, #4
	moveq	r0, #2
	bne	.L380
	b	.L428
.L427:
	ldr	lr, [sp, #20]
	cmp	lr, #4
	moveq	r0, #3
	movne	r0, #1
	b	.L428
.L487:
	ldr	r1, [r2, #16]
	ldr	r0, [r5, #12]
	mov	r2, r6
	bl	memcpy
	ldr	r1, [r5, #12]
	add	ip, r1, r6
	str	ip, [r5, #12]
	ldr	r3, [r5, #28]
	ldr	r2, [r3, #16]
	add	r1, r2, r6
	str	r1, [r3, #16]
	add	r3, r5, #16
	ldmia	r3, {r3, ip}	@ phole ldm
	add	r2, ip, r6
	rsb	r1, r6, r3
	str	r2, [r5, #20]
	str	r1, [r5, #16]
	ldr	r3, [r5, #28]
	ldr	ip, [r3, #20]
	rsb	r6, r6, ip
	str	r6, [r3, #20]
	ldr	r2, [r5, #28]
	ldr	r1, [r2, #20]
	cmp	r1, #0
	ldreq	r1, [r2, #8]
	ldr	ip, [r4, #0]
	streq	r1, [r2, #16]
	ldr	r3, [ip, #16]
	cmp	r3, #0
	bne	.L427
	b	.L488
.L483:
	ldrb	sl, [r1, #1]	@ zero_extendqisi2
	ldrb	ip, [r2, #1]	@ zero_extendqisi2
	cmp	sl, ip
	bne	.L400
	add	r2, r2, #2
.L403:
	ldrb	ip, [r1, #3]	@ zero_extendqisi2
	ldrb	sl, [r2, #1]	@ zero_extendqisi2
	cmp	sl, ip
	add	ip, r2, #1
	bne	.L401
	ldrb	ip, [r1, #4]	@ zero_extendqisi2
	ldrb	sl, [r2, #2]	@ zero_extendqisi2
	cmp	sl, ip
	add	ip, r2, #2
	bne	.L401
	ldrb	ip, [r1, #5]	@ zero_extendqisi2
	ldrb	sl, [r2, #3]	@ zero_extendqisi2
	cmp	sl, ip
	add	ip, r2, #3
	bne	.L401
	ldrb	ip, [r1, #6]	@ zero_extendqisi2
	ldrb	sl, [r2, #4]	@ zero_extendqisi2
	cmp	sl, ip
	add	ip, r2, #4
	bne	.L401
	ldrb	ip, [r1, #7]	@ zero_extendqisi2
	ldrb	sl, [r2, #5]	@ zero_extendqisi2
	cmp	sl, ip
	add	ip, r2, #5
	bne	.L401
	ldrb	ip, [r1, #8]	@ zero_extendqisi2
	ldrb	sl, [r2, #6]	@ zero_extendqisi2
	cmp	sl, ip
	add	ip, r2, #6
	bne	.L401
	ldrb	ip, [r1, #9]	@ zero_extendqisi2
	ldrb	sl, [r2, #7]	@ zero_extendqisi2
	cmp	sl, ip
	add	ip, r2, #7
	bne	.L401
	ldrb	sl, [r2, #8]!	@ zero_extendqisi2
	ldrb	ip, [r1, #10]	@ zero_extendqisi2
	cmp	sl, ip
	bne	.L402
	cmp	r2, r0
	add	r1, r1, #8
	bcc	.L403
.L402:
	mov	ip, r2
.L401:
	rsb	r0, r0, #256
	add	r1, r0, #2
	add	r0, r1, ip
	cmp	r0, #2
	ble	.L400
	ldr	r2, [r4, #116]
	cmp	r0, r2
	movcc	r2, r0
	str	r5, [r4, #112]
	str	r2, [r4, #96]
	b	.L489
.L442:
	ldrb	fp, [fp, #1]	@ zero_extendqisi2
	str	fp, [sp, #56]
	ldr	ip, [sp, #56]
	ldrb	fp, [r4, #3]	@ zero_extendqisi2
	cmp	ip, fp
	ldr	fp, [sp, #28]
	add	ip, fp, #3
	bne	.L393
	ldr	ip, [sp, #4]
	ldrb	fp, [ip, #2]	@ zero_extendqisi2
	str	fp, [sp, #56]
	ldr	fp, [sp, #56]
	ldrb	ip, [r4, #4]	@ zero_extendqisi2
	cmp	fp, ip
	ldr	fp, [sp, #4]
	add	ip, fp, #2
	bne	.L393
	ldrb	ip, [fp, #3]	@ zero_extendqisi2
	str	ip, [sp, #56]
	ldr	fp, [sp, #56]
	ldrb	ip, [r4, #5]	@ zero_extendqisi2
	cmp	fp, ip
	ldr	fp, [sp, #28]
	add	ip, fp, #5
	bne	.L393
	ldr	ip, [sp, #4]
	ldrb	fp, [ip, #4]	@ zero_extendqisi2
	str	fp, [sp, #56]
	ldr	fp, [sp, #56]
	ldrb	ip, [r4, #6]	@ zero_extendqisi2
	cmp	fp, ip
	ldr	fp, [sp, #28]
	add	ip, fp, #6
	bne	.L393
	ldr	ip, [sp, #4]
	ldrb	fp, [ip, #5]	@ zero_extendqisi2
	str	fp, [sp, #56]
	ldr	fp, [sp, #56]
	ldrb	ip, [r4, #7]	@ zero_extendqisi2
	cmp	fp, ip
	ldr	fp, [sp, #28]
	add	ip, fp, #7
	bne	.L393
	ldr	ip, [sp, #4]
	ldrb	fp, [ip, #6]	@ zero_extendqisi2
	str	fp, [sp, #28]
	ldr	fp, [sp, #28]
	ldrb	ip, [r4, #8]	@ zero_extendqisi2
	cmp	fp, ip
	ldr	fp, [sp, #4]
	add	ip, fp, #6
	bne	.L393
	ldrb	ip, [fp, #7]	@ zero_extendqisi2
	str	ip, [sp, #28]
	ldr	fp, [sp, #28]
	ldrb	ip, [r4, #9]	@ zero_extendqisi2
	cmp	fp, ip
	ldr	fp, [sp, #4]
	add	ip, fp, #7
	bne	.L393
	ldrb	ip, [fp, #8]	@ zero_extendqisi2
	str	ip, [sp, #28]
	ldr	fp, [sp, #28]
	ldrb	ip, [r4, #10]	@ zero_extendqisi2
	cmp	fp, ip
	ldr	fp, [sp, #4]
	add	ip, fp, #8
	bne	.L393
	ldr	fp, [sp, #40]
	add	r4, r4, #8
	cmp	fp, ip
	str	r4, [sp, #76]
	bls	.L393
	str	r3, [sp, #56]
	str	ip, [sp, #4]
	mov	fp, ip
	str	r5, [sp, #28]
	mov	r3, r4
	b	.L394
.L484:
	cmp	r2, #3
	bne	.L472
	ldr	ip, [r4, #112]
	rsb	r2, ip, r3
	cmp	r2, #4096
	bhi	.L406
	ldr	r0, [r4, #120]
	b	.L383
.L486:
	ldr	r5, [sp, #60]
	ldr	r4, [sp, #32]
	ldr	r3, [sp, #64]
	ldr	r7, [sp, #16]
	ldr	r6, [sp, #36]
	ldr	r8, [sp, #68]
	mov	r2, ip
	b	.L396
.L491:
	.align	2
.L490:
	.word	_length_code
	.word	_dist_code
	.size	deflate_slow, .-deflate_slow
	.section	.text.deflate_fast,"ax",%progbits
	.align	2
	.type	deflate_fast, %function
deflate_fast:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 72
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	mov	r8, #260
	mov	r4, r0
	mov	r0, #5760
	sub	sp, sp, #76
	mov	r6, #5760
	add	r2, r8, #1
	add	r3, r0, #28
	mov	r7, r6
	mov	r9, r6
	str	r1, [sp, #40]
	str	r2, [sp, #8]
	str	r3, [sp, #16]
	mov	r5, #0
	add	r6, r6, #32
	add	r7, r7, #36
	add	r9, r9, #24
.L545:
	ldr	r1, [r4, #116]
	ldr	r8, [sp, #8]
	cmp	r1, r8
	bls	.L588
.L493:
	ldr	ip, [r4, #108]
	ldr	r5, [r4, #56]
	add	fp, ip, r5
	ldrb	r1, [fp, #2]	@ zero_extendqisi2
	ldr	lr, [r4, #72]
	ldr	r2, [r4, #88]
	eor	r3, r1, lr, asl r2
	ldr	r0, [r4, #84]
	and	r5, r3, r0
	str	r5, [r4, #72]
	ldr	lr, [r4, #68]
	ldr	r1, [r4, #52]
	mov	fp, r5, asl #1
	ldrh	r5, [fp, lr]
	and	r2, r1, ip
	ldr	r0, [r4, #64]
	mov	r3, r2, asl #1
	strh	r5, [r3, r0]	@ movhi
	ldr	fp, [r4, #72]
	ldr	lr, [r4, #68]
	mov	ip, fp, asl #1
	ldrh	fp, [r4, #108]
	strh	fp, [ip, lr]	@ movhi
.L496:
	cmp	r5, #0
	bne	.L497
	ldr	r8, [r4, #96]
	ldr	r3, [r4, #108]
.L498:
	cmp	r8, #2
	bls	.L520
	ldr	r2, [r4, #112]
	ldr	r1, [r4, r6]
	rsb	ip, r2, r3
	ldr	lr, [r4, r7]
	mov	r3, ip, asl #16
	mov	r3, r3, lsr #16
	mov	r0, r1, asl #1
	strh	r3, [r0, lr]	@ movhi
	sub	sl, r8, #3
	ldr	r1, [r4, r6]
	ldr	lr, [r4, r9]
	and	ip, sl, #255
	ldr	sl, .L596
	strb	ip, [lr, r1]
	ldrb	r2, [sl, ip]	@ zero_extendqisi2
	sub	ip, r3, #1
	mov	r3, ip, asl #16
	cmp	r3, #16711680
	ldrhi	lr, .L596+4
	add	r0, r1, #1
	add	r2, r2, #292
	str	r0, [r4, r6]
	ldrls	fp, .L596+4
	add	r8, r2, #1
	addhi	r3, lr, r3, lsr #23
	add	r2, r4, r8, asl #2
	ldrh	r1, [r2, #4]
	ldrlsb	r3, [fp, r3, lsr #16]	@ zero_extendqisi2
	ldrhib	r3, [r3, #256]	@ zero_extendqisi2
	add	r8, r1, #1
	add	ip, r3, #608
	strh	r8, [r2, #4]	@ movhi
	add	r1, ip, #2
	mov	lr, r1, asl #2
	ldrh	r8, [lr, r4]
	add	r2, r8, #1
	strh	r2, [lr, r4]	@ movhi
	ldr	r3, [sp, #16]
	ldr	ip, [r4, r3]
	ldr	lr, [r4, #116]
	ldr	r3, [r4, #96]
	ldr	r8, [r4, #128]
	sub	r1, ip, #1
	cmp	r0, r1
	movne	r1, #0
	moveq	r1, #1
	rsb	r2, r3, lr
	cmp	r3, r8
	str	r2, [r4, #116]
	bhi	.L523
	cmp	r2, #2
	bls	.L523
	sub	r5, r3, #1
	str	r5, [r4, #96]
.L524:
	ldr	r3, [r4, #108]
	ldr	lr, [r4, #56]
	add	r3, r3, #1
	str	r3, [r4, #108]
	add	ip, r3, lr
	ldrb	r5, [ip, #2]	@ zero_extendqisi2
	ldr	r2, [r4, #72]
	ldr	r8, [r4, #88]
	eor	lr, r5, r2, asl r8
	ldr	r0, [r4, #84]
	and	r5, lr, r0
	str	r5, [r4, #72]
	ldr	ip, [r4, #68]
	ldr	r8, [r4, #52]
	mov	r2, r5, asl #1
	ldrh	r5, [r2, ip]
	and	lr, r3, r8
	ldr	r8, [r4, #64]
	mov	r0, lr, asl #1
	strh	r5, [r0, r8]	@ movhi
	ldr	r2, [r4, #72]
	ldr	r3, [r4, #68]
	ldrh	lr, [r4, #108]
	mov	ip, r2, asl #1
	strh	lr, [ip, r3]	@ movhi
	ldr	r0, [r4, #96]
	sub	r8, r0, #1
	cmp	r8, #0
	str	r8, [r4, #96]
	bne	.L524
	ldr	r0, [r4, #108]
	add	r3, r0, #1
	str	r3, [r4, #108]
.L525:
	cmp	r1, #0
	beq	.L545
	ldr	r2, [r4, #92]
	cmp	r2, #0
	ldrge	r1, [r4, #56]
	movlt	r1, #0
	addge	r1, r2, r1
	mov	r0, r4
	rsb	r2, r2, r3
	mov	r3, #0
	bl	_tr_flush_block
	ldr	r3, [r4, #108]
	ldr	r8, [r4, #0]
	str	r3, [r4, #92]
	ldr	r2, [r8, #28]
	ldr	r3, [r8, #16]
	ldr	sl, [r2, #20]
	cmp	r3, sl
	movcc	sl, r3
	cmp	sl, #0
	beq	.L529
	ldr	r1, [r2, #16]
	ldr	r0, [r8, #12]
	mov	r2, sl
	bl	memcpy
	ldr	r3, [r8, #12]
	add	r1, r3, sl
	str	r1, [r8, #12]
	ldr	r0, [r8, #28]
	ldr	ip, [r0, #16]
	add	r2, ip, sl
	str	r2, [r0, #16]
	add	r1, r8, #16
	ldmia	r1, {r1, r3}	@ phole ldm
	add	ip, r3, sl
	rsb	r0, sl, r1
	str	r0, [r8, #16]
	str	ip, [r8, #20]
	ldr	r3, [r8, #28]
	ldr	r2, [r3, #20]
	rsb	sl, sl, r2
	str	sl, [r3, #20]
	ldr	r1, [r8, #28]
	ldr	ip, [r1, #20]
	cmp	ip, #0
	ldreq	ip, [r1, #8]
	ldr	r0, [r4, #0]
	streq	ip, [r1, #16]
	ldr	r3, [r0, #16]
.L529:
	cmp	r3, #0
	bne	.L545
.L494:
	mov	r0, #0
.L536:
	add	sp, sp, #76
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L520:
	ldr	r1, [r4, r6]
	ldr	lr, [r4, #56]
	ldr	sl, [r4, r7]
	mov	ip, r1, asl #1
	mov	r8, #0	@ movhi
	ldrb	r3, [lr, r3]	@ zero_extendqisi2
	strh	r8, [ip, sl]	@ movhi
	ldr	r0, [r4, r6]
	ldr	r2, [r4, r9]
	add	ip, r0, #1
	strb	r3, [r2, r0]
	add	lr, r3, #36
	str	ip, [r4, r6]
	add	r8, r4, lr, asl #2
	ldrh	r1, [r8, #4]
	add	sl, r1, #1
	strh	sl, [r8, #4]	@ movhi
	ldr	sl, [sp, #16]
	ldr	r0, [r4, #116]
	ldr	r3, [r4, #108]
	ldr	lr, [r4, sl]
	sub	r2, r0, #1
	add	r3, r3, #1
	sub	r1, lr, #1
	str	r2, [r4, #116]
	str	r3, [r4, #108]
	cmp	ip, r1
	movne	r1, #0
	moveq	r1, #1
	b	.L525
.L497:
	ldr	r2, [r4, #44]
	ldr	r3, [r4, #108]
	sub	ip, r2, #260
	sub	r1, ip, #2
	rsb	r0, r5, r3
	cmp	r0, r1
	str	r3, [sp, #12]
	bhi	.L585
	ldr	ip, [r4, #136]
	sub	lr, ip, #2
	cmp	lr, #1
	bls	.L500
	ldr	ip, [sp, #12]
	ldr	lr, [r4, #56]
	cmp	ip, r1
	movhi	r8, ip
	add	r0, lr, ip
	addhi	ip, r8, #260
	ldr	r1, [r4, #124]
	addhi	ip, ip, #2
	str	r0, [sp, #20]
	rsbhi	ip, r2, ip
	movls	r2, #0
	ldr	r0, [r4, #120]
	ldr	r3, [r4, #144]
	ldrls	sl, [sp, #12]
	strls	r2, [sp, #52]
	strhi	ip, [sp, #52]
	movhi	sl, r8
	ldr	fp, [r4, #140]
	add	r8, sl, #256
	add	ip, r8, #2
	cmp	r0, fp
	add	fp, lr, ip
	str	fp, [sp, #24]
	ldr	r8, [sp, #20]
	ldr	r2, [sp, #20]
	ldrb	ip, [r8, r0]	@ zero_extendqisi2
	ldr	r8, [r4, #116]
	add	sl, r2, r0
	ldrb	fp, [sl, #-1]	@ zero_extendqisi2
	ldr	r2, [r4, #64]
	ldr	sl, [r4, #52]
	str	r8, [sp, #36]
	ldr	r8, [sp, #24]
	rsb	r8, r8, #256
	str	r8, [sp, #32]
	ldr	r8, [sp, #24]
	sub	r8, r8, #256
	str	r8, [sp, #28]
	ldr	r8, [sp, #36]
	movcs	r1, r1, lsr #2
	cmp	r3, r8
	movcc	r8, r3
	ldr	r3, [sp, #32]
	str	r8, [sp, #60]
	add	r8, r3, #2
	ldr	r3, [sp, #28]
	str	r8, [sp, #32]
	sub	r8, r3, #2
	str	r6, [sp, #48]
	str	r8, [sp, #28]
	ldr	r6, [sp, #52]
	str	r5, [sp, #44]
	str	r7, [sp, #52]
	mov	r3, r5
	str	r9, [sp, #56]
	mov	r7, r2
	mov	r9, lr
	b	.L513
.L506:
	and	lr, r3, sl
	mov	r3, lr, asl #1
	ldrh	r3, [r3, r7]
	cmp	r3, r6
	bls	.L560
.L590:
	subs	r1, r1, #1
	beq	.L560
	mov	r0, ip
	mov	fp, r5
	mov	ip, r2
.L513:
	add	lr, r9, r3
	ldrb	r2, [lr, r0]	@ zero_extendqisi2
	cmp	r2, ip
	movne	r2, ip
	mov	r8, r0
	movne	r5, fp
	movne	ip, r0
	bne	.L506
	add	ip, r0, lr
	ldrb	r5, [ip, #-1]	@ zero_extendqisi2
	cmp	r5, fp
	movne	r5, fp
	movne	ip, r0
	bne	.L506
	ldrb	ip, [lr, #0]	@ zero_extendqisi2
	ldr	fp, [sp, #20]
	str	ip, [sp, #64]
	ldrb	ip, [fp, #0]	@ zero_extendqisi2
	ldr	fp, [sp, #64]
	cmp	fp, ip
	beq	.L589
.L508:
	and	lr, r3, sl
	mov	r3, lr, asl #1
	ldrh	r3, [r3, r7]
	cmp	r3, r6
	mov	ip, r0
	bhi	.L590
.L560:
	add	r5, sp, #44
	ldmia	r5, {r5, r6, r7, r9}	@ phole ldm
.L512:
	ldr	r1, [sp, #36]
	cmp	r8, r1
	movcs	r8, r1
	str	r8, [r4, #96]
	ldr	r3, [sp, #12]
	b	.L498
.L589:
	ldrb	ip, [lr, #1]	@ zero_extendqisi2
	ldr	fp, [sp, #20]
	str	ip, [sp, #64]
	ldrb	ip, [fp, #1]	@ zero_extendqisi2
	ldr	fp, [sp, #64]
	cmp	fp, ip
	bne	.L508
	ldr	fp, [sp, #20]
	add	ip, fp, #2
	mvn	fp, ip
	str	ip, [sp, #4]
	ldr	ip, [sp, #24]
	add	ip, fp, ip
	tst	ip, #8
	str	ip, [sp, #64]
	bne	.L546
	str	r2, [sp, #20]
	str	r5, [sp, #64]
	ldr	fp, [sp, #4]
.L510:
	ldrb	r2, [fp, #1]	@ zero_extendqisi2
	ldrb	r5, [lr, #3]	@ zero_extendqisi2
	cmp	r2, r5
	add	ip, fp, #1
	bne	.L576
.L591:
	ldrb	ip, [fp, #2]	@ zero_extendqisi2
	ldrb	r5, [lr, #4]	@ zero_extendqisi2
	cmp	ip, r5
	add	ip, fp, #2
	bne	.L576
	ldrb	r5, [fp, #3]	@ zero_extendqisi2
	ldrb	r2, [lr, #5]	@ zero_extendqisi2
	cmp	r5, r2
	add	ip, fp, #3
	bne	.L576
	ldrb	ip, [fp, #4]	@ zero_extendqisi2
	ldrb	r2, [lr, #6]	@ zero_extendqisi2
	cmp	ip, r2
	add	ip, fp, #4
	bne	.L576
	ldrb	r2, [fp, #5]	@ zero_extendqisi2
	ldrb	r5, [lr, #7]	@ zero_extendqisi2
	cmp	r2, r5
	add	ip, fp, #5
	bne	.L576
	ldrb	ip, [fp, #6]	@ zero_extendqisi2
	ldrb	r5, [lr, #8]	@ zero_extendqisi2
	cmp	ip, r5
	add	ip, fp, #6
	bne	.L576
	ldrb	r5, [fp, #7]	@ zero_extendqisi2
	ldrb	r2, [lr, #9]	@ zero_extendqisi2
	cmp	r5, r2
	add	ip, fp, #7
	bne	.L576
	ldrb	ip, [fp, #8]	@ zero_extendqisi2
	ldrb	r2, [lr, #10]	@ zero_extendqisi2
	cmp	ip, r2
	add	ip, fp, #8
	bne	.L576
	ldr	r5, [sp, #24]
	cmp	r5, ip
	add	lr, lr, #8
	bls	.L576
	mov	r5, ip
	ldrb	r2, [lr, #3]	@ zero_extendqisi2
	ldrb	ip, [ip, #1]	@ zero_extendqisi2
	cmp	ip, r2
	add	ip, fp, #9
	bne	.L576
	ldrb	ip, [lr, #4]	@ zero_extendqisi2
	ldrb	fp, [r5, #2]	@ zero_extendqisi2
	cmp	fp, ip
	add	ip, r5, #2
	bne	.L576
	ldrb	ip, [lr, #5]	@ zero_extendqisi2
	ldrb	fp, [r5, #3]	@ zero_extendqisi2
	cmp	fp, ip
	add	ip, r5, #3
	bne	.L576
	ldrb	ip, [lr, #6]	@ zero_extendqisi2
	ldrb	fp, [r5, #4]	@ zero_extendqisi2
	cmp	fp, ip
	add	ip, r5, #4
	bne	.L576
	ldrb	ip, [lr, #7]	@ zero_extendqisi2
	ldrb	fp, [r5, #5]	@ zero_extendqisi2
	cmp	fp, ip
	add	ip, r5, #5
	bne	.L576
	ldrb	ip, [lr, #8]	@ zero_extendqisi2
	ldrb	fp, [r5, #6]	@ zero_extendqisi2
	cmp	fp, ip
	add	ip, r5, #6
	bne	.L576
	ldrb	ip, [lr, #9]	@ zero_extendqisi2
	ldrb	fp, [r5, #7]	@ zero_extendqisi2
	cmp	fp, ip
	add	ip, r5, #7
	bne	.L576
	ldrb	ip, [lr, #10]	@ zero_extendqisi2
	ldrb	fp, [r5, #8]	@ zero_extendqisi2
	cmp	fp, ip
	add	ip, r5, #8
	bne	.L576
	add	fp, r5, #8
	add	lr, lr, #8
	ldrb	r2, [fp, #1]	@ zero_extendqisi2
	ldrb	r5, [lr, #3]	@ zero_extendqisi2
	cmp	r2, r5
	add	ip, fp, #1
	beq	.L591
.L576:
	ldr	r5, [sp, #64]
	ldr	r2, [sp, #20]
	str	ip, [sp, #64]
.L509:
	ldr	fp, [sp, #32]
	ldr	lr, [sp, #64]
	add	ip, fp, lr
	ldr	fp, [sp, #28]
	cmp	r0, ip
	str	fp, [sp, #20]
	bge	.L508
	ldr	r8, [sp, #60]
	cmp	ip, r8
	str	r3, [r4, #112]
	bge	.L592
	ldr	r0, [sp, #28]
	add	r2, r0, ip
	ldrb	r5, [r2, #-1]	@ zero_extendqisi2
	mov	r8, ip
	ldrb	r2, [r0, ip]	@ zero_extendqisi2
	b	.L506
.L523:
	ldr	r8, [r4, #108]
	mov	r0, #0
	add	r3, r3, r8
	str	r3, [r4, #108]
	str	r0, [r4, #96]
	ldr	r2, [r4, #56]
	mov	lr, r3
	ldrb	r0, [lr, r2]!	@ zero_extendqisi2
	str	r0, [r4, #72]
	ldr	ip, [r4, #88]
	ldrb	r8, [lr, #1]	@ zero_extendqisi2
	eor	lr, r8, r0, asl ip
	ldr	r2, [r4, #84]
	and	r8, lr, r2
	str	r8, [r4, #72]
	b	.L525
.L500:
	cmp	ip, #3
	bne	.L585
	cmp	r0, #1
	beq	.L515
.L585:
	ldr	r8, [r4, #96]
	b	.L498
.L588:
	mov	r0, r4
	bl	fill_window
	ldr	r1, [r4, #116]
	ldr	sl, [sp, #40]
	cmp	r1, r8
	cmpls	sl, #0
	beq	.L494
	cmp	r1, #0
	beq	.L495
	cmp	r1, #2
	bls	.L496
	b	.L493
.L495:
	ldr	r5, [r4, #92]
	ldr	fp, [r4, #108]
	cmp	r5, #0
	ldrge	r1, [r4, #56]
	rsb	r2, r5, fp
	ldr	fp, [sp, #40]
	addge	r1, r5, r1
	mov	r0, r4
	cmp	fp, #4
	movne	r3, #0
	moveq	r3, #1
	bl	_tr_flush_block
	ldr	r6, [r4, #108]
	ldr	r5, [r4, #0]
	str	r6, [r4, #92]
	ldr	r2, [r5, #28]
	ldr	r3, [r5, #16]
	ldr	r6, [r2, #20]
	cmp	r3, r6
	movcc	r6, r3
	cmp	r6, #0
	bne	.L593
	cmp	r3, #0
	bne	.L535
.L595:
	ldr	lr, [sp, #40]
	cmp	lr, #4
	moveq	r0, #2
	bne	.L494
	b	.L536
.L515:
	ldr	sl, [r4, #56]
	ldr	r8, [sp, #12]
	ldrb	r2, [sl, r5]	@ zero_extendqisi2
	ldrb	r3, [sl, r8]	@ zero_extendqisi2
	add	r0, r8, #256
	add	ip, r0, #2
	cmp	r2, r3
	add	r0, sl, ip
	add	r2, sl, r5
	add	ip, sl, r8
	beq	.L594
.L516:
	mov	r8, #2
.L519:
	str	r8, [r4, #96]
	ldr	r3, [sp, #12]
	b	.L498
.L535:
	ldr	r1, [sp, #40]
	cmp	r1, #4
	moveq	r0, #3
	movne	r0, #1
	b	.L536
.L593:
	ldr	r1, [r2, #16]
	ldr	r0, [r5, #12]
	mov	r2, r6
	bl	memcpy
	ldr	ip, [r5, #12]
	add	r2, ip, r6
	str	r2, [r5, #12]
	ldr	r1, [r5, #28]
	ldr	r3, [r1, #16]
	add	ip, r3, r6
	str	ip, [r1, #16]
	ldr	r2, [r5, #20]
	ldr	r3, [r5, #16]
	add	r1, r2, r6
	rsb	ip, r6, r3
	str	r1, [r5, #20]
	str	ip, [r5, #16]
	ldr	r3, [r5, #28]
	ldr	r2, [r3, #20]
	rsb	r6, r6, r2
	str	r6, [r3, #20]
	ldr	r1, [r5, #28]
	ldr	ip, [r1, #20]
	cmp	ip, #0
	ldreq	ip, [r1, #8]
	ldr	r2, [r4, #0]
	streq	ip, [r1, #16]
	ldr	r3, [r2, #16]
	cmp	r3, #0
	bne	.L535
	b	.L595
.L594:
	ldrb	lr, [r2, #1]	@ zero_extendqisi2
	ldrb	r1, [ip, #1]	@ zero_extendqisi2
	cmp	lr, r1
	bne	.L516
	add	r1, ip, #2
	ldrb	r8, [r1, #1]	@ zero_extendqisi2
	ldrb	sl, [r2, #3]	@ zero_extendqisi2
	cmp	r8, sl
	add	r3, ip, #3
	bne	.L517
	ldrb	r8, [r1, #2]	@ zero_extendqisi2
	ldrb	sl, [r2, #4]	@ zero_extendqisi2
	cmp	r8, sl
	add	r3, r1, #2
	bne	.L517
	ldrb	r3, [r2, #5]	@ zero_extendqisi2
	ldrb	lr, [r1, #3]	@ zero_extendqisi2
	cmp	lr, r3
	add	r3, ip, #5
	bne	.L517
	ldrb	ip, [r1, #4]	@ zero_extendqisi2
	ldrb	sl, [r2, #6]	@ zero_extendqisi2
	cmp	ip, sl
	add	r3, r1, #4
	bne	.L517
	ldrb	r3, [r1, #5]	@ zero_extendqisi2
	ldrb	r8, [r2, #7]	@ zero_extendqisi2
	cmp	r3, r8
	add	r3, r1, #5
	bne	.L517
	ldrb	sl, [r1, #6]	@ zero_extendqisi2
	ldrb	lr, [r2, #8]	@ zero_extendqisi2
	cmp	sl, lr
	add	r3, r1, #6
	bne	.L517
	ldrb	ip, [r1, #7]	@ zero_extendqisi2
	ldrb	r8, [r2, #9]	@ zero_extendqisi2
	cmp	ip, r8
	add	r3, r1, #7
	bne	.L517
	ldrb	r3, [r2, #10]	@ zero_extendqisi2
	ldrb	lr, [r1, #8]	@ zero_extendqisi2
	cmp	lr, r3
	add	r3, r1, #8
	bne	.L517
	cmp	r3, r0
	add	sl, r2, #8
	movcc	r1, sl
	movcc	r2, r3
	bcs	.L517
.L518:
	ldrb	r3, [r1, #3]	@ zero_extendqisi2
	ldrb	ip, [r2, #1]	@ zero_extendqisi2
	cmp	ip, r3
	add	r3, r2, #1
	bne	.L517
	ldrb	sl, [r2, #2]	@ zero_extendqisi2
	ldrb	lr, [r1, #4]	@ zero_extendqisi2
	cmp	sl, lr
	add	r3, r2, #2
	bne	.L517
	ldrb	r3, [r2, #3]	@ zero_extendqisi2
	ldrb	r8, [r1, #5]	@ zero_extendqisi2
	cmp	r3, r8
	add	r3, r2, #3
	bne	.L517
	ldrb	ip, [r2, #4]	@ zero_extendqisi2
	ldrb	lr, [r1, #6]	@ zero_extendqisi2
	cmp	ip, lr
	add	r3, r2, #4
	bne	.L517
	ldrb	r8, [r2, #5]	@ zero_extendqisi2
	ldrb	sl, [r1, #7]	@ zero_extendqisi2
	cmp	r8, sl
	add	r3, r2, #5
	bne	.L517
	ldrb	r3, [r1, #8]	@ zero_extendqisi2
	ldrb	lr, [r2, #6]	@ zero_extendqisi2
	cmp	lr, r3
	add	r3, r2, #6
	bne	.L517
	ldrb	ip, [r2, #7]	@ zero_extendqisi2
	ldrb	sl, [r1, #9]	@ zero_extendqisi2
	cmp	ip, sl
	add	r3, r2, #7
	bne	.L517
	ldrb	r3, [r2, #8]	@ zero_extendqisi2
	ldrb	r8, [r1, #10]	@ zero_extendqisi2
	cmp	r3, r8
	add	r3, r2, #8
	bne	.L517
	cmp	r3, r0
	add	r1, r1, #8
	bcs	.L517
	ldrb	ip, [r3, #1]	@ zero_extendqisi2
	ldrb	sl, [r1, #3]	@ zero_extendqisi2
	cmp	ip, sl
	mov	lr, r3
	add	r3, r2, #9
	bne	.L517
	ldrb	ip, [lr, #2]	@ zero_extendqisi2
	ldrb	r8, [r1, #4]	@ zero_extendqisi2
	cmp	ip, r8
	add	r3, lr, #2
	bne	.L517
	ldrb	r3, [r1, #5]	@ zero_extendqisi2
	ldrb	sl, [lr, #3]	@ zero_extendqisi2
	cmp	sl, r3
	add	r3, lr, #3
	bne	.L517
	ldrb	ip, [lr, #4]	@ zero_extendqisi2
	ldrb	r8, [r1, #6]	@ zero_extendqisi2
	cmp	ip, r8
	add	r3, lr, #4
	bne	.L517
	ldrb	r3, [r1, #7]	@ zero_extendqisi2
	ldrb	sl, [lr, #5]	@ zero_extendqisi2
	cmp	sl, r3
	add	r3, lr, #5
	bne	.L517
	ldrb	ip, [lr, #6]	@ zero_extendqisi2
	ldrb	r8, [r1, #8]	@ zero_extendqisi2
	cmp	ip, r8
	add	r3, lr, #6
	bne	.L517
	ldrb	r3, [r1, #9]	@ zero_extendqisi2
	ldrb	sl, [lr, #7]	@ zero_extendqisi2
	cmp	sl, r3
	add	r3, lr, #7
	bne	.L517
	ldrb	ip, [lr, #8]	@ zero_extendqisi2
	ldrb	r8, [r1, #10]	@ zero_extendqisi2
	cmp	ip, r8
	add	r3, lr, #8
	bne	.L517
	add	r1, r1, #8
	add	r2, lr, #8
	b	.L518
.L546:
	ldr	fp, [sp, #4]
	ldrb	ip, [lr, #3]	@ zero_extendqisi2
	ldrb	fp, [fp, #1]	@ zero_extendqisi2
	cmp	fp, ip
	ldr	ip, [sp, #20]
	add	fp, ip, #3
	str	fp, [sp, #64]
	bne	.L509
	ldr	fp, [sp, #4]
	ldrb	ip, [fp, #2]	@ zero_extendqisi2
	ldrb	fp, [lr, #4]	@ zero_extendqisi2
	cmp	ip, fp
	ldr	fp, [sp, #4]
	add	ip, fp, #2
	str	ip, [sp, #64]
	bne	.L509
	ldr	fp, [sp, #4]
	ldrb	ip, [lr, #5]	@ zero_extendqisi2
	ldrb	fp, [fp, #3]	@ zero_extendqisi2
	cmp	fp, ip
	ldr	fp, [sp, #20]
	add	ip, fp, #5
	str	ip, [sp, #64]
	bne	.L509
	ldr	fp, [sp, #4]
	ldrb	ip, [fp, #4]	@ zero_extendqisi2
	ldrb	fp, [lr, #6]	@ zero_extendqisi2
	cmp	ip, fp
	ldr	fp, [sp, #20]
	add	ip, fp, #6
	str	ip, [sp, #64]
	bne	.L509
	ldr	fp, [sp, #4]
	ldrb	ip, [lr, #7]	@ zero_extendqisi2
	ldrb	fp, [fp, #5]	@ zero_extendqisi2
	cmp	fp, ip
	ldr	fp, [sp, #20]
	add	ip, fp, #7
	str	ip, [sp, #64]
	bne	.L509
	ldr	fp, [sp, #4]
	ldrb	ip, [fp, #6]	@ zero_extendqisi2
	ldrb	fp, [lr, #8]	@ zero_extendqisi2
	cmp	ip, fp
	ldr	fp, [sp, #4]
	add	ip, fp, #6
	str	ip, [sp, #64]
	bne	.L509
	ldr	fp, [sp, #4]
	ldrb	ip, [lr, #9]	@ zero_extendqisi2
	ldrb	fp, [fp, #7]	@ zero_extendqisi2
	cmp	fp, ip
	ldr	fp, [sp, #4]
	add	ip, fp, #7
	str	ip, [sp, #64]
	bne	.L509
	ldr	fp, [sp, #4]
	ldrb	ip, [fp, #8]	@ zero_extendqisi2
	ldrb	fp, [lr, #10]	@ zero_extendqisi2
	cmp	ip, fp
	ldr	ip, [sp, #4]
	add	ip, ip, #8
	str	ip, [sp, #64]
	bne	.L509
	ldr	fp, [sp, #24]
	cmp	fp, ip
	add	ip, lr, #8
	bls	.L509
	ldr	lr, [sp, #64]
	str	r2, [sp, #20]
	str	lr, [sp, #4]
	mov	fp, lr
	str	r5, [sp, #64]
	mov	lr, ip
	b	.L510
.L517:
	rsb	r2, r0, #256
	add	r0, r2, #2
	add	r3, r0, r3
	cmp	r3, #2
	ble	.L516
	ldr	r8, [r4, #116]
	str	r5, [r4, #112]
	cmp	r3, r8
	movcc	r8, r3
	b	.L519
.L592:
	add	r5, sp, #44
	ldmia	r5, {r5, r6, r7, r9}	@ phole ldm
	mov	r8, ip
	b	.L512
.L597:
	.align	2
.L596:
	.word	_length_code
	.word	_dist_code
	.size	deflate_fast, .-deflate_fast
	.hidden	deflate_copyright
	.global	deflate_copyright
	.section	.rodata
	.align	2
	.set	.LANCHOR0,. + 0
	.type	configuration_table, %object
	.size	configuration_table, 120
configuration_table:
	.short	0
	.short	0
	.short	0
	.short	0
	.word	deflate_stored
	.short	4
	.short	4
	.short	8
	.short	4
	.word	deflate_fast
	.short	4
	.short	5
	.short	16
	.short	8
	.word	deflate_fast
	.short	4
	.short	6
	.short	32
	.short	32
	.word	deflate_fast
	.short	4
	.short	4
	.short	16
	.short	16
	.word	deflate_slow
	.short	8
	.short	16
	.short	32
	.short	32
	.word	deflate_slow
	.short	8
	.short	16
	.short	128
	.short	128
	.word	deflate_slow
	.short	8
	.short	32
	.short	128
	.short	256
	.word	deflate_slow
	.short	32
	.short	128
	.short	258
	.short	1024
	.word	deflate_slow
	.short	32
	.short	258
	.short	258
	.short	4096
	.word	deflate_slow
	.type	deflate_copyright, %object
	.size	deflate_copyright, 53
deflate_copyright:
	.ascii	" deflate 1.2.3 Copyright 1995-2005 Jean-loup Gailly"
	.ascii	" \000"
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
