	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"IwSoundADPCM.cpp"
	.section	.text._ZN15CIwChannelADPCM4InitEv,"ax",%progbits
	.align	2
	.global	_ZN15CIwChannelADPCM4InitEv
	.hidden	_ZN15CIwChannelADPCM4InitEv
	.type	_ZN15CIwChannelADPCM4InitEv, %function
_ZN15CIwChannelADPCM4InitEv:
	.fnstart
.LFB1370:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, .L8
	ldr	ip, [r3, #0]
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
	mov	r0, ip, asl #1
	add	fp, r0, ip
	mov	r9, ip, asl #2
	ldr	r8, .L8+4
	ldr	r3, .L8+8
	mov	r2, ip, asr #3
	add	r7, r9, ip
	rsb	r5, ip, ip, asl #3
	mov	r6, fp, asl #1
	mov	sl, #1
	add	r5, r2, r5, asr #2
	add	r1, r2, ip, asr #2
	add	r4, r2, r0, asr #2
	add	r7, r2, r7, asr #2
	add	r6, r2, r6, asr #2
	add	ip, r2, fp, asr #2
	add	r0, r2, r9, asr #2
	str	sl, [r8, #0]
	str	r5, [r3, #92]
	str	r1, [r3, #68]
	str	r4, [r3, #72]
	str	ip, [r3, #76]
	str	r0, [r3, #80]
	str	r7, [r3, #84]
	str	r6, [r3, #88]
	str	r2, [r3, #64]
	sub	sp, sp, #32
	add	r3, r3, #32
	mov	r5, #4
.L2:
	ldr	r6, .L8
	add	r1, r5, #4
	ldr	r4, [r6, r5]
	ldr	ip, [r6, r1]
	mov	r9, r4, asl #1
	add	sl, r9, r4
	mov	fp, ip, asl #2
	mov	r7, ip, asl #1
	mov	r8, r4, asl #2
	add	r6, r7, ip
	add	r5, r8, r4
	add	r0, fp, ip
	mov	r2, sl, asl #1
	str	r5, [sp, #12]
	str	r0, [sp, #20]
	str	r2, [sp, #16]
	mov	r0, r4, asr #3
	mov	r5, r6, asl #1
	rsb	r2, r4, r4, asl #3
	str	r5, [sp, #24]
	str	fp, [sp, #4]
	add	r2, r0, r2, asr #2
	rsb	fp, ip, ip, asl #3
	add	r5, r1, #4
	mov	r1, ip, asr #3
	str	r2, [r3, #92]
	add	r2, r1, fp, asr #2
	ldr	fp, [sp, #12]
	add	fp, r0, fp, asr #2
	str	fp, [r3, #84]
	ldr	fp, [sp, #16]
	add	fp, r0, fp, asr #2
	str	fp, [r3, #88]
	ldr	fp, [sp, #20]
	add	fp, r1, fp, asr #2
	str	fp, [sp, #20]
	ldr	fp, [sp, #24]
	add	fp, r1, fp, asr #2
	str	fp, [sp, #24]
	add	r4, r0, r4, asr #2
	add	fp, r1, ip, asr #2
	ldr	ip, [sp, #4]
	str	r4, [r3, #68]
	ldr	r4, [sp, #20]
	str	r2, [sp, #28]
	str	fp, [sp, #12]
	add	r2, r3, #32
	add	r9, r0, r9, asr #2
	add	sl, r0, sl, asr #2
	add	r8, r0, r8, asr #2
	add	fp, r1, ip, asr #2
	str	r0, [r3, #64]
	str	r9, [r3, #72]
	str	sl, [r3, #76]
	str	r8, [r3, #80]
	ldr	r0, [sp, #12]
	str	fp, [r2, #80]
	str	r4, [r2, #84]
	add	r4, sp, #24
	ldmia	r4, {r4, fp}	@ phole ldm
	add	r7, r1, r7, asr #2
	add	r6, r1, r6, asr #2
	cmp	r5, #356
	str	r0, [r2, #68]
	str	r7, [r2, #72]
	str	r6, [r2, #76]
	str	r4, [r2, #88]
	add	r3, r2, #32
	str	fp, [r2, #92]
	str	r1, [r2, #64]
	bne	.L2
	add	sp, sp, #32
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
	bx	lr
.L9:
	.align	2
.L8:
	.word	.LANCHOR2
	.word	.LANCHOR0
	.word	.LANCHOR1
	.cantunwind
	.fnend
	.size	_ZN15CIwChannelADPCM4InitEv, .-_ZN15CIwChannelADPCM4InitEv
	.section	.text._ZN15CIwChannelADPCM22GenerateADPCMAudioFastEPsiiiiPi,"ax",%progbits
	.align	2
	.global	_ZN15CIwChannelADPCM22GenerateADPCMAudioFastEPsiiiiPi
	.hidden	_ZN15CIwChannelADPCM22GenerateADPCMAudioFastEPsiiiiPi
	.type	_ZN15CIwChannelADPCM22GenerateADPCMAudioFastEPsiiiiPi, %function
_ZN15CIwChannelADPCM22GenerateADPCMAudioFastEPsiiiiPi:
	.fnstart
.LFB1371:
	@ Function supports interworking.
	@ args = 12, pretend = 0, frame = 72
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
	ldrh	r7, [r0, #16]
	sub	sp, sp, #72
	ldr	sl, [r0, #0]
	ldr	r6, [r0, #8]
	ldr	r4, [r0, #12]
	str	r7, [sp, #20]
	ldrb	r9, [r0, #23]	@ zero_extendqisi2
	cmp	r2, #0
	ldrh	r7, [r0, #20]
	ldrb	r8, [r0, #22]	@ zero_extendqisi2
	ldr	ip, [sp, #104]
	ldr	r5, [sp, #108]
	str	r9, [sp, #32]
	beq	.L11
	mov	fp, r8, asl #24
	mov	r8, fp, asr #24
	cmp	ip, #7
	str	r8, [sp, #24]
	bgt	.L12
	cmp	r5, #0
	bne	.L13
	and	r9, r1, #3
	mov	fp, r9, lsr #1
	cmp	fp, r2
	movcs	fp, r2
	cmp	fp, #0
	sub	r8, r2, #1
	moveq	ip, r1
	beq	.L15
	sub	r9, fp, #1
	and	r9, r9, #3
	str	r9, [sp, #12]
	mov	ip, r1
	mov	r9, #1
	cmp	fp, #1
	strh	r5, [ip], #2	@ movhi
	sub	r8, r8, #1
	str	r9, [sp, #16]
	bls	.L234
	ldr	r9, [sp, #12]
	cmp	r9, #0
	beq	.L257
	cmp	r9, #1
	beq	.L226
	cmp	r9, #2
	strneh	r5, [ip], #2	@ movhi
	movne	r5, #2
	strne	r5, [sp, #16]
	mov	r9, #0	@ movhi
	strh	r9, [ip], #2	@ movhi
	ldr	r5, [sp, #16]
	add	r9, r5, #1
	subne	r8, r8, #1
	str	r9, [sp, #16]
	sub	r8, r8, #1
.L226:
	ldr	r9, [sp, #16]
	add	r9, r9, #1
	mov	r5, #0	@ movhi
	cmp	fp, r9
	str	r9, [sp, #16]
	sub	r8, r8, #1
	strh	r5, [ip], #2	@ movhi
	bls	.L234
.L257:
	str	r3, [sp, #12]
	ldr	r9, [sp, #16]
.L16:
	mov	r3, #0	@ movhi
	mov	r5, ip
	strh	r3, [r5], #2	@ movhi
	add	r9, r9, #4
	cmp	fp, r9
	strh	r3, [ip, #2]	@ movhi
	sub	r8, r8, #4
	strh	r3, [r5, #2]	@ movhi
	strh	r3, [ip, #6]	@ movhi
	add	ip, ip, #8
	bhi	.L16
	ldr	r3, [sp, #12]
.L234:
	cmp	fp, r2
	beq	.L13
.L15:
	rsb	r5, fp, r2
	mov	r9, r5, lsr #1
	str	r5, [sp, #36]
	movs	r5, r9, asl #1
	str	r9, [sp, #12]
	str	r5, [sp, #28]
	beq	.L18
	add	fp, r1, fp, asl #1
	str	fp, [sp, #16]
	sub	r5, r9, #1
	cmp	r9, #1
	and	r9, r5, #3
	ldr	r5, [sp, #16]
	mov	fp, #0
	str	fp, [r5, #0]
	mov	r5, #1
	str	r5, [sp, #52]
	bls	.L235
	cmp	r9, #0
	beq	.L255
	cmp	r9, #1
	beq	.L224
	cmp	r9, #2
	ldrne	r5, [sp, #16]
	movne	r9, #2
	strne	fp, [r5, #4]
	strne	r9, [sp, #52]
	ldr	r5, [sp, #52]
	ldr	r9, [sp, #16]
	mov	r5, r5, asl #2
	str	r5, [sp, #40]
	str	fp, [r9, r5]
	ldr	r5, [sp, #52]
	add	r9, r5, #1
	str	r9, [sp, #52]
.L224:
	ldr	r5, [sp, #52]
	mov	r9, r5, asl #2
	ldr	r5, [sp, #52]
	str	r9, [sp, #40]
	ldr	r9, [sp, #12]
	add	r5, r5, #1
	str	r5, [sp, #52]
	cmp	r9, r5
	ldr	r9, [sp, #16]
	ldr	r5, [sp, #40]
	str	fp, [r9, r5]
	bls	.L235
.L255:
	str	ip, [sp, #40]
	str	r6, [sp, #44]
	ldr	ip, [sp, #16]
	str	r7, [sp, #48]
	str	r3, [sp, #16]
	ldr	r5, [sp, #52]
	mov	r9, r8
.L19:
	str	fp, [ip, r5, asl #2]
	ldr	r3, [sp, #12]
	add	r6, r5, #1
	add	r7, r5, #3
	add	r5, r5, #4
	add	r8, r6, #1
	cmp	r3, r5
	str	fp, [ip, r6, asl #2]
	str	fp, [ip, r8, asl #2]
	str	fp, [ip, r7, asl #2]
	bhi	.L19
	ldr	ip, [sp, #40]
	add	r6, sp, #44
	ldmia	r6, {r6, r7}	@ phole ldm
	ldr	r3, [sp, #16]
	mov	r8, r9
.L235:
	ldr	r9, [sp, #28]
	ldr	r5, [sp, #36]
	cmp	r5, r9
	add	ip, ip, r9, asl #1
	rsb	r8, r9, r8
	beq	.L13
.L18:
	sub	r9, r8, #1
	mov	fp, #0	@ movhi
	cmn	r9, #1
	strh	fp, [ip, #0]	@ movhi
	and	r8, r8, #3
	mov	r5, #2
	beq	.L13
	cmp	r8, #0
	beq	.L253
	cmp	r8, #1
	beq	.L222
	cmp	r8, #2
	movne	r5, #0	@ movhi
	strneh	r5, [ip, #2]	@ movhi
	subne	r9, r9, #1
	movne	r5, #4
	mov	r8, #0	@ movhi
	strh	r8, [ip, r5]	@ movhi
	sub	r9, r9, #1
	add	r5, r5, #2
.L222:
	sub	r9, r9, #1
	mov	fp, #0	@ movhi
	cmn	r9, #1
	strh	fp, [ip, r5]	@ movhi
	add	r5, r5, #2
	beq	.L13
.L253:
	str	r6, [sp, #12]
	str	r3, [sp, #16]
	mov	fp, r7
.L20:
	add	r6, r5, #2
	sub	r9, r9, #4
	mov	r3, #0	@ movhi
	add	r7, r5, #6
	add	r8, r6, #2
	cmn	r9, #1
	strh	r3, [ip, r5]	@ movhi
	strh	r3, [ip, r6]	@ movhi
	add	r5, r5, #8
	strh	r3, [ip, r8]	@ movhi
	strh	r3, [ip, r7]	@ movhi
	bne	.L20
	ldr	r6, [sp, #12]
	ldr	r3, [sp, #16]
	mov	r7, fp
.L13:
	add	r5, r1, r2, asl #1
	rsb	ip, r3, #0
	mov	r8, r6
.L236:
	tst	r4, #1
	beq	.L23
.L24:
	cmp	r8, r3
	blt	.L27
	subs	r2, r2, #1
	rsb	r8, r3, r8
	beq	.L29
	sub	r6, r2, #1
	cmp	r3, r8
	and	r9, r6, #3
	bgt	.L27
	subs	r2, r6, #0
	add	r8, r8, ip
	beq	.L29
	cmp	r9, #0
	beq	.L219
	cmp	r9, #1
	beq	.L220
	cmp	r9, #2
	beq	.L221
	cmp	r3, r8
	bgt	.L27
	add	r8, r8, ip
	sub	r2, r2, #1
.L221:
	cmp	r3, r8
	bgt	.L27
	add	r8, r8, ip
	sub	r2, r2, #1
.L220:
	cmp	r3, r8
	bgt	.L27
	subs	r2, r2, #1
	add	r8, r8, ip
	beq	.L29
.L219:
	cmp	r3, r8
	bgt	.L27
	add	r8, r8, ip
	sub	r2, r2, #1
	cmp	r3, r8
	mov	r6, r2
	bgt	.L27
	add	r8, r8, ip
	cmp	r3, r8
	sub	r2, r2, #1
	bgt	.L27
	add	r8, r8, ip
	cmp	r3, r8
	sub	r2, r6, #2
	bgt	.L27
	subs	r2, r6, #3
	add	r8, r8, ip
	bne	.L219
.L29:
	ldr	fp, [sp, #24]
	str	sl, [r0, #0]
	strb	fp, [r0, #22]
	str	r8, [r0, #8]
	str	r4, [r0, #12]
	ldr	r3, [sp, #20]
	strh	r7, [r0, #20]	@ movhi
	strh	r3, [r0, #16]	@ movhi
	ldr	ip, [sp, #32]
	rsb	r5, r1, r5
	strb	ip, [r0, #23]
	mov	r5, r5, asr #1
.L11:
	mov	r0, r5
	add	sp, sp, #72
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
	bx	lr
.L12:
	cmp	r5, #0
	bne	.L272
	mov	fp, #-2147483648
	mov	r9, fp, asr #16
	mov	r5, #32512
	str	r9, [sp, #16]
	rsb	fp, r3, #0
	add	r9, r5, #255
	str	r9, [sp, #28]
	str	fp, [sp, #12]
	str	r1, [sp, #44]
	mov	r8, r6
	mov	r5, r1
.L90:
	cmp	r4, #0
	beq	.L66
	tst	r4, #1
	ldreq	r6, .L279
	beq	.L84
	ldrb	r1, [sl, #0]	@ zero_extendqisi2
	str	r1, [sp, #36]
.L68:
	mov	fp, r7, asl #16
	cmp	r3, r8
	str	fp, [sp, #40]
	mov	r1, fp, asr #16
	bgt	.L74
	ldr	r9, [sp, #20]
	mov	r7, r9, asl #16
	rsb	r8, r3, r8
	rsb	r7, r1, r7, asr #16
	mul	fp, r8, r7
	add	r6, r1, fp, asr #12
	mul	r6, ip, r6
	subs	r2, r2, #1
	mov	r6, r6, asr #8
	strh	r6, [r5], #2	@ movhi
	beq	.L76
	ldr	fp, [sp, #12]
	rsb	r6, r3, r8
	sub	r9, r2, #1
	cmp	r3, r8
	mul	r6, r7, r6
	mul	r7, fp, r7
	and	fp, r9, #3
	bgt	.L74
	add	r2, r1, r6, asr #12
	mul	r2, ip, r2
	str	r2, [sp, #48]
	subs	r2, r9, #0
	ldr	r9, [sp, #48]
	mov	r9, r9, asr #8
	str	r9, [sp, #48]
	strh	r9, [r5], #2	@ movhi
	ldr	r9, [sp, #12]
	add	r6, r6, r7
	add	r8, r8, r9
	beq	.L265
	cmp	fp, #0
	beq	.L267
	cmp	fp, #1
	beq	.L232
	cmp	fp, #2
	beq	.L233
	cmp	r3, r8
	bgt	.L74
	add	fp, r1, r6, asr #12
	mul	fp, ip, fp
	mov	r9, fp, asr #8
	strh	r9, [r5], #2	@ movhi
	ldr	fp, [sp, #12]
	sub	r2, r2, #1
	add	r8, r8, fp
	add	r6, r6, r7
.L233:
	cmp	r3, r8
	bgt	.L74
	add	r9, r1, r6, asr #12
	mul	r9, ip, r9
	mov	fp, r9, asr #8
	strh	fp, [r5], #2	@ movhi
	ldr	r9, [sp, #12]
	sub	r2, r2, #1
	add	r8, r8, r9
	add	r6, r6, r7
.L232:
	cmp	r3, r8
	bgt	.L74
	add	r9, r1, r6, asr #12
	mul	r9, ip, r9
	mov	r9, r9, asr #8
	strh	r9, [r5], #2	@ movhi
	ldr	fp, [sp, #12]
	subs	r2, r2, #1
	add	r8, r8, fp
	add	r6, r6, r7
	beq	.L265
.L267:
	str	r4, [sp, #56]
	str	r0, [sp, #60]
	mov	fp, sl
.L72:
	cmp	r3, r8
	add	r4, r1, r6, asr #12
	bgt	.L269
	mul	r4, ip, r4
	mov	r4, r4, asr #8
	strh	r4, [r5], #2	@ movhi
	add	sl, r6, r7
	ldr	r9, [sp, #12]
	add	r6, sl, r7
	add	r8, r8, r9
	add	r9, r1, sl, asr #12
	add	sl, r6, r7
	add	r4, r1, sl, asr #12
	add	r0, r1, r6, asr #12
	sub	r2, r2, #1
	cmp	r3, r8
	str	r4, [sp, #52]
	add	r6, sl, r7
	str	r0, [sp, #48]
	mov	r4, r5
	mov	sl, r2
	bgt	.L269
	mul	r9, ip, r9
	ldr	r0, [sp, #12]
	add	r8, r8, r0
	cmp	r3, r8
	mov	r0, r9, asr #8
	strh	r0, [r5], #2	@ movhi
	sub	r2, r2, #1
	bgt	.L269
	ldr	r0, [sp, #48]
	mul	r0, ip, r0
	ldr	r9, [sp, #12]
	add	r8, r8, r9
	mov	r2, r0, asr #8
	cmp	r3, r8
	strh	r2, [r5, #0]	@ movhi
	sub	r2, sl, #2
	add	r5, r4, #4
	bgt	.L269
	ldr	r0, [sp, #52]
	mul	r0, ip, r0
	mov	r5, r0, asr #8
	strh	r5, [r4, #4]	@ movhi
	subs	r2, sl, #3
	ldr	sl, [sp, #12]
	add	r5, r4, #6
	add	r8, r8, sl
	bne	.L72
	ldr	r4, [sp, #56]
	ldr	r0, [sp, #60]
	ldr	r1, [sp, #44]
	mov	sl, fp
.L241:
	ldr	r6, [sp, #40]
	mov	r7, r6, lsr #16
	b	.L29
.L27:
	add	r8, r8, #4096
	cmp	r3, r8
	sub	r4, r4, #1
	add	sl, sl, #1
	ble	.L273
.L23:
	cmp	r4, #0
	subne	r4, r4, #1
	addne	r8, r8, #4096
	bne	.L24
	ldr	r6, [r0, #4]
	cmp	sl, r6
	bcs	.L274
	ldr	r9, [r0, #36]
	ldr	r4, [r0, #28]
	cmp	r9, #0
	addne	sl, r9, r4
	str	sl, [r0, #36]
	add	r4, sl, r4
	ldrb	r9, [sl, #2]	@ zero_extendqisi2
	cmp	r4, r6
	ldr	r7, [r0, #24]
	ldr	fp, [r0, #32]
	subhi	r4, r6, #4
	str	r9, [sp, #24]
	rsbhi	r4, sl, r4
	rsb	r6, r7, fp
	mov	r9, #0
	subls	r4, r7, #1
	movhi	r4, r4, asl #1
	ldrh	r7, [sl, #0]
	str	r6, [r0, #32]
	add	sl, sl, #4
	str	r9, [sp, #32]
	b	.L236
.L272:
	mov	fp, #-2147483648
	mov	r8, #32512
	add	r5, r8, #255
	mov	r9, fp, asr #16
	rsb	r8, r3, #0
	rsb	fp, r3, #4096
	str	r9, [sp, #16]
	str	r8, [sp, #12]
	str	fp, [sp, #68]
	str	r1, [sp, #48]
	str	r5, [sp, #28]
	mov	r5, r1
.L64:
	cmp	r4, #0
	moveq	r8, r6
	beq	.L40
	tst	r4, #1
	beq	.L41
	ldrb	fp, [sl, #0]	@ zero_extendqisi2
	str	fp, [sp, #44]
.L42:
	mov	r1, r7, asl #16
	cmp	r3, r6
	str	r1, [sp, #52]
	movgt	fp, r1, asr #16
	bgt	.L49
	ldr	fp, [sp, #20]
	ldr	r8, [sp, #52]
	mov	r9, fp, asl #16
	mov	fp, r8, asr #16
	rsb	r8, r3, r6
	ldr	r6, [sp, #12]
	rsb	r7, fp, r9, asr #16
	mul	r6, r7, r6
	sub	r1, r2, #1
	tst	r1, #1
	str	r1, [sp, #40]
	str	r6, [sp, #36]
	mul	r1, r8, r7
	bne	.L237
.L259:
	str	r4, [sp, #56]
	str	sl, [sp, #60]
	str	r0, [sp, #64]
.L50:
	add	r0, fp, r1, asr #12
	mul	r0, ip, r0
	ldrsh	r6, [r5, #0]
	add	r0, r6, r0, asr #8
	add	r4, r0, #32768
	mov	sl, r4, lsr #16
	mov	r9, sl, asl #16
	ldr	r7, [sp, #12]
	cmp	r9, #0
	ldr	r9, [sp, #36]
	add	r4, r8, r7
	add	r7, r1, r9
	add	r6, fp, r7, asr #12
	sub	r2, r2, #1
	str	r6, [sp, #4]
	add	r1, r7, r9
	add	sl, r4, r3
	mov	r6, r8
	mov	r7, r2
	beq	.L46
	ldr	r9, [sp, #16]
	cmp	r0, r9
	movlt	r0, r9
	ldr	r9, [sp, #28]
	cmp	r0, r9
	movge	r0, r9
.L46:
	strh	r0, [r5], #2	@ movhi
	cmp	r7, #0
	mov	r0, r5
	beq	.L275
	ldr	r9, [sp, #12]
	add	r8, r4, r9
	add	r9, r8, r3
	cmp	r3, sl
	str	r9, [sp, #40]
	bgt	.L270
	ldr	r6, [sp, #4]
	mul	r6, ip, r6
	ldrsh	sl, [r5, #0]
	add	r9, sl, r6, asr #8
	add	r2, r9, #32768
	mov	r5, r2, lsr #16
	mov	sl, r5, asl #16
	cmp	sl, #0
	mov	r5, r0
	sub	r2, r7, #1
	mov	r6, r4
	beq	.L174
	ldr	r0, [sp, #16]
	ldr	r4, [sp, #28]
	cmp	r9, r0
	movlt	r9, r0
	cmp	r9, r4
	movge	r9, r4
.L174:
	ldr	r7, [sp, #40]
	cmp	r3, r7
	strh	r9, [r5], #2	@ movhi
	ble	.L50
.L270:
	add	r4, sp, #56
	ldmia	r4, {r4, sl}	@ phole ldm
	ldr	r0, [sp, #64]
.L49:
	ldr	r8, [sp, #44]
	ldr	r9, [sp, #24]
	mov	r1, r8, asr #4
	and	r8, r1, #7
	tst	r1, #8
	add	r7, r8, r9, asl #3
	ldr	r1, .L279
	add	r7, r7, #16
	ldr	r9, [r1, r7, asl #2]
	ldr	r7, [r1, r8, asl #2]
	rsbne	r9, r9, fp
	addeq	r9, fp, r9
	ldr	r8, [sp, #24]
	str	r1, [sp, #20]
	add	r1, r9, #32768
	add	r7, r7, r8
	mov	r1, r1, lsr #16
	cmp	r7, #88
	movge	r7, #88
	mov	r1, r1, asl #16
	bic	r8, r7, r7, asr #31
	cmp	r1, #0
	sub	r4, r4, #1
	add	sl, sl, #1
	str	r8, [sp, #24]
	beq	.L53
	ldr	r1, [sp, #16]
	ldr	r7, [sp, #28]
	cmp	r9, r1
	movlt	r9, r1
	cmp	r9, r7
	movge	r9, r7
.L53:
	add	r8, r6, #4096
	mov	r7, r9, asl #16
	cmp	r3, r8
	mov	r7, r7, lsr #16
	bgt	.L54
	mov	r8, r7, asl #16
	mov	r9, r8, asr #16
	rsb	fp, r9, fp
	str	r9, [sp, #36]
	ldr	r9, [sp, #12]
	mul	r9, fp, r9
	ldr	r1, [sp, #68]
	sub	r8, r2, #1
	add	r6, r1, r6
	tst	r8, #1
	mul	r1, r6, fp
	str	r9, [sp, #40]
	beq	.L258
	ldr	fp, [sp, #36]
	add	r2, fp, r1, asr #12
	mul	fp, ip, r2
	ldrsh	r9, [r5, #0]
	add	fp, r9, fp, asr #8
	add	r2, fp, #32768
	mov	r9, r2, lsr #16
	mov	r2, r9, asl #16
	cmp	r2, #0
	mov	r2, r8
	mov	r8, r6
	beq	.L163
	ldr	r9, [sp, #16]
	cmp	r9, fp
	movlt	r9, fp
	ldr	fp, [sp, #28]
	cmp	fp, r9
	movge	fp, r9
.L163:
	ldr	r9, [sp, #12]
	strh	fp, [r5], #2	@ movhi
	add	r6, r6, r9
	add	r9, r6, r3
	ldr	fp, [sp, #40]
	cmp	r3, r9
	add	r1, r1, fp
	bgt	.L54
.L258:
	str	r7, [sp, #44]
	str	r4, [sp, #56]
	str	sl, [sp, #60]
	str	r0, [sp, #64]
.L57:
	ldr	fp, [sp, #36]
	add	r9, fp, r1, asr #12
	mul	r9, ip, r9
	ldrsh	r0, [r5, #0]
	add	r0, r0, r9, asr #8
	ldr	r9, [sp, #40]
	ldr	r8, [sp, #12]
	add	r4, r0, #32768
	add	r1, r1, r9
	mov	sl, r4, lsr #16
	add	r9, fp, r1, asr #12
	add	r4, r6, r8
	ldr	fp, [sp, #12]
	ldr	r8, [sp, #40]
	mov	r7, sl, asl #16
	add	r1, r1, r8
	mov	r8, r6
	add	r6, r4, fp
	cmp	r7, #0
	sub	r2, r2, #1
	add	r7, r6, r3
	str	r7, [sp, #4]
	add	sl, r4, r3
	mov	r7, r2
	beq	.L55
	ldr	fp, [sp, #16]
	cmp	r0, fp
	movlt	r0, fp
	ldr	fp, [sp, #28]
	cmp	r0, fp
	movge	r0, fp
.L55:
	strh	r0, [r5], #2	@ movhi
	cmp	r7, #0
	mov	r0, r5
	beq	.L276
	cmp	r3, sl
	bgt	.L271
	mul	r9, ip, r9
	ldrsh	r2, [r5, #0]
	add	r9, r2, r9, asr #8
	add	r5, r9, #32768
	mov	r8, r5, lsr #16
	mov	sl, r8, asl #16
	cmp	sl, #0
	mov	r5, r0
	sub	r2, r7, #1
	mov	r8, r4
	beq	.L167
	ldr	r0, [sp, #16]
	ldr	r4, [sp, #28]
	cmp	r9, r0
	movlt	r9, r0
	cmp	r9, r4
	movge	r9, r4
.L167:
	ldr	r7, [sp, #4]
	cmp	r3, r7
	strh	r9, [r5], #2	@ movhi
	ble	.L57
.L271:
	ldr	r7, [sp, #44]
	add	r4, sp, #56
	ldmia	r4, {r4, sl}	@ phole ldm
	ldr	r0, [sp, #64]
.L54:
	cmp	r4, #0
	bgt	.L58
	ldr	r6, [sp, #52]
	mov	r6, r6, lsr #16
	str	r6, [sp, #20]
.L40:
	ldr	r1, [r0, #4]
	cmp	sl, r1
	bcs	.L277
	ldr	r4, [r0, #36]
	ldr	r7, [r0, #28]
	cmp	r4, #0
	addne	sl, r4, r7
	str	sl, [r0, #36]
	add	r9, sl, r7
	ldrb	r7, [sl, #2]	@ zero_extendqisi2
	cmp	r9, r1
	ldr	r6, [r0, #24]
	ldr	fp, [r0, #32]
	subhi	r4, r1, #4
	str	r7, [sp, #24]
	rsbhi	r4, sl, r4
	rsb	r9, r6, fp
	mov	r1, #0
	ldrh	r7, [sl, #0]
	subls	r4, r6, #1
	str	r9, [r0, #32]
	movhi	r4, r4, asl #1
	mov	r6, r8
	add	sl, sl, #4
	str	r1, [sp, #32]
	b	.L64
.L237:
	add	r6, fp, r1, asr #12
	mul	r6, ip, r6
	ldrsh	r9, [r5, #0]
	add	r9, r9, r6, asr #8
	add	r7, r9, #32768
	mov	r2, r7, lsr #16
	mov	r6, r2, asl #16
	cmp	r6, #0
	ldr	r2, [sp, #40]
	mov	r6, r8
	beq	.L170
	ldr	r7, [sp, #16]
	cmp	r9, r7
	movlt	r9, r7
	ldr	r7, [sp, #28]
	cmp	r9, r7
	movge	r9, r7
.L170:
	ldr	r7, [sp, #12]
	strh	r9, [r5], #2	@ movhi
	add	r8, r8, r7
	add	r7, r8, r3
	ldr	r9, [sp, #36]
	cmp	r3, r7
	add	r1, r1, r9
	ble	.L259
	b	.L49
.L264:
	ldr	r7, [sp, #48]
	ldr	r4, [sp, #52]
	ldr	r0, [sp, #60]
	ldr	r6, [sp, #56]
	mov	sl, r9
.L80:
	cmp	r4, #0
	bgt	.L84
	ldr	r1, [sp, #40]
	mov	fp, r1, lsr #16
	str	fp, [sp, #20]
.L66:
	ldr	r1, [r0, #4]
	cmp	sl, r1
	bcs	.L278
	ldr	r4, [r0, #36]
	ldr	r9, [r0, #28]
	cmp	r4, #0
	addne	sl, r4, r9
	str	sl, [r0, #36]
	ldr	r6, [r0, #24]
	ldr	r7, [r0, #32]
	add	fp, sl, r9
	cmp	fp, r1
	rsb	r9, r6, r7
	subls	r4, r6, #1
	ldrb	r6, [sl, #2]	@ zero_extendqisi2
	str	r6, [sp, #24]
	subhi	r4, r1, #4
	ldrh	r7, [sl, #0]
	rsbhi	r4, sl, r4
	str	r9, [r0, #32]
	mov	r9, #0
	movhi	r4, r4, asl #1
	add	sl, sl, #4
	str	r9, [sp, #32]
	b	.L90
.L84:
	ldrb	r9, [sl, #0]	@ zero_extendqisi2
	ldr	fp, [sp, #24]
	str	r9, [sp, #36]
	and	r9, r9, #7
	add	r1, r9, fp, asl #3
	add	r1, r1, #16
	ldr	fp, [sp, #36]
	ldr	r1, [r6, r1, asl #2]
	tst	fp, #8
	mov	fp, r7, asl #16
	rsbne	r1, r1, fp, asr #16
	addeq	r1, r1, fp, asr #16
	ldr	r9, [r6, r9, asl #2]
	add	r6, r1, #32768
	mov	r7, r6, lsr #16
	mov	r6, r7, asl #16
	cmp	r6, #0
	beq	.L71
	ldr	r6, [sp, #16]
	ldr	r7, [sp, #28]
	cmp	r1, r6
	movlt	r1, r6
	cmp	r1, r7
	movge	r1, r7
.L71:
	ldr	r6, [sp, #24]
	add	r9, r9, r6
	cmp	r9, #88
	movge	r9, #88
	mov	r7, r1, asl #16
	bic	r9, r9, r9, asr #31
	mov	r1, fp, lsr #16
	sub	r4, r4, #1
	str	r9, [sp, #24]
	mov	r7, r7, lsr #16
	add	r8, r8, #4096
	str	r1, [sp, #20]
	b	.L68
.L41:
	ldr	r1, .L279
	str	r1, [sp, #20]
	mov	r8, r6
.L58:
	ldrb	r6, [sl, #0]	@ zero_extendqisi2
	ldr	r9, [sp, #24]
	str	r6, [sp, #44]
	and	r6, r6, #7
	add	r1, r6, r9, asl #3
	ldr	fp, [sp, #44]
	ldr	r9, [sp, #20]
	add	r1, r1, #16
	tst	fp, #8
	ldr	fp, [r9, r1, asl #2]
	mov	r1, r7, asl #16
	rsbne	r7, fp, r1, asr #16
	addeq	r7, fp, r1, asr #16
	ldr	r6, [r9, r6, asl #2]
	add	r9, r7, #32768
	mov	fp, r9, lsr #16
	mov	r9, fp, asl #16
	cmp	r9, #0
	beq	.L45
	ldr	r9, [sp, #16]
	ldr	fp, [sp, #28]
	cmp	r7, r9
	movlt	r7, r9
	cmp	r7, fp
	movge	r7, fp
.L45:
	ldr	fp, [sp, #24]
	add	r9, r6, fp
	cmp	r9, #88
	movge	r9, #88
	bic	r6, r9, r9, asr #31
	mov	r7, r7, asl #16
	str	r6, [sp, #24]
	add	r6, r8, #4096
	mov	r8, r1, lsr #16
	sub	r4, r4, #1
	mov	r7, r7, lsr #16
	str	r8, [sp, #20]
	b	.L42
.L275:
	ldr	ip, [sp, #52]
	add	r4, sp, #56
	ldmia	r4, {r4, sl}	@ phole ldm
	ldr	r0, [sp, #64]
	ldr	r1, [sp, #48]
	mov	r7, ip, lsr #16
	b	.L29
.L273:
	subs	r2, r2, #1
	rsb	r8, r3, r8
	beq	.L29
	sub	r6, r2, #1
	cmp	r3, r8
	and	r9, r6, #3
	bgt	.L23
	subs	r2, r6, #0
	add	r8, r8, ip
	beq	.L29
	cmp	r9, #0
	beq	.L92
	cmp	r9, #1
	beq	.L217
	cmp	r9, #2
	beq	.L218
	cmp	r3, r8
	bgt	.L23
	add	r8, r8, ip
	sub	r2, r2, #1
.L218:
	cmp	r3, r8
	bgt	.L23
	add	r8, r8, ip
	sub	r2, r2, #1
.L217:
	cmp	r3, r8
	bgt	.L23
	subs	r2, r2, #1
	add	r8, r8, ip
	beq	.L29
.L92:
	cmp	r3, r8
	bgt	.L23
	add	r8, r8, ip
	sub	r2, r2, #1
	cmp	r3, r8
	mov	r6, r2
	bgt	.L23
	add	r8, r8, ip
	cmp	r3, r8
	sub	r2, r2, #1
	bgt	.L23
	add	r8, r8, ip
	cmp	r3, r8
	sub	r2, r6, #2
	bgt	.L23
	subs	r2, r6, #3
	add	r8, r8, ip
	bne	.L92
	b	.L29
.L269:
	ldr	r4, [sp, #56]
	ldr	r0, [sp, #60]
	mov	sl, fp
.L74:
	ldr	r6, [sp, #36]
	ldr	fp, [sp, #24]
	mov	r7, r6, asr #4
	and	r6, r7, #7
	tst	r7, #8
	add	r9, r6, fp, asl #3
	ldr	r7, .L279
	add	r9, r9, #16
	ldr	fp, [r7, r9, asl #2]
	ldr	r6, [r7, r6, asl #2]
	rsbne	fp, fp, r1
	addeq	fp, r1, fp
	str	r6, [sp, #20]
	ldr	r9, [sp, #24]
	mov	r6, r7
	str	fp, [sp, #36]
	add	r7, fp, #32768
	ldr	fp, [sp, #20]
	add	fp, fp, r9
	mov	r7, r7, lsr #16
	mov	r9, fp
	cmp	r9, #88
	movge	r9, #88
	mov	r7, r7, asl #16
	bic	r9, r9, r9, asr #31
	cmp	r7, #0
	sub	r4, r4, #1
	add	sl, sl, #1
	str	fp, [sp, #20]
	str	r9, [sp, #24]
	beq	.L79
	ldr	fp, [sp, #36]
	ldr	r9, [sp, #16]
	cmp	fp, r9
	movlt	fp, r9
	ldr	r9, [sp, #28]
	cmp	r9, fp
	movge	r9, fp
	str	r9, [sp, #36]
.L79:
	ldr	fp, [sp, #36]
	add	r8, r8, #4096
	mov	r7, fp, asl #16
	cmp	r3, r8
	mov	r7, r7, lsr #16
	bgt	.L80
	mov	r9, r7, asl #16
	mov	fp, r9, asr #16
	rsb	r8, r3, r8
	rsb	r9, fp, r1
	mul	r1, r8, r9
	add	r1, fp, r1, asr #12
	mul	r1, ip, r1
	subs	r2, r2, #1
	mov	r1, r1, asr #8
	str	fp, [sp, #52]
	strh	r1, [r5], #2	@ movhi
	beq	.L81
	rsb	r1, r3, r8
	mul	r1, r9, r1
	str	r1, [sp, #36]
	ldr	r1, [sp, #12]
	mul	r1, r9, r1
	sub	fp, r2, #1
	and	r9, fp, #3
	cmp	r3, r8
	str	r1, [sp, #20]
	str	r9, [sp, #56]
	bgt	.L80
	ldr	r9, [sp, #52]
	ldr	r1, [sp, #36]
	add	r2, r9, r1, asr #12
	mul	r1, ip, r2
	mov	r9, r1, asr #8
	strh	r9, [r5], #2	@ movhi
	ldr	r1, [sp, #12]
	subs	r2, fp, #0
	ldr	r9, [sp, #20]
	ldr	fp, [sp, #36]
	add	r8, r8, r1
	add	r1, fp, r9
	beq	.L260
	ldr	fp, [sp, #56]
	cmp	fp, #0
	beq	.L262
	cmp	fp, #1
	beq	.L229
	cmp	fp, #2
	beq	.L230
	cmp	r3, r8
	bgt	.L80
	ldr	fp, [sp, #52]
	add	r9, fp, r1, asr #12
	mul	r9, ip, r9
	mov	r9, r9, asr #8
	strh	r9, [r5], #2	@ movhi
	ldr	r9, [sp, #12]
	ldr	fp, [sp, #20]
	add	r8, r8, r9
	sub	r2, r2, #1
	add	r1, r1, fp
.L230:
	cmp	r3, r8
	bgt	.L80
	ldr	fp, [sp, #52]
	add	r9, fp, r1, asr #12
	mul	r9, ip, r9
	mov	r9, r9, asr #8
	strh	r9, [r5], #2	@ movhi
	ldr	r9, [sp, #12]
	ldr	fp, [sp, #20]
	add	r8, r8, r9
	sub	r2, r2, #1
	add	r1, r1, fp
.L229:
	cmp	r3, r8
	bgt	.L80
	ldr	fp, [sp, #52]
	add	r9, fp, r1, asr #12
	mul	r9, ip, r9
	mov	r9, r9, asr #8
	strh	r9, [r5], #2	@ movhi
	ldr	fp, [sp, #12]
	ldr	r9, [sp, #20]
	subs	r2, r2, #1
	add	r8, r8, fp
	add	r1, r1, r9
	beq	.L260
.L262:
	ldr	fp, [sp, #52]
	str	r7, [sp, #48]
	str	r4, [sp, #52]
	str	r0, [sp, #60]
	ldr	r4, [sp, #20]
	str	r6, [sp, #56]
	mov	r9, sl
.L82:
	cmp	r3, r8
	add	r6, fp, r1, asr #12
	bgt	.L264
	mul	r6, ip, r6
	mov	r0, r6, asr #8
	strh	r0, [r5], #2	@ movhi
	add	r7, r1, r4
	add	r1, r7, r4
	ldr	r0, [sp, #12]
	add	r6, fp, r1, asr #12
	add	r8, r8, r0
	str	r6, [sp, #20]
	add	r6, r1, r4
	sub	r2, r2, #1
	add	sl, fp, r7, asr #12
	cmp	r3, r8
	add	r7, fp, r6, asr #12
	str	r7, [sp, #36]
	add	r1, r6, r4
	mov	r7, r2
	mov	r6, r5
	bgt	.L264
	mul	sl, ip, sl
	add	r8, r8, r0
	mov	sl, sl, asr #8
	cmp	r3, r8
	strh	sl, [r5], #2	@ movhi
	sub	r2, r2, #1
	bgt	.L264
	ldr	r2, [sp, #20]
	mul	r2, ip, r2
	ldr	sl, [sp, #12]
	add	r8, r8, sl
	mov	r0, r2, asr #8
	cmp	r3, r8
	strh	r0, [r5, #0]	@ movhi
	sub	r2, r7, #2
	add	r5, r6, #4
	bgt	.L264
	ldr	r0, [sp, #36]
	mul	r0, ip, r0
	subs	r2, r7, #3
	mov	r7, r0, asr #8
	strh	r7, [r6, #4]	@ movhi
	ldr	r5, [sp, #12]
	add	r8, r8, r5
	add	r5, r6, #6
	bne	.L82
	ldr	r7, [sp, #48]
	ldr	r4, [sp, #52]
	ldr	r0, [sp, #60]
	ldr	r1, [sp, #44]
	mov	sl, r9
.L240:
	ldr	r3, [sp, #40]
	mov	r9, r3, lsr #16
	str	r9, [sp, #20]
	b	.L29
.L265:
	ldr	r1, [sp, #44]
	b	.L241
.L276:
	ldr	r2, [sp, #52]
	mov	r3, r2, lsr #16
	ldr	r7, [sp, #44]
	add	r4, sp, #56
	ldmia	r4, {r4, sl}	@ phole ldm
	ldr	r0, [sp, #64]
	ldr	r1, [sp, #48]
	str	r3, [sp, #20]
	b	.L29
.L76:
	ldr	r3, [sp, #40]
	ldr	r1, [sp, #44]
	mov	r7, r3, lsr #16
	b	.L29
.L278:
	ldr	sl, [sp, #112]
	mov	r2, #1
	ldr	r1, [sp, #44]
	str	r2, [sl, #0]
	mov	sl, #0
	b	.L29
.L277:
	ldr	r9, [sp, #112]
	mov	ip, #1
	ldr	r1, [sp, #48]
	mov	sl, #0
	str	ip, [r9, #0]
	b	.L29
.L260:
	ldr	r1, [sp, #44]
	b	.L240
.L274:
	ldr	r6, [sp, #112]
	mov	r2, #1
	str	r2, [r6, #0]
	mov	sl, r4
	b	.L29
.L81:
	ldr	ip, [sp, #40]
	mov	r2, ip, lsr #16
	ldr	r1, [sp, #44]
	str	r2, [sp, #20]
	b	.L29
.L280:
	.align	2
.L279:
	.word	.LANCHOR1
	.cantunwind
	.fnend
	.size	_ZN15CIwChannelADPCM22GenerateADPCMAudioFastEPsiiiiPi, .-_ZN15CIwChannelADPCM22GenerateADPCMAudioFastEPsiiiiPi
	.global	__aeabi_idiv
	.section	.text._ZN15CIwChannelADPCM13GenerateAudioEP20s3eSoundGenAudioInfo,"ax",%progbits
	.align	2
	.global	_ZN15CIwChannelADPCM13GenerateAudioEP20s3eSoundGenAudioInfo
	.hidden	_ZN15CIwChannelADPCM13GenerateAudioEP20s3eSoundGenAudioInfo
	.type	_ZN15CIwChannelADPCM13GenerateAudioEP20s3eSoundGenAudioInfo, %function
_ZN15CIwChannelADPCM13GenerateAudioEP20s3eSoundGenAudioInfo:
	.fnstart
.LFB1369:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	r6, [r0, #0]
	cmp	r6, #0
	.pad #28
	sub	sp, sp, #28
	mov	r5, r0
	mov	r4, r1
	ldr	fp, [r1, #4]
	add	r9, r1, #8
	ldmia	r9, {r9, sl}	@ phole ldm
	beq	.L285
.L282:
	mov	r1, #1
	ldr	r0, [r4, #0]
	bl	s3eSoundChannelGetInt
	mov	r6, r0
	mov	r0, #1
	bl	s3eSoundGetInt
	mov	r1, #3
	mov	r7, r0
	ldr	r0, [r4, #0]
	bl	s3eSoundChannelGetInt
	mov	r1, r7
	mov	r8, r0
	mov	r7, #0
	mov	r0, r6, asl #12
	add	r6, sp, #24
	str	r7, [r6, #-4]!
	bl	__aeabi_idiv
	mov	r2, r9
	mov	r3, r0
	mov	r1, fp
	mov	r0, r5
	stmia	sp, {r8, sl}	@ phole stm
	str	r6, [sp, #8]
	bl	_ZN15CIwChannelADPCM22GenerateADPCMAudioFastEPsiiiiPi
	ldr	r2, [sp, #20]
	cmp	r2, r7
	movne	r2, #1
	strneb	r2, [r4, #28]
	strne	r7, [r5, #0]
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L285:
	ldr	r3, [r1, #16]
	str	r3, [r0, #0]
	add	ip, r1, #16
	ldmia	ip, {ip, lr}	@ phole ldm
	bic	r8, lr, #1
	add	r0, ip, r8, asl #1
	str	r0, [r5, #4]
	mov	r1, #2
	ldr	r0, [r4, #0]
	bl	s3eSoundChannelGetInt
	ldr	r7, [r0, #36]
	str	r7, [r5, #24]
	ldr	r1, [r0, #40]
	str	r1, [r5, #28]
	ldr	r2, [r0, #20]
	strh	r6, [r5, #20]	@ movhi
	str	r2, [r5, #32]
	strb	r6, [r5, #22]
	str	r6, [r5, #36]
	str	r6, [r5, #8]
	str	r6, [r5, #12]
	strh	r6, [r5, #16]	@ movhi
	strb	r6, [r5, #23]
	b	.L282
	.fnend
	.size	_ZN15CIwChannelADPCM13GenerateAudioEP20s3eSoundGenAudioInfo, .-_ZN15CIwChannelADPCM13GenerateAudioEP20s3eSoundGenAudioInfo
	.hidden	_ZN15CIwChannelADPCM13isInitializedE
	.global	_ZN15CIwChannelADPCM13isInitializedE
	.section	.rodata
	.align	2
	.set	.LANCHOR2,. + 0
	.type	_ZL13stepsizeTable, %object
	.size	_ZL13stepsizeTable, 356
_ZL13stepsizeTable:
	.word	7
	.word	8
	.word	9
	.word	10
	.word	11
	.word	12
	.word	13
	.word	14
	.word	16
	.word	17
	.word	19
	.word	21
	.word	23
	.word	25
	.word	28
	.word	31
	.word	34
	.word	37
	.word	41
	.word	45
	.word	50
	.word	55
	.word	60
	.word	66
	.word	73
	.word	80
	.word	88
	.word	97
	.word	107
	.word	118
	.word	130
	.word	143
	.word	157
	.word	173
	.word	190
	.word	209
	.word	230
	.word	253
	.word	279
	.word	307
	.word	337
	.word	371
	.word	408
	.word	449
	.word	494
	.word	544
	.word	598
	.word	658
	.word	724
	.word	796
	.word	876
	.word	963
	.word	1060
	.word	1166
	.word	1282
	.word	1411
	.word	1552
	.word	1707
	.word	1878
	.word	2066
	.word	2272
	.word	2499
	.word	2749
	.word	3024
	.word	3327
	.word	3660
	.word	4026
	.word	4428
	.word	4871
	.word	5358
	.word	5894
	.word	6484
	.word	7132
	.word	7845
	.word	8630
	.word	9493
	.word	10442
	.word	11487
	.word	12635
	.word	13899
	.word	15289
	.word	16818
	.word	18500
	.word	20350
	.word	22385
	.word	24623
	.word	27086
	.word	29794
	.word	32767
	.data
	.align	2
	.set	.LANCHOR1,. + 0
	.type	_ZL6sTable, %object
	.size	_ZL6sTable, 2912
_ZL6sTable:
	.word	-1
	.word	-1
	.word	-1
	.word	-1
	.word	2
	.word	4
	.word	6
	.word	8
	.word	-1
	.word	-1
	.word	-1
	.word	-1
	.word	2
	.word	4
	.word	6
	.word	8
	.word	0
	.space	2844
	.bss
	.align	2
	.set	.LANCHOR0,. + 0
	.type	_ZN15CIwChannelADPCM13isInitializedE, %object
	.size	_ZN15CIwChannelADPCM13isInitializedE, 4
_ZN15CIwChannelADPCM13isInitializedE:
	.space	4
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
