	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"IwSoundSpec.cpp"
	.section	.text._ZN10CIwManaged9ParseOpenEP16CIwTextParserITX,"axG",%progbits,_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX,comdat
	.align	2
	.weak	_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX
	.hidden	_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX
	.type	_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX, %function
_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX:
	.fnstart
.LFB331:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX, .-_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX
	.section	.text._ZN10CIwManaged11HandleEventEP8CIwEvent,"axG",%progbits,_ZN10CIwManaged11HandleEventEP8CIwEvent,comdat
	.align	2
	.weak	_ZN10CIwManaged11HandleEventEP8CIwEvent
	.hidden	_ZN10CIwManaged11HandleEventEP8CIwEvent
	.type	_ZN10CIwManaged11HandleEventEP8CIwEvent, %function
_ZN10CIwManaged11HandleEventEP8CIwEvent:
	.fnstart
.LFB332:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #0
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN10CIwManaged11HandleEventEP8CIwEvent, .-_ZN10CIwManaged11HandleEventEP8CIwEvent
	.section	.text._ZN10CIwManaged11DebugRenderEv,"axG",%progbits,_ZN10CIwManaged11DebugRenderEv,comdat
	.align	2
	.weak	_ZN10CIwManaged11DebugRenderEv
	.hidden	_ZN10CIwManaged11DebugRenderEv
	.type	_ZN10CIwManaged11DebugRenderEv, %function
_ZN10CIwManaged11DebugRenderEv:
	.fnstart
.LFB334:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN10CIwManaged11DebugRenderEv, .-_ZN10CIwManaged11DebugRenderEv
	.section	.text._ZN11CIwResource10ApplyScaleEi,"axG",%progbits,_ZN11CIwResource10ApplyScaleEi,comdat
	.align	2
	.weak	_ZN11CIwResource10ApplyScaleEi
	.hidden	_ZN11CIwResource10ApplyScaleEi
	.type	_ZN11CIwResource10ApplyScaleEi, %function
_ZN11CIwResource10ApplyScaleEi:
	.fnstart
.LFB360:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN11CIwResource10ApplyScaleEi, .-_ZN11CIwResource10ApplyScaleEi
	.section	.text._Z24_IwSoundSpec_EndSampleCBP21s3eSoundEndSampleInfoPv,"ax",%progbits
	.align	2
	.global	_Z24_IwSoundSpec_EndSampleCBP21s3eSoundEndSampleInfoPv
	.hidden	_Z24_IwSoundSpec_EndSampleCBP21s3eSoundEndSampleInfoPv
	.type	_Z24_IwSoundSpec_EndSampleCBP21s3eSoundEndSampleInfoPv, %function
_Z24_IwSoundSpec_EndSampleCBP21s3eSoundEndSampleInfoPv:
	.fnstart
.LFB1371:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, [r0, #4]
	cmp	r0, #0
	ldreqh	r3, [r1, #10]
	orreq	r3, r3, #2
	movne	r0, #1
	streqh	r3, [r1, #10]	@ movhi
	bx	lr
	.cantunwind
	.fnend
	.size	_Z24_IwSoundSpec_EndSampleCBP21s3eSoundEndSampleInfoPv, .-_Z24_IwSoundSpec_EndSampleCBP21s3eSoundEndSampleInfoPv
	.section	.text._Z20_GetCIwSoundSpecSizev,"ax",%progbits
	.align	2
	.global	_Z20_GetCIwSoundSpecSizev
	.hidden	_Z20_GetCIwSoundSpecSizev
	.type	_Z20_GetCIwSoundSpecSizev, %function
_Z20_GetCIwSoundSpecSizev:
	.fnstart
.LFB1373:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #40
	bx	lr
	.cantunwind
	.fnend
	.size	_Z20_GetCIwSoundSpecSizev, .-_Z20_GetCIwSoundSpecSizev
	.section	.text._ZNK12CIwSoundSpec12GetClassNameEv,"ax",%progbits
	.align	2
	.global	_ZNK12CIwSoundSpec12GetClassNameEv
	.hidden	_ZNK12CIwSoundSpec12GetClassNameEv
	.type	_ZNK12CIwSoundSpec12GetClassNameEv, %function
_ZNK12CIwSoundSpec12GetClassNameEv:
	.fnstart
.LFB1374:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L17
	bx	lr
.L18:
	.align	2
.L17:
	.word	.LC0
	.cantunwind
	.fnend
	.size	_ZNK12CIwSoundSpec12GetClassNameEv, .-_ZNK12CIwSoundSpec12GetClassNameEv
	.section	.text._ZN12CIwSoundSpec14ParseAttributeEP16CIwTextParserITXPKc,"ax",%progbits
	.align	2
	.global	_ZN12CIwSoundSpec14ParseAttributeEP16CIwTextParserITXPKc
	.hidden	_ZN12CIwSoundSpec14ParseAttributeEP16CIwTextParserITXPKc
	.type	_ZN12CIwSoundSpec14ParseAttributeEP16CIwTextParserITXPKc, %function
_ZN12CIwSoundSpec14ParseAttributeEP16CIwTextParserITXPKc:
	.fnstart
.LFB1385:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #0
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN12CIwSoundSpec14ParseAttributeEP16CIwTextParserITXPKc, .-_ZN12CIwSoundSpec14ParseAttributeEP16CIwTextParserITXPKc
	.section	.text._ZN12CIwSoundSpec10ParseCloseEP16CIwTextParserITX,"ax",%progbits
	.align	2
	.global	_ZN12CIwSoundSpec10ParseCloseEP16CIwTextParserITX
	.hidden	_ZN12CIwSoundSpec10ParseCloseEP16CIwTextParserITX
	.type	_ZN12CIwSoundSpec10ParseCloseEP16CIwTextParserITX, %function
_ZN12CIwSoundSpec10ParseCloseEP16CIwTextParserITX:
	.fnstart
.LFB1386:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN12CIwSoundSpec10ParseCloseEP16CIwTextParserITX, .-_ZN12CIwSoundSpec10ParseCloseEP16CIwTextParserITX
	.section	.text._ZN8CIwArrayIs12CIwAllocatorIs15CIwMallocRouterIsEE17ReallocateDefaultIsS3_EED1Ev,"axG",%progbits,_ZN8CIwArrayIs12CIwAllocatorIs15CIwMallocRouterIsEE17ReallocateDefaultIsS3_EED1Ev,comdat
	.align	2
	.weak	_ZN8CIwArrayIs12CIwAllocatorIs15CIwMallocRouterIsEE17ReallocateDefaultIsS3_EED1Ev
	.hidden	_ZN8CIwArrayIs12CIwAllocatorIs15CIwMallocRouterIsEE17ReallocateDefaultIsS3_EED1Ev
	.type	_ZN8CIwArrayIs12CIwAllocatorIs15CIwMallocRouterIsEE17ReallocateDefaultIsS3_EED1Ev, %function
_ZN8CIwArrayIs12CIwAllocatorIs15CIwMallocRouterIsEE17ReallocateDefaultIsS3_EED1Ev:
	.fnstart
.LFB1468:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	ldrb	r5, [r0, #12]	@ zero_extendqisi2
	ands	r5, r5, #1
	mov	r4, r0
	bne	.L24
	ldr	r0, [r0, #0]
	mov	r1, #2
	bl	_ZN22CIwDefaultMallocRouter8BaseFreeEPvj
	str	r5, [r4, #0]
.L24:
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
	.fnend
	.size	_ZN8CIwArrayIs12CIwAllocatorIs15CIwMallocRouterIsEE17ReallocateDefaultIsS3_EED1Ev, .-_ZN8CIwArrayIs12CIwAllocatorIs15CIwMallocRouterIsEE17ReallocateDefaultIsS3_EED1Ev
	.section	.text._ZN8CIwArrayIa12CIwAllocatorIa15CIwMallocRouterIaEE17ReallocateDefaultIaS3_EED1Ev,"axG",%progbits,_ZN8CIwArrayIa12CIwAllocatorIa15CIwMallocRouterIaEE17ReallocateDefaultIaS3_EED1Ev,comdat
	.align	2
	.weak	_ZN8CIwArrayIa12CIwAllocatorIa15CIwMallocRouterIaEE17ReallocateDefaultIaS3_EED1Ev
	.hidden	_ZN8CIwArrayIa12CIwAllocatorIa15CIwMallocRouterIaEE17ReallocateDefaultIaS3_EED1Ev
	.type	_ZN8CIwArrayIa12CIwAllocatorIa15CIwMallocRouterIaEE17ReallocateDefaultIaS3_EED1Ev, %function
_ZN8CIwArrayIa12CIwAllocatorIa15CIwMallocRouterIaEE17ReallocateDefaultIaS3_EED1Ev:
	.fnstart
.LFB1460:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	ldrb	r5, [r0, #12]	@ zero_extendqisi2
	ands	r5, r5, #1
	mov	r4, r0
	bne	.L28
	ldr	r0, [r0, #0]
	mov	r1, #1
	bl	_ZN22CIwDefaultMallocRouter8BaseFreeEPvj
	str	r5, [r4, #0]
.L28:
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
	.fnend
	.size	_ZN8CIwArrayIa12CIwAllocatorIa15CIwMallocRouterIaEE17ReallocateDefaultIaS3_EED1Ev, .-_ZN8CIwArrayIa12CIwAllocatorIa15CIwMallocRouterIaEE17ReallocateDefaultIaS3_EED1Ev
	.section	.text._ZN12CIwSoundSpec7ResolveEv,"ax",%progbits
	.align	2
	.global	_ZN12CIwSoundSpec7ResolveEv
	.hidden	_ZN12CIwSoundSpec7ResolveEv
	.type	_ZN12CIwSoundSpec7ResolveEv, %function
_ZN12CIwSoundSpec7ResolveEv:
	.fnstart
.LFB1383:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	bl	_ZN10CIwManaged7ResolveEv
	add	r0, r4, #32
	ldr	r1, .L32
	mov	r2, #0
	bl	_Z20IwResolveManagedHashPvPKcj
	add	r0, r4, #36
	ldr	r1, .L32+4
	mov	r2, #0
	bl	_Z20IwResolveManagedHashPvPKcj
	ldmfd	sp!, {r4, lr}
	bx	lr
.L33:
	.align	2
.L32:
	.word	.LC1
	.word	.LC2
	.fnend
	.size	_ZN12CIwSoundSpec7ResolveEv, .-_ZN12CIwSoundSpec7ResolveEv
	.section	.text._ZN12CIwSoundSpec9SerialiseEv,"ax",%progbits
	.align	2
	.global	_ZN12CIwSoundSpec9SerialiseEv
	.hidden	_ZN12CIwSoundSpec9SerialiseEv
	.type	_ZN12CIwSoundSpec9SerialiseEv, %function
_ZN12CIwSoundSpec9SerialiseEv:
	.fnstart
.LFB1382:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	bl	_ZN10CIwManaged9SerialiseEv
	add	r0, r4, #16
	mov	r1, #1
	mov	r2, #16
	mov	r3, #2
	bl	_Z17IwSerialiseUInt16Rtiii
	add	r0, r4, #20
	mov	r1, #1
	mov	r2, #15
	mov	r3, #2
	bl	_Z16IwSerialiseInt16Rsiii
	add	r0, r4, #22
	mov	r1, #1
	mov	r2, #15
	mov	r3, #2
	bl	_Z16IwSerialiseInt16Rsiii
	add	r0, r4, #24
	mov	r1, #1
	mov	r2, #15
	mov	r3, #2
	bl	_Z16IwSerialiseInt16Rsiii
	add	r0, r4, #26
	mov	r1, #1
	mov	r2, #15
	mov	r3, #2
	bl	_Z16IwSerialiseInt16Rsiii
	add	r0, r4, #28
	mov	r1, #1
	mov	r2, #15
	mov	r3, #2
	bl	_Z16IwSerialiseInt16Rsiii
	add	r0, r4, #30
	mov	r1, #1
	mov	r2, #15
	mov	r3, #2
	bl	_Z16IwSerialiseInt16Rsiii
	add	r0, r4, #32
	bl	_Z22IwSerialiseManagedHashPv
	add	r0, r4, #36
	bl	_Z22IwSerialiseManagedHashPv
	ldmfd	sp!, {r4, lr}
	bx	lr
	.fnend
	.size	_ZN12CIwSoundSpec9SerialiseEv, .-_ZN12CIwSoundSpec9SerialiseEv
	.global	__cxa_end_cleanup
	.section	.text._ZN12CIwSoundSpecD0Ev,"ax",%progbits
	.align	2
	.global	_ZN12CIwSoundSpecD0Ev
	.hidden	_ZN12CIwSoundSpecD0Ev
	.type	_ZN12CIwSoundSpecD0Ev, %function
_ZN12CIwSoundSpecD0Ev:
	.fnstart
.LFB1381:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	mov	r4, r0
	ldr	r0, .L41
	ldr	r3, .L41+4
	str	r0, [r4, #0]
	ldr	r0, [r3, #0]
	cmp	r0, #0
	movne	r1, r4
.LEHB0:
	blne	_ZN15CIwSoundManager13StopSoundSpecEP12CIwSoundSpec
.LEHE0:
.L37:
	ldr	r1, .L41+8
	mov	r0, r4
	str	r1, [r4, #0]
.LEHB1:
	bl	_ZN10CIwManagedD2Ev
.LEHE1:
	mov	r0, r4
	bl	_ZdlPv
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L40:
.L38:
	ldr	r2, .L41+8
	mov	r5, r0
	str	r2, [r4, #0]
	mov	r0, r4
	bl	_ZN10CIwManagedD2Ev
	mov	r0, r5
.LEHB2:
	bl	__cxa_end_cleanup
.LEHE2:
.L42:
	.align	2
.L41:
	.word	.LANCHOR0+8
	.word	g_IwSoundManager
	.word	_ZTV18CIwManagedRefCount+8
	.global	__gxx_personality_v0
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA1381:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE1381-.LLSDACSB1381
.LLSDACSB1381:
	.uleb128 .LEHB0-.LFB1381
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L40-.LFB1381
	.uleb128 0x0
	.uleb128 .LEHB1-.LFB1381
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB2-.LFB1381
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE1381:
	.fnend
	.size	_ZN12CIwSoundSpecD0Ev, .-_ZN12CIwSoundSpecD0Ev
	.section	.text._ZN12CIwSoundSpecD1Ev,"ax",%progbits
	.align	2
	.global	_ZN12CIwSoundSpecD1Ev
	.hidden	_ZN12CIwSoundSpecD1Ev
	.type	_ZN12CIwSoundSpecD1Ev, %function
_ZN12CIwSoundSpecD1Ev:
	.fnstart
.LFB1380:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	mov	r4, r0
	ldr	r0, .L48
	ldr	r3, .L48+4
	str	r0, [r4, #0]
	ldr	r0, [r3, #0]
	cmp	r0, #0
	movne	r1, r4
.LEHB3:
	blne	_ZN15CIwSoundManager13StopSoundSpecEP12CIwSoundSpec
.LEHE3:
.L44:
	ldr	r1, .L48+8
	mov	r0, r4
	str	r1, [r4, #0]
.LEHB4:
	bl	_ZN10CIwManagedD2Ev
.LEHE4:
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L47:
.L45:
	ldr	r2, .L48+8
	mov	r5, r0
	str	r2, [r4, #0]
	mov	r0, r4
	bl	_ZN10CIwManagedD2Ev
	mov	r0, r5
.LEHB5:
	bl	__cxa_end_cleanup
.LEHE5:
.L49:
	.align	2
.L48:
	.word	.LANCHOR0+8
	.word	g_IwSoundManager
	.word	_ZTV18CIwManagedRefCount+8
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA1380:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE1380-.LLSDACSB1380
.LLSDACSB1380:
	.uleb128 .LEHB3-.LFB1380
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L47-.LFB1380
	.uleb128 0x0
	.uleb128 .LEHB4-.LFB1380
	.uleb128 .LEHE4-.LEHB4
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB5-.LFB1380
	.uleb128 .LEHE5-.LEHB5
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE1380:
	.fnend
	.size	_ZN12CIwSoundSpecD1Ev, .-_ZN12CIwSoundSpecD1Ev
	.section	.text._ZN12CIwSoundSpecD2Ev,"ax",%progbits
	.align	2
	.global	_ZN12CIwSoundSpecD2Ev
	.hidden	_ZN12CIwSoundSpecD2Ev
	.type	_ZN12CIwSoundSpecD2Ev, %function
_ZN12CIwSoundSpecD2Ev:
	.fnstart
.LFB1379:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	mov	r4, r0
	ldr	r0, .L55
	ldr	r3, .L55+4
	str	r0, [r4, #0]
	ldr	r0, [r3, #0]
	cmp	r0, #0
	movne	r1, r4
.LEHB6:
	blne	_ZN15CIwSoundManager13StopSoundSpecEP12CIwSoundSpec
.LEHE6:
.L51:
	ldr	r1, .L55+8
	mov	r0, r4
	str	r1, [r4, #0]
.LEHB7:
	bl	_ZN10CIwManagedD2Ev
.LEHE7:
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L54:
.L52:
	ldr	r2, .L55+8
	mov	r5, r0
	str	r2, [r4, #0]
	mov	r0, r4
	bl	_ZN10CIwManagedD2Ev
	mov	r0, r5
.LEHB8:
	bl	__cxa_end_cleanup
.LEHE8:
.L56:
	.align	2
.L55:
	.word	.LANCHOR0+8
	.word	g_IwSoundManager
	.word	_ZTV18CIwManagedRefCount+8
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA1379:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE1379-.LLSDACSB1379
.LLSDACSB1379:
	.uleb128 .LEHB6-.LFB1379
	.uleb128 .LEHE6-.LEHB6
	.uleb128 .L54-.LFB1379
	.uleb128 0x0
	.uleb128 .LEHB7-.LFB1379
	.uleb128 .LEHE7-.LEHB7
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB8-.LFB1379
	.uleb128 .LEHE8-.LEHB8
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE1379:
	.fnend
	.size	_ZN12CIwSoundSpecD2Ev, .-_ZN12CIwSoundSpecD2Ev
	.section	.text._ZN12CIwSoundSpecC1Ev,"ax",%progbits
	.align	2
	.global	_ZN12CIwSoundSpecC1Ev
	.hidden	_ZN12CIwSoundSpecC1Ev
	.type	_ZN12CIwSoundSpecC1Ev, %function
_ZN12CIwSoundSpecC1Ev:
	.fnstart
.LFB1377:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	bl	_ZN10CIwManagedC2Ev
	ldr	r1, .L61
	mov	r3, #0
	mov	r2, #4096
	str	r1, [r4, #0]
	strh	r2, [r4, #30]	@ movhi
	strh	r3, [r4, #16]	@ movhi
	strh	r3, [r4, #12]	@ movhi
	strh	r3, [r4, #14]	@ movhi
	str	r3, [r4, #32]
	strh	r2, [r4, #20]	@ movhi
	strh	r2, [r4, #22]	@ movhi
	strh	r3, [r4, #24]	@ movhi
	strh	r3, [r4, #26]	@ movhi
	strh	r2, [r4, #28]	@ movhi
	str	r3, [r4, #36]
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L62:
	.align	2
.L61:
	.word	.LANCHOR0+8
	.fnend
	.size	_ZN12CIwSoundSpecC1Ev, .-_ZN12CIwSoundSpecC1Ev
	.section	.text._ZN12CIwSoundSpecC2Ev,"ax",%progbits
	.align	2
	.global	_ZN12CIwSoundSpecC2Ev
	.hidden	_ZN12CIwSoundSpecC2Ev
	.type	_ZN12CIwSoundSpecC2Ev, %function
_ZN12CIwSoundSpecC2Ev:
	.fnstart
.LFB1376:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	bl	_ZN10CIwManagedC2Ev
	ldr	r1, .L67
	mov	r3, #0
	mov	r2, #4096
	str	r1, [r4, #0]
	strh	r2, [r4, #30]	@ movhi
	strh	r3, [r4, #16]	@ movhi
	strh	r3, [r4, #12]	@ movhi
	strh	r3, [r4, #14]	@ movhi
	str	r3, [r4, #32]
	strh	r2, [r4, #20]	@ movhi
	strh	r2, [r4, #22]	@ movhi
	strh	r3, [r4, #24]	@ movhi
	strh	r3, [r4, #26]	@ movhi
	strh	r2, [r4, #28]	@ movhi
	str	r3, [r4, #36]
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L68:
	.align	2
.L67:
	.word	.LANCHOR0+8
	.fnend
	.size	_ZN12CIwSoundSpecC2Ev, .-_ZN12CIwSoundSpecC2Ev
	.section	.text.T.294,"ax",%progbits
	.align	2
	.type	T.294, %function
T.294:
	.fnstart
.LFB1613:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldrb	r1, [r0, #12]	@ zero_extendqisi2
	bic	r2, r1, #1
	strb	r2, [r0, #12]
	ldrb	ip, [r0, #12]	@ zero_extendqisi2
	mov	r2, #0
	bic	r1, ip, #2
	str	r2, [r0, #4]
	strb	r1, [r0, #12]
	str	r2, [r0, #0]
	str	r2, [r0, #8]
	bx	lr
	.cantunwind
	.fnend
	.size	T.294, .-T.294
	.section	.text._Z20_CIwSoundSpecFactoryv,"ax",%progbits
	.align	2
	.global	_Z20_CIwSoundSpecFactoryv
	.hidden	_Z20_CIwSoundSpecFactoryv
	.type	_Z20_CIwSoundSpecFactoryv, %function
_Z20_CIwSoundSpecFactoryv:
	.fnstart
.LFB1372:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	mov	r0, #40
.LEHB9:
	bl	_Znwj
.LEHE9:
	mov	r4, r0
.LEHB10:
	bl	_ZN10CIwManagedC2Ev
.LEHE10:
	ldr	r0, .L78
	mov	r3, #0
	mov	r2, #4096	@ movhi
	str	r0, [r4, #0]
	strh	r3, [r4, #16]	@ movhi
	strh	r3, [r4, #12]	@ movhi
	strh	r3, [r4, #14]	@ movhi
	str	r3, [r4, #32]
	strh	r2, [r4, #20]	@ movhi
	strh	r2, [r4, #22]	@ movhi
	strh	r3, [r4, #24]	@ movhi
	strh	r3, [r4, #26]	@ movhi
	strh	r2, [r4, #28]	@ movhi
	strh	r2, [r4, #30]	@ movhi
	str	r3, [r4, #36]
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L77:
.L75:
	mov	r5, r0
	mov	r0, r4
	bl	_ZdlPv
	mov	r0, r5
.LEHB11:
	bl	__cxa_end_cleanup
.LEHE11:
.L79:
	.align	2
.L78:
	.word	.LANCHOR0+8
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA1372:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE1372-.LLSDACSB1372
.LLSDACSB1372:
	.uleb128 .LEHB9-.LFB1372
	.uleb128 .LEHE9-.LEHB9
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB10-.LFB1372
	.uleb128 .LEHE10-.LEHB10
	.uleb128 .L77-.LFB1372
	.uleb128 0x0
	.uleb128 .LEHB11-.LFB1372
	.uleb128 .LEHE11-.LEHB11
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE1372:
	.fnend
	.size	_Z20_CIwSoundSpecFactoryv, .-_Z20_CIwSoundSpecFactoryv
	.section	.text._ZNK12CIwSoundData7GetDataER8CIwArrayIs12CIwAllocatorIs15CIwMallocRouterIsEE17ReallocateDefaultIsS4_EE,"axG",%progbits,_ZNK12CIwSoundData7GetDataER8CIwArrayIs12CIwAllocatorIs15CIwMallocRouterIsEE17ReallocateDefaultIsS4_EE,comdat
	.align	2
	.weak	_ZNK12CIwSoundData7GetDataER8CIwArrayIs12CIwAllocatorIs15CIwMallocRouterIsEE17ReallocateDefaultIsS4_EE
	.hidden	_ZNK12CIwSoundData7GetDataER8CIwArrayIs12CIwAllocatorIs15CIwMallocRouterIsEE17ReallocateDefaultIsS4_EE
	.type	_ZNK12CIwSoundData7GetDataER8CIwArrayIs12CIwAllocatorIs15CIwMallocRouterIsEE17ReallocateDefaultIsS4_EE, %function
_ZNK12CIwSoundData7GetDataER8CIwArrayIs12CIwAllocatorIs15CIwMallocRouterIsEE17ReallocateDefaultIsS4_EE:
	.fnstart
.LFB1323:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	.save {r4, r5, r6, r7, r8, lr}
	mov	r5, r0
	mov	r4, r1
	ldr	r6, [r0, #24]
	bl	_ZNK12CIwSoundData13GetBufferSizeEv
	mov	r8, r0
	mov	r0, r5
	bl	_ZNK12CIwSoundData13GetBufferSizeEv
	ldrb	r3, [r4, #12]	@ zero_extendqisi2
	ands	r5, r3, #1
	mov	r7, r0
	bne	.L81
	ldr	r2, [r4, #8]
	cmp	r2, #0
	str	r5, [r4, #4]
	beq	.L81
	str	r5, [r4, #8]
	ldr	r0, [r4, #0]
	mov	r1, #2
	bl	_ZN22CIwDefaultMallocRouter8BaseFreeEPvj
	str	r5, [r4, #0]
	ldrb	r3, [r4, #12]	@ zero_extendqisi2
.L81:
	mov	r8, r8, lsr #1
	mov	r7, r7, lsr #1
	orr	r3, r3, #1
	strb	r3, [r4, #12]
	stmia	r4, {r6, r8}	@ phole stm
	str	r7, [r4, #8]
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
	.fnend
	.size	_ZNK12CIwSoundData7GetDataER8CIwArrayIs12CIwAllocatorIs15CIwMallocRouterIsEE17ReallocateDefaultIsS4_EE, .-_ZNK12CIwSoundData7GetDataER8CIwArrayIs12CIwAllocatorIs15CIwMallocRouterIsEE17ReallocateDefaultIsS4_EE
	.global	__aeabi_idivmod
	.section	.text._ZN12CIwSoundSpec4PlayEPK14CIwSoundParams,"ax",%progbits
	.align	2
	.global	_ZN12CIwSoundSpec4PlayEPK14CIwSoundParams
	.hidden	_ZN12CIwSoundSpec4PlayEPK14CIwSoundParams
	.type	_ZN12CIwSoundSpec4PlayEPK14CIwSoundParams, %function
_ZN12CIwSoundSpec4PlayEPK14CIwSoundParams:
	.fnstart
.LFB1384:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	r7, .L131
	.pad #44
	sub	sp, sp, #44
	mov	r4, r0
	ldr	r0, [r7, #0]
	mov	r6, r1
.LEHB12:
	bl	_ZN15CIwSoundManager11GetFreeInstEv
	subs	r5, r0, #0
	beq	.L84
	ldr	r3, [r4, #32]
	ldr	r0, [r7, #0]
	ldr	r2, [r3, #32]
	mov	r1, r5
	bl	_ZN15CIwSoundManager14GetFreeChannelEP12CIwSoundInst17IwSoundDataFormat
	cmp	r0, #0
	str	r0, [sp, #16]
	blt	.L127
	cmp	r6, #0
	ldreq	r3, [r7, #0]
	ldr	r8, [r4, #36]
	ldreq	r6, [r3, #36]
	cmp	r8, #0
	beq	.L87
	ldrh	r2, [r8, #22]
	cmp	r2, #0
	ldrh	r3, [r8, #24]
	bne	.L128
.L89:
	add	r0, r3, #1
	strh	r0, [r8, #24]	@ movhi
.L91:
	mov	r2, #0	@ movhi
	strh	r2, [r5, #14]	@ movhi
	ldrh	fp, [r4, #20]
	ldrsh	r3, [r4, #22]
	mov	fp, fp, asl #16
	mov	sl, fp, asr #16
	cmp	sl, r3
	beq	.L92
	subs	fp, r3, sl
	beq	.L94
	bl	IwRand
	mov	r1, fp
	bl	__aeabi_idivmod
	mov	fp, r1
.L94:
	add	r9, fp, sl
	mov	fp, r9, asl #16
.L92:
	ldrh	ip, [r4, #24]
	ldrsh	r3, [r4, #26]
	mov	r9, ip, asl #16
	mov	sl, r9, asr #16
	cmp	sl, r3
	beq	.L95
	subs	r9, r3, sl
	beq	.L97
	bl	IwRand
	mov	r1, r9
	bl	__aeabi_idivmod
	mov	r9, r1
.L97:
	add	lr, r9, sl
	mov	r9, lr, asl #16
.L95:
	ldrh	r3, [r4, #28]
	ldrsh	r1, [r4, #30]
	mov	r3, r3, asl #16
	mov	sl, r3, asr #16
	cmp	sl, r1
	beq	.L98
	subs	r1, r1, sl
	beq	.L100
	str	r1, [sp, #12]
	bl	IwRand
	ldr	r1, [sp, #12]
	bl	__aeabi_idivmod
.L100:
	add	sl, r1, sl
	mov	r3, sl, asl #16
.L98:
	str	r4, [r5, #0]
	ldrsh	r2, [r6, #0]
	mov	ip, fp, asr #16
	mul	ip, r2, ip
	mov	fp, ip, asl #4
	mov	fp, fp, lsr #16
	strh	fp, [r5, #4]	@ movhi
	ldrsh	r0, [r6, #2]
	add	r9, r0, r9, asr #16
	cmn	r9, #4096
	movlt	r9, #61440
	bge	.L129
.L102:
	strh	r9, [r5, #6]	@ movhi
	ldrsh	ip, [r6, #4]
	mov	sl, r3, asr #16
	mul	r1, ip, sl
	mov	r0, r1, asr #12
	strh	r0, [r5, #8]	@ movhi
	ldr	r3, [sp, #16]
	mov	r1, #0
	strh	r3, [r5, #12]	@ movhi
	str	r1, [r5, #20]
	ldr	lr, [r7, #0]
	ldrh	r6, [r8, #20]
	ldrh	r7, [lr, #30]
	str	r6, [sp, #20]
	tst	r7, #2
	ldrh	r9, [lr, #24]
	ldrh	r6, [r8, #16]
	ldrh	r7, [lr, #28]
	ldrh	ip, [r4, #16]
	bne	.L130
.L84:
	mov	r0, r5
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L129:
	cmp	r9, #4096
	movge	r9, #4096
	mov	r9, r9, asl #16
	mov	r9, r9, lsr #16
	b	.L102
.L128:
	cmp	r2, r3
	bne	.L89
	ldrh	sl, [r8, #26]
	ands	sl, sl, #2
	beq	.L90
	mov	r0, r8
	mov	r1, #1
	bl	_ZN13CIwSoundGroup14KillOldestInstEb
	ldrh	r3, [r8, #24]
	b	.L89
.L130:
	mov	r3, r5
	ldr	r2, .L131+4
	ldr	r0, [sp, #16]
	and	r8, ip, #1
	mov	r9, r9, asl #16
	bl	s3eSoundChannelRegister
	mov	r0, r6, asl #16
	mov	r6, r0, asr #16
	mov	r2, r9, asr #16
	mul	r2, r6, r2
	mov	fp, fp, asl #16
	mov	ip, r2, asr #12
	mov	fp, fp, asr #16
	mul	ip, fp, ip
	mov	r1, ip, asl #4
	mov	r2, r1, asr #20
	cmp	r2, #256
	movge	r2, #256
	mov	r1, #3
	ldr	r0, [sp, #16]
	bl	s3eSoundChannelSetInt
	ldr	r3, [sp, #20]
	mov	r7, r7, asl #16
	mov	r0, r3, asl #16
	mov	r6, r0, asr #16
	mov	r2, r7, asr #16
	mul	r2, r6, r2
	mov	r1, r2, asr #12
	mul	r1, sl, r1
	ldr	ip, [r5, #0]
	ldr	r3, [ip, #32]
	mov	r0, r1, asl #4
	ldr	r6, [r3, #28]
	mov	r2, r0, asr #16
	mul	r2, r6, r2
	ldr	r0, [sp, #16]
	mov	r2, r2, asr #12
	mov	r1, #1
	bl	s3eSoundChannelSetInt
.LEHE12:
	ldr	r6, [r4, #32]
	ldr	r3, [r6, #32]
	cmp	r3, #1
	beq	.L104
	cmp	r3, #2
	beq	.L105
	cmp	r3, #0
	bne	.L84
	ldrb	r1, [sp, #36]	@ zero_extendqisi2
	bic	r7, r1, #1
	strb	r7, [sp, #36]
	ldrb	lr, [sp, #36]	@ zero_extendqisi2
	bic	r4, lr, #2
	strb	r4, [sp, #36]
	str	r3, [sp, #28]
	str	r3, [sp, #24]
	str	r3, [sp, #32]
	mov	r0, r6
	ldr	r4, [r6, #24]
.LEHB13:
	bl	_ZNK12CIwSoundData13GetBufferSizeEv
	mov	r7, r0
	mov	r0, r6
	bl	_ZNK12CIwSoundData13GetBufferSizeEv
	ldrb	ip, [sp, #36]	@ zero_extendqisi2
	ands	r3, ip, #1
	mov	r6, r0
	bne	.L107
	ldr	r2, [sp, #32]
	cmp	r2, #0
	str	r3, [sp, #28]
	beq	.L107
	ldr	r0, [sp, #24]
	mov	r1, #1
	str	r3, [sp, #32]
	bl	_ZN22CIwDefaultMallocRouter8BaseFreeEPvj
	ldrb	ip, [sp, #36]	@ zero_extendqisi2
.L107:
	cmp	r8, #0
	orr	lr, ip, #1
	ldr	r0, [sp, #16]
	mov	ip, #0
	mov	r1, r4
	mov	r2, r7, asr #1
	mvnne	r3, #0
	moveq	r3, #1
	str	r6, [sp, #32]
	strb	lr, [sp, #36]
	str	ip, [sp, #0]
	str	r4, [sp, #24]
	str	r7, [sp, #28]
	bl	s3eSoundChannelPlay
.LEHE13:
	add	r0, sp, #24
.LEHB14:
	bl	_ZN8CIwArrayIa12CIwAllocatorIa15CIwMallocRouterIaEE17ReallocateDefaultIaS3_EED1Ev
.LEHE14:
	b	.L84
.L105:
	add	r6, sp, #24
	mov	r0, r6
	bl	T.294
	ldr	r0, [r4, #32]
	mov	r1, r6
.LEHB15:
	bl	_ZNK12CIwSoundData7GetDataER8CIwArrayIs12CIwAllocatorIs15CIwMallocRouterIsEE17ReallocateDefaultIsS4_EE
	ldr	r0, [sp, #16]
	ldr	r2, [r4, #32]
	mov	r1, #2
	bl	s3eSoundChannelSetInt
	ldr	r1, [sp, #28]
	cmp	r8, #0
	mov	r2, r1, asl #1
	mov	lr, #0
	ldr	r0, [sp, #16]
	ldr	r1, [sp, #24]
	mov	r2, r2, asr #1
	mvnne	r3, #0
	moveq	r3, #1
	str	lr, [sp, #0]
	bl	s3eSoundChannelPlay
.LEHE15:
.L126:
	mov	r0, r6
.LEHB16:
	bl	_ZN8CIwArrayIs12CIwAllocatorIs15CIwMallocRouterIsEE17ReallocateDefaultIsS3_EED1Ev
.LEHE16:
	b	.L84
.L87:
	ldr	r1, [r7, #0]
	ldr	r8, [r1, #32]
	b	.L91
.L104:
	add	r6, sp, #24
	mov	r0, r6
	bl	T.294
	ldr	r0, [r4, #32]
	mov	r1, r6
.LEHB17:
	bl	_ZNK12CIwSoundData7GetDataER8CIwArrayIs12CIwAllocatorIs15CIwMallocRouterIsEE17ReallocateDefaultIsS4_EE
	ldr	r0, [sp, #28]
	cmp	r8, #0
	mov	r3, r0, asl #1
	mov	r2, r3, asr #1
	mov	ip, #0
	ldr	r0, [sp, #16]
	ldr	r1, [sp, #24]
	mvnne	r3, #0
	moveq	r3, #1
	str	ip, [sp, #0]
	bl	s3eSoundChannelPlay
.LEHE17:
	b	.L126
.L127:
	mov	r1, r5
	ldr	r0, [r7, #0]
.LEHB18:
	bl	_ZN15CIwSoundManager11SetFreeInstEP12CIwSoundInst
	mov	r5, #0
	b	.L84
.L90:
	mov	r1, r5
	ldr	r0, [r7, #0]
	bl	_ZN15CIwSoundManager11SetFreeInstEP12CIwSoundInst
.LEHE18:
	mov	r5, sl
	b	.L84
.L121:
.L119:
	mov	sl, r0
	mov	r0, r6
	bl	_ZN8CIwArrayIs12CIwAllocatorIs15CIwMallocRouterIsEE17ReallocateDefaultIsS3_EED1Ev
	mov	r0, sl
.LEHB19:
	bl	__cxa_end_cleanup
.LEHE19:
.L123:
.L111:
	mov	r5, r0
	add	r0, sp, #24
	bl	_ZN8CIwArrayIa12CIwAllocatorIa15CIwMallocRouterIaEE17ReallocateDefaultIaS3_EED1Ev
	mov	r0, r5
.LEHB20:
	bl	__cxa_end_cleanup
.LEHE20:
.L122:
.L115:
	mov	r8, r0
	mov	r0, r6
	bl	_ZN8CIwArrayIs12CIwAllocatorIs15CIwMallocRouterIsEE17ReallocateDefaultIsS3_EED1Ev
	mov	r0, r8
.LEHB21:
	bl	__cxa_end_cleanup
.LEHE21:
.L132:
	.align	2
.L131:
	.word	g_IwSoundManager
	.word	_Z24_IwSoundSpec_EndSampleCBP21s3eSoundEndSampleInfoPv
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA1384:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE1384-.LLSDACSB1384
.LLSDACSB1384:
	.uleb128 .LEHB12-.LFB1384
	.uleb128 .LEHE12-.LEHB12
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB13-.LFB1384
	.uleb128 .LEHE13-.LEHB13
	.uleb128 .L123-.LFB1384
	.uleb128 0x0
	.uleb128 .LEHB14-.LFB1384
	.uleb128 .LEHE14-.LEHB14
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB15-.LFB1384
	.uleb128 .LEHE15-.LEHB15
	.uleb128 .L121-.LFB1384
	.uleb128 0x0
	.uleb128 .LEHB16-.LFB1384
	.uleb128 .LEHE16-.LEHB16
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB17-.LFB1384
	.uleb128 .LEHE17-.LEHB17
	.uleb128 .L122-.LFB1384
	.uleb128 0x0
	.uleb128 .LEHB18-.LFB1384
	.uleb128 .LEHE18-.LEHB18
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB19-.LFB1384
	.uleb128 .LEHE19-.LEHB19
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB20-.LFB1384
	.uleb128 .LEHE20-.LEHB20
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB21-.LFB1384
	.uleb128 .LEHE21-.LEHB21
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE1384:
	.fnend
	.size	_ZN12CIwSoundSpec4PlayEPK14CIwSoundParams, .-_ZN12CIwSoundSpec4PlayEPK14CIwSoundParams
	.global	__aeabi_idiv
	.section	.text._ZN12CIwSoundSpec5TraceEv,"ax",%progbits
	.align	2
	.global	_ZN12CIwSoundSpec5TraceEv
	.hidden	_ZN12CIwSoundSpec5TraceEv
	.type	_ZN12CIwSoundSpec5TraceEv, %function
_ZN12CIwSoundSpec5TraceEv:
	.fnstart
.LFB1387:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 176
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	r7, .L197
	.pad #180
	sub	sp, sp, #180
	str	r0, [sp, #36]
	ldmia	r7, {r0, r1}
	add	ip, r7, #8
	str	r0, [sp, #168]
	strb	r1, [sp, #172]
	ldmia	ip, {r0, r1}
	add	lr, sp, #168
	add	r4, sp, #160
	str	lr, [sp, #0]
	str	r0, [sp, #160]
	strb	r1, [sp, #164]
	str	r4, [sp, #4]
	ldr	r2, [sp, #36]
	ldrb	r3, [sp, #156]	@ zero_extendqisi2
	ldrb	r7, [sp, #140]	@ zero_extendqisi2
	ldr	r4, [r2, #32]
	bic	lr, r3, #1
	bic	ip, r7, #1
	ldr	r7, [r4, #20]
	strb	lr, [sp, #156]
	strb	ip, [sp, #140]
	ldrb	r1, [sp, #156]	@ zero_extendqisi2
	ldrb	r0, [sp, #140]	@ zero_extendqisi2
	bic	r2, r1, #2
	bic	r3, r0, #2
	mov	r0, #0
	strb	r2, [sp, #156]
	strb	r3, [sp, #140]
	str	r0, [sp, #144]
	str	r0, [sp, #152]
	str	r0, [sp, #148]
	str	r0, [sp, #128]
	str	r0, [sp, #136]
	str	r0, [sp, #132]
	ldr	r3, [r4, #32]
	cmp	r3, r0
	bne	.L192
	mov	r0, r4
	ldr	r5, [r4, #24]
.LEHB22:
	bl	_ZNK12CIwSoundData13GetBufferSizeEv
	mov	r6, r0
	mov	r0, r4
	bl	_ZNK12CIwSoundData13GetBufferSizeEv
	ldrb	r2, [sp, #140]	@ zero_extendqisi2
	ands	r3, r2, #1
	mov	r4, r0
	bne	.L140
	ldr	r9, [sp, #136]
	cmp	r9, #0
	str	r3, [sp, #132]
	beq	.L140
	ldr	r0, [sp, #128]
	mov	r1, #1
	str	r3, [sp, #136]
	bl	_ZN22CIwDefaultMallocRouter8BaseFreeEPvj
.LEHE22:
	ldrb	r2, [sp, #140]	@ zero_extendqisi2
.L140:
	orr	sl, r2, #1
	str	r5, [sp, #128]
	str	r6, [sp, #132]
	str	r4, [sp, #136]
	strb	sl, [sp, #140]
	mov	r5, #2
	b	.L141
.L192:
	cmp	r3, #1
	beq	.L138
	mov	r1, #1
.LEHB23:
	bl	_ZN22CIwDefaultMallocRouter8BaseFreeEPvj
.LEHE23:
	ldrb	r3, [sp, #156]	@ zero_extendqisi2
	mov	r0, #0
	tst	r3, #1
	str	r0, [sp, #128]
	beq	.L159
.L161:
	add	sp, sp, #180
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L138:
	mov	r0, r4
	ldr	r5, [r4, #24]
.LEHB24:
	bl	_ZNK12CIwSoundData13GetBufferSizeEv
	mov	r6, r0, lsr #1
	mov	r0, r4
	bl	_ZNK12CIwSoundData13GetBufferSizeEv
	ldrb	r2, [sp, #156]	@ zero_extendqisi2
	ands	r3, r2, #1
	mov	r4, r0
	bne	.L142
	ldr	fp, [sp, #152]
	cmp	fp, #0
	str	r3, [sp, #148]
	beq	.L142
	ldr	r0, [sp, #144]
	mov	r1, #2
	str	r3, [sp, #152]
	bl	_ZN22CIwDefaultMallocRouter8BaseFreeEPvj
.LEHE24:
	ldrb	r2, [sp, #156]	@ zero_extendqisi2
.L142:
	mov	r1, r4, lsr #1
	orr	r0, r2, #1
	str	r6, [sp, #148]
	str	r1, [sp, #152]
	strb	r0, [sp, #156]
	str	r5, [sp, #144]
	mov	r5, #512
.L141:
	mov	r6, #15
	mov	lr, #60
	mov	ip, #14
	str	lr, [sp, #20]
	ldr	r4, [sp, #36]
	ldr	r2, [sp, #144]
	str	ip, [sp, #32]
	str	r6, [sp, #28]
	ldr	r3, [sp, #128]
	ldr	fp, [r4, #32]
	str	r2, [sp, #24]
	str	r3, [sp, #16]
	ldr	r2, [sp, #20]
	str	r5, [sp, #12]
	add	r4, sp, #44
	mov	r5, r7
	mov	r1, r6
.L143:
	ldr	lr, [fp, #32]
	ldr	r7, [sp, #0]
	cmp	r1, #0
	ldr	sl, [sp, #4]
	mov	ip, r2, asl #16
	movge	sl, r7
	cmp	lr, #1
	mov	r9, ip, asr #16
	beq	.L193
	add	r1, sp, #12
	ldmia	r1, {r1, r7}	@ phole ldm
	ldrsb	r0, [r7, #0]
	bl	__aeabi_idiv
	mov	r6, r0, asl #16
	mov	r3, r6, lsr #16
.L179:
	mov	ip, r3, asl #16
	cmp	r9, ip, asr #16
	movge	r3, #0
	bge	.L181
	ldr	ip, [sp, #20]
	rsb	lr, ip, r3
	mov	r3, lr, asl #16
	mov	r3, r3, asr #16
	cmp	r3, #4
	movge	r3, #4
.L181:
	ldrb	r6, [sl, r3]	@ zero_extendqisi2
	mov	r2, #1
	cmp	r2, #0
	strb	r6, [r4, #0]
	mov	r7, r5
	mov	r6, r2
	beq	.L150
	ldr	r1, [fp, #32]
	ldr	lr, .L197+4
	cmp	r1, r2
	umull	r2, r3, lr, r5
	ldrne	ip, [sp, #16]
	mov	r1, r3, lsr #6
	ldreq	ip, [sp, #24]
	moveq	r1, r1, asl #1
	ldrnesb	r0, [ip, r1]
	ldreqsh	r0, [r1, ip]
	ldr	r1, [sp, #12]
	bl	__aeabi_idiv
	mov	r2, r0, asl #16
	mov	r2, r2, lsr #16
	mov	r3, r2, asl #16
	cmp	r9, r3, asr #16
	movge	r3, #0
	bge	.L185
	ldr	lr, [sp, #20]
	rsb	r3, lr, r2
	mov	ip, r3, asl #16
	mov	r3, ip, asr #16
	cmp	r3, #4
	movge	r3, #4
.L185:
	ldrb	r1, [sl, r3]	@ zero_extendqisi2
	strb	r1, [r4, r6]
	add	r6, r6, #1
	cmp	r6, #80
	add	r7, r7, r5
	bne	.L150
	b	.L186
.L172:
	ldr	lr, .L197+4
	umull	ip, r3, lr, r7
	ldr	r6, [sp, #16]
	mov	r2, r3, lsr #6
	ldrsb	r0, [r6, r2]
	ldr	r1, [sp, #12]
	bl	__aeabi_idiv
	mov	r0, r0, asl #16
	mov	r0, r0, lsr #16
.L187:
	mov	r3, r0, asl #16
	cmp	r9, r3, asr #16
	movge	r3, #0
	bge	.L189
	ldr	r2, [sp, #20]
	rsb	ip, r2, r0
	mov	lr, ip, asl #16
	mov	r3, lr, asr #16
	cmp	r3, #4
	movge	r3, #4
.L189:
	ldrb	r1, [sl, r3]	@ zero_extendqisi2
	add	r6, r8, #1
	cmp	r6, #80
	strb	r1, [r4, r8]
	add	r7, r7, r5
	beq	.L186
.L150:
	ldr	ip, .L197+4
	umull	r1, r3, ip, r7
	ldr	lr, [fp, #32]
	cmp	lr, #1
	mov	r1, r3, lsr #6
	ldreq	r2, [sp, #24]
	ldrne	r2, [sp, #16]
	moveq	r1, r1, asl #1
	ldreqsh	r0, [r1, r2]
	ldrnesb	r0, [r2, r1]
	ldr	r1, [sp, #12]
	bl	__aeabi_idiv
	mov	r8, r0, asl #16
	mov	r0, r8, lsr #16
	mov	r3, r0, asl #16
	cmp	r9, r3, asr #16
	movge	r3, #0
	bge	.L149
	ldr	r1, [sp, #20]
	rsb	r8, r1, r0
	mov	r2, r8, asl #16
	mov	r3, r2, asr #16
	cmp	r3, #4
	movge	r3, #4
.L149:
	ldrb	r3, [sl, r3]	@ zero_extendqisi2
	strb	r3, [r4, r6]
	ldr	r0, [fp, #32]
	cmp	r0, #1
	add	r8, r6, #1
	add	r7, r7, r5
	bne	.L172
	ldr	r2, .L197+4
	umull	ip, r3, r2, r7
	ldr	r1, [sp, #24]
	mov	r0, r3, lsr #6
	mov	r6, r0, asl #1
	ldrsh	r0, [r6, r1]
	ldr	r1, [sp, #12]
	bl	__aeabi_idiv
	mov	ip, r0, asl #16
	mov	r0, ip, lsr #16
	b	.L187
.L186:
	ldr	lr, [sp, #28]
	mov	ip, #0
	cmp	lr, #0
	strb	ip, [sp, #124]
	beq	.L194
	ldr	r2, [sp, #32]
	cmn	r2, #16
	blt	.L195
.L152:
	ldr	r3, [sp, #20]
	ldr	r1, [sp, #28]
	ldr	r0, [sp, #32]
	sub	r6, r3, #4
	mov	r9, r6, asl #16
	mov	r7, r9, lsr #16
	sub	r1, r1, #1
	sub	sl, r0, #1
	str	r1, [sp, #28]
	str	sl, [sp, #32]
	str	r7, [sp, #20]
	mov	r2, r7
	b	.L143
.L193:
	ldr	r1, [sp, #24]
	ldrsh	r0, [r1, #0]
	ldr	r1, [sp, #12]
	bl	__aeabi_idiv
	mov	r2, r0, asl #16
	mov	r3, r2, lsr #16
	b	.L179
.L194:
	mov	r0, r4
	mov	r1, #45
	mov	r2, #80
	bl	memset
	ldr	fp, [sp, #28]
	mov	r7, #48
	strb	r7, [sp, #44]
	strb	fp, [sp, #124]
	ldr	r6, [sp, #36]
	ldr	sl, [sp, #144]
	ldr	r9, [sp, #128]
	ldr	fp, [r6, #32]
	str	sl, [sp, #24]
	str	r9, [sp, #16]
	b	.L152
.L195:
	ldrb	r1, [sp, #140]	@ zero_extendqisi2
	tst	r1, #1
	beq	.L196
.L154:
	ldrb	lr, [sp, #156]	@ zero_extendqisi2
	tst	lr, #1
	bne	.L161
.L159:
	ldr	r0, [sp, #144]
	mov	r1, #2
.LEHB25:
	bl	_ZN22CIwDefaultMallocRouter8BaseFreeEPvj
.LEHE25:
	b	.L161
.L196:
	ldr	r0, [sp, #16]
	mov	r1, #1
.LEHB26:
	bl	_ZN22CIwDefaultMallocRouter8BaseFreeEPvj
.LEHE26:
	mov	ip, #0
	str	ip, [sp, #128]
	b	.L154
.L163:
	mov	r4, r0
.L160:
	add	r0, sp, #144
	bl	_ZN8CIwArrayIs12CIwAllocatorIs15CIwMallocRouterIsEE17ReallocateDefaultIsS3_EED1Ev
	mov	r0, r4
.LEHB27:
	bl	__cxa_end_cleanup
.LEHE27:
.L162:
.L155:
	mov	r4, r0
	add	r0, sp, #128
	bl	_ZN8CIwArrayIa12CIwAllocatorIa15CIwMallocRouterIaEE17ReallocateDefaultIaS3_EED1Ev
	b	.L160
.L198:
	.align	2
.L197:
	.word	.LANCHOR0+68
	.word	-858993459
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA1387:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE1387-.LLSDACSB1387
.LLSDACSB1387:
	.uleb128 .LEHB22-.LFB1387
	.uleb128 .LEHE22-.LEHB22
	.uleb128 .L162-.LFB1387
	.uleb128 0x0
	.uleb128 .LEHB23-.LFB1387
	.uleb128 .LEHE23-.LEHB23
	.uleb128 .L163-.LFB1387
	.uleb128 0x0
	.uleb128 .LEHB24-.LFB1387
	.uleb128 .LEHE24-.LEHB24
	.uleb128 .L162-.LFB1387
	.uleb128 0x0
	.uleb128 .LEHB25-.LFB1387
	.uleb128 .LEHE25-.LEHB25
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB26-.LFB1387
	.uleb128 .LEHE26-.LEHB26
	.uleb128 .L163-.LFB1387
	.uleb128 0x0
	.uleb128 .LEHB27-.LFB1387
	.uleb128 .LEHE27-.LEHB27
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE1387:
	.fnend
	.size	_ZN12CIwSoundSpec5TraceEv, .-_ZN12CIwSoundSpec5TraceEv
	.hidden	_ZTV12CIwSoundSpec
	.global	_ZTV12CIwSoundSpec
	.hidden	_ZTS12CIwSoundSpec
	.global	_ZTS12CIwSoundSpec
	.hidden	_ZTI12CIwSoundSpec
	.global	_ZTI12CIwSoundSpec
	.section	.rodata
	.align	3
	.set	.LANCHOR0,. + 0
	.type	_ZTV12CIwSoundSpec, %object
	.size	_ZTV12CIwSoundSpec, 68
_ZTV12CIwSoundSpec:
	.word	0
	.word	_ZTI12CIwSoundSpec
	.word	_ZN12CIwSoundSpecD1Ev
	.word	_ZN12CIwSoundSpecD0Ev
	.word	_ZN10CIwManaged9ParseOpenEP16CIwTextParserITX
	.word	_ZN12CIwSoundSpec10ParseCloseEP16CIwTextParserITX
	.word	_ZN12CIwSoundSpec14ParseAttributeEP16CIwTextParserITXPKc
	.word	_ZN12CIwSoundSpec9SerialiseEv
	.word	_ZN12CIwSoundSpec7ResolveEv
	.word	_ZN10CIwManaged15ParseCloseChildEP16CIwTextParserITXPS_
	.word	_ZN10CIwManaged11HandleEventEP8CIwEvent
	.word	_ZN10CIwManaged7SetNameEPKc
	.word	_ZNK12CIwSoundSpec12GetClassNameEv
	.word	_ZN10CIwManaged11DebugRenderEv
	.word	_ZN18CIwManagedRefCount17DebugAddMenuItemsEP7CIwMenu
	.word	_ZN18CIwManagedRefCount8_ReplaceEP10CIwManaged
	.word	_ZN11CIwResource10ApplyScaleEi
	.type	_ZZN12CIwSoundSpec5TraceEvE5C.192, %object
	.size	_ZZN12CIwSoundSpec5TraceEvE5C.192, 5
_ZZN12CIwSoundSpec5TraceEvE5C.192:
	.byte	32
	.byte	46
	.byte	58
	.byte	39
	.byte	124
	.space	3
	.type	_ZZN12CIwSoundSpec5TraceEvE5C.193, %object
	.size	_ZZN12CIwSoundSpec5TraceEvE5C.193, 5
_ZZN12CIwSoundSpec5TraceEvE5C.193:
	.byte	124
	.byte	33
	.byte	58
	.byte	39
	.byte	32
	.space	3
	.type	_ZTS12CIwSoundSpec, %object
	.size	_ZTS12CIwSoundSpec, 15
_ZTS12CIwSoundSpec:
	.ascii	"12CIwSoundSpec\000"
	.space	1
	.type	_ZTI12CIwSoundSpec, %object
	.size	_ZTI12CIwSoundSpec, 12
_ZTI12CIwSoundSpec:
	.word	_ZTVN10__cxxabiv120__si_class_type_infoE+8
	.word	_ZTS12CIwSoundSpec
	.word	_ZTI11CIwResource
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"CIwSoundSpec\000"
	.space	3
.LC1:
	.ascii	"CIwSoundData\000"
	.space	3
.LC2:
	.ascii	"CIwSoundGroup\000"
	.hidden	_ZTV12CIwSoundSpec
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
