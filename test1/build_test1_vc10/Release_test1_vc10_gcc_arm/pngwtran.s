	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"pngwtran.c"
	.section	.text.png_do_pack,"ax",%progbits
	.align	2
	.global	png_do_pack
	.hidden	png_do_pack
	.type	png_do_pack, %function
png_do_pack:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r3, [r0, #8]
	bic	ip, r3, #-16777216
	bic	r3, ip, #255
	cmp	r3, #67584
	stmfd	sp!, {r4, r5, r6, r7}
	beq	.L135
.L19:
	ldmfd	sp!, {r4, r5, r6, r7}
	bx	lr
.L135:
	cmp	r2, #2
	beq	.L5
	cmp	r2, #4
	beq	.L6
	cmp	r2, #1
	beq	.L4
.L133:
	ldr	r3, [r0, #0]
.L3:
	ldrb	ip, [r0, #10]	@ zero_extendqisi2
	mul	ip, r2, ip
	and	r1, ip, #255
	cmp	r1, #7
	mulls	r3, r1, r3
	strb	r1, [r0, #11]
	movhi	r1, r1, lsr #3
	mulhi	r3, r1, r3
	addls	r3, r3, #7
	movls	r3, r3, lsr #3
	strb	r2, [r0, #9]
	str	r3, [r0, #4]
	b	.L19
.L6:
	ldr	r3, [r0, #0]
	cmp	r3, #0
	beq	.L3
	ldrb	r5, [r1, #0]	@ zero_extendqisi2
	mov	r4, #1
	and	r6, r5, #15
	sub	ip, r3, #1
	cmp	r3, r4
	and	r7, ip, #3
	mov	r5, r6, asl #4
	mov	ip, #0
	mov	r6, r1
	bls	.L99
	cmp	r7, #0
	beq	.L16
	cmp	r7, #1
	beq	.L88
	cmp	r7, #2
	beq	.L89
	ldrb	r7, [r1, r4]	@ zero_extendqisi2
	and	r6, r7, #15
	orr	r5, r5, r6
	mov	r6, r1
	strb	r5, [r6], #1
	add	r4, r4, #1
	mov	r5, ip
	mov	ip, #4
.L89:
	ldrb	r7, [r1, r4]	@ zero_extendqisi2
	and	r7, r7, #15
	orr	r5, r5, r7, asl ip
	cmp	ip, #0
	streqb	r5, [r6], #1
	subne	ip, ip, #4
	moveq	r5, ip
	add	r4, r4, #1
	moveq	ip, #4
.L88:
	ldrb	r7, [r1, r4]	@ zero_extendqisi2
	and	r7, r7, #15
	orr	r5, r5, r7, asl ip
	cmp	ip, #0
	add	r4, r4, #1
	streqb	r5, [r6], #1
	subne	ip, ip, #4
	moveq	r5, ip
	moveq	ip, #4
	cmp	r3, r4
	bls	.L99
.L16:
	ldrb	r7, [r1, r4]	@ zero_extendqisi2
	and	r7, r7, #15
	orr	r5, r5, r7, asl ip
	cmp	ip, #0
	streqb	r5, [r6], #1
	add	r4, r4, #1
	ldrb	r7, [r1, r4]	@ zero_extendqisi2
	moveq	r5, ip
	subne	ip, ip, #4
	moveq	ip, #4
	cmp	ip, #0
	and	r7, r7, #15
	orr	r5, r5, r7, asl ip
	subne	ip, ip, #4
	streqb	r5, [r6], #1
	moveq	r5, ip
	moveq	ip, #4
.L115:
	add	r7, r4, #1
	ldrb	r7, [r1, r7]	@ zero_extendqisi2
	and	r7, r7, #15
	orr	r5, r5, r7, asl ip
	cmp	ip, #0
	streqb	r5, [r6], #1
	add	r7, r4, #2
	ldrb	r7, [r1, r7]	@ zero_extendqisi2
	moveq	r5, ip
	subne	ip, ip, #4
	moveq	ip, #4
	and	r7, r7, #15
	orr	r5, r5, r7, asl ip
	cmp	ip, #0
	add	r4, r4, #3
	streqb	r5, [r6], #1
	subne	ip, ip, #4
	moveq	r5, ip
	moveq	ip, #4
	cmp	r3, r4
	bhi	.L16
.L99:
	cmp	ip, #4
	strneb	r5, [r6, #0]
	b	.L133
.L4:
	ldr	r3, [r0, #0]
	cmp	r3, #0
	beq	.L3
	ldrb	r4, [r1, #0]	@ zero_extendqisi2
	sub	r7, r3, #1
	cmp	r4, #0
	and	r7, r7, #3
	mov	r6, r1
	mov	ip, #0
	bne	.L136
.L60:
	mov	r4, #1
	cmp	r3, r4
	mov	r5, #64
	bls	.L120
	cmp	r7, #0
	beq	.L10
	cmp	r7, r4
	beq	.L91
	cmp	r7, #2
	beq	.L92
	ldrb	r7, [r1, r4]	@ zero_extendqisi2
	cmp	r7, #0
	orrne	ip, ip, r5
	cmp	r5, #1
	strleb	ip, [r6], #1
	movle	r5, #128
	movle	ip, #0
	movgt	r5, r5, asr #1
	add	r4, r4, #1
.L92:
	ldrb	r7, [r1, r4]	@ zero_extendqisi2
	cmp	r7, #0
	orrne	ip, ip, r5
	cmp	r5, #1
	strleb	ip, [r6], #1
	movle	r5, #128
	movle	ip, #0
	movgt	r5, r5, asr #1
	add	r4, r4, #1
.L91:
	ldrb	r7, [r1, r4]	@ zero_extendqisi2
	cmp	r7, #0
	orrne	ip, ip, r5
	add	r4, r4, #1
	cmp	r5, #1
	strleb	ip, [r6], #1
	movle	r5, #128
	movle	ip, #0
	movgt	r5, r5, asr #1
	cmp	r3, r4
	bls	.L120
.L10:
	ldrb	r7, [r1, r4]	@ zero_extendqisi2
	cmp	r7, #0
	orrne	ip, ip, r5
	cmp	r5, #1
	strleb	ip, [r6], #1
	add	r4, r4, #1
	ldrb	r7, [r1, r4]	@ zero_extendqisi2
	movgt	r5, r5, asr #1
	movle	r5, #128
	movle	ip, #0
	cmp	r7, #0
	orrne	ip, ip, r5
.L75:
	cmp	r5, #1
	strleb	ip, [r6], #1
	add	r7, r4, #1
	ldrb	r7, [r1, r7]	@ zero_extendqisi2
	movle	r5, #128
	movle	ip, #0
	movgt	r5, r5, asr #1
	cmp	r7, #0
	orrne	ip, ip, r5
	cmp	r5, #1
	strleb	ip, [r6], #1
	add	r7, r4, #2
	ldrb	r7, [r1, r7]	@ zero_extendqisi2
	movle	r5, #128
	movle	ip, #0
	movgt	r5, r5, asr #1
	cmp	r7, #0
	orrne	ip, ip, r5
	add	r4, r4, #3
	cmp	r5, #1
	strleb	ip, [r6], #1
	movle	r5, #128
	movle	ip, #0
	movgt	r5, r5, asr #1
	cmp	r3, r4
	bhi	.L10
.L120:
	cmp	r5, #128
	beq	.L133
	strb	ip, [r6, #0]
	ldr	r3, [r0, #0]
	b	.L3
.L5:
	ldr	r3, [r0, #0]
	cmp	r3, #0
	beq	.L3
	ldrb	ip, [r1, #0]	@ zero_extendqisi2
	mov	r4, #1
	and	r5, ip, #3
	sub	r6, r3, #1
	cmp	r3, r4
	and	r7, r6, #3
	mov	r5, r5, asl #6
	mov	r6, r1
	mov	ip, #4
	bls	.L96
	cmp	r7, #0
	beq	.L13
	cmp	r7, #1
	beq	.L85
	cmp	r7, #2
	ldrneb	r7, [r1, r4]	@ zero_extendqisi2
	andne	r7, r7, #3
	orrne	r5, r5, r7, asl ip
	addne	r4, r4, #1
	ldrb	r7, [r1, r4]	@ zero_extendqisi2
	movne	ip, r2
	and	r7, r7, #3
	orr	r5, r5, r7, asl ip
	cmp	ip, #0
	streqb	r5, [r6], #1
	subne	ip, ip, #2
	moveq	r5, ip
	add	r4, r4, #1
	moveq	ip, #6
.L85:
	ldrb	r7, [r1, r4]	@ zero_extendqisi2
	and	r7, r7, #3
	orr	r5, r5, r7, asl ip
	cmp	ip, #0
	add	r4, r4, #1
	streqb	r5, [r6], #1
	subne	ip, ip, #2
	moveq	r5, ip
	moveq	ip, #6
	cmp	r3, r4
	bls	.L96
.L13:
	ldrb	r7, [r1, r4]	@ zero_extendqisi2
	and	r7, r7, #3
	orr	r5, r5, r7, asl ip
	cmp	ip, #0
	streqb	r5, [r6], #1
	add	r4, r4, #1
	ldrb	r7, [r1, r4]	@ zero_extendqisi2
	moveq	r5, ip
	subne	ip, ip, #2
	moveq	ip, #6
	cmp	ip, #0
	and	r7, r7, #3
	orr	r5, r5, r7, asl ip
	subne	ip, ip, #2
	streqb	r5, [r6], #1
	moveq	r5, ip
	moveq	ip, #6
.L106:
	add	r7, r4, #1
	ldrb	r7, [r1, r7]	@ zero_extendqisi2
	and	r7, r7, #3
	orr	r5, r5, r7, asl ip
	cmp	ip, #0
	streqb	r5, [r6], #1
	add	r7, r4, #2
	ldrb	r7, [r1, r7]	@ zero_extendqisi2
	moveq	r5, ip
	subne	ip, ip, #2
	moveq	ip, #6
	and	r7, r7, #3
	orr	r5, r5, r7, asl ip
	cmp	ip, #0
	add	r4, r4, #3
	streqb	r5, [r6], #1
	subne	ip, ip, #2
	moveq	r5, ip
	moveq	ip, #6
	cmp	r3, r4
	bhi	.L13
.L96:
	cmp	ip, #6
	beq	.L133
	strb	r5, [r6, #0]
	ldr	r3, [r0, #0]
	b	.L3
.L136:
	mov	ip, #128
	b	.L60
	.size	png_do_pack, .-png_do_pack
	.global	__aeabi_uidivmod
	.section	.text.png_do_shift,"ax",%progbits
	.align	2
	.global	png_do_shift
	.hidden	png_do_shift
	.type	png_do_shift, %function
png_do_shift:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 40
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldrb	r3, [r0, #8]	@ zero_extendqisi2
	cmp	r3, #3
	sub	sp, sp, #44
	mov	r4, r1
	beq	.L164
	tst	r3, #2
	bne	.L169
	ldrb	ip, [r2, #3]	@ zero_extendqisi2
	ldrb	r1, [r0, #9]	@ zero_extendqisi2
	rsb	r5, ip, r1
	str	r5, [sp, #24]
	str	ip, [sp, #8]
	mov	r6, #2
	mov	ip, #1
.L140:
	tst	r3, #4
	ldrneb	r3, [r2, #4]	@ zero_extendqisi2
	addne	r5, sp, #40
	addne	ip, r5, ip, asl #2
	rsbne	r5, r3, r1
	moveq	r6, ip
	strne	r3, [ip, #-32]
	strne	r5, [ip, #-16]
	cmp	r1, #7
	bhi	.L143
	ldrb	r3, [r2, #3]	@ zero_extendqisi2
	cmp	r3, #1
	ldr	fp, [r0, #4]
	beq	.L170
.L144:
	cmp	r1, #4
	beq	.L171
.L146:
	mov	sl, #255
.L145:
	cmp	fp, #0
	beq	.L164
	ldr	r9, [sp, #24]
	ldr	r5, [sp, #8]
	rsb	r3, r9, #0
	rsb	r1, r5, r9
	stmia	sp, {r1, r3}	@ phole stm
	mov	r0, #0
	rsb	r2, r5, #0
.L151:
	mov	ip, #0
	cmp	r9, r2
	ldrb	r7, [r4, r0]	@ zero_extendqisi2
	strb	ip, [r4, r0]
	ble	.L147
	ldmia	sp, {r1, ip}	@ phole ldm
	mov	r3, r9
	mov	r6, #0
	b	.L150
.L172:
	ldrb	r6, [r4, r0]	@ zero_extendqisi2
.L150:
	cmp	r3, #0
	andle	r8, sl, r7, asr ip
	orrgt	r6, r6, r7, asl r3
	orrle	r6, r6, r8
	add	r1, r1, r2
	strb	r6, [r4, r0]
	add	r6, r1, r5
	cmp	r6, r2
	add	r3, r2, r3
	add	ip, ip, r5
	bgt	.L172
.L147:
	add	r0, r0, #1
	cmp	fp, r0
	bhi	.L151
.L164:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L169:
	ldrb	r1, [r0, #9]	@ zero_extendqisi2
	ldrb	r6, [r2, #0]	@ zero_extendqisi2
	ldrb	ip, [r2, #2]	@ zero_extendqisi2
	ldrb	r5, [r2, #1]	@ zero_extendqisi2
	rsb	sl, r6, r1
	rsb	r7, ip, r1
	rsb	r8, r5, r1
	str	r6, [sp, #8]
	str	ip, [sp, #16]
	str	sl, [sp, #24]
	str	r8, [sp, #28]
	str	r5, [sp, #12]
	str	r7, [sp, #32]
	mov	ip, #3
	mov	r6, #4
	b	.L140
.L143:
	cmp	r1, #8
	beq	.L173
	ldr	fp, [r0, #0]
	muls	fp, r6, fp
	beq	.L164
	mov	r5, #0
	mov	r9, r6
.L163:
	mov	r0, r5
	mov	r1, r9
	bl	__aeabi_uidivmod
	add	r6, sp, #40
	add	r1, r6, r1, asl #2
	ldr	r6, [r1, #-32]
	ldr	r3, [r1, #-16]
	rsb	r1, r6, #0
	cmp	r1, r3
	movge	r3, #0
	ldrb	r2, [r4, #1]	@ zero_extendqisi2
	ldrb	r0, [r4, #0]	@ zero_extendqisi2
	movge	r2, r3
	bge	.L159
	add	r8, r2, r0, asl #8
	mov	r7, r8, asl #16
	mov	r7, r7, lsr #16
	rsb	ip, r6, r3
	rsb	r0, r3, #0
	mov	r2, #0
.L162:
	orr	sl, r2, r7, asr r0
	orr	lr, r2, r7, asl r3
	add	ip, ip, r1
	cmp	r3, #0
	mov	r2, lr, asl #16
	mov	sl, sl, asl #16
	add	r8, ip, r6
	movgt	r2, r2, lsr #16
	movle	r2, sl, lsr #16
	cmp	r1, r8
	add	r3, r1, r3
	add	r0, r0, r6
	blt	.L162
	and	r3, r2, #255
	mov	r2, r2, lsr #8
.L159:
	add	r5, r5, #1
	cmp	fp, r5
	strb	r2, [r4, #0]
	strb	r3, [r4, #1]
	bls	.L164
	add	r4, r4, #2
	b	.L163
.L171:
	cmp	r3, #3
	moveq	sl, #17
	bne	.L146
	b	.L145
.L170:
	cmp	r1, #2
	moveq	sl, #85
	bne	.L144
	b	.L145
.L173:
	ldr	r8, [r0, #0]
	muls	r8, r6, r8
	beq	.L164
	mov	r5, #0
	mov	r7, r5
.L157:
	mov	r0, r5
	mov	r1, r6
	bl	__aeabi_uidivmod
	add	r2, sp, #40
	add	r0, r2, r1, asl #2
	ldr	ip, [r0, #-32]
	ldr	r3, [r0, #-16]
	rsb	r2, ip, #0
	cmp	r3, r2
	ldrb	r9, [r4, r5]	@ zero_extendqisi2
	strb	r7, [r4, r5]
	ble	.L153
	rsb	r1, ip, r3
	rsb	r0, r3, #0
	mov	sl, #0
	b	.L156
.L174:
	ldrb	sl, [r4, r5]	@ zero_extendqisi2
.L156:
	cmp	r3, #0
	orrgt	sl, sl, r9, asl r3
	orrle	sl, sl, r9, asr r0
	add	r1, r1, r2
	strb	sl, [r4, r5]
	add	sl, ip, r1
	cmp	r2, sl
	add	r3, r3, r2
	add	r0, r0, ip
	blt	.L174
.L153:
	add	r5, r5, #1
	cmp	r8, r5
	bhi	.L157
	b	.L164
	.size	png_do_shift, .-png_do_shift
	.section	.text.png_do_write_swap_alpha,"ax",%progbits
	.align	2
	.global	png_do_write_swap_alpha
	.hidden	png_do_write_swap_alpha
	.type	png_do_write_swap_alpha, %function
png_do_write_swap_alpha:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, r5, r6, r7, r8, sl}
	ldrb	r3, [r0, #8]	@ zero_extendqisi2
	cmp	r3, #6
	beq	.L241
	cmp	r3, #4
	beq	.L242
.L184:
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl}
	bx	lr
.L242:
	ldrb	ip, [r0, #9]	@ zero_extendqisi2
	cmp	ip, #8
	beq	.L243
	ldr	ip, [r0, #0]
	cmp	ip, #0
	beq	.L184
	ldrb	r0, [r1, #0]	@ zero_extendqisi2
	ldrb	r2, [r1, #1]	@ zero_extendqisi2
	ldrb	r5, [r1, #2]	@ zero_extendqisi2
	ldrb	r4, [r1, #3]	@ zero_extendqisi2
	sub	r3, ip, #1
	cmp	ip, #1
	strb	r0, [r1, #2]
	strb	r5, [r1, #0]
	strb	r4, [r1, #1]
	strb	r2, [r1, #3]
	and	r3, r3, #3
	add	r1, r1, #4
	mov	r0, #1
	bls	.L184
	cmp	r3, #0
	beq	.L183
	cmp	r3, #1
	beq	.L239
	cmp	r3, #2
	beq	.L240
	ldrb	r0, [r1, #3]	@ zero_extendqisi2
	ldrb	r2, [r1, #0]	@ zero_extendqisi2
	ldrb	r5, [r1, #1]	@ zero_extendqisi2
	ldrb	r4, [r1, #2]	@ zero_extendqisi2
	strb	r0, [r1, #1]
	strb	r4, [r1, #0]
	strb	r2, [r1, #2]
	strb	r5, [r1, #3]
	mov	r0, #2
	add	r1, r1, #4
.L240:
	ldrb	r2, [r1, #0]	@ zero_extendqisi2
	ldrb	r3, [r1, #1]	@ zero_extendqisi2
	ldrb	r5, [r1, #2]	@ zero_extendqisi2
	ldrb	r4, [r1, #3]	@ zero_extendqisi2
	strb	r5, [r1, #0]
	strb	r4, [r1, #1]
	strb	r2, [r1, #2]
	strb	r3, [r1, #3]
	add	r0, r0, #1
	add	r1, r1, #4
.L239:
	ldrb	r2, [r1, #0]	@ zero_extendqisi2
	ldrb	r3, [r1, #1]	@ zero_extendqisi2
	ldrb	r5, [r1, #2]	@ zero_extendqisi2
	ldrb	r4, [r1, #3]	@ zero_extendqisi2
	add	r0, r0, #1
	cmp	ip, r0
	strb	r5, [r1, #0]
	strb	r4, [r1, #1]
	strb	r2, [r1, #2]
	strb	r3, [r1, #3]
	add	r1, r1, #4
	bls	.L184
.L183:
	ldrb	r6, [r1, #0]	@ zero_extendqisi2
	ldrb	r3, [r1, #1]	@ zero_extendqisi2
	ldrb	r5, [r1, #2]	@ zero_extendqisi2
	ldrb	r2, [r1, #3]	@ zero_extendqisi2
	strb	r5, [r1, #0]
	strb	r3, [r1, #3]
	strb	r2, [r1, #1]
	strb	r6, [r1, #2]
	add	r3, r1, #4
	ldrb	r4, [r3, #2]	@ zero_extendqisi2
	ldrb	r5, [r1, #4]	@ zero_extendqisi2
	ldrb	r6, [r3, #1]	@ zero_extendqisi2
	strb	r4, [r1, #4]
	ldrb	r2, [r3, #3]	@ zero_extendqisi2
	strb	r5, [r3, #2]
	strb	r2, [r3, #1]
	strb	r6, [r3, #3]
	add	r2, r3, #4
	ldrb	r6, [r2, #2]	@ zero_extendqisi2
	ldrb	r5, [r2, #1]	@ zero_extendqisi2
	ldrb	r4, [r3, #4]	@ zero_extendqisi2
	strb	r6, [r3, #4]
	ldrb	r3, [r2, #3]	@ zero_extendqisi2
	strb	r4, [r2, #2]
	strb	r3, [r2, #1]
	strb	r5, [r2, #3]
	add	r3, r1, #12
	ldrb	r4, [r3, #2]	@ zero_extendqisi2
	ldrb	r2, [r1, #12]	@ zero_extendqisi2
	ldrb	r5, [r3, #1]	@ zero_extendqisi2
	strb	r4, [r1, #12]
	add	r0, r0, #4
	ldrb	r4, [r3, #3]	@ zero_extendqisi2
	cmp	ip, r0
	strb	r5, [r3, #3]
	strb	r4, [r3, #1]
	strb	r2, [r3, #2]
	add	r1, r1, #16
	bhi	.L183
	b	.L184
.L241:
	ldrb	r3, [r0, #9]	@ zero_extendqisi2
	cmp	r3, #8
	ldr	ip, [r0, #0]
	beq	.L244
	cmp	ip, #0
	beq	.L184
	ldrb	r0, [r1, #0]	@ zero_extendqisi2
	ldrb	r2, [r1, #1]	@ zero_extendqisi2
	ldrb	sl, [r1, #2]	@ zero_extendqisi2
	ldrb	r8, [r1, #3]	@ zero_extendqisi2
	ldrb	r7, [r1, #4]	@ zero_extendqisi2
	ldrb	r6, [r1, #5]	@ zero_extendqisi2
	ldrb	r5, [r1, #6]	@ zero_extendqisi2
	ldrb	r4, [r1, #7]	@ zero_extendqisi2
	sub	r3, ip, #1
	cmp	ip, #1
	strb	r0, [r1, #6]
	strb	sl, [r1, #0]
	strb	r8, [r1, #1]
	strb	r7, [r1, #2]
	strb	r6, [r1, #3]
	strb	r5, [r1, #4]
	strb	r4, [r1, #5]
	strb	r2, [r1, #7]
	and	r3, r3, #3
	add	r1, r1, #8
	mov	r0, #1
	bls	.L184
	cmp	r3, #0
	beq	.L180
	cmp	r3, #1
	beq	.L235
	cmp	r3, #2
	beq	.L236
	ldrb	r0, [r1, #7]	@ zero_extendqisi2
	ldrb	r3, [r1, #0]	@ zero_extendqisi2
	ldrb	sl, [r1, #1]	@ zero_extendqisi2
	ldrb	r8, [r1, #2]	@ zero_extendqisi2
	ldrb	r7, [r1, #3]	@ zero_extendqisi2
	ldrb	r6, [r1, #4]	@ zero_extendqisi2
	ldrb	r5, [r1, #5]	@ zero_extendqisi2
	ldrb	r4, [r1, #6]	@ zero_extendqisi2
	strb	r0, [r1, #5]
	strb	r8, [r1, #0]
	strb	r7, [r1, #1]
	strb	r6, [r1, #2]
	strb	r5, [r1, #3]
	strb	r4, [r1, #4]
	strb	r3, [r1, #6]
	strb	sl, [r1, #7]
	mov	r0, #2
	add	r1, r1, #8
.L236:
	ldrb	r2, [r1, #0]	@ zero_extendqisi2
	ldrb	r3, [r1, #1]	@ zero_extendqisi2
	ldrb	sl, [r1, #2]	@ zero_extendqisi2
	ldrb	r8, [r1, #3]	@ zero_extendqisi2
	ldrb	r7, [r1, #4]	@ zero_extendqisi2
	ldrb	r6, [r1, #5]	@ zero_extendqisi2
	ldrb	r5, [r1, #6]	@ zero_extendqisi2
	ldrb	r4, [r1, #7]	@ zero_extendqisi2
	strb	sl, [r1, #0]
	strb	r8, [r1, #1]
	strb	r7, [r1, #2]
	strb	r6, [r1, #3]
	strb	r5, [r1, #4]
	strb	r4, [r1, #5]
	strb	r2, [r1, #6]
	strb	r3, [r1, #7]
	add	r0, r0, #1
	add	r1, r1, #8
.L235:
	ldrb	r2, [r1, #0]	@ zero_extendqisi2
	ldrb	r3, [r1, #1]	@ zero_extendqisi2
	ldrb	sl, [r1, #2]	@ zero_extendqisi2
	ldrb	r8, [r1, #3]	@ zero_extendqisi2
	ldrb	r7, [r1, #4]	@ zero_extendqisi2
	ldrb	r6, [r1, #5]	@ zero_extendqisi2
	ldrb	r5, [r1, #6]	@ zero_extendqisi2
	ldrb	r4, [r1, #7]	@ zero_extendqisi2
	add	r0, r0, #1
	cmp	ip, r0
	strb	sl, [r1, #0]
	strb	r8, [r1, #1]
	strb	r7, [r1, #2]
	strb	r6, [r1, #3]
	strb	r5, [r1, #4]
	strb	r4, [r1, #5]
	strb	r2, [r1, #6]
	strb	r3, [r1, #7]
	add	r1, r1, #8
	bls	.L184
.L180:
	ldrb	r2, [r1, #0]	@ zero_extendqisi2
	ldrb	r3, [r1, #1]	@ zero_extendqisi2
	ldrb	sl, [r1, #2]	@ zero_extendqisi2
	ldrb	r8, [r1, #3]	@ zero_extendqisi2
	ldrb	r7, [r1, #4]	@ zero_extendqisi2
	ldrb	r6, [r1, #5]	@ zero_extendqisi2
	ldrb	r5, [r1, #6]	@ zero_extendqisi2
	ldrb	r4, [r1, #7]	@ zero_extendqisi2
	strb	sl, [r1, #0]
	strb	r4, [r1, #5]
	strb	r3, [r1, #7]
	strb	r8, [r1, #1]
	strb	r7, [r1, #2]
	strb	r6, [r1, #3]
	strb	r5, [r1, #4]
	strb	r2, [r1, #6]
	add	r3, r1, #8
	ldrb	r5, [r3, #2]	@ zero_extendqisi2
	ldrb	r4, [r1, #8]	@ zero_extendqisi2
	ldrb	r2, [r3, #1]	@ zero_extendqisi2
	strb	r5, [r1, #8]
	ldrb	sl, [r3, #3]	@ zero_extendqisi2
	ldrb	r8, [r3, #4]	@ zero_extendqisi2
	ldrb	r7, [r3, #5]	@ zero_extendqisi2
	ldrb	r6, [r3, #6]	@ zero_extendqisi2
	ldrb	r5, [r3, #7]	@ zero_extendqisi2
	strb	r8, [r3, #2]
	strb	r7, [r3, #3]
	strb	r6, [r3, #4]
	strb	r4, [r3, #6]
	strb	r2, [r3, #7]
	strb	sl, [r3, #1]
	strb	r5, [r3, #5]
	add	r2, r3, #8
	ldrb	r5, [r2, #2]	@ zero_extendqisi2
	ldrb	r4, [r3, #8]	@ zero_extendqisi2
	ldrb	sl, [r2, #1]	@ zero_extendqisi2
	strb	r5, [r3, #8]
	ldrb	r8, [r2, #3]	@ zero_extendqisi2
	ldrb	r7, [r2, #4]	@ zero_extendqisi2
	ldrb	r6, [r2, #5]	@ zero_extendqisi2
	ldrb	r5, [r2, #6]	@ zero_extendqisi2
	ldrb	r3, [r2, #7]	@ zero_extendqisi2
	strb	r8, [r2, #1]
	strb	r3, [r2, #5]
	strb	sl, [r2, #7]
	strb	r7, [r2, #2]
	strb	r6, [r2, #3]
	strb	r5, [r2, #4]
	strb	r4, [r2, #6]
	add	r3, r1, #24
	ldrb	r6, [r3, #2]	@ zero_extendqisi2
	ldrb	r2, [r1, #24]	@ zero_extendqisi2
	ldrb	sl, [r3, #1]	@ zero_extendqisi2
	strb	r6, [r1, #24]
	add	r0, r0, #4
	ldrb	r8, [r3, #3]	@ zero_extendqisi2
	ldrb	r7, [r3, #4]	@ zero_extendqisi2
	ldrb	r6, [r3, #5]	@ zero_extendqisi2
	ldrb	r5, [r3, #6]	@ zero_extendqisi2
	ldrb	r4, [r3, #7]	@ zero_extendqisi2
	cmp	ip, r0
	strb	sl, [r3, #7]
	strb	r8, [r3, #1]
	strb	r7, [r3, #2]
	strb	r6, [r3, #3]
	strb	r5, [r3, #4]
	strb	r4, [r3, #5]
	strb	r2, [r3, #6]
	add	r1, r1, #32
	bhi	.L180
	b	.L184
.L243:
	ldr	r0, [r0, #0]
	cmp	r0, #0
	beq	.L184
	ldrb	r2, [r1, #0]	@ zero_extendqisi2
	ldrb	ip, [r1, #1]	@ zero_extendqisi2
	sub	r3, r0, #1
	cmp	r0, #1
	strb	r2, [r1, #1]
	strb	ip, [r1, #0]
	and	r3, r3, #3
	add	r1, r1, #2
	mov	r2, #1
	bls	.L184
	cmp	r3, #0
	beq	.L182
	cmp	r3, #1
	beq	.L237
	cmp	r3, #2
	beq	.L238
	ldrb	r3, [r1, #0]	@ zero_extendqisi2
	ldrb	ip, [r1, #1]	@ zero_extendqisi2
	mov	r2, #2
	strb	r3, [r1, #1]
	strb	ip, [r1, #0]
	add	r1, r1, #2
.L238:
	ldrb	ip, [r1, #0]	@ zero_extendqisi2
	ldrb	r3, [r1, #1]	@ zero_extendqisi2
	add	r2, r2, #1
	strb	ip, [r1, #1]
	strb	r3, [r1, #0]
	add	r1, r1, #2
.L237:
	ldrb	r3, [r1, #0]	@ zero_extendqisi2
	ldrb	ip, [r1, #1]	@ zero_extendqisi2
	add	r2, r2, #1
	cmp	r0, r2
	strb	ip, [r1, #0]
	strb	r3, [r1, #1]
	add	r1, r1, #2
	bls	.L184
.L182:
	ldrb	ip, [r1, #0]	@ zero_extendqisi2
	ldrb	r3, [r1, #1]	@ zero_extendqisi2
	strb	ip, [r1, #1]
	strb	r3, [r1, #0]
	add	r3, r1, #2
	ldrb	r5, [r1, #2]	@ zero_extendqisi2
	ldrb	r4, [r3, #1]	@ zero_extendqisi2
	strb	r4, [r1, #2]
	strb	r5, [r3, #1]
	add	ip, r3, #2
	ldrb	r4, [r3, #2]	@ zero_extendqisi2
	ldrb	r5, [ip, #1]	@ zero_extendqisi2
	strb	r5, [r3, #2]
	strb	r4, [ip, #1]
	add	r3, r1, #6
	ldrb	ip, [r1, #6]	@ zero_extendqisi2
	ldrb	r4, [r3, #1]	@ zero_extendqisi2
	add	r2, r2, #4
	cmp	r0, r2
	strb	r4, [r1, #6]
	strb	ip, [r3, #1]
	add	r1, r1, #8
	bhi	.L182
	b	.L184
.L244:
	cmp	ip, #0
	beq	.L184
	ldrb	r6, [r1, #1]	@ zero_extendqisi2
	ldrb	r5, [r1, #2]	@ zero_extendqisi2
	ldrb	r0, [r1, #0]	@ zero_extendqisi2
	ldrb	r4, [r1, #3]	@ zero_extendqisi2
	add	r3, r1, #4
	sub	r2, ip, #1
	cmp	ip, #1
	strb	r6, [r1, #0]
	strb	r5, [r1, #1]
	strb	r4, [r1, #2]
	and	r2, r2, #3
	strb	r0, [r3, #-1]
	mov	r0, #1
	bls	.L184
	cmp	r2, #0
	beq	.L179
	cmp	r2, #1
	beq	.L233
	cmp	r2, #2
	beq	.L234
	ldrb	r0, [r3, #1]	@ zero_extendqisi2
	ldrb	r4, [r3, #2]	@ zero_extendqisi2
	ldrb	r1, [r3, #3]	@ zero_extendqisi2
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	strb	r4, [r3, #1]
	strb	r0, [r3, #0]
	strb	r1, [r3, #2]
	add	r3, r3, #4
	strb	r2, [r3, #-1]
	mov	r0, #2
.L234:
	ldrb	r4, [r3, #1]	@ zero_extendqisi2
	ldrb	r5, [r3, #2]	@ zero_extendqisi2
	ldrb	r1, [r3, #3]	@ zero_extendqisi2
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	strb	r5, [r3, #1]
	strb	r4, [r3, #0]
	strb	r1, [r3, #2]
	add	r3, r3, #4
	strb	r2, [r3, #-1]
	add	r0, r0, #1
.L233:
	ldrb	r4, [r3, #1]	@ zero_extendqisi2
	ldrb	r5, [r3, #2]	@ zero_extendqisi2
	ldrb	r1, [r3, #3]	@ zero_extendqisi2
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	add	r0, r0, #1
	strb	r5, [r3, #1]
	strb	r4, [r3, #0]
	strb	r1, [r3, #2]
	cmp	ip, r0
	add	r3, r3, #4
	strb	r2, [r3, #-1]
	bls	.L184
.L179:
	ldrb	r2, [r3, #1]	@ zero_extendqisi2
	ldrb	r1, [r3, #2]	@ zero_extendqisi2
	ldrb	r5, [r3, #3]	@ zero_extendqisi2
	strb	r1, [r3, #1]
	strb	r5, [r3, #2]
	ldrb	r4, [r3, #0]	@ zero_extendqisi2
	strb	r2, [r3, #0]
	add	r2, r3, #4
	strb	r4, [r2, #-1]
	ldrb	r1, [r2, #1]	@ zero_extendqisi2
	ldrb	r4, [r3, #4]	@ zero_extendqisi2
	strb	r1, [r3, #4]
	ldrb	r5, [r2, #2]	@ zero_extendqisi2
	ldrb	r1, [r2, #3]	@ zero_extendqisi2
	strb	r5, [r2, #1]
	strb	r1, [r2, #2]
	add	r1, r2, #4
	strb	r4, [r1, #-1]
	ldrb	r5, [r1, #1]	@ zero_extendqisi2
	ldrb	r4, [r2, #4]	@ zero_extendqisi2
	strb	r5, [r2, #4]
	ldrb	r2, [r1, #2]	@ zero_extendqisi2
	ldrb	r5, [r1, #3]	@ zero_extendqisi2
	strb	r2, [r1, #1]
	strb	r5, [r1, #2]
	add	r2, r3, #12
	strb	r4, [r2, #-1]
	ldrb	r4, [r2, #1]	@ zero_extendqisi2
	ldrb	r1, [r3, #12]	@ zero_extendqisi2
	strb	r4, [r3, #12]
	add	r0, r0, #4
	ldrb	r4, [r2, #2]	@ zero_extendqisi2
	ldrb	r5, [r2, #3]	@ zero_extendqisi2
	add	r3, r3, #16
	cmp	ip, r0
	strb	r5, [r2, #2]
	strb	r4, [r2, #1]
	strb	r1, [r3, #-1]
	bhi	.L179
	b	.L184
	.size	png_do_write_swap_alpha, .-png_do_write_swap_alpha
	.section	.text.png_do_write_invert_alpha,"ax",%progbits
	.align	2
	.global	png_do_write_invert_alpha
	.hidden	png_do_write_invert_alpha
	.type	png_do_write_invert_alpha, %function
png_do_write_invert_alpha:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	str	r4, [sp, #-4]!
	ldrb	r3, [r0, #8]	@ zero_extendqisi2
	cmp	r3, #6
	beq	.L311
	cmp	r3, #4
	beq	.L312
.L254:
	ldmfd	sp!, {r4}
	bx	lr
.L312:
	ldrb	r2, [r0, #9]	@ zero_extendqisi2
	cmp	r2, #8
	ldr	r0, [r0, #0]
	beq	.L313
	cmp	r0, #0
	beq	.L254
	ldrb	r2, [r1, #2]	@ zero_extendqisi2
	ldrb	r3, [r1, #3]	@ zero_extendqisi2
	mvn	ip, r2
	cmp	r0, #1
	mvn	r2, r3
	sub	r3, r0, #1
	strb	r2, [r1, #3]
	strb	ip, [r1, #2]
	and	r3, r3, #3
	add	r1, r1, #4
	mov	r2, #1
	bls	.L254
	cmp	r3, #0
	beq	.L253
	cmp	r3, #1
	beq	.L309
	cmp	r3, #2
	beq	.L310
	ldrb	r3, [r1, #2]	@ zero_extendqisi2
	ldrb	ip, [r1, #3]	@ zero_extendqisi2
	mvn	r2, r3
	mvn	r3, ip
	strb	r2, [r1, #2]
	strb	r3, [r1, #3]
	mov	r2, #2
	add	r1, r1, #4
.L310:
	ldrb	ip, [r1, #2]	@ zero_extendqisi2
	ldrb	r3, [r1, #3]	@ zero_extendqisi2
	mvn	ip, ip
	mvn	r3, r3
	strb	ip, [r1, #2]
	strb	r3, [r1, #3]
	add	r2, r2, #1
	add	r1, r1, #4
.L309:
	ldrb	ip, [r1, #2]	@ zero_extendqisi2
	ldrb	r3, [r1, #3]	@ zero_extendqisi2
	add	r2, r2, #1
	mvn	ip, ip
	mvn	r3, r3
	cmp	r0, r2
	strb	ip, [r1, #2]
	strb	r3, [r1, #3]
	add	r1, r1, #4
	bls	.L254
.L253:
	ldrb	r3, [r1, #2]	@ zero_extendqisi2
	ldrb	r4, [r1, #3]	@ zero_extendqisi2
	mvn	ip, r3
	mvn	r3, r4
	strb	r3, [r1, #3]
	strb	ip, [r1, #2]
	add	r3, r1, #4
	ldrb	r4, [r3, #2]	@ zero_extendqisi2
	ldrb	ip, [r3, #3]	@ zero_extendqisi2
	mvn	r4, r4
	mvn	ip, ip
	strb	r4, [r3, #2]
	strb	ip, [r3, #3]
	add	r3, r3, #4
	ldrb	ip, [r3, #2]	@ zero_extendqisi2
	ldrb	r4, [r3, #3]	@ zero_extendqisi2
	mvn	ip, ip
	mvn	r4, r4
	strb	r4, [r3, #3]
	strb	ip, [r3, #2]
	add	r3, r1, #12
	ldrb	ip, [r3, #2]	@ zero_extendqisi2
	ldrb	r4, [r3, #3]	@ zero_extendqisi2
	add	r2, r2, #4
	mvn	ip, ip
	mvn	r4, r4
	cmp	r0, r2
	strb	r4, [r3, #3]
	strb	ip, [r3, #2]
	add	r1, r1, #16
	bhi	.L253
	b	.L254
.L311:
	ldrb	r3, [r0, #9]	@ zero_extendqisi2
	cmp	r3, #8
	ldr	r0, [r0, #0]
	beq	.L314
	cmp	r0, #0
	beq	.L254
	ldrb	r2, [r1, #6]	@ zero_extendqisi2
	ldrb	r3, [r1, #7]	@ zero_extendqisi2
	mvn	ip, r2
	cmp	r0, #1
	mvn	r2, r3
	sub	r3, r0, #1
	strb	r2, [r1, #7]
	strb	ip, [r1, #6]
	and	r3, r3, #3
	add	r1, r1, #8
	mov	r2, #1
	bls	.L254
	cmp	r3, #0
	beq	.L250
	cmp	r3, #1
	beq	.L305
	cmp	r3, #2
	beq	.L306
	ldrb	r3, [r1, #6]	@ zero_extendqisi2
	ldrb	ip, [r1, #7]	@ zero_extendqisi2
	mvn	r2, r3
	mvn	r3, ip
	strb	r2, [r1, #6]
	strb	r3, [r1, #7]
	mov	r2, #2
	add	r1, r1, #8
.L306:
	ldrb	ip, [r1, #6]	@ zero_extendqisi2
	ldrb	r3, [r1, #7]	@ zero_extendqisi2
	mvn	ip, ip
	mvn	r3, r3
	strb	ip, [r1, #6]
	strb	r3, [r1, #7]
	add	r2, r2, #1
	add	r1, r1, #8
.L305:
	ldrb	ip, [r1, #6]	@ zero_extendqisi2
	ldrb	r3, [r1, #7]	@ zero_extendqisi2
	add	r2, r2, #1
	mvn	ip, ip
	mvn	r3, r3
	cmp	r0, r2
	strb	ip, [r1, #6]
	strb	r3, [r1, #7]
	add	r1, r1, #8
	bls	.L254
.L250:
	ldrb	r3, [r1, #6]	@ zero_extendqisi2
	ldrb	r4, [r1, #7]	@ zero_extendqisi2
	mvn	ip, r3
	mvn	r3, r4
	strb	r3, [r1, #7]
	strb	ip, [r1, #6]
	add	r3, r1, #8
	ldrb	r4, [r3, #6]	@ zero_extendqisi2
	ldrb	ip, [r3, #7]	@ zero_extendqisi2
	mvn	r4, r4
	mvn	ip, ip
	strb	r4, [r3, #6]
	strb	ip, [r3, #7]
	add	r3, r3, #8
	ldrb	ip, [r3, #6]	@ zero_extendqisi2
	ldrb	r4, [r3, #7]	@ zero_extendqisi2
	mvn	ip, ip
	mvn	r4, r4
	strb	r4, [r3, #7]
	strb	ip, [r3, #6]
	add	r3, r1, #24
	ldrb	ip, [r3, #6]	@ zero_extendqisi2
	ldrb	r4, [r3, #7]	@ zero_extendqisi2
	add	r2, r2, #4
	mvn	ip, ip
	mvn	r4, r4
	cmp	r0, r2
	strb	r4, [r3, #7]
	strb	ip, [r3, #6]
	add	r1, r1, #32
	bhi	.L250
	b	.L254
.L313:
	cmp	r0, #0
	beq	.L254
	ldrb	r2, [r1, #1]	@ zero_extendqisi2
	sub	ip, r0, #1
	mvn	r3, r2
	cmp	r0, #1
	strb	r3, [r1, #1]
	and	r2, ip, #3
	add	r1, r1, #2
	mov	r3, #1
	bls	.L254
	cmp	r2, #0
	beq	.L252
	cmp	r2, #1
	beq	.L307
	cmp	r2, #2
	ldrneb	r3, [r1, #1]	@ zero_extendqisi2
	mvnne	r3, r3
	strneb	r3, [r1, #1]
	addne	r1, r1, #2
	ldrb	ip, [r1, #1]	@ zero_extendqisi2
	movne	r3, #2
	mvn	r2, ip
	strb	r2, [r1, #1]
	add	r3, r3, #1
	add	r1, r1, #2
.L307:
	ldrb	ip, [r1, #1]	@ zero_extendqisi2
	add	r3, r3, #1
	mvn	r2, ip
	cmp	r0, r3
	strb	r2, [r1, #1]
	add	r1, r1, #2
	bls	.L254
.L252:
	ldrb	ip, [r1, #1]	@ zero_extendqisi2
	mvn	r2, ip
	strb	r2, [r1, #1]
	add	r2, r1, #2
	ldrb	ip, [r2, #1]	@ zero_extendqisi2
	mvn	ip, ip
	strb	ip, [r2, #1]
	add	r2, r2, #2
	ldrb	ip, [r2, #1]	@ zero_extendqisi2
	mvn	ip, ip
	strb	ip, [r2, #1]
	add	r2, r1, #6
	ldrb	ip, [r2, #1]	@ zero_extendqisi2
	add	r3, r3, #4
	mvn	ip, ip
	cmp	r0, r3
	strb	ip, [r2, #1]
	add	r1, r1, #8
	bhi	.L252
	b	.L254
.L314:
	cmp	r0, #0
	beq	.L254
	ldrb	r3, [r1, #3]	@ zero_extendqisi2
	sub	r2, r0, #1
	mvn	ip, r3
	cmp	r0, #1
	strb	ip, [r1, #3]
	and	r2, r2, #3
	add	r1, r1, #4
	mov	r3, #1
	bls	.L254
	cmp	r2, #0
	beq	.L249
	cmp	r2, #1
	beq	.L303
	cmp	r2, #2
	ldrneb	r3, [r1, #3]	@ zero_extendqisi2
	mvnne	r3, r3
	strneb	r3, [r1, #3]
	addne	r1, r1, #4
	ldrb	ip, [r1, #3]	@ zero_extendqisi2
	movne	r3, #2
	mvn	r2, ip
	strb	r2, [r1, #3]
	add	r3, r3, #1
	add	r1, r1, #4
.L303:
	ldrb	ip, [r1, #3]	@ zero_extendqisi2
	add	r3, r3, #1
	mvn	r2, ip
	cmp	r0, r3
	strb	r2, [r1, #3]
	add	r1, r1, #4
	bls	.L254
.L249:
	ldrb	ip, [r1, #3]	@ zero_extendqisi2
	mvn	r2, ip
	strb	r2, [r1, #3]
	add	r2, r1, #4
	ldrb	ip, [r2, #3]	@ zero_extendqisi2
	mvn	ip, ip
	strb	ip, [r2, #3]
	add	r2, r2, #4
	ldrb	ip, [r2, #3]	@ zero_extendqisi2
	mvn	ip, ip
	strb	ip, [r2, #3]
	add	r2, r1, #12
	ldrb	ip, [r2, #3]	@ zero_extendqisi2
	add	r3, r3, #4
	mvn	ip, ip
	cmp	r0, r3
	strb	ip, [r2, #3]
	add	r1, r1, #16
	bhi	.L249
	b	.L254
	.size	png_do_write_invert_alpha, .-png_do_write_invert_alpha
	.section	.text.png_do_write_intrapixel,"ax",%progbits
	.align	2
	.global	png_do_write_intrapixel
	.hidden	png_do_write_intrapixel
	.type	png_do_write_intrapixel, %function
png_do_write_intrapixel:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl}
	ldrb	r3, [r0, #8]	@ zero_extendqisi2
	tst	r3, #2
	beq	.L324
	ldrb	r2, [r0, #9]	@ zero_extendqisi2
	cmp	r2, #8
	ldr	ip, [r0, #0]
	beq	.L345
	cmp	r2, #16
	beq	.L346
.L324:
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl}
	bx	lr
.L345:
	cmp	r3, #2
	moveq	r2, #3
	beq	.L319
	cmp	r3, #6
	bne	.L324
	mov	r2, #4
.L319:
	cmp	ip, #0
	beq	.L324
	mov	r0, r1
	ldrb	r3, [r0, #1]!	@ zero_extendqisi2
	ldrb	r4, [r1, #0]	@ zero_extendqisi2
	rsb	r4, r3, r4
	mov	r3, r1
	strb	r4, [r3], r2
	ldrb	r0, [r0, #0]	@ zero_extendqisi2
	ldrb	r4, [r1, #2]!	@ zero_extendqisi2
	cmp	ip, #1
	rsb	r4, r0, r4
	sub	r0, ip, #1
	strb	r4, [r1, #0]
	and	r1, r0, #3
	mov	r4, #1
	bls	.L324
	cmp	r1, #0
	beq	.L320
	cmp	r1, #1
	beq	.L343
	cmp	r1, #2
	beq	.L344
	mov	r5, r3
	ldrb	r4, [r3, #0]	@ zero_extendqisi2
	ldrb	r0, [r5, #1]!	@ zero_extendqisi2
	rsb	r1, r0, r4
	strb	r1, [r3, #0]
	mov	r1, r3
	ldrb	r4, [r1, #2]!	@ zero_extendqisi2
	ldrb	r0, [r5, #0]	@ zero_extendqisi2
	rsb	r5, r0, r4
	strb	r5, [r1, #0]
	add	r3, r3, r2
	mov	r4, #2
.L344:
	mov	r0, r3
	ldrb	r5, [r3, #0]	@ zero_extendqisi2
	ldrb	r1, [r0, #1]!	@ zero_extendqisi2
	rsb	r1, r1, r5
	strb	r1, [r3, #0]
	mov	r1, r3
	ldrb	r0, [r0, #0]	@ zero_extendqisi2
	ldrb	r5, [r1, #2]!	@ zero_extendqisi2
	rsb	r0, r0, r5
	strb	r0, [r1, #0]
	add	r4, r4, #1
	add	r3, r3, r2
.L343:
	mov	r0, r3
	ldrb	r5, [r3, #0]	@ zero_extendqisi2
	ldrb	r1, [r0, #1]!	@ zero_extendqisi2
	rsb	r1, r1, r5
	strb	r1, [r3, #0]
	mov	r1, r3
	ldrb	r0, [r0, #0]	@ zero_extendqisi2
	ldrb	r5, [r1, #2]!	@ zero_extendqisi2
	add	r4, r4, #1
	rsb	r0, r0, r5
	cmp	ip, r4
	strb	r0, [r1, #0]
	add	r3, r3, r2
	bls	.L324
.L320:
	ldrb	r0, [r3, #1]	@ zero_extendqisi2
	ldrb	r5, [r3, #2]	@ zero_extendqisi2
	ldrb	r6, [r3, #0]	@ zero_extendqisi2
	mov	r1, r0
	rsb	r1, r1, r5
	rsb	r0, r0, r6
	strb	r1, [r3, #2]
	strb	r0, [r3, #0]
	add	r1, r3, r2
	ldrb	r5, [r3, r2]	@ zero_extendqisi2
	ldrb	r0, [r1, #1]	@ zero_extendqisi2
	rsb	r6, r0, r5
	strb	r6, [r3, r2]
	ldrb	r5, [r1, #2]	@ zero_extendqisi2
	ldrb	r0, [r1, #1]	@ zero_extendqisi2
	rsb	r6, r0, r5
	strb	r6, [r1, #2]
	add	r0, r1, r2
	ldrb	r6, [r3, r2, asl #1]	@ zero_extendqisi2
	ldrb	r5, [r0, #1]	@ zero_extendqisi2
	rsb	r5, r5, r6
	strb	r5, [r3, r2, asl #1]
	ldrb	r5, [r0, #2]	@ zero_extendqisi2
	ldrb	r3, [r0, #1]	@ zero_extendqisi2
	rsb	r3, r3, r5
	strb	r3, [r0, #2]
	add	r3, r0, r2
	ldrb	r5, [r1, r2, asl #1]	@ zero_extendqisi2
	ldrb	r0, [r3, #1]	@ zero_extendqisi2
	rsb	r0, r0, r5
	strb	r0, [r1, r2, asl #1]
	ldrb	r0, [r3, #2]	@ zero_extendqisi2
	ldrb	r1, [r3, #1]	@ zero_extendqisi2
	add	r4, r4, #4
	rsb	r1, r1, r0
	cmp	ip, r4
	strb	r1, [r3, #2]
	add	r3, r3, r2
	bhi	.L320
	b	.L324
.L346:
	cmp	r3, #2
	moveq	r4, #6
	beq	.L322
	cmp	r3, #6
	bne	.L324
	mov	r4, #8
.L322:
	cmp	ip, #0
	beq	.L324
	mov	r5, r1
	mov	r6, r1
	mov	r0, r1
	ldrb	r8, [r5, #4]!	@ zero_extendqisi2
	ldrb	sl, [r6, #1]!	@ zero_extendqisi2
	ldrb	r3, [r0, #5]!	@ zero_extendqisi2
	ldrb	r9, [r1, #2]	@ zero_extendqisi2
	ldrb	r7, [r1, #3]	@ zero_extendqisi2
	ldrb	r2, [r1, #0]	@ zero_extendqisi2
	orr	r7, r7, r9, asl #8
	orr	r2, sl, r2, asl #8
	orr	sl, r3, r8, asl #8
	rsb	r3, r7, sl
	rsb	r2, r7, r2
	mov	sl, r2, asl #16
	mov	r8, r3, asl #16
	mov	r2, sl, lsr #16
	mov	r3, r8, lsr #16
	sub	r7, ip, #1
	mov	sl, r2, lsr #8
	mov	r8, r3, lsr #8
	cmp	ip, #1
	strb	sl, [r1], r4
	and	r7, r7, #1
	strb	r2, [r6, #0]
	strb	r8, [r5, #0]
	strb	r3, [r0, #0]
	mov	r5, #1
	bls	.L324
	cmp	r7, #0
	beq	.L323
	mov	r7, r1
	mov	r6, r1
	mov	r0, r1
	ldrb	sl, [r7, #1]!	@ zero_extendqisi2
	ldrb	r8, [r6, #4]!	@ zero_extendqisi2
	ldrb	r3, [r0, #5]!	@ zero_extendqisi2
	ldrb	r9, [r1, #2]	@ zero_extendqisi2
	ldrb	r5, [r1, #3]	@ zero_extendqisi2
	ldrb	r2, [r1, #0]	@ zero_extendqisi2
	orr	r5, r5, r9, asl #8
	orr	r2, sl, r2, asl #8
	orr	sl, r3, r8, asl #8
	rsb	r3, r5, sl
	rsb	r2, r5, r2
	mov	r8, r2, asl #16
	mov	sl, r3, asl #16
	mov	r2, r8, lsr #16
	mov	r3, sl, lsr #16
	mov	r5, #2
	mov	sl, r2, lsr #8
	mov	r8, r3, lsr #8
	cmp	ip, r5
	strb	sl, [r1], r4
	strb	r2, [r7, #0]
	strb	r8, [r6, #0]
	strb	r3, [r0, #0]
	bls	.L324
.L323:
	ldrb	sl, [r1, #1]	@ zero_extendqisi2
	ldrb	r8, [r1, #0]	@ zero_extendqisi2
	ldrb	r6, [r1, #4]	@ zero_extendqisi2
	ldrb	r3, [r1, #5]	@ zero_extendqisi2
	ldrb	r2, [r1, #2]	@ zero_extendqisi2
	ldrb	r7, [r1, #3]	@ zero_extendqisi2
	orr	r0, sl, r8, asl #8
	orr	sl, r7, r2, asl #8
	orr	r7, r3, r6, asl #8
	rsb	r6, sl, r7
	rsb	r8, sl, r0
	mov	r0, r8, asl #16
	mov	r3, r6, asl #16
	mov	r2, r0, lsr #16
	mov	r8, r3, lsr #16
	mov	r7, r2, lsr #8
	mov	sl, r8, lsr #8
	strb	r7, [r1, #0]
	strb	r2, [r1, #1]
	strb	sl, [r1, #4]
	strb	r8, [r1, #5]
	add	r3, r1, r4
	ldrb	sl, [r1, r4]	@ zero_extendqisi2
	ldrb	r7, [r3, #4]	@ zero_extendqisi2
	ldrb	r6, [r3, #1]	@ zero_extendqisi2
	ldrb	r2, [r3, #5]	@ zero_extendqisi2
	ldrb	r0, [r3, #2]	@ zero_extendqisi2
	ldrb	r8, [r3, #3]	@ zero_extendqisi2
	orr	r6, r6, sl, asl #8
	orr	r0, r8, r0, asl #8
	orr	r2, r2, r7, asl #8
	rsb	r2, r0, r2
	rsb	r0, r0, r6
	mov	r7, r0, asl #16
	mov	r6, r2, asl #16
	mov	r0, r7, lsr #16
	mov	r2, r6, lsr #16
	add	r5, r5, #2
	mov	r7, r0, lsr #8
	mov	r6, r2, lsr #8
	cmp	ip, r5
	strb	r7, [r1, r4]
	strb	r0, [r3, #1]
	strb	r6, [r3, #4]
	strb	r2, [r3, #5]
	add	r1, r3, r4
	bhi	.L323
	b	.L324
	.size	png_do_write_intrapixel, .-png_do_write_intrapixel
	.section	.text.png_do_write_transformations,"ax",%progbits
	.align	2
	.global	png_do_write_transformations
	.hidden	png_do_write_transformations
	.type	png_do_write_transformations, %function
png_do_write_transformations:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	subs	r4, r0, #0
	sub	sp, sp, #12
	beq	.L391
	ldr	r2, [r4, #140]
	tst	r2, #1048576
	mov	r1, r2
	bne	.L626
.L349:
	tst	r2, #32768
	bne	.L627
.L350:
	tst	r2, #65536
	bne	.L628
.L351:
	tst	r2, #4
	bne	.L629
.L352:
	tst	r2, #16
	bne	.L630
.L372:
	tst	r1, #8
	bne	.L631
.L373:
	tst	r1, #131072
	bne	.L632
.L374:
	tst	r1, #524288
	bne	.L633
.L382:
	tst	r1, #1
	bne	.L634
.L390:
	tst	r1, #32
	ldrne	ip, [r4, #264]
	addne	r0, r4, #284
	addne	r1, ip, #1
	blne	png_do_invert
.L391:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L626:
	ldr	r3, [r4, #120]
	cmp	r3, #0
	beq	.L349
	ldr	r2, [r4, #264]
	add	r1, r4, #284
	add	r2, r2, #1
	mov	lr, pc
	bx	r3
	ldr	r2, [r4, #140]
	tst	r2, #32768
	mov	r1, r2
	beq	.L350
.L627:
	ldr	r1, [r4, #264]
	ldr	r2, [r4, #136]
	add	r1, r1, #1
	add	r0, r4, #284
	bl	png_do_strip_filler
	ldr	r2, [r4, #140]
	tst	r2, #65536
	mov	r1, r2
	beq	.L351
.L628:
	ldr	r0, [r4, #264]
	add	r1, r0, #1
	add	r0, r4, #284
	bl	png_do_packswap
	ldr	r2, [r4, #140]
	tst	r2, #4
	mov	r1, r2
	beq	.L352
.L629:
	ldr	r0, [r4, #292]
	bic	ip, r0, #-16777216
	bic	r3, ip, #255
	cmp	r3, #67584
	ldrb	r0, [r4, #323]	@ zero_extendqisi2
	ldr	r3, [r4, #264]
	bne	.L352
	cmp	r0, #2
	add	r1, r3, #1
	beq	.L355
	cmp	r0, #4
	beq	.L356
	cmp	r0, #1
	beq	.L354
.L624:
	ldr	ip, [r4, #284]
.L353:
	ldrb	r3, [r4, #294]	@ zero_extendqisi2
	mul	r3, r0, r3
	and	r1, r3, #255
	cmp	r1, #7
	mulls	ip, r1, ip
	strb	r1, [r4, #295]
	movhi	r1, r1, lsr #3
	mulhi	ip, r1, ip
	addls	ip, ip, #7
	movls	ip, ip, lsr #3
	strb	r0, [r4, #293]
	str	ip, [r4, #288]
	mov	r1, r2
	b	.L352
.L634:
	ldr	lr, [r4, #264]
	add	r0, r4, #284
	add	r1, lr, #1
	bl	png_do_bgr
	ldr	r1, [r4, #140]
	b	.L390
.L633:
	ldrb	r3, [r4, #292]	@ zero_extendqisi2
	cmp	r3, #6
	ldr	r2, [r4, #264]
	beq	.L635
	cmp	r3, #4
	bne	.L382
	ldrb	ip, [r4, #293]	@ zero_extendqisi2
	cmp	ip, #8
	beq	.L636
	ldr	ip, [r4, #284]
	cmp	ip, #0
	beq	.L382
	ldrb	r5, [r2, #3]	@ zero_extendqisi2
	ldrb	r3, [r2, #4]	@ zero_extendqisi2
	mvn	r5, r5
	mvn	r0, r3
	sub	r1, ip, #1
	mov	r3, r2
	cmp	ip, #1
	strb	r5, [r2, #3]
	strb	r0, [r3, #4]!
	and	r2, r1, #3
	mov	r0, #1
	bls	.L580
	cmp	r2, #0
	beq	.L389
	cmp	r2, #1
	beq	.L551
	cmp	r2, #2
	beq	.L552
	ldrb	r0, [r3, #3]	@ zero_extendqisi2
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	mvn	r1, r0
	mvn	r0, r2
	strb	r1, [r3, #3]
	strb	r0, [r3, #4]!
	mov	r0, #2
.L552:
	ldrb	r1, [r3, #3]	@ zero_extendqisi2
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	mvn	r1, r1
	mvn	r2, r2
	strb	r1, [r3, #3]
	strb	r2, [r3, #4]!
	add	r0, r0, #1
.L551:
	ldrb	r1, [r3, #3]	@ zero_extendqisi2
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	add	r0, r0, #1
	mvn	r1, r1
	mvn	r2, r2
	cmp	ip, r0
	strb	r1, [r3, #3]
	strb	r2, [r3, #4]!
	bls	.L580
.L389:
	ldrb	r6, [r3, #3]	@ zero_extendqisi2
	ldrb	r1, [r3, #4]	@ zero_extendqisi2
	mvn	r5, r6
	mvn	r2, r1
	mov	r1, r3
	strb	r5, [r3, #3]
	strb	r2, [r1, #4]!
	ldrb	r6, [r1, #3]	@ zero_extendqisi2
	ldrb	r5, [r1, #4]	@ zero_extendqisi2
	mov	r2, r1
	mvn	r5, r5
	mvn	r6, r6
	strb	r6, [r1, #3]
	strb	r5, [r2, #4]!
	ldrb	r1, [r2, #3]	@ zero_extendqisi2
	ldrb	r5, [r2, #4]	@ zero_extendqisi2
	mvn	r1, r1
	mvn	r5, r5
	strb	r5, [r2, #4]
	strb	r1, [r2, #3]
	add	r2, r3, #12
	ldrb	r1, [r2, #3]	@ zero_extendqisi2
	ldrb	r5, [r2, #4]	@ zero_extendqisi2
	add	r0, r0, #4
	mvn	r1, r1
	mvn	r5, r5
	cmp	ip, r0
	strb	r5, [r2, #4]
	strb	r1, [r2, #3]
	add	r3, r3, #16
	bhi	.L389
.L580:
	ldr	r1, [r4, #140]
	b	.L382
.L632:
	ldrb	r3, [r4, #292]	@ zero_extendqisi2
	cmp	r3, #6
	ldr	r2, [r4, #264]
	beq	.L637
	cmp	r3, #4
	bne	.L374
	ldrb	r3, [r4, #293]	@ zero_extendqisi2
	cmp	r3, #8
	ldr	ip, [r4, #284]
	beq	.L638
	cmp	ip, #0
	beq	.L374
	ldrb	r5, [r2, #1]	@ zero_extendqisi2
	ldrb	r7, [r2, #3]	@ zero_extendqisi2
	ldrb	r0, [r2, #2]	@ zero_extendqisi2
	ldrb	r6, [r2, #4]	@ zero_extendqisi2
	mov	r3, r2
	sub	r1, ip, #1
	cmp	ip, #1
	strb	r7, [r2, #1]
	strb	r6, [r2, #2]
	strb	r5, [r2, #3]
	strb	r0, [r3, #4]!
	and	r2, r1, #3
	mov	r0, #1
	bls	.L576
	cmp	r2, #0
	beq	.L381
	cmp	r2, #1
	beq	.L559
	cmp	r2, #2
	beq	.L560
	ldrb	r2, [r3, #1]	@ zero_extendqisi2
	ldrb	r5, [r3, #3]	@ zero_extendqisi2
	ldrb	r0, [r3, #4]	@ zero_extendqisi2
	ldrb	r6, [r3, #2]	@ zero_extendqisi2
	strb	r5, [r3, #1]
	strb	r0, [r3, #2]
	strb	r2, [r3, #3]
	strb	r6, [r3, #4]!
	mov	r0, #2
.L560:
	ldrb	r1, [r3, #1]	@ zero_extendqisi2
	ldrb	r6, [r3, #3]	@ zero_extendqisi2
	ldrb	r2, [r3, #2]	@ zero_extendqisi2
	ldrb	r5, [r3, #4]	@ zero_extendqisi2
	strb	r6, [r3, #1]
	strb	r5, [r3, #2]
	strb	r1, [r3, #3]
	strb	r2, [r3, #4]!
	add	r0, r0, #1
.L559:
	ldrb	r1, [r3, #1]	@ zero_extendqisi2
	ldrb	r6, [r3, #3]	@ zero_extendqisi2
	ldrb	r2, [r3, #2]	@ zero_extendqisi2
	ldrb	r5, [r3, #4]	@ zero_extendqisi2
	add	r0, r0, #1
	cmp	ip, r0
	strb	r6, [r3, #1]
	strb	r5, [r3, #2]
	strb	r1, [r3, #3]
	strb	r2, [r3, #4]!
	bls	.L576
.L381:
	ldrb	r2, [r3, #1]	@ zero_extendqisi2
	ldrb	r6, [r3, #3]	@ zero_extendqisi2
	ldrb	r8, [r3, #2]	@ zero_extendqisi2
	ldrb	r5, [r3, #4]	@ zero_extendqisi2
	mov	r1, r3
	strb	r2, [r3, #3]
	strb	r6, [r3, #1]
	strb	r5, [r3, #2]
	strb	r8, [r1, #4]!
	ldrb	r6, [r1, #1]	@ zero_extendqisi2
	ldrb	r8, [r1, #3]	@ zero_extendqisi2
	ldrb	r5, [r1, #2]	@ zero_extendqisi2
	ldrb	r7, [r1, #4]	@ zero_extendqisi2
	mov	r2, r1
	strb	r8, [r1, #1]
	strb	r7, [r1, #2]
	strb	r6, [r1, #3]
	strb	r5, [r2, #4]!
	ldrb	r1, [r2, #1]	@ zero_extendqisi2
	ldrb	r7, [r2, #2]	@ zero_extendqisi2
	ldrb	r6, [r2, #3]	@ zero_extendqisi2
	ldrb	r5, [r2, #4]	@ zero_extendqisi2
	strb	r6, [r2, #1]
	strb	r7, [r2, #4]
	strb	r5, [r2, #2]
	strb	r1, [r2, #3]
	add	r2, r3, #12
	ldrb	r1, [r2, #1]	@ zero_extendqisi2
	ldrb	r7, [r2, #2]	@ zero_extendqisi2
	ldrb	r6, [r2, #3]	@ zero_extendqisi2
	ldrb	r5, [r2, #4]	@ zero_extendqisi2
	add	r0, r0, #4
	cmp	ip, r0
	strb	r7, [r2, #4]
	strb	r6, [r2, #1]
	strb	r5, [r2, #2]
	strb	r1, [r2, #3]
	add	r3, r3, #16
	bhi	.L381
.L576:
	ldr	r1, [r4, #140]
	b	.L374
.L631:
	ldr	ip, [r4, #264]
	add	r2, r4, #412
	add	r1, ip, #1
	add	r2, r2, #1
	add	r0, r4, #284
	bl	png_do_shift
	ldr	r1, [r4, #140]
	b	.L373
.L630:
	ldr	lr, [r4, #264]
	add	r0, r4, #284
	add	r1, lr, #1
	bl	png_do_swap
	ldr	r1, [r4, #140]
	b	.L372
.L637:
	ldrb	r0, [r4, #293]	@ zero_extendqisi2
	cmp	r0, #8
	beq	.L639
	ldr	r3, [r4, #284]
	cmp	r3, #0
	str	r3, [sp, #4]
	beq	.L374
	ldrb	r0, [r2, #1]	@ zero_extendqisi2
	mov	fp, r3
	ldrb	sl, [r2, #3]	@ zero_extendqisi2
	ldrb	r8, [r2, #4]	@ zero_extendqisi2
	ldrb	r7, [r2, #5]	@ zero_extendqisi2
	ldrb	r6, [r2, #6]	@ zero_extendqisi2
	ldrb	r5, [r2, #7]	@ zero_extendqisi2
	ldrb	r9, [r2, #2]	@ zero_extendqisi2
	ldrb	ip, [r2, #8]	@ zero_extendqisi2
	mov	r3, r2
	sub	r1, fp, #1
	cmp	fp, #1
	strb	r0, [r2, #7]
	strb	sl, [r2, #1]
	strb	r8, [r2, #2]
	strb	r7, [r2, #3]
	strb	r6, [r2, #4]
	strb	r5, [r2, #5]
	strb	ip, [r2, #6]
	and	r1, r1, #3
	strb	r9, [r3, #8]!
	mov	r0, #1
	bls	.L576
	cmp	r1, #0
	beq	.L616
	cmp	r1, #1
	beq	.L555
	cmp	r1, #2
	beq	.L556
	ldrb	sl, [r3, #1]	@ zero_extendqisi2
	ldrb	r8, [r3, #3]	@ zero_extendqisi2
	ldrb	r7, [r3, #4]	@ zero_extendqisi2
	ldrb	r6, [r3, #5]	@ zero_extendqisi2
	ldrb	r5, [r3, #6]	@ zero_extendqisi2
	ldrb	ip, [r3, #7]	@ zero_extendqisi2
	ldrb	r0, [r3, #8]	@ zero_extendqisi2
	ldrb	fp, [r3, #2]	@ zero_extendqisi2
	strb	r0, [r3, #6]
	strb	r8, [r3, #1]
	strb	r7, [r3, #2]
	strb	r6, [r3, #3]
	strb	r5, [r3, #4]
	strb	ip, [r3, #5]
	strb	sl, [r3, #7]
	strb	fp, [r3, #8]!
	mov	r0, #2
.L556:
	ldrb	r1, [r3, #1]	@ zero_extendqisi2
	ldrb	sl, [r3, #3]	@ zero_extendqisi2
	ldrb	r7, [r3, #4]	@ zero_extendqisi2
	ldrb	r6, [r3, #5]	@ zero_extendqisi2
	ldrb	r5, [r3, #6]	@ zero_extendqisi2
	ldrb	ip, [r3, #7]	@ zero_extendqisi2
	ldrb	r2, [r3, #2]	@ zero_extendqisi2
	ldrb	fp, [r3, #8]	@ zero_extendqisi2
	strb	sl, [r3, #1]
	strb	r7, [r3, #2]
	strb	r6, [r3, #3]
	strb	r5, [r3, #4]
	strb	ip, [r3, #5]
	strb	fp, [r3, #6]
	strb	r1, [r3, #7]
	strb	r2, [r3, #8]!
	add	r0, r0, #1
.L555:
	ldr	fp, [sp, #4]
	ldrb	r1, [r3, #1]	@ zero_extendqisi2
	ldrb	sl, [r3, #3]	@ zero_extendqisi2
	ldrb	r8, [r3, #4]	@ zero_extendqisi2
	ldrb	r7, [r3, #5]	@ zero_extendqisi2
	ldrb	r6, [r3, #6]	@ zero_extendqisi2
	ldrb	r5, [r3, #7]	@ zero_extendqisi2
	ldrb	r2, [r3, #2]	@ zero_extendqisi2
	ldrb	ip, [r3, #8]	@ zero_extendqisi2
	add	r0, r0, #1
	cmp	fp, r0
	strb	sl, [r3, #1]
	strb	r8, [r3, #2]
	strb	r7, [r3, #3]
	strb	r6, [r3, #4]
	strb	r5, [r3, #5]
	strb	ip, [r3, #6]
	strb	r1, [r3, #7]
	strb	r2, [r3, #8]!
	bls	.L576
.L616:
	mov	fp, r4
.L378:
	ldrb	ip, [r3, #1]	@ zero_extendqisi2
	ldrb	sl, [r3, #3]	@ zero_extendqisi2
	ldrb	r8, [r3, #4]	@ zero_extendqisi2
	ldrb	r7, [r3, #5]	@ zero_extendqisi2
	ldrb	r6, [r3, #6]	@ zero_extendqisi2
	ldrb	r5, [r3, #7]	@ zero_extendqisi2
	ldrb	r2, [r3, #2]	@ zero_extendqisi2
	ldrb	r4, [r3, #8]	@ zero_extendqisi2
	mov	r1, r3
	strb	sl, [r3, #1]
	strb	r8, [r3, #2]
	strb	r7, [r3, #3]
	strb	r6, [r3, #4]
	strb	r5, [r3, #5]
	strb	r4, [r3, #6]
	strb	ip, [r3, #7]
	strb	r2, [r1, #8]!
	ldrb	r4, [r1, #1]	@ zero_extendqisi2
	ldrb	r9, [r1, #3]	@ zero_extendqisi2
	ldrb	sl, [r1, #4]	@ zero_extendqisi2
	ldrb	r8, [r1, #5]	@ zero_extendqisi2
	ldrb	r7, [r1, #6]	@ zero_extendqisi2
	ldrb	r6, [r1, #7]	@ zero_extendqisi2
	ldrb	ip, [r1, #2]	@ zero_extendqisi2
	ldrb	r5, [r1, #8]	@ zero_extendqisi2
	mov	r2, r1
	strb	sl, [r1, #2]
	strb	r7, [r1, #4]
	strb	r6, [r1, #5]
	strb	r5, [r1, #6]
	strb	r4, [r1, #7]
	strb	r9, [r1, #1]
	strb	r8, [r1, #3]
	strb	ip, [r2, #8]!
	ldrb	ip, [r2, #3]	@ zero_extendqisi2
	ldrb	r9, [r2, #1]	@ zero_extendqisi2
	ldrb	r8, [r2, #2]	@ zero_extendqisi2
	strb	ip, [r2, #1]
	ldrb	r7, [r2, #4]	@ zero_extendqisi2
	ldrb	r6, [r2, #5]	@ zero_extendqisi2
	ldrb	r5, [r2, #6]	@ zero_extendqisi2
	ldrb	r1, [r2, #7]	@ zero_extendqisi2
	ldrb	sl, [r2, #8]	@ zero_extendqisi2
	strb	r7, [r2, #2]
	strb	r8, [r2, #8]
	strb	r6, [r2, #3]
	strb	r5, [r2, #4]
	strb	r1, [r2, #5]
	strb	sl, [r2, #6]
	strb	r9, [r2, #7]
	ldr	r9, [sp, #4]
	add	r2, r3, #24
	ldrb	r1, [r2, #1]	@ zero_extendqisi2
	ldrb	sl, [r2, #2]	@ zero_extendqisi2
	ldrb	r8, [r2, #3]	@ zero_extendqisi2
	ldrb	r7, [r2, #4]	@ zero_extendqisi2
	ldrb	r6, [r2, #5]	@ zero_extendqisi2
	ldrb	r5, [r2, #6]	@ zero_extendqisi2
	ldrb	r4, [r2, #7]	@ zero_extendqisi2
	ldrb	ip, [r2, #8]	@ zero_extendqisi2
	add	r0, r0, #4
	cmp	r9, r0
	strb	sl, [r2, #8]
	strb	r8, [r2, #1]
	strb	r7, [r2, #2]
	strb	r6, [r2, #3]
	strb	r5, [r2, #4]
	strb	r4, [r2, #5]
	strb	ip, [r2, #6]
	strb	r1, [r2, #7]
	add	r3, r3, #32
	bhi	.L378
	mov	r4, fp
	ldr	r1, [r4, #140]
	b	.L374
.L635:
	ldrb	ip, [r4, #293]	@ zero_extendqisi2
	cmp	ip, #8
	beq	.L640
	ldr	ip, [r4, #284]
	cmp	ip, #0
	beq	.L382
	ldrb	r5, [r2, #7]	@ zero_extendqisi2
	ldrb	r3, [r2, #8]	@ zero_extendqisi2
	mvn	r5, r5
	mvn	r0, r3
	sub	r1, ip, #1
	mov	r3, r2
	cmp	ip, #1
	strb	r5, [r2, #7]
	strb	r0, [r3, #8]!
	and	r2, r1, #3
	mov	r0, #1
	bls	.L580
	cmp	r2, #0
	beq	.L386
	cmp	r2, #1
	beq	.L547
	cmp	r2, #2
	beq	.L548
	ldrb	r0, [r3, #7]	@ zero_extendqisi2
	ldrb	r2, [r3, #8]	@ zero_extendqisi2
	mvn	r1, r0
	mvn	r0, r2
	strb	r1, [r3, #7]
	strb	r0, [r3, #8]!
	mov	r0, #2
.L548:
	ldrb	r1, [r3, #7]	@ zero_extendqisi2
	ldrb	r2, [r3, #8]	@ zero_extendqisi2
	mvn	r1, r1
	mvn	r2, r2
	strb	r1, [r3, #7]
	strb	r2, [r3, #8]!
	add	r0, r0, #1
.L547:
	ldrb	r1, [r3, #7]	@ zero_extendqisi2
	ldrb	r2, [r3, #8]	@ zero_extendqisi2
	add	r0, r0, #1
	mvn	r1, r1
	mvn	r2, r2
	cmp	ip, r0
	strb	r1, [r3, #7]
	strb	r2, [r3, #8]!
	bls	.L580
.L386:
	ldrb	r6, [r3, #7]	@ zero_extendqisi2
	ldrb	r1, [r3, #8]	@ zero_extendqisi2
	mvn	r5, r6
	mvn	r2, r1
	mov	r1, r3
	strb	r5, [r3, #7]
	strb	r2, [r1, #8]!
	ldrb	r6, [r1, #7]	@ zero_extendqisi2
	ldrb	r5, [r1, #8]	@ zero_extendqisi2
	mov	r2, r1
	mvn	r5, r5
	mvn	r6, r6
	strb	r6, [r1, #7]
	strb	r5, [r2, #8]!
	ldrb	r1, [r2, #7]	@ zero_extendqisi2
	ldrb	r5, [r2, #8]	@ zero_extendqisi2
	mvn	r1, r1
	mvn	r5, r5
	strb	r5, [r2, #8]
	strb	r1, [r2, #7]
	add	r2, r3, #24
	ldrb	r1, [r2, #7]	@ zero_extendqisi2
	ldrb	r5, [r2, #8]	@ zero_extendqisi2
	add	r0, r0, #4
	mvn	r1, r1
	mvn	r5, r5
	cmp	ip, r0
	strb	r5, [r2, #8]
	strb	r1, [r2, #7]
	add	r3, r3, #32
	bhi	.L386
	ldr	r1, [r4, #140]
	b	.L382
.L356:
	ldr	ip, [r4, #284]
	cmp	ip, #0
	beq	.L353
	sub	r6, ip, #1
	ands	r7, r6, #3
	mov	r5, #1
	mov	r6, #0
	mov	r2, r0
	beq	.L369
	ldrb	r5, [r3, #1]	@ zero_extendqisi2
	cmp	r7, #1
	and	r2, r5, #15
	mov	r6, r2, asl #4
	mov	r5, #2
	mov	r2, #0
	beq	.L369
	cmp	r7, r5
	beq	.L562
	ldrb	r7, [r3, r5]	@ zero_extendqisi2
	and	r7, r7, #15
	orr	r6, r6, r7
	strb	r6, [r1], #1
	add	r5, r5, #1
	mov	r6, r2
	mov	r2, #4
.L562:
	ldrb	r7, [r3, r5]	@ zero_extendqisi2
	and	r7, r7, #15
	orr	r6, r6, r7, asl r2
	cmp	r2, #0
	streqb	r6, [r1], #1
	subne	r2, r2, #4
	moveq	r6, r2
	add	r5, r5, #1
	moveq	r2, #4
	b	.L369
.L641:
	add	r5, r5, #1
	ldrb	r7, [r3, r5]	@ zero_extendqisi2
	cmp	r2, #0
	and	r7, r7, #15
	orr	r6, r6, r7, asl r2
	subne	r2, r2, #4
	streqb	r6, [r1], #1
	moveq	r6, r2
	moveq	r2, #4
.L590:
	add	r7, r5, #1
	ldrb	r7, [r3, r7]	@ zero_extendqisi2
	and	r7, r7, #15
	orr	r6, r6, r7, asl r2
	cmp	r2, #0
	streqb	r6, [r1], #1
	add	r7, r5, #2
	ldrb	r7, [r3, r7]	@ zero_extendqisi2
	moveq	r6, r2
	subne	r2, r2, #4
	moveq	r2, #4
	and	r7, r7, #15
	orr	r6, r6, r7, asl r2
	cmp	r2, #0
	streqb	r6, [r1], #1
	subne	r2, r2, #4
	moveq	r6, r2
	add	r5, r5, #3
	moveq	r2, #4
.L369:
	ldrb	r7, [r3, r5]	@ zero_extendqisi2
	and	r7, r7, #15
	orr	r6, r6, r7, asl r2
	cmp	r2, #0
	streqb	r6, [r1], #1
	subne	r2, r2, #4
	moveq	r6, r2
	moveq	r2, #4
	cmp	ip, r5
	bhi	.L641
	cmp	r2, #4
	beq	.L625
	strb	r6, [r1, #0]
	ldr	r2, [r4, #140]
	b	.L624
.L354:
	ldr	ip, [r4, #284]
	cmp	ip, #0
	beq	.L353
	sub	r7, ip, #1
	ands	r7, r7, #3
	mov	r5, r0
	mov	r6, #128
	mov	r2, #0
	beq	.L361
	ldrb	r5, [r3, #1]	@ zero_extendqisi2
	cmp	r5, #0
	movne	r2, r6
	cmp	r7, #1
	mov	r6, #64
	mov	r5, #2
	beq	.L361
	cmp	r7, r5
	beq	.L563
	ldrb	r7, [r3, r5]	@ zero_extendqisi2
	cmp	r7, #0
	orrne	r2, r2, r6
	cmp	r6, #1
	strleb	r2, [r1], #1
	movle	r6, #128
	movle	r2, #0
	movgt	r6, r6, asr #1
	add	r5, r5, #1
.L563:
	ldrb	r7, [r3, r5]	@ zero_extendqisi2
	cmp	r7, #0
	orrne	r2, r2, r6
	cmp	r6, #1
	strleb	r2, [r1], #1
	movle	r6, #128
	movle	r2, #0
	movgt	r6, r6, asr #1
	add	r5, r5, #1
	b	.L361
.L642:
	add	r5, r5, #1
	ldrb	r7, [r3, r5]	@ zero_extendqisi2
	cmp	r7, #0
	orrne	r2, r2, r6
.L525:
	cmp	r6, #1
	strleb	r2, [r1], #1
	add	r7, r5, #1
	ldrb	r7, [r3, r7]	@ zero_extendqisi2
	movle	r6, #128
	movle	r2, #0
	movgt	r6, r6, asr #1
	cmp	r7, #0
	orrne	r2, r2, r6
	cmp	r6, #1
	strleb	r2, [r1], #1
	add	r7, r5, #2
	ldrb	r7, [r3, r7]	@ zero_extendqisi2
	movle	r6, #128
	movle	r2, #0
	movgt	r6, r6, asr #1
	cmp	r7, #0
	orrne	r2, r2, r6
	cmp	r6, #1
	strleb	r2, [r1], #1
	movle	r6, #128
	movle	r2, #0
	movgt	r6, r6, asr #1
	add	r5, r5, #3
.L361:
	ldrb	r7, [r3, r5]	@ zero_extendqisi2
	cmp	r7, #0
	orrne	r2, r2, r6
	cmp	r6, #1
	strleb	r2, [r1], #1
	movgt	r6, r6, asr #1
	movle	r6, #128
	movle	r2, #0
	cmp	ip, r5
	bhi	.L642
	cmp	r6, #128
	beq	.L625
	strb	r2, [r1, #0]
	ldr	r2, [r4, #140]
	ldr	ip, [r4, #284]
	b	.L353
.L355:
	ldr	ip, [r4, #284]
	cmp	ip, #0
	beq	.L353
	sub	r5, ip, #1
	ands	r7, r5, #3
	mov	r6, #0
	mov	r5, #1
	mov	r2, #6
	beq	.L365
	ldrb	r6, [r3, #1]	@ zero_extendqisi2
	cmp	r7, #1
	and	r5, r6, #3
	mov	r6, r5, asl #6
	mov	r2, #4
	mov	r5, r0
	beq	.L365
	cmp	r7, #2
	ldrneb	r7, [r3, r0]	@ zero_extendqisi2
	andne	r7, r7, #3
	orrne	r6, r6, r7, asl r2
	addne	r5, r0, #1
	ldrb	r7, [r3, r5]	@ zero_extendqisi2
	movne	r2, r0
	and	r7, r7, #3
	orr	r6, r6, r7, asl r2
	cmp	r2, #0
	streqb	r6, [r1], #1
	subne	r2, r2, #2
	moveq	r6, r2
	add	r5, r5, #1
	moveq	r2, #6
	b	.L365
.L643:
	add	r5, r5, #1
	ldrb	r7, [r3, r5]	@ zero_extendqisi2
	cmp	r2, #0
	and	r7, r7, #3
	orr	r6, r6, r7, asl r2
	subne	r2, r2, #2
	streqb	r6, [r1], #1
	moveq	r6, r2
	moveq	r2, #6
.L583:
	add	r7, r5, #1
	ldrb	r7, [r3, r7]	@ zero_extendqisi2
	and	r7, r7, #3
	orr	r6, r6, r7, asl r2
	cmp	r2, #0
	streqb	r6, [r1], #1
	add	r7, r5, #2
	ldrb	r7, [r3, r7]	@ zero_extendqisi2
	moveq	r6, r2
	subne	r2, r2, #2
	moveq	r2, #6
	and	r7, r7, #3
	orr	r6, r6, r7, asl r2
	cmp	r2, #0
	streqb	r6, [r1], #1
	subne	r2, r2, #2
	moveq	r6, r2
	add	r5, r5, #3
	moveq	r2, #6
.L365:
	ldrb	r7, [r3, r5]	@ zero_extendqisi2
	and	r7, r7, #3
	orr	r6, r6, r7, asl r2
	cmp	r2, #0
	streqb	r6, [r1], #1
	subne	r2, r2, #2
	moveq	r6, r2
	moveq	r2, #6
	cmp	ip, r5
	bhi	.L643
	cmp	r2, #6
	beq	.L625
	strb	r6, [r1, #0]
	ldr	r2, [r4, #140]
	ldr	ip, [r4, #284]
	b	.L353
.L639:
	ldr	ip, [r4, #284]
	cmp	ip, #0
	beq	.L374
	ldrb	r7, [r2, #2]	@ zero_extendqisi2
	ldrb	r6, [r2, #3]	@ zero_extendqisi2
	ldrb	r0, [r2, #1]	@ zero_extendqisi2
	ldrb	r5, [r2, #4]	@ zero_extendqisi2
	mov	r3, r2
	sub	r1, ip, #1
	cmp	ip, #1
	strb	r7, [r2, #1]
	strb	r6, [r2, #2]
	strb	r5, [r2, #3]
	strb	r0, [r3, #4]!
	and	r2, r1, #3
	mov	r0, #1
	bls	.L576
	cmp	r2, #0
	beq	.L377
	cmp	r2, #1
	beq	.L553
	cmp	r2, #2
	beq	.L554
	ldrb	r0, [r3, #3]	@ zero_extendqisi2
	ldrb	r5, [r3, #2]	@ zero_extendqisi2
	ldrb	r6, [r3, #1]	@ zero_extendqisi2
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	strb	r0, [r3, #2]
	strb	r5, [r3, #1]
	strb	r2, [r3, #3]
	strb	r6, [r3, #4]!
	mov	r0, #2
.L554:
	ldrb	r6, [r3, #2]	@ zero_extendqisi2
	ldrb	r5, [r3, #3]	@ zero_extendqisi2
	ldrb	r2, [r3, #1]	@ zero_extendqisi2
	ldrb	r1, [r3, #4]	@ zero_extendqisi2
	strb	r6, [r3, #1]
	strb	r5, [r3, #2]
	strb	r1, [r3, #3]
	strb	r2, [r3, #4]!
	add	r0, r0, #1
.L553:
	ldrb	r6, [r3, #2]	@ zero_extendqisi2
	ldrb	r5, [r3, #3]	@ zero_extendqisi2
	ldrb	r2, [r3, #1]	@ zero_extendqisi2
	ldrb	r1, [r3, #4]	@ zero_extendqisi2
	add	r0, r0, #1
	cmp	ip, r0
	strb	r6, [r3, #1]
	strb	r5, [r3, #2]
	strb	r1, [r3, #3]
	strb	r2, [r3, #4]!
	bls	.L576
.L377:
	ldrb	r6, [r3, #2]	@ zero_extendqisi2
	ldrb	r5, [r3, #3]	@ zero_extendqisi2
	ldrb	r8, [r3, #1]	@ zero_extendqisi2
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	mov	r1, r3
	strb	r2, [r3, #3]
	strb	r6, [r3, #1]
	strb	r5, [r3, #2]
	strb	r8, [r1, #4]!
	ldrb	r7, [r1, #3]	@ zero_extendqisi2
	ldrb	r8, [r1, #2]	@ zero_extendqisi2
	ldrb	r5, [r1, #1]	@ zero_extendqisi2
	ldrb	r6, [r1, #4]	@ zero_extendqisi2
	mov	r2, r1
	strb	r8, [r1, #1]
	strb	r7, [r1, #2]
	strb	r6, [r1, #3]
	strb	r5, [r2, #4]!
	ldrb	r7, [r2, #1]	@ zero_extendqisi2
	ldrb	r6, [r2, #2]	@ zero_extendqisi2
	ldrb	r5, [r2, #3]	@ zero_extendqisi2
	ldrb	r1, [r2, #4]	@ zero_extendqisi2
	strb	r6, [r2, #1]
	strb	r7, [r2, #4]
	strb	r5, [r2, #2]
	strb	r1, [r2, #3]
	add	r2, r3, #12
	ldrb	r7, [r2, #1]	@ zero_extendqisi2
	ldrb	r6, [r2, #2]	@ zero_extendqisi2
	ldrb	r5, [r2, #3]	@ zero_extendqisi2
	ldrb	r1, [r2, #4]	@ zero_extendqisi2
	add	r0, r0, #4
	cmp	ip, r0
	strb	r7, [r2, #4]
	strb	r6, [r2, #1]
	strb	r5, [r2, #2]
	strb	r1, [r2, #3]
	add	r3, r3, #16
	bhi	.L377
	ldr	r1, [r4, #140]
	b	.L374
.L640:
	ldr	r0, [r4, #284]
	cmp	r0, #0
	beq	.L382
	ldrb	ip, [r2, #4]	@ zero_extendqisi2
	sub	r1, r0, #1
	mvn	r3, ip
	cmp	r0, #1
	strb	r3, [r2, #4]!
	and	r1, r1, #3
	mov	r3, #1
	bls	.L580
	cmp	r1, #0
	beq	.L385
	cmp	r1, #1
	beq	.L545
	cmp	r1, #2
	ldrneb	r3, [r2, #4]	@ zero_extendqisi2
	mvnne	r3, r3
	strneb	r3, [r2, #4]!
	ldrb	ip, [r2, #4]	@ zero_extendqisi2
	mvn	r1, ip
	movne	r3, #2
	strb	r1, [r2, #4]!
	add	r3, r3, #1
.L545:
	ldrb	ip, [r2, #4]	@ zero_extendqisi2
	add	r3, r3, #1
	mvn	r1, ip
	cmp	r0, r3
	strb	r1, [r2, #4]!
	bls	.L580
.L385:
	ldrb	r1, [r2, #4]	@ zero_extendqisi2
	mvn	ip, r1
	mov	r1, r2
	strb	ip, [r1, #4]!
	ldrb	ip, [r1, #4]	@ zero_extendqisi2
	mvn	ip, ip
	strb	ip, [r1, #4]!
	ldrb	ip, [r1, #4]	@ zero_extendqisi2
	mvn	ip, ip
	strb	ip, [r1, #4]
	add	r1, r2, #12
	ldrb	ip, [r1, #4]	@ zero_extendqisi2
	add	r3, r3, #4
	mvn	ip, ip
	cmp	r0, r3
	strb	ip, [r1, #4]
	add	r2, r2, #16
	bhi	.L385
	ldr	r1, [r4, #140]
	b	.L382
.L638:
	cmp	ip, #0
	beq	.L374
	ldrb	r0, [r2, #1]	@ zero_extendqisi2
	ldrb	r5, [r2, #2]	@ zero_extendqisi2
	mov	r3, r2
	sub	r1, ip, #1
	cmp	ip, #1
	strb	r5, [r2, #1]
	strb	r0, [r3, #2]!
	and	r2, r1, #3
	mov	r0, #1
	bls	.L576
	cmp	r2, #0
	beq	.L380
	cmp	r2, #1
	beq	.L557
	cmp	r2, #2
	ldrneb	r2, [r3, #1]	@ zero_extendqisi2
	ldrneb	r1, [r3, #2]	@ zero_extendqisi2
	strneb	r1, [r3, #1]
	strneb	r2, [r3, #2]!
	ldrb	r2, [r3, #1]	@ zero_extendqisi2
	ldrb	r1, [r3, #2]	@ zero_extendqisi2
	movne	r0, #2
	strb	r1, [r3, #1]
	strb	r2, [r3, #2]!
	add	r0, r0, #1
.L557:
	ldrb	r2, [r3, #1]	@ zero_extendqisi2
	ldrb	r1, [r3, #2]	@ zero_extendqisi2
	add	r0, r0, #1
	cmp	ip, r0
	strb	r1, [r3, #1]
	strb	r2, [r3, #2]!
	bls	.L576
.L380:
	ldrb	r6, [r3, #1]	@ zero_extendqisi2
	ldrb	r2, [r3, #2]	@ zero_extendqisi2
	mov	r1, r3
	strb	r2, [r3, #1]
	strb	r6, [r1, #2]!
	ldrb	r5, [r1, #1]	@ zero_extendqisi2
	ldrb	r6, [r1, #2]	@ zero_extendqisi2
	mov	r2, r1
	strb	r6, [r1, #1]
	strb	r5, [r2, #2]!
	ldrb	r5, [r2, #1]	@ zero_extendqisi2
	ldrb	r1, [r2, #2]	@ zero_extendqisi2
	strb	r5, [r2, #2]
	strb	r1, [r2, #1]
	add	r2, r3, #6
	ldrb	r5, [r2, #1]	@ zero_extendqisi2
	ldrb	r1, [r2, #2]	@ zero_extendqisi2
	add	r0, r0, #4
	cmp	ip, r0
	strb	r5, [r2, #2]
	strb	r1, [r2, #1]
	add	r3, r3, #8
	bhi	.L380
	ldr	r1, [r4, #140]
	b	.L374
.L636:
	ldr	r0, [r4, #284]
	cmp	r0, #0
	beq	.L382
	ldrb	ip, [r2, #2]	@ zero_extendqisi2
	sub	r1, r0, #1
	mvn	r3, ip
	cmp	r0, #1
	strb	r3, [r2, #2]!
	and	r1, r1, #3
	mov	r3, #1
	bls	.L580
	cmp	r1, #0
	beq	.L388
	cmp	r1, #1
	beq	.L549
	cmp	r1, #2
	ldrneb	r3, [r2, #2]	@ zero_extendqisi2
	mvnne	r3, r3
	strneb	r3, [r2, #2]!
	ldrb	r1, [r2, #2]	@ zero_extendqisi2
	mvn	ip, r1
	movne	r3, #2
	strb	ip, [r2, #2]!
	add	r3, r3, #1
.L549:
	ldrb	r1, [r2, #2]	@ zero_extendqisi2
	add	r3, r3, #1
	mvn	ip, r1
	cmp	r0, r3
	strb	ip, [r2, #2]!
	bls	.L580
.L388:
	ldrb	r1, [r2, #2]	@ zero_extendqisi2
	mvn	ip, r1
	mov	r1, r2
	strb	ip, [r1, #2]!
	ldrb	ip, [r1, #2]	@ zero_extendqisi2
	mvn	ip, ip
	strb	ip, [r1, #2]!
	ldrb	ip, [r1, #2]	@ zero_extendqisi2
	mvn	ip, ip
	strb	ip, [r1, #2]
	add	r1, r2, #6
	ldrb	ip, [r1, #2]	@ zero_extendqisi2
	add	r3, r3, #4
	mvn	ip, ip
	cmp	r0, r3
	strb	ip, [r1, #2]
	add	r2, r2, #8
	bhi	.L388
	ldr	r1, [r4, #140]
	b	.L382
.L625:
	ldr	r2, [r4, #140]
	ldr	ip, [r4, #284]
	b	.L353
	.size	png_do_write_transformations, .-png_do_write_transformations
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
