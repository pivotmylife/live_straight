	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"pngread.c"
	.section	.text.png_set_read_status_fn,"ax",%progbits
	.align	2
	.global	png_set_read_status_fn
	.hidden	png_set_read_status_fn
	.type	png_set_read_status_fn, %function
png_set_read_status_fn:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	strne	r1, [r0, #436]
	bx	lr
	.size	png_set_read_status_fn, .-png_set_read_status_fn
	.section	.text.png_read_destroy,"ax",%progbits
	.align	2
	.global	png_read_destroy
	.hidden	png_read_destroy
	.type	png_read_destroy, %function
png_read_destroy:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 96
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	cmp	r1, #0
	sub	sp, sp, #100
	mov	r5, r2
	mov	r4, r0
	blne	png_info_destroy
.L5:
	cmp	r5, #0
	movne	r1, r5
	movne	r0, r4
	blne	png_info_destroy
.L6:
	ldr	r1, [r4, #200]
	mov	r0, r4
	bl	png_free
	mov	r0, r4
	ldr	r1, [r4, #620]
	bl	png_free
	mov	r0, r4
	ldr	r1, [r4, #260]
	bl	png_free
	mov	r0, r4
	ldr	r1, [r4, #676]
	bl	png_free
	mov	r0, r4
	ldr	r1, [r4, #520]
	bl	png_free
	mov	r0, r4
	ldr	r1, [r4, #524]
	bl	png_free
	mov	r0, r4
	ldr	r1, [r4, #384]
	bl	png_free
	mov	r0, r4
	ldr	r1, [r4, #388]
	bl	png_free
	mov	r0, r4
	ldr	r1, [r4, #392]
	bl	png_free
	ldr	r3, [r4, #560]
	tst	r3, #4096
	bne	.L60
	tst	r3, #8192
	bic	r3, r3, #4096
	str	r3, [r4, #560]
	bne	.L61
.L8:
	tst	r3, #8
	bic	r3, r3, #8192
	str	r3, [r4, #560]
	bne	.L62
.L9:
	ldr	r1, [r4, #396]
	bic	r3, r3, #8
	cmp	r1, #0
	str	r3, [r4, #560]
	beq	.L10
	ldr	r0, [r4, #372]
	mov	r6, #1
	rsb	r7, r0, #8
	mov	r7, r6, asl r7
	cmp	r7, #0
	ble	.L11
	sub	r8, r7, #1
	ands	r8, r8, #3
	mov	r5, #0
	beq	.L13
	ldr	r1, [r1, #0]
	mov	r0, r4
	bl	png_free
	cmp	r8, #1
	mov	r5, r6
	ldr	r1, [r4, #396]
	beq	.L13
	cmp	r8, #2
	beq	.L55
	ldr	r1, [r1, r6, asl #2]
	mov	r0, r4
	bl	png_free
	ldr	r1, [r4, #396]
	mov	r5, #2
.L55:
	ldr	r1, [r1, r5, asl #2]
	mov	r0, r4
	bl	png_free
	add	r5, r5, #1
	ldr	r1, [r4, #396]
	b	.L13
.L63:
	ldr	lr, [r4, #396]
	mov	r0, r4
	ldr	r1, [lr, r6, asl #2]
	bl	png_free
	ldr	sl, [r4, #396]
	add	ip, r6, #1
	ldr	r1, [sl, ip, asl #2]
	mov	r0, r4
	bl	png_free
	ldr	r6, [r4, #396]
	add	r2, r5, #3
	ldr	r1, [r6, r2, asl #2]
	mov	r0, r4
	bl	png_free
	ldr	r1, [r4, #396]
	add	r5, r5, #4
.L13:
	ldr	r1, [r1, r5, asl #2]
	add	r6, r5, #1
	mov	r0, r4
	bl	png_free
	cmp	r7, r6
	bgt	.L63
	ldr	r1, [r4, #396]
.L11:
	mov	r0, r4
	bl	png_free
.L10:
	ldr	r1, [r4, #400]
	cmp	r1, #0
	beq	.L14
	ldr	r3, [r4, #372]
	mov	r6, #1
	rsb	r5, r3, #8
	mov	r7, r6, asl r5
	cmp	r7, #0
	ble	.L15
	sub	r0, r7, #1
	ands	r8, r0, #3
	mov	r5, #0
	beq	.L17
	ldr	r1, [r1, #0]
	mov	r0, r4
	bl	png_free
	cmp	r8, #1
	mov	r5, r6
	ldr	r1, [r4, #400]
	beq	.L17
	cmp	r8, #2
	beq	.L54
	ldr	r1, [r1, r6, asl #2]
	mov	r0, r4
	bl	png_free
	ldr	r1, [r4, #400]
	mov	r5, #2
.L54:
	ldr	r1, [r1, r5, asl #2]
	mov	r0, r4
	bl	png_free
	add	r5, r5, #1
	ldr	r1, [r4, #400]
	b	.L17
.L64:
	ldr	ip, [r4, #400]
	mov	r0, r4
	ldr	r1, [ip, r6, asl #2]
	bl	png_free
	ldr	sl, [r4, #400]
	add	r6, r6, #1
	ldr	r1, [sl, r6, asl #2]
	mov	r0, r4
	bl	png_free
	ldr	r2, [r4, #400]
	add	r8, r5, #3
	ldr	r1, [r2, r8, asl #2]
	mov	r0, r4
	bl	png_free
	ldr	r1, [r4, #400]
	add	r5, r5, #4
.L17:
	ldr	r1, [r1, r5, asl #2]
	add	r6, r5, #1
	mov	r0, r4
	bl	png_free
	cmp	r7, r6
	bgt	.L64
	ldr	r1, [r4, #400]
.L15:
	mov	r0, r4
	bl	png_free
.L14:
	ldr	r1, [r4, #404]
	cmp	r1, #0
	beq	.L18
	ldr	lr, [r4, #372]
	mov	r6, #1
	rsb	r7, lr, #8
	mov	r7, r6, asl r7
	cmp	r7, #0
	ble	.L19
	sub	r5, r7, #1
	ands	r8, r5, #3
	mov	r5, #0
	beq	.L21
	ldr	r1, [r1, #0]
	mov	r0, r4
	bl	png_free
	cmp	r8, #1
	mov	r5, r6
	ldr	r1, [r4, #404]
	beq	.L21
	cmp	r8, #2
	beq	.L53
	ldr	r1, [r1, r6, asl #2]
	mov	r0, r4
	bl	png_free
	ldr	r1, [r4, #404]
	mov	r5, #2
.L53:
	ldr	r1, [r1, r5, asl #2]
	mov	r0, r4
	bl	png_free
	add	r5, r5, #1
	ldr	r1, [r4, #404]
	b	.L21
.L65:
	ldr	sl, [r4, #404]
	mov	r0, r4
	ldr	r1, [sl, r6, asl #2]
	bl	png_free
	ldr	r8, [r4, #404]
	add	r2, r6, #1
	ldr	r1, [r8, r2, asl #2]
	mov	r0, r4
	bl	png_free
	ldr	r3, [r4, #404]
	add	r0, r5, #3
	ldr	r1, [r3, r0, asl #2]
	mov	r0, r4
	bl	png_free
	ldr	r1, [r4, #404]
	add	r5, r5, #4
.L21:
	ldr	r1, [r1, r5, asl #2]
	add	r6, r5, #1
	mov	r0, r4
	bl	png_free
	cmp	r7, r6
	bgt	.L65
	ldr	r1, [r4, #404]
.L19:
	mov	r0, r4
	bl	png_free
.L18:
	ldr	r1, [r4, #556]
	mov	r0, r4
	bl	png_free
	add	r0, r4, #144
	bl	inflateEnd
	mov	r0, r4
	ldr	r1, [r4, #460]
	bl	png_free
	add	r5, sp, #4
	mov	r0, r4
	ldr	r1, [r4, #512]
	bl	png_free
	mov	r1, r4
	mov	r2, #92
	mov	r0, r5
	bl	memcpy
	ldr	sl, [r4, #92]
	ldr	r8, [r4, #96]
	ldr	r7, [r4, #100]
	ldr	r6, [r4, #616]
	mov	r1, #0
	mov	r2, #680
	mov	r0, r4
	bl	memset
	str	sl, [r4, #92]
	str	r8, [r4, #96]
	str	r7, [r4, #100]
	str	r6, [r4, #616]
	mov	r0, r4
	mov	r1, r5
	mov	r2, #92
	bl	memcpy
	add	sp, sp, #100
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	bx	lr
.L60:
	mov	r0, r4
	ldr	r1, [r4, #304]
	bl	png_zfree
	ldr	r3, [r4, #560]
	tst	r3, #8192
	bic	r3, r3, #4096
	str	r3, [r4, #560]
	beq	.L8
.L61:
	mov	r0, r4
	ldr	r1, [r4, #420]
	bl	png_free
	ldr	r3, [r4, #560]
	tst	r3, #8
	bic	r3, r3, #8192
	str	r3, [r4, #560]
	beq	.L9
.L62:
	mov	r0, r4
	ldr	r1, [r4, #528]
	bl	png_free
	ldr	r3, [r4, #560]
	b	.L9
	.size	png_read_destroy, .-png_read_destroy
	.section	.text.png_destroy_read_struct,"ax",%progbits
	.align	2
	.global	png_destroy_read_struct
	.hidden	png_destroy_read_struct
	.type	png_destroy_read_struct, %function
png_destroy_read_struct:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, fp, lr}
	subs	fp, r0, #0
	mov	r7, r1
	mov	r8, r2
	beq	.L74
	ldr	r4, [fp, #0]
	cmp	r4, #0
	beq	.L74
	cmp	r1, #0
	ldrne	r6, [r1, #0]
	moveq	r6, r1
	cmp	r2, #0
	ldrne	r5, [r2, #0]
	moveq	r5, r2
	mov	r0, r4
	mov	r1, r6
	mov	r2, r5
	ldr	sl, [r4, #616]
	ldr	r9, [r4, #608]
	bl	png_read_destroy
	cmp	r6, #0
	beq	.L72
	mvn	r3, #0
	mov	r0, r4
	mov	r1, r6
	mov	r2, #16384
	bl	png_free_data
	mov	r0, r6
	mov	r1, sl
	mov	r2, r9
	bl	png_destroy_struct_2
	mov	r3, #0
	str	r3, [r7, #0]
.L72:
	cmp	r5, #0
	beq	.L73
	mvn	r3, #0
	mov	r0, r4
	mov	r1, r5
	mov	r2, #16384
	bl	png_free_data
	mov	r0, r5
	mov	r1, sl
	mov	r2, r9
	bl	png_destroy_struct_2
	mov	r0, #0
	str	r0, [r8, #0]
.L73:
	mov	r1, sl
	mov	r0, r4
	mov	r2, r9
	bl	png_destroy_struct_2
	mov	r1, #0
	str	r1, [fp, #0]
.L74:
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
	.size	png_destroy_read_struct, .-png_destroy_read_struct
	.section	.text.png_read_end,"ax",%progbits
	.align	2
	.global	png_read_end
	.hidden	png_read_end
	.type	png_read_end, %function
png_read_end:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, lr}
	subs	r4, r0, #0
	mov	r7, r1
	beq	.L105
	mov	r1, #0
	bl	png_crc_finish
	add	r5, r4, #312
	b	.L104
.L109:
	mov	r2, r6
	mov	r0, r4
	mov	r1, r7
	bl	png_handle_IEND
	ldr	r3, [r4, #132]
.L78:
	tst	r3, #16
	bne	.L105
.L104:
	mov	r0, r4
	bl	png_read_chunk_header
	ldr	r1, .L127
	mov	r6, r0
	mov	r2, #4
	mov	r0, r5
	bl	memcmp
	cmp	r0, #0
	beq	.L108
	mov	r0, r5
	ldr	r1, .L127+4
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L109
	mov	r1, r5
	mov	r0, r4
	bl	png_handle_as_unknown
	cmp	r0, #0
	ldr	r1, .L127+8
	mov	r0, r5
	mov	r2, #4
	beq	.L80
	bl	memcmp
	cmp	r0, #0
	bne	.L81
	cmp	r6, #0
	bne	.L82
	ldr	r3, [r4, #132]
	tst	r3, #8192
	beq	.L81
.L82:
	mov	r0, r4
	ldr	r1, .L127+12
	bl	png_error
.L81:
	mov	r2, r6
	mov	r1, r7
	mov	r0, r4
	bl	png_handle_unknown
	mov	r0, r5
	ldr	r1, .L127+16
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	bne	.L107
	ldr	r0, [r4, #132]
	orr	r3, r0, #2
	tst	r3, #16
	str	r3, [r4, #132]
	beq	.L104
.L105:
	ldmfd	sp!, {r3, r4, r5, r6, r7, lr}
	bx	lr
.L108:
	mov	r2, r6
	mov	r0, r4
	mov	r1, r7
	bl	png_handle_IHDR
	ldr	r3, [r4, #132]
	b	.L78
.L80:
	bl	memcmp
	cmp	r0, #0
	beq	.L110
	mov	r0, r5
	ldr	r1, .L127+16
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L111
	mov	r0, r5
	ldr	r1, .L127+20
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L112
	mov	r0, r5
	ldr	r1, .L127+24
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L113
	mov	r0, r5
	ldr	r1, .L127+28
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L114
	mov	r0, r5
	ldr	r1, .L127+32
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L115
	mov	r0, r5
	ldr	r1, .L127+36
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L116
	mov	r0, r5
	ldr	r1, .L127+40
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L117
	mov	r0, r5
	ldr	r1, .L127+44
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L118
	mov	r0, r5
	ldr	r1, .L127+48
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L119
	mov	r0, r5
	ldr	r1, .L127+52
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L120
	mov	r0, r5
	ldr	r1, .L127+56
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L121
	mov	r0, r5
	ldr	r1, .L127+60
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L122
	mov	r0, r5
	ldr	r1, .L127+64
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L123
	mov	r0, r5
	ldr	r1, .L127+68
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L124
	mov	r0, r5
	ldr	r1, .L127+72
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L125
	mov	r0, r5
	ldr	r1, .L127+76
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L126
	ldr	r1, .L127+80
	mov	r2, #4
	mov	r0, r5
	bl	memcmp
	cmp	r0, #0
	mov	r2, r6
	mov	r0, r4
	mov	r1, r7
	bne	.L103
	bl	png_handle_zTXt
	ldr	r3, [r4, #132]
	b	.L78
.L103:
	bl	png_handle_unknown
.L107:
	ldr	r3, [r4, #132]
	b	.L78
.L110:
	cmp	r6, #0
	bne	.L85
	ldr	r1, [r4, #132]
	tst	r1, #8192
	beq	.L86
.L85:
	mov	r0, r4
	ldr	r1, .L127+12
	bl	png_error
.L86:
	mov	r1, r6
	mov	r0, r4
	bl	png_crc_finish
	ldr	r3, [r4, #132]
	b	.L78
.L113:
	mov	r2, r6
	mov	r0, r4
	mov	r1, r7
	bl	png_handle_cHRM
	ldr	r3, [r4, #132]
	b	.L78
.L111:
	mov	r2, r6
	mov	r0, r4
	mov	r1, r7
	bl	png_handle_PLTE
	ldr	r3, [r4, #132]
	b	.L78
.L112:
	mov	r2, r6
	mov	r0, r4
	mov	r1, r7
	bl	png_handle_bKGD
	ldr	r3, [r4, #132]
	b	.L78
.L114:
	mov	r2, r6
	mov	r0, r4
	mov	r1, r7
	bl	png_handle_gAMA
	ldr	r3, [r4, #132]
	b	.L78
.L117:
	mov	r2, r6
	mov	r0, r4
	mov	r1, r7
	bl	png_handle_pCAL
	ldr	r3, [r4, #132]
	b	.L78
.L115:
	mov	r2, r6
	mov	r0, r4
	mov	r1, r7
	bl	png_handle_hIST
	ldr	r3, [r4, #132]
	b	.L78
.L116:
	mov	r2, r6
	mov	r0, r4
	mov	r1, r7
	bl	png_handle_oFFs
	ldr	r3, [r4, #132]
	b	.L78
.L118:
	mov	r2, r6
	mov	r0, r4
	mov	r1, r7
	bl	png_handle_sCAL
	ldr	r3, [r4, #132]
	b	.L78
.L119:
	mov	r2, r6
	mov	r0, r4
	mov	r1, r7
	bl	png_handle_pHYs
	ldr	r3, [r4, #132]
	b	.L78
.L120:
	mov	r2, r6
	mov	r0, r4
	mov	r1, r7
	bl	png_handle_sBIT
	ldr	r3, [r4, #132]
	b	.L78
.L121:
	mov	r2, r6
	mov	r0, r4
	mov	r1, r7
	bl	png_handle_sRGB
	ldr	r3, [r4, #132]
	b	.L78
.L122:
	mov	r2, r6
	mov	r0, r4
	mov	r1, r7
	bl	png_handle_iCCP
	ldr	r3, [r4, #132]
	b	.L78
.L123:
	mov	r2, r6
	mov	r0, r4
	mov	r1, r7
	bl	png_handle_sPLT
	ldr	r3, [r4, #132]
	b	.L78
.L124:
	mov	r2, r6
	mov	r0, r4
	mov	r1, r7
	bl	png_handle_tEXt
	ldr	r3, [r4, #132]
	b	.L78
.L125:
	mov	r2, r6
	mov	r0, r4
	mov	r1, r7
	bl	png_handle_tIME
	ldr	r3, [r4, #132]
	b	.L78
.L126:
	mov	r2, r6
	mov	r0, r4
	mov	r1, r7
	bl	png_handle_tRNS
	ldr	r3, [r4, #132]
	b	.L78
.L128:
	.align	2
.L127:
	.word	png_IHDR
	.word	png_IEND
	.word	png_IDAT
	.word	.LC0
	.word	png_PLTE
	.word	png_bKGD
	.word	png_cHRM
	.word	png_gAMA
	.word	png_hIST
	.word	png_oFFs
	.word	png_pCAL
	.word	png_sCAL
	.word	png_pHYs
	.word	png_sBIT
	.word	png_sRGB
	.word	png_iCCP
	.word	png_sPLT
	.word	png_tEXt
	.word	png_tIME
	.word	png_tRNS
	.word	png_zTXt
	.size	png_read_end, .-png_read_end
	.section	.text.png_start_read_image,"ax",%progbits
	.align	2
	.global	png_start_read_image
	.hidden	png_start_read_image
	.type	png_start_read_image, %function
png_start_read_image:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, lr}
	subs	r3, r0, #0
	beq	.L131
	ldr	r3, [r3, #136]
	tst	r3, #64
	bleq	png_read_start_row
.L131:
	ldmfd	sp!, {r3, lr}
	bx	lr
	.size	png_start_read_image, .-png_start_read_image
	.section	.text.png_read_row,"ax",%progbits
	.align	2
	.global	png_read_row
	.hidden	png_read_row
	.type	png_read_row, %function
png_read_row:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	subs	r4, r0, #0
	sub	sp, sp, #8
	mov	r9, r1
	mov	r5, r2
	beq	.L177
	ldr	r3, [r4, #136]
	tst	r3, #64
	beq	.L184
.L134:
	ldrb	r0, [r4, #319]	@ zero_extendqisi2
	cmp	r0, #0
	bne	.L185
.L135:
	ldr	r7, [r4, #132]
	tst	r7, #4
	beq	.L186
.L153:
	ldr	sl, [r4, #264]
	ldr	r8, [r4, #248]
	str	sl, [r4, #156]
	str	r8, [r4, #160]
	ldr	sl, .L192
	add	r8, r4, #144
	add	r7, r4, #312
	b	.L165
.L154:
	mov	r0, r8
	mov	r1, #1
	bl	inflate
	cmp	r0, #1
	beq	.L187
.L159:
	cmp	r0, #0
	beq	.L163
	ldr	r1, [r4, #168]
	cmp	r1, #0
	moveq	r1, sl
	mov	r0, r4
	bl	png_error
.L163:
	ldr	ip, [r4, #160]
	cmp	ip, #0
	beq	.L162
.L165:
	ldr	ip, [r4, #148]
	cmp	ip, #0
	bne	.L154
.L182:
	ldr	r6, [r4, #296]
.L181:
	cmp	r6, #0
	mov	r1, r6
	mov	r0, r4
	bne	.L188
	bl	png_crc_finish
	mov	r0, r4
	bl	png_read_chunk_header
	ldr	r1, .L192+4
	str	r0, [r4, #296]
	mov	r6, r0
	mov	r2, #4
	mov	r0, r7
	bl	memcmp
	cmp	r0, #0
	beq	.L181
	mov	r0, r4
	ldr	r1, .L192+8
	bl	png_error
	b	.L182
.L188:
	ldr	lr, [r4, #204]
	ldr	r2, [r4, #296]
	ldr	r1, [r4, #200]
	cmp	lr, r2
	movls	r2, lr
	str	lr, [r4, #148]
	str	r1, [r4, #144]
	strhi	r2, [r4, #148]
	mov	r0, r4
	bl	png_crc_read
	ldr	r2, [r4, #296]
	ldr	r3, [r4, #148]
	rsb	r1, r3, r2
	str	r1, [r4, #296]
	mov	r0, r8
	mov	r1, #1
	bl	inflate
	cmp	r0, #1
	bne	.L159
.L187:
	ldr	r0, [r4, #160]
	cmp	r0, #0
	bne	.L160
	ldr	ip, [r4, #148]
	cmp	ip, #0
	bne	.L160
	ldr	r1, [r4, #296]
	cmp	r1, #0
	beq	.L161
.L160:
	mov	r0, r4
	ldr	r1, .L192+12
	bl	png_error
.L161:
	add	r0, r4, #132
	ldmia	r0, {r0, lr}	@ phole ldm
	orr	r2, r0, #8
	orr	r3, lr, #32
	str	r2, [r4, #132]
	str	r3, [r4, #136]
.L162:
	ldrb	r3, [r4, #325]	@ zero_extendqisi2
	ldr	r2, [r4, #252]
	cmp	r3, #7
	str	r2, [r4, #284]
	mulls	r2, r3, r2
	strb	r3, [r4, #295]
	movhi	r3, r3, lsr #3
	mulhi	r2, r3, r2
	addls	r2, r2, #7
	ldrb	r0, [r4, #322]	@ zero_extendqisi2
	ldrb	lr, [r4, #326]	@ zero_extendqisi2
	ldrb	r1, [r4, #323]	@ zero_extendqisi2
	movls	r2, r2, lsr #3
	str	r2, [r4, #288]
	strb	r0, [r4, #292]
	strb	lr, [r4, #294]
	strb	r1, [r4, #293]
	ldr	r2, [r4, #264]
	ldrb	ip, [r2, #0]	@ zero_extendqisi2
	cmp	ip, #0
	bne	.L189
.L168:
	ldr	r1, [r4, #244]
	mov	r0, r4
	add	r3, r1, #1
	ldr	r1, [r4, #260]
	bl	png_memcpy_check
	ldr	ip, [r4, #588]
	tst	ip, #4
	bne	.L190
.L169:
	ldr	r0, [r4, #140]
	cmp	r0, #0
	bne	.L170
	ldr	r3, [r4, #136]
	tst	r3, #4194304
	beq	.L171
.L170:
	mov	r0, r4
	bl	png_do_read_transformations
.L171:
	ldrb	ip, [r4, #319]	@ zero_extendqisi2
	cmp	ip, #0
	beq	.L172
	ldr	r1, [r4, #140]
	tst	r1, #2
	beq	.L172
	ldrb	r2, [r4, #320]	@ zero_extendqisi2
	cmp	r2, #5
	bls	.L191
.L173:
	cmp	r5, #0
	beq	.L174
	ldr	r0, .L192+16
	ldrb	lr, [r4, #320]	@ zero_extendqisi2
	mov	r1, r5
	ldr	r2, [r0, lr, asl #2]
	mov	r0, r4
	bl	png_combine_row
.L174:
	cmp	r9, #0
	beq	.L175
	ldrb	ip, [r4, #320]	@ zero_extendqisi2
	ldr	r3, .L192+20
	mov	r1, r9
	ldr	r2, [r3, ip, asl #2]
	mov	r0, r4
	bl	png_combine_row
.L175:
	mov	r0, r4
	bl	png_read_finish_row
	ldr	r3, [r4, #436]
	cmp	r3, #0
	movne	r0, r4
	ldrne	r1, [r4, #256]
	ldrneb	r2, [r4, #320]	@ zero_extendqisi2
	movne	lr, pc
	bxne	r3
.L177:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L172:
	cmp	r9, #0
	movne	r1, r9
	movne	r0, r4
	movne	r2, #255
	blne	png_combine_row
.L176:
	cmp	r5, #0
	beq	.L175
	mov	r1, r5
	mov	r0, r4
	mov	r2, #255
	bl	png_combine_row
	b	.L175
.L185:
	ldr	r1, [r4, #140]
	tst	r1, #2
	beq	.L135
	ldrb	r2, [r4, #320]	@ zero_extendqisi2
	cmp	r2, #6
	ldrls	pc, [pc, r2, asl #2]
	b	.L135
.L143:
	.word	.L136
	.word	.L137
	.word	.L138
	.word	.L139
	.word	.L140
	.word	.L141
	.word	.L142
.L190:
	ldrb	r2, [r4, #596]	@ zero_extendqisi2
	cmp	r2, #64
	bne	.L169
	ldr	lr, [r4, #264]
	add	r0, r4, #284
	add	r1, lr, #1
	bl	png_do_read_intrapixel
	b	.L169
.L184:
	bl	png_read_start_row
	b	.L134
.L189:
	ldr	r3, [r4, #260]
	add	r2, r2, #1
	add	r3, r3, #1
	mov	r0, r4
	add	r1, r4, #284
	str	ip, [sp, #0]
	bl	png_read_filter_row
	ldr	r2, [r4, #264]
	b	.L168
.L186:
	mov	r0, r4
	ldr	r1, .L192+24
	bl	png_error
	b	.L153
.L191:
	mov	r0, r4
	bl	png_do_read_interlace
	b	.L173
.L142:
	ldr	r0, [r4, #256]
	tst	r0, #1
	bne	.L135
.L183:
	mov	r0, r4
	bl	png_read_finish_row
	b	.L177
.L137:
	ldr	r3, [r4, #256]
	tst	r3, #7
	bne	.L145
	ldr	r0, [r4, #228]
	cmp	r0, #4
	bhi	.L135
.L145:
	cmp	r5, #0
	beq	.L183
	ldr	ip, .L192+16
	mov	r1, r5
	ldr	r2, [ip, #4]
	mov	r0, r4
	bl	png_combine_row
	b	.L183
.L138:
	ldr	r3, [r4, #256]
	and	r1, r3, #7
	cmp	r1, #4
	beq	.L135
	cmp	r5, #0
	beq	.L183
	tst	r3, #4
	beq	.L183
	ldr	lr, .L192+16
	mov	r1, r5
	ldr	r2, [lr, #8]
	mov	r0, r4
	bl	png_combine_row
	b	.L183
.L139:
	ldr	r2, [r4, #256]
	tst	r2, #3
	bne	.L148
	ldr	r3, [r4, #228]
	cmp	r3, #2
	bhi	.L135
.L148:
	cmp	r5, #0
	beq	.L183
	ldr	r0, .L192+16
	mov	r1, r5
	ldr	r2, [r0, #12]
	mov	r0, r4
	bl	png_combine_row
	b	.L183
.L140:
	ldr	r3, [r4, #256]
	and	ip, r3, #3
	cmp	ip, #2
	beq	.L135
	cmp	r5, #0
	beq	.L183
	tst	r3, #2
	beq	.L183
	ldr	lr, .L192+16
	mov	r1, r5
	ldr	r2, [lr, #16]
	mov	r0, r4
	bl	png_combine_row
	b	.L183
.L136:
	ldr	ip, [r4, #256]
	tst	ip, #7
	beq	.L135
	cmp	r5, #0
	beq	.L183
	ldr	lr, .L192+16
	mov	r1, r5
	ldr	r2, [lr, #0]
	mov	r0, r4
	bl	png_combine_row
	b	.L183
.L141:
	ldr	r1, [r4, #256]
	tst	r1, #1
	bne	.L151
	ldr	r2, [r4, #228]
	cmp	r2, #1
	bhi	.L135
.L151:
	cmp	r5, #0
	beq	.L183
	ldr	r3, .L192+16
	mov	r1, r5
	ldr	r2, [r3, #20]
	mov	r0, r4
	bl	png_combine_row
	b	.L183
.L193:
	.align	2
.L192:
	.word	.LC4
	.word	png_IDAT
	.word	.LC2
	.word	.LC3
	.word	png_pass_dsp_mask
	.word	png_pass_mask
	.word	.LC1
	.size	png_read_row, .-png_read_row
	.section	.text.png_read_image,"ax",%progbits
	.align	2
	.global	png_read_image
	.hidden	png_read_image
	.type	png_read_image, %function
png_read_image:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, fp, lr}
	subs	r5, r0, #0
	mov	r6, r1
	beq	.L200
	bl	png_set_interlace_handling
	ldr	r7, [r5, #232]
	subs	fp, r0, #0
	str	r7, [r5, #236]
	movgt	r9, #0
	ble	.L200
.L196:
	cmp	r7, #0
	beq	.L199
	mov	r8, #0
	mov	r0, r5
	ldr	r1, [r6, r8]
	mov	r2, r8
	mov	r4, #1
	sub	sl, r7, #1
	bl	png_read_row
	cmp	r7, r4
	and	sl, sl, #3
	bls	.L199
	cmp	sl, #0
	beq	.L197
	cmp	sl, #1
	beq	.L213
	cmp	sl, #2
	beq	.L214
	mov	r2, r8
	mov	r0, r5
	ldr	r1, [r6, #4]
	bl	png_read_row
	mov	r4, #2
.L214:
	ldr	r1, [r6, r4, asl #2]
	mov	r0, r5
	mov	r2, #0
	bl	png_read_row
	add	r4, r4, #1
.L213:
	ldr	r1, [r6, r4, asl #2]
	mov	r0, r5
	add	r4, r4, #1
	mov	r2, #0
	bl	png_read_row
	cmp	r7, r4
	bls	.L199
.L197:
	ldr	r1, [r6, r4, asl #2]
	add	r8, r4, #1
	mov	r0, r5
	mov	r2, #0
	bl	png_read_row
	ldr	r1, [r6, r8, asl #2]
	mov	r0, r5
	mov	r2, #0
	add	r8, r8, #1
	bl	png_read_row
	ldr	r1, [r6, r8, asl #2]
	mov	r0, r5
	mov	r2, #0
	bl	png_read_row
	add	r3, r4, #3
	ldr	r1, [r6, r3, asl #2]
	add	r4, r4, #4
	mov	r0, r5
	mov	r2, #0
	bl	png_read_row
	cmp	r7, r4
	bhi	.L197
.L199:
	add	r9, r9, #1
	cmp	fp, r9
	bgt	.L196
.L200:
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
	.size	png_read_image, .-png_read_image
	.section	.text.png_read_rows,"ax",%progbits
	.align	2
	.global	png_read_rows
	.hidden	png_read_rows
	.type	png_read_rows, %function
png_read_rows:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	subs	r5, r0, #0
	mov	r4, r1
	mov	r6, r2
	mov	r8, r3
	beq	.L223
	subs	sl, r1, #0
	movne	sl, #1
	subs	r3, r2, #0
	movne	r3, #1
	ands	r7, r3, sl
	bne	.L268
	cmp	sl, #0
	beq	.L220
	cmp	r8, #0
	beq	.L223
	ldr	r1, [r1, #0]
	mov	r2, r7
	sub	r6, r8, #1
	bl	png_read_row
	cmp	r8, #1
	and	sl, r6, #3
	mov	r6, #1
	bls	.L223
	cmp	sl, #0
	beq	.L221
	cmp	sl, #1
	beq	.L262
	cmp	sl, #2
	beq	.L263
	mov	r2, r7
	mov	r0, r5
	ldr	r1, [r4, #4]
	bl	png_read_row
	mov	r6, #2
.L263:
	ldr	r1, [r4, r6, asl #2]
	mov	r0, r5
	mov	r2, #0
	bl	png_read_row
	add	r6, r6, #1
.L262:
	ldr	r1, [r4, r6, asl #2]
	mov	r0, r5
	add	r6, r6, #1
	mov	r2, #0
	bl	png_read_row
	cmp	r8, r6
	bls	.L223
.L221:
	ldr	r1, [r4, r6, asl #2]
	add	r7, r6, #1
	mov	r0, r5
	mov	r2, #0
	bl	png_read_row
	ldr	r1, [r4, r7, asl #2]
	mov	r0, r5
	mov	r2, #0
	add	r7, r7, #1
	bl	png_read_row
	ldr	r1, [r4, r7, asl #2]
	mov	r0, r5
	mov	r2, #0
	bl	png_read_row
	add	r0, r6, #3
	ldr	r1, [r4, r0, asl #2]
	add	r6, r6, #4
	mov	r0, r5
	mov	r2, #0
	bl	png_read_row
	cmp	r8, r6
	bhi	.L221
.L223:
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L268:
	cmp	r8, #0
	beq	.L223
	ldr	r1, [r1, #0]
	ldr	r2, [r2, #0]
	sub	sl, r8, #1
	bl	png_read_row
	cmp	r8, #1
	and	sl, sl, #3
	mov	r9, #1
	mov	r7, #4
	bls	.L223
	cmp	sl, #0
	beq	.L219
	cmp	sl, #1
	beq	.L260
	cmp	sl, #2
	beq	.L261
	mov	r0, r5
	ldr	r1, [r4, #4]
	ldr	r2, [r6, #4]
	bl	png_read_row
	mov	r9, #2
	mov	r7, #8
.L261:
	ldr	r1, [r4, r7]
	ldr	r2, [r6, r7]
	mov	r0, r5
	bl	png_read_row
	add	r9, r9, #1
	add	r7, r7, #4
.L260:
	ldr	r1, [r4, r7]
	ldr	r2, [r6, r7]
	add	r9, r9, #1
	mov	r0, r5
	bl	png_read_row
	cmp	r8, r9
	add	r7, r7, #4
	bls	.L223
.L219:
	add	sl, r7, #4
	ldr	r1, [r4, r7]
	ldr	r2, [r6, r7]
	mov	r0, r5
	bl	png_read_row
	ldr	r1, [r4, sl]
	ldr	r2, [r6, sl]
	mov	r0, r5
	add	sl, sl, #4
	bl	png_read_row
	ldr	r2, [r6, sl]
	mov	r0, r5
	ldr	r1, [r4, sl]
	bl	png_read_row
	add	r3, r7, #12
	ldr	r2, [r6, r3]
	add	r9, r9, #4
	mov	r0, r5
	ldr	r1, [r4, r3]
	bl	png_read_row
	cmp	r8, r9
	add	r7, r7, #16
	bhi	.L219
	b	.L223
.L220:
	cmp	r3, #0
	beq	.L223
	cmp	r8, #0
	beq	.L223
	mov	r1, sl
	ldr	r2, [r2, #0]
	sub	r4, r8, #1
	bl	png_read_row
	cmp	r8, #1
	and	r7, r4, #3
	mov	r4, #1
	bls	.L223
	cmp	r7, #0
	beq	.L222
	cmp	r7, #1
	beq	.L264
	cmp	r7, #2
	beq	.L265
	mov	r1, sl
	mov	r0, r5
	ldr	r2, [r6, #4]
	bl	png_read_row
	mov	r4, #2
.L265:
	ldr	r2, [r6, r4, asl #2]
	mov	r0, r5
	mov	r1, #0
	bl	png_read_row
	add	r4, r4, #1
.L264:
	ldr	r2, [r6, r4, asl #2]
	add	r4, r4, #1
	b	.L267
.L222:
	ldr	r2, [r6, r4, asl #2]
	add	r7, r4, #1
	mov	r0, r5
	mov	r1, #0
	bl	png_read_row
	ldr	r2, [r6, r7, asl #2]
	mov	r0, r5
	mov	r1, #0
	add	r7, r7, #1
	bl	png_read_row
	ldr	r2, [r6, r7, asl #2]
	mov	r1, #0
	mov	r0, r5
	bl	png_read_row
	add	r1, r4, #3
	ldr	r2, [r6, r1, asl #2]
	add	r4, r4, #4
.L267:
	mov	r0, r5
	mov	r1, #0
	bl	png_read_row
	cmp	r8, r4
	bhi	.L222
	b	.L223
	.size	png_read_rows, .-png_read_rows
	.section	.text.png_read_update_info,"ax",%progbits
	.align	2
	.global	png_read_update_info
	.hidden	png_read_update_info
	.type	png_read_update_info, %function
png_read_update_info:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	subs	r4, r0, #0
	mov	r5, r1
	beq	.L273
	ldr	r3, [r4, #136]
	tst	r3, #64
	beq	.L274
	ldr	r1, .L275
	bl	png_warning
.L272:
	mov	r0, r4
	mov	r1, r5
	bl	png_read_transform_info
.L273:
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L274:
	bl	png_read_start_row
	b	.L272
.L276:
	.align	2
.L275:
	.word	.LC5
	.size	png_read_update_info, .-png_read_update_info
	.section	.text.png_read_info,"ax",%progbits
	.align	2
	.global	png_read_info
	.hidden	png_read_info
	.type	png_read_info, %function
png_read_info:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	cmpne	r1, #0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	mov	r7, r1
	mov	r5, r0
	beq	.L310
	ldrb	r4, [r0, #328]	@ zero_extendqisi2
	cmp	r4, #7
	bls	.L314
.L279:
	add	r4, r5, #312
.L313:
	mov	r0, r5
	bl	png_read_chunk_header
	ldr	r1, .L338
	mov	r6, r0
	mov	r2, #4
	mov	r0, r4
	bl	memcmp
	cmp	r0, #0
	bne	.L282
	ldr	r0, [r5, #132]
	tst	r0, #8
	orrne	r0, r0, #8192
	strne	r0, [r5, #132]
.L282:
	mov	r0, r4
	ldr	r1, .L338+4
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L315
	mov	r0, r4
	ldr	r1, .L338+8
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L316
	mov	r0, r5
	mov	r1, r4
	bl	png_handle_as_unknown
	cmp	r0, #0
	mov	r0, r4
	beq	.L286
	ldr	r1, .L338
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	ldreq	r3, [r5, #132]
	orreq	r3, r3, #4
	streq	r3, [r5, #132]
	mov	r2, r6
	mov	r1, r7
	mov	r0, r5
	bl	png_handle_unknown
	mov	r0, r4
	ldr	r1, .L338+12
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	bne	.L288
	ldr	r6, [r5, #132]
	orr	r2, r6, #2
	str	r2, [r5, #132]
	b	.L313
.L315:
	mov	r2, r6
	mov	r0, r5
	mov	r1, r7
	bl	png_handle_IHDR
	b	.L313
.L286:
	ldr	r1, .L338+12
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L317
	mov	r0, r4
	ldr	r1, .L338
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L318
	mov	r0, r4
	ldr	r1, .L338+16
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L319
	mov	r0, r4
	ldr	r1, .L338+20
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L320
	mov	r0, r4
	ldr	r1, .L338+24
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L321
	mov	r0, r4
	ldr	r1, .L338+28
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L322
	mov	r0, r4
	ldr	r1, .L338+32
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L323
	mov	r0, r4
	ldr	r1, .L338+36
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L324
	mov	r0, r4
	ldr	r1, .L338+40
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L325
	mov	r0, r4
	ldr	r1, .L338+44
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L326
	mov	r0, r4
	ldr	r1, .L338+48
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L327
	mov	r0, r4
	ldr	r1, .L338+52
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L328
	mov	r0, r4
	ldr	r1, .L338+56
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L329
	mov	r0, r4
	ldr	r1, .L338+60
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L330
	mov	r0, r4
	ldr	r1, .L338+64
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L331
	mov	r0, r4
	ldr	r1, .L338+68
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L332
	mov	r0, r4
	ldr	r1, .L338+72
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L333
	ldr	r1, .L338+76
	mov	r2, #4
	mov	r0, r4
	bl	memcmp
	cmp	r0, #0
	mov	r2, r6
	mov	r0, r5
	mov	r1, r7
	bne	.L309
	bl	png_handle_zTXt
	b	.L313
.L316:
	mov	r2, r6
	mov	r0, r5
	mov	r1, r7
	bl	png_handle_IEND
	b	.L313
.L288:
	mov	r0, r4
	ldr	r1, .L338
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	bne	.L313
	ldr	r3, [r5, #132]
	tst	r3, #1
	beq	.L334
	ldrb	ip, [r5, #322]	@ zero_extendqisi2
	cmp	ip, #3
	beq	.L335
.L310:
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L317:
	mov	r2, r6
	mov	r0, r5
	mov	r1, r7
	bl	png_handle_PLTE
	b	.L313
.L319:
	mov	r2, r6
	mov	r0, r5
	mov	r1, r7
	bl	png_handle_bKGD
	b	.L313
.L320:
	mov	r2, r6
	mov	r0, r5
	mov	r1, r7
	bl	png_handle_cHRM
	b	.L313
.L321:
	mov	r2, r6
	mov	r0, r5
	mov	r1, r7
	bl	png_handle_gAMA
	b	.L313
.L322:
	mov	r2, r6
	mov	r0, r5
	mov	r1, r7
	bl	png_handle_hIST
	b	.L313
.L323:
	mov	r2, r6
	mov	r0, r5
	mov	r1, r7
	bl	png_handle_oFFs
	b	.L313
.L326:
	mov	r2, r6
	mov	r0, r5
	mov	r1, r7
	bl	png_handle_pHYs
	b	.L313
.L324:
	mov	r2, r6
	mov	r0, r5
	mov	r1, r7
	bl	png_handle_pCAL
	b	.L313
.L318:
	ldr	r3, [r5, #132]
	tst	r3, #1
	beq	.L336
	ldrb	lr, [r5, #322]	@ zero_extendqisi2
	cmp	lr, #3
	beq	.L337
.L293:
	orr	r3, r3, #4
	str	r3, [r5, #132]
	str	r6, [r5, #296]
	b	.L310
.L325:
	mov	r2, r6
	mov	r0, r5
	mov	r1, r7
	bl	png_handle_sCAL
	b	.L313
.L314:
	rsb	r6, r4, #8
	add	r1, r4, #32
	add	r1, r7, r1
	mov	r2, r6
	bl	png_read_data
	add	r8, r7, #32
	mov	r3, #8
	strb	r3, [r5, #328]
	mov	r0, r8
	mov	r1, r4
	mov	r2, r6
	bl	png_sig_cmp
	cmp	r0, #0
	beq	.L280
	cmp	r4, #3
	bhi	.L281
	mov	r0, r8
	sub	r2, r6, #4
	mov	r1, r4
	bl	png_sig_cmp
	cmp	r0, #0
	beq	.L281
	mov	r0, r5
	ldr	r1, .L338+80
	bl	png_error
	b	.L280
.L337:
	tst	r3, #2
	bne	.L293
	mov	r0, r5
	ldr	r1, .L338+84
	bl	png_error
	ldr	r3, [r5, #132]
	b	.L293
.L335:
	tst	r3, #2
	bne	.L310
	mov	r0, r5
	ldr	r1, .L338+84
	bl	png_error
	b	.L310
.L329:
	mov	r2, r6
	mov	r0, r5
	mov	r1, r7
	bl	png_handle_iCCP
	b	.L313
.L281:
	mov	r0, r5
	ldr	r1, .L338+88
	bl	png_error
.L280:
	cmp	r4, #2
	ldrls	r3, [r5, #132]
	orrls	r3, r3, #4096
	strls	r3, [r5, #132]
	b	.L279
.L327:
	mov	r2, r6
	mov	r0, r5
	mov	r1, r7
	bl	png_handle_sBIT
	b	.L313
.L328:
	mov	r2, r6
	mov	r0, r5
	mov	r1, r7
	bl	png_handle_sRGB
	b	.L313
.L336:
	mov	r0, r5
	ldr	r1, .L338+92
	bl	png_error
	ldr	r3, [r5, #132]
	orr	r3, r3, #4
	str	r3, [r5, #132]
	str	r6, [r5, #296]
	b	.L310
.L334:
	mov	r0, r5
	ldr	r1, .L338+92
	bl	png_error
	b	.L310
.L330:
	mov	r2, r6
	mov	r0, r5
	mov	r1, r7
	bl	png_handle_sPLT
	b	.L313
.L333:
	mov	r2, r6
	mov	r0, r5
	mov	r1, r7
	bl	png_handle_tRNS
	b	.L313
.L331:
	mov	r2, r6
	mov	r0, r5
	mov	r1, r7
	bl	png_handle_tEXt
	b	.L313
.L332:
	mov	r2, r6
	mov	r0, r5
	mov	r1, r7
	bl	png_handle_tIME
	b	.L313
.L309:
	bl	png_handle_unknown
	b	.L313
.L339:
	.align	2
.L338:
	.word	png_IDAT
	.word	png_IHDR
	.word	png_IEND
	.word	png_PLTE
	.word	png_bKGD
	.word	png_cHRM
	.word	png_gAMA
	.word	png_hIST
	.word	png_oFFs
	.word	png_pCAL
	.word	png_sCAL
	.word	png_pHYs
	.word	png_sBIT
	.word	png_sRGB
	.word	png_iCCP
	.word	png_sPLT
	.word	png_tEXt
	.word	png_tIME
	.word	png_tRNS
	.word	png_zTXt
	.word	.LC6
	.word	.LC9
	.word	.LC7
	.word	.LC8
	.size	png_read_info, .-png_read_info
	.section	.text.png_read_init_3,"ax",%progbits
	.align	2
	.global	png_read_init_3
	.hidden	png_read_init_3
	.type	png_read_init_3, %function
png_read_init_3:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 96
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	ldr	r4, [r0, #0]
	cmp	r4, #0
	sub	sp, sp, #100
	mov	r6, r0
	mov	r7, r2
	beq	.L351
	ldr	ip, .L355
	mov	r3, #0
	b	.L344
.L353:
	add	r3, r3, #1
.L344:
	ldrb	r2, [r3, ip]	@ zero_extendqisi2
	ldrb	r0, [r1, r3]	@ zero_extendqisi2
	cmp	r2, r0
	bne	.L352
	cmp	r2, #0
	bne	.L353
.L343:
	add	r5, sp, #4
	mov	r0, r5
	mov	r1, r4
	mov	r2, #92
	bl	memcpy
	cmp	r7, #680
	bcc	.L354
.L345:
	mov	r2, #680
	mov	r1, #0
	mov	r0, r4
	bl	memset
	mov	r2, #92
	mov	r1, r5
	mov	r0, r4
	bl	memcpy
	mov	r1, #999424
	add	r3, r1, #576
	mov	r0, #8192
	mov	r1, r0
	str	r3, [r4, #644]
	str	r3, [r4, #640]
	str	r0, [r4, #204]
	mov	r0, r4
	bl	png_malloc
	ldr	ip, .L355+4
	ldr	r2, .L355+8
	str	r0, [r4, #200]
	str	r2, [r4, #180]
	ldr	r1, .L355+12
	str	ip, [r4, #176]
	str	r4, [r4, #184]
	add	r0, r4, #144
	mov	r2, #56
	bl	inflateInit_
	add	r1, r0, #6
	cmp	r1, #6
	ldrls	pc, [pc, r1, asl #2]
	b	.L346
.L350:
	.word	.L347
	.word	.L346
	.word	.L348
	.word	.L346
	.word	.L348
	.word	.L346
	.word	.L349
.L346:
	mov	r0, r4
	ldr	r1, .L355+16
	bl	png_error
.L349:
	add	r2, r4, #200
	ldmia	r2, {r2, lr}	@ phole ldm
	mov	r1, #0
	str	r2, [r4, #156]
	str	lr, [r4, #160]
	mov	r0, r4
	mov	r2, r1
	bl	png_set_read_fn
.L351:
	add	sp, sp, #100
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L348:
	mov	r0, r4
	ldr	r1, .L355+20
	bl	png_error
	b	.L349
.L347:
	mov	r0, r4
	ldr	r1, .L355+24
	bl	png_error
	b	.L349
.L354:
	mov	r0, r4
	bl	png_destroy_struct
	mov	r0, #1
	bl	png_create_struct
	mov	r4, r0
	str	r0, [r6, #0]
	b	.L345
.L352:
	mov	r3, #0
	str	r3, [r4, #96]
	mov	r0, r4
	ldr	r1, .L355+28
	bl	png_warning
	b	.L343
.L356:
	.align	2
.L355:
	.word	png_libpng_ver
	.word	png_zalloc
	.word	png_zfree
	.word	.LC11
	.word	.LC14
	.word	.LC12
	.word	.LC13
	.word	.LC10
	.size	png_read_init_3, .-png_read_init_3
	.section	.text.png_read_init_2,"ax",%progbits
	.align	2
	.global	png_read_init_2
	.hidden	png_read_init_2
	.type	png_read_init_2, %function
png_read_init_2:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 88
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	subs	ip, r0, #0
	sub	sp, sp, #88
	str	r0, [sp, #4]
	mov	r5, r1
	mov	r4, r2
	beq	.L362
	cmp	r3, #288
	movcs	r8, #0
	movcc	r8, #1
	cmp	r2, #680
	movcs	r6, #0
	movcc	r6, #1
	orrs	r3, r8, r6
	beq	.L359
	mov	r3, #0
	cmp	r1, #0
	str	r3, [ip, #96]
	add	r7, sp, #8
	beq	.L360
	mov	r1, #80
	mov	r0, r7
	ldr	r2, .L365
	mov	r3, r5
	bl	snprintf
	ldr	r0, [sp, #4]
	mov	r1, r7
	bl	png_warning
.L360:
	mov	r1, #80
	ldr	r2, .L365+4
	ldr	r3, .L365+8
	mov	r0, r7
	bl	snprintf
	mov	r1, r7
	ldr	r0, [sp, #4]
	bl	png_warning
	cmp	r6, #0
	beq	.L361
	ldr	r1, [sp, #4]
	mov	r0, #0
	str	r0, [r1, #92]
	ldr	r2, [sp, #4]
	str	r0, [r2, #136]
	ldr	r0, [sp, #4]
	ldr	r1, .L365+12
	bl	png_error
.L361:
	cmp	r8, #0
	beq	.L359
	ldr	r3, [sp, #4]
	mov	lr, #0
	str	lr, [r3, #92]
	ldr	ip, [sp, #4]
	str	lr, [ip, #136]
	ldr	r0, [sp, #4]
	ldr	r1, .L365+16
	bl	png_error
.L359:
	mov	r1, r5
	mov	r2, r4
	add	r0, sp, #4
	bl	png_read_init_3
.L362:
	add	sp, sp, #88
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L366:
	.align	2
.L365:
	.word	.LC15
	.word	.LC16
	.word	png_libpng_ver
	.word	.LC17
	.word	.LC18
	.size	png_read_init_2, .-png_read_init_2
	.section	.text.png_create_read_struct_2,"ax",%progbits
	.align	2
	.global	png_create_read_struct_2
	.hidden	png_create_read_struct_2
	.type	png_create_read_struct_2, %function
png_create_read_struct_2:
	@ Function supports interworking.
	@ args = 12, pretend = 0, frame = 104
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	sub	sp, sp, #104
	stmia	sp, {r0, r1, r2}	@ phole stm
	mov	r0, #1
	ldr	r1, [sp, #116]
	ldr	r2, [sp, #112]
	str	r3, [sp, #12]
	bl	png_create_struct_2
	str	r0, [sp, #100]
	ldr	r0, [sp, #100]
	cmp	r0, #0
	beq	.L369
	ldr	r0, [sp, #100]
	mov	r1, #999424
	add	r3, r1, #576
	str	r3, [r0, #640]
	ldr	r2, [sp, #100]
	str	r3, [r2, #644]
	ldr	r0, [sp, #100]
	bl	setjmp
	subs	r4, r0, #0
	ldr	r0, [sp, #100]
	bne	.L386
	add	r1, sp, #112
	ldmia	r1, {r1, r2, r3}	@ phole ldm
	bl	png_set_mem_fn
	ldr	r1, [sp, #4]
	ldr	r0, [sp, #100]
	add	r2, sp, #8
	ldmia	r2, {r2, r3}	@ phole ldm
	bl	png_set_error_fn
	ldr	r1, [sp, #0]
	cmp	r1, #0
	beq	.L371
	ldr	r0, .L392
	b	.L374
.L387:
	add	r4, r4, #1
.L374:
	ldr	r3, [sp, #0]
	ldrb	r2, [r4, r0]	@ zero_extendqisi2
	ldrb	lr, [r3, r4]	@ zero_extendqisi2
	cmp	r2, lr
	ldrne	lr, [sp, #100]
	ldrne	r1, [lr, #136]
	orrne	r1, r1, #131072
	strne	r1, [lr, #136]
	movne	lr, r2
	cmp	lr, #0
	bne	.L387
	ldr	lr, [sp, #100]
	ldr	r4, [lr, #136]
	tst	r4, #131072
	beq	.L375
.L390:
	ldr	r3, [sp, #0]
	cmp	r3, #0
	beq	.L376
	ldr	r2, .L392
	ldrb	r0, [r3, #0]	@ zero_extendqisi2
	ldrb	r3, [r2, #0]	@ zero_extendqisi2
	cmp	r3, r0
	beq	.L388
.L377:
	add	r4, sp, #20
	mov	r1, #80
	ldr	r3, [sp, #0]
	mov	r0, r4
	ldr	r2, .L392+4
	bl	snprintf
	ldr	r0, [sp, #100]
	mov	r1, r4
	bl	png_warning
.L376:
	add	r4, sp, #20
	ldr	r2, .L392+8
	mov	r1, #80
	ldr	r3, .L392
	mov	r0, r4
	bl	snprintf
	ldr	r0, [sp, #100]
	mov	r1, r4
	bl	png_warning
	ldr	r2, [sp, #100]
	mov	r0, #0
	str	r0, [r2, #136]
	ldr	r0, [sp, #100]
	ldr	r1, .L392+12
	bl	png_error
.L375:
	ldr	r0, [sp, #100]
	mov	r4, #8192
	str	r4, [r0, #204]
	ldr	r4, [sp, #100]
	ldr	r0, [sp, #100]
	ldr	lr, [sp, #100]
	ldr	r1, [lr, #204]
	bl	png_malloc
	str	r0, [r4, #200]
	ldr	ip, [sp, #100]
	ldr	r1, .L392+16
	str	r1, [ip, #176]
	ldr	r3, [sp, #100]
	ldr	r2, .L392+20
	str	r2, [r3, #180]
	ldr	r0, [sp, #100]
	ldr	ip, [sp, #100]
	str	ip, [r0, #184]
	ldr	r1, [sp, #100]
	mov	r2, #56
	add	r0, r1, #144
	ldr	r1, .L392+24
	bl	inflateInit_
	add	r3, r0, #6
	cmp	r3, #6
	ldrls	pc, [pc, r3, asl #2]
	b	.L379
.L383:
	.word	.L380
	.word	.L379
	.word	.L381
	.word	.L379
	.word	.L381
	.word	.L379
	.word	.L382
.L379:
	ldr	r0, [sp, #100]
	ldr	r1, .L392+28
	bl	png_error
.L382:
	ldr	lr, [sp, #100]
	ldr	r0, [sp, #100]
	ldr	ip, [r0, #200]
	str	ip, [lr, #156]
	ldr	r3, [sp, #100]
	ldr	r1, [sp, #100]
	ldr	r2, [r1, #204]
	mov	r1, #0
	str	r2, [r3, #160]
	ldr	r0, [sp, #100]
	mov	r2, r1
	bl	png_set_read_fn
	ldr	r0, [sp, #100]
	bl	setjmp
	cmp	r0, #0
	ldreq	r0, [sp, #100]
	bne	.L389
.L369:
	add	sp, sp, #104
	ldmfd	sp!, {r4, lr}
	bx	lr
.L381:
	ldr	r0, [sp, #100]
	ldr	r1, .L392+32
	bl	png_error
	b	.L382
.L380:
	ldr	r0, [sp, #100]
	ldr	r1, .L392+36
	bl	png_error
	b	.L382
.L371:
	ldr	r1, [sp, #100]
	ldr	ip, [r1, #136]
	orr	r0, ip, #131072
	str	r0, [r1, #136]
	ldr	lr, [sp, #100]
	ldr	r4, [lr, #136]
	tst	r4, #131072
	beq	.L375
	b	.L390
.L388:
	cmp	r3, #49
	beq	.L391
	cmp	r3, #48
	bne	.L375
	ldr	r4, [sp, #0]
	ldrb	ip, [r4, #2]	@ zero_extendqisi2
	cmp	ip, #56
	bhi	.L375
	b	.L377
.L386:
	ldr	r4, [sp, #100]
	ldr	r1, [r4, #200]
	bl	png_free
	ldr	ip, [sp, #100]
	mov	r4, #0
	str	r4, [ip, #200]
	ldr	r0, [sp, #100]
	ldr	r1, [sp, #120]
	ldr	r2, [sp, #112]
	bl	png_destroy_struct_2
	mov	r0, r4
	b	.L369
.L391:
	ldr	r1, [sp, #0]
	ldrb	r3, [r2, #2]	@ zero_extendqisi2
	ldrb	r2, [r1, #2]	@ zero_extendqisi2
	cmp	r2, r3
	bne	.L377
	b	.L375
.L389:
	bl	abort
.L393:
	.align	2
.L392:
	.word	png_libpng_ver
	.word	.LC15
	.word	.LC16
	.word	.LC19
	.word	png_zalloc
	.word	png_zfree
	.word	.LC11
	.word	.LC14
	.word	.LC20
	.word	.LC21
	.size	png_create_read_struct_2, .-png_create_read_struct_2
	.section	.text.png_create_read_struct,"ax",%progbits
	.align	2
	.global	png_create_read_struct
	.hidden	png_create_read_struct
	.type	png_create_read_struct, %function
png_create_read_struct:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
	mov	ip, #0
	sub	sp, sp, #20
	str	ip, [sp, #8]
	str	ip, [sp, #0]
	str	ip, [sp, #4]
	bl	png_create_read_struct_2
	add	sp, sp, #20
	ldr	lr, [sp], #4
	bx	lr
	.size	png_create_read_struct, .-png_create_read_struct
	.section	.text.png_read_init,"ax",%progbits
	.align	2
	.global	png_read_init
	.hidden	png_read_init
	.type	png_read_init, %function
png_read_init:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 96
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	subs	r4, r0, #0
	sub	sp, sp, #100
	beq	.L406
	mov	r7, #0
	add	r6, sp, #4
	ldr	r2, .L409
	ldr	r3, .L409+4
	mov	r1, #80
	ldr	r5, .L409+8
	str	r7, [r4, #96]
	mov	r0, r6
	bl	snprintf
	mov	r0, r4
	mov	r1, r6
	bl	png_warning
	ldr	r2, .L409+12
	mov	r3, r5
	mov	r1, #80
	mov	r0, r6
	bl	snprintf
	mov	r0, r4
	mov	r1, r6
	bl	png_warning
	mov	r0, r4
	ldr	r1, .L409+16
	str	r7, [r4, #92]
	str	r7, [r4, #136]
	bl	png_error
	ldr	r1, .L409+20
	str	r7, [r4, #92]
	str	r7, [r4, #136]
	mov	r0, r4
	bl	png_error
	ldr	r3, .L409+4
	ldrb	r2, [r7, r5]	@ zero_extendqisi2
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	cmp	r2, r1
	beq	.L398
.L407:
	mov	r3, #0
	str	r3, [r4, #96]
	mov	r0, r4
	ldr	r1, .L409+24
	bl	png_warning
.L399:
	mov	r1, r4
	mov	r2, #92
	mov	r0, r6
	bl	memcpy
	mov	r0, r4
	bl	png_destroy_struct
	mov	r0, #1
	bl	png_create_struct
	mov	r2, #680
	mov	r4, r0
	mov	r1, #0
	bl	memset
	mov	r2, #92
	mov	r1, r6
	mov	r0, r4
	bl	memcpy
	mov	r1, #999424
	add	r3, r1, #576
	mov	r0, #8192
	mov	r1, r0
	str	r3, [r4, #644]
	str	r3, [r4, #640]
	str	r0, [r4, #204]
	mov	r0, r4
	bl	png_malloc
	ldr	ip, .L409+28
	ldr	r2, .L409+32
	str	r0, [r4, #200]
	str	r2, [r4, #180]
	str	ip, [r4, #176]
	str	r4, [r4, #184]
	add	r0, r4, #144
	ldr	r1, .L409+36
	mov	r2, #56
	bl	inflateInit_
	add	r0, r0, #6
	cmp	r0, #6
	ldrls	pc, [pc, r0, asl #2]
	b	.L401
.L405:
	.word	.L402
	.word	.L401
	.word	.L403
	.word	.L401
	.word	.L403
	.word	.L401
	.word	.L404
.L408:
	add	r7, r7, #1
	ldrb	r2, [r7, r5]	@ zero_extendqisi2
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	cmp	r2, r1
	bne	.L407
.L398:
	cmp	r2, #0
	add	r3, r3, #1
	bne	.L408
	b	.L399
.L401:
	mov	r0, r4
	ldr	r1, .L409+40
	bl	png_error
.L404:
	add	r2, r4, #200
	ldmia	r2, {r2, lr}	@ phole ldm
	mov	r1, #0
	str	r2, [r4, #156]
	str	lr, [r4, #160]
	mov	r0, r4
	mov	r2, r1
	bl	png_set_read_fn
.L406:
	add	sp, sp, #100
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L403:
	mov	r0, r4
	ldr	r1, .L409+44
	bl	png_error
	b	.L404
.L402:
	mov	r0, r4
	ldr	r1, .L409+48
	bl	png_error
	b	.L404
.L410:
	.align	2
.L409:
	.word	.LC15
	.word	.LC22
	.word	png_libpng_ver
	.word	.LC16
	.word	.LC17
	.word	.LC18
	.word	.LC10
	.word	png_zalloc
	.word	png_zfree
	.word	.LC11
	.word	.LC14
	.word	.LC12
	.word	.LC13
	.size	png_read_init, .-png_read_init
	.section	.text.png_read_png,"ax",%progbits
	.align	2
	.global	png_read_png
	.hidden	png_read_png
	.type	png_read_png, %function
png_read_png:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	subs	r4, r0, #0
	sub	sp, sp, #20
	mov	fp, r1
	mov	r5, r2
	beq	.L435
	tst	r2, #1024
	bne	.L451
	mov	r0, r4
	mov	r1, fp
	bl	png_read_info
	ldr	r3, [fp, #4]
	cmn	r3, #-1073741823
	bhi	.L452
.L414:
	tst	r5, #1
	bne	.L453
.L415:
	tst	r5, #2
	bne	.L454
.L416:
	tst	r5, #8
	bne	.L455
.L417:
	tst	r5, #16
	bne	.L456
.L418:
	tst	r5, #32
	bne	.L457
.L420:
	tst	r5, #64
	bne	.L458
.L421:
	tst	r5, #128
	bne	.L459
.L422:
	tst	r5, #256
	bne	.L460
.L423:
	tst	r5, #512
	bne	.L461
.L424:
	ldr	r2, [r4, #136]
	tst	r2, #64
	mov	r0, r4
	beq	.L462
	ldr	r1, .L465
	bl	png_warning
.L426:
	mov	r0, r4
	mov	r1, fp
	bl	png_read_transform_info
	mov	r0, r4
	mov	r1, fp
	mov	r2, #64
	mov	r3, #0
	bl	png_free_data
	ldr	r6, [fp, #248]
	cmp	r6, #0
	beq	.L463
.L427:
	mov	r0, r4
	bl	png_set_interlace_handling
	ldr	r7, [r4, #232]
	cmp	r0, #0
	str	r0, [sp, #4]
	movgt	r9, #0
	str	r7, [r4, #236]
	ble	.L430
.L431:
	cmp	r7, #0
	beq	.L434
	mov	r8, #0
	mov	r0, r4
	ldr	r1, [r6, r8]
	mov	r2, r8
	mov	r5, #1
	sub	sl, r7, #1
	bl	png_read_row
	cmp	r7, r5
	and	sl, sl, #3
	bls	.L434
	cmp	sl, #0
	beq	.L432
	cmp	sl, #1
	beq	.L448
	cmp	sl, #2
	beq	.L449
	mov	r2, r8
	mov	r0, r4
	ldr	r1, [r6, #4]
	bl	png_read_row
	mov	r5, #2
.L449:
	ldr	r1, [r6, r5, asl #2]
	mov	r0, r4
	mov	r2, #0
	bl	png_read_row
	add	r5, r5, #1
.L448:
	ldr	r1, [r6, r5, asl #2]
	mov	r0, r4
	add	r5, r5, #1
	mov	r2, #0
	bl	png_read_row
	cmp	r7, r5
	bls	.L434
.L432:
	ldr	r1, [r6, r5, asl #2]
	add	r8, r5, #1
	mov	r0, r4
	mov	r2, #0
	bl	png_read_row
	ldr	r1, [r6, r8, asl #2]
	mov	r0, r4
	mov	r2, #0
	add	r8, r8, #1
	bl	png_read_row
	ldr	r1, [r6, r8, asl #2]
	mov	r0, r4
	mov	r2, #0
	bl	png_read_row
	add	r2, r5, #3
	ldr	r1, [r6, r2, asl #2]
	add	r5, r5, #4
	mov	r0, r4
	mov	r2, #0
	bl	png_read_row
	cmp	r7, r5
	bhi	.L432
.L434:
	ldr	lr, [sp, #4]
	add	r9, r9, #1
	cmp	lr, r9
	bgt	.L431
.L430:
	ldr	ip, [fp, #8]
	orr	r1, ip, #32768
	str	r1, [fp, #8]
	mov	r0, r4
	mov	r1, fp
	bl	png_read_end
.L435:
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L456:
	ldrb	r0, [r4, #323]	@ zero_extendqisi2
	cmp	r0, #7
	bls	.L419
	ldrb	r1, [r4, #322]	@ zero_extendqisi2
	cmp	r1, #3
	beq	.L419
	mov	r0, r4
	mov	r1, fp
	mov	r2, #16
	bl	png_get_valid
	cmp	r0, #0
	beq	.L418
.L419:
	mov	r0, r4
	bl	png_set_expand
	tst	r5, #32
	beq	.L420
.L457:
	mov	r0, r4
	bl	png_set_invert_mono
	tst	r5, #64
	beq	.L421
.L458:
	mov	r0, r4
	mov	r1, fp
	mov	r2, #2
	bl	png_get_valid
	cmp	r0, #0
	beq	.L421
	mov	r1, fp
	mov	r0, r4
	add	r2, sp, #12
	bl	png_get_sBIT
	mov	r0, r4
	ldr	r1, [sp, #12]
	bl	png_set_shift
	b	.L421
.L462:
	bl	png_read_start_row
	b	.L426
.L461:
	mov	r0, r4
	bl	png_set_swap
	b	.L424
.L451:
	bl	png_set_invert_alpha
	mov	r0, r4
	mov	r1, fp
	bl	png_read_info
	ldr	r3, [fp, #4]
	cmn	r3, #-1073741823
	bls	.L414
.L452:
	mov	r0, r4
	ldr	r1, .L465+4
	bl	png_error
	tst	r5, #1
	beq	.L415
.L453:
	mov	r0, r4
	bl	png_set_strip_16
	tst	r5, #2
	beq	.L416
.L454:
	mov	r0, r4
	bl	png_set_strip_alpha
	tst	r5, #8
	beq	.L417
.L455:
	mov	r0, r4
	bl	png_set_packswap
	b	.L417
.L460:
	mov	r0, r4
	bl	png_set_swap_alpha
	b	.L423
.L459:
	mov	r0, r4
	bl	png_set_bgr
	b	.L422
.L463:
	ldr	r6, [fp, #4]
	mov	r0, r4
	mov	r1, r6, asl #2
	bl	png_malloc
	ldr	ip, [fp, #184]
	ldr	r3, [fp, #4]
	orr	r7, ip, #64
	cmp	r3, #0
	mov	r6, r0
	str	r7, [fp, #184]
	str	r0, [fp, #248]
	ble	.L427
	mov	r5, #0
	b	.L429
.L464:
	ldr	r6, [fp, #248]
.L429:
	mov	r1, fp
	mov	r0, r4
	bl	png_get_rowbytes
	mov	r1, r0
	mov	r0, r4
	bl	png_malloc
	str	r0, [r6, r5, asl #2]
	ldr	r0, [fp, #4]
	add	r5, r5, #1
	cmp	r0, r5
	bgt	.L464
	ldr	r6, [fp, #248]
	b	.L427
.L466:
	.align	2
.L465:
	.word	.LC5
	.word	.LC23
	.size	png_read_png, .-png_read_png
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"Too many IDAT's found\000"
	.space	2
.LC1:
	.ascii	"Invalid attempt to read row data\000"
	.space	3
.LC2:
	.ascii	"Not enough image data\000"
	.space	2
.LC3:
	.ascii	"Extra compressed data\000"
	.space	2
.LC4:
	.ascii	"Decompression error\000"
.LC5:
	.ascii	"Ignoring extra png_read_update_info() call; row buf"
	.ascii	"fer not reallocated\000"
	.space	1
.LC6:
	.ascii	"Not a PNG file\000"
	.space	1
.LC7:
	.ascii	"PNG file corrupted by ASCII conversion\000"
	.space	1
.LC8:
	.ascii	"Missing IHDR before IDAT\000"
	.space	3
.LC9:
	.ascii	"Missing PLTE before IDAT\000"
	.space	3
.LC10:
	.ascii	"Application uses deprecated png_read_init() and sho"
	.ascii	"uld be recompiled.\000"
	.space	2
.LC11:
	.ascii	"1.2.3\000"
	.space	2
.LC12:
	.ascii	"zlib memory\000"
.LC13:
	.ascii	"zlib version\000"
	.space	3
.LC14:
	.ascii	"Unknown zlib error\000"
	.space	1
.LC15:
	.ascii	"Application was compiled with png.h from libpng-%.2"
	.ascii	"0s\000"
	.space	2
.LC16:
	.ascii	"Application  is  running with png.c from libpng-%.2"
	.ascii	"0s\000"
	.space	2
.LC17:
	.ascii	"The png struct allocated by the application for rea"
	.ascii	"ding is too small.\000"
	.space	2
.LC18:
	.ascii	"The info struct allocated by application for readin"
	.ascii	"g is too small.\000"
	.space	1
.LC19:
	.ascii	"Incompatible libpng version in application and libr"
	.ascii	"ary\000"
	.space	1
.LC20:
	.ascii	"zlib memory error\000"
	.space	2
.LC21:
	.ascii	"zlib version error\000"
	.space	1
.LC22:
	.ascii	"1.0.6 or earlier\000"
	.space	3
.LC23:
	.ascii	"Image is too high to process with png_read_png()\000"
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
