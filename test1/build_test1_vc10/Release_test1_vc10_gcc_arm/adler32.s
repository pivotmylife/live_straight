	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"adler32.c"
	.global	__aeabi_uidivmod
	.section	.text.adler32,"ax",%progbits
	.align	2
	.global	adler32
	.hidden	adler32
	.type	adler32, %function
adler32:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	cmp	r2, #1
	mov	r4, r0, asl #16
	mov	r6, r1
	mov	r4, r4, lsr #16
	mov	r5, r0, lsr #16
	beq	.L55
	cmp	r1, #0
	moveq	r0, #1
	beq	.L5
	cmp	r2, #15
	bhi	.L56
	cmp	r2, #0
	beq	.L10
	ldrb	r0, [r1, #0]	@ zero_extendqisi2
	mov	r3, #1
	add	r4, r4, r0
	sub	r1, r2, #1
	cmp	r3, r2
	and	r1, r1, #3
	add	r5, r5, r4
	beq	.L10
	cmp	r1, #0
	beq	.L11
	cmp	r1, #1
	beq	.L49
	cmp	r1, #2
	ldrneb	r3, [r6, #1]	@ zero_extendqisi2
	addne	r4, r4, r3
	movne	r3, #2
	ldrb	r1, [r6, r3]	@ zero_extendqisi2
	addne	r5, r5, r4
	add	r4, r4, r1
	add	r5, r5, r4
	add	r3, r3, #1
.L49:
	ldrb	r0, [r6, r3]	@ zero_extendqisi2
	add	r3, r3, #1
	add	r4, r4, r0
	cmp	r3, r2
	add	r5, r5, r4
	beq	.L10
.L11:
	add	r1, r3, #1
	ldrb	r7, [r6, r3]	@ zero_extendqisi2
	add	ip, r1, #1
	ldrb	r0, [r6, r1]	@ zero_extendqisi2
	add	r4, r4, r7
	ldrb	r1, [r6, ip]	@ zero_extendqisi2
	add	ip, r3, #3
	add	r0, r4, r0
	ldrb	ip, [r6, ip]	@ zero_extendqisi2
	add	r4, r5, r4
	add	r3, r3, #4
	add	r5, r0, r1
	add	r0, r4, r0
	cmp	r3, r2
	add	r4, r5, ip
	add	ip, r0, r5
	add	r5, ip, r4
	bne	.L11
.L10:
	mov	r3, #-2147483648
	mov	r2, #65280
	add	r1, r3, #491520
	add	r0, r2, #240
	add	ip, r1, #113
	cmp	r4, r0
	umull	r0, ip, r5, ip
	mov	r0, ip, lsr #15
	rsb	r2, r0, r0, asl #12
	subhi	r4, r4, #65280
	add	r3, r0, r2, asl #4
	subhi	r4, r4, #241
	rsb	r0, r3, r5
	orr	r0, r4, r0, asl #16
.L5:
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L56:
	mov	r8, #5504
	add	r8, r8, #47
	cmp	r2, r8
	movhi	r7, #-2147483648
	addhi	r7, r7, #491520
	addhi	r7, r7, #113
	bls	.L9
.L21:
	sub	sl, r2, #5504
	add	r9, r6, #5504
	sub	r2, sl, #48
	add	r1, r9, #48
	mov	r3, r6
.L13:
	ldrb	r9, [r3, #0]	@ zero_extendqisi2
	ldrb	r0, [r3, #1]	@ zero_extendqisi2
	add	sl, r4, r9
	ldrb	ip, [r3, #2]	@ zero_extendqisi2
	add	r9, sl, r0
	add	r4, sl, r5
	ldrb	sl, [r3, #3]	@ zero_extendqisi2
	add	r0, r4, r9
	add	ip, r9, ip
	ldrb	r9, [r3, #4]	@ zero_extendqisi2
	add	r5, ip, sl
	add	ip, r0, ip
	ldrb	sl, [r3, #5]	@ zero_extendqisi2
	add	r0, r5, r9
	add	r4, ip, r5
	ldrb	r5, [r3, #6]	@ zero_extendqisi2
	add	ip, r0, sl
	add	r0, r4, r0
	ldrb	sl, [r3, #7]	@ zero_extendqisi2
	add	r5, ip, r5
	add	r9, r0, ip
	ldrb	r4, [r3, #8]	@ zero_extendqisi2
	add	r0, r5, sl
	ldrb	ip, [r3, #9]	@ zero_extendqisi2
	add	sl, r9, r5
	add	r9, r0, r4
	ldrb	r5, [r3, #10]	@ zero_extendqisi2
	add	r0, sl, r0
	add	r4, r0, r9
	add	sl, r9, ip
	ldrb	r9, [r3, #11]	@ zero_extendqisi2
	add	r5, sl, r5
	add	ip, r4, sl
	ldrb	sl, [r3, #12]	@ zero_extendqisi2
	add	r0, r5, r9
	add	r9, ip, r5
	ldrb	ip, [r3, #13]	@ zero_extendqisi2
	add	r4, r0, sl
	add	sl, r9, r0
	ldrb	r9, [r3, #14]	@ zero_extendqisi2
	add	ip, r4, ip
	add	r5, sl, r4
	ldrb	sl, [r3, #15]	@ zero_extendqisi2
	add	r0, ip, r9
	add	r5, r5, ip
	add	r3, r3, #16
	add	r4, r0, sl
	add	r5, r5, r0
	cmp	r1, r3
	add	r5, r5, r4
	bne	.L13
	umull	r3, r1, r7, r4
	umull	r0, r3, r7, r5
	mov	r1, r1, lsr #15
	mov	r3, r3, lsr #15
	rsb	ip, r1, r1, asl #12
	rsb	r0, r3, r3, asl #12
	add	r1, r1, ip, asl #4
	add	r3, r3, r0, asl #4
	add	r6, r6, #5504
	cmp	r2, r8
	rsb	r4, r1, r4
	rsb	r5, r3, r5
	add	r6, r6, #48
	bhi	.L21
	cmp	r2, #0
	bne	.L57
	orr	r0, r4, r5, asl #16
	b	.L5
.L55:
	ldrb	r2, [r1, #0]	@ zero_extendqisi2
	mov	ip, #65280
	add	r4, r2, r4
	add	r0, ip, #240
	cmp	r4, r0
	subhi	r4, r4, #65280
	subhi	r4, r4, #241
	mov	r1, #65280
	add	r0, r4, r5
	add	r3, r1, #240
	cmp	r0, r3
	subhi	r0, r0, #65280
	subhi	r0, r0, #241
	orr	r0, r4, r0, asl #16
	b	.L5
.L57:
	cmp	r2, #15
	bls	.L15
.L9:
	mov	r1, r2
	mov	r3, r6
.L16:
	ldrb	r0, [r3, #0]	@ zero_extendqisi2
	ldrb	ip, [r3, #1]	@ zero_extendqisi2
	add	lr, r4, r0
	ldrb	r0, [r3, #2]	@ zero_extendqisi2
	add	ip, lr, ip
	add	r4, lr, r5
	ldrb	lr, [r3, #3]	@ zero_extendqisi2
	add	r0, ip, r0
	ldrb	r5, [r3, #4]	@ zero_extendqisi2
	add	r4, r4, ip
	add	lr, r0, lr
	add	r4, r4, r0
	ldrb	r0, [r3, #5]	@ zero_extendqisi2
	add	ip, lr, r5
	add	r5, r4, lr
	ldrb	lr, [r3, #6]	@ zero_extendqisi2
	add	r0, ip, r0
	add	r4, r5, ip
	ldrb	r5, [r3, #7]	@ zero_extendqisi2
	add	lr, r0, lr
	add	r4, r4, r0
	ldrb	r0, [r3, #8]	@ zero_extendqisi2
	add	ip, lr, r5
	add	r5, r4, lr
	ldrb	lr, [r3, #9]	@ zero_extendqisi2
	add	r0, ip, r0
	add	r4, r5, ip
	ldrb	r5, [r3, #10]	@ zero_extendqisi2
	add	lr, r0, lr
	add	r4, r4, r0
	ldrb	r0, [r3, #11]	@ zero_extendqisi2
	add	ip, lr, r5
	add	r5, r4, lr
	ldrb	lr, [r3, #12]	@ zero_extendqisi2
	add	r0, ip, r0
	add	r4, r5, ip
	ldrb	ip, [r3, #13]	@ zero_extendqisi2
	add	r5, r0, lr
	add	r4, r4, r0
	ldrb	r0, [r3, #14]	@ zero_extendqisi2
	add	lr, r4, r5
	add	ip, r5, ip
	ldrb	r4, [r3, #15]	@ zero_extendqisi2
	add	r5, ip, r0
	sub	r1, r1, #16
	add	ip, lr, ip
	add	r4, r5, r4
	add	lr, ip, r5
	cmp	r1, #15
	add	r5, lr, r4
	add	r3, r3, #16
	bhi	.L16
	sub	r2, r2, #16
	bic	r1, r2, #15
	add	r3, r1, #16
	ands	r2, r2, #15
	add	r6, r6, r3
	beq	.L17
.L15:
	sub	r0, r2, #1
	ldrb	ip, [r6, #0]	@ zero_extendqisi2
	add	r1, r0, #1
	add	r4, r4, ip
	cmp	r1, #1
	and	r2, r0, #3
	add	r5, r5, r4
	mov	r3, #1
	beq	.L17
	cmp	r2, #0
	beq	.L19
	cmp	r2, #1
	beq	.L51
	cmp	r2, #2
	ldrneb	r3, [r6, #1]	@ zero_extendqisi2
	addne	r4, r4, r3
	movne	r3, #2
	ldrb	lr, [r6, r3]	@ zero_extendqisi2
	addne	r5, r5, r4
	add	r4, r4, lr
	add	r5, r5, r4
	add	r3, r3, #1
.L51:
	ldrb	r2, [r6, r3]	@ zero_extendqisi2
	add	r3, r3, #1
	add	r4, r4, r2
	cmp	r1, r3
	add	r5, r5, r4
	beq	.L17
.L19:
	add	r2, r3, #1
	ldrb	lr, [r6, r3]	@ zero_extendqisi2
	ldrb	r0, [r6, r2]	@ zero_extendqisi2
	add	ip, r2, #1
	add	r4, r4, lr
	ldrb	r2, [r6, ip]	@ zero_extendqisi2
	add	ip, r3, #3
	add	lr, r4, r0
	ldrb	ip, [r6, ip]	@ zero_extendqisi2
	add	r4, r5, r4
	add	r0, r4, lr
	add	r5, lr, r2
	add	r3, r3, #4
	add	r4, r5, ip
	add	lr, r0, r5
	cmp	r1, r3
	add	r5, lr, r4
	bne	.L19
.L17:
	mov	r1, #65536
	mov	r0, r4
	sub	r1, r1, #15
	bl	__aeabi_uidivmod
	mov	r3, #65536
	mov	r4, r1
	mov	r0, r5
	sub	r1, r3, #15
	bl	__aeabi_uidivmod
	mov	r5, r1
	orr	r0, r4, r5, asl #16
	b	.L5
	.size	adler32, .-adler32
	.section	.text.adler32_combine,"ax",%progbits
	.align	2
	.global	adler32_combine
	.hidden	adler32_combine
	.type	adler32_combine, %function
adler32_combine:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, r5, r6, r7}
	mov	r5, #-2147483648
	add	r3, r5, #491520
	add	r3, r3, #113
	umull	r4, r7, r3, r2
	mov	r6, r7, lsr #15
	rsb	r4, r6, r6, asl #12
	add	ip, r6, r4, asl #4
	mov	r5, r0, asl #16
	mov	r7, r5, lsr #16
	rsb	r2, ip, r2
	mul	ip, r7, r2
	umull	r5, r3, ip, r3
	mov	r6, r0, lsr #16
	mov	r3, r3, lsr #15
	add	r4, r7, #65280
	mov	r5, r1, asl #16
	add	r7, r6, #65280
	mov	r0, r5, lsr #16
	add	r6, r7, #241
	add	r4, r4, #240
	rsb	r7, r3, r3, asl #12
	mov	r5, #65536
	add	r3, r3, r7, asl #4
	add	r0, r4, r0
	add	r6, r6, r1, lsr #16
	sub	r5, r5, #15
	rsb	ip, r3, ip
	rsb	r2, r2, r6
	cmp	r0, r5
	add	r2, r2, ip
	bls	.L59
	sub	r1, r0, #65280
	sub	r0, r1, #241
	cmp	r0, r5
	subhi	r0, r0, #65280
	subhi	r0, r0, #241
.L59:
	mov	ip, #131072
	sub	r1, ip, #30
	cmp	r2, r1
	subhi	r2, r2, #130048
	subhi	r2, r2, #992
	mov	r3, #65536
	subhi	r2, r2, #2
	sub	ip, r3, #15
	cmp	r2, ip
	subhi	r2, r2, #65280
	subhi	r2, r2, #241
	orr	r0, r0, r2, asl #16
	ldmfd	sp!, {r4, r5, r6, r7}
	bx	lr
	.size	adler32_combine, .-adler32_combine
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
