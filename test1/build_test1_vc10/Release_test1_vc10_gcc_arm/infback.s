	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"infback.c"
	.section	.text.inflateBackInit_,"ax",%progbits
	.align	2
	.global	inflateBackInit_
	.hidden	inflateBackInit_
	.type	inflateBackInit_, %function
inflateBackInit_:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r3, #0
	stmfd	sp!, {r4, r5, r6, lr}
	mov	r4, r0
	mov	r6, r1
	mov	r5, r2
	bne	.L10
.L2:
	mvn	r0, #5
.L7:
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L10:
	ldrb	r3, [r3, #0]	@ zero_extendqisi2
	ldr	r2, [sp, #16]
	cmp	r3, #49
	cmpeq	r2, #56
	bne	.L2
	cmp	r0, #0
	cmpne	r5, #0
	bne	.L11
.L3:
	mvn	r0, #1
	b	.L7
.L11:
	cmp	r1, #7
	movgt	r2, #0
	movle	r2, #1
	cmp	r1, #15
	orrgt	r2, r2, #1
	cmp	r2, #0
	bne	.L3
	ldr	ip, [r0, #32]
	cmp	ip, #0
	ldr	r1, [r0, #36]
	str	r2, [r0, #24]
	ldreq	r2, .L12
	streq	ip, [r0, #40]
	streq	r2, [r0, #32]
	moveq	ip, r2
	cmp	r1, #0
	ldreq	ip, .L12+4
	streq	ip, [r0, #36]
	ldreq	ip, [r0, #32]
	mov	r0, #9472
	add	r2, r0, #48
	mov	r1, #1
	ldr	r0, [r4, #40]
	mov	lr, pc
	bx	ip
	subs	r3, r0, #0
	mvneq	r0, #3
	beq	.L7
	mov	r2, #1
	mov	lr, r2, asl r6
	mov	r0, #0
	mov	ip, #32768
	str	r3, [r4, #28]
	str	ip, [r3, #20]
	str	lr, [r3, #40]
	str	r5, [r3, #52]
	str	r0, [r3, #44]
	str	r6, [r3, #36]
	str	r0, [r3, #48]
	b	.L7
.L13:
	.align	2
.L12:
	.word	zcalloc
	.word	zcfree
	.size	inflateBackInit_, .-inflateBackInit_
	.section	.text.inflateBackEnd,"ax",%progbits
	.align	2
	.global	inflateBackEnd
	.hidden	inflateBackEnd
	.type	inflateBackEnd, %function
inflateBackEnd:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	subs	r4, r0, #0
	bne	.L18
.L15:
	mvn	r0, #1
.L16:
	ldmfd	sp!, {r4, lr}
	bx	lr
.L18:
	ldr	r1, [r4, #28]
	cmp	r1, #0
	beq	.L15
	ldr	r3, [r4, #36]
	cmp	r3, #0
	beq	.L15
	ldr	r0, [r4, #40]
	mov	lr, pc
	bx	r3
	mov	r0, #0
	str	r0, [r4, #28]
	b	.L16
	.size	inflateBackEnd, .-inflateBackEnd
	.section	.text.inflateBack,"ax",%progbits
	.align	2
	.global	inflateBack
	.hidden	inflateBack
	.type	inflateBack, %function
inflateBack:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 88
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	subs	r8, r0, #0
	sub	sp, sp, #100
	mov	r9, r1
	mov	sl, r2
	str	r3, [sp, #24]
	bne	.L530
.L20:
	mvn	r0, #1
.L147:
	add	sp, sp, #100
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L530:
	ldr	r6, [r8, #28]
	cmp	r6, #0
	beq	.L20
	mov	r5, #0
	mov	r4, #11
	str	r5, [r8, #24]
	str	r4, [r6, #0]
	str	r5, [r6, #44]
	str	r5, [r6, #4]
	ldr	r4, [r8, #0]
	str	r4, [sp, #92]
	cmp	r4, r5
	ldrne	r4, [r8, #4]
	str	r6, [sp, #28]
	ldr	r3, [r6, #52]
	str	r3, [sp, #32]
	mov	r1, r6
	ldr	r3, [r1], #1328
	add	r0, r6, #112
	ldr	fp, [r6, #40]
	add	ip, r6, #108
	str	r1, [sp, #28]
	str	r0, [sp, #52]
	add	r1, r6, #752
	add	r0, r6, #84
	add	r2, r6, #88
	mov	r5, #0
	str	ip, [sp, #44]
	str	r0, [sp, #48]
	str	r1, [sp, #40]
	str	r2, [sp, #56]
	mov	r7, r5
.L409:
	sub	r2, r3, #11
	cmp	r2, #16
	ldrls	pc, [pc, r2, asl #2]
	b	.L24
.L31:
	.word	.L25
	.word	.L24
	.word	.L26
	.word	.L24
	.word	.L27
	.word	.L24
	.word	.L24
	.word	.L28
	.word	.L24
	.word	.L24
	.word	.L24
	.word	.L24
	.word	.L24
	.word	.L24
	.word	.L24
	.word	.L29
	.word	.L30
.L30:
	ldr	r3, [sp, #92]
	mvn	r0, #2
.L32:
	stmia	r8, {r3, r4}	@ phole stm
	b	.L147
.L29:
	ldr	r2, [r6, #40]
	cmp	r2, fp
	bhi	.L145
.L516:
	ldr	r3, [sp, #92]
	mov	r0, #1
	b	.L32
.L28:
	mov	r1, #256
	add	r0, r1, #1
	cmp	r4, #5
	cmphi	fp, r0
	bhi	.L97
	ldr	r2, [r6, #84]
	mov	r1, #1
	mov	r0, r1, asl r2
	sub	r3, r0, #1
	ldr	r0, [r6, #76]
	and	r1, r7, r3
	add	r2, r0, r1, asl #2
	ldrb	r3, [r2, #1]	@ zero_extendqisi2
	cmp	r5, r3
	ldrb	r1, [r0, r1, asl #2]	@ zero_extendqisi2
	ldrh	r2, [r2, #2]
	bcc	.L162
	b	.L98
.L101:
	ldr	r0, [sp, #92]
	ldrb	r2, [r0], #1	@ zero_extendqisi2
	str	r0, [sp, #92]
	ldr	r1, [r6, #84]
	mov	r3, #1
	mov	r0, r3, asl r1
	add	r7, r7, r2, asl r5
	sub	r3, r0, #1
	ldr	r0, [r6, #76]
	and	r1, r7, r3
	add	r2, r0, r1, asl #2
	ldrb	r3, [r2, #1]	@ zero_extendqisi2
	add	r5, r5, #8
	cmp	r3, r5
	sub	r4, r4, #1
	ldrb	r1, [r0, r1, asl #2]	@ zero_extendqisi2
	ldrh	r2, [r2, #2]
	bls	.L98
.L162:
	cmp	r4, #0
	bne	.L101
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	subs	r4, r0, #0
	bne	.L101
.L515:
	mov	r3, r4
	mvn	r0, #4
	b	.L32
.L27:
	cmp	r5, #13
	bhi	.L34
	rsb	r3, r5, #13
	mov	r0, r3, lsr #3
	cmp	r4, #0
	and	r3, r0, #3
	beq	.L531
.L352:
	ldr	r2, [sp, #92]
	ldrb	r0, [r2], #1	@ zero_extendqisi2
	add	r7, r7, r0, asl r5
	add	r5, r5, #8
	cmp	r5, #13
	str	r2, [sp, #92]
	sub	r4, r4, #1
	bhi	.L34
	cmp	r3, #0
	beq	.L511
	cmp	r3, #1
	beq	.L406
	cmp	r3, #2
	beq	.L407
	cmp	r4, #0
	beq	.L532
.L354:
	ldr	r3, [sp, #92]
	ldrb	r1, [r3], #1	@ zero_extendqisi2
	str	r3, [sp, #92]
	add	r7, r7, r1, asl r5
	sub	r4, r4, #1
	add	r5, r5, #8
.L407:
	cmp	r4, #0
	beq	.L533
.L357:
	ldr	r0, [sp, #92]
	ldrb	r2, [r0], #1	@ zero_extendqisi2
	str	r0, [sp, #92]
	add	r7, r7, r2, asl r5
	sub	r4, r4, #1
	add	r5, r5, #8
.L406:
	cmp	r4, #0
	beq	.L534
.L360:
	ldr	r1, [sp, #92]
	ldrb	r3, [r1], #1	@ zero_extendqisi2
	add	r7, r7, r3, asl r5
	add	r5, r5, #8
	cmp	r5, #13
	str	r1, [sp, #92]
	sub	r4, r4, #1
	bhi	.L34
.L511:
	str	fp, [sp, #36]
	mov	r0, r4
	b	.L168
.L52:
	ldr	r4, [sp, #92]
	ldrb	fp, [r4], #1	@ zero_extendqisi2
	subs	r0, r0, #1
	add	r7, r7, fp, asl r5
	str	r4, [sp, #92]
	add	fp, r5, #8
	beq	.L535
.L363:
	ldr	r2, [sp, #92]
	ldrb	r4, [r2], #1	@ zero_extendqisi2
	subs	r0, r0, #1
	add	r7, r7, r4, asl fp
	add	r5, fp, #24
	str	r2, [sp, #92]
	add	r4, fp, #8
	beq	.L536
.L365:
	ldr	r1, [sp, #92]
	ldrb	r3, [r1], #1	@ zero_extendqisi2
	subs	r0, r0, #1
	add	r7, r7, r3, asl r4
	str	r1, [sp, #92]
	add	fp, fp, #16
	beq	.L537
.L367:
	ldr	r2, [sp, #92]
	ldrb	r4, [r2], #1	@ zero_extendqisi2
	cmp	r5, #13
	add	r7, r7, r4, asl fp
	str	r2, [sp, #92]
	sub	r0, r0, #1
	bhi	.L538
.L168:
	cmp	r0, #0
	bne	.L52
	add	r1, sp, #92
	mov	r0, sl
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L52
	mov	r4, r0
	b	.L515
.L26:
	and	r2, r5, #7
	bic	r5, r5, #7
	cmp	r5, #31
	mov	r7, r7, lsr r2
	bhi	.L44
	rsb	r3, r5, #31
	mov	r1, r3, lsr #3
	cmp	r4, #0
	and	r3, r1, #3
	beq	.L539
.L207:
	ldr	r2, [sp, #92]
	ldrb	r1, [r2], #1	@ zero_extendqisi2
	add	r7, r7, r1, asl r5
	add	r5, r5, #8
	cmp	r5, #31
	str	r2, [sp, #92]
	sub	r4, r4, #1
	bhi	.L44
	cmp	r3, #0
	beq	.L454
	cmp	r3, #1
	beq	.L379
	cmp	r3, #2
	beq	.L380
	cmp	r4, #0
	beq	.L540
.L209:
	ldr	r3, [sp, #92]
	ldrb	r2, [r3], #1	@ zero_extendqisi2
	str	r3, [sp, #92]
	add	r7, r7, r2, asl r5
	sub	r4, r4, #1
	add	r5, r5, #8
.L380:
	cmp	r4, #0
	beq	.L541
.L212:
	ldr	r3, [sp, #92]
	ldrb	r2, [r3], #1	@ zero_extendqisi2
	str	r3, [sp, #92]
	add	r7, r7, r2, asl r5
	sub	r4, r4, #1
	add	r5, r5, #8
.L379:
	cmp	r4, #0
	beq	.L542
.L215:
	ldr	r3, [sp, #92]
	ldrb	r2, [r3], #1	@ zero_extendqisi2
	add	r7, r7, r2, asl r5
	add	r5, r5, #8
	cmp	r5, #31
	str	r3, [sp, #92]
	sub	r4, r4, #1
	bhi	.L44
.L454:
	str	fp, [sp, #36]
	mov	r0, r4
	b	.L156
.L45:
	ldr	r4, [sp, #92]
	ldrb	fp, [r4], #1	@ zero_extendqisi2
	subs	r0, r0, #1
	add	r7, r7, fp, asl r5
	str	r4, [sp, #92]
	add	fp, r5, #8
	beq	.L543
.L218:
	ldr	r3, [sp, #92]
	ldrb	r4, [r3], #1	@ zero_extendqisi2
	subs	r0, r0, #1
	add	r7, r7, r4, asl fp
	add	r5, fp, #24
	str	r3, [sp, #92]
	add	r4, fp, #8
	beq	.L544
.L220:
	ldr	r3, [sp, #92]
	ldrb	r2, [r3], #1	@ zero_extendqisi2
	subs	r0, r0, #1
	add	r7, r7, r2, asl r4
	str	r3, [sp, #92]
	add	fp, fp, #16
	beq	.L545
.L222:
	ldr	r3, [sp, #92]
	ldrb	r4, [r3], #1	@ zero_extendqisi2
	cmp	r5, #31
	add	r7, r7, r4, asl fp
	str	r3, [sp, #92]
	sub	r0, r0, #1
	bhi	.L546
.L156:
	cmp	r0, #0
	bne	.L45
	add	r1, sp, #92
	mov	r0, sl
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L45
	mov	r4, r0
	b	.L515
.L25:
	ldr	r3, [r6, #4]
	cmp	r3, #0
	bne	.L35
	cmp	r5, #2
	bhi	.L37
	rsb	r3, r5, #2
	mov	r1, r3, lsr #3
	cmp	r4, #0
	and	r3, r1, #3
	beq	.L547
.L190:
	ldr	r2, [sp, #92]
	ldrb	r1, [r2], #1	@ zero_extendqisi2
	add	r7, r7, r1, asl r5
	add	r5, r5, #8
	cmp	r5, #2
	str	r2, [sp, #92]
	sub	r4, r4, #1
	bhi	.L37
	cmp	r3, #0
	beq	.L445
	cmp	r3, #1
	beq	.L376
	cmp	r3, #2
	beq	.L377
	cmp	r4, #0
	beq	.L548
.L192:
	ldr	r3, [sp, #92]
	ldrb	r2, [r3], #1	@ zero_extendqisi2
	str	r3, [sp, #92]
	add	r7, r7, r2, asl r5
	sub	r4, r4, #1
	add	r5, r5, #8
.L377:
	cmp	r4, #0
	beq	.L549
.L195:
	ldr	r3, [sp, #92]
	ldrb	r2, [r3], #1	@ zero_extendqisi2
	str	r3, [sp, #92]
	add	r7, r7, r2, asl r5
	sub	r4, r4, #1
	add	r5, r5, #8
.L376:
	cmp	r4, #0
	beq	.L550
.L198:
	ldr	r3, [sp, #92]
	ldrb	r2, [r3], #1	@ zero_extendqisi2
	add	r7, r7, r2, asl r5
	add	r5, r5, #8
	cmp	r5, #2
	str	r3, [sp, #92]
	sub	r4, r4, #1
	bhi	.L37
.L445:
	str	fp, [sp, #36]
	mov	r0, r4
	b	.L154
.L38:
	ldr	r4, [sp, #92]
	ldrb	fp, [r4], #1	@ zero_extendqisi2
	subs	r0, r0, #1
	add	fp, r7, fp, asl r5
	str	r4, [sp, #92]
	add	r7, r5, #8
	beq	.L551
.L201:
	ldr	r3, [sp, #92]
	ldrb	r4, [r3], #1	@ zero_extendqisi2
	subs	r0, r0, #1
	add	fp, fp, r4, asl r7
	add	r5, r7, #24
	str	r3, [sp, #92]
	add	r4, r7, #8
	beq	.L552
.L203:
	ldr	r3, [sp, #92]
	ldrb	r2, [r3], #1	@ zero_extendqisi2
	subs	r0, r0, #1
	add	fp, fp, r2, asl r4
	str	r3, [sp, #92]
	add	r7, r7, #16
	beq	.L553
.L205:
	ldr	r3, [sp, #92]
	ldrb	r4, [r3], #1	@ zero_extendqisi2
	cmp	r5, #2
	add	r7, fp, r4, asl r7
	str	r3, [sp, #92]
	sub	r0, r0, #1
	bhi	.L554
.L154:
	cmp	r0, #0
	bne	.L38
	add	r1, sp, #92
	mov	r0, sl
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L38
	mov	r4, r0
	b	.L515
.L24:
	ldr	r3, [sp, #92]
	mvn	r0, #1
	b	.L32
.L543:
	add	r1, sp, #92
	mov	r0, sl
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L218
	mov	r4, r0
	b	.L515
.L535:
	add	r1, sp, #92
	mov	r0, sl
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L363
	mov	r4, r0
	b	.L515
.L536:
	add	r1, sp, #92
	mov	r0, sl
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L365
	mov	r4, r0
	b	.L515
.L544:
	add	r1, sp, #92
	mov	r0, sl
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L220
	mov	r4, r0
	b	.L515
.L545:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L222
	mov	r4, r0
	b	.L515
.L537:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L367
	mov	r4, r0
	b	.L515
.L538:
	ldr	fp, [sp, #36]
	mov	r4, r0
.L34:
	and	r2, r7, #31
	add	r3, r2, #256
	mov	r0, r7, lsr #5
	mov	r1, r7, lsr #10
	mov	r2, #284
	add	r3, r3, #1
	and	r0, r0, #31
	and	r1, r1, #15
	add	r2, r2, #2
	add	r0, r0, #1
	add	r1, r1, #4
	cmp	r3, r2
	str	r1, [r6, #92]
	str	r3, [r6, #96]
	str	r0, [r6, #100]
	mov	r7, r7, lsr #14
	sub	r5, r5, #14
	bhi	.L53
	cmp	r0, #30
	bhi	.L53
	mov	lr, #0
	str	lr, [r6, #104]
	mov	r3, lr
	mov	r0, r4
.L55:
	cmp	r5, #2
	bhi	.L59
	cmp	r0, #0
	beq	.L555
.L56:
	ldr	r4, [sp, #92]
	ldrb	ip, [r4], #1	@ zero_extendqisi2
	str	r4, [sp, #92]
	ldr	r3, [r6, #104]
	add	r7, r7, ip, asl r5
	sub	r0, r0, #1
	add	r5, r5, #8
.L59:
	ldr	r2, .L596
	add	r1, r2, r3, asl #1
	add	r4, r1, #2176
	ldrh	lr, [r4, #0]
	ldr	ip, [r6, #92]
	add	r3, r3, #1
	and	r1, r7, #7
	add	r2, r6, lr, asl #1
	cmp	ip, r3
	strh	r1, [r2, #112]	@ movhi
	str	r3, [r6, #104]
	mov	r7, r7, lsr #3
	sub	r5, r5, #3
	bhi	.L55
	cmp	r3, #18
	mov	r4, r0
	bhi	.L61
	ldr	lr, .L596+4
	mov	r1, r3, asl #1
	ldrh	r0, [lr, r1]
	add	r2, r3, #1
	add	r0, r6, r0, asl #1
	add	r1, lr, r1
	mov	ip, #0	@ movhi
	rsb	lr, r3, #18
	cmp	r2, #18
	strh	ip, [r0, #112]	@ movhi
	add	r3, r1, #2
	and	r0, lr, #3
	bhi	.L427
	cmp	r0, #0
	beq	.L505
	cmp	r0, #1
	beq	.L403
	cmp	r0, #2
	beq	.L404
	ldrh	ip, [r1, #2]
	mov	r0, #0	@ movhi
	add	lr, r6, ip, asl #1
	strh	r0, [lr, #112]	@ movhi
	add	r2, r2, #1
	add	r3, r3, #2
.L404:
	ldrh	r0, [r3], #2
	mov	lr, #0	@ movhi
	add	r1, r6, r0, asl #1
	strh	lr, [r1, #112]	@ movhi
	add	r2, r2, #1
.L403:
	ldrh	lr, [r3], #2
	add	r2, r2, #1
	add	r1, r6, lr, asl #1
	mov	ip, #0	@ movhi
	cmp	r2, #18
	strh	ip, [r1, #112]	@ movhi
	bhi	.L427
.L505:
	str	r4, [sp, #36]
.L62:
	mov	lr, r3
	ldrh	ip, [lr], #2
	ldrh	r0, [r3, #2]
	ldrh	r4, [lr, #2]
	ldrh	r1, [r3, #6]
	add	r2, r2, #4
	add	lr, r6, ip, asl #1
	add	r1, r6, r1, asl #1
	add	ip, r6, r0, asl #1
	cmp	r2, #18
	add	r0, r6, r4, asl #1
	mov	r4, #0	@ movhi
	strh	r4, [lr, #112]	@ movhi
	add	r3, r3, #8
	strh	r4, [ip, #112]	@ movhi
	strh	r4, [r0, #112]	@ movhi
	strh	r4, [r1, #112]	@ movhi
	bls	.L62
	ldr	r4, [sp, #36]
.L427:
	str	r2, [r6, #104]
.L61:
	mov	r2, #7
	str	r2, [r6, #84]
	ldr	r3, [sp, #28]
	str	r3, [r6, #108]
	str	r3, [r6, #76]
	ldr	lr, [sp, #48]
	ldr	ip, [sp, #40]
	mov	r0, #0
	ldr	r1, [sp, #52]
	mov	r2, #19
	ldr	r3, [sp, #44]
	str	lr, [sp, #0]
	str	ip, [sp, #4]
	bl	inflate_table
	cmp	r0, #0
	beq	.L63
	ldr	r3, .L596+8
	mov	r2, #27
	str	r3, [r8, #24]
	mov	r3, r2
	str	r2, [r6, #0]
	b	.L409
.L555:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L56
	mov	r4, r0
	mov	r3, r0
	mvn	r0, #4
	b	.L32
.L546:
	ldr	fp, [sp, #36]
	mov	r4, r0
.L44:
	mov	r2, #65536
	sub	r2, r2, #1
	mov	r3, r7, asl #16
	mov	r3, r3, lsr #16
	eor	r2, r2, r7, lsr #16
	cmp	r3, r2
	beq	.L47
	ldr	r3, .L596+12
	mov	r2, #27
	str	r3, [r8, #24]
	mov	r3, r2
	str	r2, [r6, #0]
	b	.L409
.L53:
	ldr	r3, .L596+16
	mov	r2, #27
	str	r3, [r8, #24]
	mov	r3, r2
	str	r2, [r6, #0]
	b	.L409
.L551:
	add	r1, sp, #92
	mov	r0, sl
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L201
	mov	r4, r0
	b	.L515
.L552:
	add	r1, sp, #92
	mov	r0, sl
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L203
	mov	r4, r0
	b	.L515
.L553:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L205
	mov	r4, r0
	b	.L515
.L97:
	ldr	ip, [sp, #32]
	str	fp, [r8, #16]
	str	ip, [r8, #12]
	ldr	lr, [sp, #92]
	str	r4, [r8, #4]
	str	lr, [r8, #0]
	add	r1, r6, #40
	ldmia	r1, {r1, r4}	@ phole ldm
	cmp	r4, r1
	rsbcc	fp, fp, r1
	str	r7, [r6, #56]
	str	r5, [r6, #60]
	strcc	fp, [r6, #44]
	mov	r0, r8
	bl	inflate_fast
	ldr	r0, [r8, #12]
	ldr	r5, [r8, #0]
	str	r0, [sp, #32]
	ldr	fp, [r8, #16]
	str	r5, [sp, #92]
	ldr	r4, [r8, #4]
	ldr	r7, [r6, #56]
	ldr	r5, [r6, #60]
	ldr	r3, [r6, #0]
	b	.L409
.L35:
	and	r2, r5, #7
	mov	r3, #26
	mov	r7, r7, lsr r2
	bic	r5, r5, #7
	str	r3, [r6, #0]
	b	.L409
.L554:
	ldr	fp, [sp, #36]
	mov	r4, r0
.L37:
	mov	r2, r7, lsr #1
	and	r3, r2, #3
	cmp	r3, #2
	and	r7, r7, #1
	moveq	r3, #15
	str	r7, [r6, #4]
	streq	r3, [r6, #0]
	beq	.L43
	cmp	r3, #3
	beq	.L42
	cmp	r3, #1
	ldreq	r1, .L596
	moveq	r0, #9
	movne	r3, #13
	addeq	ip, r1, #2048
	moveq	r3, #18
	streq	r0, [r6, #84]
	moveq	r0, #5
	streq	ip, [r6, #80]
	streq	r0, [r6, #88]
	streq	r1, [r6, #76]
	str	r3, [r6, #0]
.L43:
	mov	r7, r2, lsr #2
	sub	r5, r5, #3
	b	.L409
.L98:
	cmp	r1, #0
	beq	.L102
	tst	r1, #240
	bne	.L103
	mov	ip, #1
	add	r1, r3, r1
	mov	r1, ip, asl r1
	sub	ip, r1, #1
	and	r1, ip, r7
	str	ip, [sp, #64]
	add	ip, r2, r1, lsr r3
	add	r1, r0, ip, asl #2
	str	r1, [sp, #36]
	ldrb	r1, [r1, #1]	@ zero_extendqisi2
	str	r1, [sp, #60]
	add	r1, r3, r1
	str	r1, [sp, #68]
	cmp	r5, r1
	ldrb	r1, [r0, ip, asl #2]	@ zero_extendqisi2
	ldr	ip, [sp, #36]
	ldrh	r0, [ip, #2]
	bcs	.L104
	str	fp, [sp, #72]
	str	r8, [sp, #68]
	str	r9, [sp, #36]
	mov	fp, sl
	mov	r8, r3
	ldr	r9, [sp, #64]
	mov	sl, r2
	b	.L161
.L105:
	ldr	ip, [sp, #92]
	ldrb	r1, [ip], #1	@ zero_extendqisi2
	add	r7, r7, r1, asl r5
	and	r3, r7, r9
	add	r1, sl, r3, lsr r8
	str	ip, [sp, #92]
	ldr	r2, [r6, #76]
	add	r3, r2, r1, asl #2
	ldrb	ip, [r3, #1]	@ zero_extendqisi2
	add	r5, r5, #8
	add	r0, r8, ip
	cmp	r0, r5
	ldrb	r1, [r2, r1, asl #2]	@ zero_extendqisi2
	sub	r4, r4, #1
	ldrh	r0, [r3, #2]
	bls	.L556
.L161:
	cmp	r4, #0
	bne	.L105
	ldr	r4, [sp, #36]
	mov	r0, fp
	add	r1, sp, #92
	mov	lr, pc
	bx	r4
	subs	r4, r0, #0
	bne	.L105
	ldr	r8, [sp, #68]
	mov	r3, r4
	mvn	r0, #4
	b	.L32
.L145:
	rsb	r2, fp, r2
	ldr	r0, [sp, #136]
	ldr	r1, [r6, #52]
	ldr	r3, [sp, #24]
	mov	lr, pc
	bx	r3
	cmp	r0, #0
	beq	.L516
.L146:
	ldr	r3, [sp, #92]
	mvn	r0, #4
	b	.L32
.L531:
	str	r3, [sp, #20]
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	subs	r4, r0, #0
	ldr	r3, [sp, #20]
	bne	.L352
	b	.L515
.L539:
	str	r3, [sp, #20]
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	subs	r4, r0, #0
	ldr	r3, [sp, #20]
	bne	.L207
	b	.L515
.L532:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	subs	r4, r0, #0
	bne	.L354
	b	.L515
.L540:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	subs	r4, r0, #0
	bne	.L209
	b	.L515
.L533:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	subs	r4, r0, #0
	bne	.L357
	b	.L515
.L541:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	subs	r4, r0, #0
	bne	.L212
	b	.L515
.L542:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	subs	r4, r0, #0
	bne	.L215
	b	.L515
.L534:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	subs	r4, r0, #0
	bne	.L360
	b	.L515
.L47:
	cmp	r3, #0
	str	r3, [r6, #64]
	beq	.L48
	str	r8, [sp, #36]
	mov	r7, r3
	ldr	r8, [sp, #32]
	b	.L155
.L49:
	cmp	fp, #0
	beq	.L557
.L50:
	cmp	r4, r7
	movcc	r5, r4
	movcs	r5, r7
	cmp	r5, fp
	movcs	r5, fp
	mov	r0, r8
	ldr	r1, [sp, #92]
	mov	r2, r5
	bl	memcpy
	ldr	r2, [sp, #92]
	add	r1, r2, r5
	str	r1, [sp, #92]
	ldr	r7, [r6, #64]
	rsb	r7, r5, r7
	cmp	r7, #0
	rsb	r4, r5, r4
	rsb	fp, r5, fp
	add	r8, r8, r5
	str	r7, [r6, #64]
	beq	.L558
.L155:
	cmp	r4, #0
	bne	.L49
	add	r1, sp, #92
	mov	r0, sl
	mov	lr, pc
	bx	r9
	subs	r4, r0, #0
	bne	.L49
	ldr	r8, [sp, #36]
	b	.L515
.L557:
	ldr	fp, [r6, #40]
	ldr	r8, [r6, #52]
	str	fp, [r6, #44]
	mov	r1, r8
	mov	r2, fp
	ldr	r0, [sp, #136]
	ldr	ip, [sp, #24]
	mov	lr, pc
	bx	ip
	cmp	r0, #0
	beq	.L50
	ldr	r8, [sp, #36]
	ldr	r3, [sp, #92]
	mvn	r0, #4
	b	.L32
.L102:
	mov	r7, r7, lsr r3
	str	r2, [r6, #64]
	rsb	r5, r3, r5
.L149:
	cmp	fp, #0
	beq	.L559
.L108:
	ldr	ip, [sp, #32]
	strb	r2, [ip], #1
	mov	r3, #18
	str	ip, [sp, #32]
	sub	fp, fp, #1
	str	r3, [r6, #0]
	b	.L409
.L558:
	str	r8, [sp, #32]
	ldr	r8, [sp, #36]
.L48:
	mov	r3, #11
	mov	r5, #0
	str	r3, [r6, #0]
	mov	r7, r5
	b	.L409
.L547:
	str	r3, [sp, #20]
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	subs	r4, r0, #0
	ldr	r3, [sp, #20]
	bne	.L190
	b	.L515
.L548:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	subs	r4, r0, #0
	bne	.L192
	b	.L515
.L549:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	subs	r4, r0, #0
	bne	.L195
	b	.L515
.L550:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	subs	r4, r0, #0
	bne	.L198
	b	.L515
.L103:
	mov	r7, r7, lsr r3
	str	r2, [r6, #64]
	rsb	r5, r3, r5
.L148:
	tst	r1, #32
	movne	r3, #11
	strne	r3, [r6, #0]
	bne	.L409
	tst	r1, #64
	beq	.L111
	ldr	r3, .L596+20
	mov	r2, #27
	str	r3, [r8, #24]
	mov	r3, r2
	str	r2, [r6, #0]
	b	.L409
.L42:
	ldr	r1, .L596+24
	mov	r3, #27
	str	r1, [r8, #24]
	str	r3, [r6, #0]
	b	.L43
.L556:
	mov	r3, r8
	mov	sl, fp
	str	ip, [sp, #60]
	ldr	r8, [sp, #68]
	ldr	r9, [sp, #36]
	ldr	fp, [sp, #72]
.L104:
	mov	r7, r7, lsr r3
	rsb	r5, r3, r5
	ldr	r3, [sp, #60]
	cmp	r1, #0
	mov	r2, r0
	rsb	r5, r3, r5
	mov	r7, r7, lsr r3
	str	r0, [r6, #64]
	bne	.L148
	b	.L149
.L559:
	ldr	fp, [r6, #40]
	ldr	lr, [r6, #52]
	str	lr, [sp, #32]
	str	fp, [r6, #44]
	ldr	r0, [sp, #136]
	ldr	r1, [sp, #32]
	mov	r2, fp
	ldr	ip, [sp, #24]
	mov	lr, pc
	bx	ip
	cmp	r0, #0
	bne	.L146
	ldr	r2, [r6, #64]
	b	.L108
.L111:
	and	r1, r1, #15
	cmp	r1, #0
	str	r1, [r6, #72]
	beq	.L112
	cmp	r1, r5
	movls	r3, r1
	bhi	.L160
	b	.L113
.L114:
	ldr	r1, [sp, #92]
	ldrb	r0, [r1], #1	@ zero_extendqisi2
	str	r1, [sp, #92]
	ldr	r3, [r6, #72]
	add	r7, r7, r0, asl r5
	add	r5, r5, #8
	cmp	r3, r5
	sub	r4, r4, #1
	mov	r1, r3
	bls	.L113
.L160:
	cmp	r4, #0
	bne	.L114
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	subs	r4, r0, #0
	bne	.L114
	b	.L515
.L113:
	mov	r2, #1
	mov	ip, r2, asl r1
	sub	r1, ip, #1
	and	r2, r1, r7
	mov	r7, r7, lsr r3
	ldr	r0, [r6, #64]
	add	ip, r0, r2
	str	ip, [r6, #64]
	rsb	r5, r3, r5
.L112:
	ldr	ip, [r6, #88]
	mov	r2, #1
	mov	r0, r2, asl ip
	ldr	r2, [r6, #80]
	sub	r3, r0, #1
	and	r1, r7, r3
	add	ip, r2, r1, asl #2
	ldrb	r3, [ip, #1]	@ zero_extendqisi2
	cmp	r5, r3
	ldrb	r0, [r2, r1, asl #2]	@ zero_extendqisi2
	ldrh	r1, [ip, #2]
	bcc	.L159
	b	.L116
.L117:
	ldr	r3, [sp, #92]
	ldrb	r2, [r3], #1	@ zero_extendqisi2
	str	r3, [sp, #92]
	ldr	r1, [r6, #88]
	mov	r0, #1
	mov	ip, r0, asl r1
	add	r7, r7, r2, asl r5
	sub	r3, ip, #1
	ldr	r2, [r6, #80]
	and	r0, r7, r3
	add	r1, r2, r0, asl #2
	ldrb	r3, [r1, #1]	@ zero_extendqisi2
	add	r5, r5, #8
	cmp	r3, r5
	sub	r4, r4, #1
	ldrb	r0, [r2, r0, asl #2]	@ zero_extendqisi2
	ldrh	r1, [r1, #2]
	bls	.L116
.L159:
	cmp	r4, #0
	bne	.L117
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	subs	r4, r0, #0
	bne	.L117
	b	.L515
.L116:
	tst	r0, #240
	mov	ip, r3
	movne	r2, r3
	bne	.L120
	mov	ip, #1
	add	r0, r3, r0
	mov	r0, ip, asl r0
	str	r1, [sp, #64]
	sub	r0, r0, #1
	ldr	ip, [sp, #64]
	and	r1, r0, r7
	str	r0, [sp, #36]
	add	r0, ip, r1, lsr r3
	add	r1, r2, r0, asl #2
	str	r1, [sp, #60]
	ldrb	ip, [r1, #1]	@ zero_extendqisi2
	ldrb	r0, [r2, r0, asl #2]	@ zero_extendqisi2
	add	r1, r3, ip
	ldr	r2, [sp, #60]
	cmp	r5, r1
	ldrh	r1, [r2, #2]
	mov	r2, ip
	bcs	.L121
	str	fp, [sp, #68]
	str	r8, [sp, #60]
	ldr	fp, [sp, #64]
	mov	r8, r3
	b	.L158
.L122:
	ldr	r3, [sp, #92]
	ldrb	r2, [r3], #1	@ zero_extendqisi2
	add	r7, r7, r2, asl r5
	ldr	ip, [sp, #36]
	and	r1, r7, ip
	add	r1, fp, r1, lsr r8
	str	r3, [sp, #92]
	ldr	r2, [r6, #80]
	add	r3, r2, r1, asl #2
	ldrb	ip, [r3, #1]	@ zero_extendqisi2
	add	r5, r5, #8
	add	r0, r8, ip
	cmp	r0, r5
	sub	r4, r4, #1
	ldrb	r0, [r2, r1, asl #2]	@ zero_extendqisi2
	ldrh	r1, [r3, #2]
	mov	r2, ip
	bls	.L560
.L158:
	cmp	r4, #0
	bne	.L122
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	subs	r4, r0, #0
	bne	.L122
	ldr	r8, [sp, #60]
	mov	r3, r4
	mvn	r0, #4
	b	.L32
.L560:
	mov	r3, r8
	ldr	fp, [sp, #68]
	ldr	r8, [sp, #60]
.L121:
	mov	r7, r7, lsr r3
	rsb	r5, r3, r5
.L120:
	tst	r0, #64
	mov	r7, r7, lsr r2
	rsb	r5, ip, r5
	beq	.L124
	ldr	r3, .L596+28
	mov	r2, #27
	str	r3, [r8, #24]
	mov	r3, r2
	str	r2, [r6, #0]
	b	.L409
.L63:
	str	r0, [r6, #104]
	add	r3, r6, #96
	ldmia	r3, {r3, ip}	@ phole ldm
	str	fp, [sp, #72]
	str	r8, [sp, #36]
	mov	r2, r0
	mov	r0, r4
.L64:
	add	r3, ip, r3
	cmp	r3, r2
	bls	.L92
	ldr	r4, [r6, #84]
	mov	r8, #1
	mov	r3, r8, asl r4
	ldr	r4, [r6, #76]
	sub	r2, r3, #1
	and	r8, r7, r2
	add	r3, r4, r8, asl #2
	ldrb	r4, [r3, #1]	@ zero_extendqisi2
	cmp	r5, r4
	ldrh	r8, [r3, #2]
	bcc	.L167
	b	.L93
.L65:
	ldr	r1, [sp, #92]
	ldrb	r8, [r1], #1	@ zero_extendqisi2
	str	r1, [sp, #92]
	ldr	r4, [r6, #84]
	mov	r1, #1
	mov	r3, r1, asl r4
	add	r7, r7, r8, asl r5
	sub	r2, r3, #1
	ldr	r4, [r6, #76]
	and	r8, r7, r2
	add	r3, r4, r8, asl #2
	ldrb	r4, [r3, #1]	@ zero_extendqisi2
	add	r5, r5, #8
	cmp	r4, r5
	sub	r0, r0, #1
	ldrh	r8, [r3, #2]
	bls	.L93
.L167:
	cmp	r0, #0
	bne	.L65
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L65
	mov	r4, r0
	mov	r3, r0
	ldr	r8, [sp, #36]
	mvn	r0, #4
	b	.L32
.L93:
	cmp	r8, #15
	bhi	.L67
	cmp	r5, r4
	bcs	.L68
	mvn	r2, r5
	add	ip, r2, r4
	mov	fp, ip, lsr #3
	cmp	r0, #0
	and	fp, fp, #3
	beq	.L561
.L254:
	ldr	ip, [sp, #92]
	ldrb	r2, [ip], #1	@ zero_extendqisi2
	add	r7, r7, r2, asl r5
	add	r5, r5, #8
	cmp	r5, r4
	str	ip, [sp, #92]
	sub	r0, r0, #1
	bcs	.L68
	cmp	fp, #0
	beq	.L469
	cmp	fp, #1
	beq	.L388
	cmp	fp, #2
	beq	.L389
	cmp	r0, #0
	beq	.L562
.L256:
	ldr	r3, [sp, #92]
	ldrb	ip, [r3], #1	@ zero_extendqisi2
	str	r3, [sp, #92]
	add	r7, r7, ip, asl r5
	sub	r0, r0, #1
	add	r5, r5, #8
.L389:
	cmp	r0, #0
	beq	.L563
.L259:
	ldr	ip, [sp, #92]
	ldrb	r2, [ip], #1	@ zero_extendqisi2
	str	ip, [sp, #92]
	add	r7, r7, r2, asl r5
	sub	r0, r0, #1
	add	r5, r5, #8
.L388:
	cmp	r0, #0
	beq	.L564
.L262:
	ldr	r3, [sp, #92]
	ldrb	r2, [r3], #1	@ zero_extendqisi2
	add	r7, r7, r2, asl r5
	add	r5, r5, #8
	cmp	r5, r4
	str	r3, [sp, #92]
	sub	r0, r0, #1
	bcs	.L68
.L469:
	str	r8, [sp, #60]
	b	.L163
.L69:
	ldr	r3, [sp, #92]
	ldrb	r8, [r3], #1	@ zero_extendqisi2
	subs	r0, r0, #1
	add	r8, r7, r8, asl r5
	str	r3, [sp, #92]
	add	r7, r5, #8
	beq	.L565
.L265:
	ldr	ip, [sp, #92]
	ldrb	fp, [ip], #1	@ zero_extendqisi2
	subs	r0, r0, #1
	add	fp, r8, fp, asl r7
	add	r5, r7, #24
	str	ip, [sp, #92]
	add	r8, r7, #8
	beq	.L566
.L267:
	ldr	r3, [sp, #92]
	ldrb	r2, [r3], #1	@ zero_extendqisi2
	subs	r0, r0, #1
	add	r8, fp, r2, asl r8
	str	r3, [sp, #92]
	add	r7, r7, #16
	beq	.L567
.L269:
	ldr	ip, [sp, #92]
	ldrb	r3, [ip], #1	@ zero_extendqisi2
	cmp	r5, r4
	add	r7, r8, r3, asl r7
	str	ip, [sp, #92]
	sub	r0, r0, #1
	bcs	.L568
.L163:
	cmp	r0, #0
	bne	.L69
	add	r1, sp, #92
	mov	r0, sl
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L69
.L503:
	mov	r4, r0
	ldr	r8, [sp, #36]
	b	.L515
.L567:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L269
	b	.L503
.L566:
	add	r1, sp, #92
	mov	r0, sl
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L267
	b	.L503
.L565:
	add	r1, sp, #92
	mov	r0, sl
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L265
	b	.L503
.L597:
	.align	2
.L596:
	.word	.LANCHOR0
	.word	.LANCHOR0+2176
	.word	.LC3
	.word	.LC1
	.word	.LC2
	.word	.LC7
	.word	.LC0
	.word	.LC8
	.word	.LC9
	.word	.LC5
	.word	.LC4
	.word	.LC6
.L67:
	cmp	r8, #16
	beq	.L569
	cmp	r8, #17
	beq	.L79
	add	r8, r4, #7
	cmp	r8, r5
	bls	.L81
	mvn	ip, r5
	add	fp, ip, r8
	mov	r1, fp, lsr #3
	cmp	r0, #0
	and	fp, r1, #3
	beq	.L570
.L325:
	ldr	ip, [sp, #92]
	ldrb	r2, [ip], #1	@ zero_extendqisi2
	add	r7, r7, r2, asl r5
	add	r5, r5, #8
	cmp	r8, r5
	str	ip, [sp, #92]
	sub	r0, r0, #1
	bls	.L81
	cmp	fp, #0
	beq	.L166
	cmp	fp, #1
	beq	.L401
	cmp	fp, #2
	beq	.L402
	cmp	r0, #0
	beq	.L571
.L327:
	ldr	r3, [sp, #92]
	ldrb	r1, [r3], #1	@ zero_extendqisi2
	str	r3, [sp, #92]
	add	r7, r7, r1, asl r5
	sub	r0, r0, #1
	add	r5, r5, #8
.L402:
	cmp	r0, #0
	beq	.L572
.L330:
	ldr	r2, [sp, #92]
	ldrb	ip, [r2], #1	@ zero_extendqisi2
	str	r2, [sp, #92]
	add	r7, r7, ip, asl r5
	sub	r0, r0, #1
	add	r5, r5, #8
.L401:
	cmp	r0, #0
	beq	.L573
.L333:
	ldr	r1, [sp, #92]
	ldrb	r3, [r1], #1	@ zero_extendqisi2
	add	r7, r7, r3, asl r5
	add	r5, r5, #8
	cmp	r8, r5
	str	r1, [sp, #92]
	sub	r0, r0, #1
	bls	.L81
.L166:
	cmp	r0, #0
	beq	.L574
.L85:
	ldr	r3, [sp, #92]
	ldrb	r2, [r3], #1	@ zero_extendqisi2
	subs	r0, r0, #1
	add	fp, r7, r2, asl r5
	str	r3, [sp, #92]
	add	r7, r5, #8
	beq	.L575
.L336:
	ldr	r2, [sp, #92]
	ldrb	ip, [r2], #1	@ zero_extendqisi2
	subs	r0, r0, #1
	add	r3, fp, ip, asl r7
	add	r5, r7, #24
	str	r2, [sp, #92]
	add	fp, r7, #8
	beq	.L576
.L338:
	ldr	ip, [sp, #92]
	ldrb	r1, [ip], #1	@ zero_extendqisi2
	subs	r0, r0, #1
	add	fp, r3, r1, asl fp
	str	ip, [sp, #92]
	add	r7, r7, #16
	beq	.L577
.L340:
	ldr	r3, [sp, #92]
	ldrb	r2, [r3], #1	@ zero_extendqisi2
	cmp	r8, r5
	add	r7, fp, r2, asl r7
	str	r3, [sp, #92]
	sub	r0, r0, #1
	bhi	.L166
.L81:
	mov	r7, r7, lsr r4
	rsb	ip, r4, #-16777216
	add	fp, ip, #16711680
	add	r1, fp, #65280
	add	r8, r1, #249
	and	r4, r7, #127
	ldr	r2, [r6, #104]
	add	r5, r5, r8
	add	r4, r4, #11
	mov	r7, r7, lsr #7
	mov	r1, #0
.L78:
	add	r8, r2, r4
	add	r3, r6, #96
	ldmia	r3, {r3, ip}	@ phole ldm
	str	r8, [sp, #68]
	ldr	fp, [sp, #68]
	add	r8, ip, r3
	cmp	fp, r8
	bhi	.L517
	add	r8, r2, #56
	mov	fp, r8, asl #1
	str	r8, [sp, #76]
	add	r8, r6, fp
	str	fp, [sp, #60]
	and	fp, r8, #3
	mov	r8, fp, lsr #1
	cmp	r8, r4
	movcs	r8, r4
	cmp	r8, #0
	str	r8, [sp, #84]
	ldrne	r8, [sp, #60]
	strneh	r1, [r6, r8]	@ movhi
	ldr	r8, [sp, #84]
	sub	fp, r4, #1
	rsb	r4, r8, r4
	str	r4, [sp, #80]
	mov	r4, r4, lsr #1
	addne	r2, r2, #1
	subne	fp, fp, #1
	movs	r8, r4, asl #1
	str	r4, [sp, #60]
	str	r8, [sp, #64]
	beq	.L88
	ldr	r3, [sp, #76]
	ldr	r4, [sp, #84]
	ldr	r8, [sp, #60]
	add	ip, r3, r4
	mov	r3, #1
	cmp	r3, r8
	sub	r8, r8, #1
	add	ip, r6, ip, asl #1
	orr	r4, r1, r1, asl #16
	and	r8, r8, #3
	str	r8, [sp, #76]
	str	r4, [ip, #0]
	bcs	.L424
	ldr	r8, [sp, #76]
	cmp	r8, #0
	beq	.L89
	cmp	r8, #1
	beq	.L392
	cmp	r8, #2
	movne	r3, #2
	strne	r4, [ip, #4]
	str	r4, [ip, r3, asl #2]
	add	r3, r3, #1
.L392:
	str	r4, [ip, r3, asl #2]
	ldr	r8, [sp, #60]
	add	r3, r3, #1
	cmp	r3, r8
	bcs	.L424
.L89:
	add	r8, r3, #1
	str	r4, [ip, r3, asl #2]
	str	r8, [sp, #76]
	add	r8, r3, #3
	str	r8, [sp, #84]
	ldr	r8, [sp, #76]
	add	r8, r8, #1
	str	r8, [sp, #12]
	ldr	r8, [sp, #60]
	add	r3, r3, #4
	cmp	r3, r8
	ldr	r8, [sp, #76]
	str	r4, [ip, r8, asl #2]
	ldr	r8, [sp, #12]
	str	r4, [ip, r8, asl #2]
	ldr	r8, [sp, #84]
	str	r4, [ip, r8, asl #2]
	bcc	.L89
.L424:
	ldr	r3, [sp, #64]
	ldr	ip, [sp, #80]
	cmp	ip, r3
	rsb	fp, r3, fp
	add	r2, r2, r3
	add	r3, r6, #96
	ldmia	r3, {r3, ip}	@ phole ldm
	beq	.L90
.L88:
	add	r4, r2, #56
	mov	r2, r4, asl #1
	sub	r4, fp, #1
	add	r8, r6, r2
	cmn	r4, #1
	strh	r1, [r6, r2]	@ movhi
	and	fp, fp, #3
	add	r2, r8, #2
	beq	.L90
	cmp	fp, #0
	beq	.L91
	cmp	fp, #1
	beq	.L390
	cmp	fp, #2
	addne	r2, r2, #2
	strneh	r1, [r8, #2]	@ movhi
	subne	r4, r4, #1
	strh	r1, [r2], #2	@ movhi
	sub	r4, r4, #1
.L390:
	sub	r4, r4, #1
	cmn	r4, #1
	strh	r1, [r2], #2	@ movhi
	beq	.L90
.L91:
	mov	r8, r2
	strh	r1, [r8], #2	@ movhi
	sub	r4, r4, #4
	cmn	r4, #1
	strh	r1, [r2, #2]	@ movhi
	strh	r1, [r8, #2]	@ movhi
	strh	r1, [r2, #6]	@ movhi
	add	r2, r2, #8
	bne	.L91
.L90:
	ldr	r2, [sp, #68]
	str	r2, [r6, #104]
	ldr	r2, [sp, #68]
	b	.L64
.L568:
	ldr	r8, [sp, #60]
.L68:
	ldr	r3, [r6, #104]
	add	ip, r6, r3, asl #1
	add	r2, r3, #1
	strh	r8, [ip, #112]	@ movhi
	rsb	r5, r4, r5
	mov	r7, r7, lsr r4
	str	r2, [r6, #104]
	add	r3, r6, #96
	ldmia	r3, {r3, ip}	@ phole ldm
	b	.L64
.L124:
	and	r0, r0, #15
	cmp	r0, #0
	str	r1, [r6, #68]
	str	r0, [r6, #72]
	beq	.L125
	cmp	r5, r0
	movcs	r3, r0
	bcc	.L157
	b	.L126
.L127:
	ldr	r1, [sp, #92]
	ldrb	r0, [r1], #1	@ zero_extendqisi2
	str	r1, [sp, #92]
	ldr	r3, [r6, #72]
	add	r7, r7, r0, asl r5
	add	r5, r5, #8
	cmp	r3, r5
	sub	r4, r4, #1
	mov	r0, r3
	bls	.L578
.L157:
	cmp	r4, #0
	bne	.L127
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	subs	r4, r0, #0
	bne	.L127
	b	.L515
.L578:
	ldr	r1, [r6, #68]
.L126:
	mov	ip, #1
	mov	r2, ip, asl r0
	sub	r2, r2, #1
	and	r2, r2, r7
	mov	r7, r7, lsr r3
	add	r1, r1, r2
	str	r1, [r6, #68]
	rsb	r5, r3, r5
.L125:
	ldr	r2, [r6, #44]
	ldr	r3, [r6, #40]
	cmp	r2, r3
	movcc	r2, fp
	movcs	r2, #0
	rsb	r2, r2, r3
	cmp	r2, r1
	bcs	.L439
	ldr	r3, .L596+32
	mov	r2, #27
	str	r3, [r8, #24]
	mov	r3, r2
	str	r2, [r6, #0]
	b	.L409
.L577:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L340
	b	.L503
.L576:
	str	r3, [sp, #20]
	add	r1, sp, #92
	mov	r0, sl
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	ldr	r3, [sp, #20]
	bne	.L338
	b	.L503
.L575:
	add	r1, sp, #92
	mov	r0, sl
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L336
	b	.L503
.L574:
	add	r1, sp, #92
	mov	r0, sl
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L85
	b	.L503
.L439:
	str	r4, [sp, #36]
	str	r7, [sp, #64]
	ldr	r4, [sp, #32]
	str	r5, [sp, #60]
	mov	r5, r3
.L408:
	cmp	fp, #0
	movne	r3, r5
	movne	r5, fp
	beq	.L579
.L133:
	ldr	r7, [r6, #68]
	rsb	r3, r7, r3
	cmp	r5, r3
	rsbls	r3, r7, r4
	rsbhi	r1, r3, r5
	ldr	r7, [r6, #64]
	addhi	r3, r4, r3
	movls	r1, r5
	orr	fp, r3, r4
	cmp	r1, r7
	movcs	r1, r7
	add	r2, r4, #4
	tst	fp, #3
	add	ip, r3, #4
	movne	r0, #0
	moveq	r0, #1
	cmp	r2, r3
	cmpcs	ip, r4
	movcs	fp, #0
	movcc	fp, #1
	cmp	r1, #3
	movls	r0, #0
	andhi	r0, r0, #1
	ands	r2, r0, fp
	rsb	r0, r1, r7
	str	r0, [r6, #64]
	rsb	fp, r1, r5
	beq	.L137
	mov	ip, r1, lsr #2
	movs	r5, ip, asl #2
	moveq	ip, r1
	moveq	r0, r4
	beq	.L139
	ldr	r2, [r3, #0]
	mov	r0, #1
	sub	r7, ip, #1
	cmp	r0, ip
	str	r2, [r4, #0]
	and	r7, r7, #3
	mov	r2, #4
	bcs	.L426
	cmp	r7, #0
	beq	.L463
	cmp	r7, #1
	beq	.L385
	cmp	r7, #2
	ldrne	r0, [r3, r2]
	strne	r0, [r4, r2]
	movne	r2, #8
	ldr	r7, [r3, r2]
	movne	r0, #2
	str	r7, [r4, r2]
	add	r0, r0, #1
	add	r2, r2, #4
.L385:
	ldr	r7, [r3, r2]
	add	r0, r0, #1
	cmp	r0, ip
	str	r7, [r4, r2]
	add	r2, r2, #4
	bcs	.L426
.L463:
	str	ip, [sp, #32]
	mov	r7, r1
.L140:
	ldr	r1, [r3, r2]
	str	r1, [r4, r2]
	add	r1, r2, #4
	ldr	ip, [r3, r1]
	str	ip, [r4, r1]
	add	r1, r1, #4
	ldr	ip, [r3, r1]
	str	ip, [r4, r1]
	ldr	ip, [sp, #32]
	add	r1, r2, #12
	add	r0, r0, #4
	cmp	r0, ip
	ldr	ip, [r3, r1]
	add	r2, r2, #16
	str	ip, [r4, r1]
	bcc	.L140
	mov	r1, r7
.L426:
	cmp	r1, r5
	add	r3, r3, r5
	add	r0, r4, r5
	rsb	ip, r5, r1
	beq	.L141
.L139:
	ldrb	r7, [r3, #0]	@ zero_extendqisi2
	mov	r2, #1
	sub	r5, ip, #1
	cmp	r2, ip
	strb	r7, [r0, #0]
	and	r5, r5, #3
	beq	.L141
	cmp	r5, #0
	beq	.L461
	cmp	r5, #1
	beq	.L383
	cmp	r5, #2
	ldrneb	r5, [r3, r2]	@ zero_extendqisi2
	strneb	r5, [r0, r2]
	movne	r2, #2
	ldrb	r5, [r3, r2]	@ zero_extendqisi2
	strb	r5, [r0, r2]
	add	r2, r2, #1
.L383:
	ldrb	r5, [r3, r2]	@ zero_extendqisi2
	strb	r5, [r0, r2]
	add	r2, r2, #1
	cmp	r2, ip
	beq	.L141
.L461:
	mov	r7, r1
.L142:
	ldrb	r1, [r3, r2]	@ zero_extendqisi2
	strb	r1, [r0, r2]
	add	r1, r2, #1
	ldrb	r5, [r3, r1]	@ zero_extendqisi2
	strb	r5, [r0, r1]
	add	r5, r1, #1
	ldrb	r1, [r3, r5]	@ zero_extendqisi2
	strb	r1, [r0, r5]
	add	r5, r2, #3
	ldrb	r1, [r3, r5]	@ zero_extendqisi2
	add	r2, r2, #4
	cmp	r2, ip
	strb	r1, [r0, r5]
	bne	.L142
	mov	r1, r7
.L141:
	ldr	r3, [r6, #64]
	cmp	r3, #0
	add	r4, r4, r1
	beq	.L580
	ldr	r5, [r6, #40]
	b	.L408
.L579:
	ldr	r4, [r6, #52]
	str	r5, [r6, #44]
	ldr	r0, [sp, #136]
	mov	r1, r4
	mov	r2, r5
	ldr	ip, [sp, #24]
	mov	lr, pc
	bx	ip
	cmp	r0, #0
	bne	.L581
	ldr	r3, [r6, #40]
	b	.L133
.L137:
	ldrb	ip, [r3, r2]	@ zero_extendqisi2
	sub	r0, r1, #1
	cmp	r1, #1
	strb	ip, [r4, r2]
	and	r0, r0, #3
	mov	r2, #1
	beq	.L141
	cmp	r0, #0
	beq	.L143
	cmp	r0, #1
	beq	.L381
	cmp	r0, #2
	ldrneb	r0, [r3, r2]	@ zero_extendqisi2
	strneb	r0, [r4, r2]
	movne	r2, #2
	ldrb	r5, [r3, r2]	@ zero_extendqisi2
	strb	r5, [r4, r2]
	add	r2, r2, #1
.L381:
	ldrb	r0, [r3, r2]	@ zero_extendqisi2
	strb	r0, [r4, r2]
	add	r2, r2, #1
	cmp	r1, r2
	beq	.L141
.L143:
	ldrb	ip, [r3, r2]	@ zero_extendqisi2
	strb	ip, [r4, r2]
	add	r0, r2, #1
	ldrb	r5, [r3, r0]	@ zero_extendqisi2
	strb	r5, [r4, r0]
	add	r5, r0, #1
	ldrb	ip, [r3, r5]	@ zero_extendqisi2
	strb	ip, [r4, r5]
	add	r0, r2, #3
	ldrb	ip, [r3, r0]	@ zero_extendqisi2
	add	r2, r2, #4
	cmp	r1, r2
	strb	ip, [r4, r0]
	bne	.L143
	b	.L141
.L580:
	str	r4, [sp, #32]
	add	r5, sp, #60
	ldmia	r5, {r5, r7}	@ phole ldm
	ldr	r4, [sp, #36]
	ldr	r3, [r6, #0]
	b	.L409
.L561:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L254
	b	.L503
.L562:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L256
	b	.L503
.L569:
	add	r8, r4, #2
	cmp	r5, r8
	bcs	.L73
	mvn	r3, r5
	add	r2, r3, r8
	mov	fp, r2, lsr #3
	cmp	r0, #0
	and	fp, fp, #3
	beq	.L582
.L291:
	ldr	r3, [sp, #92]
	ldrb	r2, [r3], #1	@ zero_extendqisi2
	add	r7, r7, r2, asl r5
	add	r5, r5, #8
	cmp	r5, r8
	str	r3, [sp, #92]
	sub	r0, r0, #1
	bcs	.L73
	cmp	fp, #0
	beq	.L164
	cmp	fp, #1
	beq	.L395
	cmp	fp, #2
	beq	.L396
	cmp	r0, #0
	beq	.L583
.L293:
	ldr	r3, [sp, #92]
	ldrb	r2, [r3], #1	@ zero_extendqisi2
	str	r3, [sp, #92]
	add	r7, r7, r2, asl r5
	sub	r0, r0, #1
	add	r5, r5, #8
.L396:
	cmp	r0, #0
	beq	.L584
.L296:
	ldr	r3, [sp, #92]
	ldrb	r2, [r3], #1	@ zero_extendqisi2
	str	r3, [sp, #92]
	add	r7, r7, r2, asl r5
	sub	r0, r0, #1
	add	r5, r5, #8
.L395:
	cmp	r0, #0
	beq	.L585
.L299:
	ldr	r3, [sp, #92]
	ldrb	r2, [r3], #1	@ zero_extendqisi2
	add	r7, r7, r2, asl r5
	add	r5, r5, #8
	cmp	r5, r8
	str	r3, [sp, #92]
	sub	r0, r0, #1
	bcc	.L164
	b	.L73
.L74:
	ldr	fp, [sp, #92]
	ldrb	r1, [fp], #1	@ zero_extendqisi2
	subs	r0, r0, #1
	str	fp, [sp, #92]
	add	fp, r7, r1, asl r5
	add	r7, r5, #8
	beq	.L586
.L302:
	ldr	r3, [sp, #92]
	ldrb	r1, [r3], #1	@ zero_extendqisi2
	subs	r0, r0, #1
	str	r3, [sp, #92]
	add	r5, r7, #24
	add	r3, fp, r1, asl r7
	add	fp, r7, #8
	beq	.L587
.L304:
	ldr	r2, [sp, #92]
	ldrb	r1, [r2], #1	@ zero_extendqisi2
	subs	r0, r0, #1
	add	fp, r3, r1, asl fp
	str	r2, [sp, #92]
	add	r7, r7, #16
	beq	.L588
.L306:
	ldr	r3, [sp, #92]
	ldrb	r2, [r3], #1	@ zero_extendqisi2
	cmp	r5, r8
	add	r7, fp, r2, asl r7
	str	r3, [sp, #92]
	sub	r0, r0, #1
	bcs	.L73
.L164:
	cmp	r0, #0
	bne	.L74
	add	r1, sp, #92
	mov	r0, sl
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L74
	b	.L503
.L588:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L306
	b	.L503
.L587:
	str	r3, [sp, #20]
	add	r1, sp, #92
	mov	r0, sl
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	ldr	r3, [sp, #20]
	bne	.L304
	b	.L503
.L586:
	add	r1, sp, #92
	mov	r0, sl
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L302
	b	.L503
.L73:
	ldr	r2, [r6, #104]
	cmp	r2, #0
	rsb	r5, r4, r5
	mov	r7, r7, lsr r4
	beq	.L517
	and	r4, r7, #3
	add	r8, r6, r2, asl #1
	ldrh	r1, [r8, #110]
	add	r4, r4, #3
	mov	r7, r7, lsr #2
	sub	r5, r5, #2
	b	.L78
.L563:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L259
	b	.L503
.L564:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L262
	b	.L503
.L571:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L327
	b	.L503
.L92:
	ldr	r3, [r6, #0]
	cmp	r3, #27
	ldr	fp, [sp, #72]
	mov	r4, r0
	ldr	r8, [sp, #36]
	beq	.L409
	mov	ip, #9
	str	ip, [r6, #84]
	ldr	r1, [sp, #28]
	str	r1, [r6, #108]
	str	r1, [r6, #76]
	ldr	r0, [sp, #40]
	ldr	lr, [sp, #48]
	str	r0, [sp, #4]
	str	lr, [sp, #0]
	mov	r0, #1
	ldr	r1, [sp, #52]
	ldr	r2, [r6, #96]
	ldr	r3, [sp, #44]
	bl	inflate_table
	cmp	r0, #0
	beq	.L95
	ldr	r3, .L596+36
	mov	r2, #27
	str	r3, [r8, #24]
	mov	r3, r2
	str	r2, [r6, #0]
	b	.L409
.L572:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L330
	b	.L503
.L570:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L325
	b	.L503
.L573:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L333
	b	.L503
.L79:
	add	r8, r4, #3
	cmp	r5, r8
	bcs	.L82
	mvn	r3, r5
	add	r2, r3, r8
	mov	ip, r2, lsr #3
	cmp	r0, #0
	and	fp, ip, #3
	beq	.L589
.L308:
	ldr	ip, [sp, #92]
	ldrb	r1, [ip], #1	@ zero_extendqisi2
	add	r7, r7, r1, asl r5
	add	r5, r5, #8
	cmp	r5, r8
	str	ip, [sp, #92]
	sub	r0, r0, #1
	bcs	.L82
	cmp	fp, #0
	beq	.L165
	cmp	fp, #1
	beq	.L398
	cmp	fp, #2
	beq	.L399
	cmp	r0, #0
	beq	.L590
.L310:
	ldr	r3, [sp, #92]
	ldrb	r2, [r3], #1	@ zero_extendqisi2
	str	r3, [sp, #92]
	add	r7, r7, r2, asl r5
	sub	r0, r0, #1
	add	r5, r5, #8
.L399:
	cmp	r0, #0
	beq	.L591
.L313:
	ldr	r1, [sp, #92]
	ldrb	ip, [r1], #1	@ zero_extendqisi2
	str	r1, [sp, #92]
	add	r7, r7, ip, asl r5
	sub	r0, r0, #1
	add	r5, r5, #8
.L398:
	cmp	r0, #0
	beq	.L592
.L316:
	ldr	r3, [sp, #92]
	ldrb	r2, [r3], #1	@ zero_extendqisi2
	add	r7, r7, r2, asl r5
	add	r5, r5, #8
	cmp	r5, r8
	str	r3, [sp, #92]
	sub	r0, r0, #1
	bcc	.L165
	b	.L82
.L83:
	ldr	fp, [sp, #92]
	ldrb	r1, [fp], #1	@ zero_extendqisi2
	subs	r0, r0, #1
	str	fp, [sp, #92]
	add	fp, r7, r1, asl r5
	add	r7, r5, #8
	beq	.L593
.L319:
	ldr	r1, [sp, #92]
	ldrb	ip, [r1], #1	@ zero_extendqisi2
	subs	r0, r0, #1
	add	r3, fp, ip, asl r7
	add	r5, r7, #24
	str	r1, [sp, #92]
	add	fp, r7, #8
	beq	.L594
.L321:
	ldr	r2, [sp, #92]
	ldrb	ip, [r2], #1	@ zero_extendqisi2
	subs	r0, r0, #1
	add	fp, r3, ip, asl fp
	str	r2, [sp, #92]
	add	r7, r7, #16
	beq	.L595
.L323:
	ldr	r3, [sp, #92]
	ldrb	r1, [r3], #1	@ zero_extendqisi2
	cmp	r5, r8
	add	r7, fp, r1, asl r7
	str	r3, [sp, #92]
	sub	r0, r0, #1
	bcs	.L82
.L165:
	cmp	r0, #0
	bne	.L83
	add	r1, sp, #92
	mov	r0, sl
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L83
	b	.L503
.L595:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L323
	b	.L503
.L594:
	str	r3, [sp, #20]
	add	r1, sp, #92
	mov	r0, sl
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	ldr	r3, [sp, #20]
	bne	.L321
	b	.L503
.L593:
	add	r1, sp, #92
	mov	r0, sl
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L319
	b	.L503
.L82:
	mov	r7, r7, lsr r4
	rsb	r3, r4, #-16777216
	add	r2, r3, #16711680
	add	ip, r2, #65280
	and	r4, r7, #7
	add	r8, ip, #253
	add	r5, r5, r8
	add	r4, r4, #3
	mov	r7, r7, lsr #3
	ldr	r2, [r6, #104]
	mov	r1, #0
	b	.L78
.L589:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L308
	b	.L503
.L591:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L313
	b	.L503
.L592:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L316
	b	.L503
.L582:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L291
	b	.L503
.L583:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L293
	b	.L503
.L584:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L296
	b	.L503
.L585:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L299
	b	.L503
.L517:
	ldr	r8, [sp, #36]
	ldr	r3, .L596+40
	mov	r2, #27
	ldr	fp, [sp, #72]
	str	r3, [r8, #24]
	str	r2, [r6, #0]
	mov	r4, r0
	ldr	r3, [r6, #0]
	b	.L409
.L581:
	ldr	r4, [sp, #36]
	ldr	r3, [sp, #92]
	mvn	r0, #4
	b	.L32
.L95:
	ldr	r3, [r6, #108]
	mov	r2, #6
	str	r2, [r6, #88]
	str	r3, [r6, #80]
	ldr	r1, [sp, #40]
	ldr	ip, [sp, #56]
	ldr	lr, [r6, #96]
	str	r1, [sp, #4]
	str	ip, [sp, #0]
	add	r0, lr, #56
	ldr	r3, [sp, #44]
	add	r1, r6, r0, asl #1
	ldr	r2, [r6, #100]
	mov	r0, #2
	bl	inflate_table
	cmp	r0, #0
	moveq	r3, #18
	streq	r3, [r6, #0]
	beq	.L28
	ldr	ip, .L596+44
	mov	lr, #27
	str	ip, [r8, #24]
	mov	r3, lr
	str	lr, [r6, #0]
	b	.L409
.L590:
	mov	r0, sl
	add	r1, sp, #92
	mov	lr, pc
	bx	r9
	cmp	r0, #0
	bne	.L310
	b	.L503
	.size	inflateBack, .-inflateBack
	.section	.rodata
	.align	2
	.set	.LANCHOR0,. + 0
	.type	lenfix.2006, %object
	.size	lenfix.2006, 2048
lenfix.2006:
	.byte	96
	.byte	7
	.short	0
	.byte	0
	.byte	8
	.short	80
	.byte	0
	.byte	8
	.short	16
	.byte	20
	.byte	8
	.short	115
	.byte	18
	.byte	7
	.short	31
	.byte	0
	.byte	8
	.short	112
	.byte	0
	.byte	8
	.short	48
	.byte	0
	.byte	9
	.short	192
	.byte	16
	.byte	7
	.short	10
	.byte	0
	.byte	8
	.short	96
	.byte	0
	.byte	8
	.short	32
	.byte	0
	.byte	9
	.short	160
	.byte	0
	.byte	8
	.short	0
	.byte	0
	.byte	8
	.short	128
	.byte	0
	.byte	8
	.short	64
	.byte	0
	.byte	9
	.short	224
	.byte	16
	.byte	7
	.short	6
	.byte	0
	.byte	8
	.short	88
	.byte	0
	.byte	8
	.short	24
	.byte	0
	.byte	9
	.short	144
	.byte	19
	.byte	7
	.short	59
	.byte	0
	.byte	8
	.short	120
	.byte	0
	.byte	8
	.short	56
	.byte	0
	.byte	9
	.short	208
	.byte	17
	.byte	7
	.short	17
	.byte	0
	.byte	8
	.short	104
	.byte	0
	.byte	8
	.short	40
	.byte	0
	.byte	9
	.short	176
	.byte	0
	.byte	8
	.short	8
	.byte	0
	.byte	8
	.short	136
	.byte	0
	.byte	8
	.short	72
	.byte	0
	.byte	9
	.short	240
	.byte	16
	.byte	7
	.short	4
	.byte	0
	.byte	8
	.short	84
	.byte	0
	.byte	8
	.short	20
	.byte	21
	.byte	8
	.short	227
	.byte	19
	.byte	7
	.short	43
	.byte	0
	.byte	8
	.short	116
	.byte	0
	.byte	8
	.short	52
	.byte	0
	.byte	9
	.short	200
	.byte	17
	.byte	7
	.short	13
	.byte	0
	.byte	8
	.short	100
	.byte	0
	.byte	8
	.short	36
	.byte	0
	.byte	9
	.short	168
	.byte	0
	.byte	8
	.short	4
	.byte	0
	.byte	8
	.short	132
	.byte	0
	.byte	8
	.short	68
	.byte	0
	.byte	9
	.short	232
	.byte	16
	.byte	7
	.short	8
	.byte	0
	.byte	8
	.short	92
	.byte	0
	.byte	8
	.short	28
	.byte	0
	.byte	9
	.short	152
	.byte	20
	.byte	7
	.short	83
	.byte	0
	.byte	8
	.short	124
	.byte	0
	.byte	8
	.short	60
	.byte	0
	.byte	9
	.short	216
	.byte	18
	.byte	7
	.short	23
	.byte	0
	.byte	8
	.short	108
	.byte	0
	.byte	8
	.short	44
	.byte	0
	.byte	9
	.short	184
	.byte	0
	.byte	8
	.short	12
	.byte	0
	.byte	8
	.short	140
	.byte	0
	.byte	8
	.short	76
	.byte	0
	.byte	9
	.short	248
	.byte	16
	.byte	7
	.short	3
	.byte	0
	.byte	8
	.short	82
	.byte	0
	.byte	8
	.short	18
	.byte	21
	.byte	8
	.short	163
	.byte	19
	.byte	7
	.short	35
	.byte	0
	.byte	8
	.short	114
	.byte	0
	.byte	8
	.short	50
	.byte	0
	.byte	9
	.short	196
	.byte	17
	.byte	7
	.short	11
	.byte	0
	.byte	8
	.short	98
	.byte	0
	.byte	8
	.short	34
	.byte	0
	.byte	9
	.short	164
	.byte	0
	.byte	8
	.short	2
	.byte	0
	.byte	8
	.short	130
	.byte	0
	.byte	8
	.short	66
	.byte	0
	.byte	9
	.short	228
	.byte	16
	.byte	7
	.short	7
	.byte	0
	.byte	8
	.short	90
	.byte	0
	.byte	8
	.short	26
	.byte	0
	.byte	9
	.short	148
	.byte	20
	.byte	7
	.short	67
	.byte	0
	.byte	8
	.short	122
	.byte	0
	.byte	8
	.short	58
	.byte	0
	.byte	9
	.short	212
	.byte	18
	.byte	7
	.short	19
	.byte	0
	.byte	8
	.short	106
	.byte	0
	.byte	8
	.short	42
	.byte	0
	.byte	9
	.short	180
	.byte	0
	.byte	8
	.short	10
	.byte	0
	.byte	8
	.short	138
	.byte	0
	.byte	8
	.short	74
	.byte	0
	.byte	9
	.short	244
	.byte	16
	.byte	7
	.short	5
	.byte	0
	.byte	8
	.short	86
	.byte	0
	.byte	8
	.short	22
	.byte	64
	.byte	8
	.short	0
	.byte	19
	.byte	7
	.short	51
	.byte	0
	.byte	8
	.short	118
	.byte	0
	.byte	8
	.short	54
	.byte	0
	.byte	9
	.short	204
	.byte	17
	.byte	7
	.short	15
	.byte	0
	.byte	8
	.short	102
	.byte	0
	.byte	8
	.short	38
	.byte	0
	.byte	9
	.short	172
	.byte	0
	.byte	8
	.short	6
	.byte	0
	.byte	8
	.short	134
	.byte	0
	.byte	8
	.short	70
	.byte	0
	.byte	9
	.short	236
	.byte	16
	.byte	7
	.short	9
	.byte	0
	.byte	8
	.short	94
	.byte	0
	.byte	8
	.short	30
	.byte	0
	.byte	9
	.short	156
	.byte	20
	.byte	7
	.short	99
	.byte	0
	.byte	8
	.short	126
	.byte	0
	.byte	8
	.short	62
	.byte	0
	.byte	9
	.short	220
	.byte	18
	.byte	7
	.short	27
	.byte	0
	.byte	8
	.short	110
	.byte	0
	.byte	8
	.short	46
	.byte	0
	.byte	9
	.short	188
	.byte	0
	.byte	8
	.short	14
	.byte	0
	.byte	8
	.short	142
	.byte	0
	.byte	8
	.short	78
	.byte	0
	.byte	9
	.short	252
	.byte	96
	.byte	7
	.short	0
	.byte	0
	.byte	8
	.short	81
	.byte	0
	.byte	8
	.short	17
	.byte	21
	.byte	8
	.short	131
	.byte	18
	.byte	7
	.short	31
	.byte	0
	.byte	8
	.short	113
	.byte	0
	.byte	8
	.short	49
	.byte	0
	.byte	9
	.short	194
	.byte	16
	.byte	7
	.short	10
	.byte	0
	.byte	8
	.short	97
	.byte	0
	.byte	8
	.short	33
	.byte	0
	.byte	9
	.short	162
	.byte	0
	.byte	8
	.short	1
	.byte	0
	.byte	8
	.short	129
	.byte	0
	.byte	8
	.short	65
	.byte	0
	.byte	9
	.short	226
	.byte	16
	.byte	7
	.short	6
	.byte	0
	.byte	8
	.short	89
	.byte	0
	.byte	8
	.short	25
	.byte	0
	.byte	9
	.short	146
	.byte	19
	.byte	7
	.short	59
	.byte	0
	.byte	8
	.short	121
	.byte	0
	.byte	8
	.short	57
	.byte	0
	.byte	9
	.short	210
	.byte	17
	.byte	7
	.short	17
	.byte	0
	.byte	8
	.short	105
	.byte	0
	.byte	8
	.short	41
	.byte	0
	.byte	9
	.short	178
	.byte	0
	.byte	8
	.short	9
	.byte	0
	.byte	8
	.short	137
	.byte	0
	.byte	8
	.short	73
	.byte	0
	.byte	9
	.short	242
	.byte	16
	.byte	7
	.short	4
	.byte	0
	.byte	8
	.short	85
	.byte	0
	.byte	8
	.short	21
	.byte	16
	.byte	8
	.short	258
	.byte	19
	.byte	7
	.short	43
	.byte	0
	.byte	8
	.short	117
	.byte	0
	.byte	8
	.short	53
	.byte	0
	.byte	9
	.short	202
	.byte	17
	.byte	7
	.short	13
	.byte	0
	.byte	8
	.short	101
	.byte	0
	.byte	8
	.short	37
	.byte	0
	.byte	9
	.short	170
	.byte	0
	.byte	8
	.short	5
	.byte	0
	.byte	8
	.short	133
	.byte	0
	.byte	8
	.short	69
	.byte	0
	.byte	9
	.short	234
	.byte	16
	.byte	7
	.short	8
	.byte	0
	.byte	8
	.short	93
	.byte	0
	.byte	8
	.short	29
	.byte	0
	.byte	9
	.short	154
	.byte	20
	.byte	7
	.short	83
	.byte	0
	.byte	8
	.short	125
	.byte	0
	.byte	8
	.short	61
	.byte	0
	.byte	9
	.short	218
	.byte	18
	.byte	7
	.short	23
	.byte	0
	.byte	8
	.short	109
	.byte	0
	.byte	8
	.short	45
	.byte	0
	.byte	9
	.short	186
	.byte	0
	.byte	8
	.short	13
	.byte	0
	.byte	8
	.short	141
	.byte	0
	.byte	8
	.short	77
	.byte	0
	.byte	9
	.short	250
	.byte	16
	.byte	7
	.short	3
	.byte	0
	.byte	8
	.short	83
	.byte	0
	.byte	8
	.short	19
	.byte	21
	.byte	8
	.short	195
	.byte	19
	.byte	7
	.short	35
	.byte	0
	.byte	8
	.short	115
	.byte	0
	.byte	8
	.short	51
	.byte	0
	.byte	9
	.short	198
	.byte	17
	.byte	7
	.short	11
	.byte	0
	.byte	8
	.short	99
	.byte	0
	.byte	8
	.short	35
	.byte	0
	.byte	9
	.short	166
	.byte	0
	.byte	8
	.short	3
	.byte	0
	.byte	8
	.short	131
	.byte	0
	.byte	8
	.short	67
	.byte	0
	.byte	9
	.short	230
	.byte	16
	.byte	7
	.short	7
	.byte	0
	.byte	8
	.short	91
	.byte	0
	.byte	8
	.short	27
	.byte	0
	.byte	9
	.short	150
	.byte	20
	.byte	7
	.short	67
	.byte	0
	.byte	8
	.short	123
	.byte	0
	.byte	8
	.short	59
	.byte	0
	.byte	9
	.short	214
	.byte	18
	.byte	7
	.short	19
	.byte	0
	.byte	8
	.short	107
	.byte	0
	.byte	8
	.short	43
	.byte	0
	.byte	9
	.short	182
	.byte	0
	.byte	8
	.short	11
	.byte	0
	.byte	8
	.short	139
	.byte	0
	.byte	8
	.short	75
	.byte	0
	.byte	9
	.short	246
	.byte	16
	.byte	7
	.short	5
	.byte	0
	.byte	8
	.short	87
	.byte	0
	.byte	8
	.short	23
	.byte	64
	.byte	8
	.short	0
	.byte	19
	.byte	7
	.short	51
	.byte	0
	.byte	8
	.short	119
	.byte	0
	.byte	8
	.short	55
	.byte	0
	.byte	9
	.short	206
	.byte	17
	.byte	7
	.short	15
	.byte	0
	.byte	8
	.short	103
	.byte	0
	.byte	8
	.short	39
	.byte	0
	.byte	9
	.short	174
	.byte	0
	.byte	8
	.short	7
	.byte	0
	.byte	8
	.short	135
	.byte	0
	.byte	8
	.short	71
	.byte	0
	.byte	9
	.short	238
	.byte	16
	.byte	7
	.short	9
	.byte	0
	.byte	8
	.short	95
	.byte	0
	.byte	8
	.short	31
	.byte	0
	.byte	9
	.short	158
	.byte	20
	.byte	7
	.short	99
	.byte	0
	.byte	8
	.short	127
	.byte	0
	.byte	8
	.short	63
	.byte	0
	.byte	9
	.short	222
	.byte	18
	.byte	7
	.short	27
	.byte	0
	.byte	8
	.short	111
	.byte	0
	.byte	8
	.short	47
	.byte	0
	.byte	9
	.short	190
	.byte	0
	.byte	8
	.short	15
	.byte	0
	.byte	8
	.short	143
	.byte	0
	.byte	8
	.short	79
	.byte	0
	.byte	9
	.short	254
	.byte	96
	.byte	7
	.short	0
	.byte	0
	.byte	8
	.short	80
	.byte	0
	.byte	8
	.short	16
	.byte	20
	.byte	8
	.short	115
	.byte	18
	.byte	7
	.short	31
	.byte	0
	.byte	8
	.short	112
	.byte	0
	.byte	8
	.short	48
	.byte	0
	.byte	9
	.short	193
	.byte	16
	.byte	7
	.short	10
	.byte	0
	.byte	8
	.short	96
	.byte	0
	.byte	8
	.short	32
	.byte	0
	.byte	9
	.short	161
	.byte	0
	.byte	8
	.short	0
	.byte	0
	.byte	8
	.short	128
	.byte	0
	.byte	8
	.short	64
	.byte	0
	.byte	9
	.short	225
	.byte	16
	.byte	7
	.short	6
	.byte	0
	.byte	8
	.short	88
	.byte	0
	.byte	8
	.short	24
	.byte	0
	.byte	9
	.short	145
	.byte	19
	.byte	7
	.short	59
	.byte	0
	.byte	8
	.short	120
	.byte	0
	.byte	8
	.short	56
	.byte	0
	.byte	9
	.short	209
	.byte	17
	.byte	7
	.short	17
	.byte	0
	.byte	8
	.short	104
	.byte	0
	.byte	8
	.short	40
	.byte	0
	.byte	9
	.short	177
	.byte	0
	.byte	8
	.short	8
	.byte	0
	.byte	8
	.short	136
	.byte	0
	.byte	8
	.short	72
	.byte	0
	.byte	9
	.short	241
	.byte	16
	.byte	7
	.short	4
	.byte	0
	.byte	8
	.short	84
	.byte	0
	.byte	8
	.short	20
	.byte	21
	.byte	8
	.short	227
	.byte	19
	.byte	7
	.short	43
	.byte	0
	.byte	8
	.short	116
	.byte	0
	.byte	8
	.short	52
	.byte	0
	.byte	9
	.short	201
	.byte	17
	.byte	7
	.short	13
	.byte	0
	.byte	8
	.short	100
	.byte	0
	.byte	8
	.short	36
	.byte	0
	.byte	9
	.short	169
	.byte	0
	.byte	8
	.short	4
	.byte	0
	.byte	8
	.short	132
	.byte	0
	.byte	8
	.short	68
	.byte	0
	.byte	9
	.short	233
	.byte	16
	.byte	7
	.short	8
	.byte	0
	.byte	8
	.short	92
	.byte	0
	.byte	8
	.short	28
	.byte	0
	.byte	9
	.short	153
	.byte	20
	.byte	7
	.short	83
	.byte	0
	.byte	8
	.short	124
	.byte	0
	.byte	8
	.short	60
	.byte	0
	.byte	9
	.short	217
	.byte	18
	.byte	7
	.short	23
	.byte	0
	.byte	8
	.short	108
	.byte	0
	.byte	8
	.short	44
	.byte	0
	.byte	9
	.short	185
	.byte	0
	.byte	8
	.short	12
	.byte	0
	.byte	8
	.short	140
	.byte	0
	.byte	8
	.short	76
	.byte	0
	.byte	9
	.short	249
	.byte	16
	.byte	7
	.short	3
	.byte	0
	.byte	8
	.short	82
	.byte	0
	.byte	8
	.short	18
	.byte	21
	.byte	8
	.short	163
	.byte	19
	.byte	7
	.short	35
	.byte	0
	.byte	8
	.short	114
	.byte	0
	.byte	8
	.short	50
	.byte	0
	.byte	9
	.short	197
	.byte	17
	.byte	7
	.short	11
	.byte	0
	.byte	8
	.short	98
	.byte	0
	.byte	8
	.short	34
	.byte	0
	.byte	9
	.short	165
	.byte	0
	.byte	8
	.short	2
	.byte	0
	.byte	8
	.short	130
	.byte	0
	.byte	8
	.short	66
	.byte	0
	.byte	9
	.short	229
	.byte	16
	.byte	7
	.short	7
	.byte	0
	.byte	8
	.short	90
	.byte	0
	.byte	8
	.short	26
	.byte	0
	.byte	9
	.short	149
	.byte	20
	.byte	7
	.short	67
	.byte	0
	.byte	8
	.short	122
	.byte	0
	.byte	8
	.short	58
	.byte	0
	.byte	9
	.short	213
	.byte	18
	.byte	7
	.short	19
	.byte	0
	.byte	8
	.short	106
	.byte	0
	.byte	8
	.short	42
	.byte	0
	.byte	9
	.short	181
	.byte	0
	.byte	8
	.short	10
	.byte	0
	.byte	8
	.short	138
	.byte	0
	.byte	8
	.short	74
	.byte	0
	.byte	9
	.short	245
	.byte	16
	.byte	7
	.short	5
	.byte	0
	.byte	8
	.short	86
	.byte	0
	.byte	8
	.short	22
	.byte	64
	.byte	8
	.short	0
	.byte	19
	.byte	7
	.short	51
	.byte	0
	.byte	8
	.short	118
	.byte	0
	.byte	8
	.short	54
	.byte	0
	.byte	9
	.short	205
	.byte	17
	.byte	7
	.short	15
	.byte	0
	.byte	8
	.short	102
	.byte	0
	.byte	8
	.short	38
	.byte	0
	.byte	9
	.short	173
	.byte	0
	.byte	8
	.short	6
	.byte	0
	.byte	8
	.short	134
	.byte	0
	.byte	8
	.short	70
	.byte	0
	.byte	9
	.short	237
	.byte	16
	.byte	7
	.short	9
	.byte	0
	.byte	8
	.short	94
	.byte	0
	.byte	8
	.short	30
	.byte	0
	.byte	9
	.short	157
	.byte	20
	.byte	7
	.short	99
	.byte	0
	.byte	8
	.short	126
	.byte	0
	.byte	8
	.short	62
	.byte	0
	.byte	9
	.short	221
	.byte	18
	.byte	7
	.short	27
	.byte	0
	.byte	8
	.short	110
	.byte	0
	.byte	8
	.short	46
	.byte	0
	.byte	9
	.short	189
	.byte	0
	.byte	8
	.short	14
	.byte	0
	.byte	8
	.short	142
	.byte	0
	.byte	8
	.short	78
	.byte	0
	.byte	9
	.short	253
	.byte	96
	.byte	7
	.short	0
	.byte	0
	.byte	8
	.short	81
	.byte	0
	.byte	8
	.short	17
	.byte	21
	.byte	8
	.short	131
	.byte	18
	.byte	7
	.short	31
	.byte	0
	.byte	8
	.short	113
	.byte	0
	.byte	8
	.short	49
	.byte	0
	.byte	9
	.short	195
	.byte	16
	.byte	7
	.short	10
	.byte	0
	.byte	8
	.short	97
	.byte	0
	.byte	8
	.short	33
	.byte	0
	.byte	9
	.short	163
	.byte	0
	.byte	8
	.short	1
	.byte	0
	.byte	8
	.short	129
	.byte	0
	.byte	8
	.short	65
	.byte	0
	.byte	9
	.short	227
	.byte	16
	.byte	7
	.short	6
	.byte	0
	.byte	8
	.short	89
	.byte	0
	.byte	8
	.short	25
	.byte	0
	.byte	9
	.short	147
	.byte	19
	.byte	7
	.short	59
	.byte	0
	.byte	8
	.short	121
	.byte	0
	.byte	8
	.short	57
	.byte	0
	.byte	9
	.short	211
	.byte	17
	.byte	7
	.short	17
	.byte	0
	.byte	8
	.short	105
	.byte	0
	.byte	8
	.short	41
	.byte	0
	.byte	9
	.short	179
	.byte	0
	.byte	8
	.short	9
	.byte	0
	.byte	8
	.short	137
	.byte	0
	.byte	8
	.short	73
	.byte	0
	.byte	9
	.short	243
	.byte	16
	.byte	7
	.short	4
	.byte	0
	.byte	8
	.short	85
	.byte	0
	.byte	8
	.short	21
	.byte	16
	.byte	8
	.short	258
	.byte	19
	.byte	7
	.short	43
	.byte	0
	.byte	8
	.short	117
	.byte	0
	.byte	8
	.short	53
	.byte	0
	.byte	9
	.short	203
	.byte	17
	.byte	7
	.short	13
	.byte	0
	.byte	8
	.short	101
	.byte	0
	.byte	8
	.short	37
	.byte	0
	.byte	9
	.short	171
	.byte	0
	.byte	8
	.short	5
	.byte	0
	.byte	8
	.short	133
	.byte	0
	.byte	8
	.short	69
	.byte	0
	.byte	9
	.short	235
	.byte	16
	.byte	7
	.short	8
	.byte	0
	.byte	8
	.short	93
	.byte	0
	.byte	8
	.short	29
	.byte	0
	.byte	9
	.short	155
	.byte	20
	.byte	7
	.short	83
	.byte	0
	.byte	8
	.short	125
	.byte	0
	.byte	8
	.short	61
	.byte	0
	.byte	9
	.short	219
	.byte	18
	.byte	7
	.short	23
	.byte	0
	.byte	8
	.short	109
	.byte	0
	.byte	8
	.short	45
	.byte	0
	.byte	9
	.short	187
	.byte	0
	.byte	8
	.short	13
	.byte	0
	.byte	8
	.short	141
	.byte	0
	.byte	8
	.short	77
	.byte	0
	.byte	9
	.short	251
	.byte	16
	.byte	7
	.short	3
	.byte	0
	.byte	8
	.short	83
	.byte	0
	.byte	8
	.short	19
	.byte	21
	.byte	8
	.short	195
	.byte	19
	.byte	7
	.short	35
	.byte	0
	.byte	8
	.short	115
	.byte	0
	.byte	8
	.short	51
	.byte	0
	.byte	9
	.short	199
	.byte	17
	.byte	7
	.short	11
	.byte	0
	.byte	8
	.short	99
	.byte	0
	.byte	8
	.short	35
	.byte	0
	.byte	9
	.short	167
	.byte	0
	.byte	8
	.short	3
	.byte	0
	.byte	8
	.short	131
	.byte	0
	.byte	8
	.short	67
	.byte	0
	.byte	9
	.short	231
	.byte	16
	.byte	7
	.short	7
	.byte	0
	.byte	8
	.short	91
	.byte	0
	.byte	8
	.short	27
	.byte	0
	.byte	9
	.short	151
	.byte	20
	.byte	7
	.short	67
	.byte	0
	.byte	8
	.short	123
	.byte	0
	.byte	8
	.short	59
	.byte	0
	.byte	9
	.short	215
	.byte	18
	.byte	7
	.short	19
	.byte	0
	.byte	8
	.short	107
	.byte	0
	.byte	8
	.short	43
	.byte	0
	.byte	9
	.short	183
	.byte	0
	.byte	8
	.short	11
	.byte	0
	.byte	8
	.short	139
	.byte	0
	.byte	8
	.short	75
	.byte	0
	.byte	9
	.short	247
	.byte	16
	.byte	7
	.short	5
	.byte	0
	.byte	8
	.short	87
	.byte	0
	.byte	8
	.short	23
	.byte	64
	.byte	8
	.short	0
	.byte	19
	.byte	7
	.short	51
	.byte	0
	.byte	8
	.short	119
	.byte	0
	.byte	8
	.short	55
	.byte	0
	.byte	9
	.short	207
	.byte	17
	.byte	7
	.short	15
	.byte	0
	.byte	8
	.short	103
	.byte	0
	.byte	8
	.short	39
	.byte	0
	.byte	9
	.short	175
	.byte	0
	.byte	8
	.short	7
	.byte	0
	.byte	8
	.short	135
	.byte	0
	.byte	8
	.short	71
	.byte	0
	.byte	9
	.short	239
	.byte	16
	.byte	7
	.short	9
	.byte	0
	.byte	8
	.short	95
	.byte	0
	.byte	8
	.short	31
	.byte	0
	.byte	9
	.short	159
	.byte	20
	.byte	7
	.short	99
	.byte	0
	.byte	8
	.short	127
	.byte	0
	.byte	8
	.short	63
	.byte	0
	.byte	9
	.short	223
	.byte	18
	.byte	7
	.short	27
	.byte	0
	.byte	8
	.short	111
	.byte	0
	.byte	8
	.short	47
	.byte	0
	.byte	9
	.short	191
	.byte	0
	.byte	8
	.short	15
	.byte	0
	.byte	8
	.short	143
	.byte	0
	.byte	8
	.short	79
	.byte	0
	.byte	9
	.short	255
	.type	distfix.2007, %object
	.size	distfix.2007, 128
distfix.2007:
	.byte	16
	.byte	5
	.short	1
	.byte	23
	.byte	5
	.short	257
	.byte	19
	.byte	5
	.short	17
	.byte	27
	.byte	5
	.short	4097
	.byte	17
	.byte	5
	.short	5
	.byte	25
	.byte	5
	.short	1025
	.byte	21
	.byte	5
	.short	65
	.byte	29
	.byte	5
	.short	16385
	.byte	16
	.byte	5
	.short	3
	.byte	24
	.byte	5
	.short	513
	.byte	20
	.byte	5
	.short	33
	.byte	28
	.byte	5
	.short	8193
	.byte	18
	.byte	5
	.short	9
	.byte	26
	.byte	5
	.short	2049
	.byte	22
	.byte	5
	.short	129
	.byte	64
	.byte	5
	.short	0
	.byte	16
	.byte	5
	.short	2
	.byte	23
	.byte	5
	.short	385
	.byte	19
	.byte	5
	.short	25
	.byte	27
	.byte	5
	.short	6145
	.byte	17
	.byte	5
	.short	7
	.byte	25
	.byte	5
	.short	1537
	.byte	21
	.byte	5
	.short	97
	.byte	29
	.byte	5
	.short	24577
	.byte	16
	.byte	5
	.short	4
	.byte	24
	.byte	5
	.short	769
	.byte	20
	.byte	5
	.short	49
	.byte	28
	.byte	5
	.short	12289
	.byte	18
	.byte	5
	.short	13
	.byte	26
	.byte	5
	.short	3073
	.byte	22
	.byte	5
	.short	193
	.byte	64
	.byte	5
	.short	0
	.type	order.2028, %object
	.size	order.2028, 38
order.2028:
	.short	16
	.short	17
	.short	18
	.short	0
	.short	8
	.short	7
	.short	9
	.short	6
	.short	10
	.short	5
	.short	11
	.short	4
	.short	12
	.short	3
	.short	13
	.short	2
	.short	14
	.short	1
	.short	15
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"invalid block type\000"
	.space	1
.LC1:
	.ascii	"invalid stored block lengths\000"
	.space	3
.LC2:
	.ascii	"too many length or distance symbols\000"
.LC3:
	.ascii	"invalid code lengths set\000"
	.space	3
.LC4:
	.ascii	"invalid bit length repeat\000"
	.space	2
.LC5:
	.ascii	"invalid literal/lengths set\000"
.LC6:
	.ascii	"invalid distances set\000"
	.space	2
.LC7:
	.ascii	"invalid literal/length code\000"
.LC8:
	.ascii	"invalid distance code\000"
	.space	2
.LC9:
	.ascii	"invalid distance too far back\000"
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
