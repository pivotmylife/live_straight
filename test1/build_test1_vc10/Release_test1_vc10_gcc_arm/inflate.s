	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"inflate.c"
	.section	.text.inflateReset,"ax",%progbits
	.align	2
	.global	inflateReset
	.hidden	inflateReset
	.type	inflateReset, %function
inflateReset:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	bne	.L6
.L2:
	mvn	r0, #1
	bx	lr
.L6:
	ldr	r3, [r0, #28]
	cmp	r3, #0
	beq	.L2
	mov	r2, #0
	mov	ip, #1
	str	r2, [r3, #28]
	add	r1, r3, #1328
	str	ip, [r0, #48]
	str	r2, [r0, #20]
	str	r2, [r0, #8]
	str	r2, [r0, #24]
	mov	r0, #32768
	str	r0, [r3, #20]
	str	r1, [r3, #76]
	str	r2, [r3, #0]
	str	r2, [r3, #4]
	str	r2, [r3, #12]
	str	r2, [r3, #32]
	str	r2, [r3, #40]
	str	r2, [r3, #44]
	str	r2, [r3, #48]
	str	r2, [r3, #56]
	str	r2, [r3, #60]
	str	r1, [r3, #108]
	str	r1, [r3, #80]
	mov	r0, r2
	bx	lr
	.size	inflateReset, .-inflateReset
	.section	.text.inflatePrime,"ax",%progbits
	.align	2
	.global	inflatePrime
	.hidden	inflatePrime
	.type	inflatePrime, %function
inflatePrime:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	str	r4, [sp, #-4]!
	bne	.L11
.L8:
	mvn	r0, #1
.L9:
	ldmfd	sp!, {r4}
	bx	lr
.L11:
	ldr	r3, [r0, #28]
	cmp	r3, #0
	beq	.L8
	cmp	r1, #16
	bgt	.L8
	ldr	ip, [r3, #60]
	add	r0, r1, ip
	cmp	r0, #32
	bhi	.L8
	mvn	r4, #0
	bic	r1, r2, r4, asl r1
	ldr	r2, [r3, #56]
	add	ip, r2, r1, asl ip
	str	r0, [r3, #60]
	str	ip, [r3, #56]
	mov	r0, #0
	b	.L9
	.size	inflatePrime, .-inflatePrime
	.section	.text.inflateInit2_,"ax",%progbits
	.align	2
	.global	inflateInit2_
	.hidden	inflateInit2_
	.type	inflateInit2_, %function
inflateInit2_:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r2, #0
	stmfd	sp!, {r4, r5, r6, lr}
	mov	r4, r0
	mov	r5, r1
	bne	.L24
.L13:
	mvn	r0, #5
.L15:
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L24:
	ldrb	r2, [r2, #0]	@ zero_extendqisi2
	cmp	r2, #49
	cmpeq	r3, #56
	moveq	r3, #0
	movne	r3, #1
	bne	.L13
	cmp	r0, #0
	mvneq	r0, #1
	beq	.L15
.L14:
	ldr	ip, [r0, #32]
	cmp	ip, #0
	str	r3, [r0, #24]
	ldreq	r3, .L26
	streq	ip, [r0, #40]
	streq	r3, [r0, #32]
	moveq	ip, r3
	ldr	r3, [r0, #36]
	cmp	r3, #0
	ldreq	r3, .L26+4
	ldreq	ip, [r0, #32]
	streq	r3, [r0, #36]
	mov	r0, #9472
	add	r2, r0, #48
	mov	r1, #1
	ldr	r0, [r4, #40]
	mov	lr, pc
	bx	ip
	subs	r1, r0, #0
	mvneq	r0, #3
	beq	.L15
	cmp	r5, #0
	movlt	r3, #0
	str	r1, [r4, #28]
	rsblt	r5, r5, #0
	strlt	r3, [r1, #8]
	blt	.L20
	mov	r2, r5, asr #4
	add	lr, r2, #1
	cmp	r5, #47
	str	lr, [r1, #8]
	andle	r5, r5, #15
.L20:
	sub	ip, r5, #8
	cmp	ip, #7
	bhi	.L25
	mov	r0, #0
	str	r0, [r1, #52]
	mov	r0, r4
	str	r5, [r1, #36]
	ldmfd	sp!, {r4, r5, r6, lr}
	b	inflateReset
.L25:
	ldr	r0, [r4, #40]
	ldr	ip, [r4, #36]
	mov	lr, pc
	bx	ip
	mov	r1, #0
	str	r1, [r4, #28]
	mvn	r0, #1
	b	.L15
.L27:
	.align	2
.L26:
	.word	zcalloc
	.word	zcfree
	.size	inflateInit2_, .-inflateInit2_
	.section	.text.inflateEnd,"ax",%progbits
	.align	2
	.global	inflateEnd
	.hidden	inflateEnd
	.type	inflateEnd, %function
inflateEnd:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	subs	r4, r0, #0
	bne	.L33
.L29:
	mvn	r0, #1
.L31:
	ldmfd	sp!, {r4, lr}
	bx	lr
.L33:
	ldr	r1, [r4, #28]
	cmp	r1, #0
	beq	.L29
	ldr	r3, [r4, #36]
	cmp	r3, #0
	beq	.L29
	ldr	r2, [r1, #52]
	cmp	r2, #0
	beq	.L30
	mov	r1, r2
	ldr	r0, [r4, #40]
	mov	lr, pc
	bx	r3
	ldr	r3, [r4, #36]
	ldr	r1, [r4, #28]
.L30:
	ldr	r0, [r4, #40]
	mov	lr, pc
	bx	r3
	mov	r0, #0
	str	r0, [r4, #28]
	b	.L31
	.size	inflateEnd, .-inflateEnd
	.section	.text.inflateGetHeader,"ax",%progbits
	.align	2
	.global	inflateGetHeader
	.hidden	inflateGetHeader
	.type	inflateGetHeader, %function
inflateGetHeader:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	bne	.L38
.L35:
	mvn	r0, #1
	bx	lr
.L38:
	ldr	r3, [r0, #28]
	cmp	r3, #0
	beq	.L35
	ldr	r2, [r3, #8]
	tst	r2, #2
	movne	r0, #0
	strne	r1, [r3, #32]
	strne	r0, [r1, #48]
	bxne	lr
	b	.L35
	.size	inflateGetHeader, .-inflateGetHeader
	.section	.text.inflateSync,"ax",%progbits
	.align	2
	.global	inflateSync
	.hidden	inflateSync
	.type	inflateSync, %function
inflateSync:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	subs	r5, r0, #0
	sub	sp, sp, #12
	bne	.L68
.L40:
	mvn	r0, #1
.L42:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L68:
	ldr	r4, [r5, #28]
	cmp	r4, #0
	beq	.L40
	ldr	ip, [r5, #4]
	cmp	ip, #0
	bne	.L41
	ldr	r3, [r4, #60]
	cmp	r3, #7
	mvnls	r0, #4
	bls	.L42
.L41:
	ldr	r1, [r4, #0]
	cmp	r1, #29
	beq	.L69
	add	r1, r4, #56
	ldmia	r1, {r1, r2}	@ phole ldm
	and	r3, r2, #7
	mov	r3, r1, asl r3
	bic	r2, r2, #7
	cmp	r2, #7
	mov	r6, #29
	str	r3, [r4, #56]
	movls	r3, #0
	str	r2, [r4, #60]
	str	r6, [r4, #0]
	strls	r3, [r4, #104]
	movls	r2, #1
	bls	.L48
	mov	r0, #0
	add	r6, sp, #4
.L46:
	strb	r3, [r6, r0]
	ldr	r1, [r4, #60]
	ldr	r3, [r4, #56]
	sub	r2, r1, #8
	mov	r3, r3, lsr #8
	cmp	r2, #7
	add	r0, r0, #1
	str	r3, [r4, #56]
	str	r2, [r4, #60]
	bhi	.L46
	subs	r2, r0, #0
	movne	r2, #1
	cmp	r2, #0
	mov	r3, #0
	str	r3, [r4, #104]
	moveq	r3, r2
	moveq	r2, #1
	beq	.L48
	mov	r1, r3
	b	.L54
.L51:
	cmp	r2, #0
	movne	r2, #1
	movne	r3, #0
	bne	.L52
	rsb	r3, r3, #4
	cmp	r3, #3
	movhi	r2, #0
	movls	r2, #1
.L52:
	add	r1, r1, #1
	cmp	r0, r1
	movls	ip, #0
	andhi	ip, r2, #1
	cmp	ip, #0
	beq	.L48
.L54:
	cmp	r3, #1
	ldrb	r2, [r6, r1]	@ zero_extendqisi2
	movhi	ip, #255
	movls	ip, #0
	cmp	r2, ip
	bne	.L51
	add	r3, r3, #1
	cmp	r3, #3
	movhi	r2, #0
	movls	r2, #1
	b	.L52
.L48:
	str	r3, [r4, #104]
	ldr	ip, [r5, #4]
.L44:
	cmp	ip, #0
	moveq	r2, #0
	andne	r2, r2, #1
	cmp	r2, #0
	ldr	r6, [r5, #0]
	beq	.L56
	mov	r2, #0
	b	.L62
.L59:
	cmp	r1, #0
	movne	r1, #1
	movne	r3, #0
	bne	.L60
	rsb	r3, r3, #4
	cmp	r3, #3
	movhi	r1, #0
	movls	r1, #1
.L60:
	add	r2, r2, #1
	cmp	r2, ip
	movcs	r1, #0
	andcc	r1, r1, #1
	cmp	r1, #0
	beq	.L56
.L62:
	cmp	r3, #1
	ldrb	r1, [r6, r2]	@ zero_extendqisi2
	movhi	r0, #255
	movls	r0, #0
	cmp	r1, r0
	bne	.L59
	add	r3, r3, #1
	cmp	r3, #3
	movhi	r1, #0
	movls	r1, #1
	b	.L60
.L56:
	str	r3, [r4, #104]
	ldmia	r5, {r1, ip}	@ phole ldm
	ldr	r6, [r5, #8]
	rsb	r3, r2, ip
	add	r6, r2, r6
	add	r2, r1, r2
	stmia	r5, {r2, r3, r6}	@ phole stm
	ldr	ip, [r4, #104]
	cmp	ip, #4
	mvnne	r0, #2
	bne	.L42
	ldr	r7, [r5, #20]
	mov	r0, r5
	bl	inflateReset
	mov	r0, #11
	str	r7, [r5, #20]
	str	r6, [r5, #8]
	str	r0, [r4, #0]
	mov	r0, #0
	b	.L42
.L69:
	ldr	r3, [r4, #104]
	cmp	r3, #3
	movhi	r2, #0
	movls	r2, #1
	b	.L44
	.size	inflateSync, .-inflateSync
	.section	.text.inflateSyncPoint,"ax",%progbits
	.align	2
	.global	inflateSyncPoint
	.hidden	inflateSyncPoint
	.type	inflateSyncPoint, %function
inflateSyncPoint:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	bne	.L75
.L71:
	mvn	r0, #1
	bx	lr
.L75:
	ldr	r3, [r0, #28]
	cmp	r3, #0
	beq	.L71
	ldr	r2, [r3, #0]
	cmp	r2, #13
	movne	r0, #0
	bxne	lr
	ldr	r0, [r3, #60]
	rsbs	r0, r0, #1
	movcc	r0, #0
	bx	lr
	.size	inflateSyncPoint, .-inflateSyncPoint
	.section	.text.inflateCopy,"ax",%progbits
	.align	2
	.global	inflateCopy
	.hidden	inflateCopy
	.type	inflateCopy, %function
inflateCopy:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	cmpne	r1, #0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	mov	r8, r1
	mov	r4, r0
	bne	.L86
.L77:
	mvn	r0, #1
.L79:
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L86:
	ldr	r5, [r1, #28]
	cmp	r5, #0
	beq	.L77
	ldr	r3, [r1, #32]
	cmp	r3, #0
	beq	.L77
	ldr	r2, [r1, #36]
	cmp	r2, #0
	beq	.L77
	mov	r0, #9472
	add	r2, r0, #48
	ldr	r0, [r1, #40]
	mov	r1, #1
	mov	lr, pc
	bx	r3
	subs	r6, r0, #0
	beq	.L85
	ldr	r7, [r5, #52]
	cmp	r7, #0
	beq	.L81
	ldr	r1, [r5, #36]
	mov	r2, #1
	mov	r1, r2, asl r1
	ldr	r0, [r8, #40]
	ldr	ip, [r8, #32]
	mov	lr, pc
	bx	ip
	subs	r7, r0, #0
	beq	.L87
.L81:
	ldmia	r8!, {r0, r1, r2, r3}
	mov	lr, r4
	stmia	lr!, {r0, r1, r2, r3}
	ldmia	r8!, {r0, r1, r2, r3}
	mov	ip, lr
	stmia	ip!, {r0, r1, r2, r3}
	ldmia	r8!, {r0, r1, r2, r3}
	stmia	ip!, {r0, r1, r2, r3}
	ldmia	r8, {r0, r1}
	mov	r3, ip
	mov	ip, #9472
	stmia	r3, {r0, r1}
	add	r2, ip, #48
	mov	r0, r6
	mov	r1, r5
	bl	memcpy
	ldr	r2, [r5, #76]
	add	r3, r5, #1328
	cmp	r2, r3
	bcc	.L82
	add	r1, r5, #9472
	add	r0, r1, #44
	cmp	r2, r0
	bhi	.L82
	ldr	r0, [r5, #80]
	rsb	r1, r3, r2
	rsb	r2, r3, r0
	bic	ip, r1, #3
	bic	r2, r2, #3
	add	r1, ip, #1328
	add	r0, r2, #1328
	add	ip, r6, r1
	add	r2, r6, r0
	str	ip, [r6, #76]
	str	r2, [r6, #80]
.L82:
	ldr	r0, [r5, #108]
	rsb	r1, r3, r0
	bic	r2, r1, #3
	add	ip, r2, #1328
	add	r3, r6, ip
	cmp	r7, #0
	str	r3, [r6, #108]
	beq	.L83
	ldr	r3, [r5, #36]
	mov	lr, #1
	mov	r2, lr, asl r3
	ldr	r1, [r5, #52]
	mov	r0, r7
	bl	memcpy
.L83:
	str	r7, [r6, #52]
	mov	r0, #0
	str	r6, [r4, #28]
	b	.L79
.L87:
	mov	r1, r6
	ldr	r0, [r8, #40]
	ldr	ip, [r8, #36]
	mov	lr, pc
	bx	ip
.L85:
	mvn	r0, #3
	b	.L79
	.size	inflateCopy, .-inflateCopy
	.section	.text.updatewindow,"ax",%progbits
	.align	2
	.type	updatewindow, %function
updatewindow:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, lr}
	ldr	r4, [r0, #28]
	mov	r6, r0
	ldr	r0, [r4, #52]
	cmp	r0, #0
	mov	r5, r1
	beq	.L98
.L89:
	ldr	r2, [r4, #40]
	cmp	r2, #0
	beq	.L99
	ldr	ip, [r6, #16]
	rsb	r5, ip, r5
	cmp	r5, r2
	bcs	.L100
.L92:
	ldr	r3, [r4, #48]
	rsb	r7, r3, r2
	ldr	r2, [r6, #12]
	cmp	r7, r5
	movcs	r7, r5
	rsb	r1, r5, r2
	add	r0, r0, r3
	mov	r2, r7
	bl	memcpy
	subs	r5, r5, r7
	bne	.L101
	ldr	r3, [r4, #48]
	ldr	lr, [r4, #40]
	add	r2, r7, r3
	ldr	r1, [r4, #44]
	cmp	r2, lr
	str	r2, [r4, #48]
	streq	r5, [r4, #48]
	cmp	lr, r1
	addhi	r7, r7, r1
	strhi	r7, [r4, #44]
	mov	r0, #0
.L90:
	ldmfd	sp!, {r3, r4, r5, r6, r7, lr}
	bx	lr
.L99:
	ldr	r0, [r4, #36]
	mov	r2, #1
	mov	r2, r2, asl r0
	mov	r3, #0
	str	r3, [r4, #44]
	str	r2, [r4, #40]
	str	r3, [r4, #48]
	ldr	ip, [r6, #16]
	rsb	r5, ip, r5
	cmp	r5, r2
	ldr	r0, [r4, #52]
	bcc	.L92
.L100:
	ldr	lr, [r6, #12]
	rsb	r1, r2, lr
	bl	memcpy
	ldr	r1, [r4, #40]
	mov	r0, #0
	str	r1, [r4, #44]
	str	r0, [r4, #48]
	b	.L90
.L101:
	ldr	ip, [r6, #12]
	ldr	r0, [r4, #52]
	rsb	r1, r5, ip
	mov	r2, r5
	bl	memcpy
	ldr	r0, [r4, #40]
	str	r5, [r4, #48]
	str	r0, [r4, #44]
	mov	r0, #0
	b	.L90
.L98:
	ldr	r1, [r4, #36]
	mov	r7, #1
	mov	r1, r7, asl r1
	ldr	r0, [r6, #40]
	mov	r2, r7
	ldr	ip, [r6, #32]
	mov	lr, pc
	bx	ip
	cmp	r0, #0
	str	r0, [r4, #52]
	moveq	r0, r7
	bne	.L89
	b	.L90
	.size	updatewindow, .-updatewindow
	.section	.text.inflateSetDictionary,"ax",%progbits
	.align	2
	.global	inflateSetDictionary
	.hidden	inflateSetDictionary
	.type	inflateSetDictionary, %function
inflateSetDictionary:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, lr}
	subs	r6, r0, #0
	mov	r7, r1
	mov	r4, r2
	bne	.L112
.L103:
	mvn	r0, #1
.L107:
	ldmfd	sp!, {r3, r4, r5, r6, r7, lr}
	bx	lr
.L112:
	ldr	r5, [r6, #28]
	cmp	r5, #0
	beq	.L103
	ldr	r3, [r5, #8]
	cmp	r3, #0
	ldr	r3, [r5, #0]
	bne	.L113
	cmp	r3, #10
	beq	.L105
.L106:
	mov	r0, r6
	ldr	r1, [r6, #16]
	bl	updatewindow
	cmp	r0, #0
	movne	r3, #28
	strne	r3, [r5, #0]
	mvnne	r0, #3
	bne	.L107
	ldr	r2, [r5, #40]
	cmp	r4, r2
	bls	.L109
	rsb	ip, r2, r4
	add	r1, r7, ip
	ldr	r0, [r5, #52]
	bl	memcpy
	ldr	r0, [r5, #40]
	str	r0, [r5, #44]
.L110:
	mov	r3, #1
	str	r3, [r5, #12]
	mov	r0, #0
	b	.L107
.L109:
	ldr	lr, [r5, #52]
	rsb	r2, r4, r2
	add	r0, r2, lr
	mov	r1, r7
	mov	r2, r4
	bl	memcpy
	str	r4, [r5, #44]
	b	.L110
.L113:
	cmp	r3, #10
	bne	.L103
.L105:
	mov	r0, #0
	mov	r1, r0
	mov	r2, r0
	bl	adler32
	mov	r1, r7
	mov	r2, r4
	bl	adler32
	ldr	r1, [r5, #24]
	cmp	r0, r1
	mvnne	r0, #2
	bne	.L107
	b	.L106
	.size	inflateSetDictionary, .-inflateSetDictionary
	.global	__aeabi_uidivmod
	.section	.text.inflate,"ax",%progbits
	.align	2
	.global	inflate
	.hidden	inflate
	.type	inflate, %function
inflate:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 96
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	subs	r4, r0, #0
	sub	sp, sp, #108
	mov	r7, r1
	bne	.L941
.L115:
	mvn	sl, #1
.L150:
	mov	r0, sl
	add	sp, sp, #108
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L941:
	ldr	r6, [r4, #28]
	cmp	r6, #0
	beq	.L115
	ldr	ip, [r4, #12]
	cmp	ip, #0
	beq	.L115
	ldr	fp, [r4, #0]
	cmp	fp, #0
	beq	.L942
.L116:
	ldr	r3, [r6, #0]
	cmp	r3, #11
	moveq	r3, #12
	streq	r3, [r6, #0]
	ldr	r8, [r4, #16]
	ldreq	ip, [r4, #12]
	ldreq	fp, [r4, #0]
	str	r8, [sp, #36]
	ldr	r5, [r4, #4]
	str	r5, [sp, #48]
	add	r2, r6, #112
	ldr	r8, [r6, #56]
	ldr	r5, [r6, #60]
	add	r1, r6, #1328
	add	r0, r6, #84
	str	r2, [sp, #68]
	ldr	r2, [sp, #36]
	add	r9, r6, #108
	mov	sl, #0
	str	r1, [sp, #52]
	str	r0, [sp, #64]
	add	r1, r6, #88
	add	r0, r6, #752
	str	r9, [sp, #60]
	str	r2, [sp, #44]
	str	r0, [sp, #56]
	mov	r2, r7
	str	r1, [sp, #76]
	ldr	r9, [sp, #48]
	str	sl, [sp, #24]
	mov	r7, r4
	mov	r4, ip
	mov	ip, r2
.L118:
	cmp	r3, #28
	ldrls	pc, [pc, r3, asl #2]
	b	.L115
.L148:
	.word	.L119
	.word	.L120
	.word	.L121
	.word	.L122
	.word	.L350
	.word	.L351
	.word	.L352
	.word	.L353
	.word	.L354
	.word	.L128
	.word	.L129
	.word	.L130
	.word	.L131
	.word	.L132
	.word	.L133
	.word	.L134
	.word	.L135
	.word	.L136
	.word	.L137
	.word	.L138
	.word	.L139
	.word	.L140
	.word	.L141
	.word	.L142
	.word	.L143
	.word	.L355
	.word	.L145
	.word	.L146
	.word	.L940
.L131:
	ldr	r2, [r6, #4]
	cmp	r2, #0
	bne	.L224
	cmp	r5, #2
	bhi	.L225
	cmp	r9, #0
	beq	.L943
	ldrb	r3, [fp], #1	@ zero_extendqisi2
	sub	r9, r9, #1
	add	r8, r8, r3, asl r5
	add	r5, r5, #8
.L225:
	mov	r3, r8, lsr #1
	and	r2, r3, #3
	cmp	r2, #2
	and	r8, r8, #1
	moveq	r2, #15
	str	r8, [r6, #4]
	streq	r2, [r6, #0]
	beq	.L231
	cmp	r2, #3
	beq	.L230
	cmp	r2, #1
	movne	r2, #13
	strne	r2, [r6, #0]
	bne	.L231
	mov	r1, #5
	add	r2, r1, #13
	str	r2, [r6, #0]
	ldr	sl, .L1043
	ldr	r2, .L1043+4
	mov	r8, #9
	str	r8, [r6, #84]
	str	r1, [r6, #88]
	str	r2, [r6, #76]
	str	sl, [r6, #80]
.L231:
	mov	r8, r3, lsr #2
	sub	r5, r5, #3
	ldr	r3, [r6, #0]
	b	.L118
.L128:
	cmp	r5, #31
	bhi	.L159
	cmp	r9, #0
	beq	.L944
	rsb	r2, r5, #31
	mov	sl, r2, lsr #3
	ands	r3, sl, #3
	beq	.L660
	ldrb	r1, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r1, asl r5
	add	r5, r5, #8
	cmp	r5, #31
	sub	r9, r9, #1
	bhi	.L159
	cmp	r9, #0
	beq	.L834
	cmp	r3, #1
	beq	.L660
	cmp	r3, #2
	beq	.L683
	ldrb	r3, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r3, asl r5
	add	r5, r5, #8
	beq	.L836
.L683:
	ldrb	sl, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, sl, asl r5
	add	r5, r5, #8
	bne	.L660
	b	.L1037
.L222:
	cmp	r9, #0
	beq	.L946
	ldrb	sl, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, sl, asl r5
	add	r5, r5, #8
	beq	.L838
	ldrb	r0, [fp, #0]	@ zero_extendqisi2
	subs	r9, r3, #2
	add	r8, r8, r0, asl r5
	add	fp, r2, #2
	add	r5, r1, #16
	beq	.L839
	ldrb	fp, [r2, #2]	@ zero_extendqisi2
	subs	r9, r3, #3
	add	r8, r8, fp, asl r5
	add	fp, r2, #3
	add	r5, r1, #24
	beq	.L947
.L660:
	ldrb	r1, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r1, asl r5
	add	r5, r5, #8
	sub	r9, r9, #1
	cmp	r5, #31
	mov	r2, fp
	mov	r3, r9
	mov	r1, r5
	bls	.L222
.L159:
	mov	r5, r8, lsr #24
	mov	r1, r8, lsr #8
	add	r2, r5, r8, asl #24
	and	sl, r1, #65280
	and	r8, r8, #65280
	add	r3, r2, sl
	add	r0, r3, r8, asl #8
	mov	r5, #10
	mov	r8, #0
	str	r0, [r6, #24]
	str	r0, [r7, #48]
	str	r5, [r6, #0]
	mov	r5, r8
.L129:
	ldr	r0, [r6, #12]
	cmp	r0, #0
	beq	.L948
	mov	r0, #0
	mov	r1, r0
	mov	r2, r0
	str	ip, [sp, #16]
	bl	adler32
	mov	sl, #11
	str	r0, [r6, #24]
	str	r0, [r7, #48]
	str	sl, [r6, #0]
	ldr	ip, [sp, #16]
.L130:
	cmp	ip, #5
	bne	.L131
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
.L149:
	str	ip, [r4, #12]
	ldr	r1, [sp, #36]
	str	fp, [r4, #0]
	str	r1, [r4, #16]
	str	r9, [r4, #4]
	ldr	ip, [r6, #40]
	cmp	ip, #0
	str	r8, [r6, #56]
	str	r5, [r6, #60]
	bne	.L336
	ldr	r3, [r6, #0]
	cmp	r3, #23
	bhi	.L337
	ldr	r0, [sp, #44]
	ldr	r2, [r4, #16]
	cmp	r2, r0
	moveq	r5, r0
	beq	.L338
.L336:
	mov	r0, r4
	ldr	r1, [sp, #44]
	bl	updatewindow
	cmp	r0, #0
	bne	.L339
.L337:
	ldr	r5, [r4, #16]
.L338:
	add	r1, sp, #44
	ldmia	r1, {r1, ip}	@ phole ldm
	ldr	r8, [r4, #4]
	ldr	r0, [r4, #8]
	ldr	r3, [r4, #20]
	rsb	r5, r5, r1
	rsb	r8, r8, ip
	add	r2, r0, r8
	add	r0, r3, r5
	str	r2, [r4, #8]
	str	r0, [r4, #20]
	ldr	r1, [r6, #28]
	ldr	r3, [r6, #8]
	add	ip, r1, r5
	cmp	r5, #0
	cmpne	r3, #0
	str	ip, [r6, #28]
	beq	.L340
	ldr	r2, [r6, #16]
	ldr	lr, [r4, #12]
	cmp	r2, #0
	rsb	r1, r5, lr
	ldr	r0, [r6, #24]
	mov	r2, r5
	beq	.L341
	bl	crc32
.L342:
	str	r0, [r6, #24]
	str	r0, [r4, #48]
.L340:
	ldr	r2, [r6, #4]
	ldr	ip, [r6, #0]
	cmp	r2, #0
	movne	r2, #64
	cmp	ip, #11
	moveq	ip, #128
	movne	ip, #0
	orrs	r8, r5, r8
	ldr	r8, [r6, #60]
	movne	r1, #0
	moveq	r1, #1
	cmp	r7, #4
	movne	r7, r1
	orreq	r7, r1, #1
	add	r0, r2, r8
	add	r3, r0, ip
	cmp	r7, #0
	str	r3, [r4, #44]
	beq	.L150
	cmp	sl, #0
	mvneq	sl, #4
	b	.L150
.L141:
	ldr	r2, [sp, #36]
	cmp	r2, #0
	beq	.L794
	ldr	sl, [sp, #44]
	ldr	r3, [r6, #68]
	rsb	r1, r2, sl
	cmp	r1, r3
	bcs	.L310
	ldr	sl, [r6, #48]
	rsb	r1, r1, r3
	cmp	r1, sl
	ldrhi	r0, [r6, #40]
	ldr	r2, [r6, #64]
	ldrhi	r3, [r6, #52]
	ldrls	r3, [r6, #52]
	rsbhi	r1, sl, r1
	rsbhi	r0, r1, r0
	rsbls	sl, r1, sl
	str	r2, [sp, #28]
	addhi	r3, r0, r3
	addls	r3, sl, r3
	cmp	r1, r2
	movcs	r1, r2
.L313:
	ldr	sl, [sp, #36]
	orr	r0, r3, r4
	cmp	r1, sl
	movcs	r1, sl
	add	r2, r4, #4
	tst	r0, #3
	add	r0, r3, #4
	movne	sl, #0
	moveq	sl, #1
	cmp	r2, r3
	cmpcs	r0, r4
	movcs	r0, #0
	movcc	r0, #1
	cmp	r1, #3
	movls	r2, #0
	andhi	r2, sl, #1
	ldr	sl, [sp, #28]
	ands	r0, r2, r0
	rsb	r2, r1, sl
	str	r2, [r6, #64]
	ldr	sl, [sp, #36]
	rsb	r2, r1, sl
	str	r2, [sp, #36]
	beq	.L314
	mov	sl, r1, lsr #2
	movs	r2, sl, asl #2
	str	sl, [sp, #28]
	str	r2, [sp, #32]
	streq	r1, [sp, #28]
	moveq	sl, r4
	beq	.L316
	mov	r0, #1
	sub	r2, sl, #1
	cmp	sl, r0
	ldr	sl, [r3, #0]
	str	sl, [r4, #0]
	and	sl, r2, #3
	mov	r2, #4
	bls	.L778
	cmp	sl, #0
	beq	.L851
	cmp	sl, #1
	beq	.L689
	cmp	sl, #2
	ldrne	r0, [r3, r2]
	strne	r0, [r4, r2]
	movne	r2, #8
	ldr	sl, [r3, r2]
	movne	r0, #2
	str	sl, [r4, r2]
	add	r0, r0, #1
	add	r2, r2, #4
.L689:
	ldr	sl, [sp, #28]
	add	r0, r0, #1
	cmp	sl, r0
	ldr	sl, [r3, r2]
	str	sl, [r4, r2]
	add	r2, r2, #4
	bls	.L778
.L851:
	str	ip, [sp, #40]
	mov	sl, r1
.L317:
	ldr	r1, [r3, r2]
	str	r1, [r4, r2]
	add	r1, r2, #4
	ldr	ip, [r3, r1]
	str	ip, [r4, r1]
	add	r1, r1, #4
	ldr	ip, [r3, r1]
	str	ip, [r4, r1]
	ldr	ip, [sp, #28]
	add	r1, r2, #12
	add	r0, r0, #4
	cmp	ip, r0
	ldr	ip, [r3, r1]
	add	r2, r2, #16
	str	ip, [r4, r1]
	bhi	.L317
	ldr	ip, [sp, #40]
	mov	r1, sl
.L778:
	ldr	r0, [sp, #32]
	rsb	r2, r0, r1
	cmp	r1, r0
	add	r3, r3, r0
	add	sl, r4, r0
	str	r2, [sp, #28]
	beq	.L318
.L316:
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	ldr	r0, [sp, #28]
	str	r2, [sp, #32]
	mov	r2, #1
	cmp	r2, r0
	sub	r0, r0, #1
	str	r0, [sp, #40]
	ldr	r0, [sp, #32]
	strb	r0, [sl, #0]
	ldr	r0, [sp, #40]
	and	r0, r0, #3
	str	r0, [sp, #32]
	beq	.L318
	cmp	r0, #0
	beq	.L849
	cmp	r0, #1
	beq	.L687
	cmp	r0, #2
	ldrneb	r0, [r3, r2]	@ zero_extendqisi2
	strneb	r0, [sl, r2]
	movne	r2, #2
	ldrb	r0, [r3, r2]	@ zero_extendqisi2
	strb	r0, [sl, r2]
	add	r2, r2, #1
.L687:
	ldrb	r0, [r3, r2]	@ zero_extendqisi2
	strb	r0, [sl, r2]
	ldr	r0, [sp, #28]
	add	r2, r2, #1
	cmp	r2, r0
	beq	.L318
.L849:
	str	r1, [sp, #32]
	str	ip, [sp, #40]
.L319:
	ldrb	r1, [r3, r2]	@ zero_extendqisi2
	strb	r1, [sl, r2]
	add	ip, r2, #1
	ldrb	r0, [r3, ip]	@ zero_extendqisi2
	strb	r0, [sl, ip]
	add	r1, ip, #1
	ldrb	r0, [r3, r1]	@ zero_extendqisi2
	strb	r0, [sl, r1]
	ldr	ip, [sp, #28]
	add	r1, r2, #3
	ldrb	r0, [r3, r1]	@ zero_extendqisi2
	add	r2, r2, #4
	cmp	r2, ip
	strb	r0, [sl, r1]
	bne	.L319
	ldr	r1, [sp, #32]
	ldr	ip, [sp, #40]
.L318:
	ldr	r3, [r6, #64]
	cmp	r3, #0
	moveq	r3, #18
	add	r4, r4, r1
	ldrne	r3, [r6, #0]
	streq	r3, [r6, #0]
	b	.L118
.L984:
	mov	r4, sl
.L292:
	mvn	r0, #0
	bic	r1, r8, r0, asl r1
	ldr	r3, [r6, #72]
	mov	r8, r8, lsr r3
	ldr	sl, [r6, #64]
	add	r2, sl, r1
	str	r2, [r6, #64]
	rsb	r5, r3, r5
.L291:
	mov	r3, #20
	str	r3, [r6, #0]
.L139:
	ldr	r2, [r6, #88]
	mvn	r3, #0
	mvn	r0, r3, asl r2
	str	r0, [sp, #40]
	ldr	r2, [r6, #80]
	and	r1, r8, r0
	add	sl, r2, r1, asl #2
	ldrb	r3, [sl, #1]	@ zero_extendqisi2
	cmp	r5, r3
	ldrb	r1, [r2, r1, asl #2]	@ zero_extendqisi2
	ldrh	r0, [sl, #2]
	bcs	.L295
	cmp	r9, #0
	beq	.L949
	ldrb	sl, [fp], #1	@ zero_extendqisi2
	add	r8, r8, sl, asl r5
	ldr	r3, [sp, #40]
	and	r1, r8, r3
	add	sl, r2, r1, asl #2
	sub	r0, r9, #1
	ldrb	r3, [sl, #1]	@ zero_extendqisi2
	mov	r9, r0
	and	r0, r0, #3
	add	r5, r5, #8
	ldrb	r1, [r2, r1, asl #2]	@ zero_extendqisi2
	str	r0, [sp, #28]
	cmp	r3, r5
	ldrh	r0, [sl, #2]
	bls	.L295
	cmp	r9, #0
	beq	.L863
	ldr	sl, [sp, #28]
	cmp	sl, #0
	beq	.L865
	cmp	sl, #1
	beq	.L696
	cmp	sl, #2
	beq	.L697
	ldrb	r0, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r0, asl r5
	ldr	r3, [sp, #40]
	and	r1, r8, r3
	add	sl, r2, r1, asl #2
	ldrb	r3, [sl, #1]	@ zero_extendqisi2
	add	r5, r5, #8
	cmp	r3, r5
	sub	r9, r9, #1
	ldrb	r1, [r2, r1, asl #2]	@ zero_extendqisi2
	ldrh	r0, [sl, #2]
	bls	.L295
.L697:
	ldrb	r0, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r0, asl r5
	ldr	r3, [sp, #40]
	and	r1, r8, r3
	add	sl, r2, r1, asl #2
	ldrb	r3, [sl, #1]	@ zero_extendqisi2
	add	r5, r5, #8
	cmp	r3, r5
	sub	r9, r9, #1
	ldrb	r1, [r2, r1, asl #2]	@ zero_extendqisi2
	ldrh	r0, [sl, #2]
	bls	.L295
.L696:
	ldrb	r0, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r0, asl r5
	ldr	r3, [sp, #40]
	and	r1, r8, r3
	add	sl, r2, r1, asl #2
	ldrb	r3, [sl, #1]	@ zero_extendqisi2
	add	r5, r5, #8
	cmp	r3, r5
	sub	r9, r9, #1
	ldrb	r1, [r2, r1, asl #2]	@ zero_extendqisi2
	ldrh	r0, [sl, #2]
	bls	.L295
	cmp	r9, #0
	beq	.L950
.L865:
	str	r4, [sp, #28]
	str	r6, [sp, #32]
	ldr	sl, [sp, #40]
	str	r7, [sp, #40]
	mov	r7, ip
.L695:
	ldrb	r6, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r6, asl r5
	and	ip, r8, sl
	add	r0, r2, ip, asl #2
	ldrb	r3, [r0, #1]	@ zero_extendqisi2
	add	r5, r5, #8
	sub	r9, r9, #1
	cmp	r3, r5
	ldrb	r1, [r2, ip, asl #2]	@ zero_extendqisi2
	mov	r4, r9
	mov	ip, fp
	mov	r6, r5
	ldrh	r0, [r0, #2]
	bls	.L867
	ldrb	r3, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r3, asl r5
	and	r1, r8, sl
	add	r0, r2, r1, asl #2
	ldrb	r3, [r0, #1]	@ zero_extendqisi2
	add	r5, r5, #8
	cmp	r3, r5
	sub	r9, r9, #1
	ldrb	r1, [r2, r1, asl #2]	@ zero_extendqisi2
	ldrh	r0, [r0, #2]
	bls	.L867
	ldrb	r9, [fp, #0]	@ zero_extendqisi2
	add	r8, r8, r9, asl r5
	and	fp, r8, sl
	add	r0, r2, fp, asl #2
	ldrb	r3, [r0, #1]	@ zero_extendqisi2
	add	r5, r6, #16
	cmp	r3, r5
	ldrb	r1, [r2, fp, asl #2]	@ zero_extendqisi2
	sub	r9, r4, #2
	add	fp, ip, #2
	ldrh	r0, [r0, #2]
	bls	.L867
	ldrb	r3, [ip, #2]	@ zero_extendqisi2
	add	r8, r8, r3, asl r5
	and	r1, r8, sl
	add	r0, r2, r1, asl #2
	ldrb	r3, [r0, #1]	@ zero_extendqisi2
	add	r5, r6, #24
	cmp	r3, r5
	sub	r9, r4, #3
	add	fp, ip, #3
	ldrb	r1, [r2, r1, asl #2]	@ zero_extendqisi2
	ldrh	r0, [r0, #2]
	bls	.L867
	cmp	r9, #0
	bne	.L695
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r6, [sp, #32]
	ldr	r4, [sp, #40]
	b	.L149
.L339:
	mov	lr, #28
	str	lr, [r6, #0]
.L940:
	mvn	sl, #3
	b	.L150
.L354:
	ldr	r2, [r6, #16]
.L127:
	tst	r2, #512
	beq	.L216
	cmp	r5, #15
	bhi	.L217
	cmp	r9, #0
	beq	.L951
	rsb	r1, r5, #15
	mov	sl, r1, lsr #3
	ands	r3, sl, #3
	beq	.L807
	ldrb	sl, [fp], #1	@ zero_extendqisi2
	add	r8, r8, sl, asl r5
	add	r5, r5, #8
	cmp	r5, #15
	sub	r9, r9, #1
	bhi	.L217
	cmp	r9, #0
	beq	.L804
	cmp	r3, #1
	beq	.L807
	cmp	r3, #2
	beq	.L678
	ldrb	r3, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r3, asl r5
	add	r5, r5, #8
	beq	.L806
.L678:
	ldrb	r1, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r1, asl r5
	add	r5, r5, #8
	beq	.L952
.L807:
	mov	sl, r2
.L659:
	ldrb	r3, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r3, asl r5
	add	r5, r5, #8
	sub	r9, r9, #1
	cmp	r5, #15
	mov	r2, fp
	mov	r3, r9
	mov	r1, r5
	bhi	.L953
	cmp	r9, #0
	beq	.L954
	ldrb	r0, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r0, asl r5
	add	r5, r5, #8
	beq	.L808
	ldrb	r0, [fp, #0]	@ zero_extendqisi2
	subs	r9, r3, #2
	add	r8, r8, r0, asl r5
	add	fp, r2, #2
	add	r5, r1, #16
	beq	.L809
	ldrb	fp, [r2, #2]	@ zero_extendqisi2
	subs	r9, r3, #3
	add	r8, r8, fp, asl r5
	add	fp, r2, #3
	add	r5, r1, #24
	bne	.L659
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L353:
	ldr	r2, [r6, #16]
.L126:
	ands	r1, r2, #4096
	beq	.L211
	cmp	r9, #0
	beq	.L789
	mov	sl, #0
.L213:
	ldr	r2, [r6, #32]
	cmp	r2, #0
	ldrb	r3, [fp, sl]	@ zero_extendqisi2
	add	sl, sl, #1
	beq	.L212
	ldr	r1, [r2, #36]
	cmp	r1, #0
	beq	.L212
	ldr	r0, [r2, #40]
	ldr	r2, [r6, #64]
	cmp	r2, r0
	addcc	r0, r2, #1
	strccb	r3, [r1, r2]
	strcc	r0, [r6, #64]
.L212:
	cmp	r3, #0
	cmpne	sl, r9
	bcc	.L213
	ldr	r1, [r6, #16]
	tst	r1, #512
	bne	.L955
.L214:
	cmp	r3, #0
	add	fp, fp, sl
	rsb	r9, sl, r9
	bne	.L790
.L939:
	ldr	r2, [r6, #16]
.L215:
	mov	r3, #8
	str	r3, [r6, #0]
	b	.L127
.L135:
	ldr	r3, [r6, #104]
	ldr	sl, [r6, #92]
	cmp	sl, r3
	bls	.L155
.L154:
	ldr	sl, .L1043+8
	add	r2, sl, r3, asl #1
.L240:
	cmp	r5, #2
	add	r3, r3, #1
	bhi	.L241
	cmp	r9, #0
	beq	.L956
	ldrb	r1, [fp], #1	@ zero_extendqisi2
	sub	r9, r9, #1
	add	r8, r8, r1, asl r5
	add	r5, r5, #8
.L241:
	ldr	r0, [r6, #92]
	ldrh	r1, [r2], #2
	cmp	r0, r3
	add	sl, r6, r1, asl #1
	and	r0, r8, #7
	strh	r0, [sl, #112]	@ movhi
	str	r3, [r6, #104]
	mov	r8, r8, lsr #3
	sub	r5, r5, #3
	bhi	.L240
.L155:
	cmp	r3, #18
	bhi	.L243
	ldr	lr, .L1043+8
	mov	r1, r3, asl #1
	ldr	r3, .L1043+12
	add	r2, lr, r1
	ldrh	sl, [lr, r1]
	rsb	r0, r2, r3
	ldr	lr, .L1043+12
	sub	r1, r0, #2
	add	r3, r2, #2
	mov	r1, r1, lsr #1
	add	r0, r6, sl, asl #1
	cmp	r3, lr
	mov	lr, #0	@ movhi
	strh	lr, [r0, #112]	@ movhi
	and	r1, r1, #3
	beq	.L750
	cmp	r1, #0
	beq	.L244
	cmp	r1, #1
	beq	.L716
	cmp	r1, #2
	ldrneh	r2, [r2, #2]
	addne	r3, r3, #2
	ldrh	r0, [r3], #2
	movne	lr, #0	@ movhi
	addne	r2, r6, r2, asl #1
	strneh	lr, [r2, #112]	@ movhi
	add	sl, r6, r0, asl #1
	mov	lr, #0	@ movhi
	strh	lr, [sl, #112]	@ movhi
.L716:
	ldrh	sl, [r3], #2
	ldr	lr, .L1043+12
	add	r2, r6, sl, asl #1
	mov	r1, #0	@ movhi
	cmp	r3, lr
	strh	r1, [r2, #112]	@ movhi
	beq	.L750
.L244:
	mov	sl, r3
	ldrh	lr, [sl], #2
	ldrh	r0, [r3, #2]
	ldrh	r1, [sl, #2]
	ldrh	r2, [r3, #6]
	ldr	sl, .L1043+12
	add	r3, r3, #8
	add	lr, r6, lr, asl #1
	add	r0, r6, r0, asl #1
	add	r1, r6, r1, asl #1
	add	r2, r6, r2, asl #1
	cmp	r3, sl
	mov	sl, #0	@ movhi
	strh	sl, [lr, #112]	@ movhi
	strh	sl, [r0, #112]	@ movhi
	strh	sl, [r1, #112]	@ movhi
	strh	sl, [r2, #112]	@ movhi
	bne	.L244
.L750:
	mov	r3, #19
	str	r3, [r6, #104]
.L243:
	mov	r1, #7
	str	r1, [r6, #84]
	ldr	r2, [sp, #52]
	str	r2, [r6, #108]
	str	r2, [r6, #76]
	ldr	sl, [sp, #64]
	ldr	lr, [sp, #56]
	mov	r0, #0
	ldr	r1, [sp, #68]
	mov	r2, #19
	ldr	r3, [sp, #60]
	str	ip, [sp, #16]
	stmia	sp, {sl, lr}	@ phole stm
	bl	inflate_table
	cmp	r0, #0
	str	r0, [sp, #24]
	ldr	ip, [sp, #16]
	bne	.L957
	mov	sl, #17
	str	sl, [r6, #0]
	ldr	r3, [sp, #24]
	str	r3, [r6, #104]
	ldr	sl, [sp, #24]
	b	.L153
.L133:
	ldr	r3, [r6, #64]
.L156:
	cmp	r3, #0
	beq	.L937
	ldr	sl, [sp, #36]
	cmp	r9, r3
	movcc	r3, r9
	cmp	r3, sl
	movcc	sl, r3
	cmp	sl, #0
	beq	.L792
	mov	r1, fp
	mov	r0, r4
	mov	r2, sl
	str	ip, [sp, #16]
	bl	memcpy
	ldr	r1, [r6, #64]
	rsb	r2, sl, r1
	str	r2, [r6, #64]
	ldr	ip, [sp, #36]
	rsb	r2, sl, ip
	str	r2, [sp, #36]
	rsb	r9, sl, r9
	add	fp, fp, sl
	add	r4, r4, sl
	ldr	r3, [r6, #0]
	ldr	ip, [sp, #16]
	b	.L118
.L132:
	and	r3, r5, #7
	bic	r5, r5, #7
	cmp	r5, #31
	mov	r8, r8, lsr r3
	bhi	.L232
	cmp	r9, #0
	beq	.L958
	rsb	r1, r5, #31
	mov	sl, r1, lsr #3
	ands	r3, sl, #3
	beq	.L661
	ldrb	r1, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r1, asl r5
	add	r5, r5, #8
	cmp	r5, #31
	sub	r9, r9, #1
	bhi	.L232
	cmp	r9, #0
	beq	.L840
	cmp	r3, #1
	beq	.L661
	cmp	r3, #2
	beq	.L684
	ldrb	r3, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r3, asl r5
	add	r5, r5, #8
	beq	.L842
.L684:
	ldrb	sl, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, sl, asl r5
	add	r5, r5, #8
	beq	.L959
.L661:
	ldrb	r3, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r3, asl r5
	add	r5, r5, #8
	sub	r9, r9, #1
	cmp	r5, #31
	mov	r2, fp
	mov	r3, r9
	mov	r1, r5
	bhi	.L232
	cmp	r9, #0
	beq	.L960
	ldrb	sl, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, sl, asl r5
	add	r5, r5, #8
	beq	.L844
	ldrb	r0, [fp, #0]	@ zero_extendqisi2
	subs	r9, r3, #2
	add	r8, r8, r0, asl r5
	add	fp, r2, #2
	add	r5, r1, #16
	beq	.L845
	ldrb	r0, [r2, #2]	@ zero_extendqisi2
	subs	r9, r3, #3
	add	r8, r8, r0, asl r5
	add	fp, r2, #3
	add	r5, r1, #24
	bne	.L661
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L140:
	ldr	r1, [r6, #72]
.L151:
	cmp	r1, #0
	ldreq	r2, [r6, #68]
	beq	.L305
	cmp	r5, r1
	bcs	.L306
	cmp	r9, #0
	beq	.L961
	mvn	r0, r5
	add	r3, r0, r1
	mov	sl, r3, lsr #3
	ands	r3, sl, #3
	beq	.L855
	ldrb	r2, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r2, asl r5
	add	r5, r5, #8
	cmp	r5, r1
	sub	r9, r9, #1
	bcs	.L306
	cmp	r9, #0
	beq	.L852
	cmp	r3, #1
	beq	.L855
	cmp	r3, #2
	beq	.L691
	ldrb	r3, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r3, asl r5
	add	r5, r5, #8
	beq	.L854
.L691:
	ldrb	r0, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r0, asl r5
	add	r5, r5, #8
	beq	.L962
.L855:
	mov	sl, r4
	b	.L671
.L308:
	cmp	r9, #0
	beq	.L963
	ldrb	r4, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r4, asl r5
	add	r5, r5, #8
	beq	.L856
	ldrb	r4, [fp, #0]	@ zero_extendqisi2
	subs	r9, r3, #2
	add	r8, r8, r4, asl r5
	add	fp, r2, #2
	add	r5, r0, #16
	beq	.L857
	ldrb	fp, [r2, #2]	@ zero_extendqisi2
	subs	r9, r3, #3
	add	r8, r8, fp, asl r5
	add	fp, r2, #3
	add	r5, r0, #24
	beq	.L964
.L671:
	ldrb	r4, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r4, asl r5
	add	r5, r5, #8
	sub	r9, r9, #1
	cmp	r5, r1
	mov	r2, fp
	mov	r3, r9
	mov	r0, r5
	bcc	.L308
	mov	r4, sl
.L306:
	mvn	r3, #0
	bic	r1, r8, r3, asl r1
	ldr	sl, [r6, #72]
	mov	r8, r8, lsr sl
	ldr	r2, [r6, #68]
	add	r2, r1, r2
	str	r2, [r6, #68]
	rsb	r5, sl, r5
.L305:
	ldr	r0, [r6, #44]
	ldr	r3, [sp, #44]
	add	r1, r3, r0
	ldr	r0, [sp, #36]
	rsb	sl, r0, r1
	cmp	sl, r2
	movcs	sl, #22
	strcs	sl, [r6, #0]
	bcs	.L141
	ldr	r1, .L1043+16
	mov	sl, #27
	str	r1, [r7, #24]
	mov	r3, sl
	str	sl, [r6, #0]
	b	.L118
.L143:
	ldr	r3, [r6, #8]
	cmp	r3, #0
	beq	.L321
	cmp	r5, #31
	bhi	.L322
	cmp	r9, #0
	beq	.L965
	rsb	sl, r5, #31
	mov	r2, sl, lsr #3
	ands	r3, r2, #3
	beq	.L675
	ldrb	r2, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r2, asl r5
	add	r5, r5, #8
	cmp	r5, #31
	sub	r9, r9, #1
	bhi	.L322
	cmp	r9, #0
	beq	.L931
	cmp	r3, #1
	beq	.L675
	cmp	r3, #2
	beq	.L720
	ldrb	r1, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r1, asl r5
	add	r5, r5, #8
	beq	.L933
.L720:
	ldrb	sl, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, sl, asl r5
	add	r5, r5, #8
	beq	.L966
.L675:
	ldrb	r3, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r3, asl r5
	add	r5, r5, #8
	sub	r9, r9, #1
	cmp	r5, #31
	mov	r2, fp
	mov	r3, r9
	mov	r1, r5
	bhi	.L322
	cmp	r9, #0
	beq	.L967
	ldrb	sl, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, sl, asl r5
	add	r5, r5, #8
	beq	.L935
	ldrb	r0, [fp, #0]	@ zero_extendqisi2
	subs	r9, r3, #2
	add	r8, r8, r0, asl r5
	add	fp, r2, #2
	add	r5, r1, #16
	beq	.L936
	ldrb	r0, [r2, #2]	@ zero_extendqisi2
	subs	r9, r3, #3
	add	r8, r8, r0, asl r5
	add	fp, r2, #3
	add	r5, r1, #24
	bne	.L675
	mov	r3, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r3
	b	.L149
.L352:
	ldr	r2, [r6, #16]
.L125:
	ands	r3, r2, #2048
	beq	.L206
	cmp	r9, #0
	beq	.L787
	mov	sl, #0
.L208:
	ldr	r2, [r6, #32]
	cmp	r2, #0
	ldrb	r3, [fp, sl]	@ zero_extendqisi2
	add	sl, sl, #1
	beq	.L207
	ldr	r1, [r2, #28]
	cmp	r1, #0
	beq	.L207
	ldr	r0, [r2, #32]
	ldr	r2, [r6, #64]
	cmp	r2, r0
	addcc	r0, r2, #1
	strccb	r3, [r1, r2]
	strcc	r0, [r6, #64]
.L207:
	cmp	r3, #0
	cmpne	sl, r9
	bcc	.L208
	ldr	r1, [r6, #16]
	tst	r1, #512
	bne	.L968
.L209:
	cmp	r3, #0
	add	fp, fp, sl
	rsb	r9, sl, r9
	bne	.L788
.L938:
	ldr	r2, [r6, #16]
.L210:
	mov	sl, #7
	mov	r3, #0
	str	sl, [r6, #0]
	str	r3, [r6, #64]
	b	.L126
.L1044:
	.align	2
.L1043:
	.word	.LANCHOR0+2048
	.word	.LANCHOR0
	.word	.LANCHOR0+2176
	.word	.LANCHOR0+2214
	.word	.LC14
	.word	.LC16
	.word	.LC4
.L351:
	ldr	r2, [r6, #16]
.L124:
	tst	r2, #1024
	beq	.L200
	ldr	r1, [r6, #64]
	cmp	r9, r1
	movcc	sl, r9
	movcs	sl, r1
	cmp	sl, #0
	beq	.L201
	ldr	r3, [r6, #32]
	cmp	r3, #0
	beq	.L202
	ldr	r0, [r3, #16]
	cmp	r0, #0
	beq	.L202
	ldr	r2, [r3, #20]
	rsb	lr, r1, r2
	ldr	r2, [r3, #24]
	add	r3, sl, lr
	cmp	r3, r2
	movls	r2, sl
	rsbhi	r2, lr, r2
	add	r0, r0, lr
	mov	r1, fp
	str	ip, [sp, #16]
	bl	memcpy
	ldr	r2, [r6, #16]
	ldr	ip, [sp, #16]
.L202:
	tst	r2, #512
	beq	.L205
	ldr	r0, [r6, #24]
	mov	r1, fp
	mov	r2, sl
	str	ip, [sp, #16]
	bl	crc32
	str	r0, [r6, #24]
	ldr	ip, [sp, #16]
.L205:
	ldr	r0, [r6, #64]
	rsb	r1, sl, r0
	str	r1, [r6, #64]
	rsb	r9, sl, r9
	add	fp, fp, sl
.L201:
	cmp	r1, #0
	bne	.L786
	ldr	r2, [r6, #16]
.L200:
	mov	r1, #6
	mov	sl, #0
	str	r1, [r6, #0]
	str	sl, [r6, #64]
	b	.L125
.L350:
	ldr	r2, [r6, #16]
.L123:
	ands	r1, r2, #1024
	beq	.L193
	cmp	r5, #15
	bhi	.L194
	cmp	r9, #0
	beq	.L969
	rsb	r3, r5, #15
	mov	r1, r3, lsr #3
	ands	r3, r1, #3
	beq	.L813
	ldrb	sl, [fp], #1	@ zero_extendqisi2
	add	r8, r8, sl, asl r5
	add	r5, r5, #8
	cmp	r5, #15
	sub	r9, r9, #1
	bhi	.L194
	cmp	r9, #0
	beq	.L810
	cmp	r3, #1
	beq	.L813
	cmp	r3, #2
	beq	.L679
	ldrb	r3, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r3, asl r5
	add	r5, r5, #8
	beq	.L812
.L679:
	ldrb	r1, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r1, asl r5
	add	r5, r5, #8
	beq	.L970
.L813:
	mov	sl, r4
.L658:
	ldrb	r4, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r4, asl r5
	add	r5, r5, #8
	sub	r9, r9, #1
	cmp	r5, #15
	mov	r1, fp
	mov	r3, r9
	mov	r0, r5
	bhi	.L971
	cmp	r9, #0
	beq	.L972
	ldrb	r4, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r4, asl r5
	add	r5, r5, #8
	beq	.L814
	ldrb	r4, [fp, #0]	@ zero_extendqisi2
	subs	r9, r3, #2
	add	r8, r8, r4, asl r5
	add	fp, r1, #2
	add	r5, r0, #16
	beq	.L815
	ldrb	fp, [r1, #2]	@ zero_extendqisi2
	subs	r9, r3, #3
	add	r8, r8, fp, asl r5
	add	fp, r1, #3
	add	r5, r0, #24
	bne	.L658
	mov	r4, r7
	mov	r7, ip
	mov	ip, sl
	ldr	sl, [sp, #24]
	b	.L149
.L122:
	cmp	r5, #15
	bhi	.L188
.L187:
	cmp	r9, #0
	beq	.L973
	rsb	sl, r5, #15
	mov	r3, sl, lsr #3
	ands	r3, r3, #3
	beq	.L657
	ldrb	r2, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r2, asl r5
	add	r5, r5, #8
	cmp	r5, #15
	sub	r9, r9, #1
	bhi	.L188
	cmp	r9, #0
	beq	.L816
	cmp	r3, #1
	beq	.L657
	cmp	r3, #2
	beq	.L680
	ldrb	r2, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r2, asl r5
	add	r5, r5, #8
	beq	.L818
.L680:
	ldrb	sl, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, sl, asl r5
	add	r5, r5, #8
	bne	.L657
	b	.L1038
.L190:
	cmp	r9, #0
	beq	.L975
	ldrb	r0, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r0, asl r5
	add	r5, r5, #8
	beq	.L820
	ldrb	sl, [fp, #0]	@ zero_extendqisi2
	subs	r9, r3, #2
	add	r8, r8, sl, asl r5
	add	fp, r2, #2
	add	r5, r1, #16
	beq	.L821
	ldrb	fp, [r2, #2]	@ zero_extendqisi2
	subs	r9, r3, #3
	add	r8, r8, fp, asl r5
	add	fp, r2, #3
	add	r5, r1, #24
	beq	.L976
.L657:
	ldrb	r2, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r2, asl r5
	add	r5, r5, #8
	sub	r9, r9, #1
	cmp	r5, #15
	mov	r2, fp
	mov	r3, r9
	mov	r1, r5
	bls	.L190
.L188:
	ldr	r5, [r6, #32]
	cmp	r5, #0
	andne	r2, r8, #255
	strne	r2, [r5, #8]
	ldrne	r5, [r6, #32]
	movne	r2, r8, lsr #8
	strne	r2, [r5, #12]
	ldr	r2, [r6, #16]
	tst	r2, #512
	bne	.L977
.L192:
	mov	r8, #4
	mov	r5, #0
	str	r8, [r6, #0]
	mov	r8, r5
	b	.L123
.L142:
	ldr	r0, [sp, #36]
	cmp	r0, #0
	beq	.L795
	ldr	r2, [r6, #64]
	mov	r3, #18
	sub	r0, r0, #1
	strb	r2, [r4], #1
	str	r0, [sp, #36]
	str	r3, [r6, #0]
	b	.L118
.L145:
	mov	r3, ip
	mov	sl, #1
	mov	ip, r4
	mov	r4, r7
	mov	r7, r3
	b	.L149
.L137:
	mov	r1, #256
	ldr	r2, [sp, #36]
	add	r3, r1, #1
	cmp	r9, #5
	cmphi	r2, r3
	bhi	.L278
	ldr	r2, [r6, #84]
	mvn	r1, #0
	mvn	r3, r1, asl r2
	str	r3, [sp, #40]
	ldr	r2, [r6, #76]
	and	r0, r8, r3
	add	sl, r2, r0, asl #2
	ldrb	r3, [sl, #1]	@ zero_extendqisi2
	cmp	r5, r3
	ldrb	r1, [r2, r0, asl #2]	@ zero_extendqisi2
	ldrh	r0, [sl, #2]
	bcs	.L279
	cmp	r9, #0
	beq	.L978
	ldrb	sl, [fp], #1	@ zero_extendqisi2
	add	r8, r8, sl, asl r5
	ldr	r3, [sp, #40]
	and	r1, r8, r3
	add	r0, r2, r1, asl #2
	str	r0, [sp, #32]
	sub	sl, r9, #1
	ldrb	r3, [r0, #1]	@ zero_extendqisi2
	and	r0, sl, #3
	add	r5, r5, #8
	mov	r9, sl
	ldrb	r1, [r2, r1, asl #2]	@ zero_extendqisi2
	ldr	sl, [sp, #32]
	str	r0, [sp, #28]
	cmp	r3, r5
	ldrh	r0, [sl, #2]
	bls	.L279
	cmp	r9, #0
	beq	.L879
	ldr	sl, [sp, #28]
	cmp	sl, #0
	beq	.L881
	cmp	sl, #1
	beq	.L703
	cmp	sl, #2
	beq	.L704
	ldrb	r1, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r1, asl r5
	ldr	r3, [sp, #40]
	and	sl, r8, r3
	add	r0, r2, sl, asl #2
	ldrb	r3, [r0, #1]	@ zero_extendqisi2
	add	r5, r5, #8
	cmp	r3, r5
	sub	r9, r9, #1
	ldrb	r1, [r2, sl, asl #2]	@ zero_extendqisi2
	ldrh	r0, [r0, #2]
	bls	.L279
.L704:
	ldrb	r1, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r1, asl r5
	ldr	r3, [sp, #40]
	and	sl, r8, r3
	add	r0, r2, sl, asl #2
	ldrb	r3, [r0, #1]	@ zero_extendqisi2
	add	r5, r5, #8
	cmp	r3, r5
	sub	r9, r9, #1
	ldrb	r1, [r2, sl, asl #2]	@ zero_extendqisi2
	ldrh	r0, [r0, #2]
	bls	.L279
.L703:
	ldrb	r3, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r3, asl r5
	ldr	sl, [sp, #40]
	and	r1, r8, sl
	add	r0, r2, r1, asl #2
	ldrb	r3, [r0, #1]	@ zero_extendqisi2
	add	r5, r5, #8
	cmp	r3, r5
	sub	r9, r9, #1
	ldrb	r1, [r2, r1, asl #2]	@ zero_extendqisi2
	ldrh	r0, [r0, #2]
	bls	.L279
	cmp	r9, #0
	beq	.L979
.L881:
	str	r4, [sp, #28]
	str	r6, [sp, #32]
	ldr	sl, [sp, #40]
	str	r7, [sp, #40]
	mov	r7, ip
.L702:
	ldrb	ip, [fp], #1	@ zero_extendqisi2
	add	r8, r8, ip, asl r5
	and	r6, r8, sl
	add	r0, r2, r6, asl #2
	ldrb	r3, [r0, #1]	@ zero_extendqisi2
	add	r5, r5, #8
	sub	r9, r9, #1
	cmp	r3, r5
	ldrb	r1, [r2, r6, asl #2]	@ zero_extendqisi2
	mov	ip, fp
	mov	r4, r9
	mov	r6, r5
	ldrh	r0, [r0, #2]
	bls	.L883
	ldrb	r3, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r3, asl r5
	and	r1, r8, sl
	add	r0, r2, r1, asl #2
	ldrb	r3, [r0, #1]	@ zero_extendqisi2
	add	r5, r5, #8
	cmp	r3, r5
	sub	r9, r9, #1
	ldrb	r1, [r2, r1, asl #2]	@ zero_extendqisi2
	ldrh	r0, [r0, #2]
	bls	.L883
	ldrb	r1, [fp, #0]	@ zero_extendqisi2
	add	r8, r8, r1, asl r5
	and	r9, r8, sl
	add	r0, r2, r9, asl #2
	ldrb	r3, [r0, #1]	@ zero_extendqisi2
	add	r5, r6, #16
	cmp	r3, r5
	ldrb	r1, [r2, r9, asl #2]	@ zero_extendqisi2
	add	fp, ip, #2
	sub	r9, r4, #2
	ldrh	r0, [r0, #2]
	bls	.L883
	ldrb	fp, [ip, #2]	@ zero_extendqisi2
	add	r8, r8, fp, asl r5
	and	r1, r8, sl
	add	r0, r2, r1, asl #2
	ldrb	r3, [r0, #1]	@ zero_extendqisi2
	add	r5, r6, #24
	cmp	r3, r5
	sub	r9, r4, #3
	add	fp, ip, #3
	ldrb	r1, [r2, r1, asl #2]	@ zero_extendqisi2
	ldrh	r0, [r0, #2]
	bls	.L883
	cmp	r9, #0
	bne	.L702
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r6, [sp, #32]
	ldr	r4, [sp, #40]
	b	.L149
.L136:
	ldr	sl, [r6, #104]
.L153:
	ldr	r2, [r6, #96]
	str	r2, [sp, #80]
	ldr	r1, [r6, #100]
	str	r4, [sp, #28]
	str	r1, [sp, #84]
	str	r7, [sp, #32]
	str	ip, [sp, #40]
	mov	r2, r1
.L246:
	ldr	r7, [sp, #80]
	add	r2, r2, r7
	cmp	r2, sl
	str	r2, [sp, #72]
	bls	.L273
	ldr	r1, [r6, #84]
	mov	r4, #1
	mov	r0, r4, asl r1
	ldr	r1, [r6, #76]
	sub	r0, r0, #1
	and	r3, r8, r0
	add	r2, r1, r3, asl #2
	ldrb	r3, [r2, #1]	@ zero_extendqisi2
	cmp	r5, r3
	ldrh	r2, [r2, #2]
	bcs	.L274
	cmp	r9, #0
	beq	.L980
	ldrb	r3, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r3, asl r5
	and	ip, r8, r0
	add	r7, r1, ip, asl #2
	ldrb	r3, [r7, #1]	@ zero_extendqisi2
	add	r5, r5, #8
	sub	r4, r9, #1
	cmp	r3, r5
	mov	r9, r4
	and	ip, r4, #3
	ldrh	r2, [r7, #2]
	bls	.L274
	cmp	r4, #0
	beq	.L912
	cmp	ip, #0
	beq	.L713
	cmp	ip, #1
	beq	.L714
	cmp	ip, #2
	beq	.L715
	ldrb	ip, [fp], #1	@ zero_extendqisi2
	add	r8, r8, ip, asl r5
	and	r3, r8, r0
	add	r2, r1, r3, asl #2
	ldrb	r3, [r2, #1]	@ zero_extendqisi2
	add	r5, r5, #8
	cmp	r3, r5
	sub	r9, r4, #1
	ldrh	r2, [r2, #2]
	bls	.L274
.L715:
	ldrb	r2, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r2, asl r5
	and	r7, r8, r0
	add	r4, r1, r7, asl #2
	ldrb	r3, [r4, #1]	@ zero_extendqisi2
	add	r5, r5, #8
	cmp	r3, r5
	sub	r9, r9, #1
	ldrh	r2, [r4, #2]
	bls	.L274
.L714:
	ldrb	r4, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r4, asl r5
	and	r3, r8, r0
	add	ip, r1, r3, asl #2
	ldrb	r3, [ip, #1]	@ zero_extendqisi2
	add	r5, r5, #8
	cmp	r3, r5
	sub	r9, r9, #1
	ldrh	r2, [ip, #2]
	bls	.L274
	cmp	r9, #0
	beq	.L981
.L713:
	ldrb	r4, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r4, asl r5
	and	r7, r8, r0
	add	r2, r1, r7, asl #2
	ldrb	r3, [r2, #1]	@ zero_extendqisi2
	add	r5, r5, #8
	sub	r9, r9, #1
	cmp	r3, r5
	mov	ip, fp
	mov	r4, r9
	mov	r7, r5
	ldrh	r2, [r2, #2]
	bls	.L274
	ldrb	r2, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r2, asl r5
	and	r3, r8, r0
	add	r2, r1, r3, asl #2
	ldrb	r3, [r2, #1]	@ zero_extendqisi2
	add	r5, r5, #8
	cmp	r3, r5
	sub	r9, r9, #1
	ldrh	r2, [r2, #2]
	bls	.L274
	ldrb	r9, [fp, #0]	@ zero_extendqisi2
	add	r8, r8, r9, asl r5
	and	r5, r8, r0
	add	r2, r1, r5, asl #2
	ldrb	r3, [r2, #1]	@ zero_extendqisi2
	add	r5, r7, #16
	cmp	r3, r5
	sub	r9, r4, #2
	add	fp, ip, #2
	ldrh	r2, [r2, #2]
	bls	.L274
	ldrb	r3, [ip, #2]	@ zero_extendqisi2
	add	r8, r8, r3, asl r5
	and	fp, r8, r0
	add	r2, r1, fp, asl #2
	ldrb	r3, [r2, #1]	@ zero_extendqisi2
	add	r5, r7, #24
	cmp	r3, r5
	sub	r9, r4, #3
	add	fp, ip, #3
	ldrh	r2, [r2, #2]
	bls	.L274
	cmp	r9, #0
	bne	.L713
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L138:
	ldr	r1, [r6, #72]
.L152:
	cmp	r1, #0
	beq	.L291
	cmp	r5, r1
	bcs	.L292
	cmp	r9, #0
	beq	.L982
	mvn	r0, r5
	add	sl, r0, r1
	mov	r3, sl, lsr #3
	ands	r3, r3, #3
	beq	.L871
	ldrb	sl, [fp], #1	@ zero_extendqisi2
	add	r8, r8, sl, asl r5
	add	r5, r5, #8
	cmp	r5, r1
	sub	r9, r9, #1
	bcs	.L292
	cmp	r9, #0
	beq	.L868
	cmp	r3, #1
	beq	.L871
	cmp	r3, #2
	beq	.L698
	ldrb	r2, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r2, asl r5
	add	r5, r5, #8
	beq	.L870
.L698:
	ldrb	r0, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r0, asl r5
	add	r5, r5, #8
	beq	.L983
.L871:
	mov	sl, r4
.L670:
	ldrb	r4, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r4, asl r5
	add	r5, r5, #8
	sub	r9, r9, #1
	cmp	r5, r1
	mov	r2, fp
	mov	r3, r9
	mov	r0, r5
	bcs	.L984
	cmp	r9, #0
	beq	.L985
	ldrb	r4, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r4, asl r5
	add	r5, r5, #8
	beq	.L872
	ldrb	r4, [fp, #0]	@ zero_extendqisi2
	subs	r9, r3, #2
	add	r8, r8, r4, asl r5
	add	fp, r2, #2
	add	r5, r0, #16
	beq	.L873
	ldrb	fp, [r2, #2]	@ zero_extendqisi2
	subs	r9, r3, #3
	add	r8, r8, fp, asl r5
	add	fp, r2, #3
	add	r5, r0, #24
	bne	.L670
	mov	r4, r7
	mov	r7, ip
	mov	ip, sl
	ldr	sl, [sp, #24]
	b	.L149
.L146:
	mov	r1, ip
	mvn	sl, #2
	mov	ip, r4
	mov	r4, r7
	mov	r7, r1
	b	.L149
.L119:
	ldr	sl, [r6, #8]
	cmp	sl, #0
	beq	.L163
	cmp	r5, #15
	bhi	.L164
	cmp	r9, #0
	beq	.L986
	rsb	r1, r5, #15
	mov	r2, r1, lsr #3
	ands	r3, r2, #3
	beq	.L654
	ldrb	r2, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r2, asl r5
	add	r5, r5, #8
	cmp	r5, #15
	sub	r9, r9, #1
	bhi	.L164
	cmp	r9, #0
	beq	.L798
	cmp	r3, #1
	beq	.L654
	cmp	r3, #2
	beq	.L677
	ldrb	r3, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r3, asl r5
	add	r5, r5, #8
	beq	.L800
.L677:
	ldrb	r1, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r1, asl r5
	add	r5, r5, #8
	beq	.L987
.L654:
	ldrb	r3, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r3, asl r5
	add	r5, r5, #8
	sub	r9, r9, #1
	cmp	r5, #15
	mov	r2, fp
	mov	r3, r9
	mov	r1, r5
	bhi	.L164
	cmp	r9, #0
	beq	.L988
	ldrb	r0, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r0, asl r5
	add	r5, r5, #8
	beq	.L802
	ldrb	r0, [fp, #0]	@ zero_extendqisi2
	subs	r9, r3, #2
	add	r8, r8, r0, asl r5
	add	fp, r2, #2
	add	r5, r1, #16
	beq	.L803
	ldrb	r0, [r2, #2]	@ zero_extendqisi2
	subs	r9, r3, #3
	add	r8, r8, r0, asl r5
	add	fp, r2, #3
	add	r5, r1, #24
	bne	.L654
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L355:
	ldr	r3, [r6, #8]
.L144:
	cmp	r3, #0
	beq	.L797
	ldr	sl, [r6, #16]
	cmp	sl, #0
	beq	.L797
	cmp	r5, #31
	bhi	.L332
	cmp	r9, #0
	beq	.L989
	rsb	sl, r5, #31
	mov	r3, sl, lsr #3
	ands	r3, r3, #3
	beq	.L676
	ldrb	r2, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r2, asl r5
	add	r5, r5, #8
	cmp	r5, #31
	sub	r9, r9, #1
	bhi	.L332
	cmp	r9, #0
	beq	.L925
	cmp	r3, #1
	beq	.L676
	cmp	r3, #2
	beq	.L719
	ldrb	r2, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r2, asl r5
	add	r5, r5, #8
	beq	.L927
.L719:
	ldrb	r1, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r1, asl r5
	add	r5, r5, #8
	bne	.L676
	b	.L1039
.L334:
	cmp	r9, #0
	beq	.L991
	ldrb	sl, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, sl, asl r5
	add	r5, r5, #8
	beq	.L929
	ldrb	r0, [fp, #0]	@ zero_extendqisi2
	subs	r9, r3, #2
	add	r8, r8, r0, asl r5
	add	fp, r2, #2
	add	r5, r1, #16
	beq	.L930
	ldrb	r0, [r2, #2]	@ zero_extendqisi2
	subs	r9, r3, #3
	add	r8, r8, r0, asl r5
	add	fp, r2, #3
	add	r5, r1, #24
	beq	.L992
.L676:
	ldrb	r2, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r2, asl r5
	add	r5, r5, #8
	sub	r9, r9, #1
	cmp	r5, #31
	mov	r2, fp
	mov	r3, r9
	mov	r1, r5
	bls	.L334
.L332:
	ldr	r1, [r6, #28]
	cmp	r1, r8
	beq	.L335
	ldr	r3, .L1043+20
	mov	sl, #27
	str	r3, [r7, #24]
	str	sl, [r6, #0]
	mov	r3, sl
	b	.L118
.L121:
	cmp	r5, #31
	bhi	.L182
.L181:
	cmp	r9, #0
	beq	.L993
	rsb	r2, r5, #31
	mov	sl, r2, lsr #3
	ands	r3, sl, #3
	beq	.L656
	ldrb	r2, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r2, asl r5
	add	r5, r5, #8
	cmp	r5, #31
	sub	r9, r9, #1
	bhi	.L182
	cmp	r9, #0
	beq	.L822
	cmp	r3, #1
	beq	.L656
	cmp	r3, #2
	beq	.L681
	ldrb	r3, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r3, asl r5
	add	r5, r5, #8
	beq	.L824
.L681:
	ldrb	r0, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r0, asl r5
	add	r5, r5, #8
	bne	.L656
	b	.L1040
.L184:
	cmp	r9, #0
	beq	.L995
	ldrb	sl, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, sl, asl r5
	add	r5, r5, #8
	beq	.L826
	ldrb	r0, [fp, #0]	@ zero_extendqisi2
	subs	r9, r3, #2
	add	r8, r8, r0, asl r5
	add	fp, r2, #2
	add	r5, r1, #16
	beq	.L827
	ldrb	fp, [r2, #2]	@ zero_extendqisi2
	subs	r9, r3, #3
	add	r8, r8, fp, asl r5
	add	fp, r2, #3
	add	r5, r1, #24
	beq	.L996
.L656:
	ldrb	r0, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r0, asl r5
	add	r5, r5, #8
	sub	r9, r9, #1
	cmp	r5, #31
	mov	r2, fp
	mov	r3, r9
	mov	r1, r5
	bls	.L184
.L182:
	ldr	r1, [r6, #32]
	cmp	r1, #0
	strne	r8, [r1, #4]
	ldr	r5, [r6, #16]
	tst	r5, #512
	bne	.L997
.L186:
	mov	r8, #0
	mov	r2, #3
	str	r2, [r6, #0]
	mov	r5, r8
	b	.L187
.L120:
	cmp	r5, #15
	bhi	.L161
	cmp	r9, #0
	beq	.L998
	rsb	sl, r5, #15
	mov	r3, sl, lsr #3
	ands	r3, r3, #3
	beq	.L655
	ldrb	r2, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r2, asl r5
	add	r5, r5, #8
	cmp	r5, #15
	sub	r9, r9, #1
	bhi	.L161
	cmp	r9, #0
	beq	.L828
	cmp	r3, #1
	beq	.L655
	cmp	r3, #2
	beq	.L682
	ldrb	r3, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r3, asl r5
	add	r5, r5, #8
	beq	.L830
.L682:
	ldrb	r1, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r1, asl r5
	add	r5, r5, #8
	bne	.L655
	b	.L1041
.L176:
	cmp	r9, #0
	beq	.L1000
	ldrb	sl, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, sl, asl r5
	add	r5, r5, #8
	beq	.L832
	ldrb	r0, [fp, #0]	@ zero_extendqisi2
	subs	r9, r3, #2
	add	r8, r8, r0, asl r5
	add	fp, r2, #2
	add	r5, r1, #16
	beq	.L833
	ldrb	r0, [r2, #2]	@ zero_extendqisi2
	subs	r9, r3, #3
	add	r8, r8, r0, asl r5
	add	fp, r2, #3
	add	r5, r1, #24
	beq	.L1001
.L655:
	ldrb	r2, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r2, asl r5
	add	r5, r5, #8
	sub	r9, r9, #1
	cmp	r5, #15
	mov	r2, fp
	mov	r3, r9
	mov	r1, r5
	bls	.L176
.L161:
	and	r1, r8, #255
	cmp	r1, #8
	str	r8, [r6, #16]
	mov	r3, r8
	bne	.L1002
	tst	r8, #57344
	bne	.L1003
	ldr	r0, [r6, #32]
	cmp	r0, #0
	movne	r3, r8, lsr #8
	andne	r3, r3, #1
	strne	r3, [r0, #0]
	ldrne	r3, [r6, #16]
	tst	r3, #512
	bne	.L1004
.L180:
	mov	r5, #2
	mov	r8, #0
	str	r5, [r6, #0]
	mov	r5, r8
	b	.L181
.L134:
	cmp	r5, #13
	bhi	.L157
	cmp	r9, #0
	beq	.L1005
	rsb	r3, r5, #13
	mov	sl, r3, lsr #3
	ands	r3, sl, #3
	beq	.L662
	ldrb	r0, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r0, asl r5
	add	r5, r5, #8
	cmp	r5, #13
	sub	r9, r9, #1
	bhi	.L157
	cmp	r9, #0
	beq	.L919
	cmp	r3, #1
	beq	.L662
	cmp	r3, #2
	beq	.L718
	ldrb	r3, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r3, asl r5
	add	r5, r5, #8
	beq	.L921
.L718:
	ldrb	r2, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r2, asl r5
	add	r5, r5, #8
	bne	.L662
	b	.L1042
.L237:
	cmp	r9, #0
	beq	.L1007
	ldrb	sl, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, sl, asl r5
	add	r5, r5, #8
	beq	.L923
	ldrb	r0, [fp, #0]	@ zero_extendqisi2
	subs	r9, r3, #2
	add	r8, r8, r0, asl r5
	add	fp, r2, #2
	add	r5, r1, #16
	beq	.L924
	ldrb	fp, [r2, #2]	@ zero_extendqisi2
	subs	r9, r3, #3
	add	r8, r8, fp, asl r5
	add	fp, r2, #3
	add	r5, r1, #24
	beq	.L1008
.L662:
	ldrb	r2, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r2, asl r5
	add	r5, r5, #8
	sub	r9, r9, #1
	cmp	r5, #13
	mov	r2, fp
	mov	r3, r9
	mov	r1, r5
	bls	.L237
.L157:
	and	sl, r8, #31
	mov	r1, r8, lsr #5
	mov	r0, r8, lsr #10
	add	r3, sl, #256
	mov	r2, #284
	add	sl, r3, #1
	and	r1, r1, #31
	and	r0, r0, #15
	add	r2, r2, #2
	add	r1, r1, #1
	add	r0, r0, #4
	cmp	sl, r2
	str	r0, [r6, #92]
	str	sl, [r6, #96]
	str	r1, [r6, #100]
	mov	r8, r8, lsr #14
	sub	r5, r5, #14
	bhi	.L238
	cmp	r1, #30
	bhi	.L238
	mov	r2, #0
	mov	r0, #16
	str	r0, [r6, #0]
	str	r2, [r6, #104]
	mov	r3, r2
	b	.L154
.L952:
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
	b	.L149
.L790:
	mov	r3, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r3
	b	.L149
.L788:
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L341:
	bl	adler32
	b	.L342
.L942:
	ldr	r3, [r4, #4]
	cmp	r3, #0
	bne	.L115
	b	.L116
.L953:
	mov	r2, sl
.L217:
	ldrh	r0, [r6, #24]
	cmp	r0, r8
	beq	.L220
	ldr	sl, .L1043+24
	mov	r0, #27
	str	sl, [r7, #24]
	mov	r3, r0
	str	r0, [r6, #0]
	b	.L118
.L211:
	ldr	r3, [r6, #32]
	cmp	r3, #0
	beq	.L215
	str	r1, [r3, #36]
	b	.L939
.L206:
	ldr	r1, [r6, #32]
	cmp	r1, #0
	beq	.L210
	str	r3, [r1, #28]
	b	.L938
.L310:
	ldr	r1, [r6, #64]
	rsb	r3, r3, r4
	str	r1, [sp, #28]
	b	.L313
.L867:
	mov	ip, r7
	add	r4, sp, #28
	ldmia	r4, {r4, r6}	@ phole ldm
	ldr	r7, [sp, #40]
.L295:
	tst	r1, #240
	mov	sl, r3
	strne	r3, [sp, #28]
	movne	r2, r3
	bne	.L299
	add	sl, r3, r1
	mvn	r1, #0
	mvn	r1, r1, asl sl
	and	sl, r1, r8
	str	r0, [sp, #40]
	add	r0, r0, sl, lsr r3
	str	r1, [sp, #32]
	add	r1, r2, r0, asl #2
	str	r1, [sp, #28]
	ldrb	sl, [r1, #1]	@ zero_extendqisi2
	str	sl, [sp, #84]
	add	r1, r3, sl
	ldr	sl, [sp, #28]
	cmp	r5, r1
	ldrb	r1, [r2, r0, asl #2]	@ zero_extendqisi2
	ldrh	r0, [sl, #2]
	ldr	sl, [sp, #84]
	str	sl, [sp, #28]
	bcs	.L300
	cmp	r9, #0
	beq	.L1009
	ldrb	r1, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r1, asl r5
	ldr	r0, [sp, #32]
	ldr	sl, [sp, #40]
	and	r1, r8, r0
	add	r0, sl, r1, lsr r3
	add	r1, r2, r0, asl #2
	str	r1, [sp, #80]
	ldrb	sl, [r1, #1]	@ zero_extendqisi2
	str	sl, [sp, #84]
	add	r1, r3, sl
	add	r5, r5, #8
	sub	sl, r9, #1
	cmp	r1, r5
	ldrb	r1, [r2, r0, asl #2]	@ zero_extendqisi2
	and	r0, sl, #3
	mov	r9, sl
	str	r0, [sp, #72]
	ldr	sl, [sp, #80]
	ldrh	r0, [sl, #2]
	ldr	sl, [sp, #84]
	str	sl, [sp, #28]
	bls	.L300
	cmp	r9, #0
	beq	.L858
	ldr	r0, [sp, #72]
	cmp	r0, #0
	beq	.L860
	cmp	r0, #1
	beq	.L693
	cmp	r0, #2
	beq	.L694
	ldrb	r1, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r1, asl r5
	ldr	r0, [sp, #32]
	ldr	sl, [sp, #40]
	and	r1, r8, r0
	add	r0, sl, r1, lsr r3
	add	r1, r2, r0, asl #2
	str	r1, [sp, #28]
	ldrb	sl, [r1, #1]	@ zero_extendqisi2
	str	sl, [sp, #84]
	add	r1, r3, sl
	ldr	sl, [sp, #28]
	add	r5, r5, #8
	cmp	r1, r5
	ldrb	r1, [r2, r0, asl #2]	@ zero_extendqisi2
	ldrh	r0, [sl, #2]
	ldr	sl, [sp, #84]
	sub	r9, r9, #1
	str	sl, [sp, #28]
	bls	.L300
.L694:
	ldrb	r1, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r1, asl r5
	ldr	r0, [sp, #32]
	ldr	sl, [sp, #40]
	and	r1, r8, r0
	add	r0, sl, r1, lsr r3
	add	r1, r2, r0, asl #2
	str	r1, [sp, #28]
	ldrb	sl, [r1, #1]	@ zero_extendqisi2
	str	sl, [sp, #84]
	add	r1, r3, sl
	ldr	sl, [sp, #28]
	add	r5, r5, #8
	cmp	r1, r5
	ldrb	r1, [r2, r0, asl #2]	@ zero_extendqisi2
	ldrh	r0, [sl, #2]
	ldr	sl, [sp, #84]
	sub	r9, r9, #1
	str	sl, [sp, #28]
	bls	.L300
.L693:
	ldrb	r1, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r1, asl r5
	ldr	r0, [sp, #32]
	ldr	sl, [sp, #40]
	and	r1, r8, r0
	add	r0, sl, r1, lsr r3
	add	r1, r2, r0, asl #2
	str	r1, [sp, #28]
	ldrb	sl, [r1, #1]	@ zero_extendqisi2
	str	sl, [sp, #84]
	add	r1, r3, sl
	ldr	sl, [sp, #28]
	add	r5, r5, #8
	cmp	r1, r5
	ldrb	r1, [r2, r0, asl #2]	@ zero_extendqisi2
	ldrh	r0, [sl, #2]
	ldr	sl, [sp, #84]
	sub	r9, r9, #1
	str	sl, [sp, #28]
	bls	.L300
	cmp	r9, #0
	beq	.L1010
.L860:
	str	r4, [sp, #72]
	b	.L692
.L302:
	ldrb	sl, [fp], #1	@ zero_extendqisi2
	add	r8, r8, sl, asl r5
	ldr	r1, [sp, #32]
	ldr	r4, [sp, #40]
	and	r0, r8, r1
	add	r0, r4, r0, lsr r3
	add	sl, r2, r0, asl #2
	str	sl, [sp, #80]
	ldrb	sl, [sl, #1]	@ zero_extendqisi2
	add	r5, r5, #8
	add	r1, r3, sl
	cmp	r1, r5
	ldr	r4, [sp, #80]
	ldrb	r1, [r2, r0, asl #2]	@ zero_extendqisi2
	str	sl, [sp, #28]
	sub	r9, r9, #1
	ldrh	r0, [r4, #2]
	bls	.L862
	ldrb	r9, [fp, #0]	@ zero_extendqisi2
	add	r8, r8, r9, asl r5
	ldr	fp, [sp, #32]
	ldr	r1, [sp, #40]
	and	sl, r8, fp
	add	r9, r1, sl, lsr r3
	add	r5, r2, r9, asl #2
	str	r5, [sp, #80]
	ldr	r0, [sp, #88]
	ldrb	sl, [r5, #1]	@ zero_extendqisi2
	add	r5, r0, #16
	add	r4, r3, sl
	ldrb	r1, [r2, r9, asl #2]	@ zero_extendqisi2
	ldr	fp, [sp, #84]
	ldr	r0, [sp, #92]
	cmp	r4, r5
	str	sl, [sp, #28]
	ldr	r4, [sp, #80]
	sub	r9, fp, #2
	add	fp, r0, #2
	ldrh	r0, [r4, #2]
	bls	.L862
	ldr	r9, [sp, #92]
	ldrb	fp, [r9, #2]	@ zero_extendqisi2
	add	r8, r8, fp, asl r5
	ldr	r0, [sp, #32]
	ldr	r1, [sp, #40]
	and	sl, r8, r0
	add	r0, r1, sl, lsr r3
	add	r5, r2, r0, asl #2
	str	r5, [sp, #28]
	ldr	r4, [sp, #88]
	ldrb	sl, [r5, #1]	@ zero_extendqisi2
	ldr	fp, [sp, #84]
	add	r5, r4, #24
	add	r9, r3, sl
	ldr	r1, [sp, #92]
	ldr	r4, [sp, #28]
	cmp	r9, r5
	sub	r9, fp, #3
	add	fp, r1, #3
	ldrb	r1, [r2, r0, asl #2]	@ zero_extendqisi2
	ldrh	r0, [r4, #2]
	str	sl, [sp, #28]
	bls	.L862
	cmp	r9, #0
	beq	.L1011
.L692:
	ldrb	sl, [fp], #1	@ zero_extendqisi2
	add	r8, r8, sl, asl r5
	ldr	r1, [sp, #32]
	ldr	r4, [sp, #40]
	and	r0, r8, r1
	add	r0, r4, r0, lsr r3
	add	sl, r2, r0, asl #2
	str	sl, [sp, #80]
	ldrb	sl, [sl, #1]	@ zero_extendqisi2
	add	r5, r5, #8
	add	r1, r3, sl
	sub	r9, r9, #1
	cmp	r1, r5
	ldr	r4, [sp, #80]
	ldrb	r1, [r2, r0, asl #2]	@ zero_extendqisi2
	str	fp, [sp, #92]
	str	r9, [sp, #84]
	str	r5, [sp, #88]
	str	sl, [sp, #28]
	ldrh	r0, [r4, #2]
	bhi	.L302
.L862:
	str	sl, [sp, #84]
	ldr	r4, [sp, #72]
.L300:
	mov	r8, r8, lsr r3
	ldr	sl, [sp, #84]
	ldr	r2, [sp, #28]
	rsb	r5, r3, r5
.L299:
	tst	r1, #64
	mov	r8, r8, lsr r2
	rsb	r5, sl, r5
	bne	.L1012
	and	r1, r1, #15
	str	r0, [r6, #68]
	mov	r0, #21
	str	r0, [r6, #0]
	str	r1, [r6, #72]
	b	.L151
.L220:
	mov	r5, #0
	mov	r8, r5
.L216:
	ldr	r3, [r6, #32]
	cmp	r3, #0
	beq	.L221
	mov	r0, r2, asr #9
	and	r1, r0, #1
	str	r1, [r3, #44]
	ldr	r2, [r6, #32]
	mov	lr, #1
	str	lr, [r2, #48]
.L221:
	mov	r0, #0
	mov	r2, r0
	mov	r1, r0
	str	ip, [sp, #16]
	bl	crc32
	mov	r2, #11
	str	r0, [r6, #24]
	str	r0, [r7, #48]
	str	r2, [r6, #0]
	mov	r3, r2
	ldr	ip, [sp, #16]
	b	.L118
.L193:
	ldr	r3, [r6, #32]
	cmp	r3, #0
	strne	r1, [r3, #16]
	ldrne	r2, [r6, #16]
.L199:
	mov	r1, #5
	str	r1, [r6, #0]
	b	.L124
.L274:
	cmp	r2, #15
	bhi	.L249
	cmp	r5, r3
	bcs	.L250
	cmp	r9, #0
	beq	.L1013
	mvn	ip, r5
	add	r1, ip, r3
	mov	r0, r1, lsr #3
	ands	r1, r0, #3
	beq	.L664
	ldrb	r0, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r0, asl r5
	add	r5, r5, #8
	cmp	r5, r3
	sub	r9, r9, #1
	bcs	.L250
	cmp	r9, #0
	beq	.L884
	cmp	r1, #1
	beq	.L664
	cmp	r1, #2
	beq	.L705
	ldrb	r7, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r7, asl r5
	add	r5, r5, #8
	beq	.L886
.L705:
	ldrb	ip, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, ip, asl r5
	add	r5, r5, #8
	beq	.L1014
.L664:
	ldrb	r7, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r7, asl r5
	add	r5, r5, #8
	sub	r9, r9, #1
	cmp	r5, r3
	mov	r0, fp
	mov	r1, r9
	mov	ip, r5
	bcs	.L250
	cmp	r9, #0
	beq	.L1015
	ldrb	r4, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r4, asl r5
	add	r5, r5, #8
	beq	.L888
	ldrb	r7, [fp, #0]	@ zero_extendqisi2
	subs	r9, r1, #2
	add	r8, r8, r7, asl r5
	add	fp, r0, #2
	add	r5, ip, #16
	beq	.L889
	ldrb	fp, [r0, #2]	@ zero_extendqisi2
	subs	r9, r1, #3
	add	r8, r8, fp, asl r5
	add	fp, r0, #3
	add	r5, ip, #24
	bne	.L664
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L322:
	ldr	sl, [sp, #36]
	ldr	r2, [sp, #44]
	ldr	r3, [r7, #20]
	rsb	r1, sl, r2
	add	sl, r3, r1
	str	sl, [r7, #20]
	ldr	r3, [r6, #28]
	cmp	r1, #0
	add	sl, r3, r1
	str	sl, [r6, #28]
	beq	.L325
	ldr	lr, [r6, #16]
	cmp	lr, #0
	mov	r2, r1
	ldr	r0, [r6, #24]
	rsb	r1, r1, r4
	str	ip, [sp, #16]
	beq	.L326
	bl	crc32
	ldr	ip, [sp, #16]
.L327:
	str	r0, [r6, #24]
	str	r0, [r7, #48]
.L325:
	ldr	r1, [r6, #16]
	cmp	r1, #0
	moveq	r2, r8, lsr #24
	moveq	r1, r8, lsr #8
	addeq	r2, r2, r8, asl #24
	andeq	r1, r1, #65280
	addeq	r1, r2, r1
	andeq	r2, r8, #65280
	addeq	r1, r1, r2, asl #8
	ldr	r2, [r6, #24]
	movne	r1, r8
	cmp	r1, r2
	bne	.L1016
	ldr	r8, [sp, #36]
	mov	r5, #0
	ldr	r3, [r6, #8]
	str	r8, [sp, #44]
	mov	r8, r5
.L321:
	mov	r2, #25
	str	r2, [r6, #0]
	b	.L144
.L955:
	ldr	r0, [r6, #24]
	mov	r1, fp
	mov	r2, sl
	str	r3, [sp, #20]
	str	ip, [sp, #16]
	bl	crc32
	str	r0, [r6, #24]
	ldr	ip, [sp, #16]
	ldr	r3, [sp, #20]
	b	.L214
.L883:
	mov	ip, r7
	add	r4, sp, #28
	ldmia	r4, {r4, r6}	@ phole ldm
	ldr	r7, [sp, #40]
.L279:
	cmp	r1, #0
	rsbeq	r5, r3, r5
	streq	r0, [r6, #64]
	moveq	r8, r8, lsr r3
	bne	.L1017
.L347:
	mov	r3, #23
	str	r3, [r6, #0]
	b	.L118
.L954:
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L232:
	mov	r1, #65536
	sub	r2, r1, #1
	mov	sl, r8, asl #16
	mov	r3, sl, lsr #16
	eor	r2, r2, r8, lsr #16
	cmp	r3, r2
	bne	.L1018
	mov	r2, #14
	mov	r5, #0
	str	r2, [r6, #0]
	str	r3, [r6, #64]
	mov	r8, r5
	b	.L156
.L809:
	mov	r3, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r3
	b	.L149
.L808:
	mov	r1, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r1
	b	.L149
.L971:
	mov	r4, sl
.L194:
	ldr	r0, [r6, #32]
	cmp	r0, #0
	str	r8, [r6, #64]
	strne	r8, [r0, #20]
	ldrne	r2, [r6, #16]
	ands	sl, r2, #512
	moveq	r8, sl
	moveq	r5, r8
	beq	.L199
	mov	r2, r8, lsr #8
	strb	r2, [sp, #101]
	strb	r8, [sp, #100]
	mov	r2, #2
	ldr	r0, [r6, #24]
	add	r1, sp, #100
	str	ip, [sp, #16]
	bl	crc32
	mov	r8, #0
	str	r0, [r6, #24]
	mov	r5, r8
	ldr	r2, [r6, #16]
	ldr	ip, [sp, #16]
	b	.L199
.L968:
	ldr	r0, [r6, #24]
	mov	r1, fp
	mov	r2, sl
	str	r3, [sp, #20]
	str	ip, [sp, #16]
	bl	crc32
	str	r0, [r6, #24]
	ldr	ip, [sp, #16]
	ldr	r3, [sp, #20]
	b	.L209
.L164:
	mov	r3, #35584
	add	r1, r3, #31
	mov	r2, sl, lsr #1
	cmp	r8, r1
	movne	r3, #0
	andeq	r3, r2, #1
	cmp	r3, #0
	bne	.L1019
	ldr	r2, [r6, #32]
	cmp	r2, #0
	str	r3, [r6, #16]
	mvnne	r3, #0
	strne	r3, [r2, #48]
	ldrne	sl, [r6, #8]
	tst	sl, #1
	beq	.L170
	mov	r0, r8, asl #8
	mov	sl, r0, asl #16
	mov	lr, sl, lsr #16
	add	r0, lr, r8, lsr #8
	mov	r1, #31
	str	ip, [sp, #16]
	bl	__aeabi_uidivmod
	subs	sl, r1, #0
	ldr	ip, [sp, #16]
	bne	.L170
	and	r2, r8, #15
	cmp	r2, #8
	beq	.L172
	ldr	r3, .L1045
	mov	lr, #27
	str	r3, [r7, #24]
	str	lr, [r6, #0]
	mov	r3, lr
	b	.L118
.L314:
	ldrb	sl, [r3, r0]	@ zero_extendqisi2
	sub	r2, r1, #1
	cmp	r1, #1
	strb	sl, [r4, r0]
	and	r0, r2, #3
	mov	r2, #1
	beq	.L318
	cmp	r0, #0
	beq	.L847
	cmp	r0, #1
	beq	.L685
	cmp	r0, #2
	ldrneb	r0, [r3, r2]	@ zero_extendqisi2
	strneb	r0, [r4, r2]
	movne	r2, #2
	ldrb	sl, [r3, r2]	@ zero_extendqisi2
	strb	sl, [r4, r2]
	add	r2, r2, #1
.L685:
	ldrb	r0, [r3, r2]	@ zero_extendqisi2
	strb	r0, [r4, r2]
	add	r2, r2, #1
	cmp	r1, r2
	beq	.L318
.L847:
	mov	sl, r5
.L320:
	ldrb	r0, [r3, r2]	@ zero_extendqisi2
	strb	r0, [r4, r2]
	add	r0, r2, #1
	ldrb	r5, [r3, r0]	@ zero_extendqisi2
	strb	r5, [r4, r0]
	add	r5, r0, #1
	ldrb	r0, [r3, r5]	@ zero_extendqisi2
	strb	r0, [r4, r5]
	add	r5, r2, #3
	ldrb	r0, [r3, r5]	@ zero_extendqisi2
	add	r2, r2, #4
	cmp	r1, r2
	strb	r0, [r4, r5]
	bne	.L320
	mov	r5, sl
	b	.L318
.L249:
	cmp	r2, #16
	beq	.L1020
	cmp	r2, #17
	beq	.L260
	add	r2, r3, #7
	cmp	r5, r2
	bcs	.L261
	cmp	r9, #0
	beq	.L1021
	mvn	r0, r5
	add	r4, r0, r2
	mov	r7, r4, lsr #3
	ands	r1, r7, #3
	beq	.L667
	ldrb	ip, [fp], #1	@ zero_extendqisi2
	add	r8, r8, ip, asl r5
	add	r5, r5, #8
	cmp	r5, r2
	sub	r9, r9, #1
	bcs	.L261
	cmp	r9, #0
	beq	.L906
	cmp	r1, #1
	beq	.L667
	cmp	r1, #2
	beq	.L712
	ldrb	r0, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r0, asl r5
	add	r5, r5, #8
	beq	.L908
.L712:
	ldrb	r1, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r1, asl r5
	add	r5, r5, #8
	beq	.L1022
.L667:
	ldrb	r1, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r1, asl r5
	add	r5, r5, #8
	sub	r9, r9, #1
	cmp	r5, r2
	mov	r0, fp
	mov	r1, r9
	mov	ip, r5
	bcs	.L261
	cmp	r9, #0
	beq	.L1023
	ldrb	r4, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r4, asl r5
	add	r5, r5, #8
	beq	.L910
	ldrb	r7, [fp, #0]	@ zero_extendqisi2
	subs	r9, r1, #2
	add	r8, r8, r7, asl r5
	add	fp, r0, #2
	add	r5, ip, #16
	beq	.L911
	ldrb	fp, [r0, #2]	@ zero_extendqisi2
	subs	r9, r1, #3
	add	r8, r8, fp, asl r5
	add	fp, r0, #3
	add	r5, ip, #24
	bne	.L667
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L230:
	ldr	r2, .L1045+4
	mov	r0, #27
	str	r2, [r7, #24]
	str	r0, [r6, #0]
	b	.L231
.L1017:
	tst	r1, #240
	rsbne	r5, r3, r5
	strne	r0, [r6, #64]
	movne	r8, r8, lsr r3
	bne	.L348
	add	sl, r3, r1
	mvn	r1, #0
	mvn	r1, r1, asl sl
	str	r1, [sp, #28]
	and	r1, r1, r8
	add	sl, r0, r1, lsr r3
	add	r1, r2, sl, asl #2
	str	r1, [sp, #32]
	str	sl, [sp, #40]
	ldrb	sl, [r1, #1]	@ zero_extendqisi2
	str	sl, [sp, #80]
	add	r1, r3, sl
	ldr	sl, [sp, #40]
	cmp	r5, r1
	ldrb	r1, [r2, sl, asl #2]	@ zero_extendqisi2
	ldr	sl, [sp, #32]
	ldrh	sl, [sl, #2]
	str	sl, [sp, #32]
	bcs	.L285
	cmp	r9, #0
	beq	.L1024
	ldrb	r1, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r1, asl r5
	ldr	sl, [sp, #28]
	and	r1, r8, sl
	add	sl, r0, r1, lsr r3
	add	r1, r2, sl, asl #2
	str	sl, [sp, #40]
	str	r1, [sp, #72]
	ldrb	sl, [r1, #1]	@ zero_extendqisi2
	sub	r9, r9, #1
	str	sl, [sp, #80]
	add	r1, r3, sl
	str	r9, [sp, #32]
	ldr	sl, [sp, #40]
	add	r5, r5, #8
	cmp	r1, r5
	ldrb	r1, [r2, sl, asl #2]	@ zero_extendqisi2
	and	sl, r9, #3
	str	sl, [sp, #40]
	ldr	sl, [sp, #72]
	ldrh	sl, [sl, #2]
	str	sl, [sp, #32]
	bls	.L285
	cmp	r9, #0
	beq	.L874
	ldr	r1, [sp, #40]
	cmp	r1, #0
	beq	.L876
	cmp	r1, #1
	beq	.L700
	cmp	r1, #2
	beq	.L701
	ldrb	r1, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r1, asl r5
	ldr	sl, [sp, #28]
	and	r1, r8, sl
	add	sl, r0, r1, lsr r3
	add	r1, r2, sl, asl #2
	str	sl, [sp, #32]
	str	r1, [sp, #40]
	ldrb	sl, [r1, #1]	@ zero_extendqisi2
	str	sl, [sp, #80]
	add	r1, r3, sl
	ldr	sl, [sp, #32]
	add	r5, r5, #8
	cmp	r1, r5
	ldrb	r1, [r2, sl, asl #2]	@ zero_extendqisi2
	ldr	sl, [sp, #40]
	ldrh	sl, [sl, #2]
	sub	r9, r9, #1
	str	sl, [sp, #32]
	bls	.L285
.L701:
	ldrb	r1, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r1, asl r5
	ldr	sl, [sp, #28]
	and	r1, r8, sl
	add	sl, r0, r1, lsr r3
	add	r1, r2, sl, asl #2
	str	sl, [sp, #32]
	str	r1, [sp, #40]
	ldrb	sl, [r1, #1]	@ zero_extendqisi2
	str	sl, [sp, #80]
	add	r1, r3, sl
	ldr	sl, [sp, #32]
	add	r5, r5, #8
	cmp	r1, r5
	ldrb	r1, [r2, sl, asl #2]	@ zero_extendqisi2
	ldr	sl, [sp, #40]
	ldrh	sl, [sl, #2]
	sub	r9, r9, #1
	str	sl, [sp, #32]
	bls	.L285
.L700:
	ldrb	r1, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r1, asl r5
	ldr	sl, [sp, #28]
	and	r1, r8, sl
	add	sl, r0, r1, lsr r3
	add	r1, r2, sl, asl #2
	str	sl, [sp, #32]
	str	r1, [sp, #40]
	ldrb	sl, [r1, #1]	@ zero_extendqisi2
	str	sl, [sp, #80]
	add	r1, r3, sl
	ldr	sl, [sp, #32]
	add	r5, r5, #8
	cmp	r1, r5
	ldrb	r1, [r2, sl, asl #2]	@ zero_extendqisi2
	ldr	sl, [sp, #40]
	ldrh	sl, [sl, #2]
	sub	r9, r9, #1
	str	sl, [sp, #32]
	bls	.L285
	cmp	r9, #0
	beq	.L1025
.L876:
	str	r4, [sp, #40]
	str	r6, [sp, #72]
.L699:
	ldrb	r4, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r4, asl r5
	ldr	r6, [sp, #28]
	and	sl, r8, r6
	add	r4, r0, sl, lsr r3
	add	r6, r2, r4, asl #2
	ldrb	sl, [r6, #1]	@ zero_extendqisi2
	add	r5, r5, #8
	add	r1, r3, sl
	sub	r9, r9, #1
	cmp	r1, r5
	ldrb	r1, [r2, r4, asl #2]	@ zero_extendqisi2
	str	fp, [sp, #88]
	str	r9, [sp, #80]
	str	r5, [sp, #84]
	ldrh	r4, [r6, #2]
	bls	.L878
	ldrb	r4, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r4, asl r5
	ldr	r6, [sp, #28]
	and	sl, r8, r6
	add	r4, r0, sl, lsr r3
	add	r6, r2, r4, asl #2
	ldrb	sl, [r6, #1]	@ zero_extendqisi2
	add	r5, r5, #8
	add	r1, r3, sl
	cmp	r1, r5
	sub	r9, r9, #1
	ldrb	r1, [r2, r4, asl #2]	@ zero_extendqisi2
	ldrh	r4, [r6, #2]
	bls	.L878
	ldrb	r9, [fp, #0]	@ zero_extendqisi2
	add	r8, r8, r9, asl r5
	ldr	r1, [sp, #28]
	and	sl, r8, r1
	add	r9, r0, sl, lsr r3
	add	r5, r2, r9, asl #2
	str	r5, [sp, #32]
	ldr	r4, [sp, #84]
	ldrb	sl, [r5, #1]	@ zero_extendqisi2
	add	r5, r4, #16
	add	r6, r3, sl
	ldr	fp, [sp, #80]
	ldr	r4, [sp, #88]
	cmp	r6, r5
	ldr	r6, [sp, #32]
	ldrb	r1, [r2, r9, asl #2]	@ zero_extendqisi2
	sub	r9, fp, #2
	add	fp, r4, #2
	ldrh	r4, [r6, #2]
	bls	.L878
	ldr	fp, [sp, #88]
	ldrb	r1, [fp, #2]	@ zero_extendqisi2
	add	r8, r8, r1, asl r5
	ldr	r4, [sp, #28]
	and	sl, r8, r4
	add	r4, r0, sl, lsr r3
	add	r5, r2, r4, asl #2
	str	r5, [sp, #32]
	ldr	r6, [sp, #84]
	ldrb	sl, [r5, #1]	@ zero_extendqisi2
	ldr	fp, [sp, #80]
	add	r5, r6, #24
	add	r9, r3, sl
	ldr	r1, [sp, #88]
	ldr	r6, [sp, #32]
	cmp	r9, r5
	sub	r9, fp, #3
	add	fp, r1, #3
	ldrb	r1, [r2, r4, asl #2]	@ zero_extendqisi2
	ldrh	r4, [r6, #2]
	bls	.L878
	cmp	r9, #0
	bne	.L699
	mov	r4, r7
	ldr	sl, [sp, #24]
	mov	r7, ip
	ldr	r6, [sp, #72]
	ldr	ip, [sp, #40]
	b	.L149
.L878:
	str	r4, [sp, #32]
	str	sl, [sp, #80]
	ldr	r4, [sp, #40]
	ldr	r6, [sp, #72]
.L285:
	mov	r8, r8, lsr r3
	rsb	r5, r3, r5
	ldr	r0, [sp, #32]
	ldr	r3, [sp, #80]
	cmp	r1, #0
	rsb	r5, r3, r5
	str	r0, [r6, #64]
	mov	r8, r8, lsr r3
	beq	.L347
.L348:
	tst	r1, #32
	bne	.L937
	tst	r1, #64
	bne	.L1026
	and	r1, r1, #15
	mov	r0, #19
	str	r0, [r6, #0]
	str	r1, [r6, #72]
	b	.L152
.L977:
	mov	lr, r8, lsr #8
	strb	lr, [sp, #101]
	strb	r8, [sp, #100]
	mov	r2, #2
	ldr	r0, [r6, #24]
	add	r1, sp, #100
	str	ip, [sp, #16]
	bl	crc32
	str	r0, [r6, #24]
	ldr	r2, [r6, #16]
	ldr	ip, [sp, #16]
	b	.L192
.L170:
	ldr	r1, .L1045+8
	mov	r3, #27
	str	r1, [r7, #24]
	str	r3, [r6, #0]
	b	.L118
.L806:
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L820:
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
	b	.L149
.L975:
	mov	r1, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r1
	b	.L149
.L991:
	mov	r1, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r1
	b	.L149
.L250:
	mov	r8, r8, lsr r3
	add	r4, sl, #1
	add	sl, r6, sl, asl #1
	strh	r2, [sl, #112]	@ movhi
	str	r4, [r6, #104]
	rsb	r5, r3, r5
.L253:
	ldr	sl, [r6, #104]
	ldr	r2, [sp, #84]
	b	.L246
.L804:
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
	b	.L149
.L951:
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
	b	.L149
.L997:
	mov	lr, r8, lsr #24
	mov	r3, r8, lsr #16
	mov	sl, r8, lsr #8
	strb	sl, [sp, #101]
	strb	r3, [sp, #102]
	strb	lr, [sp, #103]
	strb	r8, [sp, #100]
	add	r1, sp, #100
	ldr	r0, [r6, #24]
	mov	r2, #4
	str	ip, [sp, #16]
	bl	crc32
	str	r0, [r6, #24]
	ldr	ip, [sp, #16]
	b	.L186
.L929:
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
	b	.L149
.L946:
	mov	r3, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r3
	b	.L149
.L1019:
	mov	r0, #0
	mov	r1, r0
	mov	r2, r0
	str	ip, [sp, #16]
	bl	crc32
	mov	r5, #31
	mvn	ip, #116
	str	r0, [r6, #24]
	strb	r5, [sp, #100]
	strb	ip, [sp, #101]
	add	r1, sp, #100
	ldr	r0, [r6, #24]
	mov	r2, #2
	bl	crc32
	mov	r3, #1
	mov	r5, #0
	str	r0, [r6, #24]
	str	r3, [r6, #0]
	mov	r8, r5
	ldr	ip, [sp, #16]
	b	.L118
.L224:
	and	r2, r5, #7
	mov	r3, #24
	mov	r8, r8, lsr r2
	bic	r5, r5, #7
	str	r3, [r6, #0]
	b	.L118
.L1007:
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
	b	.L149
.L976:
	mov	r1, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r1
	b	.L149
.L821:
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L992:
	mov	r3, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r3
	b	.L149
.L930:
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L943:
	mov	r1, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r1
	b	.L149
.L956:
	mov	r3, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r3
	b	.L149
.L996:
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L827:
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L826:
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
	b	.L149
.L995:
	mov	r3, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r3
	b	.L149
.L1004:
	mov	lr, r8, lsr #8
	strb	lr, [sp, #101]
	strb	r8, [sp, #100]
	add	r1, sp, #100
	ldr	r0, [r6, #24]
	mov	r2, #2
	str	ip, [sp, #16]
	bl	crc32
	str	r0, [r6, #24]
	ldr	ip, [sp, #16]
	b	.L180
.L1000:
	mov	r1, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r1
	b	.L149
.L797:
	mov	r0, ip
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
.L331:
	mov	r2, #26
	str	r2, [r6, #0]
	mov	sl, #1
	b	.L149
.L1020:
	add	r2, r3, #2
	cmp	r5, r2
	bcs	.L255
	cmp	r9, #0
	beq	.L1027
	mvn	ip, r5
	add	r1, ip, r2
	mov	r0, r1, lsr #3
	ands	r1, r0, #3
	beq	.L665
	ldrb	r0, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r0, asl r5
	add	r5, r5, #8
	cmp	r5, r2
	sub	r9, r9, #1
	bcs	.L255
	cmp	r9, #0
	beq	.L894
	cmp	r1, #1
	beq	.L665
	cmp	r1, #2
	beq	.L710
	ldrb	r0, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r0, asl r5
	add	r5, r5, #8
	beq	.L896
.L710:
	ldrb	r1, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r1, asl r5
	add	r5, r5, #8
	beq	.L1028
.L665:
	ldrb	r7, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r7, asl r5
	add	r5, r5, #8
	sub	r9, r9, #1
	cmp	r5, r2
	mov	r0, fp
	mov	r1, r9
	mov	ip, r5
	bcs	.L255
	cmp	r9, #0
	beq	.L1029
	ldrb	r4, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r4, asl r5
	add	r5, r5, #8
	beq	.L898
	ldrb	r7, [fp, #0]	@ zero_extendqisi2
	subs	r9, r1, #2
	add	r8, r8, r7, asl r5
	add	fp, r0, #2
	add	r5, ip, #16
	beq	.L899
	ldrb	fp, [r0, #2]	@ zero_extendqisi2
	subs	r9, r1, #3
	add	r8, r8, fp, asl r5
	add	fp, r0, #3
	add	r5, ip, #24
	bne	.L665
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L255:
	cmp	sl, #0
	rsb	r5, r3, r5
	mov	r8, r8, lsr r3
	beq	.L1030
	and	r4, r8, #3
	add	r0, r6, sl, asl #1
	ldrh	r2, [r0, #110]
	mov	r8, r8, lsr #2
	add	r0, r4, #3
	sub	r5, r5, #2
.L259:
	ldr	r1, [sp, #72]
	add	r3, r0, sl
	cmp	r1, r3
	str	r3, [sp, #92]
	bcc	.L1031
	add	r1, sl, #56
	mov	r7, r1, asl #1
	add	r3, r6, r7
	and	ip, r3, #3
	mov	r3, ip, lsr #1
	cmp	r0, r3
	movcc	r3, r0
	cmp	r3, #0
	rsb	r4, r3, r0
	strneh	r2, [r6, r7]	@ movhi
	sub	ip, r0, #1
	mov	r7, r4, lsr #1
	addne	sl, sl, #1
	subne	ip, ip, #1
	movs	r0, r7, asl #1
	str	r4, [sp, #88]
	str	r0, [sp, #72]
	beq	.L269
	add	r1, r1, r3
	mov	r3, #1
	add	r1, r6, r1, asl #1
	orr	r0, r2, r2, asl #16
	sub	r4, r7, #1
	cmp	r3, r7
	and	r4, r4, #3
	str	r0, [r1, #0]
	bcs	.L764
	cmp	r4, #0
	beq	.L270
	cmp	r4, #1
	beq	.L708
	cmp	r4, #2
	movne	r3, #2
	strne	r0, [r1, #4]
	str	r0, [r1, r3, asl #2]
	add	r3, r3, #1
.L708:
	str	r0, [r1, r3, asl #2]
	add	r3, r3, #1
	cmp	r3, r7
	bcs	.L764
.L270:
	add	r4, r3, #1
	str	r0, [r1, r3, asl #2]
	str	r4, [sp, #80]
	add	r4, r3, #3
	str	r4, [sp, #84]
	ldr	r4, [sp, #80]
	add	r4, r4, #1
	str	r4, [sp, #12]
	ldr	r4, [sp, #80]
	str	r0, [r1, r4, asl #2]
	ldr	r4, [sp, #12]
	str	r0, [r1, r4, asl #2]
	add	r3, r3, #4
	ldr	r4, [sp, #84]
	cmp	r3, r7
	str	r0, [r1, r4, asl #2]
	bcc	.L270
.L764:
	ldr	r7, [sp, #72]
	ldr	r0, [sp, #88]
	cmp	r0, r7
	rsb	ip, r7, ip
	add	sl, sl, r7
	beq	.L1032
	ldr	r3, [r6, #96]
	str	r3, [sp, #80]
	ldr	r0, [r6, #100]
	str	r0, [sp, #84]
.L269:
	add	r1, sl, #56
	mov	r4, r1, asl #1
	sub	r1, ip, #1
	add	r0, r6, r4
	cmn	r1, #1
	strh	r2, [r6, r4]	@ movhi
	and	ip, ip, #3
	add	r3, r0, #2
	beq	.L271
	cmp	ip, #0
	beq	.L272
	cmp	ip, #1
	beq	.L706
	cmp	ip, #2
	addne	r3, r3, #2
	strneh	r2, [r0, #2]	@ movhi
	subne	r1, r1, #1
	strh	r2, [r3], #2	@ movhi
	sub	r1, r1, #1
.L706:
	sub	r1, r1, #1
	cmn	r1, #1
	strh	r2, [r3], #2	@ movhi
	beq	.L271
.L272:
	mov	r0, r3
	strh	r2, [r0], #2	@ movhi
	sub	r1, r1, #4
	cmn	r1, #1
	strh	r2, [r3, #2]	@ movhi
	strh	r2, [r0, #2]	@ movhi
	strh	r2, [r3, #6]	@ movhi
	add	r3, r3, #8
	bne	.L272
.L271:
	ldr	ip, [sp, #92]
	str	ip, [r6, #104]
	b	.L253
.L857:
	mov	r4, r7
	mov	r7, ip
	mov	ip, sl
	ldr	sl, [sp, #24]
	b	.L149
.L856:
	mov	r4, r7
	mov	r7, ip
	mov	ip, sl
	ldr	sl, [sp, #24]
	b	.L149
.L824:
	mov	r3, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r3
	b	.L149
.L830:
	mov	r3, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r3
	b	.L149
.L964:
	mov	r4, r7
	mov	r7, ip
	mov	ip, sl
	ldr	sl, [sp, #24]
	b	.L149
.L927:
	mov	r3, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r3
	b	.L149
.L261:
	mov	r8, r8, lsr r3
	rsb	r4, r3, #-16777216
	add	r7, r4, #16711680
	add	ip, r7, #65280
	and	r2, r8, #127
	add	r3, ip, #249
	add	r0, r2, #11
	add	r5, r5, r3
	mov	r8, r8, lsr #7
	mov	r2, #0
	b	.L259
.L260:
	add	r2, r3, #3
	cmp	r5, r2
	bcs	.L263
	cmp	r9, #0
	beq	.L1033
	mvn	r7, r5
	add	ip, r7, r2
	mov	r1, ip, lsr #3
	ands	r1, r1, #3
	beq	.L666
	ldrb	ip, [fp], #1	@ zero_extendqisi2
	add	r8, r8, ip, asl r5
	add	r5, r5, #8
	cmp	r5, r2
	sub	r9, r9, #1
	bcs	.L263
	cmp	r9, #0
	beq	.L900
	cmp	r1, #1
	beq	.L666
	cmp	r1, #2
	beq	.L711
	ldrb	r0, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r0, asl r5
	add	r5, r5, #8
	beq	.L902
.L711:
	ldrb	r1, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r1, asl r5
	add	r5, r5, #8
	beq	.L1034
.L666:
	ldrb	r4, [fp], #1	@ zero_extendqisi2
	add	r8, r8, r4, asl r5
	add	r5, r5, #8
	sub	r9, r9, #1
	cmp	r5, r2
	mov	r0, fp
	mov	r1, r9
	mov	ip, r5
	bcs	.L263
	cmp	r9, #0
	beq	.L1035
	ldrb	r4, [fp], #1	@ zero_extendqisi2
	subs	r9, r9, #1
	add	r8, r8, r4, asl r5
	add	r5, r5, #8
	beq	.L904
	ldrb	r7, [fp, #0]	@ zero_extendqisi2
	subs	r9, r1, #2
	add	r8, r8, r7, asl r5
	add	fp, r0, #2
	add	r5, ip, #16
	beq	.L905
	ldrb	fp, [r0, #2]	@ zero_extendqisi2
	subs	r9, r1, #3
	add	r8, r8, fp, asl r5
	add	fp, r0, #3
	add	r5, ip, #24
	bne	.L666
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L263:
	mov	r8, r8, lsr r3
	rsb	ip, r3, #-16777216
	add	r1, ip, #16711680
	add	r0, r1, #65280
	and	r2, r8, #7
	add	r3, r0, #253
	add	r5, r5, r3
	add	r0, r2, #3
	mov	r8, r8, lsr #3
	mov	r2, #0
	b	.L259
.L937:
	mov	r3, #11
	str	r3, [r6, #0]
	b	.L118
.L816:
	mov	r1, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r1
	b	.L149
.L973:
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
	b	.L149
.L1038:
	mov	r3, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r3
	b	.L149
.L1008:
	mov	r3, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r3
	b	.L149
.L924:
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L923:
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
	b	.L149
.L1011:
	mov	r4, r7
	ldr	sl, [sp, #24]
	mov	r7, ip
	ldr	ip, [sp, #72]
	b	.L149
.L912:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L981:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L980:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L947:
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L839:
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L815:
	mov	r4, r7
	mov	r7, ip
	mov	ip, sl
	ldr	sl, [sp, #24]
	b	.L149
.L814:
	mov	r4, r7
	mov	r7, ip
	mov	ip, sl
	ldr	sl, [sp, #24]
	b	.L149
.L838:
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
	b	.L149
.L963:
	mov	r4, r7
	mov	r7, ip
	mov	ip, sl
	ldr	sl, [sp, #24]
	b	.L149
.L961:
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L798:
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
	b	.L149
.L845:
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L803:
	mov	r3, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r3
	b	.L149
.L802:
	mov	r1, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r1
	b	.L149
.L988:
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
	b	.L149
.L844:
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
	b	.L149
.L960:
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L800:
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L1046:
	.align	2
.L1045:
	.word	.LC1
	.word	.LC5
	.word	.LC0
	.word	.LC11
	.word	.LC1
	.word	.LC6
	.word	.LC7
	.word	.LC13
	.word	.LC10
	.word	.LC2
	.word	.LC12
	.word	.LC15
	.word	.LC3
	.word	.LC9
	.word	.LC8
.L969:
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
	b	.L149
.L787:
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
	b	.L149
.L958:
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
	b	.L149
.L959:
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
	b	.L149
.L273:
	ldr	r3, [r6, #0]
	cmp	r3, #27
	add	r4, sp, #28
	ldmia	r4, {r4, r7}	@ phole ldm
	ldr	ip, [sp, #40]
	beq	.L118
	mov	r3, #9
	str	r3, [r6, #84]
	ldr	r2, [sp, #52]
	str	r2, [r6, #108]
	str	r2, [r6, #76]
	ldr	sl, [sp, #64]
	ldr	lr, [sp, #56]
	stmia	sp, {sl, lr}	@ phole stm
	mov	r0, #1
	ldr	r2, [r6, #96]
	ldr	r1, [sp, #68]
	ldr	r3, [sp, #60]
	str	ip, [sp, #16]
	bl	inflate_table
	cmp	r0, #0
	str	r0, [sp, #24]
	ldr	ip, [sp, #16]
	bne	.L1036
	ldr	sl, [r6, #108]
	mov	r1, #6
	str	r1, [r6, #88]
	str	sl, [r6, #80]
	ldr	r3, [sp, #76]
	ldr	r2, [sp, #56]
	ldr	lr, [r6, #96]
	str	r3, [sp, #0]
	str	r2, [sp, #4]
	add	r0, lr, #56
	ldr	r3, [sp, #60]
	add	r1, r6, r0, asl #1
	ldr	r2, [r6, #100]
	mov	r0, #2
	str	ip, [sp, #16]
	bl	inflate_table
	cmp	r0, #0
	moveq	r3, #18
	str	r0, [sp, #24]
	ldr	ip, [sp, #16]
	streq	r3, [r6, #0]
	beq	.L137
	ldr	r0, .L1045+12
	str	r0, [r7, #24]
	mov	r0, #27
	str	r0, [r6, #0]
	mov	r3, r0
	b	.L118
.L879:
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L1002:
	ldr	r3, .L1045+16
	mov	sl, #27
	str	r3, [r7, #24]
	str	sl, [r6, #0]
	mov	r3, sl
	b	.L118
.L986:
	mov	r3, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r3
	b	.L149
.L925:
	mov	r1, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r1
	b	.L149
.L1039:
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
	b	.L149
.L989:
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L1005:
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
	b	.L149
.L832:
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
	b	.L149
.L1001:
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L833:
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L326:
	bl	adler32
	ldr	ip, [sp, #16]
	b	.L327
.L818:
	mov	r3, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r3
	b	.L149
.L921:
	mov	r3, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r3
	b	.L149
.L936:
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L935:
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
	b	.L149
.L967:
	mov	r1, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r1
	b	.L149
.L873:
	mov	r4, r7
	mov	r7, ip
	mov	ip, sl
	ldr	sl, [sp, #24]
	b	.L149
.L872:
	mov	r4, r7
	mov	r7, ip
	mov	ip, sl
	ldr	sl, [sp, #24]
	b	.L149
.L985:
	mov	r4, r7
	mov	r7, ip
	mov	ip, sl
	ldr	sl, [sp, #24]
	b	.L149
.L993:
	mov	r1, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r1
	b	.L149
.L998:
	mov	r1, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r1
	b	.L149
.L828:
	mov	r1, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r1
	b	.L149
.L1041:
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
	b	.L149
.L944:
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L163:
	mov	r3, #12
	str	r3, [r6, #0]
	b	.L118
.L834:
	mov	r1, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r1
	b	.L149
.L1037:
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
	b	.L149
.L789:
	mov	r1, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r1
	b	.L149
.L840:
	mov	r1, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r1
	b	.L149
.L794:
	mov	r1, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r1
	b	.L149
.L888:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L1015:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L950:
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L836:
	mov	r3, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r3
	b	.L149
.L822:
	mov	r1, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r1
	b	.L149
.L1040:
	mov	r1, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r1
	b	.L149
.L919:
	mov	r1, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r1
	b	.L149
.L1042:
	mov	r1, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r1
	b	.L149
.L972:
	mov	r4, r7
	mov	r7, ip
	mov	ip, sl
	ldr	sl, [sp, #24]
	b	.L149
.L978:
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
	b	.L149
.L842:
	mov	r3, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r3
	b	.L149
.L1018:
	ldr	r3, .L1045+20
	mov	sl, #27
	str	r3, [r7, #24]
	str	sl, [r6, #0]
	mov	r3, sl
	b	.L118
.L238:
	ldr	r1, .L1045+24
	mov	r3, #27
	str	r1, [r7, #24]
	str	r3, [r6, #0]
	b	.L118
.L863:
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L949:
	mov	r1, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r1
	b	.L149
.L933:
	mov	r3, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r3
	b	.L149
.L870:
	mov	r3, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r3
	b	.L149
.L889:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L1021:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L1022:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L911:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L910:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L1023:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L1012:
	ldr	r3, .L1045+28
	str	r3, [r7, #24]
	mov	r3, #27
	str	r3, [r6, #0]
	b	.L118
.L278:
	str	r4, [r7, #12]
	ldr	r4, [sp, #36]
	str	fp, [r7, #0]
	str	r4, [r7, #16]
	str	r9, [r7, #4]
	str	r8, [r6, #56]
	str	r5, [r6, #60]
	mov	r0, r7
	ldr	r1, [sp, #44]
	str	ip, [sp, #16]
	bl	inflate_fast
	add	r4, r7, #12
	ldmia	r4, {r4, fp}	@ phole ldm
	str	fp, [sp, #36]
	ldr	ip, [sp, #16]
	ldr	fp, [r7, #0]
	ldr	r9, [r7, #4]
	ldr	r8, [r6, #56]
	ldr	r5, [r6, #60]
	ldr	r3, [r6, #0]
	b	.L118
.L979:
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L1036:
	ldr	sl, .L1045+32
	mov	r1, #27
	str	sl, [r7, #24]
	mov	r3, r1
	str	r1, [r6, #0]
	b	.L118
.L852:
	mov	r1, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r1
	b	.L149
.L795:
	mov	r3, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r3
	b	.L149
.L948:
	mov	r3, r4
	str	r9, [r7, #4]
	str	r3, [r7, #12]
	ldr	ip, [sp, #36]
	mov	r4, r7
	str	ip, [r7, #16]
	str	fp, [r7, #0]
	mov	sl, #2
	str	r5, [r6, #60]
	str	r8, [r6, #56]
	b	.L150
.L983:
	mov	r1, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r1
	b	.L149
.L931:
	mov	r1, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r1
	b	.L149
.L966:
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
	b	.L149
.L786:
	mov	r3, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r3
	b	.L149
.L970:
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
	b	.L149
.L965:
	mov	r3, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r3
	b	.L149
.L172:
	mov	r8, r8, lsr #4
	and	r0, r8, #15
	ldr	r2, [r6, #36]
	add	r3, r0, #8
	cmp	r3, r2
	bls	.L173
	ldr	sl, .L1045+36
	mov	r0, #27
	str	sl, [r7, #24]
	sub	r5, r5, #4
	str	r0, [r6, #0]
	mov	r3, r0
	b	.L118
.L987:
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
	b	.L149
.L1026:
	ldr	sl, .L1045+40
	mov	r1, #27
	str	sl, [r7, #24]
	mov	r3, r1
	str	r1, [r6, #0]
	b	.L118
.L173:
	mov	r2, #1
	mov	r5, r2, asl r3
	mov	r0, sl
	str	r5, [r6, #20]
	mov	r2, sl
	str	ip, [sp, #16]
	bl	adler32
	tst	r8, #512
	movne	r2, #9
	moveq	r2, #11
	str	r0, [r6, #24]
	str	r0, [r7, #48]
	str	r2, [r6, #0]
	mov	r5, sl
	mov	r3, r2
	mov	r8, sl
	ldr	ip, [sp, #16]
	b	.L118
.L854:
	mov	r3, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r3
	b	.L149
.L1016:
	ldr	r0, .L1045+44
	str	r0, [r7, #24]
	mov	r0, #27
	str	r0, [r6, #0]
	ldr	r3, [sp, #36]
	str	r3, [sp, #44]
	mov	r3, r0
	b	.L118
.L1025:
	mov	r3, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r3
	b	.L149
.L1003:
	ldr	r2, .L1045+48
	str	r2, [r7, #24]
	mov	r2, #27
	str	r2, [r6, #0]
	mov	r3, r2
	b	.L118
.L1013:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L1014:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L884:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L335:
	mov	r5, ip
	mov	ip, r4
	mov	r4, r7
	mov	r7, r5
	mov	r5, #0
	mov	r8, r5
	b	.L331
.L874:
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
	b	.L149
.L1024:
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L1033:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L792:
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L906:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L982:
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L1031:
	ldr	r7, [sp, #32]
	ldr	sl, .L1045+52
	mov	r2, #27
	ldr	r4, [sp, #28]
	ldr	ip, [sp, #40]
	mov	r3, r2
	str	sl, [r7, #24]
	str	r2, [r6, #0]
	b	.L118
.L1030:
	ldr	r7, [sp, #32]
	ldr	r3, .L1045+52
	add	sl, sl, #27
	ldr	r4, [sp, #28]
	ldr	ip, [sp, #40]
	str	r3, [r7, #24]
	str	sl, [r6, #0]
	mov	r3, sl
	b	.L118
.L810:
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
	b	.L149
.L812:
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L1010:
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L886:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L858:
	mov	r0, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r0
	b	.L149
.L1009:
	mov	r2, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r2
	b	.L149
.L962:
	mov	r1, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r1
	b	.L149
.L957:
	ldr	r0, .L1045+56
	str	r0, [r7, #24]
	mov	r0, #27
	str	r0, [r6, #0]
	mov	r3, r0
	b	.L118
.L900:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L1034:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L894:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L896:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L1027:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L1028:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L899:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L898:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L1029:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L1032:
	ldr	sl, [r6, #96]
	str	sl, [sp, #80]
	ldr	r2, [r6, #100]
	str	r2, [sp, #84]
	b	.L271
.L868:
	mov	r1, ip
	ldr	sl, [sp, #24]
	mov	ip, r4
	mov	r4, r7
	mov	r7, r1
	b	.L149
.L908:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L902:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L905:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L904:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
.L1035:
	add	sl, sp, #24
	ldmia	sl, {sl, ip}	@ phole ldm
	ldr	r4, [sp, #32]
	ldr	r7, [sp, #40]
	b	.L149
	.size	inflate, .-inflate
	.section	.text.inflateInit_,"ax",%progbits
	.align	2
	.global	inflateInit_
	.hidden	inflateInit_
	.type	inflateInit_, %function
inflateInit_:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #0
	stmfd	sp!, {r4, lr}
	mov	r4, r0
	bne	.L1055
.L1048:
	mvn	r0, #5
.L1050:
	ldmfd	sp!, {r4, lr}
	bx	lr
.L1055:
	ldrb	r3, [r1, #0]	@ zero_extendqisi2
	cmp	r3, #49
	cmpeq	r2, #56
	moveq	r2, #0
	movne	r2, #1
	bne	.L1048
	cmp	r0, #0
	mvneq	r0, #1
	beq	.L1050
	ldr	ip, [r4, #32]
	cmp	ip, #0
	ldr	r0, [r4, #36]
	str	r2, [r4, #24]
	ldreq	r2, .L1056
	streq	ip, [r4, #40]
	streq	r2, [r4, #32]
	moveq	ip, r2
	cmp	r0, #0
	ldreq	ip, .L1056+4
	mov	r2, #9472
	streq	ip, [r4, #36]
	add	r2, r2, #48
	ldreq	ip, [r4, #32]
	ldr	r0, [r4, #40]
	mov	r1, #1
	mov	lr, pc
	bx	ip
	cmp	r0, #0
	mvneq	r0, #3
	beq	.L1050
	mov	r3, #0
	mov	lr, #1
	mov	r1, #15
	str	r0, [r4, #28]
	str	r3, [r0, #52]
	str	lr, [r0, #8]
	str	r1, [r0, #36]
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	b	inflateReset
.L1057:
	.align	2
.L1056:
	.word	zcalloc
	.word	zcfree
	.size	inflateInit_, .-inflateInit_
	.section	.rodata
	.align	2
	.set	.LANCHOR0,. + 0
	.type	lenfix.2060, %object
	.size	lenfix.2060, 2048
lenfix.2060:
	.byte	96
	.byte	7
	.short	0
	.byte	0
	.byte	8
	.short	80
	.byte	0
	.byte	8
	.short	16
	.byte	20
	.byte	8
	.short	115
	.byte	18
	.byte	7
	.short	31
	.byte	0
	.byte	8
	.short	112
	.byte	0
	.byte	8
	.short	48
	.byte	0
	.byte	9
	.short	192
	.byte	16
	.byte	7
	.short	10
	.byte	0
	.byte	8
	.short	96
	.byte	0
	.byte	8
	.short	32
	.byte	0
	.byte	9
	.short	160
	.byte	0
	.byte	8
	.short	0
	.byte	0
	.byte	8
	.short	128
	.byte	0
	.byte	8
	.short	64
	.byte	0
	.byte	9
	.short	224
	.byte	16
	.byte	7
	.short	6
	.byte	0
	.byte	8
	.short	88
	.byte	0
	.byte	8
	.short	24
	.byte	0
	.byte	9
	.short	144
	.byte	19
	.byte	7
	.short	59
	.byte	0
	.byte	8
	.short	120
	.byte	0
	.byte	8
	.short	56
	.byte	0
	.byte	9
	.short	208
	.byte	17
	.byte	7
	.short	17
	.byte	0
	.byte	8
	.short	104
	.byte	0
	.byte	8
	.short	40
	.byte	0
	.byte	9
	.short	176
	.byte	0
	.byte	8
	.short	8
	.byte	0
	.byte	8
	.short	136
	.byte	0
	.byte	8
	.short	72
	.byte	0
	.byte	9
	.short	240
	.byte	16
	.byte	7
	.short	4
	.byte	0
	.byte	8
	.short	84
	.byte	0
	.byte	8
	.short	20
	.byte	21
	.byte	8
	.short	227
	.byte	19
	.byte	7
	.short	43
	.byte	0
	.byte	8
	.short	116
	.byte	0
	.byte	8
	.short	52
	.byte	0
	.byte	9
	.short	200
	.byte	17
	.byte	7
	.short	13
	.byte	0
	.byte	8
	.short	100
	.byte	0
	.byte	8
	.short	36
	.byte	0
	.byte	9
	.short	168
	.byte	0
	.byte	8
	.short	4
	.byte	0
	.byte	8
	.short	132
	.byte	0
	.byte	8
	.short	68
	.byte	0
	.byte	9
	.short	232
	.byte	16
	.byte	7
	.short	8
	.byte	0
	.byte	8
	.short	92
	.byte	0
	.byte	8
	.short	28
	.byte	0
	.byte	9
	.short	152
	.byte	20
	.byte	7
	.short	83
	.byte	0
	.byte	8
	.short	124
	.byte	0
	.byte	8
	.short	60
	.byte	0
	.byte	9
	.short	216
	.byte	18
	.byte	7
	.short	23
	.byte	0
	.byte	8
	.short	108
	.byte	0
	.byte	8
	.short	44
	.byte	0
	.byte	9
	.short	184
	.byte	0
	.byte	8
	.short	12
	.byte	0
	.byte	8
	.short	140
	.byte	0
	.byte	8
	.short	76
	.byte	0
	.byte	9
	.short	248
	.byte	16
	.byte	7
	.short	3
	.byte	0
	.byte	8
	.short	82
	.byte	0
	.byte	8
	.short	18
	.byte	21
	.byte	8
	.short	163
	.byte	19
	.byte	7
	.short	35
	.byte	0
	.byte	8
	.short	114
	.byte	0
	.byte	8
	.short	50
	.byte	0
	.byte	9
	.short	196
	.byte	17
	.byte	7
	.short	11
	.byte	0
	.byte	8
	.short	98
	.byte	0
	.byte	8
	.short	34
	.byte	0
	.byte	9
	.short	164
	.byte	0
	.byte	8
	.short	2
	.byte	0
	.byte	8
	.short	130
	.byte	0
	.byte	8
	.short	66
	.byte	0
	.byte	9
	.short	228
	.byte	16
	.byte	7
	.short	7
	.byte	0
	.byte	8
	.short	90
	.byte	0
	.byte	8
	.short	26
	.byte	0
	.byte	9
	.short	148
	.byte	20
	.byte	7
	.short	67
	.byte	0
	.byte	8
	.short	122
	.byte	0
	.byte	8
	.short	58
	.byte	0
	.byte	9
	.short	212
	.byte	18
	.byte	7
	.short	19
	.byte	0
	.byte	8
	.short	106
	.byte	0
	.byte	8
	.short	42
	.byte	0
	.byte	9
	.short	180
	.byte	0
	.byte	8
	.short	10
	.byte	0
	.byte	8
	.short	138
	.byte	0
	.byte	8
	.short	74
	.byte	0
	.byte	9
	.short	244
	.byte	16
	.byte	7
	.short	5
	.byte	0
	.byte	8
	.short	86
	.byte	0
	.byte	8
	.short	22
	.byte	64
	.byte	8
	.short	0
	.byte	19
	.byte	7
	.short	51
	.byte	0
	.byte	8
	.short	118
	.byte	0
	.byte	8
	.short	54
	.byte	0
	.byte	9
	.short	204
	.byte	17
	.byte	7
	.short	15
	.byte	0
	.byte	8
	.short	102
	.byte	0
	.byte	8
	.short	38
	.byte	0
	.byte	9
	.short	172
	.byte	0
	.byte	8
	.short	6
	.byte	0
	.byte	8
	.short	134
	.byte	0
	.byte	8
	.short	70
	.byte	0
	.byte	9
	.short	236
	.byte	16
	.byte	7
	.short	9
	.byte	0
	.byte	8
	.short	94
	.byte	0
	.byte	8
	.short	30
	.byte	0
	.byte	9
	.short	156
	.byte	20
	.byte	7
	.short	99
	.byte	0
	.byte	8
	.short	126
	.byte	0
	.byte	8
	.short	62
	.byte	0
	.byte	9
	.short	220
	.byte	18
	.byte	7
	.short	27
	.byte	0
	.byte	8
	.short	110
	.byte	0
	.byte	8
	.short	46
	.byte	0
	.byte	9
	.short	188
	.byte	0
	.byte	8
	.short	14
	.byte	0
	.byte	8
	.short	142
	.byte	0
	.byte	8
	.short	78
	.byte	0
	.byte	9
	.short	252
	.byte	96
	.byte	7
	.short	0
	.byte	0
	.byte	8
	.short	81
	.byte	0
	.byte	8
	.short	17
	.byte	21
	.byte	8
	.short	131
	.byte	18
	.byte	7
	.short	31
	.byte	0
	.byte	8
	.short	113
	.byte	0
	.byte	8
	.short	49
	.byte	0
	.byte	9
	.short	194
	.byte	16
	.byte	7
	.short	10
	.byte	0
	.byte	8
	.short	97
	.byte	0
	.byte	8
	.short	33
	.byte	0
	.byte	9
	.short	162
	.byte	0
	.byte	8
	.short	1
	.byte	0
	.byte	8
	.short	129
	.byte	0
	.byte	8
	.short	65
	.byte	0
	.byte	9
	.short	226
	.byte	16
	.byte	7
	.short	6
	.byte	0
	.byte	8
	.short	89
	.byte	0
	.byte	8
	.short	25
	.byte	0
	.byte	9
	.short	146
	.byte	19
	.byte	7
	.short	59
	.byte	0
	.byte	8
	.short	121
	.byte	0
	.byte	8
	.short	57
	.byte	0
	.byte	9
	.short	210
	.byte	17
	.byte	7
	.short	17
	.byte	0
	.byte	8
	.short	105
	.byte	0
	.byte	8
	.short	41
	.byte	0
	.byte	9
	.short	178
	.byte	0
	.byte	8
	.short	9
	.byte	0
	.byte	8
	.short	137
	.byte	0
	.byte	8
	.short	73
	.byte	0
	.byte	9
	.short	242
	.byte	16
	.byte	7
	.short	4
	.byte	0
	.byte	8
	.short	85
	.byte	0
	.byte	8
	.short	21
	.byte	16
	.byte	8
	.short	258
	.byte	19
	.byte	7
	.short	43
	.byte	0
	.byte	8
	.short	117
	.byte	0
	.byte	8
	.short	53
	.byte	0
	.byte	9
	.short	202
	.byte	17
	.byte	7
	.short	13
	.byte	0
	.byte	8
	.short	101
	.byte	0
	.byte	8
	.short	37
	.byte	0
	.byte	9
	.short	170
	.byte	0
	.byte	8
	.short	5
	.byte	0
	.byte	8
	.short	133
	.byte	0
	.byte	8
	.short	69
	.byte	0
	.byte	9
	.short	234
	.byte	16
	.byte	7
	.short	8
	.byte	0
	.byte	8
	.short	93
	.byte	0
	.byte	8
	.short	29
	.byte	0
	.byte	9
	.short	154
	.byte	20
	.byte	7
	.short	83
	.byte	0
	.byte	8
	.short	125
	.byte	0
	.byte	8
	.short	61
	.byte	0
	.byte	9
	.short	218
	.byte	18
	.byte	7
	.short	23
	.byte	0
	.byte	8
	.short	109
	.byte	0
	.byte	8
	.short	45
	.byte	0
	.byte	9
	.short	186
	.byte	0
	.byte	8
	.short	13
	.byte	0
	.byte	8
	.short	141
	.byte	0
	.byte	8
	.short	77
	.byte	0
	.byte	9
	.short	250
	.byte	16
	.byte	7
	.short	3
	.byte	0
	.byte	8
	.short	83
	.byte	0
	.byte	8
	.short	19
	.byte	21
	.byte	8
	.short	195
	.byte	19
	.byte	7
	.short	35
	.byte	0
	.byte	8
	.short	115
	.byte	0
	.byte	8
	.short	51
	.byte	0
	.byte	9
	.short	198
	.byte	17
	.byte	7
	.short	11
	.byte	0
	.byte	8
	.short	99
	.byte	0
	.byte	8
	.short	35
	.byte	0
	.byte	9
	.short	166
	.byte	0
	.byte	8
	.short	3
	.byte	0
	.byte	8
	.short	131
	.byte	0
	.byte	8
	.short	67
	.byte	0
	.byte	9
	.short	230
	.byte	16
	.byte	7
	.short	7
	.byte	0
	.byte	8
	.short	91
	.byte	0
	.byte	8
	.short	27
	.byte	0
	.byte	9
	.short	150
	.byte	20
	.byte	7
	.short	67
	.byte	0
	.byte	8
	.short	123
	.byte	0
	.byte	8
	.short	59
	.byte	0
	.byte	9
	.short	214
	.byte	18
	.byte	7
	.short	19
	.byte	0
	.byte	8
	.short	107
	.byte	0
	.byte	8
	.short	43
	.byte	0
	.byte	9
	.short	182
	.byte	0
	.byte	8
	.short	11
	.byte	0
	.byte	8
	.short	139
	.byte	0
	.byte	8
	.short	75
	.byte	0
	.byte	9
	.short	246
	.byte	16
	.byte	7
	.short	5
	.byte	0
	.byte	8
	.short	87
	.byte	0
	.byte	8
	.short	23
	.byte	64
	.byte	8
	.short	0
	.byte	19
	.byte	7
	.short	51
	.byte	0
	.byte	8
	.short	119
	.byte	0
	.byte	8
	.short	55
	.byte	0
	.byte	9
	.short	206
	.byte	17
	.byte	7
	.short	15
	.byte	0
	.byte	8
	.short	103
	.byte	0
	.byte	8
	.short	39
	.byte	0
	.byte	9
	.short	174
	.byte	0
	.byte	8
	.short	7
	.byte	0
	.byte	8
	.short	135
	.byte	0
	.byte	8
	.short	71
	.byte	0
	.byte	9
	.short	238
	.byte	16
	.byte	7
	.short	9
	.byte	0
	.byte	8
	.short	95
	.byte	0
	.byte	8
	.short	31
	.byte	0
	.byte	9
	.short	158
	.byte	20
	.byte	7
	.short	99
	.byte	0
	.byte	8
	.short	127
	.byte	0
	.byte	8
	.short	63
	.byte	0
	.byte	9
	.short	222
	.byte	18
	.byte	7
	.short	27
	.byte	0
	.byte	8
	.short	111
	.byte	0
	.byte	8
	.short	47
	.byte	0
	.byte	9
	.short	190
	.byte	0
	.byte	8
	.short	15
	.byte	0
	.byte	8
	.short	143
	.byte	0
	.byte	8
	.short	79
	.byte	0
	.byte	9
	.short	254
	.byte	96
	.byte	7
	.short	0
	.byte	0
	.byte	8
	.short	80
	.byte	0
	.byte	8
	.short	16
	.byte	20
	.byte	8
	.short	115
	.byte	18
	.byte	7
	.short	31
	.byte	0
	.byte	8
	.short	112
	.byte	0
	.byte	8
	.short	48
	.byte	0
	.byte	9
	.short	193
	.byte	16
	.byte	7
	.short	10
	.byte	0
	.byte	8
	.short	96
	.byte	0
	.byte	8
	.short	32
	.byte	0
	.byte	9
	.short	161
	.byte	0
	.byte	8
	.short	0
	.byte	0
	.byte	8
	.short	128
	.byte	0
	.byte	8
	.short	64
	.byte	0
	.byte	9
	.short	225
	.byte	16
	.byte	7
	.short	6
	.byte	0
	.byte	8
	.short	88
	.byte	0
	.byte	8
	.short	24
	.byte	0
	.byte	9
	.short	145
	.byte	19
	.byte	7
	.short	59
	.byte	0
	.byte	8
	.short	120
	.byte	0
	.byte	8
	.short	56
	.byte	0
	.byte	9
	.short	209
	.byte	17
	.byte	7
	.short	17
	.byte	0
	.byte	8
	.short	104
	.byte	0
	.byte	8
	.short	40
	.byte	0
	.byte	9
	.short	177
	.byte	0
	.byte	8
	.short	8
	.byte	0
	.byte	8
	.short	136
	.byte	0
	.byte	8
	.short	72
	.byte	0
	.byte	9
	.short	241
	.byte	16
	.byte	7
	.short	4
	.byte	0
	.byte	8
	.short	84
	.byte	0
	.byte	8
	.short	20
	.byte	21
	.byte	8
	.short	227
	.byte	19
	.byte	7
	.short	43
	.byte	0
	.byte	8
	.short	116
	.byte	0
	.byte	8
	.short	52
	.byte	0
	.byte	9
	.short	201
	.byte	17
	.byte	7
	.short	13
	.byte	0
	.byte	8
	.short	100
	.byte	0
	.byte	8
	.short	36
	.byte	0
	.byte	9
	.short	169
	.byte	0
	.byte	8
	.short	4
	.byte	0
	.byte	8
	.short	132
	.byte	0
	.byte	8
	.short	68
	.byte	0
	.byte	9
	.short	233
	.byte	16
	.byte	7
	.short	8
	.byte	0
	.byte	8
	.short	92
	.byte	0
	.byte	8
	.short	28
	.byte	0
	.byte	9
	.short	153
	.byte	20
	.byte	7
	.short	83
	.byte	0
	.byte	8
	.short	124
	.byte	0
	.byte	8
	.short	60
	.byte	0
	.byte	9
	.short	217
	.byte	18
	.byte	7
	.short	23
	.byte	0
	.byte	8
	.short	108
	.byte	0
	.byte	8
	.short	44
	.byte	0
	.byte	9
	.short	185
	.byte	0
	.byte	8
	.short	12
	.byte	0
	.byte	8
	.short	140
	.byte	0
	.byte	8
	.short	76
	.byte	0
	.byte	9
	.short	249
	.byte	16
	.byte	7
	.short	3
	.byte	0
	.byte	8
	.short	82
	.byte	0
	.byte	8
	.short	18
	.byte	21
	.byte	8
	.short	163
	.byte	19
	.byte	7
	.short	35
	.byte	0
	.byte	8
	.short	114
	.byte	0
	.byte	8
	.short	50
	.byte	0
	.byte	9
	.short	197
	.byte	17
	.byte	7
	.short	11
	.byte	0
	.byte	8
	.short	98
	.byte	0
	.byte	8
	.short	34
	.byte	0
	.byte	9
	.short	165
	.byte	0
	.byte	8
	.short	2
	.byte	0
	.byte	8
	.short	130
	.byte	0
	.byte	8
	.short	66
	.byte	0
	.byte	9
	.short	229
	.byte	16
	.byte	7
	.short	7
	.byte	0
	.byte	8
	.short	90
	.byte	0
	.byte	8
	.short	26
	.byte	0
	.byte	9
	.short	149
	.byte	20
	.byte	7
	.short	67
	.byte	0
	.byte	8
	.short	122
	.byte	0
	.byte	8
	.short	58
	.byte	0
	.byte	9
	.short	213
	.byte	18
	.byte	7
	.short	19
	.byte	0
	.byte	8
	.short	106
	.byte	0
	.byte	8
	.short	42
	.byte	0
	.byte	9
	.short	181
	.byte	0
	.byte	8
	.short	10
	.byte	0
	.byte	8
	.short	138
	.byte	0
	.byte	8
	.short	74
	.byte	0
	.byte	9
	.short	245
	.byte	16
	.byte	7
	.short	5
	.byte	0
	.byte	8
	.short	86
	.byte	0
	.byte	8
	.short	22
	.byte	64
	.byte	8
	.short	0
	.byte	19
	.byte	7
	.short	51
	.byte	0
	.byte	8
	.short	118
	.byte	0
	.byte	8
	.short	54
	.byte	0
	.byte	9
	.short	205
	.byte	17
	.byte	7
	.short	15
	.byte	0
	.byte	8
	.short	102
	.byte	0
	.byte	8
	.short	38
	.byte	0
	.byte	9
	.short	173
	.byte	0
	.byte	8
	.short	6
	.byte	0
	.byte	8
	.short	134
	.byte	0
	.byte	8
	.short	70
	.byte	0
	.byte	9
	.short	237
	.byte	16
	.byte	7
	.short	9
	.byte	0
	.byte	8
	.short	94
	.byte	0
	.byte	8
	.short	30
	.byte	0
	.byte	9
	.short	157
	.byte	20
	.byte	7
	.short	99
	.byte	0
	.byte	8
	.short	126
	.byte	0
	.byte	8
	.short	62
	.byte	0
	.byte	9
	.short	221
	.byte	18
	.byte	7
	.short	27
	.byte	0
	.byte	8
	.short	110
	.byte	0
	.byte	8
	.short	46
	.byte	0
	.byte	9
	.short	189
	.byte	0
	.byte	8
	.short	14
	.byte	0
	.byte	8
	.short	142
	.byte	0
	.byte	8
	.short	78
	.byte	0
	.byte	9
	.short	253
	.byte	96
	.byte	7
	.short	0
	.byte	0
	.byte	8
	.short	81
	.byte	0
	.byte	8
	.short	17
	.byte	21
	.byte	8
	.short	131
	.byte	18
	.byte	7
	.short	31
	.byte	0
	.byte	8
	.short	113
	.byte	0
	.byte	8
	.short	49
	.byte	0
	.byte	9
	.short	195
	.byte	16
	.byte	7
	.short	10
	.byte	0
	.byte	8
	.short	97
	.byte	0
	.byte	8
	.short	33
	.byte	0
	.byte	9
	.short	163
	.byte	0
	.byte	8
	.short	1
	.byte	0
	.byte	8
	.short	129
	.byte	0
	.byte	8
	.short	65
	.byte	0
	.byte	9
	.short	227
	.byte	16
	.byte	7
	.short	6
	.byte	0
	.byte	8
	.short	89
	.byte	0
	.byte	8
	.short	25
	.byte	0
	.byte	9
	.short	147
	.byte	19
	.byte	7
	.short	59
	.byte	0
	.byte	8
	.short	121
	.byte	0
	.byte	8
	.short	57
	.byte	0
	.byte	9
	.short	211
	.byte	17
	.byte	7
	.short	17
	.byte	0
	.byte	8
	.short	105
	.byte	0
	.byte	8
	.short	41
	.byte	0
	.byte	9
	.short	179
	.byte	0
	.byte	8
	.short	9
	.byte	0
	.byte	8
	.short	137
	.byte	0
	.byte	8
	.short	73
	.byte	0
	.byte	9
	.short	243
	.byte	16
	.byte	7
	.short	4
	.byte	0
	.byte	8
	.short	85
	.byte	0
	.byte	8
	.short	21
	.byte	16
	.byte	8
	.short	258
	.byte	19
	.byte	7
	.short	43
	.byte	0
	.byte	8
	.short	117
	.byte	0
	.byte	8
	.short	53
	.byte	0
	.byte	9
	.short	203
	.byte	17
	.byte	7
	.short	13
	.byte	0
	.byte	8
	.short	101
	.byte	0
	.byte	8
	.short	37
	.byte	0
	.byte	9
	.short	171
	.byte	0
	.byte	8
	.short	5
	.byte	0
	.byte	8
	.short	133
	.byte	0
	.byte	8
	.short	69
	.byte	0
	.byte	9
	.short	235
	.byte	16
	.byte	7
	.short	8
	.byte	0
	.byte	8
	.short	93
	.byte	0
	.byte	8
	.short	29
	.byte	0
	.byte	9
	.short	155
	.byte	20
	.byte	7
	.short	83
	.byte	0
	.byte	8
	.short	125
	.byte	0
	.byte	8
	.short	61
	.byte	0
	.byte	9
	.short	219
	.byte	18
	.byte	7
	.short	23
	.byte	0
	.byte	8
	.short	109
	.byte	0
	.byte	8
	.short	45
	.byte	0
	.byte	9
	.short	187
	.byte	0
	.byte	8
	.short	13
	.byte	0
	.byte	8
	.short	141
	.byte	0
	.byte	8
	.short	77
	.byte	0
	.byte	9
	.short	251
	.byte	16
	.byte	7
	.short	3
	.byte	0
	.byte	8
	.short	83
	.byte	0
	.byte	8
	.short	19
	.byte	21
	.byte	8
	.short	195
	.byte	19
	.byte	7
	.short	35
	.byte	0
	.byte	8
	.short	115
	.byte	0
	.byte	8
	.short	51
	.byte	0
	.byte	9
	.short	199
	.byte	17
	.byte	7
	.short	11
	.byte	0
	.byte	8
	.short	99
	.byte	0
	.byte	8
	.short	35
	.byte	0
	.byte	9
	.short	167
	.byte	0
	.byte	8
	.short	3
	.byte	0
	.byte	8
	.short	131
	.byte	0
	.byte	8
	.short	67
	.byte	0
	.byte	9
	.short	231
	.byte	16
	.byte	7
	.short	7
	.byte	0
	.byte	8
	.short	91
	.byte	0
	.byte	8
	.short	27
	.byte	0
	.byte	9
	.short	151
	.byte	20
	.byte	7
	.short	67
	.byte	0
	.byte	8
	.short	123
	.byte	0
	.byte	8
	.short	59
	.byte	0
	.byte	9
	.short	215
	.byte	18
	.byte	7
	.short	19
	.byte	0
	.byte	8
	.short	107
	.byte	0
	.byte	8
	.short	43
	.byte	0
	.byte	9
	.short	183
	.byte	0
	.byte	8
	.short	11
	.byte	0
	.byte	8
	.short	139
	.byte	0
	.byte	8
	.short	75
	.byte	0
	.byte	9
	.short	247
	.byte	16
	.byte	7
	.short	5
	.byte	0
	.byte	8
	.short	87
	.byte	0
	.byte	8
	.short	23
	.byte	64
	.byte	8
	.short	0
	.byte	19
	.byte	7
	.short	51
	.byte	0
	.byte	8
	.short	119
	.byte	0
	.byte	8
	.short	55
	.byte	0
	.byte	9
	.short	207
	.byte	17
	.byte	7
	.short	15
	.byte	0
	.byte	8
	.short	103
	.byte	0
	.byte	8
	.short	39
	.byte	0
	.byte	9
	.short	175
	.byte	0
	.byte	8
	.short	7
	.byte	0
	.byte	8
	.short	135
	.byte	0
	.byte	8
	.short	71
	.byte	0
	.byte	9
	.short	239
	.byte	16
	.byte	7
	.short	9
	.byte	0
	.byte	8
	.short	95
	.byte	0
	.byte	8
	.short	31
	.byte	0
	.byte	9
	.short	159
	.byte	20
	.byte	7
	.short	99
	.byte	0
	.byte	8
	.short	127
	.byte	0
	.byte	8
	.short	63
	.byte	0
	.byte	9
	.short	223
	.byte	18
	.byte	7
	.short	27
	.byte	0
	.byte	8
	.short	111
	.byte	0
	.byte	8
	.short	47
	.byte	0
	.byte	9
	.short	191
	.byte	0
	.byte	8
	.short	15
	.byte	0
	.byte	8
	.short	143
	.byte	0
	.byte	8
	.short	79
	.byte	0
	.byte	9
	.short	255
	.type	distfix.2061, %object
	.size	distfix.2061, 128
distfix.2061:
	.byte	16
	.byte	5
	.short	1
	.byte	23
	.byte	5
	.short	257
	.byte	19
	.byte	5
	.short	17
	.byte	27
	.byte	5
	.short	4097
	.byte	17
	.byte	5
	.short	5
	.byte	25
	.byte	5
	.short	1025
	.byte	21
	.byte	5
	.short	65
	.byte	29
	.byte	5
	.short	16385
	.byte	16
	.byte	5
	.short	3
	.byte	24
	.byte	5
	.short	513
	.byte	20
	.byte	5
	.short	33
	.byte	28
	.byte	5
	.short	8193
	.byte	18
	.byte	5
	.short	9
	.byte	26
	.byte	5
	.short	2049
	.byte	22
	.byte	5
	.short	129
	.byte	64
	.byte	5
	.short	0
	.byte	16
	.byte	5
	.short	2
	.byte	23
	.byte	5
	.short	385
	.byte	19
	.byte	5
	.short	25
	.byte	27
	.byte	5
	.short	6145
	.byte	17
	.byte	5
	.short	7
	.byte	25
	.byte	5
	.short	1537
	.byte	21
	.byte	5
	.short	97
	.byte	29
	.byte	5
	.short	24577
	.byte	16
	.byte	5
	.short	4
	.byte	24
	.byte	5
	.short	769
	.byte	20
	.byte	5
	.short	49
	.byte	28
	.byte	5
	.short	12289
	.byte	18
	.byte	5
	.short	13
	.byte	26
	.byte	5
	.short	3073
	.byte	22
	.byte	5
	.short	193
	.byte	64
	.byte	5
	.short	0
	.type	order.2129, %object
	.size	order.2129, 38
order.2129:
	.short	16
	.short	17
	.short	18
	.short	0
	.short	8
	.short	7
	.short	9
	.short	6
	.short	10
	.short	5
	.short	11
	.short	4
	.short	12
	.short	3
	.short	13
	.short	2
	.short	14
	.short	1
	.short	15
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"incorrect header check\000"
	.space	1
.LC1:
	.ascii	"unknown compression method\000"
	.space	1
.LC2:
	.ascii	"invalid window size\000"
.LC3:
	.ascii	"unknown header flags set\000"
	.space	3
.LC4:
	.ascii	"header crc mismatch\000"
.LC5:
	.ascii	"invalid block type\000"
	.space	1
.LC6:
	.ascii	"invalid stored block lengths\000"
	.space	3
.LC7:
	.ascii	"too many length or distance symbols\000"
.LC8:
	.ascii	"invalid code lengths set\000"
	.space	3
.LC9:
	.ascii	"invalid bit length repeat\000"
	.space	2
.LC10:
	.ascii	"invalid literal/lengths set\000"
.LC11:
	.ascii	"invalid distances set\000"
	.space	2
.LC12:
	.ascii	"invalid literal/length code\000"
.LC13:
	.ascii	"invalid distance code\000"
	.space	2
.LC14:
	.ascii	"invalid distance too far back\000"
	.space	2
.LC15:
	.ascii	"incorrect data check\000"
	.space	3
.LC16:
	.ascii	"incorrect length check\000"
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
