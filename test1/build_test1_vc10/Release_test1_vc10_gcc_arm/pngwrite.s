	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"pngwrite.c"
	.section	.text.png_convert_from_struct_tm,"ax",%progbits
	.align	2
	.global	png_convert_from_struct_tm
	.hidden	png_convert_from_struct_tm
	.type	png_convert_from_struct_tm, %function
png_convert_from_struct_tm:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	ip, [r1, #20]
	add	r2, ip, #1888
	add	r3, r2, #12
	strh	r3, [r0, #0]	@ movhi
	ldr	ip, [r1, #16]
	add	r2, ip, #1
	strb	r2, [r0, #2]
	ldr	r3, [r1, #12]
	strb	r3, [r0, #3]
	ldr	ip, [r1, #8]
	strb	ip, [r0, #4]
	ldr	r2, [r1, #4]
	strb	r2, [r0, #5]
	ldr	r3, [r1, #0]
	strb	r3, [r0, #6]
	bx	lr
	.size	png_convert_from_struct_tm, .-png_convert_from_struct_tm
	.section	.text.png_set_flush,"ax",%progbits
	.align	2
	.global	png_set_flush
	.hidden	png_set_flush
	.type	png_set_flush, %function
png_set_flush:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	bicne	r1, r1, r1, asr #31
	strne	r1, [r0, #364]
	bx	lr
	.size	png_set_flush, .-png_set_flush
	.section	.text.png_set_compression_level,"ax",%progbits
	.align	2
	.global	png_set_compression_level
	.hidden	png_set_compression_level
	.type	png_set_compression_level, %function
png_set_compression_level:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	ldrne	r3, [r0, #136]
	orrne	r3, r3, #2
	strne	r1, [r0, #208]
	strne	r3, [r0, #136]
	bx	lr
	.size	png_set_compression_level, .-png_set_compression_level
	.section	.text.png_set_compression_mem_level,"ax",%progbits
	.align	2
	.global	png_set_compression_mem_level
	.hidden	png_set_compression_mem_level
	.type	png_set_compression_mem_level, %function
png_set_compression_mem_level:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	ldrne	r3, [r0, #136]
	orrne	r3, r3, #4
	strne	r1, [r0, #220]
	strne	r3, [r0, #136]
	bx	lr
	.size	png_set_compression_mem_level, .-png_set_compression_mem_level
	.section	.text.png_set_compression_strategy,"ax",%progbits
	.align	2
	.global	png_set_compression_strategy
	.hidden	png_set_compression_strategy
	.type	png_set_compression_strategy, %function
png_set_compression_strategy:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	ldrne	r3, [r0, #136]
	orrne	r3, r3, #1
	strne	r1, [r0, #224]
	strne	r3, [r0, #136]
	bx	lr
	.size	png_set_compression_strategy, .-png_set_compression_strategy
	.section	.text.png_set_write_status_fn,"ax",%progbits
	.align	2
	.global	png_set_write_status_fn
	.hidden	png_set_write_status_fn
	.type	png_set_write_status_fn, %function
png_set_write_status_fn:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	strne	r1, [r0, #440]
	bx	lr
	.size	png_set_write_status_fn, .-png_set_write_status_fn
	.section	.text.png_set_write_user_transform_fn,"ax",%progbits
	.align	2
	.global	png_set_write_user_transform_fn
	.hidden	png_set_write_user_transform_fn
	.type	png_set_write_user_transform_fn, %function
png_set_write_user_transform_fn:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	ldrne	r3, [r0, #140]
	orrne	r3, r3, #1048576
	strne	r1, [r0, #120]
	strne	r3, [r0, #140]
	bx	lr
	.size	png_set_write_user_transform_fn, .-png_set_write_user_transform_fn
	.section	.text.png_set_compression_method,"ax",%progbits
	.align	2
	.global	png_set_compression_method
	.hidden	png_set_compression_method
	.type	png_set_compression_method, %function
png_set_compression_method:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	subs	r4, r0, #0
	mov	r5, r1
	beq	.L24
	cmp	r1, #8
	ldrne	r1, .L26
	blne	png_warning
.L23:
	ldr	r0, [r4, #136]
	orr	r3, r0, #16
	str	r5, [r4, #212]
	str	r3, [r4, #136]
.L24:
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L27:
	.align	2
.L26:
	.word	.LC0
	.size	png_set_compression_method, .-png_set_compression_method
	.section	.text.png_set_compression_window_bits,"ax",%progbits
	.align	2
	.global	png_set_compression_window_bits
	.hidden	png_set_compression_window_bits
	.type	png_set_compression_window_bits, %function
png_set_compression_window_bits:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	subs	r5, r0, #0
	mov	r4, r1
	beq	.L33
	cmp	r1, #15
	bgt	.L34
	cmp	r1, #7
	ble	.L35
	cmp	r1, #8
	beq	.L36
.L31:
	ldr	r0, [r5, #136]
	orr	r3, r0, #8
	str	r4, [r5, #216]
	str	r3, [r5, #136]
.L33:
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L34:
	ldr	r1, .L37
	bl	png_warning
	b	.L31
.L35:
	ldr	r1, .L37+4
	bl	png_warning
	b	.L31
.L36:
	ldr	r1, .L37+8
	bl	png_warning
	mov	r4, #9
	b	.L31
.L38:
	.align	2
.L37:
	.word	.LC1
	.word	.LC2
	.word	.LC3
	.size	png_set_compression_window_bits, .-png_set_compression_window_bits
	.global	__aeabi_dcmplt
	.global	__aeabi_dmul
	.global	__aeabi_dadd
	.global	__aeabi_d2uiz
	.global	__aeabi_ddiv
	.global	__aeabi_dcmpge
	.section	.text.png_set_filter_heuristics,"ax",%progbits
	.align	2
	.global	png_set_filter_heuristics
	.hidden	png_set_filter_heuristics
	.type	png_set_filter_heuristics, %function
png_set_filter_heuristics:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	subs	r4, r0, #0
	sub	sp, sp, #12
	mov	r8, r2
	mov	r5, r3
	ldr	r9, [sp, #48]
	beq	.L73
	cmp	r1, #2
	bgt	.L119
	cmp	r1, #0
	moveq	r3, #1
	moveq	r1, r3
	andne	r3, r1, #255
	rsbs	r2, r5, #1
	movcc	r2, #0
	orrs	r2, r2, r8, lsr #31
	bne	.L44
	cmp	r1, #1
	beq	.L44
	cmp	r8, #0
	strb	r3, [r4, #532]
	strb	r8, [r4, #533]
	beq	.L45
	ldr	r6, [r4, #536]
	cmp	r6, #0
	beq	.L120
.L46:
	ldr	r6, [r4, #540]
	cmp	r6, #0
	beq	.L48
.L50:
	ldmia	r5, {r6-r7}
	mov	r2, #0
	mov	r0, r6
	mov	r1, r7
	mov	r3, #0
	bl	__aeabi_dcmplt
	sub	sl, r8, #1
	cmp	r0, #0
	and	sl, sl, #1
	beq	.L121
	ldr	r0, [r4, #544]
	ldr	r2, [r4, #540]
	mov	r1, #256	@ movhi
	strh	r1, [r2, #0]	@ movhi
	strh	r1, [r0, #0]	@ movhi
.L116:
	mov	r7, #1
	cmp	r8, r7
	add	r5, r5, #8
	mov	r6, #2
	ble	.L45
	cmp	sl, #0
	beq	.L49
	ldmia	r5, {sl-fp}
	mov	r2, #0
	mov	r3, #0
	mov	r0, sl
	mov	r1, fp
	bl	__aeabi_dcmplt
	cmp	r0, #0
	bne	.L84
	mov	lr, #1073741824
	add	r3, lr, #7340032
	mov	r2, #0
	mov	r0, sl
	mov	r1, fp
	bl	__aeabi_dmul
	mov	ip, #1069547520
	add	r3, ip, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	ldr	r3, [r4, #544]
	strh	r0, [r3, r6]	@ movhi
	mov	r1, #1073741824
	ldmia	r5, {r2-r3}
	mov	r0, #0
	add	r1, r1, #7340032
	bl	__aeabi_ddiv
	mov	ip, #1069547520
	add	r3, ip, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	ldr	r3, [r4, #540]
	strh	r0, [r3, r6]	@ movhi
.L117:
	add	r7, r7, #1
	cmp	r8, r7
	add	r5, r5, #8
	add	r6, r6, #2
	bgt	.L49
	b	.L45
.L123:
	add	r2, r4, #540
	ldmia	r2, {r2, r3}	@ phole ldm
	mov	fp, #256	@ movhi
	strh	fp, [r2, r6]	@ movhi
	strh	fp, [r6, r3]	@ movhi
.L54:
	add	ip, r5, #8
	ldmia	ip, {fp-ip}
	mov	r2, #0
	mov	r1, ip
	mov	r3, #0
	mov	r0, fp
	str	ip, [sp, #4]
	bl	__aeabi_dcmplt
	cmp	r0, #0
	add	r6, r6, #2
	add	r7, r7, #1
	add	sl, r5, #8
	ldr	ip, [sp, #4]
	beq	.L122
	ldr	r1, [r4, #544]
	ldr	r2, [r4, #540]
	mov	r0, #256	@ movhi
	strh	r0, [r2, r6]	@ movhi
	strh	r0, [r6, r1]	@ movhi
.L118:
	add	r7, r7, #1
	cmp	r8, r7
	add	r5, sl, #8
	add	r6, r6, #2
	ble	.L45
.L49:
	ldmia	r5, {sl-fp}
	mov	r2, #0
	mov	r3, #0
	mov	r1, fp
	mov	r0, sl
	bl	__aeabi_dcmplt
	mov	ip, #1073741824
	cmp	r0, #0
	mov	r2, #0
	add	r3, ip, #7340032
	mov	r0, sl
	mov	r1, fp
	bne	.L123
	bl	__aeabi_dmul
	mov	r3, #1069547520
	mov	r2, #0
	add	r3, r3, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	ldr	fp, [r4, #544]
	strh	r0, [fp, r6]	@ movhi
	mov	r1, #1073741824
	ldmia	r5, {r2-r3}
	mov	r0, #0
	add	r1, r1, #7340032
	bl	__aeabi_ddiv
	mov	ip, #1069547520
	mov	r2, #0
	add	r3, ip, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	ldr	sl, [r4, #540]
	strh	r0, [sl, r6]	@ movhi
	b	.L54
.L129:
	add	r1, r4, #548
	ldmia	r1, {r1, r3}	@ phole ldm
	mov	ip, #8	@ movhi
	strh	ip, [r1, #8]	@ movhi
	strh	ip, [r3, #8]	@ movhi
.L73:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L44:
	mov	ip, #0
	strb	ip, [r4, #533]
	strb	r3, [r4, #532]
.L45:
	ldr	r0, [r4, #548]
	cmp	r0, #0
	beq	.L55
.L72:
	cmp	r9, #0
	beq	.L124
	ldmia	r9, {r5-r6}
	mov	r2, #0
	mov	r3, #0
	mov	r0, r5
	mov	r1, r6
	bl	__aeabi_dcmplt
	cmp	r0, #0
	bne	.L57
	mov	lr, #1069547520
	mov	r0, r5
	mov	r1, r6
	mov	r2, #0
	add	r3, lr, #3145728
	bl	__aeabi_dcmpge
	cmp	r0, #0
	bne	.L125
.L58:
	add	r6, r9, #8
	ldmia	r6, {r5-r6}
	mov	r2, #0
	mov	r3, #0
	mov	r0, r5
	mov	r1, r6
	bl	__aeabi_dcmplt
	cmp	r0, #0
	bne	.L60
	mov	ip, #1069547520
	mov	r0, r5
	mov	r1, r6
	mov	r2, #0
	add	r3, ip, #3145728
	bl	__aeabi_dcmpge
	cmp	r0, #0
	bne	.L126
.L61:
	add	r6, r9, #16
	ldmia	r6, {r5-r6}
	mov	r2, #0
	mov	r3, #0
	mov	r0, r5
	mov	r1, r6
	bl	__aeabi_dcmplt
	cmp	r0, #0
	bne	.L63
	mov	r3, #1069547520
	mov	r0, r5
	mov	r1, r6
	mov	r2, #0
	add	r3, r3, #3145728
	bl	__aeabi_dcmpge
	cmp	r0, #0
	bne	.L127
.L64:
	add	r6, r9, #24
	ldmia	r6, {r5-r6}
	mov	r2, #0
	mov	r3, #0
	mov	r0, r5
	mov	r1, r6
	bl	__aeabi_dcmplt
	cmp	r0, #0
	bne	.L66
	mov	ip, #1069547520
	mov	r0, r5
	mov	r1, r6
	mov	r2, #0
	add	r3, ip, #3145728
	bl	__aeabi_dcmpge
	cmp	r0, #0
	bne	.L128
.L67:
	add	r6, r9, #32
	ldmia	r6, {r5-r6}
	mov	r2, #0
	mov	r3, #0
	mov	r0, r5
	mov	r1, r6
	bl	__aeabi_dcmplt
	cmp	r0, #0
	bne	.L129
	mov	lr, #1069547520
	mov	r0, r5
	mov	r1, r6
	mov	r2, #0
	add	r3, lr, #3145728
	bl	__aeabi_dcmpge
	cmp	r0, #0
	beq	.L73
	mov	r1, #1073741824
	mov	r2, r5
	mov	r3, r6
	mov	r0, #0
	add	r1, r1, #2097152
	bl	__aeabi_ddiv
	mov	ip, #1069547520
	add	r3, ip, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	ldr	r3, [r4, #552]
	strh	r0, [r3, #8]	@ movhi
	mov	ip, #1073741824
	add	r3, ip, #2097152
	add	r1, r9, #32
	ldmia	r1, {r0-r1}
	mov	r2, #0
	bl	__aeabi_dmul
	mov	r3, #1069547520
	mov	r2, #0
	add	r3, r3, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	ldr	ip, [r4, #548]
	strh	r0, [ip, #8]	@ movhi
	b	.L73
.L122:
	mov	lr, #1073741824
	add	r3, lr, #7340032
	mov	r1, ip
	mov	r2, #0
	mov	r0, fp
	bl	__aeabi_dmul
	mov	ip, #1069547520
	add	r3, ip, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	ldr	r3, [r4, #544]
	strh	r0, [r3, r6]	@ movhi
	mov	ip, #1073741824
	add	r3, r5, #8
	ldmia	r3, {r2-r3}
	add	r1, ip, #7340032
	mov	r0, #0
	bl	__aeabi_ddiv
	mov	r3, #1069547520
	mov	r2, #0
	add	r3, r3, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	ldr	r5, [r4, #540]
	strh	r0, [r5, r6]	@ movhi
	b	.L118
.L66:
	ldr	r0, [r4, #552]
	ldr	r2, [r4, #548]
	mov	lr, #8	@ movhi
	strh	lr, [r2, #6]	@ movhi
	strh	lr, [r0, #6]	@ movhi
	b	.L67
.L63:
	add	r2, r4, #548
	ldmia	r2, {r2, lr}	@ phole ldm
	mov	r0, #8	@ movhi
	strh	r0, [r2, #4]	@ movhi
	strh	r0, [lr, #4]	@ movhi
	b	.L64
.L60:
	add	r2, r4, #548
	ldmia	r2, {r2, lr}	@ phole ldm
	mov	r0, #8	@ movhi
	strh	r0, [r2, #2]	@ movhi
	strh	r0, [lr, #2]	@ movhi
	b	.L61
.L57:
	ldr	r0, [r4, #552]
	ldr	r2, [r4, #548]
	mov	lr, #8	@ movhi
	strh	lr, [r2, #0]	@ movhi
	strh	lr, [r0, #0]	@ movhi
	b	.L58
.L121:
	mov	lr, #1073741824
	add	r3, lr, #7340032
	mov	r0, r6
	mov	r1, r7
	mov	r2, #0
	mov	r7, #1069547520
	bl	__aeabi_dmul
	add	r3, r7, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	ldr	ip, [r4, #544]
	strh	r0, [ip, #0]	@ movhi
	mov	r3, #1073741824
	add	r1, r3, #7340032
	mov	r0, #0
	ldmia	r5, {r2-r3}
	mov	r6, #1069547520
	bl	__aeabi_ddiv
	mov	r2, #0
	add	r3, r6, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	ldr	r7, [r4, #540]
	strh	r0, [r7, #0]	@ movhi
	b	.L116
.L84:
	ldr	r1, [r4, #544]
	ldr	r2, [r4, #540]
	mov	r0, #256	@ movhi
	strh	r0, [r2, r6]	@ movhi
	strh	r0, [r6, r1]	@ movhi
	b	.L117
.L128:
	mov	r1, #1073741824
	mov	r2, r5
	mov	r3, r6
	mov	r0, #0
	mov	r6, #1069547520
	add	r1, r1, #2097152
	bl	__aeabi_ddiv
	add	r3, r6, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	ldr	ip, [r4, #552]
	strh	r0, [ip, #6]	@ movhi
	mov	r5, #1073741824
	mov	r2, #0
	add	r3, r5, #2097152
	add	r1, r9, #24
	ldmia	r1, {r0-r1}
	bl	__aeabi_dmul
	mov	r3, #1069547520
	mov	r2, #0
	add	r3, r3, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	ldr	r6, [r4, #548]
	strh	r0, [r6, #6]	@ movhi
	b	.L67
.L127:
	mov	r1, #1073741824
	mov	r2, r5
	mov	r3, r6
	mov	r5, #1069547520
	mov	r0, #0
	add	r1, r1, #2097152
	bl	__aeabi_ddiv
	add	r3, r5, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	ldr	r3, [r4, #552]
	strh	r0, [r3, #4]	@ movhi
	mov	r6, #1073741824
	mov	r2, #0
	add	r3, r6, #2097152
	add	r1, r9, #16
	ldmia	r1, {r0-r1}
	bl	__aeabi_dmul
	mov	ip, #1069547520
	mov	r2, #0
	add	r3, ip, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	ldr	r5, [r4, #548]
	strh	r0, [r5, #4]	@ movhi
	b	.L64
.L126:
	mov	r1, #1073741824
	mov	r2, r5
	mov	r3, r6
	mov	r0, #0
	mov	r6, #1069547520
	add	r1, r1, #2097152
	bl	__aeabi_ddiv
	add	r3, r6, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	ldr	ip, [r4, #552]
	strh	r0, [ip, #2]	@ movhi
	mov	r5, #1073741824
	mov	r2, #0
	add	r3, r5, #2097152
	add	r1, r9, #8
	ldmia	r1, {r0-r1}
	bl	__aeabi_dmul
	mov	r3, #1069547520
	mov	r2, #0
	add	r3, r3, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	ldr	r6, [r4, #548]
	strh	r0, [r6, #2]	@ movhi
	b	.L61
.L125:
	mov	r1, #1073741824
	mov	r2, r5
	mov	r3, r6
	mov	r5, #1069547520
	mov	r0, #0
	add	r1, r1, #2097152
	bl	__aeabi_ddiv
	add	r3, r5, #2097152
	mov	r2, #0
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	ldr	r3, [r4, #552]
	strh	r0, [r3, #0]	@ movhi
	mov	ip, #1073741824
	add	r3, ip, #2097152
	mov	r2, #0
	ldmia	r9, {r0-r1}
	mov	r6, #1069547520
	bl	__aeabi_dmul
	mov	r2, #0
	add	r3, r6, #2097152
	bl	__aeabi_dadd
	bl	__aeabi_d2uiz
	ldr	r5, [r4, #548]
	strh	r0, [r5, #0]	@ movhi
	b	.L58
.L119:
	ldr	r1, .L132
	bl	png_warning
	b	.L73
.L124:
	ldr	r1, [r4, #552]
	ldr	r2, [r4, #548]
	mov	ip, #8	@ movhi
	strh	ip, [r2, #0]	@ movhi
	strh	ip, [r1, #0]	@ movhi
	add	r0, r4, #548
	ldmia	r0, {r0, r3}	@ phole ldm
	strh	ip, [r0, #2]	@ movhi
	strh	ip, [r3, #2]	@ movhi
	ldr	r1, [r4, #552]
	ldr	r2, [r4, #548]
	strh	ip, [r2, #4]	@ movhi
	strh	ip, [r1, #4]	@ movhi
	add	r0, r4, #548
	ldmia	r0, {r0, r3}	@ phole ldm
	strh	ip, [r0, #6]	@ movhi
	strh	ip, [r3, #6]	@ movhi
	ldr	r2, [r4, #548]
	ldr	r1, [r4, #552]
	strh	ip, [r2, #8]	@ movhi
	strh	ip, [r1, #8]	@ movhi
	b	.L73
.L55:
	mov	r1, #10
	mov	r0, r4
	bl	png_malloc
	mov	r1, #10
	str	r0, [r4, #548]
	mov	r0, r4
	bl	png_malloc
	ldr	r2, [r4, #548]
	mov	ip, #8	@ movhi
	str	r0, [r4, #552]
	strh	ip, [r2, #0]	@ movhi
	strh	ip, [r0, #0]	@ movhi
	add	r1, r4, #548
	ldmia	r1, {r1, r3}	@ phole ldm
	strh	ip, [r1, #2]	@ movhi
	strh	ip, [r3, #2]	@ movhi
	ldr	r0, [r4, #552]
	ldr	r2, [r4, #548]
	strh	ip, [r2, #4]	@ movhi
	strh	ip, [r0, #4]	@ movhi
	add	r1, r4, #548
	ldmia	r1, {r1, r3}	@ phole ldm
	strh	ip, [r1, #6]	@ movhi
	strh	ip, [r3, #6]	@ movhi
	ldr	r0, [r4, #552]
	ldr	r2, [r4, #548]
	strh	ip, [r2, #8]	@ movhi
	strh	ip, [r0, #8]	@ movhi
	b	.L72
.L120:
	mov	r1, r8
	mov	r0, r4
	bl	png_malloc
	mvn	r3, #0
	add	r1, r3, r8
	ands	r2, r1, #3
	str	r0, [r4, #536]
	beq	.L47
	mov	r6, #1
	cmp	r8, r6
	strb	r3, [r0, #0]
	ble	.L46
	cmp	r2, r6
	ldr	r0, [r4, #536]
	beq	.L47
	cmp	r2, #2
	strneb	r3, [r0, r6]
	ldrne	r0, [r4, #536]
	movne	r6, #2
	strb	r3, [r0, r6]
	add	r6, r6, #1
	ldr	r0, [r4, #536]
	b	.L47
.L130:
	ldr	lr, [r4, #536]
	strb	r3, [lr, r2]
	ldr	sl, [r4, #536]
	add	ip, r2, #1
	strb	r3, [sl, ip]
	ldr	r0, [r4, #536]
	add	r7, r6, #3
	strb	r3, [r0, r7]
	ldr	r0, [r4, #536]
	add	r6, r6, #4
.L47:
	add	r2, r6, #1
	cmp	r8, r2
	strb	r3, [r0, r6]
	bgt	.L130
	b	.L46
.L48:
	mov	r7, r8, asl #1
	mov	r1, r7
	mov	r0, r4
	bl	png_malloc
	mov	r1, r7
	str	r0, [r4, #540]
	mov	r0, r4
	bl	png_malloc
	sub	r3, r8, #1
	ands	r3, r3, #3
	str	r0, [r4, #544]
	beq	.L51
	ldr	sl, [r4, #540]
	mov	ip, #256	@ movhi
	strh	ip, [sl, r6]	@ movhi
	strh	ip, [r0, r6]	@ movhi
	mov	r6, #1
	cmp	r8, r6
	ble	.L50
	cmp	r3, r6
	ldr	r0, [r4, #544]
	beq	.L51
	cmp	r3, #2
	ldrne	r3, [r4, #540]
	movne	r6, #2
	strneh	ip, [r3, r6]	@ movhi
	strneh	ip, [r0, r6]	@ movhi
	ldrne	r0, [r4, #544]
	ldr	ip, [r4, #540]
	mov	r3, r6, asl #1
	mov	r7, #256	@ movhi
	strh	r7, [ip, r3]	@ movhi
	strh	r7, [r0, r3]	@ movhi
	add	r6, r6, #1
	ldr	r0, [r4, #544]
	b	.L51
.L131:
	add	r7, r4, #540
	ldmia	r7, {r7, lr}	@ phole ldm
	mov	sl, r3, asl #1
	strh	ip, [r7, sl]	@ movhi
	strh	ip, [lr, sl]	@ movhi
	add	r0, r3, #1
	add	r1, r4, #540
	ldmia	r1, {r1, r7}	@ phole ldm
	mov	r2, r0, asl #1
	strh	ip, [r1, r2]	@ movhi
	strh	ip, [r7, r2]	@ movhi
	add	r3, r6, #3
	ldr	r0, [r4, #544]
	ldr	sl, [r4, #540]
	mov	lr, r3, asl #1
	strh	ip, [sl, lr]	@ movhi
	strh	ip, [r0, lr]	@ movhi
	ldr	r0, [r4, #544]
	add	r6, r6, #4
.L51:
	ldr	r1, [r4, #540]
	add	r3, r6, #1
	mov	r2, r6, asl #1
	mov	ip, #256	@ movhi
	cmp	r8, r3
	strh	ip, [r1, r2]	@ movhi
	strh	ip, [r0, r2]	@ movhi
	bgt	.L131
	b	.L50
.L133:
	.align	2
.L132:
	.word	.LC4
	.size	png_set_filter_heuristics, .-png_set_filter_heuristics
	.section	.text.png_set_filter,"ax",%progbits
	.align	2
	.global	png_set_filter
	.hidden	png_set_filter
	.type	png_set_filter, %function
png_set_filter:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	subs	r4, r0, #0
	beq	.L154
	ldr	ip, [r4, #588]
	mov	r3, ip, lsr #2
	cmp	r1, #64
	movne	r3, #0
	andeq	r3, r3, #1
	cmp	r3, #0
	bne	.L136
	cmp	r1, #0
	beq	.L136
	ldr	r1, .L162
	bl	png_error
.L154:
	ldmfd	sp!, {r4, lr}
	bx	lr
.L136:
	and	r2, r2, #255
	cmp	r2, #7
	ldrls	pc, [pc, r2, asl #2]
	b	.L138
.L145:
	.word	.L139
	.word	.L140
	.word	.L141
	.word	.L142
	.word	.L143
	.word	.L144
	.word	.L144
	.word	.L144
.L144:
	mov	r0, r4
	ldr	r1, .L162+4
	bl	png_warning
.L139:
	mov	r2, #8
	strb	r2, [r4, #321]
.L146:
	ldr	r3, [r4, #264]
	cmp	r3, #0
	beq	.L154
	tst	r2, #16
	beq	.L147
	ldr	r3, [r4, #268]
	cmp	r3, #0
	beq	.L155
.L147:
	tst	r2, #32
	beq	.L148
	ldr	r3, [r4, #272]
	cmp	r3, #0
	beq	.L156
.L148:
	tst	r2, #64
	beq	.L150
	ldr	r3, [r4, #276]
	cmp	r3, #0
	beq	.L157
.L150:
	tst	r2, #128
	bne	.L158
.L152:
	cmp	r2, #0
	moveq	r3, #8
	streqb	r3, [r4, #321]
	b	.L154
.L140:
	mov	r2, #16
	strb	r2, [r4, #321]
	b	.L146
.L141:
	mov	r2, #32
	strb	r2, [r4, #321]
	b	.L146
.L138:
	strb	r2, [r4, #321]
	b	.L146
.L142:
	mov	r2, #64
	strb	r2, [r4, #321]
	b	.L146
.L143:
	mvn	r0, #127
	strb	r0, [r4, #321]
	mov	r2, #128
	b	.L146
.L158:
	ldr	r3, [r4, #280]
	cmp	r3, #0
	bne	.L152
	ldr	r2, [r4, #260]
	cmp	r2, #0
	beq	.L159
	ldr	lr, [r4, #244]
	mov	r0, r4
	add	r1, lr, #1
	bl	png_malloc
	mov	r3, #4
	str	r0, [r4, #280]
	strb	r3, [r0, #0]
	ldrb	r2, [r4, #321]	@ zero_extendqisi2
	b	.L152
.L157:
	ldr	ip, [r4, #260]
	cmp	ip, #0
	beq	.L160
	ldr	r1, [r4, #244]
	mov	r0, r4
	add	r1, r1, #1
	bl	png_malloc
	mov	r3, #3
	str	r0, [r4, #276]
	strb	r3, [r0, #0]
	ldrb	r2, [r4, #321]	@ zero_extendqisi2
	b	.L150
.L156:
	ldr	r2, [r4, #260]
	cmp	r2, #0
	beq	.L161
	ldr	lr, [r4, #244]
	mov	r0, r4
	add	r1, lr, #1
	bl	png_malloc
	mov	r3, #2
	str	r0, [r4, #272]
	strb	r3, [r0, #0]
	ldrb	r2, [r4, #321]	@ zero_extendqisi2
	b	.L148
.L155:
	ldr	r1, [r4, #244]
	mov	r0, r4
	add	r1, r1, #1
	bl	png_malloc
	mov	r3, #1
	str	r0, [r4, #268]
	strb	r3, [r0, #0]
	ldrb	r2, [r4, #321]	@ zero_extendqisi2
	b	.L147
.L160:
	mov	r0, r4
	ldr	r1, .L162+8
	bl	png_warning
	ldrb	r0, [r4, #321]	@ zero_extendqisi2
	and	r2, r0, #191
	strb	r2, [r4, #321]
	b	.L150
.L161:
	mov	r0, r4
	ldr	r1, .L162+12
	bl	png_warning
	ldrb	r3, [r4, #321]	@ zero_extendqisi2
	and	r2, r3, #223
	strb	r2, [r4, #321]
	b	.L148
.L159:
	mov	r0, r4
	ldr	r1, .L162+16
	bl	png_warning
	ldrb	r3, [r4, #321]	@ zero_extendqisi2
	and	r2, r3, #127
	strb	r2, [r4, #321]
	b	.L152
.L163:
	.align	2
.L162:
	.word	.LC9
	.word	.LC5
	.word	.LC7
	.word	.LC6
	.word	.LC8
	.size	png_set_filter, .-png_set_filter
	.section	.text.png_write_destroy,"ax",%progbits
	.align	2
	.global	png_write_destroy
	.hidden	png_write_destroy
	.type	png_write_destroy, %function
png_write_destroy:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 96
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	mov	r4, r0
	sub	sp, sp, #96
	add	r0, r0, #144
	bl	deflateEnd
	mov	r0, r4
	ldr	r1, [r4, #200]
	bl	png_free
	mov	r0, r4
	ldr	r1, [r4, #264]
	bl	png_free
	mov	r0, r4
	ldr	r1, [r4, #260]
	bl	png_free
	mov	r0, r4
	ldr	r1, [r4, #268]
	bl	png_free
	mov	r0, r4
	ldr	r1, [r4, #272]
	bl	png_free
	mov	r0, r4
	ldr	r1, [r4, #276]
	bl	png_free
	mov	r0, r4
	ldr	r1, [r4, #280]
	bl	png_free
	mov	r0, r4
	ldr	r1, [r4, #556]
	bl	png_free
	mov	r0, r4
	ldr	r1, [r4, #536]
	bl	png_free
	mov	r0, r4
	ldr	r1, [r4, #540]
	bl	png_free
	mov	r0, r4
	ldr	r1, [r4, #544]
	bl	png_free
	mov	r0, r4
	ldr	r1, [r4, #548]
	bl	png_free
	add	r6, sp, #4
	mov	r0, r4
	ldr	r1, [r4, #552]
	mov	r5, #92
	bl	png_free
	mov	r1, r4
	mov	r2, r5
	mov	r0, r6
	bl	memcpy
	ldr	sl, [r4, #92]
	ldr	r8, [r4, #96]
	ldr	r7, [r4, #100]
	ldr	r9, [r4, #616]
	mov	r1, #0
	mov	r2, #680
	mov	r0, r4
	bl	memset
	str	sl, [r4, #92]
	str	r8, [r4, #96]
	str	r7, [r4, #100]
	str	r9, [r4, #616]
	mov	r0, r4
	mov	r1, r6
	mov	r2, r5
	bl	memcpy
	add	sp, sp, #96
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
	.size	png_write_destroy, .-png_write_destroy
	.section	.text.png_destroy_write_struct,"ax",%progbits
	.align	2
	.global	png_destroy_write_struct
	.hidden	png_destroy_write_struct
	.type	png_destroy_write_struct, %function
png_destroy_write_struct:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, lr}
	subs	r7, r0, #0
	ldrne	r4, [r7, #0]
	moveq	r4, r7
	moveq	sl, r7
	moveq	r8, r7
	ldrne	r8, [r4, #616]
	ldrne	sl, [r4, #608]
	cmp	r1, #0
	mov	r5, r1
	beq	.L169
	ldr	r6, [r1, #0]
	cmp	r6, #0
	beq	.L169
	cmp	r4, #0
	beq	.L170
	mov	r2, #32512
	mvn	r3, #0
	add	r2, r2, #255
	mov	r0, r4
	mov	r1, r6
	bl	png_free_data
	ldr	r3, [r4, #572]
	cmp	r3, #0
	bne	.L173
.L170:
	mov	r1, r8
	mov	r0, r6
	mov	r2, sl
	bl	png_destroy_struct_2
	mov	r1, #0
	str	r1, [r5, #0]
.L169:
	cmp	r4, #0
	beq	.L172
	mov	r0, r4
	bl	png_write_destroy
	mov	r0, r4
	mov	r1, r8
	mov	r2, sl
	bl	png_destroy_struct_2
	mov	ip, #0
	str	ip, [r7, #0]
.L172:
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, lr}
	bx	lr
.L173:
	mov	r0, r4
	ldr	r1, [r4, #576]
	bl	png_free
	mov	r0, #0
	str	r0, [r4, #572]
	str	r0, [r4, #576]
	b	.L170
	.size	png_destroy_write_struct, .-png_destroy_write_struct
	.section	.text.png_write_flush,"ax",%progbits
	.align	2
	.global	png_write_flush
	.hidden	png_write_flush
	.type	png_write_flush, %function
png_write_flush:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	subs	r4, r0, #0
	beq	.L181
	ldr	r2, [r4, #256]
	ldr	r3, [r4, #236]
	cmp	r2, r3
	bcs	.L181
	add	r5, r4, #144
.L179:
	mov	r1, #2
	mov	r0, r5
	bl	deflate
	cmp	r0, #0
	beq	.L176
	ldr	r1, [r4, #168]
	cmp	r1, #0
	mov	r0, r4
	ldreq	r1, .L183
	bl	png_error
.L176:
	ldr	r3, [r4, #160]
	cmp	r3, #0
	beq	.L182
	ldr	r2, [r4, #204]
	cmp	r2, r3
	beq	.L180
	rsb	r2, r3, r2
	mov	r0, r4
	ldr	r1, [r4, #200]
	bl	png_write_IDAT
	add	r2, r4, #200
	ldmia	r2, {r2, ip}	@ phole ldm
	str	r2, [r4, #156]
	str	ip, [r4, #160]
.L180:
	mov	lr, #0
	str	lr, [r4, #368]
	mov	r0, r4
	bl	png_flush
.L181:
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L182:
	mov	r0, r4
	add	r1, r4, #200
	ldmia	r1, {r1, r2}	@ phole ldm
	bl	png_write_IDAT
	ldr	r1, [r4, #200]
	ldr	r0, [r4, #204]
	str	r1, [r4, #156]
	str	r0, [r4, #160]
	b	.L179
.L184:
	.align	2
.L183:
	.word	.LC10
	.size	png_write_flush, .-png_write_flush
	.section	.text.png_write_row,"ax",%progbits
	.align	2
	.global	png_write_row
	.hidden	png_write_row
	.type	png_write_row, %function
png_write_row:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	subs	r4, r0, #0
	mov	r5, r1
	beq	.L209
	ldr	r3, [r4, #256]
	cmp	r3, #0
	bne	.L187
	ldrb	r1, [r4, #320]	@ zero_extendqisi2
	cmp	r1, #0
	bne	.L187
	ldr	r2, [r4, #132]
	tst	r2, #1024
	beq	.L213
.L188:
	mov	r0, r4
	bl	png_write_start_row
.L187:
	ldrb	r0, [r4, #319]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L189
	ldr	ip, [r4, #140]
	tst	ip, #2
	beq	.L189
	ldrb	lr, [r4, #320]	@ zero_extendqisi2
	cmp	lr, #6
	ldrls	pc, [pc, lr, asl #2]
	b	.L189
.L197:
	.word	.L190
	.word	.L191
	.word	.L192
	.word	.L193
	.word	.L194
	.word	.L195
	.word	.L196
.L195:
	ldr	r0, [r4, #256]
	tst	r0, #1
	bne	.L206
	ldr	ip, [r4, #228]
	cmp	ip, #1
	bls	.L206
.L189:
	ldrb	r0, [r4, #327]	@ zero_extendqisi2
	ldrb	lr, [r4, #324]	@ zero_extendqisi2
	mul	r3, r0, lr
	and	r2, r3, #255
	ldr	r3, [r4, #240]
	cmp	r2, #7
	str	r3, [r4, #284]
	mulls	r3, r2, r3
	strb	r2, [r4, #295]
	movhi	r2, r2, lsr #3
	mulhi	r3, r2, r3
	addls	r3, r3, #7
	ldrb	ip, [r4, #322]	@ zero_extendqisi2
	ldr	r1, [r4, #264]
	movls	r3, r3, lsr #3
	strb	r0, [r4, #294]
	str	r3, [r4, #288]
	strb	ip, [r4, #292]
	strb	lr, [r4, #293]
	add	r1, r1, #1
	mov	r2, r5
	mov	r0, r4
	bl	png_memcpy_check
	ldrb	r3, [r4, #319]	@ zero_extendqisi2
	cmp	r3, #0
	ldreq	r3, [r4, #140]
	bne	.L214
.L212:
	add	r5, r4, #284
.L205:
	cmp	r3, #0
	bne	.L215
.L207:
	ldr	r2, [r4, #588]
	tst	r2, #4
	bne	.L216
.L208:
	mov	r1, r5
	mov	r0, r4
	bl	png_write_find_filter
	ldr	r3, [r4, #440]
	cmp	r3, #0
	movne	r0, r4
	ldrne	r1, [r4, #256]
	ldrneb	r2, [r4, #320]	@ zero_extendqisi2
	movne	lr, pc
	bxne	r3
.L209:
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L214:
	ldrb	r2, [r4, #320]	@ zero_extendqisi2
	cmp	r2, #5
	ldr	r3, [r4, #140]
	addhi	r5, r4, #284
	bhi	.L205
	tst	r3, #2
	beq	.L212
	ldr	r1, [r4, #264]
	add	r5, r4, #284
	add	r1, r1, #1
	mov	r0, r5
	bl	png_do_write_interlace
	ldr	r3, [r4, #284]
	cmp	r3, #0
	ldrne	r3, [r4, #140]
	bne	.L205
.L206:
	mov	r0, r4
	bl	png_write_finish_row
	b	.L209
.L216:
	ldrb	lr, [r4, #596]	@ zero_extendqisi2
	cmp	lr, #64
	bne	.L208
	ldr	r0, [r4, #264]
	add	r1, r0, #1
	mov	r0, r5
	bl	png_do_write_intrapixel
	b	.L208
.L215:
	mov	r0, r4
	bl	png_do_write_transformations
	b	.L207
.L213:
	ldr	r1, .L217
	bl	png_error
	b	.L188
.L196:
	ldr	lr, [r4, #256]
	tst	lr, #1
	bne	.L189
	b	.L206
.L190:
	ldr	r3, [r4, #256]
	tst	r3, #7
	beq	.L189
	b	.L206
.L191:
	ldr	r1, [r4, #256]
	tst	r1, #7
	bne	.L206
	ldr	r2, [r4, #228]
	cmp	r2, #4
	bhi	.L189
	b	.L206
.L192:
	ldr	ip, [r4, #256]
	and	r0, ip, #7
	cmp	r0, #4
	beq	.L189
	b	.L206
.L193:
	ldr	lr, [r4, #256]
	tst	lr, #3
	bne	.L206
	ldr	r3, [r4, #228]
	cmp	r3, #2
	bhi	.L189
	b	.L206
.L194:
	ldr	r2, [r4, #256]
	and	r1, r2, #3
	cmp	r1, #2
	beq	.L189
	b	.L206
.L218:
	.align	2
.L217:
	.word	.LC11
	.size	png_write_row, .-png_write_row
	.section	.text.png_write_rows,"ax",%progbits
	.align	2
	.global	png_write_rows
	.hidden	png_write_rows
	.type	png_write_rows, %function
png_write_rows:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	subs	r5, r0, #0
	mov	r4, r1
	mov	r7, r2
	beq	.L222
	cmp	r2, #0
	beq	.L222
	ldr	r1, [r1, #0]
	sub	r8, r2, #1
	bl	png_write_row
	cmp	r7, #1
	and	r8, r8, #3
	mov	r6, #1
	bls	.L222
	cmp	r8, #0
	beq	.L221
	cmp	r8, #1
	beq	.L235
	cmp	r8, #2
	beq	.L236
	mov	r0, r5
	ldr	r1, [r4, #4]
	bl	png_write_row
	mov	r6, #2
.L236:
	ldr	r1, [r4, r6, asl #2]
	mov	r0, r5
	bl	png_write_row
	add	r6, r6, #1
.L235:
	ldr	r1, [r4, r6, asl #2]
	mov	r0, r5
	add	r6, r6, #1
	bl	png_write_row
	cmp	r7, r6
	bls	.L222
.L221:
	ldr	r1, [r4, r6, asl #2]
	add	r8, r6, #1
	mov	r0, r5
	bl	png_write_row
	ldr	r1, [r4, r8, asl #2]
	mov	r0, r5
	add	r8, r8, #1
	bl	png_write_row
	ldr	r1, [r4, r8, asl #2]
	mov	r0, r5
	bl	png_write_row
	add	r3, r6, #3
	ldr	r1, [r4, r3, asl #2]
	add	r6, r6, #4
	mov	r0, r5
	bl	png_write_row
	cmp	r7, r6
	bhi	.L221
.L222:
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
	.size	png_write_rows, .-png_write_rows
	.section	.text.png_write_image,"ax",%progbits
	.align	2
	.global	png_write_image
	.hidden	png_write_image
	.type	png_write_image, %function
png_write_image:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	subs	r5, r0, #0
	mov	r6, r1
	beq	.L243
	bl	png_set_interlace_handling
	subs	r8, r0, #0
	ble	.L243
	ldr	r3, [r5, #232]
	mov	r7, #0
.L239:
	cmp	r3, #0
	movne	r4, #0
	beq	.L242
.L240:
	ldr	r1, [r6, r4, asl #2]
	mov	r0, r5
	bl	png_write_row
	ldr	r3, [r5, #232]
	add	r4, r4, #1
	cmp	r3, r4
	bhi	.L240
.L242:
	add	r7, r7, #1
	cmp	r8, r7
	bgt	.L239
.L243:
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
	.size	png_write_image, .-png_write_image
	.section	.text.png_convert_from_time_t,"ax",%progbits
	.align	2
	.global	png_convert_from_time_t
	.hidden	png_convert_from_time_t
	.type	png_convert_from_time_t, %function
png_convert_from_time_t:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	sub	sp, sp, #8
	add	lr, sp, #8
	str	r1, [lr, #-4]!
	mov	r4, r0
	mov	r0, lr
	bl	gmtime
	ldr	r3, [r0, #20]
	add	ip, r3, #1888
	add	r2, ip, #12
	strh	r2, [r4, #0]	@ movhi
	ldr	r1, [r0, #16]
	add	r3, r1, #1
	strb	r3, [r4, #2]
	ldr	ip, [r0, #12]
	strb	ip, [r4, #3]
	ldr	r2, [r0, #8]
	strb	r2, [r4, #4]
	ldr	r1, [r0, #4]
	strb	r1, [r4, #5]
	ldr	r3, [r0, #0]
	strb	r3, [r4, #6]
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	bx	lr
	.size	png_convert_from_time_t, .-png_convert_from_time_t
	.section	.text.png_write_end,"ax",%progbits
	.align	2
	.global	png_write_end
	.hidden	png_write_end
	.type	png_write_end, %function
png_write_end:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	subs	r7, r0, #0
	sub	sp, sp, #12
	mov	r5, r1
	beq	.L260
	ldr	r3, [r7, #132]
	tst	r3, #4
	beq	.L264
.L249:
	cmp	r5, #0
	beq	.L250
	ldr	r0, [r5, #8]
	tst	r0, #512
	bne	.L265
.L251:
	ldr	r2, [r5, #48]
	cmp	r2, #0
	ble	.L252
	mov	r4, #0
	mvn	r8, #2
	mvn	sl, #1
	b	.L256
.L266:
	mov	r0, r7
	ldr	r1, .L267
	bl	png_warning
	ldr	r2, [r5, #56]
	str	r8, [r2, r4, asl #4]
	ldr	r2, [r5, #48]
.L254:
	add	r4, r4, #1
	cmp	r2, r4
	ble	.L252
.L256:
	ldr	lr, [r5, #56]
	ldr	ip, [lr, r4, asl #4]
	mov	r6, r4, asl #4
	cmp	ip, #0
	add	r3, r6, lr
	bgt	.L266
	bne	.L255
	ldmib	r3, {r1, r2}	@ phole ldm
	mov	r0, r7
	mov	r3, ip
	str	ip, [sp, #0]
	bl	png_write_zTXt
	ldr	ip, [r5, #56]
	str	sl, [ip, r6]
	ldr	r2, [r5, #48]
	add	r4, r4, #1
	cmp	r2, r4
	bgt	.L256
.L252:
	ldr	r3, [r5, #192]
	cmp	r3, #0
	beq	.L250
	ldr	r4, [r5, #188]
	add	r1, r3, r3, asl #2
	add	r0, r4, r1, asl #2
	cmp	r0, r4
	bls	.L250
.L261:
	mov	r1, r4
	mov	r0, r7
	bl	png_handle_as_unknown
	cmp	r0, #1
	beq	.L257
	ldrb	r3, [r4, #16]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L257
	tst	r3, #8
	beq	.L257
	ldrb	ip, [r4, #3]	@ zero_extendqisi2
	mov	r2, ip, lsr #5
	cmp	r0, #3
	movne	lr, r2
	orreq	lr, r2, #1
	tst	lr, #1
	mov	r1, r4
	mov	r0, r7
	bne	.L258
	ldr	r3, [r7, #136]
	tst	r3, #65536
	beq	.L257
.L258:
	add	r2, r4, #8
	ldmia	r2, {r2, r3}	@ phole ldm
	bl	png_write_chunk
.L257:
	ldr	r2, [r5, #192]
	ldr	lr, [r5, #188]
	add	r1, r2, r2, asl #2
	add	r0, lr, r1, asl #2
	add	r4, r4, #20
	cmp	r0, r4
	bhi	.L261
.L250:
	ldr	r3, [r7, #132]
	orr	ip, r3, #8
	str	ip, [r7, #132]
	mov	r0, r7
	bl	png_write_IEND
.L260:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	bx	lr
.L255:
	cmn	ip, #1
	bne	.L254
	ldmib	r3, {r1, r2}	@ phole ldm
	mov	r0, r7
	mov	r3, #0
	bl	png_write_tEXt
	ldr	r3, [r5, #56]
	str	r8, [r3, r6]
	ldr	r2, [r5, #48]
	b	.L254
.L265:
	ldr	r1, [r7, #132]
	tst	r1, #512
	bne	.L251
	mov	r0, r7
	add	r1, r5, #60
	bl	png_write_tIME
	b	.L251
.L264:
	ldr	r1, .L267+4
	bl	png_error
	b	.L249
.L268:
	.align	2
.L267:
	.word	.LC13
	.word	.LC12
	.size	png_write_end, .-png_write_end
	.global	__aeabi_f2d
	.section	.text.png_write_info_before_PLTE,"ax",%progbits
	.align	2
	.global	png_write_info_before_PLTE
	.hidden	png_write_info_before_PLTE
	.type	png_write_info_before_PLTE, %function
png_write_info_before_PLTE:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	cmp	r0, #0
	cmpne	r1, #0
	sub	sp, sp, #56
	mov	r4, r1
	mov	r6, r0
	beq	.L282
	ldr	r5, [r0, #132]
	ands	r5, r5, #1024
	beq	.L285
.L282:
	add	sp, sp, #56
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L285:
	bl	png_write_sig
	ldr	r3, [r6, #132]
	tst	r3, #4096
	beq	.L271
	ldr	r0, [r6, #588]
	cmp	r0, #0
	bne	.L286
.L271:
	ldrb	ip, [r4, #25]	@ zero_extendqisi2
	ldrb	r3, [r4, #24]	@ zero_extendqisi2
	ldmia	r4, {r1, r2}	@ phole ldm
	str	ip, [sp, #0]
	ldrb	r0, [r4, #26]	@ zero_extendqisi2
	str	r0, [sp, #4]
	ldrb	lr, [r4, #27]	@ zero_extendqisi2
	str	lr, [sp, #8]
	ldrb	ip, [r4, #28]	@ zero_extendqisi2
	mov	r0, r6
	str	ip, [sp, #12]
	bl	png_write_IHDR
	ldr	r3, [r4, #8]
	tst	r3, #1
	bne	.L287
.L272:
	tst	r3, #2048
	bne	.L288
.L273:
	tst	r3, #4096
	bne	.L289
.L274:
	tst	r3, #2
	bne	.L290
.L275:
	tst	r3, #4
	bne	.L291
.L276:
	ldr	r3, [r4, #192]
	cmp	r3, #0
	beq	.L277
	ldr	r5, [r4, #188]
	add	r2, r3, r3, asl #2
	add	r1, r5, r2, asl #2
	cmp	r5, r1
	bcs	.L277
.L283:
	mov	r1, r5
	mov	r0, r6
	bl	png_handle_as_unknown
	cmp	r0, #1
	beq	.L278
	ldrb	r3, [r5, #16]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L278
	tst	r3, #6
	bne	.L278
	ldrb	ip, [r5, #3]	@ zero_extendqisi2
	mov	r3, ip, lsr #5
	cmp	r0, #3
	movne	r0, r3
	orreq	r0, r3, #1
	tst	r0, #1
	bne	.L279
	ldr	lr, [r6, #136]
	tst	lr, #65536
	beq	.L278
.L279:
	ldr	r3, [r5, #12]
	cmp	r3, #0
	beq	.L292
.L280:
	mov	r1, r5
	ldr	r2, [r5, #8]
	mov	r0, r6
	bl	png_write_chunk
.L278:
	add	r2, r4, #188
	ldmia	r2, {r2, r3}	@ phole ldm
	add	r0, r3, r3, asl #2
	add	r1, r2, r0, asl #2
	add	r5, r5, #20
	cmp	r1, r5
	bhi	.L283
.L277:
	ldr	lr, [r6, #132]
	orr	ip, lr, #1024
	str	ip, [r6, #132]
	b	.L282
.L287:
	ldr	r0, [r4, #40]	@ float
	bl	__aeabi_f2d
	mov	r2, r0
	mov	r3, r1
	mov	r0, r6
	bl	png_write_gAMA
	ldr	r3, [r4, #8]
	b	.L272
.L291:
	ldr	r0, [r4, #128]	@ float
	bl	__aeabi_f2d
	mov	r7, r0
	ldr	r0, [r4, #132]	@ float
	mov	r8, r1
	bl	__aeabi_f2d
	stmia	sp, {r0-r1}
	ldr	r0, [r4, #136]	@ float
	bl	__aeabi_f2d
	str	r0, [sp, #8]
	str	r1, [sp, #12]
	ldr	r0, [r4, #140]	@ float
	bl	__aeabi_f2d
	str	r0, [sp, #16]
	str	r1, [sp, #20]
	ldr	r0, [r4, #144]	@ float
	bl	__aeabi_f2d
	str	r0, [sp, #24]
	str	r1, [sp, #28]
	ldr	r0, [r4, #148]	@ float
	bl	__aeabi_f2d
	str	r0, [sp, #32]
	str	r1, [sp, #36]
	ldr	r0, [r4, #152]	@ float
	bl	__aeabi_f2d
	str	r0, [sp, #40]
	str	r1, [sp, #44]
	ldr	r0, [r4, #156]	@ float
	bl	__aeabi_f2d
	mov	r2, r7
	str	r0, [sp, #48]
	str	r1, [sp, #52]
	mov	r3, r8
	mov	r0, r6
	bl	png_write_cHRM
	b	.L276
.L290:
	mov	r0, r6
	add	r1, r4, #68
	ldrb	r2, [r4, #25]	@ zero_extendqisi2
	bl	png_write_sBIT
	ldr	r3, [r4, #8]
	b	.L275
.L289:
	add	r1, r4, #196
	ldmia	r1, {r1, r3, lr}	@ phole ldm
	mov	r0, r6
	mov	r2, #0
	str	lr, [sp, #0]
	bl	png_write_iCCP
	ldr	r3, [r4, #8]
	b	.L274
.L288:
	mov	r0, r6
	ldrb	r1, [r4, #44]	@ zero_extendqisi2
	bl	png_write_sRGB
	ldr	r3, [r4, #8]
	b	.L273
.L286:
	mov	r0, r6
	ldr	r1, .L293
	bl	png_warning
	str	r5, [r6, #588]
	b	.L271
.L292:
	mov	r0, r6
	ldr	r1, .L293+4
	bl	png_warning
	ldr	r3, [r5, #12]
	b	.L280
.L294:
	.align	2
.L293:
	.word	.LC14
	.word	.LC15
	.size	png_write_info_before_PLTE, .-png_write_info_before_PLTE
	.section	.text.png_write_info,"ax",%progbits
	.align	2
	.global	png_write_info
	.hidden	png_write_info
	.type	png_write_info, %function
png_write_info:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	cmp	r0, #0
	cmpne	r1, #0
	sub	sp, sp, #20
	mov	r4, r1
	mov	r6, r0
	bne	.L330
.L324:
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	bx	lr
.L330:
	bl	png_write_info_before_PLTE
	ldr	r3, [r4, #8]
	tst	r3, #8
	bne	.L331
	ldrb	r2, [r4, #25]	@ zero_extendqisi2
	cmp	r2, #3
	beq	.L332
.L298:
	tst	r3, #16
	bne	.L333
	tst	r3, #32
	bne	.L334
.L305:
	tst	r3, #64
	bne	.L335
.L306:
	tst	r3, #256
	bne	.L336
.L307:
	tst	r3, #1024
	bne	.L337
.L308:
	tst	r3, #16384
	bne	.L338
.L309:
	tst	r3, #128
	bne	.L339
.L310:
	tst	r3, #512
	bne	.L340
.L311:
	tst	r3, #8192
	bne	.L341
.L312:
	ldr	r2, [r4, #48]
	cmp	r2, #0
	movgt	r5, #0
	mvngt	r8, #2
	mvngt	sl, #1
	bgt	.L320
	b	.L315
.L342:
	mov	r0, r6
	ldr	r1, .L344
	bl	png_warning
	ldr	r2, [r4, #56]
	str	r8, [r2, r5, asl #4]
	ldr	r2, [r4, #48]
.L318:
	add	r5, r5, #1
	cmp	r5, r2
	bge	.L315
.L320:
	ldr	lr, [r4, #56]
	ldr	ip, [lr, r5, asl #4]
	mov	r7, r5, asl #4
	cmp	ip, #0
	add	r3, r7, lr
	bgt	.L342
	beq	.L343
	cmn	ip, #1
	bne	.L318
	ldmib	r3, {r1, r2}	@ phole ldm
	mov	r0, r6
	mov	r3, #0
	bl	png_write_tEXt
	ldr	r1, [r4, #56]
	str	r8, [r1, r7]
	ldr	r2, [r4, #48]
	add	r5, r5, #1
	cmp	r5, r2
	blt	.L320
.L315:
	ldr	r3, [r4, #192]
	cmp	r3, #0
	beq	.L324
	ldr	r5, [r4, #188]
	add	ip, r3, r3, asl #2
	add	r0, r5, ip, asl #2
	cmp	r0, r5
	bhi	.L325
	b	.L324
.L321:
	add	r2, r4, #188
	ldmia	r2, {r2, r3}	@ phole ldm
	add	lr, r3, r3, asl #2
	add	r0, r2, lr, asl #2
	add	r5, r5, #20
	cmp	r0, r5
	bls	.L324
.L325:
	mov	r1, r5
	mov	r0, r6
	bl	png_handle_as_unknown
	cmp	r0, #1
	beq	.L321
	ldrb	lr, [r5, #16]	@ zero_extendqisi2
	cmp	lr, #0
	and	r2, lr, #6
	beq	.L321
	cmp	r2, #2
	bne	.L321
	ldrb	r1, [r5, #3]	@ zero_extendqisi2
	mov	r3, r1, lsr #5
	cmp	r0, #3
	movne	r2, r3
	orreq	r2, r3, #1
	tst	r2, #1
	mov	r1, r5
	mov	r0, r6
	bne	.L322
	ldr	ip, [r6, #136]
	tst	ip, #65536
	beq	.L321
.L322:
	add	r2, r5, #8
	ldmia	r2, {r2, r3}	@ phole ldm
	bl	png_write_chunk
	b	.L321
.L343:
	ldmib	r3, {r1, r2}	@ phole ldm
	mov	r0, r6
	mov	r3, ip
	str	ip, [sp, #0]
	bl	png_write_zTXt
	ldr	r3, [r4, #56]
	str	sl, [r3, r7]
	ldr	r2, [r4, #48]
	b	.L318
.L341:
	ldr	r1, [r4, #216]
	cmp	r1, #0
	movgt	r5, #0
	ble	.L312
.L316:
	ldr	ip, [r4, #212]
	mov	r0, r6
	add	r1, ip, r5, asl #4
	bl	png_write_sPLT
	ldr	r0, [r4, #216]
	add	r5, r5, #1
	cmp	r0, r5
	bgt	.L316
	b	.L312
.L333:
	ldr	r3, [r6, #140]
	tst	r3, #524288
	bne	.L300
	ldrh	r3, [r4, #22]
	ldrb	ip, [r4, #25]	@ zero_extendqisi2
.L301:
	ldr	r1, [r4, #76]
	mov	r0, r6
	add	r2, r4, #80
	str	ip, [sp, #0]
	bl	png_write_tRNS
	ldr	r3, [r4, #8]
	tst	r3, #32
	beq	.L305
.L334:
	mov	r0, r6
	add	r1, r4, #90
	ldrb	r2, [r4, #25]	@ zero_extendqisi2
	bl	png_write_bKGD
	ldr	r3, [r4, #8]
	tst	r3, #64
	beq	.L306
.L335:
	mov	r0, r6
	ldr	r1, [r4, #124]
	ldrh	r2, [r4, #20]
	bl	png_write_hIST
	ldr	r3, [r4, #8]
	tst	r3, #256
	beq	.L307
.L336:
	ldrb	r3, [r4, #108]	@ zero_extendqisi2
	mov	r0, r6
	add	r1, r4, #100
	ldmia	r1, {r1, r2}	@ phole ldm
	bl	png_write_oFFs
	ldr	r3, [r4, #8]
	tst	r3, #1024
	beq	.L308
.L337:
	ldrb	lr, [r4, #180]	@ zero_extendqisi2
	add	r1, r4, #160
	ldmia	r1, {r1, r2, r3}	@ phole ldm
	str	lr, [sp, #0]
	ldrb	ip, [r4, #181]	@ zero_extendqisi2
	str	ip, [sp, #4]
	ldr	r0, [r4, #172]
	str	r0, [sp, #8]
	ldr	lr, [r4, #176]
	mov	r0, r6
	str	lr, [sp, #12]
	bl	png_write_pCAL
	ldr	r3, [r4, #8]
	tst	r3, #16384
	beq	.L309
.L338:
	add	r8, r4, #232
	ldmia	r8, {r7-r8}
	add	r3, r4, #224
	ldmia	r3, {r2-r3}
	ldrb	r1, [r4, #220]	@ zero_extendqisi2
	mov	r0, r6
	stmia	sp, {r7-r8}
	bl	png_write_sCAL
	ldr	r3, [r4, #8]
	tst	r3, #128
	beq	.L310
.L339:
	ldrb	r3, [r4, #120]	@ zero_extendqisi2
	mov	r0, r6
	add	r1, r4, #112
	ldmia	r1, {r1, r2}	@ phole ldm
	bl	png_write_pHYs
	ldr	r3, [r4, #8]
	tst	r3, #512
	beq	.L311
.L340:
	mov	r0, r6
	add	r1, r4, #60
	bl	png_write_tIME
	ldr	r3, [r6, #132]
	orr	r2, r3, #512
	str	r2, [r6, #132]
	ldr	r3, [r4, #8]
	b	.L311
.L331:
	mov	r0, r6
	ldr	r1, [r4, #16]
	ldrh	r2, [r4, #20]
	bl	png_write_PLTE
	ldr	r3, [r4, #8]
	b	.L298
.L300:
	ldrb	ip, [r4, #25]	@ zero_extendqisi2
	cmp	ip, #3
	ldrh	r3, [r4, #22]
	bne	.L301
	cmp	r3, #0
	beq	.L301
	mov	r2, #0
.L304:
	ldr	r0, [r4, #76]
	ldrb	ip, [r2, r0]	@ zero_extendqisi2
	mvn	r1, ip
	strb	r1, [r2, r0]
	ldrh	r3, [r4, #22]
	add	r2, r2, #1
	cmp	r3, r2
	bgt	.L304
	ldrb	ip, [r4, #25]	@ zero_extendqisi2
	b	.L301
.L332:
	mov	r0, r6
	ldr	r1, .L344+4
	bl	png_error
	ldr	r3, [r4, #8]
	b	.L298
.L345:
	.align	2
.L344:
	.word	.LC13
	.word	.LC16
	.size	png_write_info, .-png_write_info
	.section	.text.T.44,"ax",%progbits
	.align	2
	.type	T.44, %function
T.44:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	subs	r4, r0, #0
	beq	.L349
	ldr	r3, [r4, #548]
	mov	r1, #0
	mov	r2, #1
	cmp	r3, #0
	strb	r1, [r4, #533]
	strb	r2, [r4, #532]
	beq	.L350
.L348:
	ldr	r0, [r4, #552]
	mov	ip, #8	@ movhi
	strh	ip, [r3, #0]	@ movhi
	strh	ip, [r0, #0]	@ movhi
	add	r2, r4, #548
	ldmia	r2, {r2, r3}	@ phole ldm
	strh	ip, [r2, #2]	@ movhi
	strh	ip, [r3, #2]	@ movhi
	ldr	r0, [r4, #552]
	ldr	r1, [r4, #548]
	strh	ip, [r1, #4]	@ movhi
	strh	ip, [r0, #4]	@ movhi
	add	r2, r4, #548
	ldmia	r2, {r2, r3}	@ phole ldm
	strh	ip, [r2, #6]	@ movhi
	strh	ip, [r3, #6]	@ movhi
	ldr	r1, [r4, #548]
	ldr	r0, [r4, #552]
	strh	ip, [r1, #8]	@ movhi
	strh	ip, [r0, #8]	@ movhi
.L349:
	ldmfd	sp!, {r4, lr}
	bx	lr
.L350:
	mov	r1, #10
	bl	png_malloc
	mov	r1, #10
	str	r0, [r4, #548]
	mov	r0, r4
	bl	png_malloc
	ldr	r3, [r4, #548]
	mov	ip, #8	@ movhi
	str	r0, [r4, #552]
	strh	ip, [r3, #0]	@ movhi
	strh	ip, [r0, #0]	@ movhi
	ldr	r1, [r4, #552]
	ldr	r2, [r4, #548]
	strh	ip, [r2, #2]	@ movhi
	strh	ip, [r1, #2]	@ movhi
	add	r0, r4, #548
	ldmia	r0, {r0, r3}	@ phole ldm
	strh	ip, [r0, #4]	@ movhi
	strh	ip, [r3, #4]	@ movhi
	ldr	r1, [r4, #552]
	ldr	r2, [r4, #548]
	strh	ip, [r2, #6]	@ movhi
	strh	ip, [r1, #6]	@ movhi
	add	r0, r4, #548
	ldmia	r0, {r0, r3}	@ phole ldm
	strh	ip, [r0, #8]	@ movhi
	strh	ip, [r3, #8]	@ movhi
	ldr	r3, [r4, #548]
	b	.L348
	.size	T.44, .-T.44
	.section	.text.png_create_write_struct_2,"ax",%progbits
	.align	2
	.global	png_create_write_struct_2
	.hidden	png_create_write_struct_2
	.type	png_create_write_struct_2, %function
png_create_write_struct_2:
	@ Function supports interworking.
	@ args = 12, pretend = 0, frame = 104
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	sub	sp, sp, #104
	stmia	sp, {r0, r1, r2}	@ phole stm
	mov	r0, #1
	ldr	r1, [sp, #116]
	ldr	r2, [sp, #112]
	str	r3, [sp, #12]
	bl	png_create_struct_2
	str	r0, [sp, #100]
	ldr	r0, [sp, #100]
	cmp	r0, #0
	beq	.L353
	ldr	r0, [sp, #100]
	mov	r1, #999424
	add	r3, r1, #576
	str	r3, [r0, #640]
	ldr	r2, [sp, #100]
	str	r3, [r2, #644]
	ldr	r0, [sp, #100]
	bl	setjmp
	subs	r4, r0, #0
	ldr	r0, [sp, #100]
	bne	.L364
	add	r1, sp, #112
	ldmia	r1, {r1, r2, r3}	@ phole ldm
	bl	png_set_mem_fn
	ldr	r1, [sp, #4]
	ldr	r0, [sp, #100]
	add	r2, sp, #8
	ldmia	r2, {r2, r3}	@ phole ldm
	bl	png_set_error_fn
	ldr	r1, [sp, #0]
	cmp	r1, #0
	beq	.L355
	ldr	r0, .L369
	b	.L357
.L365:
	add	r4, r4, #1
.L357:
	ldr	r3, [sp, #0]
	ldrb	r2, [r4, r0]	@ zero_extendqisi2
	ldrb	lr, [r3, r4]	@ zero_extendqisi2
	cmp	r2, lr
	ldrne	lr, [sp, #100]
	ldrne	r1, [lr, #136]
	orrne	r1, r1, #131072
	strne	r1, [lr, #136]
	movne	lr, r2
	cmp	lr, #0
	bne	.L365
.L355:
	ldr	r1, [sp, #100]
	ldr	r0, [r1, #136]
	tst	r0, #131072
	beq	.L358
	ldr	r3, [sp, #0]
	cmp	r3, #0
	beq	.L359
	ldr	r2, .L369
	ldrb	ip, [r3, #0]	@ zero_extendqisi2
	ldrb	r3, [r2, #0]	@ zero_extendqisi2
	cmp	r3, ip
	beq	.L366
.L360:
	add	r4, sp, #20
	mov	r1, #80
	ldr	r3, [sp, #0]
	mov	r0, r4
	ldr	r2, .L369+4
	bl	snprintf
	ldr	r0, [sp, #100]
	mov	r1, r4
	bl	png_warning
.L359:
	add	r4, sp, #20
	mov	r1, #80
	ldr	r2, .L369+8
	ldr	r3, .L369
	mov	r0, r4
	bl	snprintf
	ldr	r0, [sp, #100]
	mov	r1, r4
	bl	png_warning
	ldr	ip, [sp, #100]
	mov	r1, #0
	str	r1, [ip, #136]
	ldr	r0, [sp, #100]
	ldr	r1, .L369+12
	bl	png_error
.L358:
	ldr	r3, [sp, #100]
	mov	r2, #8192
	str	r2, [r3, #204]
	ldr	r4, [sp, #100]
	ldr	r0, [sp, #100]
	ldr	lr, [sp, #100]
	ldr	r1, [lr, #204]
	bl	png_malloc
	mov	r1, #0
	str	r0, [r4, #200]
	ldr	r0, [sp, #100]
	mov	r2, r1
	mov	r3, r1
	bl	png_set_write_fn
	ldr	r0, [sp, #100]
	bl	T.44
	ldr	r0, [sp, #100]
	bl	setjmp
	cmp	r0, #0
	ldreq	r0, [sp, #100]
	bne	.L367
.L353:
	add	sp, sp, #104
	ldmfd	sp!, {r4, lr}
	bx	lr
.L366:
	cmp	r3, #49
	beq	.L368
	cmp	r3, #48
	bne	.L358
	ldr	r0, [sp, #0]
	ldrb	r3, [r0, #2]	@ zero_extendqisi2
	cmp	r3, #56
	bhi	.L358
	b	.L360
.L364:
	ldr	r4, [sp, #100]
	ldr	r1, [r4, #200]
	bl	png_free
	ldr	ip, [sp, #100]
	mov	r4, #0
	str	r4, [ip, #200]
	ldr	r0, [sp, #100]
	bl	png_destroy_struct
	mov	r0, r4
	b	.L353
.L368:
	ldrb	lr, [r2, #2]	@ zero_extendqisi2
	ldr	r2, [sp, #0]
	ldrb	r4, [r2, #2]	@ zero_extendqisi2
	cmp	r4, lr
	bne	.L360
	b	.L358
.L367:
	bl	abort
.L370:
	.align	2
.L369:
	.word	png_libpng_ver
	.word	.LC17
	.word	.LC18
	.word	.LC19
	.size	png_create_write_struct_2, .-png_create_write_struct_2
	.section	.text.png_create_write_struct,"ax",%progbits
	.align	2
	.global	png_create_write_struct
	.hidden	png_create_write_struct
	.type	png_create_write_struct, %function
png_create_write_struct:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
	mov	ip, #0
	sub	sp, sp, #20
	str	ip, [sp, #8]
	str	ip, [sp, #0]
	str	ip, [sp, #4]
	bl	png_create_write_struct_2
	add	sp, sp, #20
	ldr	lr, [sp], #4
	bx	lr
	.size	png_create_write_struct, .-png_create_write_struct
	.section	.text.png_write_init_3,"ax",%progbits
	.align	2
	.global	png_write_init_3
	.hidden	png_write_init_3
	.type	png_write_init_3, %function
png_write_init_3:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 96
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	ldr	r4, [r0, #0]
	cmp	r4, #0
	sub	sp, sp, #100
	mov	r6, r0
	mov	r7, r2
	beq	.L379
	ldr	ip, .L383
	mov	r3, #0
	b	.L377
.L381:
	add	r3, r3, #1
.L377:
	ldrb	r2, [r3, ip]	@ zero_extendqisi2
	ldrb	r0, [r1, r3]	@ zero_extendqisi2
	cmp	r2, r0
	bne	.L380
	cmp	r2, #0
	bne	.L381
.L376:
	add	r5, sp, #4
	mov	r0, r5
	mov	r1, r4
	mov	r2, #92
	bl	memcpy
	cmp	r7, #680
	bcc	.L382
.L378:
	mov	r1, #0
	mov	r2, #680
	mov	r0, r4
	bl	memset
	mov	r1, #999424
	add	r2, r1, #576
	str	r2, [r4, #644]
	mov	r1, r5
	str	r2, [r4, #640]
	mov	r0, r4
	mov	r2, #92
	bl	memcpy
	mov	r1, #0
	mov	r2, r1
	mov	r3, r1
	mov	r0, r4
	bl	png_set_write_fn
	mov	r1, #8192
	str	r1, [r4, #204]
	mov	r0, r4
	bl	png_malloc
	str	r0, [r4, #200]
	mov	r0, r4
	bl	T.44
.L379:
	add	sp, sp, #100
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L382:
	mov	r0, r4
	bl	png_destroy_struct
	mov	r0, #1
	bl	png_create_struct
	mov	r4, r0
	str	r0, [r6, #0]
	b	.L378
.L380:
	mov	r3, #0
	str	r3, [r4, #96]
	mov	r0, r4
	ldr	r1, .L383+4
	bl	png_warning
	b	.L376
.L384:
	.align	2
.L383:
	.word	png_libpng_ver
	.word	.LC20
	.size	png_write_init_3, .-png_write_init_3
	.section	.text.png_write_init_2,"ax",%progbits
	.align	2
	.global	png_write_init_2
	.hidden	png_write_init_2
	.type	png_write_init_2, %function
png_write_init_2:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 88
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	subs	ip, r0, #0
	sub	sp, sp, #88
	str	r0, [sp, #4]
	mov	r5, r1
	mov	r4, r2
	beq	.L390
	cmp	r3, #288
	movcs	r8, #0
	movcc	r8, #1
	cmp	r2, #680
	movcs	r6, #0
	movcc	r6, #1
	orrs	r3, r8, r6
	beq	.L387
	mov	r3, #0
	cmp	r1, #0
	str	r3, [ip, #96]
	add	r7, sp, #8
	beq	.L388
	mov	r1, #80
	mov	r0, r7
	ldr	r2, .L393
	mov	r3, r5
	bl	snprintf
	ldr	r0, [sp, #4]
	mov	r1, r7
	bl	png_warning
.L388:
	mov	r1, #80
	ldr	r2, .L393+4
	ldr	r3, .L393+8
	mov	r0, r7
	bl	snprintf
	mov	r1, r7
	ldr	r0, [sp, #4]
	bl	png_warning
	cmp	r6, #0
	beq	.L389
	ldr	r1, [sp, #4]
	mov	r0, #0
	str	r0, [r1, #92]
	ldr	r2, [sp, #4]
	str	r0, [r2, #136]
	ldr	r0, [sp, #4]
	ldr	r1, .L393+12
	bl	png_error
.L389:
	cmp	r8, #0
	beq	.L387
	ldr	r3, [sp, #4]
	mov	lr, #0
	str	lr, [r3, #92]
	ldr	ip, [sp, #4]
	str	lr, [ip, #136]
	ldr	r0, [sp, #4]
	ldr	r1, .L393+16
	bl	png_error
.L387:
	mov	r1, r5
	mov	r2, r4
	add	r0, sp, #4
	bl	png_write_init_3
.L390:
	add	sp, sp, #88
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L394:
	.align	2
.L393:
	.word	.LC17
	.word	.LC18
	.word	png_libpng_ver
	.word	.LC21
	.word	.LC22
	.size	png_write_init_2, .-png_write_init_2
	.section	.text.png_write_init,"ax",%progbits
	.align	2
	.global	png_write_init
	.hidden	png_write_init
	.type	png_write_init, %function
png_write_init:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 96
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	subs	r4, r0, #0
	sub	sp, sp, #100
	beq	.L400
	mov	r7, #0
	add	r6, sp, #4
	ldr	r2, .L403
	ldr	r3, .L403+4
	mov	r1, #80
	ldr	r5, .L403+8
	str	r7, [r4, #96]
	mov	r0, r6
	bl	snprintf
	mov	r0, r4
	mov	r1, r6
	bl	png_warning
	ldr	r2, .L403+12
	mov	r3, r5
	mov	r1, #80
	mov	r0, r6
	bl	snprintf
	mov	r0, r4
	mov	r1, r6
	bl	png_warning
	mov	r0, r4
	ldr	r1, .L403+16
	str	r7, [r4, #92]
	str	r7, [r4, #136]
	bl	png_error
	ldr	r1, .L403+20
	str	r7, [r4, #92]
	str	r7, [r4, #136]
	mov	r0, r4
	bl	png_error
	ldr	r3, .L403+4
	ldrb	r2, [r7, r5]	@ zero_extendqisi2
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	cmp	r2, r1
	beq	.L397
.L401:
	mov	r3, #0
	str	r3, [r4, #96]
	mov	r0, r4
	ldr	r1, .L403+24
	bl	png_warning
.L398:
	mov	r1, r4
	mov	r2, #92
	mov	r0, r6
	bl	memcpy
	mov	r0, r4
	bl	png_destroy_struct
	mov	r0, #1
	bl	png_create_struct
	mov	r1, #0
	mov	r2, #680
	mov	r4, r0
	bl	memset
	mov	r2, #999424
	add	r0, r2, #576
	str	r0, [r4, #644]
	mov	r1, r6
	str	r0, [r4, #640]
	mov	r2, #92
	mov	r0, r4
	bl	memcpy
	mov	r1, #0
	mov	r2, r1
	mov	r3, r1
	mov	r0, r4
	bl	png_set_write_fn
	mov	r1, #8192
	str	r1, [r4, #204]
	mov	r0, r4
	bl	png_malloc
	str	r0, [r4, #200]
	mov	r0, r4
	bl	T.44
.L400:
	add	sp, sp, #100
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L402:
	add	r7, r7, #1
	ldrb	r2, [r7, r5]	@ zero_extendqisi2
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	cmp	r2, r1
	bne	.L401
.L397:
	cmp	r2, #0
	add	r3, r3, #1
	bne	.L402
	b	.L398
.L404:
	.align	2
.L403:
	.word	.LC17
	.word	.LC23
	.word	png_libpng_ver
	.word	.LC18
	.word	.LC21
	.word	.LC22
	.word	.LC20
	.size	png_write_init, .-png_write_init
	.section	.text.png_write_png,"ax",%progbits
	.align	2
	.global	png_write_png
	.hidden	png_write_png
	.type	png_write_png, %function
png_write_png:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	cmpne	r1, #0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	mov	r6, r1
	mov	r5, r2
	mov	r4, r0
	beq	.L422
	tst	r2, #1024
	bne	.L424
	mov	r0, r4
	mov	r1, r6
	bl	png_write_info
	tst	r5, #32
	bne	.L425
.L408:
	tst	r5, #64
	beq	.L409
.L435:
	ldr	r3, [r6, #8]
	tst	r3, #2
	bne	.L426
.L409:
	tst	r5, #4
	bne	.L427
	tst	r5, #256
	bne	.L428
.L411:
	ands	r1, r5, #4096
	bne	.L429
.L412:
	tst	r5, #2048
	bne	.L430
.L413:
	tst	r5, #128
	bne	.L431
.L414:
	tst	r5, #512
	bne	.L432
.L415:
	tst	r5, #8
	bne	.L433
.L416:
	ldr	r0, [r6, #8]
	tst	r0, #32768
	bne	.L434
.L417:
	mov	r0, r4
	mov	r1, r6
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	b	png_write_end
.L422:
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L428:
	mov	r0, r4
	bl	png_set_swap_alpha
	ands	r1, r5, #4096
	beq	.L412
	b	.L429
.L424:
	bl	png_set_invert_alpha
	mov	r0, r4
	mov	r1, r6
	bl	png_write_info
	tst	r5, #32
	beq	.L408
.L425:
	mov	r0, r4
	bl	png_set_invert_mono
	tst	r5, #64
	beq	.L409
	b	.L435
.L429:
	mov	r0, r4
	mov	r1, #0
	mov	r2, #1
	bl	png_set_filler
	tst	r5, #128
	beq	.L414
.L431:
	mov	r0, r4
	bl	png_set_bgr
	tst	r5, #512
	beq	.L415
.L432:
	mov	r0, r4
	bl	png_set_swap
	tst	r5, #8
	beq	.L416
.L433:
	mov	r0, r4
	bl	png_set_packswap
	ldr	r0, [r6, #8]
	tst	r0, #32768
	beq	.L417
.L434:
	mov	r0, r4
	ldr	r7, [r6, #248]
	bl	png_set_interlace_handling
	subs	sl, r0, #0
	ble	.L417
	ldr	r3, [r4, #232]
	mov	r8, #0
.L418:
	cmp	r3, #0
	movne	r5, #0
	beq	.L421
.L419:
	ldr	r1, [r7, r5, asl #2]
	mov	r0, r4
	bl	png_write_row
	ldr	r3, [r4, #232]
	add	r5, r5, #1
	cmp	r5, r3
	bcc	.L419
.L421:
	add	r8, r8, #1
	cmp	sl, r8
	bgt	.L418
	b	.L417
.L427:
	mov	r0, r4
	bl	png_set_packing
	tst	r5, #256
	beq	.L411
	b	.L428
.L426:
	mov	r0, r4
	add	r1, r6, #68
	bl	png_set_shift
	b	.L409
.L430:
	mov	r0, r4
	mov	r2, r1
	bl	png_set_filler
	b	.L413
	.size	png_write_png, .-png_write_png
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"Only compression method 8 is supported by PNG\000"
	.space	2
.LC1:
	.ascii	"Only compression windows <= 32k supported by PNG\000"
	.space	3
.LC2:
	.ascii	"Only compression windows >= 256 supported by PNG\000"
	.space	3
.LC3:
	.ascii	"Compression window is being reset to 512\000"
	.space	3
.LC4:
	.ascii	"Unknown filter heuristic method\000"
.LC5:
	.ascii	"Unknown row filter for method 0\000"
.LC6:
	.ascii	"Can't add Up filter after starting\000"
	.space	1
.LC7:
	.ascii	"Can't add Average filter after starting\000"
.LC8:
	.ascii	"Can't add Paeth filter after starting\000"
	.space	2
.LC9:
	.ascii	"Unknown custom filter method\000"
	.space	3
.LC10:
	.ascii	"zlib error\000"
	.space	1
.LC11:
	.ascii	"png_write_info was never called before png_write_ro"
	.ascii	"w.\000"
	.space	2
.LC12:
	.ascii	"No IDATs written into file\000"
	.space	1
.LC13:
	.ascii	"Unable to write international text\000"
	.space	1
.LC14:
	.ascii	"MNG features are not allowed in a PNG datastream\000"
	.space	3
.LC15:
	.ascii	"Writing zero-length unknown chunk\000"
	.space	2
.LC16:
	.ascii	"Valid palette required for paletted images\000"
	.space	1
.LC17:
	.ascii	"Application was compiled with png.h from libpng-%.2"
	.ascii	"0s\000"
	.space	2
.LC18:
	.ascii	"Application  is  running with png.c from libpng-%.2"
	.ascii	"0s\000"
	.space	2
.LC19:
	.ascii	"Incompatible libpng version in application and libr"
	.ascii	"ary\000"
	.space	1
.LC20:
	.ascii	"Application uses deprecated png_write_init() and sh"
	.ascii	"ould be recompiled.\000"
	.space	1
.LC21:
	.ascii	"The png struct allocated by the application for wri"
	.ascii	"ting is too small.\000"
	.space	2
.LC22:
	.ascii	"The info struct allocated by the application for wr"
	.ascii	"iting is too small.\000"
	.space	1
.LC23:
	.ascii	"1.0.6 or earlier\000"
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
