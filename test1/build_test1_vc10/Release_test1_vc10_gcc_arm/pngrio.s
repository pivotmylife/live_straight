	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"pngrio.c"
	.section	.text.png_set_read_fn,"ax",%progbits
	.align	2
	.global	png_set_read_fn
	.hidden	png_set_read_fn
	.type	png_set_read_fn, %function
png_set_read_fn:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	subs	r4, r0, #0
	beq	.L6
	cmp	r2, #0
	ldreq	r3, .L8
	streq	r3, [r4, #108]
	ldr	r3, [r4, #104]
	strne	r2, [r4, #108]
	cmp	r3, #0
	str	r1, [r4, #112]
	beq	.L5
	mov	r0, #0
	str	r0, [r4, #104]
	ldr	r1, .L8+4
	mov	r0, r4
	bl	png_warning
	mov	r0, r4
	ldr	r1, .L8+8
	bl	png_warning
.L5:
	mov	r1, #0
	str	r1, [r4, #360]
.L6:
	ldmfd	sp!, {r4, lr}
	bx	lr
.L9:
	.align	2
.L8:
	.word	png_default_read_data
	.word	.LC0
	.word	.LC1
	.size	png_set_read_fn, .-png_set_read_fn
	.section	.text.png_default_read_data,"ax",%progbits
	.align	2
	.global	png_default_read_data
	.hidden	png_default_read_data
	.type	png_default_read_data, %function
png_default_read_data:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	subs	r4, r0, #0
	mov	r5, r2
	beq	.L12
	mov	r0, r1
	ldr	r3, [r4, #112]
	mov	r1, #1
	bl	fread
	cmp	r5, r0
	movne	r0, r4
	ldrne	r1, .L13
	blne	png_error
.L12:
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L14:
	.align	2
.L13:
	.word	.LC2
	.size	png_default_read_data, .-png_default_read_data
	.section	.text.png_read_data,"ax",%progbits
	.align	2
	.global	png_read_data
	.hidden	png_read_data
	.type	png_read_data, %function
png_read_data:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, lr}
	ldr	r3, [r0, #108]
	cmp	r3, #0
	beq	.L16
	mov	lr, pc
	bx	r3
.L18:
	ldmfd	sp!, {r3, lr}
	bx	lr
.L16:
	ldr	r1, .L19
	bl	png_error
	b	.L18
.L20:
	.align	2
.L19:
	.word	.LC3
	.size	png_read_data, .-png_read_data
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"It's an error to set both read_data_fn and write_da"
	.ascii	"ta_fn in the \000"
	.space	3
.LC1:
	.ascii	"same structure.  Resetting write_data_fn to NULL.\000"
	.space	2
.LC2:
	.ascii	"Read Error\000"
	.space	1
.LC3:
	.ascii	"Call to NULL read function\000"
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
