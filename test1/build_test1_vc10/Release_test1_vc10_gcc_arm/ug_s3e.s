	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"ug_s3e.c"
	.section	.text.kbcb,"ax",%progbits
	.align	2
	.global	kbcb
	.hidden	kbcb
	.type	kbcb, %function
kbcb:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, lr}
	ldr	r2, [r1, #4]
	ldr	ip, [r2, #20]
	cmp	ip, #0
	mov	r3, r0
	beq	.L2
	mov	r0, r2
	ldmia	r3, {r1, r2}	@ phole ldm
	mov	r3, #0
	mov	lr, pc
	bx	ip
.L2:
	mov	r0, #0
	ldmfd	sp!, {r3, lr}
	bx	lr
	.size	kbcb, .-kbcb
	.section	.text.mbcb,"ax",%progbits
	.align	2
	.global	mbcb
	.hidden	mbcb
	.type	mbcb, %function
mbcb:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	ldr	r4, [r1, #4]
	ldr	ip, [r4, #24]
	cmp	ip, #0
	sub	sp, sp, #8
	beq	.L6
	ldmia	r0, {r1, r2, r3, lr}	@ phole ldm
	str	lr, [sp, #0]
	mov	r0, r4
	mov	lr, pc
	bx	ip
.L6:
	mov	r0, #0
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	bx	lr
	.size	mbcb, .-mbcb
	.section	.text.mmcb,"ax",%progbits
	.align	2
	.global	mmcb
	.hidden	mmcb
	.type	mmcb, %function
mmcb:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, lr}
	ldr	r2, [r1, #4]
	ldr	r3, [r2, #28]
	cmp	r3, #0
	mov	r1, r0
	movne	r0, r2
	addne	r1, r1, #8
	ldmneia	r1, {r1, r2}	@ phole ldm
	movne	lr, pc
	bxne	r3
.L9:
	mov	r0, #0
	ldmfd	sp!, {r3, lr}
	bx	lr
	.size	mmcb, .-mmcb
	.section	.text.ugCtxFromWin,"ax",%progbits
	.align	2
	.global	ugCtxFromWin
	.hidden	ugCtxFromWin
	.type	ugCtxFromWin, %function
ugCtxFromWin:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, [r0, #32]
	bx	lr
	.size	ugCtxFromWin, .-ugCtxFromWin
	.section	.text.ugReshapeFunc,"ax",%progbits
	.align	2
	.global	ugReshapeFunc
	.hidden	ugReshapeFunc
	.type	ugReshapeFunc, %function
ugReshapeFunc:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	str	r1, [r0, #16]
	bx	lr
	.size	ugReshapeFunc, .-ugReshapeFunc
	.section	.text.ugDisplayFunc,"ax",%progbits
	.align	2
	.global	ugDisplayFunc
	.hidden	ugDisplayFunc
	.type	ugDisplayFunc, %function
ugDisplayFunc:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	str	r1, [r0, #12]
	bx	lr
	.size	ugDisplayFunc, .-ugDisplayFunc
	.section	.text.ugKeyboardFunc,"ax",%progbits
	.align	2
	.global	ugKeyboardFunc
	.hidden	ugKeyboardFunc
	.type	ugKeyboardFunc, %function
ugKeyboardFunc:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	str	r1, [r0, #20]
	bx	lr
	.size	ugKeyboardFunc, .-ugKeyboardFunc
	.section	.text.ugPointerFunc,"ax",%progbits
	.align	2
	.global	ugPointerFunc
	.hidden	ugPointerFunc
	.type	ugPointerFunc, %function
ugPointerFunc:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	str	r1, [r0, #24]
	bx	lr
	.size	ugPointerFunc, .-ugPointerFunc
	.section	.text.ugMotionFunc,"ax",%progbits
	.align	2
	.global	ugMotionFunc
	.hidden	ugMotionFunc
	.type	ugMotionFunc, %function
ugMotionFunc:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	str	r1, [r0, #28]
	bx	lr
	.size	ugMotionFunc, .-ugMotionFunc
	.section	.text.ugIdleFunc,"ax",%progbits
	.align	2
	.global	ugIdleFunc
	.hidden	ugIdleFunc
	.type	ugIdleFunc, %function
ugIdleFunc:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	str	r1, [r0, #0]
	bx	lr
	.size	ugIdleFunc, .-ugIdleFunc
	.section	.text.ugPostRedisplay,"ax",%progbits
	.align	2
	.global	ugPostRedisplay
	.hidden	ugPostRedisplay
	.type	ugPostRedisplay, %function
ugPostRedisplay:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.size	ugPostRedisplay, .-ugPostRedisplay
	.section	.text.IwMain,"ax",%progbits
	.align	2
	.global	IwMain
	.hidden	IwMain
	.type	IwMain, %function
IwMain:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
	ldr	r3, .L29
	sub	sp, sp, #12
	add	r1, sp, #8
	str	r3, [r1, #-4]!
	mov	r0, #1
	bl	main
	add	sp, sp, #12
	ldr	lr, [sp], #4
	bx	lr
.L30:
	.align	2
.L29:
	.word	.LC0
	.size	IwMain, .-IwMain
	.section	.text.ugSwapBuffers,"ax",%progbits
	.align	2
	.global	ugSwapBuffers
	.hidden	ugSwapBuffers
	.type	ugSwapBuffers, %function
ugSwapBuffers:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, lr}
	ldr	r3, [r0, #32]
	ldr	r1, [r0, #44]
	ldr	r0, [r3, #12]
	bl	eglSwapBuffers
	ldmfd	sp!, {r3, lr}
	bx	lr
	.size	ugSwapBuffers, .-ugSwapBuffers
	.section	.text.yieldCycle,"ax",%progbits
	.align	2
	.global	yieldCycle
	.hidden	yieldCycle
	.type	yieldCycle, %function
yieldCycle:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	bl	s3eTimerGetMs
	ldr	r4, .L37
	ldr	r3, [r4, #0]
	rsb	r0, r3, r0
	cmp	r0, #30
	str	r0, [r4, #0]
	movgt	r0, #0
	rsble	r0, r0, #30
	bl	s3eDeviceYield
	bl	s3eTimerGetMs
	str	r0, [r4, #0]
	ldmfd	sp!, {r4, lr}
	bx	lr
.L38:
	.align	2
.L37:
	.word	.LANCHOR0
	.size	yieldCycle, .-yieldCycle
	.section	.text.ugDestroyWindow,"ax",%progbits
	.align	2
	.global	ugDestroyWindow
	.hidden	ugDestroyWindow
	.type	ugDestroyWindow, %function
ugDestroyWindow:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	mov	r4, r0
	mov	r0, #0
	mov	r5, r1
	mov	r2, r0
	mov	r1, r0
	mov	r3, r0
	bl	eglMakeCurrent
	ldr	r1, [r4, #52]
	subs	r6, r5, #0
	movne	r6, #1
	cmp	r1, #0
	cmpne	r5, #0
	bne	.L43
.L40:
	ldr	r1, [r4, #44]
	cmp	r1, #0
	moveq	r6, #0
	andne	r6, r6, #1
	cmp	r6, #0
	beq	.L41
	mov	r0, r5
	bl	eglDestroySurface
	mov	r0, #0
	str	r0, [r4, #44]
.L41:
	mov	r0, r4
	bl	free
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L43:
	mov	r0, r5
	bl	eglDestroyContext
	mov	r3, #0
	str	r3, [r4, #52]
	b	.L40
	.size	ugDestroyWindow, .-ugDestroyWindow
	.section	.text.ugCreateWindow,"ax",%progbits
	.align	2
	.global	ugCreateWindow
	.hidden	ugCreateWindow
	.type	ugCreateWindow, %function
ugCreateWindow:
	@ Function supports interworking.
	@ args = 12, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	mov	r6, r0
	sub	sp, sp, #24
	mov	r0, #56
	mov	r4, r1
	bl	malloc
	ldr	r1, .L53
	mov	r5, r0
	mov	r0, r4
	bl	strstr
	mov	r4, #0
	mov	r7, r5
	str	r4, [r7], #4
	add	r1, r7, #4
	str	r4, [r5, #4]
	str	r4, [r1], #4
	str	r4, [r1], #4
	str	r4, [r1], #4
	str	r4, [r1], #4
	str	r4, [r1], #4
	str	r4, [r1], #4
	str	r4, [r1], #4
	str	r4, [r1], #4
	str	r4, [r1], #4
	str	r4, [r1], #4
	str	r4, [r1], #4
	str	r4, [r1, #0]
	str	r6, [r5, #32]
	ldr	r2, .L53+4
	ldr	r3, [r6, #12]
	cmp	r0, r4
	sub	r1, r2, #12
	mov	r0, r3
	movne	r1, r2
	add	ip, sp, #20
	add	r2, sp, #12
	mov	r3, #1
	str	ip, [sp, #0]
	bl	eglChooseConfig
	subs	r7, r0, #0
	beq	.L51
	ldr	r1, [sp, #12]
	str	r1, [r5, #48]
	mov	r0, #12288
	add	r2, r0, #46
	add	r3, sp, #16
	ldr	r0, [r6, #12]
	bl	eglGetConfigAttrib
	mov	r0, r4
	bl	s3eSurfaceGetInt
	str	r0, [r5, #0]
	mov	r0, #1
	bl	s3eSurfaceGetInt
	str	r0, [r5, #4]
	ldr	r7, [r5, #48]
	ldr	r8, [r6, #12]
	bl	s3eGLGetNativeWindow
	mov	r1, r7
	mov	r2, r0
	mov	r3, r4
	mov	r0, r8
	bl	eglCreateWindowSurface
	str	r0, [r5, #44]
	mov	r2, r4
	mov	r3, r4
	ldr	r1, [r5, #48]
	ldr	r0, [r6, #12]
	bl	eglCreateContext
	ldr	r1, [r5, #44]
	str	r0, [r5, #52]
	mov	r3, r0
	mov	r2, r1
	ldr	r0, [r6, #12]
	bl	eglMakeCurrent
	subs	r7, r0, #0
	strne	r5, [r6, #4]
	movne	r0, r5
	beq	.L52
.L48:
	add	sp, sp, #24
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L51:
	ldr	r1, .L53+8
	bl	s3eDebugErrorShow
	mov	r0, r5
	bl	free
	mov	r0, r7
	b	.L48
.L52:
	ldr	r1, .L53+12
	bl	s3eDebugErrorShow
	mov	r0, r5
	bl	free
	mov	r0, r7
	b	.L48
.L54:
	.align	2
.L53:
	.word	.LC1
	.word	.LANCHOR1+12
	.word	.LC2
	.word	.LC3
	.size	ugCreateWindow, .-ugCreateWindow
	.section	.text.ugInit,"ax",%progbits
	.align	2
	.global	ugInit
	.hidden	ugInit
	.type	ugInit, %function
ugInit:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	mov	r0, #20
	bl	malloc
	mov	r4, #0
	mov	r5, r0
	str	r4, [r0], #4
	add	r1, r0, #4
	str	r4, [r5, #4]
	str	r4, [r1], #4
	str	r4, [r1], #4
	mov	r0, r4
	str	r4, [r1, #0]
	bl	eglGetDisplay
	mov	r1, r4
	mov	r2, r4
	str	r0, [r5, #12]
	bl	eglInitialize
	mov	r2, r5
	mov	r0, r4
	ldr	r1, .L57
	bl	s3eKeyboardRegister
	mov	r2, r5
	mov	r0, r4
	ldr	r1, .L57+4
	bl	s3ePointerRegister
	ldr	r1, .L57+8
	mov	r2, r5
	mov	r0, #1
	bl	s3ePointerRegister
	ldr	r3, .L57+12
	mov	r0, r5
	str	r5, [r3, #4]
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L58:
	.align	2
.L57:
	.word	kbcb
	.word	mbcb
	.word	mmcb
	.word	.LANCHOR0
	.size	ugInit, .-ugInit
	.section	.text.ugFini,"ax",%progbits
	.align	2
	.global	ugFini
	.hidden	ugFini
	.type	ugFini, %function
ugFini:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, lr}
	ldr	r4, [r0, #4]
	cmp	r4, #0
	mov	r5, r0
	beq	.L60
	mov	r0, #0
	mov	r1, r0
	mov	r2, r0
	mov	r3, r0
	ldr	r6, [r5, #12]
	bl	eglMakeCurrent
	ldr	r1, [r4, #52]
	subs	r7, r6, #0
	movne	r7, #1
	cmp	r1, #0
	cmpne	r6, #0
	bne	.L64
.L61:
	ldr	r1, [r4, #44]
	cmp	r1, #0
	moveq	r7, #0
	andne	r7, r7, #1
	cmp	r7, #0
	bne	.L65
.L62:
	mov	r0, r4
	bl	free
.L60:
	ldr	r1, .L66
	mov	r0, #0
	bl	s3eKeyboardUnRegister
	ldr	r1, .L66+4
	mov	r0, #0
	bl	s3ePointerUnRegister
	ldr	r1, .L66+8
	mov	r0, #1
	bl	s3ePointerUnRegister
	ldr	r0, [r5, #12]
	bl	eglTerminate
	mov	r0, r5
	bl	free
	ldmfd	sp!, {r3, r4, r5, r6, r7, lr}
	bx	lr
.L65:
	mov	r0, r6
	bl	eglDestroySurface
	mov	r0, #0
	str	r0, [r4, #44]
	b	.L62
.L64:
	mov	r0, r6
	bl	eglDestroyContext
	mov	r3, #0
	str	r3, [r4, #52]
	b	.L61
.L67:
	.align	2
.L66:
	.word	kbcb
	.word	mbcb
	.word	mmcb
	.size	ugFini, .-ugFini
	.section	.text.ugMainLoop,"ax",%progbits
	.align	2
	.global	ugMainLoop
	.hidden	ugMainLoop
	.type	ugMainLoop, %function
ugMainLoop:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	ldr	r4, .L77
	sub	sp, sp, #8
	add	r2, sp, #8
	mov	r7, #1
	str	r0, [r4, #4]
	str	r7, [r2, #-4]!
	ldr	r5, [r0, #4]
	mov	r6, r0
	ldr	r1, .L77+4
	ldr	r0, .L77+8
	bl	s3eConfigGetInt
	ldr	r3, [r5, #16]
	cmp	r3, #0
	beq	.L76
	mov	r0, #0
	bl	s3eSurfaceGetInt
	ldr	r8, [sp, #4]
	mul	r8, r0, r8
	mov	r0, r7
	bl	s3eSurfaceGetInt
	ldr	r2, [sp, #4]
	mov	r1, r8
	mul	r2, r0, r2
	mov	r0, r5
	ldr	ip, [r5, #16]
	mov	lr, pc
	bx	ip
	b	.L76
.L74:
	ldr	r1, [r4, #4]
	ldr	r3, [r1, #0]
	cmp	r3, #0
	movne	lr, pc
	bxne	r3
.L70:
	ldr	r3, [r5, #12]
	cmp	r3, #0
	mov	r0, r5
	movne	lr, pc
	bxne	r3
.L71:
	bl	s3ePointerUpdate
	bl	s3eTimerGetMs
	ldr	ip, [r4, #0]
	rsb	r2, ip, r0
	cmp	r2, #30
	rsb	r0, r2, #30
	movgt	r0, #0
	str	r2, [r4, #0]
	bl	s3eDeviceYield
	bl	s3eTimerGetMs
	str	r0, [r4, #0]
.L76:
	bl	s3eDeviceCheckQuitRequest
	cmp	r0, #0
	mov	r0, r5
	beq	.L74
	mov	r0, r6
	bl	ugFini
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L78:
	.align	2
.L77:
	.word	.LANCHOR0
	.word	.LC5
	.word	.LC4
	.size	ugMainLoop, .-ugMainLoop
	.data
	.align	2
	.set	.LANCHOR1,. + 0
	.type	attribs.4430, %object
	.size	attribs.4430, 12
attribs.4430:
	.word	12324
	.word	1
	.word	12344
	.type	attribs2.4431, %object
	.size	attribs2.4431, 20
attribs2.4431:
	.word	12324
	.word	1
	.word	12325
	.word	1
	.word	12344
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"s3e\000"
.LC1:
	.ascii	"UG_DEPTH\000"
	.space	3
.LC2:
	.ascii	"eglChooseConfig failed - failed to find a usable GL"
	.ascii	"ES configuration. Application cannot run.\000"
	.space	3
.LC3:
	.ascii	"eglMakeCurrent failed. Application cannot run.\000"
	.space	1
.LC4:
	.ascii	"S3E\000"
.LC5:
	.ascii	"ScreenZoom\000"
	.bss
	.align	2
	.set	.LANCHOR0,. + 0
	.type	time.4521, %object
	.size	time.4521, 4
time.4521:
	.space	4
	.type	context, %object
	.size	context, 4
context:
	.space	4
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
