	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"QuImage.cpp"
	.section	.text._ZN14QuDynamicImage12safeSetPixelEiihhhh,"ax",%progbits
	.align	2
	.global	_ZN14QuDynamicImage12safeSetPixelEiihhhh
	.hidden	_ZN14QuDynamicImage12safeSetPixelEiihhhh
	.type	_ZN14QuDynamicImage12safeSetPixelEiihhhh, %function
_ZN14QuDynamicImage12safeSetPixelEiihhhh:
	.fnstart
.LFB2852:
	@ Function supports interworking.
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, r5, r6}
	cmp	r2, #0
	cmpge	r1, #0
	ldrb	r5, [sp, #12]	@ zero_extendqisi2
	ldrb	r4, [sp, #16]	@ zero_extendqisi2
	ldrb	ip, [sp, #20]	@ zero_extendqisi2
	blt	.L2
	ldr	r6, [r0, #12]
	cmp	r1, r6
	blt	.L6
.L2:
	ldr	r6, [r0, #20]
	mla	r2, r6, r2, r1
	ldr	r1, [r0, #32]
	mov	r6, #0
	strb	r6, [r0, #28]
	strb	r3, [r1, r2, asl #2]
	ldr	r1, [r0, #32]
	mov	r2, r2, asl #2
	add	r3, r2, r1
	strb	r5, [r3, #1]
	ldr	r1, [r0, #32]
	add	r3, r2, r1
	strb	r4, [r3, #2]
	ldr	r1, [r0, #32]
	add	r2, r2, r1
	strb	ip, [r2, #3]
	mov	r0, #1
.L3:
	ldmfd	sp!, {r4, r5, r6}
	bx	lr
.L6:
	ldr	r6, [r0, #16]
	cmp	r2, r6
	movlt	r0, #0
	bge	.L2
	b	.L3
	.cantunwind
	.fnend
	.size	_ZN14QuDynamicImage12safeSetPixelEiihhhh, .-_ZN14QuDynamicImage12safeSetPixelEiihhhh
	.section	.text._ZN14QuDynamicImage8setPixelEiihhhh,"ax",%progbits
	.align	2
	.global	_ZN14QuDynamicImage8setPixelEiihhhh
	.hidden	_ZN14QuDynamicImage8setPixelEiihhhh
	.type	_ZN14QuDynamicImage8setPixelEiihhhh, %function
_ZN14QuDynamicImage8setPixelEiihhhh:
	.fnstart
.LFB2853:
	@ Function supports interworking.
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	ip, [r0, #20]
	mla	ip, r2, ip, r1
	stmfd	sp!, {r4, r5}
	ldr	r5, [r0, #32]
	mov	r4, #0
	strb	r4, [r0, #28]
	ldrb	r1, [sp, #12]	@ zero_extendqisi2
	ldrb	r4, [sp, #8]	@ zero_extendqisi2
	ldrb	r2, [sp, #16]	@ zero_extendqisi2
	strb	r3, [r5, ip, asl #2]
	ldr	r3, [r0, #32]
	mov	ip, ip, asl #2
	add	r3, ip, r3
	strb	r4, [r3, #1]
	ldr	r3, [r0, #32]
	add	r3, ip, r3
	strb	r1, [r3, #2]
	ldr	r3, [r0, #32]
	add	ip, ip, r3
	strb	r2, [ip, #3]
	ldmfd	sp!, {r4, r5}
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN14QuDynamicImage8setPixelEiihhhh, .-_ZN14QuDynamicImage8setPixelEiihhhh
	.section	.text._ZN14QuDynamicImage12safeGetPixelEiiRhS0_S0_S0_,"ax",%progbits
	.align	2
	.global	_ZN14QuDynamicImage12safeGetPixelEiiRhS0_S0_S0_
	.hidden	_ZN14QuDynamicImage12safeGetPixelEiiRhS0_S0_S0_
	.type	_ZN14QuDynamicImage12safeGetPixelEiiRhS0_S0_S0_, %function
_ZN14QuDynamicImage12safeGetPixelEiiRhS0_S0_S0_:
	.fnstart
.LFB2854:
	@ Function supports interworking.
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r2, #0
	cmpge	r1, #0
	blt	.L10
	ldr	ip, [r0, #12]
	cmp	r1, ip
	bge	.L10
	ldr	ip, [r0, #16]
	cmp	r2, ip
	movlt	r0, #0
	bxlt	lr
.L10:
	ldr	ip, [r0, #20]
	mla	r2, ip, r2, r1
	ldr	r1, [r0, #32]
	ldrb	ip, [r1, r2, asl #2]	@ zero_extendqisi2
	strb	ip, [r3, #0]
	ldr	r1, [r0, #32]
	mov	ip, r2, asl #2
	add	r3, ip, r1
	ldrb	r1, [r3, #1]	@ zero_extendqisi2
	ldr	r3, [sp, #0]
	strb	r1, [r3, #0]
	ldr	r1, [r0, #32]
	add	r3, ip, r1
	ldrb	r1, [r3, #2]	@ zero_extendqisi2
	ldr	r3, [sp, #4]
	strb	r1, [r3, #0]
	ldr	r1, [r0, #32]
	add	r0, ip, r1
	ldrb	r2, [r0, #3]	@ zero_extendqisi2
	ldr	r3, [sp, #8]
	mov	r0, #1
	strb	r2, [r3, #0]
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN14QuDynamicImage12safeGetPixelEiiRhS0_S0_S0_, .-_ZN14QuDynamicImage12safeGetPixelEiiRhS0_S0_S0_
	.section	.text._ZN14QuDynamicImage8getPixelEiiRhS0_S0_S0_,"ax",%progbits
	.align	2
	.global	_ZN14QuDynamicImage8getPixelEiiRhS0_S0_S0_
	.hidden	_ZN14QuDynamicImage8getPixelEiiRhS0_S0_S0_
	.type	_ZN14QuDynamicImage8getPixelEiiRhS0_S0_S0_, %function
_ZN14QuDynamicImage8getPixelEiiRhS0_S0_S0_:
	.fnstart
.LFB2855:
	@ Function supports interworking.
	@ args = 12, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	ip, [r0, #20]
	mla	ip, r2, ip, r1
	ldr	r2, [r0, #32]
	ldrb	r1, [r2, ip, asl #2]	@ zero_extendqisi2
	strb	r1, [r3, #0]
	ldr	r2, [r0, #32]
	mov	r1, ip, asl #2
	add	r3, r1, r2
	ldrb	r2, [r3, #1]	@ zero_extendqisi2
	ldr	r3, [sp, #0]
	strb	r2, [r3, #0]
	ldr	r2, [r0, #32]
	add	r3, r1, r2
	ldrb	r2, [r3, #2]	@ zero_extendqisi2
	ldr	r3, [sp, #4]
	strb	r2, [r3, #0]
	ldr	r2, [r0, #32]
	add	ip, r1, r2
	ldrb	r2, [ip, #3]	@ zero_extendqisi2
	ldr	r3, [sp, #8]
	strb	r2, [r3, #0]
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN14QuDynamicImage8getPixelEiiRhS0_S0_S0_, .-_ZN14QuDynamicImage8getPixelEiiRhS0_S0_S0_
	.section	.text._ZN9PngLoader9getFormatEv,"ax",%progbits
	.align	2
	.global	_ZN9PngLoader9getFormatEv
	.hidden	_ZN9PngLoader9getFormatEv
	.type	_ZN9PngLoader9getFormatEv, %function
_ZN9PngLoader9getFormatEv:
	.fnstart
.LFB2868:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r1, [r0, #40]
	sub	r3, r1, #2
	cmp	r3, #4
	ldrls	r2, .L19
	movhi	r0, #6400
	addhi	r0, r0, #7
	ldrls	r0, [r2, r3, asl #2]
	bx	lr
.L20:
	.align	2
.L19:
	.word	.LANCHOR0
	.cantunwind
	.fnend
	.size	_ZN9PngLoader9getFormatEv, .-_ZN9PngLoader9getFormatEv
	.section	.text._GLOBAL__I__ZN11QuBaseImage7destroyEv,"ax",%progbits
	.align	2
	.type	_GLOBAL__I__ZN11QuBaseImage7destroyEv, %function
_GLOBAL__I__ZN11QuBaseImage7destroyEv:
	.fnstart
.LFB3256:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	ldr	r4, .L23
	ldr	r5, .L23+4
	mov	r0, r4
	bl	_ZN4_STL8ios_base9_Loc_initC1Ev
	add	r6, r4, #4
	mov	r2, r5
	ldr	r1, .L23+8
	mov	r0, r4
	bl	__aeabi_atexit
	mov	r0, r6
	bl	_ZN4_STL8ios_base4InitC1Ev
	mov	r0, r6
	mov	r2, r5
	ldr	r1, .L23+12
	bl	__aeabi_atexit
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L24:
	.align	2
.L23:
	.word	.LANCHOR1
	.word	__dso_handle
	.word	_ZN4_STL8ios_base9_Loc_initD1Ev
	.word	_ZN4_STL8ios_base4InitD1Ev
	.fnend
	.size	_GLOBAL__I__ZN11QuBaseImage7destroyEv, .-_GLOBAL__I__ZN11QuBaseImage7destroyEv
	.section	.init_array,"aw",%init_array
	.align	2
	.word	_GLOBAL__I__ZN11QuBaseImage7destroyEv(target1)
	.section	.text._ZN9PngLoader22readDataFromFileHandleEP14png_struct_defPhj,"ax",%progbits
	.align	2
	.global	_ZN9PngLoader22readDataFromFileHandleEP14png_struct_defPhj
	.hidden	_ZN9PngLoader22readDataFromFileHandleEP14png_struct_defPhj
	.type	_ZN9PngLoader22readDataFromFileHandleEP14png_struct_defPhj, %function
_ZN9PngLoader22readDataFromFileHandleEP14png_struct_defPhj:
	.fnstart
.LFB2863:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, lr}
	.save {r3, lr}
	ldr	r3, [r0, #112]
	cmp	r3, #0
	movne	r0, r1
	movne	r1, #1
	blne	s3eFileRead
.L27:
	ldmfd	sp!, {r3, lr}
	bx	lr
	.fnend
	.size	_ZN9PngLoader22readDataFromFileHandleEP14png_struct_defPhj, .-_ZN9PngLoader22readDataFromFileHandleEP14png_struct_defPhj
	.section	.text._ZN9PngLoader15deleteImageDataEv,"ax",%progbits
	.align	2
	.global	_ZN9PngLoader15deleteImageDataEv
	.hidden	_ZN9PngLoader15deleteImageDataEv
	.type	_ZN9PngLoader15deleteImageDataEv, %function
_ZN9PngLoader15deleteImageDataEv:
	.fnstart
.LFB2865:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	ldr	r0, [r0, #56]
	cmp	r0, #0
	beq	.L29
	bl	s3eFree
	mov	r3, #0
	str	r3, [r4, #56]
.L29:
	ldr	r0, [r4, #60]
	cmp	r0, #0
	beq	.L31
	bl	s3eFree
	mov	r0, #0
	str	r0, [r4, #60]
.L31:
	ldmfd	sp!, {r4, lr}
	bx	lr
	.fnend
	.size	_ZN9PngLoader15deleteImageDataEv, .-_ZN9PngLoader15deleteImageDataEv
	.section	.text._ZN9PngLoader5abortEv,"ax",%progbits
	.align	2
	.global	_ZN9PngLoader5abortEv
	.hidden	_ZN9PngLoader5abortEv
	.type	_ZN9PngLoader5abortEv, %function
_ZN9PngLoader5abortEv:
	.fnstart
.LFB2864:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	ldr	r0, [r0, #4]
	cmp	r0, #0
	beq	.L33
	bl	s3eFileClose
	mov	r3, #0
	str	r3, [r4, #4]
.L33:
	add	r1, r4, #16
	add	r0, r4, #12
	mov	r2, #0
	bl	png_destroy_read_struct
	ldmfd	sp!, {r4, lr}
	bx	lr
	.fnend
	.size	_ZN9PngLoader5abortEv, .-_ZN9PngLoader5abortEv
	.section	.text._ZN9PngLoader6updateEi,"ax",%progbits
	.align	2
	.global	_ZN9PngLoader6updateEi
	.hidden	_ZN9PngLoader6updateEi
	.type	_ZN9PngLoader6updateEi, %function
_ZN9PngLoader6updateEi:
	.fnstart
.LFB2867:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	ldrb	r5, [r0, #0]	@ zero_extendqisi2
	cmp	r5, #0
	mov	r4, r0
	movne	r0, #1
	beq	.L41
.L37:
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L41:
	ldr	ip, [r4, #52]
	ldr	lr, [r4, #24]
	ldr	r0, [r4, #60]
	rsb	r6, ip, lr
	cmp	r1, r6
	movcc	r6, r1
	mov	r3, r6
	add	r1, r0, ip, asl #2
	mov	r2, r5
	ldr	r0, [r4, #12]
	bl	png_read_rows
	ldr	r2, [r4, #52]
	ldr	r3, [r4, #24]
	add	r6, r6, r2
	cmp	r6, r3
	str	r6, [r4, #52]
	movne	r0, r5
	bne	.L37
.L38:
	mov	r5, #1
	mov	r0, r4
	strb	r5, [r4, #0]
	bl	_ZN9PngLoader5abortEv
	mov	r0, r5
	b	.L37
	.fnend
	.size	_ZN9PngLoader6updateEi, .-_ZN9PngLoader6updateEi
	.global	__aeabi_i2f
	.global	__aeabi_fdiv
	.section	.text._ZN11QuBaseImage9getUVDataEv,"ax",%progbits
	.align	2
	.global	_ZN11QuBaseImage9getUVDataEv
	.hidden	_ZN11QuBaseImage9getUVDataEv
	.type	_ZN11QuBaseImage9getUVDataEv, %function
_ZN11QuBaseImage9getUVDataEv:
	.fnstart
.LFB2848:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	.save {r4, r5, r6, r7, lr}
	mov	r4, r0
	.pad #36
	sub	sp, sp, #36
	ldr	r0, [r0, #12]
	bl	__aeabi_i2f
	mov	r5, r0
	ldr	r0, [r4, #20]
	bl	__aeabi_i2f
	mov	r1, r0
	mov	r0, r5
	bl	__aeabi_fdiv
	mov	r6, r0
	ldr	r0, [r4, #16]
	bl	__aeabi_i2f
	mov	r7, r0
	ldr	r0, [r4, #24]
	bl	__aeabi_i2f
	mov	r1, r0
	mov	r0, r7
	bl	__aeabi_fdiv
	mov	r3, #0
	add	r2, sp, #4
	str	r3, [r2], #4
	str	r3, [r2], #4
	str	r3, [r2], #4
	str	r3, [r2], #4
	str	r3, [r2], #4
	str	r3, [r2], #4
	mov	r4, r0
	str	r3, [r2, #0]
	mov	r0, #32
	str	r3, [sp, #0]
	bl	_Znaj
	str	r6, [sp, #8]	@ float
	mov	r5, sp
	mov	ip, r0
	ldmia	r5!, {r0, r1, r2, r3}
	str	r4, [sp, #28]	@ float
	str	r4, [sp, #20]	@ float
	mov	r4, ip
	str	r6, [sp, #24]	@ float
	stmia	r4!, {r0, r1, r2, r3}
	ldmia	r5, {r0, r1, r2, r3}
	stmia	r4, {r0, r1, r2, r3}
	mov	r0, ip
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
	.fnend
	.size	_ZN11QuBaseImage9getUVDataEv, .-_ZN11QuBaseImage9getUVDataEv
	.section	.text._ZN11QuBaseImage6unbindEv,"ax",%progbits
	.align	2
	.global	_ZN11QuBaseImage6unbindEv
	.hidden	_ZN11QuBaseImage6unbindEv
	.type	_ZN11QuBaseImage6unbindEv, %function
_ZN11QuBaseImage6unbindEv:
	.fnstart
.LFB2847:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, #3552
	add	r4, r4, #1
	mov	r0, r4
	mov	r1, #0
	bl	glBindTexture
	mov	r0, r4
	bl	glDisable
	ldmfd	sp!, {r4, lr}
	bx	lr
	.fnend
	.size	_ZN11QuBaseImage6unbindEv, .-_ZN11QuBaseImage6unbindEv
	.section	.text._ZN11QuBaseImage4bindEv,"ax",%progbits
	.align	2
	.global	_ZN11QuBaseImage4bindEv
	.hidden	_ZN11QuBaseImage4bindEv
	.type	_ZN11QuBaseImage4bindEv, %function
_ZN11QuBaseImage4bindEv:
	.fnstart
.LFB2846:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	ldrb	r0, [r0, #4]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L48
	mov	r1, #3552
	add	r0, r1, #1
	bl	glEnable
	mov	r0, #3552
	add	r0, r0, #1
	ldr	r1, [r4, #8]
	bl	glBindTexture
	ldrb	r0, [r4, #4]	@ zero_extendqisi2
.L48:
	ldmfd	sp!, {r4, lr}
	bx	lr
	.fnend
	.size	_ZN11QuBaseImage4bindEv, .-_ZN11QuBaseImage4bindEv
	.section	.text._ZN11QuBaseImage7destroyEv,"ax",%progbits
	.align	2
	.global	_ZN11QuBaseImage7destroyEv
	.hidden	_ZN11QuBaseImage7destroyEv
	.type	_ZN11QuBaseImage7destroyEv, %function
_ZN11QuBaseImage7destroyEv:
	.fnstart
.LFB2844:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	ldrb	r3, [r0, #4]	@ zero_extendqisi2
	cmp	r3, #0
	mov	r4, r0
	movne	r0, #1
	addne	r1, r4, #8
	blne	glDeleteTextures
.L51:
	mvn	r1, #0
	mov	r0, #0
	str	r1, [r4, #8]
	strb	r0, [r4, #4]
	ldmfd	sp!, {r4, lr}
	bx	lr
	.fnend
	.size	_ZN11QuBaseImage7destroyEv, .-_ZN11QuBaseImage7destroyEv
	.section	.text.T.560,"ax",%progbits
	.align	2
	.type	T.560, %function
T.560:
	.fnstart
.LFB3257:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	cmp	r0, #0
	.pad #12
	sub	sp, sp, #12
	str	r0, [sp, #4]
	beq	.L72
.L81:
	ldr	r3, [sp, #4]
	ldr	fp, [r3, #12]
	cmp	fp, #0
	beq	.L55
.L80:
	ldr	r4, [fp, #12]
	cmp	r4, #0
	beq	.L56
.L79:
	ldr	r5, [r4, #12]
	cmp	r5, #0
	beq	.L57
.L78:
	ldr	r6, [r5, #12]
	cmp	r6, #0
	beq	.L58
.L77:
	ldr	r7, [r6, #12]
	cmp	r7, #0
	beq	.L59
.L76:
	ldr	r8, [r7, #12]
	cmp	r8, #0
	beq	.L60
.L75:
	ldr	sl, [r8, #12]
	cmp	sl, #0
	beq	.L61
.L74:
	ldr	r9, [sl, #12]
	cmp	r9, #0
	beq	.L62
	b	.L73
.L82:
	mov	r9, r3
.L73:
	ldr	r0, [r9, #12]
	bl	T.560
	ldr	r1, [r9, #8]
	mov	r0, r9
	str	r1, [sp, #0]
	bl	free
	ldr	r3, [sp, #0]
	cmp	r3, #0
	bne	.L82
.L62:
	ldr	r9, [sl, #8]
	mov	r0, sl
	bl	free
	cmp	r9, #0
	movne	sl, r9
	bne	.L74
.L61:
	ldr	sl, [r8, #8]
	mov	r0, r8
	bl	free
	cmp	sl, #0
	movne	r8, sl
	bne	.L75
.L60:
	ldr	r8, [r7, #8]
	mov	r0, r7
	bl	free
	cmp	r8, #0
	movne	r7, r8
	bne	.L76
.L59:
	ldr	r7, [r6, #8]
	mov	r0, r6
	bl	free
	cmp	r7, #0
	movne	r6, r7
	bne	.L77
.L58:
	ldr	r6, [r5, #8]
	mov	r0, r5
	bl	free
	cmp	r6, #0
	movne	r5, r6
	bne	.L78
.L57:
	ldr	r5, [r4, #8]
	mov	r0, r4
	bl	free
	cmp	r5, #0
	movne	r4, r5
	bne	.L79
.L56:
	ldr	r4, [fp, #8]
	mov	r0, fp
	bl	free
	cmp	r4, #0
	movne	fp, r4
	bne	.L80
.L55:
	ldr	r2, [sp, #4]
	ldr	r4, [r2, #8]
	mov	r0, r2
	bl	free
	cmp	r4, #0
	strne	r4, [sp, #4]
	bne	.L81
.L72:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
	.fnend
	.size	T.560, .-T.560
	.section	.text._ZN11QuBaseImageD0Ev,"axG",%progbits,_ZN11QuBaseImageD0Ev,comdat
	.align	2
	.weak	_ZN11QuBaseImageD0Ev
	.hidden	_ZN11QuBaseImageD0Ev
	.type	_ZN11QuBaseImageD0Ev, %function
_ZN11QuBaseImageD0Ev:
	.fnstart
.LFB2821:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	ldrb	r0, [r0, #4]	@ zero_extendqisi2
	ldr	r3, .L86
	cmp	r0, #0
	str	r3, [r4, #0]
	movne	r0, #1
	addne	r1, r4, #8
	blne	glDeleteTextures
.L84:
	mov	r2, #0
	mvn	r1, #0
	mov	r0, r4
	strb	r2, [r4, #4]
	str	r1, [r4, #8]
	bl	_ZdlPv
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L87:
	.align	2
.L86:
	.word	.LANCHOR0+32
	.fnend
	.size	_ZN11QuBaseImageD0Ev, .-_ZN11QuBaseImageD0Ev
	.section	.text._ZN11QuBaseImageD1Ev,"axG",%progbits,_ZN11QuBaseImageD1Ev,comdat
	.align	2
	.weak	_ZN11QuBaseImageD1Ev
	.hidden	_ZN11QuBaseImageD1Ev
	.type	_ZN11QuBaseImageD1Ev, %function
_ZN11QuBaseImageD1Ev:
	.fnstart
.LFB2820:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	.save {r4, lr}
	mov	r4, r0
	ldrb	r0, [r0, #4]	@ zero_extendqisi2
	ldr	r3, .L91
	cmp	r0, #0
	str	r3, [r4, #0]
	movne	r0, #1
	addne	r1, r4, #8
	blne	glDeleteTextures
.L89:
	mov	r2, #0
	mvn	r1, #0
	strb	r2, [r4, #4]
	str	r1, [r4, #8]
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L92:
	.align	2
.L91:
	.word	.LANCHOR0+32
	.fnend
	.size	_ZN11QuBaseImageD1Ev, .-_ZN11QuBaseImageD1Ev
	.section	.text._ZN11QuBaseImage12setIdAndLoadEi,"ax",%progbits
	.align	2
	.global	_ZN11QuBaseImage12setIdAndLoadEi
	.hidden	_ZN11QuBaseImage12setIdAndLoadEi
	.type	_ZN11QuBaseImage12setIdAndLoadEi, %function
_ZN11QuBaseImage12setIdAndLoadEi:
	.fnstart
.LFB2849:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	ldrb	r3, [r0, #4]	@ zero_extendqisi2
	cmp	r3, #0
	mov	r4, r0
	mov	r5, r1
	movne	r0, #1
	addne	r1, r4, #8
	blne	glDeleteTextures
.L94:
	mov	r0, #1
	str	r5, [r4, #8]
	strb	r0, [r4, #4]
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
	.fnend
	.size	_ZN11QuBaseImage12setIdAndLoadEi, .-_ZN11QuBaseImage12setIdAndLoadEi
	.section	.text.T.562,"ax",%progbits
	.align	2
	.type	T.562, %function
T.562:
	.fnstart
.LFB3259:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	.save {r4, r5, r6, r7, r8, lr}
	ldr	r4, .L111
	ldr	ip, [r4, #0]
	cmp	r2, ip
	mov	r5, r2
	mov	r6, r0
	mov	r8, r3
	beq	.L97
	ldr	r3, [sp, #24]
	cmp	r3, #0
	beq	.L107
.L98:
	mov	r0, #24
	bl	malloc
	subs	r7, r0, #0
	beq	.L104
.L105:
	ldmia	r8, {ip, lr}	@ phole ldm
	str	lr, [r7, #20]
	str	ip, [r7, #16]
	str	r7, [r5, #12]
	ldr	r0, [r4, #0]
	ldr	r1, [r0, #12]
	cmp	r5, r1
	streq	r7, [r0, #12]
.L101:
	mov	r1, #0
	str	r1, [r7, #12]
	str	r1, [r7, #8]
	str	r5, [r7, #4]
	ldr	r0, [r4, #0]
	add	r1, r0, #4
	mov	r0, r7
	bl	_ZN4_STL10_Rb_globalIbE10_RebalanceEPNS_18_Rb_tree_node_baseERS3_
	ldr	r2, [r4, #4]
	add	r3, r2, #1
	str	r3, [r4, #4]
	mov	r0, r6
	str	r7, [r6, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L107:
	cmp	r1, #0
	beq	.L108
.L97:
	mov	r0, #24
	bl	malloc
	subs	r7, r0, #0
	beq	.L109
.L99:
	ldr	r1, [r8, #4]
	ldr	lr, [r8, #0]
	str	r1, [r7, #20]
	str	lr, [r7, #16]
	str	r7, [r5, #8]
	ldr	r3, [r4, #0]
	cmp	r5, r3
	beq	.L110
	ldr	r2, [r3, #8]
	cmp	r5, r2
	streq	r7, [r3, #8]
	b	.L101
.L110:
	str	r7, [r5, #4]
	ldr	r3, .L111
	ldr	ip, [r3, #0]
	str	r7, [ip, #12]
	b	.L101
.L104:
	mov	r0, #24
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
	mov	r7, r0
	b	.L105
.L109:
	mov	r0, #24
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
	mov	r7, r0
	b	.L99
.L108:
	ldr	r2, [r8, #0]
	ldr	r0, [r5, #16]
	cmp	r2, r0
	bcs	.L98
	b	.L97
.L112:
	.align	2
.L111:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	T.562, .-T.562
	.section	.text.T.563,"ax",%progbits
	.align	2
	.type	T.563, %function
T.563:
	.fnstart
.LFB3260:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	ldr	r3, .L136
	ldr	r3, [r3, #0]
	ldr	r6, [r3, #4]
	cmp	r6, #0
	.pad #16
	sub	sp, sp, #16
	mov	r4, r0
	mov	r5, r1
	beq	.L114
	ldr	r0, [r1, #0]
	b	.L118
.L133:
	mov	r6, r2
.L118:
	ldr	r2, [r6, #16]
	cmp	r0, r2
	ldrcc	r2, [r6, #8]
	ldrcs	r2, [r6, #12]
	movcc	r1, #1
	movcs	r1, #0
	cmp	r2, #0
	bne	.L133
	cmp	r1, #0
	moveq	r0, r6
	beq	.L121
.L120:
	ldr	r0, [r3, #8]
	cmp	r6, r0
	beq	.L134
	mov	r0, r6
	bl	_ZN4_STL10_Rb_globalIbE12_M_decrementEPNS_18_Rb_tree_node_baseE
.L121:
	ldr	r3, [r5, #0]
	ldr	r1, [r0, #16]
	cmp	r1, r3
	movcs	r3, #0
	strcs	r0, [r4, #0]
	strcsb	r3, [r4, #4]
	bcc	.L135
.L113:
	mov	r0, r4
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L135:
	mov	lr, #0
	mov	r2, r6
	add	r0, sp, #12
	mov	r1, lr
	mov	r3, r5
	str	lr, [sp, #0]
	bl	T.562
	ldr	r2, [sp, #12]
	mov	r0, #1
	str	r2, [r4, #0]
	strb	r0, [r4, #4]
	b	.L113
.L134:
	mov	r1, r6
	mov	r2, r6
	mov	ip, #0
	mov	r3, r5
	add	r0, sp, #8
	str	ip, [sp, #0]
	bl	T.562
	ldr	r2, [sp, #8]
	mov	r1, #1
	str	r2, [r4, #0]
	strb	r1, [r4, #4]
	b	.L113
.L114:
	mov	r6, r3
	b	.L120
.L137:
	.align	2
.L136:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	T.563, .-T.563
	.section	.text.T.561,"ax",%progbits
	.align	2
	.type	T.561, %function
T.561:
	.fnstart
.LFB3258:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	.save {r4, r5, r6, r7, lr}
	ldr	r6, .L172
	ldr	r3, [r6, #0]
	ldr	ip, [r1, #0]
	mov	r7, r1
	ldr	r1, [r3, #8]
	cmp	ip, r1
	.pad #44
	sub	sp, sp, #44
	mov	r5, r2
	mov	r4, r0
	beq	.L166
	cmp	ip, r3
	beq	.L167
	mov	r0, ip
	bl	_ZN4_STL10_Rb_globalIbE12_M_decrementEPNS_18_Rb_tree_node_baseE
	mov	ip, r0
	ldr	r0, [r7, #0]
	ldr	r3, [r5, #0]
	ldr	r2, [r0, #16]
	cmp	r3, r2
	bcs	.L153
	ldr	r1, [ip, #16]
	cmp	r3, r1
	bls	.L154
.L157:
	ldr	r1, [ip, #12]
	cmp	r1, #0
	beq	.L164
.L159:
	mov	r1, r0
	mov	ip, #0
	mov	r3, r5
	mov	r0, r4
	mov	r2, r1
	str	ip, [sp, #0]
	bl	T.562
	b	.L138
.L153:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	ip, [r7, #0]
	ldr	r3, [r5, #0]
	ldr	r2, [ip, #16]
	cmp	r2, r3
	bcs	.L156
	ldr	lr, [r6, #0]
	cmp	r0, lr
	beq	.L157
	ldr	r1, [r0, #16]
	cmp	r3, r1
	bcs	.L158
	b	.L157
.L166:
	ldr	r3, [r6, #4]
	cmp	r3, #0
	beq	.L168
	ldr	r2, [r2, #0]
	ldr	r1, [ip, #16]
	cmp	r2, r1
	bcc	.L169
	bhi	.L144
.L156:
	str	ip, [r4, #0]
.L138:
	mov	r0, r4
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L154:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
.L158:
	add	r0, sp, #32
	mov	r1, r5
	bl	T.563
	ldr	r0, [sp, #32]
	str	r0, [r4, #0]
	b	.L138
.L167:
	ldr	r2, [ip, #12]
	ldr	r3, [r5, #0]
	ldr	lr, [r2, #16]
	cmp	lr, r3
	bcc	.L170
	mov	r1, r5
	add	r0, sp, #24
	bl	T.563
	ldr	ip, [sp, #24]
	str	ip, [r4, #0]
	b	.L138
.L144:
	mov	r0, ip
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r3, [r6, #0]
	cmp	r0, r3
	beq	.L171
	ldr	r2, [r5, #0]
	ldr	r1, [r0, #16]
	cmp	r2, r1
	bcs	.L147
	ldr	ip, [r7, #0]
	ldr	r1, [ip, #12]
	cmp	r1, #0
	bne	.L159
.L164:
	mov	r2, ip
	mov	r3, r5
	mov	r0, r4
	str	ip, [sp, #0]
	bl	T.562
	b	.L138
.L170:
	mov	r3, r5
	mov	r1, #0
	str	ip, [sp, #0]
	bl	T.562
	b	.L138
.L168:
	add	r0, sp, #8
	mov	r1, r2
	bl	T.563
	ldr	r0, [sp, #8]
	str	r0, [r4, #0]
	b	.L138
.L169:
	mov	r1, ip
	mov	r2, ip
	mov	r3, r5
	mov	ip, #0
	str	ip, [sp, #0]
	bl	T.562
	b	.L138
.L147:
	add	r0, sp, #16
	mov	r1, r5
	bl	T.563
	ldr	r0, [sp, #16]
	str	r0, [r4, #0]
	b	.L138
.L171:
	ldr	lr, [r7, #0]
	mov	r3, r5
	mov	r2, lr
	mov	r0, r4
	mov	r1, #0
	str	lr, [sp, #0]
	bl	T.562
	b	.L138
.L173:
	.align	2
.L172:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	T.561, .-T.561
	.global	__cxa_end_cleanup
	.section	.text._ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_,"axG",%progbits,_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_,comdat
	.align	2
	.weak	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
	.hidden	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
	.type	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_, %function
_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_:
	.fnstart
.LFB2876:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	.save {r4, r5, r6, r7, r8, lr}
	mov	r3, #0
	str	r3, [r0, #0]
	str	r3, [r0, #4]
	ldmia	r1, {r5, r7}	@ phole ldm
	rsb	r6, r5, r7
	adds	r8, r6, #1
	mov	r4, r0
	str	r3, [r0, #8]
	beq	.L177
	mov	r0, r8
	bl	malloc
	cmp	r0, #0
	beq	.L187
.L178:
	add	r8, r0, r8
	str	r8, [r4, #8]
	str	r0, [r4, #0]
	str	r0, [r4, #4]
	b	.L184
.L177:
	ldr	r0, .L188
.LEHB0:
	bl	_ZN4_STL24__stl_throw_length_errorEPKc
	ldr	r0, [r4, #0]
.L184:
	cmp	r5, r7
	beq	.L180
	mov	r1, r5
	mov	r2, r6
	bl	memmove
	add	r0, r0, r6
.L180:
	mov	r1, #0
	str	r0, [r4, #4]
	strb	r1, [r0, #0]
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L187:
	mov	r0, r8
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
.LEHE0:
	b	.L178
.L186:
.L182:
	ldr	r3, [r4, #0]
	cmp	r3, #0
	mov	r4, r0
	movne	r0, r3
	blne	free
.L183:
	mov	r0, r4
.LEHB1:
	bl	__cxa_end_cleanup
.LEHE1:
.L189:
	.align	2
.L188:
	.word	.LC0
	.global	__gxx_personality_v0
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA2876:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2876-.LLSDACSB2876
.LLSDACSB2876:
	.uleb128 .LEHB0-.LFB2876
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L186-.LFB2876
	.uleb128 0x0
	.uleb128 .LEHB1-.LFB2876
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE2876:
	.fnend
	.size	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_, .-_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
	.section	.text._ZN14QuDynamicImage10setTextureEv,"ax",%progbits
	.align	2
	.global	_ZN14QuDynamicImage10setTextureEv
	.hidden	_ZN14QuDynamicImage10setTextureEv
	.type	_ZN14QuDynamicImage10setTextureEv, %function
_ZN14QuDynamicImage10setTextureEv:
	.fnstart
.LFB2850:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	.save {r4, r5, r6, r7, r8, sl, lr}
	ldrb	r3, [r0, #4]	@ zero_extendqisi2
	cmp	r3, #0
	.pad #36
	sub	sp, sp, #36
	mov	r4, r0
	movne	r0, #1
	addne	r1, r4, #8
	blne	glDeleteTextures
.L191:
	mvn	r6, #0
	add	sl, sp, #32
	str	r6, [sl, #-4]!
	mov	r5, #0
	mov	r7, #3312
	add	r0, r7, #5
	strb	r5, [r4, #4]
	str	r6, [r4, #8]
	mov	r1, #1
	ldr	r7, [r4, #20]
	ldr	r6, [r4, #24]
	ldr	r8, [r4, #32]
	bl	glPixelStorei
	mov	r1, sl
	mov	r0, #1
	bl	glGenTextures
	mov	r1, #3552
	add	r0, r1, #1
	ldr	r1, [sp, #28]
	bl	glBindTexture
	mov	r3, #10240
	mov	ip, #3552
	mov	r2, #33024
	add	r1, r3, #2
	add	r0, ip, #1
	add	r2, r2, #47
	bl	glTexParameterx
	mov	ip, #33024
	mov	r0, #3552
	mov	r1, #10240
	add	r2, ip, #47
	add	r0, r0, #1
	add	r1, r1, #3
	bl	glTexParameterx
	mov	r3, #3552
	mov	r2, #9728
	add	r0, r3, #1
	add	r2, r2, #1
	mov	r1, #10240
	bl	glTexParameteri
	mov	ip, #9728
	mov	r0, #3552
	mov	r1, #10240
	add	r2, ip, #1
	add	r0, r0, #1
	add	r1, r1, #1
	bl	glTexParameteri
	mov	r3, #6400
	add	ip, r3, #8
	mov	r2, #5120
	mov	r1, #3552
	add	lr, r2, #1
	add	r0, r1, #1
	mov	r2, ip
	mov	r1, r5
	mov	r3, r7
	str	r5, [sp, #4]
	str	r6, [sp, #0]
	str	lr, [sp, #12]
	str	r8, [sp, #16]
	str	ip, [sp, #8]
	bl	glTexImage2D
	ldrb	r0, [r4, #4]	@ zero_extendqisi2
	cmp	r0, r5
	ldr	r5, [sp, #28]
	movne	r0, #1
	addne	r1, r4, #8
	blne	glDeleteTextures
.L192:
	mov	r0, #1
	strb	r0, [r4, #28]
	str	r5, [r4, #8]
	strb	r0, [r4, #4]
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	bx	lr
	.fnend
	.size	_ZN14QuDynamicImage10setTextureEv, .-_ZN14QuDynamicImage10setTextureEv
	.global	__aeabi_uidiv
	.section	.text._ZN9PngLoader7setFileEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE,"ax",%progbits
	.align	2
	.global	_ZN9PngLoader7setFileEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.hidden	_ZN9PngLoader7setFileEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.type	_ZN9PngLoader7setFileEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE, %function
_ZN9PngLoader7setFileEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE:
	.fnstart
.LFB2866:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
	.save {r4, r5, lr}
	.pad #44
	sub	sp, sp, #44
	str	r0, [sp, #28]
	ldr	r0, [r1, #0]
	ldr	r1, .L214
	bl	s3eFileOpen
	ldr	r1, [sp, #28]
	add	r4, sp, #32
	mov	r3, r0
	str	r0, [r1, #4]
	mov	r2, #1
	mov	r1, #8
	mov	r0, r4
	bl	s3eFileRead
	mov	r0, r4
	mov	r1, #8
	bl	png_check_sig
	cmp	r0, #0
	bne	.L211
.L196:
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, lr}
	bx	lr
.L211:
	mov	r1, #0
	mov	r2, r1
	ldr	r0, .L214+4
	mov	r3, r1
	bl	png_create_read_struct
	ldr	r2, [sp, #28]
	cmp	r0, #0
	mov	r4, r0
	str	r0, [r2, #12]
	beq	.L210
	bl	png_create_info_struct
	ldr	r3, [sp, #28]
	cmp	r0, #0
	mov	r4, r0
	str	r0, [r3, #16]
	beq	.L210
	ldr	r5, [sp, #28]
	ldr	r0, [r5, #12]
	bl	setjmp
	subs	r4, r0, #0
	bne	.L212
	ldr	r3, [sp, #28]
	ldr	r2, .L214+8
	ldr	r0, [r3, #12]
	ldr	r1, [r3, #4]
	bl	png_set_read_fn
	ldr	r1, [sp, #28]
	ldr	r0, [r1, #12]
	mov	r1, #8
	bl	png_set_sig_bytes
	ldr	r2, [sp, #28]
	add	r0, r2, #12
	ldmia	r0, {r0, r1}	@ phole ldm
	bl	png_read_info
	ldr	r5, [sp, #28]
	add	r0, r5, #12
	ldmia	r0, {r0, r1}	@ phole ldm
	add	lr, r5, #36
	add	r2, r5, #20
	add	r3, r5, #24
	add	ip, r5, #40
	str	lr, [sp, #0]
	str	ip, [sp, #4]
	str	r4, [sp, #8]
	str	r4, [sp, #12]
	str	r4, [sp, #16]
	bl	png_get_IHDR
	ldr	ip, [sp, #28]
	mov	r5, r0
	add	r0, ip, #12
	ldmia	r0, {r0, r1}	@ phole ldm
	bl	png_get_rowbytes
	ldr	r3, [sp, #28]
	str	r0, [r3, #48]
	ldr	r2, [sp, #28]
	ldr	r1, [r2, #20]
	bl	__aeabi_uidiv
	ldr	r1, [sp, #28]
	cmp	r5, #1
	str	r0, [r1, #44]
	bne	.L210
	ldr	ip, [sp, #28]
	ldr	r3, [ip, #20]
	cmp	r3, #0
	beq	.L203
	sub	ip, r3, #1
	orr	r3, ip, ip, lsr #1
	orr	r1, r3, r3, lsr #2
	orr	r2, r1, r1, lsr #4
	orr	r5, r2, r2, lsr #8
	orr	lr, r5, r5, lsr #16
	add	r5, lr, #1
.L203:
	mul	r0, r5, r0
	ldr	ip, [sp, #28]
	ldr	r3, [ip, #24]
	cmp	r3, #0
	subne	r3, r3, #1
	orrne	r3, r3, r3, lsr #1
	orrne	r3, r3, r3, lsr #2
	orrne	r3, r3, r3, lsr #4
	orrne	r3, r3, r3, lsr #8
	str	r5, [ip, #28]
	orrne	r3, r3, r3, lsr #16
	ldr	lr, [sp, #28]
	moveq	r3, #1
	addne	r3, r3, #1
	mul	r0, r3, r0
	str	r3, [lr, #32]
	bl	s3eMalloc
	ldr	r5, [sp, #28]
	ldr	r1, [r5, #24]
	str	r0, [r5, #56]
	mov	r0, r1, asl #2
	bl	s3eMalloc
	ldr	r2, [r5, #24]
	cmp	r2, #0
	str	r0, [r5, #60]
	beq	.L206
	mov	r2, #0
	ldr	r1, [r5, #44]
	ldr	r4, [r5, #28]
	mov	r3, r2
	b	.L207
.L213:
	ldr	r0, [r5, #60]
	ldr	r1, [r5, #44]
	ldr	r4, [r5, #28]
.L207:
	mul	r1, r3, r1
	ldr	r5, [sp, #28]
	ldr	lr, [r5, #56]
	mla	r1, r4, r1, lr
	str	r1, [r0, r2, asl #2]
	ldr	r0, [r5, #24]
	add	r3, r3, #1
	cmp	r3, r0
	mov	r2, r3
	bcc	.L213
.L206:
	ldr	r2, [sp, #28]
	mov	r0, #1
	strb	r0, [r2, #1]
	b	.L196
.L212:
	ldr	r0, [r5, #4]
	cmp	r0, #0
	beq	.L200
	bl	s3eFileClose
	mov	r0, #0
	str	r0, [r5, #4]
.L200:
	ldr	ip, [sp, #28]
	mov	r2, #0
	add	r0, ip, #12
	add	r1, ip, #16
	bl	png_destroy_read_struct
	mov	r0, #0
	b	.L196
.L210:
	ldr	r0, [sp, #28]
	bl	_ZN9PngLoader5abortEv
	mov	r0, r4
	b	.L196
.L215:
	.align	2
.L214:
	.word	.LC1
	.word	.LC2
	.word	_ZN9PngLoader22readDataFromFileHandleEP14png_struct_defPhj
	.fnend
	.size	_ZN9PngLoader7setFileEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE, .-_ZN9PngLoader7setFileEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.section	.text._ZN15QuStupidPointerI9PngLoaderE3setEPS0_i,"axG",%progbits,_ZN15QuStupidPointerI9PngLoaderE3setEPS0_i,comdat
	.align	2
	.weak	_ZN15QuStupidPointerI9PngLoaderE3setEPS0_i
	.hidden	_ZN15QuStupidPointerI9PngLoaderE3setEPS0_i
	.type	_ZN15QuStupidPointerI9PngLoaderE3setEPS0_i, %function
_ZN15QuStupidPointerI9PngLoaderE3setEPS0_i:
	.fnstart
.LFB3008:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, lr}
	ldr	ip, [r0, #0]
	cmp	ip, #0
	.pad #32
	sub	sp, sp, #32
	mov	r5, r0
	mov	r4, r1
	mov	r8, r2
	beq	.L217
	ldr	r6, .L290
	ldr	r0, [r6, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L221
	mov	r1, r0
	b	.L222
.L280:
	mov	r1, r3
	mov	r3, r2
.L222:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L280
.L221:
	cmp	r3, r0
	beq	.L224
	ldr	r2, [r3, #16]
	cmp	ip, r2
	mov	r2, r3
	bcs	.L225
.L224:
	add	r2, sp, #8
	str	r3, [sp, #24]
	add	r0, sp, #28
	mov	r3, #0
	add	r1, sp, #24
	str	ip, [sp, #8]
	str	r3, [sp, #12]
	bl	T.561
	ldr	r2, [sp, #28]
.L225:
	ldr	r1, [r2, #20]
	sub	r0, r1, #1
	cmp	r0, #0
	str	r0, [r2, #20]
	beq	.L281
.L217:
	cmp	r4, #0
	stmia	r5, {r4, r8}	@ phole stm
	beq	.L275
	ldr	r5, .L290
	ldr	r3, [r5, #0]
	ldr	ip, [r3, #4]
	cmp	ip, #0
	beq	.L255
	mov	r0, r3
	mov	r2, ip
	b	.L259
.L282:
	mov	r0, r2
	mov	r2, r1
.L259:
	ldr	r1, [r2, #16]
	cmp	r4, r1
	ldrhi	r1, [r2, #12]
	ldrls	r1, [r2, #8]
	movhi	r2, r0
	cmp	r1, #0
	bne	.L282
	cmp	r3, r2
	beq	.L255
	ldr	r1, [r2, #16]
	cmp	r4, r1
	bcc	.L255
.L260:
	cmp	r2, r3
	mov	r1, r2
	beq	.L279
.L262:
	ldr	ip, [r1, #20]
	add	r0, ip, #1
	str	r0, [r1, #20]
.L275:
	add	sp, sp, #32
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L283:
	ldr	r0, [ip, #16]
	cmp	r4, r0
	movls	r3, ip
	ldrhi	ip, [ip, #12]
	ldrls	ip, [ip, #8]
.L279:
	cmp	ip, #0
	bne	.L283
	cmp	r2, r3
	beq	.L268
	ldr	ip, [r3, #16]
	cmp	r4, ip
	mov	r2, r3
	bcs	.L269
.L268:
	mov	r2, sp
	mov	lr, #0
	add	r0, sp, #20
	add	r1, sp, #16
	str	r3, [sp, #16]
	stmia	sp, {r4, lr}	@ phole stm
	bl	T.561
	ldr	r2, [sp, #20]
.L269:
	mov	r3, #0
	str	r3, [r2, #20]
	ldr	r0, [r5, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	beq	.L270
	mov	r1, r0
	b	.L274
.L284:
	mov	r1, r3
	mov	r3, r2
.L274:
	ldr	r2, [r3, #16]
	cmp	r4, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L284
	cmp	r0, r3
	beq	.L270
	ldr	r2, [r3, #16]
	cmp	r4, r2
	movcs	r0, r3
.L270:
	mov	r1, r0
	b	.L262
.L255:
	mov	r2, r3
	b	.L260
.L281:
	ldr	r1, [r6, #0]
	ldr	r7, [r1, #4]
	cmp	r7, #0
	ldr	r2, [r5, #0]
	moveq	sl, r1
	moveq	r9, r1
	beq	.L237
	mov	r0, r1
	mov	r9, r7
	b	.L230
.L285:
	mov	r0, r9
	mov	r9, r3
.L230:
	ldr	r3, [r9, #16]
	cmp	r2, r3
	ldrhi	r3, [r9, #12]
	ldrls	r3, [r9, #8]
	movhi	r9, r0
	cmp	r3, #0
	bne	.L285
	mov	r0, r1
	b	.L234
.L286:
	mov	r0, r7
	mov	r7, r3
.L234:
	ldr	r3, [r7, #16]
	cmp	r2, r3
	ldrcs	r3, [r7, #12]
	ldrcc	r3, [r7, #8]
	movcs	r7, r0
	cmp	r3, #0
	bne	.L286
	cmp	r9, r7
	mov	sl, r9
	moveq	r9, r7
	beq	.L237
	mov	r0, r9
.L238:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r7, r0
	bne	.L238
	ldr	r1, [r6, #0]
	mov	sl, r7
.L237:
	ldr	ip, [r1, #8]
	cmp	r9, ip
	beq	.L287
.L240:
	cmp	r9, sl
	bne	.L277
	b	.L243
.L246:
	mov	r9, r7
.L277:
	mov	r0, r9
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r2, [r6, #0]
	mov	r7, r0
	add	r3, r2, #12
	add	r1, r2, #4
	mov	r0, r9
	add	r2, r2, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L245:
	ldr	r3, [r6, #4]
	cmp	sl, r7
	sub	lr, r3, #1
	str	lr, [r6, #4]
	bne	.L246
.L243:
	ldr	r0, [r5, #4]
	cmp	r0, #1
	beq	.L288
	ldr	r3, [r5, #0]
	cmp	r3, #0
	beq	.L217
	ldr	r6, [r3, #-4]
	add	r6, r3, r6, asl #6
	cmp	r6, r3
	mov	r7, #0
	beq	.L250
.L289:
	sub	r6, r6, #64
	ldr	r0, [r6, #4]
	cmp	r0, #0
	beq	.L251
	bl	s3eFileClose
	str	r7, [r6, #4]
.L251:
	add	r0, r6, #12
	add	r1, r6, #16
	mov	r2, #0
	bl	png_destroy_read_struct
	ldr	r0, [r6, #56]
	cmp	r0, #0
	beq	.L252
	bl	s3eFree
	str	r7, [r6, #56]
.L252:
	ldr	r0, [r6, #60]
	cmp	r0, #0
	beq	.L253
	bl	s3eFree
	str	r7, [r6, #60]
.L253:
	ldr	r3, [r5, #0]
	cmp	r6, r3
	bne	.L289
.L250:
	sub	r0, r6, #8
	bl	_ZdaPv
	b	.L217
.L288:
	ldr	r6, [r5, #0]
	cmp	r6, #0
	beq	.L217
	mov	r0, r6
	bl	_ZN9PngLoader5abortEv
	mov	r0, r6
	bl	_ZN9PngLoader15deleteImageDataEv
	mov	r0, r6
	bl	_ZdlPv
	b	.L217
.L287:
	cmp	sl, r1
	bne	.L240
	ldr	r2, [r6, #4]
	cmp	r2, #0
	ldr	r6, .L290
	beq	.L243
	ldr	r0, [sl, #4]
	bl	T.560
	ldr	ip, [r6, #0]
	str	ip, [ip, #8]
	ldr	r1, [r6, #0]
	mov	r3, #0
	str	r3, [r1, #4]
	ldr	r0, [r6, #0]
	str	r0, [r0, #12]
	str	r3, [r6, #4]
	b	.L243
.L291:
	.align	2
.L290:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	_ZN15QuStupidPointerI9PngLoaderE3setEPS0_i, .-_ZN15QuStupidPointerI9PngLoaderE3setEPS0_i
	.section	.text._ZN11QuBaseImage11reincarnateEv,"ax",%progbits
	.align	2
	.global	_ZN11QuBaseImage11reincarnateEv
	.hidden	_ZN11QuBaseImage11reincarnateEv
	.type	_ZN11QuBaseImage11reincarnateEv, %function
_ZN11QuBaseImage11reincarnateEv:
	.fnstart
.LFB2845:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	mov	r8, #0
	mvn	r6, #0
	mov	r4, r0
	ldr	r7, [r1, #8]
	.pad #36
	sub	sp, sp, #36
	str	r6, [r1, #8]
	strb	r8, [r1, #4]
	mov	r0, #28
.LEHB2:
	bl	_Znwj
	mov	r5, r0
	ldr	r0, .L364
	mov	r3, #1
	str	r6, [r5, #8]
	str	r0, [r5, #0]
	strb	r8, [r5, #4]
	ldr	r6, .L364+4
	str	r3, [r4, #4]
	str	r5, [r4, #0]
	ldr	r3, [r6, #0]
	ldr	ip, [r3, #4]
	cmp	ip, r8
	beq	.L294
	mov	r0, r3
	mov	r2, ip
	b	.L298
.L354:
	mov	r0, r2
	mov	r2, r1
.L298:
	ldr	r1, [r2, #16]
	cmp	r5, r1
	ldrhi	r1, [r2, #12]
	ldrls	r1, [r2, #8]
	movhi	r2, r0
	cmp	r1, #0
	bne	.L354
	cmp	r3, r2
	beq	.L294
	ldr	r1, [r2, #16]
	cmp	r5, r1
	bcc	.L294
.L299:
	cmp	r2, r3
	mov	r1, r2
	beq	.L353
	ldr	lr, [r1, #20]
	add	r5, lr, #1
	str	r5, [r1, #20]
	ldr	r5, [r4, #0]
	ldrb	r0, [r5, #4]	@ zero_extendqisi2
	cmp	r0, #0
	bne	.L355
.L315:
	mov	r2, #1
	str	r7, [r5, #8]
	strb	r2, [r5, #4]
	mov	r0, r4
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L356:
	ldr	lr, [ip, #16]
	cmp	r5, lr
	movls	r3, ip
	ldrhi	ip, [ip, #12]
	ldrls	ip, [ip, #8]
.L353:
	cmp	ip, #0
	bne	.L356
	cmp	r2, r3
	beq	.L307
	ldr	r2, [r3, #16]
	cmp	r5, r2
	mov	r2, r3
	bcs	.L308
.L307:
	add	r2, sp, #8
	mov	ip, #0
	add	r0, sp, #28
	add	r1, sp, #24
	str	r3, [sp, #24]
	str	ip, [sp, #12]
	str	r5, [sp, #8]
	bl	T.561
.LEHE2:
	ldr	r2, [sp, #28]
.L308:
	mov	r3, #0
	str	r3, [r2, #20]
	ldr	r0, [r6, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	beq	.L309
	mov	r1, r0
	b	.L313
.L357:
	mov	r1, r3
	mov	r3, r2
.L313:
	ldr	r2, [r3, #16]
	cmp	r5, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L357
	cmp	r0, r3
	beq	.L309
	ldr	r1, [r3, #16]
	cmp	r5, r1
	movcs	r0, r3
.L309:
	mov	r1, r0
	ldr	lr, [r1, #20]
	add	r5, lr, #1
	str	r5, [r1, #20]
	ldr	r5, [r4, #0]
	ldrb	r0, [r5, #4]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L315
.L355:
	mov	r0, #1
	add	r1, r5, #8
.LEHB3:
	bl	glDeleteTextures
.LEHE3:
	b	.L315
.L294:
	mov	r2, r3
	b	.L299
.L350:
.L316:
	ldr	ip, [r4, #0]
	cmp	ip, #0
	ldrne	r1, [r6, #0]
	mov	sl, r0
	ldr	r8, [r4, #4]
	ldrne	r2, [r1, #4]
	movne	r3, r1
	beq	.L317
.L318:
	cmp	r2, #0
	beq	.L358
	ldr	r7, [r2, #16]
	cmp	ip, r7
	ldrhi	r7, [r2, #12]
	ldrls	r7, [r2, #8]
	movhi	r2, r3
	mov	r3, r2
	mov	r2, r7
	b	.L318
.L363:
	ldr	r3, [r4, #0]
	cmp	r3, #0
	movne	r0, r3
	ldrne	r5, [r3, #0]
	ldrne	ip, [r5, #8]
	movne	lr, pc
	bxne	ip
.L317:
	str	r8, [r4, #4]
	mov	r8, #0
	str	r8, [r4, #0]
	mov	r0, sl
.LEHB4:
	bl	__cxa_end_cleanup
.LEHE4:
.L358:
	cmp	r3, r1
	beq	.L324
	ldr	r9, [r3, #16]
	cmp	ip, r9
	mov	r2, r3
	bcs	.L325
.L324:
	mov	r2, sp
	add	r0, sp, #20
	add	r1, sp, #16
	mov	fp, #0
	str	r3, [sp, #16]
	str	ip, [sp, #0]
	str	fp, [sp, #4]
	bl	T.561
	ldr	r2, [sp, #20]
.L325:
	ldr	r3, [r2, #20]
	sub	ip, r3, #1
	cmp	ip, #0
	str	ip, [r2, #20]
	bne	.L317
	ldr	r7, [r6, #0]
	ldr	r3, [r7, #4]
	ldr	r1, [r4, #0]
	mov	r2, r3
	mov	r9, r7
.L326:
	cmp	r2, #0
	beq	.L359
	ldr	r0, [r2, #16]
	cmp	r1, r0
	ldrhi	r0, [r2, #12]
	ldrls	r0, [r2, #8]
	movhi	r2, r9
	mov	r9, r2
	mov	r2, r0
	b	.L326
.L359:
	mov	fp, r9
.L331:
	cmp	r3, #0
	beq	.L360
	ldr	r5, [r3, #16]
	cmp	r1, r5
	ldrcs	r5, [r3, #12]
	ldrcc	r5, [r3, #8]
	movcs	r3, r7
	mov	r7, r3
	mov	r3, r5
	b	.L331
.L360:
	mov	r5, r9
.L336:
	cmp	r7, r5
	beq	.L361
	mov	r0, r5
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	mov	r5, r0
	b	.L336
.L361:
	ldr	r3, [r6, #0]
	ldr	r1, [r3, #8]
	cmp	r9, r1
	ldr	r9, .L364+4
	beq	.L362
.L351:
	cmp	r5, fp
	beq	.L342
	mov	r0, fp
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r9, [r6, #0]
	mov	r7, r0
	add	r3, r9, #12
	mov	r0, fp
	add	r1, r9, #4
	add	r2, r9, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L344:
	ldr	r3, [r6, #4]
	sub	lr, r3, #1
	str	lr, [r6, #4]
	mov	fp, r7
	b	.L351
.L362:
	cmp	r7, r3
	bne	.L351
	ldr	r6, [r9, #4]
	cmp	r6, #0
	beq	.L342
	ldr	r0, [r7, #4]
	bl	T.560
	ldr	ip, [r9, #0]
	str	ip, [ip, #8]
	ldr	fp, [r9, #0]
	mov	r7, #0
	str	r7, [fp, #4]
	ldr	r2, [r9, #0]
	str	r2, [r2, #12]
	str	r7, [r9, #4]
.L342:
	ldr	r0, [r4, #4]
	cmp	r0, #1
	beq	.L363
	ldr	r0, [r4, #0]
	cmp	r0, #0
	beq	.L317
	ldr	r1, [r0, #-4]
	mov	r5, #28
	mla	r5, r1, r5, r0
.L347:
	cmp	r0, r5
	sub	r5, r5, #28
	beq	.L346
	ldr	r7, [r5, #0]
	mov	r0, r5
	ldr	ip, [r7, #4]
	mov	lr, pc
	bx	ip
	ldr	r0, [r4, #0]
	b	.L347
.L346:
	sub	r0, r0, #8
	bl	_ZdaPv
	b	.L317
.L365:
	.align	2
.L364:
	.word	.LANCHOR0+32
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA2845:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2845-.LLSDACSB2845
.LLSDACSB2845:
	.uleb128 .LEHB2-.LFB2845
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB3-.LFB2845
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L350-.LFB2845
	.uleb128 0x0
	.uleb128 .LEHB4-.LFB2845
	.uleb128 .LEHE4-.LEHB4
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE2845:
	.fnend
	.size	_ZN11QuBaseImage11reincarnateEv, .-_ZN11QuBaseImage11reincarnateEv
	.section	.text._ZN14QuDynamicImage10initializeEii,"ax",%progbits
	.align	2
	.global	_ZN14QuDynamicImage10initializeEii
	.hidden	_ZN14QuDynamicImage10initializeEii
	.type	_ZN14QuDynamicImage10initializeEii, %function
_ZN14QuDynamicImage10initializeEii:
	.fnstart
.LFB2851:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 88
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #0
	subne	r3, r1, #1
	orrne	r3, r3, r3, lsr #1
	orrne	r3, r3, r3, lsr #2
	orrne	r3, r3, r3, lsr #4
	orrne	r3, r3, r3, lsr #8
	orrne	r3, r3, r3, lsr #16
	addne	r3, r3, #1
	moveq	r3, #1
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, lr}
	mov	r5, r0
	str	r1, [r0, #12]
	str	r2, [r0, #16]
	movne	r0, r3, asl #2
	moveq	r0, #4
	cmp	r2, #0
	str	r3, [r5, #20]
	subne	r3, r2, #1
	orrne	r3, r3, r3, lsr #1
	orrne	r3, r3, r3, lsr #2
	orrne	r3, r3, r3, lsr #4
	orrne	r3, r3, r3, lsr #8
	orrne	r3, r3, r3, lsr #16
	moveq	r3, #1
	addne	r3, r3, #1
	mul	r0, r3, r0
	str	r3, [r5, #24]
	.pad #112
	sub	sp, sp, #112
.LEHB5:
	bl	_Znaj
.LEHE5:
	add	r3, r5, #20
	ldmia	r3, {r3, sl}	@ phole ldm
	mul	sl, r3, sl
	subs	r4, r0, #0
	mov	sl, sl, asl #2
	beq	.L371
	ldr	r6, .L553
	ldr	r3, [r6, #0]
	ldr	ip, [r3, #4]
	cmp	ip, #0
	beq	.L372
	mov	r0, r3
	mov	r2, ip
	b	.L376
.L529:
	mov	r0, r2
	mov	r2, r1
.L376:
	ldr	r1, [r2, #16]
	cmp	r4, r1
	ldrhi	r1, [r2, #12]
	ldrls	r1, [r2, #8]
	movhi	r2, r0
	cmp	r1, #0
	bne	.L529
	cmp	r3, r2
	beq	.L372
	ldr	r1, [r2, #16]
	cmp	r4, r1
	bcc	.L372
.L377:
	cmp	r2, r3
	mov	r1, r2
	beq	.L527
.L379:
	ldr	r3, [r1, #20]
	add	lr, r3, #1
	str	lr, [r1, #20]
.L371:
	ldr	ip, [r5, #32]
	cmp	ip, #0
	beq	.L395
	ldr	r6, .L553
	ldr	r0, [r6, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L399
	mov	r1, r0
	b	.L400
.L530:
	mov	r1, r3
	mov	r3, r2
.L400:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L530
.L399:
	cmp	r3, r0
	beq	.L402
	ldr	r1, [r3, #16]
	cmp	ip, r1
	mov	r2, r3
	bcs	.L403
.L402:
	str	ip, [sp, #52]
	add	r0, sp, #100
	mov	ip, #0
	add	r1, sp, #96
	add	r2, sp, #52
	str	ip, [sp, #56]
	str	r3, [sp, #96]
.LEHB6:
	bl	T.561
.LEHE6:
	ldr	r2, [sp, #100]
.L403:
	ldr	r7, [r2, #20]
	sub	r0, r7, #1
	cmp	r0, #0
	str	r0, [r2, #20]
	beq	.L531
.L395:
	cmp	r4, #0
	str	r4, [r5, #32]
	str	sl, [r5, #36]
	beq	.L428
	ldr	r6, .L553
	ldr	r3, [r6, #0]
	ldr	ip, [r3, #4]
	cmp	ip, #0
	beq	.L429
	mov	r0, r3
	mov	r2, ip
	b	.L433
.L532:
	mov	r0, r2
	mov	r2, r1
.L433:
	ldr	r1, [r2, #16]
	cmp	r4, r1
	ldrhi	r1, [r2, #12]
	ldrls	r1, [r2, #8]
	movhi	r2, r0
	cmp	r1, #0
	bne	.L532
	cmp	r3, r2
	beq	.L429
	ldr	r7, [r2, #16]
	cmp	r4, r7
	bcc	.L429
.L434:
	cmp	r2, r3
	mov	r1, r2
	beq	.L528
.L436:
	ldr	r7, [r1, #20]
	add	r3, r7, #1
	str	r3, [r1, #20]
	ldr	r0, [r6, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L455
	mov	r1, r0
	b	.L456
.L533:
	mov	r1, r3
	mov	r3, r2
.L456:
	ldr	r2, [r3, #16]
	cmp	r4, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L533
.L455:
	cmp	r3, r0
	beq	.L458
	ldr	r1, [r3, #16]
	cmp	r4, r1
	mov	r2, r3
	bcs	.L459
.L458:
	add	r2, sp, #36
	mov	ip, #0
	add	r0, sp, #84
	add	r1, sp, #80
	str	r3, [sp, #80]
	str	ip, [sp, #40]
	str	r4, [sp, #36]
.LEHB7:
	bl	T.561
	ldr	r2, [sp, #84]
.L459:
	ldr	r0, [r2, #20]
	sub	lr, r0, #1
	cmp	lr, #0
	str	lr, [r2, #20]
	beq	.L534
.L428:
	ldrb	r4, [r5, #4]	@ zero_extendqisi2
	cmp	r4, #0
	movne	r0, #1
	addne	r1, r5, #8
	blne	glDeleteTextures
.L484:
	mvn	r8, #0
	add	sl, sp, #112
	str	r8, [sl, #-44]!
	mov	r4, #0
	mov	r7, #3312
	add	r0, r7, #5
	strb	r4, [r5, #4]
	str	r8, [r5, #8]
	mov	r1, #1
	ldr	r7, [r5, #20]
	ldr	r6, [r5, #24]
	ldr	r8, [r5, #32]
	bl	glPixelStorei
	mov	r1, sl
	mov	r0, #1
	bl	glGenTextures
	mov	r3, #3552
	add	r0, r3, #1
	ldr	r1, [sp, #68]
	bl	glBindTexture
	mov	ip, #3552
	mov	r1, #10240
	mov	r2, #33024
	add	r0, ip, #1
	add	r1, r1, #2
	add	r2, r2, #47
	bl	glTexParameterx
	mov	r3, #10240
	mov	ip, #33024
	mov	r0, #3552
	add	r1, r3, #3
	add	r2, ip, #47
	add	r0, r0, #1
	bl	glTexParameterx
	mov	r2, #3552
	mov	r1, #9728
	add	r0, r2, #1
	add	r2, r1, #1
	mov	r1, #10240
	bl	glTexParameteri
	mov	r3, #10240
	mov	ip, #9728
	mov	r0, #3552
	add	r1, r3, #1
	add	r2, ip, #1
	add	r0, r0, #1
	bl	glTexParameteri
	mov	r2, #6400
	add	ip, r2, #8
	mov	r1, #5120
	mov	r3, #3552
	add	lr, r1, #1
	add	r0, r3, #1
	mov	r1, r4
	mov	r2, ip
	mov	r3, r7
	str	r4, [sp, #4]
	str	r6, [sp, #0]
	str	lr, [sp, #12]
	str	r8, [sp, #16]
	str	ip, [sp, #8]
	bl	glTexImage2D
	ldrb	r0, [r5, #4]	@ zero_extendqisi2
	cmp	r0, r4
	ldr	r4, [sp, #68]
	movne	r0, #1
	addne	r1, r5, #8
	blne	glDeleteTextures
.L517:
	mov	r0, #1
	strb	r0, [r5, #28]
	str	r4, [r5, #8]
	strb	r0, [r5, #4]
	add	sp, sp, #112
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L535:
	ldr	r0, [ip, #16]
	cmp	r4, r0
	movls	r3, ip
	ldrhi	ip, [ip, #12]
	ldrls	ip, [ip, #8]
.L527:
	cmp	ip, #0
	bne	.L535
	cmp	r2, r3
	beq	.L385
	ldr	r2, [r3, #16]
	cmp	r4, r2
	mov	r2, r3
	bcs	.L386
.L385:
	add	r2, sp, #60
	mov	ip, #0
	add	r0, sp, #108
	add	r1, sp, #104
	str	ip, [sp, #64]
	str	r3, [sp, #104]
	str	r4, [sp, #60]
	bl	T.561
.LEHE7:
	ldr	r2, [sp, #108]
.L386:
	mov	r7, #0
	str	r7, [r2, #20]
	ldr	r0, [r6, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	beq	.L389
	mov	r1, r0
	b	.L393
.L536:
	mov	r1, r3
	mov	r3, r2
.L393:
	ldr	r2, [r3, #16]
	cmp	r4, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L536
	cmp	r0, r3
	beq	.L389
	ldr	r6, [r3, #16]
	cmp	r4, r6
	bcs	.L394
.L389:
	mov	r3, r0
.L394:
	mov	r1, r3
	b	.L379
.L537:
	ldr	r1, [ip, #16]
	cmp	r4, r1
	movls	r3, ip
	ldrhi	ip, [ip, #12]
	ldrls	ip, [ip, #8]
.L528:
	cmp	ip, #0
	bne	.L537
	cmp	r2, r3
	beq	.L442
	ldr	ip, [r3, #16]
	cmp	r4, ip
	mov	r2, r3
	bcs	.L443
.L442:
	mov	lr, #0
	add	r0, sp, #92
	add	r1, sp, #88
	add	r2, sp, #44
	str	lr, [sp, #48]
	str	r3, [sp, #88]
	str	r4, [sp, #44]
.LEHB8:
	bl	T.561
.LEHE8:
	ldr	r2, [sp, #92]
.L443:
	mov	r0, #0
	str	r0, [r2, #20]
	ldr	r0, [r6, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	beq	.L446
	mov	r1, r0
	b	.L450
.L538:
	mov	r1, r3
	mov	r3, r2
.L450:
	ldr	r2, [r3, #16]
	cmp	r4, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L538
	cmp	r0, r3
	beq	.L446
	ldr	r2, [r3, #16]
	cmp	r4, r2
	bcs	.L451
.L446:
	mov	r3, r0
.L451:
	mov	r1, r3
	b	.L436
.L429:
	mov	r2, r3
	b	.L434
.L372:
	mov	r2, r3
	b	.L377
.L534:
	ldr	r1, [r6, #0]
	ldr	r7, [r1, #4]
	cmp	r7, #0
	moveq	r9, r1
	moveq	r8, r1
	beq	.L472
	mov	r2, r1
	mov	r9, r7
	b	.L465
.L539:
	mov	r2, r9
	mov	r9, r3
.L465:
	ldr	r3, [r9, #16]
	cmp	r4, r3
	ldrhi	r3, [r9, #12]
	ldrls	r3, [r9, #8]
	movhi	r9, r2
	cmp	r3, #0
	bne	.L539
	mov	r2, r1
	b	.L469
.L540:
	mov	r2, r7
	mov	r7, r3
.L469:
	ldr	r3, [r7, #16]
	cmp	r4, r3
	ldrcs	r3, [r7, #12]
	ldrcc	r3, [r7, #8]
	movcs	r7, r2
	cmp	r3, #0
	bne	.L540
	cmp	r9, r7
	mov	r8, r9
	moveq	r9, r7
	beq	.L472
	mov	r0, r9
.L473:
.LEHB9:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r7, r0
	bne	.L473
	ldr	r1, [r6, #0]
	mov	r8, r7
.L472:
	ldr	r2, [r1, #8]
	cmp	r9, r2
	beq	.L541
.L475:
	cmp	r9, r8
	bne	.L523
	b	.L478
.L481:
	mov	r9, r7
.L523:
	mov	r0, r9
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r2, [r6, #0]
	mov	r7, r0
	add	r3, r2, #12
	add	r1, r2, #4
	mov	r0, r9
	add	r2, r2, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
.LEHE9:
	cmp	r0, #0
	blne	free
.L480:
	ldr	r0, [r6, #4]
	cmp	r7, r8
	sub	lr, r0, #1
	str	lr, [r6, #4]
	bne	.L481
.L478:
	cmp	sl, #1
	mov	r0, r4
	beq	.L542
	bl	_ZdaPv
	b	.L428
.L531:
	ldr	r1, [r6, #0]
	ldr	r7, [r1, #4]
	cmp	r7, #0
	ldr	r2, [r5, #32]
	beq	.L406
	mov	r0, r1
	mov	r8, r7
	b	.L410
.L543:
	mov	r0, r8
	mov	r8, r3
.L410:
	ldr	r3, [r8, #16]
	cmp	r2, r3
	ldrhi	r3, [r8, #12]
	ldrls	r3, [r8, #8]
	movhi	r8, r0
	cmp	r3, #0
	bne	.L543
	mov	r0, r1
	b	.L414
.L544:
	mov	r0, r7
	mov	r7, r3
.L414:
	ldr	r3, [r7, #16]
	cmp	r2, r3
	ldrcs	r3, [r7, #12]
	ldrcc	r3, [r7, #8]
	movcs	r7, r0
	cmp	r3, #0
	bne	.L544
	cmp	r8, r7
	mov	r9, r8
	moveq	r8, r7
	beq	.L417
	mov	r0, r8
.L418:
.LEHB10:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r7, r0
	bne	.L418
	ldr	r1, [r6, #0]
	mov	r9, r7
.L417:
	ldr	r2, [r1, #8]
	cmp	r8, r2
	beq	.L545
.L420:
	cmp	r8, r9
	bne	.L522
	b	.L423
.L426:
	mov	r8, r7
.L522:
	mov	r0, r8
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r2, [r6, #0]
	mov	r7, r0
	add	r3, r2, #12
	add	r1, r2, #4
	mov	r0, r8
	add	r2, r2, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
.LEHE10:
	cmp	r0, #0
	blne	free
.L425:
	ldr	r3, [r6, #4]
	cmp	r7, r9
	sub	lr, r3, #1
	str	lr, [r6, #4]
	bne	.L426
.L423:
	ldr	r6, [r5, #36]
	cmp	r6, #1
	ldr	r0, [r5, #32]
	beq	.L546
	cmp	r0, #0
	beq	.L395
	bl	_ZdaPv
	b	.L395
.L541:
	cmp	r8, r1
	bne	.L475
	ldr	r3, [r6, #4]
	cmp	r3, #0
	ldr	r6, .L553
	beq	.L478
	ldr	r0, [r8, #4]
.LEHB11:
	bl	T.560
.LEHE11:
	ldr	ip, [r6, #0]
	str	ip, [ip, #8]
	ldr	r1, [r6, #0]
	mov	r8, #0
	str	r8, [r1, #4]
	ldr	r7, [r6, #0]
	str	r7, [r7, #12]
	str	r8, [r6, #4]
	b	.L478
.L406:
	mov	r8, r1
	mov	r9, r1
	b	.L417
.L545:
	cmp	r9, r1
	bne	.L420
	ldr	lr, [r6, #4]
	cmp	lr, #0
	beq	.L423
	ldr	r0, [r9, #4]
.LEHB12:
	bl	T.560
.LEHE12:
	ldr	r0, [r6, #0]
	str	r0, [r0, #8]
	ldr	ip, [r6, #0]
	mov	r3, #0
	str	r3, [ip, #4]
	ldr	r1, [r6, #0]
	str	r1, [r1, #12]
	str	r3, [r6, #4]
	b	.L423
.L546:
	bl	_ZdlPv
	b	.L395
.L542:
	bl	_ZdlPv
	b	.L428
.L519:
.L485:
	cmp	r4, #0
	ldrne	r1, [r6, #0]
	mov	r8, r0
	ldrne	r2, [r1, #4]
	movne	r3, r1
	beq	.L486
.L487:
	cmp	r2, #0
	beq	.L547
	ldr	r5, [r2, #16]
	cmp	r4, r5
	ldrhi	r5, [r2, #12]
	ldrls	r5, [r2, #8]
	movhi	r2, r3
	mov	r3, r2
	mov	r2, r5
	b	.L487
.L552:
	cmp	r5, r3
	bne	.L524
	ldr	r6, [r7, #4]
	cmp	r6, #0
	beq	.L513
	ldr	r0, [r5, #4]
	bl	T.560
	ldr	r3, [r7, #0]
	str	r3, [r3, #8]
	ldr	r9, [r7, #0]
	mov	r0, #0
	str	r0, [r9, #4]
	ldr	r5, [r7, #0]
	str	r5, [r5, #12]
	str	r0, [r7, #4]
.L513:
	cmp	sl, #1
	mov	r0, r4
	beq	.L548
	bl	_ZdaPv
.L486:
	mov	r0, r8
.LEHB13:
	bl	__cxa_end_cleanup
.LEHE13:
.L547:
	cmp	r3, r1
	beq	.L493
	ldr	r9, [r3, #16]
	cmp	r4, r9
	mov	r2, r3
	bcs	.L494
.L493:
	add	r2, sp, #28
	add	r0, sp, #76
	add	r1, sp, #72
	mov	r7, #0
	str	r7, [sp, #32]
	str	r3, [sp, #72]
	str	r4, [sp, #28]
	bl	T.561
	ldr	r2, [sp, #76]
.L494:
	ldr	r1, [r2, #20]
	sub	r3, r1, #1
	cmp	r3, #0
	str	r3, [r2, #20]
	bne	.L486
	ldr	r5, [r6, #0]
	ldr	r3, [r5, #4]
	mov	r7, r5
	mov	r2, r3
.L497:
	cmp	r2, #0
	beq	.L549
	ldr	ip, [r2, #16]
	cmp	r4, ip
	ldrhi	ip, [r2, #12]
	ldrls	ip, [r2, #8]
	movhi	r2, r7
	mov	r7, r2
	mov	r2, ip
	b	.L497
.L549:
	mov	r9, r7
.L502:
	cmp	r3, #0
	beq	.L550
	ldr	r2, [r3, #16]
	cmp	r4, r2
	ldrcs	r2, [r3, #12]
	ldrcc	r2, [r3, #8]
	movcs	r3, r5
	mov	r5, r3
	mov	r3, r2
	b	.L502
.L550:
	mov	r0, r7
.L507:
	cmp	r5, r0
	beq	.L551
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	b	.L507
.L551:
	ldr	r3, [r6, #0]
	ldr	lr, [r3, #8]
	cmp	r7, lr
	ldr	r7, .L553
	beq	.L552
.L524:
	cmp	r5, r9
	beq	.L513
	mov	r0, r9
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	ip, [r6, #0]
	mov	r7, r0
	add	r3, ip, #12
	mov	r0, r9
	add	r1, ip, #4
	add	r2, ip, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L515:
	ldr	r2, [r6, #4]
	sub	r1, r2, #1
	str	r1, [r6, #4]
	mov	r9, r7
	b	.L524
.L548:
	bl	_ZdlPv
	b	.L486
.L554:
	.align	2
.L553:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA2851:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2851-.LLSDACSB2851
.LLSDACSB2851:
	.uleb128 .LEHB5-.LFB2851
	.uleb128 .LEHE5-.LEHB5
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB6-.LFB2851
	.uleb128 .LEHE6-.LEHB6
	.uleb128 .L519-.LFB2851
	.uleb128 0x0
	.uleb128 .LEHB7-.LFB2851
	.uleb128 .LEHE7-.LEHB7
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB8-.LFB2851
	.uleb128 .LEHE8-.LEHB8
	.uleb128 .L519-.LFB2851
	.uleb128 0x0
	.uleb128 .LEHB9-.LFB2851
	.uleb128 .LEHE9-.LEHB9
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB10-.LFB2851
	.uleb128 .LEHE10-.LEHB10
	.uleb128 .L519-.LFB2851
	.uleb128 0x0
	.uleb128 .LEHB11-.LFB2851
	.uleb128 .LEHE11-.LEHB11
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB12-.LFB2851
	.uleb128 .LEHE12-.LEHB12
	.uleb128 .L519-.LFB2851
	.uleb128 0x0
	.uleb128 .LEHB13-.LFB2851
	.uleb128 .LEHE13-.LEHB13
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE2851:
	.fnend
	.size	_ZN14QuDynamicImage10initializeEii, .-_ZN14QuDynamicImage10initializeEii
	.section	.text._ZN10QuPngImage9loadImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE,"ax",%progbits
	.align	2
	.global	_ZN10QuPngImage9loadImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.hidden	_ZN10QuPngImage9loadImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.type	_ZN10QuPngImage9loadImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE, %function
_ZN10QuPngImage9loadImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE:
	.fnstart
.LFB2860:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 96
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	add	r3, r0, #36
	cmp	r3, r1
	.pad #100
	sub	sp, sp, #100
	mov	r4, r0
	mov	sl, r1
	beq	.L556
	ldmia	r1, {r5, r8}	@ phole ldm
	ldr	r3, [r0, #36]
	cmp	r5, r8
	mov	r1, r3
	beq	.L557
	ldr	r7, [r0, #40]
	cmp	r3, r7
	beq	.L559
	rsb	r1, r8, r5
	mvn	r2, r1
	ands	r2, r2, #3
	beq	.L707
	ldrb	r6, [r5], #1	@ zero_extendqisi2
	strb	r6, [r3], #1
	ldr	r7, [r0, #40]
	cmp	r3, r7
	beq	.L740
	cmp	r2, #1
	beq	.L707
	cmp	r2, #2
	beq	.L732
	ldrb	r2, [r5], #1	@ zero_extendqisi2
	strb	r2, [r3], #1
	ldr	ip, [r0, #40]
	cmp	r3, ip
	beq	.L740
.L732:
	ldrb	r1, [r5], #1	@ zero_extendqisi2
	strb	r1, [r3], #1
	ldr	lr, [r4, #40]
	cmp	r3, lr
	beq	.L740
.L707:
	ldrb	r0, [r5], #1	@ zero_extendqisi2
	strb	r0, [r3], #1
	cmp	r8, r5
	mov	r2, r5
	mov	r1, r3
	beq	.L557
	ldr	r0, [r4, #40]
	cmp	r3, r0
	beq	.L740
	ldrb	r0, [r5], #1	@ zero_extendqisi2
	strb	r0, [r3], #1
	ldr	r0, [r4, #40]
	cmp	r3, r0
	beq	.L740
	ldrb	r7, [r5, #0]	@ zero_extendqisi2
	strb	r7, [r3, #0]
	ldr	r0, [r4, #40]
	add	r3, r1, #2
	cmp	r3, r0
	add	r5, r2, #2
	beq	.L740
	ldrb	r5, [r2, #2]	@ zero_extendqisi2
	strb	r5, [r1, #2]
	ldr	r0, [r4, #40]
	add	r3, r1, #3
	cmp	r3, r0
	add	r5, r2, #3
	bne	.L707
.L740:
	ldr	r1, [r4, #36]
	mov	r7, r1
.L559:
	rsb	r6, r5, r8
	cmn	r6, #1
	rsb	r7, r7, r3
	beq	.L702
	rsb	r2, r6, #-16777216
	add	r3, r2, #16711680
	add	lr, r3, #65280
	add	ip, lr, #254
	cmp	r7, ip
	bls	.L561
.L702:
	ldr	r0, .L765
.LEHB14:
	bl	_ZN4_STL24__stl_throw_length_errorEPKc
	ldr	r1, [r4, #36]
.L561:
	ldr	r2, [r4, #44]
	sub	r3, r2, #1
	rsb	lr, r1, r3
	add	ip, r6, r7
	cmp	ip, lr
	bls	.L562
	add	r8, r7, #1
	cmp	r6, r7
	addcs	r7, r8, r6
	addcc	r7, r8, r7
	cmp	r7, #0
	moveq	r9, r7
	bne	.L743
.L564:
	ldr	r8, [r4, #40]
	cmp	r1, r8
	moveq	r0, r9
	beq	.L567
	rsb	r8, r1, r8
	mov	r0, r9
	mov	r2, r8
	bl	memmove
	add	r0, r0, r8
.L567:
	mov	r1, r5
	mov	r2, r6
	bl	memmove
	mov	r1, #0
	strb	r1, [r0, r6]
	ldr	r3, [r4, #36]
	cmp	r3, #0
	add	r6, r0, r6
	movne	r0, r3
	blne	free
.L568:
	add	r0, r9, r7
	str	r6, [r4, #40]
	str	r0, [r4, #44]
	str	r9, [r4, #36]
.L556:
	mov	r0, #64
	bl	_Znwj
.LEHE14:
	mov	r6, #0
	strb	r6, [r0, #1]
	str	r6, [r0, #56]
	str	r6, [r0, #60]
	str	r6, [r0, #52]
	strb	r6, [r0, #0]
	ldr	r6, .L765+4
	mov	r3, #1
	str	r3, [sp, #60]
	str	r0, [sp, #56]
	ldr	r3, [r6, #0]
	ldr	ip, [r3, #4]
	cmp	ip, #0
	mov	r5, r0
	add	r9, r4, #28
	beq	.L571
	mov	r0, r3
	mov	r2, ip
	b	.L575
.L744:
	mov	r0, r2
	mov	r2, r1
.L575:
	ldr	r1, [r2, #16]
	cmp	r5, r1
	ldrhi	r1, [r2, #12]
	ldrls	r1, [r2, #8]
	movhi	r2, r0
	cmp	r1, #0
	bne	.L744
	cmp	r3, r2
	beq	.L571
	ldr	r1, [r2, #16]
	cmp	r5, r1
	bcc	.L571
.L576:
	cmp	r2, r3
	mov	r1, r2
	beq	.L741
.L578:
	ldr	r0, [r1, #20]
	add	r3, r0, #1
	str	r3, [r1, #20]
	ldr	ip, [r4, #28]
	ldr	r5, [sp, #60]
	cmp	ip, #0
	ldr	r7, [sp, #56]
	str	r5, [sp, #4]
	beq	.L594
	ldr	r0, [r6, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L598
	mov	r1, r0
	b	.L599
.L745:
	mov	r1, r3
	mov	r3, r2
.L599:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L745
.L598:
	cmp	r3, r0
	beq	.L601
	ldr	r1, [r3, #16]
	cmp	ip, r1
	mov	r2, r3
	bcs	.L602
.L601:
	str	ip, [sp, #40]
	add	r0, sp, #84
	mov	ip, #0
	add	r1, sp, #80
	add	r2, sp, #40
	str	ip, [sp, #44]
	str	r3, [sp, #80]
.LEHB15:
	bl	T.561
.LEHE15:
	ldr	r2, [sp, #84]
.L602:
	ldr	r5, [r2, #20]
	sub	lr, r5, #1
	cmp	lr, #0
	str	lr, [r2, #20]
	beq	.L746
.L594:
	ldr	lr, [sp, #4]
	cmp	r7, #0
	str	lr, [r4, #32]
	str	r7, [r4, #28]
	beq	.L632
	ldr	r3, [r6, #0]
	ldr	ip, [r3, #4]
	cmp	ip, #0
	beq	.L633
	mov	r0, r3
	mov	r2, ip
	b	.L637
.L747:
	mov	r0, r2
	mov	r2, r1
.L637:
	ldr	r1, [r2, #16]
	cmp	r7, r1
	ldrhi	r1, [r2, #12]
	ldrls	r1, [r2, #8]
	movhi	r2, r0
	cmp	r1, #0
	bne	.L747
	cmp	r3, r2
	beq	.L633
	ldr	r0, [r2, #16]
	cmp	r7, r0
	bcc	.L633
.L638:
	cmp	r2, r3
	mov	r1, r2
	beq	.L742
.L640:
	ldr	lr, [r1, #20]
	add	r7, lr, #1
	str	r7, [r1, #20]
.L632:
	ldr	ip, [sp, #56]
	cmp	ip, #0
	ldr	r7, [sp, #60]
	beq	.L655
	ldr	r0, [r6, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L659
	mov	r1, r0
	b	.L660
.L748:
	mov	r1, r3
	mov	r3, r2
.L660:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L748
.L659:
	cmp	r3, r0
	beq	.L662
	ldr	r0, [r3, #16]
	cmp	ip, r0
	mov	r2, r3
	bcs	.L663
.L662:
	add	r2, sp, #24
	add	r0, sp, #68
	add	r1, sp, #64
	mov	r5, #0
	str	ip, [sp, #24]
	str	r5, [sp, #28]
	str	r3, [sp, #64]
.LEHB16:
	bl	T.561
	ldr	r2, [sp, #68]
.L663:
	ldr	r3, [r2, #20]
	sub	ip, r3, #1
	cmp	ip, #0
	str	ip, [r2, #20]
	beq	.L749
.L655:
	ldr	r6, [r4, #28]
	add	r5, sp, #12
	mov	r1, sl
	mov	ip, #0
	mov	r0, r5
	str	ip, [sp, #56]
	str	r7, [sp, #60]
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
.LEHE16:
	mov	r0, r6
	mov	r1, r5
.LEHB17:
	bl	_ZN9PngLoader7setFileEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
.LEHE17:
	ldr	r0, [sp, #12]
	cmp	r0, #0
	blne	free
.L700:
	ldr	r3, [r4, #28]
	ldr	r1, [r3, #20]
	str	r1, [r4, #12]
	ldr	lr, [r3, #24]
	str	lr, [r4, #16]
	ldr	r2, [r3, #28]
	str	r2, [r4, #20]
	ldr	r0, [r3, #32]
	str	r0, [r4, #24]
	ldrb	r0, [r3, #1]	@ zero_extendqisi2
	add	sp, sp, #100
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L557:
	ldr	r2, [r4, #40]
	cmp	r3, r2
	beq	.L556
	ldrb	r9, [r2, #0]	@ zero_extendqisi2
	strb	r9, [r3, #0]
	ldr	r7, [r4, #40]
	rsb	r6, r2, r3
	add	r5, r7, r6
	str	r5, [r4, #40]
	b	.L556
.L750:
	ldr	r0, [ip, #16]
	cmp	r5, r0
	movls	r3, ip
	ldrhi	ip, [ip, #12]
	ldrls	ip, [ip, #8]
.L741:
	cmp	ip, #0
	bne	.L750
	cmp	r2, r3
	beq	.L584
	ldr	r2, [r3, #16]
	cmp	r5, r2
	mov	r2, r3
	bcs	.L585
.L584:
	add	r2, sp, #48
	add	r0, sp, #92
	add	r1, sp, #88
	mov	r7, #0
	str	r7, [sp, #52]
	str	r3, [sp, #88]
	str	r5, [sp, #48]
.LEHB18:
	bl	T.561
.LEHE18:
	ldr	r2, [sp, #92]
.L585:
	mov	ip, #0
	str	ip, [r2, #20]
	ldr	r0, [r6, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	beq	.L588
	mov	r1, r0
	b	.L592
.L751:
	mov	r1, r3
	mov	r3, r2
.L592:
	ldr	r2, [r3, #16]
	cmp	r5, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L751
	cmp	r0, r3
	beq	.L588
	ldr	lr, [r3, #16]
	cmp	r5, lr
	movcs	r0, r3
.L588:
	mov	r1, r0
	b	.L578
.L752:
	ldr	r5, [ip, #16]
	cmp	r7, r5
	movls	r3, ip
	ldrhi	ip, [ip, #12]
	ldrls	ip, [ip, #8]
.L742:
	cmp	ip, #0
	bne	.L752
	cmp	r2, r3
	beq	.L646
	ldr	r2, [r3, #16]
	cmp	r7, r2
	mov	r2, r3
	bcs	.L647
.L646:
	mov	ip, #0
	add	r0, sp, #76
	add	r1, sp, #72
	add	r2, sp, #32
	str	ip, [sp, #36]
	str	r3, [sp, #72]
	str	r7, [sp, #32]
.LEHB19:
	bl	T.561
	ldr	r2, [sp, #76]
.L647:
	mov	r3, #0
	str	r3, [r2, #20]
	ldr	r0, [r6, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	beq	.L650
	mov	r1, r0
	b	.L654
.L753:
	mov	r1, r3
	mov	r3, r2
.L654:
	ldr	r2, [r3, #16]
	cmp	r7, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L753
	cmp	r0, r3
	beq	.L650
	ldr	r1, [r3, #16]
	cmp	r7, r1
	movcs	r0, r3
.L650:
	mov	r1, r0
	b	.L640
.L633:
	mov	r2, r3
	b	.L638
.L571:
	mov	r2, r3
	b	.L576
.L562:
	add	r1, r5, #1
	cmp	r8, r1
	ldr	r0, [r4, #40]
	beq	.L569
	add	r0, r0, #1
	rsb	r2, r1, r8
	bl	memmove
	ldr	r0, [r4, #40]
.L569:
	mov	lr, #0
	strb	lr, [r0, r6]
	ldrb	r9, [r5, #0]	@ zero_extendqisi2
	ldr	ip, [r4, #40]
	strb	r9, [ip, #0]
	ldr	r7, [r4, #40]
	add	r5, r7, r6
	str	r5, [r4, #40]
	b	.L556
.L746:
	ldr	r1, [r6, #0]
	ldr	r5, [r1, #4]
	cmp	r5, #0
	ldr	r2, [r4, #28]
	beq	.L605
	mov	r0, r1
	mov	r8, r5
	b	.L609
.L754:
	mov	r0, r8
	mov	r8, r3
.L609:
	ldr	r3, [r8, #16]
	cmp	r2, r3
	ldrhi	r3, [r8, #12]
	ldrls	r3, [r8, #8]
	movhi	r8, r0
	cmp	r3, #0
	bne	.L754
	mov	r0, r1
	b	.L613
.L755:
	mov	r0, r5
	mov	r5, r3
.L613:
	ldr	r3, [r5, #16]
	cmp	r2, r3
	ldrcs	r3, [r5, #12]
	ldrcc	r3, [r5, #8]
	movcs	r5, r0
	cmp	r3, #0
	bne	.L755
	cmp	r8, r5
	mov	fp, r8
	moveq	r8, r5
	beq	.L616
	mov	r0, r8
.L617:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r5, r0
	bne	.L617
	ldr	r1, [r6, #0]
	mov	fp, r5
.L616:
	ldr	r2, [r1, #8]
	cmp	r8, r2
	beq	.L756
.L619:
	cmp	r8, fp
	bne	.L728
	b	.L622
.L625:
	mov	r8, r5
.L728:
	mov	r0, r8
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r2, [r6, #0]
	mov	r5, r0
	add	r3, r2, #12
	add	r1, r2, #4
	mov	r0, r8
	add	r2, r2, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L624:
	ldr	r0, [r6, #4]
	cmp	r5, fp
	sub	r3, r0, #1
	str	r3, [r6, #4]
	bne	.L625
.L622:
	ldr	r5, [r4, #32]
	cmp	r5, #1
	beq	.L757
	ldr	r3, [r4, #28]
	cmp	r3, #0
	beq	.L594
	ldr	r8, [r3, #-4]
	add	r5, r3, r8, asl #6
	mov	r8, #0
.L735:
	ldr	r1, [r9, #0]
	cmp	r1, r5
	beq	.L628
.L758:
	sub	r5, r5, #64
	ldr	r0, [r5, #4]
	cmp	r0, #0
	beq	.L629
	bl	s3eFileClose
	str	r8, [r5, #4]
.L629:
	add	r0, r5, #12
	add	r1, r5, #16
	mov	r2, #0
	bl	png_destroy_read_struct
	ldr	r0, [r5, #56]
	cmp	r0, #0
	beq	.L630
	bl	s3eFree
	str	r8, [r5, #56]
.L630:
	ldr	r0, [r5, #60]
	cmp	r0, #0
	beq	.L735
	bl	s3eFree
.LEHE19:
	str	r8, [r5, #60]
	ldr	r1, [r9, #0]
	cmp	r1, r5
	bne	.L758
.L628:
	ldr	ip, [r4, #28]
	sub	r0, ip, #8
	bl	_ZdaPv
	b	.L594
.L749:
	ldr	r1, [r6, #0]
	ldr	r5, [r1, #4]
	cmp	r5, #0
	ldr	r2, [sp, #56]
	moveq	r8, r1
	moveq	r9, r1
	beq	.L677
	mov	r0, r1
	mov	r9, r5
	b	.L670
.L759:
	mov	r0, r9
	mov	r9, r3
.L670:
	ldr	r3, [r9, #16]
	cmp	r2, r3
	ldrhi	r3, [r9, #12]
	ldrls	r3, [r9, #8]
	movhi	r9, r0
	cmp	r3, #0
	bne	.L759
	mov	r0, r1
	b	.L674
.L760:
	mov	r0, r5
	mov	r5, r3
.L674:
	ldr	r3, [r5, #16]
	cmp	r2, r3
	ldrcs	r3, [r5, #12]
	ldrcc	r3, [r5, #8]
	movcs	r5, r0
	cmp	r3, #0
	bne	.L760
	cmp	r9, r5
	mov	r8, r9
	moveq	r9, r5
	beq	.L677
	mov	r0, r9
.L678:
.LEHB20:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r5, r0
	bne	.L678
	ldr	r1, [r6, #0]
	mov	r8, r5
.L677:
	ldr	r2, [r1, #8]
	cmp	r9, r2
	beq	.L761
.L680:
	cmp	r8, r9
	bne	.L730
	b	.L683
.L686:
	mov	r9, r5
.L730:
	mov	r0, r9
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r2, [r6, #0]
	mov	r5, r0
	add	r3, r2, #12
	add	r1, r2, #4
	mov	r0, r9
	add	r2, r2, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L685:
	ldr	r1, [r6, #4]
	cmp	r8, r5
	sub	lr, r1, #1
	str	lr, [r6, #4]
	bne	.L686
.L683:
	ldr	r5, [sp, #60]
	cmp	r5, #1
	beq	.L762
	ldr	r0, [sp, #56]
	cmp	r0, #0
	beq	.L655
	ldr	r6, [r0, #-4]
	add	r5, r0, r6, asl #6
	cmp	r0, r5
	mov	r6, #0
	beq	.L690
.L763:
	sub	r5, r5, #64
	ldr	r0, [r5, #4]
	cmp	r0, #0
	beq	.L691
	bl	s3eFileClose
	str	r6, [r5, #4]
.L691:
	add	r0, r5, #12
	add	r1, r5, #16
	mov	r2, #0
	bl	png_destroy_read_struct
	ldr	r0, [r5, #56]
	cmp	r0, #0
	beq	.L692
	bl	s3eFree
	str	r6, [r5, #56]
.L692:
	ldr	r0, [r5, #60]
	cmp	r0, #0
	beq	.L693
	bl	s3eFree
	str	r6, [r5, #60]
.L693:
	ldr	r0, [sp, #56]
	cmp	r0, r5
	bne	.L763
.L690:
	sub	r0, r0, #8
	bl	_ZdaPv
	b	.L655
.L743:
	mov	r0, r7
	bl	malloc
	subs	r9, r0, #0
	beq	.L764
	ldr	r1, [r4, #36]
	b	.L564
.L762:
	ldr	r5, [sp, #56]
	cmp	r5, #0
	beq	.L655
	mov	r0, r5
	bl	_ZN9PngLoader5abortEv
	mov	r0, r5
	bl	_ZN9PngLoader15deleteImageDataEv
.LEHE20:
	mov	r0, r5
	bl	_ZdlPv
	b	.L655
.L757:
	ldr	r5, [r4, #28]
	cmp	r5, #0
	beq	.L594
	mov	r0, r5
.LEHB21:
	bl	_ZN9PngLoader5abortEv
	mov	r0, r5
	bl	_ZN9PngLoader15deleteImageDataEv
.LEHE21:
	mov	r0, r5
	bl	_ZdlPv
	b	.L594
.L761:
	cmp	r8, r1
	bne	.L680
	ldr	r1, [r6, #4]
	cmp	r1, #0
	ldr	r5, .L765+4
	beq	.L683
	ldr	r0, [r8, #4]
.LEHB22:
	bl	T.560
	ldr	r3, [r5, #0]
	str	r3, [r3, #8]
	ldr	ip, [r5, #0]
	mov	r6, #0
	str	r6, [ip, #4]
	ldr	r0, [r5, #0]
	str	r0, [r0, #12]
	str	r6, [r5, #4]
	b	.L683
.L605:
	mov	fp, r1
	mov	r8, r1
	b	.L616
.L764:
	mov	r0, r7
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
.LEHE22:
	ldr	r1, [r4, #36]
	mov	r9, r0
	b	.L564
.L756:
	cmp	fp, r1
	bne	.L619
	ldr	r3, [r6, #4]
	cmp	r3, #0
	beq	.L622
	ldr	r0, [fp, #4]
.LEHB23:
	bl	T.560
.LEHE23:
	ldr	lr, [r6, #0]
	str	lr, [lr, #8]
	ldr	ip, [r6, #0]
	mov	r0, #0
	str	r0, [ip, #4]
	ldr	r1, [r6, #0]
	str	r1, [r1, #12]
	str	r0, [r6, #4]
	b	.L622
.L704:
.L698:
	ldr	r3, [sp, #12]
	cmp	r3, #0
	mov	r4, r0
	movne	r0, r3
	blne	free
.L699:
	mov	r0, r4
.LEHB24:
	bl	__cxa_end_cleanup
.LEHE24:
.L705:
.L695:
	mov	r4, r0
	mov	r1, #0
	add	r0, sp, #56
	ldr	r2, [sp, #60]
	bl	_ZN15QuStupidPointerI9PngLoaderE3setEPS0_i
	mov	r0, r4
.LEHB25:
	bl	__cxa_end_cleanup
.LEHE25:
.L766:
	.align	2
.L765:
	.word	.LC0
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA2860:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2860-.LLSDACSB2860
.LLSDACSB2860:
	.uleb128 .LEHB14-.LFB2860
	.uleb128 .LEHE14-.LEHB14
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB15-.LFB2860
	.uleb128 .LEHE15-.LEHB15
	.uleb128 .L705-.LFB2860
	.uleb128 0x0
	.uleb128 .LEHB16-.LFB2860
	.uleb128 .LEHE16-.LEHB16
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB17-.LFB2860
	.uleb128 .LEHE17-.LEHB17
	.uleb128 .L704-.LFB2860
	.uleb128 0x0
	.uleb128 .LEHB18-.LFB2860
	.uleb128 .LEHE18-.LEHB18
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB19-.LFB2860
	.uleb128 .LEHE19-.LEHB19
	.uleb128 .L705-.LFB2860
	.uleb128 0x0
	.uleb128 .LEHB20-.LFB2860
	.uleb128 .LEHE20-.LEHB20
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB21-.LFB2860
	.uleb128 .LEHE21-.LEHB21
	.uleb128 .L705-.LFB2860
	.uleb128 0x0
	.uleb128 .LEHB22-.LFB2860
	.uleb128 .LEHE22-.LEHB22
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB23-.LFB2860
	.uleb128 .LEHE23-.LEHB23
	.uleb128 .L705-.LFB2860
	.uleb128 0x0
	.uleb128 .LEHB24-.LFB2860
	.uleb128 .LEHE24-.LEHB24
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB25-.LFB2860
	.uleb128 .LEHE25-.LEHB25
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE2860:
	.fnend
	.size	_ZN10QuPngImage9loadImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE, .-_ZN10QuPngImage9loadImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.section	.text._ZN10QuPngImage7destroyEv,"ax",%progbits
	.align	2
	.global	_ZN10QuPngImage7destroyEv
	.hidden	_ZN10QuPngImage7destroyEv
	.type	_ZN10QuPngImage7destroyEv, %function
_ZN10QuPngImage7destroyEv:
	.fnstart
.LFB2856:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, lr}
	ldr	ip, [r0, #28]
	cmp	ip, #0
	.pad #16
	sub	sp, sp, #16
	mov	r4, r0
	add	r7, r0, #28
	ldr	r8, [r0, #32]
	beq	.L768
	ldr	r5, .L818
	ldr	r0, [r5, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L772
	mov	r1, r0
	b	.L773
.L811:
	mov	r1, r3
	mov	r3, r2
.L773:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L811
.L772:
	cmp	r3, r0
	beq	.L775
	ldr	r2, [r3, #16]
	cmp	ip, r2
	mov	r2, r3
	bcs	.L776
.L775:
	mov	r2, sp
	str	r3, [sp, #8]
	add	r0, sp, #12
	mov	r3, #0
	add	r1, sp, #8
	str	ip, [sp, #0]
	str	r3, [sp, #4]
	bl	T.561
	ldr	r2, [sp, #12]
.L776:
	ldr	r1, [r2, #20]
	sub	r0, r1, #1
	cmp	r0, #0
	str	r0, [r2, #20]
	beq	.L812
.L768:
	ldrb	r3, [r4, #4]	@ zero_extendqisi2
	mov	r2, #0
	cmp	r3, #0
	str	r2, [r4, #28]
	str	r8, [r4, #32]
	movne	r0, #1
	addne	r1, r4, #8
	blne	glDeleteTextures
.L804:
	mvn	r1, #0
	mov	r0, #0
	str	r1, [r4, #8]
	strb	r0, [r4, #4]
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L812:
	ldr	r1, [r5, #0]
	ldr	r6, [r1, #4]
	cmp	r6, #0
	ldr	r2, [r4, #28]
	moveq	r9, r1
	moveq	sl, r1
	beq	.L788
	mov	r0, r1
	mov	r9, r6
	b	.L781
.L813:
	mov	r0, r9
	mov	r9, r3
.L781:
	ldr	r3, [r9, #16]
	cmp	r2, r3
	ldrhi	r3, [r9, #12]
	ldrls	r3, [r9, #8]
	movhi	r9, r0
	cmp	r3, #0
	bne	.L813
	mov	r0, r1
	b	.L785
.L814:
	mov	r0, r6
	mov	r6, r3
.L785:
	ldr	r3, [r6, #16]
	cmp	r2, r3
	ldrcs	r3, [r6, #12]
	ldrcc	r3, [r6, #8]
	movcs	r6, r0
	cmp	r3, #0
	bne	.L814
	cmp	r9, r6
	mov	sl, r9
	moveq	r9, r6
	beq	.L788
	mov	r0, r9
.L789:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r6, r0
	bne	.L789
	ldr	r1, [r5, #0]
	mov	sl, r6
.L788:
	ldr	ip, [r1, #8]
	cmp	r9, ip
	beq	.L815
.L791:
	cmp	r9, sl
	bne	.L808
	b	.L794
.L797:
	mov	r9, r6
.L808:
	mov	r0, r9
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r2, [r5, #0]
	mov	r6, r0
	add	r3, r2, #12
	add	r1, r2, #4
	mov	r0, r9
	add	r2, r2, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L796:
	ldr	r3, [r5, #4]
	cmp	r6, sl
	sub	lr, r3, #1
	str	lr, [r5, #4]
	bne	.L797
.L794:
	ldr	r0, [r4, #32]
	cmp	r0, #1
	beq	.L816
	ldr	r3, [r4, #28]
	cmp	r3, #0
	beq	.L768
	ldr	r5, [r3, #-4]
	mov	r6, #0
	add	r5, r3, r5, asl #6
.L810:
	ldr	r1, [r7, #0]
	cmp	r1, r5
	sub	r5, r5, #64
	beq	.L800
.L817:
	ldr	r0, [r5, #4]
	cmp	r0, #0
	beq	.L801
	bl	s3eFileClose
	str	r6, [r5, #4]
.L801:
	add	r0, r5, #12
	add	r1, r5, #16
	mov	r2, #0
	bl	png_destroy_read_struct
	ldr	r0, [r5, #56]
	cmp	r0, #0
	beq	.L802
	bl	s3eFree
	str	r6, [r5, #56]
.L802:
	ldr	r0, [r5, #60]
	cmp	r0, #0
	beq	.L810
	bl	s3eFree
	str	r6, [r5, #60]
	ldr	r1, [r7, #0]
	cmp	r1, r5
	sub	r5, r5, #64
	bne	.L817
.L800:
	ldr	ip, [r4, #28]
	sub	r0, ip, #8
	bl	_ZdaPv
	b	.L768
.L816:
	ldr	r5, [r4, #28]
	cmp	r5, #0
	beq	.L768
	mov	r0, r5
	bl	_ZN9PngLoader5abortEv
	mov	r0, r5
	bl	_ZN9PngLoader15deleteImageDataEv
	mov	r0, r5
	bl	_ZdlPv
	b	.L768
.L815:
	cmp	sl, r1
	bne	.L791
	ldr	r2, [r5, #4]
	cmp	r2, #0
	ldr	r5, .L818
	beq	.L794
	ldr	r0, [sl, #4]
	bl	T.560
	ldr	ip, [r5, #0]
	str	ip, [ip, #8]
	ldr	r1, [r5, #0]
	mov	r3, #0
	str	r3, [r1, #4]
	ldr	r0, [r5, #0]
	str	r0, [r0, #12]
	str	r3, [r5, #4]
	b	.L794
.L819:
	.align	2
.L818:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	_ZN10QuPngImage7destroyEv, .-_ZN10QuPngImage7destroyEv
	.section	.text._ZN10QuPngImageD0Ev,"axG",%progbits,_ZN10QuPngImageD0Ev,comdat
	.align	2
	.weak	_ZN10QuPngImageD0Ev
	.hidden	_ZN10QuPngImageD0Ev
	.type	_ZN10QuPngImageD0Ev, %function
_ZN10QuPngImageD0Ev:
	.fnstart
.LFB2843:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, lr}
	ldr	ip, [r0, #28]
	ldr	r3, .L935
	mov	r8, r0
	cmp	ip, #0
	.pad #32
	sub	sp, sp, #32
	mov	r4, r0
	str	r3, [r8], #28
	ldr	r9, [r0, #32]
	beq	.L821
	ldr	r7, .L935+4
	ldr	r0, [r7, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L825
	mov	r1, r0
	b	.L826
.L920:
	mov	r1, r3
	mov	r3, r2
.L826:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L920
.L825:
	cmp	r3, r0
	beq	.L828
	ldr	r2, [r3, #16]
	cmp	ip, r2
	mov	r2, r3
	bcs	.L829
.L828:
	str	ip, [sp, #8]
	add	r0, sp, #28
	mov	ip, #0
	add	r1, sp, #24
	add	r2, sp, #8
	str	ip, [sp, #12]
	str	r3, [sp, #24]
.LEHB26:
	bl	T.561
.LEHE26:
	ldr	r2, [sp, #28]
.L829:
	ldr	r1, [r2, #20]
	sub	r0, r1, #1
	cmp	r0, #0
	str	r0, [r2, #20]
	beq	.L921
.L821:
	ldrb	r3, [r4, #4]	@ zero_extendqisi2
	mov	lr, #0
	cmp	r3, #0
	str	lr, [r4, #28]
	str	r9, [r4, #32]
	bne	.L922
.L859:
	ldr	r0, [r4, #36]
	mov	ip, #0
	mvn	r9, #0
	cmp	r0, #0
	strb	ip, [r4, #4]
	str	r9, [r4, #8]
	blne	free
.L861:
	ldr	ip, [r4, #28]
	cmp	ip, #0
	ldr	r9, [r4, #32]
	beq	.L865
	ldr	r7, .L935+4
	ldr	r0, [r7, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L871
	mov	r1, r0
	b	.L872
.L923:
	mov	r1, r3
	mov	r3, r2
.L872:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L923
.L871:
	cmp	r3, r0
	beq	.L874
	ldr	r2, [r3, #16]
	cmp	ip, r2
	mov	r2, r3
	bcs	.L875
.L874:
	mov	lr, #0
	add	r0, sp, #20
	add	r1, sp, #16
	mov	r2, sp
	str	r3, [sp, #16]
	stmia	sp, {ip, lr}	@ phole stm
.LEHB27:
	bl	T.561
.LEHE27:
	ldr	r2, [sp, #20]
.L875:
	ldr	r1, [r2, #20]
	sub	r0, r1, #1
	cmp	r0, #0
	str	r0, [r2, #20]
	beq	.L924
.L865:
	ldrb	lr, [r4, #4]	@ zero_extendqisi2
	ldr	ip, .L935+8
	mov	r3, #0
	cmp	lr, #0
	str	r3, [r4, #28]
	str	r9, [r4, #32]
	str	ip, [r4, #0]
	movne	r0, #1
	addne	r1, r4, #8
.LEHB28:
	blne	glDeleteTextures
.LEHE28:
.L904:
	mvn	r2, #0
	mov	r0, #0
	strb	r0, [r4, #4]
	str	r2, [r4, #8]
	mov	r0, r4
	bl	_ZdlPv
	mov	r0, r4
	add	sp, sp, #32
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L921:
	ldr	r1, [r7, #0]
	ldr	r5, [r1, #4]
	cmp	r5, #0
	ldr	r2, [r4, #28]
	beq	.L832
	mov	r0, r1
	mov	r6, r5
	b	.L836
.L925:
	mov	r0, r6
	mov	r6, r3
.L836:
	ldr	r3, [r6, #16]
	cmp	r2, r3
	ldrhi	r3, [r6, #12]
	ldrls	r3, [r6, #8]
	movhi	r6, r0
	cmp	r3, #0
	bne	.L925
	mov	r0, r1
	b	.L840
.L926:
	mov	r0, r5
	mov	r5, r3
.L840:
	ldr	r3, [r5, #16]
	cmp	r2, r3
	ldrcs	r3, [r5, #12]
	ldrcc	r3, [r5, #8]
	movcs	r5, r0
	cmp	r3, #0
	bne	.L926
	cmp	r6, r5
	mov	sl, r6
	moveq	r6, r5
	beq	.L843
	mov	r0, r6
.L844:
.LEHB29:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r5, r0
	bne	.L844
	ldr	r1, [r7, #0]
	mov	sl, r5
.L843:
	ldr	lr, [r1, #8]
	cmp	r6, lr
	beq	.L927
.L846:
	cmp	r6, sl
	bne	.L914
	b	.L849
.L852:
	mov	r6, r5
.L914:
	mov	r0, r6
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	lr, [r7, #0]
	mov	r5, r0
	add	r3, lr, #12
	mov	r0, r6
	add	r1, lr, #4
	add	r2, lr, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L851:
	ldr	ip, [r7, #4]
	cmp	sl, r5
	sub	r3, ip, #1
	str	r3, [r7, #4]
	bne	.L852
.L849:
	ldr	r2, [r4, #32]
	cmp	r2, #1
	beq	.L928
	ldr	r3, [r4, #28]
	cmp	r3, #0
	beq	.L821
	ldr	r5, [r3, #-4]
	mov	r6, #0
	add	r5, r3, r5, asl #6
.L918:
	ldr	r0, [r8, #0]
	cmp	r0, r5
	beq	.L855
.L929:
	sub	r5, r5, #64
	ldr	r0, [r5, #4]
	cmp	r0, #0
	beq	.L856
	bl	s3eFileClose
	str	r6, [r5, #4]
.L856:
	add	r0, r5, #12
	add	r1, r5, #16
	mov	r2, #0
	bl	png_destroy_read_struct
	ldr	r0, [r5, #56]
	cmp	r0, #0
	beq	.L857
	bl	s3eFree
	str	r6, [r5, #56]
.L857:
	ldr	r0, [r5, #60]
	cmp	r0, #0
	beq	.L918
	bl	s3eFree
	str	r6, [r5, #60]
	ldr	r0, [r8, #0]
	cmp	r0, r5
	bne	.L929
.L855:
	ldr	r1, [r4, #28]
	sub	r0, r1, #8
	bl	_ZdaPv
	ldrb	r3, [r4, #4]	@ zero_extendqisi2
	mov	lr, #0
	cmp	r3, #0
	str	lr, [r4, #28]
	str	r9, [r4, #32]
	beq	.L859
.L922:
	mov	r0, #1
	add	r1, r4, #8
	bl	glDeleteTextures
.LEHE29:
	b	.L859
.L924:
	ldr	r1, [r7, #0]
	ldr	r5, [r1, #4]
	cmp	r5, #0
	ldr	r2, [r4, #28]
	beq	.L876
	mov	r0, r1
	mov	r6, r5
	b	.L880
.L930:
	mov	r0, r6
	mov	r6, r3
.L880:
	ldr	r3, [r6, #16]
	cmp	r2, r3
	ldrhi	r3, [r6, #12]
	ldrls	r3, [r6, #8]
	movhi	r6, r0
	cmp	r3, #0
	bne	.L930
	mov	r0, r1
	b	.L884
.L931:
	mov	r0, r5
	mov	r5, r3
.L884:
	ldr	r3, [r5, #16]
	cmp	r2, r3
	ldrcs	r3, [r5, #12]
	ldrcc	r3, [r5, #8]
	movcs	r5, r0
	cmp	r3, #0
	bne	.L931
	cmp	r6, r5
	mov	sl, r6
	moveq	r6, r5
	beq	.L887
	mov	r0, r6
.L888:
.LEHB30:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r5, r0
	bne	.L888
	ldr	r1, [r7, #0]
	mov	sl, r5
.L887:
	ldr	r3, [r1, #8]
	cmp	r6, r3
	beq	.L932
.L890:
	cmp	sl, r6
	bne	.L916
	b	.L893
.L896:
	mov	r6, r5
.L916:
	mov	r0, r6
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	ip, [r7, #0]
	mov	r5, r0
	add	r3, ip, #12
	mov	r0, r6
	add	r1, ip, #4
	add	r2, ip, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L895:
	ldr	lr, [r7, #4]
	cmp	sl, r5
	sub	r3, lr, #1
	str	r3, [r7, #4]
	bne	.L896
.L893:
	ldr	r2, [r4, #32]
	cmp	r2, #1
	beq	.L933
	ldr	r3, [r4, #28]
	cmp	r3, #0
	beq	.L865
	ldr	r6, [r3, #-4]
	add	r5, r3, r6, asl #6
	mov	r6, #0
.L919:
	ldr	r0, [r8, #0]
	cmp	r0, r5
	beq	.L899
.L934:
	sub	r5, r5, #64
	ldr	r0, [r5, #4]
	cmp	r0, #0
	beq	.L900
	bl	s3eFileClose
	str	r6, [r5, #4]
.L900:
	add	r0, r5, #12
	add	r1, r5, #16
	mov	r2, #0
	bl	png_destroy_read_struct
	ldr	r0, [r5, #56]
	cmp	r0, #0
	beq	.L901
	bl	s3eFree
	str	r6, [r5, #56]
.L901:
	ldr	r0, [r5, #60]
	cmp	r0, #0
	beq	.L919
	bl	s3eFree
.LEHE30:
	str	r6, [r5, #60]
	ldr	r0, [r8, #0]
	cmp	r0, r5
	bne	.L934
.L899:
	ldr	r1, [r4, #28]
	sub	r0, r1, #8
	bl	_ZdaPv
	b	.L865
.L928:
	ldr	r5, [r4, #28]
	cmp	r5, #0
	beq	.L821
	mov	r0, r5
.LEHB31:
	bl	_ZN9PngLoader5abortEv
	mov	r0, r5
	bl	_ZN9PngLoader15deleteImageDataEv
.LEHE31:
	mov	r0, r5
	bl	_ZdlPv
	b	.L821
.L933:
	ldr	r5, [r4, #28]
	cmp	r5, #0
	beq	.L865
	mov	r0, r5
.LEHB32:
	bl	_ZN9PngLoader5abortEv
	mov	r0, r5
	bl	_ZN9PngLoader15deleteImageDataEv
.LEHE32:
	mov	r0, r5
	bl	_ZdlPv
	b	.L865
.L876:
	mov	sl, r1
	mov	r6, r1
	b	.L887
.L832:
	mov	sl, r1
	mov	r6, r1
	b	.L843
.L927:
	cmp	sl, r1
	bne	.L846
	ldr	r3, [r7, #4]
	cmp	r3, #0
	beq	.L849
	ldr	r0, [sl, #4]
.LEHB33:
	bl	T.560
.LEHE33:
	ldr	r1, [r7, #0]
	str	r1, [r1, #8]
	ldr	r0, [r7, #0]
	mov	ip, #0
	str	ip, [r0, #4]
	ldr	r2, [r7, #0]
	str	r2, [r2, #12]
	str	ip, [r7, #4]
	b	.L849
.L932:
	cmp	sl, r1
	bne	.L890
	ldr	ip, [r7, #4]
	cmp	ip, #0
	beq	.L893
	ldr	r0, [sl, #4]
.LEHB34:
	bl	T.560
.LEHE34:
	ldr	r1, [r7, #0]
	str	r1, [r1, #8]
	ldr	r0, [r7, #0]
	mov	lr, #0
	str	lr, [r0, #4]
	ldr	r2, [r7, #0]
	str	r2, [r2, #12]
	str	lr, [r7, #4]
	b	.L893
.L907:
.L862:
	ldr	r3, [r4, #36]
	cmp	r3, #0
	mov	r5, r0
	movne	r0, r3
	blne	free
.L863:
.L908:
.L866:
	mov	r0, r8
	mov	r1, #0
	ldr	r2, [r4, #32]
	bl	_ZN15QuStupidPointerI9PngLoaderE3setEPS0_i
.L905:
	ldr	r7, .L935+8
	mov	r0, r4
	str	r7, [r4, #0]
	bl	_ZN11QuBaseImage7destroyEv
	mov	r0, r5
.LEHB35:
	bl	__cxa_end_cleanup
.LEHE35:
.L909:
	mov	r5, r0
	b	.L905
.L936:
	.align	2
.L935:
	.word	.LANCHOR0+56
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.word	.LANCHOR0+32
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA2843:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2843-.LLSDACSB2843
.LLSDACSB2843:
	.uleb128 .LEHB26-.LFB2843
	.uleb128 .LEHE26-.LEHB26
	.uleb128 .L907-.LFB2843
	.uleb128 0x0
	.uleb128 .LEHB27-.LFB2843
	.uleb128 .LEHE27-.LEHB27
	.uleb128 .L909-.LFB2843
	.uleb128 0x0
	.uleb128 .LEHB28-.LFB2843
	.uleb128 .LEHE28-.LEHB28
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB29-.LFB2843
	.uleb128 .LEHE29-.LEHB29
	.uleb128 .L907-.LFB2843
	.uleb128 0x0
	.uleb128 .LEHB30-.LFB2843
	.uleb128 .LEHE30-.LEHB30
	.uleb128 .L909-.LFB2843
	.uleb128 0x0
	.uleb128 .LEHB31-.LFB2843
	.uleb128 .LEHE31-.LEHB31
	.uleb128 .L907-.LFB2843
	.uleb128 0x0
	.uleb128 .LEHB32-.LFB2843
	.uleb128 .LEHE32-.LEHB32
	.uleb128 .L909-.LFB2843
	.uleb128 0x0
	.uleb128 .LEHB33-.LFB2843
	.uleb128 .LEHE33-.LEHB33
	.uleb128 .L907-.LFB2843
	.uleb128 0x0
	.uleb128 .LEHB34-.LFB2843
	.uleb128 .LEHE34-.LEHB34
	.uleb128 .L909-.LFB2843
	.uleb128 0x0
	.uleb128 .LEHB35-.LFB2843
	.uleb128 .LEHE35-.LEHB35
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE2843:
	.fnend
	.size	_ZN10QuPngImageD0Ev, .-_ZN10QuPngImageD0Ev
	.section	.text._ZN10QuPngImage10setTextureEv,"ax",%progbits
	.align	2
	.global	_ZN10QuPngImage10setTextureEv
	.hidden	_ZN10QuPngImage10setTextureEv
	.type	_ZN10QuPngImage10setTextureEv, %function
_ZN10QuPngImage10setTextureEv:
	.fnstart
.LFB2862:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, lr}
	ldr	r3, [r0, #28]
	mov	r4, r0
	ldrb	r0, [r3, #0]	@ zero_extendqisi2
	cmp	r0, #0
	.pad #48
	sub	sp, sp, #48
	beq	.L939
	ldr	sl, [r3, #40]
	sub	r9, sl, #2
	cmp	r9, #4
	ldrls	r1, .L994
	mvn	ip, #0
	ldrls	r5, [r1, r9, asl #2]
	add	r9, sp, #48
	movhi	r6, #6400
	mov	lr, #3312
	ldr	sl, [r3, #56]
	add	r7, r3, #28
	ldmia	r7, {r7, r8}	@ phole ldm
	str	ip, [r9, #-4]!
	addhi	r6, r6, #7
	add	r0, lr, #5
	mov	r1, #1
	movhi	r5, r6
	movls	r6, r5
	bl	glPixelStorei
	mov	r1, r9
	mov	r0, #1
	bl	glGenTextures
	mov	r3, #3552
	add	r0, r3, #1
	ldr	r1, [sp, #44]
	bl	glBindTexture
	mov	r0, #3552
	mov	r1, #10240
	mov	r2, #33024
	add	r0, r0, #1
	add	r1, r1, #2
	add	r2, r2, #47
	bl	glTexParameterx
	mov	ip, #3552
	mov	r3, #10240
	mov	r2, #33024
	add	r0, ip, #1
	add	r1, r3, #3
	add	r2, r2, #47
	bl	glTexParameterx
	mov	r1, #9728
	mov	r0, #3552
	add	r2, r1, #1
	add	r0, r0, #1
	mov	r1, #10240
	bl	glTexParameteri
	mov	ip, #3552
	mov	r3, #10240
	mov	r2, #9728
	add	r0, ip, #1
	add	r1, r3, #1
	add	r2, r2, #1
	bl	glTexParameteri
	mov	lr, #0
	mov	r1, #5120
	mov	r0, #3552
	add	ip, r1, #1
	mov	r3, r7
	add	r0, r0, #1
	mov	r1, lr
	mov	r2, r6
	str	r5, [sp, #8]
	str	r8, [sp, #0]
	str	ip, [sp, #12]
	str	sl, [sp, #16]
	str	lr, [sp, #4]
	bl	glTexImage2D
	ldrb	r3, [r4, #4]	@ zero_extendqisi2
	cmp	r3, #0
	ldr	r5, [sp, #44]
	bne	.L985
	ldr	ip, [r4, #28]
	mov	r8, #1
	cmp	ip, #0
	strb	r8, [r4, #4]
	str	r5, [r4, #8]
	add	r7, r4, #28
	ldr	r8, [r4, #32]
	beq	.L943
.L988:
	ldr	r5, .L994+4
	ldr	r0, [r5, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L947
	mov	r1, r0
	b	.L948
.L986:
	mov	r1, r3
	mov	r3, r2
.L948:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L986
.L947:
	cmp	r3, r0
	beq	.L950
	ldr	r2, [r3, #16]
	cmp	ip, r2
	mov	r2, r3
	bcs	.L951
.L950:
	add	r2, sp, #28
	str	r3, [sp, #36]
	add	r0, sp, #40
	mov	r3, #0
	add	r1, sp, #36
	str	ip, [sp, #28]
	str	r3, [sp, #32]
	bl	T.561
	ldr	r2, [sp, #40]
.L951:
	ldr	r0, [r2, #20]
	sub	r1, r0, #1
	cmp	r1, #0
	str	r1, [r2, #20]
	beq	.L987
.L943:
	mov	r2, #0
	str	r8, [r4, #32]
	str	r2, [r4, #28]
	mov	r0, #1
.L939:
	add	sp, sp, #48
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L985:
	mov	r0, #1
	add	r1, r4, #8
	bl	glDeleteTextures
	ldr	ip, [r4, #28]
	mov	r8, #1
	cmp	ip, #0
	strb	r8, [r4, #4]
	str	r5, [r4, #8]
	add	r7, r4, #28
	ldr	r8, [r4, #32]
	bne	.L988
	b	.L943
.L987:
	ldr	r1, [r5, #0]
	ldr	r6, [r1, #4]
	cmp	r6, #0
	ldr	r2, [r4, #28]
	moveq	sl, r1
	moveq	r9, r1
	beq	.L963
	mov	r0, r1
	mov	r9, r6
	b	.L956
.L989:
	mov	r0, r9
	mov	r9, r3
.L956:
	ldr	r3, [r9, #16]
	cmp	r2, r3
	ldrhi	r3, [r9, #12]
	ldrls	r3, [r9, #8]
	movhi	r9, r0
	cmp	r3, #0
	bne	.L989
	mov	r0, r1
	b	.L960
.L990:
	mov	r0, r6
	mov	r6, r3
.L960:
	ldr	r3, [r6, #16]
	cmp	r2, r3
	ldrcs	r3, [r6, #12]
	ldrcc	r3, [r6, #8]
	movcs	r6, r0
	cmp	r3, #0
	bne	.L990
	cmp	r9, r6
	mov	sl, r9
	moveq	r9, r6
	beq	.L963
	mov	r0, r9
.L964:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r6, r0
	bne	.L964
	ldr	r1, [r5, #0]
	mov	sl, r6
.L963:
	ldr	ip, [r1, #8]
	cmp	r9, ip
	beq	.L991
.L966:
	cmp	r9, sl
	bne	.L982
	b	.L969
.L972:
	mov	r9, r6
.L982:
	mov	r0, r9
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r2, [r5, #0]
	mov	r6, r0
	add	r3, r2, #12
	add	r1, r2, #4
	mov	r0, r9
	add	r2, r2, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L971:
	ldr	r3, [r5, #4]
	cmp	r6, sl
	sub	lr, r3, #1
	str	lr, [r5, #4]
	bne	.L972
.L969:
	ldr	r1, [r4, #32]
	cmp	r1, #1
	beq	.L992
	ldr	r3, [r4, #28]
	cmp	r3, #0
	beq	.L943
	ldr	r5, [r3, #-4]
	mov	r6, #0
	add	r5, r3, r5, asl #6
.L984:
	ldr	r0, [r7, #0]
	cmp	r0, r5
	sub	r5, r5, #64
	beq	.L975
.L993:
	ldr	r0, [r5, #4]
	cmp	r0, #0
	beq	.L976
	bl	s3eFileClose
	str	r6, [r5, #4]
.L976:
	add	r0, r5, #12
	add	r1, r5, #16
	mov	r2, #0
	bl	png_destroy_read_struct
	ldr	r0, [r5, #56]
	cmp	r0, #0
	beq	.L977
	bl	s3eFree
	str	r6, [r5, #56]
.L977:
	ldr	r0, [r5, #60]
	cmp	r0, #0
	beq	.L984
	bl	s3eFree
	str	r6, [r5, #60]
	ldr	r0, [r7, #0]
	cmp	r0, r5
	sub	r5, r5, #64
	bne	.L993
.L975:
	ldr	ip, [r4, #28]
	sub	r0, ip, #8
	bl	_ZdaPv
	b	.L943
.L992:
	ldr	r5, [r4, #28]
	cmp	r5, #0
	beq	.L943
	mov	r0, r5
	bl	_ZN9PngLoader5abortEv
	mov	r0, r5
	bl	_ZN9PngLoader15deleteImageDataEv
	mov	r0, r5
	bl	_ZdlPv
	b	.L943
.L991:
	cmp	sl, r1
	bne	.L966
	ldr	r2, [r5, #4]
	cmp	r2, #0
	ldr	r5, .L994+4
	beq	.L969
	ldr	r0, [sl, #4]
	bl	T.560
	ldr	ip, [r5, #0]
	str	ip, [ip, #8]
	ldr	r0, [r5, #0]
	mov	r3, #0
	str	r3, [r0, #4]
	ldr	r1, [r5, #0]
	str	r1, [r1, #12]
	str	r3, [r5, #4]
	b	.L969
.L995:
	.align	2
.L994:
	.word	.LANCHOR0
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	_ZN10QuPngImage10setTextureEv, .-_ZN10QuPngImage10setTextureEv
	.section	.text._ZN10QuPngImage9pumpImageEi,"ax",%progbits
	.align	2
	.global	_ZN10QuPngImage9pumpImageEi
	.hidden	_ZN10QuPngImage9pumpImageEi
	.type	_ZN10QuPngImage9pumpImageEi, %function
_ZN10QuPngImage9pumpImageEi:
	.fnstart
.LFB2858:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	ldrb	r3, [r0, #4]	@ zero_extendqisi2
	cmp	r3, #0
	mov	r4, r0
	bne	.L997
	ldr	r5, [r0, #28]
	cmp	r5, #0
	beq	.L998
	ldrb	r2, [r5, #0]	@ zero_extendqisi2
	cmp	r2, #0
	beq	.L1001
.L997:
	mov	r0, #1
.L999:
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L1001:
	ldr	lr, [r5, #52]
	ldr	r3, [r5, #24]
	ldr	ip, [r5, #60]
	rsb	r6, lr, r3
	cmp	r1, r6
	movcc	r6, r1
	ldr	r0, [r5, #12]
	mov	r3, r6
	add	r1, ip, lr, asl #2
	bl	png_read_rows
	ldr	r2, [r5, #52]
	ldr	r0, [r5, #24]
	add	r6, r6, r2
	cmp	r6, r0
	str	r6, [r5, #52]
	beq	.L1002
.L998:
	mov	r0, #0
	b	.L999
.L1002:
	mov	r1, #1
	mov	r0, r5
	strb	r1, [r5, #0]
	bl	_ZN9PngLoader5abortEv
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, lr}
	b	_ZN10QuPngImage10setTextureEv
	.fnend
	.size	_ZN10QuPngImage9pumpImageEi, .-_ZN10QuPngImage9pumpImageEi
	.section	.text._ZN10QuPngImage15loadEntireImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE,"ax",%progbits
	.align	2
	.global	_ZN10QuPngImage15loadEntireImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.hidden	_ZN10QuPngImage15loadEntireImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.type	_ZN10QuPngImage15loadEntireImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE, %function
_ZN10QuPngImage15loadEntireImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE:
	.fnstart
.LFB2861:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	.pad #16
	sub	sp, sp, #16
	add	r5, sp, #4
	mov	r4, r0
	mov	r0, r5
.LEHB36:
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
.LEHE36:
	mov	r0, r4
	mov	r1, r5
.LEHB37:
	bl	_ZN10QuPngImage9loadImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
.LEHE37:
	mov	r5, r0
	ldr	r0, [sp, #4]
	cmp	r0, #0
	blne	free
.L1004:
	cmp	r5, #0
	moveq	r0, r5
	bne	.L1013
.L1006:
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L1013:
	ldrb	r3, [r4, #4]	@ zero_extendqisi2
	cmp	r3, #0
	movne	r0, #1
	bne	.L1006
	ldr	r5, [r4, #28]
	ldrb	r2, [r5, #0]	@ zero_extendqisi2
	cmp	r2, #0
	ldr	r3, [r5, #24]
	beq	.L1014
.L1010:
	mov	r0, r4
.LEHB38:
	bl	_ZN10QuPngImage10setTextureEv
	b	.L1006
.L1014:
	ldr	r1, [r5, #52]
	ldr	ip, [r5, #60]
	rsb	r6, r1, r3
	cmp	r6, r3
	movcs	r6, r3
	ldr	r0, [r5, #12]
	mov	r3, r6
	add	r1, ip, r1, asl #2
	bl	png_read_rows
	ldr	r2, [r5, #52]
	ldr	r0, [r5, #24]
	add	r6, r6, r2
	cmp	r6, r0
	str	r6, [r5, #52]
	bne	.L1010
	mov	lr, #1
	strb	lr, [r5, #0]
	mov	r0, r5
	bl	_ZN9PngLoader5abortEv
	b	.L1010
.L1012:
.L1007:
	ldr	r3, [sp, #4]
	cmp	r3, #0
	mov	r4, r0
	movne	r0, r3
	blne	free
.L1008:
	mov	r0, r4
	bl	__cxa_end_cleanup
.LEHE38:
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA2861:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2861-.LLSDACSB2861
.LLSDACSB2861:
	.uleb128 .LEHB36-.LFB2861
	.uleb128 .LEHE36-.LEHB36
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB37-.LFB2861
	.uleb128 .LEHE37-.LEHB37
	.uleb128 .L1012-.LFB2861
	.uleb128 0x0
	.uleb128 .LEHB38-.LFB2861
	.uleb128 .LEHE38-.LEHB38
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE2861:
	.fnend
	.size	_ZN10QuPngImage15loadEntireImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE, .-_ZN10QuPngImage15loadEntireImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
	.section	.text._ZN10QuPngImage11reloadImageEv,"ax",%progbits
	.align	2
	.global	_ZN10QuPngImage11reloadImageEv
	.hidden	_ZN10QuPngImage11reloadImageEv
	.type	_ZN10QuPngImage11reloadImageEv, %function
_ZN10QuPngImage11reloadImageEv:
	.fnstart
.LFB2857:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	ldr	r2, [r0, #40]
	ldr	r3, [r0, #36]
	cmp	r2, r3
	.pad #16
	sub	sp, sp, #16
	mov	r4, r0
	beq	.L1016
	mov	r6, r0
	ldr	r3, [r6], #36
	add	r5, sp, #4
.LEHB39:
	ldr	ip, [r3, #12]
	mov	lr, pc
	bx	ip
	mov	r1, r6
	mov	r0, r5
	bl	_ZN4_STL12basic_stringIcNS_11char_traitsIcEENS_9allocatorIcEEEC1ERKS5_
.LEHE39:
	mov	r0, r4
	mov	r1, r5
.LEHB40:
	bl	_ZN10QuPngImage9loadImageEN4_STL12basic_stringIcNS0_11char_traitsIcEENS0_9allocatorIcEEEE
.LEHE40:
	mov	r5, r0
	ldr	r0, [sp, #4]
	cmp	r0, #0
	blne	free
.L1018:
	cmp	r5, #0
	bne	.L1031
.L1016:
	mov	r0, #0
.L1023:
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L1031:
	ldrb	r0, [r4, #4]	@ zero_extendqisi2
	cmp	r0, #0
	movne	r0, #1
	bne	.L1023
.L1022:
	ldr	r5, [r4, #28]
	ldrb	r2, [r5, #0]	@ zero_extendqisi2
	cmp	r2, #0
	ldr	r3, [r5, #24]
	beq	.L1032
.L1024:
	mov	r0, r4
.LEHB41:
	bl	_ZN10QuPngImage10setTextureEv
	mov	r0, #1
	b	.L1023
.L1032:
	ldr	lr, [r5, #52]
	ldr	r0, [r5, #60]
	rsb	r6, lr, r3
	cmp	r6, r3
	movcs	r6, r3
	add	r1, r0, lr, asl #2
	mov	r3, r6
	ldr	r0, [r5, #12]
	bl	png_read_rows
	ldr	ip, [r5, #52]
	ldr	r1, [r5, #24]
	add	r6, r6, ip
	cmp	r6, r1
	str	r6, [r5, #52]
	bne	.L1024
	mov	r2, #1
	strb	r2, [r5, #0]
	mov	r0, r5
	bl	_ZN9PngLoader5abortEv
	b	.L1024
.L1027:
.L1020:
	ldr	r3, [sp, #4]
	cmp	r3, #0
	mov	r4, r0
	movne	r0, r3
	blne	free
.L1021:
	mov	r0, r4
	bl	__cxa_end_cleanup
.LEHE41:
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA2857:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2857-.LLSDACSB2857
.LLSDACSB2857:
	.uleb128 .LEHB39-.LFB2857
	.uleb128 .LEHE39-.LEHB39
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB40-.LFB2857
	.uleb128 .LEHE40-.LEHB40
	.uleb128 .L1027-.LFB2857
	.uleb128 0x0
	.uleb128 .LEHB41-.LFB2857
	.uleb128 .LEHE41-.LEHB41
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE2857:
	.fnend
	.size	_ZN10QuPngImage11reloadImageEv, .-_ZN10QuPngImage11reloadImageEv
	.section	.text._ZN10QuPngImageD1Ev,"axG",%progbits,_ZN10QuPngImageD1Ev,comdat
	.align	2
	.weak	_ZN10QuPngImageD1Ev
	.hidden	_ZN10QuPngImageD1Ev
	.type	_ZN10QuPngImageD1Ev, %function
_ZN10QuPngImageD1Ev:
	.fnstart
.LFB2842:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, lr}
	ldr	ip, [r0, #28]
	ldr	r3, .L1148
	mov	r8, r0
	cmp	ip, #0
	.pad #32
	sub	sp, sp, #32
	mov	r4, r0
	str	r3, [r8], #28
	ldr	r9, [r0, #32]
	beq	.L1034
	ldr	r7, .L1148+4
	ldr	r0, [r7, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L1038
	mov	r1, r0
	b	.L1039
.L1133:
	mov	r1, r3
	mov	r3, r2
.L1039:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L1133
.L1038:
	cmp	r3, r0
	beq	.L1041
	ldr	r2, [r3, #16]
	cmp	ip, r2
	mov	r2, r3
	bcs	.L1042
.L1041:
	str	ip, [sp, #8]
	add	r0, sp, #28
	mov	ip, #0
	add	r1, sp, #24
	add	r2, sp, #8
	str	ip, [sp, #12]
	str	r3, [sp, #24]
.LEHB42:
	bl	T.561
.LEHE42:
	ldr	r2, [sp, #28]
.L1042:
	ldr	r1, [r2, #20]
	sub	r0, r1, #1
	cmp	r0, #0
	str	r0, [r2, #20]
	beq	.L1134
.L1034:
	ldrb	r3, [r4, #4]	@ zero_extendqisi2
	mov	r2, #0
	cmp	r3, #0
	str	r2, [r4, #28]
	str	r9, [r4, #32]
	bne	.L1135
.L1072:
	ldr	r0, [r4, #36]
	mov	ip, #0
	mvn	r9, #0
	cmp	r0, #0
	strb	ip, [r4, #4]
	str	r9, [r4, #8]
	blne	free
.L1074:
	ldr	ip, [r4, #28]
	cmp	ip, #0
	ldr	r9, [r4, #32]
	beq	.L1078
	ldr	r7, .L1148+4
	ldr	r0, [r7, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L1084
	mov	r1, r0
	b	.L1085
.L1136:
	mov	r1, r3
	mov	r3, r2
.L1085:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L1136
.L1084:
	cmp	r3, r0
	beq	.L1087
	ldr	r0, [r3, #16]
	cmp	ip, r0
	mov	r2, r3
	bcs	.L1088
.L1087:
	mov	lr, #0
	add	r0, sp, #20
	add	r1, sp, #16
	mov	r2, sp
	str	r3, [sp, #16]
	stmia	sp, {ip, lr}	@ phole stm
.LEHB43:
	bl	T.561
.LEHE43:
	ldr	r2, [sp, #20]
.L1088:
	ldr	r3, [r2, #20]
	sub	r1, r3, #1
	cmp	r1, #0
	str	r1, [r2, #20]
	beq	.L1137
.L1078:
	ldrb	r0, [r4, #4]	@ zero_extendqisi2
	ldr	r2, .L1148+8
	mov	ip, #0
	cmp	r0, #0
	str	ip, [r4, #28]
	str	r9, [r4, #32]
	str	r2, [r4, #0]
	movne	r0, #1
	addne	r1, r4, #8
.LEHB44:
	blne	glDeleteTextures
.LEHE44:
.L1117:
	mov	r3, #0
	mvn	r1, #0
	strb	r3, [r4, #4]
	str	r1, [r4, #8]
	mov	r0, r4
	add	sp, sp, #32
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L1134:
	ldr	r1, [r7, #0]
	ldr	r5, [r1, #4]
	cmp	r5, #0
	ldr	r2, [r4, #28]
	beq	.L1045
	mov	r0, r1
	mov	r6, r5
	b	.L1049
.L1138:
	mov	r0, r6
	mov	r6, r3
.L1049:
	ldr	r3, [r6, #16]
	cmp	r2, r3
	ldrhi	r3, [r6, #12]
	ldrls	r3, [r6, #8]
	movhi	r6, r0
	cmp	r3, #0
	bne	.L1138
	mov	r0, r1
	b	.L1053
.L1139:
	mov	r0, r5
	mov	r5, r3
.L1053:
	ldr	r3, [r5, #16]
	cmp	r2, r3
	ldrcs	r3, [r5, #12]
	ldrcc	r3, [r5, #8]
	movcs	r5, r0
	cmp	r3, #0
	bne	.L1139
	cmp	r6, r5
	mov	sl, r6
	moveq	r6, r5
	beq	.L1056
	mov	r0, r6
.L1057:
.LEHB45:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r5, r0
	bne	.L1057
	ldr	r1, [r7, #0]
	mov	sl, r5
.L1056:
	ldr	r3, [r1, #8]
	cmp	r6, r3
	beq	.L1140
.L1059:
	cmp	r6, sl
	bne	.L1127
	b	.L1062
.L1065:
	mov	r6, r5
.L1127:
	mov	r0, r6
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r2, [r7, #0]
	mov	r5, r0
	add	r3, r2, #12
	add	r1, r2, #4
	mov	r0, r6
	add	r2, r2, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L1064:
	ldr	lr, [r7, #4]
	cmp	sl, r5
	sub	r3, lr, #1
	str	r3, [r7, #4]
	bne	.L1065
.L1062:
	ldr	ip, [r4, #32]
	cmp	ip, #1
	beq	.L1141
	ldr	r3, [r4, #28]
	cmp	r3, #0
	beq	.L1034
	ldr	r5, [r3, #-4]
	mov	r6, #0
	add	r5, r3, r5, asl #6
.L1131:
	ldr	r0, [r8, #0]
	cmp	r0, r5
	beq	.L1068
.L1142:
	sub	r5, r5, #64
	ldr	r0, [r5, #4]
	cmp	r0, #0
	beq	.L1069
	bl	s3eFileClose
	str	r6, [r5, #4]
.L1069:
	add	r0, r5, #12
	add	r1, r5, #16
	mov	r2, #0
	bl	png_destroy_read_struct
	ldr	r0, [r5, #56]
	cmp	r0, #0
	beq	.L1070
	bl	s3eFree
	str	r6, [r5, #56]
.L1070:
	ldr	r0, [r5, #60]
	cmp	r0, #0
	beq	.L1131
	bl	s3eFree
	str	r6, [r5, #60]
	ldr	r0, [r8, #0]
	cmp	r0, r5
	bne	.L1142
.L1068:
	ldr	r1, [r4, #28]
	sub	r0, r1, #8
	bl	_ZdaPv
	ldrb	r3, [r4, #4]	@ zero_extendqisi2
	mov	r2, #0
	cmp	r3, #0
	str	r2, [r4, #28]
	str	r9, [r4, #32]
	beq	.L1072
.L1135:
	mov	r0, #1
	add	r1, r4, #8
	bl	glDeleteTextures
.LEHE45:
	b	.L1072
.L1137:
	ldr	r1, [r7, #0]
	ldr	r5, [r1, #4]
	cmp	r5, #0
	ldr	r2, [r4, #28]
	beq	.L1089
	mov	r0, r1
	mov	r6, r5
	b	.L1093
.L1143:
	mov	r0, r6
	mov	r6, r3
.L1093:
	ldr	r3, [r6, #16]
	cmp	r2, r3
	ldrhi	r3, [r6, #12]
	ldrls	r3, [r6, #8]
	movhi	r6, r0
	cmp	r3, #0
	bne	.L1143
	mov	r0, r1
	b	.L1097
.L1144:
	mov	r0, r5
	mov	r5, r3
.L1097:
	ldr	r3, [r5, #16]
	cmp	r2, r3
	ldrcs	r3, [r5, #12]
	ldrcc	r3, [r5, #8]
	movcs	r5, r0
	cmp	r3, #0
	bne	.L1144
	cmp	r6, r5
	mov	sl, r6
	moveq	r6, r5
	beq	.L1100
	mov	r0, r6
.L1101:
.LEHB46:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r5, r0
	bne	.L1101
	ldr	r1, [r7, #0]
	mov	sl, r5
.L1100:
	ldr	r2, [r1, #8]
	cmp	r6, r2
	beq	.L1145
.L1103:
	cmp	sl, r6
	bne	.L1129
	b	.L1106
.L1109:
	mov	r6, r5
.L1129:
	mov	r0, r6
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r2, [r7, #0]
	mov	r5, r0
	add	r3, r2, #12
	add	r1, r2, #4
	mov	r0, r6
	add	r2, r2, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L1108:
	ldr	r0, [r7, #4]
	cmp	sl, r5
	sub	ip, r0, #1
	str	ip, [r7, #4]
	bne	.L1109
.L1106:
	ldr	r1, [r4, #32]
	cmp	r1, #1
	beq	.L1146
	ldr	r3, [r4, #28]
	cmp	r3, #0
	beq	.L1078
	ldr	r6, [r3, #-4]
	add	r5, r3, r6, asl #6
	mov	r6, #0
.L1132:
	ldr	lr, [r8, #0]
	cmp	lr, r5
	beq	.L1112
.L1147:
	sub	r5, r5, #64
	ldr	r0, [r5, #4]
	cmp	r0, #0
	beq	.L1113
	bl	s3eFileClose
	str	r6, [r5, #4]
.L1113:
	add	r0, r5, #12
	add	r1, r5, #16
	mov	r2, #0
	bl	png_destroy_read_struct
	ldr	r0, [r5, #56]
	cmp	r0, #0
	beq	.L1114
	bl	s3eFree
	str	r6, [r5, #56]
.L1114:
	ldr	r0, [r5, #60]
	cmp	r0, #0
	beq	.L1132
	bl	s3eFree
.LEHE46:
	str	r6, [r5, #60]
	ldr	lr, [r8, #0]
	cmp	lr, r5
	bne	.L1147
.L1112:
	ldr	r3, [r4, #28]
	sub	r0, r3, #8
	bl	_ZdaPv
	b	.L1078
.L1141:
	ldr	r5, [r4, #28]
	cmp	r5, #0
	beq	.L1034
	mov	r0, r5
.LEHB47:
	bl	_ZN9PngLoader5abortEv
	mov	r0, r5
	bl	_ZN9PngLoader15deleteImageDataEv
.LEHE47:
	mov	r0, r5
	bl	_ZdlPv
	b	.L1034
.L1146:
	ldr	r5, [r4, #28]
	cmp	r5, #0
	beq	.L1078
	mov	r0, r5
.LEHB48:
	bl	_ZN9PngLoader5abortEv
	mov	r0, r5
	bl	_ZN9PngLoader15deleteImageDataEv
.LEHE48:
	mov	r0, r5
	bl	_ZdlPv
	b	.L1078
.L1089:
	mov	sl, r1
	mov	r6, r1
	b	.L1100
.L1045:
	mov	sl, r1
	mov	r6, r1
	b	.L1056
.L1140:
	cmp	sl, r1
	bne	.L1059
	ldr	r2, [r7, #4]
	cmp	r2, #0
	beq	.L1062
	ldr	r0, [sl, #4]
.LEHB49:
	bl	T.560
.LEHE49:
	ldr	r1, [r7, #0]
	str	r1, [r1, #8]
	ldr	r0, [r7, #0]
	mov	lr, #0
	str	lr, [r0, #4]
	ldr	ip, [r7, #0]
	str	ip, [ip, #12]
	str	lr, [r7, #4]
	b	.L1062
.L1145:
	cmp	sl, r1
	bne	.L1103
	ldr	ip, [r7, #4]
	cmp	ip, #0
	beq	.L1106
	ldr	r0, [sl, #4]
.LEHB50:
	bl	T.560
.LEHE50:
	ldr	r3, [r7, #0]
	str	r3, [r3, #8]
	ldr	r1, [r7, #0]
	mov	r0, #0
	str	r0, [r1, #4]
	ldr	lr, [r7, #0]
	str	lr, [lr, #12]
	str	r0, [r7, #4]
	b	.L1106
.L1120:
.L1075:
	ldr	r3, [r4, #36]
	cmp	r3, #0
	mov	r5, r0
	movne	r0, r3
	blne	free
.L1076:
.L1121:
.L1079:
	mov	r0, r8
	mov	r1, #0
	ldr	r2, [r4, #32]
	bl	_ZN15QuStupidPointerI9PngLoaderE3setEPS0_i
.L1118:
	ldr	r7, .L1148+8
	mov	r0, r4
	str	r7, [r4, #0]
	bl	_ZN11QuBaseImage7destroyEv
	mov	r0, r5
.LEHB51:
	bl	__cxa_end_cleanup
.LEHE51:
.L1122:
	mov	r5, r0
	b	.L1118
.L1149:
	.align	2
.L1148:
	.word	.LANCHOR0+56
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.word	.LANCHOR0+32
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA2842:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE2842-.LLSDACSB2842
.LLSDACSB2842:
	.uleb128 .LEHB42-.LFB2842
	.uleb128 .LEHE42-.LEHB42
	.uleb128 .L1120-.LFB2842
	.uleb128 0x0
	.uleb128 .LEHB43-.LFB2842
	.uleb128 .LEHE43-.LEHB43
	.uleb128 .L1122-.LFB2842
	.uleb128 0x0
	.uleb128 .LEHB44-.LFB2842
	.uleb128 .LEHE44-.LEHB44
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB45-.LFB2842
	.uleb128 .LEHE45-.LEHB45
	.uleb128 .L1120-.LFB2842
	.uleb128 0x0
	.uleb128 .LEHB46-.LFB2842
	.uleb128 .LEHE46-.LEHB46
	.uleb128 .L1122-.LFB2842
	.uleb128 0x0
	.uleb128 .LEHB47-.LFB2842
	.uleb128 .LEHE47-.LEHB47
	.uleb128 .L1120-.LFB2842
	.uleb128 0x0
	.uleb128 .LEHB48-.LFB2842
	.uleb128 .LEHE48-.LEHB48
	.uleb128 .L1122-.LFB2842
	.uleb128 0x0
	.uleb128 .LEHB49-.LFB2842
	.uleb128 .LEHE49-.LEHB49
	.uleb128 .L1120-.LFB2842
	.uleb128 0x0
	.uleb128 .LEHB50-.LFB2842
	.uleb128 .LEHE50-.LEHB50
	.uleb128 .L1122-.LFB2842
	.uleb128 0x0
	.uleb128 .LEHB51-.LFB2842
	.uleb128 .LEHE51-.LEHB51
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE2842:
	.fnend
	.size	_ZN10QuPngImageD1Ev, .-_ZN10QuPngImageD1Ev
	.section	.text._ZN10QuPngImage18finishLoadingImageEv,"ax",%progbits
	.align	2
	.global	_ZN10QuPngImage18finishLoadingImageEv
	.hidden	_ZN10QuPngImage18finishLoadingImageEv
	.type	_ZN10QuPngImage18finishLoadingImageEv, %function
_ZN10QuPngImage18finishLoadingImageEv:
	.fnstart
.LFB2859:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, lr}
	ldrb	r3, [r0, #4]	@ zero_extendqisi2
	cmp	r3, #0
	.pad #48
	sub	sp, sp, #48
	mov	r4, r0
	beq	.L1151
.L1200:
	mov	r0, #1
.L1152:
	add	sp, sp, #48
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L1151:
	ldr	r5, [r0, #28]
	ldrb	r2, [r5, #0]	@ zero_extendqisi2
	cmp	r2, #0
	ldr	r3, [r5, #24]
	beq	.L1201
.L1153:
	ldr	sl, [r5, #40]
	sub	r9, sl, #2
	cmp	r9, #4
	ldrls	r2, .L1211
	mvn	ip, #0
	ldrls	r6, [r2, r9, asl #2]
	add	r9, sp, #48
	movhi	r7, #6400
	mov	lr, #3312
	ldr	sl, [r5, #56]
	ldr	r8, [r5, #28]
	ldr	r5, [r5, #32]
	str	ip, [r9, #-4]!
	addhi	r7, r7, #7
	add	r0, lr, #5
	mov	r1, #1
	movhi	r6, r7
	movls	r7, r6
	bl	glPixelStorei
	mov	r1, r9
	mov	r0, #1
	bl	glGenTextures
	mov	r1, #3552
	add	r0, r1, #1
	ldr	r1, [sp, #44]
	bl	glBindTexture
	mov	r3, #33024
	mov	r2, #10240
	mov	r0, #3552
	add	r1, r2, #2
	add	r0, r0, #1
	add	r2, r3, #47
	bl	glTexParameterx
	mov	ip, #3552
	mov	r1, #10240
	mov	r2, #33024
	add	r0, ip, #1
	add	r1, r1, #3
	add	r2, r2, #47
	bl	glTexParameterx
	mov	r3, #9728
	mov	r0, #3552
	add	r2, r3, #1
	add	r0, r0, #1
	mov	r1, #10240
	bl	glTexParameteri
	mov	ip, #3552
	mov	r1, #10240
	mov	r2, #9728
	add	r0, ip, #1
	add	r1, r1, #1
	add	r2, r2, #1
	bl	glTexParameteri
	mov	lr, #0
	mov	r0, #5120
	mov	r3, #3552
	add	ip, r0, #1
	mov	r1, lr
	add	r0, r3, #1
	mov	r2, r7
	mov	r3, r8
	str	r5, [sp, #0]
	str	r6, [sp, #8]
	str	ip, [sp, #12]
	str	sl, [sp, #16]
	str	lr, [sp, #4]
	bl	glTexImage2D
	ldrb	r5, [r4, #4]	@ zero_extendqisi2
	cmp	r5, #0
	ldr	r5, [sp, #44]
	bne	.L1202
.L1157:
	ldr	ip, [r4, #28]
	mov	r7, #1
	cmp	ip, #0
	strb	r7, [r4, #4]
	str	r5, [r4, #8]
	add	r7, r4, #28
	ldr	r8, [r4, #32]
	beq	.L1158
	ldr	r5, .L1211+4
	ldr	r0, [r5, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L1162
	mov	r1, r0
	b	.L1163
.L1203:
	mov	r1, r3
	mov	r3, r2
.L1163:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L1203
.L1162:
	cmp	r3, r0
	beq	.L1165
	ldr	r2, [r3, #16]
	cmp	ip, r2
	mov	r2, r3
	bcs	.L1166
.L1165:
	add	r2, sp, #28
	str	r3, [sp, #36]
	add	r0, sp, #40
	mov	r3, #0
	add	r1, sp, #36
	str	ip, [sp, #28]
	str	r3, [sp, #32]
	bl	T.561
	ldr	r2, [sp, #40]
.L1166:
	ldr	r1, [r2, #20]
	sub	r0, r1, #1
	cmp	r0, #0
	str	r0, [r2, #20]
	beq	.L1204
.L1158:
	mov	r2, #0
	str	r8, [r4, #32]
	str	r2, [r4, #28]
	b	.L1200
.L1202:
	mov	r0, #1
	add	r1, r4, #8
	bl	glDeleteTextures
	b	.L1157
.L1201:
	ldr	r1, [r5, #52]
	ldr	ip, [r5, #60]
	rsb	r6, r1, r3
	cmp	r6, r3
	movcs	r6, r3
	ldr	r0, [r5, #12]
	mov	r3, r6
	add	r1, ip, r1, asl #2
	bl	png_read_rows
	ldr	r2, [r5, #52]
	ldr	r0, [r5, #24]
	add	r6, r6, r2
	cmp	r6, r0
	str	r6, [r5, #52]
	beq	.L1205
.L1154:
	ldr	r5, [r4, #28]
	ldrb	r0, [r5, #0]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L1152
	b	.L1153
.L1204:
	ldr	r1, [r5, #0]
	ldr	r6, [r1, #4]
	cmp	r6, #0
	ldr	r2, [r4, #28]
	moveq	sl, r1
	moveq	r9, r1
	beq	.L1178
	mov	r0, r1
	mov	r9, r6
	b	.L1171
.L1206:
	mov	r0, r9
	mov	r9, r3
.L1171:
	ldr	r3, [r9, #16]
	cmp	r2, r3
	ldrhi	r3, [r9, #12]
	ldrls	r3, [r9, #8]
	movhi	r9, r0
	cmp	r3, #0
	bne	.L1206
	mov	r0, r1
	b	.L1175
.L1207:
	mov	r0, r6
	mov	r6, r3
.L1175:
	ldr	r3, [r6, #16]
	cmp	r2, r3
	ldrcs	r3, [r6, #12]
	ldrcc	r3, [r6, #8]
	movcs	r6, r0
	cmp	r3, #0
	bne	.L1207
	cmp	r9, r6
	mov	sl, r9
	moveq	r9, r6
	beq	.L1178
	mov	r0, r9
.L1179:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r6, r0
	bne	.L1179
	ldr	r1, [r5, #0]
	mov	sl, r6
.L1178:
	ldr	ip, [r1, #8]
	cmp	r9, ip
	beq	.L1208
.L1181:
	cmp	r9, sl
	bne	.L1197
	b	.L1184
.L1187:
	mov	r9, r6
.L1197:
	mov	r0, r9
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r2, [r5, #0]
	mov	r6, r0
	add	r3, r2, #12
	add	r1, r2, #4
	mov	r0, r9
	add	r2, r2, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L1186:
	ldr	r3, [r5, #4]
	cmp	r6, sl
	sub	lr, r3, #1
	str	lr, [r5, #4]
	bne	.L1187
.L1184:
	ldr	r0, [r4, #32]
	cmp	r0, #1
	beq	.L1209
	ldr	r3, [r4, #28]
	cmp	r3, #0
	beq	.L1158
	ldr	r6, [r3, #-4]
	add	r5, r3, r6, asl #6
	mov	r6, #0
.L1199:
	ldr	r1, [r7, #0]
	cmp	r1, r5
	sub	r5, r5, #64
	beq	.L1190
.L1210:
	ldr	r0, [r5, #4]
	cmp	r0, #0
	beq	.L1191
	bl	s3eFileClose
	str	r6, [r5, #4]
.L1191:
	add	r0, r5, #12
	add	r1, r5, #16
	mov	r2, #0
	bl	png_destroy_read_struct
	ldr	r0, [r5, #56]
	cmp	r0, #0
	beq	.L1192
	bl	s3eFree
	str	r6, [r5, #56]
.L1192:
	ldr	r0, [r5, #60]
	cmp	r0, #0
	beq	.L1199
	bl	s3eFree
	str	r6, [r5, #60]
	ldr	r1, [r7, #0]
	cmp	r1, r5
	sub	r5, r5, #64
	bne	.L1210
.L1190:
	ldr	ip, [r4, #28]
	sub	r0, ip, #8
	bl	_ZdaPv
	b	.L1158
.L1205:
	mov	r2, #1
	strb	r2, [r5, #0]
	mov	r0, r5
	bl	_ZN9PngLoader5abortEv
	b	.L1154
.L1209:
	ldr	r5, [r4, #28]
	cmp	r5, #0
	beq	.L1158
	mov	r0, r5
	bl	_ZN9PngLoader5abortEv
	mov	r0, r5
	bl	_ZN9PngLoader15deleteImageDataEv
	mov	r0, r5
	bl	_ZdlPv
	b	.L1158
.L1208:
	cmp	sl, r1
	bne	.L1181
	ldr	r2, [r5, #4]
	cmp	r2, #0
	ldr	r5, .L1211+4
	beq	.L1184
	ldr	r0, [sl, #4]
	bl	T.560
	ldr	ip, [r5, #0]
	str	ip, [ip, #8]
	ldr	r1, [r5, #0]
	mov	r3, #0
	str	r3, [r1, #4]
	ldr	r0, [r5, #0]
	str	r0, [r0, #12]
	str	r3, [r5, #4]
	b	.L1184
.L1212:
	.align	2
.L1211:
	.word	.LANCHOR0
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	_ZN10QuPngImage18finishLoadingImageEv, .-_ZN10QuPngImage18finishLoadingImageEv
	.hidden	_ZTV10QuPngImage
	.global	_ZTV10QuPngImage
	.hidden	_ZTV11QuBaseImage
	.global	_ZTV11QuBaseImage
	.hidden	_ZTS10QuPngImage
	.global	_ZTS10QuPngImage
	.hidden	_ZTI10QuPngImage
	.global	_ZTI10QuPngImage
	.hidden	_ZTS11QuBaseImage
	.global	_ZTS11QuBaseImage
	.hidden	_ZTI11QuBaseImage
	.global	_ZTI11QuBaseImage
	.section	.rodata
	.align	3
	.set	.LANCHOR0,. + 0
	.type	CSWTCH.533, %object
	.size	CSWTCH.533, 20
CSWTCH.533:
	.word	6407
	.word	6407
	.word	6407
	.word	6407
	.word	6408
	.space	4
	.type	_ZTV11QuBaseImage, %object
	.size	_ZTV11QuBaseImage, 24
_ZTV11QuBaseImage:
	.word	0
	.word	_ZTI11QuBaseImage
	.word	_ZN11QuBaseImage4bindEv
	.word	_ZN11QuBaseImageD1Ev
	.word	_ZN11QuBaseImageD0Ev
	.word	_ZN11QuBaseImage7destroyEv
	.type	_ZTV10QuPngImage, %object
	.size	_ZTV10QuPngImage, 24
_ZTV10QuPngImage:
	.word	0
	.word	_ZTI10QuPngImage
	.word	_ZN11QuBaseImage4bindEv
	.word	_ZN10QuPngImageD1Ev
	.word	_ZN10QuPngImageD0Ev
	.word	_ZN10QuPngImage7destroyEv
	.type	_ZTS10QuPngImage, %object
	.size	_ZTS10QuPngImage, 13
_ZTS10QuPngImage:
	.ascii	"10QuPngImage\000"
	.space	3
	.type	_ZTI10QuPngImage, %object
	.size	_ZTI10QuPngImage, 12
_ZTI10QuPngImage:
	.word	_ZTVN10__cxxabiv120__si_class_type_infoE+8
	.word	_ZTS10QuPngImage
	.word	_ZTI11QuBaseImage
	.type	_ZTS11QuBaseImage, %object
	.size	_ZTS11QuBaseImage, 14
_ZTS11QuBaseImage:
	.ascii	"11QuBaseImage\000"
	.space	2
	.type	_ZTI11QuBaseImage, %object
	.size	_ZTI11QuBaseImage, 8
_ZTI11QuBaseImage:
	.word	_ZTVN10__cxxabiv117__class_type_infoE+8
	.word	_ZTS11QuBaseImage
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"basic_string\000"
	.space	3
.LC1:
	.ascii	"rb\000"
	.space	1
.LC2:
	.ascii	"1.2.34\000"
	.bss
	.align	2
	.set	.LANCHOR1,. + 0
	.type	_ZN4_STLL8_LocInitE, %object
	.size	_ZN4_STLL8_LocInitE, 1
_ZN4_STLL8_LocInitE:
	.space	1
	.space	3
	.type	_ZN4_STLL8_IosInitE, %object
	.size	_ZN4_STLL8_IosInitE, 1
_ZN4_STLL8_IosInitE:
	.space	1
	.hidden	_ZTV10QuPngImage
	.hidden	_ZTV11QuBaseImage
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
