	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"gzio.c"
	.section	.text.gzungetc,"ax",%progbits
	.align	2
	.global	gzungetc
	.hidden	gzungetc
	.type	gzungetc, %function
gzungetc:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r1, #0
	bne	.L7
.L2:
	mvn	r0, #0
	bx	lr
.L7:
	ldrb	r2, [r1, #92]	@ zero_extendqisi2
	cmn	r0, #1
	movne	r3, #0
	moveq	r3, #1
	cmp	r2, #114
	orrne	r3, r3, #1
	cmp	r3, #0
	bne	.L2
	ldr	ip, [r1, #108]
	cmn	ip, #1
	bne	.L2
	ldr	r2, [r1, #56]
	ldr	ip, [r1, #104]
	cmp	r2, #1
	movne	r2, #0
	moveq	r2, #1
	cmp	r2, #0
	sub	ip, ip, #1
	strne	r3, [r1, #56]
	mov	r3, #0
	str	ip, [r1, #104]
	str	r0, [r1, #108]
	str	r2, [r1, #112]
	str	r3, [r1, #60]
	bx	lr
	.size	gzungetc, .-gzungetc
	.section	.text.gzeof,"ax",%progbits
	.align	2
	.global	gzeof
	.hidden	gzeof
	.type	gzeof, %function
gzeof:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	beq	.L9
	ldrb	r3, [r0, #92]	@ zero_extendqisi2
	cmp	r3, #114
	beq	.L13
.L9:
	mov	r0, #0
	bx	lr
.L13:
	ldr	r1, [r0, #60]
	cmp	r1, #0
	movne	r0, #1
	bxne	lr
	ldr	r0, [r0, #56]
	cmp	r0, #1
	movne	r0, #0
	moveq	r0, #1
	bx	lr
	.size	gzeof, .-gzeof
	.section	.text.gzdirect,"ax",%progbits
	.align	2
	.global	gzdirect
	.hidden	gzdirect
	.type	gzdirect, %function
gzdirect:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	beq	.L15
	ldrb	r3, [r0, #92]	@ zero_extendqisi2
	cmp	r3, #114
	ldreq	r0, [r0, #88]
	bxeq	lr
.L15:
	mov	r0, #0
	bx	lr
	.size	gzdirect, .-gzdirect
	.section	.text.gzclearerr,"ax",%progbits
	.align	2
	.global	gzclearerr
	.hidden	gzclearerr
	.type	gzclearerr, %function
gzclearerr:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	stmfd	sp!, {r3, lr}
	beq	.L21
	ldr	r1, [r0, #56]
	cmp	r1, #1
	movne	r1, #0
	mov	r3, #0
	strne	r1, [r0, #56]
	str	r3, [r0, #60]
	ldr	r0, [r0, #64]
	bl	clearerr
.L21:
	ldmfd	sp!, {r3, lr}
	bx	lr
	.size	gzclearerr, .-gzclearerr
	.section	.text.gzerror,"ax",%progbits
	.align	2
	.global	gzerror
	.hidden	gzerror
	.type	gzerror, %function
gzerror:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	subs	r4, r0, #0
	beq	.L33
	ldr	r3, [r4, #56]
	cmp	r3, #0
	str	r3, [r1, #0]
	ldreq	r0, .L36
	beq	.L24
	cmn	r3, #1
	ldrne	r5, [r4, #24]
	beq	.L34
.L27:
	cmp	r5, #0
	beq	.L28
	ldrb	r1, [r5, #0]	@ zero_extendqisi2
	cmp	r1, #0
	bne	.L29
.L28:
	ldr	r5, [r4, #56]
	ldr	r6, .L36+4
	rsb	r2, r5, #2
	ldr	r5, [r6, r2, asl #2]
.L29:
	ldr	r0, [r4, #80]
	cmp	r0, #0
	blne	free
.L30:
	ldr	r0, [r4, #84]
	bl	strlen
	mov	r6, r0
	mov	r0, r5
	bl	strlen
	add	r3, r6, r0
	add	r0, r3, #3
	bl	malloc
	cmp	r0, #0
	str	r0, [r4, #80]
	beq	.L35
	ldr	r1, [r4, #84]
	bl	strcpy
	ldr	r6, [r4, #80]
	mov	r0, r6
	bl	strlen
	ldr	r1, .L36+8
	mov	r2, #3
	add	r0, r6, r0
	bl	memcpy
	mov	r1, r5
	ldr	r0, [r4, #80]
	bl	strcat
	ldr	r0, [r4, #80]
.L24:
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L34:
	bl	__errno
	ldr	r0, [r0, #0]
	bl	strerror
	mov	r5, r0
	b	.L27
.L35:
	ldr	lr, .L36+4
	ldr	r0, [lr, #24]
	b	.L24
.L33:
	ldr	r3, .L36+4
	mvn	r0, #1
	str	r0, [r1, #0]
	ldr	r0, [r3, #16]
	b	.L24
.L37:
	.align	2
.L36:
	.word	.LC0
	.word	z_errmsg
	.word	.LC1
	.size	gzerror, .-gzerror
	.section	.text.destroy,"ax",%progbits
	.align	2
	.type	destroy, %function
destroy:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	subs	r4, r0, #0
	mvneq	r5, #1
	beq	.L40
	ldr	r0, [r4, #80]
	cmp	r0, #0
	blne	free
.L41:
	ldr	r3, [r4, #28]
	cmp	r3, #0
	beq	.L42
	ldrb	r3, [r4, #92]	@ zero_extendqisi2
	cmp	r3, #119
	beq	.L51
	cmp	r3, #114
	beq	.L52
.L42:
	mov	r5, #0
.L44:
	ldr	r0, [r4, #64]
	cmp	r0, #0
	beq	.L45
	bl	fclose
	cmp	r0, #0
	bne	.L53
.L45:
	ldr	r2, [r4, #56]
	ldr	r0, [r4, #68]
	cmp	r2, #0
	movlt	r5, r2
	cmp	r0, #0
	blne	free
.L47:
	ldr	r0, [r4, #72]
	cmp	r0, #0
	blne	free
.L48:
	ldr	r0, [r4, #84]
	cmp	r0, #0
	blne	free
.L49:
	mov	r0, r4
	bl	free
.L40:
	mov	r0, r5
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L53:
	bl	__errno
	ldr	r1, [r0, #0]
	cmp	r1, #29
	mvnne	r5, #0
	b	.L45
.L52:
	mov	r0, r4
	bl	inflateEnd
	mov	r5, r0
	b	.L44
.L51:
	mov	r0, r4
	bl	deflateEnd
	mov	r5, r0
	b	.L44
	.size	destroy, .-destroy
	.section	.text.gzclose,"ax",%progbits
	.align	2
	.global	gzclose
	.hidden	gzclose
	.type	gzclose, %function
gzclose:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	subs	r4, r0, #0
	beq	.L73
	ldrb	r3, [r4, #92]	@ zero_extendqisi2
	cmp	r3, #119
	beq	.L74
.L57:
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	destroy
.L73:
	mvn	r0, #1
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L74:
	mov	r6, #0
	str	r6, [r4, #4]
	ldr	r3, [r4, #16]
	mov	r7, #16384
.L68:
	rsbs	r5, r3, #16384
	bne	.L75
.L58:
	cmp	r6, #0
	bne	.L76
	ldr	lr, [r4, #104]
	add	ip, lr, r3
	str	ip, [r4, #104]
	mov	r0, r4
	mov	r1, #4
	bl	deflate
	ldr	r2, [r4, #104]
	ldr	r3, [r4, #16]
	cmp	r5, #0
	rsb	r2, r3, r2
	str	r2, [r4, #104]
	str	r0, [r4, #56]
	bne	.L72
	cmn	r0, #5
	moveq	r2, r5
	streq	r5, [r4, #56]
	moveq	r0, r2
	beq	.L64
.L72:
	mov	r2, r0
.L64:
	cmp	r3, #0
	movne	r6, #1
	bne	.L67
	cmp	r0, #1
	movne	r6, #0
	moveq	r6, #1
.L67:
	cmp	r2, #1
	bls	.L68
.L62:
	cmp	r0, #1
	beq	.L69
	cmp	r0, #0
	bne	.L57
.L69:
	ldr	r5, [r4, #76]
	ldr	r6, [r4, #64]
	and	r0, r5, #255
	mov	r1, r6
	bl	fputc
	mov	r2, r5, lsr #8
	and	r0, r2, #255
	mov	r1, r6
	bl	fputc
	mov	r1, r5, lsr #16
	and	r0, r1, #255
	mov	r1, r6
	bl	fputc
	mov	r1, r6
	mov	r0, r5, lsr #24
	bl	fputc
	ldr	r6, [r4, #100]
	ldr	r5, [r4, #64]
	and	r0, r6, #255
	mov	r1, r5
	bl	fputc
	mov	r0, r6, lsr #8
	mov	r1, r5
	and	r0, r0, #255
	bl	fputc
	mov	r3, r6, lsr #16
	mov	r1, r5
	and	r0, r3, #255
	bl	fputc
	mov	r0, r6, lsr #24
	mov	r1, r5
	bl	fputc
	b	.L57
.L75:
	ldr	r0, [r4, #72]
	mov	r1, #1
	mov	r2, r5
	ldr	r3, [r4, #64]
	bl	fwrite
	cmp	r5, r0
	bne	.L77
	ldr	r1, [r4, #72]
	str	r7, [r4, #16]
	str	r1, [r4, #12]
	mov	r3, r7
	b	.L58
.L76:
	ldr	r0, [r4, #56]
	b	.L62
.L77:
	mvn	r0, #0
	str	r0, [r4, #56]
	b	.L57
	.size	gzclose, .-gzclose
	.section	.text.gzwrite,"ax",%progbits
	.align	2
	.global	gzwrite
	.hidden	gzwrite
	.type	gzwrite, %function
gzwrite:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	subs	r4, r0, #0
	mov	r6, r1
	mov	r5, r2
	bne	.L87
.L79:
	mvn	r0, #1
.L85:
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L87:
	ldrb	r3, [r4, #92]	@ zero_extendqisi2
	cmp	r3, #119
	streq	r1, [r4, #0]
	streq	r2, [r4, #4]
	moveq	r3, r2
	bne	.L79
.L80:
	cmp	r3, #0
	mov	r1, #1
	mov	r2, #16384
	beq	.L83
	ldr	r0, [r4, #16]
	cmp	r0, #0
	bne	.L81
	ldr	r0, [r4, #72]
	ldr	r3, [r4, #64]
	str	r0, [r4, #12]
	bl	fwrite
	cmp	r0, #16384
	bne	.L88
	str	r0, [r4, #16]
	ldr	r3, [r4, #4]
.L81:
	add	r1, r4, #100
	ldmia	r1, {r1, r2}	@ phole ldm
	add	lr, r1, r3
	add	r0, r2, r0
	str	r0, [r4, #104]
	mov	r1, #0
	str	lr, [r4, #100]
	mov	r0, r4
	bl	deflate
	add	r1, r4, #100
	ldmia	r1, {r1, ip}	@ phole ldm
	ldr	r3, [r4, #4]
	ldr	r2, [r4, #16]
	rsb	r1, r3, r1
	rsb	r2, r2, ip
	cmp	r0, #0
	str	r1, [r4, #100]
	str	r2, [r4, #104]
	str	r0, [r4, #56]
	beq	.L80
.L83:
	mov	r1, r6
	ldr	r0, [r4, #76]
	mov	r2, r5
	bl	crc32
	ldr	r3, [r4, #4]
	str	r0, [r4, #76]
	rsb	r0, r3, r5
	b	.L85
.L88:
	mvn	r3, #0
	str	r3, [r4, #56]
	b	.L83
	.size	gzwrite, .-gzwrite
	.section	.text.gzrewind,"ax",%progbits
	.align	2
	.global	gzrewind
	.hidden	gzrewind
	.type	gzrewind, %function
gzrewind:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	subs	r4, r0, #0
	bne	.L94
.L90:
	mvn	r0, #0
.L92:
	ldmfd	sp!, {r4, lr}
	bx	lr
.L94:
	ldrb	r3, [r4, #92]	@ zero_extendqisi2
	cmp	r3, #114
	bne	.L90
	ldr	r2, [r4, #68]
	mov	ip, #0
	mvn	lr, #0
	str	r2, [r4, #0]
	mov	r1, ip
	mov	r0, ip
	str	lr, [r4, #108]
	str	ip, [r4, #56]
	str	ip, [r4, #60]
	str	ip, [r4, #4]
	mov	r2, ip
	bl	crc32
	ldr	r1, [r4, #88]
	cmp	r1, #0
	str	r0, [r4, #76]
	beq	.L95
.L91:
	mov	r2, #0
	str	r2, [r4, #100]
	str	r2, [r4, #104]
	ldr	r1, [r4, #96]
	ldr	r0, [r4, #64]
	bl	fseek
	b	.L92
.L95:
	mov	r0, r4
	bl	inflateReset
	b	.L91
	.size	gzrewind, .-gzrewind
	.section	.text.get_byte,"ax",%progbits
	.align	2
	.type	get_byte, %function
get_byte:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	ldr	r3, [r0, #60]
	cmp	r3, #0
	mov	r4, r0
	bne	.L97
	ldr	r5, [r0, #4]
	cmp	r5, #0
	beq	.L102
.L98:
	sub	r5, r5, #1
	ldr	r2, [r4, #0]
	str	r5, [r4, #4]
	ldrb	r0, [r2], #1	@ zero_extendqisi2
	str	r2, [r4, #0]
.L100:
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L103:
	mov	r0, #1
	str	r0, [r4, #60]
	ldr	r0, [r4, #64]
	bl	ferror
	cmp	r0, #0
	mvnne	r0, #0
	strne	r0, [r4, #56]
	bne	.L100
.L97:
	mvn	r0, #0
	b	.L100
.L102:
	bl	__errno
	str	r5, [r0, #0]
	mov	r1, #1
	ldr	r0, [r4, #68]
	mov	r2, #16384
	ldr	r3, [r4, #64]
	bl	fread
	cmp	r0, #0
	mov	r5, r0
	str	r0, [r4, #4]
	beq	.L103
	ldr	r1, [r4, #68]
	str	r1, [r4, #0]
	b	.L98
	.size	get_byte, .-get_byte
	.section	.text.check_header,"ax",%progbits
	.align	2
	.type	check_header, %function
check_header:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, lr}
	ldr	r5, [r0, #4]
	cmp	r5, #1
	mov	r4, r0
	ldrhi	r3, [r0, #0]
	bls	.L150
.L108:
	ldrb	ip, [r3, #0]	@ zero_extendqisi2
	cmp	ip, #31
	beq	.L151
.L110:
	mov	r2, #1
	str	r2, [r4, #88]
.L123:
	ldmfd	sp!, {r3, r4, r5, r6, r7, lr}
	bx	lr
.L151:
	ldrb	r0, [r3, #1]	@ zero_extendqisi2
	cmp	r0, #139
	bne	.L110
	ldr	lr, [r4, #0]
	sub	r5, r5, #2
	add	r6, lr, #2
	str	r5, [r4, #4]
	str	r6, [r4, #0]
	mov	r0, r4
	bl	get_byte
	mov	r6, r0
	mov	r0, r4
	bl	get_byte
	cmp	r6, #8
	mov	r5, r0
	beq	.L152
.L112:
	mvn	r3, #2
	str	r3, [r4, #56]
	b	.L123
.L150:
	cmp	r5, #0
	ldrne	r3, [r0, #0]
	ldrneb	r2, [r3, #0]	@ zero_extendqisi2
	ldrne	r3, [r0, #68]
	strneb	r2, [r3, #0]
	bl	__errno
	mov	r3, #0
	str	r3, [r0, #0]
	ldr	r0, [r4, #68]
	mov	r2, #16384
	mov	r2, r2, asr r5
	add	r0, r5, r0
	mov	r1, #1
	ldr	r3, [r4, #64]
	bl	fread
	subs	r5, r0, #0
	beq	.L153
.L107:
	ldr	r1, [r4, #4]
	ldr	r3, [r4, #68]
	add	r5, r5, r1
	cmp	r5, #1
	stmia	r4, {r3, r5}	@ phole stm
	strls	r5, [r4, #88]
	bhi	.L108
	b	.L123
.L153:
	ldr	r0, [r4, #64]
	bl	ferror
	cmp	r0, #0
	mvnne	r3, #0
	strne	r3, [r4, #56]
	b	.L107
.L152:
	tst	r0, #224
	bne	.L112
	mov	r0, r4
	bl	get_byte
	mov	r0, r4
	bl	get_byte
	mov	r0, r4
	bl	get_byte
	mov	r0, r4
	bl	get_byte
	mov	r0, r4
	bl	get_byte
	mov	r0, r4
	bl	get_byte
	tst	r5, #4
	bne	.L154
.L114:
	tst	r5, #8
	bne	.L125
.L116:
	tst	r5, #16
	bne	.L124
.L118:
	tst	r5, #2
	beq	.L120
	mov	r0, r4
	bl	get_byte
	mov	r0, r4
	bl	get_byte
.L120:
	ldr	r1, [r4, #60]
	cmp	r1, #0
	mvnne	r1, #2
	str	r1, [r4, #56]
	b	.L123
.L124:
	mov	r0, r4
	bl	get_byte
	cmp	r0, #0
	beq	.L118
	cmn	r0, #1
	bne	.L124
	b	.L118
.L125:
	mov	r0, r4
	bl	get_byte
	cmp	r0, #0
	beq	.L116
	cmn	r0, #1
	bne	.L125
	b	.L116
.L154:
	mov	r0, r4
	bl	get_byte
	mov	r7, r0
	mov	r0, r4
	bl	get_byte
	add	r6, r7, r0, asl #8
	ands	r7, r6, #3
	beq	.L115
	mov	r0, r4
	bl	get_byte
	cmn	r0, #1
	sub	r6, r6, #1
	beq	.L114
	cmp	r7, #1
	beq	.L115
	cmp	r7, #2
	beq	.L146
	mov	r0, r4
	bl	get_byte
	cmn	r0, #1
	sub	r6, r6, #1
	beq	.L114
.L146:
	mov	r0, r4
	bl	get_byte
	cmn	r0, #1
	sub	r6, r6, #1
	beq	.L114
.L115:
	cmp	r6, #0
	mov	r0, r4
	beq	.L114
	bl	get_byte
	cmn	r0, #1
	mov	r0, r4
	beq	.L114
	bl	get_byte
	cmn	r0, #1
	mov	r0, r4
	beq	.L114
	bl	get_byte
	cmn	r0, #1
	mov	r0, r4
	beq	.L114
	bl	get_byte
	cmn	r0, #1
	sub	r6, r6, #4
	bne	.L115
	b	.L114
	.size	check_header, .-check_header
	.section	.text.gzsetparams,"ax",%progbits
	.align	2
	.global	gzsetparams
	.hidden	gzsetparams
	.type	gzsetparams, %function
gzsetparams:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	subs	r4, r0, #0
	mov	r6, r1
	mov	r5, r2
	bne	.L161
.L156:
	mvn	r0, #1
.L159:
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L161:
	ldrb	r3, [r4, #92]	@ zero_extendqisi2
	cmp	r3, #119
	bne	.L156
	ldr	r0, [r4, #16]
	cmp	r0, #0
	beq	.L162
.L157:
	mov	r0, r4
	mov	r1, r6
	mov	r2, r5
	bl	deflateParams
	b	.L159
.L162:
	ldr	r0, [r4, #72]
	ldr	r3, [r4, #64]
	mov	r1, #1
	str	r0, [r4, #12]
	mov	r2, #16384
	bl	fwrite
	cmp	r0, #16384
	mvnne	r3, #0
	mov	r1, #16384
	strne	r3, [r4, #56]
	str	r1, [r4, #16]
	b	.L157
	.size	gzsetparams, .-gzsetparams
	.section	.text.gz_open,"ax",%progbits
	.align	2
	.type	gz_open, %function
gz_open:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 80
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	cmp	r0, #0
	cmpne	r1, #0
	sub	sp, sp, #112
	mov	r7, r1
	mov	r4, r0
	movne	r8, #0
	moveq	r8, #1
	mov	r6, r2
	bne	.L189
.L164:
	mov	r0, #0
.L166:
	add	sp, sp, #112
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L189:
	mov	r0, #116
	bl	malloc
	subs	r5, r0, #0
	beq	.L164
	mvn	sl, #0
	mov	r1, r8
	mov	r2, r8
	str	r8, [r5, #32]
	str	r8, [r5, #36]
	str	r8, [r5, #40]
	str	r8, [r5, #68]
	str	r8, [r5, #0]
	str	r8, [r5, #72]
	str	r8, [r5, #12]
	str	r8, [r5, #16]
	str	r8, [r5, #4]
	str	r8, [r5, #64]
	str	r8, [r5, #56]
	str	r8, [r5, #60]
	str	r8, [r5, #100]
	str	r8, [r5, #104]
	str	sl, [r5, #108]
	mov	r0, r8
	bl	crc32
	str	r8, [r5, #80]
	str	r0, [r5, #76]
	str	r8, [r5, #88]
	mov	r0, r4
	bl	strlen
	add	r0, r0, #1
	bl	malloc
	cmp	r0, #0
	mov	r9, r0
	str	r0, [r5, #84]
	beq	.L190
	mov	r1, r4
	bl	strcpy
	add	r9, sp, #32
	strb	r8, [r5, #92]
	mov	ip, r9
	mov	r1, #119
.L176:
	ldrb	r3, [r7, #0]	@ zero_extendqisi2
	cmp	r3, #114
	sub	r2, r3, #48
	streqb	r3, [r5, #92]
	and	r0, r2, #255
	cmp	r3, #119
	cmpne	r3, #97
	streqb	r1, [r5, #92]
	cmp	r0, #9
	movls	sl, r2
	bls	.L170
	cmp	r3, #102
	moveq	r8, #1
	beq	.L170
	cmp	r3, #104
	moveq	r8, #2
	beq	.L170
	cmp	r3, #82
	beq	.L174
	cmp	r3, #0
	strb	r3, [ip], #1
	beq	.L175
.L170:
	add	r3, sp, #112
	cmp	r3, ip
	add	r7, r7, #1
	bne	.L176
.L175:
	ldrb	r7, [r5, #92]	@ zero_extendqisi2
	cmp	r7, #0
	beq	.L191
	cmp	r7, #119
	beq	.L192
	mov	r0, #16384
	bl	malloc
	mvn	r1, #14
	str	r0, [r5, #0]
	str	r0, [r5, #68]
	ldr	r2, .L196
	mov	r0, r5
	mov	r3, #56
	bl	inflateInit2_
	cmp	r0, #0
	bne	.L181
	ldr	r1, [r5, #68]
	cmp	r1, #0
	beq	.L181
.L180:
	mov	r0, #16384
	str	r0, [r5, #16]
	bl	__errno
	mov	r2, #0
	cmp	r6, #0
	str	r2, [r0, #0]
	blt	.L193
	mov	r0, r6
	mov	r1, r9
	bl	fdopen
	mov	r4, r0
.L183:
	cmp	r4, #0
	str	r4, [r5, #64]
	beq	.L194
	ldrb	r3, [r5, #92]	@ zero_extendqisi2
	cmp	r3, #119
	beq	.L195
	mov	r0, r5
	bl	check_header
	ldr	r0, [r5, #64]
	bl	ftell
	ldr	r2, [r5, #4]
	rsb	r1, r2, r0
	str	r1, [r5, #96]
	mov	r0, r5
	b	.L166
.L174:
	mov	r8, #3
	b	.L170
.L192:
	mov	ip, #8
	ldr	r7, .L196
	mov	lr, #56
	mov	r1, sl
	mov	r2, ip
	mvn	r3, #14
	mov	r0, r5
	str	r7, [sp, #8]
	str	lr, [sp, #12]
	str	ip, [sp, #0]
	str	r8, [sp, #4]
	bl	deflateInit2_
	mov	r7, r0
	mov	r0, #16384
	bl	malloc
	cmp	r7, #0
	str	r0, [r5, #72]
	str	r0, [r5, #12]
	bne	.L181
	cmp	r0, #0
	bne	.L180
.L181:
	mov	r0, r5
	bl	destroy
	mov	r0, #0
	b	.L166
.L193:
	mov	r0, r4
	mov	r1, r9
	bl	fopen
	mov	r4, r0
	b	.L183
.L195:
	mov	lr, #8
	mov	ip, #0
	str	lr, [sp, #0]
	mov	r0, r4
	mov	lr, #3
	ldr	r1, .L196+4
	mov	r2, #31
	mov	r3, #139
	str	ip, [sp, #24]
	str	ip, [sp, #4]
	str	ip, [sp, #8]
	str	ip, [sp, #12]
	str	ip, [sp, #16]
	str	ip, [sp, #20]
	str	lr, [sp, #28]
	bl	fprintf
	mov	ip, #10
	str	ip, [r5, #96]
	mov	r0, r5
	b	.L166
.L191:
	mov	r0, r5
	bl	destroy
	mov	r0, r7
	b	.L166
.L190:
	mov	r0, r5
	bl	destroy
	mov	r0, r9
	b	.L166
.L194:
	mov	r0, r5
	bl	destroy
	mov	r0, r4
	b	.L166
.L197:
	.align	2
.L196:
	.word	.LC2
	.word	.LC3
	.size	gz_open, .-gz_open
	.section	.text.gzdopen,"ax",%progbits
	.align	2
	.global	gzdopen
	.hidden	gzdopen
	.type	gzdopen, %function
gzdopen:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 48
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	subs	r4, r0, #0
	sub	sp, sp, #48
	mov	r6, r1
	movlt	r0, #0
	blt	.L200
	ldr	r1, .L202
	mov	r2, r4
	mov	r0, sp
	bl	sprintf
	mov	r0, sp
	mov	r1, r6
	mov	r2, r4
	mov	r5, sp
	bl	gz_open
.L200:
	add	sp, sp, #48
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L203:
	.align	2
.L202:
	.word	.LC4
	.size	gzdopen, .-gzdopen
	.section	.text.gzopen,"ax",%progbits
	.align	2
	.global	gzopen
	.hidden	gzopen
	.type	gzopen, %function
gzopen:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mvn	r2, #0
	b	gz_open
	.size	gzopen, .-gzopen
	.section	.text.gzflush,"ax",%progbits
	.align	2
	.global	gzflush
	.hidden	gzflush
	.type	gzflush, %function
gzflush:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	subs	r4, r0, #0
	mov	r5, r1
	bne	.L223
.L207:
	mvn	r0, #1
.L210:
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L223:
	ldrb	r3, [r4, #92]	@ zero_extendqisi2
	cmp	r3, #119
	bne	.L207
	mov	r7, #0
	str	r7, [r4, #4]
	ldr	r3, [r4, #16]
	mov	r8, #16384
.L218:
	rsbs	r6, r3, #16384
	bne	.L224
.L208:
	cmp	r7, #0
	bne	.L225
	ldr	ip, [r4, #104]
	add	r1, ip, r3
	str	r1, [r4, #104]
	mov	r0, r4
	mov	r1, r5
	bl	deflate
	ldr	r2, [r4, #104]
	ldr	r3, [r4, #16]
	cmp	r6, #0
	rsb	r2, r3, r2
	str	r2, [r4, #104]
	str	r0, [r4, #56]
	bne	.L222
	cmn	r0, #5
	moveq	r2, r6
	streq	r6, [r4, #56]
	moveq	r0, r2
	beq	.L214
.L222:
	mov	r2, r0
.L214:
	cmp	r3, #0
	movne	r7, #1
	bne	.L217
	cmp	r0, #1
	movne	r7, #0
	moveq	r7, #1
.L217:
	cmp	r2, #1
	bls	.L218
.L212:
	cmp	r0, #1
	beq	.L219
	cmp	r0, #0
	bne	.L210
.L219:
	ldr	r0, [r4, #64]
	bl	fflush
	ldr	r0, [r4, #56]
	cmp	r0, #1
	moveq	r0, #0
	b	.L210
.L224:
	ldr	r0, [r4, #72]
	mov	r1, #1
	mov	r2, r6
	ldr	r3, [r4, #64]
	bl	fwrite
	cmp	r6, r0
	bne	.L226
	ldr	r0, [r4, #72]
	str	r8, [r4, #16]
	str	r0, [r4, #12]
	mov	r3, r8
	b	.L208
.L225:
	ldr	r0, [r4, #56]
	b	.L212
.L226:
	mvn	r0, #0
	str	r0, [r4, #56]
	b	.L210
	.size	gzflush, .-gzflush
	.section	.text.gzputs,"ax",%progbits
	.align	2
	.global	gzputs
	.hidden	gzputs
	.type	gzputs, %function
gzputs:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	mov	r4, r0
	mov	r0, r1
	mov	r5, r1
	bl	strlen
	cmp	r4, #0
	mov	r6, r0
	bne	.L236
.L228:
	mvn	r0, #1
.L234:
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L236:
	ldrb	r3, [r4, #92]	@ zero_extendqisi2
	cmp	r3, #119
	streq	r5, [r4, #0]
	streq	r0, [r4, #4]
	moveq	r3, r0
	bne	.L228
.L229:
	cmp	r3, #0
	mov	r1, #1
	mov	r2, #16384
	beq	.L232
	ldr	r0, [r4, #16]
	cmp	r0, #0
	bne	.L230
	ldr	r0, [r4, #72]
	ldr	r3, [r4, #64]
	str	r0, [r4, #12]
	bl	fwrite
	cmp	r0, #16384
	bne	.L237
	str	r0, [r4, #16]
	ldr	r3, [r4, #4]
.L230:
	add	r1, r4, #100
	ldmia	r1, {r1, r2}	@ phole ldm
	add	lr, r1, r3
	add	r0, r2, r0
	str	r0, [r4, #104]
	mov	r1, #0
	str	lr, [r4, #100]
	mov	r0, r4
	bl	deflate
	add	r1, r4, #100
	ldmia	r1, {r1, ip}	@ phole ldm
	ldr	r3, [r4, #4]
	ldr	r2, [r4, #16]
	rsb	r1, r3, r1
	rsb	r2, r2, ip
	cmp	r0, #0
	str	r1, [r4, #100]
	str	r2, [r4, #104]
	str	r0, [r4, #56]
	beq	.L229
.L232:
	mov	r1, r5
	ldr	r0, [r4, #76]
	mov	r2, r6
	bl	crc32
	ldr	r3, [r4, #4]
	str	r0, [r4, #76]
	rsb	r0, r3, r6
	b	.L234
.L237:
	mvn	r3, #0
	str	r3, [r4, #56]
	b	.L232
	.size	gzputs, .-gzputs
	.section	.text.gzputc,"ax",%progbits
	.align	2
	.global	gzputc
	.hidden	gzputc
	.type	gzputc, %function
gzputc:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, lr}
	subs	r4, r0, #0
	sub	sp, sp, #12
	bne	.L247
.L239:
	mvn	r0, #0
.L244:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, lr}
	bx	lr
.L247:
	strb	r1, [sp, #7]
	ldrb	r3, [r4, #92]	@ zero_extendqisi2
	cmp	r3, #119
	bne	.L239
	mov	ip, #1
	add	r5, sp, #7
	stmia	r4, {r5, ip}	@ phole stm
	ldr	r3, [r4, #16]
	b	.L243
.L249:
	cmp	ip, #0
	beq	.L242
.L243:
	cmp	r3, #0
	mov	r1, #1
	mov	r2, #16384
	bne	.L240
	ldr	r0, [r4, #72]
	ldr	r3, [r4, #64]
	str	r0, [r4, #12]
	bl	fwrite
	cmp	r0, #16384
	bne	.L248
	str	r0, [r4, #16]
	ldr	ip, [r4, #4]
	mov	r3, r0
.L240:
	ldr	r2, [r4, #100]
	ldr	r0, [r4, #104]
	add	ip, r2, ip
	add	lr, r0, r3
	str	ip, [r4, #100]
	mov	r1, #0
	str	lr, [r4, #104]
	mov	r0, r4
	bl	deflate
	add	r1, r4, #100
	ldmia	r1, {r1, r2}	@ phole ldm
	ldr	ip, [r4, #4]
	ldr	r3, [r4, #16]
	rsb	r1, ip, r1
	rsb	r2, r3, r2
	cmp	r0, #0
	str	r1, [r4, #100]
	str	r2, [r4, #104]
	str	r0, [r4, #56]
	beq	.L249
.L242:
	mov	r1, r5
	ldr	r0, [r4, #76]
	mov	r2, #1
	bl	crc32
	ldr	r3, [r4, #4]
	str	r0, [r4, #76]
	cmp	r3, #0
	ldreqb	r0, [sp, #7]	@ zero_extendqisi2
	beq	.L244
	b	.L239
.L248:
	mvn	r0, #0
	str	r0, [r4, #56]
	b	.L242
	.size	gzputc, .-gzputc
	.section	.text.getLong,"ax",%progbits
	.align	2
	.type	getLong, %function
getLong:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, lr}
	ldr	r7, [r0, #60]
	cmp	r7, #0
	mov	r4, r0
	bne	.L251
	ldr	r0, [r0, #4]
	cmp	r0, #0
	beq	.L274
.L252:
	sub	r2, r0, #1
	str	r2, [r4, #4]
	ldr	r0, [r4, #0]
	ldrb	r6, [r0], #1	@ zero_extendqisi2
	str	r0, [r4, #0]
.L255:
	cmp	r7, #0
	bne	.L275
	ldr	r0, [r4, #4]
	cmp	r0, #0
	beq	.L276
.L258:
	sub	r0, r0, #1
	ldr	r2, [r4, #0]
	str	r0, [r4, #4]
	ldrb	r1, [r2], #1	@ zero_extendqisi2
	str	r2, [r4, #0]
	mov	r5, r1, asl #8
.L261:
	cmp	r7, #0
	add	r6, r5, r6
	addne	r0, r6, #-33554432
	addne	r0, r0, #16711680
	bne	.L257
	ldr	r0, [r4, #4]
	cmp	r0, #0
	beq	.L277
.L263:
	sub	r1, r0, #1
	ldr	r3, [r4, #0]
	str	r1, [r4, #4]
	ldrb	r5, [r3], #1	@ zero_extendqisi2
	str	r3, [r4, #0]
	mov	r5, r5, asl #16
.L266:
	cmp	r7, #0
	add	r5, r5, r6
	bne	.L267
	ldr	r0, [r4, #4]
	cmp	r0, #0
	beq	.L278
.L268:
	sub	r3, r0, #1
	ldr	ip, [r4, #0]
	str	r3, [r4, #4]
	ldrb	r0, [ip], #1	@ zero_extendqisi2
	str	ip, [r4, #0]
	add	r0, r5, r0, asl #24
.L271:
	ldmfd	sp!, {r3, r4, r5, r6, r7, lr}
	bx	lr
.L251:
	mvn	r0, #16777216
	sub	r2, r0, #65536
	sub	r0, r2, #256
.L257:
	mvn	r1, #2
	str	r1, [r4, #56]
	b	.L271
.L278:
	bl	__errno
	str	r7, [r0, #0]
	mov	r1, #1
	ldr	r0, [r4, #68]
	mov	r2, #16384
	ldr	r3, [r4, #64]
	bl	fread
	cmp	r0, #0
	str	r0, [r4, #4]
	beq	.L279
	ldr	lr, [r4, #68]
	str	lr, [r4, #0]
	b	.L268
.L274:
	bl	__errno
	str	r7, [r0, #0]
	mov	r1, #1
	ldr	r0, [r4, #68]
	mov	r2, #16384
	ldr	r3, [r4, #64]
	bl	fread
	cmp	r0, #0
	str	r0, [r4, #4]
	beq	.L280
	ldr	r1, [r4, #68]
	ldr	r7, [r4, #60]
	str	r1, [r4, #0]
	b	.L252
.L276:
	bl	__errno
	str	r7, [r0, #0]
	mov	r1, #1
	ldr	r0, [r4, #68]
	mov	r2, #16384
	ldr	r3, [r4, #64]
	bl	fread
	cmp	r0, #0
	str	r0, [r4, #4]
	beq	.L281
	ldr	r7, [r4, #68]
	str	r7, [r4, #0]
	ldr	r7, [r4, #60]
	b	.L258
.L277:
	bl	__errno
	str	r7, [r0, #0]
	mov	r1, #1
	ldr	r0, [r4, #68]
	mov	r2, #16384
	ldr	r3, [r4, #64]
	bl	fread
	cmp	r0, #0
	str	r0, [r4, #4]
	beq	.L282
	ldr	ip, [r4, #68]
	ldr	r7, [r4, #60]
	str	ip, [r4, #0]
	b	.L263
.L279:
	mov	r2, #1
	str	r2, [r4, #60]
	ldr	r0, [r4, #64]
	bl	ferror
	cmp	r0, #0
	mvnne	r3, #0
	strne	r3, [r4, #56]
	addne	r0, r5, #-16777216
	bne	.L257
.L267:
	add	r0, r5, #-16777216
	b	.L257
.L282:
	mov	lr, #1
	str	lr, [r4, #60]
	ldr	r0, [r4, #64]
	bl	ferror
	cmp	r0, #0
	mvnne	r3, #0
	mov	r0, #-2147483648
	strne	r3, [r4, #56]
	mov	r5, r0, asr #15
	ldr	r7, [r4, #60]
	b	.L266
.L281:
	mov	r5, #1
	str	r5, [r4, #60]
	ldr	r0, [r4, #64]
	bl	ferror
	cmp	r0, #0
	mvnne	r3, #0
	strne	r3, [r4, #56]
	mvn	r5, #255
	ldr	r7, [r4, #60]
	b	.L261
.L280:
	mov	r3, #1
	str	r3, [r4, #60]
	ldr	r0, [r4, #64]
	bl	ferror
	mvn	r6, #0
	cmp	r0, #0
	strne	r6, [r4, #56]
	ldr	r7, [r4, #60]
	b	.L255
.L275:
	add	r3, r6, #-16777216
	sub	ip, r3, #65536
	sub	r0, ip, #256
	b	.L257
	.size	getLong, .-getLong
	.section	.text.gzread,"ax",%progbits
	.align	2
	.global	gzread
	.hidden	gzread
	.type	gzread, %function
gzread:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, lr}
	subs	r4, r0, #0
	mov	r5, r2
	bne	.L313
.L284:
	mvn	r0, #1
.L287:
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, lr}
	bx	lr
.L313:
	ldrb	r3, [r4, #92]	@ zero_extendqisi2
	cmp	r3, #114
	bne	.L284
	ldr	r3, [r4, #56]
	cmn	r3, #1
	cmnne	r3, #3
	movne	r0, #0
	moveq	r0, #1
	beq	.L285
	cmp	r3, #1
	beq	.L287
	cmp	r2, #0
	mov	r7, r1
	str	r1, [r4, #12]
	str	r2, [r4, #16]
	beq	.L288
	ldr	r3, [r4, #108]
	cmn	r3, #1
	beq	.L288
	strb	r3, [r1, #0]
	ldr	r1, [r4, #112]
	add	r2, r4, #12
	ldmia	r2, {r2, r6}	@ phole ldm
	ldr	r3, [r4, #104]
	cmp	r1, #0
	add	ip, r2, #1
	sub	r6, r6, #1
	add	r1, r3, #1
	mvn	r2, #0
	addeq	r8, r7, #1
	movne	r0, #1
	str	ip, [r4, #12]
	str	r2, [r4, #108]
	str	r1, [r4, #104]
	str	r6, [r4, #16]
	moveq	r7, r8
	strne	r0, [r4, #56]
	bne	.L287
.L290:
	mov	sl, #1
	b	.L302
.L297:
	add	r0, r4, #100
	ldmia	r0, {r0, r1}	@ phole ldm
	ldr	r2, [r4, #16]
	add	r3, r0, r3
	add	lr, r1, r2
	str	r3, [r4, #100]
	mov	r1, #0
	str	lr, [r4, #104]
	mov	r0, r4
	bl	inflate
	ldr	ip, [r4, #100]
	ldr	r1, [r4, #104]
	ldr	r2, [r4, #4]
	ldr	r3, [r4, #16]
	rsb	r2, r2, ip
	cmp	r0, #1
	rsb	ip, r3, r1
	str	r2, [r4, #100]
	str	ip, [r4, #104]
	str	r0, [r4, #56]
	beq	.L314
.L300:
	cmp	r0, #0
	bne	.L312
	ldr	r3, [r4, #60]
	cmp	r3, #0
	bne	.L312
	ldr	r6, [r4, #16]
.L302:
	cmp	r6, #0
	beq	.L312
	ldr	r0, [r4, #88]
	cmp	r0, #0
	bne	.L315
	ldr	r3, [r4, #4]
	cmp	r3, #0
	bne	.L297
	ldr	r6, [r4, #60]
	cmp	r6, #0
	bne	.L297
	bl	__errno
	str	r6, [r0, #0]
	mov	r1, #1
	ldr	r0, [r4, #68]
	mov	r2, #16384
	ldr	r3, [r4, #64]
	bl	fread
	cmp	r0, #0
	str	r0, [r4, #4]
	beq	.L316
.L298:
	ldr	r3, [r4, #68]
	str	r3, [r4, #0]
	ldr	r3, [r4, #4]
	b	.L297
.L312:
	ldr	r3, [r4, #76]
.L299:
	ldr	r2, [r4, #12]
	mov	r0, r3
	mov	r1, r7
	rsb	r2, r7, r2
	bl	crc32
	ldr	r3, [r4, #16]
	cmp	r3, r5
	str	r0, [r4, #76]
	bne	.L304
	ldr	r1, [r4, #56]
	cmn	r1, #1
	cmnne	r1, #3
	bne	.L304
.L285:
	mvn	r0, #0
	b	.L287
.L304:
	rsb	r0, r3, r5
	b	.L287
.L288:
	mov	r8, r1
	mov	r6, r5
	b	.L290
.L314:
	ldr	ip, [r4, #12]
	mov	r1, r7
	rsb	r2, r7, ip
	ldr	r0, [r4, #76]
	bl	crc32
	str	r0, [r4, #76]
	mov	r0, r4
	ldr	r7, [r4, #12]
	bl	getLong
	ldr	r3, [r4, #76]
	cmp	r0, r3
	beq	.L301
	mvn	lr, #2
	str	lr, [r4, #56]
	b	.L299
.L316:
	str	sl, [r4, #60]
	ldr	r0, [r4, #64]
	bl	ferror
	cmp	r0, #0
	beq	.L298
	mvn	r0, #0
	str	r0, [r4, #56]
	ldr	r3, [r4, #76]
	b	.L299
.L315:
	ldr	r7, [r4, #4]
	cmp	r7, r6
	movhi	r7, r6
	bhi	.L293
	cmp	r7, #0
	bne	.L293
.L294:
	mov	r0, r8
	mov	r1, #1
	mov	r2, r6
	ldr	r3, [r4, #64]
	bl	fread
	rsb	r0, r0, r6
	str	r0, [r4, #16]
.L295:
	ldr	ip, [r4, #104]
	rsb	r0, r0, r5
	ldr	r2, [r4, #100]
	add	lr, ip, r0
	cmp	r0, #0
	add	r1, r2, r0
	str	lr, [r4, #104]
	moveq	lr, #1
	str	r1, [r4, #100]
	streq	lr, [r4, #60]
	b	.L287
.L301:
	mov	r0, r4
	bl	getLong
	mov	r0, r4
	bl	check_header
	ldr	r6, [r4, #56]
	cmp	r6, #0
	bne	.L312
	mov	r0, r4
	bl	inflateReset
	mov	r0, r6
	mov	r1, r6
	mov	r2, r6
	bl	crc32
	str	r0, [r4, #76]
	ldr	r0, [r4, #56]
	b	.L300
.L293:
	mov	r2, r7
	ldr	r0, [r4, #12]
	ldr	r1, [r4, #0]
	bl	memcpy
	ldr	r6, [r4, #16]
	ldmia	r4, {r2, r3}	@ phole ldm
	rsb	r6, r7, r6
	rsb	ip, r7, r3
	add	r8, r8, r7
	cmp	r6, #0
	add	r7, r2, r7
	stmia	r4, {r7, ip}	@ phole stm
	str	r8, [r4, #12]
	str	r6, [r4, #16]
	moveq	r0, r6
	beq	.L295
	b	.L294
	.size	gzread, .-gzread
	.section	.text.gzseek,"ax",%progbits
	.align	2
	.global	gzseek
	.hidden	gzseek
	.type	gzseek, %function
gzseek:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	cmpne	r2, #2
	stmfd	sp!, {r4, r5, r6, lr}
	mov	r5, r0
	mov	r4, r1
	bne	.L343
.L318:
	mvn	r4, #0
.L326:
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L343:
	ldr	r3, [r0, #56]
	cmn	r3, #1
	beq	.L318
	cmn	r3, #3
	beq	.L318
	ldrb	r3, [r0, #92]	@ zero_extendqisi2
	cmp	r3, #119
	beq	.L344
	cmp	r2, #1
	ldreq	r3, [r0, #104]
	addeq	r4, r1, r3
	cmp	r4, #0
	blt	.L318
	ldr	r0, [r0, #88]
	cmp	r0, #0
	bne	.L345
	ldr	r3, [r5, #104]
	cmp	r4, r3
	rsbge	r4, r3, r4
	blt	.L346
.L330:
	cmp	r4, #0
	beq	.L331
	ldr	r0, [r5, #72]
	cmp	r0, #0
	beq	.L347
.L332:
	ldr	r3, [r5, #108]
	cmn	r3, #1
	beq	.L333
	ldr	r1, [r5, #104]
	ldr	r2, [r5, #112]
	add	ip, r1, #1
	cmp	r2, #0
	str	ip, [r5, #104]
	mvn	r2, #0
	movne	ip, #1
	str	r2, [r5, #108]
	strne	ip, [r5, #56]
	add	r4, r4, r2
.L333:
	cmp	r4, #0
	ble	.L331
	mov	lr, #16320
	add	r6, lr, #63
	b	.L340
.L348:
	cmp	r4, #0
	ble	.L331
.L340:
	cmp	r4, r6
	movle	r2, r4
	movgt	r2, #16384
	mov	r0, r5
	ldr	r1, [r5, #72]
	bl	gzread
	cmp	r0, #0
	rsb	r4, r0, r4
	bgt	.L348
	b	.L318
.L331:
	ldr	r4, [r5, #104]
	b	.L326
.L344:
	cmp	r2, #0
	ldreq	r3, [r0, #100]
	rsbeq	r4, r3, r1
	cmp	r4, #0
	blt	.L318
	ldr	r6, [r0, #68]
	cmp	r6, #0
	beq	.L349
.L321:
	cmp	r4, #0
	beq	.L322
	mov	r6, #16320
	add	r6, r6, #63
	b	.L339
.L350:
	cmp	r4, #0
	ble	.L322
.L339:
	cmp	r4, r6
	movle	r2, r4
	movgt	r2, #16384
	mov	r0, r5
	ldr	r1, [r5, #68]
	bl	gzwrite
	cmp	r0, #0
	rsb	r4, r0, r4
	bne	.L350
	b	.L318
.L322:
	ldr	r4, [r5, #100]
	b	.L326
.L349:
	mov	r0, #16384
	bl	malloc
	cmp	r0, #0
	str	r0, [r5, #68]
	beq	.L318
	mov	r1, r6
	mov	r2, #16384
	bl	memset
	b	.L321
.L345:
	ldr	ip, [r5, #68]
	mov	r2, #0
	mvn	r1, #0
	str	r1, [r5, #108]
	str	ip, [r5, #0]
	str	r2, [r5, #4]
	ldr	r0, [r5, #64]
	mov	r1, r4
	bl	fseek
	cmp	r0, #0
	strge	r4, [r5, #100]
	strge	r4, [r5, #104]
	bge	.L326
	b	.L318
.L347:
	mov	r0, #16384
	bl	malloc
	cmp	r0, #0
	str	r0, [r5, #72]
	bne	.L332
	b	.L318
.L346:
	mov	r0, r5
	bl	gzrewind
	cmp	r0, #0
	bge	.L330
	b	.L318
	.size	gzseek, .-gzseek
	.section	.text.gzgets,"ax",%progbits
	.align	2
	.global	gzgets
	.hidden	gzgets
	.type	gzgets, %function
gzgets:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r1, #0
	cmpne	r2, #0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	mov	r5, r2
	mov	r6, r0
	mov	r7, r1
	ble	.L352
	sub	r3, r2, #1
	ands	r8, r3, #3
	mov	r4, r1
	beq	.L356
	mov	r2, #1
	mov	r5, r3
	bl	gzread
	cmp	r0, #1
	bne	.L355
	mov	r4, r7
	ldrb	r2, [r4], #1	@ zero_extendqisi2
	cmp	r2, #10
	beq	.L355
	cmp	r8, #1
	beq	.L356
	cmp	r8, #2
	beq	.L377
	mov	r2, r0
	mov	r1, r4
	mov	r0, r6
	bl	gzread
	cmp	r0, #1
	sub	r5, r5, #1
	bne	.L355
	ldrb	ip, [r4], #1	@ zero_extendqisi2
	cmp	ip, #10
	beq	.L355
.L377:
	mov	r0, r6
	mov	r1, r4
	mov	r2, #1
	bl	gzread
	cmp	r0, #1
	sub	r5, r5, #1
	bne	.L355
	ldrb	lr, [r4], #1	@ zero_extendqisi2
	cmp	lr, #10
	beq	.L355
.L356:
	mov	r2, #1
	cmp	r5, r2
	mov	r1, r4
	mov	r0, r6
	beq	.L381
	bl	gzread
	mov	r3, r0
	cmp	r3, #1
	mov	r2, r0
	mov	r0, r6
	bne	.L355
	ldrb	r8, [r4], #1	@ zero_extendqisi2
	cmp	r8, #10
	mov	r1, r4
	sub	r5, r5, #4
	mov	r8, r4
	beq	.L355
	bl	gzread
	mov	r3, r0
	cmp	r3, #1
	mov	r2, r0
	mov	r0, r6
	bne	.L355
	ldrb	ip, [r4], #1	@ zero_extendqisi2
	cmp	ip, #10
	mov	r1, r4
	beq	.L355
	bl	gzread
	mov	r1, r0
	cmp	r1, #1
	mov	r2, r0
	mov	r0, r6
	bne	.L355
	ldrb	lr, [r4, #0]	@ zero_extendqisi2
	add	r4, r8, #2
	cmp	lr, #10
	mov	r1, r4
	beq	.L355
	bl	gzread
	cmp	r0, #1
	bne	.L355
	ldrb	r4, [r8, #2]	@ zero_extendqisi2
	cmp	r4, #10
	add	r4, r8, #3
	bne	.L356
.L355:
	mov	r3, #0
.L354:
	cmp	r4, r7
	orrne	r3, r3, #1
	cmp	r3, #0
	mov	r1, #0
	strb	r1, [r4, #0]
	movne	r0, r7
	bne	.L357
.L352:
	mov	r0, #0
.L357:
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L381:
	mov	r3, r2
	b	.L354
	.size	gzgets, .-gzgets
	.section	.text.gzgetc,"ax",%progbits
	.align	2
	.global	gzgetc
	.hidden	gzgetc
	.type	gzgetc, %function
gzgetc:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
	sub	sp, sp, #12
	add	r1, sp, #7
	mov	r2, #1
	bl	gzread
	cmp	r0, #1
	mvnne	r0, #0
	ldreqb	r0, [sp, #7]	@ zero_extendqisi2
	add	sp, sp, #12
	ldr	lr, [sp], #4
	bx	lr
	.size	gzgetc, .-gzgetc
	.section	.text.gztell,"ax",%progbits
	.align	2
	.global	gztell
	.hidden	gztell
	.type	gztell, %function
gztell:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	subs	r5, r0, #0
	bne	.L392
.L387:
	mvn	r4, #0
.L390:
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L392:
	ldr	r3, [r5, #56]
	cmn	r3, #1
	beq	.L387
	cmn	r3, #3
	beq	.L387
	ldrb	r3, [r5, #92]	@ zero_extendqisi2
	cmp	r3, #119
	beq	.L393
	ldr	r4, [r5, #104]
	cmp	r4, #0
	blt	.L387
	ldr	r0, [r5, #88]
	cmp	r0, #0
	beq	.L390
	ldr	ip, [r5, #68]
	mov	r2, #0
	mvn	lr, #0
	str	lr, [r5, #108]
	str	ip, [r5, #0]
	str	r2, [r5, #4]
	ldr	r0, [r5, #64]
	mov	r1, r4
	bl	fseek
	cmp	r0, #0
	strge	r4, [r5, #100]
	strge	r4, [r5, #104]
	bge	.L390
	b	.L387
.L393:
	ldr	r4, [r5, #68]
	cmp	r4, #0
	ldrne	r4, [r5, #100]
	bne	.L390
.L394:
	mov	r0, #16384
	bl	malloc
	cmp	r0, #0
	str	r0, [r5, #68]
	beq	.L387
	mov	r1, r4
	mov	r2, #16384
	bl	memset
	ldr	r4, [r5, #100]
	b	.L390
	.size	gztell, .-gztell
	.section	.text.gzprintf,"ax",%progbits
	.align	2
	.global	gzprintf
	.hidden	gzprintf
	.type	gzprintf, %function
gzprintf:
	@ Function supports interworking.
	@ args = 4, pretend = 12, frame = 4104
	@ frame_needed = 0, uses_anonymous_args = 1
	stmfd	sp!, {r1, r2, r3}
	stmfd	sp!, {r4, r5, r6, lr}
	sub	sp, sp, #4096
	sub	sp, sp, #12
	add	r1, sp, #4096
	add	ip, r1, #32
	mov	r1, #4096
	add	lr, sp, r1
	ldr	r2, [lr, #28]
	add	r4, sp, #4
	add	lr, sp, r1
	mov	r3, ip
	mov	r6, #0
	mov	r5, r0
	str	ip, [lr, #4]
	mov	r0, r4
	strb	r6, [r4, #4095]
	bl	vsnprintf
	mov	r6, r0
	mov	r0, #4080
	add	r3, r0, #14
	sub	r2, r6, #1
	cmp	r2, r3
	bhi	.L396
	ldrb	r2, [r4, #4095]	@ zero_extendqisi2
	cmp	r2, #0
	beq	.L405
.L396:
	mov	r0, #0
.L402:
	add	sp, sp, #12
	add	sp, sp, #4096
	ldmfd	sp!, {r4, r5, r6, lr}
	add	sp, sp, #12
	bx	lr
.L405:
	cmp	r5, #0
	bne	.L406
.L397:
	mvn	r0, #1
	b	.L402
.L406:
	ldrb	r3, [r5, #92]	@ zero_extendqisi2
	cmp	r3, #119
	bne	.L397
	stmia	r5, {r4, r6}	@ phole stm
	ldr	r3, [r5, #16]
	mov	ip, r6
	b	.L401
.L408:
	cmp	ip, #0
	beq	.L400
.L401:
	cmp	r3, #0
	mov	r1, #1
	mov	r2, #16384
	bne	.L398
	ldr	r0, [r5, #72]
	ldr	r3, [r5, #64]
	str	r0, [r5, #12]
	bl	fwrite
	cmp	r0, #16384
	bne	.L407
	str	r0, [r5, #16]
	ldr	ip, [r5, #4]
	mov	r3, r0
.L398:
	ldr	r2, [r5, #100]
	ldr	r0, [r5, #104]
	add	ip, r2, ip
	add	lr, r0, r3
	str	ip, [r5, #100]
	mov	r1, #0
	str	lr, [r5, #104]
	mov	r0, r5
	bl	deflate
	add	r1, r5, #100
	ldmia	r1, {r1, r2}	@ phole ldm
	ldr	ip, [r5, #4]
	ldr	r3, [r5, #16]
	rsb	r1, ip, r1
	rsb	r2, r3, r2
	cmp	r0, #0
	str	r1, [r5, #100]
	str	r2, [r5, #104]
	str	r0, [r5, #56]
	beq	.L408
.L400:
	ldr	r0, [r5, #76]
	add	r1, sp, #4
	mov	r2, r6
	bl	crc32
	ldr	r3, [r5, #4]
	str	r0, [r5, #76]
	rsb	r0, r3, r6
	b	.L402
.L407:
	mvn	r0, #0
	str	r0, [r5, #56]
	b	.L400
	.size	gzprintf, .-gzprintf
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"\000"
	.space	3
.LC1:
	.ascii	": \000"
	.space	1
.LC2:
	.ascii	"1.2.3\000"
	.space	2
.LC3:
	.ascii	"%c%c%c%c%c%c%c%c%c%c\000"
	.space	3
.LC4:
	.ascii	"<fd:%d>\000"
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
