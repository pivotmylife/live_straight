	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"inffast.c"
	.section	.text.inflate_fast,"ax",%progbits
	.align	2
	.global	inflate_fast
	.hidden	inflate_fast
	.type	inflate_fast, %function
inflate_fast:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 144
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
	ldr	r3, [r0, #28]
	ldr	sl, [r3, #88]
	mov	r6, #1
	mov	r9, r6, asl sl
	ldr	fp, [r3, #84]
	mov	fp, r6, asl fp
	sub	sp, sp, #144
	str	r9, [sp, #40]
	ldr	r8, [r3, #48]
	str	r8, [sp, #44]
	ldr	r7, [r3, #52]
	ldr	r9, [r0, #16]
	ldr	ip, [r0, #0]
	ldr	sl, [r0, #4]
	ldr	r2, [r0, #12]
	str	r7, [sp, #64]
	ldr	r4, [sp, #44]
	ldr	r5, [r3, #40]
	sub	r8, r9, #256
	rsb	r9, r1, r9
	ldr	r1, [sp, #64]
	str	r5, [sp, #72]
	mov	r5, r4, lsr #2
	sub	r7, r1, #1
	mov	r4, r5, asl #2
	str	r5, [sp, #76]
	str	r7, [sp, #68]
	str	r4, [sp, #80]
	ldr	r1, [r3, #44]
	sub	ip, ip, #1
	str	r1, [sp, #100]
	sub	sl, sl, #5
	sub	r2, r2, #1
	sub	r8, r8, #1
	add	sl, ip, sl
	add	r1, r3, #56
	ldmia	r1, {r1, r4}	@ phole ldm
	add	r5, r3, #76
	ldmia	r5, {r5, r7}	@ phole ldm
	add	r9, r2, r9
	str	sl, [sp, #56]
	add	sl, r2, r8
	sub	r8, fp, #1
	str	r9, [sp, #96]
	str	sl, [sp, #52]
	ldr	r9, [sp, #40]
	ldr	sl, [sp, #72]
	str	r8, [sp, #60]
	ldr	r8, [sp, #44]
	sub	fp, r9, #1
	add	r9, r8, sl
	str	fp, [sp, #92]
	str	r9, [sp, #108]
	ldr	fp, [sp, #44]
	ldr	r9, [sp, #68]
	ldr	sl, [sp, #80]
	rsb	r8, fp, #0
	add	r9, r9, sl
	str	r8, [sp, #112]
	str	r9, [sp, #116]
	mov	fp, r3
.L61:
	cmp	r4, #14
	bhi	.L2
	ldrb	r8, [ip, #1]	@ zero_extendqisi2
	add	r1, r1, r8, asl r4
	add	ip, ip, #1
	ldrb	r3, [ip, #1]	@ zero_extendqisi2
	add	r4, r4, #8
	add	r1, r1, r3, asl r4
	add	ip, ip, #1
	add	r4, r4, #8
.L2:
	ldr	sl, [sp, #60]
	and	r3, r1, sl
	b	.L295
.L8:
	tst	r8, #16
	bne	.L297
	tst	r8, #64
	bne	.L259
.L7:
	mov	r3, r6, asl r8
	sub	r8, r3, #1
	and	r3, r8, r1
	add	r3, r3, sl
.L295:
	ldrb	r8, [r5, r3, asl #2]	@ zero_extendqisi2
	add	r3, r5, r3, asl #2
	ldrb	sl, [r3, #1]	@ zero_extendqisi2
	cmp	r8, #0
	rsb	r4, sl, r4
	mov	r1, r1, lsr sl
	ldrh	sl, [r3, #2]
	bne	.L8
	strb	sl, [r2, #1]!
.L9:
	add	r8, sp, #52
	ldmia	r8, {r8, r9}	@ phole ldm
	cmp	r2, r8
	cmpcc	ip, r9
	bcc	.L61
	mov	r3, fp
	b	.L22
.L297:
	ands	r8, r8, #15
	str	sl, [sp, #40]
	beq	.L11
	cmp	r8, r4
	ldrhib	r3, [ip, #1]!	@ zero_extendqisi2
	addhi	r1, r1, r3, asl r4
	mov	r3, r6, asl r8
	sub	r9, r3, #1
	and	r3, r1, r9
	mov	r1, r1, lsr r8
	add	r9, sl, r3
	addhi	r4, r4, #8
	str	r9, [sp, #40]
	rsb	r4, r8, r4
.L11:
	cmp	r4, #14
	bls	.L298
.L13:
	ldr	r9, [sp, #92]
	and	r3, r1, r9
	ldrb	r8, [r7, r3, asl #2]	@ zero_extendqisi2
	add	r3, r7, r3, asl #2
	ldrb	sl, [r3, #1]	@ zero_extendqisi2
	ldrh	r3, [r3, #2]
	tst	r8, #16
	rsb	r4, sl, r4
	mov	r1, r1, lsr sl
	str	r3, [sp, #140]
	bne	.L14
	tst	r8, #64
	bne	.L258
	ldr	r3, [sp, #140]
	b	.L250
.L18:
	tst	r8, #64
	bne	.L258
.L250:
.L17:
	mov	r8, r6, asl r8
	sub	sl, r8, #1
	and	r8, sl, r1
	add	r3, r8, r3
	ldrb	r8, [r7, r3, asl #2]	@ zero_extendqisi2
	add	r3, r7, r3, asl #2
	ldrb	sl, [r3, #1]	@ zero_extendqisi2
	tst	r8, #16
	rsb	r4, sl, r4
	mov	r1, r1, lsr sl
	ldrh	r3, [r3, #2]
	beq	.L18
	str	r3, [sp, #140]
.L14:
	and	r8, r8, #15
	cmp	r8, r4
	bls	.L19
	ldrb	r9, [ip, #1]!	@ zero_extendqisi2
	add	r1, r1, r9, asl r4
	add	r4, r4, #8
	cmp	r8, r4
	ldrhib	r9, [ip, #1]!	@ zero_extendqisi2
	addhi	r1, r1, r9, asl r4
	addhi	r4, r4, #8
.L19:
	mov	sl, r6, asl r8
	sub	r3, sl, #1
	and	sl, r1, r3
	ldr	r9, [sp, #140]
	ldr	r3, [sp, #96]
	str	sl, [sp, #88]
	add	sl, sl, r9
	rsb	r9, r3, r2
	cmp	sl, r9
	str	sl, [sp, #104]
	str	r9, [sp, #48]
	rsb	r4, r8, r4
	mov	r1, r1, lsr r8
	bhi	.L299
	ldr	sl, [sp, #40]
	ldr	r3, .L301
	sub	r8, sl, #3
	umull	sl, r9, r3, r8
	str	r9, [sp, #48]
	ldr	r9, [sp, #104]
	rsb	sl, r9, r2
	ldrb	r3, [sl, #1]	@ zero_extendqisi2
	strb	r3, [r2, #1]
	ldrb	r9, [sl, #2]	@ zero_extendqisi2
	strb	r9, [r2, #2]
	ldr	r3, [sp, #48]
	ldrb	r9, [sl, #3]	@ zero_extendqisi2
	mov	r3, r3, lsr #1
	cmp	r8, #2
	str	r3, [sp, #48]
	strb	r9, [r2, #3]!
	and	r9, r3, #3
	add	r3, sl, #3
	bls	.L255
	cmp	r9, #0
	beq	.L285
	cmp	r9, #1
	beq	.L247
	cmp	r9, #2
	beq	.L248
	ldrb	r8, [r3, #1]	@ zero_extendqisi2
	strb	r8, [r2, #1]
	ldrb	r9, [r3, #2]	@ zero_extendqisi2
	strb	r9, [r2, #2]
	ldrb	r8, [sl, #6]	@ zero_extendqisi2
	strb	r8, [r2, #3]!
	ldr	r3, [sp, #40]
	sub	r8, r3, #6
	add	r3, sl, #6
.L248:
	ldrb	sl, [r3, #1]	@ zero_extendqisi2
	strb	sl, [r2, #1]
	ldrb	r9, [r3, #2]	@ zero_extendqisi2
	strb	r9, [r2, #2]
	ldrb	sl, [r3, #3]!	@ zero_extendqisi2
	strb	sl, [r2, #3]!
	sub	r8, r8, #3
.L247:
	ldrb	r9, [r3, #1]	@ zero_extendqisi2
	strb	r9, [r2, #1]
	ldrb	sl, [r3, #2]	@ zero_extendqisi2
	strb	sl, [r2, #2]
	sub	r8, r8, #3
	ldrb	r9, [r3, #3]!	@ zero_extendqisi2
	cmp	r8, #2
	strb	r9, [r2, #3]!
	bls	.L255
.L285:
	str	r7, [sp, #40]
	mov	r9, r5
.L59:
	ldrb	sl, [r3, #1]	@ zero_extendqisi2
	strb	sl, [r2, #1]
	ldrb	r7, [r3, #2]	@ zero_extendqisi2
	strb	r7, [r2, #2]
	ldrb	r5, [r3, #3]	@ zero_extendqisi2
	strb	r5, [r2, #3]
	add	r7, r3, #3
	ldrb	sl, [r7, #1]	@ zero_extendqisi2
	add	r5, r2, #3
	strb	sl, [r5, #1]
	ldrb	r7, [r7, #2]	@ zero_extendqisi2
	strb	r7, [r5, #2]
	ldrb	r5, [r3, #6]	@ zero_extendqisi2
	strb	r5, [r2, #6]
	add	r7, r3, #6
	ldrb	sl, [r7, #1]	@ zero_extendqisi2
	add	r5, r2, #6
	strb	sl, [r5, #1]
	ldrb	r7, [r7, #2]	@ zero_extendqisi2
	strb	r7, [r5, #2]
	ldrb	r5, [r3, #9]	@ zero_extendqisi2
	strb	r5, [r2, #9]
	add	r7, r3, #9
	ldrb	sl, [r7, #1]	@ zero_extendqisi2
	add	r5, r2, #9
	strb	sl, [r5, #1]
	ldrb	r7, [r7, #2]	@ zero_extendqisi2
	strb	r7, [r5, #2]
	sub	r8, r8, #12
	ldrb	r5, [r3, #12]!	@ zero_extendqisi2
	cmp	r8, #2
	strb	r5, [r2, #12]!
	bhi	.L59
	ldr	r7, [sp, #40]
	mov	r5, r9
.L255:
	cmp	r8, #0
	beq	.L9
	ldrb	r9, [r3, #1]	@ zero_extendqisi2
	cmp	r8, #2
	strb	r9, [r2, #1]!
	add	r3, r3, #1
	ldreqb	r8, [r3, #1]	@ zero_extendqisi2
	streqb	r8, [r2, #1]!
	b	.L9
.L259:
	tst	r8, #32
	ldreq	r5, .L301+4
	movne	r5, #11
	streq	r5, [r0, #24]
	moveq	r5, #27
	str	r5, [fp, #0]
	mov	r3, fp
.L22:
	mov	r5, r4, lsr #3
	sub	r4, r4, r5, asl #3
	mvn	r6, #0
	bic	r1, r1, r6, asl r4
	ldr	sl, [sp, #52]
	ldr	r6, [sp, #56]
	add	r8, sl, #256
	rsb	ip, r5, ip
	add	r8, r8, #1
	add	r5, r6, #5
	rsb	r5, ip, r5
	rsb	r8, r2, r8
	add	ip, ip, #1
	add	r2, r2, #1
	str	r8, [r0, #16]
	str	ip, [r0, #0]
	str	r2, [r0, #12]
	str	r5, [r0, #4]
	str	r4, [r3, #60]
	str	r1, [r3, #56]
	add	sp, sp, #144
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
	bx	lr
.L298:
	ldrb	sl, [ip, #1]	@ zero_extendqisi2
	add	r1, r1, sl, asl r4
	add	ip, ip, #1
	ldrb	r8, [ip, #1]	@ zero_extendqisi2
	add	r4, r4, #8
	add	r1, r1, r8, asl r4
	add	ip, ip, #1
	add	r4, r4, #8
	b	.L13
.L258:
	ldr	r5, .L301+8
	mov	r6, #27
	str	r5, [r0, #24]
	mov	r3, fp
	str	r6, [fp, #0]
	b	.L22
.L299:
	ldr	r8, [sp, #100]
	rsb	r3, r9, sl
	cmp	r8, r3
	str	r3, [sp, #124]
	bcc	.L300
	ldr	r8, [sp, #44]
	ldr	r9, [sp, #68]
	cmp	r8, #0
	str	r9, [sp, #4]
	bne	.L23
	ldr	r9, [sp, #40]
	ldr	r3, [sp, #124]
	cmp	r9, r3
	ldr	r9, [sp, #72]
	ldr	sl, [sp, #68]
	rsb	r8, r3, r9
	add	r8, sl, r8
	str	r8, [sp, #4]
	bls	.L24
	ldr	r8, [sp, #48]
	add	r3, r8, r9
	ldr	r9, [sp, #140]
	ldr	sl, [sp, #88]
	rsb	r3, r9, r3
	ldr	r8, [sp, #64]
	rsb	r9, sl, r3
	add	r3, r8, r9
	add	r9, r2, #1
	orr	sl, r3, r9
	str	r3, [sp, #88]
	tst	sl, #3
	ldr	r3, [sp, #88]
	movne	r8, #0
	moveq	r8, #1
	ldr	sl, [sp, #88]
	str	r8, [sp, #48]
	add	r3, r3, #4
	add	r8, r2, #5
	cmp	sl, r8
	cmpls	r9, r3
	ldr	r3, [sp, #48]
	ldr	r8, [sp, #124]
	movls	sl, #0
	movhi	sl, #1
	cmp	r8, #3
	movls	r3, #0
	andhi	r3, r3, #1
	tst	r3, sl
	str	r9, [sp, #84]
	str	sl, [sp, #120]
	beq	.L25
	mov	r9, r8, lsr #2
	movs	sl, r9, asl #2
	str	sl, [sp, #120]
	str	r9, [sp, #84]
	moveq	r3, r8
	streq	r2, [sp, #48]
	moveq	sl, r2
	beq	.L27
	ldr	r8, [sp, #88]
	ldr	r3, [sp, #44]
	ldr	sl, [r8, r3]
	str	sl, [sp, #36]
	ldr	r8, [sp, #36]
	mov	sl, #1
	sub	r3, r9, #1
	cmp	sl, r9
	add	r9, r2, #4
	str	r8, [r2, #1]
	and	r3, r3, #3
	str	r9, [sp, #48]
	bcs	.L287
	cmp	r3, #0
	beq	.L266
	cmp	r3, #1
	beq	.L229
	cmp	r3, #2
	beq	.L230
	ldr	r8, [sp, #88]
	ldr	r9, [sp, #48]
	ldr	r3, [r8, sl, asl #2]
	add	r8, r9, #4
	str	r3, [r9, #1]
	str	r8, [sp, #48]
	add	sl, sl, #1
.L230:
	ldr	r9, [sp, #88]
	ldr	r8, [sp, #48]
	ldr	r3, [r9, sl, asl #2]
	add	r9, r8, #4
	str	r3, [r8, #1]
	str	r9, [sp, #48]
	add	sl, sl, #1
.L229:
	add	r8, sp, #84
	ldmia	r8, {r8, r9}	@ phole ldm
	ldr	r3, [r9, sl, asl #2]
	ldr	r9, [sp, #48]
	add	sl, sl, #1
	cmp	sl, r8
	add	r8, r9, #4
	str	r3, [r9, #1]
	str	r8, [sp, #48]
	bcs	.L287
	ldr	r3, [sp, #4]
	str	r3, [sp, #128]
.L286:
	str	r1, [sp, #132]
	ldr	r9, [sp, #124]
	ldr	r3, [sp, #88]
	str	r2, [sp, #136]
	ldr	r1, [sp, #48]
.L28:
	ldr	r2, [r3, sl, asl #2]
	str	r2, [r1, #1]
	add	r8, sl, #1
	add	r2, r1, #4
	ldr	r1, [r3, r8, asl #2]
	str	r1, [r2, #1]
	add	r8, r8, #1
	ldr	r1, [r3, r8, asl #2]
	str	r1, [r2, #5]
	ldr	r2, [sp, #84]
	add	r8, sl, #3
	add	sl, sl, #4
	ldr	r1, [r3, r8, asl #2]
	cmp	sl, r2
	ldr	r2, [sp, #48]
	str	r1, [r2, #13]
	add	r1, r2, #16
	str	r1, [sp, #48]
	bcc	.L28
	ldr	sl, [sp, #128]
	str	r9, [sp, #124]
	str	sl, [sp, #4]
	add	r1, sp, #132
	ldmia	r1, {r1, r2}	@ phole ldm
	mov	r3, r9
.L251:
	ldr	r8, [sp, #120]
	ldr	sl, [sp, #4]
	cmp	r3, r8
	add	r9, sl, r8
	add	sl, r2, r8
	str	r9, [sp, #4]
	str	sl, [sp, #48]
	rsb	r3, r8, r3
	beq	.L55
.L27:
	ldr	r8, [sp, #4]
	ldrb	r9, [r8, #1]!	@ zero_extendqisi2
	sub	r3, r3, #1
	str	r9, [sp, #88]
	subs	r9, r3, #0
	str	r9, [sp, #48]
	ldr	r9, [sp, #88]
	and	r3, r3, #3
	strb	r9, [sl, #1]!
	beq	.L55
	cmp	r3, #0
	beq	.L264
	cmp	r3, #1
	beq	.L227
	cmp	r3, #2
	ldrneb	r3, [r8, #1]!	@ zero_extendqisi2
	strneb	r3, [sl, #1]!
	ldrne	r3, [sp, #48]
	subne	r3, r3, #1
	strne	r3, [sp, #48]
	ldrb	r3, [r8, #1]!	@ zero_extendqisi2
	strb	r3, [sl, #1]!
	ldr	r9, [sp, #48]
	sub	r3, r9, #1
	str	r3, [sp, #48]
.L227:
	ldr	r9, [sp, #48]
	ldrb	r3, [r8, #1]!	@ zero_extendqisi2
	subs	r9, r9, #1
	str	r9, [sp, #48]
	strb	r3, [sl, #1]!
	beq	.L55
	ldr	r9, [sp, #124]
	str	r2, [sp, #120]
	str	r9, [sp, #88]
	mov	r9, r7
.L30:
	mov	r7, r8
	ldrb	r2, [r7, #1]!	@ zero_extendqisi2
	mov	r3, sl
	strb	r2, [r3, #1]!
	ldrb	r2, [r7, #1]!	@ zero_extendqisi2
	strb	r2, [r3, #1]!
	ldrb	r7, [r7, #1]	@ zero_extendqisi2
	strb	r7, [r3, #1]
	ldr	r3, [sp, #48]
	ldrb	r7, [r8, #4]	@ zero_extendqisi2
	subs	r2, r3, #4
	str	r2, [sp, #48]
	add	r8, r8, #4
	strb	r7, [sl, #4]
	add	sl, sl, #4
	bne	.L30
.L290:
	ldr	r8, [sp, #88]
	ldr	r2, [sp, #120]
	str	r8, [sp, #124]
	mov	r7, r9
.L55:
	ldr	r9, [sp, #40]
	ldr	r8, [sp, #124]
	rsb	sl, r8, r9
	ldr	r9, [sp, #104]
	add	r3, r2, r8
	str	sl, [sp, #40]
	rsb	sl, r9, r3
	str	r3, [sp, #128]
	str	sl, [sp, #4]
	b	.L32
.L24:
	str	r2, [sp, #128]
.L32:
	ldr	r2, [sp, #40]
	cmp	r2, #2
	bls	.L291
	ldr	r9, .L301
	sub	r3, r2, #3
	umull	r8, r9, r3, r9
	ldr	sl, [sp, #4]
	ldr	r8, [sp, #128]
	ldrb	r2, [sl, #1]	@ zero_extendqisi2
	strb	r2, [r8, #1]
	ldrb	r2, [sl, #2]	@ zero_extendqisi2
	strb	r2, [r8, #2]
	str	r3, [sp, #40]
	ldrb	r2, [sl, #3]!	@ zero_extendqisi2
	str	sl, [sp, #4]
	ldr	sl, [sp, #40]
	strb	r2, [r8, #3]!
	mov	r3, r9, lsr #1
	cmp	sl, #2
	str	r8, [sp, #128]
	and	r3, r3, #3
	bls	.L291
	cmp	r3, #0
	beq	.L260
	cmp	r3, #1
	beq	.L223
	cmp	r3, #2
	beq	.L224
	ldr	r8, [sp, #4]
	ldr	sl, [sp, #128]
	ldrb	r3, [r8, #1]	@ zero_extendqisi2
	strb	r3, [sl, #1]
	ldrb	r9, [r8, #2]	@ zero_extendqisi2
	strb	r9, [sl, #2]
	ldrb	r2, [r8, #3]!	@ zero_extendqisi2
	str	r8, [sp, #4]
	strb	r2, [sl, #3]!
	ldr	r3, [sp, #40]
	sub	r9, r3, #3
	str	sl, [sp, #128]
	str	r9, [sp, #40]
.L224:
	ldr	sl, [sp, #4]
	ldr	r8, [sp, #128]
	ldrb	r2, [sl, #1]	@ zero_extendqisi2
	strb	r2, [r8, #1]
	ldrb	r3, [sl, #2]	@ zero_extendqisi2
	strb	r3, [r8, #2]
	ldrb	r9, [sl, #3]!	@ zero_extendqisi2
	str	sl, [sp, #4]
	strb	r9, [r8, #3]!
	ldr	r2, [sp, #40]
	sub	sl, r2, #3
	str	r8, [sp, #128]
	str	sl, [sp, #40]
.L223:
	ldr	sl, [sp, #4]
	ldr	r2, [sp, #128]
	ldrb	r3, [sl, #1]	@ zero_extendqisi2
	strb	r3, [r2, #1]
	ldrb	r8, [sl, #2]	@ zero_extendqisi2
	strb	r8, [r2, #2]
	ldr	r9, [sp, #40]
	sub	r3, r9, #3
	str	r3, [sp, #40]
	ldrb	r9, [sl, #3]!	@ zero_extendqisi2
	ldr	r8, [sp, #40]
	str	sl, [sp, #4]
	strb	r9, [r2, #3]!
	cmp	r8, #2
	str	r2, [sp, #128]
	bls	.L291
	ldr	r8, [sp, #4]
	ldr	r3, [sp, #40]
	str	r7, [sp, #104]
	mov	sl, r2
	mov	r9, r5
.L63:
	ldrb	r7, [r8, #1]	@ zero_extendqisi2
	strb	r7, [sl, #1]
	ldrb	r5, [r8, #2]	@ zero_extendqisi2
	strb	r5, [sl, #2]
	ldrb	r2, [r8, #3]	@ zero_extendqisi2
	strb	r2, [sl, #3]
	add	r5, r8, #3
	ldrb	r7, [r5, #1]	@ zero_extendqisi2
	add	r2, sl, #3
	strb	r7, [r2, #1]
	ldrb	r5, [r5, #2]	@ zero_extendqisi2
	strb	r5, [r2, #2]
	ldrb	r2, [r8, #6]	@ zero_extendqisi2
	strb	r2, [sl, #6]
	add	r5, r8, #6
	ldrb	r7, [r5, #1]	@ zero_extendqisi2
	add	r2, sl, #6
	strb	r7, [r2, #1]
	ldrb	r5, [r5, #2]	@ zero_extendqisi2
	strb	r5, [r2, #2]
	ldrb	r2, [r8, #9]	@ zero_extendqisi2
	strb	r2, [sl, #9]
	add	r5, r8, #9
	ldrb	r7, [r5, #1]	@ zero_extendqisi2
	add	r2, sl, #9
	strb	r7, [r2, #1]
	ldrb	r5, [r5, #2]	@ zero_extendqisi2
	strb	r5, [r2, #2]
	sub	r3, r3, #12
	ldrb	r2, [r8, #12]!	@ zero_extendqisi2
	cmp	r3, #2
	strb	r2, [sl, #12]!
	bhi	.L63
	str	r8, [sp, #4]
	str	r3, [sp, #40]
	ldr	r7, [sp, #104]
	str	sl, [sp, #128]
	mov	r5, r9
	mov	sl, r3
.L34:
	cmp	sl, #0
	ldreq	r2, [sp, #128]
	beq	.L9
	ldr	r2, [sp, #4]
	ldr	r8, [sp, #128]
	ldrb	r3, [r2, #1]	@ zero_extendqisi2
	strb	r3, [r8, #1]
	ldr	r9, [sp, #4]
	cmp	sl, #2
	add	r2, r8, #1
	add	r8, r9, #1
	ldreqb	sl, [r8, #1]	@ zero_extendqisi2
	streqb	sl, [r2, #1]!
	b	.L9
.L23:
	mov	r3, r8
	ldr	r8, [sp, #124]
	cmp	r3, r8
	bcs	.L35
	rsb	r9, r3, r8
	ldr	sl, [sp, #40]
	ldr	r3, [sp, #108]
	cmp	sl, r9
	str	r9, [sp, #128]
	rsb	r9, r8, r3
	ldr	r8, [sp, #68]
	add	sl, r8, r9
	str	sl, [sp, #132]
	strls	sl, [sp, #4]
	strls	r2, [sp, #128]
	bls	.L32
	ldr	r9, [sp, #48]
	ldr	sl, [sp, #108]
	ldr	r8, [sp, #140]
	add	r3, r9, sl
	ldr	r9, [sp, #88]
	rsb	sl, r8, r3
	ldr	r3, [sp, #64]
	rsb	r8, r9, sl
	add	sl, r3, r8
	add	r8, r2, #1
	orr	r9, sl, r8
	tst	r9, #3
	str	sl, [sp, #120]
	movne	r3, #0
	moveq	r3, #1
	ldr	r9, [sp, #120]
	add	sl, sl, #4
	str	r3, [sp, #84]
	add	r3, r2, #5
	cmp	r9, r3
	cmpls	r8, sl
	str	sl, [sp, #20]
	ldr	r3, [sp, #128]
	ldr	sl, [sp, #84]
	ldr	r9, [sp, #40]
	str	r8, [sp, #124]
	movls	r8, #0
	movhi	r8, #1
	cmp	r3, #3
	movls	sl, #0
	andhi	sl, sl, #1
	str	r8, [sp, #136]
	tst	sl, r8
	rsb	r8, r3, r9
	str	r8, [sp, #40]
	beq	.L37
	mov	r9, r3, lsr #2
	movs	r8, r9, asl #2
	str	r9, [sp, #124]
	str	r8, [sp, #136]
	streq	r2, [sp, #84]
	beq	.L39
	ldr	r3, [sp, #120]
	ldr	r9, [r3, #0]
	ldr	sl, [sp, #124]
	str	r9, [sp, #32]
	ldr	r8, [sp, #32]
	mov	r9, #1
	sub	r3, sl, #1
	cmp	r9, sl
	add	sl, r2, #4
	str	r8, [r2, #1]
	and	r3, r3, #3
	str	sl, [sp, #84]
	bcs	.L252
	cmp	r3, #0
	beq	.L277
	cmp	r3, #1
	beq	.L239
	cmp	r3, #2
	beq	.L240
	ldr	r3, [sp, #120]
	ldr	r8, [sp, #84]
	ldr	sl, [r3, r9, asl #2]
	str	sl, [r8, #1]
	add	sl, r8, #4
	str	sl, [sp, #84]
	add	r9, r9, #1
.L240:
	ldr	r8, [sp, #120]
	ldr	r3, [sp, #84]
	ldr	sl, [r8, r9, asl #2]
	add	r8, r3, #4
	str	sl, [r3, #1]
	str	r8, [sp, #84]
	add	r9, r9, #1
.L239:
	ldr	r8, [sp, #120]
	ldr	r3, [sp, #124]
	ldr	sl, [r8, r9, asl #2]
	ldr	r8, [sp, #84]
	add	r9, r9, #1
	cmp	r9, r3
	add	r3, r8, #4
	str	sl, [r8, #1]
	str	r3, [sp, #84]
	bcs	.L252
.L277:
	str	r1, [sp, #20]
	ldr	sl, [sp, #4]
	ldr	r3, [sp, #120]
	str	r2, [sp, #16]
	ldr	r1, [sp, #84]
.L40:
	ldr	r2, [r3, r9, asl #2]
	str	r2, [r1, #1]
	add	r8, r9, #1
	add	r2, r1, #4
	ldr	r1, [r3, r8, asl #2]
	str	r1, [r2, #1]
	add	r1, r8, #1
	ldr	r8, [r3, r1, asl #2]
	str	r8, [r2, #5]
	add	r1, r9, #3
	ldr	r8, [r3, r1, asl #2]
	ldr	r2, [sp, #124]
	ldr	r1, [sp, #84]
	add	r9, r9, #4
	str	r8, [r1, #13]
	cmp	r9, r2
	add	r1, r1, #16
	str	r1, [sp, #84]
	bcc	.L40
	str	sl, [sp, #4]
	ldr	r1, [sp, #20]
	ldr	r2, [sp, #16]
.L252:
	ldr	r8, [sp, #136]
	add	r3, sp, #128
	ldmia	r3, {r3, sl}	@ phole ldm
	add	r9, sl, r8
	cmp	r3, r8
	add	sl, r2, r8
	str	r9, [sp, #132]
	str	sl, [sp, #84]
	rsb	r3, r8, r3
	beq	.L41
.L39:
	ldr	r9, [sp, #132]
	ldrb	r8, [r9, #1]!	@ zero_extendqisi2
	sub	r3, r3, #1
	str	r9, [sp, #132]
	ldr	r9, [sp, #84]
	subs	sl, r3, #0
	str	sl, [sp, #120]
	strb	r8, [r9, #1]!
	and	sl, r3, #3
	str	r9, [sp, #84]
	beq	.L41
	cmp	sl, #0
	beq	.L274
	cmp	sl, #1
	beq	.L237
	cmp	sl, #2
	beq	.L238
	ldr	r9, [sp, #132]
	ldr	r3, [sp, #84]
	ldrb	sl, [r9, #1]!	@ zero_extendqisi2
	str	r9, [sp, #132]
	strb	sl, [r3, #1]!
	ldr	r8, [sp, #120]
	sub	r9, r8, #1
	str	r3, [sp, #84]
	str	r9, [sp, #120]
.L238:
	ldr	r8, [sp, #132]
	ldr	r9, [sp, #84]
	ldrb	sl, [r8, #1]!	@ zero_extendqisi2
	str	r8, [sp, #132]
	strb	sl, [r9, #1]!
	ldr	r3, [sp, #120]
	sub	r8, r3, #1
	str	r9, [sp, #84]
	str	r8, [sp, #120]
.L237:
	ldr	r3, [sp, #132]
	ldrb	sl, [r3, #1]!	@ zero_extendqisi2
	ldr	r8, [sp, #120]
	str	r3, [sp, #132]
	ldr	r3, [sp, #84]
	subs	r9, r8, #1
	str	r9, [sp, #120]
	strb	sl, [r3, #1]!
	str	r3, [sp, #84]
	beq	.L41
	ldr	r8, [sp, #140]
	ldr	sl, [sp, #4]
	str	r8, [sp, #124]
	str	r2, [sp, #136]
	ldr	r9, [sp, #132]
.L42:
	mov	r8, r9
	ldrb	r2, [r8, #1]!	@ zero_extendqisi2
	ldr	r3, [sp, #84]
	strb	r2, [r3, #1]!
	ldrb	r2, [r8, #1]!	@ zero_extendqisi2
	strb	r2, [r3, #1]!
	ldrb	r2, [r8, #1]	@ zero_extendqisi2
	strb	r2, [r3, #1]
	ldr	r8, [sp, #120]
	subs	r2, r8, #4
	ldr	r8, [sp, #84]
	ldrb	r3, [r9, #4]	@ zero_extendqisi2
	str	r2, [sp, #120]
	add	r2, r8, #4
	strb	r3, [r8, #4]
	add	r9, r9, #4
	str	r2, [sp, #84]
	bne	.L42
.L289:
	ldr	r9, [sp, #124]
	str	sl, [sp, #4]
	str	r9, [sp, #140]
	ldr	r2, [sp, #136]
.L41:
	ldr	r3, [sp, #128]
	ldr	r8, [sp, #44]
	ldr	r9, [sp, #40]
	add	sl, r2, r3
	cmp	r8, r9
	str	sl, [sp, #128]
	bcs	.L32
	ldr	sl, [sp, #48]
	ldr	r9, [sp, #112]
	ldr	r8, [sp, #140]
	rsb	r3, sl, r9
	add	sl, r3, r8
	ldr	r3, [sp, #88]
	add	r9, sl, r3
	add	r2, r2, r9
	add	r8, r2, #1
	str	r2, [sp, #124]
	ldr	r2, [sp, #64]
	str	r8, [sp, #84]
	ldr	r9, [sp, #124]
	orr	r3, r8, r2
	ldr	sl, [sp, #84]
	tst	r3, #3
	add	r8, r2, #4
	add	r9, r9, #5
	movne	r3, #0
	moveq	r3, #1
	cmp	r2, r9
	cmpls	sl, r8
	ldr	r2, [sp, #44]
	movls	sl, #0
	movhi	sl, #1
	str	r3, [sp, #48]
	cmp	r2, #3
	movls	r3, #0
	andhi	r3, r3, #1
	ands	r3, r3, sl
	str	r9, [sp, #120]
	str	sl, [sp, #84]
	beq	.L44
	ldr	r3, [sp, #80]
	cmp	r3, #0
	moveq	r3, r2
	ldreq	r2, [sp, #128]
	beq	.L46
	ldr	r3, [sp, #64]
	ldr	r8, [sp, #124]
	ldr	sl, [r3, #0]
	ldr	r9, [sp, #76]
	mov	r3, #1
	str	sl, [r8, #1]
	sub	r2, r9, #1
	cmp	r3, r9
	ldr	r9, [sp, #124]
	and	r2, r2, #3
	add	r8, r9, #4
	bcs	.L253
	cmp	r2, #0
	beq	.L271
	cmp	r2, #1
	beq	.L233
	cmp	r2, #2
	ldrne	r9, [sp, #64]
	ldrne	r3, [r9, #4]
	strne	r3, [r8, #1]
	ldr	r9, [sp, #64]
	movne	r3, #2
	ldr	r2, [r9, r3, asl #2]
	addne	r8, r8, #4
	str	r2, [r8, #1]
	add	r3, r3, #1
	add	r8, r8, #4
.L233:
	ldr	r9, [sp, #64]
	ldr	sl, [sp, #76]
	ldr	r2, [r9, r3, asl #2]
	add	r3, r3, #1
	cmp	r3, sl
	str	r2, [r8, #1]
	add	r8, r8, #4
	bcs	.L253
.L271:
	ldr	r9, [sp, #128]
	str	r1, [sp, #84]
	ldr	r2, [sp, #64]
	str	r0, [sp, #88]
.L47:
	ldr	r0, [r2, r3, asl #2]
	str	r0, [r8, #1]
	add	sl, r3, #1
	ldr	r1, [r2, sl, asl #2]
	add	r0, r8, #4
	str	r1, [r0, #1]
	add	sl, sl, #1
	ldr	r1, [r2, sl, asl #2]
	str	r1, [r0, #5]
	add	sl, r3, #3
	ldr	r0, [sp, #76]
	ldr	r1, [r2, sl, asl #2]
	add	r3, r3, #4
	cmp	r3, r0
	str	r1, [r8, #13]
	add	r8, r8, #16
	bcc	.L47
	str	r9, [sp, #128]
	ldr	r1, [sp, #84]
	ldr	r0, [sp, #88]
.L253:
	ldr	r3, [sp, #80]
	ldr	r2, [sp, #44]
	ldr	sl, [sp, #128]
	ldr	r9, [sp, #44]
	ldr	r8, [sp, #116]
	cmp	r2, r3
	str	r8, [sp, #4]
	add	r2, sl, r3
	rsb	r3, r3, r9
	beq	.L48
.L46:
	sub	r9, r3, #1
	str	r9, [sp, #84]
	ldr	r8, [sp, #4]
	ldrb	r3, [r8, #1]!	@ zero_extendqisi2
	ldr	r9, [sp, #84]
	strb	r3, [r2, #1]!
	ldr	sl, [sp, #84]
	cmp	r9, #0
	and	r3, sl, #3
	beq	.L48
	cmp	r3, #0
	beq	.L268
	cmp	r3, #1
	beq	.L231
	cmp	r3, #2
	ldrneb	r3, [r8, #1]!	@ zero_extendqisi2
	strneb	r3, [r2, #1]!
	ldrb	sl, [r8, #1]!	@ zero_extendqisi2
	subne	r9, r9, #1
	strb	sl, [r2, #1]!
	sub	r9, r9, #1
.L231:
	ldrb	r3, [r8, #1]!	@ zero_extendqisi2
	subs	r9, r9, #1
	strb	r3, [r2, #1]!
	beq	.L48
	ldr	r3, [sp, #128]
	str	r1, [sp, #84]
	str	r3, [sp, #48]
	mov	sl, r7
.L49:
	mov	r7, r8
	ldrb	r1, [r7, #1]!	@ zero_extendqisi2
	mov	r3, r2
	strb	r1, [r3, #1]!
	ldrb	r1, [r7, #1]!	@ zero_extendqisi2
	strb	r1, [r3, #1]!
	ldrb	r1, [r7, #1]	@ zero_extendqisi2
	strb	r1, [r3, #1]
	ldrb	r7, [r8, #4]	@ zero_extendqisi2
	subs	r9, r9, #4
	strb	r7, [r2, #4]
	add	r8, r8, #4
	add	r2, r2, #4
	bne	.L49
	ldr	r8, [sp, #48]
	ldr	r1, [sp, #84]
	str	r8, [sp, #128]
	mov	r7, sl
.L48:
	ldr	r8, [sp, #128]
	ldr	r2, [sp, #44]
	ldr	r9, [sp, #40]
	add	sl, r8, r2
	ldr	r8, [sp, #104]
	rsb	r3, r2, r9
	rsb	r2, r8, sl
	str	sl, [sp, #128]
	str	r3, [sp, #40]
	str	r2, [sp, #4]
	b	.L32
.L302:
	.align	2
.L301:
	.word	-1431655765
	.word	.LC2
	.word	.LC1
	.word	.LC0
.L35:
	mov	sl, r8
	ldr	r8, [sp, #40]
	ldr	r9, [sp, #68]
	cmp	r8, sl
	rsb	r8, sl, r3
	add	sl, r9, r8
	str	sl, [sp, #4]
	bls	.L24
	ldr	r8, [sp, #48]
	ldr	r9, [sp, #140]
	add	sl, r8, r3
	rsb	r3, r9, sl
	ldr	sl, [sp, #88]
	ldr	r8, [sp, #64]
	rsb	r9, sl, r3
	add	r3, r8, r9
	add	r9, r2, #1
	orr	sl, r3, r9
	str	r3, [sp, #88]
	tst	sl, #3
	ldr	r3, [sp, #88]
	movne	r8, #0
	moveq	r8, #1
	ldr	sl, [sp, #88]
	str	r8, [sp, #48]
	add	r3, r3, #4
	add	r8, r2, #5
	cmp	sl, r8
	cmpls	r9, r3
	ldr	r3, [sp, #48]
	ldr	r8, [sp, #124]
	str	r9, [sp, #84]
	movls	r9, #0
	movhi	r9, #1
	cmp	r8, #3
	movls	r3, #0
	andhi	r3, r3, #1
	tst	r3, r9
	str	r9, [sp, #120]
	beq	.L51
	mov	r9, r8, lsr #2
	movs	sl, r9, asl #2
	str	r9, [sp, #84]
	str	sl, [sp, #120]
	moveq	r3, r8
	streq	r2, [sp, #48]
	beq	.L53
	ldr	r3, [sp, #88]
	ldr	sl, [r3, #0]
	ldr	r8, [sp, #84]
	str	sl, [sp, #28]
	ldr	r9, [sp, #28]
	mov	sl, #1
	sub	r3, r8, #1
	cmp	sl, r8
	add	r8, r2, #4
	str	r9, [r2, #1]
	and	r3, r3, #3
	str	r8, [sp, #48]
	bcs	.L254
	cmp	r3, #0
	beq	.L282
	cmp	r3, #1
	beq	.L245
	cmp	r3, #2
	beq	.L246
	ldr	r8, [sp, #88]
	ldr	r9, [sp, #48]
	ldr	r3, [r8, sl, asl #2]
	add	r8, r9, #4
	str	r3, [r9, #1]
	str	r8, [sp, #48]
	add	sl, sl, #1
.L246:
	ldr	r9, [sp, #88]
	ldr	r8, [sp, #48]
	ldr	r3, [r9, sl, asl #2]
	add	r9, r8, #4
	str	r3, [r8, #1]
	str	r9, [sp, #48]
	add	sl, sl, #1
.L245:
	add	r8, sp, #84
	ldmia	r8, {r8, r9}	@ phole ldm
	ldr	r3, [r9, sl, asl #2]
	ldr	r9, [sp, #48]
	add	sl, sl, #1
	cmp	sl, r8
	add	r8, r9, #4
	str	r3, [r9, #1]
	str	r8, [sp, #48]
	bcs	.L254
	ldr	r8, [sp, #4]
	ldr	r9, [sp, #124]
	str	r8, [sp, #128]
	ldr	r3, [sp, #88]
	str	r1, [sp, #132]
	str	r2, [sp, #136]
.L54:
	ldr	r2, [r3, sl, asl #2]
	ldr	r8, [sp, #48]
	str	r2, [r8, #1]
	ldr	r1, [sp, #48]
	add	r1, r1, #4
	str	r1, [sp, #88]
	add	r8, sl, #1
	ldr	r2, [r3, r8, asl #2]
	str	r2, [r1, #1]
	add	r2, r8, #1
	ldr	r8, [r3, r2, asl #2]
	str	r8, [r1, #5]
	add	r2, sl, #3
	ldr	r8, [r3, r2, asl #2]
	ldr	r1, [sp, #84]
	ldr	r2, [sp, #48]
	add	sl, sl, #4
	cmp	sl, r1
	add	r1, r2, #16
	str	r8, [r2, #13]
	str	r1, [sp, #48]
	bcc	.L54
	ldr	sl, [sp, #128]
	str	r9, [sp, #124]
	str	sl, [sp, #4]
	add	r1, sp, #132
	ldmia	r1, {r1, r2}	@ phole ldm
.L254:
	ldr	r9, [sp, #120]
	ldr	r3, [sp, #4]
	ldr	r8, [sp, #124]
	add	sl, r3, r9
	cmp	r8, r9
	add	r3, r2, r9
	str	r3, [sp, #48]
	str	sl, [sp, #4]
	rsb	r3, r9, r8
	beq	.L55
.L53:
	ldr	r8, [sp, #4]
	ldrb	sl, [r8, #1]!	@ zero_extendqisi2
	sub	r3, r3, #1
	str	sl, [sp, #88]
	subs	r9, r3, #0
	ldr	sl, [sp, #48]
	str	r9, [sp, #48]
	ldr	r9, [sp, #88]
	and	r3, r3, #3
	strb	r9, [sl, #1]!
	beq	.L55
	cmp	r3, #0
	beq	.L280
	cmp	r3, #1
	beq	.L243
	cmp	r3, #2
	ldrneb	r3, [r8, #1]!	@ zero_extendqisi2
	strneb	r3, [sl, #1]!
	ldrne	r3, [sp, #48]
	subne	r3, r3, #1
	strne	r3, [sp, #48]
	ldrb	r3, [r8, #1]!	@ zero_extendqisi2
	strb	r3, [sl, #1]!
	ldr	r9, [sp, #48]
	sub	r3, r9, #1
	str	r3, [sp, #48]
.L243:
	ldr	r9, [sp, #48]
	ldrb	r3, [r8, #1]!	@ zero_extendqisi2
	subs	r9, r9, #1
	str	r9, [sp, #48]
	strb	r3, [sl, #1]!
	beq	.L55
	ldr	r9, [sp, #124]
	str	r2, [sp, #120]
	str	r9, [sp, #88]
	mov	r9, r7
.L56:
	mov	r7, r8
	ldrb	r2, [r7, #1]!	@ zero_extendqisi2
	mov	r3, sl
	strb	r2, [r3, #1]!
	ldrb	r2, [r7, #1]!	@ zero_extendqisi2
	strb	r2, [r3, #1]!
	ldrb	r7, [r7, #1]	@ zero_extendqisi2
	strb	r7, [r3, #1]
	ldr	r3, [sp, #48]
	ldrb	r7, [r8, #4]	@ zero_extendqisi2
	subs	r2, r3, #4
	str	r2, [sp, #48]
	add	r8, r8, #4
	strb	r7, [sl, #4]
	add	sl, sl, #4
	bne	.L56
	b	.L290
.L291:
	ldr	sl, [sp, #40]
	b	.L34
.L260:
	ldr	sl, [sp, #128]
	ldr	r8, [sp, #4]
	ldr	r3, [sp, #40]
	str	r7, [sp, #104]
	mov	r9, r5
	b	.L63
.L25:
	ldr	sl, [sp, #124]
	ldr	r8, [sp, #4]
	sub	r3, sl, #1
	ldrb	sl, [r8, #1]!	@ zero_extendqisi2
	subs	r9, r3, #0
	str	r9, [sp, #48]
	strb	sl, [r2, #1]
	and	r3, r3, #3
	ldr	sl, [sp, #84]
	beq	.L55
	cmp	r3, #0
	beq	.L262
	cmp	r3, #1
	beq	.L225
	cmp	r3, #2
	beq	.L226
	ldrb	r3, [r8, #1]!	@ zero_extendqisi2
	ldr	r9, [sp, #84]
	strb	r3, [r9, #1]
	ldr	r3, [sp, #48]
	sub	r9, r3, #1
	str	r9, [sp, #48]
	add	sl, sl, #1
.L226:
	ldrb	r3, [r8, #1]!	@ zero_extendqisi2
	strb	r3, [sl, #1]!
	ldr	r9, [sp, #48]
	sub	r3, r9, #1
	str	r3, [sp, #48]
.L225:
	ldr	r9, [sp, #48]
	ldrb	r3, [r8, #1]!	@ zero_extendqisi2
	subs	r9, r9, #1
	str	r9, [sp, #48]
	strb	r3, [sl, #1]!
	beq	.L55
	ldr	r9, [sp, #124]
	str	r2, [sp, #120]
	str	r9, [sp, #88]
	mov	r9, r7
.L31:
	mov	r7, r8
	ldrb	r2, [r7, #1]!	@ zero_extendqisi2
	mov	r3, sl
	strb	r2, [r3, #1]!
	ldrb	r2, [r7, #1]!	@ zero_extendqisi2
	strb	r2, [r3, #1]!
	ldrb	r7, [r7, #1]	@ zero_extendqisi2
	strb	r7, [r3, #1]
	ldr	r3, [sp, #48]
	ldrb	r7, [r8, #4]	@ zero_extendqisi2
	subs	r2, r3, #4
	str	r2, [sp, #48]
	add	r8, r8, #4
	strb	r7, [sl, #4]
	add	sl, sl, #4
	bne	.L31
	b	.L290
.L287:
	ldr	r3, [sp, #124]
	b	.L251
.L266:
	ldr	r9, [sp, #4]
	str	r9, [sp, #128]
	b	.L286
.L264:
	ldr	r3, [sp, #124]
	mov	r9, r7
	str	r3, [sp, #88]
	str	r2, [sp, #120]
	b	.L30
.L51:
	ldr	sl, [sp, #124]
	ldr	r8, [sp, #4]
	sub	r3, sl, #1
	ldrb	sl, [r8, #1]!	@ zero_extendqisi2
	subs	r9, r3, #0
	str	r9, [sp, #48]
	strb	sl, [r2, #1]
	and	r3, r3, #3
	ldr	sl, [sp, #84]
	beq	.L55
	cmp	r3, #0
	beq	.L278
	cmp	r3, #1
	beq	.L241
	cmp	r3, #2
	beq	.L242
	ldrb	r3, [r8, #1]!	@ zero_extendqisi2
	ldr	r9, [sp, #84]
	strb	r3, [r9, #1]
	ldr	r3, [sp, #48]
	sub	r9, r3, #1
	str	r9, [sp, #48]
	add	sl, sl, #1
.L242:
	ldrb	r3, [r8, #1]!	@ zero_extendqisi2
	strb	r3, [sl, #1]!
	ldr	r9, [sp, #48]
	sub	r3, r9, #1
	str	r3, [sp, #48]
.L241:
	ldr	r9, [sp, #48]
	ldrb	r3, [r8, #1]!	@ zero_extendqisi2
	subs	r9, r9, #1
	str	r9, [sp, #48]
	strb	r3, [sl, #1]!
	beq	.L55
	ldr	r9, [sp, #124]
	str	r2, [sp, #120]
	str	r9, [sp, #88]
	mov	r9, r7
.L57:
	mov	r7, r8
	ldrb	r2, [r7, #1]!	@ zero_extendqisi2
	mov	r3, sl
	strb	r2, [r3, #1]!
	ldrb	r2, [r7, #1]!	@ zero_extendqisi2
	strb	r2, [r3, #1]!
	ldrb	r7, [r7, #1]	@ zero_extendqisi2
	strb	r7, [r3, #1]
	ldr	r3, [sp, #48]
	ldrb	r7, [r8, #4]	@ zero_extendqisi2
	subs	r2, r3, #4
	str	r2, [sp, #48]
	add	r8, r8, #4
	strb	r7, [sl, #4]
	add	sl, sl, #4
	bne	.L57
	b	.L290
.L300:
	ldr	r5, .L301+12
	mov	r6, #27
	str	r5, [r0, #24]
	mov	r3, fp
	str	r6, [fp, #0]
	b	.L22
.L37:
	ldr	sl, [sp, #128]
	ldr	r9, [sp, #132]
	sub	r3, sl, #1
	ldrb	sl, [r9, #1]!	@ zero_extendqisi2
	subs	r8, r3, #0
	str	r8, [sp, #84]
	strb	sl, [r2, #1]
	ldr	sl, [sp, #124]
	and	r3, r3, #3
	str	sl, [sp, #120]
	beq	.L41
	cmp	r3, #0
	beq	.L272
	cmp	r3, #1
	beq	.L235
	cmp	r3, #2
	beq	.L236
	ldrb	sl, [r9, #1]!	@ zero_extendqisi2
	ldr	r3, [sp, #124]
	strb	sl, [r3, #1]
	ldr	r8, [sp, #84]
	ldr	r3, [sp, #120]
	sub	sl, r8, #1
	add	r8, r3, #1
	str	sl, [sp, #84]
	str	r8, [sp, #120]
.L236:
	ldrb	sl, [r9, #1]!	@ zero_extendqisi2
	ldr	r8, [sp, #120]
	strb	sl, [r8, #1]!
	ldr	r3, [sp, #84]
	str	r8, [sp, #120]
	sub	r8, r3, #1
	str	r8, [sp, #84]
.L235:
	ldr	r3, [sp, #84]
	ldrb	r8, [r9, #1]!	@ zero_extendqisi2
	subs	sl, r3, #1
	ldr	r3, [sp, #120]
	str	sl, [sp, #84]
	strb	r8, [r3, #1]!
	str	r3, [sp, #120]
	beq	.L41
	ldr	r8, [sp, #140]
	ldr	sl, [sp, #4]
	str	r8, [sp, #124]
	str	r2, [sp, #136]
.L43:
	mov	r8, r9
	ldrb	r2, [r8, #1]!	@ zero_extendqisi2
	ldr	r3, [sp, #120]
	strb	r2, [r3, #1]!
	ldrb	r2, [r8, #1]!	@ zero_extendqisi2
	strb	r2, [r3, #1]!
	ldrb	r2, [r8, #1]	@ zero_extendqisi2
	strb	r2, [r3, #1]
	ldr	r8, [sp, #84]
	subs	r2, r8, #4
	ldr	r8, [sp, #120]
	ldrb	r3, [r9, #4]	@ zero_extendqisi2
	str	r2, [sp, #84]
	add	r2, r8, #4
	strb	r3, [r8, #4]
	add	r9, r9, #4
	str	r2, [sp, #120]
	bne	.L43
	b	.L289
.L282:
	ldr	r9, [sp, #4]
	ldr	r3, [sp, #88]
	str	r9, [sp, #128]
	str	r1, [sp, #132]
	ldr	r9, [sp, #124]
	str	r2, [sp, #136]
	b	.L54
.L274:
	ldr	r3, [sp, #140]
	ldr	sl, [sp, #4]
	str	r3, [sp, #124]
	str	r2, [sp, #136]
	ldr	r9, [sp, #132]
	b	.L42
.L262:
	ldr	r3, [sp, #124]
	mov	r9, r7
	str	r3, [sp, #88]
	str	r2, [sp, #120]
	b	.L31
.L280:
	ldr	r3, [sp, #124]
	mov	r9, r7
	str	r3, [sp, #88]
	str	r2, [sp, #120]
	b	.L56
.L268:
	ldr	sl, [sp, #128]
	str	r1, [sp, #84]
	str	sl, [sp, #48]
	mov	sl, r7
	b	.L49
.L44:
	ldr	r8, [sp, #128]
	mov	r2, r3
	mov	sl, r8
.L50:
	ldr	r9, [sp, #64]
	ldrb	r3, [r9, r2]	@ zero_extendqisi2
	ldr	r9, [sp, #44]
	add	r2, r2, #1
	cmp	r9, r2
	strb	r3, [r8, #1]!
	bne	.L50
	str	sl, [sp, #128]
	b	.L48
.L272:
	ldr	r3, [sp, #140]
	ldr	sl, [sp, #4]
	str	r3, [sp, #124]
	str	r2, [sp, #136]
	b	.L43
.L278:
	ldr	r3, [sp, #124]
	mov	r9, r7
	str	r3, [sp, #88]
	str	r2, [sp, #120]
	b	.L57
	.size	inflate_fast, .-inflate_fast
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"invalid distance too far back\000"
	.space	2
.LC1:
	.ascii	"invalid distance code\000"
	.space	2
.LC2:
	.ascii	"invalid literal/length code\000"
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
