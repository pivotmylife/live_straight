	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"pngtrans.c"
	.section	.text.png_set_bgr,"ax",%progbits
	.align	2
	.global	png_set_bgr
	.hidden	png_set_bgr
	.type	png_set_bgr, %function
png_set_bgr:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	ldrne	r3, [r0, #140]
	orrne	r3, r3, #1
	strne	r3, [r0, #140]
	bx	lr
	.size	png_set_bgr, .-png_set_bgr
	.section	.text.png_set_swap,"ax",%progbits
	.align	2
	.global	png_set_swap
	.hidden	png_set_swap
	.type	png_set_swap, %function
png_set_swap:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	bxeq	lr
	ldrb	r3, [r0, #323]	@ zero_extendqisi2
	cmp	r3, #16
	ldreq	r3, [r0, #140]
	orreq	r3, r3, #16
	streq	r3, [r0, #140]
	bx	lr
	.size	png_set_swap, .-png_set_swap
	.section	.text.png_set_packing,"ax",%progbits
	.align	2
	.global	png_set_packing
	.hidden	png_set_packing
	.type	png_set_packing, %function
png_set_packing:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	bxeq	lr
	ldrb	r3, [r0, #323]	@ zero_extendqisi2
	cmp	r3, #7
	ldrls	r3, [r0, #140]
	movls	r2, #8
	orrls	r3, r3, #4
	strlsb	r2, [r0, #324]
	strls	r3, [r0, #140]
	bx	lr
	.size	png_set_packing, .-png_set_packing
	.section	.text.png_set_packswap,"ax",%progbits
	.align	2
	.global	png_set_packswap
	.hidden	png_set_packswap
	.type	png_set_packswap, %function
png_set_packswap:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	bxeq	lr
	ldrb	r3, [r0, #323]	@ zero_extendqisi2
	cmp	r3, #7
	ldrls	r3, [r0, #140]
	orrls	r3, r3, #65536
	strls	r3, [r0, #140]
	bx	lr
	.size	png_set_packswap, .-png_set_packswap
	.section	.text.png_set_shift,"ax",%progbits
	.align	2
	.global	png_set_shift
	.hidden	png_set_shift
	.type	png_set_shift, %function
png_set_shift:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, lr}
	subs	r3, r0, #0
	beq	.L16
	ldr	ip, [r3, #140]
	add	r0, r3, #412
	orr	r2, ip, #8
	str	r2, [r3, #140]
	add	r0, r0, #1
	mov	r2, #5
	bl	memcpy
.L16:
	ldmfd	sp!, {r3, lr}
	bx	lr
	.size	png_set_shift, .-png_set_shift
	.section	.text.png_set_interlace_handling,"ax",%progbits
	.align	2
	.global	png_set_interlace_handling
	.hidden	png_set_interlace_handling
	.type	png_set_interlace_handling, %function
png_set_interlace_handling:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	beq	.L18
	ldrb	r3, [r0, #319]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L18
	ldr	r2, [r0, #140]
	orr	r1, r2, #2
	str	r1, [r0, #140]
	mov	r0, #7
	bx	lr
.L18:
	mov	r0, #1
	bx	lr
	.size	png_set_interlace_handling, .-png_set_interlace_handling
	.section	.text.png_set_filler,"ax",%progbits
	.align	2
	.global	png_set_filler
	.hidden	png_set_filler
	.type	png_set_filler, %function
png_set_filler:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	bxeq	lr
	ldr	ip, [r0, #140]
	mov	r3, #328
	cmp	r2, #1
	orr	r2, ip, #32768
	add	ip, r3, #2
	ldr	r3, [r0, #136]
	orreq	r3, r3, #128
	bicne	r3, r3, #128
	str	r3, [r0, #136]
	ldrb	r3, [r0, #322]	@ zero_extendqisi2
	cmp	r3, #2
	and	r1, r1, #255
	moveq	r3, #4
	strh	r1, [r0, ip]	@ movhi
	str	r2, [r0, #140]
	streqb	r3, [r0, #327]
	bxeq	lr
	cmp	r3, #0
	bxne	lr
	ldrb	r2, [r0, #323]	@ zero_extendqisi2
	cmp	r2, #7
	movhi	r2, #2
	strhib	r2, [r0, #327]
	bx	lr
	.size	png_set_filler, .-png_set_filler
	.section	.text.png_set_swap_alpha,"ax",%progbits
	.align	2
	.global	png_set_swap_alpha
	.hidden	png_set_swap_alpha
	.type	png_set_swap_alpha, %function
png_set_swap_alpha:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	ldrne	r3, [r0, #140]
	orrne	r3, r3, #131072
	strne	r3, [r0, #140]
	bx	lr
	.size	png_set_swap_alpha, .-png_set_swap_alpha
	.section	.text.png_set_invert_alpha,"ax",%progbits
	.align	2
	.global	png_set_invert_alpha
	.hidden	png_set_invert_alpha
	.type	png_set_invert_alpha, %function
png_set_invert_alpha:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	ldrne	r3, [r0, #140]
	orrne	r3, r3, #524288
	strne	r3, [r0, #140]
	bx	lr
	.size	png_set_invert_alpha, .-png_set_invert_alpha
	.section	.text.png_set_invert_mono,"ax",%progbits
	.align	2
	.global	png_set_invert_mono
	.hidden	png_set_invert_mono
	.type	png_set_invert_mono, %function
png_set_invert_mono:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	ldrne	r3, [r0, #140]
	orrne	r3, r3, #32
	strne	r3, [r0, #140]
	bx	lr
	.size	png_set_invert_mono, .-png_set_invert_mono
	.section	.text.png_do_invert,"ax",%progbits
	.align	2
	.global	png_do_invert
	.hidden	png_do_invert
	.type	png_do_invert, %function
png_do_invert:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, r5, r6, r7, r8, sl}
	ldrb	r3, [r0, #8]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L37
	ldr	r4, [r0, #4]
	cmp	r4, #0
	beq	.L48
	rsb	r0, r1, #0
	and	r0, r0, #3
	cmp	r0, r4
	movcs	r0, r4
	cmp	r0, #0
	moveq	r3, r1
	moveq	r2, r0
	beq	.L40
	ldrb	r3, [r1, #0]	@ zero_extendqisi2
	sub	ip, r0, #1
	mvn	r2, r3
	cmp	r0, #1
	mov	r3, r1
	strb	r2, [r3], #1
	and	ip, ip, #3
	mov	r2, #1
	bls	.L119
	cmp	ip, #0
	beq	.L41
	cmp	ip, #1
	beq	.L113
	cmp	ip, #2
	ldrneb	r2, [r1, #1]	@ zero_extendqisi2
	mvnne	r2, r2
	strneb	r2, [r1, #1]
	movne	r2, #2
	ldrb	ip, [r1, r2]	@ zero_extendqisi2
	addne	r3, r3, #1
	mvn	ip, ip
	strb	ip, [r1, r2]
	add	r3, r3, #1
	add	r2, r2, #1
.L113:
	ldrb	ip, [r1, r2]	@ zero_extendqisi2
	mvn	ip, ip
	strb	ip, [r1, r2]
	add	r2, r2, #1
	cmp	r0, r2
	add	r3, r3, #1
	bls	.L119
.L41:
	ldrb	r5, [r1, r2]	@ zero_extendqisi2
	mvn	ip, r5
	strb	ip, [r1, r2]
	add	ip, r2, #1
	ldrb	r5, [r1, ip]	@ zero_extendqisi2
	mvn	r5, r5
	strb	r5, [r1, ip]
	add	ip, ip, #1
	ldrb	r5, [r1, ip]	@ zero_extendqisi2
	mvn	r5, r5
	strb	r5, [r1, ip]
	add	ip, r2, #3
	ldrb	r5, [r1, ip]	@ zero_extendqisi2
	add	r2, r2, #4
	mvn	r5, r5
	cmp	r0, r2
	strb	r5, [r1, ip]
	add	r3, r3, #4
	bhi	.L41
.L119:
	cmp	r4, r0
	beq	.L48
.L40:
	rsb	r8, r0, r4
	mov	r6, r8, lsr #2
	movs	r7, r6, asl #2
	beq	.L42
	add	r1, r1, r0
	ldr	r5, [r1, #0]
	mov	ip, #1
	mvn	r0, r5
	cmp	ip, r6
	sub	r5, r6, #1
	str	r0, [r1, #0]
	and	r5, r5, #3
	mov	r0, #4
	bcs	.L120
	cmp	r5, #0
	beq	.L43
	cmp	r5, #1
	beq	.L111
	cmp	r5, #2
	ldrne	r0, [r1, #4]
	mvnne	r0, r0
	strne	r0, [r1, #4]
	movne	r0, #8
	ldr	r5, [r1, r0]
	movne	ip, #2
	mvn	r5, r5
	str	r5, [r1, r0]
	add	ip, ip, #1
	add	r0, r0, #4
.L111:
	ldr	r5, [r1, r0]
	add	ip, ip, #1
	mvn	r5, r5
	cmp	ip, r6
	str	r5, [r1, r0]
	add	r0, r0, #4
	bcs	.L120
.L43:
	ldr	sl, [r1, r0]
	mvn	r5, sl
	str	r5, [r1, r0]
	add	r5, r0, #4
	ldr	sl, [r1, r5]
	mvn	sl, sl
	str	sl, [r1, r5]
	add	r5, r5, #4
	ldr	sl, [r1, r5]
	mvn	sl, sl
	str	sl, [r1, r5]
	add	r5, r0, #12
	ldr	sl, [r1, r5]
	add	ip, ip, #4
	mvn	sl, sl
	cmp	ip, r6
	str	sl, [r1, r5]
	add	r0, r0, #16
	bcc	.L43
.L120:
	cmp	r8, r7
	add	r3, r3, r7
	add	r2, r2, r7
	beq	.L48
.L42:
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	mvn	r0, r2
	add	ip, r2, #1
	mvn	r1, r1
	cmp	r4, ip
	add	ip, r0, r4
	strb	r1, [r3, #0]
	and	r0, ip, #3
	mov	r1, #1
	bls	.L48
	cmp	r0, #0
	beq	.L44
	cmp	r0, #1
	beq	.L109
	cmp	r0, #2
	ldrneb	r1, [r3, #1]	@ zero_extendqisi2
	mvnne	r1, r1
	strneb	r1, [r3, #1]
	movne	r1, #2
	ldrb	ip, [r3, r1]	@ zero_extendqisi2
	mvn	r0, ip
	strb	r0, [r3, r1]
	add	r1, r1, #1
.L109:
	ldrb	r0, [r3, r1]	@ zero_extendqisi2
	mvn	ip, r0
	strb	ip, [r3, r1]
	add	r1, r1, #1
	add	r0, r1, r2
	cmp	r4, r0
	bls	.L48
.L44:
	ldrb	r5, [r3, r1]	@ zero_extendqisi2
	mvn	r0, r5
	strb	r0, [r3, r1]
	add	r0, r1, #1
	ldrb	ip, [r3, r0]	@ zero_extendqisi2
	mvn	r5, ip
	strb	r5, [r3, r0]
	add	r5, r0, #1
	ldrb	ip, [r3, r5]	@ zero_extendqisi2
	mvn	ip, ip
	strb	ip, [r3, r5]
	add	r0, r1, #3
	ldrb	ip, [r3, r0]	@ zero_extendqisi2
	add	r1, r1, #4
	add	r5, r1, r2
	mvn	ip, ip
	cmp	r4, r5
	strb	ip, [r3, r0]
	bhi	.L44
.L48:
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl}
	bx	lr
.L37:
	ldrh	r2, [r0, #8]
	mov	ip, #2048
	add	r3, ip, #4
	cmp	r2, r3
	beq	.L121
	mov	ip, #4096
	add	r3, ip, #4
	cmp	r2, r3
	bne	.L48
	ldr	r4, [r0, #4]
	cmp	r4, #0
	beq	.L48
	ldrb	r3, [r1, #0]	@ zero_extendqisi2
	ldrb	ip, [r1, #1]	@ zero_extendqisi2
	sub	r0, r4, #1
	mvn	r2, r3
	mov	r0, r0, lsr #2
	mvn	r3, ip
	cmp	r4, #4
	strb	r2, [r1, #0]
	strb	r3, [r1, #1]
	and	r0, r0, #3
	add	r2, r1, #4
	mov	r3, #4
	bls	.L48
	cmp	r0, #0
	beq	.L47
	cmp	r0, #1
	beq	.L117
	cmp	r0, #2
	beq	.L118
	ldrb	r3, [r1, #4]	@ zero_extendqisi2
	mvn	ip, r3
	strb	ip, [r1, #4]
	ldrb	r0, [r2, #1]	@ zero_extendqisi2
	mvn	r3, r0
	strb	r3, [r2, #1]
	add	r2, r2, #4
	mov	r3, #8
.L118:
	ldrb	ip, [r1, r3]	@ zero_extendqisi2
	mvn	r0, ip
	strb	r0, [r1, r3]
	ldrb	ip, [r2, #1]	@ zero_extendqisi2
	mvn	r0, ip
	strb	r0, [r2, #1]
	add	r3, r3, #4
	add	r2, r2, #4
.L117:
	ldrb	ip, [r1, r3]	@ zero_extendqisi2
	mvn	r0, ip
	strb	r0, [r1, r3]
	ldrb	ip, [r2, #1]	@ zero_extendqisi2
	add	r3, r3, #4
	mvn	r0, ip
	cmp	r4, r3
	strb	r0, [r2, #1]
	add	r2, r2, #4
	bls	.L48
.L47:
	ldrb	ip, [r1, r3]	@ zero_extendqisi2
	mvn	r5, ip
	strb	r5, [r1, r3]
	ldrb	r0, [r2, #1]	@ zero_extendqisi2
	mvn	ip, r0
	strb	ip, [r2, #1]
	add	ip, r3, #4
	ldrb	r5, [r1, ip]	@ zero_extendqisi2
	mvn	r0, r5
	strb	r0, [r1, ip]
	add	r0, r2, #4
	ldrb	r5, [r0, #1]	@ zero_extendqisi2
	mvn	r5, r5
	strb	r5, [r0, #1]
	add	ip, ip, #4
	ldrb	r5, [r1, ip]	@ zero_extendqisi2
	mvn	r5, r5
	strb	r5, [r1, ip]
	add	r0, r0, #4
	ldrb	ip, [r0, #1]	@ zero_extendqisi2
	mvn	ip, ip
	strb	ip, [r0, #1]
	add	r0, r3, #12
	ldrb	ip, [r1, r0]	@ zero_extendqisi2
	mvn	ip, ip
	strb	ip, [r1, r0]
	add	r0, r2, #12
	ldrb	ip, [r0, #1]	@ zero_extendqisi2
	add	r3, r3, #16
	mvn	ip, ip
	cmp	r4, r3
	strb	ip, [r0, #1]
	add	r2, r2, #16
	bhi	.L47
	b	.L48
.L121:
	ldr	r0, [r0, #4]
	cmp	r0, #0
	beq	.L48
	ldrb	ip, [r1, #0]	@ zero_extendqisi2
	sub	r2, r0, #1
	mvn	r3, ip
	mov	r2, r2, lsr #1
	cmp	r0, #2
	strb	r3, [r1, #0]
	and	r2, r2, #3
	mov	r3, #2
	bls	.L48
	cmp	r2, #0
	beq	.L46
	cmp	r2, #1
	beq	.L115
	cmp	r2, #2
	ldrneb	r3, [r1, #2]	@ zero_extendqisi2
	mvnne	r3, r3
	strneb	r3, [r1, #2]
	movne	r3, #4
	ldrb	ip, [r1, r3]	@ zero_extendqisi2
	mvn	r2, ip
	strb	r2, [r1, r3]
	add	r3, r3, #2
.L115:
	ldrb	ip, [r1, r3]	@ zero_extendqisi2
	mvn	r2, ip
	strb	r2, [r1, r3]
	add	r3, r3, #2
	cmp	r0, r3
	bls	.L48
.L46:
	ldrb	ip, [r1, r3]	@ zero_extendqisi2
	mvn	r2, ip
	strb	r2, [r1, r3]
	add	r2, r3, #2
	ldrb	ip, [r1, r2]	@ zero_extendqisi2
	mvn	ip, ip
	strb	ip, [r1, r2]
	add	r2, r2, #2
	ldrb	ip, [r1, r2]	@ zero_extendqisi2
	mvn	ip, ip
	strb	ip, [r1, r2]
	add	r2, r3, #6
	ldrb	ip, [r1, r2]	@ zero_extendqisi2
	add	r3, r3, #8
	mvn	ip, ip
	cmp	r0, r3
	strb	ip, [r1, r2]
	bhi	.L46
	b	.L48
	.size	png_do_invert, .-png_do_invert
	.section	.text.png_do_swap,"ax",%progbits
	.align	2
	.global	png_do_swap
	.hidden	png_do_swap
	.type	png_do_swap, %function
png_do_swap:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, r5}
	ldrb	r3, [r0, #9]	@ zero_extendqisi2
	cmp	r3, #16
	beq	.L140
.L125:
	ldmfd	sp!, {r4, r5}
	bx	lr
.L140:
	ldr	ip, [r0, #0]
	ldrb	r2, [r0, #10]	@ zero_extendqisi2
	muls	ip, r2, ip
	beq	.L125
	ldrb	r2, [r1, #0]	@ zero_extendqisi2
	ldrb	r0, [r1, #1]	@ zero_extendqisi2
	sub	r3, ip, #1
	cmp	ip, #1
	strb	r2, [r1, #1]
	strb	r0, [r1, #0]
	and	r3, r3, #3
	add	r1, r1, #2
	mov	r2, #1
	bls	.L125
	cmp	r3, #0
	beq	.L124
	cmp	r3, #1
	beq	.L138
	cmp	r3, #2
	beq	.L139
	ldrb	r2, [r1, #0]	@ zero_extendqisi2
	ldrb	r0, [r1, #1]	@ zero_extendqisi2
	strb	r2, [r1, #1]
	strb	r0, [r1, #0]
	mov	r2, #2
	add	r1, r1, #2
.L139:
	ldrb	r0, [r1, #0]	@ zero_extendqisi2
	ldrb	r3, [r1, #1]	@ zero_extendqisi2
	add	r2, r2, #1
	strb	r0, [r1, #1]
	strb	r3, [r1, #0]
	add	r1, r1, #2
.L138:
	ldrb	r3, [r1, #0]	@ zero_extendqisi2
	ldrb	r0, [r1, #1]	@ zero_extendqisi2
	add	r2, r2, #1
	cmp	ip, r2
	strb	r0, [r1, #0]
	strb	r3, [r1, #1]
	add	r1, r1, #2
	bls	.L125
.L124:
	ldrb	r3, [r1, #0]	@ zero_extendqisi2
	ldrb	r4, [r1, #1]	@ zero_extendqisi2
	strb	r3, [r1, #1]
	strb	r4, [r1, #0]
	add	r3, r1, #2
	ldrb	r5, [r1, #2]	@ zero_extendqisi2
	ldrb	r0, [r3, #1]	@ zero_extendqisi2
	strb	r0, [r1, #2]
	strb	r5, [r3, #1]
	add	r0, r3, #2
	ldrb	r4, [r3, #2]	@ zero_extendqisi2
	ldrb	r5, [r0, #1]	@ zero_extendqisi2
	strb	r5, [r3, #2]
	strb	r4, [r0, #1]
	add	r3, r1, #6
	ldrb	r0, [r1, #6]	@ zero_extendqisi2
	ldrb	r4, [r3, #1]	@ zero_extendqisi2
	add	r2, r2, #4
	cmp	ip, r2
	strb	r4, [r1, #6]
	strb	r0, [r3, #1]
	add	r1, r1, #8
	bhi	.L124
	b	.L125
	.size	png_do_swap, .-png_do_swap
	.section	.text.png_do_packswap,"ax",%progbits
	.align	2
	.global	png_do_packswap
	.hidden	png_do_packswap
	.type	png_do_packswap, %function
png_do_packswap:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldrb	r3, [r0, #9]	@ zero_extendqisi2
	cmp	r3, #7
	bxhi	lr
	cmp	r3, #1
	ldr	r0, [r0, #4]
	ldreq	r2, .L162
	beq	.L144
	cmp	r3, #2
	ldreq	r2, .L162+4
	beq	.L144
	cmp	r3, #4
	bxne	lr
	ldr	r2, .L162+8
.L144:
	add	r0, r1, r0
	cmp	r1, r0
	bxcs	lr
	ldrb	r3, [r1, #0]	@ zero_extendqisi2
	ldrb	ip, [r2, r3]	@ zero_extendqisi2
	mov	r3, r1
	strb	ip, [r3], #1
	mvn	ip, r1
	add	r1, ip, r0
	cmp	r0, r3
	and	r1, r1, #3
	bxls	lr
	cmp	r1, #0
	beq	.L146
	cmp	r1, #1
	beq	.L160
	cmp	r1, #2
	ldrneb	r1, [r3, #0]	@ zero_extendqisi2
	ldrneb	r1, [r2, r1]	@ zero_extendqisi2
	strneb	r1, [r3], #1
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	ldrb	ip, [r2, r1]	@ zero_extendqisi2
	strb	ip, [r3], #1
.L160:
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	ldrb	ip, [r2, r1]	@ zero_extendqisi2
	strb	ip, [r3], #1
	cmp	r0, r3
	bxls	lr
.L146:
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	ldrb	ip, [r2, r1]	@ zero_extendqisi2
	mov	r1, r3
	strb	ip, [r1], #1
	ldrb	ip, [r3, #1]	@ zero_extendqisi2
	ldrb	ip, [r2, ip]	@ zero_extendqisi2
	strb	ip, [r3, #1]
	ldrb	ip, [r1, #1]	@ zero_extendqisi2
	ldrb	ip, [r2, ip]	@ zero_extendqisi2
	strb	ip, [r1, #1]
	ldrb	ip, [r3, #3]	@ zero_extendqisi2
	ldrb	r1, [r2, ip]	@ zero_extendqisi2
	strb	r1, [r3, #3]
	add	r3, r3, #4
	cmp	r0, r3
	bhi	.L146
	bx	lr
.L163:
	.align	2
.L162:
	.word	.LANCHOR0
	.word	.LANCHOR0+256
	.word	.LANCHOR0+512
	.size	png_do_packswap, .-png_do_packswap
	.section	.text.png_do_strip_filler,"ax",%progbits
	.align	2
	.global	png_do_strip_filler
	.hidden	png_do_strip_filler
	.type	png_do_strip_filler, %function
png_do_strip_filler:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldrb	r3, [r0, #8]	@ zero_extendqisi2
	cmp	r3, #2
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl}
	ldr	ip, [r0, #0]
	andeq	r4, r2, #4194304
	beq	.L166
	cmp	r3, #6
	andne	r4, r2, #4194304
	beq	.L314
.L167:
	cmp	r3, #0
	bne	.L315
.L182:
	ldrb	r5, [r0, #10]	@ zero_extendqisi2
	cmp	r5, #2
	beq	.L316
.L168:
	cmp	r4, #0
	ldrneb	r3, [r0, #8]	@ zero_extendqisi2
	bicne	r3, r3, #4
	strneb	r3, [r0, #8]
.L196:
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl}
	bx	lr
.L315:
	cmp	r3, #4
	bne	.L168
	cmp	r4, #0
	beq	.L196
	b	.L182
.L314:
	ands	r4, r2, #4194304
	beq	.L196
.L166:
	ldrb	r5, [r0, #10]	@ zero_extendqisi2
	cmp	r5, #4
	bne	.L167
	ldrb	r3, [r0, #9]	@ zero_extendqisi2
	cmp	r3, #8
	beq	.L317
	tst	r2, #128
	beq	.L318
	cmp	ip, #1
	bls	.L178
	ldrb	r9, [r1, #8]	@ zero_extendqisi2
	ldrb	sl, [r1, #9]	@ zero_extendqisi2
	ldrb	r8, [r1, #10]	@ zero_extendqisi2
	ldrb	r7, [r1, #11]	@ zero_extendqisi2
	ldrb	r2, [r1, #12]	@ zero_extendqisi2
	ldrb	r3, [r1, #13]	@ zero_extendqisi2
	mov	r6, #2
	sub	r5, ip, #2
	cmp	r6, ip
	strb	r2, [r1, #10]
	strb	r3, [r1, #11]
	strb	r9, [r1, #6]
	strb	sl, [r1, #7]
	strb	r8, [r1, #8]
	strb	r7, [r1, #9]
	and	r5, r5, #3
	add	r2, r1, #8
	add	r3, r1, #6
	beq	.L178
	cmp	r5, #0
	beq	.L179
	cmp	r5, #1
	beq	.L299
	cmp	r5, #2
	beq	.L300
	ldrb	r5, [r2, #8]	@ zero_extendqisi2
	strb	r5, [r3, #6]
	ldrb	r6, [r2, #9]	@ zero_extendqisi2
	strb	r6, [r3, #7]
	ldrb	r5, [r2, #10]	@ zero_extendqisi2
	strb	r5, [r3, #8]
	ldrb	r6, [r2, #11]	@ zero_extendqisi2
	strb	r6, [r3, #9]
	ldrb	r5, [r2, #12]	@ zero_extendqisi2
	strb	r5, [r3, #10]
	ldrb	r5, [r2, #13]	@ zero_extendqisi2
	mov	r6, #3
	strb	r5, [r3, #11]
	add	r2, r2, #8
	add	r3, r1, #12
.L300:
	ldrb	r1, [r2, #8]	@ zero_extendqisi2
	strb	r1, [r3, #6]
	ldrb	r1, [r2, #9]	@ zero_extendqisi2
	strb	r1, [r3, #7]
	ldrb	r1, [r2, #10]	@ zero_extendqisi2
	strb	r1, [r3, #8]
	ldrb	r1, [r2, #11]	@ zero_extendqisi2
	strb	r1, [r3, #9]
	ldrb	r1, [r2, #12]	@ zero_extendqisi2
	strb	r1, [r3, #10]
	ldrb	r1, [r2, #13]	@ zero_extendqisi2
	add	r6, r6, #1
	strb	r1, [r3, #11]
	add	r2, r2, #8
	add	r3, r3, #6
.L299:
	ldrb	r1, [r2, #8]	@ zero_extendqisi2
	strb	r1, [r3, #6]
	ldrb	r1, [r2, #9]	@ zero_extendqisi2
	strb	r1, [r3, #7]
	ldrb	r1, [r2, #10]	@ zero_extendqisi2
	strb	r1, [r3, #8]
	ldrb	r1, [r2, #11]	@ zero_extendqisi2
	strb	r1, [r3, #9]
	ldrb	r1, [r2, #12]	@ zero_extendqisi2
	strb	r1, [r3, #10]
	add	r6, r6, #1
	ldrb	r1, [r2, #13]	@ zero_extendqisi2
	cmp	r6, ip
	strb	r1, [r3, #11]
	add	r2, r2, #8
	add	r3, r3, #6
	beq	.L178
.L179:
	ldrb	r7, [r2, #8]	@ zero_extendqisi2
	strb	r7, [r3, #6]
	ldrb	r5, [r2, #9]	@ zero_extendqisi2
	strb	r5, [r3, #7]
	ldrb	r1, [r2, #10]	@ zero_extendqisi2
	strb	r1, [r3, #8]
	ldrb	r7, [r2, #11]	@ zero_extendqisi2
	strb	r7, [r3, #9]
	ldrb	r5, [r2, #12]	@ zero_extendqisi2
	strb	r5, [r3, #10]
	ldrb	r1, [r2, #13]	@ zero_extendqisi2
	strb	r1, [r3, #11]
	add	r1, r2, #8
	ldrb	r7, [r1, #8]	@ zero_extendqisi2
	add	r5, r3, #6
	strb	r7, [r5, #6]
	ldrb	r7, [r1, #9]	@ zero_extendqisi2
	strb	r7, [r5, #7]
	ldrb	r7, [r1, #10]	@ zero_extendqisi2
	strb	r7, [r5, #8]
	ldrb	r7, [r1, #11]	@ zero_extendqisi2
	strb	r7, [r5, #9]
	ldrb	r7, [r1, #12]	@ zero_extendqisi2
	strb	r7, [r5, #10]
	ldrb	r7, [r1, #13]	@ zero_extendqisi2
	strb	r7, [r5, #11]
	add	r1, r1, #8
	ldrb	r7, [r1, #8]	@ zero_extendqisi2
	add	r5, r3, #12
	strb	r7, [r5, #6]
	ldrb	r7, [r1, #9]	@ zero_extendqisi2
	strb	r7, [r5, #7]
	ldrb	r7, [r1, #10]	@ zero_extendqisi2
	strb	r7, [r5, #8]
	ldrb	r7, [r1, #11]	@ zero_extendqisi2
	strb	r7, [r5, #9]
	ldrb	r7, [r1, #12]	@ zero_extendqisi2
	strb	r7, [r5, #10]
	ldrb	r1, [r1, #13]	@ zero_extendqisi2
	strb	r1, [r5, #11]
	add	r5, r2, #24
	ldrb	r7, [r5, #8]	@ zero_extendqisi2
	add	r1, r3, #18
	strb	r7, [r1, #6]
	ldrb	r7, [r5, #9]	@ zero_extendqisi2
	strb	r7, [r1, #7]
	ldrb	r7, [r5, #10]	@ zero_extendqisi2
	strb	r7, [r1, #8]
	ldrb	r7, [r5, #11]	@ zero_extendqisi2
	strb	r7, [r1, #9]
	ldrb	r7, [r5, #12]	@ zero_extendqisi2
	strb	r7, [r1, #10]
	add	r6, r6, #4
	ldrb	r5, [r5, #13]	@ zero_extendqisi2
	cmp	r6, ip
	strb	r5, [r1, #11]
	add	r2, r2, #32
	add	r3, r3, #24
	bne	.L179
.L178:
	add	r2, ip, ip, asl #1
	mov	ip, r2, asl #1
	mov	r3, #48
	strb	r3, [r0, #11]
	str	ip, [r0, #4]
.L175:
	mov	r3, #3
	strb	r3, [r0, #10]
	b	.L168
.L316:
	ldrb	r3, [r0, #9]	@ zero_extendqisi2
	cmp	r3, #8
	beq	.L319
	tst	r2, #128
	bne	.L191
	cmp	ip, #0
	beq	.L193
	ldrb	r2, [r1, #2]	@ zero_extendqisi2
	ldrb	r3, [r1, #3]	@ zero_extendqisi2
	sub	r6, ip, #1
	cmp	ip, #1
	strb	r2, [r1, #0]
	strb	r3, [r1, #1]
	and	r6, r6, #3
	add	r3, r1, #4
	mov	r2, #1
	add	r1, r1, #2
	bls	.L193
	cmp	r6, #0
	beq	.L195
	cmp	r6, #1
	beq	.L309
	cmp	r6, #2
	beq	.L310
	ldrb	r2, [r3, #2]	@ zero_extendqisi2
	strb	r2, [r1, #0]
	ldrb	r2, [r3, #3]	@ zero_extendqisi2
	add	r3, r3, #4
	strb	r2, [r1, #1]
	add	r1, r1, #2
	mov	r2, r5
.L310:
	ldrb	r5, [r3, #2]	@ zero_extendqisi2
	strb	r5, [r1, #0]
	ldrb	r5, [r3, #3]	@ zero_extendqisi2
	add	r2, r2, #1
	strb	r5, [r1, #1]
	add	r3, r3, #4
	add	r1, r1, #2
.L309:
	ldrb	r5, [r3, #2]	@ zero_extendqisi2
	strb	r5, [r1, #0]
	add	r2, r2, #1
	ldrb	r5, [r3, #3]	@ zero_extendqisi2
	cmp	ip, r2
	strb	r5, [r1, #1]
	add	r3, r3, #4
	add	r1, r1, #2
	bls	.L193
.L195:
	ldrb	r7, [r3, #2]	@ zero_extendqisi2
	strb	r7, [r1, #0]
	ldrb	r6, [r3, #3]	@ zero_extendqisi2
	strb	r6, [r1, #1]
	add	r6, r3, #4
	ldrb	r5, [r6, #2]	@ zero_extendqisi2
	strb	r5, [r1, #2]
	ldrb	r7, [r6, #3]	@ zero_extendqisi2
	add	r5, r1, #2
	strb	r7, [r5, #1]
	add	r6, r6, #4
	ldrb	r7, [r6, #2]	@ zero_extendqisi2
	strb	r7, [r5, #2]
	ldrb	r6, [r6, #3]	@ zero_extendqisi2
	strb	r6, [r5, #3]
	add	r5, r3, #12
	ldrb	r6, [r5, #2]	@ zero_extendqisi2
	strb	r6, [r1, #6]
	add	r2, r2, #4
	ldrb	r5, [r5, #3]	@ zero_extendqisi2
	cmp	ip, r2
	strb	r5, [r1, #7]
	add	r3, r3, #16
	add	r1, r1, #8
	bhi	.L195
.L193:
	mov	ip, ip, asl #1
	mov	r1, #16
	strb	r1, [r0, #11]
	str	ip, [r0, #4]
.L190:
	mov	r3, #1
	strb	r3, [r0, #10]
	b	.L168
.L191:
	cmp	ip, #1
	bls	.L193
	ldrb	r3, [r1, #5]	@ zero_extendqisi2
	ldrb	r6, [r1, #4]	@ zero_extendqisi2
	sub	r2, ip, #2
	cmp	r5, ip
	strb	r3, [r1, #3]
	strb	r6, [r1, #2]
	add	r3, r1, #2
	and	r2, r2, #3
	add	r1, r1, #4
	beq	.L193
	cmp	r2, #0
	beq	.L194
	cmp	r2, #1
	beq	.L307
	cmp	r2, #2
	beq	.L308
	ldrb	r5, [r1, #4]	@ zero_extendqisi2
	strb	r5, [r3, #2]
	ldrb	r2, [r1, #5]	@ zero_extendqisi2
	mov	r5, #3
	strb	r2, [r3, #3]
	add	r1, r1, #4
	add	r3, r3, #2
.L308:
	ldrb	r2, [r1, #4]	@ zero_extendqisi2
	strb	r2, [r3, #2]
	ldrb	r2, [r1, #5]	@ zero_extendqisi2
	add	r5, r5, #1
	strb	r2, [r3, #3]
	add	r1, r1, #4
	add	r3, r3, #2
.L307:
	ldrb	r2, [r1, #4]	@ zero_extendqisi2
	strb	r2, [r3, #2]
	add	r5, r5, #1
	ldrb	r2, [r1, #5]	@ zero_extendqisi2
	cmp	r5, ip
	strb	r2, [r3, #3]
	add	r1, r1, #4
	add	r3, r3, #2
	beq	.L193
.L194:
	ldrb	r6, [r1, #4]	@ zero_extendqisi2
	strb	r6, [r3, #2]
	ldrb	r2, [r1, #5]	@ zero_extendqisi2
	strb	r2, [r3, #3]
	add	r6, r1, #4
	ldrb	r7, [r6, #4]	@ zero_extendqisi2
	add	r2, r3, #2
	strb	r7, [r2, #2]
	ldrb	r7, [r6, #5]	@ zero_extendqisi2
	strb	r7, [r2, #3]
	add	r6, r6, #4
	ldrb	r7, [r6, #4]	@ zero_extendqisi2
	add	r2, r2, #2
	strb	r7, [r2, #2]
	ldrb	r7, [r6, #5]	@ zero_extendqisi2
	strb	r7, [r2, #3]
	add	r6, r1, #12
	ldrb	r7, [r6, #4]	@ zero_extendqisi2
	add	r2, r3, #6
	strb	r7, [r2, #2]
	add	r5, r5, #4
	ldrb	r6, [r6, #5]	@ zero_extendqisi2
	cmp	r5, ip
	strb	r6, [r2, #3]
	add	r1, r1, #16
	add	r3, r3, #8
	bne	.L194
	b	.L193
.L318:
	cmp	ip, #0
	beq	.L178
	ldrb	r6, [r1, #5]	@ zero_extendqisi2
	ldrb	sl, [r1, #2]	@ zero_extendqisi2
	ldrb	r8, [r1, #3]	@ zero_extendqisi2
	ldrb	r7, [r1, #4]	@ zero_extendqisi2
	ldrb	r2, [r1, #6]	@ zero_extendqisi2
	ldrb	r3, [r1, #7]	@ zero_extendqisi2
	sub	r5, ip, #1
	cmp	ip, #1
	strb	r6, [r1, #3]
	strb	r2, [r1, #4]
	strb	r3, [r1, #5]
	strb	sl, [r1, #0]
	strb	r8, [r1, #1]
	strb	r7, [r1, #2]
	and	r5, r5, #3
	add	r3, r1, #6
	add	r2, r1, #8
	mov	r6, #1
	bls	.L178
	cmp	r5, #0
	beq	.L180
	cmp	r5, #1
	beq	.L301
	cmp	r5, #2
	beq	.L302
	ldrb	r6, [r2, #2]	@ zero_extendqisi2
	strb	r6, [r3, #0]
	ldrb	r5, [r2, #3]	@ zero_extendqisi2
	strb	r5, [r3, #1]
	ldrb	r6, [r2, #4]	@ zero_extendqisi2
	strb	r6, [r3, #2]
	ldrb	r5, [r2, #5]	@ zero_extendqisi2
	strb	r5, [r3, #3]
	ldrb	r6, [r2, #6]	@ zero_extendqisi2
	strb	r6, [r3, #4]
	ldrb	r5, [r2, #7]	@ zero_extendqisi2
	mov	r6, #2
	strb	r5, [r3, #5]
	add	r2, r2, #8
	add	r3, r1, #12
.L302:
	ldrb	r1, [r2, #2]	@ zero_extendqisi2
	strb	r1, [r3, #0]
	ldrb	r1, [r2, #3]	@ zero_extendqisi2
	strb	r1, [r3, #1]
	ldrb	r1, [r2, #4]	@ zero_extendqisi2
	strb	r1, [r3, #2]
	ldrb	r1, [r2, #5]	@ zero_extendqisi2
	strb	r1, [r3, #3]
	ldrb	r1, [r2, #6]	@ zero_extendqisi2
	strb	r1, [r3, #4]
	ldrb	r1, [r2, #7]	@ zero_extendqisi2
	add	r6, r6, #1
	strb	r1, [r3, #5]
	add	r2, r2, #8
	add	r3, r3, #6
.L301:
	ldrb	r1, [r2, #2]	@ zero_extendqisi2
	strb	r1, [r3, #0]
	ldrb	r1, [r2, #3]	@ zero_extendqisi2
	strb	r1, [r3, #1]
	ldrb	r1, [r2, #4]	@ zero_extendqisi2
	strb	r1, [r3, #2]
	ldrb	r1, [r2, #5]	@ zero_extendqisi2
	strb	r1, [r3, #3]
	ldrb	r1, [r2, #6]	@ zero_extendqisi2
	strb	r1, [r3, #4]
	add	r6, r6, #1
	ldrb	r1, [r2, #7]	@ zero_extendqisi2
	cmp	ip, r6
	strb	r1, [r3, #5]
	add	r2, r2, #8
	add	r3, r3, #6
	bls	.L178
.L180:
	ldrb	r5, [r2, #2]	@ zero_extendqisi2
	strb	r5, [r3, #0]
	ldrb	r7, [r2, #3]	@ zero_extendqisi2
	strb	r7, [r3, #1]
	ldrb	r1, [r2, #4]	@ zero_extendqisi2
	strb	r1, [r3, #2]
	ldrb	r5, [r2, #5]	@ zero_extendqisi2
	strb	r5, [r3, #3]
	ldrb	r7, [r2, #6]	@ zero_extendqisi2
	strb	r7, [r3, #4]
	ldrb	r1, [r2, #7]	@ zero_extendqisi2
	strb	r1, [r3, #5]
	add	r1, r2, #8
	ldrb	r5, [r1, #2]	@ zero_extendqisi2
	strb	r5, [r3, #6]
	ldrb	r7, [r1, #3]	@ zero_extendqisi2
	add	r5, r3, #6
	strb	r7, [r5, #1]
	ldrb	r7, [r1, #4]	@ zero_extendqisi2
	strb	r7, [r5, #2]
	ldrb	r7, [r1, #5]	@ zero_extendqisi2
	strb	r7, [r5, #3]
	ldrb	r7, [r1, #6]	@ zero_extendqisi2
	strb	r7, [r5, #4]
	ldrb	r7, [r1, #7]	@ zero_extendqisi2
	strb	r7, [r5, #5]
	add	r1, r1, #8
	ldrb	r5, [r1, #2]	@ zero_extendqisi2
	strb	r5, [r3, #12]
	ldrb	r7, [r1, #3]	@ zero_extendqisi2
	add	r5, r3, #12
	strb	r7, [r5, #1]
	ldrb	r7, [r1, #4]	@ zero_extendqisi2
	strb	r7, [r5, #2]
	ldrb	r7, [r1, #5]	@ zero_extendqisi2
	strb	r7, [r5, #3]
	ldrb	r7, [r1, #6]	@ zero_extendqisi2
	strb	r7, [r5, #4]
	ldrb	r1, [r1, #7]	@ zero_extendqisi2
	strb	r1, [r5, #5]
	add	r1, r2, #24
	ldrb	r5, [r1, #2]	@ zero_extendqisi2
	strb	r5, [r3, #18]
	ldrb	r7, [r1, #3]	@ zero_extendqisi2
	add	r5, r3, #18
	strb	r7, [r5, #1]
	ldrb	r7, [r1, #4]	@ zero_extendqisi2
	strb	r7, [r5, #2]
	ldrb	r7, [r1, #5]	@ zero_extendqisi2
	strb	r7, [r5, #3]
	ldrb	r7, [r1, #6]	@ zero_extendqisi2
	strb	r7, [r5, #4]
	add	r6, r6, #4
	ldrb	r1, [r1, #7]	@ zero_extendqisi2
	cmp	ip, r6
	strb	r1, [r5, #5]
	add	r3, r3, #24
	add	r2, r2, #32
	bhi	.L180
	b	.L178
.L319:
	tst	r2, #128
	bne	.L185
	cmp	ip, #0
	beq	.L187
	ldrb	r2, [r1, #1]	@ zero_extendqisi2
	sub	r6, ip, #1
	mov	r3, r1
	cmp	ip, #1
	strb	r2, [r3], #2
	and	r6, r6, #3
	mov	r2, #1
	bls	.L187
	cmp	r6, #0
	beq	.L189
	cmp	r6, #1
	beq	.L305
	cmp	r6, #2
	ldrneb	r2, [r3, #1]	@ zero_extendqisi2
	strneb	r2, [r1, #1]
	addne	r3, r3, #2
	movne	r2, r5
	ldrb	r5, [r3, #1]	@ zero_extendqisi2
	add	r3, r3, #2
	strb	r5, [r1, r2]
	add	r2, r2, #1
.L305:
	ldrb	r5, [r3, #1]	@ zero_extendqisi2
	strb	r5, [r1, r2]
	add	r2, r2, #1
	cmp	ip, r2
	add	r3, r3, #2
	bls	.L187
.L189:
	ldrb	r7, [r3, #1]	@ zero_extendqisi2
	strb	r7, [r1, r2]
	add	r6, r3, #2
	ldrb	r7, [r6, #1]	@ zero_extendqisi2
	add	r5, r2, #1
	strb	r7, [r1, r5]
	ldrb	r6, [r6, #3]	@ zero_extendqisi2
	add	r5, r5, #1
	strb	r6, [r1, r5]
	add	r5, r2, #3
	ldrb	r6, [r3, #7]	@ zero_extendqisi2
	add	r2, r2, #4
	cmp	ip, r2
	strb	r6, [r1, r5]
	add	r3, r3, #8
	bhi	.L189
.L187:
	mov	r1, #8
	strb	r1, [r0, #11]
	str	ip, [r0, #4]
	b	.L190
.L317:
	tst	r2, #128
	bne	.L170
	cmp	ip, #0
	moveq	r6, ip
	beq	.L172
	ldrb	r5, [r1, #1]	@ zero_extendqisi2
	ldrb	r2, [r1, #2]	@ zero_extendqisi2
	ldrb	r3, [r1, #3]	@ zero_extendqisi2
	sub	r6, ip, #1
	cmp	ip, #1
	strb	r5, [r1, #0]
	strb	r2, [r1, #1]
	strb	r3, [r1, #2]
	and	r6, r6, #3
	add	r3, r1, #3
	add	r2, r1, #4
	mov	r5, #1
	bls	.L313
	cmp	r6, #0
	beq	.L174
	cmp	r6, #1
	beq	.L297
	cmp	r6, #2
	beq	.L298
	ldrb	r6, [r2, #1]	@ zero_extendqisi2
	strb	r6, [r3, #0]
	ldrb	r5, [r2, #2]	@ zero_extendqisi2
	strb	r5, [r3, #1]
	ldrb	r6, [r2, #3]	@ zero_extendqisi2
	mov	r5, #2
	strb	r6, [r3, #2]
	add	r2, r2, #4
	add	r3, r1, #6
.L298:
	ldrb	r1, [r2, #1]	@ zero_extendqisi2
	strb	r1, [r3, #0]
	ldrb	r6, [r2, #2]	@ zero_extendqisi2
	strb	r6, [r3, #1]
	ldrb	r1, [r2, #3]	@ zero_extendqisi2
	add	r5, r5, #1
	strb	r1, [r3, #2]
	add	r2, r2, #4
	add	r3, r3, #3
.L297:
	ldrb	r6, [r2, #1]	@ zero_extendqisi2
	strb	r6, [r3, #0]
	ldrb	r1, [r2, #2]	@ zero_extendqisi2
	strb	r1, [r3, #1]
	add	r5, r5, #1
	ldrb	r6, [r2, #3]	@ zero_extendqisi2
	cmp	ip, r5
	strb	r6, [r3, #2]
	add	r2, r2, #4
	add	r3, r3, #3
	bls	.L313
.L174:
	ldrb	r6, [r2, #1]	@ zero_extendqisi2
	strb	r6, [r3, #0]
	ldrb	r7, [r2, #2]	@ zero_extendqisi2
	strb	r7, [r3, #1]
	ldrb	r1, [r2, #3]	@ zero_extendqisi2
	strb	r1, [r3, #2]
	add	r1, r2, #4
	ldrb	r6, [r1, #1]	@ zero_extendqisi2
	strb	r6, [r3, #3]
	ldrb	r7, [r1, #2]	@ zero_extendqisi2
	add	r6, r3, #3
	strb	r7, [r6, #1]
	ldrb	r7, [r1, #3]	@ zero_extendqisi2
	strb	r7, [r6, #2]
	add	r1, r1, #4
	ldrb	r6, [r1, #1]	@ zero_extendqisi2
	strb	r6, [r3, #6]
	ldrb	r7, [r1, #2]	@ zero_extendqisi2
	add	r6, r3, #6
	strb	r7, [r6, #1]
	ldrb	r1, [r1, #3]	@ zero_extendqisi2
	strb	r1, [r6, #2]
	add	r1, r2, #12
	ldrb	r7, [r1, #1]	@ zero_extendqisi2
	strb	r7, [r3, #9]
	ldrb	r7, [r1, #2]	@ zero_extendqisi2
	add	r6, r3, #9
	strb	r7, [r6, #1]
	add	r5, r5, #4
	ldrb	r1, [r1, #3]	@ zero_extendqisi2
	cmp	ip, r5
	strb	r1, [r6, #2]
	add	r3, r3, #12
	add	r2, r2, #16
	bhi	.L174
.L313:
	mov	r6, ip, asl #1
.L172:
	add	ip, r6, ip
	mov	r3, #24
	strb	r3, [r0, #11]
	str	ip, [r0, #4]
	b	.L175
.L185:
	cmp	ip, #0
	beq	.L187
	sub	r2, ip, #1
	cmp	ip, #1
	and	r2, r2, #3
	mov	r3, #1
	bls	.L187
	cmp	r2, #0
	beq	.L188
	cmp	r2, #1
	beq	.L303
	cmp	r2, #2
	ldrneb	r3, [r1, r5]	@ zero_extendqisi2
	strneb	r3, [r1, #1]
	movne	r3, r5
	ldrb	r2, [r1, r3, asl #1]	@ zero_extendqisi2
	strb	r2, [r1, r3]
	add	r3, r3, #1
.L303:
	ldrb	r2, [r1, r3, asl #1]	@ zero_extendqisi2
	strb	r2, [r1, r3]
	add	r3, r3, #1
	cmp	ip, r3
	bls	.L187
.L188:
	ldrb	r2, [r1, r3, asl #1]	@ zero_extendqisi2
	strb	r2, [r1, r3]
	add	r2, r3, #1
	ldrb	r5, [r1, r2, asl #1]	@ zero_extendqisi2
	strb	r5, [r1, r2]
	add	r2, r2, #1
	ldrb	r5, [r1, r2, asl #1]	@ zero_extendqisi2
	strb	r5, [r1, r2]
	add	r2, r3, #3
	ldrb	r5, [r1, r2, asl #1]	@ zero_extendqisi2
	add	r3, r3, #4
	cmp	ip, r3
	strb	r5, [r1, r2]
	bhi	.L188
	b	.L187
.L170:
	cmp	ip, #1
	bls	.L313
	mov	r6, ip, asl #1
	add	r8, r6, ip
	sub	r7, r8, #3
	ldrb	r2, [r1, #5]	@ zero_extendqisi2
	sub	r5, r7, #3
	ldrb	r8, [r1, #4]	@ zero_extendqisi2
	add	r7, r1, r7
	ldrb	sl, [r1, #6]	@ zero_extendqisi2
	add	r3, r1, #3
	add	r5, r5, r5, asl #1
	cmp	r3, r7
	strb	r2, [r1, #4]
	strb	r8, [r1, #3]
	strb	sl, [r1, #5]
	and	r5, r5, #3
	add	r2, r1, #4
	beq	.L172
	cmp	r5, #0
	beq	.L173
	cmp	r5, #1
	beq	.L295
	cmp	r5, #2
	beq	.L296
	ldrb	r5, [r2, #4]	@ zero_extendqisi2
	strb	r5, [r3, #3]
	ldrb	r5, [r2, #5]	@ zero_extendqisi2
	strb	r5, [r3, #4]
	ldrb	r5, [r2, #6]	@ zero_extendqisi2
	add	r2, r2, #4
	strb	r5, [r3, #5]
	add	r3, r1, #6
.L296:
	ldrb	r1, [r2, #4]	@ zero_extendqisi2
	strb	r1, [r3, #3]
	ldrb	r1, [r2, #5]	@ zero_extendqisi2
	strb	r1, [r3, #4]
	ldrb	r1, [r2, #6]	@ zero_extendqisi2
	add	r2, r2, #4
	strb	r1, [r3, #5]
	add	r3, r3, #3
.L295:
	ldrb	r1, [r2, #4]	@ zero_extendqisi2
	strb	r1, [r3, #3]
	ldrb	r1, [r2, #5]	@ zero_extendqisi2
	strb	r1, [r3, #4]
	ldrb	r1, [r2, #6]	@ zero_extendqisi2
	strb	r1, [r3, #5]
	add	r3, r3, #3
	cmp	r3, r7
	add	r2, r2, #4
	beq	.L172
.L173:
	ldrb	r8, [r2, #4]	@ zero_extendqisi2
	strb	r8, [r3, #3]
	ldrb	r5, [r2, #5]	@ zero_extendqisi2
	strb	r5, [r3, #4]
	ldrb	r1, [r2, #6]	@ zero_extendqisi2
	strb	r1, [r3, #5]
	add	r1, r2, #4
	ldrb	r8, [r1, #4]	@ zero_extendqisi2
	add	r5, r3, #3
	strb	r8, [r5, #3]
	ldrb	r8, [r1, #5]	@ zero_extendqisi2
	strb	r8, [r5, #4]
	ldrb	r8, [r1, #6]	@ zero_extendqisi2
	strb	r8, [r5, #5]
	add	r1, r1, #4
	ldrb	r8, [r1, #4]	@ zero_extendqisi2
	add	r5, r3, #6
	strb	r8, [r5, #3]
	ldrb	r8, [r1, #5]	@ zero_extendqisi2
	strb	r8, [r5, #4]
	ldrb	r1, [r1, #6]	@ zero_extendqisi2
	strb	r1, [r5, #5]
	add	r5, r2, #12
	ldrb	r8, [r5, #4]	@ zero_extendqisi2
	add	r1, r3, #9
	strb	r8, [r1, #3]
	ldrb	r8, [r5, #5]	@ zero_extendqisi2
	strb	r8, [r1, #4]
	add	r3, r3, #12
	ldrb	r5, [r5, #6]	@ zero_extendqisi2
	cmp	r3, r7
	strb	r5, [r1, #5]
	add	r2, r2, #16
	bne	.L173
	b	.L172
	.size	png_do_strip_filler, .-png_do_strip_filler
	.section	.text.png_do_bgr,"ax",%progbits
	.align	2
	.global	png_do_bgr
	.hidden	png_do_bgr
	.type	png_do_bgr, %function
png_do_bgr:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, r5, r6}
	ldrb	r3, [r0, #8]	@ zero_extendqisi2
	tst	r3, #2
	beq	.L329
	ldrb	r2, [r0, #9]	@ zero_extendqisi2
	cmp	r2, #8
	ldr	r0, [r0, #0]
	beq	.L386
	cmp	r2, #16
	beq	.L387
.L329:
	ldmfd	sp!, {r4, r5, r6}
	bx	lr
.L386:
	cmp	r3, #2
	beq	.L388
	cmp	r3, #6
	bne	.L329
	cmp	r0, #0
	beq	.L329
	ldrb	r2, [r1, #0]	@ zero_extendqisi2
	ldrb	ip, [r1, #2]	@ zero_extendqisi2
	sub	r3, r0, #1
	cmp	r0, #1
	strb	r2, [r1, #2]
	strb	ip, [r1, #0]
	and	r3, r3, #3
	add	r1, r1, #4
	mov	r2, #1
	bls	.L329
	cmp	r3, #0
	beq	.L325
	cmp	r3, #1
	beq	.L380
	cmp	r3, #2
	beq	.L381
	ldrb	r2, [r1, #0]	@ zero_extendqisi2
	ldrb	ip, [r1, #2]	@ zero_extendqisi2
	strb	r2, [r1, #2]
	strb	ip, [r1, #0]
	mov	r2, #2
	add	r1, r1, #4
.L381:
	ldrb	ip, [r1, #0]	@ zero_extendqisi2
	ldrb	r3, [r1, #2]	@ zero_extendqisi2
	add	r2, r2, #1
	strb	ip, [r1, #2]
	strb	r3, [r1, #0]
	add	r1, r1, #4
.L380:
	ldrb	r3, [r1, #0]	@ zero_extendqisi2
	ldrb	ip, [r1, #2]	@ zero_extendqisi2
	add	r2, r2, #1
	cmp	r0, r2
	strb	ip, [r1, #0]
	strb	r3, [r1, #2]
	add	r1, r1, #4
	bls	.L329
.L325:
	ldrb	ip, [r1, #0]	@ zero_extendqisi2
	ldrb	r3, [r1, #2]	@ zero_extendqisi2
	strb	ip, [r1, #2]
	strb	r3, [r1, #0]
	add	r3, r1, #4
	ldrb	r5, [r1, #4]	@ zero_extendqisi2
	ldrb	r4, [r3, #2]	@ zero_extendqisi2
	strb	r4, [r1, #4]
	strb	r5, [r3, #2]
	add	ip, r3, #4
	ldrb	r4, [r3, #4]	@ zero_extendqisi2
	ldrb	r5, [ip, #2]	@ zero_extendqisi2
	strb	r5, [r3, #4]
	strb	r4, [ip, #2]
	add	r3, r1, #12
	ldrb	ip, [r1, #12]	@ zero_extendqisi2
	ldrb	r4, [r3, #2]	@ zero_extendqisi2
	add	r2, r2, #4
	cmp	r0, r2
	strb	r4, [r1, #12]
	strb	ip, [r3, #2]
	add	r1, r1, #16
	bhi	.L325
	b	.L329
.L387:
	cmp	r3, #2
	beq	.L389
	cmp	r3, #6
	bne	.L329
	cmp	r0, #0
	beq	.L329
	ldrb	ip, [r1, #5]	@ zero_extendqisi2
	ldrb	r4, [r1, #0]	@ zero_extendqisi2
	ldrb	r5, [r1, #4]	@ zero_extendqisi2
	ldrb	r2, [r1, #1]	@ zero_extendqisi2
	sub	r3, r0, #1
	cmp	r0, #1
	strb	ip, [r1, #1]
	strb	r5, [r1, #0]
	strb	r4, [r1, #4]
	strb	r2, [r1, #5]
	and	r3, r3, #3
	add	r1, r1, #8
	mov	ip, #1
	bls	.L329
	cmp	r3, #0
	beq	.L328
	cmp	r3, #1
	beq	.L384
	cmp	r3, #2
	beq	.L385
	ldrb	ip, [r1, #0]	@ zero_extendqisi2
	ldrb	r4, [r1, #4]	@ zero_extendqisi2
	ldrb	r5, [r1, #1]	@ zero_extendqisi2
	ldrb	r2, [r1, #5]	@ zero_extendqisi2
	strb	ip, [r1, #4]
	strb	r4, [r1, #0]
	strb	r2, [r1, #1]
	strb	r5, [r1, #5]
	mov	ip, #2
	add	r1, r1, #8
.L385:
	ldrb	r4, [r1, #0]	@ zero_extendqisi2
	ldrb	r5, [r1, #4]	@ zero_extendqisi2
	ldrb	r3, [r1, #1]	@ zero_extendqisi2
	ldrb	r2, [r1, #5]	@ zero_extendqisi2
	strb	r5, [r1, #0]
	strb	r4, [r1, #4]
	strb	r2, [r1, #1]
	strb	r3, [r1, #5]
	add	ip, ip, #1
	add	r1, r1, #8
.L384:
	ldrb	r4, [r1, #0]	@ zero_extendqisi2
	ldrb	r5, [r1, #4]	@ zero_extendqisi2
	ldrb	r3, [r1, #1]	@ zero_extendqisi2
	ldrb	r2, [r1, #5]	@ zero_extendqisi2
	add	ip, ip, #1
	cmp	r0, ip
	strb	r5, [r1, #0]
	strb	r4, [r1, #4]
	strb	r2, [r1, #1]
	strb	r3, [r1, #5]
	add	r1, r1, #8
	bls	.L329
.L328:
	ldrb	r4, [r1, #0]	@ zero_extendqisi2
	ldrb	r5, [r1, #4]	@ zero_extendqisi2
	ldrb	r3, [r1, #1]	@ zero_extendqisi2
	ldrb	r2, [r1, #5]	@ zero_extendqisi2
	strb	r5, [r1, #0]
	strb	r3, [r1, #5]
	strb	r4, [r1, #4]
	strb	r2, [r1, #1]
	add	r3, r1, #8
	ldrb	r2, [r3, #4]	@ zero_extendqisi2
	ldrb	r5, [r1, #8]	@ zero_extendqisi2
	strb	r2, [r1, #8]
	ldrb	r2, [r3, #1]	@ zero_extendqisi2
	ldrb	r4, [r3, #5]	@ zero_extendqisi2
	strb	r5, [r3, #4]
	strb	r4, [r3, #1]
	strb	r2, [r3, #5]
	add	r2, r3, #8
	ldrb	r5, [r2, #4]	@ zero_extendqisi2
	ldrb	r4, [r3, #8]	@ zero_extendqisi2
	strb	r5, [r3, #8]
	ldrb	r5, [r2, #1]	@ zero_extendqisi2
	ldrb	r3, [r2, #5]	@ zero_extendqisi2
	strb	r4, [r2, #4]
	strb	r3, [r2, #1]
	strb	r5, [r2, #5]
	add	r3, r1, #24
	ldrb	r5, [r3, #4]	@ zero_extendqisi2
	ldrb	r4, [r1, #24]	@ zero_extendqisi2
	strb	r5, [r1, #24]
	add	ip, ip, #4
	ldrb	r5, [r3, #1]	@ zero_extendqisi2
	ldrb	r2, [r3, #5]	@ zero_extendqisi2
	cmp	r0, ip
	strb	r5, [r3, #5]
	strb	r4, [r3, #4]
	strb	r2, [r3, #1]
	add	r1, r1, #32
	bhi	.L328
	b	.L329
.L388:
	cmp	r0, #0
	beq	.L329
	ldrb	r2, [r1, #0]	@ zero_extendqisi2
	ldrb	ip, [r1, #2]	@ zero_extendqisi2
	sub	r4, r0, #1
	cmp	r0, #1
	strb	ip, [r1, #0]
	strb	r2, [r1, #2]
	and	r4, r4, #3
	mov	ip, #1
	add	r2, r1, #3
	bls	.L329
	cmp	r4, #0
	beq	.L324
	cmp	r4, #1
	beq	.L378
	cmp	r4, #2
	beq	.L379
	ldrb	ip, [r2, #2]	@ zero_extendqisi2
	ldrb	r4, [r2, #0]	@ zero_extendqisi2
	strb	ip, [r2, #0]
	strb	r4, [r2, #2]
	mov	ip, r3
	add	r2, r1, #6
.L379:
	ldrb	r1, [r2, #0]	@ zero_extendqisi2
	ldrb	r3, [r2, #2]	@ zero_extendqisi2
	add	ip, ip, #1
	strb	r1, [r2, #2]
	strb	r3, [r2, #0]
	add	r2, r2, #3
.L378:
	ldrb	r3, [r2, #0]	@ zero_extendqisi2
	ldrb	r1, [r2, #2]	@ zero_extendqisi2
	add	ip, ip, #1
	cmp	r0, ip
	strb	r1, [r2, #0]
	strb	r3, [r2, #2]
	add	r2, r2, #3
	bls	.L329
.L324:
	ldrb	r1, [r2, #0]	@ zero_extendqisi2
	ldrb	r3, [r2, #2]	@ zero_extendqisi2
	strb	r1, [r2, #2]
	strb	r3, [r2, #0]
	add	r3, r2, #3
	ldrb	r4, [r3, #2]	@ zero_extendqisi2
	ldrb	r1, [r2, #3]	@ zero_extendqisi2
	strb	r4, [r2, #3]
	strb	r1, [r3, #2]
	add	r3, r2, #6
	ldrb	r4, [r3, #2]	@ zero_extendqisi2
	ldrb	r1, [r2, #6]	@ zero_extendqisi2
	strb	r4, [r2, #6]
	strb	r1, [r3, #2]
	add	r3, r2, #9
	ldrb	r1, [r2, #9]	@ zero_extendqisi2
	ldrb	r4, [r3, #2]	@ zero_extendqisi2
	add	ip, ip, #4
	cmp	r0, ip
	strb	r4, [r2, #9]
	strb	r1, [r3, #2]
	add	r2, r2, #12
	bhi	.L324
	b	.L329
.L389:
	cmp	r0, #0
	beq	.L329
	ldrb	r2, [r1, #1]	@ zero_extendqisi2
	ldrb	ip, [r1, #5]	@ zero_extendqisi2
	ldrb	r5, [r1, #0]	@ zero_extendqisi2
	ldrb	r6, [r1, #4]	@ zero_extendqisi2
	sub	r4, r0, #1
	cmp	r0, #1
	strb	ip, [r1, #1]
	strb	r2, [r1, #5]
	strb	r6, [r1, #0]
	strb	r5, [r1, #4]
	and	r4, r4, #3
	mov	ip, #1
	add	r2, r1, #6
	bls	.L329
	cmp	r4, #0
	beq	.L327
	cmp	r4, #1
	beq	.L382
	cmp	r4, #2
	beq	.L383
	ldrb	ip, [r2, #5]	@ zero_extendqisi2
	ldrb	r4, [r2, #0]	@ zero_extendqisi2
	ldrb	r5, [r2, #4]	@ zero_extendqisi2
	ldrb	r6, [r2, #1]	@ zero_extendqisi2
	strb	r5, [r2, #0]
	strb	ip, [r2, #1]
	strb	r6, [r2, #5]
	strb	r4, [r2, #4]
	mov	ip, r3
	add	r2, r1, #12
.L383:
	ldrb	r4, [r2, #0]	@ zero_extendqisi2
	ldrb	r5, [r2, #4]	@ zero_extendqisi2
	ldrb	r3, [r2, #1]	@ zero_extendqisi2
	ldrb	r1, [r2, #5]	@ zero_extendqisi2
	strb	r5, [r2, #0]
	strb	r4, [r2, #4]
	strb	r1, [r2, #1]
	strb	r3, [r2, #5]
	add	ip, ip, #1
	add	r2, r2, #6
.L382:
	ldrb	r4, [r2, #0]	@ zero_extendqisi2
	ldrb	r5, [r2, #4]	@ zero_extendqisi2
	ldrb	r3, [r2, #1]	@ zero_extendqisi2
	ldrb	r1, [r2, #5]	@ zero_extendqisi2
	add	ip, ip, #1
	cmp	r0, ip
	strb	r5, [r2, #0]
	strb	r4, [r2, #4]
	strb	r1, [r2, #1]
	strb	r3, [r2, #5]
	add	r2, r2, #6
	bls	.L329
.L327:
	ldrb	r4, [r2, #0]	@ zero_extendqisi2
	ldrb	r5, [r2, #4]	@ zero_extendqisi2
	ldrb	r3, [r2, #1]	@ zero_extendqisi2
	ldrb	r1, [r2, #5]	@ zero_extendqisi2
	strb	r4, [r2, #4]
	strb	r3, [r2, #5]
	strb	r5, [r2, #0]
	strb	r1, [r2, #1]
	add	r3, r2, #6
	ldrb	r1, [r3, #4]	@ zero_extendqisi2
	ldrb	r4, [r2, #6]	@ zero_extendqisi2
	strb	r1, [r2, #6]
	ldrb	r5, [r3, #1]	@ zero_extendqisi2
	ldrb	r1, [r3, #5]	@ zero_extendqisi2
	strb	r4, [r3, #4]
	strb	r5, [r3, #5]
	strb	r1, [r3, #1]
	add	r3, r2, #12
	ldrb	r1, [r3, #4]	@ zero_extendqisi2
	ldrb	r4, [r2, #12]	@ zero_extendqisi2
	strb	r1, [r2, #12]
	ldrb	r5, [r3, #1]	@ zero_extendqisi2
	ldrb	r1, [r3, #5]	@ zero_extendqisi2
	strb	r4, [r3, #4]
	strb	r5, [r3, #5]
	strb	r1, [r3, #1]
	add	r3, r2, #18
	ldrb	r5, [r3, #4]	@ zero_extendqisi2
	ldrb	r4, [r2, #18]	@ zero_extendqisi2
	strb	r5, [r2, #18]
	add	ip, ip, #4
	ldrb	r5, [r3, #1]	@ zero_extendqisi2
	ldrb	r1, [r3, #5]	@ zero_extendqisi2
	cmp	r0, ip
	strb	r5, [r3, #5]
	strb	r4, [r3, #4]
	strb	r1, [r3, #1]
	add	r2, r2, #24
	bhi	.L327
	b	.L329
	.size	png_do_bgr, .-png_do_bgr
	.section	.text.png_set_user_transform_info,"ax",%progbits
	.align	2
	.global	png_set_user_transform_info
	.hidden	png_set_user_transform_info
	.type	png_set_user_transform_info, %function
png_set_user_transform_info:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	strneb	r3, [r0, #129]
	strne	r1, [r0, #124]
	strneb	r2, [r0, #128]
	bx	lr
	.size	png_set_user_transform_info, .-png_set_user_transform_info
	.section	.text.png_get_user_transform_ptr,"ax",%progbits
	.align	2
	.global	png_get_user_transform_ptr
	.hidden	png_get_user_transform_ptr
	.type	png_get_user_transform_ptr, %function
png_get_user_transform_ptr:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	ldrne	r0, [r0, #124]
	bx	lr
	.size	png_get_user_transform_ptr, .-png_get_user_transform_ptr
	.section	.text.png_set_add_alpha,"ax",%progbits
	.align	2
	.global	png_set_add_alpha
	.hidden	png_set_add_alpha
	.type	png_set_add_alpha, %function
png_set_add_alpha:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	bxeq	lr
	ldr	ip, [r0, #140]
	mov	r3, #328
	cmp	r2, #1
	orr	r2, ip, #32768
	add	ip, r3, #2
	ldr	r3, [r0, #136]
	orreq	r3, r3, #128
	bicne	r3, r3, #128
	str	r3, [r0, #136]
	ldrb	r3, [r0, #322]	@ zero_extendqisi2
	cmp	r3, #2
	and	r1, r1, #255
	moveq	r3, #4
	strh	r1, [r0, ip]	@ movhi
	str	r2, [r0, #140]
	streqb	r3, [r0, #327]
	beq	.L402
	cmp	r3, #0
	bne	.L402
	ldrb	r1, [r0, #323]	@ zero_extendqisi2
	cmp	r1, #7
	movhi	r1, #2
	strhib	r1, [r0, #327]
.L402:
	orr	r2, r2, #16777216
	str	r2, [r0, #140]
	bx	lr
	.size	png_set_add_alpha, .-png_set_add_alpha
	.section	.rodata
	.align	2
	.set	.LANCHOR0,. + 0
	.type	onebppswaptable, %object
	.size	onebppswaptable, 256
onebppswaptable:
	.byte	0
	.byte	-128
	.byte	64
	.byte	-64
	.byte	32
	.byte	-96
	.byte	96
	.byte	-32
	.byte	16
	.byte	-112
	.byte	80
	.byte	-48
	.byte	48
	.byte	-80
	.byte	112
	.byte	-16
	.byte	8
	.byte	-120
	.byte	72
	.byte	-56
	.byte	40
	.byte	-88
	.byte	104
	.byte	-24
	.byte	24
	.byte	-104
	.byte	88
	.byte	-40
	.byte	56
	.byte	-72
	.byte	120
	.byte	-8
	.byte	4
	.byte	-124
	.byte	68
	.byte	-60
	.byte	36
	.byte	-92
	.byte	100
	.byte	-28
	.byte	20
	.byte	-108
	.byte	84
	.byte	-44
	.byte	52
	.byte	-76
	.byte	116
	.byte	-12
	.byte	12
	.byte	-116
	.byte	76
	.byte	-52
	.byte	44
	.byte	-84
	.byte	108
	.byte	-20
	.byte	28
	.byte	-100
	.byte	92
	.byte	-36
	.byte	60
	.byte	-68
	.byte	124
	.byte	-4
	.byte	2
	.byte	-126
	.byte	66
	.byte	-62
	.byte	34
	.byte	-94
	.byte	98
	.byte	-30
	.byte	18
	.byte	-110
	.byte	82
	.byte	-46
	.byte	50
	.byte	-78
	.byte	114
	.byte	-14
	.byte	10
	.byte	-118
	.byte	74
	.byte	-54
	.byte	42
	.byte	-86
	.byte	106
	.byte	-22
	.byte	26
	.byte	-102
	.byte	90
	.byte	-38
	.byte	58
	.byte	-70
	.byte	122
	.byte	-6
	.byte	6
	.byte	-122
	.byte	70
	.byte	-58
	.byte	38
	.byte	-90
	.byte	102
	.byte	-26
	.byte	22
	.byte	-106
	.byte	86
	.byte	-42
	.byte	54
	.byte	-74
	.byte	118
	.byte	-10
	.byte	14
	.byte	-114
	.byte	78
	.byte	-50
	.byte	46
	.byte	-82
	.byte	110
	.byte	-18
	.byte	30
	.byte	-98
	.byte	94
	.byte	-34
	.byte	62
	.byte	-66
	.byte	126
	.byte	-2
	.byte	1
	.byte	-127
	.byte	65
	.byte	-63
	.byte	33
	.byte	-95
	.byte	97
	.byte	-31
	.byte	17
	.byte	-111
	.byte	81
	.byte	-47
	.byte	49
	.byte	-79
	.byte	113
	.byte	-15
	.byte	9
	.byte	-119
	.byte	73
	.byte	-55
	.byte	41
	.byte	-87
	.byte	105
	.byte	-23
	.byte	25
	.byte	-103
	.byte	89
	.byte	-39
	.byte	57
	.byte	-71
	.byte	121
	.byte	-7
	.byte	5
	.byte	-123
	.byte	69
	.byte	-59
	.byte	37
	.byte	-91
	.byte	101
	.byte	-27
	.byte	21
	.byte	-107
	.byte	85
	.byte	-43
	.byte	53
	.byte	-75
	.byte	117
	.byte	-11
	.byte	13
	.byte	-115
	.byte	77
	.byte	-51
	.byte	45
	.byte	-83
	.byte	109
	.byte	-19
	.byte	29
	.byte	-99
	.byte	93
	.byte	-35
	.byte	61
	.byte	-67
	.byte	125
	.byte	-3
	.byte	3
	.byte	-125
	.byte	67
	.byte	-61
	.byte	35
	.byte	-93
	.byte	99
	.byte	-29
	.byte	19
	.byte	-109
	.byte	83
	.byte	-45
	.byte	51
	.byte	-77
	.byte	115
	.byte	-13
	.byte	11
	.byte	-117
	.byte	75
	.byte	-53
	.byte	43
	.byte	-85
	.byte	107
	.byte	-21
	.byte	27
	.byte	-101
	.byte	91
	.byte	-37
	.byte	59
	.byte	-69
	.byte	123
	.byte	-5
	.byte	7
	.byte	-121
	.byte	71
	.byte	-57
	.byte	39
	.byte	-89
	.byte	103
	.byte	-25
	.byte	23
	.byte	-105
	.byte	87
	.byte	-41
	.byte	55
	.byte	-73
	.byte	119
	.byte	-9
	.byte	15
	.byte	-113
	.byte	79
	.byte	-49
	.byte	47
	.byte	-81
	.byte	111
	.byte	-17
	.byte	31
	.byte	-97
	.byte	95
	.byte	-33
	.byte	63
	.byte	-65
	.byte	127
	.byte	-1
	.type	twobppswaptable, %object
	.size	twobppswaptable, 256
twobppswaptable:
	.byte	0
	.byte	64
	.byte	-128
	.byte	-64
	.byte	16
	.byte	80
	.byte	-112
	.byte	-48
	.byte	32
	.byte	96
	.byte	-96
	.byte	-32
	.byte	48
	.byte	112
	.byte	-80
	.byte	-16
	.byte	4
	.byte	68
	.byte	-124
	.byte	-60
	.byte	20
	.byte	84
	.byte	-108
	.byte	-44
	.byte	36
	.byte	100
	.byte	-92
	.byte	-28
	.byte	52
	.byte	116
	.byte	-76
	.byte	-12
	.byte	8
	.byte	72
	.byte	-120
	.byte	-56
	.byte	24
	.byte	88
	.byte	-104
	.byte	-40
	.byte	40
	.byte	104
	.byte	-88
	.byte	-24
	.byte	56
	.byte	120
	.byte	-72
	.byte	-8
	.byte	12
	.byte	76
	.byte	-116
	.byte	-52
	.byte	28
	.byte	92
	.byte	-100
	.byte	-36
	.byte	44
	.byte	108
	.byte	-84
	.byte	-20
	.byte	60
	.byte	124
	.byte	-68
	.byte	-4
	.byte	1
	.byte	65
	.byte	-127
	.byte	-63
	.byte	17
	.byte	81
	.byte	-111
	.byte	-47
	.byte	33
	.byte	97
	.byte	-95
	.byte	-31
	.byte	49
	.byte	113
	.byte	-79
	.byte	-15
	.byte	5
	.byte	69
	.byte	-123
	.byte	-59
	.byte	21
	.byte	85
	.byte	-107
	.byte	-43
	.byte	37
	.byte	101
	.byte	-91
	.byte	-27
	.byte	53
	.byte	117
	.byte	-75
	.byte	-11
	.byte	9
	.byte	73
	.byte	-119
	.byte	-55
	.byte	25
	.byte	89
	.byte	-103
	.byte	-39
	.byte	41
	.byte	105
	.byte	-87
	.byte	-23
	.byte	57
	.byte	121
	.byte	-71
	.byte	-7
	.byte	13
	.byte	77
	.byte	-115
	.byte	-51
	.byte	29
	.byte	93
	.byte	-99
	.byte	-35
	.byte	45
	.byte	109
	.byte	-83
	.byte	-19
	.byte	61
	.byte	125
	.byte	-67
	.byte	-3
	.byte	2
	.byte	66
	.byte	-126
	.byte	-62
	.byte	18
	.byte	82
	.byte	-110
	.byte	-46
	.byte	34
	.byte	98
	.byte	-94
	.byte	-30
	.byte	50
	.byte	114
	.byte	-78
	.byte	-14
	.byte	6
	.byte	70
	.byte	-122
	.byte	-58
	.byte	22
	.byte	86
	.byte	-106
	.byte	-42
	.byte	38
	.byte	102
	.byte	-90
	.byte	-26
	.byte	54
	.byte	118
	.byte	-74
	.byte	-10
	.byte	10
	.byte	74
	.byte	-118
	.byte	-54
	.byte	26
	.byte	90
	.byte	-102
	.byte	-38
	.byte	42
	.byte	106
	.byte	-86
	.byte	-22
	.byte	58
	.byte	122
	.byte	-70
	.byte	-6
	.byte	14
	.byte	78
	.byte	-114
	.byte	-50
	.byte	30
	.byte	94
	.byte	-98
	.byte	-34
	.byte	46
	.byte	110
	.byte	-82
	.byte	-18
	.byte	62
	.byte	126
	.byte	-66
	.byte	-2
	.byte	3
	.byte	67
	.byte	-125
	.byte	-61
	.byte	19
	.byte	83
	.byte	-109
	.byte	-45
	.byte	35
	.byte	99
	.byte	-93
	.byte	-29
	.byte	51
	.byte	115
	.byte	-77
	.byte	-13
	.byte	7
	.byte	71
	.byte	-121
	.byte	-57
	.byte	23
	.byte	87
	.byte	-105
	.byte	-41
	.byte	39
	.byte	103
	.byte	-89
	.byte	-25
	.byte	55
	.byte	119
	.byte	-73
	.byte	-9
	.byte	11
	.byte	75
	.byte	-117
	.byte	-53
	.byte	27
	.byte	91
	.byte	-101
	.byte	-37
	.byte	43
	.byte	107
	.byte	-85
	.byte	-21
	.byte	59
	.byte	123
	.byte	-69
	.byte	-5
	.byte	15
	.byte	79
	.byte	-113
	.byte	-49
	.byte	31
	.byte	95
	.byte	-97
	.byte	-33
	.byte	47
	.byte	111
	.byte	-81
	.byte	-17
	.byte	63
	.byte	127
	.byte	-65
	.byte	-1
	.type	fourbppswaptable, %object
	.size	fourbppswaptable, 256
fourbppswaptable:
	.byte	0
	.byte	16
	.byte	32
	.byte	48
	.byte	64
	.byte	80
	.byte	96
	.byte	112
	.byte	-128
	.byte	-112
	.byte	-96
	.byte	-80
	.byte	-64
	.byte	-48
	.byte	-32
	.byte	-16
	.byte	1
	.byte	17
	.byte	33
	.byte	49
	.byte	65
	.byte	81
	.byte	97
	.byte	113
	.byte	-127
	.byte	-111
	.byte	-95
	.byte	-79
	.byte	-63
	.byte	-47
	.byte	-31
	.byte	-15
	.byte	2
	.byte	18
	.byte	34
	.byte	50
	.byte	66
	.byte	82
	.byte	98
	.byte	114
	.byte	-126
	.byte	-110
	.byte	-94
	.byte	-78
	.byte	-62
	.byte	-46
	.byte	-30
	.byte	-14
	.byte	3
	.byte	19
	.byte	35
	.byte	51
	.byte	67
	.byte	83
	.byte	99
	.byte	115
	.byte	-125
	.byte	-109
	.byte	-93
	.byte	-77
	.byte	-61
	.byte	-45
	.byte	-29
	.byte	-13
	.byte	4
	.byte	20
	.byte	36
	.byte	52
	.byte	68
	.byte	84
	.byte	100
	.byte	116
	.byte	-124
	.byte	-108
	.byte	-92
	.byte	-76
	.byte	-60
	.byte	-44
	.byte	-28
	.byte	-12
	.byte	5
	.byte	21
	.byte	37
	.byte	53
	.byte	69
	.byte	85
	.byte	101
	.byte	117
	.byte	-123
	.byte	-107
	.byte	-91
	.byte	-75
	.byte	-59
	.byte	-43
	.byte	-27
	.byte	-11
	.byte	6
	.byte	22
	.byte	38
	.byte	54
	.byte	70
	.byte	86
	.byte	102
	.byte	118
	.byte	-122
	.byte	-106
	.byte	-90
	.byte	-74
	.byte	-58
	.byte	-42
	.byte	-26
	.byte	-10
	.byte	7
	.byte	23
	.byte	39
	.byte	55
	.byte	71
	.byte	87
	.byte	103
	.byte	119
	.byte	-121
	.byte	-105
	.byte	-89
	.byte	-73
	.byte	-57
	.byte	-41
	.byte	-25
	.byte	-9
	.byte	8
	.byte	24
	.byte	40
	.byte	56
	.byte	72
	.byte	88
	.byte	104
	.byte	120
	.byte	-120
	.byte	-104
	.byte	-88
	.byte	-72
	.byte	-56
	.byte	-40
	.byte	-24
	.byte	-8
	.byte	9
	.byte	25
	.byte	41
	.byte	57
	.byte	73
	.byte	89
	.byte	105
	.byte	121
	.byte	-119
	.byte	-103
	.byte	-87
	.byte	-71
	.byte	-55
	.byte	-39
	.byte	-23
	.byte	-7
	.byte	10
	.byte	26
	.byte	42
	.byte	58
	.byte	74
	.byte	90
	.byte	106
	.byte	122
	.byte	-118
	.byte	-102
	.byte	-86
	.byte	-70
	.byte	-54
	.byte	-38
	.byte	-22
	.byte	-6
	.byte	11
	.byte	27
	.byte	43
	.byte	59
	.byte	75
	.byte	91
	.byte	107
	.byte	123
	.byte	-117
	.byte	-101
	.byte	-85
	.byte	-69
	.byte	-53
	.byte	-37
	.byte	-21
	.byte	-5
	.byte	12
	.byte	28
	.byte	44
	.byte	60
	.byte	76
	.byte	92
	.byte	108
	.byte	124
	.byte	-116
	.byte	-100
	.byte	-84
	.byte	-68
	.byte	-52
	.byte	-36
	.byte	-20
	.byte	-4
	.byte	13
	.byte	29
	.byte	45
	.byte	61
	.byte	77
	.byte	93
	.byte	109
	.byte	125
	.byte	-115
	.byte	-99
	.byte	-83
	.byte	-67
	.byte	-51
	.byte	-35
	.byte	-19
	.byte	-3
	.byte	14
	.byte	30
	.byte	46
	.byte	62
	.byte	78
	.byte	94
	.byte	110
	.byte	126
	.byte	-114
	.byte	-98
	.byte	-82
	.byte	-66
	.byte	-50
	.byte	-34
	.byte	-18
	.byte	-2
	.byte	15
	.byte	31
	.byte	47
	.byte	63
	.byte	79
	.byte	95
	.byte	111
	.byte	127
	.byte	-113
	.byte	-97
	.byte	-81
	.byte	-65
	.byte	-49
	.byte	-33
	.byte	-17
	.byte	-1
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
