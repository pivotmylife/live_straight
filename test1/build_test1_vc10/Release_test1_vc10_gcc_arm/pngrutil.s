	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"pngrutil.c"
	.section	.text.png_get_uint_32,"ax",%progbits
	.align	2
	.global	png_get_uint_32
	.hidden	png_get_uint_32
	.type	png_get_uint_32, %function
png_get_uint_32:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldrb	r3, [r0, #3]	@ zero_extendqisi2
	ldrb	r1, [r0, #0]	@ zero_extendqisi2
	ldrb	r2, [r0, #1]	@ zero_extendqisi2
	add	ip, r3, r1, asl #24
	ldrb	r0, [r0, #2]	@ zero_extendqisi2
	add	r3, ip, r2, asl #16
	add	r0, r3, r0, asl #8
	bx	lr
	.size	png_get_uint_32, .-png_get_uint_32
	.section	.text.png_get_int_32,"ax",%progbits
	.align	2
	.global	png_get_int_32
	.hidden	png_get_int_32
	.type	png_get_int_32, %function
png_get_int_32:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldrb	r3, [r0, #3]	@ zero_extendqisi2
	ldrb	r1, [r0, #0]	@ zero_extendqisi2
	ldrb	r2, [r0, #1]	@ zero_extendqisi2
	add	ip, r3, r1, asl #24
	ldrb	r0, [r0, #2]	@ zero_extendqisi2
	add	r3, ip, r2, asl #16
	add	r0, r3, r0, asl #8
	bx	lr
	.size	png_get_int_32, .-png_get_int_32
	.section	.text.png_get_uint_16,"ax",%progbits
	.align	2
	.global	png_get_uint_16
	.hidden	png_get_uint_16
	.type	png_get_uint_16, %function
png_get_uint_16:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldrb	r2, [r0, #0]	@ zero_extendqisi2
	ldrb	r3, [r0, #1]	@ zero_extendqisi2
	add	r1, r3, r2, asl #8
	mov	r0, r1, asl #16
	mov	r0, r0, lsr #16
	bx	lr
	.size	png_get_uint_16, .-png_get_uint_16
	.section	.text.png_get_uint_31,"ax",%progbits
	.align	2
	.global	png_get_uint_31
	.hidden	png_get_uint_31
	.type	png_get_uint_31, %function
png_get_uint_31:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	ldrb	r3, [r1, #3]	@ zero_extendqisi2
	ldrb	r4, [r1, #0]	@ zero_extendqisi2
	ldrb	r2, [r1, #1]	@ zero_extendqisi2
	add	ip, r3, r4, asl #24
	ldrb	r3, [r1, #2]	@ zero_extendqisi2
	add	r4, ip, r2, asl #16
	adds	r4, r4, r3, asl #8
	bmi	.L11
.L8:
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
.L11:
	ldr	r1, .L12
	bl	png_error
	b	.L8
.L13:
	.align	2
.L12:
	.word	.LC0
	.size	png_get_uint_31, .-png_get_uint_31
	.global	__aeabi_uidiv
	.section	.text.png_read_start_row,"ax",%progbits
	.align	2
	.global	png_read_start_row
	.hidden	png_read_start_row
	.type	png_read_start_row, %function
png_read_start_row:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r1, #0
	stmfd	sp!, {r4, r5, r6, lr}
	mov	r4, r0
	str	r1, [r0, #148]
	bl	png_init_read_transformations
	ldrb	r3, [r4, #319]	@ zero_extendqisi2
	cmp	r3, #0
	beq	.L15
	ldr	r5, [r4, #140]
	tst	r5, #2
	beq	.L45
	ldr	r1, [r4, #232]
	str	r1, [r4, #236]
.L17:
	ldrb	r3, [r4, #320]	@ zero_extendqisi2
	ldr	r2, .L51
	ldr	r0, .L51+4
	ldr	r6, [r4, #228]
	ldr	r1, [r2, r3, asl #2]
	ldr	ip, [r0, r3, asl #2]
	sub	lr, r6, #1
	add	r2, lr, r1
	rsb	r0, ip, r2
	bl	__aeabi_uidiv
	ldrb	r3, [r4, #325]	@ zero_extendqisi2
	cmp	r3, #7
	str	r0, [r4, #252]
	mulls	r0, r3, r0
	movhi	r2, r3, lsr #3
	mulhi	r0, r2, r0
	addls	r0, r0, #7
	movls	r0, r0, lsr #3
	add	r0, r0, #1
	str	r0, [r4, #248]
	b	.L20
.L15:
	ldr	ip, [r4, #244]
	ldr	r1, [r4, #232]
	ldr	r6, [r4, #228]
	add	r5, ip, #1
	str	r5, [r4, #248]
	str	r1, [r4, #236]
	str	r6, [r4, #252]
	ldr	r5, [r4, #140]
	ldrb	r3, [r4, #325]	@ zero_extendqisi2
.L20:
	tst	r5, #4
	beq	.L21
	ldrb	lr, [r4, #323]	@ zero_extendqisi2
	cmp	lr, #7
	movls	r3, #8
.L21:
	ands	r0, r5, #4096
	beq	.L22
	ldrb	r2, [r4, #322]	@ zero_extendqisi2
	cmp	r2, #3
	beq	.L46
	cmp	r2, #0
	beq	.L47
	cmp	r2, #2
	bne	.L22
	mov	lr, #308
	add	ip, lr, #2
	ldrh	r2, [r4, ip]
	cmp	r2, #0
	ldrne	r2, .L51+8
	movne	r3, r3, asl #2
	smullne	r1, r2, r3, r2
	subne	r3, r2, r3, asr #31
.L22:
	ands	r1, r5, #32768
	beq	.L27
	ldrb	r2, [r4, #322]	@ zero_extendqisi2
	cmp	r2, #3
	beq	.L28
	cmp	r2, #0
	beq	.L48
	cmp	r2, #2
	bne	.L27
	cmp	r3, #32
	movgt	r3, #64
	ble	.L28
.L27:
	tst	r5, #16384
	beq	.L30
	mov	lr, #308
	add	ip, lr, #2
	ldrh	r2, [r4, ip]
	cmp	r2, #0
	beq	.L31
	cmp	r0, #0
	beq	.L31
.L32:
	cmp	r3, #16
	bgt	.L35
	mov	r3, #32
.L30:
	tst	r5, #1048576
	beq	.L37
	ldrb	r0, [r4, #129]	@ zero_extendqisi2
	ldrb	r1, [r4, #128]	@ zero_extendqisi2
	mul	r0, r1, r0
	cmp	r3, r0
	movlt	r3, r0
.L37:
	add	r6, r6, #7
	cmp	r3, #7
	bic	r6, r6, #7
	movgt	r5, r3, lsr #3
	mulle	r5, r6, r3
	mulgt	r5, r6, r5
	movle	r5, r5, lsr #3
	add	ip, r3, #7
	add	r2, r5, #1
	add	r5, r2, ip, asr #3
	ldr	r3, [r4, #668]
	add	r5, r5, #64
	cmp	r5, r3
	bhi	.L49
	ldr	r3, [r4, #244]
	cmn	r3, #1
	beq	.L50
.L41:
	ldr	lr, [r4, #672]
	add	r3, r3, #1
	cmp	r3, lr
	ldrls	r1, [r4, #260]
	bls	.L43
	mov	r0, r4
	ldr	r1, [r4, #260]
	bl	png_free
	ldr	r3, [r4, #244]
	mov	r0, r4
	add	r1, r3, #1
	bl	png_malloc
	mov	r1, r0
	ldr	r0, [r4, #244]
	add	r3, r0, #1
	str	r1, [r4, #260]
	str	r3, [r4, #672]
.L43:
	mov	r2, #0
	mov	r0, r4
	bl	png_memset_check
	ldr	ip, [r4, #136]
	orr	r2, ip, #64
	str	r2, [r4, #136]
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L31:
	cmp	r1, #0
	bne	.L32
	ldrb	r2, [r4, #322]	@ zero_extendqisi2
	cmp	r2, #4
	beq	.L32
	cmp	r3, #8
	bgt	.L36
	cmp	r2, #6
	movne	r3, #24
	moveq	r3, #32
	b	.L30
.L45:
	ldr	r3, .L51+12
	ldr	lr, [r4, #232]
	ldr	r6, .L51+16
	ldr	r1, [r3, #0]
	sub	ip, lr, #1
	ldr	r2, [r6, #0]
	add	r0, ip, r1
	rsb	r0, r2, r0
	bl	__aeabi_uidiv
	str	r0, [r4, #236]
	b	.L17
.L36:
	cmp	r2, #6
	movne	r3, #48
	bne	.L30
.L35:
	mov	r3, #64
	b	.L30
.L48:
	cmp	r3, #8
	movle	r3, #16
	ble	.L27
.L28:
	mov	r3, #32
	b	.L27
.L49:
	mov	r0, r4
	ldr	r1, [r4, #620]
	bl	png_free
	mov	r1, r5
	mov	r0, r4
	bl	png_malloc
	ldr	r3, [r4, #244]
	add	r1, r0, #32
	cmn	r3, #1
	str	r1, [r4, #264]
	str	r5, [r4, #668]
	str	r0, [r4, #620]
	bne	.L41
.L50:
	mov	r0, r4
	ldr	r1, .L51+20
	bl	png_error
	ldr	r3, [r4, #244]
	b	.L41
.L47:
	mov	lr, #308
	add	ip, lr, #2
	ldrh	r1, [r4, ip]
	cmp	r3, #8
	movlt	r3, #8
	cmp	r1, #0
	movne	r3, r3, asl #1
	b	.L22
.L46:
	mov	r2, #308
	add	r3, r2, #2
	ldrh	r3, [r4, r3]
	cmp	r3, #0
	movne	r3, #32
	moveq	r3, #24
	b	.L22
.L52:
	.align	2
.L51:
	.word	png_pass_inc
	.word	png_pass_start
	.word	1431655766
	.word	png_pass_yinc
	.word	png_pass_ystart
	.word	.LC1
	.size	png_read_start_row, .-png_read_start_row
	.section	.text.png_crc_error,"ax",%progbits
	.align	2
	.global	png_crc_error
	.hidden	png_crc_error
	.type	png_crc_error, %function
png_crc_error:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	ldrb	r3, [r0, #312]	@ zero_extendqisi2
	tst	r3, #32
	sub	sp, sp, #8
	mov	r4, r0
	ldr	r3, [r0, #136]
	beq	.L54
	and	r0, r3, #768
	cmp	r0, #768
	beq	.L55
.L56:
	mov	r0, r4
	add	r1, sp, #4
	mov	r2, #4
	bl	png_read_data
	ldrb	r0, [sp, #5]	@ zero_extendqisi2
	ldrb	r2, [sp, #4]	@ zero_extendqisi2
	mov	ip, r0, asl #16
	ldrb	r3, [sp, #7]	@ zero_extendqisi2
	add	r1, ip, r2, asl #24
	ldrb	r2, [sp, #6]	@ zero_extendqisi2
	add	r0, r1, r3
	ldr	ip, [r4, #300]
	add	r1, r0, r2, asl #8
	subs	r0, ip, r1
	movne	r0, #1
.L57:
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	bx	lr
.L54:
	tst	r3, #2048
	beq	.L56
.L55:
	mov	r0, r4
	add	r1, sp, #4
	mov	r2, #4
	bl	png_read_data
	mov	r0, #0
	b	.L57
	.size	png_crc_error, .-png_crc_error
	.section	.text.png_read_filter_row,"ax",%progbits
	.align	2
	.global	png_read_filter_row
	.hidden	png_read_filter_row
	.type	png_read_filter_row, %function
png_read_filter_row:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	sub	sp, sp, #12
	ldr	ip, [sp, #48]
	cmp	ip, #4
	ldrls	pc, [pc, ip, asl #2]
	b	.L60
.L66:
	.word	.L96
	.word	.L62
	.word	.L63
	.word	.L64
	.word	.L65
.L60:
	ldr	r1, .L270
	str	r2, [sp, #4]
	bl	png_warning
	ldr	r2, [sp, #4]
	mov	r3, #0
	strb	r3, [r2, #0]
.L96:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L65:
	ldrb	r6, [r1, #11]	@ zero_extendqisi2
	add	r0, r6, #7
	mov	r6, r0, asr #3
	ldr	r4, [r1, #4]
	cmp	r6, #0
	rsb	r4, r6, r4
	moveq	ip, r2
	moveq	r6, r3
	beq	.L84
	orr	ip, r2, r3
	tst	ip, #3
	add	r1, r2, #4
	add	ip, r3, #4
	movne	r0, #0
	moveq	r0, #1
	cmp	r3, r1
	cmpls	r2, ip
	movls	r1, #0
	movhi	r1, #1
	cmp	r6, #3
	movls	r0, #0
	andhi	r0, r0, #1
	ands	r0, r0, r1
	beq	.L85
	mov	sl, r6, lsr #2
	movs	r7, sl, asl #2
	moveq	ip, r3
	moveq	r0, r2
	beq	.L87
	ldr	r0, [r3, #0]
	ldr	r8, [r2, #0]
	bic	ip, r0, #-2147483648
	eor	r1, r8, r0
	bic	r0, r8, #-2147483648
	bic	r0, r0, #8388608
	bic	r8, ip, #8388608
	bic	r1, r1, #2130706432
	bic	ip, r8, #32768
	bic	r1, r1, #8323072
	bic	r8, r0, #32768
	bic	ip, ip, #128
	bic	r0, r8, #128
	bic	r8, r1, #32512
	add	r0, ip, r0
	bic	ip, r8, #127
	mov	r8, #1
	eor	r1, r0, ip
	cmp	r8, sl
	sub	r0, sl, #1
	str	r1, [r2, #0]
	and	r0, r0, #3
	mov	r1, #4
	bcs	.L263
	cmp	r0, #0
	beq	.L88
	cmp	r0, #1
	beq	.L258
	cmp	r0, #2
	beq	.L259
	ldr	ip, [r3, #4]
	ldr	r0, [r2, #4]
	bic	r5, ip, #-2147483648
	eor	r1, r0, ip
	bic	r8, r0, #-2147483648
	bic	ip, r5, #8388608
	bic	r0, r8, #8388608
	bic	r5, r1, #2130706432
	bic	r1, r5, #8323072
	bic	r8, ip, #32768
	bic	r0, r0, #32768
	bic	ip, r8, #128
	bic	r5, r0, #128
	bic	r8, r1, #32512
	add	r0, ip, r5
	bic	ip, r8, #127
	eor	r1, r0, ip
	str	r1, [r2, #4]
	mov	r8, #2
	mov	r1, #8
.L259:
	ldr	r5, [r3, r1]
	ldr	ip, [r2, r1]
	eor	r0, ip, r5
	bic	r5, r5, #-2147483648
	bic	ip, ip, #-2147483648
	bic	r5, r5, #8388608
	bic	ip, ip, #8388608
	bic	r0, r0, #2130706432
	bic	r5, r5, #32768
	bic	ip, ip, #32768
	bic	r0, r0, #8323072
	bic	r5, r5, #128
	bic	ip, ip, #128
	bic	r0, r0, #32512
	add	ip, r5, ip
	bic	r0, r0, #127
	eor	r5, ip, r0
	str	r5, [r2, r1]
	add	r8, r8, #1
	add	r1, r1, #4
.L258:
	ldr	r5, [r3, r1]
	ldr	ip, [r2, r1]
	eor	r0, ip, r5
	bic	r5, r5, #-2147483648
	bic	ip, ip, #-2147483648
	bic	r5, r5, #8388608
	bic	ip, ip, #8388608
	bic	r0, r0, #2130706432
	bic	r5, r5, #32768
	bic	ip, ip, #32768
	bic	r0, r0, #8323072
	bic	r5, r5, #128
	bic	ip, ip, #128
	bic	r0, r0, #32512
	add	ip, r5, ip
	bic	r0, r0, #127
	add	r8, r8, #1
	eor	r0, ip, r0
	cmp	r8, sl
	str	r0, [r2, r1]
	add	r1, r1, #4
	bcs	.L263
.L88:
	ldr	ip, [r3, r1]
	ldr	r9, [r2, r1]
	bic	r5, ip, #-2147483648
	eor	r0, r9, ip
	bic	ip, r9, #-2147483648
	bic	ip, ip, #8388608
	bic	r9, r5, #8388608
	bic	r0, r0, #2130706432
	bic	r5, r9, #32768
	bic	r0, r0, #8323072
	bic	r9, ip, #32768
	bic	r5, r5, #128
	bic	ip, r9, #128
	bic	r9, r0, #32512
	add	ip, r5, ip
	bic	r0, r9, #127
	eor	r9, ip, r0
	str	r9, [r2, r1]
	add	r9, r1, #4
	ldr	r5, [r3, r9]
	ldr	ip, [r2, r9]
	eor	r0, ip, r5
	bic	r5, r5, #-2147483648
	bic	ip, ip, #-2147483648
	bic	r5, r5, #8388608
	bic	ip, ip, #8388608
	bic	r0, r0, #2130706432
	bic	r5, r5, #32768
	bic	ip, ip, #32768
	bic	r0, r0, #8323072
	bic	r5, r5, #128
	bic	r0, r0, #32512
	bic	ip, ip, #128
	add	ip, r5, ip
	bic	r5, r0, #127
	eor	r0, ip, r5
	str	r0, [r2, r9]
	add	r9, r9, #4
	ldr	r5, [r3, r9]
	ldr	ip, [r2, r9]
	eor	r0, ip, r5
	bic	r5, r5, #-2147483648
	bic	ip, ip, #-2147483648
	bic	r5, r5, #8388608
	bic	ip, ip, #8388608
	bic	r0, r0, #2130706432
	bic	r5, r5, #32768
	bic	ip, ip, #32768
	bic	r0, r0, #8323072
	bic	r5, r5, #128
	bic	r0, r0, #32512
	bic	ip, ip, #128
	add	ip, r5, ip
	bic	r5, r0, #127
	eor	r0, ip, r5
	str	r0, [r2, r9]
	add	r9, r1, #12
	ldr	r5, [r3, r9]
	ldr	ip, [r2, r9]
	eor	r0, ip, r5
	bic	r5, r5, #-2147483648
	bic	ip, ip, #-2147483648
	bic	r5, r5, #8388608
	bic	ip, ip, #8388608
	bic	r0, r0, #2130706432
	bic	r5, r5, #32768
	bic	ip, ip, #32768
	bic	r0, r0, #8323072
	bic	r5, r5, #128
	bic	ip, ip, #128
	bic	r0, r0, #32512
	add	ip, r5, ip
	bic	r0, r0, #127
	add	r8, r8, #4
	eor	r0, ip, r0
	cmp	r8, sl
	str	r0, [r2, r9]
	add	r1, r1, #16
	bcc	.L88
.L263:
	cmp	r6, r7
	add	r0, r2, r7
	add	ip, r3, r7
	beq	.L89
.L87:
	ldrb	sl, [ip, #0]	@ zero_extendqisi2
	ldrb	r1, [r0, #0]	@ zero_extendqisi2
	mvn	r5, r7
	add	r8, r7, #1
	add	r1, sl, r1
	add	r5, r5, r6
	cmp	r6, r8
	strb	r1, [r0, #0]
	and	r5, r5, #3
	mov	r1, #1
	bls	.L89
	cmp	r5, #0
	beq	.L90
	cmp	r5, #1
	beq	.L256
	cmp	r5, #2
	ldrneb	r1, [r0, #1]	@ zero_extendqisi2
	ldrneb	r5, [ip, #1]	@ zero_extendqisi2
	addne	r1, r5, r1
	strneb	r1, [r0, #1]
	movne	r1, #2
	ldrb	r8, [ip, r1]	@ zero_extendqisi2
	ldrb	r5, [r0, r1]	@ zero_extendqisi2
	add	r5, r8, r5
	strb	r5, [r0, r1]
	add	r1, r1, #1
.L256:
	ldrb	r8, [ip, r1]	@ zero_extendqisi2
	ldrb	r5, [r0, r1]	@ zero_extendqisi2
	add	r5, r8, r5
	strb	r5, [r0, r1]
	add	r1, r1, #1
	add	r5, r1, r7
	cmp	r6, r5
	bls	.L89
.L90:
	ldrb	r8, [ip, r1]	@ zero_extendqisi2
	ldrb	r9, [r0, r1]	@ zero_extendqisi2
	add	r5, r8, r9
	strb	r5, [r0, r1]
	add	r5, r1, #1
	ldrb	sl, [ip, r5]	@ zero_extendqisi2
	ldrb	r8, [r0, r5]	@ zero_extendqisi2
	add	r9, sl, r8
	strb	r9, [r0, r5]
	add	r9, r5, #1
	ldrb	sl, [ip, r9]	@ zero_extendqisi2
	ldrb	r8, [r0, r9]	@ zero_extendqisi2
	add	r8, sl, r8
	strb	r8, [r0, r9]
	add	r5, r1, #3
	ldrb	r9, [ip, r5]	@ zero_extendqisi2
	ldrb	r8, [r0, r5]	@ zero_extendqisi2
	add	r1, r1, #4
	add	sl, r1, r7
	add	r8, r9, r8
	cmp	r6, sl
	strb	r8, [r0, r5]
	bhi	.L90
.L89:
	add	ip, r2, r6
	add	r6, r3, r6
.L84:
	cmp	r4, #0
	beq	.L96
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	ldrb	r7, [r2, #0]	@ zero_extendqisi2
	ldrb	r8, [r6, #0]	@ zero_extendqisi2
	rsb	r5, r1, r7
	rsb	r0, r1, r8
	add	sl, r5, r0
	cmp	sl, #0
	rsblt	sl, sl, #0
	cmp	r0, #0
	rsblt	r0, r0, #0
	cmp	r5, #0
	rsblt	r5, r5, #0
	cmp	r0, r5
	cmple	r0, sl
	sub	r0, r4, #1
	and	r0, r0, #1
	ble	.L266
	cmp	r5, sl
	movle	r7, r8
	movgt	r7, r1
.L266:
	ldrb	r5, [ip, #0]	@ zero_extendqisi2
	mov	r1, #1
	add	r7, r7, r5
	cmp	r4, r1
	strb	r7, [ip, #0]
	bls	.L96
	cmp	r0, #0
	beq	.L95
	ldrb	r0, [r3, r1]	@ zero_extendqisi2
	ldrb	r8, [r2, r1]	@ zero_extendqisi2
	ldrb	sl, [r6, r1]	@ zero_extendqisi2
	rsb	r7, r0, r8
	rsb	r5, r0, sl
	add	r9, r7, r5
	cmp	r9, #0
	rsblt	r9, r9, #0
	cmp	r5, #0
	rsblt	r5, r5, #0
	cmp	r7, #0
	rsblt	r7, r7, #0
	cmp	r5, r7
	cmple	r5, r9
	ble	.L268
	cmp	r7, r9
	movle	r8, sl
	movgt	r8, r0
.L268:
	ldrb	r5, [ip, r1]	@ zero_extendqisi2
	add	r7, r8, r5
	strb	r7, [ip, r1]
	add	r1, r1, #1
	cmp	r4, r1
	bls	.L96
.L95:
	ldrb	r0, [r3, r1]	@ zero_extendqisi2
	ldrb	r9, [r2, r1]	@ zero_extendqisi2
	ldrb	r7, [r6, r1]	@ zero_extendqisi2
	rsb	sl, r0, r9
	rsb	r5, r0, r7
	add	r8, sl, r5
	cmp	r8, #0
	rsblt	r8, r8, #0
	cmp	sl, #0
	rsblt	sl, sl, #0
	cmp	r5, #0
	rsblt	r5, r5, #0
	cmp	r5, sl
	cmple	r5, r8
	ble	.L93
	cmp	sl, r8
	movle	r9, r7
	movgt	r9, r0
.L93:
	ldrb	sl, [ip, r1]	@ zero_extendqisi2
	add	r7, r9, sl
	strb	r7, [ip, r1]
	add	r0, r1, #1
	ldrb	r1, [r3, r0]	@ zero_extendqisi2
	ldrb	r8, [r6, r0]	@ zero_extendqisi2
	ldrb	r7, [r2, r0]	@ zero_extendqisi2
	rsb	r5, r1, r8
	rsb	r9, r1, r7
	add	sl, r9, r5
	cmp	sl, #0
	rsblt	sl, sl, #0
	cmp	r9, #0
	rsblt	r9, r9, #0
	cmp	r5, #0
	rsblt	r5, r5, #0
	cmp	r5, r9
	cmple	r5, sl
	ble	.L269
	cmp	r9, sl
	movle	r7, r8
	movgt	r7, r1
.L269:
	ldrb	r5, [ip, r0]	@ zero_extendqisi2
	add	r1, r0, #1
	add	r7, r7, r5
	cmp	r4, r1
	strb	r7, [ip, r0]
	bhi	.L95
	b	.L96
.L62:
	ldrb	r0, [r1, #11]	@ zero_extendqisi2
	ldr	r6, [r1, #4]
	add	r8, r0, #7
	mov	r8, r8, asr #3
	cmp	r6, r8
	bls	.L96
	add	r3, r2, r8
	orr	r4, r3, r2
	mvn	ip, r8
	tst	r4, #3
	add	r0, r3, #4
	add	ip, ip, r6
	add	r4, r2, #4
	movne	r7, #0
	moveq	r7, #1
	add	fp, ip, #1
	cmp	r2, r0
	cmpls	r3, r4
	movls	r1, #0
	movhi	r1, #1
	cmp	fp, #3
	movls	r7, #0
	andhi	r7, r7, #1
	ands	r0, r7, r1
	beq	.L67
	mov	r7, fp, lsr #2
	movs	sl, r7, asl #2
	beq	.L69
	ldr	r0, [r2, #0]
	ldr	r5, [r3, #0]
	bic	ip, r0, #-2147483648
	eor	r1, r5, r0
	bic	r0, r5, #-2147483648
	bic	r0, r0, #8388608
	bic	r5, ip, #8388608
	bic	r1, r1, #2130706432
	bic	ip, r5, #32768
	bic	r1, r1, #8323072
	bic	r5, r0, #32768
	bic	ip, ip, #128
	bic	r0, r5, #128
	bic	r5, r1, #32512
	add	r0, ip, r0
	bic	ip, r5, #127
	mov	r5, #1
	eor	r1, r0, ip
	cmp	r5, r7
	sub	r0, r7, #1
	str	r1, [r3, #0]
	and	r0, r0, #3
	mov	r1, #4
	bcs	.L260
	cmp	r0, #0
	beq	.L70
	cmp	r0, #1
	beq	.L241
	cmp	r0, #2
	beq	.L242
	ldr	ip, [r2, #4]
	ldr	r0, [r3, #4]
	bic	r1, ip, #-2147483648
	eor	r5, r0, ip
	bic	r4, r0, #-2147483648
	bic	ip, r1, #8388608
	bic	r0, r4, #8388608
	bic	r1, r5, #2130706432
	bic	r4, ip, #32768
	bic	r5, r0, #32768
	bic	r1, r1, #8323072
	bic	ip, r4, #128
	bic	r0, r5, #128
	bic	r4, r1, #32512
	add	r5, ip, r0
	bic	r1, r4, #127
	eor	r4, r5, r1
	str	r4, [r3, #4]
	mov	r5, #2
	mov	r1, #8
.L242:
	ldr	r4, [r2, r1]
	ldr	ip, [r3, r1]
	eor	r0, ip, r4
	bic	r4, r4, #-2147483648
	bic	ip, ip, #-2147483648
	bic	r4, r4, #8388608
	bic	ip, ip, #8388608
	bic	r0, r0, #2130706432
	bic	r4, r4, #32768
	bic	ip, ip, #32768
	bic	r0, r0, #8323072
	bic	r4, r4, #128
	bic	ip, ip, #128
	bic	r0, r0, #32512
	add	ip, r4, ip
	bic	r0, r0, #127
	eor	r4, ip, r0
	str	r4, [r3, r1]
	add	r5, r5, #1
	add	r1, r1, #4
.L241:
	ldr	r4, [r2, r1]
	ldr	ip, [r3, r1]
	eor	r0, ip, r4
	bic	r4, r4, #-2147483648
	bic	ip, ip, #-2147483648
	bic	r4, r4, #8388608
	bic	ip, ip, #8388608
	bic	r0, r0, #2130706432
	bic	r4, r4, #32768
	bic	ip, ip, #32768
	bic	r0, r0, #8323072
	bic	r4, r4, #128
	bic	ip, ip, #128
	bic	r0, r0, #32512
	add	ip, r4, ip
	bic	r0, r0, #127
	add	r5, r5, #1
	eor	r0, ip, r0
	cmp	r5, r7
	str	r0, [r3, r1]
	add	r1, r1, #4
	bcs	.L260
.L70:
	ldr	ip, [r2, r1]
	ldr	r9, [r3, r1]
	bic	r4, ip, #-2147483648
	eor	r0, r9, ip
	bic	ip, r9, #-2147483648
	bic	ip, ip, #8388608
	bic	r9, r4, #8388608
	bic	r0, r0, #2130706432
	bic	r4, r9, #32768
	bic	r0, r0, #8323072
	bic	r9, ip, #32768
	bic	r4, r4, #128
	bic	ip, r9, #128
	bic	r9, r0, #32512
	add	ip, r4, ip
	bic	r0, r9, #127
	eor	r9, ip, r0
	str	r9, [r3, r1]
	add	r9, r1, #4
	ldr	r4, [r2, r9]
	ldr	ip, [r3, r9]
	eor	r0, ip, r4
	bic	r4, r4, #-2147483648
	bic	ip, ip, #-2147483648
	bic	r4, r4, #8388608
	bic	ip, ip, #8388608
	bic	r0, r0, #2130706432
	bic	r4, r4, #32768
	bic	ip, ip, #32768
	bic	r0, r0, #8323072
	bic	r4, r4, #128
	bic	r0, r0, #32512
	bic	ip, ip, #128
	add	ip, r4, ip
	bic	r4, r0, #127
	eor	r0, ip, r4
	str	r0, [r3, r9]
	add	r9, r9, #4
	ldr	r4, [r2, r9]
	ldr	ip, [r3, r9]
	eor	r0, ip, r4
	bic	r4, r4, #-2147483648
	bic	ip, ip, #-2147483648
	bic	r4, r4, #8388608
	bic	ip, ip, #8388608
	bic	r0, r0, #2130706432
	bic	r4, r4, #32768
	bic	ip, ip, #32768
	bic	r0, r0, #8323072
	bic	r4, r4, #128
	bic	r0, r0, #32512
	bic	ip, ip, #128
	add	ip, r4, ip
	bic	r4, r0, #127
	eor	r0, ip, r4
	str	r0, [r3, r9]
	add	r9, r1, #12
	ldr	r4, [r2, r9]
	ldr	ip, [r3, r9]
	eor	r0, ip, r4
	bic	r4, r4, #-2147483648
	bic	ip, ip, #-2147483648
	bic	r4, r4, #8388608
	bic	ip, ip, #8388608
	bic	r0, r0, #2130706432
	bic	r4, r4, #32768
	bic	ip, ip, #32768
	bic	r0, r0, #8323072
	bic	r4, r4, #128
	bic	ip, ip, #128
	bic	r0, r0, #32512
	add	ip, r4, ip
	bic	r0, r0, #127
	add	r5, r5, #4
	eor	r0, ip, r0
	cmp	r5, r7
	str	r0, [r3, r9]
	add	r1, r1, #16
	bcc	.L70
.L260:
	cmp	fp, sl
	add	r8, sl, r8
	add	r3, r3, sl
	add	r2, r2, sl
	beq	.L96
.L69:
	ldrb	r4, [r2, #0]	@ zero_extendqisi2
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	mvn	r0, r8
	add	ip, r8, #1
	add	r1, r4, r1
	add	r0, r0, r6
	cmp	r6, ip
	strb	r1, [r3, #0]
	and	r0, r0, #3
	mov	r1, #1
	bls	.L96
	cmp	r0, #0
	beq	.L71
	cmp	r0, #1
	beq	.L239
	cmp	r0, #2
	ldrneb	r1, [r3, #1]	@ zero_extendqisi2
	ldrneb	r0, [r2, #1]	@ zero_extendqisi2
	addne	r1, r0, r1
	strneb	r1, [r3, #1]
	movne	r1, #2
	ldrb	ip, [r2, r1]	@ zero_extendqisi2
	ldrb	r0, [r3, r1]	@ zero_extendqisi2
	add	r0, ip, r0
	strb	r0, [r3, r1]
	add	r1, r1, #1
.L239:
	ldrb	ip, [r2, r1]	@ zero_extendqisi2
	ldrb	r0, [r3, r1]	@ zero_extendqisi2
	add	r0, ip, r0
	strb	r0, [r3, r1]
	add	r1, r1, #1
	add	r0, r1, r8
	cmp	r6, r0
	bls	.L96
.L71:
	ldrb	ip, [r2, r1]	@ zero_extendqisi2
	ldrb	r5, [r3, r1]	@ zero_extendqisi2
	add	r0, ip, r5
	strb	r0, [r3, r1]
	add	r0, r1, #1
	ldrb	r4, [r2, r0]	@ zero_extendqisi2
	ldrb	ip, [r3, r0]	@ zero_extendqisi2
	add	r5, r4, ip
	strb	r5, [r3, r0]
	add	r5, r0, #1
	ldrb	r4, [r2, r5]	@ zero_extendqisi2
	ldrb	ip, [r3, r5]	@ zero_extendqisi2
	add	ip, r4, ip
	strb	ip, [r3, r5]
	add	r0, r1, #3
	ldrb	r5, [r2, r0]	@ zero_extendqisi2
	ldrb	ip, [r3, r0]	@ zero_extendqisi2
	add	r1, r1, #4
	add	r4, r1, r8
	add	ip, r5, ip
	cmp	r6, r4
	strb	ip, [r3, r0]
	bhi	.L71
	b	.L96
.L63:
	ldr	r5, [r1, #4]
	cmp	r5, #0
	beq	.L96
	orr	r0, r2, r3
	tst	r0, #3
	add	r1, r2, #4
	add	ip, r3, #4
	movne	r0, #0
	moveq	r0, #1
	cmp	r3, r1
	cmpls	r2, ip
	movls	r1, #0
	movhi	r1, #1
	cmp	r5, #3
	movls	r0, #0
	andhi	r0, r0, #1
	ands	r0, r0, r1
	beq	.L73
	mov	r8, r5, lsr #2
	movs	r6, r8, asl #2
	beq	.L75
	ldr	r0, [r3, #0]
	ldr	r7, [r2, #0]
	bic	ip, r0, #-2147483648
	eor	r1, r7, r0
	bic	r0, r7, #-2147483648
	bic	r0, r0, #8388608
	bic	r7, ip, #8388608
	bic	r1, r1, #2130706432
	bic	ip, r7, #32768
	bic	r1, r1, #8323072
	bic	r7, r0, #32768
	bic	ip, ip, #128
	bic	r0, r7, #128
	bic	r7, r1, #32512
	add	r0, ip, r0
	bic	ip, r7, #127
	mov	r7, #1
	eor	r1, r0, ip
	cmp	r7, r8
	sub	ip, r8, #1
	str	r1, [r2, #0]
	and	r0, ip, #3
	mov	r1, #4
	bcs	.L261
	cmp	r0, #0
	beq	.L76
	cmp	r0, #1
	beq	.L247
	cmp	r0, #2
	beq	.L248
	ldr	ip, [r3, #4]
	ldr	r0, [r2, #4]
	bic	r1, ip, #-2147483648
	eor	r4, r0, ip
	bic	r7, r0, #-2147483648
	bic	ip, r1, #8388608
	bic	r0, r7, #8388608
	bic	r1, r4, #2130706432
	bic	r7, ip, #32768
	bic	r4, r0, #32768
	bic	r1, r1, #8323072
	bic	ip, r7, #128
	bic	r0, r4, #128
	bic	r7, r1, #32512
	bic	r1, r7, #127
	add	r4, ip, r0
	eor	r7, r4, r1
	str	r7, [r2, #4]
	mov	r1, #8
	mov	r7, #2
.L248:
	ldr	r4, [r3, r1]
	ldr	ip, [r2, r1]
	eor	r0, ip, r4
	bic	r4, r4, #-2147483648
	bic	ip, ip, #-2147483648
	bic	r4, r4, #8388608
	bic	ip, ip, #8388608
	bic	r0, r0, #2130706432
	bic	r4, r4, #32768
	bic	ip, ip, #32768
	bic	r0, r0, #8323072
	bic	r4, r4, #128
	bic	ip, ip, #128
	bic	r0, r0, #32512
	add	ip, r4, ip
	bic	r0, r0, #127
	eor	r4, ip, r0
	str	r4, [r2, r1]
	add	r7, r7, #1
	add	r1, r1, #4
.L247:
	ldr	r4, [r3, r1]
	ldr	ip, [r2, r1]
	eor	r0, ip, r4
	bic	r4, r4, #-2147483648
	bic	ip, ip, #-2147483648
	bic	r4, r4, #8388608
	bic	ip, ip, #8388608
	bic	r0, r0, #2130706432
	bic	r4, r4, #32768
	bic	ip, ip, #32768
	bic	r0, r0, #8323072
	bic	r4, r4, #128
	bic	ip, ip, #128
	bic	r0, r0, #32512
	add	ip, r4, ip
	bic	r0, r0, #127
	add	r7, r7, #1
	eor	r0, ip, r0
	cmp	r7, r8
	str	r0, [r2, r1]
	add	r1, r1, #4
	bcs	.L261
.L76:
	ldr	ip, [r3, r1]
	ldr	sl, [r2, r1]
	bic	r4, ip, #-2147483648
	eor	r0, sl, ip
	bic	ip, sl, #-2147483648
	bic	ip, ip, #8388608
	bic	sl, r4, #8388608
	bic	r0, r0, #2130706432
	bic	r4, sl, #32768
	bic	r0, r0, #8323072
	bic	sl, ip, #32768
	bic	r4, r4, #128
	bic	ip, sl, #128
	bic	sl, r0, #32512
	add	ip, r4, ip
	bic	r0, sl, #127
	eor	sl, ip, r0
	str	sl, [r2, r1]
	add	sl, r1, #4
	ldr	r4, [r3, sl]
	ldr	ip, [r2, sl]
	eor	r0, ip, r4
	bic	r4, r4, #-2147483648
	bic	ip, ip, #-2147483648
	bic	r4, r4, #8388608
	bic	ip, ip, #8388608
	bic	r0, r0, #2130706432
	bic	r4, r4, #32768
	bic	ip, ip, #32768
	bic	r0, r0, #8323072
	bic	r4, r4, #128
	bic	r0, r0, #32512
	bic	ip, ip, #128
	add	ip, r4, ip
	bic	r4, r0, #127
	eor	r0, ip, r4
	str	r0, [r2, sl]
	add	sl, sl, #4
	ldr	r4, [r3, sl]
	ldr	ip, [r2, sl]
	eor	r0, ip, r4
	bic	r4, r4, #-2147483648
	bic	ip, ip, #-2147483648
	bic	r4, r4, #8388608
	bic	ip, ip, #8388608
	bic	r0, r0, #2130706432
	bic	r4, r4, #32768
	bic	ip, ip, #32768
	bic	r0, r0, #8323072
	bic	r4, r4, #128
	bic	r0, r0, #32512
	bic	ip, ip, #128
	add	ip, r4, ip
	bic	r4, r0, #127
	eor	r0, ip, r4
	str	r0, [r2, sl]
	add	sl, r1, #12
	ldr	r4, [r3, sl]
	ldr	ip, [r2, sl]
	eor	r0, ip, r4
	bic	r4, r4, #-2147483648
	bic	ip, ip, #-2147483648
	bic	r4, r4, #8388608
	bic	ip, ip, #8388608
	bic	r0, r0, #2130706432
	bic	r4, r4, #32768
	bic	ip, ip, #32768
	bic	r0, r0, #8323072
	bic	r4, r4, #128
	bic	ip, ip, #128
	bic	r0, r0, #32512
	add	ip, r4, ip
	bic	r0, r0, #127
	add	r7, r7, #4
	eor	r0, ip, r0
	cmp	r7, r8
	str	r0, [r2, sl]
	add	r1, r1, #16
	bcc	.L76
.L261:
	cmp	r5, r6
	add	r2, r2, r6
	add	r3, r3, r6
	beq	.L96
.L75:
	ldrb	r4, [r3, #0]	@ zero_extendqisi2
	ldrb	r1, [r2, #0]	@ zero_extendqisi2
	mvn	r0, r6
	add	ip, r6, #1
	add	r1, r4, r1
	add	r0, r0, r5
	cmp	r5, ip
	strb	r1, [r2, #0]
	and	r0, r0, #3
	mov	r1, #1
	bls	.L96
	cmp	r0, #0
	beq	.L77
	cmp	r0, #1
	beq	.L245
	cmp	r0, #2
	ldrneb	r1, [r2, #1]	@ zero_extendqisi2
	ldrneb	r0, [r3, #1]	@ zero_extendqisi2
	addne	r1, r0, r1
	strneb	r1, [r2, #1]
	movne	r1, #2
	ldrb	ip, [r3, r1]	@ zero_extendqisi2
	ldrb	r0, [r2, r1]	@ zero_extendqisi2
	add	r0, ip, r0
	strb	r0, [r2, r1]
	add	r1, r1, #1
.L245:
	ldrb	ip, [r3, r1]	@ zero_extendqisi2
	ldrb	r0, [r2, r1]	@ zero_extendqisi2
	add	r0, ip, r0
	strb	r0, [r2, r1]
	add	r1, r1, #1
	add	r0, r1, r6
	cmp	r5, r0
	bls	.L96
.L77:
	ldrb	ip, [r3, r1]	@ zero_extendqisi2
	ldrb	r7, [r2, r1]	@ zero_extendqisi2
	add	r0, ip, r7
	strb	r0, [r2, r1]
	add	r0, r1, #1
	ldrb	r4, [r3, r0]	@ zero_extendqisi2
	ldrb	ip, [r2, r0]	@ zero_extendqisi2
	add	r7, r4, ip
	strb	r7, [r2, r0]
	add	r7, r0, #1
	ldrb	r4, [r3, r7]	@ zero_extendqisi2
	ldrb	ip, [r2, r7]	@ zero_extendqisi2
	add	ip, r4, ip
	strb	ip, [r2, r7]
	add	r0, r1, #3
	ldrb	r7, [r3, r0]	@ zero_extendqisi2
	ldrb	ip, [r2, r0]	@ zero_extendqisi2
	add	r1, r1, #4
	add	r4, r1, r6
	add	ip, r7, ip
	cmp	r5, r4
	strb	ip, [r2, r0]
	bhi	.L77
	b	.L96
.L271:
	.align	2
.L270:
	.word	.LC2
.L64:
	ldrb	ip, [r1, #11]	@ zero_extendqisi2
	add	r0, ip, #7
	mov	r0, r0, asr #3
	ldr	r4, [r1, #4]
	cmp	r0, #0
	rsb	r4, r0, r4
	moveq	r0, r2
	beq	.L80
	ldrb	ip, [r3, #0]	@ zero_extendqisi2
	ldrb	r1, [r2, #0]	@ zero_extendqisi2
	cmp	r0, #1
	add	r1, r1, ip, lsr #1
	sub	ip, r0, #1
	strb	r1, [r2, #0]
	and	ip, ip, #3
	mov	r1, #1
	bls	.L262
	cmp	ip, #0
	beq	.L81
	cmp	ip, #1
	beq	.L251
	cmp	ip, #2
	ldrneb	r1, [r2, #1]	@ zero_extendqisi2
	ldrneb	ip, [r3, #1]	@ zero_extendqisi2
	addne	r1, r1, ip, lsr #1
	strneb	r1, [r2, #1]
	movne	r1, #2
	ldrb	r5, [r3, r1]	@ zero_extendqisi2
	ldrb	ip, [r2, r1]	@ zero_extendqisi2
	add	ip, ip, r5, lsr #1
	strb	ip, [r2, r1]
	add	r1, r1, #1
.L251:
	ldrb	r5, [r3, r1]	@ zero_extendqisi2
	ldrb	ip, [r2, r1]	@ zero_extendqisi2
	add	ip, ip, r5, lsr #1
	strb	ip, [r2, r1]
	add	r1, r1, #1
	cmp	r0, r1
	bls	.L262
.L81:
	ldrb	r5, [r3, r1]	@ zero_extendqisi2
	ldrb	r6, [r2, r1]	@ zero_extendqisi2
	add	ip, r6, r5, lsr #1
	strb	ip, [r2, r1]
	add	ip, r1, #1
	ldrb	r6, [r3, ip]	@ zero_extendqisi2
	ldrb	r5, [r2, ip]	@ zero_extendqisi2
	add	r5, r5, r6, lsr #1
	strb	r5, [r2, ip]
	add	ip, ip, #1
	ldrb	r6, [r3, ip]	@ zero_extendqisi2
	ldrb	r5, [r2, ip]	@ zero_extendqisi2
	add	r5, r5, r6, lsr #1
	strb	r5, [r2, ip]
	add	ip, r1, #3
	ldrb	r6, [r3, ip]	@ zero_extendqisi2
	ldrb	r5, [r2, ip]	@ zero_extendqisi2
	add	r1, r1, #4
	add	r5, r5, r6, lsr #1
	cmp	r0, r1
	strb	r5, [r2, ip]
	bhi	.L81
.L262:
	add	r3, r3, r0
	add	r0, r2, r0
.L80:
	cmp	r4, #0
	beq	.L96
	ldrb	r5, [r2, #0]	@ zero_extendqisi2
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	ldrb	ip, [r0, #0]	@ zero_extendqisi2
	add	r1, r5, r1
	add	r1, ip, r1, lsr #1
	cmp	r4, #1
	sub	ip, r4, #1
	strb	r1, [r0, #0]
	and	ip, ip, #3
	mov	r1, #1
	bls	.L96
	cmp	ip, #0
	beq	.L82
	cmp	ip, #1
	beq	.L249
	cmp	ip, #2
	beq	.L250
	ldrb	r6, [r3, #1]	@ zero_extendqisi2
	ldrb	r5, [r2, #1]	@ zero_extendqisi2
	ldrb	ip, [r0, #1]	@ zero_extendqisi2
	add	r1, r5, r6
	add	r6, ip, r1, lsr #1
	strb	r6, [r0, #1]
	mov	r1, #2
.L250:
	ldrb	r6, [r2, r1]	@ zero_extendqisi2
	ldrb	ip, [r3, r1]	@ zero_extendqisi2
	ldrb	r5, [r0, r1]	@ zero_extendqisi2
	add	ip, r6, ip
	add	ip, r5, ip, lsr #1
	strb	ip, [r0, r1]
	add	r1, r1, #1
.L249:
	ldrb	r6, [r2, r1]	@ zero_extendqisi2
	ldrb	ip, [r3, r1]	@ zero_extendqisi2
	ldrb	r5, [r0, r1]	@ zero_extendqisi2
	add	ip, r6, ip
	add	ip, r5, ip, lsr #1
	strb	ip, [r0, r1]
	add	r1, r1, #1
	cmp	r4, r1
	bls	.L96
.L82:
	ldrb	r6, [r2, r1]	@ zero_extendqisi2
	ldrb	ip, [r3, r1]	@ zero_extendqisi2
	ldrb	r5, [r0, r1]	@ zero_extendqisi2
	add	r7, r6, ip
	add	ip, r5, r7, lsr #1
	strb	ip, [r0, r1]
	add	ip, r1, #1
	ldrb	r7, [r2, ip]	@ zero_extendqisi2
	ldrb	r5, [r3, ip]	@ zero_extendqisi2
	ldrb	r6, [r0, ip]	@ zero_extendqisi2
	add	r5, r7, r5
	add	r5, r6, r5, lsr #1
	strb	r5, [r0, ip]
	add	ip, ip, #1
	ldrb	r7, [r2, ip]	@ zero_extendqisi2
	ldrb	r5, [r3, ip]	@ zero_extendqisi2
	ldrb	r6, [r0, ip]	@ zero_extendqisi2
	add	r5, r7, r5
	add	r7, r6, r5, lsr #1
	strb	r7, [r0, ip]
	add	ip, r1, #3
	ldrb	r7, [r2, ip]	@ zero_extendqisi2
	ldrb	r5, [r3, ip]	@ zero_extendqisi2
	ldrb	r6, [r0, ip]	@ zero_extendqisi2
	add	r5, r7, r5
	add	r1, r1, #4
	add	r5, r6, r5, lsr #1
	cmp	r4, r1
	strb	r5, [r0, ip]
	bhi	.L82
	b	.L96
.L85:
	ldrb	ip, [r2, r0]	@ zero_extendqisi2
	ldrb	r5, [r3, r0]	@ zero_extendqisi2
	mov	r1, #1
	add	r5, r5, ip
	cmp	r6, r1
	sub	ip, r6, #1
	strb	r5, [r2, r0]
	and	r0, ip, #3
	bls	.L89
	cmp	r0, #0
	beq	.L91
	cmp	r0, #1
	beq	.L254
	cmp	r0, #2
	ldrneb	ip, [r3, r1]	@ zero_extendqisi2
	ldrneb	r0, [r2, r1]	@ zero_extendqisi2
	addne	r0, ip, r0
	strneb	r0, [r2, r1]
	movne	r1, #2
	ldrb	ip, [r3, r1]	@ zero_extendqisi2
	ldrb	r0, [r2, r1]	@ zero_extendqisi2
	add	r0, ip, r0
	strb	r0, [r2, r1]
	add	r1, r1, #1
.L254:
	ldrb	ip, [r3, r1]	@ zero_extendqisi2
	ldrb	r0, [r2, r1]	@ zero_extendqisi2
	add	r0, ip, r0
	strb	r0, [r2, r1]
	add	r1, r1, #1
	cmp	r6, r1
	bls	.L89
.L91:
	ldrb	ip, [r3, r1]	@ zero_extendqisi2
	ldrb	r5, [r2, r1]	@ zero_extendqisi2
	add	r0, ip, r5
	strb	r0, [r2, r1]
	add	r0, r1, #1
	ldrb	r5, [r3, r0]	@ zero_extendqisi2
	ldrb	ip, [r2, r0]	@ zero_extendqisi2
	add	ip, r5, ip
	strb	ip, [r2, r0]
	add	r0, r0, #1
	ldrb	r5, [r3, r0]	@ zero_extendqisi2
	ldrb	ip, [r2, r0]	@ zero_extendqisi2
	add	ip, r5, ip
	strb	ip, [r2, r0]
	add	r0, r1, #3
	ldrb	r5, [r3, r0]	@ zero_extendqisi2
	ldrb	ip, [r2, r0]	@ zero_extendqisi2
	add	r1, r1, #4
	add	ip, r5, ip
	cmp	r6, r1
	strb	ip, [r2, r0]
	bhi	.L91
	b	.L89
.L73:
	ldrb	ip, [r2, r0]	@ zero_extendqisi2
	ldrb	r4, [r3, r0]	@ zero_extendqisi2
	mov	r1, #1
	add	r4, r4, ip
	cmp	r5, r1
	sub	ip, r5, #1
	strb	r4, [r2, r0]
	and	r0, ip, #3
	bls	.L96
	cmp	r0, #0
	beq	.L78
	cmp	r0, #1
	beq	.L243
	cmp	r0, #2
	ldrneb	ip, [r3, r1]	@ zero_extendqisi2
	ldrneb	r0, [r2, r1]	@ zero_extendqisi2
	addne	r0, ip, r0
	strneb	r0, [r2, r1]
	movne	r1, #2
	ldrb	ip, [r3, r1]	@ zero_extendqisi2
	ldrb	r0, [r2, r1]	@ zero_extendqisi2
	add	r0, ip, r0
	strb	r0, [r2, r1]
	add	r1, r1, #1
.L243:
	ldrb	ip, [r3, r1]	@ zero_extendqisi2
	ldrb	r0, [r2, r1]	@ zero_extendqisi2
	add	r0, ip, r0
	strb	r0, [r2, r1]
	add	r1, r1, #1
	cmp	r5, r1
	bls	.L96
.L78:
	ldrb	ip, [r3, r1]	@ zero_extendqisi2
	ldrb	r4, [r2, r1]	@ zero_extendqisi2
	add	r0, ip, r4
	strb	r0, [r2, r1]
	add	r0, r1, #1
	ldrb	r4, [r3, r0]	@ zero_extendqisi2
	ldrb	ip, [r2, r0]	@ zero_extendqisi2
	add	ip, r4, ip
	strb	ip, [r2, r0]
	add	r0, r0, #1
	ldrb	r4, [r3, r0]	@ zero_extendqisi2
	ldrb	ip, [r2, r0]	@ zero_extendqisi2
	add	ip, r4, ip
	strb	ip, [r2, r0]
	add	r0, r1, #3
	ldrb	r4, [r3, r0]	@ zero_extendqisi2
	ldrb	ip, [r2, r0]	@ zero_extendqisi2
	add	r1, r1, #4
	add	ip, r4, ip
	cmp	r5, r1
	strb	ip, [r2, r0]
	bhi	.L78
	b	.L96
.L67:
	ldrb	r7, [r2, r0]	@ zero_extendqisi2
	ldrb	r4, [r3, r0]	@ zero_extendqisi2
	mov	r1, #1
	add	r5, r8, r1
	add	r4, r7, r4
	cmp	r6, r5
	strb	r4, [r3, r0]
	and	ip, ip, #3
	bls	.L96
	cmp	ip, #0
	beq	.L72
	cmp	ip, #1
	beq	.L237
	cmp	ip, #2
	ldrneb	ip, [r2, r1]	@ zero_extendqisi2
	ldrneb	r0, [r3, r1]	@ zero_extendqisi2
	addne	r0, ip, r0
	strneb	r0, [r3, r1]
	movne	r1, #2
	ldrb	ip, [r2, r1]	@ zero_extendqisi2
	ldrb	r0, [r3, r1]	@ zero_extendqisi2
	add	r0, ip, r0
	strb	r0, [r3, r1]
	add	r1, r1, #1
.L237:
	ldrb	ip, [r2, r1]	@ zero_extendqisi2
	ldrb	r0, [r3, r1]	@ zero_extendqisi2
	add	r0, ip, r0
	strb	r0, [r3, r1]
	add	r1, r1, #1
	add	r0, r8, r1
	cmp	r6, r0
	bls	.L96
.L72:
	ldrb	ip, [r2, r1]	@ zero_extendqisi2
	ldrb	r5, [r3, r1]	@ zero_extendqisi2
	add	r0, ip, r5
	strb	r0, [r3, r1]
	add	r0, r1, #1
	ldrb	r4, [r2, r0]	@ zero_extendqisi2
	ldrb	ip, [r3, r0]	@ zero_extendqisi2
	add	r5, r4, ip
	strb	r5, [r3, r0]
	add	r5, r0, #1
	ldrb	r4, [r2, r5]	@ zero_extendqisi2
	ldrb	ip, [r3, r5]	@ zero_extendqisi2
	add	ip, r4, ip
	strb	ip, [r3, r5]
	add	r0, r1, #3
	ldrb	r5, [r2, r0]	@ zero_extendqisi2
	ldrb	ip, [r3, r0]	@ zero_extendqisi2
	add	r1, r1, #4
	add	r4, r8, r1
	add	ip, r5, ip
	cmp	r6, r4
	strb	ip, [r3, r0]
	bhi	.L72
	b	.L96
	.size	png_read_filter_row, .-png_read_filter_row
	.section	.text.png_do_read_interlace,"ax",%progbits
	.align	2
	.global	png_do_read_interlace
	.hidden	png_do_read_interlace
	.type	png_do_read_interlace, %function
png_do_read_interlace:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	sub	sp, sp, #36
	str	r0, [sp, #0]
	ldr	ip, [r0, #264]
	cmn	r0, #284
	cmnne	ip, #1
	ldrb	r2, [r0, #320]	@ zero_extendqisi2
	ldr	r3, [r0, #140]
	add	ip, ip, #1
	beq	.L320
	ldr	r1, .L464
	ldr	r4, [r1, r2, asl #2]
	ldr	r1, [r0, #284]
	ldrb	r6, [r0, #295]	@ zero_extendqisi2
	mul	r0, r1, r4
	cmp	r6, #2
	str	r0, [sp, #16]
	beq	.L276
	cmp	r6, #4
	beq	.L277
	cmp	r6, #1
	beq	.L459
	cmp	r1, #0
	beq	.L280
	ldr	r2, [sp, #16]
	mov	r6, r6, lsr #3
	sub	fp, r1, #1
	sub	r3, r2, #1
	mla	r3, r6, r3, ip
	rsb	sl, r6, #0
	mla	ip, fp, r6, ip
	mul	r8, sl, r4
	mov	lr, #0
	str	r3, [sp, #4]
	str	ip, [sp, #12]
	str	lr, [sp, #8]
	str	r8, [sp, #20]
	mov	fp, r4
	add	r8, sp, #24
	mov	r1, ip
.L317:
	mov	r0, r8
	mov	r2, r6
	bl	memcpy
	cmp	fp, #0
	ble	.L315
	mov	r1, r8
	ldr	r0, [sp, #4]
	mov	r2, r6
	bl	memcpy
	mov	r5, #1
	ldr	r1, [sp, #4]
	sub	r4, fp, #1
	cmp	fp, r5
	and	r7, r4, #3
	add	r4, r1, sl
	ble	.L409
	cmp	r7, #0
	beq	.L457
	cmp	r7, #1
	beq	.L400
	cmp	r7, #2
	beq	.L401
	mov	r0, r4
	mov	r1, r8
	mov	r2, r6
	bl	memcpy
	add	r4, r4, sl
	mov	r5, #2
.L401:
	mov	r0, r4
	mov	r1, r8
	mov	r2, r6
	bl	memcpy
	add	r4, r4, sl
	add	r5, r5, #1
.L400:
	mov	r0, r4
	add	r5, r5, #1
	mov	r1, r8
	mov	r2, r6
	bl	memcpy
	cmp	fp, r5
	add	r4, r4, sl
	ble	.L409
.L457:
	mov	r0, r4
.L316:
	add	r9, r0, sl
	mov	r1, r8
	mov	r2, r6
	bl	memcpy
	add	r7, r9, sl
	mov	r1, r8
	mov	r2, r6
	mov	r0, r9
	bl	memcpy
	add	r4, r7, sl
	mov	r1, r8
	mov	r2, r6
	mov	r0, r7
	bl	memcpy
	add	r5, r5, #4
	mov	r0, r4
	mov	r1, r8
	mov	r2, r6
	bl	memcpy
	cmp	fp, r5
	add	r0, r4, sl
	bgt	.L316
.L409:
	ldr	lr, [sp, #4]
	ldr	r0, [sp, #20]
	add	ip, lr, r0
	str	ip, [sp, #4]
.L315:
	ldr	r1, [sp, #0]
	ldr	r2, [sp, #8]
	ldr	r3, [r1, #284]
	add	r2, r2, #1
	cmp	r3, r2
	str	r2, [sp, #8]
	bls	.L460
	ldr	r1, [sp, #12]
	add	lr, r1, sl
	str	lr, [sp, #12]
	mov	r1, lr
	b	.L317
.L460:
	ldrb	r6, [r1, #295]	@ zero_extendqisi2
.L280:
	ldr	r3, [sp, #16]
	ldr	ip, [sp, #0]
	str	r3, [ip, #284]
	cmp	r6, #7
	ldrls	r2, [sp, #16]
	mulls	r6, r2, r6
	movhi	r6, r6, lsr #3
	mulhi	r6, r3, r6
	addls	r6, r6, #7
	ldr	r0, [sp, #0]
	movls	r6, r6, lsr #3
	str	r6, [r0, #288]
.L320:
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L276:
	ands	r7, r3, #65536
	add	r8, r1, #3
	beq	.L290
	ldr	r3, [sp, #16]
	add	r5, r3, #3
	and	r7, r5, #3
	and	r0, r8, #3
	mov	r8, r0, asl #1
	mov	r2, r7, asl #1
	mvn	r5, #1
	mov	r0, #0
	mov	r7, #6
.L291:
	cmp	r1, #0
	beq	.L280
	ldr	fp, [sp, #16]
	sub	sl, r1, #1
	sub	r3, fp, #1
	add	r6, ip, sl, lsr #2
	mov	fp, #16128
	str	r6, [sp, #4]
	add	r3, ip, r3, lsr #2
	add	r6, fp, #63
	mov	fp, #0
.L300:
	ldr	sl, [sp, #4]
	cmp	r4, #0
	ldrb	ip, [sl, #0]	@ zero_extendqisi2
	ble	.L292
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	rsb	sl, r2, #6
	mov	ip, ip, asr r8
	and	r9, r1, r6, asr sl
	and	ip, ip, #3
	orr	sl, r9, ip, asl r2
	mov	r1, r3
	sub	r9, r4, #1
	cmp	r0, r2
	strb	sl, [r1], #-1
	and	r9, r9, #3
	add	sl, r2, r5
	beq	.L461
.L411:
	mov	r1, #1
	cmp	r1, r4
	mov	r2, sl
	beq	.L412
	cmp	r9, #0
	beq	.L297
	cmp	r9, r1
	beq	.L392
	cmp	r9, #2
	beq	.L393
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	rsb	r9, sl, #6
	and	r2, r2, r6, asr r9
	orr	r2, r2, ip, asl sl
	mov	r9, r3
	strb	r2, [r9], #-1
	cmp	r0, sl
	add	r2, sl, r5
	moveq	r3, r9
	moveq	r2, r7
	add	r1, r1, #1
.L393:
	ldrb	sl, [r3, #0]	@ zero_extendqisi2
	rsb	r9, r2, #6
	and	r9, sl, r6, asr r9
	orr	r9, r9, ip, asl r2
	mov	sl, r3
	strb	r9, [sl], #-1
	cmp	r0, r2
	moveq	r3, sl
	add	r2, r2, r5
	add	r1, r1, #1
	moveq	r2, r7
.L392:
	ldrb	sl, [r3, #0]	@ zero_extendqisi2
	rsb	r9, r2, #6
	and	r9, sl, r6, asr r9
	orr	r9, r9, ip, asl r2
	mov	sl, r3
	strb	r9, [sl], #-1
	cmp	r0, r2
	add	r1, r1, #1
	add	r2, r2, r5
	moveq	r3, sl
	moveq	r2, r7
	cmp	r1, r4
	bne	.L297
	b	.L412
.L419:
	ldrb	sl, [r3, #0]	@ zero_extendqisi2
	rsb	r9, r2, #6
	and	r9, sl, r6, asr r9
	orr	r9, r9, ip, asl r2
	mov	sl, r3
	strb	r9, [sl], #-1
	cmp	r0, r2
	moveq	r3, sl
	add	r2, r2, r5
	ldrb	sl, [r3, #0]	@ zero_extendqisi2
	moveq	r2, r7
	rsb	r9, r2, #6
	and	r9, sl, r6, asr r9
	orr	r9, r9, ip, asl r2
	mov	sl, r3
	strb	r9, [sl], #-1
	cmp	r0, r2
	add	r1, r1, #3
	add	r2, r2, r5
	moveq	r3, sl
	moveq	r2, r7
	cmp	r1, r4
	beq	.L412
.L297:
	ldrb	sl, [r3, #0]	@ zero_extendqisi2
	rsb	r9, r2, #6
	and	r9, sl, r6, asr r9
	orr	r9, r9, ip, asl r2
	mov	sl, r3
	strb	r9, [sl], #-1
	cmp	r0, r2
	moveq	r3, sl
	add	r2, r2, r5
	ldrb	sl, [r3, #0]	@ zero_extendqisi2
	moveq	r2, r7
	rsb	r9, r2, #6
	and	r9, sl, r6, asr r9
	orr	r9, r9, ip, asl r2
	cmp	r0, r2
	mov	sl, r3
	strb	r9, [sl], #-1
	add	r1, r1, #1
	add	r2, r2, r5
	moveq	r3, sl
	moveq	r2, r7
	b	.L419
.L412:
	ldr	ip, [sp, #0]
	ldr	r1, [ip, #284]
.L292:
	cmp	r8, r0
	ldreq	sl, [sp, #4]
	add	fp, fp, #1
	subeq	sl, sl, #1
	streq	sl, [sp, #4]
	moveq	r8, r7
	addne	r8, r8, r5
	cmp	fp, r1
	bcc	.L300
	ldr	r0, [sp, #0]
	ldrb	r6, [r0, #295]	@ zero_extendqisi2
	b	.L280
.L461:
	mov	r3, r1
	mov	sl, r7
	b	.L411
.L459:
	ands	r7, r3, #65536
	add	r8, r1, #7
	beq	.L278
	add	r2, r0, #7
	and	r8, r8, #7
	and	r2, r2, #7
	mvn	r5, #0
	mov	r0, #0
	mov	r7, #7
.L279:
	cmp	r1, #0
	beq	.L280
	sub	fp, r1, #1
	ldr	r6, [sp, #16]
	add	sl, ip, fp, lsr #3
	sub	r3, r6, #1
	str	sl, [sp, #4]
	mov	r6, #32512
	add	r3, ip, r3, lsr #3
	add	r6, r6, #127
	mov	fp, #0
.L289:
	ldr	sl, [sp, #4]
	cmp	r4, #0
	ldrb	ip, [sl, #0]	@ zero_extendqisi2
	ble	.L281
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	rsb	sl, r2, #7
	mov	ip, ip, asr r8
	and	r9, r1, r6, asr sl
	and	ip, ip, #1
	orr	sl, r9, ip, asl r2
	mov	r1, r3
	sub	r9, r4, #1
	cmp	r0, r2
	strb	sl, [r1], #-1
	and	r9, r9, #3
	add	sl, r2, r5
	beq	.L462
.L437:
	mov	r1, #1
	cmp	r1, r4
	mov	r2, sl
	beq	.L438
	cmp	r9, #0
	beq	.L286
	cmp	r9, r1
	beq	.L398
	cmp	r9, #2
	beq	.L399
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	rsb	r9, sl, #7
	and	r2, r2, r6, asr r9
	orr	r2, r2, ip, asl sl
	mov	r9, r3
	strb	r2, [r9], #-1
	cmp	r0, sl
	add	r2, sl, r5
	moveq	r3, r9
	moveq	r2, r7
	add	r1, r1, #1
.L399:
	ldrb	sl, [r3, #0]	@ zero_extendqisi2
	rsb	r9, r2, #7
	and	r9, sl, r6, asr r9
	orr	r9, r9, ip, asl r2
	mov	sl, r3
	strb	r9, [sl], #-1
	cmp	r0, r2
	moveq	r3, sl
	add	r2, r2, r5
	add	r1, r1, #1
	moveq	r2, r7
.L398:
	ldrb	sl, [r3, #0]	@ zero_extendqisi2
	rsb	r9, r2, #7
	and	r9, sl, r6, asr r9
	orr	r9, r9, ip, asl r2
	mov	sl, r3
	strb	r9, [sl], #-1
	cmp	r0, r2
	add	r1, r1, #1
	add	r2, r2, r5
	moveq	r3, sl
	moveq	r2, r7
	cmp	r1, r4
	bne	.L286
	b	.L438
.L445:
	ldrb	sl, [r3, #0]	@ zero_extendqisi2
	rsb	r9, r2, #7
	and	r9, sl, r6, asr r9
	orr	r9, r9, ip, asl r2
	mov	sl, r3
	strb	r9, [sl], #-1
	cmp	r0, r2
	moveq	r3, sl
	add	r2, r2, r5
	ldrb	sl, [r3, #0]	@ zero_extendqisi2
	moveq	r2, r7
	rsb	r9, r2, #7
	and	r9, sl, r6, asr r9
	orr	r9, r9, ip, asl r2
	mov	sl, r3
	strb	r9, [sl], #-1
	cmp	r0, r2
	add	r1, r1, #3
	add	r2, r2, r5
	moveq	r3, sl
	moveq	r2, r7
	cmp	r1, r4
	beq	.L438
.L286:
	ldrb	sl, [r3, #0]	@ zero_extendqisi2
	rsb	r9, r2, #7
	and	r9, sl, r6, asr r9
	orr	r9, r9, ip, asl r2
	mov	sl, r3
	strb	r9, [sl], #-1
	cmp	r0, r2
	moveq	r3, sl
	add	r2, r2, r5
	ldrb	sl, [r3, #0]	@ zero_extendqisi2
	moveq	r2, r7
	rsb	r9, r2, #7
	and	r9, sl, r6, asr r9
	orr	r9, r9, ip, asl r2
	cmp	r0, r2
	mov	sl, r3
	strb	r9, [sl], #-1
	add	r1, r1, #1
	add	r2, r2, r5
	moveq	r3, sl
	moveq	r2, r7
	b	.L445
.L438:
	ldr	ip, [sp, #0]
	ldr	r1, [ip, #284]
.L281:
	cmp	r8, r0
	ldreq	sl, [sp, #4]
	add	fp, fp, #1
	subeq	sl, sl, #1
	streq	sl, [sp, #4]
	moveq	r8, r7
	addne	r8, r8, r5
	cmp	fp, r1
	bcc	.L289
	ldr	r0, [sp, #0]
	ldrb	r6, [r0, #295]	@ zero_extendqisi2
	b	.L280
.L462:
	mov	r3, r1
	mov	sl, r7
	b	.L437
.L277:
	ands	r3, r3, #65536
	beq	.L301
	ldr	r0, [sp, #16]
	add	r3, r1, #1
	add	r5, r0, #1
	and	r7, r5, #1
	and	r8, r3, #1
	mov	r2, r7, asl #2
	mov	r8, r8, asl #2
	mvn	r5, #3
	mov	r0, #0
	mov	r7, r6
.L302:
	cmp	r1, #0
	beq	.L280
	ldr	r6, [sp, #16]
	sub	sl, r1, #1
	sub	r3, r6, #1
	add	fp, ip, sl, lsr #1
	mov	r6, #3840
	str	fp, [sp, #4]
	add	r3, ip, r3, lsr #1
	add	r6, r6, #15
	mov	fp, #0
.L314:
	ldr	sl, [sp, #4]
	cmp	r4, #0
	ldrb	ip, [sl, #0]	@ zero_extendqisi2
	ble	.L306
	ldrb	r1, [r3, #0]	@ zero_extendqisi2
	rsb	sl, r2, #4
	mov	ip, ip, asr r8
	and	r9, r1, r6, asr sl
	and	ip, ip, #15
	orr	sl, r9, ip, asl r2
	mov	r1, r3
	sub	r9, r4, #1
	cmp	r0, r2
	strb	sl, [r1], #-1
	and	r9, r9, #3
	add	sl, r2, r5
	beq	.L463
.L424:
	mov	r1, #1
	cmp	r1, r4
	mov	r2, sl
	beq	.L425
	cmp	r9, #0
	beq	.L311
	cmp	r9, r1
	beq	.L395
	cmp	r9, #2
	beq	.L396
	ldrb	r2, [r3, #0]	@ zero_extendqisi2
	rsb	r9, sl, #4
	and	r2, r2, r6, asr r9
	orr	r2, r2, ip, asl sl
	mov	r9, r3
	strb	r2, [r9], #-1
	cmp	r0, sl
	add	r2, sl, r5
	moveq	r3, r9
	moveq	r2, r7
	add	r1, r1, #1
.L396:
	ldrb	sl, [r3, #0]	@ zero_extendqisi2
	rsb	r9, r2, #4
	and	r9, sl, r6, asr r9
	orr	r9, r9, ip, asl r2
	mov	sl, r3
	strb	r9, [sl], #-1
	cmp	r0, r2
	moveq	r3, sl
	add	r2, r2, r5
	add	r1, r1, #1
	moveq	r2, r7
.L395:
	ldrb	sl, [r3, #0]	@ zero_extendqisi2
	rsb	r9, r2, #4
	and	r9, sl, r6, asr r9
	orr	r9, r9, ip, asl r2
	mov	sl, r3
	strb	r9, [sl], #-1
	cmp	r0, r2
	add	r1, r1, #1
	add	r2, r2, r5
	moveq	r3, sl
	moveq	r2, r7
	cmp	r1, r4
	bne	.L311
	b	.L425
.L432:
	ldrb	sl, [r3, #0]	@ zero_extendqisi2
	rsb	r9, r2, #4
	and	r9, sl, r6, asr r9
	orr	r9, r9, ip, asl r2
	mov	sl, r3
	strb	r9, [sl], #-1
	cmp	r0, r2
	moveq	r3, sl
	add	r2, r2, r5
	ldrb	sl, [r3, #0]	@ zero_extendqisi2
	moveq	r2, r7
	rsb	r9, r2, #4
	and	r9, sl, r6, asr r9
	orr	r9, r9, ip, asl r2
	mov	sl, r3
	strb	r9, [sl], #-1
	cmp	r0, r2
	add	r1, r1, #3
	add	r2, r2, r5
	moveq	r3, sl
	moveq	r2, r7
	cmp	r1, r4
	beq	.L425
.L311:
	ldrb	sl, [r3, #0]	@ zero_extendqisi2
	rsb	r9, r2, #4
	and	r9, sl, r6, asr r9
	orr	r9, r9, ip, asl r2
	mov	sl, r3
	strb	r9, [sl], #-1
	cmp	r0, r2
	moveq	r3, sl
	add	r2, r2, r5
	ldrb	sl, [r3, #0]	@ zero_extendqisi2
	moveq	r2, r7
	rsb	r9, r2, #4
	and	r9, sl, r6, asr r9
	orr	r9, r9, ip, asl r2
	cmp	r0, r2
	mov	sl, r3
	strb	r9, [sl], #-1
	add	r1, r1, #1
	add	r2, r2, r5
	moveq	r3, sl
	moveq	r2, r7
	b	.L432
.L425:
	ldr	ip, [sp, #0]
	ldr	r1, [ip, #284]
.L306:
	cmp	r8, r0
	ldreq	sl, [sp, #4]
	add	fp, fp, #1
	subeq	sl, sl, #1
	streq	sl, [sp, #4]
	moveq	r8, r7
	addne	r8, r8, r5
	cmp	fp, r1
	bcc	.L314
	ldr	r0, [sp, #0]
	ldrb	r6, [r0, #295]	@ zero_extendqisi2
	b	.L280
.L463:
	mov	r3, r1
	mov	sl, r7
	b	.L424
.L290:
	ldr	r3, [sp, #16]
	add	r2, r3, #3
	mvn	r5, r8
	mvn	r0, r2
	and	r8, r5, #3
	and	r2, r0, #3
	mov	r8, r8, asl #1
	mov	r2, r2, asl #1
	mov	r5, r6
	mov	r0, #6
	b	.L291
.L301:
	ldr	sl, [sp, #16]
	tst	r1, #1
	add	r2, sl, #1
	movne	r8, #4
	moveq	r8, #0
	ands	r7, r2, #1
	movne	r7, r3
	movne	r5, r6
	movne	r0, r6
	movne	r2, r7
	moveq	r5, r6
	moveq	r0, r6
	moveq	r2, r6
	b	.L302
.L278:
	ldr	sl, [sp, #16]
	add	r5, sl, #7
	mvn	r0, r5
	mvn	r8, r8
	and	r2, r0, #7
	and	r8, r8, #7
	mov	r5, r6
	mov	r0, #7
	b	.L279
.L465:
	.align	2
.L464:
	.word	png_pass_inc
	.size	png_do_read_interlace, .-png_do_read_interlace
	.section	.text.png_combine_row,"ax",%progbits
	.align	2
	.global	png_combine_row
	.hidden	png_combine_row
	.type	png_combine_row, %function
png_combine_row:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	cmp	r2, #255
	sub	sp, sp, #20
	mov	r4, r2
	mov	r7, r1
	beq	.L736
	ldrb	r5, [r0, #295]	@ zero_extendqisi2
	cmp	r5, #2
	beq	.L473
	cmp	r5, #4
	beq	.L474
	cmp	r5, #1
	beq	.L737
	ldr	fp, [r0, #228]
	cmp	fp, #0
	mov	r5, r5, lsr #3
	ldr	r8, [r0, #264]
	beq	.L503
	sub	r0, fp, #1
	ands	r3, r0, #3
	add	r8, r8, #1
	mov	sl, r1
	mov	r6, #128
	mov	r9, #0
	beq	.L502
	tst	r2, #128
	bne	.L738
.L601:
	cmp	r3, #1
	add	r8, r8, r5
	add	sl, r7, r5
	mov	r6, #64
	mov	r9, #1
	beq	.L502
	cmp	r3, #2
	beq	.L631
	tst	r6, r4
	bne	.L739
.L603:
	cmp	r6, #1
	movne	r6, r6, lsr #1
	moveq	r6, #128
	add	r9, r9, #1
	add	r8, r8, r5
	add	sl, sl, r5
.L631:
	tst	r6, r4
	bne	.L740
.L606:
	cmp	r6, #1
	movne	r6, r6, lsr #1
	moveq	r6, #128
	add	r9, r9, #1
	add	r8, r8, r5
	add	sl, sl, r5
	b	.L502
.L499:
	cmp	r6, #1
	add	r9, r9, #1
	moveq	r6, #128
	movne	r6, r6, lsr #1
	cmp	fp, r9
	bls	.L503
	tst	r6, r4
	add	r8, r8, r5
	add	sl, sl, r5
	bne	.L741
	cmp	r6, #1
	movne	r6, r6, lsr #1
	moveq	r6, #128
	tst	r6, r4
	add	r8, r8, r5
	add	sl, sl, r5
	bne	.L742
.L612:
	cmp	r6, #1
	movne	r6, r6, lsr #1
	moveq	r6, #128
	tst	r6, r4
	add	r8, r8, r5
	add	sl, sl, r5
	bne	.L743
.L614:
	cmp	r6, #1
	movne	r6, r6, lsr #1
	moveq	r6, #128
	add	r9, r9, #3
	add	r8, r8, r5
	add	sl, sl, r5
.L502:
	tst	r6, r4
	beq	.L499
	mov	r0, sl
	mov	r1, r8
	mov	r2, r5
	bl	memcpy
	b	.L499
.L736:
	ldrb	r3, [r0, #295]	@ zero_extendqisi2
	ldr	r2, [r0, #228]
	cmp	r3, #7
	mulls	r2, r3, r2
	ldr	r1, [r0, #264]
	movhi	r3, r3, lsr #3
	addls	r2, r2, #7
	mulhi	r2, r3, r2
	movls	r2, r2, lsr #3
	mov	r0, r7
	add	r1, r1, #1
	bl	memcpy
.L503:
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L474:
	ldr	r3, [r0, #140]
	ldr	sl, [r0, #228]
	ands	ip, r3, #65536
	moveq	r8, r5
	movne	ip, r5
	mvneq	r5, #3
	movne	r8, #0
	cmp	sl, #0
	ldr	r3, [r0, #264]
	beq	.L503
	tst	r2, #128
	mov	r1, #3840
	sub	r2, sl, #1
	and	r9, r2, #3
	add	r0, r3, #1
	add	r6, r1, #15
	mov	r2, r7
	beq	.L538
	ldrb	fp, [r0, #0]	@ zero_extendqisi2
	ldrb	r3, [r7, #0]	@ zero_extendqisi2
	mov	fp, fp, asr r8
	rsb	r1, r8, #4
	and	r3, r3, r6, asr r1
	and	fp, fp, #15
	orr	r3, r3, fp, asl r8
	strb	r3, [r7, #0]
.L538:
	cmp	r8, ip
	addeq	r2, r7, #1
	mov	r7, #1
	addne	r3, r8, r5
	addeq	r0, r0, #1
	moveq	r3, r8
	cmp	sl, r7
	mov	r1, #64
	bls	.L503
	cmp	r9, #0
	beq	.L732
	cmp	r9, r7
	beq	.L626
	cmp	r9, #2
	beq	.L627
	tst	r1, r4
	beq	.L542
	ldrb	r9, [r0, #0]	@ zero_extendqisi2
	ldrb	fp, [r2, #0]	@ zero_extendqisi2
	str	fp, [sp, #12]
	mov	fp, r9, asr r3
	ldr	r9, [sp, #12]
	str	fp, [sp, #4]
	rsb	fp, r3, #4
	and	fp, r9, r6, asr fp
	ldr	r9, [sp, #4]
	and	r9, r9, #15
	orr	fp, fp, r9, asl r3
	str	r9, [sp, #12]
	strb	fp, [r2, #0]
.L542:
	cmp	r3, ip
	addne	r3, r3, r5
	addeq	r0, r0, #1
	addeq	r2, r2, #1
	moveq	r3, r8
	cmp	r1, #1
	movne	r1, r1, asr #1
	moveq	r1, #128
	add	r7, r7, #1
.L627:
	tst	r1, r4
	beq	.L547
	ldrb	r9, [r0, #0]	@ zero_extendqisi2
	ldrb	fp, [r2, #0]	@ zero_extendqisi2
	str	fp, [sp, #12]
	mov	fp, r9, asr r3
	ldr	r9, [sp, #12]
	str	fp, [sp, #4]
	rsb	fp, r3, #4
	and	fp, r9, r6, asr fp
	ldr	r9, [sp, #4]
	and	r9, r9, #15
	orr	fp, fp, r9, asl r3
	str	r9, [sp, #12]
	strb	fp, [r2, #0]
.L547:
	cmp	r3, ip
	addne	r3, r3, r5
	addeq	r0, r0, #1
	addeq	r2, r2, #1
	moveq	r3, r8
	cmp	r1, #1
	movne	r1, r1, asr #1
	moveq	r1, #128
	add	r7, r7, #1
.L626:
	tst	r1, r4
	beq	.L552
	ldrb	r9, [r0, #0]	@ zero_extendqisi2
	ldrb	fp, [r2, #0]	@ zero_extendqisi2
	str	fp, [sp, #12]
	mov	fp, r9, asr r3
	ldr	r9, [sp, #12]
	str	fp, [sp, #4]
	rsb	fp, r3, #4
	and	fp, r9, r6, asr fp
	ldr	r9, [sp, #4]
	and	r9, r9, #15
	orr	fp, fp, r9, asl r3
	str	r9, [sp, #12]
	strb	fp, [r2, #0]
.L552:
	cmp	r3, ip
	addne	r3, r3, r5
	addeq	r0, r0, #1
	addeq	r2, r2, #1
	moveq	r3, r8
	add	r7, r7, #1
	cmp	r1, #1
	movne	r1, r1, asr #1
	moveq	r1, #128
	cmp	sl, r7
	bls	.L503
.L732:
	str	sl, [sp, #12]
	mov	fp, r8
.L498:
	tst	r1, r4
	beq	.L493
	ldrb	r9, [r0, #0]	@ zero_extendqisi2
	ldrb	sl, [r2, #0]	@ zero_extendqisi2
	mov	r8, r9, asr r3
	rsb	r9, r3, #4
	and	sl, sl, r6, asr r9
	and	r8, r8, #15
	orr	sl, sl, r8, asl r3
	strb	sl, [r2, #0]
.L493:
	cmp	r3, ip
	addeq	r0, r0, #1
	addeq	r2, r2, #1
	moveq	r3, fp
	addne	r3, r3, r5
	cmp	r1, #1
	moveq	r1, #128
	movne	r1, r1, asr #1
	tst	r1, r4
	add	r7, r7, #1
	beq	.L557
	ldrb	r9, [r0, #0]	@ zero_extendqisi2
	ldrb	sl, [r2, #0]	@ zero_extendqisi2
	mov	r8, r9, asr r3
	rsb	r9, r3, #4
	and	sl, sl, r6, asr r9
	and	r8, r8, #15
	orr	sl, sl, r8, asl r3
	strb	sl, [r2, #0]
.L557:
	cmp	r3, ip
	addne	r3, r3, r5
	addeq	r0, r0, #1
	addeq	r2, r2, #1
	moveq	r3, fp
	cmp	r1, #1
	movne	r1, r1, asr #1
	moveq	r1, #128
	tst	r1, r4
	beq	.L561
	ldrb	r9, [r0, #0]	@ zero_extendqisi2
	ldrb	sl, [r2, #0]	@ zero_extendqisi2
	mov	r8, r9, asr r3
	rsb	r9, r3, #4
	and	sl, sl, r6, asr r9
	and	r8, r8, #15
	orr	sl, sl, r8, asl r3
	strb	sl, [r2, #0]
.L561:
	cmp	r3, ip
	addne	r3, r3, r5
	addeq	r0, r0, #1
	addeq	r2, r2, #1
	moveq	r3, fp
	cmp	r1, #1
	movne	r1, r1, asr #1
	moveq	r1, #128
	tst	r1, r4
	beq	.L565
	ldrb	r9, [r0, #0]	@ zero_extendqisi2
	ldrb	sl, [r2, #0]	@ zero_extendqisi2
	mov	r8, r9, asr r3
	rsb	r9, r3, #4
	and	sl, sl, r6, asr r9
	and	r8, r8, #15
	orr	sl, sl, r8, asl r3
	strb	sl, [r2, #0]
.L565:
	cmp	r3, ip
	ldr	r8, [sp, #12]
	addne	r3, r3, r5
	addeq	r0, r0, #1
	addeq	r2, r2, #1
	moveq	r3, fp
	add	r7, r7, #3
	cmp	r1, #1
	movne	r1, r1, asr #1
	moveq	r1, #128
	cmp	r8, r7
	bhi	.L498
	b	.L503
.L737:
	ldr	ip, [r0, #140]
	ldr	sl, [r0, #228]
	and	ip, ip, #65536
	cmp	ip, #0
	movne	ip, #7
	movne	r5, #1
	mvneq	r5, #0
	movne	r8, #0
	moveq	r8, #7
	cmp	sl, #0
	ldr	r0, [r0, #264]
	beq	.L503
	tst	r2, #128
	mov	r6, #32512
	sub	r9, sl, #1
	add	r0, r0, #1
	add	r6, r6, #127
	and	r9, r9, #3
	mov	r2, r1
	beq	.L569
	ldrb	fp, [r0, #0]	@ zero_extendqisi2
	ldrb	r3, [r1, #0]	@ zero_extendqisi2
	mov	fp, fp, asr r8
	rsb	r1, r8, #7
	and	r3, r3, r6, asr r1
	and	fp, fp, #1
	orr	r3, r3, fp, asl r8
	strb	r3, [r7, #0]
.L569:
	cmp	r8, ip
	addeq	r2, r7, #1
	mov	r7, #1
	addne	r3, r8, r5
	addeq	r0, r0, #1
	moveq	r3, r8
	cmp	sl, r7
	mov	r1, #64
	bls	.L503
	cmp	r9, #0
	beq	.L734
	cmp	r9, r7
	beq	.L629
	cmp	r9, #2
	beq	.L630
	tst	r1, r4
	beq	.L573
	ldrb	r9, [r0, #0]	@ zero_extendqisi2
	ldrb	fp, [r2, #0]	@ zero_extendqisi2
	str	fp, [sp, #12]
	mov	fp, r9, asr r3
	ldr	r9, [sp, #12]
	str	fp, [sp, #4]
	rsb	fp, r3, #7
	and	fp, r9, r6, asr fp
	ldr	r9, [sp, #4]
	and	r9, r9, r7
	orr	fp, fp, r9, asl r3
	str	r9, [sp, #12]
	strb	fp, [r2, #0]
.L573:
	cmp	r3, ip
	addne	r3, r3, r5
	addeq	r0, r0, #1
	addeq	r2, r2, #1
	moveq	r3, r8
	cmp	r1, #1
	movne	r1, r1, asr #1
	moveq	r1, #128
	add	r7, r7, #1
.L630:
	tst	r1, r4
	beq	.L578
	ldrb	r9, [r0, #0]	@ zero_extendqisi2
	ldrb	fp, [r2, #0]	@ zero_extendqisi2
	str	fp, [sp, #12]
	mov	fp, r9, asr r3
	ldr	r9, [sp, #12]
	str	fp, [sp, #4]
	rsb	fp, r3, #7
	and	fp, r9, r6, asr fp
	ldr	r9, [sp, #4]
	and	r9, r9, #1
	orr	fp, fp, r9, asl r3
	str	r9, [sp, #12]
	strb	fp, [r2, #0]
.L578:
	cmp	r3, ip
	addne	r3, r3, r5
	addeq	r0, r0, #1
	addeq	r2, r2, #1
	moveq	r3, r8
	cmp	r1, #1
	movne	r1, r1, asr #1
	moveq	r1, #128
	add	r7, r7, #1
.L629:
	tst	r1, r4
	beq	.L583
	ldrb	r9, [r0, #0]	@ zero_extendqisi2
	ldrb	fp, [r2, #0]	@ zero_extendqisi2
	str	fp, [sp, #12]
	mov	fp, r9, asr r3
	ldr	r9, [sp, #12]
	str	fp, [sp, #4]
	rsb	fp, r3, #7
	and	fp, r9, r6, asr fp
	ldr	r9, [sp, #4]
	and	r9, r9, #1
	orr	fp, fp, r9, asl r3
	str	r9, [sp, #12]
	strb	fp, [r2, #0]
.L583:
	cmp	r3, ip
	addne	r3, r3, r5
	addeq	r0, r0, #1
	addeq	r2, r2, #1
	moveq	r3, r8
	add	r7, r7, #1
	cmp	r1, #1
	movne	r1, r1, asr #1
	moveq	r1, #128
	cmp	sl, r7
	bls	.L503
.L734:
	str	sl, [sp, #12]
	mov	fp, r8
.L482:
	tst	r1, r4
	beq	.L477
	ldrb	r9, [r0, #0]	@ zero_extendqisi2
	ldrb	sl, [r2, #0]	@ zero_extendqisi2
	mov	r8, r9, asr r3
	rsb	r9, r3, #7
	and	sl, sl, r6, asr r9
	and	r8, r8, #1
	orr	sl, sl, r8, asl r3
	strb	sl, [r2, #0]
.L477:
	cmp	r3, ip
	addeq	r0, r0, #1
	addeq	r2, r2, #1
	moveq	r3, fp
	addne	r3, r3, r5
	cmp	r1, #1
	moveq	r1, #128
	movne	r1, r1, asr #1
	tst	r1, r4
	add	r7, r7, #1
	beq	.L588
	ldrb	r9, [r0, #0]	@ zero_extendqisi2
	ldrb	sl, [r2, #0]	@ zero_extendqisi2
	mov	r8, r9, asr r3
	rsb	r9, r3, #7
	and	sl, sl, r6, asr r9
	and	r8, r8, #1
	orr	sl, sl, r8, asl r3
	strb	sl, [r2, #0]
.L588:
	cmp	r3, ip
	addne	r3, r3, r5
	addeq	r0, r0, #1
	addeq	r2, r2, #1
	moveq	r3, fp
	cmp	r1, #1
	movne	r1, r1, asr #1
	moveq	r1, #128
	tst	r1, r4
	beq	.L592
	ldrb	r9, [r0, #0]	@ zero_extendqisi2
	ldrb	sl, [r2, #0]	@ zero_extendqisi2
	mov	r8, r9, asr r3
	rsb	r9, r3, #7
	and	sl, sl, r6, asr r9
	and	r8, r8, #1
	orr	sl, sl, r8, asl r3
	strb	sl, [r2, #0]
.L592:
	cmp	r3, ip
	addne	r3, r3, r5
	addeq	r0, r0, #1
	addeq	r2, r2, #1
	moveq	r3, fp
	cmp	r1, #1
	movne	r1, r1, asr #1
	moveq	r1, #128
	tst	r1, r4
	beq	.L596
	ldrb	r9, [r0, #0]	@ zero_extendqisi2
	ldrb	sl, [r2, #0]	@ zero_extendqisi2
	mov	r8, r9, asr r3
	rsb	r9, r3, #7
	and	sl, sl, r6, asr r9
	and	r8, r8, #1
	orr	sl, sl, r8, asl r3
	strb	sl, [r2, #0]
.L596:
	cmp	r3, ip
	ldr	r8, [sp, #12]
	addne	r3, r3, r5
	addeq	r0, r0, #1
	addeq	r2, r2, #1
	moveq	r3, fp
	add	r7, r7, #3
	cmp	r1, #1
	movne	r1, r1, asr #1
	moveq	r1, #128
	cmp	r8, r7
	bhi	.L482
	b	.L503
.L473:
	ldr	r2, [r0, #140]
	ldr	sl, [r0, #228]
	and	ip, r2, #65536
	cmp	ip, #0
	movne	ip, #6
	movne	r8, #0
	moveq	r8, #6
	movne	r5, #2
	mvneq	r5, #1
	cmp	sl, #0
	ldr	r0, [r0, #264]
	beq	.L503
	mov	r6, #16128
	sub	r1, sl, #1
	tst	r4, #128
	add	r0, r0, #1
	add	r6, r6, #63
	and	r9, r1, #3
	mov	r2, r7
	beq	.L507
	ldrb	fp, [r0, #0]	@ zero_extendqisi2
	ldrb	r3, [r7, #0]	@ zero_extendqisi2
	mov	fp, fp, asr r8
	rsb	r1, r8, #6
	and	r3, r3, r6, asr r1
	and	fp, fp, #3
	orr	r3, r3, fp, asl r8
	strb	r3, [r7, #0]
.L507:
	cmp	r8, ip
	addeq	r2, r7, #1
	mov	r7, #1
	addne	r3, r8, r5
	addeq	r0, r0, #1
	moveq	r3, r8
	cmp	sl, r7
	mov	r1, #64
	bls	.L503
	cmp	r9, #0
	beq	.L730
	cmp	r9, r7
	beq	.L623
	cmp	r9, #2
	beq	.L624
	tst	r1, r4
	beq	.L511
	ldrb	r9, [r0, #0]	@ zero_extendqisi2
	ldrb	fp, [r2, #0]	@ zero_extendqisi2
	str	fp, [sp, #12]
	mov	fp, r9, asr r3
	ldr	r9, [sp, #12]
	str	fp, [sp, #4]
	rsb	fp, r3, #6
	and	fp, r9, r6, asr fp
	ldr	r9, [sp, #4]
	and	r9, r9, #3
	orr	fp, fp, r9, asl r3
	str	r9, [sp, #12]
	strb	fp, [r2, #0]
.L511:
	cmp	r3, ip
	addne	r3, r3, r5
	addeq	r0, r0, #1
	addeq	r2, r2, #1
	moveq	r3, r8
	cmp	r1, #1
	movne	r1, r1, asr #1
	moveq	r1, #128
	add	r7, r7, #1
.L624:
	tst	r1, r4
	beq	.L516
	ldrb	r9, [r0, #0]	@ zero_extendqisi2
	ldrb	fp, [r2, #0]	@ zero_extendqisi2
	str	fp, [sp, #12]
	mov	fp, r9, asr r3
	ldr	r9, [sp, #12]
	str	fp, [sp, #4]
	rsb	fp, r3, #6
	and	fp, r9, r6, asr fp
	ldr	r9, [sp, #4]
	and	r9, r9, #3
	orr	fp, fp, r9, asl r3
	str	r9, [sp, #12]
	strb	fp, [r2, #0]
.L516:
	cmp	r3, ip
	addne	r3, r3, r5
	addeq	r0, r0, #1
	addeq	r2, r2, #1
	moveq	r3, r8
	cmp	r1, #1
	movne	r1, r1, asr #1
	moveq	r1, #128
	add	r7, r7, #1
.L623:
	tst	r1, r4
	beq	.L521
	ldrb	r9, [r0, #0]	@ zero_extendqisi2
	ldrb	fp, [r2, #0]	@ zero_extendqisi2
	str	fp, [sp, #12]
	mov	fp, r9, asr r3
	ldr	r9, [sp, #12]
	str	fp, [sp, #4]
	rsb	fp, r3, #6
	and	fp, r9, r6, asr fp
	ldr	r9, [sp, #4]
	and	r9, r9, #3
	orr	fp, fp, r9, asl r3
	str	r9, [sp, #12]
	strb	fp, [r2, #0]
.L521:
	cmp	r3, ip
	addne	r3, r3, r5
	addeq	r0, r0, #1
	addeq	r2, r2, #1
	moveq	r3, r8
	add	r7, r7, #1
	cmp	r1, #1
	movne	r1, r1, asr #1
	moveq	r1, #128
	cmp	sl, r7
	bls	.L503
.L730:
	str	sl, [sp, #12]
	mov	fp, r8
.L490:
	tst	r1, r4
	beq	.L485
	ldrb	r9, [r0, #0]	@ zero_extendqisi2
	ldrb	sl, [r2, #0]	@ zero_extendqisi2
	mov	r8, r9, asr r3
	rsb	r9, r3, #6
	and	sl, sl, r6, asr r9
	and	r8, r8, #3
	orr	sl, sl, r8, asl r3
	strb	sl, [r2, #0]
.L485:
	cmp	r3, ip
	addeq	r0, r0, #1
	addeq	r2, r2, #1
	moveq	r3, fp
	addne	r3, r3, r5
	cmp	r1, #1
	moveq	r1, #128
	movne	r1, r1, asr #1
	tst	r1, r4
	add	r7, r7, #1
	beq	.L526
	ldrb	r9, [r0, #0]	@ zero_extendqisi2
	ldrb	sl, [r2, #0]	@ zero_extendqisi2
	mov	r8, r9, asr r3
	rsb	r9, r3, #6
	and	sl, sl, r6, asr r9
	and	r8, r8, #3
	orr	sl, sl, r8, asl r3
	strb	sl, [r2, #0]
.L526:
	cmp	r3, ip
	addne	r3, r3, r5
	addeq	r0, r0, #1
	addeq	r2, r2, #1
	moveq	r3, fp
	cmp	r1, #1
	movne	r1, r1, asr #1
	moveq	r1, #128
	tst	r1, r4
	beq	.L530
	ldrb	r9, [r0, #0]	@ zero_extendqisi2
	ldrb	sl, [r2, #0]	@ zero_extendqisi2
	mov	r8, r9, asr r3
	rsb	r9, r3, #6
	and	sl, sl, r6, asr r9
	and	r8, r8, #3
	orr	sl, sl, r8, asl r3
	strb	sl, [r2, #0]
.L530:
	cmp	r3, ip
	addne	r3, r3, r5
	addeq	r0, r0, #1
	addeq	r2, r2, #1
	moveq	r3, fp
	cmp	r1, #1
	movne	r1, r1, asr #1
	moveq	r1, #128
	tst	r1, r4
	beq	.L534
	ldrb	r9, [r0, #0]	@ zero_extendqisi2
	ldrb	sl, [r2, #0]	@ zero_extendqisi2
	mov	r8, r9, asr r3
	rsb	r9, r3, #6
	and	sl, sl, r6, asr r9
	and	r8, r8, #3
	orr	sl, sl, r8, asl r3
	strb	sl, [r2, #0]
.L534:
	cmp	r3, ip
	ldr	r8, [sp, #12]
	addne	r3, r3, r5
	addeq	r0, r0, #1
	addeq	r2, r2, #1
	moveq	r3, fp
	add	r7, r7, #3
	cmp	r1, #1
	movne	r1, r1, asr #1
	moveq	r1, #128
	cmp	r8, r7
	bhi	.L490
	b	.L503
.L741:
	mov	r0, sl
	mov	r1, r8
	mov	r2, r5
	bl	memcpy
	cmp	r6, #1
	movne	r6, r6, lsr #1
	moveq	r6, #128
	tst	r6, r4
	add	r8, r8, r5
	add	sl, sl, r5
	beq	.L612
.L742:
	mov	r0, sl
	mov	r1, r8
	mov	r2, r5
	bl	memcpy
	cmp	r6, #1
	movne	r6, r6, lsr #1
	moveq	r6, #128
	tst	r6, r4
	add	r8, r8, r5
	add	sl, sl, r5
	beq	.L614
.L743:
	mov	r0, sl
	mov	r1, r8
	mov	r2, r5
	bl	memcpy
	b	.L614
.L738:
	mov	r0, r1
	mov	r2, r5
	mov	r1, r8
	str	r3, [sp, #8]
	bl	memcpy
	ldr	r3, [sp, #8]
	b	.L601
.L740:
	mov	r0, sl
	mov	r1, r8
	mov	r2, r5
	bl	memcpy
	b	.L606
.L739:
	mov	r0, sl
	mov	r1, r8
	mov	r2, r5
	bl	memcpy
	b	.L603
	.size	png_combine_row, .-png_combine_row
	.section	.text.png_check_chunk_name,"ax",%progbits
	.align	2
	.global	png_check_chunk_name
	.hidden	png_check_chunk_name
	.type	png_check_chunk_name, %function
png_check_chunk_name:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, lr}
	ldrb	r3, [r1, #0]	@ zero_extendqisi2
	sub	ip, r3, #65
	cmp	ip, #57
	movls	r2, #0
	movhi	r2, #1
	sub	ip, r3, #91
	cmp	ip, #5
	movhi	r3, r2
	orrls	r3, r2, #1
	cmp	r3, #0
	bne	.L745
	ldrb	r3, [r1, #1]	@ zero_extendqisi2
	sub	ip, r3, #65
	cmp	ip, #57
	movls	r2, #0
	movhi	r2, #1
	sub	ip, r3, #91
	cmp	ip, #5
	movhi	r3, r2
	orrls	r3, r2, #1
	cmp	r3, #0
	beq	.L748
.L745:
	ldr	r1, .L749
	bl	png_chunk_error
.L747:
	ldmfd	sp!, {r3, lr}
	bx	lr
.L748:
	ldrb	r3, [r1, #2]	@ zero_extendqisi2
	sub	ip, r3, #65
	cmp	ip, #57
	movls	r2, #0
	movhi	r2, #1
	sub	ip, r3, #91
	cmp	ip, #5
	movhi	r3, r2
	orrls	r3, r2, #1
	cmp	r3, #0
	bne	.L745
	ldrb	ip, [r1, #3]	@ zero_extendqisi2
	sub	r1, ip, #65
	cmp	r1, #57
	movls	r2, #0
	movhi	r2, #1
	sub	r3, ip, #91
	cmp	r3, #5
	movhi	r1, r2
	orrls	r1, r2, #1
	cmp	r1, #0
	bne	.L745
	b	.L747
.L750:
	.align	2
.L749:
	.word	.LC3
	.size	png_check_chunk_name, .-png_check_chunk_name
	.section	.text.png_decompress_chunk,"ax",%progbits
	.align	2
	.global	png_decompress_chunk
	.hidden	png_decompress_chunk
	.type	png_decompress_chunk, %function
png_decompress_chunk:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 64
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	subs	r8, r1, #0
	sub	sp, sp, #68
	mov	r4, r0
	str	r2, [sp, #4]
	mov	r9, r3
	bne	.L752
	ldr	r3, [r0, #676]
	mov	ip, r2
	add	r1, r0, #200
	ldmia	r1, {r1, r2}	@ phole ldm
	add	r0, r3, r9
	rsb	r3, r9, ip
	cmp	r3, #0
	str	r0, [r4, #144]
	str	r1, [r4, #156]
	str	r2, [r4, #160]
	str	r3, [r4, #148]
	add	r6, r4, #144
	mov	r5, r8
	mov	fp, r8
	beq	.L767
.L766:
	mov	r0, r6
	mov	r1, #1
	bl	inflate
	cmp	r0, #1
	mov	r7, r0
	bhi	.L774
	ldr	r3, [r4, #160]
	cmp	r3, #0
	movne	r2, r0
	orreq	r2, r0, #1
	tst	r2, #255
	beq	.L761
	cmp	r5, #0
	beq	.L775
	ldr	r0, [r4, #204]
	add	lr, r0, #1
	rsb	sl, r3, lr
	add	r1, sl, r8
	mov	r0, r4
	bl	png_malloc_warn
	subs	sl, r0, #0
	beq	.L776
.L765:
	mov	r2, r8
	mov	r1, r5
	mov	r0, sl
	bl	memcpy
	mov	r1, r5
	mov	r0, r4
	bl	png_free
	ldr	r1, [r4, #204]
	ldr	r0, [r4, #160]
	rsb	r2, r0, r1
	add	r0, sl, r8
	ldr	r1, [r4, #200]
	bl	memcpy
	ldr	r5, [r4, #160]
	ldr	r2, [r4, #204]
	rsb	r3, r5, r2
	add	r8, r8, r3
	strb	fp, [sl, r8]
	mov	r5, sl
.L764:
	cmp	r7, #1
	beq	.L760
	ldr	lr, [r4, #200]
	ldr	r7, [r4, #204]
	str	lr, [r4, #156]
	str	r7, [r4, #160]
.L761:
	ldr	r3, [r4, #148]
	cmp	r3, #0
	bne	.L766
.L767:
	add	r7, sp, #12
	mov	r0, r7
	mov	r1, #52
	ldr	r2, .L783
	add	r3, r4, #312
	bl	snprintf
.L769:
	mov	r1, r7
	mov	r0, r4
	bl	png_warning
	cmp	r5, #0
	beq	.L777
.L770:
	mov	r8, #0
	strb	r8, [r5, r9]
	mov	r8, r9
.L760:
	mov	r0, r6
	bl	inflateReset
	mov	r3, #0
	str	r3, [r4, #148]
	mov	r0, r4
	ldr	r1, [r4, #676]
	bl	png_free
	str	r5, [r4, #676]
	ldr	ip, [sp, #104]
	str	r8, [ip, #0]
.L773:
	add	sp, sp, #68
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L775:
	ldr	r8, [r4, #204]
	rsb	r5, r3, r8
	add	r8, r5, r9
	mov	r0, r4
	add	r1, r8, #1
	bl	png_malloc_warn
	subs	r5, r0, #0
	beq	.L778
.L763:
	ldr	r1, [r4, #200]
	rsb	r2, r9, r8
	add	r0, r5, r9
	bl	memcpy
	mov	r0, r5
	ldr	r1, [r4, #676]
	mov	r2, r9
	bl	memcpy
	strb	fp, [r5, r8]
	b	.L764
.L752:
	add	r5, sp, #12
	ldr	r2, .L783+4
	mov	r3, r8
	mov	r1, #50
	mov	r0, r5
	bl	snprintf
	mov	r0, r4
	mov	r1, r5
	bl	png_warning
	ldr	r0, [r4, #676]
	mov	r1, #0
	strb	r1, [r0, r9]
	ldr	r2, [sp, #104]
	str	r9, [r2, #0]
	b	.L773
.L774:
	ldr	r1, [r4, #168]
	cmp	r1, #0
	ldreq	r1, .L783+8
	mov	r0, r4
	bl	png_warning
	mov	r0, r6
	bl	inflateReset
	mov	r2, #0
	cmp	r5, #0
	str	r2, [r4, #148]
	beq	.L779
.L757:
	add	r3, r8, r5
	mov	ip, #0
	strb	ip, [r3, #-1]
	ldr	r0, [sp, #4]
	ldr	r1, [r4, #676]
	sub	r8, r0, #1
	rsb	lr, r5, r1
	add	r8, r8, lr
	cmp	r8, #31
	movcs	r8, #31
	add	r0, r5, r9
	ldr	r1, .L783+8
	mov	r2, r8
	bl	memcpy
	cmp	r7, #1
	beq	.L760
	cmn	r7, #5
	beq	.L780
	cmn	r7, #3
	bne	.L767
	add	r7, sp, #12
	mov	r0, r7
	mov	r1, #52
	ldr	r2, .L783+12
	add	r3, r4, #312
	bl	snprintf
	b	.L769
.L776:
	mov	r1, r5
	mov	r0, r4
	bl	png_free
	mov	r0, r4
	ldr	r1, [r4, #676]
	bl	png_free
	str	sl, [r4, #676]
	mov	r0, r4
	ldr	r1, .L783+16
	bl	png_error
	b	.L765
.L780:
	add	r7, sp, #12
	mov	r0, r7
	mov	r1, #52
	ldr	r2, .L783+20
	add	r3, r4, #312
	bl	snprintf
	b	.L769
.L778:
	mov	r0, r4
	ldr	r1, [r4, #676]
	bl	png_free
	str	r5, [r4, #676]
	mov	r0, r4
	ldr	r1, .L783+24
	bl	png_error
	b	.L763
.L777:
	mov	r0, r4
	add	r1, r9, #1
	bl	png_malloc_warn
	subs	r5, r0, #0
	beq	.L781
.L771:
	mov	r0, r5
	ldr	r1, [r4, #676]
	mov	r2, r9
	bl	memcpy
	b	.L770
.L779:
	add	r8, r9, #32
	mov	r0, r4
	mov	r1, r8
	bl	png_malloc_warn
	subs	r5, r0, #0
	beq	.L782
.L758:
	mov	r0, r5
	ldr	r1, [r4, #676]
	mov	r2, r9
	bl	memcpy
	b	.L757
.L781:
	mov	r0, r4
	ldr	r1, [r4, #676]
	bl	png_free
	str	r5, [r4, #676]
	mov	r0, r4
	ldr	r1, .L783+28
	bl	png_error
	b	.L771
.L782:
	mov	r0, r4
	ldr	r1, [r4, #676]
	bl	png_free
	str	r5, [r4, #676]
	mov	r0, r4
	ldr	r1, .L783+32
	bl	png_error
	b	.L758
.L784:
	.align	2
.L783:
	.word	.LC9
	.word	.LC11
	.word	.LANCHOR0
	.word	.LC8
	.word	.LC6
	.word	.LC7
	.word	.LC5
	.word	.LC10
	.word	.LC4
	.size	png_decompress_chunk, .-png_decompress_chunk
	.section	.text.png_crc_read,"ax",%progbits
	.align	2
	.global	png_crc_read
	.hidden	png_crc_read
	.type	png_crc_read, %function
png_crc_read:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	subs	r6, r0, #0
	mov	r5, r1
	mov	r4, r2
	beq	.L787
	bl	png_read_data
	mov	r0, r6
	mov	r1, r5
	mov	r2, r4
	bl	png_calculate_crc
.L787:
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
	.size	png_crc_read, .-png_crc_read
	.section	.text.png_read_chunk_header,"ax",%progbits
	.align	2
	.global	png_read_chunk_header
	.hidden	png_read_chunk_header
	.type	png_read_chunk_header, %function
png_read_chunk_header:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	sub	sp, sp, #8
	mov	r1, sp
	mov	r2, #8
	mov	r4, r0
	bl	png_read_data
	ldrb	ip, [sp, #1]	@ zero_extendqisi2
	ldrb	r6, [sp, #0]	@ zero_extendqisi2
	mov	r1, ip, asl #16
	ldrb	r2, [sp, #3]	@ zero_extendqisi2
	add	r0, r1, r6, asl #24
	ldrb	r3, [sp, #2]	@ zero_extendqisi2
	add	r5, r0, r2
	adds	r5, r5, r3, asl #8
	bmi	.L793
.L789:
	ldr	lr, [sp, #4]
	mov	r0, r4
	str	lr, [r4, #312]
	add	r6, r4, #312
	bl	png_reset_crc
	mov	r0, r4
	mov	r1, r6
	mov	r2, #4
	bl	png_calculate_crc
	ldrb	r1, [r4, #312]	@ zero_extendqisi2
	sub	ip, r1, #65
	cmp	ip, #57
	movls	r2, #0
	movhi	r2, #1
	sub	r0, r1, #91
	cmp	r0, #5
	movhi	r3, r2
	orrls	r3, r2, #1
	cmp	r3, #0
	bne	.L790
	ldrb	r1, [r4, #313]	@ zero_extendqisi2
	sub	ip, r1, #65
	cmp	ip, #57
	movls	r2, #0
	movhi	r2, #1
	sub	r0, r1, #91
	cmp	r0, #5
	movhi	r3, r2
	orrls	r3, r2, #1
	cmp	r3, #0
	beq	.L794
.L790:
	mov	r0, r4
	ldr	r1, .L795
	bl	png_chunk_error
.L791:
	mov	r0, r5
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L794:
	ldrb	r1, [r4, #314]	@ zero_extendqisi2
	sub	r2, r1, #65
	cmp	r2, #57
	movls	r0, #0
	movhi	r0, #1
	sub	r3, r1, #91
	cmp	r3, #5
	movhi	lr, r0
	orrls	lr, r0, #1
	cmp	lr, #0
	bne	.L790
	ldrb	r3, [r4, #315]	@ zero_extendqisi2
	sub	r1, r3, #65
	cmp	r1, #57
	movls	r0, #0
	movhi	r0, #1
	sub	lr, r3, #91
	cmp	lr, #5
	movhi	ip, r0
	orrls	ip, r0, #1
	cmp	ip, #0
	bne	.L790
	b	.L791
.L793:
	mov	r0, r4
	ldr	r1, .L795+4
	bl	png_error
	b	.L789
.L796:
	.align	2
.L795:
	.word	.LC3
	.word	.LC0
	.size	png_read_chunk_header, .-png_read_chunk_header
	.section	.text.png_crc_finish,"ax",%progbits
	.align	2
	.global	png_crc_finish
	.hidden	png_crc_finish
	.type	png_crc_finish, %function
png_crc_finish:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	ldr	r7, [r0, #204]
	cmp	r7, r1
	sub	sp, sp, #12
	mov	r4, r0
	movcs	r8, r1
	bcs	.L799
	rsb	r8, r7, r1
	rsb	sl, r7, #0
	mov	r6, r7
	b	.L800
.L812:
	ldr	r6, [r4, #204]
	mov	r8, r3
.L800:
	ldr	r5, [r4, #200]
	mov	r0, r4
	mov	r1, r5
	mov	r2, r6
	bl	png_read_data
	mov	r2, r6
	mov	r0, r4
	mov	r1, r5
	bl	png_calculate_crc
	add	r3, r8, sl
	add	r2, r3, r7
	cmp	r7, r2
	bcc	.L812
.L799:
	cmp	r8, #0
	bne	.L813
.L801:
	ldrb	r3, [r4, #312]	@ zero_extendqisi2
	tst	r3, #32
	ldr	r3, [r4, #136]
	beq	.L802
	and	r0, r3, #768
	cmp	r0, #768
	beq	.L803
.L804:
	mov	r0, r4
	add	r1, sp, #4
	mov	r2, #4
	bl	png_read_data
	ldrb	r2, [sp, #5]	@ zero_extendqisi2
	ldrb	r1, [sp, #4]	@ zero_extendqisi2
	mov	r3, r2, asl #16
	ldrb	ip, [sp, #7]	@ zero_extendqisi2
	add	r0, r3, r1, asl #24
	ldrb	r1, [sp, #6]	@ zero_extendqisi2
	add	r2, r0, ip
	ldr	r3, [r4, #300]
	add	ip, r2, r1, asl #8
	cmp	ip, r3
	beq	.L811
	ldrb	lr, [r4, #312]	@ zero_extendqisi2
	tst	lr, #32
	ldr	r3, [r4, #136]
	beq	.L807
	tst	r3, #512
	beq	.L808
.L809:
	mov	r0, r4
	ldr	r1, .L814
	bl	png_chunk_error
	mov	r0, #1
	b	.L806
.L802:
	tst	r3, #2048
	beq	.L804
.L803:
	mov	r0, r4
	add	r1, sp, #4
	mov	r2, #4
	bl	png_read_data
.L811:
	mov	r0, #0
.L806:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	bx	lr
.L807:
	tst	r3, #1024
	beq	.L809
.L808:
	mov	r0, r4
	ldr	r1, .L814
	bl	png_chunk_warning
	mov	r0, #1
	b	.L806
.L813:
	ldr	r5, [r4, #200]
	mov	r0, r4
	mov	r1, r5
	mov	r2, r8
	bl	png_read_data
	mov	r0, r4
	mov	r1, r5
	mov	r2, r8
	bl	png_calculate_crc
	b	.L801
.L815:
	.align	2
.L814:
	.word	.LC12
	.size	png_crc_finish, .-png_crc_finish
	.section	.text.png_read_finish_row,"ax",%progbits
	.align	2
	.global	png_read_finish_row
	.hidden	png_read_finish_row
	.type	png_read_finish_row, %function
png_read_finish_row:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	ldr	r1, [r0, #256]
	ldr	r2, [r0, #236]
	add	r3, r1, #1
	cmp	r3, r2
	sub	sp, sp, #8
	mov	r4, r0
	str	r3, [r0, #256]
	bcc	.L838
	ldrb	sl, [r0, #319]	@ zero_extendqisi2
	cmp	sl, #0
	bne	.L848
.L818:
	ldr	sl, [r4, #136]
	tst	sl, #32
	bne	.L849
	add	r5, sp, #7
	mov	r9, #1
	str	r5, [r4, #156]
	str	r9, [r4, #160]
	add	sl, r4, #144
	ldr	r9, .L857
	add	r5, r4, #312
	mov	r6, sp
	b	.L835
.L824:
	mov	r0, sl
	mov	r1, #1
	bl	inflate
	cmp	r0, #1
	beq	.L850
	cmp	r0, #0
	beq	.L833
	ldr	r1, [r4, #168]
	cmp	r1, #0
	moveq	r1, r9
	mov	r0, r4
	bl	png_error
.L833:
	ldr	r3, [r4, #160]
	cmp	r3, #0
	beq	.L851
.L835:
	ldr	r1, [r4, #148]
	cmp	r1, #0
	bne	.L824
	ldr	r7, [r4, #296]
	subs	r1, r7, #0
	mov	r0, r4
	beq	.L826
.L853:
	add	r8, r4, #200
	ldmia	r8, {r8, lr}	@ phole ldm
	cmp	lr, r7
	movls	r7, lr
	str	lr, [r4, #148]
	mov	r0, r4
	strhi	r7, [r4, #148]
	mov	r1, r8
	mov	r2, r7
	str	r8, [r4, #144]
	bl	png_read_data
	mov	r0, r4
	mov	r2, r7
	mov	r1, r8
	bl	png_calculate_crc
	ldr	r2, [r4, #296]
	ldr	ip, [r4, #148]
	rsb	r0, ip, r2
	str	r0, [r4, #296]
	b	.L824
.L849:
	ldr	r3, [r4, #296]
	add	sl, r4, #144
.L822:
	cmp	r3, #0
	bne	.L836
	ldr	r3, [r4, #148]
	cmp	r3, #0
	beq	.L837
.L836:
	mov	r0, r4
	ldr	r1, .L857+4
	bl	png_warning
.L837:
	mov	r0, sl
	bl	inflateReset
	ldr	r0, [r4, #132]
	orr	r1, r0, #8
	str	r1, [r4, #132]
.L838:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L856:
	mov	r0, r4
	ldr	r1, .L857+8
	bl	png_error
.L825:
	mov	r0, r4
	str	r7, [r4, #296]
	bl	png_reset_crc
	mov	r0, r4
	mov	r1, r5
	mov	r2, #4
	bl	png_read_data
	mov	r0, r4
	mov	r1, r5
	mov	r2, #4
	bl	png_calculate_crc
	mov	r0, r5
	ldr	r1, .L857+12
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	bne	.L852
.L845:
	ldr	r7, [r4, #296]
	subs	r1, r7, #0
	mov	r0, r4
	bne	.L853
.L826:
	bl	png_crc_finish
	mov	r0, r4
	mov	r1, r6
	mov	r2, #4
	bl	png_read_data
	ldrb	r1, [sp, #1]	@ zero_extendqisi2
	ldrb	r3, [sp, #0]	@ zero_extendqisi2
	mov	r2, r1, asl #16
	ldrb	ip, [sp, #3]	@ zero_extendqisi2
	add	r0, r2, r3, asl #24
	ldrb	r8, [sp, #2]	@ zero_extendqisi2
	add	r7, r0, ip
	adds	r7, r7, r8, asl #8
	bpl	.L825
	b	.L856
.L852:
	mov	r0, r4
	ldr	r1, .L857+16
	bl	png_error
	b	.L845
.L851:
	mov	r0, r4
	ldr	r1, .L857+20
	bl	png_warning
	add	r2, r4, #132
	ldmia	r2, {r2, ip}	@ phole ldm
	orr	r0, r2, #8
	orr	r1, ip, #32
	str	r0, [r4, #132]
	str	r1, [r4, #136]
	ldr	r3, [r4, #296]
.L832:
	mov	lr, #0
	str	lr, [r4, #160]
	b	.L822
.L848:
	ldr	r5, [r0, #244]
	mov	r2, #0
	add	r3, r5, #1
	str	r2, [r0, #256]
	ldr	r1, [r0, #260]
	bl	png_memset_check
	ldr	r8, .L857+24
	ldrb	r5, [r4, #320]	@ zero_extendqisi2
	ldr	r7, .L857+28
	ldr	r9, .L857+32
	ldr	sl, .L857+36
.L821:
	add	r0, r5, #1
	and	r5, r0, #255
	cmp	r5, #6
	strb	r5, [r4, #320]
	bhi	.L818
	ldr	r0, [r4, #228]
	ldr	r1, [r8, r5, asl #2]
	sub	r2, r0, #1
	ldr	lr, [r7, r5, asl #2]
	add	r6, r2, r1
	rsb	r0, lr, r6
	bl	__aeabi_uidiv
	ldrb	r1, [r4, #325]	@ zero_extendqisi2
	cmp	r1, #7
	mulls	r2, r1, r0
	movhi	r2, r1, lsr #3
	mulhi	r2, r0, r2
	addls	r2, r2, #7
	ldr	r3, [r4, #140]
	movls	r2, r2, lsr #3
	add	ip, r2, #1
	tst	r3, #2
	mov	r6, r0
	str	r0, [r4, #252]
	str	ip, [r4, #248]
	bne	.L838
	ldr	r2, [r4, #232]
	ldr	r1, [r9, r5, asl #2]
	sub	lr, r2, #1
	ldr	r3, [sl, r5, asl #2]
	add	ip, lr, r1
	rsb	r0, r3, ip
	bl	__aeabi_uidiv
	cmp	r6, #0
	str	r0, [r4, #236]
	beq	.L821
	b	.L838
.L850:
	ldr	r3, [r4, #160]
	cmp	r3, #0
	beq	.L830
	ldr	r1, [r4, #148]
	cmp	r1, #0
	beq	.L855
.L830:
	mov	r0, r4
	ldr	r1, .L857+40
	bl	png_warning
	ldr	r3, [r4, #296]
.L831:
	ldr	lr, [r4, #132]
	ldr	r2, [r4, #136]
	orr	ip, lr, #8
	orr	r0, r2, #32
	str	ip, [r4, #132]
	str	r0, [r4, #136]
	b	.L832
.L855:
	ldr	r3, [r4, #296]
	cmp	r3, #0
	beq	.L831
	b	.L830
.L858:
	.align	2
.L857:
	.word	.LC15
	.word	.LC17
	.word	.LC0
	.word	png_IDAT
	.word	.LC13
	.word	.LC16
	.word	png_pass_inc
	.word	png_pass_start
	.word	png_pass_yinc
	.word	png_pass_ystart
	.word	.LC14
	.size	png_read_finish_row, .-png_read_finish_row
	.section	.text.png_handle_zTXt,"ax",%progbits
	.align	2
	.global	png_handle_zTXt
	.hidden	png_handle_zTXt
	.type	png_handle_zTXt, %function
png_handle_zTXt:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	ldr	r3, [r0, #132]
	tst	r3, #1
	sub	sp, sp, #16
	mov	r4, r0
	mov	r8, r1
	mov	r5, r2
	beq	.L874
.L860:
	tst	r3, #4
	orrne	r3, r3, #8
	strne	r3, [r4, #132]
	ldr	r1, [r4, #676]
	mov	r0, r4
	bl	png_free
	mov	r0, r4
	add	r1, r5, #1
	bl	png_malloc_warn
	cmp	r0, #0
	mov	r6, r0
	str	r0, [r4, #676]
	mov	r0, r4
	beq	.L875
	mov	r1, r6
	mov	r2, r5
	bl	png_read_data
	mov	r0, r4
	mov	r1, r6
	mov	r2, r5
	bl	png_calculate_crc
	mov	r0, r4
	mov	r1, #0
	bl	png_crc_finish
	cmp	r0, #0
	bne	.L873
	ldr	r1, [r4, #676]
	strb	r0, [r1, r5]
	ldr	r2, [r4, #676]
	ldrb	r3, [r2, #0]	@ zero_extendqisi2
	cmp	r3, #0
	mov	r7, r2
	beq	.L866
.L867:
	ldrb	r0, [r7, #1]!	@ zero_extendqisi2
	cmp	r0, #0
	bne	.L867
.L866:
	sub	lr, r5, #2
	add	ip, r2, lr
	cmp	r7, ip
	bcs	.L876
	ldrb	r6, [r7, #1]	@ zero_extendqisi2
	cmp	r6, #0
	add	r7, r7, #1
	bne	.L877
.L869:
	add	r3, r7, #1
	rsb	r6, r2, r3
	mov	r1, #0
	mov	r2, r5
	mov	r3, r6
	mov	r0, r4
	add	r5, sp, #12
	str	r5, [sp, #0]
	bl	png_decompress_chunk
	mov	r0, r4
	mov	r1, #16
	bl	png_malloc_warn
	subs	r5, r0, #0
	beq	.L878
	mov	r7, #0
	str	r7, [r5, #0]
	ldr	lr, [r4, #676]
	str	lr, [r5, #4]
	ldr	ip, [r4, #676]
	add	r0, ip, r6
	str	r0, [r5, #8]
	ldr	r1, [sp, #12]
	mov	r2, r5
	mov	r3, #1
	str	r1, [r5, #12]
	mov	r0, r4
	mov	r1, r8
	bl	png_set_text_2
	mov	r1, r5
	mov	r6, r0
	mov	r0, r4
	bl	png_free
	mov	r0, r4
	ldr	r1, [r4, #676]
	bl	png_free
	cmp	r6, r7
	str	r7, [r4, #676]
	bne	.L879
.L871:
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L876:
	ldr	r1, .L880
	mov	r0, r4
	bl	png_warning
.L873:
	mov	r0, r4
	ldr	r1, [r4, #676]
	bl	png_free
	mov	r2, #0
	str	r2, [r4, #676]
	b	.L871
.L874:
	ldr	r1, .L880+4
	bl	png_error
	ldr	r3, [r4, #132]
	b	.L860
.L879:
	mov	r0, r4
	ldr	r1, .L880+8
	bl	png_error
	b	.L871
.L877:
	mov	r0, r4
	ldr	r1, .L880+12
	bl	png_warning
	ldr	r2, [r4, #676]
	b	.L869
.L875:
	ldr	r1, .L880+16
	bl	png_warning
	b	.L871
.L878:
	ldr	r1, .L880+20
	mov	r0, r4
	bl	png_warning
	mov	r0, r4
	ldr	r1, [r4, #676]
	bl	png_free
	str	r5, [r4, #676]
	b	.L871
.L881:
	.align	2
.L880:
	.word	.LC20
	.word	.LC18
	.word	.LC23
	.word	.LC21
	.word	.LC19
	.word	.LC22
	.size	png_handle_zTXt, .-png_handle_zTXt
	.section	.text.png_handle_tEXt,"ax",%progbits
	.align	2
	.global	png_handle_tEXt
	.hidden	png_handle_tEXt
	.type	png_handle_tEXt, %function
png_handle_tEXt:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	ldr	r3, [r0, #132]
	tst	r3, #1
	mov	r4, r0
	mov	r7, r1
	mov	r5, r2
	beq	.L895
.L883:
	tst	r3, #4
	orrne	r3, r3, #8
	strne	r3, [r4, #132]
	ldr	r1, [r4, #676]
	mov	r0, r4
	bl	png_free
	mov	r0, r4
	add	r1, r5, #1
	bl	png_malloc_warn
	cmp	r0, #0
	mov	r6, r0
	str	r0, [r4, #676]
	mov	r0, r4
	beq	.L896
	mov	r1, r6
	mov	r2, r5
	bl	png_read_data
	mov	r0, r4
	mov	r1, r6
	mov	r2, r5
	bl	png_calculate_crc
	mov	r0, r4
	mov	r1, #0
	bl	png_crc_finish
	cmp	r0, #0
	bne	.L897
	ldr	r8, [r4, #676]
	strb	r0, [r8, r5]
	ldrb	r0, [r8, #0]	@ zero_extendqisi2
	cmp	r0, #0
	add	r5, r8, r5
	mov	r6, r8
	beq	.L889
.L890:
	ldrb	r1, [r6, #1]!	@ zero_extendqisi2
	cmp	r1, #0
	bne	.L890
.L889:
	cmp	r6, r5
	mov	r0, r4
	mov	r1, #16
	addne	r6, r6, #1
	bl	png_malloc_warn
	subs	r5, r0, #0
	beq	.L898
	mvn	ip, #0
	str	ip, [r5, #0]
	str	r6, [r5, #8]
	mov	r0, r6
	str	r8, [r5, #4]
	bl	strlen
	mov	r2, r5
	mov	r3, #1
	str	r0, [r5, #12]
	mov	r1, r7
	mov	r0, r4
	bl	png_set_text_2
	ldr	r1, [r4, #676]
	mov	r6, r0
	mov	r0, r4
	bl	png_free
	mov	r2, #0
	str	r2, [r4, #676]
	mov	r1, r5
	mov	r0, r4
	bl	png_free
	cmp	r6, #0
	bne	.L899
.L893:
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L899:
	mov	r0, r4
	ldr	r1, .L900
	bl	png_warning
	b	.L893
.L895:
	ldr	r1, .L900+4
	bl	png_error
	ldr	r3, [r4, #132]
	b	.L883
.L897:
	mov	r0, r4
	ldr	r1, [r4, #676]
	bl	png_free
	mov	r3, #0
	str	r3, [r4, #676]
	b	.L893
.L896:
	ldr	r1, .L900+8
	bl	png_warning
	b	.L893
.L898:
	ldr	r1, .L900+12
	mov	r0, r4
	bl	png_warning
	mov	r0, r4
	ldr	r1, [r4, #676]
	bl	png_free
	str	r5, [r4, #676]
	b	.L893
.L901:
	.align	2
.L900:
	.word	.LC27
	.word	.LC24
	.word	.LC25
	.word	.LC26
	.size	png_handle_tEXt, .-png_handle_tEXt
	.section	.text.png_handle_tIME,"ax",%progbits
	.align	2
	.global	png_handle_tIME
	.hidden	png_handle_tIME
	.type	png_handle_tIME, %function
png_handle_tIME:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	ldr	r3, [r0, #132]
	tst	r3, #1
	sub	sp, sp, #20
	mov	r4, r0
	mov	r6, r1
	mov	r5, r2
	beq	.L909
	cmp	r1, #0
	beq	.L904
	ldr	r2, [r1, #8]
	tst	r2, #512
	bne	.L910
.L904:
	tst	r3, #4
	orrne	r3, r3, #8
	strne	r3, [r4, #132]
	cmp	r5, #7
	beq	.L907
.L911:
	mov	r0, r4
	ldr	r1, .L912
	bl	png_warning
	mov	r0, r4
	mov	r1, r5
	bl	png_crc_finish
.L908:
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L909:
	ldr	r1, .L912+4
	bl	png_error
	ldr	r3, [r4, #132]
	tst	r3, #4
	orrne	r3, r3, #8
	strne	r3, [r4, #132]
	cmp	r5, #7
	bne	.L911
.L907:
	add	r7, sp, #8
	mov	r0, r4
	mov	r1, r7
	mov	r2, r5
	bl	png_read_data
	mov	r0, r4
	mov	r1, r7
	mov	r2, r5
	bl	png_calculate_crc
	mov	r0, r4
	mov	r1, #0
	bl	png_crc_finish
	cmp	r0, #0
	bne	.L908
	ldrb	r7, [sp, #8]	@ zero_extendqisi2
	ldrb	r5, [sp, #9]	@ zero_extendqisi2
	add	r0, sp, #16
	add	r1, r5, r7, asl #8
	ldrb	lr, [sp, #12]	@ zero_extendqisi2
	ldrb	r7, [sp, #14]	@ zero_extendqisi2
	ldrb	r5, [sp, #13]	@ zero_extendqisi2
	ldrb	ip, [sp, #11]	@ zero_extendqisi2
	ldrb	r3, [sp, #10]	@ zero_extendqisi2
	strh	r1, [r0, #-16]!	@ movhi
	mov	r2, sp
	mov	r0, r4
	mov	r1, r6
	strb	r7, [sp, #6]
	strb	r5, [sp, #5]
	strb	lr, [sp, #4]
	strb	ip, [sp, #3]
	strb	r3, [sp, #2]
	bl	png_set_tIME
	b	.L908
.L910:
	ldr	r1, .L912+8
	bl	png_warning
	mov	r0, r4
	mov	r1, r5
	bl	png_crc_finish
	b	.L908
.L913:
	.align	2
.L912:
	.word	.LC30
	.word	.LC28
	.word	.LC29
	.size	png_handle_tIME, .-png_handle_tIME
	.global	__aeabi_dcmple
	.section	.text.png_handle_sCAL,"ax",%progbits
	.align	2
	.global	png_handle_sCAL
	.hidden	png_handle_sCAL
	.type	png_handle_sCAL, %function
png_handle_sCAL:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	r3, [r0, #132]
	tst	r3, #1
	sub	sp, sp, #36
	mov	r4, r0
	mov	r5, r1
	mov	r8, r2
	beq	.L934
	tst	r3, #4
	bne	.L935
	cmp	r1, #0
	beq	.L916
	ldr	r3, [r1, #8]
	tst	r3, #16384
	bne	.L936
.L916:
	mov	r0, r4
	add	r1, r8, #1
	bl	png_malloc_warn
	cmp	r0, #0
	mov	r6, r0
	str	r0, [r4, #676]
	mov	r0, r4
	beq	.L937
	mov	r1, r6
	mov	r2, r8
	bl	png_read_data
	mov	r0, r4
	mov	r1, r6
	mov	r2, r8
	bl	png_calculate_crc
	mov	r0, r4
	mov	r1, #0
	bl	png_crc_finish
	cmp	r0, #0
	bne	.L933
	ldr	r6, [r4, #676]
	strb	r0, [r6, r8]
	ldr	r2, [r4, #676]
	add	r6, sp, #28
	add	r0, r2, #1
	mov	r1, r6
	bl	strtod
	mov	fp, r1
	ldr	r1, [sp, #28]
	mov	sl, r0
	ldrb	r0, [r1, #0]	@ zero_extendqisi2
	cmp	r0, #0
	bne	.L938
	ldr	r2, [r4, #676]
	ldrb	r9, [r2, #0]	@ zero_extendqisi2
	cmp	r9, #0
	mov	r9, r2
	beq	.L923
.L924:
	ldrb	ip, [r9, #1]!	@ zero_extendqisi2
	cmp	ip, #0
	bne	.L924
.L923:
	add	r9, r9, #1
	add	lr, r2, r8
	cmp	r9, lr
	bhi	.L939
	mov	r1, r6
	mov	r0, r9
	bl	strtod
	str	r0, [sp, #16]
	str	r1, [sp, #20]
	ldr	r2, [sp, #28]
	ldrb	r6, [r2, #0]	@ zero_extendqisi2
	cmp	r6, #0
	bne	.L940
	ldr	r7, [r4, #676]
	add	r8, r7, r8
	cmp	r9, r8
	bhi	.L927
	mov	r0, sl
	mov	r1, fp
	mov	r2, #0
	mov	r3, #0
	bl	__aeabi_dcmple
	cmp	r0, #0
	bne	.L927
	add	r1, sp, #16
	ldmia	r1, {r0-r1}
	mov	r2, #0
	mov	r3, #0
	bl	__aeabi_dcmple
	cmp	r0, #0
	beq	.L932
.L927:
	ldr	r1, .L941
	mov	r0, r4
	bl	png_warning
.L933:
	mov	r0, r4
	ldr	r1, [r4, #676]
	bl	png_free
	mov	r3, #0
	str	r3, [r4, #676]
.L930:
	add	sp, sp, #36
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L934:
	ldr	r1, .L941+4
	bl	png_error
	b	.L916
.L936:
	ldr	r1, .L941+8
	bl	png_warning
	mov	r0, r4
	mov	r1, r8
	bl	png_crc_finish
	b	.L930
.L935:
	ldr	r1, .L941+12
	bl	png_warning
	mov	r0, r4
	mov	r1, r8
	bl	png_crc_finish
	b	.L930
.L939:
	ldr	r1, .L941+16
	mov	r0, r4
	bl	png_warning
	b	.L933
.L938:
	mov	r0, r4
	ldr	r1, .L941+20
	bl	png_warning
	b	.L930
.L937:
	ldr	r1, .L941+24
	bl	png_warning
	b	.L930
.L940:
	mov	r0, r4
	ldr	r1, .L941+28
	bl	png_warning
	b	.L930
.L932:
	ldrb	r2, [r7, #0]	@ zero_extendqisi2
	add	r8, sp, #16
	ldmia	r8, {r7-r8}
	mov	r1, r5
	mov	r0, r4
	stmia	sp, {sl-fp}
	str	r7, [sp, #8]
	str	r8, [sp, #12]
	bl	png_set_sCAL
	mov	r0, r4
	ldr	r1, [r4, #676]
	bl	png_free
	str	r6, [r4, #676]
	b	.L930
.L942:
	.align	2
.L941:
	.word	.LC38
	.word	.LC31
	.word	.LC33
	.word	.LC32
	.word	.LC36
	.word	.LC35
	.word	.LC34
	.word	.LC37
	.size	png_handle_sCAL, .-png_handle_sCAL
	.section	.text.png_handle_pCAL,"ax",%progbits
	.align	2
	.global	png_handle_pCAL
	.hidden	png_handle_pCAL
	.type	png_handle_pCAL, %function
png_handle_pCAL:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	r3, [r0, #132]
	tst	r3, #1
	sub	sp, sp, #60
	mov	r4, r0
	mov	r5, r1
	mov	r6, r2
	beq	.L1003
	tst	r3, #4
	ldrne	r1, .L1008
	bne	.L1002
	cmp	r5, #0
	beq	.L945
	ldr	r3, [r5, #8]
	tst	r3, #1024
	bne	.L1004
.L945:
	ldr	r1, [r4, #676]
	mov	r0, r4
	bl	png_free
	mov	r0, r4
	add	r1, r6, #1
	bl	png_malloc_warn
	cmp	r0, #0
	mov	r7, r0
	str	r0, [r4, #676]
	mov	r0, r4
	beq	.L1005
	mov	r1, r7
	mov	r2, r6
	bl	png_read_data
	mov	r0, r4
	mov	r1, r7
	mov	r2, r6
	bl	png_calculate_crc
	mov	r0, r4
	mov	r1, #0
	bl	png_crc_finish
	cmp	r0, #0
	bne	.L1001
	ldr	r1, [r4, #676]
	strb	r0, [r1, r6]
	ldr	r9, [r4, #676]
	ldrb	r0, [r9, #0]	@ zero_extendqisi2
	cmp	r0, #0
	mov	r8, r9
	beq	.L951
.L952:
	ldrb	r2, [r8, #1]!	@ zero_extendqisi2
	cmp	r2, #0
	bne	.L952
.L951:
	add	r9, r9, r6
	add	ip, r8, #12
	cmp	r9, ip
	bls	.L1006
	ldrb	r7, [r8, #9]	@ zero_extendqisi2
	ldrb	r6, [r8, #10]	@ zero_extendqisi2
	add	lr, r8, #1
	ldrb	r0, [lr, #2]	@ zero_extendqisi2
	subs	fp, r6, #2
	movne	fp, #1
	cmp	r7, #0
	add	r1, r8, #5
	movne	fp, #0
	ldrb	r3, [lr, #1]	@ zero_extendqisi2
	ldrb	ip, [r1, #3]	@ zero_extendqisi2
	ldrb	r2, [r8, #1]	@ zero_extendqisi2
	cmp	fp, #0
	ldrb	fp, [lr, #3]	@ zero_extendqisi2
	str	r0, [sp, #48]
	ldrb	lr, [r8, #5]	@ zero_extendqisi2
	str	lr, [sp, #36]
	ldrb	r0, [r1, #1]	@ zero_extendqisi2
	str	r0, [sp, #40]
	ldrb	lr, [r1, #2]	@ zero_extendqisi2
	str	lr, [sp, #44]
	bne	.L954
	cmp	r7, #1
	cmpne	r7, #2
	movne	r1, #0
	moveq	r1, #1
	cmp	r6, #3
	moveq	r1, #0
	andne	r1, r1, #1
	cmp	r1, #0
	bne	.L954
	subs	r0, r6, #4
	movne	r0, #1
	cmp	r7, #3
	movne	r0, #0
	cmp	r0, #0
	bne	.L954
	cmp	r7, #3
	bhi	.L1007
.L956:
	ldrb	sl, [r8, #11]	@ zero_extendqisi2
	add	r8, r8, #11
	cmp	sl, #0
	str	r8, [sp, #52]
	mov	sl, r8
	beq	.L958
.L959:
	ldrb	lr, [sl, #1]!	@ zero_extendqisi2
	cmp	lr, #0
	bne	.L959
.L958:
	mov	r0, r4
	mov	r1, r6, asl #2
	str	r2, [sp, #28]
	str	r3, [sp, #24]
	str	ip, [sp, #32]
	bl	png_malloc_warn
	subs	r8, r0, #0
	ldr	r2, [sp, #28]
	ldr	r3, [sp, #24]
	ldr	ip, [sp, #32]
	beq	.L960
	cmp	r6, #0
	movne	lr, #0
	beq	.L962
.L966:
	add	sl, sl, #1
	cmp	r9, sl
	str	sl, [r8, lr, asl #2]
	bcc	.L963
	ldrb	r1, [sl, #0]	@ zero_extendqisi2
	cmp	r1, #0
	beq	.L964
	rsb	r0, sl, r9
	ands	r1, r0, #3
	beq	.L968
	add	sl, sl, #1
	cmp	r9, sl
	bcc	.L963
	ldrb	r0, [sl, #0]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L964
	cmp	r1, #1
	beq	.L968
	cmp	r1, #2
	beq	.L989
	ldrb	r1, [sl, #1]!	@ zero_extendqisi2
	cmp	r1, #0
	beq	.L964
.L989:
	ldrb	r0, [sl, #1]!	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L964
.L968:
	add	sl, sl, #1
	cmp	r9, sl
	mov	r1, sl
	bcc	.L963
	ldrb	r0, [sl, #0]	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L964
	ldrb	r0, [sl, #1]!	@ zero_extendqisi2
	cmp	r0, #0
	beq	.L964
	ldrb	sl, [r1, #2]	@ zero_extendqisi2
	cmp	sl, #0
	add	sl, r1, #2
	beq	.L964
	ldrb	r0, [r1, #3]	@ zero_extendqisi2
	cmp	r0, #0
	add	sl, r1, #3
	bne	.L968
.L964:
	add	lr, lr, #1
	cmp	lr, r6
	blt	.L966
.L962:
	add	fp, fp, r2, asl #24
	ldr	r2, [sp, #36]
	ldr	r1, [sp, #40]
	add	lr, ip, r2, asl #24
	ldr	r0, [sp, #44]
	add	ip, lr, r1, asl #16
	ldr	r1, [sp, #48]
	add	lr, ip, r0, asl #8
	add	r3, fp, r3, asl #16
	ldr	ip, [sp, #52]
	ldr	r2, [r4, #676]
	add	r3, r3, r1, asl #8
	mov	r0, r4
	mov	r1, r5
	str	lr, [sp, #0]
	str	r7, [sp, #4]
	str	r6, [sp, #8]
	str	ip, [sp, #12]
	str	r8, [sp, #16]
	bl	png_set_pCAL
	b	.L1000
.L1006:
	ldr	r1, .L1008+4
	mov	r0, r4
	bl	png_warning
.L1001:
	mov	r0, r4
	ldr	r1, [r4, #676]
	bl	png_free
	mov	r3, #0
	str	r3, [r4, #676]
.L967:
	add	sp, sp, #60
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L954:
	ldr	r1, .L1008+8
	mov	r0, r4
	bl	png_warning
	b	.L1001
.L1003:
	ldr	r1, .L1008+12
	bl	png_error
	b	.L945
.L1004:
	ldr	r1, .L1008+16
.L1002:
	bl	png_warning
	mov	r0, r4
	mov	r1, r6
	add	sp, sp, #60
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	b	png_crc_finish
.L963:
	mov	r0, r4
	ldr	r1, .L1008+4
	bl	png_warning
.L1000:
	mov	r0, r4
	ldr	r1, [r4, #676]
	bl	png_free
	mov	r3, #0
	str	r3, [r4, #676]
	mov	r0, r4
	mov	r1, r8
	bl	png_free
	b	.L967
.L1005:
	ldr	r1, .L1008+20
	bl	png_warning
	b	.L967
.L1007:
	mov	r0, r4
	ldr	r1, .L1008+24
	str	r2, [sp, #28]
	str	r3, [sp, #24]
	str	ip, [sp, #32]
	bl	png_warning
	ldr	ip, [sp, #32]
	ldr	r3, [sp, #24]
	ldr	r2, [sp, #28]
	b	.L956
.L960:
	mov	r0, r4
	ldr	r1, [r4, #676]
	bl	png_free
	str	r8, [r4, #676]
	mov	r0, r4
	ldr	r1, .L1008+28
	bl	png_warning
	b	.L967
.L1009:
	.align	2
.L1008:
	.word	.LC40
	.word	.LC43
	.word	.LC44
	.word	.LC39
	.word	.LC41
	.word	.LC42
	.word	.LC45
	.word	.LC46
	.size	png_handle_pCAL, .-png_handle_pCAL
	.section	.text.png_handle_hIST,"ax",%progbits
	.align	2
	.global	png_handle_hIST
	.hidden	png_handle_hIST
	.type	png_handle_hIST, %function
png_handle_hIST:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 520
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	ldr	r3, [r0, #132]
	tst	r3, #1
	sub	sp, sp, #520
	mov	r4, r0
	mov	r9, r1
	mov	r5, r2
	beq	.L1035
	tst	r3, #4
	bne	.L1036
	tst	r3, #2
	beq	.L1037
	cmp	r1, #0
	beq	.L1012
	ldr	r3, [r1, #8]
	tst	r3, #64
	bne	.L1038
.L1012:
	mov	r1, #308
	ldrh	r0, [r4, r1]
	mov	sl, r5, lsr #1
	cmp	sl, #256
	cmpls	r0, sl
	bne	.L1016
.L1039:
	cmp	sl, #0
	beq	.L1018
	add	r5, sp, #516
	mov	r2, #2
	mov	r0, r4
	mov	r1, r5
	bl	png_read_data
	mov	r2, #2
	mov	r0, r4
	mov	r1, r5
	bl	png_calculate_crc
	ldrb	r7, [sp, #517]	@ zero_extendqisi2
	ldrb	r2, [sp, #516]	@ zero_extendqisi2
	sub	r8, sl, #1
	add	r6, r7, r2, asl #8
	cmp	sl, #1
	add	r7, sp, #4
	strh	r6, [r7, #0]	@ movhi
	and	r8, r8, #3
	mov	r6, #1
	bls	.L1018
	cmp	r8, #0
	beq	.L1019
	cmp	r8, #1
	beq	.L1033
	cmp	r8, #2
	beq	.L1034
	mov	r2, #2
	mov	r0, r4
	mov	r1, r5
	bl	png_read_data
	mov	r1, r5
	mov	r2, #2
	mov	r0, r4
	bl	png_calculate_crc
	ldrb	r6, [sp, #517]	@ zero_extendqisi2
	ldrb	r2, [sp, #516]	@ zero_extendqisi2
	add	r1, r6, r2, asl #8
	mov	r6, #2
	strh	r1, [r7, r6]	@ movhi
.L1034:
	mov	r2, #2
	mov	r0, r4
	mov	r1, r5
	bl	png_read_data
	mov	r0, r4
	mov	r1, r5
	mov	r2, #2
	bl	png_calculate_crc
	ldrb	r1, [sp, #516]	@ zero_extendqisi2
	ldrb	r3, [sp, #517]	@ zero_extendqisi2
	mov	r0, r6, asl #1
	add	ip, r3, r1, asl #8
	strh	ip, [r7, r0]	@ movhi
	add	r6, r6, #1
.L1033:
	mov	r2, #2
	mov	r0, r4
	mov	r1, r5
	bl	png_read_data
	mov	r0, r4
	mov	r1, r5
	mov	r2, #2
	bl	png_calculate_crc
	ldrb	r1, [sp, #516]	@ zero_extendqisi2
	ldrb	r0, [sp, #517]	@ zero_extendqisi2
	mov	ip, r6, asl #1
	add	r6, r6, #1
	add	r2, r0, r1, asl #8
	cmp	sl, r6
	strh	r2, [r7, ip]	@ movhi
	bls	.L1018
.L1019:
	mov	r2, #2
	mov	r0, r4
	mov	r1, r5
	bl	png_read_data
	mov	r0, r4
	mov	r1, r5
	mov	r2, #2
	bl	png_calculate_crc
	ldrb	r3, [sp, #517]	@ zero_extendqisi2
	ldrb	r0, [sp, #516]	@ zero_extendqisi2
	mov	r8, r6, asl #1
	add	ip, r3, r0, asl #8
	strh	ip, [r7, r8]	@ movhi
	mov	r2, #2
	mov	r0, r4
	mov	r1, r5
	bl	png_read_data
	mov	r0, r4
	mov	r1, r5
	mov	r2, #2
	bl	png_calculate_crc
	ldrb	r2, [sp, #516]	@ zero_extendqisi2
	ldrb	r1, [sp, #517]	@ zero_extendqisi2
	add	r8, r6, #1
	add	ip, r1, r2, asl #8
	mov	r3, r8, asl #1
	strh	ip, [r7, r3]	@ movhi
	mov	r2, #2
	mov	r0, r4
	mov	r1, r5
	bl	png_read_data
	mov	r0, r4
	mov	r1, r5
	mov	r2, #2
	bl	png_calculate_crc
	ldrb	r3, [sp, #517]	@ zero_extendqisi2
	ldrb	r0, [sp, #516]	@ zero_extendqisi2
	add	r8, r8, #1
	add	ip, r3, r0, asl #8
	mov	r8, r8, asl #1
	strh	ip, [r7, r8]	@ movhi
	mov	r2, #2
	mov	r0, r4
	mov	r1, r5
	bl	png_read_data
	mov	r0, r4
	mov	r1, r5
	mov	r2, #2
	bl	png_calculate_crc
	ldrb	r1, [sp, #516]	@ zero_extendqisi2
	ldrb	r2, [sp, #517]	@ zero_extendqisi2
	add	r0, r6, #3
	add	r6, r6, #4
	add	ip, r2, r1, asl #8
	mov	r3, r0, asl #1
	cmp	sl, r6
	strh	ip, [r7, r3]	@ movhi
	bhi	.L1019
.L1018:
	mov	r0, r4
	mov	r1, #0
	bl	png_crc_finish
	cmp	r0, #0
	moveq	r0, r4
	moveq	r1, r9
	addeq	r2, sp, #4
	bleq	png_set_hIST
.L1020:
	add	sp, sp, #520
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L1035:
	ldr	r1, .L1040
	bl	png_error
	mov	r1, #308
	ldrh	r0, [r4, r1]
	mov	sl, r5, lsr #1
	cmp	sl, #256
	cmpls	r0, sl
	beq	.L1039
.L1016:
	mov	r0, r4
	ldr	r1, .L1040+4
	bl	png_warning
	mov	r0, r4
	mov	r1, r5
	bl	png_crc_finish
	b	.L1020
.L1037:
	ldr	r1, .L1040+8
	bl	png_warning
	mov	r0, r4
	mov	r1, r5
	bl	png_crc_finish
	b	.L1020
.L1036:
	ldr	r1, .L1040+12
	bl	png_warning
	mov	r0, r4
	mov	r1, r5
	bl	png_crc_finish
	b	.L1020
.L1038:
	ldr	r1, .L1040+16
	bl	png_warning
	mov	r0, r4
	mov	r1, r5
	bl	png_crc_finish
	b	.L1020
.L1041:
	.align	2
.L1040:
	.word	.LC47
	.word	.LC51
	.word	.LC49
	.word	.LC48
	.word	.LC50
	.size	png_handle_hIST, .-png_handle_hIST
	.global	__aeabi_idivmod
	.global	__aeabi_idiv
	.section	.text.png_handle_sPLT,"ax",%progbits
	.align	2
	.global	png_handle_sPLT
	.hidden	png_handle_sPLT
	.type	png_handle_sPLT, %function
png_handle_sPLT:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	ldr	r3, [r0, #132]
	tst	r3, #1
	sub	sp, sp, #16
	mov	r4, r0
	mov	r7, r1
	mov	r5, r2
	beq	.L1063
	tst	r3, #4
	bne	.L1064
.L1044:
	ldr	r1, [r4, #676]
	mov	r0, r4
	bl	png_free
	add	r1, r5, #1
	mov	r0, r4
	bl	png_malloc
	mov	r6, r0
	str	r0, [r4, #676]
	mov	r1, r6
	mov	r0, r4
	mov	r2, r5
	bl	png_read_data
	mov	r0, r4
	mov	r1, r6
	mov	r2, r5
	bl	png_calculate_crc
	mov	r0, r4
	mov	r1, #0
	bl	png_crc_finish
	cmp	r0, #0
	bne	.L1065
	ldr	r1, [r4, #676]
	strb	r0, [r1, r5]
	ldr	r8, [r4, #676]
	ldrb	r0, [r8, #0]	@ zero_extendqisi2
	cmp	r0, #0
	mov	r3, r8
	beq	.L1048
.L1049:
	ldrb	r2, [r3, #1]!	@ zero_extendqisi2
	cmp	r2, #0
	bne	.L1049
.L1048:
	sub	r6, r5, #2
	add	ip, r8, r6
	add	r6, r3, #1
	cmp	r6, ip
	bhi	.L1066
	ldrb	r9, [r3, #1]	@ zero_extendqisi2
	add	r6, r6, #1
	cmp	r9, #8
	rsb	lr, r6, r8
	moveq	sl, #6
	movne	sl, #10
	add	r5, lr, r5
	mov	r0, r5
	mov	r1, sl
	strb	r9, [sp, #4]
	bl	__aeabi_idivmod
	subs	r9, r1, #0
	bne	.L1067
	mov	r1, sl
	mov	r0, r5
	bl	__aeabi_idiv
	ldr	r1, .L1069
	cmp	r0, r1
	str	r0, [sp, #12]
	bhi	.L1068
	add	r2, r0, r0, asl #2
	mov	r1, r2, asl #1
	mov	r0, r4
	bl	png_malloc_warn
	cmp	r0, #0
	str	r0, [sp, #8]
	beq	.L1055
	ldr	ip, [sp, #12]
	cmp	ip, #0
	movgt	r1, r9
	bgt	.L1060
	b	.L1057
.L1058:
	ldrb	lr, [r2], #2	@ zero_extendqisi2
	ldrb	ip, [r6, #1]	@ zero_extendqisi2
	add	ip, ip, lr, asl #8
	strh	ip, [r0, r9]	@ movhi
	ldrb	ip, [r6, #2]	@ zero_extendqisi2
	ldrb	lr, [r2, #1]	@ zero_extendqisi2
	add	r0, lr, ip, asl #8
	strh	r0, [r3, #2]	@ movhi
	add	lr, r2, #2
	ldrb	ip, [r2, #2]	@ zero_extendqisi2
	ldrb	r2, [lr, #1]	@ zero_extendqisi2
	add	r6, r2, ip, asl #8
	strh	r6, [r3, #4]	@ movhi
	add	r6, lr, #2
	ldrb	ip, [r6, #1]	@ zero_extendqisi2
	ldrb	r0, [lr, #2]	@ zero_extendqisi2
	add	r2, ip, r0, asl #8
	strh	r2, [r3, #6]	@ movhi
	add	r6, r6, #2
.L1059:
	ldrb	ip, [r6, #1]	@ zero_extendqisi2
	ldrb	r0, [r6, #0]	@ zero_extendqisi2
	add	r2, ip, r0, asl #8
	strh	r2, [r3, #8]	@ movhi
	ldr	r3, [sp, #12]
	add	r1, r1, #1
	cmp	r3, r1
	add	r9, r9, #10
	ble	.L1057
	ldr	r0, [sp, #8]
	add	r6, r6, #2
.L1060:
	ldrb	r3, [sp, #4]	@ zero_extendqisi2
	cmp	r3, #8
	mov	r2, r6
	add	r3, r0, r9
	bne	.L1058
	ldrb	lr, [r2], #1	@ zero_extendqisi2
	strh	lr, [r0, r9]	@ movhi
	ldrb	ip, [r6, #1]	@ zero_extendqisi2
	strh	ip, [r3, #2]	@ movhi
	ldrb	r0, [r2, #1]	@ zero_extendqisi2
	strh	r0, [r3, #4]	@ movhi
	add	r6, r2, #1
	ldrb	lr, [r6, #1]	@ zero_extendqisi2
	add	r6, r6, #2
	strh	lr, [r3, #6]	@ movhi
	b	.L1059
.L1063:
	ldr	r1, .L1069+4
	bl	png_error
	b	.L1044
.L1057:
	ldr	r3, [r4, #676]
	add	lr, sp, #16
	str	r3, [lr, #-16]!
	mov	r1, r7
	mov	r2, sp
	mov	r3, #1
	mov	r0, r4
	bl	png_set_sPLT
	mov	r0, r4
	ldr	r1, [r4, #676]
	bl	png_free
	mov	r1, #0
	str	r1, [r4, #676]
	mov	r0, r4
	ldr	r1, [sp, #8]
	bl	png_free
.L1061:
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L1067:
	mov	r1, r8
	mov	r0, r4
	bl	png_free
	mov	r0, #0
	str	r0, [r4, #676]
	ldr	r1, .L1069+8
	mov	r0, r4
	bl	png_warning
	b	.L1061
.L1065:
	mov	r0, r4
	ldr	r1, [r4, #676]
	bl	png_free
	mov	r3, #0
	str	r3, [r4, #676]
	b	.L1061
.L1068:
	mov	r0, r4
	ldr	r1, .L1069+12
	bl	png_warning
	b	.L1061
.L1064:
	ldr	r1, .L1069+16
	bl	png_warning
	mov	r0, r4
	mov	r1, r5
	bl	png_crc_finish
	b	.L1061
.L1066:
	mov	r1, r8
	mov	r0, r4
	bl	png_free
	mov	r3, #0
	str	r3, [r4, #676]
	mov	r0, r4
	ldr	r1, .L1069+20
	bl	png_warning
	b	.L1061
.L1055:
	mov	r0, r4
	ldr	r1, .L1069+24
	bl	png_warning
	b	.L1061
.L1070:
	.align	2
.L1069:
	.word	429496729
	.word	.LC52
	.word	.LC55
	.word	.LC56
	.word	.LC53
	.word	.LC54
	.word	.LC57
	.size	png_handle_sPLT, .-png_handle_sPLT
	.section	.text.png_handle_iCCP,"ax",%progbits
	.align	2
	.global	png_handle_iCCP
	.hidden	png_handle_iCCP
	.type	png_handle_iCCP, %function
png_handle_iCCP:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	ldr	r3, [r0, #132]
	tst	r3, #1
	sub	sp, sp, #20
	mov	r4, r0
	mov	r5, r1
	mov	r7, r2
	beq	.L1089
	tst	r3, #4
	bne	.L1090
	tst	r3, #2
	bne	.L1091
.L1073:
	cmp	r5, #0
	beq	.L1076
	ldr	r3, [r5, #8]
	tst	r3, #4096
	bne	.L1092
.L1076:
	ldr	r1, [r4, #676]
	mov	r0, r4
	bl	png_free
	add	r1, r7, #1
	mov	r0, r4
	bl	png_malloc
	mov	r6, r0
	str	r0, [r4, #676]
	mov	r1, r6
	mov	r0, r4
	mov	r2, r7
	bl	png_read_data
	mov	r0, r4
	mov	r1, r6
	mov	r2, r7
	bl	png_calculate_crc
	mov	r0, r4
	mov	r1, #0
	bl	png_crc_finish
	cmp	r0, #0
	bne	.L1088
	ldr	r1, [r4, #676]
	strb	r0, [r1, r7]
	ldr	r1, [r4, #676]
	ldrb	r0, [r1, #0]	@ zero_extendqisi2
	cmp	r0, #0
	mov	r3, r1
	beq	.L1079
.L1080:
	ldrb	r2, [r3, #1]!	@ zero_extendqisi2
	cmp	r2, #0
	bne	.L1080
.L1079:
	sub	r6, r7, #1
	add	ip, r1, r6
	add	r6, r3, #1
	cmp	r6, ip
	bcs	.L1093
	ldrb	lr, [r3, #1]	@ zero_extendqisi2
	cmp	lr, #0
	bne	.L1094
.L1082:
	add	r2, r6, #1
	rsb	r6, r1, r2
	mov	r0, r4
	mov	r2, r7
	mov	r1, #0
	add	r7, sp, #12
	mov	r3, r6
	str	r7, [sp, #0]
	bl	png_decompress_chunk
	ldr	r0, [sp, #12]
	cmp	r6, r0
	movls	r7, #0
	movhi	r7, #1
	rsb	ip, r6, r0
	cmp	ip, #3
	orrls	r7, r7, #1
	cmp	r7, #0
	bne	.L1095
	ldr	r2, [r4, #676]
	add	lr, r2, r6
	ldrb	r1, [r2, r6]	@ zero_extendqisi2
	ldrb	r3, [lr, #3]	@ zero_extendqisi2
	ldrb	r0, [lr, #1]	@ zero_extendqisi2
	orr	r3, r3, r1, asl #24
	ldrb	r1, [lr, #2]	@ zero_extendqisi2
	orr	r3, r3, r0, asl #16
	orr	r3, r3, r1, asl #8
	cmp	ip, r3
	movhi	ip, r3
	bls	.L1096
.L1085:
	mov	r1, r5
	mov	r3, #0
	mov	r0, r4
	str	lr, [sp, #0]
	str	ip, [sp, #4]
	bl	png_set_iCCP
.L1088:
	mov	r0, r4
	ldr	r1, [r4, #676]
	bl	png_free
	mov	ip, #0
	str	ip, [r4, #676]
.L1086:
	add	sp, sp, #20
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L1089:
	ldr	r1, .L1097
	bl	png_error
	b	.L1073
.L1096:
	bcs	.L1085
	mov	r1, r2
	mov	r0, r4
	bl	png_free
	str	r7, [r4, #676]
	mov	r0, r4
	ldr	r1, .L1097+4
	bl	png_warning
	b	.L1086
.L1091:
	ldr	r1, .L1097+8
	bl	png_warning
	b	.L1073
.L1093:
	mov	r0, r4
	bl	png_free
	mov	r3, #0
	str	r3, [r4, #676]
	mov	r0, r4
	ldr	r1, .L1097+12
	bl	png_warning
	b	.L1086
.L1092:
	mov	r0, r4
	ldr	r1, .L1097+16
	bl	png_warning
	mov	r0, r4
	mov	r1, r7
	bl	png_crc_finish
	b	.L1086
.L1090:
	ldr	r1, .L1097+20
	bl	png_warning
	mov	r0, r4
	mov	r1, r7
	bl	png_crc_finish
	b	.L1086
.L1094:
	ldr	r1, .L1097+24
	mov	r0, r4
	bl	png_warning
	ldr	r1, [r4, #676]
	b	.L1082
.L1095:
	mov	r0, r4
	ldr	r1, [r4, #676]
	bl	png_free
	mov	r1, #0
	str	r1, [r4, #676]
	mov	r0, r4
	ldr	r1, .L1097+28
	bl	png_warning
	b	.L1086
.L1098:
	.align	2
.L1097:
	.word	.LC58
	.word	.LC65
	.word	.LC60
	.word	.LC62
	.word	.LC61
	.word	.LC59
	.word	.LC63
	.word	.LC64
	.size	png_handle_iCCP, .-png_handle_iCCP
	.section	.text.png_handle_sBIT,"ax",%progbits
	.align	2
	.global	png_handle_sBIT
	.hidden	png_handle_sBIT
	.type	png_handle_sBIT, %function
png_handle_sBIT:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	ldr	ip, [r0, #132]
	sub	sp, sp, #8
	mov	r3, #0
	tst	ip, #1
	mov	r4, r0
	strb	r3, [sp, #4]
	mov	r6, r1
	mov	r5, r2
	strb	r3, [sp, #7]
	strb	r3, [sp, #6]
	strb	r3, [sp, #5]
	beq	.L1111
	tst	ip, #4
	bne	.L1112
	tst	ip, #2
	bne	.L1113
.L1101:
	cmp	r6, #0
	beq	.L1104
	ldr	r0, [r6, #8]
	tst	r0, #2
	bne	.L1114
.L1104:
	ldrb	r7, [r4, #322]	@ zero_extendqisi2
	cmp	r7, #3
	ldrneb	r7, [r4, #326]	@ zero_extendqisi2
	cmp	r5, #4
	cmpls	r5, r7
	moveq	r7, #0
	movne	r7, #1
	bne	.L1115
	add	r8, sp, #4
	mov	r0, r4
	mov	r1, r8
	mov	r2, r5
	bl	png_read_data
	mov	r0, r4
	mov	r1, r8
	mov	r2, r5
	bl	png_calculate_crc
	mov	r0, r4
	mov	r1, r7
	bl	png_crc_finish
	cmp	r0, #0
	bne	.L1110
	ldrb	r1, [r4, #322]	@ zero_extendqisi2
	tst	r1, #2
	bne	.L1116
	ldrb	r0, [sp, #4]	@ zero_extendqisi2
	ldrb	r1, [sp, #5]	@ zero_extendqisi2
	strb	r0, [r4, #410]
	strb	r1, [r4, #412]
	strb	r0, [r4, #411]
	strb	r0, [r4, #408]
	strb	r0, [r4, #409]
.L1109:
	mov	r0, r4
	mov	r1, r6
	add	r2, r4, #408
	bl	png_set_sBIT
	b	.L1110
.L1115:
	mov	r0, r4
	ldr	r1, .L1117
	bl	png_warning
	mov	r0, r4
	mov	r1, r5
	bl	png_crc_finish
.L1110:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L1111:
	ldr	r1, .L1117+4
	bl	png_error
	b	.L1101
.L1113:
	ldr	r1, .L1117+8
	bl	png_warning
	b	.L1101
.L1116:
	ldrb	r3, [sp, #4]	@ zero_extendqisi2
	ldrb	lr, [sp, #5]	@ zero_extendqisi2
	ldrb	r2, [sp, #6]	@ zero_extendqisi2
	ldrb	ip, [sp, #7]	@ zero_extendqisi2
	strb	r3, [r4, #408]
	strb	lr, [r4, #409]
	strb	r2, [r4, #410]
	strb	ip, [r4, #412]
	b	.L1109
.L1114:
	mov	r0, r4
	ldr	r1, .L1117+12
	bl	png_warning
	mov	r0, r4
	mov	r1, r5
	bl	png_crc_finish
	b	.L1110
.L1112:
	ldr	r1, .L1117+16
	bl	png_warning
	mov	r0, r4
	mov	r1, r5
	bl	png_crc_finish
	b	.L1110
.L1118:
	.align	2
.L1117:
	.word	.LC70
	.word	.LC66
	.word	.LC68
	.word	.LC69
	.word	.LC67
	.size	png_handle_sBIT, .-png_handle_sBIT
	.section	.text.png_handle_IEND,"ax",%progbits
	.align	2
	.global	png_handle_IEND
	.hidden	png_handle_IEND
	.type	png_handle_IEND, %function
png_handle_IEND:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	ldr	r1, [r0, #132]
	and	r3, r1, #5
	cmp	r3, #5
	mov	r4, r0
	mov	r5, r2
	ldrne	r1, .L1123
	blne	png_error
.L1120:
	ldr	r2, [r4, #132]
	cmp	r5, #0
	orr	r0, r2, #24
	str	r0, [r4, #132]
	movne	r0, r4
	ldrne	r1, .L1123+4
	blne	png_warning
.L1121:
	mov	r0, r4
	mov	r1, r5
	ldmfd	sp!, {r4, r5, r6, lr}
	b	png_crc_finish
.L1124:
	.align	2
.L1123:
	.word	.LC71
	.word	.LC72
	.size	png_handle_IEND, .-png_handle_IEND
	.section	.text.png_handle_PLTE,"ax",%progbits
	.align	2
	.global	png_handle_PLTE
	.hidden	png_handle_PLTE
	.type	png_handle_PLTE, %function
png_handle_PLTE:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 776
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	r3, [r0, #132]
	tst	r3, #1
	sub	sp, sp, #780
	mov	r4, r0
	mov	r9, r1
	mov	r6, r2
	beq	.L1152
	tst	r3, #4
	bne	.L1153
	tst	r3, #2
	bne	.L1154
.L1127:
	ldrb	r2, [r4, #322]	@ zero_extendqisi2
	orr	r3, r3, #2
	tst	r2, #2
	str	r3, [r4, #132]
	beq	.L1155
.L1130:
	cmp	r6, #768
	bls	.L1156
.L1131:
	cmp	r2, #3
	mov	r0, r4
	ldr	r1, .L1158
	beq	.L1133
	bl	png_warning
	mov	r0, r4
	mov	r1, r6
	bl	png_crc_finish
.L1137:
	add	sp, sp, #780
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L1156:
	ldr	ip, .L1158+4
	umull	r1, ip, r6, ip
	mov	r1, ip, lsr #1
	add	r0, r1, r1, asl #1
	cmp	r6, r0
	bne	.L1131
.L1132:
	ldr	r5, .L1158+8
	smull	r3, r5, r6, r5
	sub	r5, r5, r6, asr #31
	cmp	r5, #0
	ble	.L1134
	add	r6, sp, #772
	mov	r2, #3
	mov	r0, r4
	mov	r1, r6
	sub	fp, r5, #1
	and	fp, fp, r2
	bl	png_read_data
	mov	r2, #3
	mov	r0, r4
	mov	r1, r6
	bl	png_calculate_crc
	ldrb	sl, [sp, #772]	@ zero_extendqisi2
	add	r8, sp, #776
	strb	sl, [r8, #-772]!
	ldrb	r7, [sp, #773]	@ zero_extendqisi2
	strb	r7, [r8, #1]
	ldrb	r2, [sp, #774]	@ zero_extendqisi2
	cmp	r5, #1
	strb	r2, [r8, #2]
	mov	sl, #1
	mov	r7, #3
	ble	.L1134
	cmp	fp, #0
	beq	.L1135
	cmp	fp, #1
	beq	.L1150
	cmp	fp, #2
	beq	.L1151
	mov	r2, r7
	mov	r0, r4
	mov	r1, r6
	bl	png_read_data
	mov	r0, r4
	mov	r1, r6
	mov	r2, r7
	bl	png_calculate_crc
	ldrb	r1, [sp, #772]	@ zero_extendqisi2
	mov	sl, r8
	strb	r1, [sl, #3]!
	ldrb	r0, [sp, #773]	@ zero_extendqisi2
	strb	r0, [sl, #1]
	ldrb	r7, [sp, #774]	@ zero_extendqisi2
	strb	r7, [sl, #2]
	mov	sl, #2
	mov	r7, #6
.L1151:
	mov	r2, #3
	mov	r0, r4
	mov	r1, r6
	bl	png_read_data
	mov	r0, r4
	mov	r2, #3
	mov	r1, r6
	bl	png_calculate_crc
	ldrb	r0, [sp, #772]	@ zero_extendqisi2
	mov	r3, r8
	strb	r0, [r3, r7]!
	ldrb	ip, [sp, #773]	@ zero_extendqisi2
	strb	ip, [r3, #1]
	ldrb	r2, [sp, #774]	@ zero_extendqisi2
	strb	r2, [r3, #2]
	add	sl, sl, #1
	add	r7, r7, #3
.L1150:
	mov	r2, #3
	mov	r0, r4
	mov	r1, r6
	bl	png_read_data
	mov	r1, r6
	mov	r2, #3
	mov	r0, r4
	bl	png_calculate_crc
	ldrb	ip, [sp, #772]	@ zero_extendqisi2
	mov	r3, r8
	strb	ip, [r3, r7]!
	ldrb	r2, [sp, #773]	@ zero_extendqisi2
	strb	r2, [r3, #1]
	add	sl, sl, #1
	ldrb	r1, [sp, #774]	@ zero_extendqisi2
	cmp	r5, sl
	strb	r1, [r3, #2]
	add	r7, r7, #3
	ble	.L1134
.L1135:
	mov	r2, #3
	mov	r0, r4
	mov	r1, r6
	bl	png_read_data
	mov	r0, r4
	mov	r1, r6
	mov	r2, #3
	bl	png_calculate_crc
	ldrb	r1, [sp, #772]	@ zero_extendqisi2
	mov	r3, r8
	strb	r1, [r3, r7]!
	ldrb	r0, [sp, #773]	@ zero_extendqisi2
	strb	r0, [r3, #1]
	ldrb	ip, [sp, #774]	@ zero_extendqisi2
	mov	r2, #3
	strb	ip, [r3, #2]
	mov	r0, r4
	mov	r1, r6
	bl	png_read_data
	mov	r0, r4
	mov	r1, r6
	mov	r2, #3
	bl	png_calculate_crc
	ldrb	r2, [sp, #772]	@ zero_extendqisi2
	mov	r3, r8
	add	r1, r7, #3
	strb	r2, [r3, r1]!
	ldrb	r0, [sp, #773]	@ zero_extendqisi2
	strb	r0, [r3, #1]
	ldrb	ip, [sp, #774]	@ zero_extendqisi2
	mov	r2, #3
	strb	ip, [r3, #2]
	mov	r0, r4
	mov	r1, r6
	bl	png_read_data
	mov	r0, r4
	mov	r1, r6
	mov	r2, #3
	bl	png_calculate_crc
	ldrb	r2, [sp, #772]	@ zero_extendqisi2
	mov	r3, r8
	add	r1, r7, #6
	strb	r2, [r3, r1]!
	ldrb	r0, [sp, #773]	@ zero_extendqisi2
	strb	r0, [r3, #1]
	ldrb	ip, [sp, #774]	@ zero_extendqisi2
	mov	r2, #3
	strb	ip, [r3, #2]
	mov	r0, r4
	mov	r1, r6
	bl	png_read_data
	mov	r0, r4
	mov	r1, r6
	mov	r2, #3
	bl	png_calculate_crc
	ldrb	r2, [sp, #772]	@ zero_extendqisi2
	mov	r3, r8
	add	ip, r7, #9
	strb	r2, [r3, ip]!
	ldrb	r1, [sp, #773]	@ zero_extendqisi2
	strb	r1, [r3, #1]
	add	sl, sl, #4
	ldrb	r0, [sp, #774]	@ zero_extendqisi2
	cmp	r5, sl
	strb	r0, [r3, #2]
	add	r7, r7, #12
	bgt	.L1135
.L1134:
	mov	r1, #0
	mov	r0, r4
	bl	png_crc_finish
	add	r2, sp, #4
	mov	r0, r4
	mov	r1, r9
	mov	r3, r5
	bl	png_set_PLTE
	ldrb	r2, [r4, #322]	@ zero_extendqisi2
	cmp	r2, #3
	bne	.L1137
	cmp	r9, #0
	beq	.L1137
	ldr	lr, [r9, #8]
	tst	lr, #16
	beq	.L1137
	mov	r6, #308
	add	r6, r6, #2
	mov	r5, r5, asl #16
	ldrh	r3, [r4, r6]
	mov	r5, r5, lsr #16
	cmp	r3, r5
	bhi	.L1157
.L1136:
	ldrh	ip, [r9, #22]
	cmp	ip, r5
	bls	.L1137
	mov	r0, r4
	ldr	r1, .L1158+12
	bl	png_warning
	strh	r5, [r9, #22]	@ movhi
	b	.L1137
.L1152:
	ldr	r1, .L1158+16
	bl	png_error
	ldr	r3, [r4, #132]
	ldrb	r2, [r4, #322]	@ zero_extendqisi2
	orr	r3, r3, #2
	tst	r2, #2
	str	r3, [r4, #132]
	bne	.L1130
.L1155:
	mov	r0, r4
	ldr	r1, .L1158+20
	bl	png_warning
	mov	r0, r4
	mov	r1, r6
	bl	png_crc_finish
	b	.L1137
.L1133:
	bl	png_error
	b	.L1132
.L1154:
	ldr	r1, .L1158+24
	bl	png_error
	ldr	r3, [r4, #132]
	b	.L1127
.L1153:
	ldr	r1, .L1158+28
	bl	png_warning
	mov	r0, r4
	mov	r1, r6
	bl	png_crc_finish
	b	.L1137
.L1157:
	mov	r0, r4
	ldr	r1, .L1158+32
	bl	png_warning
	strh	r5, [r4, r6]	@ movhi
	b	.L1136
.L1159:
	.align	2
.L1158:
	.word	.LC77
	.word	-1431655765
	.word	1431655766
	.word	.LC79
	.word	.LC73
	.word	.LC76
	.word	.LC75
	.word	.LC74
	.word	.LC78
	.size	png_handle_PLTE, .-png_handle_PLTE
	.section	.text.png_handle_IHDR,"ax",%progbits
	.align	2
	.global	png_handle_IHDR
	.hidden	png_handle_IHDR
	.type	png_handle_IHDR, %function
png_handle_IHDR:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 24
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	ldr	r3, [r0, #132]
	tst	r3, #1
	sub	sp, sp, #52
	mov	r4, r0
	mov	sl, r1
	mov	r5, r2
	bne	.L1175
.L1161:
	cmp	r5, #13
	movne	r0, r4
	ldrne	r1, .L1178
	blne	png_error
.L1162:
	ldr	lr, [r4, #132]
	add	r8, sp, #32
	orr	ip, lr, #1
	str	ip, [r4, #132]
	mov	r1, r8
	mov	r2, #13
	mov	r0, r4
	bl	png_read_data
	mov	r1, r8
	mov	r2, #13
	mov	r0, r4
	bl	png_calculate_crc
	mov	r1, #0
	mov	r0, r4
	bl	png_crc_finish
	ldrb	r7, [sp, #33]	@ zero_extendqisi2
	ldrb	r6, [sp, #32]	@ zero_extendqisi2
	mov	r5, r7, asl #16
	ldrb	r2, [sp, #35]	@ zero_extendqisi2
	add	r1, r5, r6, asl #24
	ldrb	r0, [sp, #34]	@ zero_extendqisi2
	add	r8, r1, r2
	adds	r8, r8, r0, asl #8
	bmi	.L1176
.L1163:
	ldrb	r7, [sp, #37]	@ zero_extendqisi2
	ldrb	r2, [sp, #36]	@ zero_extendqisi2
	mov	r6, r7, asl #16
	ldrb	r1, [sp, #39]	@ zero_extendqisi2
	add	r5, r6, r2, asl #24
	ldrb	r0, [sp, #38]	@ zero_extendqisi2
	add	r3, r5, r1
	adds	r3, r3, r0, asl #8
	bmi	.L1177
.L1164:
	ldrb	lr, [sp, #40]	@ zero_extendqisi2
	ldrb	ip, [sp, #41]	@ zero_extendqisi2
	ldrb	r5, [sp, #42]	@ zero_extendqisi2
	ldrb	r7, [sp, #43]	@ zero_extendqisi2
	ldrb	r6, [sp, #44]	@ zero_extendqisi2
	str	r8, [r4, #228]
	str	r3, [r4, #232]
	strb	lr, [r4, #323]
	strb	r6, [r4, #319]
	strb	ip, [r4, #322]
	strb	r7, [r4, #596]
	strb	r5, [r4, #636]
	cmp	ip, #6
	ldrls	pc, [pc, ip, asl #2]
	b	.L1165
.L1170:
	.word	.L1166
	.word	.L1165
	.word	.L1167
	.word	.L1166
	.word	.L1168
	.word	.L1165
	.word	.L1169
.L1169:
	mov	r2, #4
	strb	r2, [r4, #326]
.L1171:
	mul	r2, lr, r2
	ldr	r1, [r4, #228]
	and	r0, r2, #255
	cmp	r0, #7
	strb	r0, [r4, #325]
	mulls	r0, r1, r0
	movhi	r0, r0, lsr #3
	mulhi	r0, r1, r0
	addls	r0, r0, #7
	movls	r0, r0, lsr #3
	str	r0, [r4, #244]
	mov	r1, sl
	mov	r0, r4
	mov	r2, r8
	str	lr, [sp, #0]
	str	ip, [sp, #4]
	str	r6, [sp, #8]
	str	r5, [sp, #12]
	str	r7, [sp, #16]
	bl	png_set_IHDR
	add	sp, sp, #52
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	bx	lr
.L1168:
	mov	r2, #2
	strb	r2, [r4, #326]
	b	.L1171
.L1167:
	mov	r2, #3
	strb	r2, [r4, #326]
	b	.L1171
.L1166:
	mov	r2, #1
	strb	r2, [r4, #326]
	b	.L1171
.L1165:
	ldrb	r2, [r4, #326]	@ zero_extendqisi2
	b	.L1171
.L1175:
	ldr	r1, .L1178+4
	bl	png_error
	b	.L1161
.L1177:
	mov	r0, r4
	ldr	r1, .L1178+8
	str	r3, [sp, #28]
	bl	png_error
	ldr	r3, [sp, #28]
	b	.L1164
.L1176:
	mov	r0, r4
	ldr	r1, .L1178+8
	bl	png_error
	b	.L1163
.L1179:
	.align	2
.L1178:
	.word	.LC81
	.word	.LC80
	.word	.LC0
	.size	png_handle_IHDR, .-png_handle_IHDR
	.global	__aeabi_i2f
	.global	__aeabi_fdiv
	.global	__aeabi_f2d
	.section	.text.png_handle_gAMA,"ax",%progbits
	.align	2
	.global	png_handle_gAMA
	.hidden	png_handle_gAMA
	.type	png_handle_gAMA, %function
png_handle_gAMA:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	ldr	r3, [r0, #132]
	tst	r3, #1
	sub	sp, sp, #12
	mov	r4, r0
	mov	r5, r1
	mov	r6, r2
	beq	.L1190
	tst	r3, #4
	bne	.L1191
	tst	r3, #2
	bne	.L1192
.L1182:
	cmp	r5, #0
	beq	.L1185
	ldr	ip, [r5, #8]
	bic	r2, ip, #2032
	bic	r1, r2, #14
	mov	r0, r1, asl #20
	mov	r3, r0, lsr #20
	cmp	r3, #1
	beq	.L1193
.L1185:
	cmp	r6, #4
	beq	.L1186
	mov	r0, r4
	ldr	r1, .L1196
	bl	png_warning
	mov	r0, r4
	mov	r1, r6
	bl	png_crc_finish
.L1189:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L1186:
	add	r7, sp, #4
	mov	r0, r4
	mov	r1, r7
	mov	r2, r6
	bl	png_read_data
	mov	r0, r4
	mov	r1, r7
	mov	r2, r6
	bl	png_calculate_crc
	mov	r0, r4
	mov	r1, #0
	bl	png_crc_finish
	cmp	r0, #0
	bne	.L1189
	ldrb	ip, [sp, #5]	@ zero_extendqisi2
	ldrb	r3, [sp, #4]	@ zero_extendqisi2
	mov	r2, ip, asl #16
	ldrb	r1, [sp, #7]	@ zero_extendqisi2
	add	r0, r2, r3, asl #24
	ldrb	lr, [sp, #6]	@ zero_extendqisi2
	add	r6, r0, r1
	adds	r6, r6, lr, asl #8
	beq	.L1194
	cmp	r5, #0
	beq	.L1188
	ldr	lr, [r5, #8]
	tst	lr, #2048
	beq	.L1188
	sub	r1, r6, #44800
	sub	r0, r1, #200
	cmp	r0, #1000
	bhi	.L1195
.L1188:
	mov	r0, r6
	bl	__aeabi_i2f
	mov	ip, #1191182336
	add	r2, ip, #12779520
	add	r1, r2, #20480
	bl	__aeabi_fdiv
	str	r0, [r4, #376]	@ float
	bl	__aeabi_f2d
	mov	r2, r0
	mov	r3, r1
	mov	r0, r4
	mov	r1, r5
	bl	png_set_gAMA
	mov	r0, r4
	mov	r1, r5
	mov	r2, r6
	bl	png_set_gAMA_fixed
	b	.L1189
.L1190:
	ldr	r1, .L1196+4
	bl	png_error
	b	.L1182
.L1192:
	ldr	r1, .L1196+8
	bl	png_warning
	b	.L1182
.L1191:
	ldr	r1, .L1196+12
	bl	png_warning
	mov	r0, r4
	mov	r1, r6
	bl	png_crc_finish
	b	.L1189
.L1193:
	mov	r0, r4
	ldr	r1, .L1196+16
	bl	png_warning
	mov	r0, r4
	mov	r1, r6
	bl	png_crc_finish
	b	.L1189
.L1194:
	mov	r0, r4
	ldr	r1, .L1196+20
	bl	png_warning
	b	.L1189
.L1195:
	mov	r0, r4
	ldr	r1, .L1196+24
	bl	png_warning
	ldr	r3, .L1196+28
	mov	r2, r6
	ldr	r0, [r3, #0]
	ldr	r1, .L1196+32
	bl	fprintf
	b	.L1189
.L1197:
	.align	2
.L1196:
	.word	.LC86
	.word	.LC82
	.word	.LC84
	.word	.LC83
	.word	.LC85
	.word	.LC87
	.word	.LC88
	.word	__aeabi_stderr
	.word	.LC89
	.size	png_handle_gAMA, .-png_handle_gAMA
	.section	.text.png_handle_cHRM,"ax",%progbits
	.align	2
	.global	png_handle_cHRM
	.hidden	png_handle_cHRM
	.type	png_handle_cHRM, %function
png_handle_cHRM:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 104
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	r3, [r0, #132]
	tst	r3, #1
	sub	sp, sp, #164
	mov	r4, r0
	mov	r5, r1
	mov	r6, r2
	beq	.L1208
	tst	r3, #4
	bne	.L1209
	tst	r3, #2
	bne	.L1210
.L1200:
	cmp	r5, #0
	beq	.L1203
	ldr	ip, [r5, #8]
	bic	r2, ip, #2032
	bic	r1, r2, #11
	mov	r0, r1, asl #20
	mov	r3, r0, lsr #20
	cmp	r3, #4
	beq	.L1211
.L1203:
	cmp	r6, #32
	beq	.L1204
	mov	r0, r4
	ldr	r1, .L1212
	bl	png_warning
	mov	r0, r4
	mov	r1, r6
	bl	png_crc_finish
.L1207:
	add	sp, sp, #164
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L1204:
	add	r7, sp, #128
	mov	r0, r4
	mov	r1, r7
	mov	r2, r6
	bl	png_read_data
	mov	r0, r4
	mov	r1, r7
	mov	r2, r6
	bl	png_calculate_crc
	mov	r0, r4
	mov	r1, #0
	bl	png_crc_finish
	cmp	r0, #0
	bne	.L1207
	ldrb	r2, [sp, #136]	@ zero_extendqisi2
	str	r2, [sp, #60]
	ldrb	r2, [sp, #140]	@ zero_extendqisi2
	str	r2, [sp, #76]
	ldrb	r2, [sp, #144]	@ zero_extendqisi2
	ldrb	lr, [sp, #129]	@ zero_extendqisi2
	str	r2, [sp, #80]
	ldrb	r2, [sp, #148]	@ zero_extendqisi2
	ldrb	r0, [sp, #128]	@ zero_extendqisi2
	mov	r1, lr, asl #16
	ldrb	ip, [sp, #131]	@ zero_extendqisi2
	str	r2, [sp, #84]
	ldrb	r2, [sp, #152]	@ zero_extendqisi2
	add	r3, r1, r0, asl #24
	add	r3, r3, ip
	str	r2, [sp, #112]
	ldrb	ip, [sp, #135]	@ zero_extendqisi2
	ldrb	r2, [sp, #156]	@ zero_extendqisi2
	str	ip, [sp, #88]
	str	r2, [sp, #120]
	ldrb	ip, [sp, #143]	@ zero_extendqisi2
	ldrb	r2, [sp, #139]	@ zero_extendqisi2
	str	ip, [sp, #96]
	str	r2, [sp, #92]
	ldrb	ip, [sp, #151]	@ zero_extendqisi2
	ldrb	r2, [sp, #147]	@ zero_extendqisi2
	ldrb	r0, [sp, #130]	@ zero_extendqisi2
	ldrb	r1, [sp, #132]	@ zero_extendqisi2
	ldrb	r6, [sp, #133]	@ zero_extendqisi2
	ldrb	r7, [sp, #137]	@ zero_extendqisi2
	ldrb	r8, [sp, #141]	@ zero_extendqisi2
	ldrb	sl, [sp, #145]	@ zero_extendqisi2
	ldrb	r9, [sp, #149]	@ zero_extendqisi2
	ldrb	fp, [sp, #153]	@ zero_extendqisi2
	ldrb	lr, [sp, #157]	@ zero_extendqisi2
	str	r2, [sp, #100]
	str	ip, [sp, #104]
	ldrb	r2, [sp, #155]	@ zero_extendqisi2
	str	r2, [sp, #116]
	add	r2, r3, r0, asl #8
	ldr	r0, [sp, #76]
	mov	r8, r8, asl #16
	add	r8, r8, r0, asl #24
	ldrb	r0, [sp, #150]	@ zero_extendqisi2
	ldrb	ip, [sp, #159]	@ zero_extendqisi2
	str	r0, [sp, #76]
	ldrb	r0, [sp, #154]	@ zero_extendqisi2
	str	ip, [sp, #124]
	ldr	ip, [sp, #80]
	str	r0, [sp, #80]
	ldrb	r0, [sp, #158]	@ zero_extendqisi2
	mov	r6, r6, asl #16
	add	r6, r6, r1, asl #24
	ldr	r1, [sp, #84]
	str	r0, [sp, #84]
	ldr	r0, [sp, #88]
	ldr	r3, [sp, #60]
	add	r6, r6, r0
	mov	r7, r7, asl #16
	ldr	r0, [sp, #92]
	add	r7, r7, r3, asl #24
	add	r7, r7, r0
	ldr	r0, [sp, #96]
	mov	sl, sl, asl #16
	add	r8, r8, r0
	ldr	r0, [sp, #100]
	add	sl, sl, ip, asl #24
	add	sl, sl, r0
	mov	r9, r9, asl #16
	ldr	r0, [sp, #104]
	str	r2, [sp, #108]
	ldr	r3, [sp, #120]
	ldr	r2, [sp, #112]
	add	r9, r9, r1, asl #24
	add	r9, r9, r0
	mov	fp, fp, asl #16
	ldr	r0, [sp, #116]
	mov	lr, lr, asl #16
	add	fp, fp, r2, asl #24
	add	lr, lr, r3, asl #24
	ldrb	r3, [sp, #146]	@ zero_extendqisi2
	ldrb	r1, [sp, #138]	@ zero_extendqisi2
	ldrb	r2, [sp, #142]	@ zero_extendqisi2
	add	fp, fp, r0
	ldr	r0, [sp, #124]
	add	sl, sl, r3, asl #8
	ldr	r3, [sp, #84]
	ldrb	ip, [sp, #134]	@ zero_extendqisi2
	add	lr, lr, r0
	add	r7, r7, r1, asl #8
	add	r8, r8, r2, asl #8
	ldr	r1, [sp, #80]
	ldr	r2, [sp, #76]
	add	lr, lr, r3, asl #8
	ldr	r0, [sp, #108]
	str	lr, [sp, #88]
	add	r6, r6, ip, asl #8
	add	r9, r9, r2, asl #8
	add	fp, fp, r1, asl #8
	bl	__aeabi_i2f
	mov	ip, #1191182336
	add	r2, ip, #12779520
	add	r1, r2, #20480
	bl	__aeabi_fdiv
	str	r0, [sp, #104]	@ float
	mov	r0, r6
	bl	__aeabi_i2f
	mov	r1, #1191182336
	add	r3, r1, #12779520
	add	r1, r3, #20480
	bl	__aeabi_fdiv
	str	r0, [sp, #84]	@ float
	mov	r0, r7
	bl	__aeabi_i2f
	mov	ip, #1191182336
	add	r2, ip, #12779520
	add	r1, r2, #20480
	bl	__aeabi_fdiv
	str	r0, [sp, #100]	@ float
	mov	r0, r8
	bl	__aeabi_i2f
	mov	r1, #1191182336
	add	r3, r1, #12779520
	add	r1, r3, #20480
	bl	__aeabi_fdiv
	str	r0, [sp, #80]	@ float
	mov	r0, sl
	bl	__aeabi_i2f
	mov	ip, #1191182336
	add	r2, ip, #12779520
	add	r1, r2, #20480
	bl	__aeabi_fdiv
	str	r0, [sp, #96]	@ float
	mov	r0, r9
	bl	__aeabi_i2f
	mov	r1, #1191182336
	add	r3, r1, #12779520
	add	r1, r3, #20480
	bl	__aeabi_fdiv
	str	r0, [sp, #76]	@ float
	mov	r0, fp
	bl	__aeabi_i2f
	mov	ip, #1191182336
	add	r2, ip, #12779520
	add	r1, r2, #20480
	bl	__aeabi_fdiv
	str	r0, [sp, #92]	@ float
	ldr	r0, [sp, #88]
	bl	__aeabi_i2f
	mov	r1, #1191182336
	add	r3, r1, #12779520
	add	r1, r3, #20480
	bl	__aeabi_fdiv
	cmp	r5, #0
	mov	ip, r0
	beq	.L1205
	ldr	r3, [r5, #8]
	tst	r3, #2048
	beq	.L1205
	ldr	r0, [sp, #108]
	sub	r1, r0, #30208
	sub	r2, r1, #62
	cmp	r2, #2000
	bhi	.L1206
	sub	r6, r6, #31744
	sub	r6, r6, #156
	cmp	r6, #2000
	bhi	.L1206
	sub	r7, r7, #62976
	sub	r7, r7, #24
	cmp	r7, #2000
	bhi	.L1206
	sub	r8, r8, #32000
	cmp	r8, #2000
	bhi	.L1206
	sub	sl, sl, #28928
	sub	sl, sl, #72
	cmp	sl, #2000
	bhi	.L1206
	sub	r9, r9, #58880
	sub	r9, r9, #120
	cmp	r9, #2000
	bhi	.L1206
	sub	fp, fp, #13952
	sub	fp, fp, #48
	cmp	fp, #2000
	bhi	.L1206
	ldr	r2, [sp, #88]
	sub	r1, r2, #4992
	sub	r3, r1, #8
	cmp	r3, #2000
	bls	.L1207
.L1206:
	mov	r0, r4
	ldr	r1, .L1212+4
	str	ip, [sp, #64]
	bl	png_warning
	ldr	r0, [sp, #104]	@ float
	bl	__aeabi_f2d
	mov	r5, r0
	ldr	r0, [sp, #84]	@ float
	mov	r6, r1
	bl	__aeabi_f2d
	stmia	sp, {r0-r1}
	ldr	r0, [sp, #100]	@ float
	bl	__aeabi_f2d
	str	r0, [sp, #8]
	str	r1, [sp, #12]
	ldr	r0, [sp, #80]	@ float
	bl	__aeabi_f2d
	ldr	r4, .L1212+8
	ldr	lr, [r4, #0]
	mov	r2, r5
	mov	r3, r6
	str	r0, [sp, #16]
	str	r1, [sp, #20]
	mov	r0, lr
	ldr	r1, .L1212+12
	bl	fprintf
	ldr	r0, [sp, #96]	@ float
	bl	__aeabi_f2d
	mov	r5, r0
	ldr	r0, [sp, #76]	@ float
	mov	r6, r1
	bl	__aeabi_f2d
	stmia	sp, {r0-r1}
	ldr	r0, [sp, #92]	@ float
	bl	__aeabi_f2d
	ldr	ip, [sp, #64]
	str	r0, [sp, #8]
	str	r1, [sp, #12]
	mov	r0, ip
	bl	__aeabi_f2d
	ldr	ip, [r4, #0]
	mov	r2, r5
	str	r0, [sp, #16]
	str	r1, [sp, #20]
	mov	r3, r6
	mov	r0, ip
	ldr	r1, .L1212+16
	bl	fprintf
	b	.L1207
.L1208:
	ldr	r1, .L1212+20
	bl	png_error
	b	.L1200
.L1210:
	ldr	r1, .L1212+24
	bl	png_warning
	b	.L1200
.L1209:
	ldr	r1, .L1212+28
	bl	png_warning
	mov	r0, r4
	mov	r1, r6
	bl	png_crc_finish
	b	.L1207
.L1205:
	ldr	r0, [sp, #104]	@ float
	str	ip, [sp, #64]
	bl	__aeabi_f2d
	mov	r2, r0
	ldr	r0, [sp, #84]	@ float
	str	r2, [sp, #72]
	str	r1, [sp, #68]
	bl	__aeabi_f2d
	stmia	sp, {r0-r1}
	ldr	r0, [sp, #100]	@ float
	bl	__aeabi_f2d
	str	r0, [sp, #8]
	str	r1, [sp, #12]
	ldr	r0, [sp, #80]	@ float
	bl	__aeabi_f2d
	str	r0, [sp, #16]
	str	r1, [sp, #20]
	ldr	r0, [sp, #96]	@ float
	bl	__aeabi_f2d
	str	r0, [sp, #24]
	str	r1, [sp, #28]
	ldr	r0, [sp, #76]	@ float
	bl	__aeabi_f2d
	str	r0, [sp, #32]
	str	r1, [sp, #36]
	ldr	r0, [sp, #92]	@ float
	bl	__aeabi_f2d
	ldr	r3, [sp, #64]
	str	r0, [sp, #40]
	str	r1, [sp, #44]
	mov	r0, r3
	bl	__aeabi_f2d
	ldr	r2, [sp, #72]
	str	r0, [sp, #48]
	str	r1, [sp, #52]
	ldr	r3, [sp, #68]
	mov	r0, r4
	mov	r1, r5
	bl	png_set_cHRM
	ldr	ip, [sp, #88]
	mov	r0, r4
	mov	r1, r5
	ldr	r2, [sp, #108]
	mov	r3, r6
	stmia	sp, {r7, r8, sl}	@ phole stm
	str	r9, [sp, #12]
	str	fp, [sp, #16]
	str	ip, [sp, #20]
	bl	png_set_cHRM_fixed
	b	.L1207
.L1211:
	mov	r0, r4
	ldr	r1, .L1212+32
	bl	png_warning
	mov	r0, r4
	mov	r1, r6
	bl	png_crc_finish
	b	.L1207
.L1213:
	.align	2
.L1212:
	.word	.LC94
	.word	.LC95
	.word	__aeabi_stderr
	.word	.LC96
	.word	.LC97
	.word	.LC90
	.word	.LC92
	.word	.LC91
	.word	.LC93
	.size	png_handle_cHRM, .-png_handle_cHRM
	.section	.text.png_handle_oFFs,"ax",%progbits
	.align	2
	.global	png_handle_oFFs
	.hidden	png_handle_oFFs
	.type	png_handle_oFFs, %function
png_handle_oFFs:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	ldr	r3, [r0, #132]
	tst	r3, #1
	sub	sp, sp, #28
	mov	r4, r0
	mov	r5, r1
	mov	r6, r2
	beq	.L1221
	tst	r3, #4
	bne	.L1222
	cmp	r1, #0
	beq	.L1216
	ldr	r3, [r1, #8]
	tst	r3, #256
	bne	.L1223
.L1216:
	cmp	r6, #9
	beq	.L1219
.L1224:
	mov	r0, r4
	ldr	r1, .L1225
	bl	png_warning
	mov	r0, r4
	mov	r1, r6
	bl	png_crc_finish
.L1220:
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L1219:
	add	r7, sp, #12
	mov	r0, r4
	mov	r1, r7
	mov	r2, r6
	bl	png_read_data
	mov	r0, r4
	mov	r1, r7
	mov	r2, r6
	bl	png_calculate_crc
	mov	r0, r4
	mov	r1, #0
	bl	png_crc_finish
	cmp	r0, #0
	bne	.L1220
	ldrb	lr, [sp, #13]	@ zero_extendqisi2
	ldrb	ip, [sp, #17]	@ zero_extendqisi2
	ldrb	r0, [sp, #12]	@ zero_extendqisi2
	ldrb	r1, [sp, #16]	@ zero_extendqisi2
	mov	r2, lr, asl #16
	mov	r3, ip, asl #16
	ldrb	lr, [sp, #15]	@ zero_extendqisi2
	ldrb	ip, [sp, #19]	@ zero_extendqisi2
	add	r3, r3, r1, asl #24
	add	r2, r2, r0, asl #24
	ldrb	r1, [sp, #18]	@ zero_extendqisi2
	ldrb	r0, [sp, #14]	@ zero_extendqisi2
	add	r2, r2, lr
	add	lr, r3, ip
	ldrb	ip, [sp, #20]	@ zero_extendqisi2
	add	r2, r2, r0, asl #8
	add	r3, lr, r1, asl #8
	mov	r0, r4
	mov	r1, r5
	str	ip, [sp, #0]
	bl	png_set_oFFs
	b	.L1220
.L1221:
	ldr	r1, .L1225+4
	bl	png_error
	cmp	r6, #9
	bne	.L1224
	b	.L1219
.L1223:
	ldr	r1, .L1225+8
	bl	png_warning
	mov	r0, r4
	mov	r1, r6
	bl	png_crc_finish
	b	.L1220
.L1222:
	ldr	r1, .L1225+12
	bl	png_warning
	mov	r0, r4
	mov	r1, r6
	bl	png_crc_finish
	b	.L1220
.L1226:
	.align	2
.L1225:
	.word	.LC101
	.word	.LC98
	.word	.LC100
	.word	.LC99
	.size	png_handle_oFFs, .-png_handle_oFFs
	.section	.text.png_handle_pHYs,"ax",%progbits
	.align	2
	.global	png_handle_pHYs
	.hidden	png_handle_pHYs
	.type	png_handle_pHYs, %function
png_handle_pHYs:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	ldr	r3, [r0, #132]
	tst	r3, #1
	sub	sp, sp, #28
	mov	r4, r0
	mov	r5, r1
	mov	r6, r2
	beq	.L1234
	tst	r3, #4
	bne	.L1235
	cmp	r1, #0
	beq	.L1229
	ldr	r3, [r1, #8]
	tst	r3, #128
	bne	.L1236
.L1229:
	cmp	r6, #9
	beq	.L1232
.L1237:
	mov	r0, r4
	ldr	r1, .L1238
	bl	png_warning
	mov	r0, r4
	mov	r1, r6
	bl	png_crc_finish
.L1233:
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L1232:
	add	r7, sp, #12
	mov	r0, r4
	mov	r1, r7
	mov	r2, r6
	bl	png_read_data
	mov	r0, r4
	mov	r1, r7
	mov	r2, r6
	bl	png_calculate_crc
	mov	r0, r4
	mov	r1, #0
	bl	png_crc_finish
	cmp	r0, #0
	bne	.L1233
	ldrb	lr, [sp, #13]	@ zero_extendqisi2
	ldrb	ip, [sp, #17]	@ zero_extendqisi2
	ldrb	r0, [sp, #12]	@ zero_extendqisi2
	ldrb	r1, [sp, #16]	@ zero_extendqisi2
	mov	r2, lr, asl #16
	mov	r3, ip, asl #16
	ldrb	lr, [sp, #15]	@ zero_extendqisi2
	ldrb	ip, [sp, #19]	@ zero_extendqisi2
	add	r3, r3, r1, asl #24
	add	r2, r2, r0, asl #24
	ldrb	r1, [sp, #18]	@ zero_extendqisi2
	ldrb	r0, [sp, #14]	@ zero_extendqisi2
	add	r2, r2, lr
	add	lr, r3, ip
	ldrb	ip, [sp, #20]	@ zero_extendqisi2
	add	r2, r2, r0, asl #8
	add	r3, lr, r1, asl #8
	mov	r0, r4
	mov	r1, r5
	str	ip, [sp, #0]
	bl	png_set_pHYs
	b	.L1233
.L1234:
	ldr	r1, .L1238+4
	bl	png_error
	cmp	r6, #9
	bne	.L1237
	b	.L1232
.L1236:
	ldr	r1, .L1238+8
	bl	png_warning
	mov	r0, r4
	mov	r1, r6
	bl	png_crc_finish
	b	.L1233
.L1235:
	ldr	r1, .L1238+12
	bl	png_warning
	mov	r0, r4
	mov	r1, r6
	bl	png_crc_finish
	b	.L1233
.L1239:
	.align	2
.L1238:
	.word	.LC105
	.word	.LC102
	.word	.LC104
	.word	.LC103
	.size	png_handle_pHYs, .-png_handle_pHYs
	.section	.text.png_handle_bKGD,"ax",%progbits
	.align	2
	.global	png_handle_bKGD
	.hidden	png_handle_bKGD
	.type	png_handle_bKGD, %function
png_handle_bKGD:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	ldr	r3, [r0, #132]
	tst	r3, #1
	sub	sp, sp, #12
	mov	r4, r0
	mov	r6, r1
	mov	r5, r2
	beq	.L1255
	tst	r3, #4
	bne	.L1256
	ldrb	r2, [r0, #322]	@ zero_extendqisi2
	cmp	r2, #3
	beq	.L1257
.L1245:
	cmp	r6, #0
	beq	.L1242
	ldr	r3, [r6, #8]
	tst	r3, #32
	bne	.L1258
.L1242:
	cmp	r2, #3
	moveq	r3, #1
	beq	.L1247
	tst	r2, #2
	movne	r3, #6
	moveq	r3, #2
.L1247:
	cmp	r5, r3
	beq	.L1249
	mov	r0, r4
	ldr	r1, .L1261
	bl	png_warning
	mov	r0, r4
	mov	r1, r5
	bl	png_crc_finish
.L1254:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L1249:
	mov	r0, r4
	mov	r1, sp
	mov	r2, r5
	bl	png_read_data
	mov	r0, r4
	mov	r1, sp
	mov	r2, r5
	bl	png_calculate_crc
	mov	r0, r4
	mov	r1, #0
	bl	png_crc_finish
	cmp	r0, #0
	mov	r7, sp
	bne	.L1254
	ldrb	r3, [r4, #322]	@ zero_extendqisi2
	cmp	r3, #3
	beq	.L1259
	tst	r3, #2
	bne	.L1253
	ldrb	ip, [sp, #0]	@ zero_extendqisi2
	ldrb	r1, [sp, #1]	@ zero_extendqisi2
	add	r0, r1, ip, asl #8
	mov	r3, r0, asl #16
	mov	lr, #340
	mov	r2, #344
	mov	r3, r3, lsr #16
	add	ip, lr, #2
	add	r0, r2, #2
	mov	r1, #348
	mov	lr, #344
	strh	r3, [r4, ip]	@ movhi
	strh	r3, [r4, r1]	@ movhi
	strh	r3, [r4, r0]	@ movhi
	strh	r3, [r4, lr]	@ movhi
.L1251:
	mov	r0, r4
	mov	r1, r6
	add	r2, r4, #340
	bl	png_set_bKGD
	b	.L1254
.L1255:
	ldr	r1, .L1261+4
	bl	png_error
	ldrb	r2, [r4, #322]	@ zero_extendqisi2
	b	.L1242
.L1257:
	tst	r3, #2
	bne	.L1245
	ldr	r1, .L1261+8
	bl	png_warning
	mov	r0, r4
	mov	r1, r5
	bl	png_crc_finish
	b	.L1254
.L1258:
	mov	r0, r4
	ldr	r1, .L1261+12
	bl	png_warning
	mov	r0, r4
	mov	r1, r5
	bl	png_crc_finish
	b	.L1254
.L1256:
	ldr	r1, .L1261+16
	bl	png_warning
	mov	r0, r4
	mov	r1, r5
	bl	png_crc_finish
	b	.L1254
.L1253:
	ldrb	r7, [sp, #0]	@ zero_extendqisi2
	ldrb	ip, [sp, #1]	@ zero_extendqisi2
	ldrb	lr, [sp, #4]	@ zero_extendqisi2
	ldrb	r5, [sp, #2]	@ zero_extendqisi2
	ldrb	r0, [sp, #3]	@ zero_extendqisi2
	ldrb	r1, [sp, #5]	@ zero_extendqisi2
	mov	r2, #340
	add	r2, r2, #2
	mov	r3, #344
	add	ip, ip, r7, asl #8
	add	r1, r1, lr, asl #8
	add	r0, r0, r5, asl #8
	add	lr, r3, #2
	strh	ip, [r4, r2]	@ movhi
	mov	r2, #344
	strh	r0, [r4, r2]	@ movhi
	strh	r1, [r4, lr]	@ movhi
	b	.L1251
.L1259:
	ldrb	r3, [sp, #0]	@ zero_extendqisi2
	cmp	r6, #0
	strb	r3, [r4, #340]
	beq	.L1251
	ldrh	r2, [r6, #20]
	cmp	r2, #0
	beq	.L1251
	cmp	r2, r3
	bls	.L1260
	ldr	r1, [r4, #304]
	add	r3, r3, r3, asl #1
	ldrb	r0, [r3, r1]	@ zero_extendqisi2
	mov	ip, #340
	add	r2, ip, #2
	strh	r0, [r4, r2]	@ movhi
	add	ip, r3, r1
	ldrb	r1, [ip, #1]	@ zero_extendqisi2
	mov	lr, #344
	strh	r1, [r4, lr]	@ movhi
	mov	r0, #344
	ldrb	r2, [ip, #2]	@ zero_extendqisi2
	add	r3, r0, #2
	strh	r2, [r4, r3]	@ movhi
	b	.L1251
.L1260:
	mov	r0, r4
	ldr	r1, .L1261+20
	bl	png_warning
	b	.L1254
.L1262:
	.align	2
.L1261:
	.word	.LC110
	.word	.LC106
	.word	.LC108
	.word	.LC109
	.word	.LC107
	.word	.LC111
	.size	png_handle_bKGD, .-png_handle_bKGD
	.section	.text.png_handle_sRGB,"ax",%progbits
	.align	2
	.global	png_handle_sRGB
	.hidden	png_handle_sRGB
	.type	png_handle_sRGB, %function
png_handle_sRGB:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	ldr	r3, [r0, #132]
	tst	r3, #1
	sub	sp, sp, #12
	mov	r4, r0
	mov	r5, r1
	mov	r6, r2
	beq	.L1276
	tst	r3, #4
	bne	.L1277
	tst	r3, #2
	bne	.L1278
.L1265:
	cmp	r5, #0
	beq	.L1268
	ldr	r3, [r5, #8]
	tst	r3, #2048
	bne	.L1279
.L1268:
	cmp	r6, #1
	beq	.L1269
	mov	r0, r4
	ldr	r1, .L1282
	bl	png_warning
	mov	r0, r4
	mov	r1, r6
	bl	png_crc_finish
.L1275:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L1269:
	add	r7, sp, #4
	mov	r0, r4
	mov	r1, r7
	mov	r2, r6
	bl	png_read_data
	mov	r0, r4
	mov	r1, r7
	mov	r2, r6
	bl	png_calculate_crc
	mov	r0, r4
	mov	r1, #0
	bl	png_crc_finish
	cmp	r0, #0
	bne	.L1275
	ldrb	r6, [sp, #4]	@ zero_extendqisi2
	cmp	r6, #3
	bgt	.L1280
	cmp	r5, #0
	beq	.L1271
	ldr	r3, [r5, #8]
	tst	r3, #1
	beq	.L1272
	ldr	r1, [r5, #252]
	sub	r0, r1, #44800
	sub	r2, r0, #200
	cmp	r2, #1000
	bhi	.L1281
.L1272:
	tst	r3, #4
	beq	.L1271
	ldr	r2, [r5, #256]
	sub	r3, r2, #30208
	sub	lr, r3, #62
	cmp	lr, #2000
	bhi	.L1273
	ldr	ip, [r5, #260]
	sub	r1, ip, #31744
	sub	r0, r1, #156
	cmp	r0, #2000
	bhi	.L1273
	ldr	r2, [r5, #264]
	sub	r3, r2, #62976
	sub	lr, r3, #24
	cmp	lr, #2000
	bhi	.L1273
	ldr	r1, [r5, #268]
	sub	r0, r1, #32000
	cmp	r0, #2000
	bhi	.L1273
	ldr	r3, [r5, #272]
	sub	lr, r3, #28928
	sub	ip, lr, #72
	cmp	ip, #2000
	bhi	.L1273
	ldr	r1, [r5, #276]
	sub	r0, r1, #58880
	sub	r2, r0, #120
	cmp	r2, #2000
	bhi	.L1273
	ldr	r3, [r5, #280]
	sub	lr, r3, #13952
	sub	ip, lr, #48
	cmp	ip, #2000
	bhi	.L1273
	ldr	r1, [r5, #284]
	sub	r0, r1, #4992
	sub	r2, r0, #8
	cmp	r2, #2000
	bls	.L1271
.L1273:
	mov	r0, r4
	ldr	r1, .L1282+4
	bl	png_warning
.L1271:
	mov	r0, r4
	mov	r1, r5
	mov	r2, r6
	bl	png_set_sRGB_gAMA_and_cHRM
	b	.L1275
.L1276:
	ldr	r1, .L1282+8
	bl	png_error
	b	.L1265
.L1278:
	ldr	r1, .L1282+12
	bl	png_warning
	b	.L1265
.L1279:
	mov	r0, r4
	ldr	r1, .L1282+16
	bl	png_warning
	mov	r0, r4
	mov	r1, r6
	bl	png_crc_finish
	b	.L1275
.L1277:
	ldr	r1, .L1282+20
	bl	png_warning
	mov	r0, r4
	mov	r1, r6
	bl	png_crc_finish
	b	.L1275
.L1281:
	mov	r0, r4
	ldr	r1, .L1282+24
	bl	png_warning
	ldr	ip, .L1282+28
	ldr	r1, .L1282+32
	ldr	r0, [ip, #0]
	ldr	r2, [r4, #592]
	bl	fprintf
	ldr	r3, [r5, #8]
	b	.L1272
.L1280:
	mov	r0, r4
	ldr	r1, .L1282+36
	bl	png_warning
	b	.L1275
.L1283:
	.align	2
.L1282:
	.word	.LC116
	.word	.LC95
	.word	.LC112
	.word	.LC114
	.word	.LC115
	.word	.LC113
	.word	.LC88
	.word	__aeabi_stderr
	.word	.LC118
	.word	.LC117
	.size	png_handle_sRGB, .-png_handle_sRGB
	.section	.text.png_handle_unknown,"ax",%progbits
	.align	2
	.global	png_handle_unknown
	.hidden	png_handle_unknown
	.type	png_handle_unknown, %function
png_handle_unknown:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	ldr	r6, [r0, #132]
	tst	r6, #4
	mov	r4, r0
	mov	r7, r1
	mov	r5, r2
	beq	.L1285
	add	r0, r0, #312
	ldr	r1, .L1301
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	orrne	r6, r6, #8
	strne	r6, [r4, #132]
.L1285:
	ldrb	r3, [r4, #312]	@ zero_extendqisi2
	tst	r3, #32
	beq	.L1296
.L1286:
	ldr	r2, [r4, #136]
	tst	r2, #32768
	bne	.L1287
	ldr	ip, [r4, #568]
	cmp	ip, #0
	moveq	r1, r5
	beq	.L1288
.L1287:
	add	r6, r4, #312
	ldmia	r6, {r0, r1}
	cmp	r5, #0
	mov	r1, #0
	str	r0, [r4, #648]
	strb	r1, [r4, #652]
	str	r5, [r4, #660]
	streq	r5, [r4, #656]
	bne	.L1297
	ldr	r3, [r4, #568]
	cmp	r3, #0
	beq	.L1291
.L1300:
	add	r5, r4, #648
	mov	r0, r4
	mov	r1, r5
	mov	lr, pc
	bx	r3
	cmp	r0, #0
	blt	.L1298
	bne	.L1293
	ldrb	lr, [r4, #312]	@ zero_extendqisi2
	tst	lr, #32
	beq	.L1299
.L1294:
	mov	r1, r7
	mov	r2, r5
	mov	r0, r4
	mov	r3, #1
	bl	png_set_unknown_chunks
.L1293:
	ldr	r1, [r4, #656]
	mov	r0, r4
	bl	png_free
	mov	r1, #0
	str	r1, [r4, #656]
.L1288:
	mov	r0, r4
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	b	png_crc_finish
.L1296:
	mov	r0, r4
	add	r1, r4, #312
	bl	png_handle_as_unknown
	cmp	r0, #3
	beq	.L1286
	ldr	r0, [r4, #568]
	cmp	r0, #0
	bne	.L1286
	mov	r0, r4
	ldr	r1, .L1301+4
	bl	png_chunk_error
	b	.L1286
.L1297:
	mov	r1, r5
	mov	r0, r4
	bl	png_malloc
	mov	r8, r0
	str	r0, [r4, #656]
	mov	r1, r8
	mov	r0, r4
	mov	r2, r5
	bl	png_read_data
	mov	r0, r4
	mov	r1, r8
	mov	r2, r5
	bl	png_calculate_crc
	ldr	r3, [r4, #568]
	cmp	r3, #0
	bne	.L1300
.L1291:
	mov	r1, r7
	mov	r0, r4
	add	r2, r4, #648
	mov	r3, #1
	bl	png_set_unknown_chunks
	b	.L1293
.L1299:
	mov	r1, r6
	mov	r0, r4
	bl	png_handle_as_unknown
	cmp	r0, #3
	beq	.L1294
	mov	r0, r4
	ldr	r1, .L1301+4
	bl	png_chunk_error
	b	.L1294
.L1298:
	mov	r0, r4
	ldr	r1, .L1301+8
	bl	png_chunk_error
	b	.L1293
.L1302:
	.align	2
.L1301:
	.word	png_IDAT
	.word	.LC119
	.word	.LC120
	.size	png_handle_unknown, .-png_handle_unknown
	.section	.text.png_handle_tRNS,"ax",%progbits
	.align	2
	.global	png_handle_tRNS
	.hidden	png_handle_tRNS
	.type	png_handle_tRNS, %function
png_handle_tRNS:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 264
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	ldr	r3, [r0, #132]
	tst	r3, #1
	sub	sp, sp, #272
	mov	r4, r0
	mov	r6, r1
	mov	r5, r2
	beq	.L1321
	tst	r3, #4
	bne	.L1322
	cmp	r1, #0
	beq	.L1305
	ldr	r3, [r1, #8]
	tst	r3, #16
	bne	.L1323
.L1305:
	ldrb	r3, [r4, #322]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L1308
.L1327:
	cmp	r5, #2
	beq	.L1309
.L1320:
	mov	r0, r4
	ldr	r1, .L1328
	bl	png_warning
	mov	r0, r4
	mov	r1, r5
	bl	png_crc_finish
.L1318:
	add	sp, sp, #272
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L1308:
	cmp	r3, #2
	beq	.L1324
	cmp	r3, #3
	bne	.L1313
	ldr	r2, [r4, #132]
	tst	r2, #2
	beq	.L1325
.L1314:
	mov	r3, #308
	ldrh	r1, [r4, r3]
	cmp	r5, r1
	cmpls	r5, #256
	bhi	.L1320
	cmp	r5, #0
	mov	r0, r4
	beq	.L1326
	add	r1, sp, #8
	mov	r2, r5
	bl	png_crc_read
	mov	ip, #308
	add	r0, ip, #2
	strh	r5, [r4, r0]	@ movhi
.L1310:
	mov	r0, r4
	mov	r1, #0
	bl	png_crc_finish
	mov	r2, #308
	cmp	r0, #0
	add	r3, r2, #2
	movne	r2, #0	@ movhi
	strneh	r2, [r4, r3]	@ movhi
	bne	.L1318
	ldrh	r3, [r4, r3]
	mov	r0, r4
	mov	r1, r6
	add	r4, r4, #424
	add	r2, sp, #8
	str	r4, [sp, #0]
	bl	png_set_tRNS
	b	.L1318
.L1321:
	ldr	r1, .L1328+4
	bl	png_error
	ldrb	r3, [r4, #322]	@ zero_extendqisi2
	cmp	r3, #0
	bne	.L1308
	b	.L1327
.L1313:
	mov	r0, r4
	ldr	r1, .L1328+8
	bl	png_warning
	mov	r0, r4
	mov	r1, r5
	bl	png_crc_finish
	b	.L1318
.L1323:
	ldr	r1, .L1328+12
	bl	png_warning
	mov	r0, r4
	mov	r1, r5
	bl	png_crc_finish
	b	.L1318
.L1322:
	ldr	r1, .L1328+16
	bl	png_warning
	mov	r0, r4
	mov	r1, r5
	bl	png_crc_finish
	b	.L1318
.L1309:
	add	r7, sp, #264
	mov	r0, r4
	mov	r1, r7
	mov	r2, r5
	bl	png_read_data
	mov	r0, r4
	mov	r1, r7
	mov	r2, r5
	bl	png_calculate_crc
	ldrb	r0, [sp, #264]	@ zero_extendqisi2
	ldrb	r2, [sp, #265]	@ zero_extendqisi2
	mov	r3, #308
	add	r1, r2, r0, asl #8
	mov	ip, #432
	add	r0, r3, #2
	mov	r2, #1	@ movhi
	strh	r1, [r4, ip]	@ movhi
	strh	r2, [r4, r0]	@ movhi
	b	.L1310
.L1324:
	cmp	r5, #6
	bne	.L1320
	mov	r2, r5
	mov	r0, r4
	add	r1, sp, #264
	bl	png_crc_read
	ldrb	r8, [sp, #264]	@ zero_extendqisi2
	ldrb	lr, [sp, #265]	@ zero_extendqisi2
	ldrb	r7, [sp, #266]	@ zero_extendqisi2
	ldrb	ip, [sp, #267]	@ zero_extendqisi2
	ldrb	r5, [sp, #268]	@ zero_extendqisi2
	ldrb	r0, [sp, #269]	@ zero_extendqisi2
	mov	r1, #424
	add	lr, lr, r8, asl #8
	add	r1, r1, #2
	mov	r3, #308
	mov	r2, #428
	strh	lr, [r4, r1]	@ movhi
	add	ip, ip, r7, asl #8
	add	r0, r0, r5, asl #8
	add	r3, r3, #2
	add	r2, r2, #2
	mov	r1, #428
	mov	lr, #1	@ movhi
	strh	ip, [r4, r1]	@ movhi
	strh	r0, [r4, r2]	@ movhi
	strh	lr, [r4, r3]	@ movhi
	b	.L1310
.L1326:
	ldr	r1, .L1328+20
	bl	png_warning
	mov	r0, r4
	mov	r1, r5
	bl	png_crc_finish
	b	.L1318
.L1325:
	mov	r0, r4
	ldr	r1, .L1328+24
	bl	png_warning
	b	.L1314
.L1329:
	.align	2
.L1328:
	.word	.LC124
	.word	.LC121
	.word	.LC127
	.word	.LC123
	.word	.LC122
	.word	.LC126
	.word	.LC125
	.size	png_handle_tRNS, .-png_handle_tRNS
	.section	.rodata
	.align	2
	.set	.LANCHOR0,. + 0
	.type	msg.4197, %object
	.size	msg.4197, 31
msg.4197:
	.ascii	"Error decoding compressed text\000"
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"PNG unsigned integer out of range.\000"
	.space	1
.LC1:
	.ascii	"Row has too many bytes to allocate in memory.\000"
	.space	2
.LC2:
	.ascii	"Ignoring bad adaptive filter type\000"
	.space	2
.LC3:
	.ascii	"invalid chunk type\000"
	.space	1
.LC4:
	.ascii	"Not enough memory to decompress chunk\000"
	.space	2
.LC5:
	.ascii	"Not enough memory to decompress chunk.\000"
	.space	1
.LC6:
	.ascii	"Not enough memory to decompress chunk..\000"
.LC7:
	.ascii	"Buffer error in compressed datastream in %s chunk\000"
	.space	2
.LC8:
	.ascii	"Data error in compressed datastream in %s chunk\000"
.LC9:
	.ascii	"Incomplete compressed datastream in %s chunk\000"
	.space	3
.LC10:
	.ascii	"Not enough memory for text.\000"
.LC11:
	.ascii	"Unknown zTXt compression type %d\000"
	.space	3
.LC12:
	.ascii	"CRC error\000"
	.space	2
.LC13:
	.ascii	"Not enough image data\000"
	.space	2
.LC14:
	.ascii	"Extra compressed data\000"
	.space	2
.LC15:
	.ascii	"Decompression Error\000"
.LC16:
	.ascii	"Extra compressed data.\000"
	.space	1
.LC17:
	.ascii	"Extra compression data\000"
	.space	1
.LC18:
	.ascii	"Missing IHDR before zTXt\000"
	.space	3
.LC19:
	.ascii	"Out of memory processing zTXt chunk.\000"
	.space	3
.LC20:
	.ascii	"Truncated zTXt chunk\000"
	.space	3
.LC21:
	.ascii	"Unknown compression type in zTXt chunk\000"
	.space	1
.LC22:
	.ascii	"Not enough memory to process zTXt chunk.\000"
	.space	3
.LC23:
	.ascii	"Insufficient memory to store zTXt chunk.\000"
	.space	3
.LC24:
	.ascii	"Missing IHDR before tEXt\000"
	.space	3
.LC25:
	.ascii	"No memory to process text chunk.\000"
	.space	3
.LC26:
	.ascii	"Not enough memory to process text chunk.\000"
	.space	3
.LC27:
	.ascii	"Insufficient memory to process text chunk.\000"
	.space	1
.LC28:
	.ascii	"Out of place tIME chunk\000"
.LC29:
	.ascii	"Duplicate tIME chunk\000"
	.space	3
.LC30:
	.ascii	"Incorrect tIME chunk length\000"
.LC31:
	.ascii	"Missing IHDR before sCAL\000"
	.space	3
.LC32:
	.ascii	"Invalid sCAL after IDAT\000"
.LC33:
	.ascii	"Duplicate sCAL chunk\000"
	.space	3
.LC34:
	.ascii	"Out of memory while processing sCAL chunk\000"
	.space	2
.LC35:
	.ascii	"malformed width string in sCAL chunk\000"
	.space	3
.LC36:
	.ascii	"Truncated sCAL chunk\000"
	.space	3
.LC37:
	.ascii	"malformed height string in sCAL chunk\000"
	.space	2
.LC38:
	.ascii	"Invalid sCAL data\000"
	.space	2
.LC39:
	.ascii	"Missing IHDR before pCAL\000"
	.space	3
.LC40:
	.ascii	"Invalid pCAL after IDAT\000"
.LC41:
	.ascii	"Duplicate pCAL chunk\000"
	.space	3
.LC42:
	.ascii	"No memory for pCAL purpose.\000"
.LC43:
	.ascii	"Invalid pCAL data\000"
	.space	2
.LC44:
	.ascii	"Invalid pCAL parameters for equation type\000"
	.space	2
.LC45:
	.ascii	"Unrecognized equation type for pCAL chunk\000"
	.space	2
.LC46:
	.ascii	"No memory for pCAL params.\000"
	.space	1
.LC47:
	.ascii	"Missing IHDR before hIST\000"
	.space	3
.LC48:
	.ascii	"Invalid hIST after IDAT\000"
.LC49:
	.ascii	"Missing PLTE before hIST\000"
	.space	3
.LC50:
	.ascii	"Duplicate hIST chunk\000"
	.space	3
.LC51:
	.ascii	"Incorrect hIST chunk length\000"
.LC52:
	.ascii	"Missing IHDR before sPLT\000"
	.space	3
.LC53:
	.ascii	"Invalid sPLT after IDAT\000"
.LC54:
	.ascii	"malformed sPLT chunk\000"
	.space	3
.LC55:
	.ascii	"sPLT chunk has bad length\000"
	.space	2
.LC56:
	.ascii	"sPLT chunk too long\000"
.LC57:
	.ascii	"sPLT chunk requires too much memory\000"
.LC58:
	.ascii	"Missing IHDR before iCCP\000"
	.space	3
.LC59:
	.ascii	"Invalid iCCP after IDAT\000"
.LC60:
	.ascii	"Out of place iCCP chunk\000"
.LC61:
	.ascii	"Duplicate iCCP chunk\000"
	.space	3
.LC62:
	.ascii	"Malformed iCCP chunk\000"
	.space	3
.LC63:
	.ascii	"Ignoring nonzero compression type in iCCP chunk\000"
.LC64:
	.ascii	"Profile size field missing from iCCP chunk\000"
	.space	1
.LC65:
	.ascii	"Ignoring truncated iCCP profile.\000"
	.space	3
.LC66:
	.ascii	"Missing IHDR before sBIT\000"
	.space	3
.LC67:
	.ascii	"Invalid sBIT after IDAT\000"
.LC68:
	.ascii	"Out of place sBIT chunk\000"
.LC69:
	.ascii	"Duplicate sBIT chunk\000"
	.space	3
.LC70:
	.ascii	"Incorrect sBIT chunk length\000"
.LC71:
	.ascii	"No image in file\000"
	.space	3
.LC72:
	.ascii	"Incorrect IEND chunk length\000"
.LC73:
	.ascii	"Missing IHDR before PLTE\000"
	.space	3
.LC74:
	.ascii	"Invalid PLTE after IDAT\000"
.LC75:
	.ascii	"Duplicate PLTE chunk\000"
	.space	3
.LC76:
	.ascii	"Ignoring PLTE chunk in grayscale PNG\000"
	.space	3
.LC77:
	.ascii	"Invalid palette chunk\000"
	.space	2
.LC78:
	.ascii	"Truncating incorrect tRNS chunk length\000"
	.space	1
.LC79:
	.ascii	"Truncating incorrect info tRNS chunk length\000"
.LC80:
	.ascii	"Out of place IHDR\000"
	.space	2
.LC81:
	.ascii	"Invalid IHDR chunk\000"
	.space	1
.LC82:
	.ascii	"Missing IHDR before gAMA\000"
	.space	3
.LC83:
	.ascii	"Invalid gAMA after IDAT\000"
.LC84:
	.ascii	"Out of place gAMA chunk\000"
.LC85:
	.ascii	"Duplicate gAMA chunk\000"
	.space	3
.LC86:
	.ascii	"Incorrect gAMA chunk length\000"
.LC87:
	.ascii	"Ignoring gAMA chunk with gamma=0\000"
	.space	3
.LC88:
	.ascii	"Ignoring incorrect gAMA value when sRGB is also pre"
	.ascii	"sent\000"
.LC89:
	.ascii	"gamma = (%d/100000)\000"
.LC90:
	.ascii	"Missing IHDR before cHRM\000"
	.space	3
.LC91:
	.ascii	"Invalid cHRM after IDAT\000"
.LC92:
	.ascii	"Missing PLTE before cHRM\000"
	.space	3
.LC93:
	.ascii	"Duplicate cHRM chunk\000"
	.space	3
.LC94:
	.ascii	"Incorrect cHRM chunk length\000"
.LC95:
	.ascii	"Ignoring incorrect cHRM value when sRGB is also pre"
	.ascii	"sent\000"
.LC96:
	.ascii	"wx=%f, wy=%f, rx=%f, ry=%f\012\000"
.LC97:
	.ascii	"gx=%f, gy=%f, bx=%f, by=%f\012\000"
.LC98:
	.ascii	"Missing IHDR before oFFs\000"
	.space	3
.LC99:
	.ascii	"Invalid oFFs after IDAT\000"
.LC100:
	.ascii	"Duplicate oFFs chunk\000"
	.space	3
.LC101:
	.ascii	"Incorrect oFFs chunk length\000"
.LC102:
	.ascii	"Missing IHDR before pHYs\000"
	.space	3
.LC103:
	.ascii	"Invalid pHYs after IDAT\000"
.LC104:
	.ascii	"Duplicate pHYs chunk\000"
	.space	3
.LC105:
	.ascii	"Incorrect pHYs chunk length\000"
.LC106:
	.ascii	"Missing IHDR before bKGD\000"
	.space	3
.LC107:
	.ascii	"Invalid bKGD after IDAT\000"
.LC108:
	.ascii	"Missing PLTE before bKGD\000"
	.space	3
.LC109:
	.ascii	"Duplicate bKGD chunk\000"
	.space	3
.LC110:
	.ascii	"Incorrect bKGD chunk length\000"
.LC111:
	.ascii	"Incorrect bKGD chunk index value\000"
	.space	3
.LC112:
	.ascii	"Missing IHDR before sRGB\000"
	.space	3
.LC113:
	.ascii	"Invalid sRGB after IDAT\000"
.LC114:
	.ascii	"Out of place sRGB chunk\000"
.LC115:
	.ascii	"Duplicate sRGB chunk\000"
	.space	3
.LC116:
	.ascii	"Incorrect sRGB chunk length\000"
.LC117:
	.ascii	"Unknown sRGB intent\000"
.LC118:
	.ascii	"incorrect gamma=(%d/100000)\012\000"
	.space	3
.LC119:
	.ascii	"unknown critical chunk\000"
	.space	1
.LC120:
	.ascii	"error in user chunk\000"
.LC121:
	.ascii	"Missing IHDR before tRNS\000"
	.space	3
.LC122:
	.ascii	"Invalid tRNS after IDAT\000"
.LC123:
	.ascii	"Duplicate tRNS chunk\000"
	.space	3
.LC124:
	.ascii	"Incorrect tRNS chunk length\000"
.LC125:
	.ascii	"Missing PLTE before tRNS\000"
	.space	3
.LC126:
	.ascii	"Zero length tRNS chunk\000"
	.space	1
.LC127:
	.ascii	"tRNS chunk not allowed with alpha channel\000"
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
