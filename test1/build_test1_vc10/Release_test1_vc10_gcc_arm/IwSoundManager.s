	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"IwSoundManager.cpp"
	.section	.text._ZN15CIwSoundManager16SetMaxSoundInstsEj,"ax",%progbits
	.align	2
	.global	_ZN15CIwSoundManager16SetMaxSoundInstsEj
	.hidden	_ZN15CIwSoundManager16SetMaxSoundInstsEj
	.type	_ZN15CIwSoundManager16SetMaxSoundInstsEj, %function
_ZN15CIwSoundManager16SetMaxSoundInstsEj:
	.fnstart
.LFB1382:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN15CIwSoundManager16SetMaxSoundInstsEj, .-_ZN15CIwSoundManager16SetMaxSoundInstsEj
	.section	.text._ZN15CIwSoundManager11GetFreeInstEv,"ax",%progbits
	.align	2
	.global	_ZN15CIwSoundManager11GetFreeInstEv
	.hidden	_ZN15CIwSoundManager11GetFreeInstEv
	.type	_ZN15CIwSoundManager11GetFreeInstEv, %function
_ZN15CIwSoundManager11GetFreeInstEv:
	.fnstart
.LFB1386:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	add	r2, r0, #16
	ldmia	r2, {r2, r3}	@ phole ldm
	cmp	r3, r2
	addcc	r2, r3, #1
	strcc	r2, [r0, #20]
	ldrcc	r2, [r0, #12]
	ldrcc	r0, [r2, r3, asl #2]
	ldrcch	r3, [r0, #10]
	biccc	r3, r3, #4
	movcs	r0, #0
	strcch	r3, [r0, #10]	@ movhi
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN15CIwSoundManager11GetFreeInstEv, .-_ZN15CIwSoundManager11GetFreeInstEv
	.section	.text._ZN15CIwSoundManager11SetFreeInstEP12CIwSoundInst,"ax",%progbits
	.align	2
	.global	_ZN15CIwSoundManager11SetFreeInstEP12CIwSoundInst
	.hidden	_ZN15CIwSoundManager11SetFreeInstEP12CIwSoundInst
	.type	_ZN15CIwSoundManager11SetFreeInstEP12CIwSoundInst, %function
_ZN15CIwSoundManager11SetFreeInstEP12CIwSoundInst:
	.fnstart
.LFB1387:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	str	r4, [sp, #-4]!
	ldrh	r2, [r1, #10]
	ldrh	r3, [r1, #16]
	orr	ip, r2, #4
	add	r3, r3, #1
	strh	ip, [r1, #10]	@ movhi
	strh	r3, [r1, #16]	@ movhi
	ldr	r2, [r0, #20]
	ldr	r3, [r0, #12]
	sub	ip, r2, #1
	add	ip, r3, ip, asl #2
	cmp	r3, ip
	bhi	.L12
	ldr	r2, [r3, #0]
	cmp	r2, r1
	beq	.L9
	rsb	r2, r3, ip
	mov	r2, r2, lsr #2
	ands	r2, r2, #3
	beq	.L13
	add	r3, r3, #4
	cmp	ip, r3
	bcc	.L12
	ldr	r4, [r3, #0]
	cmp	r4, r1
	beq	.L37
	cmp	r2, #1
	beq	.L13
	cmp	r2, #2
	beq	.L32
	ldr	r2, [r3, #4]!
	cmp	r2, r1
	beq	.L37
.L32:
	ldr	r2, [r3, #4]!
	cmp	r2, r1
	beq	.L37
.L13:
	add	r3, r3, #4
	cmp	ip, r3
	mov	r2, r3
	bcc	.L12
	ldr	r4, [r3, #0]
	cmp	r4, r1
	beq	.L37
	ldr	r4, [r3, #4]!
	cmp	r4, r1
	beq	.L37
	ldr	r3, [r2, #8]
	cmp	r3, r1
	add	r3, r2, #8
	beq	.L37
	ldr	r3, [r2, #12]
	cmp	r3, r1
	add	r3, r2, #12
	bne	.L13
.L37:
	mov	r2, r1
.L9:
	ldr	r1, [ip, #0]
	str	r1, [r3, #0]
	str	r2, [ip, #0]
	ldr	ip, [r0, #20]
	sub	r1, ip, #1
	str	r1, [r0, #20]
.L12:
	ldmfd	sp!, {r4}
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN15CIwSoundManager11SetFreeInstEP12CIwSoundInst, .-_ZN15CIwSoundManager11SetFreeInstEP12CIwSoundInst
	.section	.text._ZN13CIwChannelPCMIaE17GenerateAudioFastEPsiiiiPi,"axG",%progbits,_ZN13CIwChannelPCMIaE17GenerateAudioFastEPsiiiiPi,comdat
	.align	2
	.weak	_ZN13CIwChannelPCMIaE17GenerateAudioFastEPsiiiiPi
	.hidden	_ZN13CIwChannelPCMIaE17GenerateAudioFastEPsiiiiPi
	.type	_ZN13CIwChannelPCMIaE17GenerateAudioFastEPsiiiiPi, %function
_ZN13CIwChannelPCMIaE17GenerateAudioFastEPsiiiiPi:
	.fnstart
.LFB1515:
	@ Function supports interworking.
	@ args = 12, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
	cmp	r2, #0
	sub	sp, sp, #8
	mov	ip, r0
	ldr	r7, [sp, #40]
	ldr	r0, [sp, #44]
	beq	.L40
	ldr	r6, [ip, #8]
	cmp	r0, #0
	sub	r6, r6, #1
	ldr	r4, [ip, #0]
	ldr	r0, [ip, #12]
	moveq	r5, r1
	beq	.L119
	mov	r5, #32512
	add	r5, r5, #255
	mov	r8, #-2147483648
	str	r5, [sp, #4]
	mov	r8, r8, asr #16
	mov	r5, r1
.L118:
	cmp	r4, r6
	beq	.L46
	bhi	.L156
	cmp	r2, #0
	beq	.L48
	cmp	r7, #0
	beq	.L104
	ldrsb	sl, [r4, #0]
	ldrsb	r9, [r4, #1]
	rsb	r9, sl, r9
	mul	r9, r0, r9
	add	sl, sl, r9, asr #12
	mul	sl, r7, sl
	ldrsh	r9, [r5, #0]
	add	sl, r9, sl, asr #8
	add	r9, sl, #32768
	mov	r9, r9, lsr #16
	mov	r9, r9, asl #16
	cmp	r9, #0
	sub	r9, r2, #1
	and	r9, r9, #1
	beq	.L67
	ldr	fp, [sp, #4]
	cmp	sl, r8
	movlt	sl, r8
	cmp	sl, fp
	movge	sl, fp
.L67:
	add	r0, r0, r3
	add	r4, r4, r0, asr #12
	cmp	r6, r4
	mov	r0, r0, asl #20
	strh	sl, [r5], #2	@ movhi
	mov	r0, r0, lsr #20
	sub	r2, r2, #1
	bls	.L118
	cmp	r2, #0
	beq	.L48
	cmp	r9, #0
	beq	.L138
	ldrsb	sl, [r4, #0]
	ldrsb	r9, [r4, #1]
	rsb	fp, sl, r9
	mul	fp, r0, fp
	add	r9, sl, fp, asr #12
	mul	r9, r7, r9
	ldrsh	fp, [r5, #0]
	add	r9, fp, r9, asr #8
	add	sl, r9, #32768
	mov	sl, sl, lsr #16
	mov	sl, sl, asl #16
	cmp	sl, #0
	beq	.L69
	ldr	sl, [sp, #4]
	cmp	r9, r8
	movlt	r9, r8
	cmp	r9, sl
	movge	r9, sl
.L69:
	add	r0, r0, r3
	add	r4, r4, r0, asr #12
	cmp	r6, r4
	mov	r0, r0, asl #20
	strh	r9, [r5], #2	@ movhi
	mov	r0, r0, lsr #20
	sub	r2, r2, #1
	bls	.L118
	cmp	r2, #0
	beq	.L48
.L138:
	mov	fp, ip
.L59:
	ldrsb	ip, [r4, #0]
	ldrsb	sl, [r4, #1]
	rsb	r9, ip, sl
	mul	r9, r0, r9
	add	sl, ip, r9, asr #12
	mul	sl, r7, sl
	ldrsh	r9, [r5, #0]
	add	sl, r9, sl, asr #8
	add	ip, sl, #32768
	mov	ip, ip, lsr #16
	mov	ip, ip, asl #16
	add	r0, r0, r3
	cmp	ip, #0
	add	r4, r4, r0, asr #12
	sub	r2, r2, #1
	mov	r0, r0, asl #20
	beq	.L50
	ldr	ip, [sp, #4]
	cmp	sl, r8
	movlt	sl, r8
	cmp	sl, ip
	movge	sl, ip
.L50:
	strh	sl, [r5], #2	@ movhi
	cmp	r6, r4
	mov	r0, r0, lsr #20
	mov	sl, r5
	mov	ip, r2
	bls	.L139
	ldrsb	r2, [r4, #0]
	ldrsb	r9, [r4, #1]
	rsb	r9, r2, r9
	mul	r9, r0, r9
	add	r2, r2, r9, asr #12
	mul	r2, r7, r2
	ldrsh	r9, [r5, #0]
	add	r2, r9, r2, asr #8
	add	r5, r2, #32768
	mov	r5, r5, lsr #16
	mov	r5, r5, asl #16
	cmp	r5, #0
	beq	.L72
	ldr	r5, [sp, #4]
	cmp	r2, r8
	movlt	r2, r8
	cmp	r2, r5
	movge	r2, r5
.L72:
	add	r0, r0, r3
	add	r4, r4, r0, asr #12
	mov	r5, sl
	mov	r0, r0, asl #20
	cmp	r6, r4
	strh	r2, [r5], #2	@ movhi
	mov	r0, r0, lsr #20
	sub	r2, ip, #1
	bls	.L139
	cmp	r2, #0
	bne	.L59
	mov	ip, fp
.L48:
	rsb	r3, r1, r5
	str	r4, [ip, #0]
	str	r0, [ip, #12]
	mov	r0, r3, asr #1
.L40:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
	bx	lr
.L52:
	cmp	r2, #0
	beq	.L44
	ldrsb	sl, [r4, #0]
	mul	sl, r7, sl
	add	r8, r0, r3
	mov	r0, sl, asr #8
	strh	r0, [r5], #2	@ movhi
	mov	r0, r8, asl #20
	add	r4, r4, r8, asr #12
	mov	r0, r0, lsr #20
	sub	r2, r2, #1
.L119:
	cmp	r4, r6
	beq	.L52
	bhi	.L156
	cmp	r2, #0
	beq	.L48
	cmp	r7, #0
	beq	.L106
	ldrsb	r8, [r4, #0]
	ldrsb	sl, [r4, #1]
	rsb	sl, r8, sl
	mul	sl, r0, sl
	add	sl, r8, sl, asr #12
	mul	sl, r7, sl
	add	r0, r0, r3
	add	r4, r4, r0, asr #12
	sub	r8, r2, #1
	mov	sl, sl, asr #8
	mov	r0, r0, asl #20
	cmp	r6, r4
	mov	r2, r8
	strh	sl, [r5], #2	@ movhi
	mov	r0, r0, lsr #20
	and	r8, r8, #3
	bls	.L119
	cmp	r2, #0
	beq	.L48
	cmp	r8, #0
	beq	.L147
	cmp	r8, #1
	beq	.L113
	cmp	r8, #2
	beq	.L114
	ldrsb	r8, [r4, #0]
	ldrsb	sl, [r4, #1]
	rsb	sl, r8, sl
	mul	sl, r0, sl
	add	r8, r8, sl, asr #12
	mul	r8, r7, r8
	add	r0, r0, r3
	add	r4, r4, r0, asr #12
	mov	r8, r8, asr #8
	mov	r0, r0, asl #20
	cmp	r6, r4
	strh	r8, [r5], #2	@ movhi
	mov	r0, r0, lsr #20
	sub	r2, r2, #1
	bls	.L119
.L114:
	ldrsb	r8, [r4, #0]
	ldrsb	sl, [r4, #1]
	rsb	sl, r8, sl
	mul	sl, r0, sl
	add	r8, r8, sl, asr #12
	mul	r8, r7, r8
	add	r0, r0, r3
	add	r4, r4, r0, asr #12
	mov	r8, r8, asr #8
	mov	r0, r0, asl #20
	cmp	r6, r4
	strh	r8, [r5], #2	@ movhi
	mov	r0, r0, lsr #20
	sub	r2, r2, #1
	bls	.L119
.L113:
	ldrsb	r8, [r4, #0]
	ldrsb	sl, [r4, #1]
	rsb	sl, r8, sl
	mul	sl, r0, sl
	add	r8, r8, sl, asr #12
	mul	r8, r7, r8
	add	r0, r0, r3
	add	r4, r4, r0, asr #12
	mov	r8, r8, asr #8
	mov	r0, r0, asl #20
	cmp	r6, r4
	strh	r8, [r5], #2	@ movhi
	mov	r0, r0, lsr #20
	sub	r2, r2, #1
	bls	.L119
	cmp	r2, #0
	beq	.L48
.L147:
	str	ip, [sp, #4]
	mov	fp, r1
.L61:
	ldrsb	sl, [r4, #0]
	ldrsb	r1, [r4, #1]
	rsb	ip, sl, r1
	mul	ip, r0, ip
	add	r1, sl, ip, asr #12
	mul	r1, r7, r1
	add	ip, r0, r3
	mov	sl, r1, asr #8
	strh	sl, [r5], #2	@ movhi
	mov	r1, ip, asl #20
	add	r4, r4, ip, asr #12
	mov	r0, r1, lsr #20
	sub	r2, r2, #1
	cmp	r6, r4
	add	ip, r0, r3
	mov	r1, r5
	mov	sl, r2
	bls	.L149
	ldrsb	r8, [r4, #0]
	ldrsb	r9, [r4, #1]
	rsb	r9, r8, r9
	mul	r0, r9, r0
	add	r8, r8, r0, asr #12
	mul	r8, r7, r8
	add	r4, r4, ip, asr #12
	mov	r0, ip, asl #20
	mov	r8, r8, asr #8
	mov	r0, r0, lsr #20
	cmp	r6, r4
	strh	r8, [r5], #2	@ movhi
	add	ip, r0, r3
	sub	r2, r2, #1
	bls	.L149
	ldrsb	r2, [r4, #0]
	ldrsb	r8, [r4, #1]
	rsb	r8, r2, r8
	mul	r0, r8, r0
	add	r2, r2, r0, asr #12
	mul	r2, r7, r2
	add	r4, r4, ip, asr #12
	mov	r0, ip, asl #20
	mov	r2, r2, asr #8
	mov	r0, r0, lsr #20
	cmp	r6, r4
	strh	r2, [r5, #0]	@ movhi
	add	ip, r0, r3
	add	r5, r1, #4
	sub	r2, sl, #2
	bls	.L149
	ldrsb	r2, [r4, #0]
	ldrsb	r5, [r4, #1]
	rsb	r5, r2, r5
	mul	r0, r5, r0
	add	r2, r2, r0, asr #12
	mul	r2, r7, r2
	add	r4, r4, ip, asr #12
	mov	r5, r2, asr #8
	mov	r0, ip, asl #20
	cmp	r6, r4
	strh	r5, [r1, #4]	@ movhi
	sub	r2, sl, #3
	add	r5, r1, #6
	mov	r0, r0, lsr #20
	bls	.L149
	cmp	r2, #0
	bne	.L61
	ldr	ip, [sp, #4]
	mov	r1, fp
	b	.L48
.L104:
	add	r0, r0, r3
	add	r4, r4, r0, asr #12
	sub	sl, r2, #1
	mov	r0, r0, asl #20
	cmp	r6, r4
	mov	r2, sl
	add	r5, r5, #2
	mov	r0, r0, lsr #20
	and	sl, sl, #3
	bls	.L118
	cmp	r2, #0
	beq	.L48
	cmp	sl, #0
	beq	.L142
	cmp	sl, #1
	beq	.L110
	cmp	sl, #2
	beq	.L111
	add	r0, r0, r3
	add	r4, r4, r0, asr #12
	cmp	r6, r4
	mov	r0, r0, asl #20
	add	r5, r5, #2
	mov	r0, r0, lsr #20
	sub	r2, r2, #1
	bls	.L118
.L111:
	add	r0, r0, r3
	add	r4, r4, r0, asr #12
	cmp	r6, r4
	mov	r0, r0, asl #20
	add	r5, r5, #2
	mov	r0, r0, lsr #20
	sub	r2, r2, #1
	bls	.L118
.L110:
	add	r0, r0, r3
	add	r4, r4, r0, asr #12
	cmp	r6, r4
	mov	r0, r0, asl #20
	add	r5, r5, #2
	mov	r0, r0, lsr #20
	sub	r2, r2, #1
	bls	.L118
	cmp	r2, #0
	beq	.L48
.L142:
	mov	fp, r7
.L60:
	add	r9, r0, r3
	mov	r7, r9, asl #20
	add	r4, r4, r9, asr #12
	add	r5, r5, #2
	sub	r2, r2, #1
	mov	r0, r7, lsr #20
	cmp	r6, r4
	add	r7, r0, r3
	mov	sl, r5
	mov	r9, r2
	bls	.L144
	add	r4, r4, r7, asr #12
	mov	r0, r7, asl #20
	mov	r0, r0, lsr #20
	cmp	r6, r4
	add	r7, r0, r3
	add	r5, r5, #2
	sub	r2, r2, #1
	bls	.L144
	mov	r2, r7, asl #20
	add	r4, r4, r7, asr #12
	mov	r0, r2, lsr #20
	cmp	r6, r4
	add	r7, r0, r3
	add	r5, sl, #4
	sub	r2, r9, #2
	bls	.L144
	add	r4, r4, r7, asr #12
	mov	r0, r7, asl #20
	cmp	r6, r4
	sub	r2, r9, #3
	add	r5, sl, #6
	mov	r0, r0, lsr #20
	bls	.L144
	cmp	r2, #0
	bne	.L60
	b	.L48
.L106:
	add	r0, r0, r3
	add	r4, r4, r0, asr #12
	sub	r8, r2, #1
	cmp	r6, r4
	mov	r2, r0, asl #20
	mov	r0, r2, lsr #20
	strh	r7, [r5], #2	@ movhi
	mov	r2, r8
	and	r8, r8, #3
	bls	.L119
	cmp	r2, #0
	beq	.L48
	cmp	r8, #0
	beq	.L62
	cmp	r8, #1
	beq	.L116
	cmp	r8, #2
	beq	.L117
	add	r0, r0, r3
	add	r4, r4, r0, asr #12
	cmp	r6, r4
	mov	r0, r0, asl #20
	strh	r7, [r5], #2	@ movhi
	mov	r0, r0, lsr #20
	sub	r2, r2, #1
	bls	.L119
.L117:
	add	r8, r0, r3
	add	r4, r4, r8, asr #12
	mov	r0, r8, asl #20
	cmp	r6, r4
	mov	r8, #0	@ movhi
	strh	r8, [r5], #2	@ movhi
	mov	r0, r0, lsr #20
	sub	r2, r2, #1
	bls	.L119
.L116:
	add	sl, r0, r3
	add	r4, r4, sl, asr #12
	mov	r0, sl, asl #20
	cmp	r6, r4
	mov	sl, #0	@ movhi
	strh	sl, [r5], #2	@ movhi
	mov	r0, r0, lsr #20
	sub	r2, r2, #1
	bls	.L119
.L155:
	cmp	r2, #0
	beq	.L48
.L62:
	add	r9, r0, r3
	mov	r8, #0	@ movhi
	mov	sl, r9, asl #20
	add	r4, r4, r9, asr #12
	strh	r8, [r5], #2	@ movhi
	sub	r2, r2, #1
	mov	r0, sl, lsr #20
	cmp	r6, r4
	add	sl, r0, r3
	mov	r8, r5
	mov	r9, r2
	bls	.L119
	add	r4, r4, sl, asr #12
	mov	r0, sl, asl #20
	mov	r0, r0, lsr #20
	mov	sl, #0	@ movhi
	cmp	r6, r4
	strh	sl, [r5], #2	@ movhi
	sub	r2, r2, #1
	add	sl, r0, r3
	bls	.L119
	mov	r2, sl, asl #20
	add	r4, r4, sl, asr #12
	mov	r0, r2, lsr #20
	mov	fp, #0	@ movhi
	cmp	r6, r4
	strh	fp, [r5, #0]	@ movhi
	add	sl, r0, r3
	add	r5, r8, #4
	sub	r2, r9, #2
	bls	.L119
	add	r4, r4, sl, asr #12
	mov	r0, sl, asl #20
	cmp	r6, r4
	strh	fp, [r8, #4]	@ movhi
	sub	r2, r9, #3
	add	r5, r8, #6
	mov	r0, r0, lsr #20
	bhi	.L155
	b	.L119
.L46:
	cmp	r2, #0
	beq	.L44
	ldrsb	sl, [r6, #0]
	mul	sl, r7, sl
	ldrsh	r4, [r5, #0]
	add	sl, r4, sl, asr #8
	add	r4, sl, #32768
	mov	r4, r4, lsr #16
	mov	r4, r4, asl #16
	cmp	r4, #0
	beq	.L45
	ldr	fp, [sp, #4]
	cmp	sl, r8
	movlt	sl, r8
	cmp	sl, fp
	movge	sl, fp
.L45:
	add	r4, r0, r3
	mov	r0, r4, asl #20
	strh	sl, [r5], #2	@ movhi
	mov	r0, r0, lsr #20
	sub	r2, r2, #1
	add	r4, r6, r4, asr #12
	b	.L118
.L139:
	mov	ip, fp
	b	.L118
.L149:
	ldr	ip, [sp, #4]
	mov	r1, fp
	b	.L119
.L144:
	mov	r7, fp
	b	.L118
.L156:
	ldr	r2, [sp, #48]
	mov	r3, #1
	str	r3, [r2, #0]
	b	.L48
.L44:
	mov	r4, r6
	b	.L48
	.cantunwind
	.fnend
	.size	_ZN13CIwChannelPCMIaE17GenerateAudioFastEPsiiiiPi, .-_ZN13CIwChannelPCMIaE17GenerateAudioFastEPsiiiiPi
	.section	.text._ZN13CIwChannelPCMIsE17GenerateAudioFastEPsiiiiPi,"axG",%progbits,_ZN13CIwChannelPCMIsE17GenerateAudioFastEPsiiiiPi,comdat
	.align	2
	.weak	_ZN13CIwChannelPCMIsE17GenerateAudioFastEPsiiiiPi
	.hidden	_ZN13CIwChannelPCMIsE17GenerateAudioFastEPsiiiiPi
	.type	_ZN13CIwChannelPCMIsE17GenerateAudioFastEPsiiiiPi, %function
_ZN13CIwChannelPCMIsE17GenerateAudioFastEPsiiiiPi:
	.fnstart
.LFB1516:
	@ Function supports interworking.
	@ args = 12, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
	cmp	r2, #0
	sub	sp, sp, #8
	mov	ip, r0
	ldr	r6, [sp, #40]
	ldr	r0, [sp, #44]
	beq	.L158
	ldr	r5, [ip, #8]
	cmp	r0, #0
	sub	r5, r5, #2
	ldr	r4, [ip, #0]
	ldr	fp, [ip, #12]
	moveq	r0, r1
	beq	.L237
	mov	r0, #32512
	add	r0, r0, #255
	mov	r8, #-2147483648
	str	r0, [sp, #0]
	mov	r8, r8, asr #16
	mov	r0, r1
.L236:
	cmp	r4, r5
	beq	.L164
	bhi	.L274
	cmp	r2, #0
	beq	.L166
	cmp	r6, #0
	bne	.L275
	add	fp, fp, r3
	mov	r7, fp, asr #12
	add	r4, r4, r7, asl #1
	mov	fp, fp, asl #20
	sub	r7, r2, #1
	cmp	r5, r4
	mov	r2, r7
	add	r0, r0, #2
	mov	fp, fp, lsr #20
	and	r7, r7, #3
	bls	.L236
	cmp	r2, #0
	beq	.L166
	cmp	r7, #0
	beq	.L260
	cmp	r7, #1
	beq	.L228
	cmp	r7, #2
	beq	.L229
	add	fp, fp, r3
	mov	r7, fp, asr #12
	add	r4, r4, r7, asl #1
	mov	fp, fp, asl #20
	cmp	r5, r4
	add	r0, r0, #2
	mov	fp, fp, lsr #20
	sub	r2, r2, #1
	bls	.L236
.L229:
	add	fp, fp, r3
	mov	r7, fp, asr #12
	add	r4, r4, r7, asl #1
	mov	fp, fp, asl #20
	cmp	r5, r4
	add	r0, r0, #2
	mov	fp, fp, lsr #20
	sub	r2, r2, #1
	bls	.L236
.L228:
	add	fp, fp, r3
	mov	r7, fp, asr #12
	add	r4, r4, r7, asl #1
	mov	fp, fp, asl #20
	cmp	r5, r4
	add	r0, r0, #2
	mov	fp, fp, lsr #20
	sub	r2, r2, #1
	bls	.L236
	cmp	r2, #0
	beq	.L166
.L260:
	str	r6, [sp, #4]
	mov	r9, r8
.L178:
	add	r8, fp, r3
	mov	sl, r8, asr #12
	mov	r6, r8, asl #20
	add	r4, r4, sl, asl #1
	mov	fp, r6, lsr #20
	add	r0, r0, #2
	sub	r2, r2, #1
	add	r6, fp, r3
	cmp	r5, r4
	mov	sl, r6, asr #12
	mov	r7, r0
	mov	r8, r2
	bls	.L262
	mov	fp, r6, asl #20
	add	r4, r4, sl, asl #1
	mov	fp, fp, lsr #20
	add	r6, fp, r3
	cmp	r5, r4
	mov	sl, r6, asr #12
	add	r0, r0, #2
	sub	r2, r2, #1
	bls	.L262
	mov	r2, r6, asl #20
	add	r4, r4, sl, asl #1
	mov	fp, r2, lsr #20
	add	r6, fp, r3
	cmp	r5, r4
	mov	sl, r6, asr #12
	add	r0, r7, #4
	sub	r2, r8, #2
	bls	.L262
	add	r4, r4, sl, asl #1
	mov	fp, r6, asl #20
	cmp	r5, r4
	sub	r2, r8, #3
	add	r0, r7, #6
	mov	fp, fp, lsr #20
	bls	.L262
	cmp	r2, #0
	bne	.L178
.L166:
	rsb	r0, r1, r0
	str	fp, [ip, #12]
	str	r4, [ip, #0]
	mov	r0, r0, asr #1
.L158:
	add	sp, sp, #8
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
	bx	lr
.L170:
	cmp	r2, #0
	beq	.L162
	ldrsh	r8, [r4, #0]
	mul	r8, r6, r8
	add	r7, fp, r3
	mov	r8, r8, asr #8
	mov	fp, r7, asl #20
	strh	r8, [r0], #2	@ movhi
	mov	r7, r7, asr #12
	add	r4, r4, r7, asl #1
	mov	fp, fp, lsr #20
	sub	r2, r2, #1
.L237:
	cmp	r4, r5
	beq	.L170
	bhi	.L276
	cmp	r2, #0
	beq	.L166
	cmp	r6, #0
	beq	.L224
	ldrsh	r7, [r4, #0]
	ldrsh	r8, [r4, #2]
	rsb	r8, r7, r8
	mul	r8, fp, r8
	add	r8, r7, r8, asr #12
	mul	r8, r6, r8
	add	fp, fp, r3
	mov	r7, fp, asr #12
	add	r4, r4, r7, asl #1
	mov	r8, r8, asr #8
	sub	r7, r2, #1
	mov	fp, fp, asl #20
	cmp	r5, r4
	mov	r2, r7
	strh	r8, [r0], #2	@ movhi
	mov	fp, fp, lsr #20
	and	r7, r7, #3
	bls	.L237
	cmp	r2, #0
	beq	.L166
	cmp	r7, #0
	beq	.L265
	cmp	r7, #1
	beq	.L231
	cmp	r7, #2
	beq	.L232
	ldrsh	r7, [r4, #0]
	ldrsh	r8, [r4, #2]
	rsb	r8, r7, r8
	mul	r8, fp, r8
	add	r7, r7, r8, asr #12
	mul	r7, r6, r7
	add	fp, fp, r3
	mov	r8, fp, asr #12
	add	r4, r4, r8, asl #1
	mov	r7, r7, asr #8
	mov	fp, fp, asl #20
	cmp	r5, r4
	strh	r7, [r0], #2	@ movhi
	mov	fp, fp, lsr #20
	sub	r2, r2, #1
	bls	.L237
.L232:
	ldrsh	r7, [r4, #0]
	ldrsh	r8, [r4, #2]
	rsb	r8, r7, r8
	mul	r8, fp, r8
	add	r7, r7, r8, asr #12
	mul	r7, r6, r7
	add	fp, fp, r3
	mov	r8, fp, asr #12
	add	r4, r4, r8, asl #1
	mov	r7, r7, asr #8
	mov	fp, fp, asl #20
	cmp	r5, r4
	strh	r7, [r0], #2	@ movhi
	mov	fp, fp, lsr #20
	sub	r2, r2, #1
	bls	.L237
.L231:
	ldrsh	r7, [r4, #0]
	ldrsh	r8, [r4, #2]
	rsb	r8, r7, r8
	mul	r8, fp, r8
	add	r7, r7, r8, asr #12
	mul	r7, r6, r7
	add	fp, fp, r3
	mov	r8, fp, asr #12
	add	r4, r4, r8, asl #1
	mov	r7, r7, asr #8
	mov	fp, fp, asl #20
	cmp	r5, r4
	strh	r7, [r0], #2	@ movhi
	mov	fp, fp, lsr #20
	sub	r2, r2, #1
	bls	.L237
	cmp	r2, #0
	beq	.L166
.L265:
	str	ip, [sp, #0]
	str	r1, [sp, #4]
.L179:
	ldrsh	r9, [r4, #0]
	ldrsh	r8, [r4, #2]
	rsb	ip, r9, r8
	mul	ip, fp, ip
	add	r1, r9, ip, asr #12
	mul	r1, r6, r1
	add	r9, fp, r3
	mov	r8, r1, asr #8
	mov	ip, r9, asr #12
	mov	r1, r9, asl #20
	strh	r8, [r0], #2	@ movhi
	add	r4, r4, ip, asl #1
	mov	fp, r1, lsr #20
	sub	r2, r2, #1
	add	ip, fp, r3
	cmp	r5, r4
	mov	r9, ip, asr #12
	mov	r1, r0
	mov	r8, r2
	bls	.L273
	ldrsh	r7, [r4, #0]
	ldrsh	sl, [r4, #2]
	rsb	sl, r7, sl
	mul	fp, sl, fp
	add	r7, r7, fp, asr #12
	mul	r7, r6, r7
	mov	fp, ip, asl #20
	add	r4, r4, r9, asl #1
	mov	fp, fp, lsr #20
	mov	r7, r7, asr #8
	add	ip, fp, r3
	cmp	r5, r4
	strh	r7, [r0], #2	@ movhi
	sub	r2, r2, #1
	mov	r7, ip, asr #12
	bls	.L273
	ldrsh	r2, [r4, #0]
	ldrsh	sl, [r4, #2]
	rsb	sl, r2, sl
	mul	fp, sl, fp
	add	r2, r2, fp, asr #12
	mul	r2, r6, r2
	mov	fp, ip, asl #20
	add	r4, r4, r7, asl #1
	mov	fp, fp, lsr #20
	mov	r2, r2, asr #8
	add	ip, fp, r3
	cmp	r5, r4
	strh	r2, [r0, #0]	@ movhi
	mov	r7, ip, asr #12
	add	r0, r1, #4
	sub	r2, r8, #2
	bls	.L273
	ldrsh	r2, [r4, #0]
	ldrsh	r0, [r4, #2]
	rsb	r0, r2, r0
	mul	fp, r0, fp
	add	r2, r2, fp, asr #12
	mul	r2, r6, r2
	add	r4, r4, r7, asl #1
	mov	r0, r2, asr #8
	mov	fp, ip, asl #20
	cmp	r5, r4
	strh	r0, [r1, #4]	@ movhi
	sub	r2, r8, #3
	add	r0, r1, #6
	mov	fp, fp, lsr #20
	bls	.L273
	cmp	r2, #0
	bne	.L179
	ldr	ip, [sp, #0]
	ldr	r1, [sp, #4]
	b	.L166
.L275:
	ldrsh	r7, [r4, #0]
	ldrsh	r9, [r4, #2]
	rsb	sl, r7, r9
	mul	sl, fp, sl
	add	r7, r7, sl, asr #12
	mul	r7, r6, r7
	ldrsh	r9, [r0, #0]
	add	r7, r9, r7, asr #8
	add	sl, r7, #32768
	mov	r9, sl, lsr #16
	mov	sl, r9, asl #16
	cmp	sl, #0
	sub	r9, r2, #1
	and	r9, r9, #1
	beq	.L185
	ldr	sl, [sp, #0]
	cmp	r7, r8
	movlt	r7, r8
	cmp	r7, sl
	movge	r7, sl
.L185:
	add	fp, fp, r3
	mov	sl, fp, asr #12
	add	r4, r4, sl, asl #1
	mov	fp, fp, asl #20
	cmp	r5, r4
	strh	r7, [r0], #2	@ movhi
	mov	fp, fp, lsr #20
	sub	r2, r2, #1
	bls	.L236
	cmp	r2, #0
	beq	.L166
	cmp	r9, #0
	beq	.L256
	ldrsh	r7, [r4, #0]
	ldrsh	sl, [r4, #2]
	rsb	r9, r7, sl
	mul	r9, fp, r9
	add	sl, r7, r9, asr #12
	mul	sl, r6, sl
	ldrsh	r9, [r0, #0]
	add	sl, r9, sl, asr #8
	add	r7, sl, #32768
	mov	r7, r7, lsr #16
	mov	r7, r7, asl #16
	cmp	r7, #0
	beq	.L187
	ldr	r7, [sp, #0]
	cmp	sl, r8
	movlt	sl, r8
	cmp	sl, r7
	movge	sl, r7
.L187:
	add	fp, fp, r3
	mov	r7, fp, asr #12
	add	r4, r4, r7, asl #1
	mov	fp, fp, asl #20
	cmp	r5, r4
	strh	sl, [r0], #2	@ movhi
	mov	fp, fp, lsr #20
	sub	r2, r2, #1
	bls	.L236
	cmp	r2, #0
	beq	.L166
.L256:
	mov	r9, ip
.L177:
	ldrsh	ip, [r4, #0]
	ldrsh	r7, [r4, #2]
	rsb	sl, ip, r7
	mul	sl, fp, sl
	add	r7, ip, sl, asr #12
	mul	r7, r6, r7
	ldrsh	ip, [r0, #0]
	add	r7, ip, r7, asr #8
	add	sl, r7, #32768
	mov	ip, sl, lsr #16
	add	fp, fp, r3
	mov	ip, ip, asl #16
	mov	sl, fp, asr #12
	cmp	ip, #0
	add	r4, r4, sl, asl #1
	sub	r2, r2, #1
	mov	fp, fp, asl #20
	beq	.L168
	ldr	sl, [sp, #0]
	cmp	r7, r8
	movlt	r7, r8
	cmp	r7, sl
	movge	r7, sl
.L168:
	strh	r7, [r0], #2	@ movhi
	cmp	r5, r4
	mov	fp, fp, lsr #20
	mov	r7, r0
	mov	ip, r2
	bls	.L257
	ldrsh	r2, [r4, #0]
	ldrsh	sl, [r4, #2]
	rsb	sl, r2, sl
	mul	sl, fp, sl
	add	r2, r2, sl, asr #12
	mul	r2, r6, r2
	ldrsh	sl, [r0, #0]
	add	r2, sl, r2, asr #8
	add	r0, r2, #32768
	mov	r0, r0, lsr #16
	mov	r0, r0, asl #16
	cmp	r0, #0
	beq	.L190
	ldr	sl, [sp, #0]
	cmp	r2, r8
	movlt	r2, r8
	cmp	r2, sl
	movge	r2, sl
.L190:
	add	fp, fp, r3
	mov	r0, fp, asr #12
	add	r4, r4, r0, asl #1
	mov	fp, fp, asl #20
	mov	r0, r7
	cmp	r5, r4
	strh	r2, [r0], #2	@ movhi
	mov	fp, fp, lsr #20
	sub	r2, ip, #1
	bls	.L257
	cmp	r2, #0
	bne	.L177
	mov	ip, r9
	b	.L166
.L224:
	add	fp, fp, r3
	mov	r7, fp, asr #12
	add	r4, r4, r7, asl #1
	cmp	r5, r4
	sub	r7, r2, #1
	mov	r2, fp, asl #20
	mov	fp, r2, lsr #20
	strh	r6, [r0], #2	@ movhi
	mov	r2, r7
	and	r7, r7, #3
	bls	.L237
	cmp	r2, #0
	beq	.L166
	cmp	r7, #0
	beq	.L270
	cmp	r7, #1
	beq	.L234
	cmp	r7, #2
	beq	.L235
	add	fp, fp, r3
	mov	r7, fp, asr #12
	add	r4, r4, r7, asl #1
	mov	fp, fp, asl #20
	cmp	r5, r4
	strh	r6, [r0], #2	@ movhi
	mov	fp, fp, lsr #20
	sub	r2, r2, #1
	bls	.L237
.L235:
	add	fp, fp, r3
	mov	r7, fp, asr #12
	add	r4, r4, r7, asl #1
	mov	fp, fp, asl #20
	mov	r7, #0	@ movhi
	cmp	r5, r4
	strh	r7, [r0], #2	@ movhi
	mov	fp, fp, lsr #20
	sub	r2, r2, #1
	bls	.L237
.L234:
	add	sl, fp, r3
	mov	r7, sl, asr #12
	add	r4, r4, r7, asl #1
	mov	fp, sl, asl #20
	cmp	r5, r4
	mov	sl, #0	@ movhi
	strh	sl, [r0], #2	@ movhi
	mov	fp, fp, lsr #20
	sub	r2, r2, #1
	bls	.L237
	cmp	r2, #0
	beq	.L166
.L270:
	mov	r9, r6
.L180:
	add	r8, fp, r3
	mov	r7, r8, asr #12
	mov	r6, r8, asl #20
	mov	sl, #0	@ movhi
	add	r4, r4, r7, asl #1
	strh	sl, [r0], #2	@ movhi
	mov	fp, r6, lsr #20
	sub	r2, r2, #1
	add	r7, fp, r3
	cmp	r5, r4
	mov	sl, r7, asr #12
	mov	r6, r0
	mov	r8, r2
	bls	.L272
	mov	fp, r7, asl #20
	add	r4, r4, sl, asl #1
	mov	fp, fp, lsr #20
	mov	sl, #0	@ movhi
	add	r7, fp, r3
	cmp	r5, r4
	strh	sl, [r0], #2	@ movhi
	sub	r2, r2, #1
	mov	sl, r7, asr #12
	bls	.L272
	mov	fp, r7, asl #20
	add	r4, r4, sl, asl #1
	mov	fp, fp, lsr #20
	mov	r2, #0	@ movhi
	add	r7, fp, r3
	cmp	r5, r4
	strh	r2, [r0, #0]	@ movhi
	mov	sl, r7, asr #12
	add	r0, r6, #4
	sub	r2, r8, #2
	bls	.L272
	add	r4, r4, sl, asl #1
	mov	fp, r7, asl #20
	cmp	r5, r4
	mov	r7, #0	@ movhi
	strh	r7, [r6, #4]	@ movhi
	sub	r2, r8, #3
	add	r0, r6, #6
	mov	fp, fp, lsr #20
	bls	.L272
	cmp	r2, #0
	bne	.L180
	b	.L166
.L164:
	cmp	r2, #0
	beq	.L162
	ldrsh	r7, [r5, #0]
	mul	r7, r6, r7
	ldrsh	r4, [r0, #0]
	add	r7, r4, r7, asr #8
	add	r4, r7, #32768
	mov	r4, r4, lsr #16
	mov	r4, r4, asl #16
	cmp	r4, #0
	beq	.L163
	ldr	r4, [sp, #0]
	cmp	r7, r8
	movlt	r7, r8
	cmp	r7, r4
	movge	r7, r4
.L163:
	add	r4, fp, r3
	mov	fp, r4, asl #20
	mov	r4, r4, asr #12
	strh	r7, [r0], #2	@ movhi
	add	r4, r5, r4, asl #1
	mov	fp, fp, lsr #20
	sub	r2, r2, #1
	b	.L236
.L257:
	mov	ip, r9
	b	.L236
.L262:
	ldr	r6, [sp, #4]
	mov	r8, r9
	b	.L236
.L273:
	ldr	ip, [sp, #0]
	ldr	r1, [sp, #4]
	b	.L237
.L272:
	mov	r6, r9
	b	.L237
.L274:
	ldr	r6, [sp, #48]
	mov	r3, #1
	str	r3, [r6, #0]
	b	.L166
.L276:
	ldr	r2, [sp, #48]
	mov	r3, #1
	str	r3, [r2, #0]
	b	.L166
.L162:
	mov	r4, r5
	b	.L166
	.cantunwind
	.fnend
	.size	_ZN13CIwChannelPCMIsE17GenerateAudioFastEPsiiiiPi, .-_ZN13CIwChannelPCMIsE17GenerateAudioFastEPsiiiiPi
	.section	.text._ZN15CIwSoundManager9ResumeAllEv,"ax",%progbits
	.align	2
	.global	_ZN15CIwSoundManager9ResumeAllEv
	.hidden	_ZN15CIwSoundManager9ResumeAllEv
	.type	_ZN15CIwSoundManager9ResumeAllEv, %function
_ZN15CIwSoundManager9ResumeAllEv:
	.fnstart
.LFB1391:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	ldr	r3, [r0, #20]
	cmp	r3, #0
	mov	r5, r0
	beq	.L280
	mov	r4, #0
.L279:
	ldr	r1, [r5, #12]
	ldr	r0, [r1, r4, asl #2]
	bl	_ZN12CIwSoundInst6ResumeEv
	ldr	r0, [r5, #20]
	add	r4, r4, #1
	cmp	r0, r4
	bhi	.L279
.L280:
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
	.fnend
	.size	_ZN15CIwSoundManager9ResumeAllEv, .-_ZN15CIwSoundManager9ResumeAllEv
	.section	.text._ZN15CIwSoundManager8PauseAllEv,"ax",%progbits
	.align	2
	.global	_ZN15CIwSoundManager8PauseAllEv
	.hidden	_ZN15CIwSoundManager8PauseAllEv
	.type	_ZN15CIwSoundManager8PauseAllEv, %function
_ZN15CIwSoundManager8PauseAllEv:
	.fnstart
.LFB1390:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	ldr	r3, [r0, #20]
	cmp	r3, #0
	mov	r5, r0
	beq	.L285
	mov	r4, #0
.L284:
	ldr	r1, [r5, #12]
	ldr	r0, [r1, r4, asl #2]
	bl	_ZN12CIwSoundInst5PauseEv
	ldr	r0, [r5, #20]
	add	r4, r4, #1
	cmp	r0, r4
	bhi	.L284
.L285:
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
	.fnend
	.size	_ZN15CIwSoundManager8PauseAllEv, .-_ZN15CIwSoundManager8PauseAllEv
	.section	.text._ZN15CIwSoundManager14GetFreeChannelEP12CIwSoundInst17IwSoundDataFormat,"ax",%progbits
	.align	2
	.global	_ZN15CIwSoundManager14GetFreeChannelEP12CIwSoundInst17IwSoundDataFormat
	.hidden	_ZN15CIwSoundManager14GetFreeChannelEP12CIwSoundInst17IwSoundDataFormat
	.type	_ZN15CIwSoundManager14GetFreeChannelEP12CIwSoundInst17IwSoundDataFormat, %function
_ZN15CIwSoundManager14GetFreeChannelEP12CIwSoundInst17IwSoundDataFormat:
	.fnstart
.LFB1385:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, lr}
	.save {r3, r4, r5, r6, r7, r8, sl, lr}
	ldrh	r3, [r0, #30]
	tst	r3, #2
	mov	r7, r0
	mov	r6, r1
	mov	r5, r2
	bne	.L288
.L295:
	mvn	r4, #0
.L289:
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, lr}
	bx	lr
.L288:
	bl	s3eSoundGetFreeChannel
	ldr	r1, [r7, #16]
	cmp	r0, r1
	mov	r4, r0
	bge	.L295
	cmp	r5, #1
	beq	.L292
	cmp	r5, #2
	beq	.L293
	cmp	r5, #0
	bne	.L289
	ldr	r2, .L296
	ldr	sl, [r2, #0]
	add	r8, r0, r0, asl #1
	add	r7, sl, r8, asl #3
	mov	r1, #1
	ldr	r2, .L296+4
	mov	r3, r7
	bl	s3eSoundChannelRegister
	str	r5, [sl, r8, asl #3]
	str	r6, [r7, #20]
	b	.L289
.L292:
	ldr	r7, .L296
	ldr	r8, [r7, #4]
	add	r7, r0, r0, asl #1
	add	sl, r8, r7, asl #3
	mov	r1, r5
	ldr	r2, .L296+8
	mov	r3, sl
	bl	s3eSoundChannelRegister
	mov	r0, #0
	str	r0, [r8, r7, asl #3]
	str	r6, [sl, #20]
	b	.L289
.L293:
	ldr	r3, .L296
	add	lr, r0, r0, asl #2
	ldr	r5, [r3, #8]
	add	r6, lr, lr, asl #5
	add	r6, r0, r6, asl #1
	mov	r1, #1
	ldr	r2, .L296+12
	add	r3, r5, r6, asl #2
	bl	s3eSoundChannelRegister
	mov	ip, #0
	str	ip, [r5, r6, asl #2]
	b	.L289
.L297:
	.align	2
.L296:
	.word	.LANCHOR0
	.word	_ZN13CIwChannelPCMIaE15GenerateAudioCBEP20s3eSoundGenAudioInfoPS0_
	.word	_ZN13CIwChannelPCMIsE15GenerateAudioCBEP20s3eSoundGenAudioInfoPS0_
	.word	_ZN15CIwChannelADPCM15GenerateAudioCBEP20s3eSoundGenAudioInfoPS_
	.fnend
	.size	_ZN15CIwSoundManager14GetFreeChannelEP12CIwSoundInst17IwSoundDataFormat, .-_ZN15CIwSoundManager14GetFreeChannelEP12CIwSoundInst17IwSoundDataFormat
	.section	.text._ZN15CIwChannelADPCM15GenerateAudioCBEP20s3eSoundGenAudioInfoPS_,"axG",%progbits,_ZN15CIwChannelADPCM15GenerateAudioCBEP20s3eSoundGenAudioInfoPS_,comdat
	.align	2
	.weak	_ZN15CIwChannelADPCM15GenerateAudioCBEP20s3eSoundGenAudioInfoPS_
	.hidden	_ZN15CIwChannelADPCM15GenerateAudioCBEP20s3eSoundGenAudioInfoPS_
	.type	_ZN15CIwChannelADPCM15GenerateAudioCBEP20s3eSoundGenAudioInfoPS_, %function
_ZN15CIwChannelADPCM15GenerateAudioCBEP20s3eSoundGenAudioInfoPS_:
	.fnstart
.LFB413:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, lr}
	.save {r3, lr}
	mov	r3, r0
	mov	r0, r1
	mov	r1, r3
	bl	_ZN15CIwChannelADPCM13GenerateAudioEP20s3eSoundGenAudioInfo
	ldmfd	sp!, {r3, lr}
	bx	lr
	.fnend
	.size	_ZN15CIwChannelADPCM15GenerateAudioCBEP20s3eSoundGenAudioInfoPS_, .-_ZN15CIwChannelADPCM15GenerateAudioCBEP20s3eSoundGenAudioInfoPS_
	.global	__aeabi_idiv
	.section	.text._ZN13CIwChannelPCMIsE13GenerateAudioEP20s3eSoundGenAudioInfo,"axG",%progbits,_ZN13CIwChannelPCMIsE13GenerateAudioEP20s3eSoundGenAudioInfo,comdat
	.align	2
	.weak	_ZN13CIwChannelPCMIsE13GenerateAudioEP20s3eSoundGenAudioInfo
	.hidden	_ZN13CIwChannelPCMIsE13GenerateAudioEP20s3eSoundGenAudioInfo
	.type	_ZN13CIwChannelPCMIsE13GenerateAudioEP20s3eSoundGenAudioInfo, %function
_ZN13CIwChannelPCMIsE13GenerateAudioEP20s3eSoundGenAudioInfo:
	.fnstart
.LFB1500:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	r3, [r0, #0]
	cmp	r3, #0
	.pad #28
	sub	sp, sp, #28
	mov	r4, r0
	mov	r6, r1
	ldmib	r1, {r7, r8, r9}	@ phole ldm
	beq	.L346
.L301:
	mov	r1, #1
	ldr	r0, [r6, #0]
	bl	s3eSoundChannelGetInt
	mov	sl, r0
	mov	r0, #1
	bl	s3eSoundGetInt
	mov	r1, r0
	mov	r0, sl, asl #12
	bl	__aeabi_idiv
	mov	r1, #3
	mov	sl, r0
	ldr	r0, [r6, #0]
	bl	s3eSoundChannelGetInt
	cmp	sl, #4096
	mov	r5, r0
	beq	.L302
	add	ip, sp, #24
	mov	fp, #0
	str	fp, [ip, #-4]!
	mov	r3, sl
	mov	r1, r7
	mov	r2, r8
	mov	r0, r4
	stmia	sp, {r5, r9, ip}	@ phole stm
	bl	_ZN13CIwChannelPCMIsE17GenerateAudioFastEPsiiiiPi
	ldr	r3, [sp, #20]
	cmp	r3, fp
	movne	r3, #1
	strneb	r3, [r6, #28]
	strne	fp, [r4, #0]
.L303:
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L302:
	ldr	r2, [r4, #8]
	ldr	r3, [r4, #0]
	b	.L313
.L349:
	cmp	r9, #0
	mov	sl, sl, asl #1
	beq	.L305
.L306:
	add	r3, r3, sl
	str	r3, [r4, #0]
	add	r7, r7, sl
.L307:
	cmp	r3, r2
	beq	.L347
.L312:
	cmp	r8, #0
	beq	.L348
.L313:
	rsb	lr, r3, r2
	mov	sl, lr, asr #1
	cmp	sl, r8
	movge	sl, r8
	cmp	r5, #0
	rsb	r8, sl, r8
	beq	.L349
	cmp	r9, #0
	bne	.L308
	cmp	sl, #0
	beq	.L307
	ldrsh	lr, [r3, #0]
	mul	lr, r5, lr
	mov	r1, lr, asr #8
	strh	r1, [r7, #0]	@ movhi
	ldr	r0, [r4, #0]
	sub	r2, sl, #1
	subs	r1, r2, #0
	add	r3, r0, #2
	and	ip, r2, #3
	str	r3, [r4, #0]
	mov	r2, #2
	beq	.L345
	cmp	ip, #0
	beq	.L311
	cmp	ip, #1
	beq	.L342
	cmp	ip, #2
	beq	.L343
	ldrsh	ip, [r0, #2]
	mul	ip, r5, ip
	mov	r3, ip, asr #8
	strh	r3, [r7, r2]	@ movhi
	ldr	r2, [r4, #0]
	add	r3, r2, #2
	str	r3, [r4, #0]
	sub	r1, r1, #1
	mov	r2, #4
.L343:
	ldrsh	r3, [r3, #0]
	mul	r3, r5, r3
	mov	r0, r3, asr #8
	strh	r0, [r7, r2]	@ movhi
	ldr	lr, [r4, #0]
	add	r3, lr, #2
	str	r3, [r4, #0]
	sub	r1, r1, #1
	add	r2, r2, #2
.L342:
	ldrsh	r0, [r3, #0]
	mul	r0, r5, r0
	mov	lr, r0, asr #8
	strh	lr, [r7, r2]	@ movhi
	ldr	ip, [r4, #0]
	subs	r1, r1, #1
	add	r3, ip, #2
	str	r3, [r4, #0]
	add	r2, r2, #2
	beq	.L345
.L311:
	ldrsh	lr, [r3, #0]
	mul	lr, r5, lr
	mov	ip, lr, asr #8
	strh	ip, [r7, r2]	@ movhi
	ldr	r3, [r4, #0]
	add	r0, r3, #2
	str	r0, [r4, #0]
	ldrsh	lr, [r3, #2]
	mul	lr, r5, lr
	mov	ip, lr, asr #8
	add	lr, r2, #2
	strh	ip, [r7, lr]	@ movhi
	ldr	r0, [r4, #0]
	add	r3, r0, #2
	str	r3, [r4, #0]
	ldrsh	ip, [r0, #2]
	mul	ip, r5, ip
	add	r3, lr, #2
	mov	lr, ip, asr #8
	strh	lr, [r7, r3]	@ movhi
	ldr	ip, [r4, #0]
	add	r0, ip, #2
	str	r0, [r4, #0]
	ldrsh	lr, [ip, #2]
	mul	lr, r5, lr
	add	r3, r2, #6
	mov	r0, lr, asr #8
	strh	r0, [r7, r3]	@ movhi
	ldr	ip, [r4, #0]
	subs	r1, r1, #4
	add	r3, ip, #2
	str	r3, [r4, #0]
	add	r2, r2, #8
	bne	.L311
.L345:
	ldr	r2, [r4, #8]
	cmp	r3, r2
	add	r7, r7, sl, asl #1
	bne	.L312
.L347:
	mov	r3, #0
	str	r3, [r4, #0]
	ldr	r2, [r6, #8]
	mov	r1, #1
	strb	r1, [r6, #28]
	rsb	r0, r8, r2
	b	.L303
.L308:
	cmp	sl, #0
	beq	.L307
	ldrsh	r1, [r3, #0]
	mul	r1, r5, r1
	ldrh	ip, [r7, #0]
	add	r0, ip, r1, asr #8
	strh	r0, [r7, #0]	@ movhi
	ldr	r0, [r4, #0]
	sub	r2, sl, #1
	subs	r1, r2, #0
	add	r3, r0, #2
	and	ip, r2, #3
	str	r3, [r4, #0]
	mov	r2, #2
	beq	.L344
	cmp	ip, #0
	beq	.L310
	cmp	ip, #1
	beq	.L340
	cmp	ip, #2
	beq	.L341
	ldrsh	lr, [r0, #2]
	mul	lr, r5, lr
	ldrh	r3, [r7, r2]
	add	ip, r3, lr, asr #8
	strh	ip, [r7, r2]	@ movhi
	ldr	r0, [r4, #0]
	add	r3, r0, #2
	str	r3, [r4, #0]
	sub	r1, r1, #1
	mov	r2, #4
.L341:
	ldrsh	lr, [r3, #0]
	mul	lr, r5, lr
	ldrh	r0, [r7, r2]
	add	ip, r0, lr, asr #8
	strh	ip, [r7, r2]	@ movhi
	ldr	r0, [r4, #0]
	add	r3, r0, #2
	str	r3, [r4, #0]
	sub	r1, r1, #1
	add	r2, r2, #2
.L340:
	ldrsh	lr, [r3, #0]
	mul	lr, r5, lr
	ldrh	r0, [r7, r2]
	add	ip, r0, lr, asr #8
	strh	ip, [r7, r2]	@ movhi
	ldr	r3, [r4, #0]
	subs	r1, r1, #1
	add	r3, r3, #2
	str	r3, [r4, #0]
	add	r2, r2, #2
	beq	.L344
.L310:
	ldrsh	r3, [r3, #0]
	mul	r3, r5, r3
	ldrh	r0, [r7, r2]
	add	ip, r0, r3, asr #8
	strh	ip, [r7, r2]	@ movhi
	ldr	r3, [r4, #0]
	add	lr, r3, #2
	str	lr, [r4, #0]
	ldrsh	r0, [r3, #2]
	mul	r0, r5, r0
	add	r3, r2, #2
	ldrh	ip, [r7, r3]
	add	lr, ip, r0, asr #8
	strh	lr, [r7, r3]	@ movhi
	ldr	lr, [r4, #0]
	add	r0, lr, #2
	str	r0, [r4, #0]
	ldrsh	r0, [lr, #2]
	mul	r0, r5, r0
	add	lr, r3, #2
	ldrh	ip, [r7, lr]
	add	r0, ip, r0, asr #8
	strh	r0, [r7, lr]	@ movhi
	ldr	lr, [r4, #0]
	add	r3, lr, #2
	str	r3, [r4, #0]
	ldrsh	r0, [lr, #2]
	mul	r0, r5, r0
	add	r3, r2, #6
	ldrh	ip, [r7, r3]
	add	lr, ip, r0, asr #8
	strh	lr, [r7, r3]	@ movhi
	ldr	r3, [r4, #0]
	subs	r1, r1, #4
	add	r3, r3, #2
	str	r3, [r4, #0]
	add	r2, r2, #8
	bne	.L310
.L344:
	add	r7, r7, sl, asl #1
	ldr	r2, [r4, #8]
	b	.L307
.L305:
	mov	r2, sl
	mov	r0, r7
	mov	r1, r9
	bl	memset
	ldr	r3, [r4, #0]
	ldr	r2, [r4, #8]
	b	.L306
.L348:
	ldr	r0, [r6, #8]
	b	.L303
.L346:
	ldr	r5, [r1, #16]
	str	r5, [r0, #0]
	ldr	r1, [r1, #20]
	ldr	r0, [r6, #16]
	add	r2, r0, r1, asl #1
	str	r2, [r4, #8]
	str	r3, [r4, #12]
	b	.L301
	.fnend
	.size	_ZN13CIwChannelPCMIsE13GenerateAudioEP20s3eSoundGenAudioInfo, .-_ZN13CIwChannelPCMIsE13GenerateAudioEP20s3eSoundGenAudioInfo
	.section	.text._ZN13CIwChannelPCMIsE15GenerateAudioCBEP20s3eSoundGenAudioInfoPS0_,"axG",%progbits,_ZN13CIwChannelPCMIsE15GenerateAudioCBEP20s3eSoundGenAudioInfoPS0_,comdat
	.align	2
	.weak	_ZN13CIwChannelPCMIsE15GenerateAudioCBEP20s3eSoundGenAudioInfoPS0_
	.hidden	_ZN13CIwChannelPCMIsE15GenerateAudioCBEP20s3eSoundGenAudioInfoPS0_
	.type	_ZN13CIwChannelPCMIsE15GenerateAudioCBEP20s3eSoundGenAudioInfoPS0_, %function
_ZN13CIwChannelPCMIsE15GenerateAudioCBEP20s3eSoundGenAudioInfoPS0_:
	.fnstart
.LFB1466:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, r0
	mov	r0, r1
	mov	r1, r3
	b	_ZN13CIwChannelPCMIsE13GenerateAudioEP20s3eSoundGenAudioInfo
	.cantunwind
	.fnend
	.size	_ZN13CIwChannelPCMIsE15GenerateAudioCBEP20s3eSoundGenAudioInfoPS0_, .-_ZN13CIwChannelPCMIsE15GenerateAudioCBEP20s3eSoundGenAudioInfoPS0_
	.section	.text._ZN13CIwChannelPCMIaE13GenerateAudioEP20s3eSoundGenAudioInfo,"axG",%progbits,_ZN13CIwChannelPCMIaE13GenerateAudioEP20s3eSoundGenAudioInfo,comdat
	.align	2
	.weak	_ZN13CIwChannelPCMIaE13GenerateAudioEP20s3eSoundGenAudioInfo
	.hidden	_ZN13CIwChannelPCMIaE13GenerateAudioEP20s3eSoundGenAudioInfo
	.type	_ZN13CIwChannelPCMIaE13GenerateAudioEP20s3eSoundGenAudioInfo, %function
_ZN13CIwChannelPCMIaE13GenerateAudioEP20s3eSoundGenAudioInfo:
	.fnstart
.LFB1499:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	r3, [r0, #0]
	cmp	r3, #0
	.pad #28
	sub	sp, sp, #28
	mov	r4, r0
	mov	r6, r1
	ldmib	r1, {r7, r8, r9}	@ phole ldm
	beq	.L398
.L353:
	mov	r1, #1
	ldr	r0, [r6, #0]
	bl	s3eSoundChannelGetInt
	mov	sl, r0
	mov	r0, #1
	bl	s3eSoundGetInt
	mov	r1, r0
	mov	r0, sl, asl #12
	bl	__aeabi_idiv
	mov	r1, #3
	mov	sl, r0
	ldr	r0, [r6, #0]
	bl	s3eSoundChannelGetInt
	cmp	sl, #4096
	mov	r5, r0
	beq	.L354
	add	ip, sp, #24
	mov	fp, #0
	str	fp, [ip, #-4]!
	mov	r3, sl
	mov	r1, r7
	mov	r2, r8
	mov	r0, r4
	stmia	sp, {r5, r9, ip}	@ phole stm
	bl	_ZN13CIwChannelPCMIaE17GenerateAudioFastEPsiiiiPi
	ldr	r3, [sp, #20]
	cmp	r3, fp
	movne	r3, #1
	strneb	r3, [r6, #28]
	strne	fp, [r4, #0]
.L355:
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L354:
	ldr	r2, [r4, #8]
	ldr	r3, [r4, #0]
	b	.L365
.L401:
	cmp	r9, #0
	mov	fp, sl, asl #1
	beq	.L357
.L358:
	add	r3, r3, sl
	str	r3, [r4, #0]
	add	r7, r7, fp
.L359:
	cmp	r3, r2
	beq	.L399
.L364:
	cmp	r8, #0
	beq	.L400
.L365:
	rsb	sl, r3, r2
	cmp	sl, r8
	movge	sl, r8
	cmp	r5, #0
	rsb	r8, sl, r8
	beq	.L401
	cmp	r9, #0
	bne	.L360
	cmp	sl, #0
	beq	.L359
	ldrsb	lr, [r3, #0]
	mul	lr, r5, lr
	mov	r1, lr, asr #8
	strh	r1, [r7, #0]	@ movhi
	ldr	r0, [r4, #0]
	sub	r2, sl, #1
	subs	r1, r2, #0
	add	r3, r0, #1
	and	ip, r2, #3
	str	r3, [r4, #0]
	mov	r2, #2
	beq	.L397
	cmp	ip, #0
	beq	.L363
	cmp	ip, #1
	beq	.L394
	cmp	ip, #2
	beq	.L395
	ldrsb	ip, [r0, #1]
	mul	ip, r5, ip
	mov	r3, ip, asr #8
	strh	r3, [r7, r2]	@ movhi
	ldr	r2, [r4, #0]
	add	r3, r2, #1
	str	r3, [r4, #0]
	sub	r1, r1, #1
	mov	r2, #4
.L395:
	ldrsb	r3, [r3, #0]
	mul	r3, r5, r3
	mov	r0, r3, asr #8
	strh	r0, [r7, r2]	@ movhi
	ldr	lr, [r4, #0]
	add	r3, lr, #1
	str	r3, [r4, #0]
	sub	r1, r1, #1
	add	r2, r2, #2
.L394:
	ldrsb	r0, [r3, #0]
	mul	r0, r5, r0
	mov	lr, r0, asr #8
	strh	lr, [r7, r2]	@ movhi
	ldr	ip, [r4, #0]
	subs	r1, r1, #1
	add	r3, ip, #1
	str	r3, [r4, #0]
	add	r2, r2, #2
	beq	.L397
.L363:
	ldrsb	lr, [r3, #0]
	mul	lr, r5, lr
	mov	ip, lr, asr #8
	strh	ip, [r7, r2]	@ movhi
	ldr	r3, [r4, #0]
	add	r0, r3, #1
	str	r0, [r4, #0]
	ldrsb	lr, [r3, #1]
	mul	lr, r5, lr
	mov	ip, lr, asr #8
	add	lr, r2, #2
	strh	ip, [r7, lr]	@ movhi
	ldr	r0, [r4, #0]
	add	r3, r0, #1
	str	r3, [r4, #0]
	ldrsb	ip, [r0, #1]
	mul	ip, r5, ip
	add	r3, lr, #2
	mov	lr, ip, asr #8
	strh	lr, [r7, r3]	@ movhi
	ldr	ip, [r4, #0]
	add	r0, ip, #1
	str	r0, [r4, #0]
	ldrsb	lr, [ip, #1]
	mul	lr, r5, lr
	add	r3, r2, #6
	mov	r0, lr, asr #8
	strh	r0, [r7, r3]	@ movhi
	ldr	ip, [r4, #0]
	subs	r1, r1, #4
	add	r3, ip, #1
	str	r3, [r4, #0]
	add	r2, r2, #8
	bne	.L363
.L397:
	ldr	r2, [r4, #8]
	cmp	r3, r2
	add	r7, r7, sl, asl #1
	bne	.L364
.L399:
	mov	r3, #0
	str	r3, [r4, #0]
	ldr	r2, [r6, #8]
	mov	r1, #1
	strb	r1, [r6, #28]
	rsb	r0, r8, r2
	b	.L355
.L360:
	cmp	sl, #0
	beq	.L359
	ldrsb	r1, [r3, #0]
	mul	r1, r5, r1
	ldrh	r2, [r7, #0]
	add	r0, r2, r1, asr #8
	strh	r0, [r7, #0]	@ movhi
	ldr	r0, [r4, #0]
	sub	lr, sl, #1
	add	r3, r0, #1
	subs	r1, lr, #0
	str	r3, [r4, #0]
	and	ip, lr, #3
	mov	r2, #2
	beq	.L396
	cmp	ip, #0
	beq	.L362
	cmp	ip, #1
	beq	.L392
	cmp	ip, #2
	beq	.L393
	ldrsb	lr, [r0, #1]
	mul	lr, r5, lr
	ldrh	r3, [r7, r2]
	add	ip, r3, lr, asr #8
	strh	ip, [r7, r2]	@ movhi
	ldr	r0, [r4, #0]
	add	r3, r0, #1
	str	r3, [r4, #0]
	sub	r1, r1, #1
	mov	r2, #4
.L393:
	ldrsb	lr, [r3, #0]
	mul	lr, r5, lr
	ldrh	r0, [r7, r2]
	add	ip, r0, lr, asr #8
	strh	ip, [r7, r2]	@ movhi
	ldr	r0, [r4, #0]
	add	r3, r0, #1
	str	r3, [r4, #0]
	sub	r1, r1, #1
	add	r2, r2, #2
.L392:
	ldrsb	lr, [r3, #0]
	mul	lr, r5, lr
	ldrh	r0, [r7, r2]
	add	ip, r0, lr, asr #8
	strh	ip, [r7, r2]	@ movhi
	ldr	r3, [r4, #0]
	subs	r1, r1, #1
	add	r3, r3, #1
	str	r3, [r4, #0]
	add	r2, r2, #2
	beq	.L396
.L362:
	ldrsb	r3, [r3, #0]
	mul	r3, r5, r3
	ldrh	r0, [r7, r2]
	add	ip, r0, r3, asr #8
	strh	ip, [r7, r2]	@ movhi
	ldr	r3, [r4, #0]
	add	lr, r3, #1
	str	lr, [r4, #0]
	ldrsb	r0, [r3, #1]
	mul	r0, r5, r0
	add	r3, r2, #2
	ldrh	ip, [r7, r3]
	add	lr, ip, r0, asr #8
	strh	lr, [r7, r3]	@ movhi
	ldr	lr, [r4, #0]
	add	r0, lr, #1
	str	r0, [r4, #0]
	ldrsb	r0, [lr, #1]
	mul	r0, r5, r0
	add	lr, r3, #2
	ldrh	ip, [r7, lr]
	add	r0, ip, r0, asr #8
	strh	r0, [r7, lr]	@ movhi
	ldr	lr, [r4, #0]
	add	r3, lr, #1
	str	r3, [r4, #0]
	ldrsb	r0, [lr, #1]
	mul	r0, r5, r0
	add	r3, r2, #6
	ldrh	ip, [r7, r3]
	add	lr, ip, r0, asr #8
	strh	lr, [r7, r3]	@ movhi
	ldr	r3, [r4, #0]
	subs	r1, r1, #4
	add	r3, r3, #1
	str	r3, [r4, #0]
	add	r2, r2, #8
	bne	.L362
.L396:
	add	r7, r7, sl, asl #1
	ldr	r2, [r4, #8]
	b	.L359
.L357:
	mov	r2, fp
	mov	r0, r7
	mov	r1, r9
	bl	memset
	ldr	r3, [r4, #0]
	ldr	r2, [r4, #8]
	b	.L358
.L400:
	ldr	r0, [r6, #8]
	b	.L355
.L398:
	ldr	r5, [r1, #16]
	str	r5, [r0, #0]
	ldr	r1, [r1, #20]
	ldr	r0, [r6, #16]
	add	r2, r0, r1, asl #1
	str	r2, [r4, #8]
	str	r3, [r4, #12]
	b	.L353
	.fnend
	.size	_ZN13CIwChannelPCMIaE13GenerateAudioEP20s3eSoundGenAudioInfo, .-_ZN13CIwChannelPCMIaE13GenerateAudioEP20s3eSoundGenAudioInfo
	.section	.text._ZN13CIwChannelPCMIaE15GenerateAudioCBEP20s3eSoundGenAudioInfoPS0_,"axG",%progbits,_ZN13CIwChannelPCMIaE15GenerateAudioCBEP20s3eSoundGenAudioInfoPS0_,comdat
	.align	2
	.weak	_ZN13CIwChannelPCMIaE15GenerateAudioCBEP20s3eSoundGenAudioInfoPS0_
	.hidden	_ZN13CIwChannelPCMIaE15GenerateAudioCBEP20s3eSoundGenAudioInfoPS0_
	.type	_ZN13CIwChannelPCMIaE15GenerateAudioCBEP20s3eSoundGenAudioInfoPS0_, %function
_ZN13CIwChannelPCMIaE15GenerateAudioCBEP20s3eSoundGenAudioInfoPS0_:
	.fnstart
.LFB1465:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r3, r0
	mov	r0, r1
	mov	r1, r3
	b	_ZN13CIwChannelPCMIaE13GenerateAudioEP20s3eSoundGenAudioInfo
	.cantunwind
	.fnend
	.size	_ZN13CIwChannelPCMIaE15GenerateAudioCBEP20s3eSoundGenAudioInfoPS0_, .-_ZN13CIwChannelPCMIaE15GenerateAudioCBEP20s3eSoundGenAudioInfoPS0_
	.section	.text._ZN15CIwSoundManagerD0Ev,"ax",%progbits
	.align	2
	.global	_ZN15CIwSoundManagerD0Ev
	.hidden	_ZN15CIwSoundManagerD0Ev
	.type	_ZN15CIwSoundManagerD0Ev, %function
_ZN15CIwSoundManagerD0Ev:
	.fnstart
.LFB1381:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	mov	r4, r0
	ldr	r5, .L412
	ldr	r0, .L412+4
	mov	r3, #0
	str	r0, [r4, #0]
	str	r3, [r5, #12]
	ldr	r3, [r4, #32]
	cmp	r3, #0
	movne	r0, r3
	ldrne	r1, [r3, #0]
	ldrne	ip, [r1, #4]
	movne	lr, pc
	bxne	ip
.L405:
	ldr	r0, [r4, #36]
	bl	_ZdlPv
	ldr	r0, [r4, #8]
	cmp	r0, #0
	blne	_ZdaPv
.L406:
	ldr	r0, [r4, #12]
	cmp	r0, #0
	blne	_ZdaPv
.L407:
	ldr	r0, [r5, #0]
	cmp	r0, #0
	blne	_ZdaPv
.L408:
	ldr	r0, [r5, #4]
	cmp	r0, #0
	blne	_ZdaPv
.L409:
	ldr	r0, [r5, #8]
	cmp	r0, #0
	blne	_ZdaPv
.L410:
	mov	r0, r4
	bl	_ZdlPv
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L413:
	.align	2
.L412:
	.word	.LANCHOR0
	.word	.LANCHOR1+8
	.fnend
	.size	_ZN15CIwSoundManagerD0Ev, .-_ZN15CIwSoundManagerD0Ev
	.section	.text._ZN15CIwSoundManagerD1Ev,"ax",%progbits
	.align	2
	.global	_ZN15CIwSoundManagerD1Ev
	.hidden	_ZN15CIwSoundManagerD1Ev
	.type	_ZN15CIwSoundManagerD1Ev, %function
_ZN15CIwSoundManagerD1Ev:
	.fnstart
.LFB1380:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	mov	r4, r0
	ldr	r5, .L422
	ldr	r0, .L422+4
	mov	r3, #0
	str	r0, [r4, #0]
	str	r3, [r5, #12]
	ldr	r3, [r4, #32]
	cmp	r3, #0
	movne	r0, r3
	ldrne	r1, [r3, #0]
	ldrne	ip, [r1, #4]
	movne	lr, pc
	bxne	ip
.L415:
	ldr	r0, [r4, #36]
	bl	_ZdlPv
	ldr	r0, [r4, #8]
	cmp	r0, #0
	blne	_ZdaPv
.L416:
	ldr	r0, [r4, #12]
	cmp	r0, #0
	blne	_ZdaPv
.L417:
	ldr	r0, [r5, #0]
	cmp	r0, #0
	blne	_ZdaPv
.L418:
	ldr	r0, [r5, #4]
	cmp	r0, #0
	blne	_ZdaPv
.L419:
	ldr	r0, [r5, #8]
	cmp	r0, #0
	blne	_ZdaPv
.L420:
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L423:
	.align	2
.L422:
	.word	.LANCHOR0
	.word	.LANCHOR1+8
	.fnend
	.size	_ZN15CIwSoundManagerD1Ev, .-_ZN15CIwSoundManagerD1Ev
	.section	.text._ZN15CIwSoundManagerD2Ev,"ax",%progbits
	.align	2
	.global	_ZN15CIwSoundManagerD2Ev
	.hidden	_ZN15CIwSoundManagerD2Ev
	.type	_ZN15CIwSoundManagerD2Ev, %function
_ZN15CIwSoundManagerD2Ev:
	.fnstart
.LFB1379:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	mov	r4, r0
	ldr	r5, .L432
	ldr	r0, .L432+4
	mov	r3, #0
	str	r0, [r4, #0]
	str	r3, [r5, #12]
	ldr	r3, [r4, #32]
	cmp	r3, #0
	movne	r0, r3
	ldrne	r1, [r3, #0]
	ldrne	ip, [r1, #4]
	movne	lr, pc
	bxne	ip
.L425:
	ldr	r0, [r4, #36]
	bl	_ZdlPv
	ldr	r0, [r4, #8]
	cmp	r0, #0
	blne	_ZdaPv
.L426:
	ldr	r0, [r4, #12]
	cmp	r0, #0
	blne	_ZdaPv
.L427:
	ldr	r0, [r5, #0]
	cmp	r0, #0
	blne	_ZdaPv
.L428:
	ldr	r0, [r5, #4]
	cmp	r0, #0
	blne	_ZdaPv
.L429:
	ldr	r0, [r5, #8]
	cmp	r0, #0
	blne	_ZdaPv
.L430:
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L433:
	.align	2
.L432:
	.word	.LANCHOR0
	.word	.LANCHOR1+8
	.fnend
	.size	_ZN15CIwSoundManagerD2Ev, .-_ZN15CIwSoundManagerD2Ev
	.global	__cxa_end_cleanup
	.section	.text._ZN15CIwSoundManagerC1Ev,"ax",%progbits
	.align	2
	.global	_ZN15CIwSoundManagerC1Ev
	.hidden	_ZN15CIwSoundManagerC1Ev
	.type	_ZN15CIwSoundManagerC1Ev, %function
_ZN15CIwSoundManagerC1Ev:
	.fnstart
.LFB1373:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	r3, .L534
	ldr	r9, .L534+4
	mov	r4, r0
	str	r3, [r0, #0]
	.pad #12
	sub	sp, sp, #12
	mov	r0, #32
	str	r4, [r9, #12]
.LEHB0:
	bl	_Znwj
.LEHE0:
	mov	r5, r0
.LEHB1:
	bl	_ZN13CIwSoundGroupC1Ev
.LEHE1:
.L436:
	str	r5, [r4, #32]
	mov	r0, #8
.LEHB2:
	bl	_Znwj
.LEHE2:
	mov	r5, r0
.LEHB3:
	bl	_ZN14CIwSoundParamsC1Ev
.LEHE3:
	add	r2, sp, #8
	mov	r6, #8
	str	r6, [r2, #-4]!
	mov	r1, #4096	@ movhi
	mov	r6, #0
	str	r5, [r4, #36]
	mov	r5, #2	@ movhi
	strh	r5, [r4, #30]	@ movhi
	strh	r1, [r4, #24]	@ movhi
	strh	r1, [r4, #28]	@ movhi
	str	r6, [r4, #8]
	ldr	r1, .L534+8
	str	r6, [r4, #12]
	str	r6, [r4, #20]
	strh	r6, [r4, #26]	@ movhi
	ldr	r0, .L534+12
.LEHB4:
	bl	s3eConfigGetInt
	mov	r0, #3
	bl	s3eSoundGetInt
	ldr	r5, [sp, #4]
	cmp	r5, r0
	movge	r5, r0
	add	r0, r5, r5, asl #1
	str	r5, [r4, #16]
	mov	r0, r0, asl #3
	bl	_Znaj
	cmp	r5, r6
	beq	.L441
	mov	r2, #1
	sub	r7, r5, #1
	cmp	r2, r5
	and	r1, r7, #3
	str	r6, [r0, #0]
	str	r6, [r0, #8]
	str	r6, [r0, #12]
	str	r6, [r0, #20]
	add	r3, r0, #24
	beq	.L441
	cmp	r1, #0
	beq	.L443
	cmp	r1, #1
	beq	.L515
	cmp	r1, #2
	beq	.L516
	str	r6, [r3, #20]
	str	r6, [r3, #0]
	str	r6, [r3, #8]
	str	r6, [r3, #12]
	mov	r2, #2
	add	r3, r0, #48
.L516:
	str	r6, [r3, #0]
	str	r6, [r3, #8]
	str	r6, [r3, #12]
	str	r6, [r3, #20]
	add	r2, r2, #1
	add	r3, r3, #24
.L515:
	add	r2, r2, #1
	cmp	r2, r5
	str	r6, [r3, #0]
	str	r6, [r3, #8]
	str	r6, [r3, #12]
	str	r6, [r3, #20]
	add	r3, r3, #24
	beq	.L441
.L443:
	add	r2, r2, #4
	add	lr, r3, #24
	add	ip, r3, #48
	add	sl, r3, #72
	cmp	r2, r5
	str	r6, [r3, #8]
	str	r6, [r3, #12]
	str	r6, [r3, #20]
	str	r6, [lr, #8]
	str	r6, [lr, #20]
	str	r6, [lr, #12]
	str	r6, [ip, #20]
	str	r6, [r3, #0]
	str	r6, [r3, #24]
	str	r6, [r3, #48]
	str	r6, [ip, #8]
	str	r6, [ip, #12]
	str	r6, [r3, #72]
	str	r6, [sl, #20]
	str	r6, [sl, #8]
	str	r6, [sl, #12]
	add	r3, r3, #96
	bne	.L443
.L441:
	str	r0, [r9, #0]
	ldr	r5, [r4, #16]
	add	r2, r5, r5, asl #1
	mov	r0, r2, asl #3
	bl	_Znaj
	cmp	r5, #0
	beq	.L444
	mov	r1, #1
	mov	r3, #0
	sub	r6, r5, #1
	cmp	r1, r5
	and	ip, r6, #3
	str	r3, [r0, #0]
	str	r3, [r0, #8]
	str	r3, [r0, #12]
	str	r3, [r0, #20]
	add	r2, r0, #24
	beq	.L444
	cmp	ip, #0
	beq	.L446
	cmp	ip, #1
	beq	.L513
	cmp	ip, #2
	beq	.L514
	str	r3, [r2, #20]
	str	r3, [r2, #0]
	str	r3, [r2, #8]
	str	r3, [r2, #12]
	mov	r1, #2
	add	r2, r0, #48
.L514:
	str	r3, [r2, #0]
	str	r3, [r2, #8]
	str	r3, [r2, #12]
	str	r3, [r2, #20]
	add	r1, r1, #1
	add	r2, r2, #24
.L513:
	add	r1, r1, #1
	cmp	r1, r5
	str	r3, [r2, #0]
	str	r3, [r2, #8]
	str	r3, [r2, #12]
	str	r3, [r2, #20]
	add	r2, r2, #24
	beq	.L444
.L446:
	add	r1, r1, #4
	add	ip, r2, #24
	add	sl, r2, #48
	add	r7, r2, #72
	cmp	r1, r5
	str	r3, [r2, #8]
	str	r3, [r2, #12]
	str	r3, [r2, #20]
	str	r3, [ip, #8]
	str	r3, [ip, #20]
	str	r3, [ip, #12]
	str	r3, [sl, #20]
	str	r3, [r2, #0]
	str	r3, [r2, #24]
	str	r3, [r2, #48]
	str	r3, [sl, #8]
	str	r3, [sl, #12]
	str	r3, [r2, #72]
	str	r3, [r7, #20]
	str	r3, [r7, #8]
	str	r3, [r7, #12]
	add	r2, r2, #96
	bne	.L446
.L444:
	str	r0, [r9, #4]
	ldr	sl, [r4, #16]
	add	r1, sl, sl, asl #2
	add	r3, r1, r1, asl #5
	add	r0, sl, r3, asl #1
	mov	r0, r0, asl #2
	bl	_Znaj
.LEHE4:
	cmp	sl, #0
	mov	r7, r0
	beq	.L447
	sub	fp, sl, #1
	mov	r6, #0
	ands	fp, fp, #3
	sub	sl, sl, #2
	ldr	r5, .L534+16
	mov	r8, r6
	beq	.L449
	str	r6, [r7, #0]
	ldr	r6, [r5, #0]
	cmp	r6, #0
	beq	.L525
.L474:
	mov	r3, #1312
	cmp	fp, #1
	add	r6, r3, #12
	sub	sl, sl, #1
	beq	.L449
	cmp	fp, #2
	beq	.L512
	str	r8, [r7, r6]
	ldr	r2, [r5, #0]
	cmp	r2, #0
	beq	.L526
.L475:
	add	r0, r6, #1312
	add	r6, r0, #12
	sub	sl, sl, #1
.L512:
	str	r8, [r7, r6]
	ldr	r1, [r5, #0]
	cmp	r1, #0
	beq	.L527
.L477:
	add	lr, r6, #1312
	add	r6, lr, #12
	sub	sl, sl, #1
	b	.L449
.L448:
	add	r2, r6, #1312
	cmn	sl, #1
	add	r6, r2, #12
	beq	.L447
.L531:
	str	r8, [r7, r6]
	ldr	ip, [r5, #0]
	cmp	ip, #0
	sub	sl, sl, #1
	beq	.L528
.L480:
	add	r2, r6, #1312
	add	r3, r2, #12
	str	r8, [r7, r3]
	ldr	ip, [r5, #0]
	cmp	ip, #0
	beq	.L529
.L481:
	add	lr, r6, #2640
	add	r1, lr, #8
	str	r8, [r7, r1]
	ldr	r0, [r5, #0]
	cmp	r0, #0
	beq	.L530
.L482:
	add	r6, r6, #3968
	add	r6, r6, #4
	sub	sl, sl, #3
.L449:
	str	r8, [r7, r6]
	ldr	lr, [r5, #0]
	cmp	lr, #0
	bne	.L448
.LEHB5:
	bl	_ZN15CIwChannelADPCM4InitEv
.LEHE5:
	add	r2, r6, #1312
	cmn	sl, #1
	add	r6, r2, #12
	bne	.L531
.L447:
	str	r7, [r9, #8]
	ldr	r6, [r4, #16]
	add	r7, r6, r6, asl #1
	mov	r0, r7, asl #3
.LEHB6:
	bl	_Znaj
.LEHE6:
	cmp	r6, #0
	mov	r7, r0
	beq	.L451
.L452:
	sub	r8, r6, #1
	ands	r8, r8, #3
	sub	r6, r6, #2
	mov	r5, r0
	beq	.L453
.LEHB7:
	bl	_ZN12CIwSoundInstC1Ev
	cmp	r8, #1
	sub	r6, r6, #1
	add	r5, r7, #24
	beq	.L453
	cmp	r8, #2
	beq	.L511
	mov	r0, r5
	bl	_ZN12CIwSoundInstC1Ev
	add	r5, r5, #24
	sub	r6, r6, #1
.L511:
	mov	r0, r5
	bl	_ZN12CIwSoundInstC1Ev
	add	r5, r5, #24
	sub	r6, r6, #1
	b	.L453
.L532:
	mov	r0, r5
	sub	r6, r6, #1
	bl	_ZN12CIwSoundInstC1Ev
	add	r0, r5, #24
	bl	_ZN12CIwSoundInstC1Ev
	add	r0, r5, #48
	bl	_ZN12CIwSoundInstC1Ev
	add	r5, r5, #72
	sub	r6, r6, #3
.L453:
	mov	r0, r5
	bl	_ZN12CIwSoundInstC1Ev
.LEHE7:
	cmn	r6, #1
	add	r5, r5, #24
	bne	.L532
.L451:
	ldr	r0, [r4, #16]
	str	r7, [r4, #8]
	mov	r0, r0, asl #2
.LEHB8:
	bl	_Znaj
.LEHE8:
	ldr	r3, [r4, #16]
	cmp	r3, #0
	movne	r2, #0
	str	r0, [r4, #12]
	movne	r3, r2
	bne	.L457
	b	.L455
.L533:
	ldr	r0, [r4, #12]
.L457:
	ldr	ip, [r4, #8]
	add	lr, ip, r2
	str	lr, [r0, r3, asl #2]
	ldr	r1, [r4, #16]
	add	r3, r3, #1
	cmp	r1, r3
	add	r2, r2, #24
	bhi	.L533
.L455:
	mov	r0, r4
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L530:
.LEHB9:
	bl	_ZN15CIwChannelADPCM4InitEv
	b	.L482
.L529:
	bl	_ZN15CIwChannelADPCM4InitEv
	b	.L481
.L528:
	bl	_ZN15CIwChannelADPCM4InitEv
	b	.L480
.L525:
	bl	_ZN15CIwChannelADPCM4InitEv
	b	.L474
.L527:
	bl	_ZN15CIwChannelADPCM4InitEv
	b	.L477
.L526:
	bl	_ZN15CIwChannelADPCM4InitEv
.LEHE9:
	b	.L475
.L461:
.L523:
.L438:
	mov	r4, r0
	mov	r0, r5
	bl	_ZdlPv
	mov	r0, r4
.LEHB10:
	bl	__cxa_end_cleanup
.L462:
	b	.L523
.L459:
.L524:
.L456:
	mov	r9, r0
	mov	r0, r7
	bl	_ZdaPv
	mov	r0, r9
	bl	__cxa_end_cleanup
.LEHE10:
.L460:
	b	.L524
.L535:
	.align	2
.L534:
	.word	.LANCHOR1+8
	.word	.LANCHOR0
	.word	.LC1
	.word	.LC0
	.word	_ZN15CIwChannelADPCM13isInitializedE
	.global	__gxx_personality_v0
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA1373:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE1373-.LLSDACSB1373
.LLSDACSB1373:
	.uleb128 .LEHB0-.LFB1373
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB1-.LFB1373
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L462-.LFB1373
	.uleb128 0x0
	.uleb128 .LEHB2-.LFB1373
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB3-.LFB1373
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L461-.LFB1373
	.uleb128 0x0
	.uleb128 .LEHB4-.LFB1373
	.uleb128 .LEHE4-.LEHB4
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB5-.LFB1373
	.uleb128 .LEHE5-.LEHB5
	.uleb128 .L460-.LFB1373
	.uleb128 0x0
	.uleb128 .LEHB6-.LFB1373
	.uleb128 .LEHE6-.LEHB6
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB7-.LFB1373
	.uleb128 .LEHE7-.LEHB7
	.uleb128 .L459-.LFB1373
	.uleb128 0x0
	.uleb128 .LEHB8-.LFB1373
	.uleb128 .LEHE8-.LEHB8
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB9-.LFB1373
	.uleb128 .LEHE9-.LEHB9
	.uleb128 .L460-.LFB1373
	.uleb128 0x0
	.uleb128 .LEHB10-.LFB1373
	.uleb128 .LEHE10-.LEHB10
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE1373:
	.fnend
	.size	_ZN15CIwSoundManagerC1Ev, .-_ZN15CIwSoundManagerC1Ev
	.section	.text._ZN15CIwSoundManagerC2Ev,"ax",%progbits
	.align	2
	.global	_ZN15CIwSoundManagerC2Ev
	.hidden	_ZN15CIwSoundManagerC2Ev
	.type	_ZN15CIwSoundManagerC2Ev, %function
_ZN15CIwSoundManagerC2Ev:
	.fnstart
.LFB1372:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	ldr	r3, .L636
	ldr	r9, .L636+4
	mov	r4, r0
	str	r3, [r0, #0]
	.pad #12
	sub	sp, sp, #12
	mov	r0, #32
	str	r4, [r9, #12]
.LEHB11:
	bl	_Znwj
.LEHE11:
	mov	r5, r0
.LEHB12:
	bl	_ZN13CIwSoundGroupC1Ev
.LEHE12:
.L538:
	str	r5, [r4, #32]
	mov	r0, #8
.LEHB13:
	bl	_Znwj
.LEHE13:
	mov	r5, r0
.LEHB14:
	bl	_ZN14CIwSoundParamsC1Ev
.LEHE14:
	add	r2, sp, #8
	mov	r6, #8
	str	r6, [r2, #-4]!
	mov	r1, #4096	@ movhi
	mov	r6, #0
	str	r5, [r4, #36]
	mov	r5, #2	@ movhi
	strh	r5, [r4, #30]	@ movhi
	strh	r1, [r4, #24]	@ movhi
	strh	r1, [r4, #28]	@ movhi
	str	r6, [r4, #8]
	ldr	r1, .L636+8
	str	r6, [r4, #12]
	str	r6, [r4, #20]
	strh	r6, [r4, #26]	@ movhi
	ldr	r0, .L636+12
.LEHB15:
	bl	s3eConfigGetInt
	mov	r0, #3
	bl	s3eSoundGetInt
	ldr	r5, [sp, #4]
	cmp	r5, r0
	movge	r5, r0
	add	r0, r5, r5, asl #1
	str	r5, [r4, #16]
	mov	r0, r0, asl #3
	bl	_Znaj
	cmp	r5, r6
	beq	.L543
	mov	r2, #1
	sub	r7, r5, #1
	cmp	r2, r5
	and	r1, r7, #3
	str	r6, [r0, #0]
	str	r6, [r0, #8]
	str	r6, [r0, #12]
	str	r6, [r0, #20]
	add	r3, r0, #24
	beq	.L543
	cmp	r1, #0
	beq	.L545
	cmp	r1, #1
	beq	.L617
	cmp	r1, #2
	beq	.L618
	str	r6, [r3, #20]
	str	r6, [r3, #0]
	str	r6, [r3, #8]
	str	r6, [r3, #12]
	mov	r2, #2
	add	r3, r0, #48
.L618:
	str	r6, [r3, #0]
	str	r6, [r3, #8]
	str	r6, [r3, #12]
	str	r6, [r3, #20]
	add	r2, r2, #1
	add	r3, r3, #24
.L617:
	add	r2, r2, #1
	cmp	r2, r5
	str	r6, [r3, #0]
	str	r6, [r3, #8]
	str	r6, [r3, #12]
	str	r6, [r3, #20]
	add	r3, r3, #24
	beq	.L543
.L545:
	add	r2, r2, #4
	add	lr, r3, #24
	add	ip, r3, #48
	add	sl, r3, #72
	cmp	r2, r5
	str	r6, [r3, #8]
	str	r6, [r3, #12]
	str	r6, [r3, #20]
	str	r6, [lr, #8]
	str	r6, [lr, #20]
	str	r6, [lr, #12]
	str	r6, [ip, #20]
	str	r6, [r3, #0]
	str	r6, [r3, #24]
	str	r6, [r3, #48]
	str	r6, [ip, #8]
	str	r6, [ip, #12]
	str	r6, [r3, #72]
	str	r6, [sl, #20]
	str	r6, [sl, #8]
	str	r6, [sl, #12]
	add	r3, r3, #96
	bne	.L545
.L543:
	str	r0, [r9, #0]
	ldr	r5, [r4, #16]
	add	r2, r5, r5, asl #1
	mov	r0, r2, asl #3
	bl	_Znaj
	cmp	r5, #0
	beq	.L546
	mov	r1, #1
	mov	r3, #0
	sub	r6, r5, #1
	cmp	r1, r5
	and	ip, r6, #3
	str	r3, [r0, #0]
	str	r3, [r0, #8]
	str	r3, [r0, #12]
	str	r3, [r0, #20]
	add	r2, r0, #24
	beq	.L546
	cmp	ip, #0
	beq	.L548
	cmp	ip, #1
	beq	.L615
	cmp	ip, #2
	beq	.L616
	str	r3, [r2, #20]
	str	r3, [r2, #0]
	str	r3, [r2, #8]
	str	r3, [r2, #12]
	mov	r1, #2
	add	r2, r0, #48
.L616:
	str	r3, [r2, #0]
	str	r3, [r2, #8]
	str	r3, [r2, #12]
	str	r3, [r2, #20]
	add	r1, r1, #1
	add	r2, r2, #24
.L615:
	add	r1, r1, #1
	cmp	r1, r5
	str	r3, [r2, #0]
	str	r3, [r2, #8]
	str	r3, [r2, #12]
	str	r3, [r2, #20]
	add	r2, r2, #24
	beq	.L546
.L548:
	add	r1, r1, #4
	add	ip, r2, #24
	add	sl, r2, #48
	add	r7, r2, #72
	cmp	r1, r5
	str	r3, [r2, #8]
	str	r3, [r2, #12]
	str	r3, [r2, #20]
	str	r3, [ip, #8]
	str	r3, [ip, #20]
	str	r3, [ip, #12]
	str	r3, [sl, #20]
	str	r3, [r2, #0]
	str	r3, [r2, #24]
	str	r3, [r2, #48]
	str	r3, [sl, #8]
	str	r3, [sl, #12]
	str	r3, [r2, #72]
	str	r3, [r7, #20]
	str	r3, [r7, #8]
	str	r3, [r7, #12]
	add	r2, r2, #96
	bne	.L548
.L546:
	str	r0, [r9, #4]
	ldr	sl, [r4, #16]
	add	r1, sl, sl, asl #2
	add	r3, r1, r1, asl #5
	add	r0, sl, r3, asl #1
	mov	r0, r0, asl #2
	bl	_Znaj
.LEHE15:
	cmp	sl, #0
	mov	r7, r0
	beq	.L549
	sub	fp, sl, #1
	mov	r6, #0
	ands	fp, fp, #3
	sub	sl, sl, #2
	ldr	r5, .L636+16
	mov	r8, r6
	beq	.L551
	str	r6, [r7, #0]
	ldr	r6, [r5, #0]
	cmp	r6, #0
	beq	.L627
.L576:
	mov	r3, #1312
	cmp	fp, #1
	add	r6, r3, #12
	sub	sl, sl, #1
	beq	.L551
	cmp	fp, #2
	beq	.L614
	str	r8, [r7, r6]
	ldr	r2, [r5, #0]
	cmp	r2, #0
	beq	.L628
.L577:
	add	r0, r6, #1312
	add	r6, r0, #12
	sub	sl, sl, #1
.L614:
	str	r8, [r7, r6]
	ldr	r1, [r5, #0]
	cmp	r1, #0
	beq	.L629
.L579:
	add	lr, r6, #1312
	add	r6, lr, #12
	sub	sl, sl, #1
	b	.L551
.L550:
	add	r2, r6, #1312
	cmn	sl, #1
	add	r6, r2, #12
	beq	.L549
.L633:
	str	r8, [r7, r6]
	ldr	ip, [r5, #0]
	cmp	ip, #0
	sub	sl, sl, #1
	beq	.L630
.L582:
	add	r2, r6, #1312
	add	r3, r2, #12
	str	r8, [r7, r3]
	ldr	ip, [r5, #0]
	cmp	ip, #0
	beq	.L631
.L583:
	add	lr, r6, #2640
	add	r1, lr, #8
	str	r8, [r7, r1]
	ldr	r0, [r5, #0]
	cmp	r0, #0
	beq	.L632
.L584:
	add	r6, r6, #3968
	add	r6, r6, #4
	sub	sl, sl, #3
.L551:
	str	r8, [r7, r6]
	ldr	lr, [r5, #0]
	cmp	lr, #0
	bne	.L550
.LEHB16:
	bl	_ZN15CIwChannelADPCM4InitEv
.LEHE16:
	add	r2, r6, #1312
	cmn	sl, #1
	add	r6, r2, #12
	bne	.L633
.L549:
	str	r7, [r9, #8]
	ldr	r6, [r4, #16]
	add	r7, r6, r6, asl #1
	mov	r0, r7, asl #3
.LEHB17:
	bl	_Znaj
.LEHE17:
	cmp	r6, #0
	mov	r7, r0
	beq	.L553
.L554:
	sub	r8, r6, #1
	ands	r8, r8, #3
	sub	r6, r6, #2
	mov	r5, r0
	beq	.L555
.LEHB18:
	bl	_ZN12CIwSoundInstC1Ev
	cmp	r8, #1
	sub	r6, r6, #1
	add	r5, r7, #24
	beq	.L555
	cmp	r8, #2
	beq	.L613
	mov	r0, r5
	bl	_ZN12CIwSoundInstC1Ev
	add	r5, r5, #24
	sub	r6, r6, #1
.L613:
	mov	r0, r5
	bl	_ZN12CIwSoundInstC1Ev
	add	r5, r5, #24
	sub	r6, r6, #1
	b	.L555
.L634:
	mov	r0, r5
	sub	r6, r6, #1
	bl	_ZN12CIwSoundInstC1Ev
	add	r0, r5, #24
	bl	_ZN12CIwSoundInstC1Ev
	add	r0, r5, #48
	bl	_ZN12CIwSoundInstC1Ev
	add	r5, r5, #72
	sub	r6, r6, #3
.L555:
	mov	r0, r5
	bl	_ZN12CIwSoundInstC1Ev
.LEHE18:
	cmn	r6, #1
	add	r5, r5, #24
	bne	.L634
.L553:
	ldr	r0, [r4, #16]
	str	r7, [r4, #8]
	mov	r0, r0, asl #2
.LEHB19:
	bl	_Znaj
.LEHE19:
	ldr	r3, [r4, #16]
	cmp	r3, #0
	movne	r2, #0
	str	r0, [r4, #12]
	movne	r3, r2
	bne	.L559
	b	.L557
.L635:
	ldr	r0, [r4, #12]
.L559:
	ldr	ip, [r4, #8]
	add	lr, ip, r2
	str	lr, [r0, r3, asl #2]
	ldr	r1, [r4, #16]
	add	r3, r3, #1
	cmp	r1, r3
	add	r2, r2, #24
	bhi	.L635
.L557:
	mov	r0, r4
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L632:
.LEHB20:
	bl	_ZN15CIwChannelADPCM4InitEv
	b	.L584
.L631:
	bl	_ZN15CIwChannelADPCM4InitEv
	b	.L583
.L630:
	bl	_ZN15CIwChannelADPCM4InitEv
	b	.L582
.L627:
	bl	_ZN15CIwChannelADPCM4InitEv
	b	.L576
.L629:
	bl	_ZN15CIwChannelADPCM4InitEv
	b	.L579
.L628:
	bl	_ZN15CIwChannelADPCM4InitEv
.LEHE20:
	b	.L577
.L563:
.L625:
.L540:
	mov	r4, r0
	mov	r0, r5
	bl	_ZdlPv
	mov	r0, r4
.LEHB21:
	bl	__cxa_end_cleanup
.L564:
	b	.L625
.L561:
.L626:
.L558:
	mov	r9, r0
	mov	r0, r7
	bl	_ZdaPv
	mov	r0, r9
	bl	__cxa_end_cleanup
.LEHE21:
.L562:
	b	.L626
.L637:
	.align	2
.L636:
	.word	.LANCHOR1+8
	.word	.LANCHOR0
	.word	.LC1
	.word	.LC0
	.word	_ZN15CIwChannelADPCM13isInitializedE
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA1372:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE1372-.LLSDACSB1372
.LLSDACSB1372:
	.uleb128 .LEHB11-.LFB1372
	.uleb128 .LEHE11-.LEHB11
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB12-.LFB1372
	.uleb128 .LEHE12-.LEHB12
	.uleb128 .L564-.LFB1372
	.uleb128 0x0
	.uleb128 .LEHB13-.LFB1372
	.uleb128 .LEHE13-.LEHB13
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB14-.LFB1372
	.uleb128 .LEHE14-.LEHB14
	.uleb128 .L563-.LFB1372
	.uleb128 0x0
	.uleb128 .LEHB15-.LFB1372
	.uleb128 .LEHE15-.LEHB15
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB16-.LFB1372
	.uleb128 .LEHE16-.LEHB16
	.uleb128 .L562-.LFB1372
	.uleb128 0x0
	.uleb128 .LEHB17-.LFB1372
	.uleb128 .LEHE17-.LEHB17
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB18-.LFB1372
	.uleb128 .LEHE18-.LEHB18
	.uleb128 .L561-.LFB1372
	.uleb128 0x0
	.uleb128 .LEHB19-.LFB1372
	.uleb128 .LEHE19-.LEHB19
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB20-.LFB1372
	.uleb128 .LEHE20-.LEHB20
	.uleb128 .L562-.LFB1372
	.uleb128 0x0
	.uleb128 .LEHB21-.LFB1372
	.uleb128 .LEHE21-.LEHB21
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE1372:
	.fnend
	.size	_ZN15CIwSoundManagerC2Ev, .-_ZN15CIwSoundManagerC2Ev
	.section	.text._Z11IwSoundInitv,"ax",%progbits
	.align	2
	.global	_Z11IwSoundInitv
	.hidden	_Z11IwSoundInitv
	.type	_Z11IwSoundInitv, %function
_Z11IwSoundInitv:
	.fnstart
.LFB1369:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	mov	r0, #40
.LEHB22:
	bl	_Znwj
.LEHE22:
	mov	r4, r0
.LEHB23:
	bl	_ZN15CIwSoundManagerC1Ev
.LEHE23:
.LEHB24:
	bl	_Z20_GetCIwSoundDataSizev
	ldr	r1, .L644
	mov	r2, r0
	ldr	r0, .L644+4
	bl	_Z17IwClassFactoryAddPKcPFPvvEj
	bl	_Z25_GetCIwSoundDataADPCMSizev
	ldr	r1, .L644+8
	mov	r2, r0
	ldr	r0, .L644+12
	bl	_Z17IwClassFactoryAddPKcPFPvvEj
	bl	_Z21_GetCIwSoundGroupSizev
	ldr	r1, .L644+16
	mov	r2, r0
	ldr	r0, .L644+20
	bl	_Z17IwClassFactoryAddPKcPFPvvEj
	bl	_Z20_GetCIwSoundSpecSizev
	ldr	r1, .L644+24
	mov	r2, r0
	ldr	r0, .L644+28
	bl	_Z17IwClassFactoryAddPKcPFPvvEj
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L642:
.L640:
	mov	r5, r0
	mov	r0, r4
	bl	_ZdlPv
	mov	r0, r5
	bl	__cxa_end_cleanup
.LEHE24:
.L645:
	.align	2
.L644:
	.word	_Z20_CIwSoundDataFactoryv
	.word	.LC2
	.word	_Z25_CIwSoundDataADPCMFactoryv
	.word	.LC3
	.word	_Z21_CIwSoundGroupFactoryv
	.word	.LC4
	.word	_Z20_CIwSoundSpecFactoryv
	.word	.LC5
	.personality	__gxx_personality_v0
	.handlerdata
.LLSDA1369:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE1369-.LLSDACSB1369
.LLSDACSB1369:
	.uleb128 .LEHB22-.LFB1369
	.uleb128 .LEHE22-.LEHB22
	.uleb128 0x0
	.uleb128 0x0
	.uleb128 .LEHB23-.LFB1369
	.uleb128 .LEHE23-.LEHB23
	.uleb128 .L642-.LFB1369
	.uleb128 0x0
	.uleb128 .LEHB24-.LFB1369
	.uleb128 .LEHE24-.LEHB24
	.uleb128 0x0
	.uleb128 0x0
.LLSDACSE1369:
	.fnend
	.size	_Z11IwSoundInitv, .-_Z11IwSoundInitv
	.section	.text._ZN15CIwSoundManager15UpdateCompletedEi,"ax",%progbits
	.align	2
	.global	_ZN15CIwSoundManager15UpdateCompletedEi
	.hidden	_ZN15CIwSoundManager15UpdateCompletedEi
	.type	_ZN15CIwSoundManager15UpdateCompletedEi, %function
_ZN15CIwSoundManager15UpdateCompletedEi:
	.fnstart
.LFB1383:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	ldr	ip, [r0, #12]
	ldr	r5, [ip, r1, asl #2]
	mov	r4, r0
	ldr	r0, [r5, #0]
	ldr	r3, [r0, #36]
	cmp	r3, #0
	ldrneh	r2, [r3, #24]
	subne	r2, r2, #1
	strneh	r2, [r3, #24]	@ movhi
	ldr	r3, [r5, #20]
	cmp	r3, #0
	movne	r0, r5
	movne	lr, pc
	bxne	r3
.L648:
	ldrh	r2, [r5, #10]
	ldrh	ip, [r5, #16]
	bic	r1, r2, #2
	orr	r0, r1, #4
	add	r3, ip, #1
	strh	r0, [r5, #10]	@ movhi
	strh	r3, [r5, #16]	@ movhi
	ldr	r2, [r4, #20]
	ldr	r3, [r4, #12]
	sub	r1, r2, #1
	add	r1, r3, r1, asl #2
	cmp	r3, r1
	bhi	.L653
	ldr	r0, [r3, #0]
	cmp	r5, r0
	beq	.L650
	rsb	ip, r3, r1
	mov	r0, ip, lsr #2
	ands	r2, r0, #3
	beq	.L654
	add	r3, r3, #4
	cmp	r1, r3
	bcc	.L653
	ldr	r0, [r3, #0]
	cmp	r5, r0
	beq	.L650
	cmp	r2, #1
	beq	.L654
	cmp	r2, #2
	beq	.L673
	ldr	r2, [r3, #4]!
	cmp	r5, r2
	beq	.L650
.L673:
	ldr	ip, [r3, #4]!
	cmp	r5, ip
	beq	.L650
.L654:
	add	r3, r3, #4
	cmp	r1, r3
	mov	r2, r3
	bcc	.L653
	ldr	ip, [r3, #0]
	cmp	r5, ip
	beq	.L650
	ldr	r0, [r3, #4]!
	cmp	r5, r0
	beq	.L650
	ldr	r3, [r2, #8]
	cmp	r5, r3
	add	r3, r2, #8
	beq	.L650
	ldr	ip, [r2, #12]
	cmp	r5, ip
	add	r3, r2, #12
	bne	.L654
.L650:
	ldr	r2, [r1, #0]
	str	r2, [r3, #0]
	str	r5, [r1, #0]
	ldr	r1, [r4, #20]
	sub	r3, r1, #1
	str	r3, [r4, #20]
.L653:
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
	.fnend
	.size	_ZN15CIwSoundManager15UpdateCompletedEi, .-_ZN15CIwSoundManager15UpdateCompletedEi
	.section	.text._ZN15CIwSoundManager6UpdateEv,"ax",%progbits
	.align	2
	.global	_ZN15CIwSoundManager6UpdateEv
	.hidden	_ZN15CIwSoundManager6UpdateEv
	.type	_ZN15CIwSoundManager6UpdateEv, %function
_ZN15CIwSoundManager6UpdateEv:
	.fnstart
.LFB1384:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, lr}
	.save {r3, r4, r5, r6, r7, lr}
	ldrh	r3, [r0, #30]
	tst	r3, #2
	mov	r4, r0
	beq	.L698
	ldr	r0, [r0, #4]
	ldr	r2, [r4, #20]
	add	r1, r0, #1
	cmp	r2, #0
	str	r1, [r4, #4]
	beq	.L680
	mov	r6, #0
	b	.L689
.L682:
	ldrh	r0, [r5, #14]
	add	r3, r0, #1
	strh	r3, [r5, #14]	@ movhi
	ldr	r2, [r4, #20]
.L681:
	add	r6, r6, #1
	cmp	r6, r2
	bcs	.L731
.L689:
	ldr	r3, [r4, #12]
	ldr	r5, [r3, r6, asl #2]
	ldr	r3, [r5, #0]
	cmp	r3, #0
	beq	.L681
	ldrh	r2, [r5, #10]
	tst	r2, #2
	beq	.L682
	ldr	ip, [r3, #36]
	cmp	ip, #0
	ldrneh	r2, [ip, #24]
	subne	r2, r2, #1
	strneh	r2, [ip, #24]	@ movhi
	ldr	r3, [r5, #20]
	cmp	r3, #0
	movne	r0, r5
	movne	lr, pc
	bxne	r3
.L684:
	ldrh	r1, [r5, #10]
	ldrh	r3, [r5, #16]
	bic	ip, r1, #2
	add	r0, r3, #1
	orr	r2, ip, #4
	strh	r0, [r5, #16]	@ movhi
	strh	r2, [r5, #10]	@ movhi
	ldr	r2, [r4, #20]
	ldr	r3, [r4, #12]
	sub	r1, r2, #1
	add	r0, r3, r1, asl #2
	cmp	r3, r0
	bhi	.L685
	ldr	ip, [r3, #0]
	cmp	r5, ip
	beq	.L686
	rsb	ip, r3, r0
	mov	r1, ip, lsr #2
	ands	r1, r1, #3
	beq	.L701
	add	r3, r3, #4
	cmp	r0, r3
	bcc	.L685
	ldr	ip, [r3, #0]
	cmp	r5, ip
	beq	.L686
	cmp	r1, #1
	beq	.L701
	cmp	r1, #2
	beq	.L722
	ldr	r1, [r3, #4]!
	cmp	r5, r1
	beq	.L686
.L722:
	ldr	ip, [r3, #4]!
	cmp	r5, ip
	beq	.L686
.L701:
	add	r3, r3, #4
	cmp	r0, r3
	mov	r1, r3
	bcc	.L685
	ldr	ip, [r3, #0]
	cmp	r5, ip
	beq	.L686
	ldr	ip, [r3, #4]!
	cmp	r5, ip
	beq	.L686
	ldr	r3, [r1, #8]
	cmp	r5, r3
	add	r3, r1, #8
	beq	.L686
	ldr	ip, [r1, #12]
	cmp	r5, ip
	add	r3, r1, #12
	bne	.L701
.L686:
	ldr	r2, [r0, #0]
	str	r2, [r3, #0]
	str	r5, [r0, #0]
	ldr	r0, [r4, #20]
	sub	r2, r0, #1
	str	r2, [r4, #20]
.L685:
	sub	r6, r6, #1
	add	r6, r6, #1
	cmp	r6, r2
	bcc	.L689
.L731:
	cmp	r2, #0
	beq	.L729
	ldr	r1, [r4, #12]
	ldr	r5, [r1, #0]
	ldr	r3, [r5, #0]
	cmp	r3, #0
	movne	r6, #0
	bne	.L690
	b	.L698
.L692:
	ldrsh	r2, [r4, #24]
	ldrsh	lr, [r3, #16]
	ldrsh	ip, [r3, #20]
	mul	lr, r2, lr
	ldrsh	r7, [r4, #28]
	mul	r7, ip, r7
	ldrsh	r1, [r5, #4]
	mov	r0, lr, asr #12
	mul	r0, r1, r0
	ldrsh	r3, [r5, #8]
	mov	ip, r7, asr #12
	mul	ip, r3, ip
	mov	r2, r0, asl #4
	mov	r2, r2, asr #20
	cmp	r2, #256
	movge	r2, #256
	mov	lr, ip, asl #4
	mov	r1, #3
	ldrh	r0, [r5, #12]
	mov	r7, lr, lsr #16
	bl	s3eSoundChannelSetInt
	ldr	r1, [r5, #0]
	ldr	r3, [r1, #32]
	mov	r7, r7, asl #16
	ldr	r0, [r3, #28]
	mov	r7, r7, asr #16
	mul	r0, r7, r0
	mov	r1, #1
	mov	r2, r0, asr #12
	ldrh	r0, [r5, #12]
	bl	s3eSoundChannelSetInt
	ldrh	r2, [r5, #10]
	bic	r3, r2, #1
	strh	r3, [r5, #10]	@ movhi
	ldr	r2, [r4, #20]
	cmp	r2, r6
	bls	.L732
.L694:
	ldr	ip, [r4, #12]
	ldr	r5, [ip, r6, asl #2]
	ldr	r3, [r5, #0]
	cmp	r3, #0
	beq	.L698
.L690:
	ldr	r3, [r3, #36]
	ldrh	r0, [r5, #10]
	cmp	r3, #0
	ldreq	r3, [r4, #32]
	tst	r0, #1
	add	r6, r6, #1
	bne	.L692
	ldrh	r1, [r3, #26]
	tst	r1, #1
	bne	.L692
	ldrh	ip, [r4, #30]
	tst	ip, #1
	bne	.L692
	cmp	r2, r6
	bhi	.L694
.L732:
	cmp	r2, #0
	movne	r3, #0
	beq	.L729
.L697:
	ldr	r1, [r4, #12]
	ldr	r0, [r1, r3, asl #2]
	ldr	r1, [r0, #0]
	cmp	r1, #0
	add	r3, r3, #1
	beq	.L695
	ldr	ip, [r1, #36]
	cmp	ip, #0
	ldrneh	r2, [ip, #26]
	bicne	r2, r2, #1
	strneh	r2, [ip, #26]	@ movhi
	ldrne	r2, [r4, #20]
.L695:
	cmp	r3, r2
	bcc	.L697
.L729:
	ldrh	r3, [r4, #30]
.L680:
	bic	r3, r3, #1
	strh	r3, [r4, #30]	@ movhi
.L698:
	ldmfd	sp!, {r3, r4, r5, r6, r7, lr}
	bx	lr
	.fnend
	.size	_ZN15CIwSoundManager6UpdateEv, .-_ZN15CIwSoundManager6UpdateEv
	.section	.text._ZN15CIwSoundManager7StopAllEv,"ax",%progbits
	.align	2
	.global	_ZN15CIwSoundManager7StopAllEv
	.hidden	_ZN15CIwSoundManager7StopAllEv
	.type	_ZN15CIwSoundManager7StopAllEv, %function
_ZN15CIwSoundManager7StopAllEv:
	.fnstart
.LFB1388:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	.save {r3, r4, r5, lr}
	ldr	r3, [r0, #20]
	cmp	r3, #0
	mov	r5, r0
	beq	.L734
	mov	r4, #0
.L735:
	ldr	r1, [r5, #12]
	ldr	r0, [r1, r4, asl #2]
	bl	_ZN12CIwSoundInst4StopEv
	ldr	r0, [r5, #20]
	add	r4, r4, #1
	cmp	r0, r4
	bhi	.L735
.L734:
	mov	r0, r5
	bl	_ZN15CIwSoundManager6UpdateEv
	mov	r0, #50
	bl	s3eDeviceYield
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
	.fnend
	.size	_ZN15CIwSoundManager7StopAllEv, .-_ZN15CIwSoundManager7StopAllEv
	.section	.text._Z16IwSoundTerminatev,"ax",%progbits
	.align	2
	.global	_Z16IwSoundTerminatev
	.hidden	_Z16IwSoundTerminatev
	.type	_Z16IwSoundTerminatev, %function
_Z16IwSoundTerminatev:
	.fnstart
.LFB1370:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	ldr	r4, .L744
	ldr	r6, [r4, #12]
	ldr	r3, [r6, #20]
	cmp	r3, #0
	beq	.L739
	mov	r5, #0
.L740:
	ldr	r1, [r6, #12]
	ldr	r0, [r1, r5, asl #2]
	bl	_ZN12CIwSoundInst4StopEv
	ldr	r0, [r6, #20]
	add	r5, r5, #1
	cmp	r5, r0
	bcc	.L740
.L739:
	mov	r0, r6
	bl	_ZN15CIwSoundManager6UpdateEv
	mov	r0, #50
	bl	s3eDeviceYield
	ldr	r3, [r4, #12]
	cmp	r3, #0
	movne	r0, r3
	ldrne	r2, [r3, #0]
	ldrne	ip, [r2, #4]
	movne	lr, pc
	bxne	ip
.L742:
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L745:
	.align	2
.L744:
	.word	.LANCHOR0
	.fnend
	.size	_Z16IwSoundTerminatev, .-_Z16IwSoundTerminatev
	.section	.text._ZN15CIwSoundManager13StopSoundSpecEP12CIwSoundSpec,"ax",%progbits
	.align	2
	.global	_ZN15CIwSoundManager13StopSoundSpecEP12CIwSoundSpec
	.hidden	_ZN15CIwSoundManager13StopSoundSpecEP12CIwSoundSpec
	.type	_ZN15CIwSoundManager13StopSoundSpecEP12CIwSoundSpec, %function
_ZN15CIwSoundManager13StopSoundSpecEP12CIwSoundSpec:
	.fnstart
.LFB1389:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	.save {r4, r5, r6, r7, r8, lr}
	subs	r8, r1, #0
	mov	r6, r0
	beq	.L756
	ldr	r4, [r0, #20]
	subs	r5, r4, #1
	bmi	.L748
	sub	r4, r4, #-1073741823
	mov	r4, r4, asl #2
.L755:
	ldr	r1, [r6, #12]
	ldr	r0, [r1, r4]
	ldr	r3, [r0, #0]
	cmp	r8, r3
	beq	.L782
.L749:
	subs	r5, r5, #1
	sub	r4, r4, #4
	bpl	.L755
.L748:
	mov	r0, #50
	bl	s3eDeviceYield
.L756:
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L782:
	bl	_ZN12CIwSoundInst4StopEv
	ldr	ip, [r6, #12]
	ldr	r7, [ip, r4]
	ldr	r2, [r7, #0]
	ldr	r0, [r2, #36]
	cmp	r0, #0
	ldrneh	r2, [r0, #24]
	subne	r2, r2, #1
	strneh	r2, [r0, #24]	@ movhi
	ldr	r3, [r7, #20]
	cmp	r3, #0
	movne	r0, r7
	movne	lr, pc
	bxne	r3
.L751:
	ldrh	lr, [r7, #10]
	ldrh	ip, [r7, #16]
	bic	r2, lr, #2
	add	r3, ip, #1
	orr	r0, r2, #4
	strh	r0, [r7, #10]	@ movhi
	strh	r3, [r7, #16]	@ movhi
	ldr	r1, [r6, #20]
	ldr	r3, [r6, #12]
	sub	lr, r1, #1
	add	r1, r3, lr, asl #2
	cmp	r3, r1
	bhi	.L749
	ldr	r0, [r3, #0]
	cmp	r7, r0
	beq	.L752
	rsb	r0, r3, r1
	mov	lr, r0, lsr #2
	ands	r2, lr, #3
	beq	.L757
	add	r3, r3, #4
	cmp	r1, r3
	bcc	.L749
	ldr	ip, [r3, #0]
	cmp	r7, ip
	beq	.L752
	cmp	r2, #1
	beq	.L757
	cmp	r2, #2
	beq	.L777
	ldr	r2, [r3, #4]!
	cmp	r7, r2
	beq	.L752
.L777:
	ldr	lr, [r3, #4]!
	cmp	r7, lr
	beq	.L752
.L757:
	add	r3, r3, #4
	cmp	r1, r3
	mov	r2, r3
	bcc	.L749
	ldr	ip, [r3, #0]
	cmp	r7, ip
	beq	.L752
	ldr	r0, [r3, #4]!
	cmp	r7, r0
	beq	.L752
	ldr	r3, [r2, #8]
	cmp	r7, r3
	add	r3, r2, #8
	beq	.L752
	ldr	ip, [r2, #12]
	cmp	r7, ip
	add	r3, r2, #12
	bne	.L757
.L752:
	ldr	r2, [r1, #0]
	str	r2, [r3, #0]
	str	r7, [r1, #0]
	ldr	r3, [r6, #20]
	sub	r1, r3, #1
	str	r1, [r6, #20]
	b	.L749
	.fnend
	.size	_ZN15CIwSoundManager13StopSoundSpecEP12CIwSoundSpec, .-_ZN15CIwSoundManager13StopSoundSpecEP12CIwSoundSpec
	.hidden	g_IwSoundManager
	.global	g_IwSoundManager
	.hidden	_ZN15CIwSoundManager14s_ChannelsPCM8E
	.global	_ZN15CIwSoundManager14s_ChannelsPCM8E
	.hidden	_ZN15CIwSoundManager15s_ChannelsPCM16E
	.global	_ZN15CIwSoundManager15s_ChannelsPCM16E
	.hidden	_ZN15CIwSoundManager15s_ChannelsADPCME
	.global	_ZN15CIwSoundManager15s_ChannelsADPCME
	.hidden	_ZTV15CIwSoundManager
	.global	_ZTV15CIwSoundManager
	.hidden	_ZTS15CIwSoundManager
	.global	_ZTS15CIwSoundManager
	.hidden	_ZTI15CIwSoundManager
	.global	_ZTI15CIwSoundManager
	.section	.rodata
	.align	3
	.set	.LANCHOR1,. + 0
	.type	_ZTV15CIwSoundManager, %object
	.size	_ZTV15CIwSoundManager, 16
_ZTV15CIwSoundManager:
	.word	0
	.word	_ZTI15CIwSoundManager
	.word	_ZN15CIwSoundManagerD1Ev
	.word	_ZN15CIwSoundManagerD0Ev
	.type	_ZTS15CIwSoundManager, %object
	.size	_ZTS15CIwSoundManager, 18
_ZTS15CIwSoundManager:
	.ascii	"15CIwSoundManager\000"
	.space	2
	.type	_ZTI15CIwSoundManager, %object
	.size	_ZTI15CIwSoundManager, 8
_ZTI15CIwSoundManager:
	.word	_ZTVN10__cxxabiv117__class_type_infoE+8
	.word	_ZTS15CIwSoundManager
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"SOUND\000"
	.space	2
.LC1:
	.ascii	"MaxChannels\000"
.LC2:
	.ascii	"CIwSoundData\000"
	.space	3
.LC3:
	.ascii	"CIwSoundDataADPCM\000"
	.space	2
.LC4:
	.ascii	"CIwSoundGroup\000"
	.space	2
.LC5:
	.ascii	"CIwSoundSpec\000"
	.bss
	.align	2
	.set	.LANCHOR0,. + 0
	.type	_ZN15CIwSoundManager14s_ChannelsPCM8E, %object
	.size	_ZN15CIwSoundManager14s_ChannelsPCM8E, 4
_ZN15CIwSoundManager14s_ChannelsPCM8E:
	.space	4
	.type	_ZN15CIwSoundManager15s_ChannelsPCM16E, %object
	.size	_ZN15CIwSoundManager15s_ChannelsPCM16E, 4
_ZN15CIwSoundManager15s_ChannelsPCM16E:
	.space	4
	.type	_ZN15CIwSoundManager15s_ChannelsADPCME, %object
	.size	_ZN15CIwSoundManager15s_ChannelsADPCME, 4
_ZN15CIwSoundManager15s_ChannelsADPCME:
	.space	4
	.type	g_IwSoundManager, %object
	.size	g_IwSoundManager, 4
g_IwSoundManager:
	.space	4
	.hidden	_ZTV15CIwSoundManager
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
