	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"QuGlobals.cpp"
	.section	.text._ZN8QuGlobal17getDeviceRotationEv,"ax",%progbits
	.align	2
	.global	_ZN8QuGlobal17getDeviceRotationEv
	.hidden	_ZN8QuGlobal17getDeviceRotationEv
	.type	_ZN8QuGlobal17getDeviceRotationEv, %function
_ZN8QuGlobal17getDeviceRotationEv:
	.fnstart
.LFB3704:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, [r0, #56]
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN8QuGlobal17getDeviceRotationEv, .-_ZN8QuGlobal17getDeviceRotationEv
	.section	.text._ZN8QuGlobal8setAccelE9QuVector3IfE,"ax",%progbits
	.align	2
	.global	_ZN8QuGlobal8setAccelE9QuVector3IfE
	.hidden	_ZN8QuGlobal8setAccelE9QuVector3IfE
	.type	_ZN8QuGlobal8setAccelE9QuVector3IfE, %function
_ZN8QuGlobal8setAccelE9QuVector3IfE:
	.fnstart
.LFB3707:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 16
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	sub	sp, sp, #16
	add	ip, sp, #4
	stmia	ip, {r1, r2, r3}
	add	r3, r0, #44
	ldmia	ip, {r0, r1, r2}
	stmia	r3, {r0, r1, r2}
	add	sp, sp, #16
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN8QuGlobal8setAccelE9QuVector3IfE, .-_ZN8QuGlobal8setAccelE9QuVector3IfE
	.section	.text._ZN8QuGlobal17setDeviceRotationE15GDeviceRotation,"ax",%progbits
	.align	2
	.global	_ZN8QuGlobal17setDeviceRotationE15GDeviceRotation
	.hidden	_ZN8QuGlobal17setDeviceRotationE15GDeviceRotation
	.type	_ZN8QuGlobal17setDeviceRotationE15GDeviceRotation, %function
_ZN8QuGlobal17setDeviceRotationE15GDeviceRotation:
	.fnstart
.LFB3708:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	str	r1, [r0, #56]
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN8QuGlobal17setDeviceRotationE15GDeviceRotation, .-_ZN8QuGlobal17setDeviceRotationE15GDeviceRotation
	.section	.text._ZN8QuGlobal13setScreenSizeEii,"ax",%progbits
	.align	2
	.global	_ZN8QuGlobal13setScreenSizeEii
	.hidden	_ZN8QuGlobal13setScreenSizeEii
	.type	_ZN8QuGlobal13setScreenSizeEii, %function
_ZN8QuGlobal13setScreenSizeEii:
	.fnstart
.LFB3709:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	str	r2, [r0, #32]
	str	r1, [r0, #28]
	bx	lr
	.cantunwind
	.fnend
	.size	_ZN8QuGlobal13setScreenSizeEii, .-_ZN8QuGlobal13setScreenSizeEii
	.section	.text._GLOBAL__I__ZN8QuGlobal17getDeviceRotationEv,"ax",%progbits
	.align	2
	.type	_GLOBAL__I__ZN8QuGlobal17getDeviceRotationEv, %function
_GLOBAL__I__ZN8QuGlobal17getDeviceRotationEv:
	.fnstart
.LFB5001:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	ldr	r4, .L11
	ldr	r5, .L11+4
	mov	r0, r4
	bl	_ZN4_STL8ios_base9_Loc_initC1Ev
	add	r6, r4, #4
	mov	r2, r5
	ldr	r1, .L11+8
	mov	r0, r4
	bl	__aeabi_atexit
	mov	r0, r6
	bl	_ZN4_STL8ios_base4InitC1Ev
	mov	r0, r6
	mov	r2, r5
	ldr	r1, .L11+12
	bl	__aeabi_atexit
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L12:
	.align	2
.L11:
	.word	.LANCHOR0
	.word	__dso_handle
	.word	_ZN4_STL8ios_base9_Loc_initD1Ev
	.word	_ZN4_STL8ios_base4InitD1Ev
	.fnend
	.size	_GLOBAL__I__ZN8QuGlobal17getDeviceRotationEv, .-_GLOBAL__I__ZN8QuGlobal17getDeviceRotationEv
	.section	.init_array,"aw",%init_array
	.align	2
	.word	_GLOBAL__I__ZN8QuGlobal17getDeviceRotationEv(target1)
	.global	__aeabi_i2f
	.global	__aeabi_fdiv
	.section	.text._ZN8QuGlobal8getAccelE15GDeviceRotation,"ax",%progbits
	.align	2
	.global	_ZN8QuGlobal8getAccelE15GDeviceRotation
	.hidden	_ZN8QuGlobal8getAccelE15GDeviceRotation
	.type	_ZN8QuGlobal8getAccelE15GDeviceRotation, %function
_ZN8QuGlobal8getAccelE15GDeviceRotation:
	.fnstart
.LFB3705:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, lr}
	.save {r3, r4, r5, r6, r7, lr}
	mov	r4, r0
	bl	s3eAccelerometerGetY
	bl	__aeabi_i2f
	mov	r3, #1140850688
	add	r1, r3, #7995392
	bl	__aeabi_fdiv
	mov	r5, r0
	bl	s3eAccelerometerGetX
	bl	__aeabi_i2f
	mov	r2, #1140850688
	add	r1, r2, #7995392
	bl	__aeabi_fdiv
	mov	r6, r0
	bl	s3eAccelerometerGetZ
	mov	r7, r0
	mov	r0, #11
	bl	s3eSurfaceGetInt
	cmp	r0, #2
	addeq	r6, r6, #-2147483648
	addeq	r5, r5, #-2147483648
	str	r6, [r4, #0]	@ float
	str	r5, [r4, #4]	@ float
	mov	r0, r7
	bl	__aeabi_i2f
	mov	r1, #1140850688
	add	r1, r1, #7995392
	bl	__aeabi_fdiv
	str	r0, [r4, #8]	@ float
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, r6, r7, lr}
	bx	lr
	.fnend
	.size	_ZN8QuGlobal8getAccelE15GDeviceRotation, .-_ZN8QuGlobal8getAccelE15GDeviceRotation
	.section	.text.T.837,"ax",%progbits
	.align	2
	.type	T.837, %function
T.837:
	.fnstart
.LFB5002:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	cmp	r0, #0
	.pad #12
	sub	sp, sp, #12
	str	r0, [sp, #4]
	beq	.L36
.L45:
	ldr	r3, [sp, #4]
	ldr	fp, [r3, #12]
	cmp	fp, #0
	beq	.L19
.L44:
	ldr	r4, [fp, #12]
	cmp	r4, #0
	beq	.L20
.L43:
	ldr	r5, [r4, #12]
	cmp	r5, #0
	beq	.L21
.L42:
	ldr	r6, [r5, #12]
	cmp	r6, #0
	beq	.L22
.L41:
	ldr	r7, [r6, #12]
	cmp	r7, #0
	beq	.L23
.L40:
	ldr	r8, [r7, #12]
	cmp	r8, #0
	beq	.L24
.L39:
	ldr	sl, [r8, #12]
	cmp	sl, #0
	beq	.L25
.L38:
	ldr	r9, [sl, #12]
	cmp	r9, #0
	beq	.L26
	b	.L37
.L47:
	mov	r9, r3
.L37:
	ldr	r0, [r9, #12]
	bl	T.837
	ldr	r1, [r9, #8]
	mov	r0, r9
	str	r1, [sp, #0]
	bl	free
	ldr	r3, [sp, #0]
	cmp	r3, #0
	bne	.L47
.L26:
	ldr	r9, [sl, #8]
	mov	r0, sl
	bl	free
	cmp	r9, #0
	movne	sl, r9
	bne	.L38
.L25:
	ldr	sl, [r8, #8]
	mov	r0, r8
	bl	free
	cmp	sl, #0
	movne	r8, sl
	bne	.L39
.L24:
	ldr	r8, [r7, #8]
	mov	r0, r7
	bl	free
	cmp	r8, #0
	movne	r7, r8
	bne	.L40
.L23:
	ldr	r7, [r6, #8]
	mov	r0, r6
	bl	free
	cmp	r7, #0
	movne	r6, r7
	bne	.L41
.L22:
	ldr	r6, [r5, #8]
	mov	r0, r5
	bl	free
	cmp	r6, #0
	movne	r5, r6
	bne	.L42
.L21:
	ldr	r5, [r4, #8]
	mov	r0, r4
	bl	free
	cmp	r5, #0
	movne	r4, r5
	bne	.L43
.L20:
	ldr	r4, [fp, #8]
	mov	r0, fp
	bl	free
	cmp	r4, #0
	movne	fp, r4
	bne	.L44
.L19:
	ldr	r2, [sp, #4]
	ldr	r4, [r2, #8]
	mov	r0, r2
	bl	free
	cmp	r4, #0
	strne	r4, [sp, #4]
	bne	.L45
.L36:
	add	sp, sp, #12
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
	.fnend
	.size	T.837, .-T.837
	.section	.text.T.839,"ax",%progbits
	.align	2
	.type	T.839, %function
T.839:
	.fnstart
.LFB5004:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	.save {r4, r5, r6, r7, r8, lr}
	ldr	r4, .L63
	ldr	ip, [r4, #0]
	cmp	r2, ip
	mov	r5, r2
	mov	r6, r0
	mov	r8, r3
	beq	.L49
	ldr	r3, [sp, #24]
	cmp	r3, #0
	beq	.L59
.L50:
	mov	r0, #24
	bl	malloc
	subs	r7, r0, #0
	beq	.L56
.L57:
	ldmia	r8, {ip, lr}	@ phole ldm
	str	lr, [r7, #20]
	str	ip, [r7, #16]
	str	r7, [r5, #12]
	ldr	r0, [r4, #0]
	ldr	r1, [r0, #12]
	cmp	r5, r1
	streq	r7, [r0, #12]
.L53:
	mov	r1, #0
	str	r1, [r7, #12]
	str	r1, [r7, #8]
	str	r5, [r7, #4]
	ldr	r0, [r4, #0]
	add	r1, r0, #4
	mov	r0, r7
	bl	_ZN4_STL10_Rb_globalIbE10_RebalanceEPNS_18_Rb_tree_node_baseERS3_
	ldr	r2, [r4, #4]
	add	r3, r2, #1
	str	r3, [r4, #4]
	mov	r0, r6
	str	r7, [r6, #0]
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
.L59:
	cmp	r1, #0
	beq	.L60
.L49:
	mov	r0, #24
	bl	malloc
	subs	r7, r0, #0
	beq	.L61
.L51:
	ldr	r1, [r8, #4]
	ldr	lr, [r8, #0]
	str	r1, [r7, #20]
	str	lr, [r7, #16]
	str	r7, [r5, #8]
	ldr	r3, [r4, #0]
	cmp	r5, r3
	beq	.L62
	ldr	r2, [r3, #8]
	cmp	r5, r2
	streq	r7, [r3, #8]
	b	.L53
.L62:
	str	r7, [r5, #4]
	ldr	r3, .L63
	ldr	ip, [r3, #0]
	str	r7, [ip, #12]
	b	.L53
.L56:
	mov	r0, #24
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
	mov	r7, r0
	b	.L57
.L61:
	mov	r0, #24
	bl	_ZN4_STL14__malloc_allocILi0EE13_S_oom_mallocEj
	mov	r7, r0
	b	.L51
.L60:
	ldr	r2, [r8, #0]
	ldr	r0, [r5, #16]
	cmp	r2, r0
	bcs	.L50
	b	.L49
.L64:
	.align	2
.L63:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	T.839, .-T.839
	.section	.text.T.840,"ax",%progbits
	.align	2
	.type	T.840, %function
T.840:
	.fnstart
.LFB5005:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	.save {r4, r5, r6, lr}
	ldr	r3, .L88
	ldr	r3, [r3, #0]
	ldr	r6, [r3, #4]
	cmp	r6, #0
	.pad #16
	sub	sp, sp, #16
	mov	r4, r0
	mov	r5, r1
	beq	.L66
	ldr	r0, [r1, #0]
	b	.L70
.L85:
	mov	r6, r2
.L70:
	ldr	r2, [r6, #16]
	cmp	r2, r0
	ldrhi	r2, [r6, #8]
	ldrls	r2, [r6, #12]
	movhi	r1, #1
	movls	r1, #0
	cmp	r2, #0
	bne	.L85
	cmp	r1, #0
	moveq	r0, r6
	beq	.L73
.L72:
	ldr	r0, [r3, #8]
	cmp	r6, r0
	beq	.L86
	mov	r0, r6
	bl	_ZN4_STL10_Rb_globalIbE12_M_decrementEPNS_18_Rb_tree_node_baseE
.L73:
	ldr	r3, [r5, #0]
	ldr	r1, [r0, #16]
	cmp	r1, r3
	movcs	r3, #0
	strcs	r0, [r4, #0]
	strcsb	r3, [r4, #4]
	bcc	.L87
.L65:
	mov	r0, r4
	add	sp, sp, #16
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L87:
	mov	lr, #0
	mov	r2, r6
	add	r0, sp, #12
	mov	r1, lr
	mov	r3, r5
	str	lr, [sp, #0]
	bl	T.839
	ldr	r2, [sp, #12]
	mov	r0, #1
	str	r2, [r4, #0]
	strb	r0, [r4, #4]
	b	.L65
.L86:
	mov	r1, r6
	mov	r2, r6
	mov	ip, #0
	mov	r3, r5
	add	r0, sp, #8
	str	ip, [sp, #0]
	bl	T.839
	ldr	r2, [sp, #8]
	mov	r1, #1
	str	r2, [r4, #0]
	strb	r1, [r4, #4]
	b	.L65
.L66:
	mov	r6, r3
	b	.L72
.L89:
	.align	2
.L88:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	T.840, .-T.840
	.section	.text.T.838,"ax",%progbits
	.align	2
	.type	T.838, %function
T.838:
	.fnstart
.LFB5003:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, lr}
	.save {r4, r5, r6, r7, lr}
	ldr	r6, .L124
	ldr	r3, [r6, #0]
	ldr	ip, [r1, #0]
	mov	r7, r1
	ldr	r1, [r3, #8]
	cmp	ip, r1
	.pad #44
	sub	sp, sp, #44
	mov	r5, r2
	mov	r4, r0
	beq	.L118
	cmp	ip, r3
	beq	.L119
	mov	r0, ip
	bl	_ZN4_STL10_Rb_globalIbE12_M_decrementEPNS_18_Rb_tree_node_baseE
	mov	ip, r0
	ldr	r0, [r7, #0]
	ldr	r3, [r5, #0]
	ldr	r2, [r0, #16]
	cmp	r3, r2
	bcs	.L105
	ldr	r1, [ip, #16]
	cmp	r3, r1
	bls	.L106
.L109:
	ldr	r1, [ip, #12]
	cmp	r1, #0
	beq	.L116
.L111:
	mov	r1, r0
	mov	ip, #0
	mov	r3, r5
	mov	r0, r4
	mov	r2, r1
	str	ip, [sp, #0]
	bl	T.839
	b	.L90
.L105:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	ip, [r7, #0]
	ldr	r3, [r5, #0]
	ldr	r2, [ip, #16]
	cmp	r2, r3
	bcs	.L108
	ldr	lr, [r6, #0]
	cmp	r0, lr
	beq	.L109
	ldr	r1, [r0, #16]
	cmp	r3, r1
	bcs	.L110
	b	.L109
.L118:
	ldr	r3, [r6, #4]
	cmp	r3, #0
	beq	.L120
	ldr	r2, [r2, #0]
	ldr	r1, [ip, #16]
	cmp	r2, r1
	bcc	.L121
	bhi	.L96
.L108:
	str	ip, [r4, #0]
.L90:
	mov	r0, r4
	add	sp, sp, #44
	ldmfd	sp!, {r4, r5, r6, r7, lr}
	bx	lr
.L106:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
.L110:
	add	r0, sp, #32
	mov	r1, r5
	bl	T.840
	ldr	r0, [sp, #32]
	str	r0, [r4, #0]
	b	.L90
.L119:
	ldr	r2, [ip, #12]
	ldr	r3, [r5, #0]
	ldr	lr, [r2, #16]
	cmp	lr, r3
	bcc	.L122
	mov	r1, r5
	add	r0, sp, #24
	bl	T.840
	ldr	ip, [sp, #24]
	str	ip, [r4, #0]
	b	.L90
.L96:
	mov	r0, ip
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r3, [r6, #0]
	cmp	r0, r3
	beq	.L123
	ldr	r2, [r5, #0]
	ldr	r1, [r0, #16]
	cmp	r2, r1
	bcs	.L99
	ldr	ip, [r7, #0]
	ldr	r1, [ip, #12]
	cmp	r1, #0
	bne	.L111
.L116:
	mov	r2, ip
	mov	r3, r5
	mov	r0, r4
	str	ip, [sp, #0]
	bl	T.839
	b	.L90
.L122:
	mov	r3, r5
	mov	r1, #0
	str	ip, [sp, #0]
	bl	T.839
	b	.L90
.L120:
	add	r0, sp, #8
	mov	r1, r2
	bl	T.840
	ldr	r0, [sp, #8]
	str	r0, [r4, #0]
	b	.L90
.L121:
	mov	r1, ip
	mov	r2, ip
	mov	r3, r5
	mov	ip, #0
	str	ip, [sp, #0]
	bl	T.839
	b	.L90
.L99:
	add	r0, sp, #16
	mov	r1, r5
	bl	T.840
	ldr	r0, [sp, #16]
	str	r0, [r4, #0]
	b	.L90
.L123:
	ldr	lr, [r7, #0]
	mov	r3, r5
	mov	r2, lr
	mov	r0, r4
	mov	r1, #0
	str	lr, [sp, #0]
	bl	T.839
	b	.L90
.L125:
	.align	2
.L124:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	T.838, .-T.838
	.section	.text._ZN8QuGlobal10setManagerE15QuStupidPointerI13QuBaseManagerE,"ax",%progbits
	.align	2
	.global	_ZN8QuGlobal10setManagerE15QuStupidPointerI13QuBaseManagerE
	.hidden	_ZN8QuGlobal10setManagerE15QuStupidPointerI13QuBaseManagerE
	.type	_ZN8QuGlobal10setManagerE15QuStupidPointerI13QuBaseManagerE, %function
_ZN8QuGlobal10setManagerE15QuStupidPointerI13QuBaseManagerE:
	.fnstart
.LFB3706:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 32
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	.save {r4, r5, r6, r7, r8, r9, sl, lr}
	ldr	ip, [r0, #4]
	cmp	ip, #0
	.pad #32
	sub	sp, sp, #32
	mov	r6, r0
	ldmia	r1, {r4, sl}	@ phole ldm
	beq	.L127
	ldr	r5, .L197
	ldr	r0, [r5, #0]
	ldr	r3, [r0, #4]
	cmp	r3, #0
	moveq	r3, r0
	beq	.L131
	mov	r1, r0
	b	.L132
.L187:
	mov	r1, r3
	mov	r3, r2
.L132:
	ldr	r2, [r3, #16]
	cmp	ip, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r1
	cmp	r2, #0
	bne	.L187
.L131:
	cmp	r3, r0
	beq	.L134
	ldr	r2, [r3, #16]
	cmp	ip, r2
	mov	r2, r3
	bcs	.L135
.L134:
	add	r2, sp, #8
	str	r3, [sp, #24]
	add	r0, sp, #28
	mov	r3, #0
	add	r1, sp, #24
	str	ip, [sp, #8]
	str	r3, [sp, #12]
	bl	T.838
	ldr	r2, [sp, #28]
.L135:
	ldr	r1, [r2, #20]
	sub	r0, r1, #1
	cmp	r0, #0
	str	r0, [r2, #20]
	beq	.L188
.L127:
	cmp	r4, #0
	stmib	r6, {r4, sl}	@ phole stm
	beq	.L181
	ldr	r5, .L197
	ldr	r3, [r5, #0]
	ldr	ip, [r3, #4]
	cmp	ip, #0
	beq	.L161
	mov	r0, r3
	mov	r2, ip
	b	.L165
.L189:
	mov	r0, r2
	mov	r2, r1
.L165:
	ldr	r1, [r2, #16]
	cmp	r4, r1
	ldrhi	r1, [r2, #12]
	ldrls	r1, [r2, #8]
	movhi	r2, r0
	cmp	r1, #0
	bne	.L189
	cmp	r3, r2
	beq	.L161
	ldr	r0, [r2, #16]
	cmp	r4, r0
	bcc	.L161
.L166:
	cmp	r2, r3
	mov	r1, r2
	beq	.L186
.L168:
	ldr	ip, [r1, #20]
	add	r0, ip, #1
	str	r0, [r1, #20]
.L181:
	add	sp, sp, #32
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, lr}
	bx	lr
.L190:
	ldr	r1, [ip, #16]
	cmp	r4, r1
	movls	r3, ip
	ldrhi	ip, [ip, #12]
	ldrls	ip, [ip, #8]
.L186:
	cmp	ip, #0
	bne	.L190
	cmp	r2, r3
	beq	.L174
	ldr	ip, [r3, #16]
	cmp	r4, ip
	mov	r2, r3
	bcs	.L175
.L174:
	mov	r2, sp
	mov	lr, #0
	add	r0, sp, #20
	add	r1, sp, #16
	str	r3, [sp, #16]
	stmia	sp, {r4, lr}	@ phole stm
	bl	T.838
	ldr	r2, [sp, #20]
.L175:
	mov	r3, #0
	str	r3, [r2, #20]
	ldr	r1, [r5, #0]
	ldr	r3, [r1, #4]
	cmp	r3, #0
	beq	.L168
	mov	r0, r1
	b	.L180
.L191:
	mov	r0, r3
	mov	r3, r2
.L180:
	ldr	r2, [r3, #16]
	cmp	r4, r2
	ldrhi	r2, [r3, #12]
	ldrls	r2, [r3, #8]
	movhi	r3, r0
	cmp	r2, #0
	bne	.L191
	cmp	r1, r3
	beq	.L168
	ldr	r2, [r3, #16]
	cmp	r4, r2
	movcs	r1, r3
	b	.L168
.L161:
	mov	r2, r3
	b	.L166
.L188:
	ldr	r1, [r5, #0]
	ldr	r7, [r1, #4]
	cmp	r7, #0
	ldr	r2, [r6, #4]
	moveq	r8, r1
	moveq	r9, r1
	beq	.L147
	mov	r0, r1
	mov	r9, r7
	b	.L140
.L192:
	mov	r0, r9
	mov	r9, r3
.L140:
	ldr	r3, [r9, #16]
	cmp	r2, r3
	ldrhi	r3, [r9, #12]
	ldrls	r3, [r9, #8]
	movhi	r9, r0
	cmp	r3, #0
	bne	.L192
	mov	r0, r1
	b	.L144
.L193:
	mov	r0, r7
	mov	r7, r3
.L144:
	ldr	r3, [r7, #16]
	cmp	r2, r3
	ldrcs	r3, [r7, #12]
	ldrcc	r3, [r7, #8]
	movcs	r7, r0
	cmp	r3, #0
	bne	.L193
	cmp	r9, r7
	mov	r8, r9
	moveq	r9, r7
	beq	.L147
	mov	r0, r9
.L148:
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	cmp	r7, r0
	bne	.L148
	ldr	r1, [r5, #0]
	mov	r8, r7
.L147:
	ldr	ip, [r1, #8]
	cmp	r9, ip
	beq	.L194
.L150:
	cmp	r9, r8
	bne	.L183
.L153:
	ldr	r0, [r6, #8]
	cmp	r0, #1
	ldr	r3, [r6, #4]
	beq	.L195
.L157:
	cmp	r3, #0
	beq	.L127
	ldr	ip, [r3, #-4]
	add	r0, r3, ip, asl #2
	cmp	r3, r0
	bne	.L184
	b	.L158
.L196:
	mov	r0, r5
.L184:
	sub	r5, r0, #4
	ldr	r2, [r0, #-4]
	mov	r0, r5
	ldr	ip, [r2, #0]
	mov	lr, pc
	bx	ip
	ldr	r0, [r6, #4]
	cmp	r0, r5
	bne	.L196
.L158:
	sub	r0, r0, #8
	bl	_ZdaPv
	b	.L127
.L156:
	mov	r9, r7
.L183:
	mov	r0, r9
	bl	_ZN4_STL10_Rb_globalIbE12_M_incrementEPNS_18_Rb_tree_node_baseE
	ldr	r2, [r5, #0]
	mov	r7, r0
	add	r3, r2, #12
	add	r1, r2, #4
	mov	r0, r9
	add	r2, r2, #8
	bl	_ZN4_STL10_Rb_globalIbE20_Rebalance_for_eraseEPNS_18_Rb_tree_node_baseERS3_S4_S4_
	cmp	r0, #0
	blne	free
.L155:
	ldr	r3, [r5, #4]
	cmp	r7, r8
	sub	lr, r3, #1
	str	lr, [r5, #4]
	bne	.L156
	ldr	r0, [r6, #8]
	cmp	r0, #1
	ldr	r3, [r6, #4]
	bne	.L157
.L195:
	cmp	r3, #0
	beq	.L127
	mov	r0, r3
	ldr	r1, [r3, #0]
	ldr	ip, [r1, #4]
	mov	lr, pc
	bx	ip
	b	.L127
.L194:
	cmp	r8, r1
	bne	.L150
	ldr	r2, [r5, #4]
	cmp	r2, #0
	ldr	r5, .L197
	beq	.L153
	ldr	r0, [r8, #4]
	bl	T.837
	ldr	ip, [r5, #0]
	str	ip, [ip, #8]
	ldr	r1, [r5, #0]
	mov	r3, #0
	str	r3, [r1, #4]
	ldr	r0, [r5, #0]
	str	r0, [r0, #12]
	str	r3, [r5, #4]
	b	.L153
.L198:
	.align	2
.L197:
	.word	_ZN21QuStupidPointerHelper6mSPMapE
	.fnend
	.size	_ZN8QuGlobal10setManagerE15QuStupidPointerI13QuBaseManagerE, .-_ZN8QuGlobal10setManagerE15QuStupidPointerI13QuBaseManagerE
	.bss
	.align	2
	.set	.LANCHOR0,. + 0
	.type	_ZN4_STLL8_LocInitE, %object
	.size	_ZN4_STLL8_LocInitE, 1
_ZN4_STLL8_LocInitE:
	.space	1
	.space	3
	.type	_ZN4_STLL8_IosInitE, %object
	.size	_ZN4_STLL8_IosInitE, 1
_ZN4_STLL8_IosInitE:
	.space	1
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
