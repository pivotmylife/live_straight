	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"compress.c"
	.section	.text.compressBound,"ax",%progbits
	.align	2
	.global	compressBound
	.hidden	compressBound
	.type	compressBound, %function
compressBound:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	add	r1, r0, #11
	add	r3, r1, r0, lsr #12
	add	r0, r3, r0, lsr #14
	bx	lr
	.size	compressBound, .-compressBound
	.section	.text.compress2,"ax",%progbits
	.align	2
	.global	compress2
	.hidden	compress2
	.type	compress2, %function
compress2:
	@ Function supports interworking.
	@ args = 4, pretend = 0, frame = 56
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	sub	sp, sp, #56
	ldr	lr, [r1, #0]
	mov	ip, #0
	stmia	sp, {r2, r3}	@ phole stm
	str	r0, [sp, #12]
	mov	r5, r1
	mov	r0, sp
	ldr	r1, [sp, #72]
	ldr	r2, .L10
	mov	r3, #56
	str	lr, [sp, #16]
	str	ip, [sp, #40]
	str	ip, [sp, #32]
	str	ip, [sp, #36]
	bl	deflateInit_
	cmp	r0, #0
	mov	r4, sp
	beq	.L9
.L5:
	add	sp, sp, #56
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L9:
	mov	r0, sp
	mov	r1, #4
	bl	deflate
	cmp	r0, #1
	mov	r6, r0
	beq	.L6
	mov	r0, sp
	bl	deflateEnd
	cmp	r6, #0
	movne	r0, r6
	mvneq	r0, #4
	b	.L5
.L6:
	ldr	r3, [sp, #20]
	mov	r0, sp
	str	r3, [r5, #0]
	bl	deflateEnd
	b	.L5
.L11:
	.align	2
.L10:
	.word	.LC0
	.size	compress2, .-compress2
	.section	.text.compress,"ax",%progbits
	.align	2
	.global	compress
	.hidden	compress
	.type	compress, %function
compress:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 56
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	sub	sp, sp, #56
	ldr	lr, [r1, #0]
	mov	ip, #0
	stmia	sp, {r2, r3}	@ phole stm
	str	r0, [sp, #12]
	mov	r5, r1
	mov	r0, sp
	mvn	r1, #0
	ldr	r2, .L18
	mov	r3, #56
	str	lr, [sp, #16]
	str	ip, [sp, #40]
	str	ip, [sp, #32]
	str	ip, [sp, #36]
	bl	deflateInit_
	cmp	r0, #0
	mov	r4, sp
	beq	.L17
.L14:
	add	sp, sp, #56
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L17:
	mov	r0, sp
	mov	r1, #4
	bl	deflate
	cmp	r0, #1
	mov	r6, r0
	beq	.L15
	mov	r0, sp
	bl	deflateEnd
	cmp	r6, #0
	movne	r0, r6
	mvneq	r0, #4
	b	.L14
.L15:
	ldr	r3, [sp, #20]
	mov	r0, sp
	str	r3, [r5, #0]
	bl	deflateEnd
	b	.L14
.L19:
	.align	2
.L18:
	.word	.LC0
	.size	compress, .-compress
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"1.2.3\000"
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
