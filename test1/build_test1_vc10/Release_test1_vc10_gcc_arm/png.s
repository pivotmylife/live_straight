	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"png.c"
	.section	.text.png_get_io_ptr,"ax",%progbits
	.align	2
	.global	png_get_io_ptr
	.hidden	png_get_io_ptr
	.type	png_get_io_ptr, %function
png_get_io_ptr:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	ldrne	r0, [r0, #112]
	bx	lr
	.size	png_get_io_ptr, .-png_get_io_ptr
	.section	.text.png_init_io,"ax",%progbits
	.align	2
	.global	png_init_io
	.hidden	png_init_io
	.type	png_init_io, %function
png_init_io:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	strne	r1, [r0, #112]
	bx	lr
	.size	png_init_io, .-png_init_io
	.section	.text.png_get_copyright,"ax",%progbits
	.align	2
	.global	png_get_copyright
	.hidden	png_get_copyright
	.type	png_get_copyright, %function
png_get_copyright:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L10
	bx	lr
.L11:
	.align	2
.L10:
	.word	.LC0
	.size	png_get_copyright, .-png_get_copyright
	.section	.text.png_get_libpng_ver,"ax",%progbits
	.align	2
	.global	png_get_libpng_ver
	.hidden	png_get_libpng_ver
	.type	png_get_libpng_ver, %function
png_get_libpng_ver:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L14
	bx	lr
.L15:
	.align	2
.L14:
	.word	.LC1
	.size	png_get_libpng_ver, .-png_get_libpng_ver
	.section	.text.png_get_header_ver,"ax",%progbits
	.align	2
	.global	png_get_header_ver
	.hidden	png_get_header_ver
	.type	png_get_header_ver, %function
png_get_header_ver:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L18
	bx	lr
.L19:
	.align	2
.L18:
	.word	.LC1
	.size	png_get_header_ver, .-png_get_header_ver
	.section	.text.png_get_header_version,"ax",%progbits
	.align	2
	.global	png_get_header_version
	.hidden	png_get_header_version
	.type	png_get_header_version, %function
png_get_header_version:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	ldr	r0, .L22
	bx	lr
.L23:
	.align	2
.L22:
	.word	.LC2
	.size	png_get_header_version, .-png_get_header_version
	.section	.text.png_access_version_number,"ax",%progbits
	.align	2
	.global	png_access_version_number
	.hidden	png_access_version_number
	.type	png_access_version_number, %function
png_access_version_number:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mov	r0, #10176
	add	r0, r0, #58
	bx	lr
	.size	png_access_version_number, .-png_access_version_number
	.section	.text.png_mmx_support,"ax",%progbits
	.align	2
	.global	png_mmx_support
	.hidden	png_mmx_support
	.type	png_mmx_support, %function
png_mmx_support:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	mvn	r0, #0
	bx	lr
	.size	png_mmx_support, .-png_mmx_support
	.section	.text.png_64bit_product,"ax",%progbits
	.align	2
	.global	png_64bit_product
	.hidden	png_64bit_product
	.type	png_64bit_product, %function
png_64bit_product:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, r5, r6}
	mov	r4, #65536
	sub	ip, r4, #1
	and	r4, r1, ip
	mov	r5, r0, lsr #16
	mul	r6, r4, r5
	and	r0, r0, ip
	mov	r1, r1, lsr #16
	mla	r6, r0, r1, r6
	mul	r4, r0, r4
	mul	r1, r5, r1
	add	r0, r6, r4, lsr #16
	and	ip, r4, ip
	orr	ip, ip, r0, asl #16
	add	r0, r1, r0, lsr #16
	str	r0, [r2, #0]
	str	ip, [r3, #0]
	ldmfd	sp!, {r4, r5, r6}
	bx	lr
	.size	png_64bit_product, .-png_64bit_product
	.section	.text.png_data_freer,"ax",%progbits
	.align	2
	.global	png_data_freer
	.hidden	png_data_freer
	.type	png_data_freer, %function
png_data_freer:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	cmpne	r1, #0
	stmfd	sp!, {r3, lr}
	beq	.L34
	cmp	r2, #1
	beq	.L36
	cmp	r2, #2
	beq	.L37
	ldr	r1, .L38
	bl	png_warning
.L34:
	ldmfd	sp!, {r3, lr}
	bx	lr
.L36:
	ldr	r2, [r1, #184]
	orr	r3, r2, r3
	str	r3, [r1, #184]
	b	.L34
.L37:
	ldr	ip, [r1, #184]
	bic	r0, ip, r3
	str	r0, [r1, #184]
	b	.L34
.L39:
	.align	2
.L38:
	.word	.LC3
	.size	png_data_freer, .-png_data_freer
	.section	.text.png_reset_zstream,"ax",%progbits
	.align	2
	.global	png_reset_zstream
	.hidden	png_reset_zstream
	.type	png_reset_zstream, %function
png_reset_zstream:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	stmfd	sp!, {r3, lr}
	mvneq	r0, #1
	addne	r0, r0, #144
	blne	inflateReset
.L42:
	ldmfd	sp!, {r3, lr}
	bx	lr
	.size	png_reset_zstream, .-png_reset_zstream
	.section	.text.png_handle_as_unknown,"ax",%progbits
	.align	2
	.global	png_handle_as_unknown
	.hidden	png_handle_as_unknown
	.type	png_handle_as_unknown, %function
png_handle_as_unknown:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	cmpne	r1, #0
	stmfd	sp!, {r4, r5, r6, r7, r8, lr}
	mov	r5, r1
	beq	.L45
	ldr	r6, [r0, #572]
	cmp	r6, #0
	ble	.L45
	sub	r7, r6, #1
	ldr	r3, [r0, #576]
	add	r4, r7, r7, asl #2
	ands	r8, r7, #3
	add	r4, r4, r3
	beq	.L48
	mov	r0, r1
	mov	r2, #4
	mov	r1, r4
	bl	memcmp
	cmp	r0, #0
	beq	.L67
	cmp	r8, #1
	mov	r6, r7
	sub	r4, r4, #5
	beq	.L48
	cmp	r8, #2
	beq	.L65
	mov	r0, r5
	mov	r1, r4
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	beq	.L67
	sub	r6, r7, #1
	sub	r4, r4, #5
.L65:
	mov	r1, r4
	mov	r0, r5
	mov	r2, #4
	bl	memcmp
	cmp	r0, #0
	subne	r6, r6, #1
	subne	r4, r4, #5
	beq	.L67
.L48:
	mov	r2, #4
	mov	r1, r4
	mov	r0, r5
	bl	memcmp
	cmp	r0, #0
	mov	r2, #4
	mov	r0, r5
	beq	.L67
.L46:
	sub	r4, r4, #5
	cmp	r6, #1
	mov	r1, r4
	beq	.L45
	bl	memcmp
	cmp	r0, #0
	mov	r2, #4
	mov	r0, r5
	mov	r7, r4
	beq	.L67
	sub	r4, r4, #5
	mov	r1, r4
	bl	memcmp
	cmp	r0, #0
	mov	r2, #4
	mov	r0, r5
	beq	.L67
	sub	r4, r7, #10
	mov	r1, r4
	bl	memcmp
	cmp	r0, #0
	beq	.L67
	sub	r4, r7, #15
	mov	r2, #4
	mov	r1, r4
	mov	r0, r5
	bl	memcmp
	cmp	r0, #0
	sub	r6, r6, #4
	mov	r2, #4
	mov	r0, r5
	bne	.L46
.L67:
	ldrb	r0, [r4, #4]	@ zero_extendqisi2
	b	.L47
.L45:
	mov	r0, #0
.L47:
	ldmfd	sp!, {r4, r5, r6, r7, r8, lr}
	bx	lr
	.size	png_handle_as_unknown, .-png_handle_as_unknown
	.section	.text.png_sig_cmp,"ax",%progbits
	.align	2
	.global	png_sig_cmp
	.hidden	png_sig_cmp
	.type	png_sig_cmp, %function
png_sig_cmp:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	ldr	r3, .L75
	sub	sp, sp, #8
	mov	r4, r0
	mov	ip, r1
	ldmia	r3, {r0, r1}
	cmp	r2, #8
	mov	r3, sp
	stmia	r3, {r0, r1}
	movhi	r2, #8
	bhi	.L70
	cmp	r2, #0
	bne	.L70
.L71:
	mvn	r0, #0
.L73:
	add	sp, sp, #8
	ldmfd	sp!, {r4, lr}
	bx	lr
.L70:
	cmp	ip, #7
	bhi	.L71
	add	r1, r2, ip
	cmp	r1, #8
	rsbhi	r2, ip, #8
	add	r1, r3, ip
	add	r0, r4, ip
	bl	memcmp
	b	.L73
.L76:
	.align	2
.L75:
	.word	.LANCHOR0
	.size	png_sig_cmp, .-png_sig_cmp
	.global	__aeabi_uidiv
	.section	.text.png_zalloc,"ax",%progbits
	.align	2
	.global	png_zalloc
	.hidden	png_zalloc
	.type	png_zalloc, %function
png_zalloc:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, lr}
	mov	r4, r0
	mov	r6, r1
	mvn	r0, #0
	mov	r1, r2
	mov	r5, r2
	bl	__aeabi_uidiv
	cmp	r0, r6
	ldr	r7, [r4, #136]
	bcc	.L81
	orr	r3, r7, #1048576
	str	r3, [r4, #136]
	mul	r1, r6, r5
	mov	r0, r4
	bl	png_malloc
	str	r7, [r4, #136]
.L79:
	ldmfd	sp!, {r3, r4, r5, r6, r7, lr}
	bx	lr
.L81:
	mov	r0, r4
	ldr	r1, .L82
	bl	png_warning
	mov	r0, #0
	b	.L79
.L83:
	.align	2
.L82:
	.word	.LC4
	.size	png_zalloc, .-png_zalloc
	.section	.text.png_convert_to_rfc1123,"ax",%progbits
	.align	2
	.global	png_convert_to_rfc1123
	.hidden	png_convert_to_rfc1123
	.type	png_convert_to_rfc1123, %function
png_convert_to_rfc1123:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	subs	r4, r0, #0
	sub	sp, sp, #28
	mov	r6, r1
	moveq	r0, r4
	beq	.L86
	ldr	r2, [r4, #556]
	cmp	r2, #0
	beq	.L89
.L87:
	ldrb	r0, [r6, #6]	@ zero_extendqisi2
	ldrb	r1, [r6, #2]	@ zero_extendqisi2
	ldr	r3, .L90
	ldr	r7, .L90+4
	ldrb	sl, [r6, #4]	@ zero_extendqisi2
	ldrb	r8, [r6, #5]	@ zero_extendqisi2
	sub	r1, r1, #1
	umull	ip, r3, r0, r3
	ldr	r5, .L90+8
	ldr	lr, .L90+12
	smull	ip, r7, r1, r7
	umull	ip, r5, sl, r5
	umull	ip, lr, r8, lr
	mov	r3, r3, lsr #4
	mov	ip, r1, asr #31
	rsb	r7, ip, r7, asr #1
	rsb	ip, r3, r3, asl #4
	add	ip, r3, ip, asl #2
	mov	r5, r5, lsr #4
	mov	lr, lr, lsr #5
	add	r7, r7, r7, asl #1
	ldrb	r3, [r6, #3]	@ zero_extendqisi2
	sub	r7, r1, r7, asl #2
	add	r5, r5, r5, asl #1
	rsb	r1, ip, r0
	rsb	lr, lr, lr, asl #4
	ldr	r0, .L90+16
	ldrh	r6, [r6, #0]
	sub	r5, sl, r5, asl #3
	sub	lr, r8, lr, asl #2
	and	ip, r1, #255
	add	r7, r0, r7, asl #2
	and	lr, lr, #255
	mov	r0, r2
	and	r5, r5, #255
	and	r3, r3, #31
	mov	r1, #29
	ldr	r2, .L90+20
	str	r7, [sp, #0]
	str	r6, [sp, #4]
	str	r5, [sp, #8]
	str	lr, [sp, #12]
	str	ip, [sp, #16]
	bl	snprintf
	ldr	r0, [r4, #556]
.L86:
	add	sp, sp, #28
	ldmfd	sp!, {r4, r5, r6, r7, r8, sl, lr}
	bx	lr
.L89:
	mov	r1, #29
	bl	png_malloc
	mov	r2, r0
	str	r0, [r4, #556]
	b	.L87
.L91:
	.align	2
.L90:
	.word	1126548799
	.word	715827883
	.word	-1431655765
	.word	-2004318071
	.word	.LANCHOR0+8
	.word	.LC5
	.size	png_convert_to_rfc1123, .-png_convert_to_rfc1123
	.section	.text.png_zfree,"ax",%progbits
	.align	2
	.global	png_zfree
	.hidden	png_zfree
	.type	png_zfree, %function
png_zfree:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, lr}
	bl	png_free
	ldmfd	sp!, {r3, lr}
	bx	lr
	.size	png_zfree, .-png_zfree
	.section	.text.png_info_init_3,"ax",%progbits
	.align	2
	.global	png_info_init_3
	.hidden	png_info_init_3
	.type	png_info_init_3, %function
png_info_init_3:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	mov	r4, r0
	ldr	r0, [r0, #0]
	cmp	r0, #0
	beq	.L97
	cmp	r1, #288
	bcc	.L98
.L96:
	mov	r1, #0
	mov	r2, #288
	bl	memset
.L97:
	ldmfd	sp!, {r4, lr}
	bx	lr
.L98:
	bl	png_destroy_struct
	mov	r0, #2
	bl	png_create_struct
	str	r0, [r4, #0]
	b	.L96
	.size	png_info_init_3, .-png_info_init_3
	.section	.text.png_calculate_crc,"ax",%progbits
	.align	2
	.global	png_calculate_crc
	.hidden	png_calculate_crc
	.type	png_calculate_crc, %function
png_calculate_crc:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	ldrb	r3, [r0, #312]	@ zero_extendqisi2
	tst	r3, #32
	mov	r4, r0
	ldr	r3, [r0, #136]
	beq	.L100
	and	r0, r3, #768
	cmp	r0, #768
	beq	.L103
.L102:
	ldr	r0, [r4, #300]
	bl	crc32
	str	r0, [r4, #300]
.L103:
	ldmfd	sp!, {r4, lr}
	bx	lr
.L100:
	tst	r3, #2048
	bne	.L103
	b	.L102
	.size	png_calculate_crc, .-png_calculate_crc
	.section	.text.png_reset_crc,"ax",%progbits
	.align	2
	.global	png_reset_crc
	.hidden	png_reset_crc
	.type	png_reset_crc, %function
png_reset_crc:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	mov	r4, r0
	mov	r0, #0
	mov	r1, r0
	mov	r2, r0
	bl	crc32
	str	r0, [r4, #300]
	ldmfd	sp!, {r4, lr}
	bx	lr
	.size	png_reset_crc, .-png_reset_crc
	.section	.text.png_set_sig_bytes,"ax",%progbits
	.align	2
	.global	png_set_sig_bytes
	.hidden	png_set_sig_bytes
	.type	png_set_sig_bytes, %function
png_set_sig_bytes:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, lr}
	subs	r5, r0, #0
	mov	r4, r1
	beq	.L109
	cmp	r1, #8
	bgt	.L110
.L108:
	bic	r4, r4, r4, asr #31
	strb	r4, [r5, #328]
.L109:
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L110:
	ldr	r1, .L111
	bl	png_error
	b	.L108
.L112:
	.align	2
.L111:
	.word	.LC6
	.size	png_set_sig_bytes, .-png_set_sig_bytes
	.section	.text.png_check_sig,"ax",%progbits
	.align	2
	.global	png_check_sig
	.hidden	png_check_sig
	.type	png_check_sig, %function
png_check_sig:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 8
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
	ldr	r3, .L118
	mov	r2, r1
	sub	sp, sp, #12
	mov	ip, r0
	ldmia	r3, {r0, r1}
	cmp	r2, #8
	mov	r3, sp
	stmia	r3, {r0, r1}
	movhi	r2, #8
	bhi	.L115
	cmp	r2, #0
	moveq	r0, r2
	bne	.L115
.L116:
	add	sp, sp, #12
	ldr	lr, [sp], #4
	bx	lr
.L115:
	mov	r0, ip
	mov	r1, sp
	bl	memcmp
	rsbs	r0, r0, #1
	movcc	r0, #0
	b	.L116
.L119:
	.align	2
.L118:
	.word	.LANCHOR0
	.size	png_check_sig, .-png_check_sig
	.section	.text.png_check_cHRM_fixed,"ax",%progbits
	.align	2
	.global	png_check_cHRM_fixed
	.hidden	png_check_cHRM_fixed
	.type	png_check_cHRM_fixed, %function
png_check_cHRM_fixed:
	@ Function supports interworking.
	@ args = 20, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, fp, lr}
	subs	r4, r0, #0
	mov	r5, r1
	mov	r6, r2
	mov	r7, r3
	add	r8, sp, #40
	ldmia	r8, {r8, sl}	@ phole ldm
	add	r9, sp, #48
	ldmia	r9, {r9, fp}	@ phole ldm
	moveq	r0, r4
	beq	.L122
	cmp	r2, #0
	movgt	r3, #0
	movle	r3, #1
	orrs	r3, r3, r1, lsr #31
	bne	.L123
	orrs	r2, r8, r7
	bpl	.L130
.L123:
	mov	r0, r4
	ldr	r1, .L136
	bl	png_warning
	mov	r0, #0
.L124:
	rsb	r1, r6, #99328
	add	r6, r1, #672
	cmp	r5, r6
	bgt	.L131
.L125:
	rsb	ip, r8, #99328
	add	r5, ip, #672
	cmp	r7, r5
	bgt	.L132
.L126:
	rsb	r2, r9, #99328
	add	r3, r2, #672
	cmp	sl, r3
	bgt	.L133
.L127:
	ldr	r5, [sp, #56]
	rsb	r1, r5, #99328
	add	r6, r1, #672
	cmp	fp, r6
	bgt	.L134
.L128:
	ldr	r6, [sp, #56]
	rsb	fp, r7, fp
	rsb	ip, r8, r6
	mov	r5, fp, asl #16
	rsb	r8, r8, r9
	rsb	r7, r7, sl
	mov	r2, ip, asl #16
	mov	r3, r5, lsr #16
	mov	r1, r8, lsr #16
	mov	r5, r7, lsr #16
	mov	r2, r2, lsr #16
	mul	r9, r5, r2
	mul	r6, r3, r1
	mov	r7, r7, asl #16
	mov	r8, r8, asl #16
	mov	r7, r7, lsr #16
	mov	r8, r8, lsr #16
	mov	ip, ip, lsr #16
	mov	fp, fp, lsr #16
	mla	r6, r8, fp, r6
	mla	r9, r7, ip, r9
	mul	r2, r7, r2
	mul	r8, r3, r8
	mul	ip, r5, ip
	mul	fp, r1, fp
	add	r9, r9, r2, lsr #16
	add	r6, r6, r8, lsr #16
	add	ip, ip, r9, lsr #16
	add	fp, fp, r6, lsr #16
	cmp	ip, fp
	beq	.L135
.L122:
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, r9, sl, fp, lr}
	bx	lr
.L130:
	orrs	r3, r9, sl
	bmi	.L123
	ldr	r2, [sp, #56]
	orrs	r2, r2, fp
	movpl	r0, #1
	bpl	.L124
	b	.L123
.L134:
	mov	r0, r4
	ldr	r1, .L136+4
	bl	png_warning
	mov	r0, #0
	b	.L128
.L133:
	mov	r0, r4
	ldr	r1, .L136+8
	bl	png_warning
	mov	r0, #0
	b	.L127
.L132:
	mov	r0, r4
	ldr	r1, .L136+12
	bl	png_warning
	mov	r0, #0
	b	.L126
.L131:
	mov	r0, r4
	ldr	r1, .L136+16
	bl	png_warning
	mov	r0, #0
	b	.L125
.L135:
	mov	r8, r8, asl #16
	mov	ip, r2, asl #16
	mov	r1, r8, lsr #16
	mov	r3, ip, lsr #16
	orr	r9, r3, r9, asl #16
	orr	r8, r1, r6, asl #16
	cmp	r9, r8
	bne	.L122
	mov	r0, r4
	ldr	r1, .L136+20
	bl	png_warning
	mov	r0, #0
	b	.L122
.L137:
	.align	2
.L136:
	.word	.LC7
	.word	.LC11
	.word	.LC10
	.word	.LC9
	.word	.LC8
	.word	.LC12
	.size	png_check_cHRM_fixed, .-png_check_cHRM_fixed
	.section	.text.png_free_data,"ax",%progbits
	.align	2
	.global	png_free_data
	.hidden	png_free_data
	.type	png_free_data, %function
png_free_data:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	cmpne	r1, #0
	stmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, lr}
	mov	r4, r1
	movne	r7, #0
	moveq	r7, #1
	mov	r6, r2
	mov	r8, r3
	mov	r5, r0
	beq	.L171
	ldr	r3, [r1, #184]
	and	r2, r3, #16384
	tst	r2, r6
	mov	r2, r3
	beq	.L140
	cmn	r8, #1
	beq	.L175
	ldr	r1, [r1, #56]
	cmp	r1, #0
	beq	.L140
	mov	sl, r8, asl #4
	add	r1, r1, sl
	ldr	r1, [r1, #4]
	cmp	r1, #0
	beq	.L140
	bl	png_free
	ldr	r0, [r4, #56]
	add	sl, r0, sl
	str	r7, [sl, #4]
	ldr	r3, [r4, #184]
	mov	r2, r3
.L140:
	and	r1, r6, #8192
	tst	r1, r3
	bne	.L176
.L145:
	and	r0, r6, #256
	tst	r0, r3
	ldrne	r2, [r4, #8]
	and	r1, r6, #128
	bicne	r2, r2, #16384
	strne	r2, [r4, #8]
	movne	r2, r3
	tst	r1, r3
	bne	.L177
	and	ip, r6, #16
	tst	ip, r2
	bne	.L178
.L152:
	and	r0, r6, #32
	tst	r0, r2
	beq	.L153
.L184:
	cmn	r8, #1
	beq	.L154
	ldr	r3, [r4, #212]
	cmp	r3, #0
	beq	.L153
	ldr	r1, [r3, r8, asl #4]
	mov	r0, r5
	bl	png_free
	ldr	r1, [r4, #212]
	mov	r7, r8, asl #4
	add	r0, r1, r7
	ldr	r1, [r0, #8]
	mov	r0, r5
	bl	png_free
	ldr	r2, [r4, #212]
	mov	ip, #0
	str	ip, [r2, r8, asl #4]
	ldr	r3, [r4, #212]
	add	r7, r3, r7
	str	ip, [r7, #8]
.L153:
	ldr	r1, [r5, #656]
	cmp	r1, #0
	beq	.L158
	mov	r0, r5
	bl	png_free
	mov	ip, #0
	str	ip, [r5, #656]
.L158:
	ldr	r3, [r4, #184]
	and	r2, r3, #512
	tst	r2, r6
	beq	.L159
	cmn	r8, #1
	beq	.L160
	ldr	r2, [r4, #188]
	cmp	r2, #0
	beq	.L159
	add	r1, r8, r8, asl #2
	mov	r7, r1, asl #2
	add	lr, r2, r7
	mov	r0, r5
	ldr	r1, [lr, #8]
	bl	png_free
	ldr	r0, [r4, #188]
	mov	r3, #0
	add	r7, r0, r7
	str	r3, [r7, #8]
	ldr	r3, [r4, #184]
.L159:
	and	r0, r6, #8
	tst	r0, r3
	bne	.L179
	and	r0, r6, #4096
	tst	r0, r3
	bne	.L180
.L164:
	and	r2, r6, #64
	tst	r2, r3
	beq	.L165
.L182:
	ldr	r1, [r4, #248]
	cmp	r1, #0
	beq	.L166
	ldr	lr, [r4, #4]
	cmp	lr, #0
	ble	.L167
	mov	r7, #0
	mov	sl, r7
	b	.L169
.L181:
	ldr	r1, [r4, #248]
.L169:
	ldr	r1, [r1, r7, asl #2]
	mov	r0, r5
	bl	png_free
	ldr	r3, [r4, #248]
	str	sl, [r3, r7, asl #2]
	ldr	r0, [r4, #4]
	add	r7, r7, #1
	cmp	r0, r7
	bgt	.L181
	ldr	r1, [r4, #248]
.L167:
	mov	r0, r5
	bl	png_free
	mov	r1, #0
	str	r1, [r4, #248]
	ldr	r3, [r4, #184]
.L166:
	ldr	r2, [r4, #8]
	bic	ip, r2, #32768
	str	ip, [r4, #8]
.L165:
	cmn	r8, #1
	bicne	r6, r6, #16896
	bicne	r6, r6, #32
	biceq	r3, r3, r6
	bicne	r6, r3, r6
	streq	r3, [r4, #184]
	strne	r6, [r4, #184]
.L171:
	ldmfd	sp!, {r3, r4, r5, r6, r7, r8, sl, lr}
	bx	lr
.L180:
	ldr	r1, [r4, #16]
	mov	r0, r5
	bl	png_free
	mov	r3, #0
	ldr	ip, [r4, #8]
	strh	r3, [r4, #20]	@ movhi
	str	r3, [r4, #16]
	ldr	r3, [r4, #184]
	and	r2, r6, #64
	bic	r1, ip, #8
	tst	r2, r3
	str	r1, [r4, #8]
	beq	.L165
	b	.L182
.L176:
	mov	r0, r5
	ldr	r1, [r4, #76]
	bl	png_free
	ldr	r3, [r4, #8]
	bic	r0, r3, #16
	ldr	r3, [r4, #184]
	mov	ip, #0
	str	ip, [r4, #76]
	str	r0, [r4, #8]
	mov	r2, r3
	b	.L145
.L177:
	ldr	r1, [r4, #160]
	mov	r0, r5
	bl	png_free
	ldr	r1, [r4, #172]
	mov	r0, r5
	bl	png_free
	ldr	r1, [r4, #176]
	mov	r7, #0
	cmp	r1, #0
	str	r7, [r4, #160]
	str	r7, [r4, #172]
	beq	.L148
	ldrb	lr, [r4, #181]	@ zero_extendqisi2
	cmp	lr, r7
	beq	.L149
	mov	sl, r7
	b	.L151
.L183:
	ldr	r1, [r4, #176]
.L151:
	ldr	r1, [r1, r7, asl #2]
	mov	r0, r5
	bl	png_free
	ldr	ip, [r4, #176]
	str	sl, [ip, r7, asl #2]
	ldrb	r2, [r4, #181]	@ zero_extendqisi2
	add	r7, r7, #1
	cmp	r2, r7
	bgt	.L183
	ldr	r1, [r4, #176]
.L149:
	mov	r0, r5
	bl	png_free
	mov	r3, #0
	str	r3, [r4, #176]
.L148:
	ldr	r0, [r4, #8]
	ldr	r2, [r4, #184]
	and	ip, r6, #16
	bic	r1, r0, #1024
	tst	ip, r2
	str	r1, [r4, #8]
	beq	.L152
.L178:
	ldr	r1, [r4, #196]
	mov	r0, r5
	bl	png_free
	mov	r0, r5
	ldr	r1, [r4, #200]
	bl	png_free
	ldr	r1, [r4, #8]
	bic	r2, r1, #4096
	str	r2, [r4, #8]
	ldr	r2, [r4, #184]
	and	r0, r6, #32
	mov	r3, #0
	tst	r0, r2
	str	r3, [r4, #200]
	str	r3, [r4, #196]
	beq	.L153
	b	.L184
.L179:
	mov	r0, r5
	ldr	r1, [r4, #124]
	bl	png_free
	ldr	r2, [r4, #8]
	ldr	r3, [r4, #184]
	and	r0, r6, #4096
	bic	r1, r2, #64
	mov	ip, #0
	tst	r0, r3
	str	ip, [r4, #124]
	str	r1, [r4, #8]
	beq	.L164
	b	.L180
.L154:
	ldr	ip, [r4, #216]
	cmp	ip, #0
	beq	.L155
	ble	.L156
	mov	r7, #0
.L157:
	mov	r3, r7
	mov	r0, r5
	mov	r1, r4
	mov	r2, #32
	bl	png_free_data
	ldr	r3, [r4, #216]
	add	r7, r7, #1
	cmp	r3, r7
	bgt	.L157
.L156:
	mov	r0, r5
	ldr	r1, [r4, #212]
	bl	png_free
	mov	r2, #0
	str	r2, [r4, #216]
	str	r2, [r4, #212]
.L155:
	ldr	r1, [r4, #8]
	bic	r0, r1, #8192
	str	r0, [r4, #8]
	b	.L153
.L175:
	ldr	r3, [r1, #48]
	cmp	r3, #0
	ble	.L143
.L144:
	mov	r3, r7
	mov	r0, r5
	mov	r1, r4
	mov	r2, #16384
	bl	png_free_data
	ldr	ip, [r4, #48]
	add	r7, r7, #1
	cmp	ip, r7
	bgt	.L144
.L143:
	mov	r0, r5
	ldr	r1, [r4, #56]
	bl	png_free
	ldr	r3, [r4, #184]
	mov	r2, #0
	str	r2, [r4, #48]
	str	r2, [r4, #56]
	mov	r2, r3
	b	.L140
.L160:
	ldr	ip, [r4, #192]
	cmp	ip, #0
	beq	.L159
	ble	.L161
	mov	r7, #0
.L162:
	mov	r3, r7
	mov	r2, #512
	mov	r0, r5
	mov	r1, r4
	bl	png_free_data
	ldr	r2, [r4, #192]
	add	r7, r7, #1
	cmp	r2, r7
	bgt	.L162
.L161:
	mov	r0, r5
	ldr	r1, [r4, #188]
	bl	png_free
	mov	r3, #0
	str	r3, [r4, #192]
	str	r3, [r4, #188]
	ldr	r3, [r4, #184]
	b	.L159
	.size	png_free_data, .-png_free_data
	.section	.text.png_info_destroy,"ax",%progbits
	.align	2
	.global	png_info_destroy
	.hidden	png_info_destroy
	.type	png_info_destroy, %function
png_info_destroy:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	mov	r2, #32512
	stmfd	sp!, {r3, r4, r5, lr}
	add	r2, r2, #255
	mov	r4, r0
	mvn	r3, #0
	mov	r5, r1
	bl	png_free_data
	ldr	r3, [r4, #572]
	cmp	r3, #0
	beq	.L186
	mov	r0, r4
	ldr	r1, [r4, #576]
	bl	png_free
	mov	r0, #0
	str	r0, [r4, #572]
	str	r0, [r4, #576]
.L186:
	cmp	r5, #0
	movne	r0, r5
	movne	r1, #0
	movne	r2, #288
	blne	memset
.L188:
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
	.size	png_info_destroy, .-png_info_destroy
	.section	.text.png_info_init,"ax",%progbits
	.align	2
	.global	png_info_init
	.hidden	png_info_init
	.type	png_info_init, %function
png_info_init:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	stmfd	sp!, {r3, lr}
	beq	.L191
	bl	png_destroy_struct
	mov	r0, #2
	bl	png_create_struct
	mov	r1, #0
	mov	r2, #288
	bl	memset
.L191:
	ldmfd	sp!, {r3, lr}
	bx	lr
	.size	png_info_init, .-png_info_init
	.section	.text.png_destroy_info_struct,"ax",%progbits
	.align	2
	.global	png_destroy_info_struct
	.hidden	png_destroy_info_struct
	.type	png_destroy_info_struct, %function
png_destroy_info_struct:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	subs	r4, r0, #0
	mov	r6, r1
	beq	.L195
	cmp	r1, #0
	beq	.L195
	ldr	r5, [r1, #0]
	cmp	r5, #0
	beq	.L195
	mov	r2, #32512
	mvn	r3, #0
	add	r2, r2, #255
	mov	r1, r5
	bl	png_free_data
	ldr	r3, [r4, #572]
	cmp	r3, #0
	bne	.L196
.L194:
	mov	r1, #0
	mov	r2, #288
	mov	r0, r5
	bl	memset
	ldr	r1, [r4, #616]
	mov	r0, r5
	ldr	r2, [r4, #608]
	bl	png_destroy_struct_2
	mov	r1, #0
	str	r1, [r6, #0]
.L195:
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L196:
	mov	r0, r4
	ldr	r1, [r4, #576]
	bl	png_free
	mov	r0, #0
	str	r0, [r4, #572]
	str	r0, [r4, #576]
	b	.L194
	.size	png_destroy_info_struct, .-png_destroy_info_struct
	.section	.text.png_create_info_struct,"ax",%progbits
	.align	2
	.global	png_create_info_struct
	.hidden	png_create_info_struct
	.type	png_create_info_struct, %function
png_create_info_struct:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, lr}
	subs	r4, r0, #0
	beq	.L199
	ldr	r2, [r4, #608]
	ldr	r1, [r4, #612]
	mov	r0, #2
	bl	png_create_struct_2
	subs	r4, r0, #0
	movne	r1, #0
	movne	r2, #288
	blne	memset
.L199:
	mov	r0, r4
	ldmfd	sp!, {r4, lr}
	bx	lr
	.size	png_create_info_struct, .-png_create_info_struct
	.hidden	png_libpng_ver
	.global	png_libpng_ver
	.hidden	png_sig
	.global	png_sig
	.hidden	png_IHDR
	.global	png_IHDR
	.hidden	png_IDAT
	.global	png_IDAT
	.hidden	png_IEND
	.global	png_IEND
	.hidden	png_PLTE
	.global	png_PLTE
	.hidden	png_bKGD
	.global	png_bKGD
	.hidden	png_cHRM
	.global	png_cHRM
	.hidden	png_gAMA
	.global	png_gAMA
	.hidden	png_hIST
	.global	png_hIST
	.hidden	png_iCCP
	.global	png_iCCP
	.hidden	png_iTXt
	.global	png_iTXt
	.hidden	png_oFFs
	.global	png_oFFs
	.hidden	png_pCAL
	.global	png_pCAL
	.hidden	png_sCAL
	.global	png_sCAL
	.hidden	png_pHYs
	.global	png_pHYs
	.hidden	png_sBIT
	.global	png_sBIT
	.hidden	png_sPLT
	.global	png_sPLT
	.hidden	png_sRGB
	.global	png_sRGB
	.hidden	png_tEXt
	.global	png_tEXt
	.hidden	png_tIME
	.global	png_tIME
	.hidden	png_tRNS
	.global	png_tRNS
	.hidden	png_zTXt
	.global	png_zTXt
	.hidden	png_pass_start
	.global	png_pass_start
	.hidden	png_pass_inc
	.global	png_pass_inc
	.hidden	png_pass_ystart
	.global	png_pass_ystart
	.hidden	png_pass_yinc
	.global	png_pass_yinc
	.hidden	png_pass_mask
	.global	png_pass_mask
	.hidden	png_pass_dsp_mask
	.global	png_pass_dsp_mask
	.section	.rodata
	.align	2
	.set	.LANCHOR0,. + 0
	.type	C.0.4089, %object
	.size	C.0.4089, 8
C.0.4089:
	.byte	-119
	.byte	80
	.byte	78
	.byte	71
	.byte	13
	.byte	10
	.byte	26
	.byte	10
	.type	short_months.4394, %object
	.size	short_months.4394, 48
short_months.4394:
	.ascii	"Jan\000"
	.ascii	"Feb\000"
	.ascii	"Mar\000"
	.ascii	"Apr\000"
	.ascii	"May\000"
	.ascii	"Jun\000"
	.ascii	"Jul\000"
	.ascii	"Aug\000"
	.ascii	"Sep\000"
	.ascii	"Oct\000"
	.ascii	"Nov\000"
	.ascii	"Dec\000"
	.type	png_libpng_ver, %object
	.size	png_libpng_ver, 18
png_libpng_ver:
	.ascii	"1.2.34\000"
	.space	11
	.space	2
	.type	png_sig, %object
	.size	png_sig, 8
png_sig:
	.byte	-119
	.byte	80
	.byte	78
	.byte	71
	.byte	13
	.byte	10
	.byte	26
	.byte	10
	.type	png_pass_start, %object
	.size	png_pass_start, 28
png_pass_start:
	.word	0
	.word	4
	.word	0
	.word	2
	.word	0
	.word	1
	.word	0
	.type	png_pass_inc, %object
	.size	png_pass_inc, 28
png_pass_inc:
	.word	8
	.word	8
	.word	4
	.word	4
	.word	2
	.word	2
	.word	1
	.type	png_pass_ystart, %object
	.size	png_pass_ystart, 28
png_pass_ystart:
	.word	0
	.word	0
	.word	4
	.word	0
	.word	2
	.word	0
	.word	1
	.type	png_pass_yinc, %object
	.size	png_pass_yinc, 28
png_pass_yinc:
	.word	8
	.word	8
	.word	8
	.word	4
	.word	4
	.word	2
	.word	2
	.type	png_pass_mask, %object
	.size	png_pass_mask, 28
png_pass_mask:
	.word	128
	.word	8
	.word	136
	.word	34
	.word	170
	.word	85
	.word	255
	.type	png_pass_dsp_mask, %object
	.size	png_pass_dsp_mask, 28
png_pass_dsp_mask:
	.word	255
	.word	15
	.word	255
	.word	51
	.word	255
	.word	85
	.word	255
	.data
	.align	2
	.type	png_IHDR, %object
	.size	png_IHDR, 5
png_IHDR:
	.byte	73
	.byte	72
	.byte	68
	.byte	82
	.byte	0
	.space	3
	.type	png_IDAT, %object
	.size	png_IDAT, 5
png_IDAT:
	.byte	73
	.byte	68
	.byte	65
	.byte	84
	.byte	0
	.space	3
	.type	png_IEND, %object
	.size	png_IEND, 5
png_IEND:
	.byte	73
	.byte	69
	.byte	78
	.byte	68
	.byte	0
	.space	3
	.type	png_PLTE, %object
	.size	png_PLTE, 5
png_PLTE:
	.byte	80
	.byte	76
	.byte	84
	.byte	69
	.byte	0
	.space	3
	.type	png_bKGD, %object
	.size	png_bKGD, 5
png_bKGD:
	.byte	98
	.byte	75
	.byte	71
	.byte	68
	.byte	0
	.space	3
	.type	png_cHRM, %object
	.size	png_cHRM, 5
png_cHRM:
	.byte	99
	.byte	72
	.byte	82
	.byte	77
	.byte	0
	.space	3
	.type	png_gAMA, %object
	.size	png_gAMA, 5
png_gAMA:
	.byte	103
	.byte	65
	.byte	77
	.byte	65
	.byte	0
	.space	3
	.type	png_hIST, %object
	.size	png_hIST, 5
png_hIST:
	.byte	104
	.byte	73
	.byte	83
	.byte	84
	.byte	0
	.space	3
	.type	png_iCCP, %object
	.size	png_iCCP, 5
png_iCCP:
	.byte	105
	.byte	67
	.byte	67
	.byte	80
	.byte	0
	.space	3
	.type	png_iTXt, %object
	.size	png_iTXt, 5
png_iTXt:
	.byte	105
	.byte	84
	.byte	88
	.byte	116
	.byte	0
	.space	3
	.type	png_oFFs, %object
	.size	png_oFFs, 5
png_oFFs:
	.byte	111
	.byte	70
	.byte	70
	.byte	115
	.byte	0
	.space	3
	.type	png_pCAL, %object
	.size	png_pCAL, 5
png_pCAL:
	.byte	112
	.byte	67
	.byte	65
	.byte	76
	.byte	0
	.space	3
	.type	png_sCAL, %object
	.size	png_sCAL, 5
png_sCAL:
	.byte	115
	.byte	67
	.byte	65
	.byte	76
	.byte	0
	.space	3
	.type	png_pHYs, %object
	.size	png_pHYs, 5
png_pHYs:
	.byte	112
	.byte	72
	.byte	89
	.byte	115
	.byte	0
	.space	3
	.type	png_sBIT, %object
	.size	png_sBIT, 5
png_sBIT:
	.byte	115
	.byte	66
	.byte	73
	.byte	84
	.byte	0
	.space	3
	.type	png_sPLT, %object
	.size	png_sPLT, 5
png_sPLT:
	.byte	115
	.byte	80
	.byte	76
	.byte	84
	.byte	0
	.space	3
	.type	png_sRGB, %object
	.size	png_sRGB, 5
png_sRGB:
	.byte	115
	.byte	82
	.byte	71
	.byte	66
	.byte	0
	.space	3
	.type	png_tEXt, %object
	.size	png_tEXt, 5
png_tEXt:
	.byte	116
	.byte	69
	.byte	88
	.byte	116
	.byte	0
	.space	3
	.type	png_tIME, %object
	.size	png_tIME, 5
png_tIME:
	.byte	116
	.byte	73
	.byte	77
	.byte	69
	.byte	0
	.space	3
	.type	png_tRNS, %object
	.size	png_tRNS, 5
png_tRNS:
	.byte	116
	.byte	82
	.byte	78
	.byte	83
	.byte	0
	.space	3
	.type	png_zTXt, %object
	.size	png_zTXt, 5
png_zTXt:
	.byte	122
	.byte	84
	.byte	88
	.byte	116
	.byte	0
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"\012 libpng version 1.2.34 - December 18, 2008\012 "
	.ascii	"  Copyright (c) 1998-2008 Glenn Randers-Pehrson\012"
	.ascii	"   Copyright (c) 1996-1997 Andreas Dilger\012   Cop"
	.ascii	"yright (c) 1995-1996 Guy Eric Schalnat, Group 42, I"
	.ascii	"nc.\012\000"
	.space	3
.LC1:
	.ascii	"1.2.34\000"
	.space	1
.LC2:
	.ascii	" libpng version 1.2.34 - December 18, 2008\012\012\000"
	.space	3
.LC3:
	.ascii	"Unknown freer parameter in png_data_freer.\000"
	.space	1
.LC4:
	.ascii	"Potential overflow in png_zalloc()\000"
	.space	1
.LC5:
	.ascii	"%d %s %d %02d:%02d:%02d +0000\000"
	.space	2
.LC6:
	.ascii	"Too many bytes for PNG signature.\000"
	.space	2
.LC7:
	.ascii	"Ignoring attempt to set negative chromaticity value"
	.ascii	"\000"
.LC8:
	.ascii	"Invalid cHRM white point\000"
	.space	3
.LC9:
	.ascii	"Invalid cHRM red point\000"
	.space	1
.LC10:
	.ascii	"Invalid cHRM green point\000"
	.space	3
.LC11:
	.ascii	"Invalid cHRM blue point\000"
.LC12:
	.ascii	"Ignoring attempt to set cHRM RGB triangle with zero"
	.ascii	" area\000"
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
