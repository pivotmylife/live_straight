	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"pngmem.c"
	.section	.text.png_set_mem_fn,"ax",%progbits
	.align	2
	.global	png_set_mem_fn
	.hidden	png_set_mem_fn
	.type	png_set_mem_fn, %function
png_set_mem_fn:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	strne	r3, [r0, #616]
	strne	r1, [r0, #608]
	strne	r2, [r0, #612]
	bx	lr
	.size	png_set_mem_fn, .-png_set_mem_fn
	.section	.text.png_get_mem_ptr,"ax",%progbits
	.align	2
	.global	png_get_mem_ptr
	.hidden	png_get_mem_ptr
	.type	png_get_mem_ptr, %function
png_get_mem_ptr:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	cmp	r0, #0
	ldrne	r0, [r0, #608]
	bx	lr
	.size	png_get_mem_ptr, .-png_get_mem_ptr
	.section	.text.png_memset_check,"ax",%progbits
	.align	2
	.global	png_memset_check
	.hidden	png_memset_check
	.type	png_memset_check, %function
png_memset_check:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, lr}
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	bl	memset
	ldmfd	sp!, {r3, lr}
	bx	lr
	.size	png_memset_check, .-png_memset_check
	.section	.text.png_memcpy_check,"ax",%progbits
	.align	2
	.global	png_memcpy_check
	.hidden	png_memcpy_check
	.type	png_memcpy_check, %function
png_memcpy_check:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r3, lr}
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	bl	memcpy
	ldmfd	sp!, {r3, lr}
	bx	lr
	.size	png_memcpy_check, .-png_memcpy_check
	.section	.text.png_free_default,"ax",%progbits
	.align	2
	.global	png_free_default
	.hidden	png_free_default
	.type	png_free_default, %function
png_free_default:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	cmpne	r1, #0
	stmfd	sp!, {r3, lr}
	movne	r0, r1
	blne	free
.L14:
	ldmfd	sp!, {r3, lr}
	bx	lr
	.size	png_free_default, .-png_free_default
	.section	.text.png_destroy_struct_2,"ax",%progbits
	.align	2
	.global	png_destroy_struct_2
	.hidden	png_destroy_struct_2
	.type	png_destroy_struct_2, %function
png_destroy_struct_2:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 680
	@ frame_needed = 0, uses_anonymous_args = 0
	str	lr, [sp, #-4]!
	subs	ip, r0, #0
	sub	sp, sp, #684
	mov	r3, r1
	beq	.L19
	cmp	r1, #0
	beq	.L18
	mov	r1, ip
	mov	r0, sp
	str	r2, [sp, #608]
	mov	lr, pc
	bx	r3
.L19:
	add	sp, sp, #684
	ldr	lr, [sp], #4
	bx	lr
.L18:
	bl	free
	b	.L19
	.size	png_destroy_struct_2, .-png_destroy_struct_2
	.section	.text.png_malloc_default,"ax",%progbits
	.align	2
	.global	png_malloc_default
	.hidden	png_malloc_default
	.type	png_malloc_default, %function
png_malloc_default:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	cmpne	r1, #0
	stmfd	sp!, {r3, lr}
	moveq	r0, #0
	movne	r0, r1
	blne	malloc
.L22:
	ldmfd	sp!, {r3, lr}
	bx	lr
	.size	png_malloc_default, .-png_malloc_default
	.section	.text.png_malloc,"ax",%progbits
	.align	2
	.global	png_malloc
	.hidden	png_malloc
	.type	png_malloc, %function
png_malloc:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	cmpne	r1, #0
	stmfd	sp!, {r3, r4, r5, lr}
	mov	r5, r0
	moveq	r4, #0
	beq	.L26
	ldr	r3, [r0, #612]
	cmp	r3, #0
	beq	.L27
	mov	lr, pc
	bx	r3
	mov	r4, r0
.L28:
	cmp	r4, #0
	beq	.L30
.L26:
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L30:
	ldr	r3, [r5, #136]
	tst	r3, #1048576
	bne	.L26
	mov	r0, r5
	ldr	r1, .L31
	bl	png_error
	b	.L26
.L27:
	bl	png_malloc_default
	mov	r4, r0
	b	.L28
.L32:
	.align	2
.L31:
	.word	.LC0
	.size	png_malloc, .-png_malloc
	.section	.text.png_create_struct_2,"ax",%progbits
	.align	2
	.global	png_create_struct_2
	.hidden	png_create_struct_2
	.type	png_create_struct_2, %function
png_create_struct_2:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 680
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	cmp	r0, #2
	sub	sp, sp, #680
	mov	r4, r1
	moveq	r6, #288
	bne	.L40
.L35:
	cmp	r4, #0
	beq	.L38
	mov	r0, sp
	mov	r1, r6
	str	r2, [sp, #608]
	mov	lr, pc
	bx	r4
	subs	r5, r0, #0
	movne	r2, r6
	movne	r1, #0
	blne	memset
.L37:
	mov	r0, r5
	add	sp, sp, #680
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L40:
	cmp	r0, #1
	movne	r5, #0
	bne	.L37
	mov	r6, #680
	b	.L35
.L38:
	mov	r0, r6
	bl	malloc
	subs	r5, r0, #0
	beq	.L37
	mov	r1, r4
	mov	r2, r6
	bl	memset
	b	.L37
	.size	png_create_struct_2, .-png_create_struct_2
	.section	.text.T.6,"ax",%progbits
	.align	2
	.type	T.6, %function
T.6:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	stmfd	sp!, {r3, lr}
	blne	free
.L43:
	ldmfd	sp!, {r3, lr}
	bx	lr
	.size	T.6, .-T.6
	.section	.text.png_destroy_struct,"ax",%progbits
	.align	2
	.global	png_destroy_struct
	.hidden	png_destroy_struct
	.type	png_destroy_struct, %function
png_destroy_struct:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	b	T.6
	.size	png_destroy_struct, .-png_destroy_struct
	.section	.text.T.7,"ax",%progbits
	.align	2
	.type	T.7, %function
T.7:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #2
	stmfd	sp!, {r3, r4, r5, lr}
	moveq	r5, #288
	bne	.L52
.L48:
	mov	r0, r5
	bl	malloc
	subs	r4, r0, #0
	movne	r2, r5
	movne	r1, #0
	blne	memset
.L50:
	mov	r0, r4
	ldmfd	sp!, {r3, r4, r5, lr}
	bx	lr
.L52:
	cmp	r0, #1
	movne	r4, #0
	bne	.L50
	mov	r5, #680
	b	.L48
	.size	T.7, .-T.7
	.section	.text.png_create_struct,"ax",%progbits
	.align	2
	.global	png_create_struct
	.hidden	png_create_struct
	.type	png_create_struct, %function
png_create_struct:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	b	T.7
	.size	png_create_struct, .-png_create_struct
	.section	.text.png_free,"ax",%progbits
	.align	2
	.global	png_free
	.hidden	png_free
	.type	png_free, %function
png_free:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	cmp	r0, #0
	cmpne	r1, #0
	stmfd	sp!, {r3, lr}
	beq	.L58
	ldr	r3, [r0, #616]
	cmp	r3, #0
	beq	.L57
	mov	lr, pc
	bx	r3
.L58:
	ldmfd	sp!, {r3, lr}
	bx	lr
.L57:
	mov	r0, r1
	bl	free
	b	.L58
	.size	png_free, .-png_free
	.section	.text.png_malloc_warn,"ax",%progbits
	.align	2
	.global	png_malloc_warn
	.hidden	png_malloc_warn
	.type	png_malloc_warn, %function
png_malloc_warn:
	@ Function supports interworking.
	@ args = 0, pretend = 0, frame = 0
	@ frame_needed = 0, uses_anonymous_args = 0
	stmfd	sp!, {r4, r5, r6, lr}
	subs	r4, r0, #0
	mov	r5, r1
	moveq	r5, r4
	beq	.L61
	ldr	r6, [r4, #136]
	cmp	r5, #0
	orr	r3, r6, #1048576
	str	r3, [r4, #136]
	beq	.L63
	ldr	r3, [r4, #612]
	cmp	r3, #0
	beq	.L64
	mov	lr, pc
	bx	r3
	mov	r5, r0
.L65:
	cmp	r5, #0
	beq	.L67
.L63:
	str	r6, [r4, #136]
.L61:
	mov	r0, r5
	ldmfd	sp!, {r4, r5, r6, lr}
	bx	lr
.L67:
	ldr	r0, [r4, #136]
	tst	r0, #1048576
	bne	.L63
	mov	r0, r4
	ldr	r1, .L68
	bl	png_error
	b	.L63
.L64:
	bl	png_malloc_default
	mov	r5, r0
	b	.L65
.L69:
	.align	2
.L68:
	.word	.LC0
	.size	png_malloc_warn, .-png_malloc_warn
	.section	.rodata.str1.4,"aMS",%progbits,1
	.align	2
.LC0:
	.ascii	"Out of Memory!\000"
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
