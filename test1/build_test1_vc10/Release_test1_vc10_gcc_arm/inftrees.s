	.arch armv4t
	.fpu softvfp
	.eabi_attribute 20, 1
	.eabi_attribute 21, 1
	.eabi_attribute 23, 3
	.eabi_attribute 24, 1
	.eabi_attribute 25, 1
	.eabi_attribute 26, 2
	.eabi_attribute 30, 2
	.eabi_attribute 18, 2
	.file	"inftrees.c"
	.section	.text.inflate_table,"ax",%progbits
	.align	2
	.global	inflate_table
	.hidden	inflate_table
	.type	inflate_table, %function
inflate_table:
	@ Function supports interworking.
	@ args = 8, pretend = 0, frame = 136
	@ frame_needed = 0, uses_anonymous_args = 0
	@ link register save eliminated.
	stmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
	sub	sp, sp, #136
	add	r4, sp, #104
	add	r5, r4, #4
	add	r4, r5, #4
	add	ip, r4, #4
	add	r8, ip, #4
	add	sl, r8, #4
	mov	r6, #0
	add	r7, sl, #4
	str	r6, [r7, #4]
	cmp	r2, #0
	str	r6, [r5, #4]
	str	r6, [r4, #4]
	str	r6, [ip, #4]
	str	r6, [r8, #4]
	str	r6, [sl, #4]
	ldr	ip, [sp, #172]
	mov	sl, r0
	str	r1, [sp, #36]
	str	r3, [sp, #40]
	str	r6, [sp, #104]
	str	r6, [sp, #108]
	beq	.L2
	ldrh	r8, [r1, #0]
	add	r5, sp, #136
	add	r3, r5, r8, asl #1
	ldrh	r0, [r3, #-32]!
	sub	r1, r2, #1
	add	r0, r0, #1
	cmp	r2, #1
	strh	r0, [r3, #0]	@ movhi
	and	r1, r1, #3
	mov	r3, #1
	bls	.L2
	cmp	r1, #0
	beq	.L149
	cmp	r1, #1
	beq	.L136
	cmp	r1, #2
	beq	.L137
	ldr	fp, [sp, #36]
	mov	r3, #2
	ldrh	r4, [fp, r3]
	add	r5, sp, #136
	add	r1, r5, r4, asl #1
	ldrh	r7, [r1, #-32]!
	add	r0, r7, #1
	strh	r0, [r1, #0]	@ movhi
.L137:
	ldr	r4, [sp, #36]
	mov	r5, r3, asl #1
	ldrh	r7, [r4, r5]
	add	r5, sp, #136
	add	r1, r5, r7, asl #1
	ldrh	r0, [r1, #-32]!
	add	r8, r0, #1
	strh	r8, [r1, #0]	@ movhi
	add	r3, r3, #1
.L136:
	ldr	r7, [sp, #36]
	mov	r8, r3, asl #1
	ldrh	r1, [r7, r8]
	add	r0, sp, #136
	add	r1, r0, r1, asl #1
	ldrh	r8, [r1, #-32]!
	add	r3, r3, #1
	add	r0, r8, #1
	cmp	r2, r3
	strh	r0, [r1, #0]	@ movhi
	movhi	r1, r7
	bls	.L2
.L3:
	mov	r7, r3, asl #1
	ldrh	r8, [r1, r7]
	add	r7, sp, #136
	add	r0, r7, r8, asl #1
	ldrh	r6, [r0, #-32]
	add	r8, r3, #1
	mov	r4, r8, asl #1
	ldrh	r4, [r1, r4]
	add	r6, r6, #1
	strh	r6, [r0, #-32]	@ movhi
	add	r0, r7, r4, asl #1
	ldrh	r5, [r0, #-32]
	add	r5, r5, #1
	strh	r5, [r0, #-32]	@ movhi
	add	r4, r1, r8, asl #1
	ldrh	r0, [r4, #2]
	add	r0, r7, r0, asl #1
	ldrh	r8, [r0, #-32]
	add	r4, r8, #1
	strh	r4, [r0, #-32]	@ movhi
	add	r8, r1, r3, asl #1
	ldrh	r0, [r8, #6]
	add	r0, r7, r0, asl #1
	ldrh	r4, [r0, #-32]
	add	r3, r3, #4
	add	r4, r4, #1
	cmp	r2, r3
	strh	r4, [r0, #-32]	@ movhi
	bhi	.L3
.L2:
	ldrh	r3, [sp, #134]
	ldr	r8, [sp, #168]
	cmp	r3, #0
	ldr	r8, [r8, #0]
	movne	fp, #15
	str	r8, [sp, #44]
	strne	fp, [sp, #32]
	bne	.L5
	ldrh	r1, [sp, #132]
	cmp	r1, #0
	movne	r0, #14
	strne	r0, [sp, #32]
	bne	.L5
	ldrh	r1, [sp, #130]
	cmp	r1, #0
	movne	r1, #13
	strne	r1, [sp, #32]
	bne	.L5
	ldrh	r1, [sp, #128]
	cmp	r1, #0
	movne	r4, #12
	strne	r4, [sp, #32]
	bne	.L5
	ldrh	r1, [sp, #126]
	cmp	r1, #0
	movne	r5, #11
	strne	r5, [sp, #32]
	bne	.L5
	ldrh	r1, [sp, #124]
	cmp	r1, #0
	movne	r7, #10
	strne	r7, [sp, #32]
	bne	.L5
	ldrh	r1, [sp, #122]
	cmp	r1, #0
	movne	r8, #9
	strne	r8, [sp, #32]
	bne	.L5
	ldrh	r1, [sp, #120]
	cmp	r1, #0
	movne	fp, #8
	strne	fp, [sp, #32]
	bne	.L5
	ldrh	r1, [sp, #118]
	cmp	r1, #0
	movne	r0, #7
	strne	r0, [sp, #32]
	bne	.L5
	ldrh	r1, [sp, #116]
	cmp	r1, #0
	movne	r1, #6
	strne	r1, [sp, #32]
	bne	.L5
	ldrh	r1, [sp, #114]
	cmp	r1, #0
	movne	r4, #5
	strne	r4, [sp, #32]
	bne	.L5
	ldrh	r1, [sp, #112]
	cmp	r1, #0
	movne	r5, #4
	strne	r5, [sp, #32]
	bne	.L5
	ldrh	r1, [sp, #110]
	cmp	r1, #0
	movne	r7, #3
	strne	r7, [sp, #32]
	bne	.L5
	ldrh	r1, [sp, #108]
	cmp	r1, #0
	bne	.L18
	ldrh	r0, [sp, #106]
	cmp	r0, #0
	bne	.L152
	ldr	r4, [sp, #40]
	ldr	r2, [r4, #0]
	mov	ip, #64
	mov	r3, r2
	strb	ip, [r3], #4
	mov	r1, #1
	strh	r0, [r2, #2]	@ movhi
	strb	r1, [r2, #1]
	strh	r0, [r3, #2]	@ movhi
	strb	r1, [r3, #1]
	strb	ip, [r2, #4]
	ldr	ip, [sp, #40]
	add	r4, r3, #4
	str	r4, [ip, #0]
	ldr	r2, [sp, #168]
	str	r1, [r2, #0]
.L23:
	add	sp, sp, #136
	ldmfd	sp!, {r4, r5, r6, r7, r8, r9, sl, fp}
	bx	lr
.L18:
	mov	r8, #2
	str	r8, [sp, #32]
.L5:
	ldrh	r9, [sp, #106]
	cmp	r9, #0
	movne	fp, #1
	strne	fp, [sp, #16]
	bne	.L22
	ldrh	r1, [sp, #108]
	cmp	r1, #0
	movne	r4, #2
	strne	r4, [sp, #16]
	bne	.L22
	ldrh	r0, [sp, #110]
	cmp	r0, #0
	movne	r5, #3
	strne	r5, [sp, #16]
	bne	.L22
	ldrh	r0, [sp, #112]
	cmp	r0, #0
	movne	r7, #4
	strne	r7, [sp, #16]
	bne	.L22
	ldrh	r0, [sp, #114]
	cmp	r0, #0
	movne	r8, #5
	strne	r8, [sp, #16]
	bne	.L22
	ldrh	r0, [sp, #116]
	cmp	r0, #0
	movne	fp, #6
	strne	fp, [sp, #16]
	bne	.L22
	ldrh	r0, [sp, #118]
	cmp	r0, #0
	movne	r0, #7
	strne	r0, [sp, #16]
	bne	.L22
	ldrh	r0, [sp, #120]
	cmp	r0, #0
	movne	r1, #8
	strne	r1, [sp, #16]
	bne	.L22
	ldrh	r0, [sp, #122]
	cmp	r0, #0
	movne	r4, #9
	strne	r4, [sp, #16]
	bne	.L22
	ldrh	r0, [sp, #124]
	cmp	r0, #0
	movne	r5, #10
	strne	r5, [sp, #16]
	bne	.L22
	ldrh	r0, [sp, #126]
	cmp	r0, #0
	movne	r7, #11
	strne	r7, [sp, #16]
	bne	.L22
	ldrh	r0, [sp, #128]
	cmp	r0, #0
	movne	r8, #12
	strne	r8, [sp, #16]
	bne	.L22
	ldrh	r0, [sp, #130]
	cmp	r0, #0
	movne	fp, #13
	strne	fp, [sp, #16]
	bne	.L22
	ldrh	r0, [sp, #132]
	cmp	r0, #0
	movne	r0, #14
	strne	r0, [sp, #16]
	bne	.L22
	cmp	r3, #0
	moveq	r4, #16
	moveq	r6, #2
	streq	r4, [sp, #16]
	bne	.L153
.L39:
	rsbs	r6, r1, r6, asl #1
	bmi	.L38
	ldrh	r0, [sp, #110]
	rsbs	r6, r0, r6, asl #1
	bmi	.L38
	ldrh	r5, [sp, #112]
	rsbs	r6, r5, r6, asl #1
	bmi	.L38
	ldrh	r4, [sp, #114]
	rsbs	r6, r4, r6, asl #1
	bmi	.L38
	ldrh	r7, [sp, #116]
	str	r7, [sp, #4]
	rsbs	r7, r7, r6, asl #1
	bmi	.L38
	ldrh	r6, [sp, #118]
	rsbs	r7, r6, r7, asl #1
	bmi	.L38
	ldrh	r8, [sp, #120]
	rsbs	r7, r8, r7, asl #1
	str	r7, [sp, #12]
	bmi	.L38
	ldrh	fp, [sp, #122]
	rsbs	r7, fp, r7, asl #1
	str	fp, [sp, #28]
	str	r7, [sp, #12]
	bmi	.L38
	ldrh	fp, [sp, #124]
	rsbs	r7, fp, r7, asl #1
	str	fp, [sp, #24]
	str	r7, [sp, #12]
	bmi	.L38
	ldrh	fp, [sp, #126]
	rsbs	r7, fp, r7, asl #1
	str	fp, [sp, #20]
	str	r7, [sp, #48]
	bmi	.L38
	ldrh	fp, [sp, #128]
	rsbs	r7, fp, r7, asl #1
	str	fp, [sp, #12]
	str	r7, [sp, #48]
	bmi	.L38
	ldrh	fp, [sp, #130]
	rsbs	r7, fp, r7, asl #1
	str	fp, [sp, #52]
	str	r7, [sp, #56]
	bmi	.L38
	ldrh	fp, [sp, #132]
	rsbs	r7, fp, r7, asl #1
	str	fp, [sp, #48]
	str	r7, [sp, #56]
	bmi	.L38
	rsb	r3, r3, r7, asl #1
	cmp	r3, #0
	blt	.L38
	bne	.L154
.L40:
	add	r7, r9, r1
	mov	r3, r7, asl #16
	mov	fp, r3, lsr #16
	add	r1, r0, fp
	mov	r0, r1, asl #16
	mov	r7, r0, lsr #16
	add	r3, r5, r7
	str	fp, [sp, #68]
	mov	fp, r3, asl #16
	mov	r1, fp, lsr #16
	add	r0, r4, r1
	mov	r5, r0, asl #16
	str	r7, [sp, #64]
	ldr	r7, [sp, #4]
	mov	r3, r5, lsr #16
	add	fp, r7, r3
	mov	r4, fp, asl #16
	mov	fp, r4, lsr #16
	str	r1, [sp, #60]
	add	r1, r6, fp
	mov	r6, r1, asl #16
	mov	r6, r6, lsr #16
	add	r0, r8, r6
	mov	r5, r0, asl #16
	ldr	r7, [sp, #28]
	mov	r5, r5, lsr #16
	str	r3, [sp, #56]
	add	r3, r7, r5
	mov	r4, r3, asl #16
	ldr	r1, [sp, #24]
	mov	r4, r4, lsr #16
	add	r8, r1, r4
	mov	r0, r8, asl #16
	ldr	r7, [sp, #20]
	mov	r0, r0, lsr #16
	add	r3, r7, r0
	ldr	r8, [sp, #12]
	mov	r1, r3, asl #16
	mov	r1, r1, lsr #16
	add	r7, r8, r1
	mov	r3, r7, asl #16
	ldr	r7, [sp, #52]
	mov	r3, r3, lsr #16
	add	r8, r7, r3
	mov	r8, r8, asl #16
	ldr	r7, [sp, #48]
	mov	r8, r8, lsr #16
	add	r7, r7, r8
	str	r7, [sp, #12]
	strh	r7, [sp, #102]	@ movhi
	mov	r7, #0	@ movhi
	strh	r7, [sp, #74]	@ movhi
	ldr	r7, [sp, #68]
	strh	r7, [sp, #78]	@ movhi
	ldr	r7, [sp, #64]
	strh	r7, [sp, #80]	@ movhi
	ldr	r7, [sp, #60]
	strh	r7, [sp, #82]	@ movhi
	ldr	r7, [sp, #56]
	cmp	r2, #0
	strh	r9, [sp, #76]	@ movhi
	strh	r7, [sp, #84]	@ movhi
	strh	fp, [sp, #86]	@ movhi
	strh	r6, [sp, #88]	@ movhi
	strh	r5, [sp, #90]	@ movhi
	strh	r4, [sp, #92]	@ movhi
	strh	r0, [sp, #94]	@ movhi
	strh	r1, [sp, #96]	@ movhi
	strh	r3, [sp, #98]	@ movhi
	strh	r8, [sp, #100]	@ movhi
	beq	.L41
	ldr	r4, [sp, #36]
	ldrh	r3, [r4, #0]
	sub	r8, r2, #1
	cmp	r3, #0
	and	r1, r8, #3
	bne	.L155
.L100:
	mov	r3, #1
	cmp	r2, r3
	bls	.L41
	cmp	r1, #0
	beq	.L148
	cmp	r1, r3
	beq	.L134
	cmp	r1, #2
	beq	.L135
	ldr	r8, [sp, #36]
	ldrh	r1, [r8, #2]
	cmp	r1, #0
	beq	.L102
	add	r4, sp, #136
	add	r1, r4, r1, asl #1
	ldrh	r0, [r1, #-64]!
	add	r5, r0, r3
	add	fp, ip, r0, asl #1
	strh	r5, [r1, #0]	@ movhi
	strh	r3, [fp, #0]	@ movhi
.L102:
	add	r3, r3, #1
.L135:
	ldr	fp, [sp, #36]
	mov	r8, r3, asl #1
	ldrh	r1, [fp, r8]
	cmp	r1, #0
	beq	.L105
	add	r7, sp, #136
	add	r1, r7, r1, asl #1
	ldrh	r0, [r1, #-64]!
	add	r4, r0, #1
	add	r5, ip, r0, asl #1
	strh	r4, [r1, #0]	@ movhi
	strh	r3, [r5, #0]	@ movhi
.L105:
	add	r3, r3, #1
.L134:
	ldr	r8, [sp, #36]
	mov	fp, r3, asl #1
	ldrh	r1, [r8, fp]
	cmp	r1, #0
	beq	.L108
	add	fp, sp, #136
	add	r1, fp, r1, asl #1
	ldrh	r0, [r1, #-64]!
	add	r4, r0, #1
	add	r5, ip, r0, asl #1
	strh	r4, [r1, #0]	@ movhi
	strh	r3, [r5, #0]	@ movhi
.L108:
	add	r3, r3, #1
	cmp	r2, r3
	bls	.L41
.L148:
	ldr	r1, [sp, #36]
.L43:
	mov	r6, r3, asl #1
	ldrh	r4, [r1, r6]
	add	r7, r3, #1
	cmp	r4, #0
	mov	r0, r7, asl #1
	beq	.L42
	add	r5, sp, #136
	add	r8, r5, r4, asl #1
	ldrh	r6, [r8, #-64]
	add	fp, r6, #1
	mov	r5, r6, asl #1
	strh	fp, [r8, #-64]	@ movhi
	strh	r3, [r5, ip]	@ movhi
.L42:
	ldrh	r3, [r1, r0]
	cmp	r3, #0
	beq	.L111
	add	r4, sp, #136
	add	r3, r4, r3, asl #1
	ldrh	r5, [r3, #-64]
	add	r8, r5, #1
	mov	r6, r5, asl #1
	strh	r8, [r3, #-64]	@ movhi
	strh	r7, [r6, ip]	@ movhi
.L111:
	add	r4, r7, #1
	mov	fp, r4, asl #1
	ldrh	r6, [r1, fp]
	add	r0, r7, #2
	cmp	r6, #0
	mov	r5, r0, asl #1
	add	r3, r7, #3
	beq	.L113
	add	fp, sp, #136
	add	r6, fp, r6, asl #1
	ldrh	r7, [r6, #-64]
	add	r8, r7, #1
	mov	r7, r7, asl #1
	strh	r4, [r7, ip]	@ movhi
	strh	r8, [r6, #-64]	@ movhi
.L113:
	ldrh	r4, [r1, r5]
	cmp	r4, #0
	beq	.L115
	add	r6, sp, #136
	add	r4, r6, r4, asl #1
	ldrh	r8, [r4, #-64]
	add	r6, r8, #1
	mov	r5, r8, asl #1
	strh	r0, [r5, ip]	@ movhi
	strh	r6, [r4, #-64]	@ movhi
.L115:
	cmp	r2, r3
	bhi	.L43
.L41:
	cmp	sl, #0
	beq	.L45
	cmp	sl, #1
	beq	.L156
	ldr	r4, .L158
	mvn	r7, #0
	str	r7, [sp, #48]
	add	r7, r4, #64
	str	r4, [sp, #60]
	str	r7, [sp, #64]
.L47:
	ldr	r8, [sp, #44]
	ldr	r2, [sp, #32]
	ldr	fp, [sp, #16]
	cmp	r2, r8
	movcs	r2, r8
	cmp	fp, r2
	movcc	fp, r2
	mov	r2, #1
	mov	r0, r2, asl fp
	cmp	sl, r2
	movne	sl, #0
	moveq	sl, #1
	mov	r1, #1440
	ldr	r5, [sp, #40]
	str	fp, [sp, #28]
	str	r0, [sp, #44]
	str	sl, [sp, #56]
	add	r3, r1, #15
	mov	r1, sl
	ldr	r4, [r5, #0]
	cmp	r0, r3
	movls	r1, #0
	andhi	r1, r1, #1
	sub	r5, r0, #1
	cmp	r1, #0
	str	r4, [sp, #20]
	str	r5, [sp, #52]
	bne	.L48
	mvn	r7, #0
	str	ip, [sp, #12]
	ldr	sl, [sp, #16]
	str	r7, [sp, #24]
	mov	r4, fp
	mov	fp, r1
.L49:
	ldrh	r5, [ip, #0]
	ldr	ip, [sp, #48]
	cmp	ip, r5
	rsb	ip, fp, sl
	and	r6, ip, #255
	movgt	r8, #0
	bgt	.L51
	ldrlt	r2, [sp, #64]
	ldrlt	r7, [sp, #60]
	movlt	r3, r5, asl #1
	ldrlth	r5, [r2, r3]
	ldrltb	r8, [r7, r3]	@ zero_extendqisi2
	movge	r8, #96
	movge	r5, #0
.L51:
	mov	r0, #1
	mov	r0, r0, asl r4
	mov	r3, #1
	mov	ip, r3, asl ip
	add	r9, r0, r1, lsr fp
	ldr	r7, [sp, #20]
	rsb	r3, ip, r9
	rsb	r2, ip, ip, asl #30
	str	r0, [sp, #16]
	mov	r9, r2, asl #2
	add	r3, r7, r3, asl #2
	rsb	r2, ip, r0
	rsb	r7, ip, #0
	mov	r0, r1
.L53:
	add	r2, r2, r7
	add	r1, r2, ip
	cmp	r1, #0
	strh	r5, [r3, #2]	@ movhi
	strb	r6, [r3, #1]
	strb	r8, [r3], r9
	bne	.L53
	mov	r1, r0
	mov	r8, #1
	sub	r0, sl, #1
	mov	r3, r8, asl r0
	tst	r3, r1
	beq	.L54
.L77:
	mov	r3, r3, lsr #1
	tst	r3, r1
	bne	.L77
.L54:
	cmp	r3, #0
	subne	r2, r3, #1
	andne	r1, r2, r1
	moveq	r1, r3
	addne	r1, r1, r3
	add	ip, sp, #136
	mov	r3, sl, asl #1
	add	r5, r3, ip
	ldrh	r2, [r5, #-32]
	sub	ip, r2, #1
	mov	r5, ip, asl #16
	movs	r2, r5, lsr #16
	bne	.L59
	ldr	r5, [sp, #32]
	cmp	sl, r5
	beq	.L60
	ldr	r7, [sp, #12]
	ldrh	sl, [r7, #2]
	ldr	r8, [sp, #36]
	mov	r0, sl, asl #1
	ldrh	sl, [r0, r8]
.L59:
	ldr	r0, [sp, #28]
	add	ip, sp, #136
	add	r3, r3, ip
	cmp	sl, r0
	strh	r2, [r3, #-32]	@ movhi
	ldrls	r0, [sp, #24]
	bls	.L62
	ldr	r2, [sp, #52]
	ldr	r3, [sp, #24]
	and	r0, r1, r2
	cmp	r0, r3
	beq	.L62
	ldr	r4, [sp, #28]
	cmp	fp, #0
	moveq	fp, r4
	rsb	r4, fp, sl
	ldr	r3, [sp, #32]
	add	r2, r4, fp
	cmp	r3, r2
	bls	.L64
	mov	r2, r2, asl #1
	add	r7, r2, ip
	ldrh	r5, [r7, #-32]
	mov	r7, #1
	rsb	r5, r5, r7, asl r4
	cmp	r5, #0
	ble	.L64
	add	r8, fp, r7
	add	r3, r8, r4
	ldr	r8, [sp, #32]
	rsb	r6, r3, r8
	add	ip, sp, #104
	ands	r6, r6, #3
	add	r2, ip, r2
	beq	.L65
	cmp	r8, r3
	mov	r7, r8
	add	r4, r4, #1
	bls	.L64
	ldrh	ip, [r2, #2]!
	rsb	r5, ip, r5, asl #1
	cmp	r5, #0
	add	r3, r3, #1
	ble	.L64
	cmp	r6, #1
	beq	.L144
	cmp	r6, #2
	beq	.L130
	ldrh	ip, [r2, #2]!
	rsb	r5, ip, r5, asl #1
	cmp	r5, #0
	add	r4, r4, #1
	add	r3, r3, #1
	ble	.L64
.L130:
	ldrh	ip, [r2, #2]!
	rsb	r5, ip, r5, asl #1
	cmp	r5, #0
	add	r4, r4, #1
	add	r3, r3, #1
	ble	.L64
.L144:
	ldr	r8, [sp, #32]
.L65:
	add	r4, r4, #1
	cmp	r8, r3
	mov	ip, r2
	add	r3, r3, #4
	mov	r6, r4
	bls	.L64
	ldrh	r7, [ip, #2]!
	rsb	r5, r7, r5, asl #1
	cmp	r5, #0
	ble	.L64
	ldrh	r7, [ip, #2]!
	rsb	r5, r7, r5, asl #1
	cmp	r5, #0
	add	r4, r4, #1
	ble	.L64
	ldrh	r7, [ip, #2]
	rsb	r5, r7, r5, asl #1
	cmp	r5, #0
	add	ip, r2, #6
	add	r4, r6, #2
	add	r2, r2, #8
	ble	.L64
	ldrh	r4, [ip, #2]
	rsb	r5, r4, r5, asl #1
	cmp	r5, #0
	add	r4, r6, #3
	bgt	.L65
.L64:
	ldr	ip, [sp, #44]
	mov	r3, #1
	add	r2, ip, r3, asl r4
	mov	r5, #1440
	ldr	r3, [sp, #56]
	add	r5, r5, #15
	cmp	r2, r5
	movls	r3, #0
	andhi	r3, r3, #1
	cmp	r3, #0
	str	r2, [sp, #44]
	bne	.L48
	ldr	r7, [sp, #40]
	ldr	r3, [r7, #0]
	strb	r4, [r3, r0, asl #2]
	ldr	r6, [r7, #0]
	ldr	r9, [sp, #28]
	mov	r3, r0, asl #2
	add	r2, r6, r3
	strb	r9, [r2, #1]
	add	r5, sp, #16
	ldmia	r5, {r5, ip}	@ phole ldm
	ldr	r2, [r7, #0]
	add	r8, ip, r5, asl #2
	rsb	r6, r2, r8
	add	r3, r2, r3
	mov	r9, r6, asr #2
	str	r8, [sp, #20]
	strh	r9, [r3, #2]	@ movhi
.L62:
	ldr	r9, [sp, #12]
	add	r8, r9, #2
	str	r8, [sp, #12]
	str	r0, [sp, #24]
	mov	ip, r8
	b	.L49
.L154:
	ldr	fp, [sp, #32]
	rsbs	r3, sl, #1
	movcc	r3, #0
	cmp	fp, #1
	orrne	r3, r3, #1
	cmp	r3, #0
	beq	.L40
.L38:
	mvn	r0, #0
	b	.L23
.L153:
	mov	r1, #15
	str	r1, [sp, #16]
.L22:
	rsbs	r6, r9, #2
	ldrplh	r1, [sp, #108]
	bpl	.L39
	mvn	r0, #0
	b	.L23
.L45:
	mov	r2, #19
	str	ip, [sp, #60]
	str	ip, [sp, #64]
	str	r2, [sp, #48]
	b	.L47
.L155:
	add	r4, sp, #136
	add	r3, r4, r3, asl #1
	ldrh	r5, [r3, #-64]!
	mov	fp, #0	@ movhi
	add	r0, r5, #1
	add	r8, ip, r5, asl #1
	strh	r0, [r3, #0]	@ movhi
	strh	fp, [r8, #0]	@ movhi
	b	.L100
.L48:
	mov	r0, #1
	b	.L23
.L149:
	ldr	r1, [sp, #36]
	b	.L3
.L60:
	cmp	r1, #0
	beq	.L67
	ldr	sl, [sp, #28]
	and	r7, sl, #255
	sub	r8, sl, #1
	ldr	sl, [sp, #20]
	mov	r4, #64
	mov	r5, #1
.L75:
	cmp	fp, #0
	beq	.L69
	ldr	r2, [sp, #52]
	ldr	ip, [sp, #24]
	and	r3, r1, r2
	cmp	r3, ip
	ldrne	r3, [sp, #40]
	ldrne	sl, [r3, #0]
	movne	r6, r7
	movne	fp, #0
	movne	r0, r8
.L69:
	mov	ip, r1, lsr fp
	mov	r3, r5, asl r0
	add	r2, sl, ip, asl #2
	strb	r4, [sl, ip, asl #2]
	tst	r3, r1
	mov	ip, #0	@ movhi
	strb	r6, [r2, #1]
	strh	ip, [r2, #2]	@ movhi
	beq	.L71
.L76:
	mov	r3, r3, lsr #1
	tst	r3, r1
	bne	.L76
.L71:
	cmp	r3, #0
	bne	.L157
.L67:
	ldr	ip, [sp, #40]
	ldr	r1, [sp, #44]
	ldr	r0, [ip, #0]
	add	r2, r0, r1, asl #2
	str	r2, [ip, #0]
	ldr	r0, [sp, #28]
	ldr	r3, [sp, #168]
	str	r0, [r3, #0]
	mov	r0, #0
	b	.L23
.L157:
	sub	r2, r3, #1
	and	r1, r2, r1
	adds	r1, r1, r3
	bne	.L75
	b	.L67
.L156:
	ldr	r1, .L158+4
	mov	r3, #256
	add	r0, r1, #64
	str	r1, [sp, #60]
	str	r3, [sp, #48]
	str	r0, [sp, #64]
	b	.L47
.L152:
	mov	r4, #1
	str	r4, [sp, #32]
	mov	r9, r0
	str	r4, [sp, #16]
	b	.L22
.L159:
	.align	2
.L158:
	.word	.LANCHOR0+128
	.word	.LANCHOR0-514
	.size	inflate_table, .-inflate_table
	.hidden	inflate_copyright
	.global	inflate_copyright
	.section	.rodata
	.align	2
	.set	.LANCHOR0,. + 0
	.type	lext.1924, %object
	.size	lext.1924, 62
lext.1924:
	.short	16
	.short	16
	.short	16
	.short	16
	.short	16
	.short	16
	.short	16
	.short	16
	.short	17
	.short	17
	.short	17
	.short	17
	.short	18
	.short	18
	.short	18
	.short	18
	.short	19
	.short	19
	.short	19
	.short	19
	.short	20
	.short	20
	.short	20
	.short	20
	.short	21
	.short	21
	.short	21
	.short	21
	.short	16
	.short	201
	.short	196
	.space	2
	.type	lbase.1923, %object
	.size	lbase.1923, 62
lbase.1923:
	.short	3
	.short	4
	.short	5
	.short	6
	.short	7
	.short	8
	.short	9
	.short	10
	.short	11
	.short	13
	.short	15
	.short	17
	.short	19
	.short	23
	.short	27
	.short	31
	.short	35
	.short	43
	.short	51
	.short	59
	.short	67
	.short	83
	.short	99
	.short	115
	.short	131
	.short	163
	.short	195
	.short	227
	.short	258
	.short	0
	.short	0
	.space	2
	.type	dext.1926, %object
	.size	dext.1926, 64
dext.1926:
	.short	16
	.short	16
	.short	16
	.short	16
	.short	17
	.short	17
	.short	18
	.short	18
	.short	19
	.short	19
	.short	20
	.short	20
	.short	21
	.short	21
	.short	22
	.short	22
	.short	23
	.short	23
	.short	24
	.short	24
	.short	25
	.short	25
	.short	26
	.short	26
	.short	27
	.short	27
	.short	28
	.short	28
	.short	29
	.short	29
	.short	64
	.short	64
	.type	dbase.1925, %object
	.size	dbase.1925, 64
dbase.1925:
	.short	1
	.short	2
	.short	3
	.short	4
	.short	5
	.short	7
	.short	9
	.short	13
	.short	17
	.short	25
	.short	33
	.short	49
	.short	65
	.short	97
	.short	129
	.short	193
	.short	257
	.short	385
	.short	513
	.short	769
	.short	1025
	.short	1537
	.short	2049
	.short	3073
	.short	4097
	.short	6145
	.short	8193
	.short	12289
	.short	16385
	.short	24577
	.short	0
	.short	0
	.type	inflate_copyright, %object
	.size	inflate_copyright, 47
inflate_copyright:
	.ascii	" inflate 1.2.3 Copyright 1995-2005 Mark Adler \000"
	.ident	"GCC: (Sourcery G++ Lite 2010q1-188) 4.4.1"
